@echo off
@setlocal enableextensions
@cd /d "%~dp0"
keytool -importcert -alias gkcomarchserversca -file "GKCOMARCHSERVERSCA.crt" -keystore "%JAVA_HOME%\jre\lib\security\cacerts" -noprompt -storepass changeit
pause