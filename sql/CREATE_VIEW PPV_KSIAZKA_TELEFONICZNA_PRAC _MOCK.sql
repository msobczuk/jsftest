

 CREATE OR REPLACE FORCE VIEW "EGADM1"."PPV_KSIAZKA_TELEFONICZNA_PRAC" as 
select distinct
  --(select PLO_ID from PP_KSIAZKA_LOKALIZACJE where plo_wartosc = pkp_kraj) as KTP_PKP_KRAJ_ID,
  prc_id KTP_PRC_ID ,
  INITCAP(prc_imie) KTP_prc_imie,
  INITCAP(prc_nazwisko) KTP_prc_nazwisko,
  (SELECT tl_numer from ckk_telefony where prc_id = tl_prc_id and TL_F_DOMYSLNY = 'T' and TL_TYP = 'E') KTP_tl_numer_email,
  (SELECT tl_numer from ckk_telefony where prc_id = tl_prc_id and TL_F_DOMYSLNY = 'T' and TL_TYP = 'T') KTP_tl_numer_telefon,
  (SELECT tl_numer from ckk_telefony where prc_id = tl_prc_id and TL_F_DOMYSLNY = 'T' and TL_TYP = 'F') KTP_tl_numer_fax,

  --zat_ob_id KTP_ob_jo_id,
  --concat(zat_ob_id, concat(' - ', ob_nazwa)) KTP_ob_jednostka_organizacyjna,
  DZLLIST                                      KTP_ob_jednostka_organizacyjna,

  PKP_STANOWISKO KTP_PKP_stanowisko,
  prc_aktywny KTP_prc_czy_aktywny,

  PKP_UWAGI     KTP_PKP_UWAGI,

  PKP_KRAJ      KTP_PKP_KRAJ,
  PKP_MIASTO    KTP_PKP_MIASTO,
  PKP_BUDYNEK   KTP_PKP_BUDYNEK,
  PKP_PIETRO    KTP_PKP_PIETRO,
  PKP_POKOJ     KTP_PKP_POKOJ,
 
  PKP_KRAJ_ID       KTP_PKP_KRAJ_ID,
  PKP_MIASTO_ID     KTP_PKP_MIASTO_ID,
  PKP_BUDYNEK_ID    KTP_PKP_BUDYNEK_ID,
  PKP_PIETRO_ID     KTP_PKP_PIETRO_ID,
  PKP_POKOJ_ID      KTP_PKP_POKOJ_ID          
  
from ek_pracownicy,
      pp_ksiazka_kontaktowa
      , (  select Z.ZAT_PRC_ID,  wm_concat(distinct Z.ZAT_OB_ID) DZLLIST
             from EGADM1.EK_ZATRUDNIENIE Z
            where zat_typ_umowy = 0
              AND zat_data_zmiany <= LAST_DAY (sysdate)
              AND NVL (zat_data_do, sysdate) >= sysdate
            group by Z.ZAT_PRC_ID
        ) DZL
               
WHERE  EK_PRACOWNICY.PRC_ID = PKP_PRC_ID (+)
   AND EK_PRACOWNICY.PRC_ID = DZL.ZAT_PRC_ID(+)

ORDER BY INITCAP(prc_nazwisko), INITCAP(prc_imie), EK_PRACOWNICY.PRC_ID      
