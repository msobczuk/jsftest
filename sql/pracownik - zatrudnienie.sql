select 
    ZAT_ID              pp_z_id ,
    zat_data_zmiany     pp_z_datazmiany , 
    zat_data_do         pp_z_datado , 
    zat_stawka          pp_z_stawka , 
    rv_meaning          pp_z_typ , 
      ( select
        rv_meaning
        from  ek_ref_codes
        where rv_domain = 'EK_RODZAJ_ZAT'
        and rv_low_value=zat_f_rodzaj)     pp_z_rodzaj , 
      
      ( select stn_nazwa
        from zpt_stanowiska
        where stn_id = zat_stn_id)         pp_z_stanowisko
          
from ek_zatrudnienie, ek_ref_codes
where zat_prc_id =  100220 --?
    and zat_typ_umowy = rv_low_value
    and rv_domain = 'TYP_UMOWY'
    
order by zat_typ_umowy, zat_data_zmiany desc 