CREATE OR REPLACE TRIGGER PPADM.ppg_wosp_usun_zalaczniki
BEFORE DELETE 
	ON PPADM.PPT_OPS_OPIS_STANOWISKA
	FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/ppg_wosp_usun_zalaczniki.trg 3 1.0.3 09.03.18 09:07 MGOLDA                                  $
-- $Revision:: 3                                                                                                                                              $
-- $Workfile:: ppg_wosp_usun_zalaczniki.trg                                                                                                                   $
-- $Modtime:: 09.03.18 09:07                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE
	v_wosp_id NUMBER(10,0);
BEGIN
	v_wosp_id := :old.wosp_id;
	DELETE FROM zkt_zalaczniki
	WHERE ZAL_WOSP_ID = v_wosp_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_wosp_usun_zalaczniki');
END;
/

