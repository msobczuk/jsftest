CREATE OR REPLACE VIEW PPADM.PPV_WARTOSCI_DOMYSLNE
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/views/PPV_WARTOSCI_DOMYSLNE.vw 1 1.0.1 02.03.18 17:18 DZIOBEK                                        $
-- $Revision:: 1                                                                                                                                              $
-- $Workfile:: PPV_WARTOSCI_DOMYSLNE.vw                                                                                                                       $
-- $Modtime:: 02.03.18 17:18                                                                                                                                  $
-- $Author:: DZIOBEK                                                                                                                                          $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
SELECT   APL.APL_NAZWA wdv_apl_nazwa,
            GWD.GWD_NAZWA wdv_gwd_nazwa,
            TWD.TWD_NAZWA wdv_twd_nazwa,
            WD.WD_WARTOSC wdv_wd_wartosc,
            wd.WD_ID wdv_wd_id,
            wd.WD_STAN wdv_wd_stan,
            wd.WD_UZYTKOWNIK wdv_wd_uzytkownik,
            gwd.GWD_ID wdv_gwd_id,
            gwd.GWD_POZIOM_DOSTEPU wdv_gwd_poziom_dostepu,
            gwd.GWD_OPIS wdv_gwd_opis,
            twd.TWD_ID wdv_twd_id,
            twd.TWD_POZIOM_DOSTEPU wdv_twd_poziom_dostepu,
            twd.TWD_OPIS wdv_twd_opis
     FROM            CSS_WARTOSCI_DOMYSLNE WD
                  JOIN
                     CSS_TYPY_WD TWD
                  ON WD.WD_TWD_ID = TWD.TWD_ID
                     --AND wd.WD_frm_id = TWD.TWD_FRM_ID
               JOIN
                  CSS_GRUPY_WD gwd
               ON WD.WD_GWD_ID = GWD.GWD_ID 
               --AND wd.WD_frm_id = gWD.gWD_FRM_ID
            JOIN
               EAADM.EAT_APLIKACJE APL
            ON GWD.gwd_app_id = apl.apl_id
    --WHERE   wd_frm_id = 1;;
/


