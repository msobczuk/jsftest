CREATE OR REPLACE TRIGGER PPADM.ppg_nab_usun_zalaczniki
BEFORE DELETE 
	ON PPADM.ppt_rek_nabory
	FOR EACH ROW
	---------------------------------------------------------------------------------------------------------------------------------------------------------------
	-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/ppg_nab_usun_zalaczniki.trg 3 1.0.3 09.03.18 09:07 MGOLDA                                   $
	-- $Revision:: 3                                                                                                                                              $
	-- $Workfile:: ppg_nab_usun_zalaczniki.trg                                                                                                                    $
	-- $Modtime:: 09.03.18 09:07                                                                                                                                  $
	-- $Author:: MGOLDA                                                                                                                                           $
	---------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE
	v_nab_id NUMBER(10,0);
BEGIN
	v_nab_id := :old.nab_id;
	DELETE FROM zkt_zalaczniki
	WHERE zal_pp_nab_id = v_nab_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_nab_usun_zalaczniki');
END;
/

