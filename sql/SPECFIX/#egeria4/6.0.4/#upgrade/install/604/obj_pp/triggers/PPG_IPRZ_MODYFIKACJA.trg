CREATE OR REPLACE TRIGGER PPADM.PPG_IPRZ_MODYFIKACJA 
AFTER UPDATE ON PPADM.PPT_IPR_IPRZ 
FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/PPG_IPRZ_MODYFIKACJA.trg 3 1.0.3 09.03.18 09:06 MGOLDA                                      $
-- $Revision:: 3                                                                                                                                              $
-- $Workfile:: PPG_IPRZ_MODYFIKACJA.trg                                                                                                                       $
-- $Modtime:: 09.03.18 09:06                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN
  wstaw_wiadomosc(:NEW.IPRZ_PRC_ID,'Twoj indywidualny program rozwoju zawodowego zostal zmieniony .', sysdate);
END;
/


