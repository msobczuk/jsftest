CREATE OR REPLACE TRIGGER PPADM.PPG_SZK_AU_SYNCHRONIZACJA AFTER UPDATE ON PPADM.PPT_SZK_SZKOLENIA FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/PPG_SZK_AU_SYNCHRONIZACJA.trg 1 1.0.1 12.03.18 13:29 MGOLDA                                 $
-- $Revision:: 1                                                                                                                                              $
-- $Workfile:: PPG_SZK_AU_SYNCHRONIZACJA.trg                                                                                                                  $
-- $Modtime:: 12.03.18 13:29                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE

CURSOR cur_uczestnicy IS
        SELECT usz_id, usz_prc_id, USZ_EK_SZK_ID
        FROM PPT_SZK_UCZESTNICY_SZKOLEN
        WHERE usz_szk_id = :NEW.szk_id AND USZ_OBECNOSC = 'T';

v_kwa_id number(10);
v_nowe_szkol_id number(10);
v_nazwa_szkol VARCHAR2(100);	
v_rodzaj_szkolenia number(3);
v_organizator VARCHAR2(240);
uczestnik cur_uczestnicy%ROWTYPE;
po_r_szk EGADM1.EK_PCK_SZK.t_r_szk;
BEGIN
  IF UPDATING  THEN
	IF(:NEW.SZK_STATUS = 'ZREAL') THEN
	
	OPEN cur_uczestnicy;
    LOOP
        FETCH cur_uczestnicy INTO uczestnik;
        EXIT WHEN cur_uczestnicy%NOTFOUND;
       
	   BEGIN
			select kwa_id into v_kwa_id from ek_kwalifikacje where kwa_prc_id = uczestnik.usz_prc_id;
      select EK_SEQ_SZK.nextval into v_nowe_szkol_id from dual;
			
			BEGIN
				select of_nazwa, to_number(of_rodzaj), KL_SKROT 
				into v_nazwa_szkol, v_rodzaj_szkolenia, v_organizator 
				from ppt_szk_oferty_szkolen 
				JOIN CKK_KLIENCI on kl_kod = of_kl_kod
				where of_id = :NEW.SZK_OF_ID;

        po_r_szk.szk_id := v_nowe_szkol_id;
        po_r_szk.szk_kwa_id := v_kwa_id;
        po_r_szk.szk_nazwa  := v_nazwa_szkol; 
        po_r_szk.szk_rodzaj  := v_rodzaj_szkolenia; 
        po_r_szk.szk_data_do := :NEW.SZK_DATA_DO;
        po_r_szk.szk_data_od := :NEW.SZK_DATA_OD;
        po_r_szk.szk_organizator  := v_organizator;       
        po_r_szk.szk_data_waznosci := :NEW.SZK_DATA_WAZNOSCI;

        EK_PCK_SZK.wstaw(po_r_szk);
		WSTAW_WIADOMOSC(uczestnik.usz_prc_id, 'SZKOLENIE WYGASLO', :NEW.SZK_DATA_WAZNOSCI);
				--insert into ek_szkolenia (szk_id, szk_data_od, szk_data_do, szk_data_waznosci, szk_nazwa, szk_rodzaj, szk_organizator, szk_kwa_id, SZK_FRM_ID) values 
        --(v_nowe_szkol_id, :NEW.SZK_DATA_OD, :NEW.SZK_DATA_DO, :NEW.SZK_DATA_WAZNOSCI, v_nazwa_szkol, v_rodzaj_szkolenia, v_organizator, v_kwa_id, 1);
				IF(nvl(to_char(uczestnik.USZ_EK_SZK_ID), 'null') <> 'null') THEN

            EK_PCK_SZK.usun(uczestnik.USZ_EK_SZK_ID);
        
					--DELETE FROM ek_szkolenia where szk_id = uczestnik.USZ_EK_SZK_ID;
					
				END IF;
        
        --insert into PPT_ERR_LOG (ERR_ID, ERR_TRESC_BLEDU) values (PPS_ERR.NEXTVAL, 'Przed updatem - uczestnik ' || uczestnik.USZ_ID || '; v_nowe_szkol_id: ' || v_nowe_szkol_id || ';'  );

				UPDATE PPT_SZK_UCZESTNICY_SZKOLEN set USZ_EK_SZK_ID = v_nowe_szkol_id where USZ_ID = uczestnik.USZ_ID;

        --insert into PPT_ERR_LOG (ERR_ID, ERR_TRESC_BLEDU) values (PPS_ERR.NEXTVAL, 'Po update - uczestnik ' || uczestnik.USZ_ID || '; v_nowe_szkol_id: ' || v_nowe_szkol_id || ';'  );

			EXCEPTION
				WHEN NO_DATA_FOUND THEN
					insert into PPT_ERR_LOG (ERR_ID, ERR_TRESC_BLEDU) values (PPS_ERR.NEXTVAL, 'Blad odczytu danych o nazwie, rodzaju i organizatorze szkolenia');
				NULL;
			END;
			
	   EXCEPTION
		WHEN NO_DATA_FOUND THEN
			insert into PPT_ERR_LOG (ERR_ID, ERR_TRESC_BLEDU) values (PPS_ERR.NEXTVAL, 'Brak wpisu w EK_KWALIFIKACJE dla pracownika o id ' || uczestnik.usz_prc_id);
			NULL;
		END;  
	   
    END LOOP;
    CLOSE cur_uczestnicy;
		
	
	END IF;
  END IF;
END;
/


