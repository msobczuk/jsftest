CREATE OR REPLACE VIEW PPADM.PPV_KONTA_SZCZEGOLY
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/views/PPV_KONTA_SZCZEGOLY.vw 1 1.0.1 02.03.18 17:17 DZIOBEK                                          $
-- $Revision:: 1                                                                                                                                              $
-- $Workfile:: PPV_KONTA_SZCZEGOLY.vw                                                                                                                         $
-- $Modtime:: 02.03.18 17:17                                                                                                                                  $
-- $Author:: DZIOBEK                                                                                                                                          $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
SELECT
			-- $Revision:: 1       $--
			ekv_konta_pracownikow.knt_prc_id pp_prc_id,
            knd_id pp_kb_id,
            drl_kod pp_kb_listid,
            drl_nazwa pp_kb_nazlist,
            EK_KONTA_PLAC.knp_kwota pp_kb_kwota,
            NVL2(EK_KONTA_PLAC.knp_procent,knp_procent||'%',knp_procent) pp_kb_proc,
            knp_f_calosc pp_kb_fcalosc
     FROM   ekv_konta_pracownikow, ek_konta_plac, ek_def_rodzajow_list
    WHERE       EK_KONTA_PLAC.knp_id IS NOT NULL
            AND ekv_konta_pracownikow.knd_f_aktualne = 'T'
            AND EK_KONTA_PLAC.knp_knt_id(+) = knd_knt_id
            AND drl_kod(+) = knp_drl_kod
/


