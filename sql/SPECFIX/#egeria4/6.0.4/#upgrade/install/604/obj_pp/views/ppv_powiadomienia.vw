CREATE OR REPLACE VIEW PPADM.PPV_POWIADOMIENIA
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/views/PPV_POWIADOMIENIA.vw 1 1.0.1 02.03.18 17:18 DZIOBEK                                            $
-- $Revision:: 1                                                                                                                                              $
-- $Workfile:: PPV_POWIADOMIENIA.vw                                                                                                                           $
-- $Modtime:: 02.03.18 17:18                                                                                                                                  $
-- $Author:: DZIOBEK                                                                                                                                          $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
SELECT   UZT.uzt_nazwa pp_nazwa_uzytkownika,
            uzt.UZT_PRC_ID PP_PRC_ID,
            WDM_ID PP_WDM_ID,                     -- identyfikator wiadomoL?ci
            NVL (WDM_DATA_WYSLANIA, WDM_DATA_WSTAWIENIA)
               PP_DATA_POWIADOMIENIA,          -- data i godzina powiadomienia
            WDM_F_PRZECZYTANA PP_F_PRZECZYTANA, -- flaga czy powiadomienie odczytane N/T (nie/tak)
            WDM_TYP PP_TYP, -- typ powiadomienia E/A/M (Egeria/Mail/SMS) - zawsze E
            WDM_TEMAT PP_TEMAT,                         -- temat powiadomienia
            WDM_TRESC PP_TRESC                        -- treL?A? powiadomienia
     FROM      eat_wiadomosci wdm
            JOIN
               eat_uzytkownicy uzt
            ON uzt.UZT_NAZWA = wdm.WDM_UZT_NAZWA
    WHERE   wdm_typ = 'E'
/


