CREATE OR REPLACE TRIGGER PPADM.PPG_URPL_AIUDR_SYNCHRONIZACJA AFTER INSERT OR UPDATE OR DELETE ON PPADM.PPT_ABS_URLOPY_PLANOWANE FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/PPG_URPL_AIUDR_SYNCHRONIZACJA.trg 3 1.0.3 09.03.18 09:07 MGOLDA                             $
-- $Revision:: 3                                                                                                                                              $
-- $Workfile:: PPG_URPL_AIUDR_SYNCHRONIZACJA.trg                                                                                                              $
-- $Modtime:: 09.03.18 09:07                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE

BEGIN

  IF nvl(SYS_CONTEXT('EA_GLOBALS', 'APP_SYMBOL'), 'null')='PP' THEN 
    IF INSERTING  THEN
	
    INSERT INTO EGADM1.EK_URLOPY_PLANOWANE (URP_DATA_DO, URP_DATA_OD, URP_PRC_ID, URP_ID, URP_DG_KOD) VALUES (:NEW.URPL_DATA_DO, :NEW.URPL_DATA_OD, :NEW.URPL_PRC_ID, EGADM1.EK_SEQ_URP.NEXTVAL, 'UR_PLAN');
	
    END IF;
    IF UPDATING  THEN
       UPDATE EGADM1.EK_URLOPY_PLANOWANE SET URP_DATA_DO = :NEW.URPL_DATA_DO, URP_DATA_OD = :NEW.URPL_DATA_OD
		where URP_PRC_ID = :NEW.URPL_PRC_ID
		and URP_DATA_OD = :NEW.URPL_DATA_OD;
    END IF;
    IF DELETING  THEN
		DELETE  FROM EGADM1.EK_URLOPY_PLANOWANE where URP_PRC_ID = :OLD.URPL_PRC_ID
			and URP_DATA_OD = :OLD.URPL_DATA_OD;
	
    END IF;
  END IF;
END;
/


