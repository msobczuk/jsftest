create or replace PROCEDURE       ppadm.zsz_dopisz_do_usz(p_zsz_id number) as
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/procedures/eg.ppp_zsz_dopisz_do_usz.prc 1 1.0.1 19.03.18 09:12 DZIOBEK                                                  $
-- $Revision:: 1                                                                                                                                                                 $
-- $Workfile:: eg.ppp_zsz_dopisz_do_usz.prc                                                                                                                                      $
-- $Modtime:: 19.03.18 09:12                                                                                                                                                     $
-- $Author:: DZIOBEK                                                                                                                                                             $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    v_prc_id NUMBER(10);
    v_str varchar(250);
    v_cnt NUMBER(10);
    v_usz_id NUMBER(10);
begin 
   PPADM.PLOG('start zsz_dopisz_do_usz /  ' || p_zsz_id);
    select zsz_prc_id into v_prc_id from PPADM.PPT_SZK_ZAPISY_SZKOLEN where ZSZ_ID = p_zsz_id;
--    DBMS_OUTPUT.put_line('v_prc_id: ' || v_prc_id);
    FOR r in (select * from PPADM.PPT_SZK_PLAN_INDYW where PIS_ZSZ_ID=p_zsz_id and not PIS_SZK_ID is null) LOOP
        select count(*) into v_cnt from PPADM.PPT_SZK_UCZESTNICY_SZKOLEN where USZ_PRC_ID = v_prc_id and USZ_SZK_ID = r.PIS_SZK_ID;
--        DBMS_OUTPUT.PUT_LINE('---- v_cnt: ' || v_cnt || '; r.PIS_SZK_ID: ' || r.PIS_SZK_ID || '; v_prc_id: ' || v_prc_id  );
        if v_cnt = 0 then 
--            DBMS_OUTPUT.PUT_LINE ('wstaw usz dla r.PIS_SZK_ID: ' || r.PIS_SZK_ID );
            select nvl(PKEY_value,0)+1 into v_usz_id from PPT_JPA_GENKEYS 
            where PKEY_TABLE = 'PPT_SZK_UCZESTNICY_SZKOLEN';
            update PPT_JPA_GENKEYS set PKEY_VALUE = v_usz_id
            where PKEY_TABLE = 'PPT_SZK_UCZESTNICY_SZKOLEN';
--             DBMS_OUTPUT.PUT_LINE ('-- insert into PPADM.PPT_SZK_UCZESTNICY_SZKOLEN  
--                            ( USZ_ID  ,         USZ_PRC_ID,          USZ_SZK_ID,           USZ_REZ,       USZ_OBECNOSC) ' 
--             || ' values ('|| v_usz_id || ', ' || v_prc_id  || ', ' || r.PIS_SZK_ID|| ', ' || 'T'|| ', ' || 'N' || ') '  );
            insert into PPADM.PPT_SZK_UCZESTNICY_SZKOLEN 
                    ( USZ_ID  , USZ_PRC_ID, USZ_SZK_ID, USZ_REZ, USZ_OBECNOSC)
            values  ( v_usz_id, v_prc_id  , r.PIS_SZK_ID, 'T', 'N' );
        end if;
    END LOOP;
--    DBMS_OUTPUT.put_line('stop zsz_dopisz_do_usz /  ' || p_zsz_id);
EXCEPTION
   WHEN OTHERS THEN
      raise_application_error(-20001,'Blad PL/SQL wew. procedury ppadm.zsz_dopisz_do_usz - '||SQLCODE||' -ERROR- '||SQLERRM);
end;
/

