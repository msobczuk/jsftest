CREATE OR REPLACE TRIGGER PPADM.PPG_LST_AIUDR_TS
  AFTER INSERT OR DELETE OR UPDATE ON PPADM.PPT_ADM_LISTY_WARTOSCI
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/PPG_LST_AIUDR_TS.trg 3 1.0.3 09.03.18 09:06 MGOLDA                                          $
-- $Revision:: 3                                                                                                                                              $
-- $Workfile:: PPG_LST_AIUDR_TS.trg                                                                                                                           $
-- $Modtime:: 09.03.18 09:06                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN
    UPDATE PPT_JPA_GENKEYS SET PKEY_TS = SYSDATE WHERE PKEY_TABLE = 'PPT_ADM_LISTY_WARTOSCI';
END;
/


