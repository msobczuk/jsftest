CREATE OR REPLACE VIEW PPADM.PPV_STRUKTURA_PODLEGLOSCI
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/views/PPV_STRUKTURA_PODLEGLOSCI.vw 1 1.0.1 02.03.18 17:18 DZIOBEK                                    $
-- $Revision:: 1                                                                                                                                              $
-- $Workfile:: PPV_STRUKTURA_PODLEGLOSCI.vw                                                                                                                   $
-- $Modtime:: 02.03.18 17:18                                                                                                                                  $
-- $Author:: DZIOBEK                                                                                                                                          $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
select stx_stjo_id spod_stjo_id,stx_stjo_id_nad spod_stjo_id_nad, z1.sto_prc_id spod_prc_id,z2.sto_prc_id spod_prc_id_prz,z2.sto_f_zastepstwo spod_f_zastepstwo 
from ZPT_STANOWISKA_NAD,ZPT_STANOWISKA_OPIS z1, ZPT_STANOWISKA_OPIS z2 where stx_stjo_id=z1.sto_stjo_id
    and stx_stjo_id_nad=z2.sto_stjo_id
    and ppp_global.odczytaj_date_dla_struktury_po between stx_data_od and nvl(stx_data_do,ppp_global.odczytaj_date_dla_struktury_po)
    and ppp_global.odczytaj_date_dla_struktury_po between z1.sto_data_od and nvl(z1.sto_data_do,ppp_global.odczytaj_date_dla_struktury_po)
    and ppp_global.odczytaj_date_dla_struktury_po between z2.sto_data_od and nvl(z2.sto_data_do,ppp_global.odczytaj_date_dla_struktury_po)
/


