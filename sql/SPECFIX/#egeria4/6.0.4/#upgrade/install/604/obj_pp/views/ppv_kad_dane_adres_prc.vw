CREATE OR REPLACE VIEW PPADM.PPV_KAD_DANE_ADRES_PRC
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/views/PPV_KAD_DANE_ADRES_PRC.vw 3 1.0.3 20.03.18 08:28 DZIOBEK                                       $
-- $Revision:: 3                                                                                                                                              $
-- $Workfile:: PPV_KAD_DANE_ADRES_PRC.vw                                                                                                                      $
-- $Modtime:: 20.03.18 08:28                                                                                                                                  $
-- $Author:: DZIOBEK                                                                                                                                          $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
select  
     prc_id              pp_prcid, 
     prc_imie            pp_imie, 
     prc_nazwisko        pp_nazwisko, 
     P.PRC_IMIE_MATKI    pp_imiematki, 
     P.PRC_IMIE_OJCA     pp_imieojca, 
     P.PRC_DATA_UR       pp_dataurodzenia, 
     P.PRC_MIEJSCE_UR    pp_miejsceurodzenia,  
     P.PRC_PESEL         pp_pesel,  
     P.PRC_PASZPORT      pp_paszport,  
     P.PRC_DOWOD_OSOB            pp_dowodosobisty,  
     P.PRC_DATA_WYDANIA_DOWODU   pp_datatwydaniadowodu,     
     P.PRC_DATA_WAZNOSCI_DOWODU  pp_datawaznoscidowodu,    
     P.PRC_ORGAN_WYDAJACY        pp_organwydajacy, 
     P.PRC_OBYWATELSTWO          pp_obywatelstwo, 
     US.KL_NAZWA                         pp_urzadskarbowy, 
     ek_pck_adresy.adres (KL_ADR_ID)     pp_us_adres, 
    (select KR_NAZWA from  CSS_KRAJE where KR_ID = ADR.ADR_KR_ID)            pp_adreskraj, 
    (select WOJ_NAZWA from  CSS_WOJEWODZTWA where WOJ_ID = ADR.ADR_WOJ_ID)   pp_adreswojwodztwo, 
    ADR.ADR_POWIAT                                                           pp_adrespowiat,  
    ADR.ADR_GMINA                                                            pp_adresgmina, 
    ek_pck_adresy.adres (ADR.adr_id)                                         pp_adres, 
    ADR.ADR_POCZTA                                                           pp_adrespoczta, 
   ADR.ADR_KOD_POCZTOWY                                                     pp_adreskodpocztowy,    
    (select KR_NAZWA from  CSS_KRAJE where KR_ID = ADR_O.ADR_KR_ID)            pp_adrestymczkraj, 
    (select WOJ_NAZWA from  CSS_WOJEWODZTWA where WOJ_ID = ADR_O.ADR_WOJ_ID)   pp_adrestymczwojwodztwo, 
    ADR_O.ADR_POWIAT                                                           pp_adrestymczpowiat, 
    ADR_O.ADR_GMINA                                                            pp_adrestymczgmina, 
    ek_pck_adresy.adres (ADR_O.adr_id)                                         pp_adrestymcz, 
    ADR_O.ADR_KOD_POCZTOWY                                                     pp_adrestymczkodpocztowy, 
    (select KR_NAZWA from  CSS_KRAJE where KR_ID = ADR_K.ADR_KR_ID)            pp_adreskorespkraj, 
    (select WOJ_NAZWA from  CSS_WOJEWODZTWA where WOJ_ID = ADR_K.ADR_WOJ_ID)   pp_adreskorespwojwodztwo, 
    ADR_K.ADR_POWIAT                                                           pp_adreskoresppowiat,  
    ADR_K.ADR_GMINA                                                            pp_adreskorespgmina, 
    ek_pck_adresy.adres (ADR_K.adr_id)                                         pp_adreskores, 
    ADR_K.ADR_KOD_POCZTOWY                                                     pp_adreskoreskodpocztowy, 
    (select KR_NAZWA from  CSS_KRAJE where KR_ID = ADR_P.ADR_KR_ID)            pp_adrespitkraj, 
    (select WOJ_NAZWA from  CSS_WOJEWODZTWA where WOJ_ID = ADR_P.ADR_WOJ_ID)   pp_adrespitwojwodztwo, 
    ADR_P.ADR_POWIAT                                                           pp_adrespitpowiat,  
    ADR_P.ADR_GMINA                                                            pp_adrespitgmina, 
    ek_pck_adresy.adres (ADR_P.adr_id)                                         pp_adrespit, 
    ADR_P.ADR_KOD_POCZTOWY                                                     pp_adrespitkodpocztowy, 
    (select KR_NAZWA from  CSS_KRAJE where KR_ID = ADR_I.ADR_KR_ID)            pp_adresinnykraj, 
    (select WOJ_NAZWA from  CSS_WOJEWODZTWA where WOJ_ID = ADR_I.ADR_WOJ_ID)   pp_adresinnywojwodztwo, 
    ADR_I.ADR_POWIAT                                                           pp_adresinnypowiat,  
    ADR_I.ADR_GMINA                                                            pp_adresinnygmina, 
    ek_pck_adresy.adres (ADR_I.adr_id)                                         pp_adresinny, 
    ADR_I.ADR_KOD_POCZTOWY                                                     pp_adresinnykodpocztowy 
  from   EGADM1.EK_PRACOWNICY P,  
         EGADM1.CKK_KLIENCI_AKT US,  
 		EGADM1.CKK_ADRESY ADR, 
         EGADM1.CKK_ADRESY ADR_O,          
         EGADM1.CKK_ADRESY ADR_K, 
         EGADM1.CKK_ADRESY ADR_P, 
         EGADM1.CKK_ADRESY ADR_I 
  where 
     P.PRC_USK_ID = US.KL_KOD(+) 
     and P.PRC_ID = ADR.adr_prc_id (+) 
     and 'T' = ADR.Adr_zatwierdzony (+) 
     and 'S' = ADR.adr_typ (+) 
     and P.PRC_ID = ADR_O.adr_prc_id (+) 
     and 'T' = ADR_O.Adr_zatwierdzony (+) 
     and 'O' = ADR_O.adr_typ (+) 
     and P.PRC_ID = ADR_K.adr_prc_id (+) 
     and 'T' = ADR_K.Adr_zatwierdzony (+) 
     and 'K' = ADR_K.adr_typ (+) 
     and P.PRC_ID = ADR_P.adr_prc_id (+) 
     and 'T' = ADR_P.Adr_zatwierdzony (+) 
     and 'P' = ADR_P.adr_typ (+) 
     and P.PRC_ID = ADR_I.adr_prc_id (+) 
     and 'T' = ADR_I.Adr_zatwierdzony (+) 
     and 'W' = ADR_I.adr_typ (+)
/

