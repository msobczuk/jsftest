CREATE OR REPLACE VIEW PPADM.PPV_LISTA_PLAC_SKLADNIKI
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/views/PPV_LISTA_PLAC_SKLADNIKI.vw 1 1.0.1 02.03.18 17:17 DZIOBEK                                     $
-- $Revision:: 1                                                                                                                                              $
-- $Workfile:: PPV_LISTA_PLAC_SKLADNIKI.vw                                                                                                                    $
-- $Modtime:: 02.03.18 17:17                                                                                                                                  $
-- $Author:: DZIOBEK                                                                                                                                          $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
SELECT
			  -- $Revision:: 1       $--
			  sk_id PP_SK_ID,
              sk_prc_id PP_SK_PRC_ID,
              TO_CHAR (lst_data_obliczen, 'yyyy-mm') PP_LST_OKRES,
              lst_drl_kod PP_LST_DRL_KOD,
              drl_nazwa PP_DRL_NAZWA,
              lst_data_obliczen PP_LST_DATA_OBLICZEN,
              dsk_kod PP_DSK_KOD,
              dsk_nazwa PP_DSK_NAZWA,
              sk_wartosc PP_SK_WARTOSC,
              lst_numer || ' - ' || lst_temat PP_LST_NAZWA,
              DG_DK_KOD PP_DG_DK_KOD,
              DG_NUMER PP_DG_NUMER,
              DG_KOD PP_DG_KOD,
              DG_NAZWA PP_DG_NAZWA
       FROM   ek_skladniki,
              ek_listy,
              ek_def_rodzajow_list,
              ek_def_skladnikow,
              ek_grupy_kodow,
              ek_def_grup,
              ek_def_kategorii
      WHERE       sk_lst_id = lst_id
              AND drl_kod = lst_drl_kod
              AND sk_dsk_id = dsk_id
   --           AND lst_zatwierdzona = 'T'
              AND NOT sk_wartosc = 0
              AND DK_KOD = DG_DK_KOD -- zlaczenie definicji kategorii z degeinicja grup
              AND GK_DG_KOD = DG_KOD  --zlaczenie definicji grup z grupa kodow
              AND GK_DSK_ID = SK_DSK_ID --zlaczenie skladnikow(definicji skladnikow) z grupa kodow
            AND dk_kod = 'PP_SKL_PL'
   --and sk_prc_id = 126677
   --and to_char(lst_data_obliczen, 'yyyy-mm') = '2007-05'
   --and extract(year from LST_DATA_WYPLATY) = nvl(2007, extract(year from LST_DATA_WYPLATY))
/


