CREATE OR REPLACE TRIGGER PPADM.PPG_ERR_INSERT
  BEFORE INSERT ON PPADM.PPT_ERR_LOG
  REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/PPG_ERR_INSERT.trg 3 1.0.3 09.03.18 09:06 MGOLDA                                            $
-- $Revision:: 3                                                                                                                                              $
-- $Workfile:: PPG_ERR_INSERT.trg                                                                                                                             $
-- $Modtime:: 09.03.18 09:06                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN
   SELECT PPADM.PPS_ERR.nextval INTO :new.err_id FROM DUAL;
END;
/


