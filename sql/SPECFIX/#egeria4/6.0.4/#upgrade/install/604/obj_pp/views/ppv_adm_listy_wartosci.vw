CREATE OR REPLACE VIEW PPADM.PPV_ADM_LISTY_WARTOSCI
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/views/PPV_ADM_LISTY_WARTOSCI.vw 1 1.0.1 02.03.18 17:18 DZIOBEK                                       $
-- $Revision:: 1                                                                                                                                              $
-- $Workfile:: PPV_ADM_LISTY_WARTOSCI.vw                                                                                                                      $
-- $Modtime:: 02.03.18 17:18                                                                                                                                  $
-- $Author:: DZIOBEK                                                                                                                                          $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
SELECT lst_id,
       lst_lp,
       lst_nazwa,
       lst_query,
       DECODE(eap_lang.get_lang_id,
	     1,lst_opis,
         2,lst_opis2,
         3,lst_opis3,
         4,lst_opis4,
         5,lst_opis5,
         6,lst_opis6,
         7,lst_opis7,
         8,lst_opis8,
         9,lst_opis9,
		 lst_opis) lst_opis,
       lst_dlugosc,
       DECODE(eap_lang.get_lang_id,
         1,lst_etykieta,
         2,lst_etykieta2,
         3,lst_etykieta3,
         4,lst_etykieta4,
         5,lst_etykieta5,
         6,lst_etykieta6,
         7,lst_etykieta7,
         8,lst_etykieta8,
         9,lst_etykieta9,
		 lst_etykieta) lst_etykieta,
       lst_f_zwrot,
       lst_f_ukryte,
       lst_nazwa_pola
  FROM PPT_ADM_LISTY_WARTOSCI
/


