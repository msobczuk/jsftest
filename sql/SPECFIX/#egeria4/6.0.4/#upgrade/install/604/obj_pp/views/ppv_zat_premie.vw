CREATE OR REPLACE VIEW PPADM.PPV_ZAT_PREMIE
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/views/PPV_ZAT_PREMIE.vw 1 1.0.1 02.03.18 17:18 DZIOBEK                                               $
-- $Revision:: 1                                                                                                                                              $
-- $Workfile:: PPV_ZAT_PREMIE.vw                                                                                                                              $
-- $Modtime:: 02.03.18 17:18                                                                                                                                  $
-- $Author:: DZIOBEK                                                                                                                                          $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
SELECT
   			-- $Revision:: 1       $--
			ek_pr.PRC_IMIE PP_IMIE,
            ek_pr.PRC_NAZWISKO PP_NAZWISKO,
            ek_z.ZAT_ID PP_ZAT_ID,
            ek_z.ZAT_PRC_ID PP_ZAT_PRC_ID,
            ek_tp.TP_NAZWA PP_NAZWA,
            ek_tp.TP_OPIS PP_OPIS,
            ek_p.PRM_KWOTA PP_KWOTA,
            ek_p.PRM_PROCENT PP_PROCENT,
            ek_p.PRM_MNOZNIK PP_MNOZNIK,
            ek_p.PRM_DATA_OD PP_DATA_OD,
            ek_p.PRM_DATA_DO PP_DATA_DO,
            ek_p.PRM_STATUS PP_STATUS
     FROM   ek_pracownicy ek_pr,
            ek_zatrudnienie ek_z,
            ek_premie ek_p,
            ek_typy_premii ek_tp
    WHERE       ek_pr.PRC_ID = ek_z.ZAT_PRC_ID
            AND ek_z.zat_id = ek_p.PRM_ZAT_ID
            AND ek_tp.tp_id = ek_p.prm_tp_id
/


