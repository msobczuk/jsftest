CREATE OR REPLACE TRIGGER PPADM.PPG_WND_BIUR_NR
BEFORE INSERT OR UPDATE ON PPADM.PPT_DEL_WNIOSKI_DELEGACJI  FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/PPG_WND_BIUR_NR.trg 5 1.0.5 20.03.18 08:40 DZIOBEK                                          $
-- $Revision:: 5                                                                                                                                              $
-- $Workfile:: PPG_WND_BIUR_NR.trg                                                                                                                            $
-- $Modtime:: 20.03.18 08:40                                                                                                                                  $
-- $Author:: DZIOBEK                                                                                                                                          $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE
   --PRAGMA AUTONOMOUS_TRANSACTION;
  v_rodz varchar2(10);
BEGIN 
  IF :NEW.WND_RODZAJ is null OR :NEW.WND_RODZAJ = 0 THEN 
    v_rodz := 'I';
  END IF;
  IF :NEW.WND_RODZAJ = 1 THEN 
    v_rodz := 'K';
  END IF;
  IF :NEW.WND_RODZAJ = 2 THEN 
    v_rodz := 'Z';
  END IF;
  if (:NEW.WND_NUMER is null) THEN 
    :NEW.WND_NUMER_NR := PPADM.DEL_NUMER( :NEW.WND_DATA_WNIOSKU, :NEW.WND_RODZAJ);
    :NEW.WND_NUMER := v_rodz || '/' || substr('0000000000'||:NEW.WND_NUMER_NR ,-4)  || '/'||EXTRACT(YEAR FROM :NEW.WND_DATA_WNIOSKU);
  END IF;
END;
/

