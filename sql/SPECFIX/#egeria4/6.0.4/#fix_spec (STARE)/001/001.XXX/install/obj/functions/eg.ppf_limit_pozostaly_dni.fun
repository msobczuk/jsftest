CREATE OR REPLACE FUNCTION "PPADM"."LIMIT_POZOSTALY_DNI" (
  p_prc_id NUMBER,
  p_rda_id NUMBER,
  p_dzien  DATE DEFAULT SYSDATE
) return number 
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                         																										  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
as
  v_ret number:=0;
  v_dni number:=0;
  v_godz number:=0;
  v_blad varchar2(4000);
begin 
  v_ret := EKP_API_INTRANET.LIMIT_POZOSTALY(p_prc_id, p_rda_id, sysdate, v_godz, v_dni, v_blad);
  return v_dni;
end;
/


