CREATE OR REPLACE PROCEDURE "PPADM"."ARKUSZ_2_EGR" (p_id number) 
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                         																										  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
as
  v_prc_id NUMBER;
  v_kwa_id NUMBER;
  v_slpr_data_od DATE;
  v_slpr_data_do DATE;
  v_po_r_szk ek_pck_szk.t_r_szk;
begin
  select SLPR_PRC_ID, SLPR_DATA_OD, SLPR_DATA_DO into v_prc_id, v_slpr_data_od, v_slpr_data_do from PPT_SLP_SLUZBA_PRZYG where SLPR_ID = p_id;
  select KWA_ID into v_kwa_id from EK_KWALIFIKACJE where KWA_PRC_ID = v_prc_id;
  v_po_r_szk.szk_kwa_id :=  v_kwa_id;
  v_po_r_szk.szk_nazwa  := 'sp'; 
  v_po_r_szk.szk_rodzaj  := 14; 
  v_po_r_szk.szk_data_od := v_slpr_data_od;
  v_po_r_szk.szk_data_do := v_slpr_data_do;
  ek_pck_szk.wstaw(v_po_r_szk);
end;
/


