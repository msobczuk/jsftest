create or replace FUNCTION "PPADM"."GET_DEPARTAMENT_ID" (p_OB_ID NUMBER) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 v_OB_ID NUMBER;
BEGIN

    SELECT x.OB_ID into v_OB_ID FROM
		(SELECT 
			OB_ID
		FROM ek_obiekty_w_przedsieb
		WHERE 1=1
			and ob_id not in (1, 100001)
		START WITH OB_ID=p_OB_ID
		CONNECT BY NOCYCLE PRIOR OB_OB_ID=OB_ID
		ORDER BY LEVEL desc
		)x
	WHERE
		ROWNUM=1;

    return v_OB_ID; 	

  EXCEPTION
	WHEN OTHERS THEN
		return null;
  
END;
/


