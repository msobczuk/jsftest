CREATE OR REPLACE TRIGGER PPG_PIS_USUN_ZALACZNIKI 
BEFORE DELETE ON PPT_SZK_PLAN_INDYW
FOR EACH ROW
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision:: 1                                                                                                                                                                                                 $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE
	v_pis_id NUMBER(10,0);
BEGIN
	v_pis_id := :old.PIS_ID;
	DELETE FROM zkt_zalaczniki
	WHERE ZAL_PIS_ID = v_pis_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_pis_usun_zalaczniki');
END;
/
