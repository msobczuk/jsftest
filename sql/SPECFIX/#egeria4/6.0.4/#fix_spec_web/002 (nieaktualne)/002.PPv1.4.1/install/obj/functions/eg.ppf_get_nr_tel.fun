CREATE OR REPLACE FUNCTION ppadm.get_nr_tel(pPRC_ID  in  number)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  RETURN VARCHAR2
IS
  return_text  VARCHAR2(10000) := NULL;
BEGIN
  FOR x IN (select 
				  NT.NT_NUMER_TELEFONU NUMER
				  from EK_PRACOWNICY ekp 
				  join EK_ZATRUDNIENIE ekz on ekp.prc_id=ekz.zat_prc_id and ekz.zat_typ_umowy = 0 and trunc(SYSDATE) BETWEEN ekz.zat_data_zmiany AND nvl(ekz.zat_data_do,trunc(SYSDATE) )
				  left join ROTV_PRZYPISANIA_TELEFONOW PNT on (PNT.PNT_PRC_ID=ekz.ZAT_PRC_ID AND PNT.PNT_DATA_OD <= trunc(sysdate) AND trunc(sysdate) <=  nvl(PNT.PNT_DATA_DO, trunc(SYSDATE+(365*1000)) ) )
				  left join ROTT_NUMERY_TELEFONOW NT on NT.NT_ID=PNT.PNT_NT_ID
				  WHERE ekp.PRC_ID=pPRC_ID
			) LOOP

    IF x.NUMER is not null THEN
        return_text := return_text || x.NUMER || '; ' ;	     
    END IF;
    
  END LOOP;
  RETURN return_text;
  
  EXCEPTION
	WHEN OTHERS THEN
		return null;
END;
/	

