CREATE OR REPLACE TRIGGER "PPADM"."PPG_PKL_USUNZALACZNIKI"
BEFORE DELETE ON "PPADM"."PPT_DEL_POZYCJE_KALKULACJI"
REFERENCING FOR EACH ROW
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision:: 1                                                                                                                                                                                                 $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  DECLARE
	v_pkl_id NUMBER(10,0);
BEGIN
	v_pkl_id := :old.PKL_ID;
	DELETE FROM zkt_zalaczniki
	WHERE ZAL_PKL_ID = v_pkl_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_pkl_usun_zalaczniki');
END;
/


