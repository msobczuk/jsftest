CREATE OR REPLACE TRIGGER "PPADM"."PPG_UMSZ_USUNZALACZNIKI"
BEFORE DELETE ON "PPADM"."PPT_SZK_UMOWY"
REFERENCING FOR EACH ROW
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision:: 1                                                                                                                                                                                                 $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  DECLARE
	v_umsz_id NUMBER(10,0);
BEGIN
	v_umsz_id := :old.UMSZ_ID;
	DELETE FROM zkt_zalaczniki
	WHERE ZAL_UMSZ_ID = v_umsz_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_umsz_usun_zalaczniki');
END;
/

