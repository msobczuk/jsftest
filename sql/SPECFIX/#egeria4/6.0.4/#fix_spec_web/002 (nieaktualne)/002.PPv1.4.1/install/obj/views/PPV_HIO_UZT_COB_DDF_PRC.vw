CREATE OR REPLACE VIEW PPADM.PPV_HIO_UZT_COB_DDF_PRC
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
SELECT 
      HIO_ID,
      HIO.HIO_KLUCZ_OBCY_ID,
      HIO.HIO_DATA,
      HIO.HIO_UZT_NAZWA,
      COB.COB_ID,
      COB.COB_NAZWA,
      PRC.PRC_IMIE,
      prc.PRC_NAZWISKO,
      HIO_OPIS,
      HIO_KOB_KOD
FROM
   --(select * from CSST_HISTORIA_OBIEGOW order by HIO_ID DESC) HIO

    CSST_HISTORIA_OBIEGOW hio 
    join 
    ( 
      select -- jesli czynnosc powtarzano (bo byly korekty) - tylko ta ostatnio wykonana
        max(hio_id) max_hio_id --, hio_kob_kod, hio_klucz_obcy_id, HIO_COB_ID 
      from CSST_HISTORIA_OBIEGOW 
      group by  hio_kob_kod, hio_klucz_obcy_id, HIO_COB_ID
    ) lasthio
    on lasthio.max_hio_id = hio.hio_id
    
    left join EAADM.EAT_UZYTKOWNICY UZT on UZT.UZT_NAZWA = HIO.HIO_UZT_NAZWA
    left join CSST_CZYNNOSCI_OBIEGOW COB on HIO.HIO_COB_ID = COB.COB_ID
    left join EAADM.EAT_DOSTEPY_DO_FIRM DDF on DDF.DDF_FRM_ID=EAADM.EAP_GLOBALS.ODCZYTAJ_FIRME and DDF.DDF_UZT_NAZWA=HIO.HIO_UZT_NAZWA
    left join EGADM1.EK_PRACOWNICY PRC on DDF.DDF_PRC_ID=PRC.PRC_ID
 order by hio.HIO_ID DESC
/