CREATE OR REPLACE TRIGGER "PPADM"."PPG_ZSZ_USUNZALACZNIKI"
BEFORE DELETE ON "PPADM"."PPT_SZK_ZAPISY_SZKOLEN"
REFERENCING FOR EACH ROW
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision:: 1                                                                                                                                                                                                 $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  DECLARE
	v_zsz_id NUMBER(10,0);
BEGIN
	v_zsz_id := :old.ZSZ_ID;
	DELETE FROM zkt_zalaczniki
	WHERE ZAL_ZSZ_ID = v_zsz_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_zsz_usun_zalaczniki');
END;
/


