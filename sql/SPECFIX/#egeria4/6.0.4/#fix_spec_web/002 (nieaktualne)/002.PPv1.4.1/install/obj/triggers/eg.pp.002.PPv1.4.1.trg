PROMPT ----------------------------------------------------------------------;
PROMPT 6.0.4.001_1.4.1


CREATE OR REPLACE TRIGGER PPADM.PPG_NARC_BIUR_AUDYT
  BEFORE INSERT or UPDATE ON PPADM.PPT_REK_REZERWY_CELOWE   FOR EACH ROW
DECLARE
  v_pom VARCHAR2 (255);
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.NARC_AUDYT_UT, :NEW.NARC_AUDYT_DT, v_pom, :NEW.NARC_AUDYT_LM, :OLD.NARC_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.NARC_AUDYT_UT, :NEW.NARC_AUDYT_DT, v_pom, :NEW.NARC_AUDYT_LM, :OLD.NARC_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.NARC_AUDYT_UM, :NEW.NARC_AUDYT_DM, :NEW.NARC_AUDYT_KM, :NEW.NARC_AUDYT_LM, :OLD.NARC_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.NARC_AUDYT_UM, :NEW.NARC_AUDYT_DM, :NEW.NARC_AUDYT_KM, :NEW.NARC_AUDYT_LM, :OLD.NARC_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;
/


create or replace TRIGGER PPADM.PPG_CZYN_BIUR_AUDYT
  BEFORE INSERT or UPDATE ON PPADM.PPT_DEL_CZYNNOSCI_SLUZBOWE   FOR EACH ROW
DECLARE
  v_pom VARCHAR2 (255);
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.CZYN_AUDYT_UT, :NEW.CZYN_AUDYT_DT, v_pom, :NEW.CZYN_AUDYT_LM, :OLD.CZYN_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.CZYN_AUDYT_UT, :NEW.CZYN_AUDYT_DT, v_pom, :NEW.CZYN_AUDYT_LM, :OLD.CZYN_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.CZYN_AUDYT_UM, :NEW.CZYN_AUDYT_DM, :NEW.CZYN_AUDYT_KM, :NEW.CZYN_AUDYT_LM, :OLD.CZYN_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.CZYN_AUDYT_UM, :NEW.CZYN_AUDYT_DM, :NEW.CZYN_AUDYT_KM, :NEW.CZYN_AUDYT_LM, :OLD.CZYN_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;
/

create or replace TRIGGER PPADM.PPG_NABE_BIUR_AUDYT
  BEFORE INSERT or UPDATE ON PPADM.PPT_REK_ETAPY   FOR EACH ROW
DECLARE
  v_pom VARCHAR2 (255);
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.NABE_AUDYT_UT, :NEW.NABE_AUDYT_DT, v_pom, :NEW.NABE_AUDYT_LM, :OLD.NABE_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.NABE_AUDYT_UT, :NEW.NABE_AUDYT_DT, v_pom, :NEW.NABE_AUDYT_LM, :OLD.NABE_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.NABE_AUDYT_UM, :NEW.NABE_AUDYT_DM, :NEW.NABE_AUDYT_KM, :NEW.NABE_AUDYT_LM, :OLD.NABE_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.NABE_AUDYT_UM, :NEW.NABE_AUDYT_DM, :NEW.NABE_AUDYT_KM, :NEW.NABE_AUDYT_LM, :OLD.NABE_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;
/






PROMPT ----------------------------------------------------------------------;

