CREATE OR REPLACE TRIGGER EAADM.PPG_WDM_AIR
  AFTER INSERT ON EAADM.EAT_WIADOMOSCI   
  FOR EACH ROW
DECLARE 
  c_procedura VARCHAR2(100) :='Trigger: EAADM.PPG_WDM_AIR';  
BEGIN
----------------------------------------------------------------------------------------------------------------------------------
-- $Header:  $
-- $Revision::        $
-- $Workfile::  $
-- $Modtime::    $
-- $Author::   $
----------------------------------------------------------------------------------------------------------------------------------
-- TRIGGER WYSYLA POWIADOMIENIE O NOWEJ WIADOMOSCI DO ZAINTERESOWANEGO UŻYTKOWNIKA PP
-- JESLI UZYTKWONIK NIE JEST W DANEJ CHWILI ZALOGOWANY W PP (BRAK WPISU W EAT_SESJE ZEWNETRZNE) NIC SIE NIE WYDARZY
-- JESLI TRANSAKCJA W KTOREJ DZIALA TRIGGER ZOSTANIE WYCOFANA, KOMUNIKAT TAKZE NIE ZOSTANIE WYSLANY
-- PROCEDURA PPADM.MSG WEWN. KORZYSTA Z DBMS_ALERT A PARAMETR P_AUTONOMOUS_TRANSACTION => 'N' POWODUJE ZE ZOSTANIE 
-- WYKORZYSTANA WERJSA NIEATONOMICZNA
----------------------------------------------------------------------------------------------------------------------------------

  IF  (TRUNC(NVL (:new.WDM_DATA_WYSLANIA, :new.WDM_DATA_WSTAWIENIA)) <= TRUNC(sysdate))  THEN 
     PPADM.MSG( :new.WDM_UZT_NAZWA, p_wdm_id => :new.WDM_ID, P_AUTONOMOUS_TRANSACTION => 'N' );
  END IF;
  
EXCEPTION
   WHEN OTHERS THEN
   BEGIN 
    eap_blad.zglos (p_procedura           => c_procedura, p_dodatkowe_info      => '');
    --PPADM.plog('EXCEPTION/PPG_WDM_BIR... SQLCODE: '|| SQLCODE || '; SQLERRM: '|| SQLERRM  );                 
  END;
END;
/
