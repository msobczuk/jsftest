create or replace TRIGGER "PPADM"."PPG_OCE_WIADOMOSC"
  AFTER INSERT OR UPDATE ON "PPADM"."PPT_OCE_OCENY"
  REFERENCING FOR EACH ROW
  DECLARE
x varchar2(50);
BEGIN
  IF inserting THEN
       x := null;
       --select PRC_UZYTKOWNIK into x from EGADM1.EK_PRACOWNICY where PRC_ID = :NEW.OCE_PRC_ID;
       wstaw_wiadomosc(:NEW.OCE_PRC_ID,'Zbliża się termin oceny.',SYSDATE);
       --select PRC_UZYTKOWNIK into x from EGADM1.EK_PRACOWNICY where PRC_ID = :NEW.OCE_PRC_ID_OCENIAJACY;
       wstaw_wiadomosc(:NEW.OCE_PRC_ID_OCENIAJACY,'Zbliża się termin oceny.',SYSDATE);
  ELSE
    IF (:OLD.OCE_DATA_OCENY <> :NEW.OCE_DATA_OCENY) or (:OLD.OCE_PRC_ID <> :NEW.OCE_PRC_ID) or (:OLD.OCE_PRC_ID_OCENIAJACY <> :NEW.OCE_PRC_ID_OCENIAJACY) THEN
       x := null;
       --select PRC_UZYTKOWNIK into x from EGADM1.EK_PRACOWNICY where PRC_ID = :NEW.OCE_PRC_ID;
       wstaw_wiadomosc(:NEW.OCE_PRC_ID,'Zbliża się termin oceny.',SYSDATE);
       --select PRC_UZYTKOWNIK into x from EGADM1.EK_PRACOWNICY where PRC_ID = :NEW.OCE_PRC_ID_OCENIAJACY;
       wstaw_wiadomosc(:NEW.OCE_PRC_ID_OCENIAJACY,'Zbliża się termin oceny.',SYSDATE);
    END IF;
  END IF;
END;
/

ALTER TRIGGER PPG_OCE_WIADOMOSC DISABLE;
/




