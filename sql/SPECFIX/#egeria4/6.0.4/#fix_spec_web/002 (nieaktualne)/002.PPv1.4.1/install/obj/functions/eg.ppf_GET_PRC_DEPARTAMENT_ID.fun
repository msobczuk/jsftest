create or replace FUNCTION  "PPADM"."GET_PRC_DEPARTAMENT_ID" (p_PRC_ID NUMBER, p_na_dzien DATE DEFAULT SYSDATE) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
 v_zat_ob_id NUMBER;
  
BEGIN

    select zat.zat_ob_id 
      into v_zat_ob_id  
    from EGADM1.EK_ZATRUDNIENIE zat
    where zat.ZAT_PRC_ID = p_prc_id 
        -- and zat.zat_frm_id = 1
        AND trunc(p_na_dzien) BETWEEN zat_data_zmiany AND nvl(zat_data_do, trunc(SYSDATE+(365*1000)))
        AND rownum = 1;

    --return v_zat_ob_id;
    return GET_DEPARTAMENT_ID(v_zat_ob_id);

EXCEPTION
	WHEN OTHERS THEN
		return null;
  
END;
/

