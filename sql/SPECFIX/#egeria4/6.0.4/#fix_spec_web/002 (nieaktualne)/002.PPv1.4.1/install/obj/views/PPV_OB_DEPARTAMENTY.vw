CREATE OR REPLACE VIEW PPADM.PPV_OB_DEPARTAMENTY
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
select 
      ob.ob_id, 
      ob.ob_nazwa, 
      ob.ob_kod, 
      ob.ob_pelny_kod, 
      dep.ob_id as dep_id, 
      dep.ob_nazwa as dep_nazwa, 
      dep.ob_kod as dep_kod, 
      dep.ob_pelny_kod as dep_pelny_kod
from css_obiekty_w_przedsieb ob
  left join EGADM1.CSS_OBIEKTY_W_PRZEDSIEB dep on PPADM.GET_DEPARTAMENT_ID(ob.ob_id) = dep.ob_id
order by OB_ID
/