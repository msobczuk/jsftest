CREATE OR REPLACE PROCEDURE PPADM.POINFORMUJ_BDG_O_AKCEPT_WN_REK ( NAB_ID IN NUMBER )
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------

AS 

CURSOR c_odbiorcy(p_nab_id NUMBER) IS

-- przedstawiciele BDG wskazani na wniosku
select KOM_PRC_ID prc_id
  from PPADM.PPT_REK_NABOR_KOMISJA
 where KOM_TYP = 'B'
   and KOM_NAB_ID = p_nab_id
   
union

-- naczelnik do spraw naboru i rekrutacji
select zat_prc_id prc_id
  from ek_zatrudnienie
 where zat_stn_id IN (select distinct stjo_stn_id
                        from zpt_st_w_jo
                       where stjo_stn_id IN (select stn_id
                                               from zpt_stanowiska
                                              where upper(stn_nazwa) like '%NACZELNIK WYDZIA�U%'))
   and zat_ob_id IN (select ob_id from css_obiekty_w_przedsieb where ob_kod = 'BDG02');
   
BEGIN

  FOR i IN c_odbiorcy(nab_id)
  LOOP
    WIADOMOSC(i.prc_id, 'Wniosek rekrutacji [id: ' || nab_id || '] zosta� zaakceptowany przez Dyrektora Generalnego.', 'WIADOMO�� SYSTEMOWA PP', P_AUTONOMOUS_TRANSACTION => 'T');
  END LOOP;

END;
/






