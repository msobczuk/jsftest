CREATE OR REPLACE FUNCTION PPADM.TXT_TO_NUMBER (P_TEXT_NUMBER IN VARCHAR2, P_DELIMITER IN VARCHAR2 DEFAULT ', ') RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  v_ret NUMBER;
BEGIN
  v_ret := to_number(P_TEXT_NUMBER, '99999D99', 'NLS_NUMERIC_CHARACTERS='',.''');
  
  return v_ret;

EXCEPTION
  WHEN OTHERS THEN
    return null;
END;
/




