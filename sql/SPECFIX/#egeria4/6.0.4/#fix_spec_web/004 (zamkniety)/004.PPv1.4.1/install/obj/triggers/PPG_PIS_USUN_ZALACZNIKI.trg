CREATE OR REPLACE TRIGGER PPADM.PPG_PIS_USUN_ZALACZNIKI
BEFORE DELETE ON PPADM.PPT_SZK_PLAN_INDYW
REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------     
  DECLARE
	v_pis_id NUMBER(10,0);
BEGIN
	v_pis_id := :old.PIS_ID;
	DELETE FROM egadm1.zkt_zalaczniki
	WHERE ZAL_PIS_ID = v_pis_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_pis_usun_zalaczniki');
END;
/






