CREATE OR REPLACE FUNCTION PPADM.URLOP_GODZ_LICZBA_GODZIN( P_PRC_ID NUMBER, P_DATA_OD DATE, P_DATA_DO DATE) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 v_nr NUMBER :=0;
 
 v_od date := p_data_od;
 v_do date := p_data_do;
 
 v_GWD_OD DATE;
 v_GWD_DO DATE;
BEGIN
  if (v_od = null) then
   v_od :=  sysdate + 10000;
  end if;
  if (v_do = null) then
   v_do :=  sysdate - 10000;
  end if;

  FOR x IN(
        SELECT 
            GWD.GWD_OD,
            GWD.GWD_DO 
        FROM
        EGADM1.ek_plany_indywidualne PI 
		left join EGADM1.EK_GODZINY_W_DNIU GWD on GWD.GWD_PI_ID=PI.PI_ID
        WHERE PI.PI_PRC_ID=P_PRC_ID 
              and PI.PI_DZIEN=TRUNC( v_od )
			  and GWD.GWD_RG_KOD='GD'
  ) LOOP
 
      IF x.GWD_DO > v_do THEN
        v_GWD_DO := v_do;
      ELSE
        v_GWD_DO := x.GWD_DO;
      END IF;
            
      IF x.GWD_OD < v_od THEN
        v_GWD_OD := v_od;
      ELSE
        v_GWD_OD := x.GWD_OD;
      END IF;
            
      IF round( (  v_GWD_DO - v_GWD_OD  )*24,6) > 0 THEN
        v_nr := v_nr + round( (  v_GWD_DO - v_GWD_OD  )*24,6);
      END IF; 
  END LOOP;

  
  IF v_nr>0 THEN
    RETURN v_nr;
  ELSE
    RETURN 0;
  END IF;

  EXCEPTION
	WHEN OTHERS THEN
    plog('URLOP_GODZ_LICZBA_GODZIN=> P_PRC_ID='||P_PRC_ID ||' P_DATA_OD='|| P_DATA_OD ||' P_DATA_DO='|| P_DATA_DO ||' SQLERRM: ' || SQLERRM() || ' ; SQLCODE ' || SQLCODE() );
		return 0;

END;
/





