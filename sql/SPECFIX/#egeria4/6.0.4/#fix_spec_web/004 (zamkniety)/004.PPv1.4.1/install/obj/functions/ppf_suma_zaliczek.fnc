CREATE OR REPLACE FUNCTION PPADM.SUMA_ZALICZEK (p_WND_ID NUMBER,  p_KAL_RODZAJ varchar2) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 v_suma NUMBER;
BEGIN
  
SELECT 
  ROUND(SUM(DKZ.DKZ_KWOTA*nvl(DKZ_KURS,1)/nvl(WAL.WAL_JEDNOSTKA_KURSU,1)),2) suma into v_suma
    FROM PPT_DEL_KALKULACJE KAL
       JOIN PPT_DEL_ZALICZKI DKZ on DKZ.DKZ_KAL_ID=KAL.KAL_ID
	   join EGADM1.CSS_WALUTY WAL on WAL.WAL_ID=DKZ.DKZ_WAL_ID
    WHERE KAL.KAL_WND_ID=p_WND_ID
          and KAL.KAL_RODZAJ=p_KAL_RODZAJ;
	
    return v_suma; 	

  EXCEPTION
	WHEN OTHERS THEN
		return null;
  
END;
/





