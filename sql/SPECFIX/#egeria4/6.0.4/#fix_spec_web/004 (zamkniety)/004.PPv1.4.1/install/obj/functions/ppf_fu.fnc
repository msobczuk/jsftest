CREATE OR REPLACE FUNCTION PPADM.FU (p_UZT_NAZWA varchar2, p_FU_NAZWA varchar2) return varchar2 as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	v_nr NUMBER := 0;
BEGIN
  --PLOG(' ppadm.fu ' || p_UZT_NAZWA || ' ' || p_FU_NAZWA );
  
	SELECT count(*) into v_nr
	FROM eat_uprawnienia upr 
	   join eat_funkcje_uzytkowe fu on fu.FU_ID = upr_fu_id 
	   join eat_aplikacje apl on apl.APL_ID = fu.FU_APL_ID 
	WHERE 
		 APL.APL_SYMBOL = 'PP' 
		 and UPR.UPR_UZT_NAZWA = p_UZT_NAZWA
		 and FU.FU_NAZWA = p_FU_NAZWA;
	
	IF v_nr > 0 THEN
		return 'T';
	ELSE
		return 'N';
	END IF;

	EXCEPTION
		WHEN OTHERS THEN
			return null;	
END;
/



