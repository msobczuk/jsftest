CREATE OR REPLACE TRIGGER PPADM.PPG_SZK_WN_REF_DELETE_ZF
BEFORE DELETE ON PPADM.PPT_SZK_WN_REFUNDACJE
REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
--------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
  BEGIN
  DELETE FROM PPT_ZRODLA_FINANSOWANIA WHERE ZFI_ENCJA_ID=:OLD.WNR_ID AND ZFI_KATEGORIA='PPT_SZK_WN_REFUNDACJE';
END;
/





