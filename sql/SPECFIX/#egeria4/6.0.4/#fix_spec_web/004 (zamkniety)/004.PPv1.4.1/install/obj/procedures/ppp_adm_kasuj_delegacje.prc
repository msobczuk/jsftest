CREATE OR REPLACE PROCEDURE PPADM.ADM_KASUJ_DELEGACJE (p_wnd_id NUMBER) as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    v_ln number := 0;
    v_kob_kod varchar2(250);
begin
    plog('ppadm.adm_kasuj_delegacje / ' || p_wnd_id);   v_ln:=5;

    for r in (select * from ppt_del_wnioski_delegacji where wnd_wnd_id = p_wnd_id) loop
        ppadm.adm_kasuj_delegacje(r.wnd_id);
    end loop;

    adm_usun_dane_obiegu('PPADM.PPT_DEL_WNIOSKI_DELEGACJI', p_wnd_id); v_ln:=11;

    delete from ppadm.ppt_del_angaze where (ang_wnd_id is null or ang_wnd_id=p_wnd_id)  and ang_pkzf_id in (select pkzf_id from ppadm.ppt_del_zrodla_finansowania where pkzf_pkl_id in (select pkl_id from ppt_del_pozycje_kalkulacji where pkl_kal_id in (select kal_id from ppt_del_kalkulacje where kal_wnd_id = p_wnd_id)));
    delete from ppadm.ppt_del_zrodla_finansowania where pkzf_pkl_id in (select pkl_id from ppt_del_pozycje_kalkulacji where pkl_kal_id in (select kal_id from ppt_del_kalkulacje where kal_wnd_id = p_wnd_id));
    delete from ppadm.ppt_del_pozycje_kalkulacji where pkl_kal_id in (select kal_id from ppt_del_kalkulacje where kal_wnd_id = p_wnd_id);
    delete from ppadm.ppt_del_zaliczki where dkz_kal_id in (select kal_id from ppt_del_kalkulacje where kal_wnd_id = p_wnd_id);
    delete from ppadm.ppt_del_trasy where trs_kal_id in (select kal_id from ppt_del_kalkulacje where kal_wnd_id = p_wnd_id);
    delete from ppadm.PPT_DEL_CZYNNOSCI_SLUZBOWE where czyn_kal_id in (select kal_id from ppt_del_kalkulacje where kal_wnd_id = p_wnd_id);
    delete from ppadm.ppt_del_kalkulacje where kal_wnd_id = p_wnd_id;
    delete from ppadm.ppt_del_angaze where ang_wnd_id = p_wnd_id;
    delete from ppadm.ppt_del_cele_delegacji where cdel_wnd_id = p_wnd_id;
    delete from ppadm.ppt_del_projekty where proj_wnd_id = p_wnd_id;
    delete from ppadm.ppt_del_rozliczenia_delegacji where rozd_wnd_id = p_wnd_id;
    delete from ppadm.ppt_del_srodki_lokomocji where slok_wnd_id = p_wnd_id;
    delete from ppadm.ppt_del_zapraszajacy where zapr_wnd_id = p_wnd_id;
    --TODO select RLZ_WND_ID from EGADM1.PBT_REALIZACJE;
    delete from ppadm.ppt_del_wnioski_delegacji where wnd_id = p_wnd_id;

exception
    when OTHERS then 
        plog('ppadm.adm_kasuj_delegacje / p_wnd_id: ' || p_wnd_id || ' -> err_code: ' || SQLCODE || ' err_msg: ' || SUBSTR(SQLERRM, 1, 200) || ' v_ln: ' || v_ln);
        raise_application_error(-20001,'ppadm.adm_kasuj_delegacje / p_wnd_id: ' || p_wnd_id || ' -> err_code: ' || SQLCODE || ' err_msg: ' || SQLERRM || ' v_ln: ' || v_ln);
end;
/







