CREATE OR REPLACE FUNCTION PPADM.REK_NUMER (P_NAB_ID NUMBER) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 v_nr NUMBER :=1;
 v_rodzaj varchar2(20);
 v_data_tworzenia date;
BEGIN

  SELECT NAB_RODZAJ_NABORU, NAB_AUDYT_DT
         into v_rodzaj, v_data_tworzenia
  FROM PPT_REK_NABORY NA
  WHERE NAB_ID=P_NAB_ID;
  
  select NVL( max(NAB_NUMER_NR),0) into v_nr 
  FROM PPT_REK_NABORY 
  WHERE EXTRACT(YEAR FROM NAB_AUDYT_DT) = EXTRACT(YEAR FROM v_data_tworzenia)  
    AND NAB_RODZAJ_NABORU = v_rodzaj;
 
  RETURN v_nr+1;
END;
/






