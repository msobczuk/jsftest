CREATE OR REPLACE FUNCTION PPADM.GET_RODZAJ_UMOWY 
(
  P_PRC_ID IN NUMBER 
, P_NA_DZIEN IN DATE DEFAULT SYSDATE
) RETURN VARCHAR2 AS 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  v_rodzaj_umowy css_ref_codes.rv_abbreviation%type;
BEGIN
  select distinct rv_abbreviation into v_rodzaj_umowy from EK_ZATRUDNIENIE zat
  inner join css_ref_codes on zat.zat_prc_id = P_PRC_ID  and rv_domain like 'TYP_UMOWY' and rv_low_value = zat_typ_umowy
  and trunc(P_NA_DZIEN) BETWEEN zat_data_zmiany AND nvl(zat_data_do, trunc(SYSDATE+(365*1000)))
  AND rownum = 1;
  RETURN v_rodzaj_umowy;
EXCEPTION
	WHEN OTHERS THEN
		return null;
END GET_RODZAJ_UMOWY;
/






