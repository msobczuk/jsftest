CREATE OR REPLACE PROCEDURE PPADM.ADM_USUN_DANE_OBIEGU (p_table varchar2, p_id number) as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    v_ln number := 0;
    v_kob_kod varchar2(250);
begin
    plog('ppadm.adm_usun_dane_obiegu / ' || p_table || ' / ' || p_id || '...'); v_ln:=5;

    select ews_kob_kod 
        into v_kob_kod 
    from EGADM1.csst_encje_w_stanach
        join EGADM1.csst_kategorie_obiegow on ews_kob_kod=kob_kod
    where kob_nazwa_tabeli =  p_table --'PPADM.PPT_DEL_WNIOSKI_DELEGACJI'
      and ews_klucz_obcy_id = p_id --134
      and rownum = 1; v_ln:=13;

    cssp_obiegi_utl.USUN_DANE (v_kob_kod, p_id); v_ln:=15; 

exception
    when OTHERS then 
        plog('ppadm.adm_usun_dane_obiegu -> err_code: ' || SQLCODE || ' err_msg: ' || SUBSTR(SQLERRM, 1, 200) || ' v_ln: ' || v_ln);
end;
/



