CREATE OR REPLACE PROCEDURE PPADM.WYPLAC_ZALICZKE ( p_DKZ_DOK_ID IN NUMBER )
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------

AS 

CURSOR c_odbiorcy IS
select 
    WD.WD_UZYTKOWNIK uzt
from CSS_TYPY_WD TWD 
join CSS_WARTOSCI_DOMYSLNE WD on WD.WD_TWD_ID = TWD.TWD_ID
where upper(TWD.TWD_NAZWA) like 'WDR%UPRAW%KASJER%'
and WD.wd_wartosc = 'T'
;

msg VARCHAR2(4000);
s_pracownik VARCHAR2(400);
s_data_wyplaty VARCHAR2(400);
s_numer VARCHAR2(400);
s_kwota NUMBER(10);
s_wal_symbol VARCHAR2(10);
s_typ_dokumentu VARCHAR2(3);
s_dok_id NUMBER(10);

   
BEGIN
  SELECT 
		 PRC.PRC_IMIE ||' '|| PRC.PRC_NAZWISKO pracownik,
		 TO_CHAR(DKZ.DKZ_DATA_WYP, 'DD.MM.YYYY') data_wyplaty,
		 WND.WND_NUMER,
      CASE
        WHEN KAL.KAL_RODZAJ='WST' THEN DKZ.DKZ_KWOTA
        WHEN KAL.KAL_RODZAJ='ROZ' THEN decode(DKZ.DKZ_KWOTA ,0 ,DKZ.DKZ_SRODKI_WLASNE, DKZ.DKZ_KWOTA)
      END KWOTA,
      WAL.WAL_SYMBOL,
      CASE
        WHEN KAL.KAL_RODZAJ='WST' THEN 'KW'
        WHEN KAL.KAL_RODZAJ='ROZ' THEN decode(DKZ.DKZ_KWOTA ,0 ,'KW', 'KP')
      END TYP_DOKUMENTU,
      DKZ.DKZ_DOK_ID
		 into s_pracownik, s_data_wyplaty, s_numer, s_kwota, s_wal_symbol, s_typ_dokumentu, s_dok_id
	FROM PPT_DEL_WNIOSKI_DELEGACJI WND
	join PPT_DEL_KALKULACJE KAL on KAL.KAL_WND_ID=WND.WND_ID
	join PPT_DEL_ZALICZKI DKZ on DKZ.DKZ_KAL_ID=KAL.KAL_ID
	join EGADM1.EK_PRACOWNICY PRC on WND.WND_PRC_ID=PRC.PRC_ID
  join EGADM1.CSS_WALUTY WAL on WAL.WAL_ID=DKZ.DKZ_WAL_ID
	WHERE DKZ.DKZ_DOK_ID=p_DKZ_DOK_ID
  ;

	SELECT 
		 PRC.PRC_IMIE ||' '|| PRC.PRC_NAZWISKO pracownik,
		 TO_CHAR(DKZ.DKZ_DATA_WYP, 'DD.MM.YYYY') data_wyplaty,
		 WND.WND_NUMER 
		 into s_pracownik, s_data_wyplaty, s_numer
	FROM PPT_DEL_WNIOSKI_DELEGACJI WND
	join PPT_DEL_KALKULACJE KAL on KAL.KAL_WND_ID=WND.WND_ID
	join PPT_DEL_ZALICZKI DKZ on DKZ.DKZ_KAL_ID=KAL.KAL_ID
	join EGADM1.EK_PRACOWNICY PRC on WND.WND_PRC_ID=PRC.PRC_ID
	WHERE DKZ.DKZ_DOK_ID=p_DKZ_DOK_ID
	;
	
	msg:= 'Przygotowano dokument '||s_typ_dokumentu||' dla pracownika ' || s_pracownik || ' z dat� rozliczenia: ' || 
         s_data_wyplaty || ' r. zwi�zan� z delegacj�: '|| s_numer ||' na kwot�: '||s_kwota ||' '||s_wal_symbol || ', numer dokumentu: '||s_dok_id ||'.';




  FOR i IN c_odbiorcy
  LOOP
    --DBMS_OUTPUT.PUT_LINE(i.uzt);
	--Pamietac zeby zmienic adres w US
	PPADM.WIADOMOSC_UZT(i.uzt, msg, 'Nowa zaliczka dla pracownika '|| s_pracownik, P_WDM_TYP => 'M', P_WDM_ADRES=>'mikolaj.golda@comarch.pl');
  END LOOP;
  
  EXCEPTION
    WHEN NO_DATA_FOUND THEN 
        null;

END;
/




