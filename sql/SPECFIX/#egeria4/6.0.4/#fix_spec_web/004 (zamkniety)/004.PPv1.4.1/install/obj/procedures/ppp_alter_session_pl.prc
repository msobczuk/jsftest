CREATE OR REPLACE PROCEDURE PPADM.ALTER_SESSION_PL as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
begin 
  execute immediate 'alter session set NLS_LANGUAGE = ''POLISH'' ';
  execute immediate 'alter session set NLS_TERRITORY = ''POLAND'' ';
  execute immediate 'alter session set NLS_CURRENCY = ''z�'' ';
  execute immediate 'alter session set NLS_ISO_CURRENCY = ''POLAND'' ';
  execute immediate 'alter session set NLS_NUMERIC_CHARACTERS = '', '' ';
  execute immediate 'alter session set NLS_CALENDAR = ''GREGORIAN'' ';
  execute immediate 'alter session set NLS_DATE_FORMAT = ''YYYY/MM/DD HH24:MI:SS'' ';
  execute immediate 'alter session set NLS_DATE_LANGUAGE = ''POLISH'' ';
  execute immediate 'alter session set NLS_SORT = ''POLISH'' ';
  execute immediate 'alter session set NLS_TIME_FORMAT = ''HH24:MI:SSXFF'' ';
  execute immediate 'alter session set NLS_TIMESTAMP_FORMAT = ''YYYY/MM/DD HH24:MI:SSXFF'' ';
  execute immediate 'alter session set NLS_TIME_TZ_FORMAT = ''HH24:MI:SSXFF TZR'' ';
  execute immediate 'alter session set NLS_TIMESTAMP_TZ_FORMAT = ''YYYY/MM/DD HH24:MI:SSXFF TZR'' ';
  execute immediate 'alter session set NLS_DUAL_CURRENCY = ''z�'' ';
  execute immediate 'alter session set NLS_COMP = ''BINARY'' ';
  execute immediate 'alter session set NLS_LENGTH_SEMANTICS = ''BYTE'' ';
  execute immediate 'alter session set NLS_NCHAR_CONV_EXCP = ''FALSE'' ';
end;
/



