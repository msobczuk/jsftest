CREATE OR REPLACE FUNCTION PPADM.PRZELOZONY (P_PRC_ID NUMBER, P_DATA DATE default sysdate) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 v_prc_id_prz NUMBER;
BEGIN
  
  ppp_global.ustaw_date_dla_struktury_po(P_DATA);
  SELECT SPOD_PRC_ID_PRZ into v_prc_id_prz 
  FROM PPV_STRUKTURA_PODLEGLOSCI 
  WHERE 
		SPOD_PRC_ID  = P_PRC_ID
    and rownum=1; 
  
  return v_prc_id_prz;

  EXCEPTION
	WHEN OTHERS THEN
		return null;
  
END;
/









