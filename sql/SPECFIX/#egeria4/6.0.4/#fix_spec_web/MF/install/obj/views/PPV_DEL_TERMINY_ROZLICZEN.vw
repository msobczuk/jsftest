CREATE OR REPLACE VIEW ppadm.ppv_del_terminy_rozliczen AS 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------  
SELECT  wnd_id, MAX(data_1)+14 AS termin_rozliczenia FROM 
(
 
  SELECT wnd_data_wyjazdu as data_1, wnd_id
  FROM PPT_DEL_WNIOSKI_DELEGACJI 
  
  UNION 
  
  SELECT wnd_data_powrotu as data_1, wnd_id
  FROM PPT_DEL_WNIOSKI_DELEGACJI 
  
  UNION 
  
  SELECT trs_data_do, kal_wnd_id
  FROM PPT_DEL_TRASY
    JOIN PPT_DEL_KALKULACJE ON trs_kal_id = kal_id
) 
GROUP BY WND_ID;
/




















































