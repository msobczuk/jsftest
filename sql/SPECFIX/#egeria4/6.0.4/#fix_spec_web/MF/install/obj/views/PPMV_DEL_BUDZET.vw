create materialized view ppadm.ppmv_del_budzet refresh complete start with (sysdate) next (sysdate + 15/1440) with rowid -- co 15 minut
-- drop materialized view ppadm.ppmv_del_budzet;
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
as
/*
Wnioski wg zrodeł finanosowania i ich kwota zaangażowania
Dla wniosku ustalane są wymiary wg tabeli realizacji, w której odkładane są wpisy na bazie stanowisk kosztów i kont ksiegowych.
Dla wymiarów ustalne jest SK aktulane na dzień 1 stycznia roku podziału.

UWAGA: może się zdarzyc ze nie ma stanowiska dopsowanego do wymiaru wynikajacego z realizacji wtedy SK_ID jest puste

W widoku budżetów można na dzień dobry dodać warunek aby kwota_zaangazowania była >=0 oraz posortowac dane wg:
1. wnr_data_wystawienia
2. wnr_id
3. wymiary_zf
4. rok_podzialu
*/
select numer_centrala,
       numer_resort,
       wymiary_zf,
       wymiary_zf_skrocony,
       sk_kod,
       sk_kod_skrocony,
       sk_id,
       sk_opis,
       rok_podzialu,
       kwota_zaangazowana,
       (rozlicz_wn+niezreal_umowy+zobowiazania_do_wn+zobowiazania_do_um+nieezreal_zw_do_wn+wart_kal_del+wart_rozl_del) wykorzystano,
       greatest(kwota_zaangazowana-(rozlicz_wn+niezreal_umowy+zobowiazania_do_wn+zobowiazania_do_um+nieezreal_zw_do_wn+wart_kal_del+wart_rozl_del),0) pozostalo,
       case when kwota_zaangazowana <> 0  then
       greatest(round((kwota_zaangazowana-(rozlicz_wn+niezreal_umowy+zobowiazania_do_wn+zobowiazania_do_um+nieezreal_zw_do_wn+wart_kal_del+wart_rozl_del))*100/kwota_zaangazowana,2),0)
       else 0
       end procent_pozostalo
from
(
select numer_centrala,
       numer_resort,
       (case when v.dec is not null then 'ZF' else 'WY' end)||'-'||v.dys || '-' || v.roz || '-' || v.par || '-' || v.dep  ||'-' || v.rez
       || '-' || v.zrz || '-' || v.tyt || '-' || v.prg|| '-' || v.prj|| '-' || v.twy||
       '-' ||v.bza || '-' || v.sym || '-' || v.kme || (case when v.dec is not null then '-' || v.dec else '' end) wymiary_zf,
       (case when v.dec is not null then 'ZF'  else 'WY' end)||'-'||v.dys || '-' || v.roz || '-' || v.par || '-' || v.dep || '-' || v.zrz || '-' || v.tyt || '-' || v.bza || '-' || v.sym || '-' || v.kme || (case when v.dec is not null then '-' || v.dec else '' end) wymiary_zf_skrocony,
       sk_kod,
       case when s.sk_id is not null then
                 tsk_kod || '-' || s.dys || '-' || s.roz || '-' || s.par || '-' || s.dep || '-' || s.zrz || '-' || s.tyt || '-' || s.bza || '-' || s.sym || '-' || s.kme || (case when s.dec is not null then '-' || s.dec else '' end)
            else ''
            end sk_kod_skrocony,
       s.sk_id,
       sk_opis,
       cast(rok as varchar2(10)) rok_podzialu,
       wdrp_zak_kontrola_budzetowa_mf.zaang_ks_wn_bez_um /*zaangazowanie wydatkow zaksiegowane*/
                                      (p_wnr_id      => wnr_id,
                                       p_rlw_id      => NULL,
                                       p_rok         => rok,
                                       p_dys         => v.dys,
                                       p_rdz         => v.rdz,
                                       p_roz         => v.roz,
                                       p_par         => v.par,
                                       p_dep         => v.dep,
                                       p_rez         => v.rez,
                                       --p_gpr         => gpr,
                                       p_zrz         => v.zrz,
                                       p_tyt         => v.tyt,
                                       p_prg         => v.prg,
                                       p_prj         => v.prj,
                                       p_twy         => v.twy,
                                       p_bza         => v.bza,
                                       p_sym         => v.sym,
                                       p_kme         => v.kme)
                                       +
      wdrp_zak_kontrola_budzetowa_mf.zaang_wd_wn_bez_um /*zaangazowanie wydatkow w drodze*/
                                      (p_wnr_id      => wnr_id,
                                       p_rlw_id      => NULL,
                                       p_rok         => rok,
                                       p_dys         => v.dys,
                                       p_rdz         => v.rdz,
                                       p_roz         => v.roz,
                                       p_par         => v.par,
                                       p_dep         => v.dep,
                                       p_rez         => v.rez,
                                       --p_gpr         => gpr,
                                       p_zrz         => v.zrz,
                                       p_tyt         => v.tyt,
                                       p_prg         => v.prg,
                                       p_prj         => v.prj,
                                       p_twy         => v.twy,
                                       p_bza         => v.bza,
                                       p_sym         => v.sym,
                                       p_kme         => v.kme
                                      )
        +
      wdrp_zak_kontrola_budzetowa_mf.zaang_zf_wn_bez_um /*zaangazowanie zapewnienia finansowania*/
                                      (p_wnr_id      => wnr_id,
                                       p_rlw_id      => NULL,
                                       p_rok         => rok,
                                       p_dys         => v.dys,
                                       p_rdz         => v.rdz,
                                       p_roz         => v.roz,
                                       p_par         => v.par,
                                       p_dep         => v.dep,
                                       p_rez         => v.rez,
                                       --p_gpr         => gpr,
                                       p_zrz         => v.zrz,
                                       p_tyt         => v.tyt,
                                       p_prg         => v.prg,
                                       p_prj         => v.prj,
                                       p_twy         => v.twy,
                                       p_bza         => v.bza,
                                       p_sym         => v.sym,
                                       p_kme         => v.kme
                                      )
                                       kwota_zaangazowana /*bieżąca kwota zaangażowania*/,
wdrp_zak_kontrola_budzetowa_mf.rozliczenie_zaang_wn
                                    (p_wnr_id      => v.wnr_id,
                                     p_rlw_id      => NULL,
                                     p_rok         => v.rok,
                                     p_dys         => v.dys,
                                     p_rdz         => v.rdz,
                                     p_roz         => v.roz,
                                     p_par         => v.par,
                                     p_dep         => v.dep,
                                     p_rez         => v.rez,
                                     p_gpr         => NVL (v.gpr, '00'),
                                     p_zrz         => v.zrz,
                                     p_tyt         => v.tyt,
                                     p_prg         => v.prg,
                                     p_prj         => v.prj,
                                     /*p_twy  => twy,*/
                                     p_bza         => v.bza,
                                     p_sym         => v.sym,
                                     p_kme         => v.kme
                                    ) rozlicz_wn /*Rozliczenie zaang.*/,
                 wdrp_zak_kontrola_budzetowa_mf.wart_nieezreal_um_do_wn
                    (p_wnr_id                     => v.wnr_id,
                     p_rlw_id                     => NULL,
                     p_rok                        => v.rok,
                     p_uwzgl_stan_stop_obieg      => wdrp_zak_kontrola_budzetowa_mf.f_g_uwzgl_stan_stop_w_obiegu,
                     p_dys                        => v.dys,
                     p_rdz                        => v.rdz,
                     p_roz                        => v.roz,
                     p_par                        => v.par,
                     p_dep                        => v.dep,
                     p_rez                        => v.rez,
                     p_gpr                        => NVL (v.gpr, '00'),
                     p_zrz                        => v.zrz,
                     p_tyt                        => v.tyt,
                     p_prg                        => v.prg,
                     p_prj                        => v.prj,
                     /*p_twy  => twy,*/
                     p_bza                        => v.bza,
                     p_sym                        => v.sym,
                     p_kme                        => v.kme
                    ) niezreal_umowy /*niezrealizowane umowy*/,
                 wdrp_zak_kontrola_budzetowa_mf.wart_real_zob_do_wn
                    (p_wnr_id                     => v.wnr_id,
                     p_rlw_id                     => NULL,
                     p_rok                        => v.rok,
                     p_uwzgl_stan_stop_obieg      => wdrp_zak_kontrola_budzetowa_mf.f_g_uwzgl_stan_stop_w_obiegu,
                     p_dys                        => v.dys,
                     p_rdz                        => v.rdz,
                     p_roz                        => v.roz,
                     p_par                        => v.par,
                     p_dep                        => v.dep,
                     p_rez                        => v.rez,
                     p_gpr                        => NVL (v.gpr, '00'),
                     p_zrz                        => v.zrz,
                     p_tyt                        => v.tyt,
                     p_prg                        => v.prg,
                     p_prj                        => v.prj,
                     /*p_twy  => twy,*/
                     p_bza                        => v.bza,
                     p_sym                        => v.sym,
                     p_kme                        => v.kme
                    ) zobowiazania_do_wn /*zobowiązania do wniosku*/,
                 wdrp_zak_kontrola_budzetowa_mf.wart_real_um_do_wn
                    (p_wnr_id                     => v.wnr_id,
                     p_rlw_id                     => NULL,
                     p_rok                        => v.rok,
                     p_uwzgl_stan_stop_obieg      => wdrp_zak_kontrola_budzetowa_mf.f_g_uwzgl_stan_stop_w_obiegu,
                     p_dys                        => v.dys,
                     p_rdz                        => v.rdz,
                     p_roz                        => v.roz,
                     p_par                        => v.par,
                     p_dep                        => v.dep,
                     p_rez                        => v.rez,
                     p_gpr                        => NVL (v.gpr, '00'),
                     p_zrz                        => v.zrz,
                     p_tyt                        => v.tyt,
                     p_prg                        => v.prg,
                     p_prj                        => v.prj,
                     /*p_twy  => twy,*/
                     p_bza                        => v.bza,
                     p_sym                        => v.sym,
                     p_kme                        => v.kme
                    ) zobowiazania_do_um /*zobowiązania do umowy*/,
                 wdrp_zak_kontrola_budzetowa_mf.wart_nieezreal_zw_do_wn
                    (p_wnr_id                     => v.wnr_id,
                     p_rlw_id                     => NULL,
                     p_rok                        => v.rok,
                     p_uwzgl_stan_stop_obieg      => wdrp_zak_kontrola_budzetowa_mf.f_g_uwzgl_stan_stop_w_obiegu,
                     p_dys                        => v.dys,
                     p_rdz                        => v.rdz,
                     p_roz                        => v.roz,
                     p_par                        => v.par,
                     p_dep                        => v.dep,
                     p_rez                        => v.rez,
                     p_gpr                        => NVL (v.gpr, '00'),
                     p_zrz                        => v.zrz,
                     p_tyt                        => v.tyt,
                     p_prg                        => v.prg,
                     p_prj                        => v.prj,
                     /*p_twy  => twy,*/
                     p_bza                        => v.bza,
                     p_sym                        => v.sym,
                     p_kme                        => v.kme
                    ) nieezreal_zw_do_wn /*niezrealizowane zamówienia do wniosków*/,
                 wdrp_zak_kontrola_budzetowa_mf.wart_kal_del
                                       (p_wnd_id               => NULL,
                                        p_wnr_id               => v.wnr_id,
                                        p_rok                  => v.rok,
                                        p_dys                  => v.dys,
                                        p_rdz                  => v.rdz,
                                        p_roz                  => v.roz,
                                        p_par                  => v.par,
                                        p_dep                  => v.dep,
                                        p_rez                  => v.rez,
                                        p_gpr                  => NVL (v.gpr,
                                                                       '00'
                                                                      ),
                                        p_zrz                  => v.zrz,
                                        p_tyt                  => v.tyt,
                                        p_prg                  => v.prg,
                                        p_prj                  => v.prj,
                                        /*p_twy  => twy,*/
                                        p_bza                  => v.bza,
                                        p_sym                  => v.sym,
                                        p_kme                  => v.kme,
                                        p_par_bez_blokady      => 'T'
                                       ) wart_kal_del /*nirzliczone delegace*/,
                 wdrp_zak_kontrola_budzetowa_mf.wart_rozl_del
                                      (p_wnd_id               => NULL,
                                       p_wnr_id               => v.wnr_id,
                                       p_rok                  => v.rok,
                                       p_dys                  => v.dys,
                                       p_rdz                  => v.rdz,
                                       p_roz                  => v.roz,
                                       p_par                  => v.par,
                                       p_dep                  => v.dep,
                                       p_rez                  => v.rez,
                                       p_gpr                  => NVL (v.gpr,
                                                                      '00'
                                                                     ),
                                       p_zrz                  => v.zrz,
                                       p_tyt                  => v.tyt,
                                       p_prg                  => v.prg,
                                       p_prj                  => v.prj,
                                       /*p_twy  => twy,*/
                                       p_bza                  => v.bza,
                                       p_sym                  => v.sym,
                                       p_kme                  => v.kme,
                                       p_par_bez_blokady      => 'T'
                                      ) wart_rozl_del     /*rozliczone delegcje*/
from egadm1.wdrv_fk_wymiary_i_sk_wnioskow v, egadm1.wdrv_css_stanowiska_kosztow s
where v.sk_id = s.sk_id(+)
 and rok >= ek_pck_utl01.rok_z_daty(sysdate)-1 /* ograniczenie źrodeł i wniosków do tych z źrodłami finansowania na rok bieżący - 1 w górę*/
 and (v.par LIKE '303_'
      OR v.par LIKE '430_'
      OR v.par LIKE '441_'
      OR v.par LIKE '442_'
      OR v.par LIKE '443_'
      OR v.par LIKE '455_'
      OR v.par LIKE '470_')
 and exists (select 1
               from csst_encje_w_stanach, csst_def_stanow
              where ews_klucz_obcy_id = wnr_id
                and ews_dstn_id = dstn_id
                and dstn_nazwa in ('ZATWIERDZONY','REALIZACJA')
                and ews_kob_kod like 'WNR%')

)
/
