CREATE OR REPLACE VIEW ppadm.ppv_del_data_wyp_zaliczki AS 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------  
SELECT 
  --MIN(DKZ.DKZ_DATA_WYP) data_wyplaty_zaliczki,
  MAX(DKZ.DKZ_DATA_WYP) data_wyplaty_zaliczki,
  WND.WND_ID
FROM PPT_DEL_WNIOSKI_DELEGACJI WND
  join PPT_DEL_KALKULACJE KAL on (KAL.KAL_WND_ID=WND.WND_ID and KAL.KAL_RODZAJ='WST')
  JOIN PPT_DEL_ZALICZKI DKZ ON DKZ.DKZ_KAL_ID=KAL.KAL_ID
WHERE DKZ.DKZ_DATA_WYP IS NOT NULL
GROUP BY  WND.WND_ID;
/




















































