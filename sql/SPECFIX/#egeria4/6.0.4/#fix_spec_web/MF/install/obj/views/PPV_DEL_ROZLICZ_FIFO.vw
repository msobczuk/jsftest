create or replace view ppadm.ppv_del_rozlicz_fifo as 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------  
-- https://asktom.oracle.com/pls/asktom/asktom.search?tag=how-to-create-fifo-report-based-on-evaluation-system
with zaliczki as (
    --zaliczki / mock 
    select 
        
        zal.*,

        sum (ZAL_KWOTA) -- FIRST IN ...
         over (partition by zal_wal_id order by zal_id rows between unbounded preceding and current row) 
         as zal_run_total,

        nvl(sum (ZAL_KWOTA) -- FIRST IN ...
         over (partition by zal_wal_id order by zal_id rows between unbounded preceding and 1 preceding), 0) 
         as zal_prev_run_total        

    from (
        --------------------------------------------------------------------------
        -- zaliczki i wyp�aty z n/z (dp przpisania na realne dane)
        --------------------------------------------------------------------------
        
        
        select 
          zal.dkz_id zal_id, 
          CASE when zal.dkz_kwota > zal.dkz_srodki_wlasne then zal.dkz_zal_wydano
          ELSE zal.dkz_srodki_wlasne + zal.dkz_zal_wydano END ZAL_KWOTA,
          zal.dkz_wal_id zal_wal_id,
          dok.dok_kurs ZAL_KURS --- kurs w sumie nie zawsze ten ale mo�na i tak bo b�dzie na kgt przeciez te� ustawiony
          from ppt_del_zaliczki zal 
          join ppt_del_kalkulacje kal on zal.dkz_kal_id = kal.kal_id
          join kgt_dokumenty dok on zal.dkz_dok_id = dok.dok_id
          where dkz_dok_id is not null and kal.kal_rodzaj = 'ROZ'
          and kal_wnd_id = PPP_INTEGRACJA_FIN.odczytaj_rozlicz_fifo_wnd_id --262 -- tu zmienna
          
          --order by kal.kal_id desc
        
        
       -- select 1 zal_id, 60.00 ZAL_KWOTA, 20 zal_wal_id, 5.556677 ZAL_KURS from dual -- pierwszy dok N/Z
       -- union
      --  select 2 zal_id, 25.67 ZAL_KWOTA, 20 zal_wal_id, 5.445566 ZAL_KURS from dual -- drugi dok N/Z
        --------------------------------------------------------------------------
        --------------------------------------------------------------------------
         
        union --sztuczny zas�b dla tej czesci pkl, ktore nie maja pokrycia w w/w dok n/z
        select 
            9999999999 zal_id, -- celowo wysoki
            9999999.99 ZAL_KWOTA, --celowo bardzo duzo
            20 zal_wal_id, 
            5.00000 ZAL_KURS --sztucznie ustalony kurs dla pozycji nie majacych pokrycia w dok N/Z
        from dual        
        --------------------------------------------------------------------------
    ) zal
),

pkl as (
    select 
        pkl.*,
        
        sum (pkl_kwota_mf) --  ... FIRST OUT
         over (partition by pkl_wal_id order by pkl_id rows between unbounded preceding and current row) 
         as pkl_run_total,

        nvl(sum (pkl_kwota_mf) --  ... FIRST OUT
         over (partition by pkl_wal_id order by pkl_id rows between unbounded preceding and 1 preceding), 0) 
         as pkl_prev_run_total        
    from ( 
          --------------------------------------------------------------------------
          -- pozycje kalkulacji rozliczajacej 
          --------------------------------------------------------------------------
          select -- pozycje kalkulacji
              pkl_id, pkl_kwota_mf, pkl_wal_id, pkl_tpoz_id, pkzf_sk_id, pkzf_kwota    
          from ppt_del_kalkulacje
              join ppt_del_pozycje_kalkulacji pkl on pkl_kal_id = kal_id
              join ppt_del_zrodla_finansowania zf on zf.pkzf_pkl_id = pkl.pkl_id
          where kal_wnd_id = PPP_INTEGRACJA_FIN.odczytaj_rozlicz_fifo_wnd_id
               and kal_rodzaj = 'ROZ'
               and pkl.Pkl_Forma_Platnosci in ('01', '04')
               --and pkl.pkl_wal_id = 20
          --------------------------------------------------------------------------
          --------------------------------------------------------------------------
    ) pkl     
)

select --*
    zal_id, pkl_id, zal_kurs, zal_wal_id, pkl_tpoz_id,
    pkl_kwota_mf, 
    zal_kwota,  
    pkzf_sk_id, -- id zf
    pkzf_kwota, -- kwota zf
    --pkl_prev_run_total, pkl_run_total,  
    --zal_prev_run_total, zal_run_total,  
    least (
       pkl_kwota_mf,
       zal_kwota,
       pkl_run_total - zal_prev_run_total,
       zal_run_total - pkl_prev_run_total
     ) PKL_KWOTA_PER_KURS,
     
    round( 
    least (
       pkl_kwota_mf,
       zal_kwota,
       pkl_run_total - zal_prev_run_total,
       zal_run_total - pkl_prev_run_total
     ) * zal_kurs
     ,2)
     PKL_KWOTA_PLN
     
from zaliczki
    join pkl on zal_wal_id = pkl_wal_id
          and    pkl_run_total >= zal_prev_run_total
          and    zal_run_total >= pkl_prev_run_total
          and     least (
                   pkl_kwota_mf,
                   zal_kwota,
                   pkl_run_total - zal_prev_run_total,
                   zal_run_total - pkl_prev_run_total
                 ) > 0
order  by zal_id, pkl_id;
/




