create or replace view ppadm.ppv_del_terminy_wykupu_hoteli as
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------  
SELECT
  WN.WND_ID,
  LISTAGG(TO_CHAR(PKL.PKL_TERMIN_WYKUPU,'YYYY-MM-DD HH24:Mi'), '; ') WITHIN GROUP (ORDER BY PKL.PKL_TERMIN_WYKUPU) termin_platnosci_za_hotel
FROM PPT_DEL_WNIOSKI_DELEGACJI WN
  join PPT_DEL_KALKULACJE KAL on KAL.KAL_WND_ID = WN.WND_ID and KAL.KAL_RODZAJ='WST'
  join PPT_DEL_POZYCJE_KALKULACJI PKL on PKL.PKL_KAL_ID=KAL.KAL_ID 
  JOIN PPT_DEL_TYPY_POZYCJI TPOZ ON TPOZ.TPOZ_ID=PKL.PKL_TPOZ_ID AND TPOZ.TPOZ_KOD='DH'
WHERE PKL.PKL_TERMIN_WYKUPU IS NOT NULL
GROUP BY wn.wnd_id;
/









































