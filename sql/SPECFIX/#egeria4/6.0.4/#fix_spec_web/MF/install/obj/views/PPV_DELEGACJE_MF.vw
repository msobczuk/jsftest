create or replace view ppadm.ppv_delegacje_mf as  
select
-- $Revision::          $-- 
    x.*,
    
    (
      CASE  
      WHEN dstn_nazwa = 'START' THEN 'Wprowadzona'
      WHEN (wnd_rodzaj = '1' and dstn_nazwa IN('POZIOM10','POZIOM20')) or (wnd_rodzaj = '2' and dstn_nazwa IN('POZIOM05','POZIOM10')) THEN 'Do akceptacji'
      WHEN (wnd_rodzaj = '1' AND dstn_nazwa IN('POZIOM30') ) OR (wnd_rodzaj='2' AND dstn_nazwa IN('POZIOM20','POZIOM23','POZIOM27','POZIOM30','POZIOM40','POZIOM45')) THEN 'Zaakceptowana'
      WHEN (wnd_rodzaj = '1' AND dstn_nazwa IN('POZIOM40') ) OR (wnd_rodzaj='2' AND dstn_nazwa IN('POZIOM50','POZIOM60')) THEN 'Zatwierdzona'
      WHEN dstn_nazwa = 'ANULOWANY' AND wnd_f_bezkoszt = 'T' THEN 'Anulowana bezkosztowa'
      WHEN dstn_nazwa = 'ANULOWANY' AND wnd_f_bezkoszt = 'N' THEN 'Anulowana z kosztami'
      WHEN (wnd_rodzaj = '1' AND dstn_nazwa IN('POZIOM50','POZIOM60') ) OR (wnd_rodzaj='2' AND dstn_nazwa IN('POZIOM70','POZIOM73','POZIOM77','POZIOM80','POZIOM90','POZIOM100')) THEN 'Przekazana do rozliczenia'
      WHEN dstn_nazwa = 'STOP' THEN 'Rozliczona'
      END 
    ) as Status1, 
    
    (
      SELECT DKZ.DKZ_DATA_WYP
      FROM PPT_DEL_WNIOSKI_DELEGACJI WNDZ
        join PPT_DEL_KALKULACJE KAL on (KAL.KAL_WND_ID=WNDZ.WND_ID and KAL.KAL_RODZAJ='WST')
        join PPT_DEL_ZALICZKI DKZ on DKZ.DKZ_KAL_ID=KAL.KAL_ID
      WHERE 
        rownum=1
        and x.WND_ID=WNDZ.WND_ID
    ) data_wyplaty_zaliczki, 
    
        
    (
        SELECT 
          LISTAGG(TO_CHAR(TRS.TRS_TERMIN_WYKUPU_BILETU,'YYYY-MM-DD HH24:Mi'), '; ') WITHIN GROUP (ORDER BY TRS.TRS_TERMIN_WYKUPU_BILETU)
        FROM PPT_DEL_WNIOSKI_DELEGACJI WN
        join PPT_DEL_KALKULACJE KAL on KAL.KAL_WND_ID = WN.WND_ID and KAL.KAL_RODZAJ='WST'
        join PPT_DEL_TRASY TRS on TRS.TRS_KAL_ID=KAL.KAL_ID
        WHERE WN.WND_ID=x.WND_ID
    ) termin_wykupu_bilety, 

     
    (  
        SELECT 
        LISTAGG(TO_CHAR(PKL.PKL_TERMIN_WYKUPU,'YYYY-MM-DD HH24:Mi'), '; ') WITHIN GROUP (ORDER BY PKL.PKL_TERMIN_WYKUPU)
        FROM PPT_DEL_WNIOSKI_DELEGACJI WN
        join PPT_DEL_KALKULACJE KAL on KAL.KAL_WND_ID = WN.WND_ID and KAL.KAL_RODZAJ='WST'
        join PPT_DEL_POZYCJE_KALKULACJI PKL on PKL.PKL_KAL_ID=KAL.KAL_ID 
        join PPT_DEL_TYPY_POZYCJI TPOZ on TPOZ.TPOZ_ID=PKL.PKL_TPOZ_ID and TPOZ.TPOZ_KOD='DH'
        WHERE WN.WND_ID=x.WND_ID
    ) termin_platnosci_za_hotel,

 
    case when exists ( 
                Select 1 --sum(Pkl.Pkl_Refundacja_Kwota)
                From Ppt_Del_Kalkulacje KAL
                join Ppt_Del_Pozycje_Kalkulacji PKL on KAL.KAL_ID = Pkl.Pkl_Kal_Id
                Where KAL.KAL_WND_ID=x.WND_ID
                    and Kal.Kal_Rodzaj = 'WST' 
                    and Pkl.Pkl_Refundacja_Kwota <> 0
             ) then 'T' else 'N'
    end as czy_refundacja    

from (
         SELECT
         WND.WND_ID wnd_id,
         WND.WND_RODZAJ,
         --decode(wnd.wnd_rodzaj, 2, ppadm.FAKTYCZNY_TERMIN_ROZ_DEL_Z_V2(wnd.wnd_id), null) data_rozliczenia,
         WND.WND_DATA_WYJAZDU wnd_data_wyjazdu,
         WND.WND_DATA_POWROTU wnd_data_powrotu, 
         WND.WND_NUMER wnd_numer,
         WND.WND_F_BEZKOSZTOWA wnd_f_bezkoszt,
         decode(WND_POW.WND_NUMER, null, 'Brak delegacji powiązanej', WND_POW.WND_NUMER)wnd_numer_pow,
         PRC.PRC_IMIE || ' ' || PRC.PRC_NAZWISKO prc_imie_nazwisko,
         decode(WND.WND_STATUS, 0, 'anulowany', 'w toku') wnd_status,
         
         decode(WND.WND_RODZAJ, 2, 'Zagraniczna', 'Krajowa') wnd_rodzaj_opis,
         decode( decode(WND.WND_RODZAJ, 2, ewsz.dstn_opis, ewsk.dstn_opis), null, 'Obieg nie rozpoczęty', decode(WND.WND_RODZAJ, 2, ewsz.dstn_opis, ewsk.dstn_opis)) pp_stan,
         ewsz.dstn_nazwa || ewsk.dstn_nazwa   as dstn_nazwa ,
         ewsz.EWS_KOB_KOD || ewsk.EWS_KOB_KOD as EWS_KOB_KOD,
         ob.OB_KOD departament,
        
         kwota_pln_wst wstepny_koszt, -- PPADM.KOSZTY_DELEGACJI(WND.WND_ID ,'WST') wstepny_koszt,
         kwota_pln_roz poniesiony_koszt, -- PPADM.KOSZTY_DELEGACJI(WND.WND_ID ,'ROZ')   poniesiony_koszt,
         zal_wst.suma_pln suma_zaliczek,
		 wnd.wnd_opis cel_wyjazdu
         
        FROM  PPADM.PPT_DEL_WNIOSKI_DELEGACJI WND
              JOIN EK_PRACOWNICY PRC ON PRC.PRC_ID= WND.WND_PRC_ID
              LEFT JOIN PPADM.PPT_DEL_WNIOSKI_DELEGACJI WND_POW ON WND.WND_WND_ID = WND_POW.WND_ID
              left JOIN ek_zatrudnienie ZAT on (ZAT.ZAT_PRC_ID=PRC.PRC_ID and zat.zat_typ_umowy = 0 AND trunc(SYSDATE) >= zat.zat_data_zmiany AND trunc(SYSDATE) <= nvl(zat.zat_data_do,trunc(SYSDATE) ))
              left JOIN CSS_OBIEKTY_W_PRZEDSIEB ob on ob.OB_ID=ZAT.ZAT_OB_ID
              
              left join ppv_del_wart_pln_wst wartplnwst on wartplnwst.wnd_id = wnd.wnd_id
              left join ppv_del_wart_pln_roz wartplnroz on wartplnroz.wnd_id = wnd.wnd_id
              left join (SELECT
                            KAL_WND_ID wnd_id,
                            sum(kwota_pln) suma_pln
                          FROM PPV_DELZ_DKZ_WART_PLN
                          WHERE 1=1
                            and KAL_RODZAJ = 'WST'
                            and DKZ_DOK_ID is not null
                            and nvl(dok_f_anulowany,'N') <> 'T'
                        GROUP BY KAL_WND_ID
                        )zal_wst on zal_wst.wnd_id = wnd.wnd_id
              
              left join ppv_encje_w_stanach EWSZ on EWSZ.EWS_KOB_KOD = 'WN_DELZv2'  and  EWSZ.EWS_KLUCZ_OBCY_ID = WND.WND_ID 
              left join ppv_encje_w_stanach EWSK on EWSK.EWS_KOB_KOD = 'WN_DELKv2'  and  EWSK.EWS_KLUCZ_OBCY_ID = WND.WND_ID 
        
              , ppv_zalogowany u 
              , (select count(*) cnt from ppv_zalogowany_funkcje where fu_nazwa = 'PP_DELZ_ADMINISTRATOR') FU_DELZ_ADM
              , (select count(*) cnt from ppv_zalogowany_funkcje where fu_nazwa = 'PP_DELK_ADMINISTRATOR') FU_DELK_ADM
              , (select count(*) cnt from ppv_zalogowany_funkcje where fu_nazwa = 'PP_ADMINISTRATOR') FU_PP_ADM
        
        WHERE wnd.wnd_prc_id = u.prc_id  
        
            OR (FU_DELK_ADM.cnt > 0 and WND.WND_RODZAJ=1)
            OR (FU_DELZ_ADM.cnt > 0 and WND.WND_RODZAJ=2)
            OR FU_PP_ADM.cnt > 0
) x
ORDER BY WND_ID DESC
;
/








































































