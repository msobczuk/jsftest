create or replace view PPADM.PPV_DEL_WND_ZF as
SELECT 
-- $Revision:: 1               $ ---
  kal_wnd_id wnd_id,
  LISTAGG(kod_skrocony,'; ') WITHIN GROUP (ORDER BY pkzf_sk_id) zrodla_finansowania
from(
SELECT DISTINCT
  kal.KAL_WND_ID,
  --pkzf.pkzf_id,
  pkzf.pkzf_sk_id,
  tsk_kod || '-' || dys || '-' || rdz || '-' || roz || '-' || par || '-' || dep || '-' || zrz || '-' || tyt || '-' || bza || '-' || sym || '-' || kme || (CASE WHEN DEC IS NOT NULL THEN '-' || DEC ELSE '' END) kod_skrocony
FROM PPT_DEL_ZRODLA_FINANSOWANIA pkzf 
join egadm1.wdrv_css_stanowiska_kosztow sk on sk.sk_id = pkzf.pkzf_sk_id
join PPT_DEL_POZYCJE_KALKULACJI pkl on pkl.pkl_id = pkzf.PKZF_PKL_ID
join PPT_DEL_KALKULACJE kal on kal.kal_id = pkl.PKL_KAL_ID
--WHERE kal.KAL_RODZAJ = 'WST'
)
GROUP BY kal_wnd_id
ORDER BY kal_wnd_id desc
;
/

