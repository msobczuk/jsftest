create or replace view ppadm.ppv_del_suma_pkl_pln as
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------  
SELECT KAL_WND_ID, KAL_RODZAJ, 
  --ROUND(SUM(PKL.PKL_KWOTA_MF)*nvl(avg(PKL_KURS),1)/nvl(avg(WAL.WAL_JEDNOSTKA_KURSU),1), 2) kwota AVG?!!!!!
  ROUND(SUM( PKL.PKL_KWOTA_MF * nvl(PKL_KURS,1) / nvl(WAL.WAL_JEDNOSTKA_KURSU,1) ), 2 ) suma_pkl_pln
FROM PPT_DEL_KALKULACJE KAL
  JOIN PPT_DEL_POZYCJE_KALKULACJI PKL on PKL.PKL_KAL_ID=KAL.KAL_ID 
  JOIN EGADM1.CSS_WALUTY WAL ON WAL.WAL_ID=PKL.PKL_WAL_ID
WHERE 1=1 --KAL.KAL_WND_ID=p_WND_ID
      --AND KAL.KAL_RODZAJ=p_KAL_RODZAJ
      AND KAL_RODZAJ IN ('WST', 'ROZ')
GROUP BY KAL_WND_ID, KAL_RODZAJ--, wal.WAL_SYMBOL
;
/













































