CREATE OR REPLACE VIEW PPADM.PPV_ZAKT_WNIOSKI_REALIZATOROW as 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------  
select 
  wnr.*, 
  wnr.wnr_numer_wnioskodawcy
     || DECODE (wnr.wnr_numer_realizatora,
                NULL, NULL,
                DECODE (wnr.wnr_numer_wnioskodawcy,
                        NULL, wnr.wnr_numer_realizatora,
                        ', ' || wnr.wnr_numer_realizatora
                       )
               ) numer_lacznie
from ZAKT_WNIOSKI_REALIZATOROW wnr
/













