create or replace FUNCTION "PPADM"."GET_DEPARTAMENT_ID" (p_OB_ID NUMBER) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$Header::                                                                                                                                                                          $
$Revision::                                                                                                                                                                        $
$Workfile::                                                                                                                                                                        $
$Modtime::                                                                                                                                                                         $
$Author::                                                                                                                                                                          $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

BEGIN

    for r in (
      SELECT OB_ID
      FROM ek_obiekty_w_przedsieb
      WHERE 1=1
        and ob_ob_id is not null --and ob_id not in (1, 100001)
      START WITH OB_ID=p_OB_ID
      CONNECT BY NOCYCLE PRIOR OB_OB_ID=OB_ID
      ORDER BY LEVEL desc
		)loop
        return r.OB_ID;
    end loop;

    return p_ob_id; --null; 	
  
END;
/


