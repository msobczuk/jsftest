CREATE OR REPLACE VIEW "PPADM"."PPV_DEL_ROZLICZ_FIFO_1" ("PK", "PKL_ID", "TPOZ_KOD", "TPOZ_NAZWA", "DOK_ID", "ZAL_ID", "ZAL_KURS", "ZAL_WAL_ID", "WAL_SYMBOL", "PKL_TPOZ_ID", "PKL_KWOTA_MF", "ZAL_KWOTA", "PKZF_SK_ID", "PKZF_KWOTA", "PKL_KWOTA_PER_KURS", "PKL_KWOTA_PLN") AS
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
--------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
  with zaliczki as (
  --zaliczki / mock 
  select 
      zal.*,
      sum (ZAL_KWOTA) -- FIRST IN ...
       over (partition by zal_wal_id order by zal_id rows between unbounded preceding and current row) 
       as zal_run_total,

      nvl(sum (ZAL_KWOTA) -- FIRST IN ...
       over (partition by zal_wal_id order by zal_id rows between unbounded preceding and 1 preceding), 0) 
       as zal_prev_run_total        

  from (
      --------------------------------------------------------------------------
      -- zaliczki i wyp�aty z n/z (dp przpisania na realne dane)
      --------------------------------------------------------------------------
      
      SELECT --dok.DOK_WAL_ID_KAL, dok.DOK_WAL_ID_WYST, dok.DOK_WAL_ID_NIEOC,
          --zal.dkz_id zal_id, 
          dok.dok_id zal_id, 
--          CASE when zal.dkz_kwota > zal.dkz_srodki_wlasne then zal.dkz_zal_wydano
--          ELSE zal.dkz_srodki_wlasne + zal.dkz_zal_wydano END ZAL_KWOTA,
          dok.dok_kwota_waluty  ZAL_KWOTA,
          dok.DOK_WAL_ID_WYST  zal_wal_id,
          --zal.dkz_wal_id zal_wal_id,
           dok.dok_kurs ZAL_KURS --- kurs w sumie nie zawsze ten ale mo�na i tak bo b�dzie na kgt przeciez te� ustawiony
        from ppt_del_zaliczki zal 
          JOIN ppt_del_kalkulacje kal ON zal.dkz_kal_id = kal.kal_id
          join kgt_dokumenty dok ON zal.dkz_dok_id = dok.dok_id
        where dkz_dok_id is not null and kal.kal_rodzaj IN ('ROZ','WST')
          and kal_wnd_id = PPP_INTEGRACJA_FIN.odczytaj_rozlicz_fifo_wnd_id --262 -- tu zmienna
        
      
      
     -- select 1 zal_id, 60.00 ZAL_KWOTA, 20 zal_wal_id, 5.556677 ZAL_KURS from dual -- pierwszy dok N/Z
     -- union
    --  select 2 zal_id, 25.67 ZAL_KWOTA, 20 zal_wal_id, 5.445566 ZAL_KURS from dual -- drugi dok N/Z
      --------------------------------------------------------------------------
      --------------------------------------------------------------------------
       
--      union --sztuczny zas�b dla tej czesci pkl, ktore nie maja pokrycia w w/w dok n/z
--      select 
--          9999999999 zal_id, -- celowo wysoki
--          9999999.99 ZAL_KWOTA, --celowo bardzo duzo
--          20 zal_wal_id, 
--          5.00000 ZAL_KURS --sztucznie ustalony kurs dla pozycji nie majacych pokrycia w dok N/Z
--      from dual    
--      
--      union --sztuczny zas�b dla tej czesci pkl, ktore nie maja pokrycia w w/w dok n/z
--      select 
--          9999999999 zal_id, -- celowo wysoki
--          9999999.99 ZAL_KWOTA, --celowo bardzo duzo
--          1 zal_wal_id, 
--          5.00000 ZAL_KURS --sztucznie ustalony kurs dla pozycji nie majacych pokrycia w dok N/Z
--      FROM dual    
      
      UNION 
        SELECT DISTINCT
          9999999999 zal_id,
          9999999.99 ZAL_KWOTA,
          pkl.PKL_WAL_ID AS zal_wal_id, 
          0.00000 ZAL_KURS 
        FROM PPT_DEL_POZYCJE_KALKULACJI pkl
          JOIN PPT_DEL_KALKULACJE kal ON pkl.pkl_kal_id = kal.kal_id
        WHERE kal_wnd_id =  PPP_INTEGRACJA_FIN.odczytaj_rozlicz_fifo_wnd_id
        AND kal.kal_rodzaj IN ('ROZ', 'WST')

      
      order by zal_id
      
      --------------------------------------------------------------------------
  ) zal
),

pkl as (
  select 
      pkl.*,
      
      sum (pkl_kwota_mf) --  ... FIRST OUT
       over (partition by pkl_wal_id order by pkl_id rows between unbounded preceding and current row) 
       as pkl_run_total,

      nvl(sum (pkl_kwota_mf) --  ... FIRST OUT
       over (partition by pkl_wal_id order by pkl_id rows between unbounded preceding and 1 preceding), 0) 
       as pkl_prev_run_total        
  from ( 
        --------------------------------------------------------------------------
        -- pozycje kalkulacji rozliczajacej 
        --------------------------------------------------------------------------
        select -- pozycje kalkulacji
            pkl_id, pkl_kwota_mf, pkl_wal_id, pkl_tpoz_id, pkzf_sk_id, pkzf_kwota    
        from ppt_del_kalkulacje
            join ppt_del_pozycje_kalkulacji pkl on pkl_kal_id = kal_id
            JOIN ppt_del_zrodla_finansowania zf ON zf.pkzf_pkl_id = pkl.pkl_id
             join PPT_DEL_TYPY_POZYCJI tpoz on tpoz.tpoz_id = pkl.pkl_tpoz_id
        where kal_wnd_id = PPP_INTEGRACJA_FIN.odczytaj_rozlicz_fifo_wnd_id
             and kal_rodzaj = 'ROZ'
             and pkl.Pkl_Forma_Platnosci in ('01', '04')
             --and pkl.pkl_wal_id = 20
       order by  pkl_id
        --------------------------------------------------------------------------
        --------------------------------------------------------------------------
  ) pkl     
)

SELECT --*
  pkl_id||zal_id pk,
  pkl_id, 
  tpoz.tpoz_kod,
  tpoz.tpoz_nazwa, 
  decode (zal_id, 9999999999, null, zal_id) dok_id,  
  zal_id, 
  zal_kurs, 
  zal_wal_id, 
  wal_symbol,
  pkl_tpoz_id,
  pkl_kwota_mf, 
  decode (zal_kwota, 9999999.99, null, zal_kwota) zal_kwota,  
  pkzf_sk_id, -- id zf
  pkzf_kwota, -- kwota zf
  --pkl_prev_run_total, pkl_run_total,  
  --zal_prev_run_total, zal_run_total,  
  least (
     pkl_kwota_mf,
     zal_kwota,
     pkl_run_total - zal_prev_run_total,
     zal_run_total - pkl_prev_run_total
   ) PKL_KWOTA_PER_KURS,
   
  round( 
  least (
     pkl_kwota_mf,
     zal_kwota,
     pkl_run_total - zal_prev_run_total,
     zal_run_total - pkl_prev_run_total
   ) * zal_kurs
   ,2)
   PKL_KWOTA_PLN
   
from zaliczki
  join pkl on zal_wal_id = pkl_wal_id
        and    pkl_run_total >= zal_prev_run_total
        and    zal_run_total >= pkl_prev_run_total
        and     least (
                 pkl_kwota_mf,
                 zal_kwota,
                 pkl_run_total - zal_prev_run_total,
                 zal_run_total - pkl_prev_run_total
               ) > 0
   JOIN PPT_DEL_TYPY_POZYCJI tpoz ON tpoz.tpoz_id = pkl.pkl_tpoz_id
   join css_waluty on zal_wal_id = wal_id
order  by zal_id, pkl_id;
/





