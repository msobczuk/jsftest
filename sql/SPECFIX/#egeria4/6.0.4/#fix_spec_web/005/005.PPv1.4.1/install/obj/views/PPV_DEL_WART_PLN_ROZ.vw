create or replace view PPADM.PPV_DEL_WART_PLN_ROZ as
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
select
    -- $Revision:: 1         $--
    kal_wnd_id as wnd_id,
    sum(kwota_pln) kwota_pln_roz
from PPV_DELZ_DKZ_WART_PLN
where 1=1
    and (kal_rodzaj='ROZ' or dok_f_anulowany is not null)
	and EXISTS (
              SELECT 
                1
              FROM PPT_DEL_KALKULACJE kal
              join PPT_DEL_POZYCJE_KALKULACJI pkl on kal.kal_id = pkl.PKL_KAL_ID
              WHERE 
                kal.KAL_ID = PPV_DELZ_DKZ_WART_PLN.kal_id
                and kal.kal_rodzaj='ROZ'
    )
group by kal_wnd_id
/


