create or replace function ppadm.st_wniosek_zm_os_odp(P_PS_ID NUMBER, P_PRC_ID_NA NUMBER, P_DATA DATE default sysdate, P_OPIS VARCHAR2 default null) return varchar2 as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--    exec ppadm.ppp_generuj_trigger_tbts('EGADM1', 'STT_WNIOSKI');
--    exec ppadm.ppp_generuj_trigger_tbts('EGADM1', 'STT_WNIOSKI_PARAMETRY');
--    exec ppadm.ppp_generuj_trigger_tbts('EGADM1', 'STT_INWENTARYZACJE');
--    grant select, insert, update, delete on egadm1.stt_inwentaryzacje to PPADM;
--    grant select, insert, update, delete on egadm1.stt_wnioski to PPADM;
--    grant select, insert, update, delete on egadm1.stt_wnioski_parametry to PPADM;
--    wartosc domyslna ST / 'RODZAJ_DOK_WNIOSKI', ... 'ZM_OS_ODP' ... --> 'MT' ?!
---------------------------------------------------------------------------------------------------------------------
-- funkcja towrzy wniosek o przesuniecie srodka trwalego (podzialu p_ps_id) na innego pracownika
-- wg podanych parametrów i zwraca null;
-- w przypadku bledu zwraca opis bledu
---------------------------------------------------------------------------------------------------------------------
    v_inw_id NUMBER;--id inwentaryzacji
    v_wn_id NUMBER;
    v_wn_numer  stt_wnioski.wn_numer%TYPE; -- varchar2
    v_data DATE;
    v_msg varchar2(4000);

BEGIN

   for r0 in (  select *
                from stt_wnioski
                     join stt_srodki_dane on wn_sdn_id = sdn_id
                     join stt_podzialy_srodka ps on ps.ps_sdn_id = sdn_id and ps_id = P_PS_ID --czy jest podzial
                where wn_f_anulowany != 'T' and wn_f_dok_wygenerowano != 'T'
   ) loop
        return 'Dla środka z numerem '''|| r0.SDN_NUMER_INW ||''' istnieje inny niezatwierdzony, nieprzeprocesowany do końca wniosek z numerem ''' || r0.wn_numer || '''';
   end loop;




    v_inw_id := st_inwentaryzacja_wstaw(P_DATA);

    for r in (
        ---------------------------------------------------------------------------------
        -- warunki brzegowe: jest inw, sdn, oraz podzial gdzie ps_prc_id=p_prc_id_z
        ---------------------------------------------------------------------------------
        select  i.*, sdn.*, ps.*, u.*
        from ppv_zalogowany u
            join ek_pracownicy prc_na on prc_na.prc_id = P_PRC_ID_NA-- czy jest prc docelowy
            join stt_inwentaryzacje i on i_id = v_inw_id --czy jest inwentaryzacja
            join stt_podzialy_srodka ps on ps_id = P_PS_ID --czy jest podzial
            join EGADM1.stt_srodki_dane sdn on sdn_id = ps_sdn_id
        ---------------------------------------------------------------------------------
    ) loop

        v_wn_numer := r.i_numer||'.1'; -- st_wniosek_numer(r.sdn_id); --'PP.WN/'||sysdate;
        v_data := r.i_data; --sysdate;

        insert into stt_wnioski (wn_i_id,   wn_sdn_id,  wn_sdn_s_id,  wn_numer,     wn_data,    wn_prc_id,   wn_uzt_nazwa  )
        values                  (v_inw_id,  r.sdn_id,   r.sdn_s_id,   v_wn_numer,   v_data,     r.prc_id,    r.uzt_nazwa   )
        returning wn_id into v_wn_id;

        insert into EGADM1.stt_wnioski_parametry
                (wnp_wn_id,     wnp_kod,        wnp_wartosc_z,   wnp_wartosc_na,     wnp_ps_id   )
        values  (v_wn_id,       'ZM_OS_ODP',    r.ps_prc_id,     P_PRC_ID_NA,        r.ps_id     );

        update stt_inwentaryzacje
        set i_data = v_data, i_odz_id = NVL(r.SDN_ODZ_ID, EAADM.eap_globals.odczytaj_oddzial)
        where i_id = v_inw_id; --data inw z kazdym kolejnym wnioskiem kroczy do przodu?



        v_msg := 'Środki trwałe / lista wniosków: wygenerowano wniosek (o zmiane os. odp.) ' || v_wn_numer;
        ppadm.st_wniosek_wiadomosci(v_wn_id, v_msg);

        return '000000'||v_wn_id; --brak bledow / wniosek dodany

    end loop;

    --brak wg parametrow: inwentaryzacji, sdn, podzialu, albo ktoregos pracownika
     --RAISE_APPLICATION_ERROR (-20000,'ppadm.st_wniosek_przesun_os_odp( P_INW_ID:' || P_INW_ID ||'; P_PS_ID:'|| P_PS_ID ||'; P_PRC_ID_NA:'|| P_PRC_ID_NA || ') -> brak danych!!' );
     return 'ppadm.st_wniosek_zm_os_odp(P_PS_ID:'|| P_PS_ID ||'; P_PRC_ID_NA:'|| P_PRC_ID_NA || ') -> brak danych!!';
END;
/
