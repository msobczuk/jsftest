CREATE OR REPLACE VIEW PPADM.PPV_HISTORIA_OBIEGOW_LAST
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
select 
  hio.* 
from PPV_HISTORIA_OBIEGOW hio
	    join ( 
              select -- jesli czynnosc powtarzano (bo np. byly korekty) - tylko ta ostatnio wykonana
                max(hio_id) max_hio_id --, hio_kob_kod, hio_klucz_obcy_id, HIO_COB_ID 
              from CSST_HISTORIA_OBIEGOW 
              group by  hio_kob_kod, hio_klucz_obcy_id, HIO_COB_ID
            ) lasthio
          on lasthio.max_hio_id = hio.hio_id
order by HIO_OBG_ID, HIO_KLUCZ_OBCY_ID, HIO_ID DESC
/