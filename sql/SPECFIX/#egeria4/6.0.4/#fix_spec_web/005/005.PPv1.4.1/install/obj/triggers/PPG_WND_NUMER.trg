create or replace TRIGGER PPADM.PPG_WND_NUMER
  before INSERT OR UPDATE ON PPADM.PPT_DEL_WNIOSKI_DELEGACJI FOR EACH ROW
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                           $
-- $Revision::                                                                                                                                                         $
-- $Workfile::                                                                                                                                                         $
-- $Modtime::                                                                                                                                                          $
-- $Author::                                                                                                                                                           $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE
BEGIN
    IF (:NEW.WND_NUMER IS NULL) THEN
     --:new.wnd_numer := 'x'||sysdate;
     :new.wnd_numer := PPADM.NUMER_OBLICZ( p_tablica => 'PPT_DEL_WNIOSKI_DELEGACJI', p_wznu_rodzaj => :NEW.WND_RODZAJ, p_p1 => :NEW.WND_PRC_ID); 
    --PPADM.PLOG('nowy numer: ' || PPADM.NUMER_OBLICZ( p_tablica => 'PPT_DEL_WNIOSKI_DELEGACJI', p_wznu_rodzaj => :NEW.WND_RODZAJ, p_p1 => :NEW.WND_PRC_ID));
    END IF;
END;
/

ALTER TRIGGER PPG_WND_BIUR_NR DISABLE;
/
