CREATE OR REPLACE VIEW PPADM.PPV_DELZ_DKZ_WART_PLN AS 
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
select 
-- $Revision:: 1         $--
 dkz.* , 
 kal.*,
 DOK.DOK_F_ANULOWANY,
 dok.dok_f_zatwierdzony,
 dok.dok_dok_id_zap,

 case 
 when dkz.dkz_wal_id = 1      then   nvl(dok.DOK_KWOTA * sign(dkz.dkz_kwota), dkz.dkz_kwota) --PLN
 when nvl(dkz.dkz_kwota, 0)<0 then -(nvl(dok.DOK_KWOTA, -round(dkz.dkz_kwota * (dkz.dkz_kurs/wal.wal_jednostka_kursu), 2)))
 when nvl(dkz.dkz_kwota, 0)>0 then  (nvl(dok.DOK_KWOTA,  round(dkz.dkz_kwota * decode (dkz.dkz_czy_rownowartosc, 'T', nbp.KW_PRZELICZNIK_SREDNI, nbp.KW_PRZELICZNIK_SPRZEDAZY),2)))
 else 0
 end as kwota_pln


 , nvl( dok.dok_kurs,
    case
    when dkz.dkz_wal_id = 1      then 1 --PLN
    when nvl(dkz.dkz_kwota, 0)<0 then dkz.dkz_kurs
    when nvl(dkz.dkz_kwota, 0)>0 then decode (dkz.dkz_czy_rownowartosc, 'T', nbp.KW_PRZELICZNIK_SREDNI_W1, nbp.KW_PRZELICZNIK_SPRZEDAZY_W1)
    else 0
    end
 ) kurs


from PPT_DEL_ZALICZKI dkz
  join PPT_DEL_KALKULACJE kal on dkz.dkz_kal_id=kal.kal_id
  join css_waluty wal on dkz.dkz_wal_id=wal.wal_id
  left join EGADM1.KGT_DOKUMENTY dok on dok.dok_id = dkz.dkz_dok_id
  left join PPV_KURSY_NBP  nbp
      on dkz.dkz_wal_id = nbp.wal_id and (kal.kal_data_kursu >= kw_data and kal.kal_data_kursu < nvl(kw_data_do, kal.kal_data_kursu+1))
where kal_rodzaj <> 'WNK'
  and nvl(dok.dok_f_anulowany,'N') <> 'T'
  --where kal.kal_wnd_id = :P_WND_ID
/
