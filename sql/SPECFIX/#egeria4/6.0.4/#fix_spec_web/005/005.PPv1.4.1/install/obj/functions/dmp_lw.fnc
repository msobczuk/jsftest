create or replace function ppadm.dmp_lw(p_lw_nazwa varchar2) return clob
as
    /*

    funkcja realizuje  zrzut definicji wybranej listy wartosci
    rezultatem jest skrypt dml, ktory w docelowej bazie kasuje i wgrywa na nowo definicje LW wskazana przez parametr p_lw_nazwa
    przykad wywolania:

    begin
        dbms_output.put_line(dmp_lw('PPL_PODWOJNE_ZATRUDNIENIE'));
    end;

    */

    --p_lw_nazwa varchar2(255) := 'PPL_PODWOJNE_ZATRUDNIENIE';
    v_clob clob := '';
begin

    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    v_clob := v_clob ||'-- ' || sys_context('userenv','db_name') ||' / '|| USER ||' / '|| TO_CHAR(SYSDATE,' YYYY.MM.DD HH24:MI:SS') || ' --> '|| CHR(13)||CHR(10);
    v_clob := v_clob ||'-- DMP_LW / P_LW_NAZWA: '|| p_lw_nazwa || CHR(13)||CHR(10);
    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    v_clob := v_clob || CHR(13)||CHR(10);
    v_clob := v_clob || 'DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = '''||p_lw_nazwa||''' );' || CHR(13)||CHR(10);
    v_clob := v_clob || 'DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = '''||p_lw_nazwa||''';' || CHR(13)||CHR(10);
    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    v_clob := v_clob || CHR(13)||CHR(10);
    v_clob := v_clob || ppadm.gen_dml_table_inserts_clob('PPADM', 'PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('''||p_lw_nazwa||''')', 'LST_NAZWA, LST_LP');
    v_clob := v_clob || ppadm.gen_dml_table_inserts_clob('PPADM', 'PPT_ADM_LISTY_TABELE', 'LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( '''||p_lw_nazwa||''' ) ) ');

    --dbms_output.put_line(v_clob);
    return v_clob;
end;
/
