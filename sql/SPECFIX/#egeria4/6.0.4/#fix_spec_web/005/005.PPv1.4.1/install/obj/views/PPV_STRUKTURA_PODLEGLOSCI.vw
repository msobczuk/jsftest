CREATE OR REPLACE VIEW "PPADM"."PPV_STRUKTURA_PODLEGLOSCI" as
select
   -- $Revision:: 1       $--
    stx_stjo_id spod_stjo_id,
    stx_stjo_id_nad spod_stjo_id_nad,
    stoprc.sto_id spod_sto_id_prc,
    stoprz.sto_id spod_sto_id_prz,
    stoprc.sto_prc_id spod_prc_id,
    stoprz.sto_prc_id spod_prc_id_prz,
    stoprz.sto_f_zastepstwo spod_f_zastepstwo_prc,
    stoprz.sto_f_zastepstwo spod_f_zastepstwo_prz
from dual
    join ZPT_STANOWISKA_NAD stx on ppp_global.odczytaj_date_dla_struktury_po between stx_data_od and nvl(stx_data_do,ppp_global.odczytaj_date_dla_struktury_po)
    join ZPT_STANOWISKA_OPIS stoprc on stoprc.STO_STJO_ID = stx.STX_STJO_ID     and ppp_global.odczytaj_date_dla_struktury_po between stoprc.sto_data_od and nvl(stoprc.sto_data_do, ppp_global.odczytaj_date_dla_struktury_po)
    join ZPT_STANOWISKA_OPIS stoprz on stoprz.STO_STJO_ID = stx.STX_STJO_ID_NAD and ppp_global.odczytaj_date_dla_struktury_po between stoprz.sto_data_od and nvl(stoprz.sto_data_do, ppp_global.odczytaj_date_dla_struktury_po)
/