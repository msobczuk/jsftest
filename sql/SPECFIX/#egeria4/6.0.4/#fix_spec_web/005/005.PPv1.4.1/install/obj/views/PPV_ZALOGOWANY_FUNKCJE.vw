CREATE OR REPLACE VIEW PPADM.PPV_ZALOGOWANY_FUNKCJE as
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------  
SELECT
     fu.FU_ID
    ,fu.FU_NAZWA
    ,fu.FU_OPIS
FROM ppv_zalogowany u
    join eat_uprawnienia upr  on u.uzt_nazwa = UPR_UZT_NAZWA
    join eat_funkcje_uzytkowe fu on fu.FU_ID = upr_fu_id
    join eat_aplikacje apl on apl.APL_ID = fu.FU_APL_ID and APL.APL_SYMBOL = 'PP'
/









