create or replace FUNCTION  "PPADM"."GET_PRC_DEPARTAMENT_ID" (p_PRC_ID NUMBER, p_na_dzien DATE DEFAULT SYSDATE) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
BEGIN

  for r in (
      select zat.zat_ob_id 
      from EGADM1.EK_ZATRUDNIENIE zat
      where zat.ZAT_PRC_ID = p_prc_id 
          AND trunc(p_na_dzien) BETWEEN zat_data_zmiany AND nvl(zat_data_do, trunc(SYSDATE+(365*1000)))
      order by zat.ZAT_TYP_UMOWY    
  ) loop
      return GET_DEPARTAMENT_ID(r.zat_ob_id);
  end loop;
  
  return null;
  
END;
/

