create or replace PROCEDURE       PPADM.MSG_AUTONOMOUS_TRANSACTION(
    P_USER VARCHAR2 DEFAULT '',
    P_TEMAT VARCHAR2 DEFAULT '',
    P_TEKST VARCHAR2 DEFAULT '',
    P_WDM_ID NUMBER DEFAULT '',
    P_SEVERITY VARCHAR2 DEFAULT 'INFO')
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- PROCEDURA DO WYSY�ANIA KOMUNIKAT�W (DYMK�W) NA EKRAN ZALOGOWANEGO U�YTKOWNIKA W PORTALU; DO ROZSY�ANIA WYKORZYSTUJE PPP_MSG ORAZ PAKIET DBMS_ALERT
-- WERSJA Z PRAGMA AUTONOMOUS_TRANSACTION DO STOSOWANIA W SYTUACJI JESLI CHCEMY WYSLAC POWIADOMIENIE NIEZALEZNIE OD TEGO CZY TRANSAKCJA ZOSTANIE ZATWIERDZONA
-- CZY WYCOFANA (NP. KOMUNIKATY W D�UGOTRWA�YCH PROCES�W LUB Z AKCJI ZANIM ZOSTANIE WYWOA�NY EW. WYJ�TEK W CELU WYCOFANIA TRANSAKCJI)
-- parametry:
-- P_USER  - nazwa usera z EAT_UZYTKOWNICY albo identyfikator sesji klieta PP (maks. 32 bajty / kolumna SZW_ID_KLIENTA_SESJI w tab. EAADM.EAT_SESJE_ZEWNETRZNE)
--           (opcjonalne jesli podajmey WDM_ID); UWAGA: je�li p_user = '%', to b�dzie broadcast do aktualnie zalogowanych w PP
-- P_TEMAT - opcjonalnie jesli podajmey WDM_ID; jesli nie podano to temat zostanie pobrany z EAT_WIADOMOSCI
-- P_TEKST - jesli nie podano to tresc zostanie pobrany z EAT_WIADOMOSCI ... powinno to byc maks. 1800 bajt�w (razem z tematem i severity, inaczej obcinamy)
-- P_WDM_ID jeden z (select wdm_id from eat_wiadomosci)
--          Jesli pp_wdm_id podany, to na dymku dodamy przycisk z linkiem do calej wiadomosci w eat_wiadomosci...
--             przy czym je�li nie podano tematu to na podstawie p_wdm_id zostanie (przez kod JAVA) pobrany z eat_wiadomosci,
--             a jesli nie podano p_tekst, analogicznie zostanie (przez kod JAVA) pobrana poczatkowa tresc wiadomosci (np. pierwsze 300 znak�w)
-- P_SEVERITY: jeden z 'INFO', 'WARN', 'ERROR', 'LOGOUT' (kazdy inny i tak zostanie potraktowany w PP jak 'INFO'; UWAGA: 'LOGOUT' wyloguje sesj� PP)
--
-- Jesli pp_wdm_id nie podany oraz jednocze�nie nie podano takze P_TEMAT ani P_TEKST to procedura nic nie zrobi.
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS
  -----------------------------------------------------------
  PRAGMA AUTONOMOUS_TRANSACTION;
  -----------------------------------------------------------

  v_user  varchar2(32) := P_USER;
  v_temat varchar2(255) := P_TEMAT;
  v_tekst varchar2(4000):= P_TEKST;
  v_data varchar2(32767):= '';
  v_cnt_sesje NUMBER(10) := 0;

BEGIN

  IF ( P_TEMAT IS NULL    AND   P_TEKST IS NULL   AND   P_WDM_ID IS NULL ) THEN
    return;
  END IF;

  v_temat := substr(v_temat, 1, 255);  --v_temat := convert(v_temat, 'utf8', 'EE8ISO8859P2' ); --text, dest charset, source charset
  v_tekst := substr(v_tekst, 1, 3600);  --v_tekst := REPLACE(v_tekst,CHR(10),'</br>'); to samo robi kod JAVA
  --v_tekst := convert(v_tekst, 'utf8', 'EE8ISO8859P2' ); --text, dest charset, source charset

  IF ( NVL(v_user,'Z') <> '%' ) THEN
    select count(*) into v_cnt_sesje from EAADM.EAT_SESJE_ZEWNETRZNE where szw_uzt_nazwa = v_user or SZW_ID_KLIENTA_SESJI = v_user;
    IF ( v_cnt_sesje = 0 ) THEN
      dbms_output.put_line('.....');
      return; -- P_USER nie jest zalogowany w PP
    END IF;
  END IF;

  v_data :=  P_SEVERITY   || chr(10)
          || p_wdm_id     || chr(10)
          || v_user       || chr(10)
          || v_temat      || chr(10)
          || v_tekst;

  PPADM.PPP_MSG.SEND(v_data);

  -----------------------------------------------------------
  COMMIT; --to autonomiczna wersja procedury MSG_ON_COMMIT
  -----------------------------------------------------------
END;
