create or replace function ppadm.dmp_sl(p_sl_nazwa varchar2) return clob
as
    /*

    funkcja realizuje  zrzut definicji wybranego slownika
    rezultatem jest skrypt dml, ktory w docelowej bazie kasuje i wgrywa na nowo definicje slownika (parametr p_sl_nazwa)
    przykad wywolania:

    begin
        dbms_output.put_line(dmp_sl('SL_TESTOWY'));
    end;

    */

    --p_lw_nazwa varchar2(255) := 'PPL_PODWOJNE_ZATRUDNIENIE';
    v_clob clob := '';
begin

    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    v_clob := v_clob ||'-- ' || sys_context('userenv','db_name') ||' / '|| USER ||' / '|| TO_CHAR(SYSDATE,' YYYY.MM.DD HH24:MI:SS') || ' --> '|| CHR(13)||CHR(10);
    v_clob := v_clob ||'-- DMP_SL / P_SL_NAZWA: '|| p_sl_nazwa || CHR(13)||CHR(10);
    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    v_clob := v_clob || CHR(13)||CHR(10);
--    v_clob := v_clob || 'DELETE FROM PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW WHERE WSL_SL_NAZWA in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = '''||p_lw_nazwa||''' );' || CHR(13)||CHR(10);
    v_clob := v_clob || 'DELETE FROM PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW WHERE WSL_SL_NAZWA = '''||p_sl_nazwa||''';' || CHR(13)||CHR(10);
    v_clob := v_clob || 'DELETE FROM PPADM.PPT_ADM_SLOWNIKI WHERE SL_NAZWA = '''||p_sl_nazwa||''';' || CHR(13)||CHR(10);
    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    v_clob := v_clob || CHR(13)||CHR(10);
    v_clob := v_clob || ppadm.gen_dml_table_inserts_clob('PPADM', 'PPT_ADM_SLOWNIKI','SL_NAZWA in ('''||p_sl_nazwa||''')', 'SL_NAZWA');
    v_clob := v_clob || ppadm.gen_dml_table_inserts_clob('PPADM', 'PPT_ADM_WARTOSCI_SLOWNIKOW','WSL_SL_NAZWA in ('''||p_sl_nazwa||''')', 'WSL_SL_NAZWA, WSL_LP');

    --dbms_output.put_line(v_clob);
    return v_clob;
end;
/
