create or replace PACKAGE BODY PPADM.PPP_DDL
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
--------------------------------------------------------------------------------------------------------------------------------------------------------------- 
as 
    -----------------------------------------------------------------------------------------------------------------
    -- tabele, ktore trzeba dodac (lub zmodyfikowac im komentarz)
    -----------------------------------------------------------------------------------------------------------------
    cursor g_cu_tables (p_tb_id number default null) is
        select distinct tb_owner, tb_table_name, tb_column_prefix
          from ppadm.ppt_tables
          left join sys.all_tables on tb_owner = owner and tb_table_name = table_name
         where tb_table_name is not null and table_name is null -- tylko faktycznie brakujace tabele   
           and tb_status = 'Z'
           and (p_tb_id is null or tb_id = p_tb_id);
    --
    g_v_tables g_cu_tables%rowtype;
    -----------------------------------------------------------------------------------------------------------------
    -- kolumny, ktore trzeba dodac lub zmodyfikowac (np. zwiekszyc precyzje typu, albo dodac indeks lub fk)
    -----------------------------------------------------------------------------------------------------------------
    cursor g_cu_columns (p_tb_id number default null, p_tc_id number default null) is
        select tc.*,
               ac.*, 
               cc.comments,
               tb.tb_owner,
               tb.tb_table_name,
               case when ac.column_name is null then 'T' else 'N' end nowa
          from ppadm.ppt_tab_cols tc 
          join ppadm.ppt_tables tb on tb.tb_id = tc.tc_tb_id
          left join (
                    select data_type || case
                             when data_precision is not null and nvl(data_scale, 0) > 0 then '(' || data_precision || ',' || data_scale || ')'
                             when data_precision is not null and nvl(data_scale, 0) = 0 then '(' || data_precision || ')'
                             when data_precision is null and data_scale is not null then '(*,' || data_scale || ')'
                             when char_length > 0 then '(' || char_length || 
                                case char_used 
                                    when 'B' then '' --' Byte'
                                    when 'C' then ' Char'
                                    else null 
                                end || ')'
                             end || decode(nullable, 'N', ' NOT NULL') ac_datatype,
                           ac0.*
                      from sys.all_tab_columns ac0            
               ) ac on tb.tb_owner = ac.owner and tb.tb_table_name = ac.table_name and tc.tc_column_name = ac.column_name
          left join sys.all_col_comments cc on ac.owner = cc.owner and ac.table_name = cc.table_name and ac.column_name = cc.column_name
--          left join sys.all_indexes ind on nvl(tc.tc_uk_name, tc.tc_ix_name) = ind.index_name
          left join sys.all_indexes ix on tc.tc_ix_name = ix.index_name
          left join sys.all_indexes uk on tc.tc_uk_name = uk.index_name
          left join sys.all_constraints con on con.constraint_name = 'PPC_' || replace(tc.tc_column_name, 'ID', 'FK')
         where 1=1
           and tc.tc_column_name not like 'SYS\_%' escape '\'
           and (ac.column_name is null
                or upper(ac.ac_datatype) <> upper(tc.tc_data_type) || decode(tc.tc_nullable, 'N', ' NOT NULL', '')
                or nvl(cc.comments, '-') <> nvl(tc.tc_desc, '-')
--                or (ind.index_name is null and nvl(tc.tc_uk_name, tc.tc_ix_name) is not null)
                or (ix.index_name is null and tc.tc_ix_name is not null) --brak indeksu
                or (uk.index_name is null and tc.tc_uk_name is not null) --brak indeksu wg uk
                or (con.constraint_name is null and tc.tc_fk_column_name is not null))
           and tc.tc_status = 'Z'
           and (p_tb_id is null or tc.tc_tb_id = p_tb_id)
           and (p_tc_id is null or tc.tc_id = p_tc_id)
         order by tb.tb_owner, tb.tb_table_name, tc.tc_id;
    --
    g_v_columns g_cu_columns%rowtype;
    -----------------------------------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------
    
    -----------------------------------------------------------------------------------------------------------------
    -- porcedury pomocniczne do wpisywania danych do tabeli eaadm.eat_dziennik_instalacji
    -----------------------------------------------------------------------------------------------------------------
    
    procedure wstaw_do_dziennik_instalacji(
        p_opis	VARCHAR2
    )
    as 
    begin
        UPDATE ppadm.PPT_DZIENNIK_INSTALACJI
          set INSP_TEXT = INSP_TEXT || ' ' || p_opis
          where INSP_SESSIONID = sys_context('USERENV','SESSIONID')        
            and INSP_F_ZAPISANE = 'N';
        if sql%rowcount = 0 then
          insert into ppadm.PPT_DZIENNIK_INSTALACJI (INSP_ID, INSP_SESSIONID, INSP_TEXT) VALUES(PPS_INSP.nextval, sys_context('USERENV','SESSIONID'), p_opis);
        end if;
        commit;	
    END;
    
    procedure zapisz_do_dziennika_instalacji(
        p_wersja VARCHAR2,
        p_instalator VARCHAR2
    )
    as
        v_instalacja eaadm.eap_obj_instalacja.t_instalacja;
        v_ins_id number(10);
        v_apl_id number(10);
        v_rowid rowid;
    begin
    
        SELECT 
              apl_id, 
              APL_INS_ID 
              into v_apl_id, v_ins_id
        FROM EAT_APLIKACJE 
        WHERE APL_SYMBOL = 'PP'
        ;
      
        ppadm.plog('apl_id: ' || v_ins_id || ' APL_INS_ID: ' || v_apl_id );
        
        for r in (
            SELECT
              *
            FROM ppadm.PPT_DZIENNIK_INSTALACJI insp
            Where insp.INSP_SESSIONID = sys_context('USERENV','SESSIONID')
            and insp.INSP_F_ZAPISANE = 'N'
        )loop
          
          v_instalacja.pdi_id := null; 
          v_instalacja.pdi_data_instalacji := sysdate;
          v_instalacja.pdi_wersja := p_wersja;
          v_instalacja.pdi_instalator := p_instalator; 
          v_instalacja.pdi_ins_id := v_ins_id;
          v_instalacja.pdi_apl_id := v_apl_id;
          v_instalacja.pdi_opis := 'Zbiorczy wpis o instalacji PP, szczegóły w tabeli ppadm.PPT_DZIENNIK_INSTALACJI(' ||r.insp_id||')';
          
          v_rowid := EAADM.EAP_OBJ_INSTALACJA.WSTAW(v_instalacja);
        UPDATE ppadm.PPT_DZIENNIK_INSTALACJI
        set INSP_PDI_ID = (select pdi_id from eaadm.eat_dziennik_instalacji where rowid = v_rowid),
            INSP_F_ZAPISANE = 'T'
        where INSP_SESSIONID = sys_context('USERENV','SESSIONID')	
        and   INSP_F_ZAPISANE = 'N';
      end loop;
      
      --commit;  
    end zapisz_do_dziennika_instalacji;



    procedure komentarz_tb(p_owner varchar2, p_tabela varchar2, p_komentarz varchar2) as
    begin 
        --execute immediate 'comment on table ' || p_owner ||'.'|| p_tabela || ' is '':p1''' using  p_komentarz; ORA-06512: ...  "DDL statement is executed in an illegal context" ...
        execute immediate 'comment on table ' || p_owner||'.'|| p_tabela || ' is '''|| replace(p_komentarz,'''','''''') || '''';
    END;

    procedure komentarz_kol(p_owner varchar2, p_tabela varchar2, p_kolumna varchar2, p_komentarz varchar2) as
    begin 
        execute immediate 'comment on column ' || p_owner||'.'||p_tabela ||'.'||p_kolumna || ' is '''|| replace(p_komentarz,'''','''''') || '''';
    END;
    
    procedure ddl_table(
        p_nazwa IN VARCHAR2,
        p_column_prefix IN VARCHAR2,
        p_kolumna_typ IN VARCHAR2 DEFAULT 'NUMBER(10)',
        p_komentarz IN VARCHAR2 DEFAULT '-',
        p_schemat IN VARCHAR2 DEFAULT 'PPADM',
        p_aplikacja IN VARCHAR2 DEFAULT 'PP',
        p_przestrzen_tabeli IN VARCHAR2 DEFAULT 'PP_D',
        p_przestrzen_fk IN VARCHAR2 DEFAULT 'PP_X',
        p_using_index_fk IN VARCHAR2 DEFAULT 'T')
    AS
    begin
          eaadm.eap_ddl.stworz_tabele( 
            p_nazwa =>  p_nazwa, 
            p_kolumna_nazwa => p_column_prefix ||'_ID',
            p_kolumna_typ => p_kolumna_typ,
            p_komentarz => p_komentarz,
            p_aplikacja => p_aplikacja,
            p_przestrzen => p_przestrzen_tabeli,
            p_schemat => p_schemat
        );

        eaadm.eap_ddl.dodaj_klucz_glowny (
            p_nazwa_tabeli => p_nazwa, 
            p_schemat => p_schemat,
            p_nazwy_kolumn => p_column_prefix ||'_ID', 
            p_nazwa_klucza => 'PPC_' || p_column_prefix ||'_PK', 
            p_przestrzen => p_przestrzen_fk,
            p_using_index => p_using_index_fk
        );

        --eap_audyt.tworz_kolumny_audytowe(p_nazwa);
        --eap_audyt.utworz_wyzwalacz_audytowy(p_schemat, p_nazwa, p_column_prefix, p_aplikacja, TRUE);
		PPP_AUDYT.UTWORZ_WYZWALACZ_AUDYTOWY(p_schemat, p_nazwa);
        ppp_generuj_trigger_tbts(p_schemat, p_nazwa);
		
        wstaw_do_dziennik_instalacji('Dodanie tabeli ' || p_schemat ||'.'|| p_nazwa ||CHR(13)||CHR(10));
    end ddl_table;
    
    procedure ddl_table(
        p_tb_id number) 
    as
    begin 
        for r in g_cu_tables(p_tb_id) loop
             ddl_table(
                p_nazwa => r.tb_table_name, 
                p_column_prefix => r.tb_column_prefix,
                p_kolumna_typ => 'NUMBER(10)',
                p_komentarz => '-',
                p_schemat => r.tb_owner,
                p_aplikacja => 'PP',
                p_przestrzen_tabeli => 'PP_D',
                p_przestrzen_fk => 'PP_X',
                p_using_index_fk => 'T'
            );
        end loop;
    end ddl_table;
    
    
    
    procedure ddl_column(
        p_nazwa_tabeli IN VARCHAR2,
        p_nazwa IN VARCHAR2,
        p_schemat IN VARCHAR2,
        p_typ IN VARCHAR2,
        p_komentarz IN VARCHAR2 DEFAULT NULL,
        p_nullable IN VARCHAR2 DEFAULT 'T',
        p_default IN VARCHAR2 DEFAULT NULL,
        p_nowa IN VARCHAR2 DEFAULT 'T',
        p_tc_fk_column_name IN VARCHAR2 DEFAULT NULL,
        p_tc_ix_name IN VARCHAR2 DEFAULT NULL,
        p_tc_uk_name IN VARCHAR2 DEFAULT NULL)
    as
        v_owner          VARCHAR2(30);
        v_table_name     VARCHAR2(30);
        v_fk_column_name VARCHAR2(30);
    begin
    
        if (p_nowa = 'T') then
            eaadm.eap_ddl.dodaj_kolumne(p_nazwa_tabeli, p_nazwa, p_schemat, p_typ, p_komentarz, p_nullable, p_default);
        else
            eaadm.eap_ddl.edytuj_kolumne(p_nazwa_tabeli, p_nazwa, p_schemat, p_typ, p_komentarz, p_nullable, p_default);
        end if;
  
        if (p_tc_fk_column_name is not null) then
            v_owner          := substr(p_tc_fk_column_name, 1, instr(p_tc_fk_column_name, '.', 1, 1) - 1);
            v_table_name     := substr(p_tc_fk_column_name, instr(p_tc_fk_column_name, '.', 1, 1) + 1, 
                                    instr(p_tc_fk_column_name, '.', 1, 2) - instr(p_tc_fk_column_name, '.', 1, 1) - 1);
            v_fk_column_name := substr(p_tc_fk_column_name, instr(p_tc_fk_column_name, '.', 1, 2) + 1);
            
            eaadm.eap_ddl.dodaj_klucz_obcy(
                p_nazwa_tabeli_pierwszej => p_nazwa_tabeli,
                p_nazwy_kolumn_pierwszej => p_nazwa,
                p_nazwa_tabeli_drugiej => v_table_name,
                p_nazwy_kolumn_drugiej => v_fk_column_name,
                p_schemat => p_schemat,
                p_schemat_drugi => v_owner,
                p_nazwa_klucza => 'PPC_' || replace(p_nazwa, 'ID' ,'FK')
            );
        end if;
              
        if (p_tc_ix_name is not null) then
          eaadm.eap_ddl.stworz_indeks( 
                      p_nazwa_tabeli => p_nazwa_tabeli,
                      p_schemat => p_schemat,
                      p_kolumny_tabeli => p_nazwa,
                      p_nazwa_indeksu => p_tc_ix_name
           );
        end if;
              
        if (p_tc_uk_name is not null) then
          eaadm.eap_ddl.stworz_indeks( 
                      p_nazwa_tabeli => p_nazwa_tabeli,
                      p_schemat => p_schemat,
                      p_kolumny_tabeli => p_nazwa,
                      p_nazwa_indeksu => p_tc_uk_name,
                      p_unikalny => 'T'
           );
        end if;
    
        wstaw_do_dziennik_instalacji('    Dodanie kolumny ' || p_nazwa || ' typu ' || p_typ || ' do tabeli ' || p_schemat || '.' || p_nazwa_tabeli ||CHR(13)||CHR(10));
    end ddl_column;
    
    procedure ddl_column(
        p_tc_id number)
    as
    begin 
        for r in g_cu_columns(p_tc_id => p_tc_id) loop
            ddl_column(
                p_nazwa_tabeli => r.TB_TABLE_NAME,
                p_nazwa => r.tc_column_name,
                p_schemat => r.TB_OWNER,
                p_typ => r.tc_data_type,
                p_komentarz => r.TC_DESC,
                p_nullable => r.TC_NULLABLE,
                p_default => r.TC_DEFAULT,
                p_nowa => r.nowa,
                p_tc_fk_column_name => r.TC_FK_COLUMN_NAME,
                p_tc_ix_name => r.TC_IX_NAME,
                p_tc_uk_name => r.TC_UK_NAME
            ); 
        end loop;
    end ddl_column;
    
    procedure ddl_columns(
        p_tb_id number)
    as
    begin 
        for r in g_cu_columns(p_tb_id => p_tb_id) loop
            ddl_column(
                p_nazwa_tabeli => r.TB_TABLE_NAME,
                p_nazwa => r.tc_column_name,
                p_schemat => r.TB_OWNER,
                p_typ => r.tc_data_type,
                p_komentarz => r.TC_DESC,
                p_nullable => r.TC_NULLABLE,
                p_default => r.TC_DEFAULT,
                p_nowa => r.nowa,
                p_tc_fk_column_name => r.TC_FK_COLUMN_NAME,
                p_tc_ix_name => r.TC_IX_NAME,
                p_tc_uk_name => r.TC_UK_NAME
            ); 
        end loop;
    end ddl_columns;
    
    procedure ddl_table_and_columns(
        p_tb_id number)
    as
    begin 
        ddl_table(p_tb_id);
        ddl_columns(p_tb_id);
    end ddl_table_and_columns;
    
    procedure ddl_all_tables_and_columns
    as
    begin 
        ddl_tables;
        ddl_columns;
    end ddl_all_tables_and_columns;
    
    procedure ddl_tables as
        v_opis varchar2(4000) := 'Dodanie tabel: ';
    begin 
    
        for r in g_cu_tables loop
             ddl_table(
                p_nazwa => r.tb_table_name, 
                p_column_prefix => r.tb_column_prefix,
                p_kolumna_typ => 'NUMBER(10)',
                p_komentarz => '-',
                p_schemat => r.tb_owner,
                p_aplikacja => 'PP',
                p_przestrzen_tabeli => 'PP_D',
                p_przestrzen_fk => 'PP_X',
                p_using_index_fk => 'T'
            );

        end loop;

    
        --zakladam, ze komentarze mozna zmieniac w ppt_tbales pozniej (chce je aktualizowac takze po utworzeniu tabel)
        for r in (
            -----------------------------------------------------------------------------------------------------------------
            -- tabele, ktorym trzeba zmodyfikowac komentarz
            -----------------------------------------------------------------------------------------------------------------
                select distinct
                    tb_owner, tb_table_name, TB_DESC
                from ppadm.ppt_tables
                  join sys.all_tables at on tb_owner = at.owner and tb_table_name = table_name --tb musi istniec
                  left join sys.all_tab_comments atc on tb_owner = atc.owner and tb_table_name = atc.table_name
                where 1=1
                  and TB_STATUS = 'Z' --tylko zatwierdzone zmiany
                  and tb_owner = 'PPADM' -- aktualizuj tylko komentarze tb. PPADM
                  and nvl(TB_DESC,'-') <> nvl(comments,'-') -- aktualizuj tylko nowe i zmiany
            -----------------------------------------------------------------------------------------------------------------             
        ) loop
            komentarz_tb(r.tb_owner, r.tb_table_name, r.TB_DESC);
        end loop;
    
    end ddl_tables;
    
    
    
    
    
    procedure ddl_columns as
        v_opis varchar2(4000) := 'Dodanie kolumn: ';
    begin 
        for r in g_cu_columns loop

            ddl_column(
                p_nazwa_tabeli => r.TB_TABLE_NAME,
                p_nazwa => r.tc_column_name,
                p_schemat => r.TB_OWNER,
                p_typ => r.tc_data_type,
                p_komentarz => r.TC_DESC,
                p_nullable => r.TC_NULLABLE,
                p_default => r.TC_DEFAULT,
                p_nowa => r.nowa,
                p_tc_fk_column_name => r.TC_FK_COLUMN_NAME,
                p_tc_ix_name => r.TC_IX_NAME,
                p_tc_uk_name => r.TC_UK_NAME
            );

            if (nvl(r.TC_DESC,'-') <> nvl(r.comments,'-')) then
              komentarz_kol(r.TB_OWNER, r.TB_TABLE_NAME, r.tc_column_name, r.TC_DESC);
            end if;
             
        end loop;
   
    end ddl_columns;
    

    procedure ddl_constraints as
    begin 
      null;
    end ddl_constraints;
    
    function is_valid(p_tb_id number) 
        return varchar2
    is
    begin
        open g_cu_tables(p_tb_id);
        fetch g_cu_tables into g_v_tables;
        if g_cu_tables%found then 
            close g_cu_tables;
            return 'N';
        end if;
        --
        close g_cu_tables;
        --
        open g_cu_columns(p_tb_id);
        fetch g_cu_columns into g_v_columns;
        if g_cu_columns%found then 
            close g_cu_columns;
            return 'N';
        end if;
        --
        close g_cu_columns;
        --
        return 'T';
    END;
END;
/
