create or replace view PPADM.PPV_ZALOGOWANY_PROFILE as
select
    -- $Revision::          $--
	EAT_PROFILE.*, PPV_PROFILE_PROFILE_MACIERZ.*
from EAADM.EAT_DOSTEPY_DO_FIRM 
  join PPV_PROFILE_PROFILE_MACIERZ on PRF_ID_NAD = DDF_PRF_ID
  join EAADM.EAT_PROFILE on PRF_ID = PRF_ID_POD
where ddf_uzt_nazwa = (select uzt_nazwa from ppv_zalogowany)
/

