create or replace procedure PPADM.PPP_AKT_ZAL_DOK_PDF_DELEGACJI(P_DOK_ID NUMBER, P_NAZWA_RAPORTU VARCHAR2, P_OPIS_ZALACZNIKA VARCHAR2 DEFAULT NULL) AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                           $
-- $Revision::                                                                                                                                                         $
-- $Workfile::                                                                                                                                                         $
-- $Modtime::                                                                                                                                                          $
-- $Author::                                                                                                                                                           $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- procedura wysy�a do serwera/serwer�w apl. PP zlecenie aktualizacji pierwszego za��cznika o nazwie Delegacja_krajowa_rozliczenie.pdf
-- P_DOK_ID id wiersza z KGT_DOKUMENTY
-- przyk�ady wywolania:
-- exec PPP_AKT_ZAL_DOK_PDF_DELEGACJI(806868, 'Delegacja_krajowa_polecenie');
-- exec PPP_AKT_ZAL_DOK_PDF_DELEGACJI(806868, 'Delegacja_krajowa_rozliczenie');
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  zalacznik zkt_zalaczniki%ROWTYPE;
  v_new_zal_id  NUMBER(10);
BEGIN

    FOR r in (
        select *
        from kgt_dokumenty
            join EGADM1.zkt_zalaczniki on dok_id = zal_dok_id
            join PPADM.ppt_del_wnioski_delegacji on WND_DOK_ID = DOK_ID
        where zal_dok_id = P_DOK_ID
            and zal_nazwa_pliku like '%'|| P_NAZWA_RAPORTU || '.pdf' --Delegacja_krajowa_rozliczenie
        order by zal_id
    ) LOOP
    
        UPDATE ZKT_ZALACZNIKI SET zal_blob = NULL WHERE zal_id = r.ZAL_ID;
        
        ppadm.msg('%'
            , P_AUTONOMOUS_TRANSACTION => 'N'
            , P_SEVERITY=> 'JASPERTOPDF_ZALACZNIK' --komunkat techniczny - rodzaj zadania
            , P_TEKST=> '' --parametry raportu
            || 'P_DOK_ID='||P_DOK_ID||chr(10)
        );
        return; --wystarczy tylko pierwszy zalacznik (jesli jest ich wiecej reszte ignorujemy);
    END LOOP;
    
    
    --jesli nie ma co aktualizowac - dodajemy nowy zalacznik
    
    zalacznik.ZAL_F_UPRAWNIENIA_SPECJALNE := 'N';
    zalacznik.zal_nazwa 		    := P_NAZWA_RAPORTU||'.pdf';
    zalacznik.zal_typ 			    := 1 ;
    zalacznik.ZAL_DOK_ID        :=  P_DOK_ID ; 
    zalacznik.zal_nazwa_pliku   := P_NAZWA_RAPORTU||'.pdf';
    zalacznik.zal_blob          := NULL;
    zalacznik.ZAL_OPIS          := NVL( P_OPIS_ZALACZNIKA, P_NAZWA_RAPORTU||'.pdf' ) ;
    v_new_zal_id := ZKP_ZALACZNIKI.DODAJ_ZALACZNIK(zalacznik);

     ppadm.msg('%'
            , P_AUTONOMOUS_TRANSACTION => 'N'     
            , P_SEVERITY=> 'JASPERTOPDF_ZALACZNIK' --komunkat techniczny - rodzaj zadania
            , P_TEKST=> '' --parametry raportu
            || 'P_DOK_ID='||P_DOK_ID||chr(10)
     ); 
    
END;
/