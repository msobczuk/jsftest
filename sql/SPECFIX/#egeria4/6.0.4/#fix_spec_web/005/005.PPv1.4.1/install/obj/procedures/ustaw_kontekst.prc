create or replace procedure PPADM.USTAW_KONTEKST(P_ID_KLIENTA_SESJI VARCHAR2, P_MSG BOOLEAN DEFAULT TRUE) as
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
    v_komunikat varchar2(4000);
    v_ret varchar2(100);
begin

    for r in (select * from ppv_zalogowany where id_klienta_sesji <> P_ID_KLIENTA_SESJI) loop
        v_ret := eap_web.ustaw_kontekst( P_ID_KLIENTA_SESJI, v_komunikat );
    end loop;

    if P_MSG then
    for u in (
                select u.*
                from ppv_zalogowany u
                where u.id_klienta_sesji = P_ID_KLIENTA_SESJI
    ) loop
       msg( P_ID_KLIENTA_SESJI,  -- zamiast uzytkownika przsylamy komunikat dowybraej sesji
            SYSDATE || ' - WYKONANO PPADM.USTAW_KONTEKST ',-- temat
           '' --tresc ...
           ||chr(13)||chr(10)||'------------------------------------------------------------------------------------'
           ||chr(13)||chr(10)||'USER: ' ||   USER   ||'; CURRENT_SCHEMA: ' || SYS_CONTEXT('USERENV','CURRENT_SCHEMA')
           ||chr(13)||chr(10)||'SESSIONID: '|| SYS_CONTEXT('USERENV', 'SESSIONID') ||'; SID: ' || SYS_CONTEXT('USERENV','SID')
           ||chr(13)||chr(10)||'CLIENT_IDENTIFIER: ' || SYS_CONTEXT('USERENV', 'CLIENT_IDENTIFIER')
           ||chr(13)||chr(10)||'HOST: ' || SYS_CONTEXT('USERENV','HOST') || '; MODULE: ' || SYS_CONTEXT('USERENV', 'MODULE')
           ||chr(13)||chr(10)||'------------------------------------------------------------------------------------'
           ||chr(13)||chr(10)||'uzt_nazwa_zewn: '       || u.uzt_nazwa_zewn
           ||chr(13)||chr(10)||'uzt_opis: '             || u.uzt_opis
           ||chr(13)||chr(10)||'prc_id: '               || u.prc_id
           ||chr(13)||chr(10)||'prc_numer: '            || u.prc_numer
           ||chr(13)||chr(10)||'prc_imie: '             || u.prc_imie
           ||chr(13)||chr(10)||'prc_nazwisko: '         || u.prc_nazwisko
           ||chr(13)||chr(10)||'odczytaj_firme: '       || eap_globals.odczytaj_firme
           ||chr(13)||chr(10)||'odczytaj_oddzial: '     || eap_globals.odczytaj_oddzial
           ||chr(13)||chr(10)||'------------------------------------------------------------------------------------'
         );
    end loop;
    end if;
end;
/

