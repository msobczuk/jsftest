create or replace function PPADM.ST_WNIOSEK_GENERUJ_DOK(P_WN_ID NUMBER) return varchar2 as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- funkcja generuje dokument (w kgt_dokumenty) dla wskazanedo wniosku i zwraca (jako tekst) utworzony dok_id ('000000'||dok_id);
-- w przypadku bledu zwraca opis bledu
-------------------------------------------------------------------------------------------------------------------------------------------
--grant execute on EGADM1.STP_WNIOSKI_INW to ppadm;
--grant execute on EGADM1.NZP_TYMCZASOWO to ppadm;
       v_dok_id varchar2(40);
       v_msg varchar2(4000);
begin

    for r in (
        select *
        from EGADM1.stt_wnioski
            join EGADM1.stt_inwentaryzacje on wn_i_id = i_id
            left join EGADM1.stt_wnioski_parametry on wn_id = wnp_wn_id and wnp_kod in ('ZM_OS_ODP', 'LIKW')
            left join EGADM1.stt_podzialy_srodka on wnp_ps_id = ps_id
        where wn_id = p_wn_id
    ) loop

        EGADM1.NZP_TYMCZASOWO.USUN('STT_WNIOSKI', P_WN_ID); --na wypadek regenrowania po bledach
        EGADM1.NZP_TYMCZASOWO.DODAJ('STT_WNIOSKI', P_WN_ID);
        EGADM1.STP_WNIOSKI_INW.GENERUJ_DOKUMENTY(R.I_NUMER); -- dok_sdn_wn_id
        select wn_dok_id into v_dok_id from stt_wnioski where wn_id = p_wn_id;
        EGADM1.NZP_TYMCZASOWO.USUN('STT_WNIOSKI', P_WN_ID);

        v_msg := '�rodki trwa�e / lista wniosk�w: wygenerowano dokument ' || v_dok_id || ' dla wniosku '||r.WN_NUMER;
        ppadm.st_wniosek_wiadomosci(r.WN_ID, v_msg);

        return '000000' || v_dok_id;

    end loop;

    --brak wg parametrow: inwentaryzacji, sdn, podzialu, albo ktoregos pracownika
     --RAISE_APPLICATION_ERROR (-20000,'PPADM.ST_WNIOSEK_GENERUJ_DOK( P_WN_ID:' || P_WN_ID || ') -> brak danych!!' );
     return 'PPADM.ST_WNIOSEK_GENERUJ_DOK( P_WN_ID:' || P_WN_ID || ') -> brak danych!!';
end;
/
