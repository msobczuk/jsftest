create or replace procedure       PPADM.PPP_GENERUJ_TRIGGER_TBTS(p_owner VARCHAR2, p_table_name VARCHAR2) AUTHID CURRENT_USER as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                           $
-- $Revision::                                                                                                                                                         $
-- $Workfile::                                                                                                                                                         $
-- $Modtime::                                                                                                                                                          $
-- $Author::                                                                                                                                                           $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    v_owner varchar2(50) := UPPER(p_owner);             -- 'egadm1' -> 'EGADM1';
    v_table_name varchar2(200) := UPPER(p_table_name);  -- 'ek_pracownicy' ->  'EK_PRACOWNICY';
    v_table varchar2(250) := UPPER( v_owner ||'.'||v_table_name ); --> 'EGADM1.EK_PRAOCWINCY' (docelowy PK w TBTS)
    v_prefix varchar2(250);
    v_trigger varchar2(250);
begin
    if v_table_name = 'PPT_TAB_TIMESTAMPS' or v_table_name = 'DUAL'  then
        return;
    end if;

    SELECT SUBSTR (column_name, 1, INSTR (column_name, '_')) prefix
            INTO v_prefix
    FROM sys.dba_tab_columns
    WHERE  owner = v_owner and table_name = v_table_name AND ROWNUM = 1;

    v_trigger:= v_owner||'.PPG_'||v_prefix||'AIUD_TBTS';--... gdyby na EGADM1.EK_PRACOWNICY nie byo trg. audytowego, trigger utw. jako ->  'EGADM1.PPG_PRC_AIUD_TBTS'...
    for r in (
        select * from SYS.all_triggers
        where table_owner = v_owner and table_name = v_table_name
        and trigger_name like '%\_BIUR\_AUDYT' escape '\'
        and rownum=1
    ) loop
        v_trigger:=  v_owner||'.'||replace(r.trigger_name, '_BIUR_AUDYT', '_AIUD_TBTS' ); --... ale skoro jest trigger autytowy (EGADM1.EKG_PRC_BIUR_AUDYT) wiêc utowrzymy np. 'EGADM1.PPG_PRC_AIUD_TBTS'...
    end loop;

    EXECUTE IMMEDIATE '
CREATE OR REPLACE TRIGGER '||v_trigger||'
    AFTER INSERT OR UPDATE OR DELETE ON '||v_table||'
DECLARE
  PRAGMA AUTONOMOUS_TRANSACTION;
begin
    update PPADM.PPT_TAB_TIMESTAMPS set TBTS_TS = systimestamp where tbts_table = '''||v_table|| ''';
    commit;
end;';

   update PPADM.PPT_TAB_TIMESTAMPS set tbts_trigger = v_trigger where tbts_table = v_table;
   IF SQL%ROWCOUNT <1 THEN
        insert into PPADM.PPT_TAB_TIMESTAMPS (TBTS_TABLE, tbts_trigger, tbts_table_owner, tbts_table_name) values (v_table, v_trigger, v_owner, v_table_name);
   END IF;
   COMMIT;
end;
/



