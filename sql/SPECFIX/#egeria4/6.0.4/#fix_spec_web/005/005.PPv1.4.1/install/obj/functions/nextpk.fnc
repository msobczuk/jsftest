CREATE OR REPLACE FUNCTION PPADM.NEXTPK(P_TABLE_NAME VARCHAR2) RETURN NUMBER AS
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                         $
-- $Revision::                                                                                                                                                                       $
-- $Workfile::                                                                                                                                                                       $
-- $Modtime::                                                                                                                                                                        $
-- $Author::                                                                                                                                                                         $
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Funkcja do nadawania nowych wartosci dla PK w tabelach PPADM, gdzie korzystamy z hibernate'a (zamiast sekwencji), a kolejne klucze s� trzymane w tabli PPT_JPA_GENKEYS ...
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PRAGMA AUTONOMOUS_TRANSACTION;
    V_RET NUMBER;
BEGIN
    UPDATE PPT_JPA_GENKEYS SET PKEY_VALUE = PKEY_VALUE + 1 WHERE PKEY_TABLE = UPPER(P_TABLE_NAME)  RETURNING PKEY_VALUE-1 INTO V_RET;
    IF SQL%ROWCOUNT<1 THEN
        INSERT INTO PPT_JPA_GENKEYS(PKEY_TABLE, PKEY_VALUE, PKEY_TS) VALUES ( UPPER(P_TABLE_NAME), 2, SYSDATE );
        V_RET:=1;
    END IF;
    COMMIT;
    RETURN V_RET;
END;
/
