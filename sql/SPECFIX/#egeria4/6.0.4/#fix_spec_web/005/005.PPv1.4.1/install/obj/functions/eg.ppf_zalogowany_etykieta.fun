create or replace function ppadm.ppf_zalogowany_etykieta
	return varchar2
is
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
--------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
	v_etykieta varchar2(512);
begin

	select p.prc_imie || ' ' ||
		p.prc_nazwisko || ' ' ||
		'(' || p.uzt_nazwa || ')'|| ' - ' ||
		s.stn_nazwa || ' - ' ||
		jo.ob_pelny_kod labelka
		into v_etykieta
	from  ppv_zalogowany p
        left join egadm1.ek_zatrudnienie z on z.zat_prc_id = p.prc_id and  zat_typ_umowy = '0' and sysdate between zat_data_zmiany and coalesce(zat_data_do, sysdate)
        left join egadm1.zpt_stanowiska s on s.stn_id = z.zat_stn_id
        left join egadm1.css_obiekty_w_przedsieb jo on jo.ob_id = z.zat_ob_id;

	return v_etykieta;

end ppf_zalogowany_etykieta;
/








