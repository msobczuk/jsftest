create or replace function PPADM.PPF_BLOKADA_LOGOWANIA_UZT(P_UZT_NAZWA_ZEWNETRZNA VARCHAR2) return VARCHAR2
as
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    v_cnt number;
    v_ret varchar2(1000);
begin
    -- ��期���
    -- wersja std nie przewidujemy zadnych blokad; w wersji spersonalizowanej mozna zastosowac wlasny kod
    -- Je�eli u�ytkownik nie ma �adnej blokady to funkcja zwraca null.
    -- W przypadku blokady zwrocony zostaje komunikat z powodem blokady.
    --
    -- 1. Sprawdzenie czy konto istnieje
    select count(*) into v_cnt from EAADM.EAT_UZYTKOWNICY uzt
    where lower(UZT_NAZWA_ZEWN) = lower(P_UZT_NAZWA_ZEWNETRZNA); --parametr - np. 'imie.nazwisko@comarch.com'
    if v_cnt = 0 then
        return 'Logowanie niemo�liwe - konto nie istnieje';
    end if;
    -- 2. Sprawdzenie czy konto nie jest zablokowane
    select count(*) into v_cnt from EAADM.EAT_UZYTKOWNICY uzt
    where lower(UZT_NAZWA_ZEWN) = lower(P_UZT_NAZWA_ZEWNETRZNA)
        and UZT.UZT_F_ZABLOKOWANY = 'N'; --parametr - np. 'imie.nazwisko@comarch.com'
    if v_cnt = 0 then
        return 'Logowanie niemo�liwe - konto zosta�o zablokowane';
    end if;
    -- 3. Sprawdzenie czy u�ytkownik posiada aktualne umowy umo�liwiaj�ce dost�p do Portalu
    eap_globals.ustaw_konsolidacje('T');
    select count(*) into v_cnt
    from EAADM.EAT_UZYTKOWNICY uzt
    join EAADM.EAT_DOSTEPY_DO_FIRM ddf on UZT.UZT_NAZWA = DDF.DDF_UZT_NAZWA
    join (
        ------------------------------------------------------------------------------------------------------------------
        -- macierz przacownikow nadrz/podrz: podrzedni + nadrzedni + pozostali(takze jako nadrzedni)
        ------------------------------------------------------------------------------------------------------------------
        select prc_prc_id as prc_id_l,  prc_id prc_id_r,  1 lvl from EGADM1.ek_pracownicy where prc_prc_id is not null --podrzedni
        union select prc_id,            prc_prc_id,      -1 lvl from EGADM1.ek_pracownicy where prc_prc_id is not null --nadrzedni
        union select prc_id,            prc_id,           0 lvl from EGADM1.ek_pracownicy --pozostali (bez ukladu nadrz/podrz i jednoczesnie przekatna macierzy)
        ------------------------------------------------------------------------------------------------------------------
    ) on ddf_prc_id = prc_id_l

    join EGADM1.EK_ZATRUDNIENIE zat on prc_id_r = zat.zat_prc_id
        and sysdate between zat.zat_data_zmiany and nvl (zat.zat_data_do, sysdate) --tylko aktualne umowy
        and zat.zat_typ_umowy in (0,1,2,3) --tylko okreslonego typu

    where lower(UZT_NAZWA_ZEWN) = lower(P_UZT_NAZWA_ZEWNETRZNA); --parametr - np. 'kordian.kurdziel@comarch.com'
    eap_globals.ustaw_konsolidacje('N');
    if v_cnt = 0 then
        return 'Logowanie niemo�liwe - brak aktualnej umowy o prac�';
    end if;
    return null; --jesli nie ma blokady
    EXCEPTION
      WHEN OTHERS
      THEN
        eap_globals.ustaw_konsolidacje('N');
        eap_blad.zglos (p_procedura => 'PPADM.PPF_BLOKADA_LOGOWANIA_UZT');
        return 'Logowanie niemo�liwe. Skontaktuj si� z administratorem.';
end;
/





