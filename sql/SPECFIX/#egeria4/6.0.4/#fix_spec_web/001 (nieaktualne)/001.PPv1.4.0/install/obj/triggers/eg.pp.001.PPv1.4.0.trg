PROMPT ----------------------------------------------------------------------;
PROMPT 000


CREATE OR REPLACE TRIGGER PPADM.PPG_UDST_BIUR_AUDYT
BEFORE INSERT OR UPDATE ON PPADM.PPT_UDOSTEPNIENIA_DANYCH
REFERENCING FOR EACH ROW
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.UDST_AUDYT_UT, :NEW.UDST_AUDYT_DT, :NEW.UDST_AUDYT_KT, :NEW.UDST_AUDYT_LM, :OLD.UDST_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.UDST_AUDYT_UT, :NEW.UDST_AUDYT_DT, :NEW.UDST_AUDYT_KT, :NEW.UDST_AUDYT_LM, :OLD.UDST_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.UDST_AUDYT_UM, :NEW.UDST_AUDYT_DM, :NEW.UDST_AUDYT_KM, :NEW.UDST_AUDYT_LM, :OLD.UDST_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.UDST_AUDYT_UM, :NEW.UDST_AUDYT_DM, :NEW.UDST_AUDYT_KM, :NEW.UDST_AUDYT_LM, :OLD.UDST_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;
/

create or replace TRIGGER PPADM.PPG_NAP_BIUR_AUDYT
  BEFORE INSERT or UPDATE ON PPADM.PPT_REK_PROJEKTY   FOR EACH ROW
DECLARE
  v_pom VARCHAR2 (255);
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.NAP_AUDYT_UT, :NEW.NAP_AUDYT_DT, v_pom, :NEW.NAP_AUDYT_LM, :OLD.NAP_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.NAP_AUDYT_UT, :NEW.NAP_AUDYT_DT, v_pom, :NEW.NAP_AUDYT_LM, :OLD.NAP_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.NAP_AUDYT_UM, :NEW.NAP_AUDYT_DM, :NEW.NAP_AUDYT_KM, :NEW.NAP_AUDYT_LM, :OLD.NAP_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.NAP_AUDYT_UM, :NEW.NAP_AUDYT_DM, :NEW.NAP_AUDYT_KM, :NEW.NAP_AUDYT_LM, :OLD.NAP_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;
/




PROMPT ----------------------------------------------------------------------;

