CREATE OR REPLACE FUNCTION "PPADM"."OBIEG_STAN" (P_ID NUMBER, P_KATEGORIA VARCHAR2) return VARCHAR2 as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#fix_spec_web/001/001.PPv1.4.0/install/obj/functions/eg.ppf_obieg_stan.fun 1 1.0.1 25.04.18 14:23 MGOLDA                                                                            $
-- $Revision:: 1                                                                                                                                                                                                 $
-- $Workfile:: eg.ppf_obieg_stan.fun                                                                                                                                                                             $
-- $Modtime:: 25.04.18 14:23                                                                                                                                                                                     $
-- $Author:: MGOLDA                                                                                                                                                                                              $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  v_ret varchar2(250);
BEGIN
  select dstn.dstn_opis into v_ret 
  from egadm1.csst_encje_w_stanach ews
  join csst_def_stanow dstn on ews_dstn_id = dstn_id
  where ews.EWS_KOB_KOD = P_KATEGORIA
    and ews.EWS_KLUCZ_OBCY_ID = p_id;
  return v_ret;

  exception
   when NO_DATA_FOUND then
    return '';
END;
/


