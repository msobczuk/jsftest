  CREATE OR REPLACE TRIGGER "PPADM"."PPG_WND_USUN_ZALACZNIKI"
  BEFORE DELETE ON "PPADM"."PPT_DEL_WNIOSKI_DELEGACJI"
  REFERENCING FOR EACH ROW
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#fix_spec_web/001/001.PPv1.4.0/install/obj/triggers/PPG_WND_USUN_ZALACZNIKI.trg 1 1.0.1 25.04.18 14:31 MGOLDA                                                                       $
-- $Revision:: 1                                                                                                                                                                                                 $
-- $Workfile:: PPG_WND_USUN_ZALACZNIKI.trg                                                                                                                                                                       $
-- $Modtime:: 25.04.18 14:31                                                                                                                                                                                     $
-- $Author:: MGOLDA                                                                                                                                                                                              $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  DECLARE
	v_wnd_id NUMBER(10,0);
BEGIN
	v_wnd_id := :old.wnd_id;
	DELETE FROM zkt_zalaczniki
	WHERE ZAL_WND_ID = v_wnd_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_wnd_usun_zalaczniki');
END;
/


