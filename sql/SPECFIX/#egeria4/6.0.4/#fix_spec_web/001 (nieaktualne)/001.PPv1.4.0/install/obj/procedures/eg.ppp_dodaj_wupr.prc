CREATE OR REPLACE PROCEDURE "PPADM"."DODAJ_WUPR" (p_nazwa_xhtml varchar2, p_funkcja_uzytkowa varchar2  default '', p_stan_obiegu varchar2 default '', p_czy_wysiwetlac varchar2 default 'T', p_czy_edytowalne varchar2 default 'T')
as 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  v_wupr_id number;
begin 

  delete from PPT_WEB_UPRAWNIENIA 
  where 1=1
    and WUPR_NAZWA_XHTML = p_nazwa_xhtml 
    and WUPR_ZESTAW_POL_ID = 'content' 
    and NVL(WUPR_FUNKCJA_UZYTKOWA,'X') = NVL(p_funkcja_uzytkowa, 'X')
    and NVL(WUPR_STAN_OBIEGU ,'Y') = NVL(p_stan_obiegu,'Y');


  UPDATE PPT_JPA_GENKEYS set PKEY_VALUE =  PKEY_VALUE +1 where PKEY_TABLE = 'PPT_WEB_UPRAWNIENIA';
  select PKEY_VALUE into v_wupr_id from PPT_JPA_GENKEYS where PKEY_TABLE = 'PPT_WEB_UPRAWNIENIA';

  insert into PPT_WEB_UPRAWNIENIA ( WUPR_NAZWA_XHTML,   WUPR_ZESTAW_POL_ID,   WUPR_FUNKCJA_UZYTKOWA,  WUPR_STAN_OBIEGU,    WUPR_CZY_WYSWIETLAC,   WUPR_CZY_EDYTOWALNE, wupr_id      )
  values                          ( p_nazwa_xhtml ,     'content',            p_funkcja_uzytkowa      ,p_stan_obiegu  ,    p_czy_wysiwetlac,      p_czy_edytowalne,    v_wupr_id    );

--exception 
-- when others then 
--    null;
end;
/


