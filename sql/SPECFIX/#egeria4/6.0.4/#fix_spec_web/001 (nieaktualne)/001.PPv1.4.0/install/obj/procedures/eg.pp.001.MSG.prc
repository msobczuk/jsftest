CREATE OR REPLACE PROCEDURE PPADM.MSG(
	P_USER VARCHAR2 DEFAULT '', 
	P_TEMAT VARCHAR2 DEFAULT '', 
	P_TEKST VARCHAR2 DEFAULT '', 
	P_WDM_ID NUMBER DEFAULT '', 
	P_SEVERITY VARCHAR2 DEFAULT 'INFO')
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- PROCEDURA DO WYSYŁANIA KOMUNIKATÓW (DYMKÓW) NA EKRAN ZALOGOWANEGO UŻYTKOWNIKA W PORTALU
-- parametry: 
-- P_USER  - nazwa usera z EAT_UZYTKOWNICY (opcjonalnie jesli podajmey WDM_ID) UWAGA: jeśli p_user = '%', to będzie broadcast do zalogowanych w PP
-- P_TEMAT - opcjonalnie jesli podajmey WDM_ID; jesli nie podano to temat zostanie pobrany z EAT_WIADOMOSCI
-- P_TEKST - jesli nie podano to tresc zostanie pobrany z EAT_WIADOMOSCI ... powinno to byc maks. 3600 znaków (inaczej obcinamy)
-- P_WDM_ID jeden z (select wdm_id from eat_wiadomosci) 
--          Jesli pp_wdm_id podany, to na dymku dodamy przycisk z linkiem do calej wiadomosci w eat_wiadomosci...  
--             przy czym jeśli nie podano tematu to na podstawie p_wdm_id zostanie (przez kod JAVA) pobrany z eat_wiadomosci,
--             a jesli nie podano p_tekst, analogicznie zostanie (przez kod JAVA) pobrana poczatkowa tresc wiadomosci (np. pierwsze 300 znaków)
-- P_SEVERITY: jeden z 'INFO', 'WARN', 'ERROR' (kazdy inny i tak zostanie potraktowany w PP jak 'INFO')
-- Jesli pp_wdm_id nie podany oraz jednocześnie nie podano takze P_TEMAT ani P_TEKST to procedura nic nie zrobi.
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
  v_user  varchar2(30) := P_USER;
  v_temat varchar2(255) := P_TEMAT;
  v_tekst varchar2(4000):= P_TEKST;
  v_data varchar2(32767):= '';
 
  v_req  utl_http.req;
  v_resp utl_http.resp;
  v_html_text varchar2(32767);
  
  v_cnt_sesje NUMBER(10) := 0;
  
  v_wdm_temat VARCHAR2(255);
  v_wdm_tresc VARCHAR2(4000);
  v_wdm_uzt_nazwa VARCHAR2(30);
  
BEGIN
  
  IF ( P_TEMAT IS NULL    AND   P_TEKST IS NULL   AND   P_WDM_ID IS NULL ) THEN
    return;
  END IF;
  
  v_temat := substr(v_temat, 1, 255);
  --v_temat := convert(v_temat, 'utf8', 'EE8ISO8859P2' ); --text, dest charset, source charset
  v_tekst := substr(v_tekst, 1, 3600);
  --v_tekst := REPLACE(v_tekst,CHR(10),'</br>'); to samo robi kod JAVA
  --v_tekst := convert(v_tekst, 'utf8', 'EE8ISO8859P2' ); --text, dest charset, source charset
  
  IF (not p_wdm_id is null) THEN 
    
    select WDM_TEMAT,    substr(WDM_TRESC, 1, 3600),  wdm_uzt_nazwa 
    into   v_wdm_temat,  v_wdm_tresc,                 v_wdm_uzt_nazwa
    from eaadm.eat_wiadomosci 
    where wdm_id = p_wdm_id;
    
    if (v_user is null) then 
      v_user := v_wdm_uzt_nazwa;
    end if;
    
    if (v_temat is null) then 
      v_temat := v_wdm_temat;
    end if;
    
    if (v_tekst is null) then 
      v_tekst := v_wdm_tresc;
    end if;
    
  END IF;
  
  IF ( NVL(v_user,'Z') <> '%' ) THEN
    select count(*) into v_cnt_sesje from EAADM.EAT_SESJE_ZEWNETRZNE where szw_uzt_nazwa = v_user;
    IF ( v_cnt_sesje = 0 ) THEN 
      dbms_output.put_line('.....');
      return; -- P_USER nie jest zalogowany w PP
    END IF;
  END IF;  
  

  v_data :=  P_SEVERITY   || chr(10)
          || p_wdm_id     || chr(10)
          || v_user       || chr(10)
          || v_temat      || chr(10)
          || v_tekst;

  --DBMS_OUTPUT.PUT_LINE(v_data);
  PPADM.PPP_MSG.SEND(v_data);
  
END;
/

