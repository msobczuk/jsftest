CREATE OR REPLACE FUNCTION "PPADM"."PRC_OB_NAZWA" (p_prc_id number, p_data_zat date default sysdate) return varchar2 as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#fix_spec_web/001/001.PPv1.4.0/install/obj/functions/eg.ppf_prc_ob_nazwa.fun 1 1.0.1 25.04.18 14:23 MGOLDA                                                                          $
-- $Revision:: 1                                                                                                                                                                                                 $
-- $Workfile:: eg.ppf_prc_ob_nazwa.fun                                                                                                                                                                           $
-- $Modtime:: 25.04.18 14:23                                                                                                                                                                                     $
-- $Author:: MGOLDA                                                                                                                                                                                              $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	v_ret varchar2(200);
BEGIN

	SELECT OB.OB_NAZWA INTO v_ret 
	FROM EGADM1.EK_OBIEKTY_W_PRZEDSIEB OB
	WHERE OB.OB_ID = prc_ob(p_prc_id, p_data_zat);

	return v_ret;

EXCEPTION
  WHEN OTHERS THEN
    return null;		
END;
/


