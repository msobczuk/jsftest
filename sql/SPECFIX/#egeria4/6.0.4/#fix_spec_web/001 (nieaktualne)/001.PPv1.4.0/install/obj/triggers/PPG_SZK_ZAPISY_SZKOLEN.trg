CREATE OR REPLACE TRIGGER "PPADM"."PPG_SZK_ZAPISY_SZKOLEN"
  BEFORE DELETE ON "PPADM"."PPT_SZK_ZAPISY_SZKOLEN"
  REFERENCING FOR EACH ROW
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  DECLARE
	v_wnd_id NUMBER(10,0);
BEGIN
	v_wnd_id := :old.ZSZ_ID;
	DELETE FROM zkt_zalaczniki
	WHERE ZAL_ZSZ_ID = v_wnd_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => PPG_SZK_ZAPISY_SZKOLEN');
END;
/

