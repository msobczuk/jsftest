CREATE OR REPLACE FUNCTION "PPADM"."LIMIT_POZOSTALY_DNI" (
  p_prc_id NUMBER,
  p_rda_id NUMBER,
  p_dzien  DATE DEFAULT SYSDATE
) return number as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#fix_spec_web/001/001.PPv1.4.0/install/obj/functions/eg.ppf_limit_pozostaly_dni.fun 1 1.0.1 25.04.18 14:23 MGOLDA                                                                   $
-- $Revision:: 1                                                                                                                                                                                                 $
-- $Workfile:: eg.ppf_limit_pozostaly_dni.fun                                                                                                                                                                    $
-- $Modtime:: 25.04.18 14:23                                                                                                                                                                                     $
-- $Author:: MGOLDA                                                                                                                                                                                              $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  v_ret number:=0;
  v_dni number:=0;
  v_godz number:=0;
  v_blad varchar2(4000);
begin 
  v_ret := EKP_API_INTRANET.LIMIT_POZOSTALY(p_prc_id, p_rda_id, sysdate, v_godz, v_dni, v_blad);
  return v_dni;
end;
/

