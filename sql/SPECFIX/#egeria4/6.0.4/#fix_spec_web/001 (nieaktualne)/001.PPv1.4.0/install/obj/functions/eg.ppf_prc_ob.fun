CREATE OR REPLACE FUNCTION "PPADM"."PRC_OB" (p_prc_id number, p_data_zat date default sysdate) return number as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#fix_spec_web/001/001.PPv1.4.0/install/obj/functions/eg.ppf_prc_ob.fun 1 1.0.1 25.04.18 14:23 MGOLDA                                                                                $
-- $Revision:: 1                                                                                                                                                                                                 $
-- $Workfile:: eg.ppf_prc_ob.fun                                                                                                                                                                                 $
-- $Modtime:: 25.04.18 14:23                                                                                                                                                                                     $
-- $Author:: MGOLDA                                                                                                                                                                                              $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	v_ret NUMBER;
BEGIN

	SELECT OB.OB_ID into v_ret 
	FROM egadm1.ek_zatrudnienie ZAT
	LEFT JOIN egadm1.EK_OBIEKTY_W_PRZEDSIEB OB on ZAT.ZAT_OB_ID=OB.OB_ID
	WHERE (NVL (zat_data_do, SYSDATE) >= p_data_zat) AND (zat_data_zmiany <= LAST_DAY (p_data_zat))
		   and ZAT.ZAT_PRC_ID=p_prc_id;

	return v_ret;

EXCEPTION
  WHEN OTHERS THEN
    return null;	
END;
/


