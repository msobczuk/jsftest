CREATE OR REPLACE PROCEDURE PPADM.MSG(
    P_USER VARCHAR2 DEFAULT '', 
    P_TEMAT VARCHAR2 DEFAULT '', 
    P_TEKST VARCHAR2 DEFAULT '', 
    P_WDM_ID NUMBER DEFAULT '', 
    P_SEVERITY VARCHAR2 DEFAULT 'INFO',
    P_AUTONOMOUS_TRANSACTION VARCHAR2 DEFAULT 'T')
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- PROCEDURA DO WYSYŁANIA KOMUNIKATÓW (DYMKÓW) NA EKRAN ZALOGOWANEGO UŻYTKOWNIKA W PORTALU; DO ROZSYŁANIA WYKORZYSTUJE PPP_MSG ORAZ PAKIET DBMS_ALERT;
-- JESLI WSKAZANY UŻYTKOWNIK NIE JEST W DANEJ CHWILI ZALOGOWANY (BRAK WPISU W EAT_SEJSE_ZEWNETRZNE) NIC SIE NIE STANIE
--
-- parametry: 
-- P_USER  - nazwa usera z EAT_UZYTKOWNICY (opcjonalnie jesli podajmey WDM_ID) UWAGA: jeśli p_user = '%', to będzie broadcast do zalogowanych w PP
-- P_TEMAT - opcjonalnie jesli podajmey WDM_ID; jesli nie podano to temat zostanie pobrany z EAT_WIADOMOSCI
-- P_TEKST - jesli nie podano to tresc zostanie pobrany z EAT_WIADOMOSCI ... powinno to byc maks. 1800 bajtów (razem z tematem i severity, inaczej obcinamy)
-- P_WDM_ID jeden z (select wdm_id from eat_wiadomosci) 
--          Jesli pp_wdm_id podany, to na dymku dodamy przycisk z linkiem do calej wiadomosci w eat_wiadomosci...  
--             przy czym jeśli nie podano tematu to na podstawie p_wdm_id zostanie (przez kod JAVA) pobrany z eat_wiadomosci,
--             a jesli nie podano p_tekst, analogicznie zostanie (przez kod JAVA) pobrana poczatkowa tresc wiadomosci (np. pierwsze 300 znaków)
--
-- Jesli pp_wdm_id nie podany oraz jednocześnie nie podano takze P_TEMAT ani P_TEKST to procedura nic nie zrobi.
--
-- P_SEVERITY: jeden z 'INFO', 'WARN', 'ERROR', 'LOGOUT' (kazdy inny i tak zostanie potraktowany w PP jak 'INFO')
--             AKTUALNA IMPLEMENTACJA W PP POZAWALA TEZ NA WYLOGOWANIE WSZYSTKICH SESJI UZYTKOWNIKA, 
--             NP. ABY WYLOGOWAC UZYTKOWNIKA TEST ZE WSZYSTKICH SERWERÓW I SESJI APP PP MOZNA WYKONAC:
--             PPADM.MSG('TEST', '%', P_SEVERITY => 'LOGOUT');
--
-- Sa dwie wersje - autonomiczna (przydatna np. w procesach dugotrwalychj lub kacjach, przed ich odwoalaniem wyjatkami), oraz nieautonomiczna do stosowania 
-- w triggerach np. po dodaniu czegos do EAT_WIADOMOSCI. Decyzje o wywolaniu wlasciwej wersji ustala parametr: 
--
-- P_AUTONOMOUS_TRANSACTION => 'T' - domyslnie zostanie wykonana wersja PPADM.MSG_AUTONOMOUS_TRANSACTION 
--                              DO STOSOWANIA W SYTUACJI JESLI CHCEMY WYSLAC POWIADOMIENIE NIEZALEZNIE OD TEGO CZY TRANSAKCJA ZOSTANIE ZATWIERDZONA 
--                             (NP. KOMUNIKATY W DŁUGOTRWAŁYCH PROCESÓW LUB Z AKCJI ZANIM ZOSTANIE WYWOAŁNY EW. WYJĄTEK W CELU WYCOFANIA TRANSAKCJI)
--           albo
--
-- P_AUTONOMOUS_TRANSACTION => 'N' (lub dowolna inna wartosc) zostanie wykoanan niemal identyczna procedura PPADM.MSG_ON_COMMIT, 
--                              która rozsyla komunikaty tylko w przypadku zatwierdzeniu transakcji, w ktorej zostanie wywolana   
--                             (WERSJA DO STOSOWANIA W TRIGGERACH, JESLI INFORMACJA O ZMIANACH W TABELACH MAJĄ BYC ROZSYLANE PO ZATW. TRANSKACJI DZ. TRIGGERA)
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 


BEGIN

  IF P_AUTONOMOUS_TRANSACTION = 'T' THEN 
    PPADM.MSG_AUTONOMOUS_TRANSACTION (P_USER, P_TEMAT, P_TEKST, P_WDM_ID, P_SEVERITY);
  ELSE
    PPADM.MSG_ON_COMMIT              (P_USER, P_TEMAT, P_TEKST, P_WDM_ID, P_SEVERITY);
  END IF;

END;
/
