CREATE OR REPLACE TRIGGER EGADM1.PPG_URP_AIUDR_SYNCHRONIZACJA AFTER INSERT OR UPDATE OR DELETE ON EGADM1.EK_URLOPY_PLANOWANE FOR EACH ROW
DECLARE
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

v_id NUMBER(10);
v_id_planowane_dni NUMBER(10);
v_id_dni NUMBER(10);
start_date number;
end_date number;
v_num number;

BEGIN
  IF nvl(SYS_CONTEXT('EA_GLOBALS', 'APP_SYMBOL'), 'null')<>'PP' THEN 
    IF INSERTING  THEN
	
      select PKEY_VALUE into v_id from PPADM.PPT_JPA_GENKEYS where pkey_table = 'PPT_ABS_URLOPY_PLANOWANE';	
	
      INSERT INTO PPADM.PPT_ABS_URLOPY_PLANOWANE (URPL_DATA_DO, URPL_DATA_OD, URPL_PRC_ID, URPL_ID, URPL_STATUS) VALUES (:NEW.URP_DATA_DO, :NEW.URP_DATA_OD, :NEW.URP_PRC_ID, v_id ,'Z');
	
      start_date := to_number(to_char(:NEW.URP_DATA_OD, 'j'));
      end_date := to_number(to_char(:NEW.URP_DATA_DO, 'j'));
	
      for cur_r in start_date..end_date loop
	
        select PKEY_VALUE into v_id_planowane_dni from PPADM.PPT_JPA_GENKEYS where pkey_table = 'PPT_ABS_URLOPY_PLANOWANE_DNI';	
  
        v_num := EXTRACT(YEAR FROM to_date(cur_r, 'j'));

        insert into PPADM.PPT_ABS_URLOPY_PLANOWANE_DNI (URPD_ID, URPD_DATA, URPD_PRC_ID, URPD_URPL_ID, URPD_ROK) values (v_id_planowane_dni, to_date(cur_r, 'j'), :NEW.URP_PRC_ID, v_id, v_num);
    
      	v_id_planowane_dni := v_id_planowane_dni + 1;
	
      	UPDATE PPADM.PPT_JPA_GENKEYS set PKEY_VALUE = v_id_planowane_dni where PKEY_TABLE = 'PPT_ABS_URLOPY_PLANOWANE_DNI';

	
      end loop;
	
      v_id := v_id + 1;
	
      UPDATE PPADM.PPT_JPA_GENKEYS set PKEY_VALUE = v_id where PKEY_TABLE = 'PPT_ABS_URLOPY_PLANOWANE';
	
    END IF;
    IF UPDATING  THEN
       UPDATE PPADM.PPT_ABS_URLOPY_PLANOWANE SET URPL_DATA_DO = :NEW.URP_DATA_DO, URPL_DATA_OD = :NEW.URP_DATA_OD
        where URPL_PRC_ID = :NEW.URP_PRC_ID
        and URPL_DATA_OD = :NEW.URP_DATA_OD;
    END IF;
    
    IF DELETING  THEN
    --      select URPL_ID into v_id_dni from PPADM.PPT_ABS_URLOPY_PLANOWANE where URPL_PRC_ID = :OLD.URP_PRC_ID
    --        and URPL_DATA_OD = :OLD.URP_DATA_OD;	
    --      DELETE  FROM PPADM.PPT_ABS_URLOPY_PLANOWANE_DNI where URPD_URPL_ID = v_id_dni;
    --      DELETE  FROM PPADM.PPT_ABS_URLOPY_PLANOWANE where URPL_ID = v_id_dni;

    --ppadm.plog('kasowanie urpl ...');
    for r in  (select * from PPADM.PPT_ABS_URLOPY_PLANOWANE where URPL_PRC_ID = :OLD.URP_PRC_ID and URPL_DATA_OD = :OLD.URP_DATA_OD) loop
        DELETE  FROM PPADM.PPT_ABS_URLOPY_PLANOWANE_DNI where URPD_URPL_ID = r.URPL_ID;
        DELETE  FROM PPADM.PPT_ABS_URLOPY_PLANOWANE where URPL_ID = r.URPL_ID;
        --ppadm.plog('kasowanie urpl ' || r.URPL_ID);
        PPADM.WSTAW_WIADOMOSC( r.urpl_prc_id, 'W systemie EGR/EK skasowano twój planowany urlop w dniach ' || TO_CHAR(r.urpl_data_od, 'DD-MM-YYYY')||' r.' || ' ' || TO_CHAR(r.urpl_data_do, 'DD-MM-YYYY')||' r.');
    end loop;

    END IF;
  END IF;
END;
/


