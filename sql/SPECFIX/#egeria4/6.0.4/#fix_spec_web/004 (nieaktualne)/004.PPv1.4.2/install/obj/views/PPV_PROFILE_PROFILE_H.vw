create or replace view PPADM.PPV_PROFILE_PROFILE_H as
select  
-- hierarchia profili jako pelna macierz przejsc prf/prf=lvl
-- $Revision:: 1       $--
    CONNECT_BY_ROOT prf_prf_id_nad prf_id_nad, 
    CONNECT_BY_ROOT pnad.prf_nazwa prf_nazwa_nad,
    prf_prf_id_pod prf_id_pod,
    ppod.prf_nazwa prf_nazwa_pod,
    level prf_lvl, 
    SYS_CONNECT_BY_PATH(ppod.prf_id||'.'||ppod.prf_nazwa, ' / ') prf_pth
    
from EAT_PROFILE_PROFILE 
    join eat_profile ppod on prf_prf_id_pod = ppod.prf_id
    join eat_profile pnad on prf_prf_id_nad = pnad.prf_id
    
CONNECT BY NOCYCLE PRIOR prf_prf_id_pod = prf_prf_id_nad  

union 

select -- poziom 0 (przekatna macierzy prf x prf)
    prf_id, 
    prf_nazwa, 
    prf_id, 
    prf_nazwa, 
    0, 
    null 
from eat_profile

order by prf_id_nad, prf_id_pod, prf_lvl;
