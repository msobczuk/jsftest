CREATE OR REPLACE TRIGGER PPADM.PPG_LST_AIUD_TBTS
    AFTER INSERT OR UPDATE OR DELETE ON PPADM.PPT_ADM_LISTY_WARTOSCI 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                           $
-- $Revision::                                                                                                                                                         $
-- $Workfile::                                                                                                                                                         $
-- $Modtime::                                                                                                                                                          $
-- $Author::                                                                                                                                                           $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------	
begin 
    --update PPADM.PPT_TAB_TIMESTAMPS set TBTS_TS = systimestamp where tbts_table = 'PPADM.PPT_ADM_LISTY_WARTOSCI'; za czesto
    IF UPDATING('LST_NAZWA') THEN --jesli to update z Hiberante'a (czyli wszystkie kolumny), a nie tylko zmiana na LST_WCZYTWANO_KIEDY (co zmienia sie bardzo czesto)
        update PPADM.PPT_TAB_TIMESTAMPS set TBTS_TS = systimestamp where tbts_table = 'PPADM.PPT_ADM_LISTY_WARTOSCI';
    END IF;
end;
/
