CREATE OR REPLACE TRIGGER "EGADM1"."PPG_ZAT_ANONIMIZACJA_INFO"
AFTER INSERT ON "EGADM1"."EK_ZATRUDNIENIE" FOR EACH ROW
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                    $
-- $Revision::                                                                                                                                                  $
-- $Workfile::                                                                                                                                                  $
-- $Modtime::                                                                                                                                                   $
-- $Author::                                                                                                                                                    $
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Po zatrudnieniu kandydata z rekrutacji PP wstawiamy z przysz�� dat� (3 miesi�ce od zatrudnienia) powiadomienia/eat_wiadomosci 
-- dla p. komisji rekrutacyjnej o koniecznosci anonimizacji danych pozosta�ych kandydat�w (niezatrudnionych w procesie rekrutacji)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE
  c_procedura CONSTANT VARCHAR2(100) := 'Trigger.ppg_zat_anonimizacja_info';
BEGIN
    
    for r in (
  
        --odbiorcy oczekujacy na powiadomienia o konieczno�ci anonimizacji danych pozosta�ych (niezatrudnionych) kandydat�w  
        select distinct
        
            prc_id, prc_ka_id,
            prc_numer, prc_imie, prc_nazwisko, --zatrudniany
            ka_nab_id,  --zatrudniany -kandydat-> nabor_id
            kom.kom_prc_id --nabor -komisja-> odbiorca komunikatu
            --(select count(*) from ppadm.ppt_rek_kandydaci k2 where k2.ka_nab_id = k.ka_nab_id and k2.ka_plec <> 'X' and k.ka_f_czy_zatrudniony <> 'T') do_anonimizacji_cnt
        from ek_pracownicy
            join PPADM.ppt_rek_kandydaci k on prc_ka_id = ka_zp_id
            join PPADM.ppt_rek_nabor_komisja kom on ka_nab_id = KOM_NAB_ID and KOM_TYP = 'B'
        where prc_id = :NEW.zat_prc_id --161625 --zatrudniany kandydat...
            and  (select count(*) from ppadm.ppt_rek_kandydaci k2 where k2.ka_nab_id = k.ka_nab_id and k2.ka_plec <> 'X' and k.ka_f_czy_zatrudniony <> 'T') > 0 -- do_anonimizacji_cnt
            -- je�li sa jeszcze jacy� kandydaci do anonimizacji
    ) loop
    
        PPADM.WIADOMOSC(
            P_PRC_ID => r.prc_id, 
        
            P_TRESC => 
                'Min�y 3 miesi�ce od zatrudnienia pracownika nr: ' || r.prc_numer || ' [' || r.prc_imie || ' ' || r.prc_nazwisko || ']'
            || '. Mo�na zanonimizowa� pozosta�ych niezatrudnionych kandydat�w z naboru: ' || r.ka_nab_id || '.'
            ||CHR(13)||CHR(10)||CHR(13)||CHR(10)
            || ppadm.get_adres() ||'KartotekaRekrutacji?pp_rek_kartoteki='||r.ka_nab_id --link/url do w�a�ciwej rekrutacji
            ||CHR(13)||CHR(10)||CHR(13)||CHR(10)
        
            , P_TEMAT => 'WIADOMO�� SYSTEMOWA PP'
            , P_WDM_DATA_WSTAWIENIA => ADD_MONTHS(:NEW.zat_data_przyj, 3)
            , P_AUTONOMOUS_TRANSACTION => 'T'
        );
        
    end loop;
  
EXCEPTION
	WHEN OTHERS THEN
      PPADM.plog(c_procedura 
          || '; :NEW.zat_id: ' || :NEW.zat_id
          || '; SQLCODE: ' || SQLCODE 
          || '; SQLERRM: ' || SUBSTR(SQLERRM,1,3500) 
      );
      --eap_blad.zglos (p_procedura => c_procedura);
END;
/
