create or replace FUNCTION "PPADM"."KOSZTY_DELEGACJI" (p_WND_ID NUMBER, p_KAL_RODZAJ varchar2) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 v_suma NUMBER;
BEGIN
  		  
SELECT sum(kwota) into v_suma FROM(
			SELECT 
				ROUND(SUM(PKL.PKL_KWOTA_MF)*nvl(avg(PKL_KURS),1)/nvl(avg(WAL.WAL_JEDNOSTKA_KURSU),1), 2) kwota
			FROM PPT_DEL_KALKULACJE KAL
				JOIN PPT_DEL_POZYCJE_KALKULACJI PKL on PKL.PKL_KAL_ID=KAL.KAL_ID 
				join EGADM1.CSS_WALUTY WAL on WAL.WAL_ID=PKL.PKL_WAL_ID
			WHERE KAL.KAL_WND_ID=p_WND_ID
					  and KAL.KAL_RODZAJ=p_KAL_RODZAJ
			group by wal.WAL_SYMBOL
)
;		  
		  
  return v_suma; 	

  EXCEPTION
	WHEN OTHERS THEN
		return null;
  
END;
/