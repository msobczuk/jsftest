  CREATE OR REPLACE TRIGGER "PPADM"."PPG_URLOP_WIADOMOSC"
  BEFORE INSERT OR UPDATE ON "PPADM"."PPT_ABS_URLOPY_WNIOSKOWANE"
  REFERENCING FOR EACH ROW
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#fix_spec_web/001/001.PPv1.4.0/install/obj/triggers/PPG_URLOP_WIADOMOSC.trg 1 1.0.1 25.04.18 14:30 MGOLDA                                                                           $
-- $Revision:: 1                                                                                                                                                                                                 $
-- $Workfile:: PPG_URLOP_WIADOMOSC.trg                                                                                                                                                                           $
-- $Modtime:: 25.04.18 14:30                                                                                                                                                                                     $
-- $Author:: MGOLDA                                                                                                                                                                                              $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  DECLARE
  PRC_ID_PRZ NUMBER(10);
  IMIE_PRACOWNIKA VARCHAR2(20 BYTE);
  NAZWISKO_PRACOWNIKA VARCHAR2(100 BYTE);
  NUMER_PRACOWNIKA NUMBER(6,0);
  
BEGIN
	IF INSERTING THEN 
	    --ppp_global.ustaw_date_dla_struktury_po(sysdate);
		SELECT PRC_IMIE,PRC_NAZWISKO, PRC_NUMER into IMIE_PRACOWNIKA, NAZWISKO_PRACOWNIKA, NUMER_PRACOWNIKA FROM EGADM1.EK_PRACOWNICY WHERE PRC_ID = :NEW.UPRW_PRC_ID; -- imie, nazwisko i numer osoby wnioskującej o urlop
		--SELECT SPOD_PRC_ID_PRZ into PRC_ID_PRZ FROM PPV_STRUKTURA_PODLEGLOSCI WHERE SPOD_PRC_ID = :NEW.UPRW_PRC_ID and ROWNUM=1; -- wybrac przełożonego dla pracownika biorącego urlop
		PRC_ID_PRZ:=PPADM.PRZELOZONY(:NEW.UPRW_PRC_ID);-- wybrac przełożonego dla pracownika biorącego urlop		
  
				
		IF not(PRC_ID_PRZ IS NULL) then
			PPADM.WIADOMOSC(PRC_ID_PRZ, 'Pracownik '
										|| IMIE_PRACOWNIKA 
										||' '
										|| NAZWISKO_PRACOWNIKA 
										|| ' ('
										||NUMER_PRACOWNIKA
										||') wnioskuję o urlop w dniach: '
										||:NEW.UPRW_DATA_OD 
										|| ' - '
										||:NEW.UPRW_DATA_DO
                    || '.', P_WDM_TYP => 'M'); --wiadomosc do przelozonego
		END IF;

		IF not(:NEW.UPRW_ZATW_PRC_ID IS NULL) then
			PPADM.WIADOMOSC(:NEW.UPRW_ZATW_PRC_ID, 'Pracownik '
										|| IMIE_PRACOWNIKA 
										||' '
										|| NAZWISKO_PRACOWNIKA 
										|| ' ('
										||NUMER_PRACOWNIKA
										||') wnioskuję o urlop w dniach: '
										||:NEW.UPRW_DATA_OD 
										|| ' - '
										||:NEW.UPRW_DATA_DO
                    || '.', P_WDM_TYP => 'M'); --wiadomosc do zatwierdzajcaego
		END IF;		

	    IF not(:NEW.UPRW_ZAST_PRC_ID IS NULL) then
			PPADM.WIADOMOSC(:NEW.UPRW_ZAST_PRC_ID, 'Pracownik '
													|| IMIE_PRACOWNIKA 
													||' '
													|| NAZWISKO_PRACOWNIKA 
													|| ' ('
													||NUMER_PRACOWNIKA
													||') wybrał Cię do zastąpienia go podczas urlop w dniach: '
													||:NEW.UPRW_DATA_OD 
													|| ' - '
													||:NEW.UPRW_DATA_DO
                          || '.', P_WDM_TYP => 'M'); --wiadomosc do zastępującego
		END IF;  	  
	END IF;

	IF UPDATING THEN 
		--ppp_global.ustaw_date_dla_struktury_po(sysdate);
		SELECT PRC_IMIE,PRC_NAZWISKO, PRC_NUMER into IMIE_PRACOWNIKA, NAZWISKO_PRACOWNIKA, NUMER_PRACOWNIKA FROM EGADM1.EK_PRACOWNICY WHERE PRC_ID = :NEW.UPRW_PRC_ID; -- imie, nazwisko i numer osoby wnioskującej o urlop
		--SELECT SPOD_PRC_ID_PRZ into PRC_ID_PRZ FROM PPV_STRUKTURA_PODLEGLOSCI WHERE SPOD_PRC_ID = :NEW.UPRW_PRC_ID and ROWNUM=1; -- wybrac przełożonego dla pracownika biorącego urlop
		PRC_ID_PRZ:=PPADM.PRZELOZONY(:NEW.UPRW_PRC_ID);-- wybrac przełożonego dla pracownika biorącego urlop	

   
				
		IF not(:NEW.UPRW_ZATW_PRC_ID IS NULL) and (:OLD.UPRW_DATA_OD<>:NEW.UPRW_DATA_OD or :OLD.UPRW_DATA_DO<>:NEW.UPRW_DATA_DO ) then
			PPADM.WIADOMOSC(:NEW.UPRW_ZATW_PRC_ID, 'Pracownik '
										|| IMIE_PRACOWNIKA 
										||' '
										|| NAZWISKO_PRACOWNIKA 
										|| ' (' 
										|| NUMER_PRACOWNIKA 
										||') zmodyfikował urlop o identyfikatorze: ' 
										|| :NEW.UPRW_ID 
										|| '. Pierwotnie urlop był zaplanowany w dniach: '
										||:OLD.UPRW_DATA_OD 
										|| ' - '
										||:OLD.UPRW_DATA_DO 
										|| '. Obecnie planowany jest w dniach: '
										||:NEW.UPRW_DATA_OD 
										|| ' - '
										||:NEW.UPRW_DATA_DO
                    || '.', P_WDM_TYP => 'M'); --wiadomosc do zatwierdzajcaego
		END IF;
		
		IF not(PRC_ID_PRZ IS NULL) and (:OLD.UPRW_DATA_OD<>:NEW.UPRW_DATA_OD or :OLD.UPRW_DATA_DO<>:NEW.UPRW_DATA_DO ) then
			PPADM.WIADOMOSC(PRC_ID_PRZ, 'Pracownik '
										|| IMIE_PRACOWNIKA 
										||' '
										|| NAZWISKO_PRACOWNIKA 
										|| ' (' 
										|| NUMER_PRACOWNIKA 
										||') zmodyfikował urlop o identyfikatorze: ' 
										|| :NEW.UPRW_ID 
										|| '. Pierwotnie urlop był zaplanowany w dniach: '
										||:OLD.UPRW_DATA_OD 
										|| ' - '
										||:OLD.UPRW_DATA_DO 
										|| '. Obecnie planowany jest w dniach: '
										||:NEW.UPRW_DATA_OD 
										|| ' - '
										||:NEW.UPRW_DATA_DO
                    || '.', P_WDM_TYP => 'M'); --wiadomosc do przelozonego
		END IF;


	    IF not(:NEW.UPRW_ZAST_PRC_ID IS NULL) and not(:OLD.UPRW_ZAST_PRC_ID IS NULL) and :OLD.UPRW_ZAST_PRC_ID=:NEW.UPRW_ZAST_PRC_ID and(:OLD.UPRW_DATA_OD<>:NEW.UPRW_DATA_OD or :OLD.UPRW_DATA_DO<>:NEW.UPRW_DATA_DO ) then
			PPADM.WIADOMOSC(:NEW.UPRW_ZAST_PRC_ID, 'Pracownik '
													|| IMIE_PRACOWNIKA 
													||' '
													|| NAZWISKO_PRACOWNIKA 
													|| ' ('
													||NUMER_PRACOWNIKA
													||') zmodyfikował urlop, w którym wytypował Cię do zastąpienia go. Pierwotnie urlop był planowany w dniach: '
													||:OLD.UPRW_DATA_OD 
													|| ' - '
													||:OLD.UPRW_DATA_DO
													||'. Obecnie jest planowany w dniach: '
													||:NEW.UPRW_DATA_OD 
													|| ' - '
													||:NEW.UPRW_DATA_DO
                          || '.', P_WDM_TYP => 'M'); --wiadomosc do zastępującego
		END IF;
    
		IF not(:NEW.UPRW_ZAST_PRC_ID IS NULL) and (:OLD.UPRW_ZAST_PRC_ID is null or :OLD.UPRW_ZAST_PRC_ID<>:NEW.UPRW_ZAST_PRC_ID ) then
			PPADM.WIADOMOSC(:NEW.UPRW_ZAST_PRC_ID, 'Pracownik '
													|| IMIE_PRACOWNIKA 
													||' '
													|| NAZWISKO_PRACOWNIKA 
													|| ' ('
													||NUMER_PRACOWNIKA
													||') wybrał Cię do zastąpienia go podczas urlop w dniach: '
													||:NEW.UPRW_DATA_OD 
													|| ' - '
													||:NEW.UPRW_DATA_DO
                          || '.', P_WDM_TYP => 'M'); --wiadomosc do zastępującego
		END IF; 
    
		IF not(:OLD.UPRW_ZAST_PRC_ID IS NULL) and (:NEW.UPRW_ZAST_PRC_ID IS NULL or :OLD.UPRW_ZAST_PRC_ID<>:NEW.UPRW_ZAST_PRC_ID)  then
			PPADM.WIADOMOSC(:OLD.UPRW_ZAST_PRC_ID, 'Pracownik '
													|| IMIE_PRACOWNIKA 
													||' '
													|| NAZWISKO_PRACOWNIKA 
													|| ' ('
													||NUMER_PRACOWNIKA
													||') zrezygnował z wyboru Ciebie do zastąpienia go podczas urlop w dniach: '
													||:OLD.UPRW_DATA_OD 
													|| ' - '
													||:OLD.UPRW_DATA_DO
                          || '.', P_WDM_TYP => 'M'); --wiadomosc do pierwotnego zastępującego
		END IF; 
    

	END IF; 
      
    EXCEPTION
      WHEN OTHERS THEN
      PPADM.PLOG('TRIGGER => PPG_URLOP_WIADOMOSC');
  
END;
/


