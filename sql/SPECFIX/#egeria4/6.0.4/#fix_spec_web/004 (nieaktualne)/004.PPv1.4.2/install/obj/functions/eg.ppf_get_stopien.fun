create or replace function ppadm.get_stopien(p_prc_id NUMBER, p_data_oceny date default sysdate, p_offset NUMBER default 0) return varchar2 as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	v_ret varchar2(100);
BEGIN
	SELECT 
		 slow2.WSL_OPIS INTO v_ret 
	FROM EGADM1.EK_ZATRUDNIENIE zat_akt
	left join CSS_WARTOSCI_SLOWNIKOW slow on slow.WSL_OPIS = zat_akt.ZAT_DEF_2 AND slow.WSL_SL_NAZWA = 'EK_KSC_STOPIEN'
	join CSS_WARTOSCI_SLOWNIKOW slow2 on slow2.WSL_WARTOSC = nvl(slow.WSL_WARTOSC,'0')+p_offset||'' AND  slow2.WSL_SL_NAZWA = 'EK_KSC_STOPIEN'
	WHERE 
		  zat_akt.ZAT_PRC_ID=p_prc_id
		  AND    zat_akt.zat_data_zmiany <=   trunc(p_data_oceny)
		  AND                                 trunc(p_data_oceny)  <=  nvl(zat_akt.zat_data_do, trunc(SYSDATE+(365*1000)) )     
		  AND rownum=1;
      
  return v_ret;     

	EXCEPTION
		WHEN OTHERS THEN
			return null;	
END;
/







