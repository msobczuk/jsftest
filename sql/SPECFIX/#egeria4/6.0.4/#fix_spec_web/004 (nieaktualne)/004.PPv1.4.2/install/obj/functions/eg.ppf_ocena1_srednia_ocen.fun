CREATE OR REPLACE FUNCTION PPADM.SREDNIA_OCEN (P_OCE_ID IN NUMBER) RETURN NUMBER AS 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 v_srednia NUMBER;
BEGIN
  SELECT ROUND(AVG(kryt_ocena), 2)
    INTO v_srednia
    FROM ppt_oce_kryteria
   WHERE KRYT_OCE_ID = P_OCE_ID;
  
  RETURN v_srednia;
END;
/