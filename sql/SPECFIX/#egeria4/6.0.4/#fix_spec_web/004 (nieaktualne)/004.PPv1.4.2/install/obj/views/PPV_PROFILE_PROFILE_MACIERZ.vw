create or replace view PPADM.PPV_PROFILE_PROFILE_MACIERZ AS
select --uklad profili jako macierz przejsc prf_id_nad x prf_id_pod = min(prf_lvl)
-- $Revision:: 1       $--
    prf_id_nad, prf_id_pod, min(prf_lvl) min_lvl 
from ppadm.ppv_profile_profile_h
group by prf_id_nad, prf_id_pod
order by prf_id_nad, prf_id_pod, min_lvl;