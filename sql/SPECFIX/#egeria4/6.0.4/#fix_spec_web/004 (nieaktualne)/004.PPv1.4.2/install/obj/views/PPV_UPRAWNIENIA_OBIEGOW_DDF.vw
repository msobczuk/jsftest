create or replace view PPADM.PPV_UPRAWNIENIA_OBIEGOW_DDF AS
select --uprawnieni (ddf_uzt_nazwa/ddf_prc_id) do czynnosci obiegach
-- $Revision:: 1       $--
     ddf_uzt_nazwa, ddf_prc_id, cob_swo_dstn_id_z, cob_id, cob_swo_dstn_id_do--, cob_nazwa 
from EAADM.eat_dostepy_do_firm ddf
join ( --uprawnienia / profile do czynnosci z aktualnego stanu w obiegu
            select distinct 
                prf_id_nad, prf_id_pod, cob_id, cob_swo_dstn_id_z, cob_swo_dstn_id_do--, cob_nazwa
            from EGADM1.csst_uprawnienia_obiegow 
                join EGADM1.csst_czynnosci_obiegow on cob_id = uwo_cob_id
                join ppadm.PPV_PROFILE_PROFILE_MACIERZ hprf on hprf.prf_id_pod=uwo_prf_id
     ) uwo on ddf_prf_id = prf_id_nad and ddf_frm_id = eaadm.eap_globals.odczytaj_firme
where ddf_frm_id = EAP_GLOBALS.odczytaj_firme;