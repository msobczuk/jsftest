create or replace TRIGGER EAADM.PPG_SZW_ADR_LOGOUT
  AFTER DELETE ON EAADM.EAT_SESJE_ZEWNETRZNE FOR EACH ROW
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- PORTAL PRACOWNICZY:
-- TRIGGER PO SKASOWANIU WIERSZA Z EAT_SESJE_ZEWNETRZNE WYSYLA KOMUNIKAT SYSTEMOWY   
-- ABY DANA SESJA PP ZOSTALA TAKZE WYLOGOWANA Z SERWERA APLIKACJI WEB
----------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN
  IF (:OLD.SZW_KLIENT_INFO = 'EG.PP') THEN
    PPADM.MSG(:OLD.SZW_UZT_NAZWA, :OLD.SZW_ID_KLIENTA_SESJI, P_SEVERITY => 'LOGOUT');
  END IF;
  
	EXCEPTION
		WHEN OTHERS THEN
			NULL;
END;
/
