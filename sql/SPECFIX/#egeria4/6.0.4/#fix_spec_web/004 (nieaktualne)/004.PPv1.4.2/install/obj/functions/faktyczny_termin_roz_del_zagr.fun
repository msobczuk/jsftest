create or replace FUNCTION PPADM.FAKTYCZNY_TERMIN_ROZ_DEL_ZAGR(P_WND_ID NUMBER) 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RETURN DATE AS
 v_ret DATE := null;
BEGIN

SELECT max(faktyczna_data) into v_ret
FROM (
	SELECT 
	  --max(nvl(dok.DOK_DATA_OPERACJI, TO_DATE('2099-01-01', 'YYYY-MM-DD'))) faktyczna_data--jezeli data wynosi 2099/01/01 znaczy ze jeszcze nie rozliczono
    max(
        --nvl(dok.DOK_DATA_OPERACJI, TO_DATE('2099-01-01', 'YYYY-MM-DD'))
        case when DOK_DOK_ID_ZAP is not null then nvl(dok.DOK_DATA_OPERACJI, TO_DATE('2099-01-01', 'YYYY-MM-DD'))
        else TO_DATE('2099-01-01', 'YYYY-MM-DD') end
    ) faktyczna_data--jezeli data wynosi 2099/01/01 znaczy ze jeszcze nie rozliczono
	FROM PPT_DEL_KALKULACJE KAL 
	join PPT_DEL_ZALICZKI DKZ on DKZ.DKZ_KAL_ID = KAL.KAL_ID and KAL.KAL_RODZAJ = 'ROZ'
	left join EGADM1.KGT_DOKUMENTY DOK on DOK.DOK_ID = DKZ.DKZ_DOK_ID
	WHERE DKZ.DKZ_KWOTA > 0
	and KAL.KAL_WND_ID = P_WND_ID
	GROUP BY KAL_WND_ID
	union
	select 
	  hio_data faktyczna_data
	from PPV_HISTORIA_OBIEGOW_LAST
	where HIO_KLUCZ_OBCY_ID = P_WND_ID
	and hio_obg_id = (SELECT EWS_OBG_ID FROM csst_encje_w_stanach WHERE EWS_KLUCZ_OBCY_ID = P_WND_ID and EWS_KOB_KOD = 'WN_DELZv2')
	--and (cob_nazwa like 'Przekazanie rozliczenia do zatwierdzenia przez delegującego' or ob_nazwa like 'Zatwierdzenie środków wynikających z rozliczenia do wypłaty przez dyrekcje BDG')
	and ((DSTN_NAZWA_Z = 'POZIOM70' and DSTN_NAZWA = 'POZIOM80') or (DSTN_NAZWA_Z = 'POZIOM77' and DSTN_NAZWA = 'POZIOM80'))
)
;

  IF v_ret = TO_DATE('2099-01-01', 'YYYY-MM-DD') THEN
    return null;
  ELSE
    return v_ret;
  END IF;

  EXCEPTION
    WHEN NO_DATA_FOUND THEN
		return null;
	WHEN OTHERS THEN
		return null;

END;
/

