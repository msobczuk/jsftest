create or replace TRIGGER "PPADM"."PPG_WNR_USUN_ZALACZNIKI"
  BEFORE DELETE ON "PPADM"."PPT_SZK_WN_REFUNDACJE"
  REFERENCING FOR EACH ROW

  DECLARE
	v_wnr_id NUMBER(10,0);
BEGIN
	v_wnr_id := :old.wnr_id;
	DELETE FROM zkt_zalaczniki
	WHERE ZAL_WNREF_ID = v_wnr_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_wnr_usun_zalaczniki');
END;
/
