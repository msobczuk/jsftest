CREATE OR REPLACE PROCEDURE PPADM.wstaw_wiadomosc 
(
  PRC_ID IN NUMBER 
, TRESC IN VARCHAR2 
, DATA_WYSLANIA IN DATE default sysdate
)
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/procedures/eg.ppp_1_WSTAW_WIADOMOSC.prc 1 1.0.1 19.03.18 09:12 DZIOBEK                               $
-- $Revision:: 1                                                                                                                                              $
-- $Workfile:: eg.ppp_1_WSTAW_WIADOMOSC.prc                                                                                                                   $
-- $Modtime:: 19.03.18 09:12                                                                                                                                  $
-- $Author:: DZIOBEK                                                                                                                                          $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
   PRAGMA AUTONOMOUS_TRANSACTION;
   x_uzt VARCHAR2(50);
   err_code VARCHAR2(50);
   err_msg VARCHAR2(300);
   is_found_rec boolean := false;    

   cursor c1 IS
       SELECT uzt_nazwa 
        FROM eat_uzytkownicy where uzt_prc_id = PRC_ID;
        
   cursor c2 IS
       SELECT DDF_UZT_NAZWA 
        FROM eat_dostepy_do_firm where DDF_PRC_ID = PRC_ID and ddf_frm_id = 1;
BEGIN

  IF(PRC_ID is not null) THEN
	IF(TRESC is not null) THEN
		
		FOR uzytkownik in c1
		LOOP
			is_found_rec := true;
		
			--DBMS_OUTPUT.PUT_LINE('INSERT');
			--DBMS_OUTPUT.PUT_LINE(EAADM.EAS_WDM.nextval||'  '||uzytkownik.uzt_nazwa||'  '||DATA_WYSLANIA||'  '||TRESC);
			--insert into EAT_WIADOMOSCI (WDM_ID, WDM_FRM_ID, WDM_UZT_NAZWA, WDM_DATA_WSTAWIENIA, WDM_F_PRZECZYTANA, WDM_TYP, WDM_DATA_WYSLANIA, WDM_TRESC)
			--	values					  (EAADM.EAS_WDM.nextval, 1, uzytkownik.uzt_nazwa, sysdate, 'N', 'E', DATA_WYSLANIA, TRESC);
      WSTAW_WIADOMOSC_UZT(uzytkownik.uzt_nazwa, TRESC, DATA_WYSLANIA);
		END LOOP;
		
		--IF(not is_found_rec) THEN
			FOR uzytkownik in c2
			LOOP
				is_found_rec := true;
				
				--DBMS_OUTPUT.PUT_LINE('INSERT');
				--DBMS_OUTPUT.PUT_LINE(EAADM.EAS_WDM.nextval||'  '||uzytkownik.DDF_UZT_NAZWA||'  '||DATA_WYSLANIA||'  '||TRESC);
				--insert into EAT_WIADOMOSCI (WDM_ID, WDM_FRM_ID, WDM_UZT_NAZWA, WDM_DATA_WSTAWIENIA, WDM_F_PRZECZYTANA, WDM_TYP, WDM_DATA_WYSLANIA, WDM_TRESC)
				--	values					  (EAADM.EAS_WDM.nextval, 1, uzytkownik.DDF_UZT_NAZWA, sysdate, 'N', 'E', DATA_WYSLANIA, TRESC);
        WSTAW_WIADOMOSC_UZT(uzytkownik.DDF_UZT_NAZWA, TRESC, DATA_WYSLANIA);

			END LOOP;
		--END IF;
		
		IF(not is_found_rec) THEN
			insert into PPT_ERR_LOG (ERR_TRESC_BLEDU) values ('Nie znaleziono pracownika o prc_id - ' || PRC_ID || ' w tabeli EAT_UZYTKOWNICY i EAT_DOSTEPY_DO_FIRM');
		END IF;
		
		commit;
	END IF;

END IF;
EXCEPTION
  WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 200);
      
  DBMS_OUTPUT.PUT_LINE(err_code);
  DBMS_OUTPUT.PUT_LINE(err_msg);

      INSERT INTO PPT_ERR_LOG (ERR_TRESC_BLEDU)
      VALUES (err_code || ' - ' || err_msg);
      rollback;
END wstaw_wiadomosc;
/



