create or replace FUNCTION "GET_PRC_STJO_ID" (p_prc_id NUMBER, p_na_dzien DATE DEFAULT SYSDATE) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
 v_stjo_id NUMBER;

BEGIN

    select stjo_id
      into v_stjo_id
      from EK_ZATRUDNIENIE zat,
           EK_OBIEKTY_W_PRZEDSIEB ob,
           ZPT_ST_W_JO stjo
     where zat.zat_prc_id = p_prc_id
       and trunc(p_na_dzien) BETWEEN zat_data_zmiany AND nvl(zat_data_do, trunc(SYSDATE+(365*1000)))
       and ob.ob_id = zat.zat_ob_id
       and stjo.stjo_ob_id = zat.zat_ob_id
       and stjo.stjo_stn_id = zat.zat_stn_id;

    return v_stjo_id;

EXCEPTION
	WHEN OTHERS THEN
		return null;

END;
/
