create or replace FUNCTION PPADM.DYREKTOR_DEPARTAMENTU_ZF(P_SK_ID NUMBER, P_DATA DATE default sysdate) 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RETURN NUMBER AS
 v_ob_id_dep NUMBER;
BEGIN

    select
        ob_id 
    into v_ob_id_dep
    from EGADM1.wdrv_css_stanowiska_kosztow sk 
       left join css_obiekty_w_przedsieb ob on sk.DEP = ob.ob_skrot 
            and ob_typ_jednostki = 'D' and ob_stan_definicji = 'Z' and ob_ob_id = 1
    where sk_id = P_SK_ID
        and rownum=1;

    return DYREKTOR_DEPARTAMENTU (v_ob_id_dep, P_DATA) ;

  EXCEPTION
	WHEN OTHERS THEN
		return null;
END;
/