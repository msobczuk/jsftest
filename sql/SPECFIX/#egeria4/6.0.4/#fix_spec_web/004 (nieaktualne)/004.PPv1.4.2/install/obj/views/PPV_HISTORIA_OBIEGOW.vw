CREATE OR REPLACE VIEW PPADM.PPV_HISTORIA_OBIEGOW
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
AS 
SELECT 
      HIO_ID,
      HIO.HIO_KLUCZ_OBCY_ID,
      HIO.HIO_DATA,
      HIO.HIO_UZT_NAZWA,
      HIO.HIO_OPIS,
      HIO.HIO_KOB_KOD, 
      HIO.HIO_OBG_ID,
      HIO.HIO_DSTN_ID,
      HIO.HIO_TYP,
      COB.COB_ID,
      COB.COB_NAZWA,
	  COB.COB_NAZWA2,
      DSTN_DO.DSTN_ID, 
      DSTN_DO.DSTN_NAZWA,      
      DSTN_Z.DSTN_OPIS,      
      DSTN_Z.DSTN_ID as DSTN_ID_Z, 
      DSTN_Z.DSTN_NAZWA as DSTN_NAZWA_Z,
      DSTN_Z.DSTN_OPIS as DSTN_OPIS_Z,
      PRC.PRC_NUMER,
      PRC.PRC_IMIE,
      prc.PRC_NAZWISKO,
      zat.zat_id, 
      zat.zat_ob_id, 
      stn.stn_id, 
      stn.stn_nazwa, 
      obdep.ob_id, 
      obdep.ob_nazwa, 
      obdep.ob_kod, 
      obdep.ob_pelny_kod, 
      obdep.dep_id, 
      obdep.dep_nazwa, 
      obdep.dep_kod, 
      obdep.dep_pelny_kod
FROM
    CSST_HISTORIA_OBIEGOW hio 
	
	--    join 
	--    ( 
	--      select -- jesli czynnosc powtarzano (bo byly korekty) - tylko ta ostatnio wykonana
	--        max(hio_id) max_hio_id --, hio_kob_kod, hio_klucz_obcy_id, HIO_COB_ID 
	--      from CSST_HISTORIA_OBIEGOW 
	--      group by  hio_kob_kod, hio_klucz_obcy_id, HIO_COB_ID
	--    ) lasthio
	--    on lasthio.max_hio_id = hio.hio_id

    left join EAADM.EAT_UZYTKOWNICY UZT on UZT.UZT_NAZWA = HIO.HIO_UZT_NAZWA
    left join EAADM.EAT_DOSTEPY_DO_FIRM DDF on DDF.DDF_FRM_ID=EAADM.EAP_GLOBALS.ODCZYTAJ_FIRME and DDF.DDF_UZT_NAZWA=HIO.HIO_UZT_NAZWA
    left join CSST_CZYNNOSCI_OBIEGOW COB on HIO.HIO_COB_ID = COB.COB_ID
    left join EGADM1.CSST_DEF_STANOW dstn_z on dstn_z.dstn_id = cob.cob_swo_dstn_id_z
    left join EGADM1.CSST_DEF_STANOW dstn_do on dstn_do.dstn_id = cob.cob_swo_dstn_id_do
    left join EGADM1.EK_PRACOWNICY PRC on DDF.DDF_PRC_ID=PRC.PRC_ID
    left join EGADM1.EK_ZATRUDNIENIE zat on zat_typ_umowy = 0 and prc.prc_id = zat.zat_prc_id and hio.hio_data BETWEEN zat_data_zmiany AND nvl(zat_data_do, trunc(SYSDATE+(365*1000))) 
    left join EGADM1.ZPT_STANOWISKA stn on zat.zat_stn_id = stn.stn_id
    left join PPV_OB_DEPARTAMENTY obdep on obdep.ob_id = zat.zat_ob_id
    
ORDER BY 
	HIO.HIO_OBG_ID, HIO.HIO_KLUCZ_OBCY_ID, hio.HIO_ID DESC
/