REM G��wny skrypt instalacyjny

@@egeria_parametry.sql;

SPOOL &pi_spoolpath;



PROMPT proba polaczenia jako &pi_appowner;
CONNECT &pi_appowner/&pi_apppwd@&pi_db
SET SERVEROUTPUT ON SIZE 999999
SET FEEDBACK OFF
SET SCAN ON
SET VERIFY ON;

PROMPT ----------------------------------------------------------------------;
PROMPT -- &pi_opis                                                         --;
PROMPT ----------------------------------------------------------------------;
PROMPT
PROMPT ----------------------------------------------------------------------;
PROMPT -- Parametry upgrade                                                --;
PROMPT ----------------------------------------------------------------------;

PROMPT _SQLPLUS_RELEASE = &_SQLPLUS_RELEASE;
PROMPT _EDITOR          = &_EDITOR         ;
PROMPT PI_OSOBA         = &PI_OSOBA        ;
PROMPT PI_WERSJA        = &PI_WERSJA       ;
PROMPT PI_POPRAWKA      = &PI_POPRAWKA     ;
PROMPT PI_OPIS          = &PI_OPIS         ;
PROMPT PI_DB            = &PI_DB           ;
PROMPT PI_APPOWNER      = &PI_APPOWNER     ;
PROMPT PI_INSTANCJA     = &PI_INSTANCJA    ;
PROMPT PI_EAOWNER       = &PI_EAOWNER      ;
PROMPT PI_SPOOLPATH     = &PI_SPOOLPATH    ;
PROMPT PI_ZATRZYMAJ     = &PI_ZATRZYMAJ    ;
PROMPT _O_VERSION       = &_O_VERSION      ;
PROMPT _O_RELEASE       = &_O_RELEASE      ;

PROMPT ----------------------------------------------------------------------;

PROMPT ----------------------------------------------------------------------;
PROMPT -- Uaktualnienie obiekt�w                                           --;
PROMPT ----------------------------------------------------------------------;
PROMPT
PROMPT -------------------------- Przed instalacj� --------------------------;

PROMPT -------------------------- Tabele ------------------------------------;
prompt obj\eg.095.tab
@@obj\eg.095.tab
prompt obj\eg.095.tab-komentarze.tab
@@obj\eg.095.tab-komentarze.tab

PROMPT -------------------------- Klucze g��wne -----------------------------;
prompt obj\eg.095.pk
@@obj\eg.095.pk

PROMPT -------------------------- Klucze obce -------------------------------;
prompt obj\eg.095.con
@@obj\eg.095.con

PROMPT -------------------------- Specyfikacje pakiet�w ---------------------;
SET SCAN OFF

SET SCAN ON
PROMPT -------------------------- Indeksy -----------------------------------;

PROMPT -------------------------- Wielofirmowo�� ----------------------------;

PROMPT -------------------------- Widoki ------------------------------------;
prompt obj\views\ppv_powiadomienia.vw
@@obj\views\ppv_powiadomienia.vw

PROMPT -------------------------- Sekwencje ---------------------------------;

PROMPT -------------------------- Procedury ---------------------------------;
SET SCAN OFF

SET SCAN ON
PROMPT -------------------------- Funkcje -----------------------------------;
SET SCAN OFF

SET SCAN ON
PROMPT -------------------------- Granty ------------------------------------;
prompt obj\eg.095.rgr
@@obj\eg.095.rgr

PROMPT -------------------------- Synonimy ----------------------------------;
prompt obj\eg.095.syn
@@obj\eg.095.syn

PROMPT -------------------------- Cia�a pakiet�w ----------------------------;
SET SCAN OFF

SET SCAN ON
PROMPT -------------------------- Wyzwalacze --------------------------------;
prompt obj\triggers\PPG_ANSP_BIUR_AUDYT.trg
@@obj\triggers\PPG_ANSP_BIUR_AUDYT.trg
prompt obj\triggers\PPG_IPRZ_BIUR_AUDYT.trg
@@obj\triggers\PPG_IPRZ_BIUR_AUDYT.trg
prompt obj\triggers\PPG_KOM_BIUR_AUDYT.trg
@@obj\triggers\PPG_KOM_BIUR_AUDYT.trg
prompt obj\triggers\PPG_KRYT_BIUR_AUDYT.trg
@@obj\triggers\PPG_KRYT_BIUR_AUDYT.trg
prompt obj\triggers\PPG_NAB_BIUR_AUDYT.trg
@@obj\triggers\PPG_NAB_BIUR_AUDYT.trg
prompt obj\triggers\PPG_NSK_BIUR_AUDYT.trg
@@obj\triggers\PPG_NSK_BIUR_AUDYT.trg
prompt obj\triggers\PPG_NSM_BIUR_AUDYT.trg
@@obj\triggers\PPG_NSM_BIUR_AUDYT.trg
prompt obj\triggers\PPG_NWN_BIUR_AUDYT.trg
@@obj\triggers\PPG_NWN_BIUR_AUDYT.trg
prompt obj\triggers\PPG_NZE_BIUR_AUDYT.trg
@@obj\triggers\PPG_NZE_BIUR_AUDYT.trg
prompt obj\triggers\PPG_OF_BIUR_AUDYT.trg
@@obj\triggers\PPG_OF_BIUR_AUDYT.trg
prompt obj\triggers\PPG_PIS_BIUR_AUDYT.trg
@@obj\triggers\PPG_PIS_BIUR_AUDYT.trg
prompt obj\triggers\PPG_PKS_BIUR_AUDYT.trg
@@obj\triggers\PPG_PKS_BIUR_AUDYT.trg
prompt obj\triggers\PPG_PKZF_BIUR_AUDYT.trg
@@obj\triggers\PPG_PKZF_BIUR_AUDYT.trg
prompt obj\triggers\PPG_PROJ_BIUR_AUDYT.trg
@@obj\triggers\PPG_PROJ_BIUR_AUDYT.trg
prompt obj\triggers\PPG_SKST_BIUR_AUDYT.trg
@@obj\triggers\PPG_SKST_BIUR_AUDYT.trg
prompt obj\triggers\PPG_SLPB_BIUR_AUDYT.trg
@@obj\triggers\PPG_SLPB_BIUR_AUDYT.trg
prompt obj\triggers\PPG_SLPR_BIUR_AUDYT.trg
@@obj\triggers\PPG_SLPR_BIUR_AUDYT.trg
prompt obj\triggers\PPG_SWYK_BIUR_AUDYT.trg
@@obj\triggers\PPG_SWYK_BIUR_AUDYT.trg
prompt obj\triggers\PPG_SZK_BIUR_AUDYT.trg
@@obj\triggers\PPG_SZK_BIUR_AUDYT.trg
prompt obj\triggers\PPG_UMSZ_BIUR_AUDYT.trg
@@obj\triggers\PPG_UMSZ_BIUR_AUDYT.trg
prompt obj\triggers\PPG_UPRW_BIUR_AUDYT.trg
@@obj\triggers\PPG_UPRW_BIUR_AUDYT.trg
prompt obj\triggers\PPG_WNO_BIUR_AUDYT.trg
@@obj\triggers\PPG_WNO_BIUR_AUDYT.trg
prompt obj\triggers\PPG_ZAPR_BIUR_AUDYT.trg
@@obj\triggers\PPG_ZAPR_BIUR_AUDYT.trg
prompt obj\triggers\PPG_ZSZ_BIUR_AUDYT.trg
@@obj\triggers\PPG_ZSZ_BIUR_AUDYT.trg

PROMPT -------------------------- Kompilacja obiekt�w -----------------------;
@@_compile_all.utl
PROMPT -------------------------- Po instalacji -----------------------------;

PROMPT -------------------------- Warto�ci domy�lne -------------------------;

PROMPT -------------------------- Warto�ci s�ownik�w ------------------------;
prompt dmp\eg.095.sl
@@dmp\eg.095.sl


PROMPT ----------------------------------------------------------------------;
PROMPT -- Rejestracja instalacji w EA                                      --;
PROMPT -- + aktualizacja informacji o wersji aplikacji                     --;
PROMPT ----------------------------------------------------------------------;

DEFINE p_lista_app = "'PP'";
@egeria_journal



PROMPT ----------------------------------------------------------------------;
PROMPT -- KONIEC instalacji poprawek w bazie.                              --;
PROMPT -- Utworzono plik spool: &pi_spoolpath                              --;
PROMPT ----------------------------------------------------------------------;

COMMIT;
SPOOL OFF
