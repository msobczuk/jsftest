ACCEPT pi_osoba     = '';
DEFINE pi_wersja    = '6.0.3';
DEFINE pi_poprawka  = '004.095_PP';
DEFINE pi_opis      = '[ZGL RD1110000] - Instalacja nowej wersji schematu PP ';

DEFINE pi_db        = neo10;
DEFINE pi_syspwd    = aaa;
DEFINE pi_systempwd = aaa;
DEFINE pi_appowner  = egadm1;
DEFINE pi_apppwd    = aaa1;
DEFINE pi_instancja = 1;
DEFINE pi_eaowner   = eaadm;
DEFINE pi_eapwd     = aaa;
DEFINE pi_frm_id    = 1;
DEFINE pi_spoolpath = ..\FRM&pi_frm_id..&pi_wersja..&pi_poprawka..lst;
