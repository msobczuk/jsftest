update PPT_ADM_SQLS set PPSL_QUERY=
'SELECT 
distinct 
sdn.SDN_NUMER_INW pp_numer_inw,
sdn.SDN_NAZWA pp_nazwa_srodka,
sdn.SDN_TYP pp_typ_srodka,
decode(sdn.SDN_F_AMORTYZOWANY,''T'', ''TAK'', ''N'', ''NIE'') pp_amortyzowany,
sdn.SDN_KOD_KRESKOWY 
pp_kod_kreskowy,
sdn.SDN_CHARAKTERYSTYKA pp_charakterystyka,
sdn.SDN_DATA_NABYCIA pp_data_nabycia,
sps.PS_PRC_ID pp_prc_id,
sps.PS_SK_ID pp_sk_id,
sps.PS_PROCENT pp_ps_procent,
skk.KL_NAZWA_RODZAJOWA pp_kl_nazwa,
smu.MU_NAZWA  pp_lokalizacja,
prc.PRC_NUMER pp_prc_numer,
concat(prc.prc_imie, concat('' '', prc.prc_nazwisko )) pp_prc_nazwa,
csk.sk_kod pp_sk_kod,
csk.SK_OPIS pp_sk_nazwa,
prc.PRC_NUMER

from STV_SRODKI_DANE sdn, STT_PODZIALY_SRODKA sps, STT_KLASY_KST skk, STT_MIEJSCA_UZYWANIA smu, EK_PRACOWNICY prc, CSS_STANOWISKA_KOSZTOW csk

where 
sps.PS_SDN_ID = sdn.SDN_ID
and sdn.KL_ID = skk.KL_ID
--and sps.PS_MU_ID = smu.MU_ID
and sps.PS_PRC_ID = prc.PRC_ID
and sps.PS_SK_ID = csk.SK_ID
and prc.PRC_ID= ?'
where PPSL_ID='sqlPracownikSrodkiTrwale';