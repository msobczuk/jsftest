PROMPT ----------------------------------------------------------------------;
PROMPT 002.036

SET DEFINE OFF;
Insert into EGADM1.PPT_ADM_SQLS
   (PPSL_ID, PPSL_QUERY, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE, PPSL_AUDYT_UT, PPSL_AUDYT_DT)
 Values
   ('sqlid', 'select * from dual', 1800000, 'testowy słownik/zapytanie..............', 1, '-', TO_DATE('11/28/2016 17:42:56', 'MM/DD/YYYY HH24:MI:SS'));
Insert into EGADM1.PPT_ADM_SQLS
   (PPSL_ID, PPSL_QUERY, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE, PPSL_AUDYT_UT, PPSL_AUDYT_DT)
 Values
   ('pracownicy', '
 select 
    prc_id, 
    prc_imie, 
    prc_nazwisko, 
    PRC_AKTYWNY, 
    prc_data_ur 
 from  egadm1.ek_pracownicy
 where rownum <=500
   or prc_imie = ''Gumbert''
 order by prc_nazwisko, prc_imie  
', 1800000, 'lista pracowników', 0, '-', TO_DATE('11/28/2016 17:42:56', 'MM/DD/YYYY HH24:MI:SS'));
Insert into EGADM1.PPT_ADM_SQLS
   (PPSL_ID, PPSL_QUERY, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE, PPSL_AUDYT_UT, PPSL_AUDYT_DT)
 Values
   ('sqlKsTel', 'SELECT * 
FROM PPV_KSIAZKA_TELEFONICZNA_PRAC 
-- filtr, zeby szybciej ladowac na srodowisku. dev
where not KKON_KRAJ_ID is null
or  PP_IMIE = ''Gumbert''
or rownum <= 200
', 1800000, 'Ksiazka telefoniczna', 0, '-', TO_DATE('11/28/2016 17:42:56', 'MM/DD/YYYY HH24:MI:SS'));
Insert into EGADM1.PPT_ADM_SQLS
   (PPSL_ID, PPSL_QUERY, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE, PPSL_AUDYT_UT, PPSL_AUDYT_DT)
 Values
   ('sqlDzialy', 'SELECT 
      OB_ID        PP_DZL_ID 
    , OB_KOD     PP_DZL_KOD
    , OB_NAZWA    PP_DZL_NAZWA
FROM EGADM1.EK_OBIEKTY_W_PRZEDSIEB', 1800000, 'Dzialy / wydzialy', 0, '-', TO_DATE('11/28/2016 17:42:56', 'MM/DD/YYYY HH24:MI:SS'));
COMMIT;





SET DEFINE OFF;
Insert into EGADM1.PPT_JPA_GENKEYS
   (PKEY_TABLE, PKEY_VALUE, PKEY_AUDYT_UT, PKEY_AUDYT_DT, PKEY_AUDYT_UM, PKEY_AUDYT_DM, PKEY_AUDYT_LM, PKEY_AUDYT_KM)
 Values
   ('PPT_URLOPY_PLANOWANE', 35, '-', TO_DATE('11/28/2016 15:48:45', 'MM/DD/YYYY HH24:MI:SS'), 'EGADM1', TO_DATE('12/08/2016 15:25:10', 'MM/DD/YYYY HH24:MI:SS'), '4', 'PCLCELUCH\Administrator');
Insert into EGADM1.PPT_JPA_GENKEYS
   (PKEY_TABLE, PKEY_VALUE, PKEY_AUDYT_UT, PKEY_AUDYT_DT, PKEY_AUDYT_UM, PKEY_AUDYT_DM, PKEY_AUDYT_LM, PKEY_AUDYT_KM)
 Values
   ('PPT_URLOPY_PLANOWANE_DNI', 131, '-', TO_DATE('11/28/2016 15:48:45', 'MM/DD/YYYY HH24:MI:SS'), 'EGADM1', TO_DATE('12/08/2016 15:25:10', 'MM/DD/YYYY HH24:MI:SS'), '18', 'PCLCELUCH\Administrator');
Insert into EGADM1.PPT_JPA_GENKEYS
   (PKEY_TABLE, PKEY_VALUE, PKEY_AUDYT_UT, PKEY_AUDYT_DT, PKEY_AUDYT_LM)
 Values
   ('PPT_PPZD_ZDJECIA', 1, 'EGADM1', TO_DATE('12/05/2016 13:45:08', 'MM/DD/YYYY HH24:MI:SS'), '1');
COMMIT;


PROMPT ----------------------------------------------------------------------;
