REM G��wny skrypt instalacyjny

@@egeria_parametry.sql;

SPOOL &pi_spoolpath;

@@ea_install.sql

PROMPT proba polaczenia jako &pi_appowner;
CONNECT &pi_appowner/&pi_apppwd@&pi_db
SET SERVEROUTPUT ON SIZE 999999
SET FEEDBACK OFF
SET SCAN ON
SET VERIFY ON;

PROMPT ----------------------------------------------------------------------;
PROMPT -- &pi_opis                                                         --;
PROMPT ----------------------------------------------------------------------;
PROMPT
PROMPT ----------------------------------------------------------------------;
PROMPT -- Parametry upgrade                                                --;
PROMPT ----------------------------------------------------------------------;

PROMPT _SQLPLUS_RELEASE = &_SQLPLUS_RELEASE;
PROMPT _EDITOR          = &_EDITOR         ;
PROMPT PI_OSOBA         = &PI_OSOBA        ;
PROMPT PI_WERSJA        = &PI_WERSJA       ;
PROMPT PI_POPRAWKA      = &PI_POPRAWKA     ;
PROMPT PI_OPIS          = &PI_OPIS         ;
PROMPT PI_DB            = &PI_DB           ;
PROMPT PI_APPOWNER      = &PI_APPOWNER     ;
PROMPT PI_INSTANCJA     = &PI_INSTANCJA    ;
PROMPT PI_EAOWNER       = &PI_EAOWNER      ;
PROMPT PI_SPOOLPATH     = &PI_SPOOLPATH    ;
PROMPT PI_ZATRZYMAJ     = &PI_ZATRZYMAJ    ;
PROMPT _O_VERSION       = &_O_VERSION      ;
PROMPT _O_RELEASE       = &_O_RELEASE      ;

PROMPT ----------------------------------------------------------------------;

PROMPT ----------------------------------------------------------------------;
PROMPT -- Uaktualnienie obiekt�w                                           --;
PROMPT ----------------------------------------------------------------------;
PROMPT
PROMPT -------------------------- Przed instalacj� --------------------------;

PROMPT -------------------------- Tabele ------------------------------------;
prompt obj\eg.000.tab
@@obj\eg.000.tab
prompt obj\eg.000.tab-komentarze.tab
@@obj\eg.000.tab-komentarze.tab

PROMPT -------------------------- Klucze g��wne -----------------------------;
prompt obj\eg.000.pk
@@obj\eg.000.pk

PROMPT -------------------------- Klucze obce -------------------------------;
prompt obj\eg.000.con
@@obj\eg.000.con

PROMPT -------------------------- Specyfikacje pakiet�w ---------------------;
SET SCAN OFF

SET SCAN ON
PROMPT -------------------------- Indeksy -----------------------------------;
prompt obj\eg.000.ind
@@obj\eg.000.ind

PROMPT -------------------------- Wielofirmowo�� ----------------------------;

PROMPT -------------------------- Widoki ------------------------------------;
prompt obj\views\ppv_konta_lista.vw
@@obj\views\ppv_konta_lista.vw
prompt obj\views\ppv_konta_szczegoly.vw
@@obj\views\ppv_konta_szczegoly.vw
prompt obj\views\ppv_ksiazka_telefoniczna_prac.vw
@@obj\views\ppv_ksiazka_telefoniczna_prac.vw
prompt obj\views\ppv_lista_plac_skladniki.vw
@@obj\views\ppv_lista_plac_skladniki.vw
prompt obj\views\ppv_powiadomienia.vw
@@obj\views\ppv_powiadomienia.vw
prompt obj\views\ppv_zat_inneumowy_lista.vw
@@obj\views\ppv_zat_inneumowy_lista.vw
prompt obj\views\ppv_zat_premie.vw
@@obj\views\ppv_zat_premie.vw
prompt obj\views\ppv_zat_umowa_lista.vw
@@obj\views\ppv_zat_umowa_lista.vw

PROMPT -------------------------- Sekwencje ---------------------------------;

PROMPT -------------------------- Procedury ---------------------------------;
SET SCAN OFF

SET SCAN ON
PROMPT -------------------------- Funkcje -----------------------------------;
SET SCAN OFF

SET SCAN ON
PROMPT -------------------------- Granty ------------------------------------;
prompt obj\eg.000.rgr
@@obj\eg.000.rgr

PROMPT -------------------------- Synonimy ----------------------------------;
prompt obj\eg.000.syn
@@obj\eg.000.syn

PROMPT -------------------------- Cia�a pakiet�w ----------------------------;
SET SCAN OFF

SET SCAN ON
PROMPT -------------------------- Wyzwalacze --------------------------------;
prompt obj\triggers\PPG_KKON_BIUR_AUDYT.trg
@@obj\triggers\PPG_KKON_BIUR_AUDYT.trg
prompt obj\triggers\PPG_KLOK_BIUR_AUDYT.trg
@@obj\triggers\PPG_KLOK_BIUR_AUDYT.trg
prompt obj\triggers\PPG_PKEY_BIUR_AUDYT.trg
@@obj\triggers\PPG_PKEY_BIUR_AUDYT.trg
prompt obj\triggers\PPG_PPSL_BIUR_AUDYT.trg
@@obj\triggers\PPG_PPSL_BIUR_AUDYT.trg
prompt obj\triggers\PPG_URPD_BIUR_AUDYT.trg
@@obj\triggers\PPG_URPD_BIUR_AUDYT.trg
prompt obj\triggers\PPG_URPL_BIUR_AUDYT.trg
@@obj\triggers\PPG_URPL_BIUR_AUDYT.trg

PROMPT -------------------------- Kompilacja obiekt�w -----------------------;
@@_compile_all.utl
PROMPT -------------------------- Po instalacji -----------------------------;

PROMPT -------------------------- Warto�ci domy�lne -------------------------;

PROMPT -------------------------- Warto�ci s�ownik�w ------------------------;
prompt dmp\eg.000.sl
@@dmp\eg.000.sl


PROMPT ----------------------------------------------------------------------;
PROMPT -- Rejestracja instalacji w EA                                      --;
PROMPT -- + aktualizacja informacji o wersji aplikacji                     --;
PROMPT ----------------------------------------------------------------------;

DEFINE p_lista_app = "'EA','PP'";
@egeria_journal



PROMPT ----------------------------------------------------------------------;
PROMPT -- KONIEC instalacji poprawek w bazie.                              --;
PROMPT -- Utworzono plik spool: &pi_spoolpath                              --;
PROMPT ----------------------------------------------------------------------;

COMMIT;
SPOOL OFF
EXIT;
 
EXIT;
 
