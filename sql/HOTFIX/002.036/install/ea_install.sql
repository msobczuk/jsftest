PROMPT proba polaczenia jako &pi_eaowner;
CONNECT &pi_eaowner/&pi_eapwd@&pi_db
SET SERVEROUTPUT ON SIZE 999999
SET FEEDBACK OFF
SET SCAN ON
SET VERIFY ON;

PROMPT ----------------------------------------------------------------------;
PROMPT -- &pi_opis                                                         --;
PROMPT ----------------------------------------------------------------------;
PROMPT
PROMPT ----------------------------------------------------------------------;
PROMPT -- Parametry upgrade                                                --;
PROMPT ----------------------------------------------------------------------;

PROMPT _SQLPLUS_RELEASE = &_SQLPLUS_RELEASE;
PROMPT _EDITOR          = &_EDITOR         ;
PROMPT PI_OSOBA         = &PI_OSOBA        ;
PROMPT PI_WERSJA        = &PI_WERSJA       ;
PROMPT PI_POPRAWKA      = &PI_POPRAWKA     ;
PROMPT PI_OPIS          = &PI_OPIS         ;
PROMPT PI_DB            = &PI_DB           ;
PROMPT PI_APPOWNER      = &PI_APPOWNER     ;
PROMPT PI_INSTANCJA     = &PI_INSTANCJA    ;
PROMPT PI_EAOWNER       = &PI_EAOWNER      ;
PROMPT PI_SPOOLPATH     = &PI_SPOOLPATH    ;
PROMPT PI_ZATRZYMAJ     = &PI_ZATRZYMAJ    ;
PROMPT _O_VERSION       = &_O_VERSION      ;
PROMPT _O_RELEASE       = &_O_RELEASE      ;

PROMPT
PROMPT ----------------------------------------------------------------------;

PROMPT ----------------------------------------------------------------------;
PROMPT -- Uaktualnienie obiekt�w &pi_eaowner                                    --;
PROMPT ----------------------------------------------------------------------;
PROMPT
PROMPT -------------------------- Przed instalacj� --------------------------;

PROMPT -------------------------- Tablespace --------------------------------;

PROMPT -------------------------- Tabele ------------------------------------;

PROMPT -------------------------- Klucze g��wne -----------------------------;

PROMPT -------------------------- Klucze obce -------------------------------;

PROMPT -------------------------- Specyfikacje pakiet�w ---------------------;
SET SCAN OFF

SET SCAN ON
PROMPT -------------------------- Indeksy -----------------------------------;

PROMPT -------------------------- Widoki ------------------------------------;

PROMPT -------------------------- Sekwencje ---------------------------------;

PROMPT -------------------------- Procedury ---------------------------------;
SET SCAN OFF

SET SCAN ON
PROMPT -------------------------- Funkcje -----------------------------------;
SET SCAN OFF

SET SCAN ON
PROMPT -------------------------- Granty ------------------------------------;

PROMPT -------------------------- Synonimy ----------------------------------;

PROMPT -------------------------- Cia�a pakiet�w ----------------------------;
SET SCAN OFF

SET SCAN ON
PROMPT -------------------------- Wyzwalacze --------------------------------;

PROMPT -------------------------- Kompilacja obiekt�w -----------------------;
@@_compile_all.utl
PROMPT -------------------------- Po instalacji -----------------------------;

PROMPT -------------------------- Warto�ci domy�lne -------------------------;

PROMPT -------------------------- Warto�ci s�ownik�w ------------------------;
prompt dmp\ea.000.sl
@@dmp\ea.000.sl


PROMPT ----------------------------------------------------------------------;
PROMPT -- Zakoczono uaktualnienie obiekt�w &pi_eaowner                          --;
PROMPT ----------------------------------------------------------------------;
