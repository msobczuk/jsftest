CREATE OR REPLACE FORCE VIEW PPV_ZAT_UMOWA_LISTA
(
   PP_Z_PRC_ID,
   PP_Z_ID,
   PP_Z_UMOWA_AKT,
   PP_Z_DATAPRZYJ,
   PP_Z_DATAZMIANY,
   PP_Z_DATADO,
   PP_Z_STAWKA,
   PP_Z_TYP_UMOWY,
   PP_Z_UMOWA,
   PP_Z_TYP_ANAGAZ,
   PP_Z_ANGAZ,
   PP_Z_F_RODZAJ,
   PP_Z_RODZAJ,
   PP_Z_WYMIAR_KOD,
   PP_Z_WYMIAR,
   PP_Z_STANOWISKO,
   PP_Z_FUNKCJA,
   PP_Z_ZASZEREGOWANIE,
   PP_Z_PRZELOZONY,
   PP_Z_JED_ORG,
   PP_Z_WAR_SZCZEG
)
AS
     SELECT   
		   	  -- $Revision:: 1       $--
			  ZAT_PRC_ID PP_Z_PRC_ID,
              ZAT_ID pp_z_id,
              CASE
                 WHEN (NVL (zat_data_do, SYSDATE) >= SYSDATE)
                      AND (zat_data_zmiany <= LAST_DAY (SYSDATE))
                 THEN
                    'T'
                 ELSE
                    'N'
              END
                 PP_Z_UMOWA_AKT,
              zat_data_przyj pp_z_dataprzyj,
              zat_data_zmiany pp_z_datazmiany,
              zat_data_do pp_z_datado,
              zat_stawka pp_z_stawka,
              ZAT_TYP_UMOWY pp_z_typ_umowy,
              (SELECT   rv_meaning
                 FROM   ek_ref_codes
                WHERE   rv_domain = 'TYP_UMOWY'
                        AND rv_low_value = zat_typ_umowy)
                 pp_z_umowa,
              ek_z.ZAT_TYP_ANGAZ pp_z_typ_anagaz,
              (SELECT   rv_meaning
                 FROM   ek_ref_codes
                WHERE   rv_domain = 'TYP_ANGAZU'
                        AND rv_low_value = ek_z.ZAT_TYP_ANGAZ)
                 pp_z_angaz,
              ek_z.zat_f_rodzaj pp_z_f_rodzaj,
              (SELECT   rv_meaning
                 FROM   ek_ref_codes
                WHERE   rv_domain = 'EK_RODZAJ_ZAT'
                        AND rv_low_value = zat_f_rodzaj)
                 pp_z_rodzaj,
              ek_z.zat_wymiar pp_z_wymiar_kod,
              (SELECT   rv_meaning
                 FROM   ek_ref_codes
                WHERE   rv_domain = 'TYP ETATU' AND rv_low_value = zat_wymiar)
                 pp_z_wymiar,
              (SELECT   stn_nazwa
                 FROM   zpt_stanowiska
                WHERE   stn_id = zat_stn_id)
                 pp_z_stanowisko,
              (SELECT   fnk_nazwa
                 FROM   ek_funkcje
                WHERE   fnk_id = zat_fnk_id)
                 pp_z_funkcja,
              (SELECT   kz_numer
                 FROM   ek_kategorie_zaszeregowania
                WHERE   kz_id = zat_kz_id)
                 PP_Z_ZASZEREGOWANIE,
              NVL (TRIM (ek_szef.PRC_IMIE || ' ' || ek_szef.PRC_NAZWISKO),
                   'Brak danych')
                 PP_Z_PRZELOZONY,
              ob.OB_ID || ' - ' || ob.OB_NAZWA PP_Z_JED_ORG,
              (SELECT   RV_LOW_VALUE
                 FROM   EK_REF_CODES
                WHERE   RV_DOMAIN = 'EK_WAR_SZCZ'
                        AND rv_low_value = zat_war_szcz)
                 PP_Z_WAR_SZCZEG
       FROM   ek_zatrudnienie ek_z,
              ek_pracownicy ek_szef,
              ek_obiekty_w_przedsieb ob
      WHERE       ek_z.ZAT_PRC_ID_SZEF = ek_szef.PRC_ID(+)
              AND zat_typ_umowy = 0                      --TYLKO UMOWY O PRACE
              AND ek_z.ZAT_OB_ID = ob.OB_ID(+)
   ORDER BY   zat_data_przyj DESC, zat_data_zmiany DESC
/