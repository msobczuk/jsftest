CREATE OR REPLACE FORCE VIEW PPV_KONTA_SZCZEGOLY
(
   PP_PRC_ID,
   PP_KB_ID,
   PP_KB_LISTID,
   PP_KB_NAZLIST,
   PP_KB_KWOTA,
   PP_KB_PROC,
   PP_KB_FCALOSC
)
AS
   SELECT   
			-- $Revision:: 1       $--
			ekv_konta_pracownikow.knt_prc_id pp_prc_id,
            knd_id pp_kb_id,
            drl_kod pp_kb_listid,
            drl_nazwa pp_kb_nazlist,
            EK_KONTA_PLAC.knp_kwota pp_kb_kwota,
            EK_KONTA_PLAC.knp_procent pp_kb_proc,
            EK_KONTA_PLAC.knp_f_calosc pp_kb_fcalosc
     FROM   ekv_konta_pracownikow, ek_konta_plac, ek_def_rodzajow_list
    WHERE       EK_KONTA_PLAC.knp_id IS NOT NULL
            AND ekv_konta_pracownikow.knd_f_aktualne = 'T'
            AND EK_KONTA_PLAC.knp_knt_id(+) = knd_knt_id
            AND drl_kod(+) = knp_drl_kod
/