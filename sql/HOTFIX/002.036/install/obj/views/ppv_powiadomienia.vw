CREATE OR REPLACE FORCE VIEW PPV_POWIADOMIENIA
(
   PP_PRC_ID,
   PP_WDM_ID,
   PP_DATA_POWIADOMIENIA,
   PP_F_PRZECZYTANA,
   PP_TYP,
   PP_TEMAT,
   PP_TRESC
)
AS
   SELECT   100220 PP_PRC_ID,                       -- ZAWSZE Gumbert Majewski
            WDM_ID PP_WDM_ID,                      -- identyfikator wiadomości
            NVL (WDM_DATA_WYSLANIA, WDM_DATA_WSTAWIENIA)
               PP_DATA_POWIADOMIENIA,          -- data i godzina powiadomienia
            WDM_F_PRZECZYTANA PP_F_PRZECZYTANA, -- flaga czy powiadomienie odczytane N/T (nie/tak)
            WDM_TYP PP_TYP, -- typ powiadomienia E/A/M (Egeria/Mail/SMS) - zawsze E
            WDM_TEMAT PP_TEMAT,                         -- temat powiadomienia
            WDM_TRESC PP_TRESC                          -- treść powiadomienia
     FROM   eat_wiadomosci wdm
    WHERE   wdm_typ = 'E'
/