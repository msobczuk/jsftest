CREATE OR REPLACE FORCE VIEW PPV_KONTA_LISTA
(
   PP_PRC_ID,
   PP_KB_ID,
   PP_KB_NUMER,
   PP_KB_FIBAN,
   PP_KB_NAZBAN,
   PP_KB_IDKONTA,
   PP_KB_LICZLISTY
)
AS
     SELECT 
		-- $Revision:: 1       $--
           pracownicy.PRC_ID pp_prc_id,
              knd_id pp_kb_id,
              knd_numer pp_kb_numer,
              knd_f_iban pp_kb_fiban,
              bnk_nazwa pp_kb_nazban,
              knd_knt_id pp_kb_idkonta,
              COUNT (knp_knt_id) pp_kb_liczlisty
       FROM   ekv_konta_pracownikow,
              ek_konta_plac,
              css_banki,
              EK_PRACOWNICY pracownicy
      WHERE       knt_prc_id = pracownicy.PRC_ID
              AND knd_f_aktualne = 'T'
              AND bnk_stan_definicji = 'A'
              AND knp_knt_id(+) = knd_knt_id
              AND bnk_numer(+) = SUBSTR (knd_numer_banku, 1, 4)
   GROUP BY   knd_id,
              knd_numer,
              knd_f_iban,
              bnk_nazwa,
              knd_knt_id,
              pracownicy.PRC_ID
/

