REM W��czenie wy�wietlania wyj�cia z PL/SQL
SET SERVEROUTPUT ON SIZE 999999

REM Wy��czenie wy�wietlania liczby zwr�conych rekord�w przez zapytanie
SET FEEDBACK OFF

REM W��czenie wyszukiwania &zmiennych
SET SCAN ON

REM W��czenie wy�wietlania podstawie� do zmiennych
SET VERIFY ON

@@egeria_parametry.sql;

PROMPT proba polaczenia jako %APPOWNER%;
CONNECT %APPCONN%;

SPOOL &pi_spoolpath;

@@ea_install

PROMPT proba polaczenia jako %APPOWNER%;
CONNECT %APPCONN%;

PROMPT ----------------------------------------------------------------------;
PROMPT -- &pi_opis                                                         --;
PROMPT ----------------------------------------------------------------------;
PROMPT
PROMPT ----------------------------------------------------------------------;
PROMPT -- Parametry upgrade                                                --;
PROMPT ----------------------------------------------------------------------;

PROMPT _SQLPLUS_RELEASE = &_SQLPLUS_RELEASE;
PROMPT _EDITOR          = &_EDITOR         ;
PROMPT PI_OSOBA         = &PI_OSOBA        ;
PROMPT PI_WERSJA        = &PI_WERSJA       ;
PROMPT PI_POPRAWKA      = &PI_POPRAWKA     ;
PROMPT PI_OPIS          = &PI_OPIS         ;
PROMPT PI_DB            = &PI_DB           ;
PROMPT PI_APPOWNER      = &PI_APPOWNER     ;
PROMPT PI_INSTANCJA     = &PI_INSTANCJA    ;
PROMPT PI_EAOWNER       = &PI_EAOWNER      ;
PROMPT PI_SPOOLPATH     = &PI_SPOOLPATH    ;
PROMPT P_LISTA_APP      = &P_LISTA_APP     ;
PROMPT PI_ZATRZYMAJ     = &PI_ZATRZYMAJ    ;
PROMPT _O_VERSION       = &_O_VERSION      ;
PROMPT _O_RELEASE       = &_O_RELEASE      ;

PROMPT ----------------------------------------------------------------------;

PROMPT ----------------------------------------------------------------------;
PROMPT -- Uaktualnienie obiekt�w                                           --;
PROMPT ----------------------------------------------------------------------;
PROMPT
PROMPT -------------------------- Tabele ------------------------------------;
%TAB%
PROMPT -------------------------- Klucze g��wne -----------------------------;
%PK%
PROMPT -------------------------- Klucze obce -------------------------------;
%CON%
PROMPT -------------------------- Specyfikacje pakiet�w ---------------------;
%PCS%
PROMPT -------------------------- Indeksy -----------------------------------;
%IND%
PROMPT -------------------------- Widoki ------------------------------------;
%VW%
PROMPT -------------------------- Sekwencje ---------------------------------;
%SEQ%
PROMPT -------------------------- Granty ------------------------------------;
%RGR%
PROMPT -------------------------- Synonimy ----------------------------------;
%SYN%
PROMPT -------------------------- Procedury ---------------------------------;
%PRC%
PROMPT -------------------------- Funkcje -----------------------------------;
%FUN%
PROMPT -------------------------- Cia�a pakiet�w ----------------------------;
%PCB%
PROMPT -------------------------- Wyzwalacze --------------------------------;
%TRG%
PROMPT -------------------------- Po instalacji -----------------------------;
%POST%
PROMPT -------------------------- Warto�ci domy�lne -------------------------;
%WD%
PROMPT -------------------------- Warto�ci s�ownik�w ------------------------;
%SL%

PROMPT ----------------------------------------------------------------------;
PROMPT -- Rejestracja instalacji w EA                                      --;
PROMPT -- + aktualizacja informacji o wersji aplikacji                     --;
PROMPT ----------------------------------------------------------------------;

@egeria_journal

PROMPT ----------------------------------------------------------------------;
PROMPT -- KONIEC instalacji poprawek w bazie.                              --;
PROMPT -- Utworzono plik spool: &pi_spoolpath                              --;
PROMPT ----------------------------------------------------------------------;

COMMIT;
SPOOL OFF

