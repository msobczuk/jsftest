DECLARE
  v_type    eap_obj_instalacja.t_instalacja;
  v_rowid   ROWID;
BEGIN
  v_type.pdi_data_instalacji := SYSDATE;
  v_type.pdi_wersja := '&pi_wersja';
  IF '&pi_poprawka' IS NOT NULL AND '&pi_poprawka' <> '0' THEN
    v_type.pdi_wersja := v_type.pdi_wersja || '.&pi_poprawka';
  END IF;
  v_type.pdi_instalator := '&pi_osoba';
  v_type.pdi_opis := '&pi_opis';
  FOR a IN (SELECT apl_id, apl_ins_id 
              FROM eat_aplikacje 
             WHERE apl_symbol IN (&p_lista_app)) LOOP
    v_type.pdi_id := NULL;
    v_type.pdi_ins_id := a.apl_ins_id;
    v_type.pdi_apl_id := a.apl_id;
    v_rowid := eap_obj_instalacja.wstaw(v_type);
    UPDATE eat_aplikacje
       SET apl_wersja = '&pi_wersja'
     WHERE apl_id = a.apl_id;
  END LOOP;  
  COMMIT;
END;
/
