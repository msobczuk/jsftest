create or replace PACKAGE BODY PPP_HOTFIX AS

c_package_name constant varchar2(10):='PPP_HOTFIX';
g_hotfix_no PPT_HOTFIX_DATA.HFIX_NO%TYPE;
g_hotfix_data date;

    /*
     Wstawienie zapisu do tabeli logu migracji
    */
    procedure sm(p_nazwa_procedury VARCHAR2 default null,
                 p_komunikat VARCHAR2 DEFAULT NULL,
                 p_komunikat_rodzaj VARCHAR2 DEFAULT 'I')
    as
    PRAGMA AUTONOMOUS_TRANSACTION;
    begin
    
    INSERT INTO ppt_log
    (
     MLOG_NAZWA_PROCEDURY, 
     MLOG_KOMUNIKAT, 
     MLOG_KOMUNIKAT_RODZAJ
    ) 
    VALUES 
    (
     p_nazwa_procedury,
     p_komunikat,
     p_komunikat_rodzaj
    );
    COMMIT;
    
    exception when others then 
      null;
    end sm;

  /*
   Zarzadzanie zapisem w tabeli ze skryptami HotFix
  */
  procedure sm_hotfix(p_row PPT_HOTFIX_DATA%ROWTYPE)
  is
  c_nazwa_procedury varchar2(30):=c_package_name||'.'||'sm_hotfix';
  begin
   begin
      sm(c_nazwa_procedury,'HotFix: '||p_row.HFIX_POLECENIE_TYP||', '||p_row.HFIX_TABLE_NAME||', '||p_row.HFIX_COLUMN_PK||' - dodaj...');
      INSERT INTO PPT_HOTFIX_DATA
      (HFIX_NO, 
       HFIX_DATA, 
       HFIX_POLECENIE_TYP, 
       HFIX_TABLE_NAME, 
       HFIX_COLUMN_PK, 
       HFIX_POLECENIE_SQL)
      VALUES
      (p_row.HFIX_NO, 
       p_row.HFIX_DATA, 
       p_row.HFIX_POLECENIE_TYP, 
       p_row.HFIX_TABLE_NAME, 
       p_row.HFIX_COLUMN_PK, 
       p_row.HFIX_POLECENIE_SQL);
       sm(c_nazwa_procedury,'HotFix: '||p_row.HFIX_POLECENIE_TYP||', '||p_row.HFIX_TABLE_NAME||', '||p_row.HFIX_COLUMN_PK||' - zako�czono');
   exception
    when others then
       sm(c_nazwa_procedury,'HotFix: '||p_row.HFIX_POLECENIE_TYP||', '||p_row.HFIX_TABLE_NAME||', '||p_row.HFIX_COLUMN_PK||' - blad');
       sm(c_nazwa_procedury,SQLCODE||' - '||SQLERRM);
   end;
  end sm_hotfix;
  
  /*
  Generowanie list warto�ci
  */
  procedure gen_dml_listy
  is
  begin
   ekp_install.zrzuc_lst(p_wstaw_commit => true
                        ,p_warunek => ' lst_nazwa like (''PP%'')');
  end gen_dml_listy;
  
  /*
   Generowanie skryptu dla EAT_UZYCIE_W_PROFILACH
  */
  procedure gen_dml_uwp
  is
  cursor c_cursor is
  select
  prf.PRF_NAZWA UWP_PRF_NAZWA,
  fu.FU_NAZWA UWP_FU_NAZWA
  from eat_uzycia_w_profilach uwp,
       eat_funkcje_uzytkowe fu,
       eat_profile prf,
       eat_aplikacje apl
  where uwp.UWP_FU_ID=fu.FU_ID
  and uwp.UWP_PRF_ID=prf.PRF_ID
  and apl.APL_ID=fu.FU_APL_ID
  and ((prf.PRF_F_PORTAL='T') or (apl.APL_SYMBOL='PP'))
  MINUS
  select
  prf.PRF_NAZWA UWP_PRF_NAZWA,
  fu.FU_NAZWA UWP_FU_NAZWA
  from eat_uzycia_w_profilach@conf uwp,
       eat_funkcje_uzytkowe@conf fu,
       eat_profile@conf prf,
       eat_aplikacje@conf apl
  where uwp.UWP_FU_ID=fu.FU_ID
  and uwp.UWP_PRF_ID=prf.PRF_ID
  and apl.APL_ID=fu.FU_APL_ID
  and ((prf.PRF_F_PORTAL='T') or (apl.APL_SYMBOL='PP'))
  ;
  begin
    for c_rec in c_cursor loop
      ekp_install.drukuj('DECLARE');
      ekp_install.drukuj('c_prf_nazwa varchar2(100):='''||c_rec.UWP_PRF_NAZWA||''';');
      ekp_install.drukuj('c_fu_nazwa varchar2(100):='''||c_rec.UWP_FU_NAZWA||''';');
      ekp_install.drukuj('v_prf_id number;');
      ekp_install.drukuj('v_fu_id number;');
      ekp_install.drukuj('v_error boolean:=false;');
      ekp_install.drukuj('v_dml_type varchar2(1):='''';');
      ekp_install.drukuj('BEGIN');
      ekp_install.drukuj('begin');
      ekp_install.drukuj('select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;');
      ekp_install.drukuj('exception');
      ekp_install.drukuj('when others then');
      ekp_install.drukuj('v_error:=true;');
      ekp_install.drukuj('eap_blad.zglos(p_procedura=>''import PP: eat_uzycia_w_profilach'',');
      ekp_install.drukuj('p_dodatkowe_info=>''nie istnieje '||c_rec.UWP_PRF_NAZWA||''');');
      ekp_install.drukuj('end;');
      ekp_install.drukuj('begin');
      ekp_install.drukuj('select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;');
      ekp_install.drukuj('exception');
      ekp_install.drukuj('when others then');
      ekp_install.drukuj('v_error:=true;');
      ekp_install.drukuj('eap_blad.zglos(p_procedura=>''import PP: eat_uzycia_w_profilach'',');
      ekp_install.drukuj('p_dodatkowe_info=>''nie istnieje funkcja u�ytkowa '||c_rec.UWP_FU_NAZWA||''');');
      ekp_install.drukuj('end;');
      ekp_install.drukuj('if not(v_error) then');
      ekp_install.drukuj('select decode(count(*),0,''I'',''U'') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;');
      ekp_install.drukuj('if v_dml_type=''I'' then');
      ekp_install.drukuj('INSERT INTO EAT_UZYCIA_W_PROFILACH');
      ekp_install.drukuj('(uwp_prf_id, uwp_fu_id)');
      ekp_install.drukuj('VALUES');
      ekp_install.drukuj('(v_prf_id, v_fu_id);');
      ekp_install.drukuj('COMMIT;');
      ekp_install.drukuj('end if;');
      ekp_install.drukuj('end if;');
      ekp_install.drukuj('END;');
      ekp_install.drukuj('/');
    end loop;
  end gen_dml_uwp;
  
  /*
  Generowanie skryptu instalacyjnego dla Profili 
  */
  procedure gen_dml_prf
  is
  cursor c_cursor is
  select
  DECODE(NVL(prf_cel.PRF_NAZWA,'I'),'I','I', 'U') DML_TYPE,
  count(prf_cel.PRF_NAZWA) ILE_CEL,
  anl.PRF_NAZWA,
  anl.PRF_INS_ID,
  anl.PRF_FRM_ID,
  anl.PRF_F_EGERIA,
  anl.PRF_F_KOKPITY,
  anl.PRF_F_PORTAL,
  anl.PRF_F_CONTROLLING,
  anl.PRF_F_GENERATOR,
  anl.PRF_F_EDUKACJA,
  anl.PRF_OPIS
  from EAT_PROFILE@conf prf_cel
  right join 
  (        
          select
          prf.PRF_NAZWA,
          prf.PRF_INS_ID,
          prf.PRF_FRM_ID,
          prf.PRF_F_EGERIA,
          prf.PRF_F_KOKPITY,
          prf.PRF_F_PORTAL,
          prf.PRF_F_CONTROLLING,
          prf.PRF_F_GENERATOR,
          prf.PRF_F_EDUKACJA,
          prf.PRF_OPIS
          from EAT_PROFILE prf
          where prf.PRF_F_PORTAL='T'
          -- PO TESTACH USUNAC                      
          -- and ((prf.PRF_NAZWA like 'PPP_TEST_COMARCH%') or (prf.PRF_NAZWA='PPP_DELZ_DYR_KOM') or (prf.PRF_NAZWA='PPP_DELZ_BAD') or (prf.PRF_NAZWA='PPP_DELZ_BDG') or (prf.PRF_NAZWA='PPP_OCE_SEKRETARIAT') or (prf.PRF_NAZWA='EK_WA_BAD') or (prf.PRF_NAZWA='EK_WA_BDG') or (prf.PRF_NAZWA='EK_WA_SEKRETARIAT'))
          --
          MINUS
          select
          prf.PRF_NAZWA,
          prf.PRF_INS_ID,
          prf.PRF_FRM_ID,
          prf.PRF_F_EGERIA,
          prf.PRF_F_KOKPITY,
          prf.PRF_F_PORTAL,
          prf.PRF_F_CONTROLLING,
          prf.PRF_F_GENERATOR,
          prf.PRF_F_EDUKACJA,
          prf.PRF_OPIS
          from EAT_PROFILE@conf prf
  ) anl on anl.PRF_NAZWA=prf_cel.PRF_NAZWA
  group by
  prf_cel.PRF_NAZWA,
  anl.PRF_NAZWA,
  anl.PRF_INS_ID,
  anl.PRF_FRM_ID,
  anl.PRF_F_EGERIA,
  anl.PRF_F_KOKPITY,
  anl.PRF_F_PORTAL,
  anl.PRF_F_CONTROLLING,
  anl.PRF_F_GENERATOR,
  anl.PRF_F_EDUKACJA,
  anl.PRF_OPIS
  ;
  begin
   for c_rec in c_cursor loop
    if c_rec.DML_TYPE='I' and c_rec.ILE_CEL=0 then
     --Generowanie skryptu dodajacego profil
     ekp_install.drukuj('DECLARE');
     ekp_install.drukuj('v_rec eap_obj_profil.t_profil;');
     ekp_install.drukuj('v_prf_rowid_cel ROWID;');
     ekp_install.drukuj('BEGIN ');
     ekp_install.drukuj('v_rec.prf_id:=eap_utl.pobierz_numer(''EAS_PRF'');');
     ekp_install.drukuj('v_rec.prf_ins_id:='||c_rec.prf_ins_id||';');
     ekp_install.drukuj('v_rec.prf_frm_id:='||c_rec.prf_frm_id||';');
     ekp_install.zrzuc_parametr('v_rec.prf_nazwa:=',c_rec.prf_nazwa,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_opis:=',c_rec.prf_opis,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_f_egeria:=',c_rec.prf_f_egeria,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_f_kokpity:=',c_rec.prf_f_kokpity,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_f_portal:=',c_rec.prf_f_portal,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_f_controlling:=',c_rec.prf_f_controlling,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_f_edukacja:=',c_rec.prf_f_edukacja,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_f_generator:=',c_rec.prf_f_generator,''''); ekp_install.drukuj(';');
     ekp_install.drukuj('v_prf_rowid_cel:=eap_obj_profil.wstaw(v_rec);');
     ekp_install.drukuj('COMMIT;');
     ekp_install.drukuj('END;');
     ekp_install.drukuj('/');
    end if;
    
    if c_rec.DML_TYPE='U' and c_rec.ILE_CEL=1 then
    --Generowanie skryptu aktualizuj�cego profil
     ekp_install.drukuj('DECLARE');
     ekp_install.drukuj('v_rec eap_obj_profil.t_profil;');
     ekp_install.drukuj('v_prf_rowid_cel ROWID;');
     ekp_install.drukuj('v_error boolean:=false;');
     ekp_install.drukuj('BEGIN ');
     ekp_install.drukuj('begin');
     ekp_install.zrzuc_parametr('select prf.prf_id, rowid into v_rec.prf_id, v_rec.prf_rowid from eat_profile prf where prf.prf_nazwa=',c_rec.prf_nazwa,''''); ekp_install.drukuj(';');
     ekp_install.drukuj('exception');
     ekp_install.drukuj('when others then');
     ekp_install.zrzuc_parametr('eap_blad.zglos (p_procedura => ','gen_dml_prf.'||c_rec.prf_nazwa,''''); ekp_install.drukuj(');');
     ekp_install.drukuj('v_error:=true;');
     ekp_install.drukuj('end;');
     ekp_install.drukuj('v_rec.prf_ins_id:='||c_rec.prf_ins_id||';');
     ekp_install.drukuj('v_rec.prf_frm_id:='||c_rec.prf_frm_id||';');
     ekp_install.zrzuc_parametr('v_rec.prf_nazwa:=',c_rec.prf_nazwa,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_opis:=',c_rec.prf_opis,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_f_egeria:=',c_rec.prf_f_egeria,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_f_kokpity:=',c_rec.prf_f_kokpity,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_f_portal:=',c_rec.prf_f_portal,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_f_controlling:=',c_rec.prf_f_controlling,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_f_edukacja:=',c_rec.prf_f_edukacja,''''); ekp_install.drukuj(';');
     ekp_install.zrzuc_parametr('v_rec.prf_f_generator:=',c_rec.prf_f_generator,''''); ekp_install.drukuj(';');
     ekp_install.drukuj('if not(v_error) then'); 
     ekp_install.drukuj('eap_obj_profil.modyfikuj(v_rec);');
     ekp_install.drukuj('COMMIT;');
     ekp_install.drukuj('end if;');
     ekp_install.drukuj('END;');
     ekp_install.drukuj('/');
    end if;
   end loop;
  end gen_dml_prf;
  
  /*
   Generowanie skryptu dla tabeli EAT_PROFILE_PROFILE
  */
  procedure gen_dml_prf_prf
  is
  cursor c_cursor is
  select
  prf_nad.PRF_NAZWA PRF_NAD_NAZWA,
  prf_pod.PRF_NAZWA PRF_POD_NAZWA
  from eat_profile_profile prf_drzewo,
       eat_profile prf_nad,
       eat_profile prf_pod
  where prf_drzewo.PRF_PRF_ID_NAD=prf_nad.PRF_ID
  and   prf_drzewo.PRF_PRF_ID_POD=prf_pod.PRF_ID
  and   ((prf_nad.PRF_F_PORTAL='T') or (prf_nad.PRF_F_PORTAL='T'))
  MINUS
  select
  prf_nad.PRF_NAZWA PRF_NAD_NAZWA,
  prf_pod.PRF_NAZWA PRF_POD_NAZWA
  from eat_profile_profile@conf prf_drzewo,
       eat_profile@conf prf_nad,
       eat_profile@conf prf_pod
  where prf_drzewo.PRF_PRF_ID_NAD=prf_nad.PRF_ID
  and   prf_drzewo.PRF_PRF_ID_POD=prf_pod.PRF_ID;
  begin
   for v_rec in c_cursor loop
    ekp_install.drukuj('DECLARE');
    ekp_install.zrzuc_parametr('c_prf_nad_nazwa varchar2(100):=',v_rec.PRF_NAD_NAZWA,''''); ekp_install.drukuj(';');
    ekp_install.zrzuc_parametr('c_prf_pod_nazwa varchar2(100):=',v_rec.PRF_POD_NAZWA,''''); ekp_install.drukuj(';');
    ekp_install.drukuj('v_prf_nad_id number;');
    ekp_install.drukuj('v_prf_pod_id number;');
    ekp_install.drukuj('v_error boolean:=false;');
    ekp_install.drukuj('v_dml_type varchar2(1):='''';');
    ekp_install.drukuj('BEGIN');
    ekp_install.drukuj('begin');
    ekp_install.drukuj('select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;');
    ekp_install.drukuj('exception');
    ekp_install.drukuj('when others then');
    ekp_install.drukuj('v_error:=true;');
    ekp_install.drukuj('eap_blad.zglos(p_procedura=>''import PP: EAT_PROFILE_PROFILE'',');
    ekp_install.drukuj('p_dodatkowe_info=>''nie istnieje PROFIL '||v_rec.PRF_NAD_NAZWA||''');');
    ekp_install.drukuj('end;');
    ekp_install.drukuj('begin');
    ekp_install.drukuj('select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;');
    ekp_install.drukuj('exception');
    ekp_install.drukuj('when others then');
    ekp_install.drukuj('v_error:=true;');
    ekp_install.drukuj('eap_blad.zglos(p_procedura=>''import PP: EAT_PROFILE_PROFILE'',');
    ekp_install.drukuj('p_dodatkowe_info=>''nie istnieje PROFIL '||v_rec.PRF_POD_NAZWA||''');');
    ekp_install.drukuj('end;');
    ekp_install.drukuj('if not(v_error) then');
    ekp_install.drukuj('select decode(count(*),0,''I'',''U'') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;');
    ekp_install.drukuj('if v_dml_type=''I'' then');
    ekp_install.drukuj('INSERT INTO EAT_PROFILE_PROFILE');
    ekp_install.drukuj('(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)');
    ekp_install.drukuj('VALUES');
    ekp_install.drukuj('(v_prf_nad_id, v_prf_pod_id);');
    ekp_install.drukuj('COMMIT;');
    ekp_install.drukuj('end if;');
    ekp_install.drukuj('end if;');
    ekp_install.drukuj('END;');
    ekp_install.drukuj('/');
   end loop; 
  end gen_dml_prf_prf;
  
  /*
   Generowanie skryptu instalacyjnego dla Funkcji U�ytkownika
  */
  procedure gen_dml_fu
  is
  c_nazwa_procedury constant varchar(100):=c_package_name||'.gen_dml_fu';
  cursor c_cursor is
  select
  DECODE(NVL(fu_cel.FU_NAZWA,'I'),'I','I', 'U') DML_TYPE,
  anl.FU_INS_ID, 
  anl.FU_NAZWA, 
  anl.FU_NAZWA_ROLI, 
  anl.FU_OPIS
  from EAT_FUNKCJE_UZYTKOWE@conf fu_cel
  right join
  (
  select 
  fu.FU_INS_ID, 
  fu.FU_NAZWA, 
  fu.FU_NAZWA_ROLI, 
  fu.FU_OPIS
  from EAADM.EAT_FUNKCJE_UZYTKOWE fu
  where fu.fu_apl_id IN (select apl.apl_id from EAT_APLIKACJE apl where apl.APL_SYMBOL='PP')
  -- PO TESTACH USUNAC
  --and fu.FU_NAZWA='PP_SZKOL_WYDZ_SZKOLEN'
  MINUS
  select 
  fu.FU_INS_ID, 
  fu.FU_NAZWA, 
  fu.FU_NAZWA_ROLI, 
  fu.FU_OPIS
  from EAADM.EAT_FUNKCJE_UZYTKOWE@conf fu
  where fu.fu_apl_id IN (select apl.apl_id from EAT_APLIKACJE@conf apl where apl.APL_SYMBOL='PP')
  ) anl on fu_cel.FU_INS_ID=anl.FU_INS_ID
       and fu_cel.FU_NAZWA=anl.FU_NAZWA
  ;
  v_opcja number:=0;
begin
 for c_rec in c_cursor loop
     if c_rec.DML_TYPE='I' then
        v_opcja:=1;
     elsif c_rec.DML_TYPE='U' then
        v_opcja:=2;
     else
        v_opcja:=0;
     end if;   
     ekp_install.zrzuc_fu(p_wstaw_commit => true
                          ,p_opcje => v_opcja
                          ,p_warunek => 'fu_nazwa='''||c_rec.FU_NAZWA||'''');
     if c_rec.FU_NAZWA_ROLI is not null then
     --Generowanie polecenia update do zarzadzania kolumna FU_NAZWA_ROLI
     ekp_install.drukuj('BEGIN');
     ekp_install.drukuj('UPDATE EAT_FUNKCJE_UZYTKOWE SET ');
     ekp_install.zrzuc_parametr('FU_NAZWA_ROLI=',c_rec.fu_nazwa_roli,'''');
     ekp_install.zrzuc_parametr('WHERE FU_NAZWA=',c_rec.fu_nazwa,'''');
     ekp_install.drukuj('AND FU_INS_ID='||c_rec.fu_ins_id||';');
     ekp_install.drukuj('COMMIT;');
     ekp_install.drukuj('END;');
     ekp_install.drukuj('/');
     end if;
 end loop;
end gen_dml_fu;
  
  /*
   Uruchomienie polecenia SQL na bazie docelowej
  */
  procedure uruchom_polecenie_cel(p_sql varchar2)
  is
  begin
   null;
  end uruchom_polecenie_cel;


--------------------------------------------------------------------------------
-- DO WYRZUCENIA NIE MA MECHANIZMU POROWNYWANIA DDL
--------------------------------------------------------------------------------
  /*
   Generuj liste obiektow bazodanowych:
   1. obiekt nie istnieje w bazie docelowej - nalezy go dodac
   2. obiekt istnieje w bazie docelowej ale jego definicja ulegla zmianie - nalezy go zaktualizowac
  */
  procedure generuj_liste_obiektow
  is 
  p_wzor_ddl CLOB:=empty_clob();  
  p_cel_ddl CLOB:=empty_clob();
  p_wzor_dlugosc number:=0;
  p_wynik number;
  begin
   begin
     select 
     SYS.DBMS_METADATA.GET_DDL('PACKAGE','PPP_MIGRACJA','EGADM1') 
     into p_wzor_ddl
     from dual;
   exception 
    when others then
     p_wzor_ddl:=empty_clob();
   end;
   
   begin
     select 
     SYS.DBMS_METADATA.GET_DDL('PACKAGE','PPP_API_PP_EGERIA','EGADM1') 
     into p_cel_ddl
     from dual;
   exception 
    when others then
     p_cel_ddl:=empty_clob();
   end;  
   
   p_wynik:=DBMS_LOB.COMPARE(p_cel_ddl, p_wzor_ddl);
   
   if p_wynik=0 then
      DBMS_OUTPUT.put_line('Polecenia s� identyczne');
   else
      DBMS_OUTPUT.put_line('Polecenia NIE S� identyczne');
   end if;
   
  end generuj_liste_obiektow;

  /*
   Synchroznizacja pakiet�
  */
  procedure synch_pakietow
  is
  begin
   null;
  end synch_pakietow;

  /*
   Synchronizacja wyzwalaczy
  */
  procedure synch_wyzwalacze
  is
  begin
   null;
  end synch_wyzwalacze;

  /*
   Synchronizacja indeksow
  */
  procedure synch_indeksy
  is
  begin
   null;
  end synch_indeksy;
  /*
   Synchronizacja ogranicze� 
  */
  procedure synch_ograniczenia
  is
  begin
   null;
  end synch_ograniczenia;

  /*
   Synchronizacja funkcji
  */
  procedure synch_funkcje
  is
  begin
   null;
  end synch_funkcje;

  /*
   Synchronizacja procedur
  */
  procedure synch_procedury
  is
  begin
   null;
  end synch_procedury;

  /*
   Synchronizacja widok�w
  */
  procedure sych_widoki
  is
  begin
   null;
  end sych_widoki;

  /*
   Synchronizacja TABELE - komentarze
  */
  procedure synch_tabele_komentarze
  is
  begin
   null;
  end synch_tabele_komentarze; 

  /*
   Synchronizacja TABEL - nowe tabele
  */
  procedure synch_tabele_nowe
  is  
  cursor c_tabele is
  --------------------------------------------------------------------------------
  -- LISTA TABEL KTORE NIE ISTNIEJA W BAZIE DOCELOWEJ
  --------------------------------------------------------------------------------
  select
  tab.OWNER,
  tab.TABLE_NAME
  from all_tables tab
  where tab.owner in (select aobj_owner from ppt_objects group by aobj_owner)
  and tab.table_name in (select aobj_name from ppt_objects where aobj_type='TABLE' and AOBJ_IS_VERSIONED='T')
  MINUS
  select
  tab.OWNER,
  tab.TABLE_NAME
  from all_tables@conf tab
  where tab.owner in (select aobj_owner from ppt_objects group by aobj_owner)
  and tab.table_name in (select aobj_name from ppt_objects where aobj_type='TABLE' and AOBJ_IS_VERSIONED='T')
  ;
  
  begin
   null;
  end synch_tabele_nowe;

  /*
   Synchronizacja TABEL - istniejace tabele
  */
  procedure synch_tabele_modyfikacja
  is  
  begin
   null;
  end synch_tabele_modyfikacja;
  
  /*
   Inicjacja obiektow bazodanowych podlegajacych kontroli roznicowej:
   1. TABELE
   2. WIDOKI
   3. PAKIETY
   4. FUNKCJE
   5. PROCEDURY   
  */
  function inicjuj_zakres_obiektow
  return boolean
  is
   p_wynik boolean:=true;
  begin
   begin
      --------------------------------------------------------------------------------
      -- LISTA TABEL PODLEGAJACYCH KONTROLI MECHANIZMU ROZNICOWEGO
      --------------------------------------------------------------------------------
      MERGE INTO PPT_OBJECTS des
      USING
      (select
      tab.OWNER AOBJ_OWNER,
      tab.TABLE_NAME AOBJ_NAME,
      'TABLE' AOBJ_TYPE,
      'T' AOBJ_IS_VERSIONED
      from all_tables tab
      where tab.OWNER IN ('EGADM1','EAADM','EGINT')
      and ((tab.TABLE_NAME LIKE 'PPT%')
      or (tab.TABLE_NAME LIKE 'DELT%')
      or (tab.TABLE_NAME LIKE 'WEB%'))
      ) sou
      ON (sou.AOBJ_OWNER=des.AOBJ_OWNER
      and sou.AOBJ_NAME=des.AOBJ_NAME
      and sou.AOBJ_TYPE=des.AOBJ_TYPE
      )
      when NOT MATCHED then
      INSERT (des.AOBJ_OWNER, des.AOBJ_NAME, des.AOBJ_TYPE, des.AOBJ_IS_VERSIONED)
      VALUES (sou.AOBJ_OWNER, sou.AOBJ_NAME, sou.AOBJ_TYPE, sou.AOBJ_IS_VERSIONED)
      ;   
   exception
    when others then
     p_wynik:=false;
   end;
   begin
      --------------------------------------------------------------------------------
      -- LISTA WIDOKOW PODLEGAJACYCH KONTROLI MECHANIZMU ROZNICOWEGO
      --------------------------------------------------------------------------------
      MERGE INTO PPT_OBJECTS des
      USING
      (select
      vie.OWNER AOBJ_OWNER,
      vie.VIEW_NAME AOBJ_NAME,
      'VIEW' AOBJ_TYPE,
      'T' AOBJ_IS_VERSIONED
      from SYS.ALL_VIEWS vie
      where vie.OWNER IN ('EGADM1','EAADM','EGINT')
      and vie.VIEW_NAME LIKE 'PPV%'
      ) sou
      ON (sou.AOBJ_OWNER=des.AOBJ_OWNER
      and sou.AOBJ_NAME=des.AOBJ_NAME
      and sou.AOBJ_TYPE=des.AOBJ_TYPE
      )
      when NOT MATCHED then
      INSERT (des.AOBJ_OWNER, des.AOBJ_NAME, des.AOBJ_TYPE, des.AOBJ_IS_VERSIONED)
      VALUES (sou.AOBJ_OWNER, sou.AOBJ_NAME, sou.AOBJ_TYPE, sou.AOBJ_IS_VERSIONED)
      ;
   exception
    when others then
     p_wynik:=false;
   end;

  --------------------------------------------------------------------------------
  -- LISTA PAKIETOW PODLEGAJACYCH KONTROLI MECHANIZMU ROZNICOWEGO
  --------------------------------------------------------------------------------
  --TODO

  --------------------------------------------------------------------------------
  -- LISTA PROCEDUR PODLEGAJACYCH KONTROLI MECHANIZMU ROZNICOWEGO
  --------------------------------------------------------------------------------
  --TODO
  
  --------------------------------------------------------------------------------
  -- LISTA FUNKCJI PODLEGAJACYCH KONTROLI MECHANIZMU ROZNICOWEGO
  --------------------------------------------------------------------------------
  --TODO
   
   
  end inicjuj_zakres_obiektow;


  /*
   Generowanie Hotfix DDL 
  */  
  procedure hotfix_ddl_generuj(p_hotfix_no in varchar2)
  is
  p_procedura_nazwa varchar(100):=c_package_name||'.'||'hotfix_ddl_generuj';
  p_wynik boolean;
  begin
   g_hotfix_no:=p_hotfix_no;
   g_hotfix_data:=sysdate;
   sm(p_procedura_nazwa,'Generowanie Hotfix '||g_hotfix_no||'Modu� Portal Pracowniczy - START');
   
   p_wynik:=inicjuj_zakres_obiektow;
   if p_wynik then
     begin
      synch_tabele_nowe;
     exception
      when others then
       null;
     end;
   end if;
   
   sm(p_procedura_nazwa,'Generowanie Hotfix '||g_hotfix_no||'Modu� Portal Pracowniczy - ZAKO�CZONO');
  end hotfix_ddl_generuj;

END PPP_HOTFIX;