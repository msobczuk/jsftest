BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_sl  css_slowniki%ROWTYPE;
BEGIN
DELETE css_wartosci_slownikow 
 WHERE wsl_sl_nazwa = ''PS_PRZYCZYNA_WN_OPIS_STANOW'';
v_r_sl.sl_nazwa := ''PS_PRZYCZYNA_WN_OPIS_STANOW'';
v_r_sl.sl_opis := ''Przyczyna z�o�enia wniosku o zatwierdzenie opisu stanowiska pracy'';
v_r_sl.sl_max_dlugosc := 240;
v_r_sl.sl_max_ilosc := NULL;
v_r_sl.sl_poziom_dostepu := ''1'';
v_r_sl.sl_wzorzec := '''';
ekp_install.wstaw_slownik (v_r_sl, 0); 
COMMIT;
END;',
'css_slowniki',
'Wstawienie i modyfikacja slownika: PS_PRZYCZYNA_WN_OPIS_STANOW'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''A'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''PS_PRZYCZYNA_WN_OPIS_STANOW'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Aktualizacja'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Aktualizacja opisu stanowiska pracy'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: PS_PRZYCZYNA_WN_OPIS_STANOW'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''N'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''PS_PRZYCZYNA_WN_OPIS_STANOW'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Nowe'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Nowo utworzone stanowisko'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: PS_PRZYCZYNA_WN_OPIS_STANOW'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''LN'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''PS_PRZYCZYNA_WN_OPIS_STANOW'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Likwidacja Nowe'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Likwidacja opisu stanowiska w zwi�zku z utworzeniem nowego stanowiska'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: PS_PRZYCZYNA_WN_OPIS_STANOW'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''LA'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''PS_PRZYCZYNA_WN_OPIS_STANOW'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Likwidacja Aktualizacja'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Likwidacja opisu stanowiska w zwi�zku z przej�ciem zada� przez zaktualizowane stanowisko'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: PS_PRZYCZYNA_WN_OPIS_STANOW'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_sl  css_slowniki%ROWTYPE;
BEGIN
DELETE css_wartosci_slownikow 
 WHERE wsl_sl_nazwa = ''PS_TYP_WN_OPIS_STANOW'';
v_r_sl.sl_nazwa := ''PS_TYP_WN_OPIS_STANOW'';
v_r_sl.sl_opis := ''Typ wniosku o zatwierdzenie opisu stanowiska pracy'';
v_r_sl.sl_max_dlugosc := 240;
v_r_sl.sl_max_ilosc := NULL;
v_r_sl.sl_poziom_dostepu := ''0'';
v_r_sl.sl_wzorzec := '''';
ekp_install.wstaw_slownik (v_r_sl, 0); 
COMMIT;
END;',
'css_slowniki',
'Wstawienie i modyfikacja slownika: PS_TYP_WN_OPIS_STANOW'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''N'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''PS_TYP_WN_OPIS_STANOW'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Snownisko nieb�d�ce wy�szym'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := '''';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: PS_TYP_WN_OPIS_STANOW'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''W'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''PS_TYP_WN_OPIS_STANOW'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Stanowisko wy�sze'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := '''';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: PS_TYP_WN_OPIS_STANOW'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_sl  css_slowniki%ROWTYPE;
BEGIN
DELETE css_wartosci_slownikow 
 WHERE wsl_sl_nazwa = ''REK_RODZAJ'';
v_r_sl.sl_nazwa := ''REK_RODZAJ'';
v_r_sl.sl_opis := ''Rodzaj naboru / rekrutacji'';
v_r_sl.sl_max_dlugosc := 240;
v_r_sl.sl_max_ilosc := NULL;
v_r_sl.sl_poziom_dostepu := ''0'';
v_r_sl.sl_wzorzec := '''';
ekp_install.wstaw_slownik (v_r_sl, 0); 
COMMIT;
END;',
'css_slowniki',
'Wstawienie i modyfikacja slownika: REK_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WEW_ST_POM'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_RODZAJ'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''$WN3$'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Rekrutacja wewn�trzna na stanowisko pomocnicze'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''KSC'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_RODZAJ'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''$WN2$'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Nab�r do korpusu s�u�by cywilnej'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''KSAP'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_RODZAJ'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''$WN4$'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Nab�r dla absolwent�w KSAP'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''ST_POM'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_RODZAJ'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''$WN3$'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Nab�r na stanowiska pomocnicze'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''JED_PODL'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_RODZAJ'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''$WN2$'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Rekrutacja przeprowadzana w jednostkach podleg�ych i nadzorowanych'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WEWN'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_RODZAJ'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''$WN1$'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Rekrutacja wewn�trzna w Ministerstwie'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WYZ_ST_SC'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_RODZAJ'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''$WN5$'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Nab�r na wy�sze stanowisko w s�u�bie cywilnej'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_sl  css_slowniki%ROWTYPE;
BEGIN
DELETE css_wartosci_slownikow 
 WHERE wsl_sl_nazwa = ''REK_SKL_RODZAJ'';
v_r_sl.sl_nazwa := ''REK_SKL_RODZAJ'';
v_r_sl.sl_opis := ''Rodzaj sk�adnika wynagrodzania'';
v_r_sl.sl_max_dlugosc := 240;
v_r_sl.sl_max_ilosc := NULL;
v_r_sl.sl_poziom_dostepu := ''0'';
v_r_sl.sl_wzorzec := '''';
ekp_install.wstaw_slownik (v_r_sl, 0); 
COMMIT;
END;',
'css_slowniki',
'Wstawienie i modyfikacja slownika: REK_SKL_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WYNAGR'';
v_r_wsl.wsl_wartosc_max := ''1'';
v_r_wsl.wsl_sl_nazwa := ''REK_SKL_RODZAJ'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Wynagrodzenie zasadnicze'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Wynagrodzenie zasadnicze'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_SKL_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''PREMIA_GW'';
v_r_wsl.wsl_wartosc_max := ''2'';
v_r_wsl.wsl_sl_nazwa := ''REK_SKL_RODZAJ'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Premia gwarantowana'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Premia gwarantowana'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_SKL_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''PREMIA_UZ'';
v_r_wsl.wsl_wartosc_max := ''3'';
v_r_wsl.wsl_sl_nazwa := ''REK_SKL_RODZAJ'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Premia uznaniowa'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Premia uznaniowa'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_SKL_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''DOD_STAZ'';
v_r_wsl.wsl_wartosc_max := ''4'';
v_r_wsl.wsl_sl_nazwa := ''REK_SKL_RODZAJ'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Dodatek sta�owy'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Dodatek sta�owy'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_SKL_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''DOD_SPEC'';
v_r_wsl.wsl_wartosc_max := ''5'';
v_r_wsl.wsl_sl_nazwa := ''REK_SKL_RODZAJ'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Dodatek specjalny'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Dodatek specjalny'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_SKL_RODZAJ'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_sl  css_slowniki%ROWTYPE;
BEGIN
DELETE css_wartosci_slownikow 
 WHERE wsl_sl_nazwa = ''REK_ZAK_STATUS'';
v_r_sl.sl_nazwa := ''REK_ZAK_STATUS'';
v_r_sl.sl_opis := ''Wynik naboru/rekrutacji'';
v_r_sl.sl_max_dlugosc := 240;
v_r_sl.sl_max_ilosc := NULL;
v_r_sl.sl_poziom_dostepu := ''2'';
v_r_sl.sl_wzorzec := '''';
ekp_install.wstaw_slownik (v_r_sl, 0); 
COMMIT;
END;',
'css_slowniki',
'Wstawienie i modyfikacja slownika: REK_ZAK_STATUS'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WYN1'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_ZAK_STATUS'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Nab�r zako�czony wyborem kandydatki / kandydata'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Nab�r zako�czony wyborem kandydatki / kandydata'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_ZAK_STATUS'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WYN2'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_ZAK_STATUS'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Brak ofert kandydatek/kandydat�w'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Brak ofert kandydatek/kandydat�w'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_ZAK_STATUS'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WYN3'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_ZAK_STATUS'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Oferty kandydatek/kandydat�w nie spe�ni�y wymaga� formalnych'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Oferty kandydatek/kandydat�w nie spe�ni�y wymaga� formalnych'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_ZAK_STATUS'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WYN4'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_ZAK_STATUS'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Nie wy�oniono najlepszych kandydatek/kandydat�w'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Nie wy�oniono najlepszych kandydatek/kandydat�w'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_ZAK_STATUS'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WYN5'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_ZAK_STATUS'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Nab�r zako�czony bez wyboru kandydatki/kandydata'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Nab�r zako�czony bez wyboru kandydatki/kandydata'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_ZAK_STATUS'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WYN6'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_ZAK_STATUS'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Decyzja kandydatki/kandydata o rezygnacji z obj�cia stanowiska'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Decyzja kandydatki/kandydata o rezygnacji z obj�cia stanowiska'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_ZAK_STATUS'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WYN7'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_ZAK_STATUS'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Anulowano nab�r'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Anulowano nab�r'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_ZAK_STATUS'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WYN8'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_ZAK_STATUS'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Wp�yn�� wniosek o zatrudnienie kandydata'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Wp�yn�� wniosek o zatrudnienie kandydata'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_ZAK_STATUS'
);
END;
/
BEGIN
eap_api_install.wykonaj_polecenie('
DECLARE
v_r_wsl css_wartosci_slownikow%ROWTYPE;
BEGIN
v_r_wsl.wsl_wartosc := ''WYN9'';
v_r_wsl.wsl_wartosc_max := '''';
v_r_wsl.wsl_sl_nazwa := ''REK_ZAK_STATUS'';
v_r_wsl.wsl_typ := ''UC'';
v_r_wsl.wsl_alias := ''Wp�yn�� wniosek o przeniesienie kandydata'';
v_r_wsl.wsl_alias2 := '''';
v_r_wsl.wsl_alias3 := '''';
v_r_wsl.wsl_alias4 := '''';
v_r_wsl.wsl_alias5 := '''';
v_r_wsl.wsl_alias6 := '''';
v_r_wsl.wsl_alias7 := '''';
v_r_wsl.wsl_alias8 := '''';
v_r_wsl.wsl_alias9 := '''';
v_r_wsl.wsl_opis := ''Wp�yn�� wniosek o przeniesienie kandydata'';
v_r_wsl.wsl_opis2 := '''';
v_r_wsl.wsl_opis3 := '''';
v_r_wsl.wsl_opis4 := '''';
v_r_wsl.wsl_opis5 := '''';
v_r_wsl.wsl_opis6 := '''';
v_r_wsl.wsl_opis7 := '''';
v_r_wsl.wsl_opis8 := '''';
v_r_wsl.wsl_opis9 := '''';
ekp_install.wstaw_wartosc_slownika (v_r_wsl, 0); 
COMMIT;
END;',
'css_wartosci_slownikow',
'Wstawienie i modyfikacja slownika: REK_ZAK_STATUS'
);
END;
/

