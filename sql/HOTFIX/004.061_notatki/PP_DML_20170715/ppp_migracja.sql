create or replace PACKAGE PPP_MIGRACJA AS 

/* 
 Pakiet do obs�ugi mechanizmu synchronizaji s�ownik�w konfiguracyjnych:
 �rodowisko: DEV -> CONF
 
 Synchronizacja odbywa si� za po�rednictwem Public Databae Lins o nazwie CONF 
 
*/ 

procedure sm(p_nazwa_procedury VARCHAR2 default null,
             p_komunikat VARCHAR2 DEFAULT NULL,
             p_komunikat_rodzaj VARCHAR2 DEFAULT 'I');
             

/*
 G��wna procedura migurj�ca dane s�ownik�w 
*/
procedure migruj_konfiguracja;

END PPP_MIGRACJA;