BEGIN
eap_api_install.modyfikuj_funkcje( 
p_nazwa =>'PP_DELZ_ADMINISTRATOR'
,
p_nowa_nazwa =>'PP_DELZ_ADMINISTRATOR'
,
p_nowy_opis =>''
,
p_nowa_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_DELZ_ADMINISTRATOR'
WHERE FU_NAZWA='PP_DELZ_ADMINISTRATOR'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_DELZ_BAD'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_DELZ_BAD'
WHERE FU_NAZWA='PP_DELZ_BAD'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_DELZ_BDG'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_DELZ_BDG'
WHERE FU_NAZWA='PP_DELZ_BDG'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_DELZ_DYR_BDG'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_DELZ_DYR_BDG'
WHERE FU_NAZWA='PP_DELZ_DYR_BDG'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_DELZ_DYR_KOM'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_DELZ_DYR_KOM'
WHERE FU_NAZWA='PP_DELZ_DYR_KOM'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_DELZ_DYR_PROJ'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_DELZ_DYR_PROJ'
WHERE FU_NAZWA='PP_DELZ_DYR_PROJ'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_DELZ_KOORD_PROJ'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_DELZ_KOORD_PROJ'
WHERE FU_NAZWA='PP_DELZ_KOORD_PROJ'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_DELZ_NACZ_DEL'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_DELZ_NACZ_DEL'
WHERE FU_NAZWA='PP_DELZ_NACZ_DEL'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_DELZ_PRACOWNIK'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_DELZ_PRACOWNIK'
WHERE FU_NAZWA='PP_DELZ_PRACOWNIK'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_IPRZ_ADMINISTRATOR'
,
p_opis =>'Uprawnienia do IPRZ - poziom administratora'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_IPRZ_ADMINISTRATOR'
WHERE FU_NAZWA='PP_IPRZ_ADMINISTRATOR'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_IPRZ_DG'
,
p_opis =>'Uprawnienia do IPRZ - uprawnienie Dyrektora Generalnego'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_IPRZ_DG'
WHERE FU_NAZWA='PP_IPRZ_DG'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_IPRZ_PRACOWNIK'
,
p_opis =>'Uprawnienia do IPRZ - przegl�danie w�asnych IPRZ'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_IPRZ_PRACOWNIK'
WHERE FU_NAZWA='PP_IPRZ_PRACOWNIK'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_IPRZ_PRZELOZONY'
,
p_opis =>'Uprawnienia do IPRZ - poziom prze�o�onego'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_IPRZ_PRZELOZONY'
WHERE FU_NAZWA='PP_IPRZ_PRZELOZONY'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_OCE_ADMINISTRATOR'
,
p_opis =>'Uprawnienia do oceny okresowej - poziom administratora'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_OCE_ADMINISTRATOR'
WHERE FU_NAZWA='PP_OCE_ADMINISTRATOR'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_OCE_OCENIAJACY'
,
p_opis =>'Uprawnienia do oceny okresowej dla oceniaj�cego'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_OCE_OCENIAJACY'
WHERE FU_NAZWA='PP_OCE_OCENIAJACY'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_OCE_OCENIANY'
,
p_opis =>'Uprawnienia do oceny okresowej dla ocenianego'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_OCE_OCENIANY'
WHERE FU_NAZWA='PP_OCE_OCENIANY'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_OCE_SEKRETARIAT'
,
p_opis =>'Uprawnienia do oceny okresowej - wszystkie oceny z danego JO'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_OCE_SEKRETARIAT'
WHERE FU_NAZWA='PP_OCE_SEKRETARIAT'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_OKU_ADMINISTRATOR'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_OKU_BHP'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_OKU_PRACOWNIK'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_OKU_PRZELOZONY'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_REK_ADMINISTRATOR'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_REK_ADMINISTRATOR'
WHERE FU_NAZWA='PP_REK_ADMINISTRATOR'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_REK_KADRY'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_REK_KADRY'
WHERE FU_NAZWA='PP_REK_KADRY'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_REK_PRACOWNIK'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_REK_PRACOWNIK'
WHERE FU_NAZWA='PP_REK_PRACOWNIK'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_REK_WYNAGRODZENIA'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_REK_WYNAGRODZENIA'
WHERE FU_NAZWA='PP_REK_WYNAGRODZENIA'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_SLPR_ADMINISTRATOR'
,
p_opis =>'Uprawnienia do s�u�by przygotowawczej - poziom administratora'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_SLPR_ADMINISTRATOR'
WHERE FU_NAZWA='PP_SLPR_ADMINISTRATOR'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_SLPR_BDG'
,
p_opis =>'Uprawnienia do s�u�by przygotowawczej - akceptacja wniosku'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_SLPR_BDG'
WHERE FU_NAZWA='PP_SLPR_BDG'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_SLPR_DG'
,
p_opis =>''
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_SLPR_DG'
WHERE FU_NAZWA='PP_SLPR_DG'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_SLPR_KIER_KOM'
,
p_opis =>'Uprawnienia do s�u�by przygotowawczej - wszystkie wnioski z danego JO'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_SLPR_KIER_KOM'
WHERE FU_NAZWA='PP_SLPR_KIER_KOM'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_SLPR_PRACOWNIK'
,
p_opis =>'Uprawnienia do s�u�by przygotowawczej - wprowadzanie wniosku i swoim imieniu lub innych os�b'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_SLPR_PRACOWNIK'
WHERE FU_NAZWA='PP_SLPR_PRACOWNIK'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_SLPR_PRZELOZONY'
,
p_opis =>'Uprawnienia do s�u�by przygotowawczej - wszystkie wnioski podw�adnych'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_SLPR_PRZELOZONY'
WHERE FU_NAZWA='PP_SLPR_PRZELOZONY'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_SZKOL_ADMINISTRATOR'
,
p_opis =>'Modu� szkolenia'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_SZKOL_ADMINISTRATOR'
WHERE FU_NAZWA='PP_SZKOL_ADMINISTRATOR'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_SZKOL_PRACOWNIK'
,
p_opis =>'Zapisy na szkolenia przez pracownika'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_SZKOL_PRACOWNIK'
WHERE FU_NAZWA='PP_SZKOL_PRACOWNIK'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_SZKOL_PRZELOZONY'
,
p_opis =>'Modu� szkole� - prze�o�ony'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_SZKOL_PRZELOZONY'
WHERE FU_NAZWA='PP_SZKOL_PRZELOZONY'
AND FU_INS_ID=1;
COMMIT;
END;
/
BEGIN
eap_api_install.dodaj_funkcje( 
p_nazwa =>'PP_SZKOL_WYDZ_SZKOLEN'
,
p_opis =>'Modu� szkoleniowy - wydzia� szkole�'
,
p_apl =>'PP'
);
COMMIT;
END;
/
BEGIN
UPDATE EAT_FUNKCJE_UZYTKOWE SET 
FU_NAZWA_ROLI='PP_SZKOL_WYDZ_SZKOLEN'
WHERE FU_NAZWA='PP_SZKOL_WYDZ_SZKOLEN'
AND FU_INS_ID=1;
COMMIT;
END;
/