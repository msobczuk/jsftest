declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='CKK_ADRESY';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='OST_LISTY_SZKOLEN';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='ZPT_KANDYDACI';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='ZPT_OCENY';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='OST_ANKIETY_POZYCJE';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='PPT_URLOPY_PLANOWANE';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='PPT_URLOPY_PLANOWANE_DNI';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='OST_PLAN_INDYW';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='OST_WN_SKIEROWANIE_STUDIA';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='ZPT_SLPR_BLOKI';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='ZPT_SLUZBA_PRZYG';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='OST_UMOWY';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='ZPT_IPRZ';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='ZPT_OC_KRYTERIA';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='ZPT_NABORY';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='ZPT_NABOR_KOMISJA';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='ZPT_NABOR_SKLAD';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='OST_ZAPISY_SZKOLEN';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/
declare
c_procedura varchar2(30):='PPT_JPA_GENKEYS_dodaj_popraw';
V_PKEY_TABLE varchar(60):='PPT_WNIOSEK_OKULARY';
V_PKEY_VALUE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_JPA_GENKEYS where PKEY_TABLE=V_PKEY_TABLE;
if p_ile=0 then
begin
insert into PPT_JPA_GENKEYS
(PKEY_TABLE, PKEY_VALUE)
values
(V_PKEY_TABLE, V_PKEY_VALUE);
COMMIT;
ekp_install.drukuj('Wstawiono sekwencje dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad wstawiania sekwencji dla moduu Portal Pracowniczy dla tabeli: ' ||V_PKEY_TABLE);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/