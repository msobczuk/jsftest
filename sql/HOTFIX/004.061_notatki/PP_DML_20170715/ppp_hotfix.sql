create or replace PACKAGE PPP_HOTFIX AS 

  /* Generowanie skrypt�w synchronizuj�cych obiekty bazy danych  */ 
  
  /*
   Generowanie Hotfix DDL 
  */  
  procedure hotfix_ddl_generuj(p_hotfix_no in varchar2);
  
  /*
  Generowanie skryptu instalacyjnego dla Profili 
  */
  procedure gen_dml_prf;
  
  /*
   Generowanie skryptu dla tabeli EAT_PROFILE_PROFILE
  */
  procedure gen_dml_prf_prf;
  
  /*
   Generowanie skryptu dla EAT_UZYCIE_W_PROFILACH
  */
  procedure gen_dml_uwp;
  
  /*
  Generowanie list warto�ci
  */
  procedure gen_dml_listy;
  
  /*
   Generowanie skryptu synchronizuj�cego Funkcje U�ytkowego moduu Portal Pracowniczy
  */
  procedure gen_dml_fu;

END PPP_HOTFIX;