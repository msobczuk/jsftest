DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_ADMINISTRATOR'
;
c_prf_pod_nazwa varchar2(100):='EK - pe�en dost�p'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_ADMINISTRATOR');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK - pe�en dost�p');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_ADMINISTRATOR'
;
c_prf_pod_nazwa varchar2(100):='PPP_DELZ_ADMINISTRATOR'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_ADMINISTRATOR');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_DELZ_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_ADMINISTRATOR'
;
c_prf_pod_nazwa varchar2(100):='PPP_IPRZ_ADMINISTRATOR'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_ADMINISTRATOR');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_IPRZ_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_ADMINISTRATOR'
;
c_prf_pod_nazwa varchar2(100):='PPP_OCE_ADMINISTRATOR'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_ADMINISTRATOR');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_OCE_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_ADMINISTRATOR'
;
c_prf_pod_nazwa varchar2(100):='PPP_OKU_ADMINISTRATOR'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_ADMINISTRATOR');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_OKU_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_ADMINISTRATOR'
;
c_prf_pod_nazwa varchar2(100):='PPP_REK_ADMINISTRATOR'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_ADMINISTRATOR');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_REK_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_ADMINISTRATOR'
;
c_prf_pod_nazwa varchar2(100):='PPP_SLPR_ADMINISTRATOR'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_ADMINISTRATOR');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_SLPR_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_ADMINISTRATOR'
;
c_prf_pod_nazwa varchar2(100):='PPP_SZKOL_ADMINISTRATOR'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_ADMINISTRATOR');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_SZKOL_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_ADMINISTRATOR'
;
c_prf_pod_nazwa varchar2(100):='ST - pe�en dost�p'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_ADMINISTRATOR');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL ST - pe�en dost�p');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_BDG'
;
c_prf_pod_nazwa varchar2(100):='PPP_SLPR_BDG'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_BDG');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_SLPR_BDG');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_BHP'
;
c_prf_pod_nazwa varchar2(100):='PPP_OKU_BHP'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_BHP');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_OKU_BHP');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_DYREKTOR_GEN'
;
c_prf_pod_nazwa varchar2(100):='PPP_IPRZ_DG'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_DYREKTOR_GEN');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_IPRZ_DG');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_DYREKTOR_GEN'
;
c_prf_pod_nazwa varchar2(100):='PPP_SLPR_DG'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_DYREKTOR_GEN');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_SLPR_DG');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_DYR_BDG'
;
c_prf_pod_nazwa varchar2(100):='PPP_DELZ_DYR_BDG'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_DYR_BDG');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_DELZ_DYR_BDG');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_DYR_PROJ'
;
c_prf_pod_nazwa varchar2(100):='PPP_DELZ_DYR_PROJ'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_DYR_PROJ');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_DELZ_DYR_PROJ');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_KADRY'
;
c_prf_pod_nazwa varchar2(100):='PPP_REK_KADRY'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_KADRY');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_REK_KADRY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_KIER_KOM'
;
c_prf_pod_nazwa varchar2(100):='PPP_DELZ_DYR_KOM'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_KIER_KOM');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_DELZ_DYR_KOM');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_KIER_KOM'
;
c_prf_pod_nazwa varchar2(100):='PPP_SLPR_KIER_KOM'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_KIER_KOM');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_SLPR_KIER_KOM');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_KOORD_PROJ'
;
c_prf_pod_nazwa varchar2(100):='PPP_DELZ_KOORD_PROJ'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_KOORD_PROJ');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_DELZ_KOORD_PROJ');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_NACZ_DEL'
;
c_prf_pod_nazwa varchar2(100):='PPP_DELZ_NACZ_DEL'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_NACZ_DEL');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_DELZ_NACZ_DEL');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_PRACOWNIK'
;
c_prf_pod_nazwa varchar2(100):='PPP_DELZ_PRACOWNIK'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_PRACOWNIK');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_DELZ_PRACOWNIK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_PRACOWNIK'
;
c_prf_pod_nazwa varchar2(100):='PPP_IPRZ_PRACOWNIK'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_PRACOWNIK');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_IPRZ_PRACOWNIK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_PRACOWNIK'
;
c_prf_pod_nazwa varchar2(100):='PPP_OCE_OCENIAJACY'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_PRACOWNIK');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_OCE_OCENIAJACY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_PRACOWNIK'
;
c_prf_pod_nazwa varchar2(100):='PPP_OCE_OCENIANY'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_PRACOWNIK');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_OCE_OCENIANY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_PRACOWNIK'
;
c_prf_pod_nazwa varchar2(100):='PPP_OKU_PRACOWNIK'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_PRACOWNIK');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_OKU_PRACOWNIK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_PRACOWNIK'
;
c_prf_pod_nazwa varchar2(100):='PPP_REK_PRACOWNIK'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_PRACOWNIK');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_REK_PRACOWNIK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_PRACOWNIK'
;
c_prf_pod_nazwa varchar2(100):='PPP_SLPR_PRACOWNIK'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_PRACOWNIK');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_SLPR_PRACOWNIK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_PRACOWNIK'
;
c_prf_pod_nazwa varchar2(100):='PPP_SZKOL_PRACOWNIK'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_PRACOWNIK');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_SZKOL_PRACOWNIK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_PRZELOZONY'
;
c_prf_pod_nazwa varchar2(100):='PPP_IPRZ_PRZELOZONY'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_PRZELOZONY');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_IPRZ_PRZELOZONY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_PRZELOZONY'
;
c_prf_pod_nazwa varchar2(100):='PPP_OKU_PRZELOZONY'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_PRZELOZONY');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_OKU_PRZELOZONY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_PRZELOZONY'
;
c_prf_pod_nazwa varchar2(100):='PPP_SLPR_PRZELOZONY'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_PRZELOZONY');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_SLPR_PRZELOZONY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_PRZELOZONY'
;
c_prf_pod_nazwa varchar2(100):='PPP_SZKOL_PRZELOZONY'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_PRZELOZONY');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_SZKOL_PRZELOZONY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_WYDZ_SZKOL'
;
c_prf_pod_nazwa varchar2(100):='PPP_SZKOL_WYDZ_SZKOLEN'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_WYDZ_SZKOL');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_SZKOL_WYDZ_SZKOLEN');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nad_nazwa varchar2(100):='EK_WA_WYNAGRODZENIA'
;
c_prf_pod_nazwa varchar2(100):='PPP_REK_WYNAGRODZENIA'
;
v_prf_nad_id number;
v_prf_pod_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_nad_id from eat_profile where prf_nazwa=c_prf_nad_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL EK_WA_WYNAGRODZENIA');
end;
begin
select prf_id into v_prf_pod_id from eat_profile where prf_nazwa=c_prf_pod_nazwa;
exception
when others then
v_error:=false;
eap_blad.zglos(p_procedura=>'import PP: EAT_PROFILE_PROFILE',
p_dodatkowe_info=>'nie istnieje PROFIL PPP_REK_WYNAGRODZENIA');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from eat_profile_profile where prf_prf_id_nad=v_prf_pod_id and prf_prf_id_pod=v_prf_pod_id;
if v_dml_type='I' then
INSERT INTO EAT_PROFILE_PROFILE
(PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
VALUES
(v_prf_nad_id, v_prf_pod_id);
COMMIT;
end if;
end if;
END;
/

