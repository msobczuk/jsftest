BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLUZBA_PRZYG'
,
p_dlugosc => 140,
p_etykieta =>'S�uzba przygotowawcza'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'ID_SLUZBY_PRZYG'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    SLPR.SLPR_ID      ID_SLUZBY_PRZYG,' || CHR(10) || '' ||
           '    ek.PRC_IMIE       IMIE_PRACOWNIKA,' || CHR(10) || '' ||
           '    ek.PRC_NAZWISKO   NAZWISKO_PRACOWNIKA,' || CHR(10) || '' ||
           '    op.OB_SKROT       SKROT_KOM_ORG,' || CHR(10) || '' ||
           '    SLPR.SLPR_DATA_OD SLUZBA_OD,' || CHR(10) || '' ||
           '    SLPR.SLPR_DATA_DO SLUZBA_DO,' || CHR(10) || '' ||
           '    0,' || CHR(10) || '' ||
           '    0' || CHR(10) || '' ||
           '  from EK_PRACOWNICY ek,' || CHR(10) || '' ||
           '       ZPT_SLUZBA_PRZYG SLPR,' || CHR(10) || '' ||
           '       CSS_OBIEKTY_W_PRZEDSIEB OP,' || CHR(10) || '' ||
           '      (select ' || CHR(10) || '' ||
           '       ek_zat.ZAT_PRC_ID,' || CHR(10) || '' ||
           '       ek_zat.ZAT_OB_ID,' || CHR(10) || '' ||
           '       ek_zat.zat_data_zmiany,' || CHR(10) || '' ||
           '       zpt_sta.STN_NAZWA' || CHR(10) || '' ||
           '       from EK_ZATRUDNIENIE ek_zat,' || CHR(10) || '' ||
           '            ZPT_STANOWISKA zpt_sta' || CHR(10) || '' ||
           '       where ek_zat.ZAT_STN_ID=zpt_sta.STN_ID' || CHR(10) || '' ||
           '       group by ek_zat.ZAT_PRC_ID,ek_zat.ZAT_OB_ID, ek_zat.zat_data_zmiany, zpt_sta.STN_NAZWA' || CHR(10) || '' ||
           '      ) ek_zat1' || CHR(10) || '' ||
           '    where ' || CHR(10) || '' ||
           '    SLPR.SLPR_PRC_ID(+)=ek.PRC_ID' || CHR(10) || '' ||
           'AND ek_zat1.ZAT_PRC_ID(+)=ek.PRC_ID' || CHR(10) || '' ||
           'AND ek_zat1.ZAT_OB_ID=OP.OB_ID(+)' || CHR(10) || '' ||
           'AND(ek_zat1.zat_data_zmiany is null or  ek_zat1.zat_data_zmiany = ( select MAX(ek1.zat_data_zmiany) ' || CHR(10) || '' ||
           '                             FROM ek_zatrudnienie ek1' || CHR(10) || '' ||
           '                    WHERE ek1.zat_data_zmiany <= TRUNC(sysdate)' || CHR(10) || '' ||
           '                      AND ek1.zat_prc_id = prc_id))' || CHR(10) || '' ||
           'AND SLPR.SLPR_ID LIKE :POLE_1' || CHR(10) || '' ||
           'and NVL(upper(ek.PRC_IMIE),''%'') LIKE upper(:POLE_2)' || CHR(10) || '' ||
           'and NVL(upper(ek.PRC_NAZWISKO),''%'') LIKE upper(:POLE_3)' || CHR(10) || '' ||
           'AND NVL(upper(op.OB_SKROT),''%'') LIKE upper(:POLE_4)' || CHR(10) || '' ||
           'AND NVL(to_char(SLPR.SLPR_DATA_OD),''%'') LIKE upper(:POLE_5)' || CHR(10) || '' ||
           'AND NVL(to_char(SLPR.SLPR_DATA_DO),''%'') LIKE upper(:POLE_6)'
,
p_opis => 'Arkusz s�u�by przygotowawczej'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLUZBA_PRZYG'
,
p_dlugosc => 100,
p_etykieta =>'Imie'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'IMIE_PRACOWNIKA'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLUZBA_PRZYG'
,
p_dlugosc => 100,
p_etykieta =>'Nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'NAZWISKO_PRACOWNIKA'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Ocena egzaminu P/N'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_ocena_egzamin'
,
p_lp => '13'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Ocena egzaminu'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_ocena_egzamin_dec'
,
p_lp => '14'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Nr pracownika'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_prc_id'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_LISTA'
,
p_dlugosc => 100,
p_etykieta =>'Pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_imie_nazwisko'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Imi�'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_prc_imie'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_prc_nazwisko'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Jednostka organizacyjna'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_ob_nazwa'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Data z�o�enia wniosku'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_data_zl_wn'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Data ostatniej zmiany statusu'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_data_ost_zm'
,
p_lp => '8'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_wn_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    zi.iprz_id pp_wn_id,' || CHR(10) || '' ||
           '    zi.iprz_prc_id pp_prc_id,' || CHR(10) || '' ||
           '    INITCAP(ek.prc_imie||'' ''||ek.prc_nazwisko) pp_prc_imie_nazwisko,' || CHR(10) || '' ||
           '    ek.prc_imie pp_prc_imie,' || CHR(10) || '' ||
           '    ek.prc_nazwisko pp_prc_nazwisko,' || CHR(10) || '' ||
           '    zi.iprz_audyt_dt pp_data_zl_wn,' || CHR(10) || '' ||
           '    ob.OB_NAZWA         pp_ob_nazwa,' || CHR(10) || '' ||
           '    zi.iprz_audyt_dm pp_data_ost_zm    ' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           '  from ZPT_IPRZ zi' || CHR(10) || '' ||
           '  JOIN  ek_pracownicy ek' || CHR(10) || '' ||
           '  ON ek.prc_id = zi.iprz_prc_id' || CHR(10) || '' ||
           '  JOIN (select zat_prc_id,ZAT_OB_ID from EK_ZATRUDNIENIE' || CHR(10) || '' ||
           'group by ZAT_PRC_ID,ZAT_OB_ID) ez' || CHR(10) || '' ||
           '  ON ek.PRC_ID = ez.ZAT_PRC_ID ' || CHR(10) || '' ||
           'LEFT OUTER JOIN CSS_OBIEKTY_W_PRZEDSIEB ob' || CHR(10) || '' ||
           '  ON ez.ZAT_OB_ID = ob.OB_ID'
,
p_opis => 'Lista arkuszy'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_BADANIA'
,
p_dlugosc => 50,
p_etykieta =>'Data badania'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_bad_data_wykonania'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_BADANIA'
,
p_dlugosc => 50,
p_etykieta =>'Rodzaj badania'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_bad_rodzaj'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_BADANIA'
,
p_dlugosc => 50,
p_etykieta =>'Data wa�no�ci'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_bad_data_waznosci'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_BADANIA'
,
p_dlugosc => 50,
p_etykieta =>'Opis badania'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_bad_opis'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_WNIOSKI_ALL'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_skst_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           'sk.SKST_ID                            pp_skst_id,' || CHR(10) || '' ||
           'sk.SKST_PRC_ID                        pp_skst_prc_id,' || CHR(10) || '' ||
           'ek.PRC_IMIE                           pp_prc_imie,' || CHR(10) || '' ||
           'ek.PRC_NAZWISKO                       pp_prc_nazwisko,' || CHR(10) || '' ||
           'ek.PRC_IMIE||'' ''||ek.PRC_NAZWISKO     pp_pracownik,' || CHR(10) || '' ||
           'sk.SKST_ORGANIZATOR                   pp_skst_organizator,' || CHR(10) || '' ||
           'sk.SKST_FORMA                         pp_skst_forma,' || CHR(10) || '' ||
           'sk.SKST_KOSZT                         pp_skst_koszt,' || CHR(10) || '' ||
           'sk.SKST_UZASADNIANIE                  pp_skst_uzasadnianie' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           'from OST_WN_SKIEROWANIE_STUDIA sk' || CHR(10) || '' ||
           'JOIN EK_PRACOWNICY ek' || CHR(10) || '' ||
           'ON ek.PRC_ID = sk.SKST_PRC_ID' || CHR(10) || '' ||
           'and sk.skst_prc_id like (:PARAM_1)'
,
p_opis => 'Zapisy na studia - wszystkie wnioski'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_WNIOSKI_ALL'
,
p_dlugosc => 50,
p_etykieta =>'Nr pracownika'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_skst_prc_id'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_WNIOSKI_ALL'
,
p_dlugosc => 50,
p_etykieta =>'Imi�'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_imie'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_WNIOSKI_ALL'
,
p_dlugosc => 50,
p_etykieta =>'Nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_nazwisko'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_WNIOSKI_ALL'
,
p_dlugosc => 100,
p_etykieta =>'Pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pracownik'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_WNIOSKI_ALL'
,
p_dlugosc => 50,
p_etykieta =>'Organizator'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_skst_organizator'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_WNIOSKI_ALL'
,
p_dlugosc => 50,
p_etykieta =>'Forma'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_skst_forma'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_WNIOSKI_ALL'
,
p_dlugosc => 50,
p_etykieta =>'Koszt'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_skst_koszt'
,
p_lp => '8'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_WNIOSKI_ALL'
,
p_dlugosc => 200,
p_etykieta =>'Uzasadnienie'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_skst_uzasadnianie'
,
p_lp => '9'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_SPEC'
,
p_dlugosc => 100,
p_etykieta =>'pp_pis_id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pis_id'
,
p_lp => '1'
,
p_query => 'select  ' || CHR(10) || '' ||
           ' opi.PIS_ID			pp_pis_id ' || CHR(10) || '' ||
           ' , opi.PIS_SSPE_OBSZAR_WIEDZY	pp_pis_obszar_wiedzy ' || CHR(10) || '' ||
           ' , opi.PIS_SSPE_FORMA		pp_pis_forma ' || CHR(10) || '' ||
           ' , opi.PIS_TEMAT 			pp_pis_temat ' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           ' from OST_PLAN_INDYW opi  ' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           ' where ' || CHR(10) || '' ||
           ' 	opi.PIS_SZK_POWSZECH is null  ' || CHR(10) || '' ||
           ' 	and opi.PIS_IPRZ_ID = :POLE_1'
,
p_opis => 'Szkolenia'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_SPEC'
,
p_dlugosc => 100,
p_etykieta =>'pp_pis_obszar_wiedzy'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pis_obszar_wiedzy'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_SPEC'
,
p_dlugosc => 100,
p_etykieta =>'pp_pis_forma'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pis_forma'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_SPEC'
,
p_dlugosc => 100,
p_etykieta =>'pp_pis_temat'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pis_temat'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KRAJE'
,
p_dlugosc => 100,
p_etykieta =>'KLOK_ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '	    L.KLOK_ID,' || CHR(10) || '' ||
           '	    L.KLOK_KLOK_ID,' || CHR(10) || '' ||
           '	    L.KLOK_KOD,' || CHR(10) || '' ||
           '	    L.KLOK_WARTOSC' || CHR(10) || '' ||
           '	from  EGADM1.PPT_KSIAZKA_LOKALIZACJE L' || CHR(10) || '' ||
           '	where  L.KLOK_KLOK_ID is null' || CHR(10) || '' ||
           '	order by  L.KLOK_KOD, L.KLOK_WARTOSC'
,
p_opis => 'Kraje'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KRAJE'
,
p_dlugosc => 100,
p_etykieta =>'KLOK_KLOK_ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KRAJE'
,
p_dlugosc => 100,
p_etykieta =>'KLOK_KOD'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KRAJE'
,
p_dlugosc => 100,
p_etykieta =>'KLOK_WARTOSC'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'pp_szk_id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '        es.SZK_ID pp_szk_id' || CHR(10) || '' ||
           '		, es.SZK_NAZWA pp_szk_nazwa' || CHR(10) || '' ||
           '		, decode(es.SZK_RODZAJ, jez.WSL_WARTOSC, jez.WSL_ALIAS) pp_szk_rodzaj' || CHR(10) || '' ||
           '		, es.SZK_KOMENTARZ pp_szk_komentarz' || CHR(10) || '' ||
           '		, es.SZK_ORGANIZATOR pp_szk_organizator' || CHR(10) || '' ||
           '		, es.SZK_DATA_OD pp_szk_od' || CHR(10) || '' ||
           '		, es.SZK_DATA_DO pp_szk_do' || CHR(10) || '' ||
           '        , ek.prc_id' || CHR(10) || '' ||
           '    ' || CHR(10) || '' ||
           '        ' || CHR(10) || '' ||
           '    from EK_SZKOLENIA es, CSS_WARTOSCI_SLOWNIKOW jez, EK_KWALIFIKACJE ekw, EK_PRACOWNICY ek' || CHR(10) || '' ||
           '    ' || CHR(10) || '' ||
           '    where jez.WSL_SL_NAZWA = ''RODZAJ_SZKOLENIA''' || CHR(10) || '' ||
           '    and es.SZK_KWA_ID = ekw.KWA_ID' || CHR(10) || '' ||
           '    and ekw.KWA_PRC_ID = ek.PRC_ID' || CHR(10) || '' ||
           '    and es.SZK_RODZAJ = jez.WSL_WARTOSC' || CHR(10) || '' ||
           '    and ek.PRC_ID = :POLE_1'
,
p_opis => 'Szolenia'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL'
,
p_dlugosc => 100,
p_etykieta =>'pp_szk_nazwa'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_nazwa'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL'
,
p_dlugosc => 100,
p_etykieta =>'pp_szk_rodzaj'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_rodzaj'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL'
,
p_dlugosc => 150,
p_etykieta =>'pp_szk_komentarz'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_komentarz'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL'
,
p_dlugosc => 100,
p_etykieta =>'pp_szk_organizator'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_organizator'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL'
,
p_dlugosc => 100,
p_etykieta =>'pp_szk_od'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_od'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL'
,
p_dlugosc => 100,
p_etykieta =>'pp_szk_do'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_do'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL'
,
p_dlugosc => 100,
p_etykieta =>'prc_id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'prc_id'
,
p_lp => '8'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_STANOWISKA_KOSZTOW'
,
p_dlugosc => 100,
p_etykieta =>'pp_sk_kod'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_sk_kod'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '		sk_kod pp_sk_kod' || CHR(10) || '' ||
           '		, sk_id pp_sk_id' || CHR(10) || '' ||
           '		, sk_opis pp_sk_opis ' || CHR(10) || '' ||
           '	' || CHR(10) || '' ||
           '	from CSS_STANOWISKA_KOSZTOW ' || CHR(10) || '' ||
           '	where sk_stan_definicji <> ''A'''
,
p_opis => 'Stanowiska koszt�w'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_STANOWISKA_KOSZTOW'
,
p_dlugosc => 100,
p_etykieta =>'pp_sk_id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_sk_id'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_STANOWISKA_KOSZTOW'
,
p_dlugosc => 100,
p_etykieta =>'pp_sk_opis'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_sk_opis'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_WYDZIALY'
,
p_dlugosc => 100,
p_etykieta =>'pp_ob_id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_ob_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '		OB_ID pp_ob_id' || CHR(10) || '' ||
           '		, OB_NAZWA pp_ob_nazwa ' || CHR(10) || '' ||
           '	from CSS_OBIEKTY_W_PRZEDSIEB' || CHR(10) || '' ||
           '	where ob_ob_id = :POLE_1'
,
p_opis => 'Wydzia�y'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_WYDZIALY'
,
p_dlugosc => 100,
p_etykieta =>'pp_ob_nazwa'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_ob_nazwa'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ'
,
p_dlugosc => 50,
p_etykieta =>'IPRZ'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_iprz_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           'iprz.IPRZ_ID pp_iprz_id,' || CHR(10) || '' ||
           'ekp.PRC_IMIE pp_prc_imie,' || CHR(10) || '' ||
           'ekp.PRC_NAZWISKO pp_prc_nazwisko,' || CHR(10) || '' ||
           'iprz.IPRZ_SCIEZKA pp_iprz_sciezka,' || CHR(10) || '' ||
           'iprz.IPRZ_PROGRAM pp_iprz_program,' || CHR(10) || '' ||
           '0,0,0' || CHR(10) || '' ||
           'from ' || CHR(10) || '' ||
           'ZPT_IPRZ iprz,' || CHR(10) || '' ||
           'EK_PRACOWNICY ekp' || CHR(10) || '' ||
           'where ' || CHR(10) || '' ||
           'iprz.IPRZ_PRC_ID=ekp.PRC_ID' || CHR(10) || '' ||
           'and iprz.IPRZ_ID LIKE :POLE_1' || CHR(10) || '' ||
           'and UPPER(ekp.PRC_IMIE) LIKE UPPER(:POLE_2)' || CHR(10) || '' ||
           'and UPPER(ekp.PRC_NAZWISKO) LIKE UPPER(:POLE_3)' || CHR(10) || '' ||
           'and UPPER(iprz.IPRZ_SCIEZKA) LIKE UPPER(:POLE_4)' || CHR(10) || '' ||
           'and UPPER(iprz.IPRZ_PROGRAM) LIKE UPPER(:POLE_5)' || CHR(10) || '' ||
           ''
,
p_opis => 'Indywidualny program rozwoju zawodowego'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ'
,
p_dlugosc => 100,
p_etykieta =>'Imie'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_imie'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ'
,
p_dlugosc => 100,
p_etykieta =>'Nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_nazwisko'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ'
,
p_dlugosc => 150,
p_etykieta =>'Sciezka'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_iprz_sciezka'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ'
,
p_dlugosc => 100,
p_etykieta =>'Program'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_iprz_program'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_KARTOTEKI'
,
p_dlugosc => 0,
p_etykieta =>'Identyfikator naboru'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'NAB_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           'NAB_ID,' || CHR(10) || '' ||
           'NAB_STN_ID,' || CHR(10) || '' ||
           'NAB_STJ_ID,' || CHR(10) || '' ||
           'NAB_NR_OFERTY_PRC,' || CHR(10) || '' ||
           'NAB_OB_ID,' || CHR(10) || '' ||
           'NAB_RODZAJ_NABORU,' || CHR(10) || '' ||
           'NAB_WYMIAR_ETATU,' || CHR(10) || '' ||
           'NAB_LICZBA_WAKATOW,' || CHR(10) || '' ||
           'NAB_RODZAJ_NABORU,' || CHR(10) || '' ||
           'NAB_MNOZNIK_MIN,' || CHR(10) || '' ||
           'NAB_MNOZNIK_MAX,' || CHR(10) || '' ||
           'NAB_UZASADNIENIE,' || CHR(10) || '' ||
           'NAB_UWAGI,' || CHR(10) || '' ||
           'NAB_NR_OGL_BIP,' || CHR(10) || '' ||
           'NAB_DATA_PUBLIKACJI,' || CHR(10) || '' ||
           'NAB_DATA_ZAKONC,' || CHR(10) || '' ||
           'NAB_TERMIN_SKL_OF,' || CHR(10) || '' ||
           'NAB_CZY_INF_NIEPELN,' || CHR(10) || '' ||
           'NAB_CZY_INF_WSK_NIEPELN,' || CHR(10) || '' ||
           'NAB_CZY_INF_CDZ' || CHR(10) || '' ||
           'from ZPT_NABORY'
,
p_opis => 'Lista kartotek'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_KARTOTEKI'
,
p_dlugosc => 50,
p_etykieta =>'Stanowisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'NAB_STN_ID'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_KARTOTEKI'
,
p_dlugosc => 50,
p_etykieta =>'Nr oferty pracy'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'NAB_NR_OFERTY_PRC'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_WNIOSKI_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Stanowisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'STN_NAZWA'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_WNIOSKI_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Nr oferty pracy'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'NAB_NR_OFERTY_PRC'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_WNIOSKI_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Jednostka organizacyjna'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'NAB_OB_ID'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLUZBA_PRZYG'
,
p_dlugosc => 120,
p_etykieta =>'Komorka organizacyjna'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'SKROT_KOM_ORG'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_NABOR'
,
p_dlugosc => 100,
p_etykieta =>'Stanowisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_stn_nazwa'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_UMOWY'
,
p_dlugosc => 50,
p_etykieta =>'Umowa'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_umsz_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           'umw.UMSZ_ID pp_umsz_id,' || CHR(10) || '' ||
           'ekp.PRC_IMIE pp_prc_imie,' || CHR(10) || '' ||
           'ekp.PRC_NAZWISKO pp_prc_nazwisko,' || CHR(10) || '' ||
           'umw.UMSZ_PRZEDMIOT pp_umsz_przedmiot,' || CHR(10) || '' ||
           'umw.UMSZ_DATA_PODPISANIA pp_umsz_data_podpisania,' || CHR(10) || '' ||
           'umw.UMSZ_DATA_OBOWIAZYWANIA pp_umsz_data_obowiazywania,' || CHR(10) || '' ||
           '0,0' || CHR(10) || '' ||
           'from OST_UMOWY umw,EK_PRACOWNICY ekp, EK_ZATRUDNIENIE ekz,ZPT_STANOWISKA sta,CSS_OBIEKTY_W_PRZEDSIEB ob' || CHR(10) || '' ||
           'where umw.UMSZ_PRC_ID=ekp.PRC_ID' || CHR(10) || '' ||
           'and ekp.PRC_ID = ekz.ZAT_PRC_ID(+)' || CHR(10) || '' ||
           'and ekz.ZAT_STN_ID=sta.STN_ID' || CHR(10) || '' ||
           'and ekz.ZAT_OB_ID = ob.OB_ID(+)' || CHR(10) || '' ||
           'and(ekz.zat_data_zmiany is null or  ekz.zat_data_zmiany = ( select MAX(ek1.zat_data_zmiany) ' || CHR(10) || '' ||
           '                             FROM ek_zatrudnienie ek1' || CHR(10) || '' ||
           '                    WHERE ek1.zat_data_zmiany <= TRUNC(sysdate)' || CHR(10) || '' ||
           '                      AND ek1.zat_prc_id = ekp.prc_id))' || CHR(10) || '' ||
           'AND umw.UMSZ_ID LIKE :POLE_1' || CHR(10) || '' ||
           'and NVL(upper(ekp.PRC_IMIE),''%'') LIKE upper(:POLE_2)' || CHR(10) || '' ||
           'and NVL(upper(ekp.PRC_NAZWISKO),''%'') LIKE upper(:POLE_3)' || CHR(10) || '' ||
           'and NVL(upper(umw.UMSZ_PRZEDMIOT),''%'') LIKE upper(:POLE_4)' || CHR(10) || '' ||
           'and NVL(to_char(umw.UMSZ_DATA_PODPISANIA),''%'') LIKE :POLE_5' || CHR(10) || '' ||
           'and NVL(to_char(umw.UMSZ_DATA_OBOWIAZYWANIA),''%'') LIKE :POLE_6'
,
p_opis => 'Dane umowy podpisanej z pracownikiem'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_UMOWY'
,
p_dlugosc => 100,
p_etykieta =>'Imie'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_imie'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_UMOWY'
,
p_dlugosc => 100,
p_etykieta =>'Nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_nazwisko'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_UMOWY'
,
p_dlugosc => 150,
p_etykieta =>'Przedmiot umowy'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_umsz_przedmiot'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_UMOWY'
,
p_dlugosc => 90,
p_etykieta =>'Data podpisania'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_umsz_data_podpisania'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_UMOWY'
,
p_dlugosc => 90,
p_etykieta =>'Data obowiazywania'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_umsz_data_obowiazywania'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 15,
p_etykieta =>'Stan obiegu'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'opis_obiegu'
,
p_lp => '15'
,
p_query => ''
,
p_opis => 'T'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_ZAPISY_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'Identyfikator zapisu'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_zsz_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           'zsz_id pp_zsz_id' || CHR(10) || '' ||
           ', prc_imie || '' '' || prc_nazwisko pp_pracownik' || CHR(10) || '' ||
           'from ost_zapisy_szkolen, ek_pracownicy' || CHR(10) || '' ||
           'where zsz_prc_id = prc_id'
,
p_opis => 'Lista szkole� w planie szkoleniowym'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_ZAPISY_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'Pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pracownik'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_MOJ_PLAN_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'Identyfikator'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_pis_id'
,
p_lp => '1'
,
p_query => 'select distinct' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           'opi.pis_id pp_pis_id' || CHR(10) || '' ||
           ', case when opi.PIS_IPRZ_ID is not null' || CHR(10) || '' ||
           '  then ''TAK''' || CHR(10) || '' ||
           '  else ''NIE''' || CHR(10) || '' ||
           '  end as pp_czy_iprz' || CHR(10) || '' ||
           ', opi.pis_rok pp_pis_rok' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           ', case when opi.pis_szk_powszech is null and opi.pis_temat is not null' || CHR(10) || '' ||
           '  then pis_temat' || CHR(10) || '' ||
           '  else ''-/-''' || CHR(10) || '' ||
           '  end as pp_pis_temat' || CHR(10) || '' ||
           '  ' || CHR(10) || '' ||
           ', case when opi.pis_szk_powszech is not null' || CHR(10) || '' ||
           '  then decode(opi.pis_szk_powszech, szkol_powszech.wsl_wartosc, szkol_powszech.wsl_opis' || CHR(10) || '' ||
           '  )' || CHR(10) || '' ||
           '  else ''-/-''' || CHR(10) || '' ||
           '  end as pp_szk_powszech' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           ', case when opi.pis_szk_powszech is null  and pis_sspe_obszar_wiedzy is not null' || CHR(10) || '' ||
           'then decode(opi.pis_sspe_obszar_wiedzy, obszar_wiedzy.wsl_wartosc, obszar_wiedzy.wsl_opis)' || CHR(10) || '' ||
           'else ''-/-''' || CHR(10) || '' ||
           'end as pp_obszar_wiedzy' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           ', case when opi.pis_szk_powszech is null and opi.pis_sspe_forma is not null' || CHR(10) || '' ||
           'then decode(opi.pis_sspe_forma, forma_organizacji.wsl_wartosc, forma_organizacji.wsl_opis)' || CHR(10) || '' ||
           'else ''-/-''' || CHR(10) || '' ||
           'end as pp_forma' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           ', case when opi.pis_szk_powszech is null' || CHR(10) || '' ||
           '  then opi.pis_organizator' || CHR(10) || '' ||
           '  else ''-/-''' || CHR(10) || '' ||
           '  end as pp_pis_organizator' || CHR(10) || '' ||
           '  ' || CHR(10) || '' ||
           '  , case when opi.pis_szk_powszech is null' || CHR(10) || '' ||
           '  then opi.pis_miejsce_szkol' || CHR(10) || '' ||
           '  else ''-/-''' || CHR(10) || '' ||
           '  end as pp_pis_miejsce_szkol' || CHR(10) || '' ||
           '  ' || CHR(10) || '' ||
           '  , case when opi.pis_szk_powszech is null' || CHR(10) || '' ||
           '  then opi.pis_termin_od' || CHR(10) || '' ||
           '  else null' || CHR(10) || '' ||
           '  end as pp_pis_termin_od' || CHR(10) || '' ||
           '  ' || CHR(10) || '' ||
           '    , case when opi.pis_szk_powszech is null' || CHR(10) || '' ||
           '  then opi.pis_termin_do' || CHR(10) || '' ||
           '  else null' || CHR(10) || '' ||
           '  end as pp_pis_termin_do' || CHR(10) || '' ||
           '  ' || CHR(10) || '' ||
           '      , case when opi.pis_szk_powszech is null' || CHR(10) || '' ||
           '  then opi.pis_uzasadnienie' || CHR(10) || '' ||
           '  else ''-/-''' || CHR(10) || '' ||
           '  end as pp_pis_uzasadnienie' || CHR(10) || '' ||
           '  ' || CHR(10) || '' ||
           '  ' || CHR(10) || '' ||
           'from OST_PLAN_INDYW opi, CSS_WARTOSCI_SLOWNIKOW szkol_powszech, CSS_WARTOSCI_SLOWNIKOW obszar_wiedzy, CSS_WARTOSCI_SLOWNIKOW forma_organizacji' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           'where ' || CHR(10) || '' ||
           'pis_prc_id = ? and' || CHR(10) || '' ||
           'szkol_powszech.wsl_sl_nazwa = ''OS_SZKOLENIA_POWSZECHNE''' || CHR(10) || '' ||
           'and obszar_wiedzy.wsl_sl_nazwa = ''OS_OBSZAR_WIEDZY''' || CHR(10) || '' ||
           'and forma_organizacji.wsl_sl_nazwa = ''OS_FORMA_ORGANIZACJI''' || CHR(10) || '' ||
           'and (opi.PIS_SZK_POWSZECH = szkol_powszech.WSL_WARTOSC ' || CHR(10) || '' ||
           'or(' || CHR(10) || '' ||
           ' opi.PIS_SSPE_OBSZAR_WIEDZY = obszar_wiedzy.WSL_WARTOSC' || CHR(10) || '' ||
           'and opi.PIS_SSPE_FORMA = forma_organizacji.WSL_WARTOSC)' || CHR(10) || '' ||
           ')' || CHR(10) || '' ||
           'order by pp_pis_id' || CHR(10) || '' ||
           ''
,
p_opis => 'Plan szkoleniowy pracownika'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_MOJ_PLAN_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'Data wprowadzenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pis_rok'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_MOJ_PLAN_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'Temat szkolenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pis_temat'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_DO_AKCEPT'
,
p_dlugosc => 50,
p_etykieta =>'Pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pracownik'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_DO_AKCEPT'
,
p_dlugosc => 50,
p_etykieta =>'Stan w obiegu'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'opis_obiegu'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_MOJ_PLAN_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'Tytu� szkolenia powszechnego'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_powszech'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_MOJ_PLAN_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'Obszar wiedzy'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_obszar_wiedzy'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_MOJ_PLAN_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'Forma organizacji'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_forma'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_MOJ_PLAN_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'Organizator'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pis_organizator'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_MOJ_PLAN_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'Miejsce szkolenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pis_miejsce_szkol'
,
p_lp => '8'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_MOJ_PLAN_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'Termin od'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pis_termin_od'
,
p_lp => '9'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_MOJ_PLAN_SZKOL'
,
p_dlugosc => 50,
p_etykieta =>'Termin do'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pis_termin_do'
,
p_lp => '10'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_MOJ_PLAN_SZKOL'
,
p_dlugosc => 500,
p_etykieta =>'Uzasadnienie'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pis_uzasadnienie'
,
p_lp => '11'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_LISTY_SZKOL'
,
p_dlugosc => 100,
p_etykieta =>'Lista szkoleniowa'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'LISTA_SZK_ID'
,
p_lp => '1'
,
p_query => 'SELECT ' || CHR(10) || '' ||
           'lista.lsz_id LISTA_SZK_ID,' || CHR(10) || '' ||
           'szkol.SZK_NAZWA NAZWA_SZKOLENIA,' || CHR(10) || '' ||
           'ekp.PRC_IMIE IMIE_PRACOWNIKA,' || CHR(10) || '' ||
           'ekp.PRC_NAZWISKO NAZWISKO_PRACOWNIKA,' || CHR(10) || '' ||
           '0,0,0,0' || CHR(10) || '' ||
           'FROM' || CHR(10) || '' ||
           'OST_SZKOLENIA szkol,' || CHR(10) || '' ||
           'OST_LISTY_SZKOLEN lista,' || CHR(10) || '' ||
           'ek_pracownicy ekp' || CHR(10) || '' ||
           'WHERE ' || CHR(10) || '' ||
           'lista.LSZ_PRC_ID=ekp.PRC_ID' || CHR(10) || '' ||
           'and szkol.SZK_ID=lista.LSZ_SZK_ID' || CHR(10) || '' ||
           'and to_char(lista.lsz_id) LIKE :POLE_1' || CHR(10) || '' ||
           'and UPPER(szkol.SZK_NAZWA) LIKE UPPER(:POLE_2)' || CHR(10) || '' ||
           'and UPPER(ekp.PRC_IMIE) LIKE UPPER(:POLE_3)' || CHR(10) || '' ||
           'and UPPER(ekp.PRC_NAZWISKO) LIKE UPPER(:POLE_4)'
,
p_opis => 'Listy szkoleniowe'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_LISTY_SZKOL'
,
p_dlugosc => 175,
p_etykieta =>'Szkolenie'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'NAZWA_SZKOLENIA'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_LISTY_SZKOL'
,
p_dlugosc => 100,
p_etykieta =>'Imie'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'IMIE_PRACOWNIKA'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_LISTY_SZKOL'
,
p_dlugosc => 100,
p_etykieta =>'Nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'NAZWISKO_PRACOWNIKA'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_DEL_KRAJ_OBIEG'
,
p_dlugosc => 0,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Polecenia podr�y w obiegu'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_DEL_KRAJ_ALL'
,
p_dlugosc => 0,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Wszystkie polecenia podr�y'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_DEL_KRAJ_ROZL_OBIEG'
,
p_dlugosc => 0,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Rozliczenia w obiegu'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_DEL_KRAJ_ROZL_ALL'
,
p_dlugosc => 0,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Wszystkie rozliczenia'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_DEL_ZAG_OBIEG'
,
p_dlugosc => 0,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Wnioski w obiegu'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_DEL_ZAG_ALL'
,
p_dlugosc => 0,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Wszystkie wnioski'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_DEL_ZAG_ROZL_OBIEG'
,
p_dlugosc => 0,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Rozliczenia w obiegu'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_DEL_ZAG_ROZL_ALL'
,
p_dlugosc => 0,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select 
' || CHR(10) || '' ||
           '    OCE_ID                  ,
' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,
' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,
' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,
' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,
' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,
' || CHR(10) || '' ||
           '    OCE_OPIS                ,
' || CHR(10) || '' ||
           '    OCE_OPIS2               
' || CHR(10) || '' ||
           'from ZPT_OCENY' || CHR(10) || '' ||
           'where OCE_PRC_ID like (:POLE_1)'
,
p_opis => 'Wszystkie rozliczenia'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_WNIOSKI_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_nabor_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           'zn.nab_id pp_nabor_id,' || CHR(10) || '' ||
           'zn.NAB_STANOWISKO pp_stanowisko,' || CHR(10) || '' ||
           'zn.NAB_DATA_PUBLIKACJI pp_data_publikacji,' || CHR(10) || '' ||
           'ob.ob_id pp_ob_id,' || CHR(10) || '' ||
           'ob.OB_NAZWA pp_jednostka,' || CHR(10) || '' ||
           'cds.dstn_opis pp_stan ' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           'from zpt_nabory zn, csst_encje_w_stanach ces, CSST_DEF_STANOW cds, CSS_OBIEKTY_W_PRZEDSIEB ob' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           'where' || CHR(10) || '' ||
           'zn.nab_id = ces.EWS_KLUCZ_OBCY_ID' || CHR(10) || '' ||
           'and ces.ews_kob_kod = ''WN_NABORY''' || CHR(10) || '' ||
           'and ces.EWS_DSTN_ID = cds.DSTN_ID' || CHR(10) || '' ||
           'and ob.ob_id = zn.nab_ob_id'
,
p_opis => 'Wnioski w obiegu'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_WNIOSKI_LISTA'
,
p_dlugosc => 0,
p_etykieta =>'NAB_ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'NAB_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           'NAB_ID,' || CHR(10) || '' ||
           'NAB_STN_ID,' || CHR(10) || '' ||
           'STN_NAZWA,' || CHR(10) || '' ||
           'NAB_NR_OFERTY_PRC,' || CHR(10) || '' ||
           'NAB_OB_ID,' || CHR(10) || '' ||
           'NAB_RODZAJ_NABORU,' || CHR(10) || '' ||
           'NAB_WYMIAR_ETATU,' || CHR(10) || '' ||
           'NAB_LICZBA_WAKATOW,' || CHR(10) || '' ||
           'NAB_STJ_ID,' || CHR(10) || '' ||
           'NAB_MNOZNIK_MIN,' || CHR(10) || '' ||
           'NAB_UZASADNIENIE,' || CHR(10) || '' ||
           'NAB_UWAGI,' || CHR(10) || '' ||
           'NAB_NR_OGL_BIP,' || CHR(10) || '' ||
           'NAB_DATA_PUBLIKACJI,' || CHR(10) || '' ||
           'NAB_DATA_ZAKONC,' || CHR(10) || '' ||
           'NAB_TERMIN_SKL_OF,' || CHR(10) || '' ||
           'NAB_CZY_INF_NIEPELN,' || CHR(10) || '' ||
           'NAB_CZY_INF_WSK_NIEPELN,' || CHR(10) || '' ||
           'NAB_CZY_INF_CDZ,' || CHR(10) || '' ||
           'NAB_MNOZNIK_MAX,' || CHR(10) || '' ||
           'NAB_WYNIK,' || CHR(10) || '' ||
           'NAB_WAKAT_POTW,' || CHR(10) || '' ||
           'NAB_DATA_WOLNE_OD,' || CHR(10) || '' ||
           'NAB_ETAT_ZWYK,' || CHR(10) || '' ||
           'NAB_ETAT_ZASTEPSTWO,' || CHR(10) || '' ||
           'NAB_ETAT_REZERWA,' || CHR(10) || '' ||
           'NAB_ETAT_PROJEKT,' || CHR(10) || '' ||
           'NAB_OG_ZAKRES_OBOW,' || CHR(10) || '' ||
           'NAB_OG_WYMAGANIA_NIEZB,' || CHR(10) || '' ||
           'NAB_OG_WYMAGANIA_DODAT,' || CHR(10) || '' ||
           'NAB_OG_WYMAGANE_WYKSZ,' || CHR(10) || '' ||
           'NAB_OG_TERMIN_ZGL_EMAIL,' || CHR(10) || '' ||
           'NAB_OG_WYM_DOK,' || CHR(10) || '' ||
           'NAB_OG_TERMIN_ROZM,' || CHR(10) || '' ||
           'NAB_OG_KONTAKT' || CHR(10) || '' ||
           'from ZPT_NABORY, ZPT_STANOWISKA' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           'where zpt_stanowiska.stn_id = ZPT_NABORY.NAB_STN_ID'
,
p_opis => 'Lista wniosk�w'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_STAN_WNIOSKI_OBIEG'
,
p_dlugosc => 0,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Wnioski w obiegu'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_STAN_WNIOSKI_ALL'
,
p_dlugosc => 0,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Wszyskie wnioski'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_slpr_id'
,
p_lp => '1'
,
p_query => ' select ' || CHR(10) || '' ||
           '    zsp.SLPR_ID             pp_slpr_id,' || CHR(10) || '' ||
           '    ek.PRC_IMIE             pp_prc_imie,' || CHR(10) || '' ||
           '    ek.PRC_NAZWISKO         pp_prc_nazwisko,' || CHR(10) || '' ||
           '    ek.PRC_IMIE||'' ''||ek.PRC_NAZWISKO  pp_pracownik,' || CHR(10) || '' ||
           '    ek.PRC_NUMER            pp_prc_numer,' || CHR(10) || '' ||
           '    cop.OB_NAZWA            pp_cop_nazwa,' || CHR(10) || '' ||
           '    zst.STN_NAZWA           pp_zst_nazwa_stanowiska,' || CHR(10) || '' ||
           '    zsp.SLPR_DATA_OD        pp_termin_rozp_sluzby,' || CHR(10) || '' ||
           '    zsp.SLPR_DATA_DO        pp_termin_zak_sluzby,' || CHR(10) || '' ||
           '    zsp.SLPR_AUDYT_DT       pp_data_zlozenia_wniosku,' || CHR(10) || '' ||
           '    zsp.SLPR_OCENA          pp_ocena_sluzby,' || CHR(10) || '' ||
           '    DECODE(zsp.SLPR_OCENA, ''P'',''Pozytywna'',''N'',''Negatywna'')   pp_ocena_sluzby_dec,' || CHR(10) || '' ||
           '    zsp.SLPR_EGZAMIN        pp_ocena_egzamin,' || CHR(10) || '' ||
           '    DECODE(zsp.SLPR_EGZAMIN, ''P'',''Pozytywna'',''N'',''Negatywna'')     pp_ocena_egzamin_dec,' || CHR(10) || '' ||
           '     css_def_st.dstn_opis opis_obiegu' || CHR(10) || '' ||
           '  from EK_PRACOWNICY ek' || CHR(10) || '' ||
           '  JOIN ZPT_SLUZBA_PRZYG zsp    ' || CHR(10) || '' ||
           '  ON zsp.SLPR_PRC_ID = ek.PRC_ID' || CHR(10) || '' ||
           '  JOIN (select zat_prc_id,ZAT_OB_ID, ZAT_STN_ID from EK_ZATRUDNIENIE' || CHR(10) || '' ||
           'group by ZAT_PRC_ID,ZAT_OB_ID, zat_stn_id) ez' || CHR(10) || '' ||
           '  ON ek.prc_id = ez.ZAT_PRC_ID' || CHR(10) || '' ||
           '  LEFT OUTER JOIN CSS_OBIEKTY_W_PRZEDSIEB cop' || CHR(10) || '' ||
           '  ON ez.ZAT_OB_ID = cop.OB_ID' || CHR(10) || '' ||
           '  JOIN ZPT_STANOWISKA zst' || CHR(10) || '' ||
           '  ON zst.STN_ID = ez.ZAT_STN_ID' || CHR(10) || '' ||
           '   and ek.PRC_ID like (:PARAM_1)' || CHR(10) || '' ||
           '   LEFT join  CSST_ENCJE_W_STANACH  css_enc' || CHR(10) || '' ||
           '  on css_enc.EWS_KLUCZ_OBCY_ID = zsp.SLPR_ID' || CHR(10) || '' ||
           '  LEFT join csst_def_stanow css_def_st on  css_def_st.dstn_id = css_enc.ews_dstn_id'
,
p_opis => 'Lista arkuszy'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_KARTOTEKI'
,
p_dlugosc => 50,
p_etykieta =>'Rodzaj naboru'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'NAB_RODZAJ_NABORU'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_JEDNOSTKI'
,
p_dlugosc => 100,
p_etykieta =>'pp_ob_id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_ob_id'
,
p_lp => '1'
,
p_query => 'select' || CHR(10) || '' ||
           '		OB_ID pp_ob_id ' || CHR(10) || '' ||
           '		, OB_NAZWA pp_ob_nazwa  ' || CHR(10) || '' ||
           '	from CSS_OBIEKTY_W_PRZEDSIEB'
,
p_opis => 'Jednostki'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_JEDNOSTKI'
,
p_dlugosc => 100,
p_etykieta =>'pp_ob_nazwa'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_ob_nazwa'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_TESTT'
,
p_dlugosc => 100,
p_etykieta =>'WND_ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '1'
,
p_query => 'SELECT ' || CHR(10) || '' ||
           '		    WND_ID, WND_NUMER, WND_DATA_WYJAZDU, WND_DATA_WNIOSKU, WND_OPIS, WND_PRC_ID_DEL ' || CHR(10) || '' ||
           '		   from DELT_WNIOSKI_DELEGACJI ' || CHR(10) || '' ||
           '		   where WND_F_CZY_GRUPOWA = ''T'''
,
p_opis => 'Delegacje - wnioski'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 0,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_slpr_id'
,
p_lp => '1'
,
p_query => ' select ' || CHR(10) || '' ||
           '    zsp.SLPR_ID             pp_slpr_id,' || CHR(10) || '' ||
           '    ek.PRC_IMIE             pp_prc_imie,' || CHR(10) || '' ||
           '    ek.PRC_NAZWISKO         pp_prc_nazwisko,' || CHR(10) || '' ||
           '    ek.PRC_IMIE||'' ''||ek.PRC_NAZWISKO  pp_pracownik,' || CHR(10) || '' ||
           '    ek.PRC_NUMER            pp_prc_numer,' || CHR(10) || '' ||
           '    cop.OB_NAZWA            pp_cop_nazwa,' || CHR(10) || '' ||
           '    zst.STN_NAZWA           pp_zst_nazwa_stanowiska,' || CHR(10) || '' ||
           '    zsp.SLPR_DATA_OD        pp_termin_rozp_sluzby,' || CHR(10) || '' ||
           '    zsp.SLPR_DATA_DO        pp_termin_zak_sluzby,' || CHR(10) || '' ||
           '    zsp.SLPR_AUDYT_DT       pp_data_zlozenia_wniosku,' || CHR(10) || '' ||
           '    zsp.SLPR_OCENA          pp_ocena_sluzby,' || CHR(10) || '' ||
           '    DECODE(zsp.SLPR_OCENA, ''P'',''Pozytywna'',''N'',''Negatywna'')   pp_ocena_sluzby_dec,' || CHR(10) || '' ||
           '    zsp.SLPR_EGZAMIN        pp_ocena_egzamin,' || CHR(10) || '' ||
           '    DECODE(zsp.SLPR_EGZAMIN, ''P'',''Pozytywna'',''N'',''Negatywna'')     pp_ocena_egzamin_dec,' || CHR(10) || '' ||
           '    css_def_st.dstn_opis opis_obiegu' || CHR(10) || '' ||
           '  from EK_PRACOWNICY ek' || CHR(10) || '' ||
           '  JOIN ZPT_SLUZBA_PRZYG zsp    ' || CHR(10) || '' ||
           '  ON zsp.SLPR_PRC_ID = ek.PRC_ID' || CHR(10) || '' ||
           '  JOIN (select zat_prc_id,ZAT_OB_ID, ZAT_STN_ID from EK_ZATRUDNIENIE' || CHR(10) || '' ||
           'group by ZAT_PRC_ID,ZAT_OB_ID, zat_stn_id) ez' || CHR(10) || '' ||
           '  ON ek.prc_id = ez.ZAT_PRC_ID' || CHR(10) || '' ||
           '  LEFT OUTER JOIN CSS_OBIEKTY_W_PRZEDSIEB cop' || CHR(10) || '' ||
           '  ON ez.ZAT_OB_ID = cop.OB_ID' || CHR(10) || '' ||
           '  JOIN ZPT_STANOWISKA zst' || CHR(10) || '' ||
           '  ON zst.STN_ID = ez.ZAT_STN_ID' || CHR(10) || '' ||
           ' and ek.PRC_ID like ''%'' join (select * from CSST_ENCJE_W_STANACH WHERE EWS_KOB_KOD = ''WN_SLPR'' ) css_enc' || CHR(10) || '' ||
           '  on css_enc.EWS_KLUCZ_OBCY_ID = zsp.SLPR_ID' || CHR(10) || '' ||
           '  join csst_def_stanow css_def_st on  css_def_st.dstn_id = css_enc.ews_dstn_id'
,
p_opis => 'Arkusze w obiegu'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCE_LISTA_PLAN'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Lista planowanych ocen'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_RCP_WNIOSKI'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Wnioski RCP w obiegu'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_URL_WNIOSKI'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Wnioski urlopowe w obiegu'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_RODZINA'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_czr_id'
,
p_lp => '1'
,
p_query => 'select' || CHR(10) || '' ||
           '        CZR_ID              pp_czr_id,' || CHR(10) || '' ||
           '        czr_prc_id          pp_prc_id ,' || CHR(10) || '' ||
           '        czr_pesel           pp_czr_pesel , ' || CHR(10) || '' ||
           '        czr_imie            pp_czr_imie ,' || CHR(10) || '' ||
           '        czr_nazwisko        pp_czr_nazwisko ,' || CHR(10) || '' ||
           '        czr_imie || '' '' || czr_nazwisko czlonek_rodziny,' || CHR(10) || '' ||
           '        czr_data_urodzenia  pp_czr_data_urodzenia, ' || CHR(10) || '' ||
           '        rv_meaning          pp_czr_pokrewienstwo ,' || CHR(10) || '' ||
           '       	to_char(czr_uzysk_ub, ''DD-MM-YYYY'')  pp_czr_uzysk_ub , ' || CHR(10) || '' ||
           '   		to_char(czr_utrata_ub,''DD-MM-YYYY'')  pp_czr_utrata_ub ,  ' || CHR(10) || '' ||
           '    	(SELECT ek_pck_adresy.adres(adr_id) FROM CKK_ADRESY WHERE adr_typ=''S'' AND adr_czr_id = czr_id AND Adr_zatwierdzony = ''T'' AND Adr_f_aktualne = ''T'') pp_czr_adres' || CHR(10) || '' ||
           '    	' || CHR(10) || '' ||
           '    from ek_czlonkowie_rodziny, ek_ref_codes ' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           '    where  czr_prc_id like (:POLE_1) ' || CHR(10) || '' ||
           '    and rv_low_value(+) = czr_pokrewienstwo ' || CHR(10) || '' ||
           '        and rv_domain(+) = ''STOPIEN_POKREWIENSTWA'''
,
p_opis => 'Dane kadrowe - rodzina'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_BADANIA'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_bad_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '		eb.bad_id			 pp_bad_id,' || CHR(10) || '' ||
           '		eb.bad_data_waznosci pp_bad_data_waznosci,' || CHR(10) || '' ||
           '		eb.bad_data  		 pp_bad_data_wykonania, ' || CHR(10) || '' ||
           '		eb.bad_opis 		 pp_bad_opis,' || CHR(10) || '' ||
           '		rc.rv_meaning 		 pp_bad_rodzaj' || CHR(10) || '' ||
           '	from ek_badania eb, ek_ref_codes rc' || CHR(10) || '' ||
           '	where eb.bad_prc_id like (:PARAM_1)' || CHR(10) || '' ||
           '		and eb.bad_rodzaj = rc.RV_LOW_VALUE' || CHR(10) || '' ||
           '      and rc.rv_domain = ''RODZAJ BADANIA''' || CHR(10) || '' ||
           '	  and bad_rodzaj = ''O''  ' || CHR(10) || '' ||
           '	' || CHR(10) || '' ||
           '	UNION ' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           '	select ' || CHR(10) || '' ||
           '		eb.bad_id			 pp_bad_id,' || CHR(10) || '' ||
           '		eb.bad_data_waznosci pp_bad_data_waznosci,' || CHR(10) || '' ||
           '		eb.bad_data 		 pp_bad_data_wykonania,' || CHR(10) || '' ||
           '		eb.bad_opis 		 pp_bad_opis,' || CHR(10) || '' ||
           '		rc.rv_meaning 		 pp_bad_rodzaj' || CHR(10) || '' ||
           '	from ek_badania eb, ek_ref_codes rc' || CHR(10) || '' ||
           '	  where eb.bad_prc_id like (:PARAM_2)' || CHR(10) || '' ||
           '	 	and eb.bad_rodzaj = rc.RV_LOW_VALUE' || CHR(10) || '' ||
           '		and rc.rv_domain = ''RODZAJ BADANIA''' || CHR(10) || '' ||
           '		and bad_rodzaj = ''W''  ' || CHR(10) || '' ||
           '		and not exists (select 1 from ek_badania where bad_prc_id like (:PARAM_3) and bad_rodzaj = ''O'' )  ' || CHR(10) || '' ||
           '	order by pp_bad_data_waznosci desc'
,
p_opis => 'Dane kadrowe - badania'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_BANK'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'PP_KB_ID'
,
p_lp => '1'
,
p_query => 'select' || CHR(10) || '' ||
           'PP_KB_ID,' || CHR(10) || '' ||
           'PP_PRC_ID,' || CHR(10) || '' ||
           'PP_KB_NUMER,' || CHR(10) || '' ||
           'PP_KB_NAZBAN,' || CHR(10) || '' ||
           'PP_KB_IDKONTA' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           'from PPV_KONTA_LISTA ' || CHR(10) || '' ||
           'where pp_prc_id like (:PARAM_1)'
,
p_opis => 'Dane p�acowe - konta bankowe'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_STAN_AKT_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Lista opis�w do aktualizacji'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_WNIOSKI'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Zapisy na studia - wnioski w obiegu'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_MOJE'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_lsz_id'
,
p_lp => '1'
,
p_query => 'SELECT		' || CHR(10) || '' ||
           '		listy.LSZ_ID                      pp_lsz_id,' || CHR(10) || '' ||
           '    szkol.SZK_ID                      pp_szk_id,' || CHR(10) || '' ||
           '    szkol.SZK_NAZWA                   pp_szk_nazwa,' || CHR(10) || '' ||
           '    LSZ_DATA_ROZPOCZECIA              pp_lsz_data_rozpoczecia,' || CHR(10) || '' ||
           '    LSZ_DATA_UKONCZENIA               pp_lsz_data_ukonczenia,' || CHR(10) || '' ||
           '    LSZ_CZY_ZALICZYL                  pp_lsz_czy_zaliczyl,' || CHR(10) || '' ||
           '    DECODE(LSZ_CZY_ZALICZYL, ''N'', ''Nie'', ''T'', ''Tak'') pp_lsz_czy_zaliczyl_dec,' || CHR(10) || '' ||
           '    SZK_DATA_DO- SZK_DATA_OD + 1 AS   pp_liczba_dni,' || CHR(10) || '' ||
           '    listy.LSZ_DATA_WYPELNIENIA        pp_lsz_data_wypelnienia' || CHR(10) || '' ||
           '          FROM OST_LISTY_SZKOLEN listy' || CHR(10) || '' ||
           '		JOIN ost_szkolenia szkol' || CHR(10) || '' ||
           '		ON listy.LSZ_SZK_ID    = szkol.SZK_ID' || CHR(10) || '' ||
           '		WHERE listy.LSZ_PRC_ID like (:POLE_1)'
,
p_opis => 'Szkolenia pracownika'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_PLAN'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    OCE_ID                  ,' || CHR(10) || '' ||
           '    OCE_AUDYT_DT      ,' || CHR(10) || '' ||
           '    OCE_DATA_OCENY          ,' || CHR(10) || '' ||
           '    OCE_DATA_OD             ,' || CHR(10) || '' ||
           '    OCE_DATA_DO             ,' || CHR(10) || '' ||
           '    OCE_MAX_DATA_OCENY      ,' || CHR(10) || '' ||
           '    OCE_OPIS                ,' || CHR(10) || '' ||
           '    OCE_OPIS2               ' || CHR(10) || '' ||
           'from ZPT_OCENY'
,
p_opis => 'Plan szkole� pracownika'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_DO_AKCEPT'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_zsz_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    zsz_id pp_zsz_id' || CHR(10) || '' ||
           ', ek.prc_imie || '' '' || ek.prc_nazwisko pp_pracownik' || CHR(10) || '' ||
           ', css_def_st.dstn_opis opis_obiegu' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           '  from EK_PRACOWNICY ek' || CHR(10) || '' ||
           '  JOIN ost_zapisy_szkolen zsz    ' || CHR(10) || '' ||
           '  ON zsz.zsz_PRC_ID = ek.PRC_ID' || CHR(10) || '' ||
           '  join (select * from CSST_ENCJE_W_STANACH WHERE EWS_KOB_KOD = ''WN_SZKOL_ZAPISY'' ) css_enc' || CHR(10) || '' ||
           '  on css_enc.EWS_KLUCZ_OBCY_ID = zsz.zsz_id' || CHR(10) || '' ||
           '  join csst_def_stanow css_def_st on  css_def_st.dstn_id = css_enc.ews_dstn_id'
,
p_opis => 'Szkolenia do akceptacji'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANKIETY'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_lsz_id'
,
p_lp => '1'
,
p_query => 'SELECT ' || CHR(10) || '' ||
           '		listy.lsz_id                                  pp_lsz_id,' || CHR(10) || '' ||
           '		szkol.SZK_ID                                  pp_szk_id,' || CHR(10) || '' ||
           '    szkol.SZK_NAZWA                               pp_szk_nazwa,' || CHR(10) || '' ||
           '    SZK_DATA_DO- SZK_DATA_OD + 1 AS               pp_liczba_dni,' || CHR(10) || '' ||
           '    listy.LSZ_DATA_ROZPOCZECIA                    pp_lsz_data_rozpoczecia,' || CHR(10) || '' ||
           '    listy.LSZ_DATA_UKONCZENIA                     pp_lsz_data_ukonczenia' || CHR(10) || '' ||
           '          ' || CHR(10) || '' ||
           '          FROM OST_LISTY_SZKOLEN listy' || CHR(10) || '' ||
           '		JOIN ost_szkolenia szkol' || CHR(10) || '' ||
           '		ON listy.LSZ_SZK_ID    = szkol.SZK_ID' || CHR(10) || '' ||
           '		WHERE listy.LSZ_PRC_ID like (:POLE_1) and listy.LSZ_DATA_WYPELNIENIA is null'
,
p_opis => 'Ankiety do wype�nienia'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_RODZINA'
,
p_dlugosc => 50,
p_etykieta =>'Pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_prc_id'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_RODZINA'
,
p_dlugosc => 100,
p_etykieta =>'Cz�onek rodziny'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'czlonek_rodziny'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_RODZINA'
,
p_dlugosc => 50,
p_etykieta =>'Imi�'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_czr_imie'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_RODZINA'
,
p_dlugosc => 100,
p_etykieta =>'Nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_czr_nazwisko'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_RODZINA'
,
p_dlugosc => 100,
p_etykieta =>'Data urodzenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_czr_data_urodzenia'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_RODZINA'
,
p_dlugosc => 80,
p_etykieta =>'Pesel'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_czr_pesel'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_RODZINA'
,
p_dlugosc => 400,
p_etykieta =>'Stopie� pokrewie�stwa'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_czr_pokrewienstwo'
,
p_lp => '8'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_RODZINA'
,
p_dlugosc => 100,
p_etykieta =>'Uzyskanie ubezpieczenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_czr_uzysk_ub'
,
p_lp => '9'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_RODZINA'
,
p_dlugosc => 100,
p_etykieta =>'Utrata ubezpieczenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_czr_utrata_ub'
,
p_lp => '10'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_RODZINA'
,
p_dlugosc => 300,
p_etykieta =>'Adres'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_czr_adres'
,
p_lp => '11'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANKIETY'
,
p_dlugosc => 50,
p_etykieta =>'Szkolenie'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_szk_id'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANKIETY'
,
p_dlugosc => 100,
p_etykieta =>'Nazwa szkolenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_nazwa'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANKIETY'
,
p_dlugosc => 50,
p_etykieta =>'Liczba dni'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_liczba_dni'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANKIETY'
,
p_dlugosc => 50,
p_etykieta =>'Data rozpocz�cia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_lsz_data_rozpoczecia'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANKIETY'
,
p_dlugosc => 50,
p_etykieta =>'Data uko�czenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_lsz_data_ukonczenia'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANK_NWYP'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_lsz_id'
,
p_lp => '1'
,
p_query => 'SELECT ' || CHR(10) || '' ||
           '		listy.lsz_id                                  pp_lsz_id,' || CHR(10) || '' ||
           '		szkol.SZK_ID                                  pp_szk_id,' || CHR(10) || '' ||
           '    prac.prc_imie || '' '' || prac.prc_nazwisko     pp_pracownik,' || CHR(10) || '' ||
           '    szkol.SZK_NAZWA                               pp_szk_nazwa,' || CHR(10) || '' ||
           '    SZK_DATA_DO- SZK_DATA_OD + 1 AS               pp_liczba_dni,' || CHR(10) || '' ||
           '    listy.LSZ_DATA_ROZPOCZECIA                    pp_lsz_data_rozpoczecia,' || CHR(10) || '' ||
           '    listy.LSZ_DATA_UKONCZENIA                     pp_lsz_data_ukonczenia' || CHR(10) || '' ||
           '          ' || CHR(10) || '' ||
           '          FROM OST_LISTY_SZKOLEN listy' || CHR(10) || '' ||
           '		JOIN ost_szkolenia szkol' || CHR(10) || '' ||
           '		ON listy.LSZ_SZK_ID    = szkol.SZK_ID' || CHR(10) || '' ||
           '    JOIN EK_PRACOWNICY prac' || CHR(10) || '' ||
           '    ON  prac.PRC_ID = listy.LSZ_PRC_ID' || CHR(10) || '' ||
           '		WHERE listy.LSZ_DATA_WYPELNIENIA is null'
,
p_opis => 'Lista ankiet niewype�nionych'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANK_NWYP'
,
p_dlugosc => 50,
p_etykieta =>'Szkolenie'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_szk_id'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANK_NWYP'
,
p_dlugosc => 100,
p_etykieta =>'Pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pracownik'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANK_NWYP'
,
p_dlugosc => 100,
p_etykieta =>'Nazwa szkolenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_nazwa'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANK_NWYP'
,
p_dlugosc => 50,
p_etykieta =>'Liczba dni'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_liczba_dni'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANK_NWYP'
,
p_dlugosc => 50,
p_etykieta =>'Data rozpocz�cia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_lsz_data_rozpoczecia'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_ANK_NWYP'
,
p_dlugosc => 50,
p_etykieta =>'Data uko�czenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_lsz_data_ukonczenia'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_MOJE'
,
p_dlugosc => 50,
p_etykieta =>'Szkolenie'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_szk_id'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_MOJE'
,
p_dlugosc => 100,
p_etykieta =>'Nazwa szkolenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_nazwa'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_MOJE'
,
p_dlugosc => 50,
p_etykieta =>'Data rozpocz�cia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_lsz_data_rozpoczecia'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_MOJE'
,
p_dlugosc => 50,
p_etykieta =>'Data uko�czenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_lsz_data_ukonczenia'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_MOJE'
,
p_dlugosc => 50,
p_etykieta =>'Zaliczone T/N'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_lsz_czy_zaliczyl'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_MOJE'
,
p_dlugosc => 50,
p_etykieta =>'Zaliczone'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_lsz_czy_zaliczyl_dec'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_MOJE'
,
p_dlugosc => 50,
p_etykieta =>'Liczba dni'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_liczba_dni'
,
p_lp => '8'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_MOJE'
,
p_dlugosc => 50,
p_etykieta =>'Data wype�nienia ankiety'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_lsz_data_wypelnienia'
,
p_lp => '9'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_wn_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           ' zi.iprz_id pp_wn_id,' || CHR(10) || '' ||
           '    zi.iprz_prc_id pp_prc_id,' || CHR(10) || '' ||
           '    ek.prc_imie||'' ''||ek.prc_nazwisko pp_prc_imie_nazwisko,' || CHR(10) || '' ||
           '    ek.prc_imie pp_prc_imie,' || CHR(10) || '' ||
           '    ek.prc_nazwisko pp_prc_nazwisko,' || CHR(10) || '' ||
           '    zi.iprz_audyt_dt pp_data_zl_wn,' || CHR(10) || '' ||
           '    ob.OB_NAZWA         pp_ob_nazwa,' || CHR(10) || '' ||
           '    zi.iprz_audyt_dm pp_data_ost_zm   ,' || CHR(10) || '' ||
           'cds.dstn_opis pp_stan ' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           'from ' || CHR(10) || '' ||
           ' csst_encje_w_stanach ces' || CHR(10) || '' ||
           ', CSST_DEF_STANOW cds' || CHR(10) || '' ||
           ', zpt_iprz zi' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           ' JOIN  ek_pracownicy ek' || CHR(10) || '' ||
           '  ON ek.prc_id = zi.iprz_prc_id' || CHR(10) || '' ||
           '  JOIN (select zat_prc_id,ZAT_OB_ID from EK_ZATRUDNIENIE' || CHR(10) || '' ||
           'group by ZAT_PRC_ID,ZAT_OB_ID) ez' || CHR(10) || '' ||
           '  ON ek.PRC_ID = ez.ZAT_PRC_ID ' || CHR(10) || '' ||
           'LEFT OUTER JOIN CSS_OBIEKTY_W_PRZEDSIEB ob' || CHR(10) || '' ||
           '  ON ez.ZAT_OB_ID = ob.OB_ID' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           'where' || CHR(10) || '' ||
           'zi.iprz_id = ces.EWS_KLUCZ_OBCY_ID' || CHR(10) || '' ||
           'and ces.ews_kob_kod = ''WN_IPRZ_ARKUSZ''' || CHR(10) || '' ||
           'and ces.EWS_DSTN_ID = cds.DSTN_ID'
,
p_opis => 'Arkusze w obiegu'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_LISTA_RAP'
,
p_dlugosc => 100,
p_etykieta =>'Nazwa raportu'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_rap_nazwa'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_RODZAJE'
,
p_dlugosc => 50,
p_etykieta =>'KOD'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'WSL_WARTOSC'
,
p_lp => '1'
,
p_query => 'select  ' || CHR(10) || '' ||
           'WSL_WARTOSC,' || CHR(10) || '' ||
           'WSL_ALIAS,' || CHR(10) || '' ||
           'WSL_OPIS' || CHR(10) || '' ||
           'from CSS_WARTOSCI_SLOWNIKOW' || CHR(10) || '' ||
           'where WSL_SL_NAZWA = ''REK_RODZAJ'''
,
p_opis => 'Rodzaje nabor�w'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_RODZAJE'
,
p_dlugosc => 50,
p_etykieta =>'Alias'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'WSL_ALIAS'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_RODZAJE'
,
p_dlugosc => 200,
p_etykieta =>'Rodzaj naboru'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'WSL_OPIS'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_LISTA_RAP'
,
p_dlugosc => 50,
p_etykieta =>'RAP_ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_rap_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           'RAP_ID        pp_rap_id,' || CHR(10) || '' ||
           'RAP_NAZWA     pp_rap_nazwa,' || CHR(10) || '' ||
           'RAP_KATEGORIA pp_rap_kategoria,' || CHR(10) || '' ||
           'RAP_SCHEMAT   pp_rap_schemat' || CHR(10) || '' ||
           'from css_raporty' || CHR(10) || '' ||
           'where RAP_KATEGORIA like ''EK%''' || CHR(10) || '' ||
           'and RAP_KATEGORIA like ''%'' || :POLE_1 || ''%'''
,
p_opis => 'Lista raport�w'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_LISTA_RAP'
,
p_dlugosc => 50,
p_etykieta =>'Kategoria'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_rap_kategoria'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_LISTA_RAP'
,
p_dlugosc => 50,
p_etykieta =>'Schemat'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_rap_schemat'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCENA_PRC'
,
p_dlugosc => 50,
p_etykieta =>'Data oceny'
,
p_f_zwrot => 'N'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'DATA_OCENY'
,
p_lp => '8'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_NABOR'
,
p_dlugosc => 70,
p_etykieta =>'Oferta pracy'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_nab_nr_oferty_prc'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_NABOR'
,
p_dlugosc => 70,
p_etykieta =>'Ogloszenie BIP'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_nab_nr_ogl_bip'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_NABOR'
,
p_dlugosc => 50,
p_etykieta =>'Nabor'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_nab_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           'nab.NAB_ID pp_nab_id,' || CHR(10) || '' ||
           'nab.NAB_NR_OFERTY_PRC pp_nab_nr_oferty_prc,' || CHR(10) || '' ||
           'nab.NAB_NR_OGL_BIP pp_nab_nr_ogl_bip,' || CHR(10) || '' ||
           'stn.STN_NAZWA pp_stn_nazwa,' || CHR(10) || '' ||
           '0,0,0,0' || CHR(10) || '' ||
           'from ZPT_NABORY nab,ZPT_STANOWISKA stn' || CHR(10) || '' ||
           'where ' || CHR(10) || '' ||
           'stn.STN_ID(+)=nab.NAB_STN_ID' || CHR(10) || '' ||
           'AND to_char(nab.NAB_ID) LIKE :POLE_1' || CHR(10) || '' ||
           'AND NVL(UPPER(nab.NAB_NR_OFERTY_PRC), ''%'') LIKE UPPER(:POLE_2)' || CHR(10) || '' ||
           'AND NVL(to_char(nab.NAB_NR_OGL_BIP), ''%'') LIKE :POLE_3' || CHR(10) || '' ||
           'AND NVL(UPPER(stn.STN_NAZWA), ''%'') LIKE UPPER(:POLE_4)'
,
p_opis => 'Nabor na stanowisko'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLUZBA_PRZYG'
,
p_dlugosc => 60,
p_etykieta =>'Data od'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'SLUZBA_OD'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLUZBA_PRZYG'
,
p_dlugosc => 60,
p_etykieta =>'Data do'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'SLUZBA_DO'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_umowa_id'
,
p_lp => '1'
,
p_query => 'select' || CHR(10) || '' ||
           '  ou.UMSZ_ID                                pp_umowa_id,' || CHR(10) || '' ||
           '  ek.prc_imie || '' '' || ek.prc_nazwisko   pp_prc_imie_nazwisko,' || CHR(10) || '' ||
           '  ou.UMSZ_DATA_PODPISANIA                   pp_data_podpisania,' || CHR(10) || '' ||
           '  ou.UMSZ_DATA_OBOWIAZYWANIA                pp_data_obowiazywania,' || CHR(10) || '' ||
           '  cob.OB_NAZWA                              pp_jorg,' || CHR(10) || '' ||
           '  ou.UMSZ_CZY_UREGULOWANO                   pp_umsz_czy_uregulowano,' || CHR(10) || '' ||
           '  DECODE(ou.UMSZ_CZY_UREGULOWANO, ''T'', ''Tak'', ''N'',''Nie'') pp_umsz_czy_uregulowano_dec' || CHR(10) || '' ||
           'from ost_umowy ou' || CHR(10) || '' ||
           'JOIN ek_pracownicy ek' || CHR(10) || '' ||
           'ON ou.umsz_prc_id = ek.prc_id' || CHR(10) || '' ||
           'and ou.umsz_prc_id like (:PARAM_1)' || CHR(10) || '' ||
           'JOIN ek_zatrudnienie ez' || CHR(10) || '' ||
           'ON ez.ZAT_PRC_ID = ek.prc_id ' || CHR(10) || '' ||
           'JOIN CSS_OBIEKTY_W_PRZEDSIEB cob' || CHR(10) || '' ||
           'ON cob.OB_ID = ez.zat_ob_id'
,
p_opis => 'Lista um�w lojalno�ciowych'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_NROZ'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_umowa_id'
,
p_lp => '1'
,
p_query => 'select' || CHR(10) || '' ||
           '  ou.UMSZ_ID                                pp_umowa_id,' || CHR(10) || '' ||
           '  ek.prc_imie || '' '' || ek.prc_nazwisko   pp_prc_imie_nazwisko,' || CHR(10) || '' ||
           '  ou.UMSZ_DATA_PODPISANIA                   pp_data_podpisania,' || CHR(10) || '' ||
           '  ou.UMSZ_DATA_OBOWIAZYWANIA                pp_data_obowiazywania,' || CHR(10) || '' ||
           '  cob.OB_NAZWA                              pp_jorg,' || CHR(10) || '' ||
           '  ou.UMSZ_CZY_UREGULOWANO                   pp_umsz_czy_uregulowano,' || CHR(10) || '' ||
           '  DECODE(ou.UMSZ_CZY_UREGULOWANO, ''T'', ''Tak'', ''N'',''Nie'') pp_umsz_czy_uregulowano_dec' || CHR(10) || '' ||
           'from ost_umowy ou' || CHR(10) || '' ||
           'JOIN ek_pracownicy ek' || CHR(10) || '' ||
           'ON ou.umsz_prc_id = ek.prc_id' || CHR(10) || '' ||
           'and ou.umsz_prc_id like (:PARAM_1)' || CHR(10) || '' ||
           'and ou.umsz_czy_uregulowano = ''N''' || CHR(10) || '' ||
           'JOIN ek_zatrudnienie ez' || CHR(10) || '' ||
           'ON ez.ZAT_PRC_ID = ek.prc_id ' || CHR(10) || '' ||
           'JOIN CSS_OBIEKTY_W_PRZEDSIEB cob' || CHR(10) || '' ||
           'ON cob.OB_ID = ez.zat_ob_id'
,
p_opis => 'Lista um�w lojalno�ciowych nierozliczonych'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_NROZ'
,
p_dlugosc => 100,
p_etykieta =>'Pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_imie_nazwisko'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_NROZ'
,
p_dlugosc => 50,
p_etykieta =>'Data podpisania'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_data_podpisania'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_NROZ'
,
p_dlugosc => 50,
p_etykieta =>'Data obowi�zywania'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_data_obowiazywania'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_NROZ'
,
p_dlugosc => 50,
p_etykieta =>'Jednostka organizacyjna'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_jorg'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_NROZ'
,
p_dlugosc => 50,
p_etykieta =>'Rozliczono T/N'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_umsz_czy_uregulowano'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_NROZ'
,
p_dlugosc => 50,
p_etykieta =>'Rozliczono'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_umsz_czy_uregulowano_dec'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_ROZ'
,
p_dlugosc => 50,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_umowa_id'
,
p_lp => '1'
,
p_query => 'select' || CHR(10) || '' ||
           '  ou.UMSZ_ID                                pp_umowa_id,' || CHR(10) || '' ||
           '  ek.prc_imie || '' '' || ek.prc_nazwisko   pp_prc_imie_nazwisko,' || CHR(10) || '' ||
           '  ou.UMSZ_DATA_PODPISANIA                   pp_data_podpisania,' || CHR(10) || '' ||
           '  ou.UMSZ_DATA_OBOWIAZYWANIA                pp_data_obowiazywania,' || CHR(10) || '' ||
           '  cob.OB_NAZWA                              pp_jorg,' || CHR(10) || '' ||
           '  ou.UMSZ_CZY_UREGULOWANO                   pp_umsz_czy_uregulowano,' || CHR(10) || '' ||
           '  DECODE(ou.UMSZ_CZY_UREGULOWANO, ''T'', ''Tak'', ''N'',''Nie'') pp_umsz_czy_uregulowano_dec' || CHR(10) || '' ||
           'from ost_umowy ou' || CHR(10) || '' ||
           'JOIN ek_pracownicy ek' || CHR(10) || '' ||
           'ON ou.umsz_prc_id = ek.prc_id' || CHR(10) || '' ||
           'and ou.umsz_prc_id like (:PARAM_1)' || CHR(10) || '' ||
           'and ou.umsz_czy_uregulowano = ''T''' || CHR(10) || '' ||
           'JOIN ek_zatrudnienie ez' || CHR(10) || '' ||
           'ON ez.ZAT_PRC_ID = ek.prc_id ' || CHR(10) || '' ||
           'JOIN CSS_OBIEKTY_W_PRZEDSIEB cob' || CHR(10) || '' ||
           'ON cob.OB_ID = ez.zat_ob_id'
,
p_opis => 'Lista um�w lojalno�ciowych rozliczonych'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_ROZ'
,
p_dlugosc => 100,
p_etykieta =>'Pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_imie_nazwisko'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_ROZ'
,
p_dlugosc => 50,
p_etykieta =>'Data podpisania'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_data_podpisania'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_ROZ'
,
p_dlugosc => 50,
p_etykieta =>'Data obowi�zywania'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_data_obowiazywania'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_ROZ'
,
p_dlugosc => 50,
p_etykieta =>'Jednostka organizacyjna'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_jorg'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_ROZ'
,
p_dlugosc => 50,
p_etykieta =>'Rozliczono T/N'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_umsz_czy_uregulowano'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ_ROZ'
,
p_dlugosc => 50,
p_etykieta =>'Rozliczono'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_umsz_czy_uregulowano_dec'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ'
,
p_dlugosc => 100,
p_etykieta =>'Pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_imie_nazwisko'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ'
,
p_dlugosc => 50,
p_etykieta =>'Data podpisania'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_data_podpisania'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ'
,
p_dlugosc => 50,
p_etykieta =>'Data obowi�zywania'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_data_obowiazywania'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ'
,
p_dlugosc => 50,
p_etykieta =>'Jednostka organizacyjna'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_jorg'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ'
,
p_dlugosc => 50,
p_etykieta =>'Rozliczono T/N'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_umsz_czy_uregulowano'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOL_LOJ'
,
p_dlugosc => 50,
p_etykieta =>'Rozliczono'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_umsz_czy_uregulowano_dec'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Imi�'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_prc_imie'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_prc_nazwisko'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 100,
p_etykieta =>'Pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pracownik'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Nr pracownika'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_prc_numer'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Jednostka organizacyjna'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_cop_nazwa'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Stanowisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_zst_nazwa_stanowiska'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Termin rozp. s�u�by'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_termin_rozp_sluzby'
,
p_lp => '8'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Termin zak. s�u�by'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_termin_zak_sluzby'
,
p_lp => '9'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Data z�o�enia wniosku'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_data_zlozenia_wniosku'
,
p_lp => '10'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Ocena s�u�by P/N'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_ocena_sluzby'
,
p_lp => '11'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'Ocena s�u�by'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_ocena_sluzby_dec'
,
p_lp => '12'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_SZKOLENI'
,
p_dlugosc => 100,
p_etykieta =>'pp_szk_od'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_od'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_SZKOLENI'
,
p_dlugosc => 100,
p_etykieta =>'pp_szk_do'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_do'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_SZKOLENI'
,
p_dlugosc => 100,
p_etykieta =>'prc_id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '8'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOLENIA_SPEC'
,
p_dlugosc => 100,
p_etykieta =>'pp_pis_id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOLENIA_SPEC'
,
p_dlugosc => 100,
p_etykieta =>'pp_pis_forma'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SZKOLENIA_SPEC'
,
p_dlugosc => 100,
p_etykieta =>'pp_pis_temat'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC'
,
p_dlugosc => 100,
p_etykieta =>'pp_prc_id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_id'
,
p_lp => '1'
,
p_query => 'SELECT PRC_ID           pp_prc_id ' || CHR(10) || '' ||
           '       ,PRC_NUMER        pp_prc_numer ' || CHR(10) || '' ||
           '       ,INITCAP(PRC_NAZWISKO)     pp_prc_nazwisko ' || CHR(10) || '' ||
           '       ,INITCAP(PRC_IMIE)         pp_prc_imie ' || CHR(10) || '' ||
           '       ,OB_NAZWA         pp_ob_nazwa ' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           '    FROM  EK_PRACOWNICY, EK_ZATRUDNIENIE ek,           CSS_OBIEKTY_W_PRZEDSIEB ' || CHR(10) || '' ||
           ' WHERE PRC_ID = ek.ZAT_PRC_ID(+)  ' || CHR(10) || '' ||
           '   AND ek.ZAT_OB_ID = OB_ID(+) ' || CHR(10) || '' ||
           '   AND(ek.zat_data_zmiany is null or  ek.zat_data_zmiany = ( select MAX(ek1.zat_data_zmiany)  ' || CHR(10) || '' ||
           '                              FROM ek_zatrudnienie ek1 ' || CHR(10) || '' ||
           '                     WHERE ek1.zat_data_zmiany <= TRUNC(sysdate) ' || CHR(10) || '' ||
           '                       AND ek1.zat_prc_id = prc_id))'
,
p_opis => 'Pracownicy'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC'
,
p_dlugosc => 100,
p_etykieta =>'pp_prc_numer'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_numer'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC'
,
p_dlugosc => 100,
p_etykieta =>'pp_prc_nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_nazwisko'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC'
,
p_dlugosc => 100,
p_etykieta =>'pp_prc_imie'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_imie'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC'
,
p_dlugosc => 100,
p_etykieta =>'pp_prc_nazwa'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_ob_nazwa'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_TEST_COMARCH'
,
p_dlugosc => 30,
p_etykieta =>'Data systemowa'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => '<uzupe�nij>'
,
p_lp => '1'
,
p_query => 'select SYSDATE from dual'
,
p_opis => 'Comarch Test'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KART_REK_PRC'
,
p_dlugosc => 100,
p_etykieta =>'pp_prc_id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '1'
,
p_query => 'SELECT PRC_ID           pp_prc_id' || CHR(10) || '' ||
           '      ,PRC_NUMER        pp_prc_numer' || CHR(10) || '' ||
           '      ,PRC_NAZWISKO || '' '' || PRC_IMIE    pp_pracownik' || CHR(10) || '' ||
           '      ,OB_NAZWA         pp_ob_nazwa' || CHR(10) || '' ||
           ',0,0,0,0' || CHR(10) || '' ||
           '   FROM  EK_PRACOWNICY, EK_ZATRUDNIENIE ek,           CSS_OBIEKTY_W_PRZEDSIEB' || CHR(10) || '' ||
           'WHERE PRC_ID = ek.ZAT_PRC_ID(+) ' || CHR(10) || '' ||
           '  AND ek.ZAT_OB_ID = OB_ID(+)' || CHR(10) || '' ||
           '  AND(ek.zat_data_zmiany is null or  ek.zat_data_zmiany = ( select MAX(ek1.zat_data_zmiany) ' || CHR(10) || '' ||
           '                             FROM ek_zatrudnienie ek1' || CHR(10) || '' ||
           '                    WHERE ek1.zat_data_zmiany <= TRUNC(sysdate)' || CHR(10) || '' ||
           '                      AND ek1.zat_prc_id = prc_id))'
,
p_opis => 'Kartoteka rekrutacji pracownika'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KART_REK_PRC'
,
p_dlugosc => 100,
p_etykieta =>'pp_prc_numer'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KART_REK_PRC'
,
p_dlugosc => 100,
p_etykieta =>'pp_pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KART_REK_PRC'
,
p_dlugosc => 100,
p_etykieta =>'pp_ob_nazwa'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCE_TYPOCENY'
,
p_dlugosc => 50,
p_etykieta =>'Kod'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'WSL_WARTOSC'
,
p_lp => '1'
,
p_query => 'select WSL_WARTOSC, WSL_ALIAS' || CHR(10) || '' ||
           'from CSS_WARTOSCI_SLOWNIKOW' || CHR(10) || '' ||
           'where WSL_SL_NAZWA like ''OCE_TYP_OCENY'''
,
p_opis => 'Typy ocen okresowych'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCE_TYPOCENY'
,
p_dlugosc => 300,
p_etykieta =>'Opis'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'WSL_ALIAS'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_STANOWISKA'
,
p_dlugosc => 0,
p_etykieta =>'Id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_stn_id'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '  stn_id pp_stn_id' || CHR(10) || '' ||
           '  ' || CHR(10) || '' ||
           '  , stn_nazwa pp_stn_nazwa' || CHR(10) || '' ||
           '  ' || CHR(10) || '' ||
           'from zpt_stanowiska' || CHR(10) || '' ||
           'where stn_stan_definicji = ''U''' || CHR(10) || '' ||
           ''
,
p_opis => 'Lista stanowisk'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_STANOWISKA'
,
p_dlugosc => 50,
p_etykieta =>'Stanowisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_stn_nazwa'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCENA_PRC'
,
p_dlugosc => 50,
p_etykieta =>'Ocena'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCENA_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    zo.oce_id OCENA_ID,' || CHR(10) || '' ||
           '    ek.PRC_IMIE  OCENIANY_IMIE,' || CHR(10) || '' ||
           '    ek.PRC_NAZWISKO   OCENIANY_NAZWISKO,  ' || CHR(10) || '' ||
           '    ek_zat1.STN_NAZWA STANOWISKO_OCENIANY,' || CHR(10) || '' ||
           '    ek2.PRC_IMIE  OCENIAJACY_IMIE,' || CHR(10) || '' ||
           '    ek2.PRC_NAZWISKO OCENIAJACY_NAZWISKO,' || CHR(10) || '' ||
           '    ek_zat2.STN_NAZWA STANOWISKO_OCENIAJACY,' || CHR(10) || '' ||
           '    zo.OCE_DATA_OCENY DATA_OCENY' || CHR(10) || '' ||
           '  from ZPT_OCENY zo, ' || CHR(10) || '' ||
           '       EK_PRACOWNICY ek, ' || CHR(10) || '' ||
           '       EK_PRACOWNICY ek2,' || CHR(10) || '' ||
           '      (select ' || CHR(10) || '' ||
           '       ek_zat.ZAT_PRC_ID,' || CHR(10) || '' ||
           '       zpt_sta.STN_NAZWA' || CHR(10) || '' ||
           '       from EK_ZATRUDNIENIE ek_zat,' || CHR(10) || '' ||
           '            ZPT_STANOWISKA zpt_sta' || CHR(10) || '' ||
           '       where ek_zat.ZAT_STN_ID=zpt_sta.STN_ID' || CHR(10) || '' ||
           '       group by ek_zat.ZAT_PRC_ID, zpt_sta.STN_NAZWA' || CHR(10) || '' ||
           '      ) ek_zat1,' || CHR(10) || '' ||
           '      (select ' || CHR(10) || '' ||
           '       ek_zat.ZAT_PRC_ID,' || CHR(10) || '' ||
           '       zpt_sta.STN_NAZWA' || CHR(10) || '' ||
           '       from EK_ZATRUDNIENIE ek_zat,' || CHR(10) || '' ||
           '            ZPT_STANOWISKA zpt_sta' || CHR(10) || '' ||
           '       where ek_zat.ZAT_STN_ID=zpt_sta.STN_ID' || CHR(10) || '' ||
           '       group by ek_zat.ZAT_PRC_ID, zpt_sta.STN_NAZWA' || CHR(10) || '' ||
           '      ) ek_zat2' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           '  where zo.OCE_PRC_ID = ek.PRC_ID' || CHR(10) || '' ||
           '    and zo.OCE_PRC_ID_OCENIAJACY = ek2.PRC_ID' || CHR(10) || '' ||
           '    and ek_zat1.ZAT_PRC_ID=ek.PRC_ID' || CHR(10) || '' ||
           '    and ek_zat2.ZAT_PRC_ID=ek2.PRC_ID' || CHR(10) || '' ||
           ' AND zo.oce_id LIKE :POLE_1' || CHR(10) || '' ||
           ' and NVL(upper(ek.PRC_IMIE),''%'') LIKE upper(:POLE_2)' || CHR(10) || '' ||
           ' and NVL(upper(ek.PRC_NAZWISKO),''%'') LIKE upper(:POLE_3)' || CHR(10) || '' ||
           ' and NVL(upper(ek_zat1.STN_NAZWA),''%'') LIKE upper(:POLE_4)' || CHR(10) || '' ||
           ' and NVL(upper(ek2.PRC_IMIE),''%'') LIKE upper(:POLE_5)' || CHR(10) || '' ||
           ' and NVL(upper(ek2.PRC_NAZWISKO),''%'') LIKE upper(:POLE_6)' || CHR(10) || '' ||
           ' and NVL(upper(ek_zat2.STN_NAZWA),''%'') LIKE upper(:POLE_7)' || CHR(10) || '' ||
           ' and NVL(to_char(zo.OCE_DATA_OCENY),''%'') LIKE upper(:POLE_8)'
,
p_opis => 'Ocena pracownika'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCENA_PRC'
,
p_dlugosc => 100,
p_etykieta =>'Imie'
,
p_f_zwrot => 'N'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCENIANY_IMIE'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCENA_PRC'
,
p_dlugosc => 100,
p_etykieta =>'Nazwisko'
,
p_f_zwrot => 'N'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCENIANY_NAZWISKO'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCENA_PRC'
,
p_dlugosc => 100,
p_etykieta =>'Stanowisko'
,
p_f_zwrot => 'N'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'STANOWISKO_OCENIANY'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCENA_PRC'
,
p_dlugosc => 110,
p_etykieta =>'Imie oceniajacego'
,
p_f_zwrot => 'N'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCENIAJACY_IMIE'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCENA_PRC'
,
p_dlugosc => 110,
p_etykieta =>'Nazwisko oceniajacego'
,
p_f_zwrot => 'N'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCENIAJACY_NAZWISKO'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCENA_PRC'
,
p_dlugosc => 110,
p_etykieta =>'Stanowisko oceniajacego'
,
p_f_zwrot => 'N'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'STANOWISKO_OCENIAJACY'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KANDYDACI_REK'
,
p_dlugosc => 100,
p_etykieta =>'Identyfikator kandydata'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'KA_ID'
,
p_lp => '1'
,
p_query => 'select' || CHR(10) || '' ||
           'KA_ID,' || CHR(10) || '' ||
           'KA_NAB_ID,' || CHR(10) || '' ||
           'KA_IMIE,' || CHR(10) || '' ||
           'KA_NAZWISKO,' || CHR(10) || '' ||
           'KA_PLEC,0,0,0' || CHR(10) || '' ||
           'from ZPT_KANDYDACI' || CHR(10) || '' ||
           'where to_char(KA_NAB_ID) LIKE :POLE_1' || CHR(10) || '' ||
           'and to_char(KA_ID) LIKE :POLE_2'
,
p_opis => 'Lista kandydatow do rekrutacji'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KANDYDACI_REK'
,
p_dlugosc => 100,
p_etykieta =>'Identyfikator naboru'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'KA_NAB_ID'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_WYKLADOWCY'
,
p_dlugosc => 50,
p_etykieta =>'Imie i nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_wyk_osoba'
,
p_lp => '1'
,
p_query => 'select distinct' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           ' wyk.SWYK_NAZWISKO || '' '' || wyk.SWYK_IMIE pp_wyk_osoba,' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           'wyk.SWYK_STOPIEN_NAUK pp_stopien,' || CHR(10) || '' ||
           'wyk.SWYK_TYTUL pp_tytul,' || CHR(10) || '' ||
           'wyk.SWYK_ID pp_wyk_id' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           'from OST_WYKLADOWCY wyk, ost_listy_szkolen ols ' || CHR(10) || '' ||
           'where wyk.SWYK_SZK_ID = ols.LSZ_SZK_ID' || CHR(10) || '' ||
           'and ols.LSZ_PRC_ID = :POLE_1' || CHR(10) || '' ||
           'and ols.LSZ_ID = :POLE_2'
,
p_opis => 'Lista wykladowcow'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_WYKLADOWCY'
,
p_dlugosc => 50,
p_etykieta =>'Stopien naukowy'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_stopien'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_WYKLADOWCY'
,
p_dlugosc => 50,
p_etykieta =>'Tytu�'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_tytul'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_WYKLADOWCY'
,
p_dlugosc => 50,
p_etykieta =>'Id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_wyk_id'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_IM_NAZ_LACZNIE'
,
p_dlugosc => 100,
p_etykieta =>'ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_id'
,
p_lp => '1'
,
p_query => 'SELECT PRC_ID           pp_prc_id' || CHR(10) || '' ||
           '      ,PRC_NUMER        pp_prc_numer' || CHR(10) || '' ||
           '      ,PRC_NAZWISKO || '' '' || PRC_IMIE    pp_pracownik' || CHR(10) || '' ||
           '      ,OB_NAZWA         pp_ob_nazwa' || CHR(10) || '' ||
           '   FROM  EK_PRACOWNICY, EK_ZATRUDNIENIE ek,           CSS_OBIEKTY_W_PRZEDSIEB' || CHR(10) || '' ||
           '   WHERE PRC_ID = ek.ZAT_PRC_ID(+) ' || CHR(10) || '' ||
           '      AND ek.ZAT_OB_ID = OB_ID(+)' || CHR(10) || '' ||
           '      AND(ek.zat_data_zmiany is null or  ek.zat_data_zmiany = ( select                 MAX(ek1.zat_data_zmiany) ' || CHR(10) || '' ||
           '             FROM ek_zatrudnienie ek1' || CHR(10) || '' ||
           '                    WHERE ek1.zat_data_zmiany <= TRUNC(sysdate)' || CHR(10) || '' ||
           '                      AND ek1.zat_prc_id = prc_id))'
,
p_opis => 'Lista pracownikow, imi� i nazwisko ��cznie w 1 kolumnie'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_IM_NAZ_LACZNIE'
,
p_dlugosc => 100,
p_etykieta =>'Numer ewidencyjny pracownika'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_numer'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_IM_NAZ_LACZNIE'
,
p_dlugosc => 100,
p_etykieta =>'Imi� i nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pracownik'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_IM_NAZ_LACZNIE'
,
p_dlugosc => 100,
p_etykieta =>'Nazwa'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_ob_nazwa'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_NIEOBECNOSCI'
,
p_dlugosc => 100,
p_etykieta =>'Id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_abs_id'
,
p_lp => '1'
,
p_query => 'select' || CHR(10) || '' ||
           '    AB_ID			pp_abs_id, ' || CHR(10) || '' ||
           '    AB_DATA_DO			pp_abs_data_do,' || CHR(10) || '' ||
           '    AB_DATA_OD              pp_abs_data_od,' || CHR(10) || '' ||
           '    AB_DNI_WYKORZYSTANE		pp_abs_ilosc_dni,' || CHR(10) || '' ||
           '    rda_nazwa			pp_abs_nazwa' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           '    from ek_absencje, ek_rodzaje_absencji' || CHR(10) || '' ||
           '    where ab_rda_id = rda_id ' || CHR(10) || '' ||
           '	and ab_prc_id = :POLE_1' || CHR(10) || '' ||
           '	and extract(year from AB_DATA_DO) = nvl(:POLE_2, extract(year from     AB_DATA_DO))' || CHR(10) || '' ||
           'order by AB_DATA_OD desc'
,
p_opis => 'Nieobecno�ci pracownika'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_NIEOBECNOSCI'
,
p_dlugosc => 100,
p_etykieta =>'Data zako�czenia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_abs_data_do'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_NIEOBECNOSCI'
,
p_dlugosc => 100,
p_etykieta =>'Data rozpocz�cia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_abs_data_od'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_NIEOBECNOSCI'
,
p_dlugosc => 100,
p_etykieta =>'Ilo�� dni nieobecno�ci'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_abs_ilosc_dni'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_NIEOBECNOSCI'
,
p_dlugosc => 100,
p_etykieta =>'Rodzaj nieobecno�ci'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_abs_nazwa'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Imi�'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_prc_imie'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_prc_nazwisko'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 100,
p_etykieta =>'Pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_pracownik'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Nr pracownika'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_prc_numer'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Jednostka organizacyjna'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_cop_nazwa'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Stanowisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_zst_nazwa_stanowiska'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Termin rozp. s�u�by'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_termin_rozp_sluzby'
,
p_lp => '8'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Termin zak. s�u�by'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_termin_zak_sluzby'
,
p_lp => '9'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Data z�o�enia wniosku'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_data_zlozenia_wniosku'
,
p_lp => '10'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Ocena s�u�by P/N'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_ocena_sluzby'
,
p_lp => '11'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Ocena s�u�by'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_ocena_sluzby_dec'
,
p_lp => '12'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Ocena egzaminu P/N'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_ocena_egzamin'
,
p_lp => '13'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Ocena egzaminu'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_ocena_egzamin_dec'
,
p_lp => '14'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_SLPR_OBIEG'
,
p_dlugosc => 15,
p_etykieta =>'Stan obiegu'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'opis_obiegu'
,
p_lp => '15'
,
p_query => ''
,
p_opis => 'Stan obiegu'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_BANK'
,
p_dlugosc => 50,
p_etykieta =>'Nr pracownika'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'PP_PRC_ID'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_BANK'
,
p_dlugosc => 100,
p_etykieta =>'Numer konta'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'PP_KB_NUMER'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_KAD_BANK'
,
p_dlugosc => 100,
p_etykieta =>'Nazwa banku'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'PP_KB_NAZBAN'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCE_LISTA'
,
p_dlugosc => 50,
p_etykieta =>'OCENA_ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'OCENA_ID'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '    zo.oce_id OCENA_ID,' || CHR(10) || '' ||
           '    ek.PRC_IMIE || ''  '' || ek.PRC_NAZWISKO   OCENIANY,' || CHR(10) || '' ||
           '    ek_zat1.STN_NAZWA STANOWISKO_OCENIANY,' || CHR(10) || '' ||
           '    ek2.PRC_IMIE || ''  '' || ek2.PRC_NAZWISKO OCENIAJACY,' || CHR(10) || '' ||
           '    ek_zat2.STN_NAZWA STANOWISKO_OCENIAJACY,' || CHR(10) || '' ||
           '    zo.OCE_MAX_DATA_OCENY,' || CHR(10) || '' ||
           '    zo.OCE_DATA_OCENY,  ' || CHR(10) || '' ||
           '    0' || CHR(10) || '' ||
           '  from ZPT_OCENY zo, ' || CHR(10) || '' ||
           '       EK_PRACOWNICY ek, ' || CHR(10) || '' ||
           '       EK_PRACOWNICY ek2,' || CHR(10) || '' ||
           '      (select ' || CHR(10) || '' ||
           '       ek_zat.ZAT_PRC_ID,' || CHR(10) || '' ||
           '       zpt_sta.STN_NAZWA' || CHR(10) || '' ||
           '       from EK_ZATRUDNIENIE ek_zat,' || CHR(10) || '' ||
           '            ZPT_STANOWISKA zpt_sta' || CHR(10) || '' ||
           '       where ek_zat.ZAT_STN_ID=zpt_sta.STN_ID' || CHR(10) || '' ||
           '       group by ek_zat.ZAT_PRC_ID, zpt_sta.STN_NAZWA' || CHR(10) || '' ||
           '      ) ek_zat1,' || CHR(10) || '' ||
           '      (select ' || CHR(10) || '' ||
           '       ek_zat.ZAT_PRC_ID,' || CHR(10) || '' ||
           '       zpt_sta.STN_NAZWA' || CHR(10) || '' ||
           '       from EK_ZATRUDNIENIE ek_zat,' || CHR(10) || '' ||
           '            ZPT_STANOWISKA zpt_sta' || CHR(10) || '' ||
           '       where ek_zat.ZAT_STN_ID=zpt_sta.STN_ID' || CHR(10) || '' ||
           '       group by ek_zat.ZAT_PRC_ID, zpt_sta.STN_NAZWA' || CHR(10) || '' ||
           '      ) ek_zat2' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           '  where zo.OCE_PRC_ID = ek.PRC_ID' || CHR(10) || '' ||
           '    and zo.OCE_PRC_ID_OCENIAJACY = ek2.PRC_ID' || CHR(10) || '' ||
           '    and ek_zat1.ZAT_PRC_ID=ek.PRC_ID' || CHR(10) || '' ||
           '    and ek_zat2.ZAT_PRC_ID=ek2.PRC_ID' || CHR(10) || '' ||
           ' --AND zo.oce_id LIKE :POLE_1' || CHR(10) || '' ||
           ' --and upper(ek.PRC_IMIE) LIKE upper(:POLE_2)' || CHR(10) || '' ||
           ' --and upper(ek.PRC_NAZWISKO) LIKE upper(:POLE_3)' || CHR(10) || '' ||
           ' --and upper(ek_zat1.STN_NAZWA) LIKE upper(:POLE_4)' || CHR(10) || '' ||
           ' --and upper(ek2.PRC_IMIE) LIKE upper(:POLE_5)' || CHR(10) || '' ||
           ' --and upper(ek2.PRC_NAZWISKO) LIKE upper(:POLE_6)' || CHR(10) || '' ||
           ' --and upper(ek_zat2.STN_NAZWA) LIKE upper(:POLE_7)'
,
p_opis => 'Lista ocen pracownika'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCE_LISTA'
,
p_dlugosc => 150,
p_etykieta =>'Oceniany'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCENIANY'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCE_LISTA'
,
p_dlugosc => 150,
p_etykieta =>'Stanowisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'STANOWISKO_OCENIANY'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCE_LISTA'
,
p_dlugosc => 150,
p_etykieta =>'Oceniaj�cy'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCENIAJACY'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCE_LISTA'
,
p_dlugosc => 20,
p_etykieta =>'Termin oceny'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_MAX_DATA_OCENY'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCE_LISTA'
,
p_dlugosc => 20,
p_etykieta =>'Data oceny'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'OCE_DATA_OCENY'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_OCE_LISTA'
,
p_dlugosc => 150,
p_etykieta =>'Stanowisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'STANOWISKO_OCENIAJACY'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_WNIOSKI_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Stanowisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_stanowisko'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_WNIOSKI_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Data publikacji'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_data_publikacji'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_WNIOSKI_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'ID jednostki'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_ob_id'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_WNIOSKI_OBIEG'
,
p_dlugosc => 100,
p_etykieta =>'Nazwa jednostki'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_jednostka'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_REK_WNIOSKI_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Stan obiegu'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_stan'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'ID pracownika'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_prc_id'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_OBIEG'
,
p_dlugosc => 100,
p_etykieta =>'Pracownik'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_prc_imie_nazwisko'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Imi�'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'p_prc_imie'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Nazwisko'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_prc_nazwisko'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Data z�o�enia wniosku'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_data_zl_wn'
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_OBIEG'
,
p_dlugosc => 100,
p_etykieta =>'Jednostka organizacyjna'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_ob_nazwa'
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Data ostatniej zmiany'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => 'pp_data_ost_zm'
,
p_lp => '8'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_IPRZ_OBIEG'
,
p_dlugosc => 50,
p_etykieta =>'Stan obiegu'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_stan'
,
p_lp => '9'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_INNEFORMYZATR'
,
p_dlugosc => 50,
p_etykieta =>'PRC_ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => '<uzupe�nij>'
,
p_lp => '1'
,
p_query => 'select ' || CHR(10) || '' ||
           '' || CHR(10) || '' ||
           'PP_Z_PRC_ID,' || CHR(10) || '' ||
           'PP_Z_ID,' || CHR(10) || '' ||
           'PP_Z_UMOWA_AKT,' || CHR(10) || '' ||
           'PP_Z_DATAPRZYJ,' || CHR(10) || '' ||
           'PP_Z_DATAZMIANY,' || CHR(10) || '' ||
           'PP_Z_DATADO,' || CHR(10) || '' ||
           'null x,' || CHR(10) || '' ||
           'null y' || CHR(10) || '' ||
           '     ' || CHR(10) || '' ||
           'from PPV_ZAT_INNEUMOWY_LISTA' || CHR(10) || '' ||
           'where PP_Z_PRC_ID=:POLE_1'
,
p_opis => 'Inne formy zatrudnienia'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_INNEFORMYZATR'
,
p_dlugosc => 50,
p_etykieta =>'Z_ID'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_INNEFORMYZATR'
,
p_dlugosc => 100,
p_etykieta =>'Umowa aktualna'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_INNEFORMYZATR'
,
p_dlugosc => 200,
p_etykieta =>'Data przyj�cia'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_INNEFORMYZATR'
,
p_dlugosc => 200,
p_etykieta =>'Data zmiany'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_INNEFORMYZATR'
,
p_dlugosc => 200,
p_etykieta =>'Data do'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => ''
,
p_lp => '6'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_INNEFORMYZATR'
,
p_dlugosc => 0,
p_etykieta =>'-'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => ''
,
p_lp => '7'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_INNEFORMYZATR'
,
p_dlugosc => 0,
p_etykieta =>'-'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'T'
,
p_nazwa_pola => ''
,
p_lp => '8'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_SZKOLENI'
,
p_dlugosc => 100,
p_etykieta =>'pp_szk_id'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_id'
,
p_lp => '1'
,
p_query => '  select ' || CHR(10) || '' ||
           '        es.SZK_ID pp_szk_id' || CHR(10) || '' ||
           '		, es.SZK_NAZWA pp_szk_nazwa' || CHR(10) || '' ||
           '		, decode(es.SZK_RODZAJ, jez.WSL_WARTOSC, jez.WSL_ALIAS) pp_szk_rodzaj' || CHR(10) || '' ||
           '		, es.SZK_KOMENTARZ pp_szk_komentarz' || CHR(10) || '' ||
           '		, es.SZK_ORGANIZATOR pp_szk_organizator' || CHR(10) || '' ||
           '		, es.SZK_DATA_OD pp_szk_od' || CHR(10) || '' ||
           '		, es.SZK_DATA_DO pp_szk_do' || CHR(10) || '' ||
           '        , ek.prc_id' || CHR(10) || '' ||
           '    ' || CHR(10) || '' ||
           '        ' || CHR(10) || '' ||
           '    from EK_SZKOLENIA es, CSS_WARTOSCI_SLOWNIKOW jez, EK_KWALIFIKACJE ekw, EK_PRACOWNICY ek' || CHR(10) || '' ||
           '    ' || CHR(10) || '' ||
           '    where jez.WSL_SL_NAZWA = ''RODZAJ_SZKOLENIA''' || CHR(10) || '' ||
           '    and es.SZK_KWA_ID = ekw.KWA_ID' || CHR(10) || '' ||
           '    and ekw.KWA_PRC_ID = ek.PRC_ID' || CHR(10) || '' ||
           '    and es.SZK_RODZAJ = jez.WSL_WARTOSC' || CHR(10) || '' ||
           '    and ek.PRC_ID = :POLE_1'
,
p_opis => 'Szkolenia pracownika'
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_SZKOLENI'
,
p_dlugosc => 100,
p_etykieta =>'pp_szk_nazwa'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_nazwa'
,
p_lp => '2'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_SZKOLENI'
,
p_dlugosc => 100,
p_etykieta =>'pp_szk_rodzaj'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_rodzaj'
,
p_lp => '3'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_SZKOLENI'
,
p_dlugosc => 100,
p_etykieta =>'pp_szk_komentarz'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_komentarz'
,
p_lp => '4'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/
BEGIN
  eap_api_install.dodaj_liste( 
p_nazwa => 'PP_PRC_SZKOLENI'
,
p_dlugosc => 100,
p_etykieta =>'pp_szk_organizator'
,
p_f_zwrot => 'T'
,
p_f_ukryte => 'N'
,
p_nazwa_pola => 'pp_szk_organizator'
,
p_lp => '5'
,
p_query => ''
,
p_opis => ''
);
COMMIT;
END;
/

