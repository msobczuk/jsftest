declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlPracownikSrodkiTrwale';
V_PPSL_QUERY varchar2(4000):='
SELECT distinct 
	sdn.SDN_NUMER_INW pp_numer_inw,
	sdn.SDN_NAZWA pp_nazwa_srodka,
	sdn.SDN_TYP pp_typ_srodka,
	decode(sdn.SDN_F_AMORTYZOWANY,''T'', ''TAK'', ''N'', ''NIE'') pp_amortyzowany,
	sdn.SDN_KOD_KRESKOWY pp_kod_kreskowy,
	sdn.SDN_CHARAKTERYSTYKA pp_charakterystyka,
	sdn.SDN_DATA_NABYCIA pp_data_nabycia,
	sps.PS_PRC_ID pp_prc_id,
	sps.PS_SK_ID pp_sk_id,
	sps.PS_PROCENT pp_ps_procent,
	skk.KL_NAZWA_RODZAJOWA pp_kl_nazwa,
	smu.MU_NAZWA  pp_lokalizacja,
	prc.PRC_NUMER pp_prc_numer,
	concat(prc.prc_imie, concat('' '', prc.prc_nazwisko )) pp_prc_nazwa,
	csk.sk_kod pp_sk_kod,
	csk.SK_OPIS pp_sk_nazwa
from STV_SRODKI_DANE sdn, 
	STT_PODZIALY_SRODKA sps, 
	STT_KLASY_KST skk, 
	STT_MIEJSCA_UZYWANIA smu, 
	EK_PRACOWNICY prc, 
	CSS_STANOWISKA_KOSZTOW csk
where 
	sps.PS_SDN_ID = sdn.SDN_ID
	and sdn.KL_ID = skk.KL_ID
	and sps.PS_MU_ID = smu.MU_ID
	and sps.PS_PRC_ID = prc.PRC_ID
	and sps.PS_SK_ID = csk.SK_ID
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='Lista środków trwałych';
V_PPSL_SCOPE number;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/


declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlPowiadomienia';
V_PPSL_QUERY varchar2(4000):='
select 
	PP_TRESC, 
	to_char(pp_data_powiadomienia, ''YYYY-MM'') pp_pow_okres, 
	pp_wdm_id , 
	pp_data_powiadomienia,
	pp_temat,
	CAST(SUBSTR(pp_tresc,0,50) AS VARCHAR2(50)) pp_tresc_skrot,PP_F_PRZECZYTANA, 
	''N'' CZY_PRZECZYTANA_2
from PPV_POWIADOMIENIA where PP_NAZWA_UZYTKOWNIKA=?
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='Powiadomienia';
V_PPSL_SCOPE number;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlKsLokalizacje';
V_PPSL_QUERY varchar2(4000):='
select 
    L.KLOK_ID,
    L.KLOK_KLOK_ID,
    L.KLOK_KOD,
    L.KLOK_WARTOSC
from  EGADM1.PPT_KSIAZKA_LOKALIZACJE L
-- where 1>0 and 0<10
order by  L.KLOK_KOD, L.KLOK_WARTOSC
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='lokalizacje dla pliku wyszukiwanieFiltr.xhtml';
V_PPSL_SCOPE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlPracownikPremieZatrudnieniaAktualnego';
V_PPSL_QUERY varchar2(4000):='
select 
	PP_IMIE,
	PP_NAZWISKO,
	PP_ZAT_ID,
	PP_ZAT_PRC_ID,
	PP_NAZWA,
	PP_OPIS,
	PP_KWOTA,
	PP_PROCENT,
	PP_MNOZNIK,
	PP_DATA_OD,
	PP_DATA_DO,
	PP_STATUS
from PPV_ZAT_PREMIE
where PP_ZAT_PRC_ID=?
    and PP_ZAT_ID=?
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='Pracownik zatrudnienie';
V_PPSL_SCOPE number;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlPracownikZatrudnienieAktualne';
V_PPSL_QUERY varchar2(4000):='
select 
	PP_Z_PRC_ID,
	PP_Z_ID,
	PP_Z_UMOWA_AKT,
	PP_Z_DATAPRZYJ,
	PP_Z_DATAZMIANY,
	PP_Z_DATADO,
	PP_Z_STAWKA,
	PP_Z_TYP_UMOWY,
	PP_Z_UMOWA,
	PP_Z_TYP_ANAGAZ,
	PP_Z_ANGAZ,
	PP_Z_F_RODZAJ,
	PP_Z_RODZAJ,
	PP_Z_WYMIAR_KOD,
	PP_Z_WYMIAR,
	
	PP_Z_STANOWISKO,
	PP_Z_FUNKCJA,
	PP_Z_ZASZEREGOWANIE,
	PP_Z_PRZELOZONY,
	PP_Z_JED_ORG,
	PP_Z_WAR_SZCZEG
          
from PPV_ZAT_UMOWA_LISTA
where PP_Z_PRC_ID=?
    and PP_Z_UMOWA_AKT=''T''
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='Pracownik zatrudnienie';
V_PPSL_SCOPE number;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlPracownikZatrudnienieHistoria';
V_PPSL_QUERY varchar2(4000):='
select 
	PP_Z_PRC_ID,
	PP_Z_ID,
	PP_Z_UMOWA_AKT,
	PP_Z_DATAPRZYJ,
	PP_Z_DATAZMIANY,
	PP_Z_DATADO,
	PP_Z_STAWKA,
	PP_Z_TYP_UMOWY,
	PP_Z_UMOWA,
	PP_Z_TYP_ANAGAZ,
	PP_Z_ANGAZ,
	PP_Z_F_RODZAJ,
	PP_Z_RODZAJ,
	PP_Z_WYMIAR_KOD,
	PP_Z_WYMIAR,
	
	PP_Z_STANOWISKO,
	PP_Z_FUNKCJA,
	PP_Z_ZASZEREGOWANIE,
	PP_Z_PRZELOZONY,
	PP_Z_JED_ORG,
	PP_Z_WAR_SZCZEG,
	( select count (*) 
	  from PPV_ZAT_PREMIE
	  where PP_ZAT_PRC_ID=PP_Z_PRC_ID
	  and PP_ZAT_ID=PP_Z_ID  
	) PP_Z_PREMIE_CNT
          
from PPV_ZAT_UMOWA_LISTA
where PP_Z_PRC_ID=?
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='Pracownik zatrudnienie';
V_PPSL_SCOPE number;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlListaOcenPrzelozony';
V_PPSL_QUERY varchar2(4000):='
select 
    zo.oce_id OCENA_ID,
    ek.PRC_IMIE || ''  '' || ek.PRC_NAZWISKO   OCENIANY,
    ek_zat1.STN_NAZWA STANOWISKO_OCENIANY,
    ek2.PRC_IMIE || ''  '' || ek2.PRC_NAZWISKO OCENIAJACY,
    ek_zat2.STN_NAZWA STANOWISKO_OCENIAJACY,
    zo.OCE_MAX_DATA_OCENY,
    zo.OCE_DATA_OCENY
    
  from ZPT_OCENY zo, 
       EK_PRACOWNICY ek, 
       EK_PRACOWNICY ek2,
      (select 
       ek_zat.ZAT_PRC_ID,
       zpt_sta.STN_NAZWA
       from EK_ZATRUDNIENIE ek_zat,
            ZPT_STANOWISKA zpt_sta
       where ek_zat.ZAT_STN_ID=zpt_sta.STN_ID
       group by ek_zat.ZAT_PRC_ID, zpt_sta.STN_NAZWA
      ) ek_zat1,
      (select 
       ek_zat.ZAT_PRC_ID,
       zpt_sta.STN_NAZWA
       from EK_ZATRUDNIENIE ek_zat,
            ZPT_STANOWISKA zpt_sta
       where ek_zat.ZAT_STN_ID=zpt_sta.STN_ID
       group by ek_zat.ZAT_PRC_ID, zpt_sta.STN_NAZWA
      ) ek_zat2

  where zo.OCE_PRC_ID = ek.PRC_ID
    and zo.OCE_PRC_ID_OCENIAJACY = ek2.PRC_ID
    and ek_zat1.ZAT_PRC_ID=ek.PRC_ID
    and ek_zat2.ZAT_PRC_ID=ek2.PRC_ID
  	 AND zo.oce_prc_id IN
	(select PRC.PRC_ID
    from EK_PRACOWNICY PRC
        left join EGADM1.EK_PIK_ZATRUDNIENIE PZAT on PRC.PRC_ID = PZAT.PZAT_PRC_ID and PZAT.PZAT_TYP_UMOWY=0
       and sysdate <= NVL(PZAT.PZAT_OKRES_DO,sysdate)
        left join EGADM1.EK_ZATRUDNIENIE ZAT on PZAT.PZAT_ZAT_ID=ZAT.ZAT_ID
    where ZAT.ZAT_PRC_ID_SZEF = ?) 

';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='Lista ocen przelozony';
V_PPSL_SCOPE number;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlParametryRap';
V_PPSL_QUERY varchar2(4000):='
select 
	PAR_ID        pp_rap_id,
	PAR_RAP_ID    pp_par_rap_id,
	RAP_NAZWA     pp_rap_nazwa,
	PAR_NAZWA     pp_par_nazwa,
	PAR_WARTOSC   pp_par_wartosc,
	PAR_LST_ID    pp_par_lst_id,
	LST_NAZWA     pp_lst_nazwa,
	PAR_INTERFEJS pp_par_interfejs,
	PAR_ETYKIETA  pp_par_etykieta,
	PAR_OPIS      pp_par_opis
from css_parametry_raportow
JOIN EAT_LISTY ON PAR_LST_ID = LST_ID
JOIN css_raporty ON PAR_RAP_ID = RAP_ID
where PAR_RAP_ID = ?
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='Raporty';
V_PPSL_SCOPE number;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='PP_OCE_LISTA_PLAN1';
V_PPSL_QUERY varchar2(4000):='
select 
    zo.oce_id OCENA_ID,
    ek.PRC_IMIE || ''  '' || ek.PRC_NAZWISKO   OCENIANY,
    ek_zat1.STN_NAZWA STANOWISKO_OCENIANY,
    ek2.PRC_IMIE || ''  '' || ek2.PRC_NAZWISKO OCENIAJACY,
    ek_zat2.STN_NAZWA STANOWISKO_OCENIAJACY,
    zo.OCE_MAX_DATA_OCENY,
    zo.OCE_DATA_OCENY
    
  from ZPT_OCENY zo, 
       EK_PRACOWNICY ek, 
       EK_PRACOWNICY ek2,
      (select 
       ek_zat.ZAT_PRC_ID,
       zpt_sta.STN_NAZWA
       from EK_ZATRUDNIENIE ek_zat,
            ZPT_STANOWISKA zpt_sta
       where ek_zat.ZAT_STN_ID=zpt_sta.STN_ID
       group by ek_zat.ZAT_PRC_ID, zpt_sta.STN_NAZWA
      ) ek_zat1,
      (select 
       ek_zat.ZAT_PRC_ID,
       zpt_sta.STN_NAZWA
       from EK_ZATRUDNIENIE ek_zat,
            ZPT_STANOWISKA zpt_sta
       where ek_zat.ZAT_STN_ID=zpt_sta.STN_ID
       group by ek_zat.ZAT_PRC_ID, zpt_sta.STN_NAZWA
      ) ek_zat2

  where zo.OCE_PRC_ID = ek.PRC_ID
    and zo.OCE_PRC_ID_OCENIAJACY = ek2.PRC_ID
    and ek_zat1.ZAT_PRC_ID=ek.PRC_ID
    and ek_zat2.ZAT_PRC_ID=ek2.PRC_ID
    AND zo.oce_prc_id_oceniajacy=?
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='Lista ocen oceniajacy, Lista ocen oceniany';
V_PPSL_SCOPE number;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlListaDelegacji';
V_PPSL_QUERY varchar2(4000):='
SELECT WND.WND_ID, WND.WND_DATA_WYJAZDU, WND.WND_DATA_POWROTU, WND.WND_NUMER, WND_POW.WND_NUMER wnd_numer_pow, PRC.PRC_IMIE || '' '' || PRC.PRC_NAZWISKO
FROM  EGADM1.DELT_WNIOSKI_DELEGACJI WND
    LEFT JOIN EGADM1.DELT_CELE_DELEGACJI CDEL ON CDEL.CDEL_WND_ID=WND.WND_ID AND CDEL.CDEL_LP=1
  LEFT JOIN EGADM1.DELT_WNIOSKI_DELEGACJI WND_POW ON WND.WND_WND_ID = WND_POW.WND_ID
  JOIN EK_PRACOWNICY PRC ON PRC.PRC_ID= WND.WND_PRC_ID_DEL
WHERE  WND.WND_PRC_ID_DEL = ? 
ORDER BY  WND.WND_ID DESC
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='Lista delegacji';
V_PPSL_SCOPE number;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlid';
V_PPSL_QUERY varchar2(4000):='select * from dual';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='testowy s�?ownik/zapytanie..............';
V_PPSL_SCOPE number:=1;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='pracownicy';
V_PPSL_QUERY varchar2(4000):='
    select distinct
          PRC.PRC_ID
        , PRC.PRC_IMIE
        , PRC.PRC_NAZWISKO

        , PZAT_STANOWISKO STN_NAZWA
        , PZAT.PZAT_OB_ID || '' - '' || PZAT.PZAT_OB_NAZWA JED_ORG

        , PZAT.PZAT_OB_NAZWA
        , PZAT.PZAT_OB_ID
        , ZAT.ZAT_PRC_ID_SZEF

        --, PZAT.*
        --, ZAT.*
    from EK_PRACOWNICY PRC
        left join EGADM1.EK_PIK_ZATRUDNIENIE PZAT on PRC.PRC_ID = PZAT.PZAT_PRC_ID and PZAT.PZAT_TYP_UMOWY=0 and sysdate <= NVL(PZAT.PZAT_OKRES_DO,sysdate)
        --left join EGADM1.EK_PIK_ZATRUDNIENIE PZAT on PRC.PRC_ID = PZAT.PZAT_PRC_ID and PZAT.PZAT_TYP_UMOWY=0 and sysdate <= NVL(PZAT.PZAT_DATA_DO,sysdate)
        left join EGADM1.EK_ZATRUDNIENIE ZAT on PZAT.PZAT_ZAT_ID=ZAT.ZAT_ID
    order by PRC.PRC_NAZWISKO, PRC.PRC_IMIE  
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=300000;
V_PPSL_DESCRIPTION varchar(1024):='lista pracowników';
V_PPSL_SCOPE number:=0;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlKsTel';
V_PPSL_QUERY varchar2(4000):='
SELECT *
FROM PPV_KSIAZKA_TELEFONICZNA_PRAC
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='Ksiazka telefoniczna';
V_PPSL_SCOPE number:=0;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlDzialy';
V_PPSL_QUERY varchar2(4000):='
SELECT
      OB_ID        PP_DZL_ID
    , OB_KOD     PP_DZL_KOD
    , OB_NAZWA    PP_DZL_NAZWA
FROM EGADM1.EK_OBIEKTY_W_PRZEDSIEB
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='Dzialy / wydzialy';
V_PPSL_SCOPE number:=0;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/



declare
c_procedura varchar2(30):='PPT_ADM_SQLS_dodaj_popraw';
V_PPSL_ID varchar2(50):='sqlPracownikInneFormyZatrudnienia';
V_PPSL_QUERY varchar2(4000):='
select 
	PP_Z_PRC_ID,
	PP_Z_ID,
	PP_Z_UMOWA_AKT,
	PP_Z_DATAPRZYJ,
	PP_Z_DATAZMIANY,
	PP_Z_DATADO,
	PP_Z_STAWKA,
	PP_Z_TYP_UMOWY,
	PP_Z_UMOWA,
	PP_Z_TYP_ANAGAZ,
	PP_Z_ANGAZ,
	PP_Z_F_RODZAJ,
	PP_Z_RODZAJ,
	PP_Z_WYMIAR_KOD,
	PP_Z_WYMIAR,
	
	PP_Z_STANOWISKO,
	PP_Z_FUNKCJA,
	PP_Z_ZASZEREGOWANIE,
	PP_Z_PRZELOZONY,
	PP_Z_JED_ORG,
	PP_Z_WAR_SZCZEG, 
	( select count (*) 
	  from PPV_ZAT_PREMIE
	  where PP_ZAT_PRC_ID=PP_Z_PRC_ID
	  and PP_ZAT_ID=PP_Z_ID  
	) PP_Z_PREMIE_CNT
          
from PPV_ZAT_INNEUMOWY_LISTA
where PP_Z_PRC_ID=?
';
V_PPSL_QUERY_INSERTED varchar2(4000)
;
V_PPSL_QUERY_UPDATED varchar2(4000)
;
V_PPSL_QUERY_DELETED varchar2(4000)
;
V_PPSL_CACHEDURATION number:=1800000;
V_PPSL_DESCRIPTION varchar(1024):='Pracownik zatrudnienie';
V_PPSL_SCOPE number;
p_ile number;
begin
select count(*) into p_ile from PPT_ADM_SQLS where ppsl_id=V_PPSL_ID;
if p_ile=0 then
begin
insert into PPT_ADM_SQLS
(PPSL_ID, PPSL_QUERY, PPSL_QUERY_INSERTED, PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED, PPSL_CACHEDURATION, PPSL_DESCRIPTION, PPSL_SCOPE)
values
(V_PPSL_ID, V_PPSL_QUERY, V_PPSL_QUERY_INSERTED, V_PPSL_QUERY_UPDATED,
V_PPSL_QUERY_DELETED, V_PPSL_CACHEDURATION, V_PPSL_DESCRIPTION, V_PPSL_SCOPE);
COMMIT;
ekp_install.drukuj('Wstawiono polecnie SQL dla moduu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie wstawiania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
if p_ile=1 then
begin
UPDATE PPT_ADM_SQLS SET
PPSL_QUERY=V_PPSL_QUERY,
PPSL_QUERY_INSERTED=V_PPSL_QUERY_INSERTED,
PPSL_QUERY_UPDATED=V_PPSL_QUERY_UPDATED,
PPSL_QUERY_DELETED=V_PPSL_QUERY_DELETED,
PPSL_CACHEDURATION=V_PPSL_CACHEDURATION,
PPSL_DESCRIPTION=V_PPSL_DESCRIPTION,
PPSL_SCOPE=V_PPSL_SCOPE
WHERE PPSL_ID=V_PPSL_ID;
COMMIT;
ekp_install.drukuj('Zmodyfikowano polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
exception
when others then
ROLLBACK;
ekp_install.drukuj('Blad w trakcie modyfikowania polecnie SQL dla modulu Portal Pracowniczy o kodzie: ' || V_PPSL_ID);
eap_blad.zglos (p_procedura => c_procedura);
end;
end if;
end;
/

