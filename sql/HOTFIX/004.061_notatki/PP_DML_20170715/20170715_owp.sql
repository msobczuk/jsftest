DECLARE
c_prf_nazwa varchar2(100):='PPP_DELZ_ADMINISTRATOR';
c_fu_nazwa varchar2(100):='PP_DELZ_ADMINISTRATOR';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_DELZ_ADMINISTRATOR');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_DELZ_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_DELZ_BAD';
c_fu_nazwa varchar2(100):='PP_DELZ_BAD';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_DELZ_BAD');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_DELZ_BAD');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_DELZ_BDG';
c_fu_nazwa varchar2(100):='PP_DELZ_BDG';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_DELZ_BDG');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_DELZ_BDG');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_DELZ_DYR_BDG';
c_fu_nazwa varchar2(100):='PP_DELZ_DYR_BDG';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_DELZ_DYR_BDG');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_DELZ_DYR_BDG');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_DELZ_DYR_KOM';
c_fu_nazwa varchar2(100):='PP_DELZ_DYR_KOM';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_DELZ_DYR_KOM');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_DELZ_DYR_KOM');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_DELZ_DYR_PROJ';
c_fu_nazwa varchar2(100):='PP_DELZ_DYR_PROJ';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_DELZ_DYR_PROJ');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_DELZ_DYR_PROJ');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_DELZ_KOORD_PROJ';
c_fu_nazwa varchar2(100):='PP_DELZ_KOORD_PROJ';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_DELZ_KOORD_PROJ');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_DELZ_KOORD_PROJ');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_DELZ_NACZ_DEL';
c_fu_nazwa varchar2(100):='PP_DELZ_NACZ_DEL';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_DELZ_NACZ_DEL');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_DELZ_NACZ_DEL');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_DELZ_PRACOWNIK';
c_fu_nazwa varchar2(100):='PP_DELZ_PRACOWNIK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_DELZ_PRACOWNIK');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_DELZ_PRACOWNIK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_IPRZ_ADMINISTRATOR';
c_fu_nazwa varchar2(100):='PP_IPRZ_ADMINISTRATOR';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_IPRZ_ADMINISTRATOR');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_IPRZ_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_IPRZ_DG';
c_fu_nazwa varchar2(100):='PP_IPRZ_DG';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_IPRZ_DG');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_IPRZ_DG');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_IPRZ_PRACOWNIK';
c_fu_nazwa varchar2(100):='PP_IPRZ_PRACOWNIK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_IPRZ_PRACOWNIK');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_IPRZ_PRACOWNIK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_IPRZ_PRZELOZONY';
c_fu_nazwa varchar2(100):='PP_IPRZ_PRZELOZONY';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_IPRZ_PRZELOZONY');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_IPRZ_PRZELOZONY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_OCE_ADMINISTRATOR';
c_fu_nazwa varchar2(100):='PP_OCE_ADMINISTRATOR';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_OCE_ADMINISTRATOR');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_OCE_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_OCE_OCENIAJACY';
c_fu_nazwa varchar2(100):='PP_OCE_OCENIAJACY';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_OCE_OCENIAJACY');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_OCE_OCENIAJACY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_OCE_OCENIANY';
c_fu_nazwa varchar2(100):='PP_OCE_OCENIANY';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_OCE_OCENIANY');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_OCE_OCENIANY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_OCE_SEKRETARIAT';
c_fu_nazwa varchar2(100):='PP_OCE_SEKRETARIAT';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_OCE_SEKRETARIAT');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_OCE_SEKRETARIAT');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_OKU_ADMINISTRATOR';
c_fu_nazwa varchar2(100):='PP_OKU_ADMINISTRATOR';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_OKU_ADMINISTRATOR');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_OKU_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_OKU_BHP';
c_fu_nazwa varchar2(100):='PP_OKU_BHP';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_OKU_BHP');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_OKU_BHP');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_OKU_PRACOWNIK';
c_fu_nazwa varchar2(100):='PP_OKU_PRACOWNIK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_OKU_PRACOWNIK');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_OKU_PRACOWNIK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_OKU_PRZELOZONY';
c_fu_nazwa varchar2(100):='PP_OKU_PRZELOZONY';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_OKU_PRZELOZONY');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_OKU_PRZELOZONY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_REK_ADMINISTRATOR';
c_fu_nazwa varchar2(100):='PP_REK_ADMINISTRATOR';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_REK_ADMINISTRATOR');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_REK_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_REK_KADRY';
c_fu_nazwa varchar2(100):='PP_REK_KADRY';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_REK_KADRY');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_REK_KADRY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_REK_PRACOWNIK';
c_fu_nazwa varchar2(100):='PP_REK_PRACOWNIK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_REK_PRACOWNIK');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_REK_PRACOWNIK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_REK_WYNAGRODZENIA';
c_fu_nazwa varchar2(100):='PP_REK_WYNAGRODZENIA';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_REK_WYNAGRODZENIA');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_REK_WYNAGRODZENIA');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_SLPR_ADMINISTRATOR';
c_fu_nazwa varchar2(100):='PP_SLPR_ADMINISTRATOR';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_SLPR_ADMINISTRATOR');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_SLPR_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_SLPR_BDG';
c_fu_nazwa varchar2(100):='PP_SLPR_BDG';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_SLPR_BDG');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_SLPR_BDG');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_SLPR_DG';
c_fu_nazwa varchar2(100):='PP_SLPR_DG';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_SLPR_DG');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_SLPR_DG');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_SLPR_KIER_KOM';
c_fu_nazwa varchar2(100):='PP_SLPR_KIER_KOM';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_SLPR_KIER_KOM');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_SLPR_KIER_KOM');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_SLPR_PRACOWNIK';
c_fu_nazwa varchar2(100):='PP_SLPR_PRACOWNIK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_SLPR_PRACOWNIK');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_SLPR_PRACOWNIK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_SLPR_PRZELOZONY';
c_fu_nazwa varchar2(100):='PP_SLPR_PRZELOZONY';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_SLPR_PRZELOZONY');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_SLPR_PRZELOZONY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_SZKOL_ADMINISTRATOR';
c_fu_nazwa varchar2(100):='PP_SZKOL_ADMINISTRATOR';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_SZKOL_ADMINISTRATOR');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_SZKOL_ADMINISTRATOR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_SZKOL_PRACOWNIK';
c_fu_nazwa varchar2(100):='PP_SZKOL_PRACOWNIK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_SZKOL_PRACOWNIK');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_SZKOL_PRACOWNIK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_SZKOL_PRZELOZONY';
c_fu_nazwa varchar2(100):='PP_SZKOL_PRZELOZONY';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_SZKOL_PRZELOZONY');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_SZKOL_PRZELOZONY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='PPP_SZKOL_WYDZ_SZKOLEN';
c_fu_nazwa varchar2(100):='PP_SZKOL_WYDZ_SZKOLEN';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje PPP_SZKOL_WYDZ_SZKOLEN');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa PP_SZKOL_WYDZ_SZKOLEN');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_KON_ADMWORD';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_KON_ADMWORD');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_KON_REK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_KON_REK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_KON_SLOW_SZK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_KON_SLOW_SZK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_KON_WD';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_KON_WD');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_RAP_RAPWORD';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_RAP_RAPWORD');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_RAP_REK_FORM';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_RAP_REK_FORM');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_RAP_REK_HARM';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_RAP_REK_HARM');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_RAP_REK_RANK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_RAP_REK_RANK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_RAP_SZK_SZK_KAR';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_RAP_SZK_SZK_KAR');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_RAP_SZK_SZK_OCE';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_RAP_SZK_SZK_OCE');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_RAP_SZK_SZK_PLA';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_RAP_SZK_SZK_PLA');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_RAP_SZK_SZK_UMO';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_RAP_SZK_SZK_UMO');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_RAP_SZK_SZK_ZAP';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_RAP_SZK_SZK_ZAP');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_RAP_SZK_SZK_ZAP2';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_RAP_SZK_SZK_ZAP2');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_REK_ANONSE';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_REK_ANONSE');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_REK_ETAPY';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_REK_ETAPY');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_REK_KANDYDAT';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_REK_KANDYDAT');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_REK_WARTKN';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_REK_WARTKN');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_REK_WARTKS';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_REK_WARTKS');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_SERWIS';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_SERWIS');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_STN_OPIS_PRC';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_STN_OPIS_PRC');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_STN_OPIS_STN';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_STN_OPIS_STN');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_STN_PLAN_ZIW';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_STN_PLAN_ZIW');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_STN_SCK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_STN_SCK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_STN_STN_W_JO';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_STN_STN_W_JO');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_SZK_BUD_SZK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_SZK_BUD_SZK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_SZK_KART_SZK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_SZK_KART_SZK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_SZK_LIST_SZK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_SZK_LIST_SZK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='MENU_ZP_SZK_REJ_SZK';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa MENU_ZP_SZK_REJ_SZK');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/
DECLARE
c_prf_nazwa varchar2(100):='Portal Prac. - pe�en dost�p';
c_fu_nazwa varchar2(100):='ZP_FORMATKA_STARTOWA';
v_prf_id number;
v_fu_id number;
v_error boolean:=false;
v_dml_type varchar2(1):='';
BEGIN
begin
select prf_id into v_prf_id from eat_profile where prf_nazwa=c_prf_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje Portal Prac. - pe�en dost�p');
end;
begin
select fu_id into v_fu_id from EAT_FUNKCJE_UZYTKOWE where fu_nazwa=c_fu_nazwa;
exception
when others then
v_error:=true;
eap_blad.zglos(p_procedura=>'import PP: eat_uzycia_w_profilach',
p_dodatkowe_info=>'nie istnieje funkcja u�ytkowa ZP_FORMATKA_STARTOWA');
end;
if not(v_error) then
select decode(count(*),0,'I','U') into v_dml_type from EAT_UZYCIA_W_PROFILACH where uwp_prf_id=v_prf_id and uwp_fu_id=v_fu_id;
if v_dml_type='I' then
INSERT INTO EAT_UZYCIA_W_PROFILACH
(uwp_prf_id, uwp_fu_id)
VALUES
(v_prf_id, v_fu_id);
COMMIT;
end if;
end if;
END;
/

