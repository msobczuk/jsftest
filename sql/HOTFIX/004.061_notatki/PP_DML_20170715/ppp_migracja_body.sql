create or replace PACKAGE BODY PPP_MIGRACJA AS
/* 
 Pakiet do obs�ugi mechanizmu synchronizaji s�ownik�w konfiguracyjnych:
 �rodowisko: DEV -> CONF
 
 Synchronizacja odbywa si� za po�rednictwem Public Databae Lins o nazwie CONF 
 
*/ 

c_package_name varchar2(30):='PPP_MIGRACJA';

/*
 Wstawienie zapisu do mapy migracyjnej
*/
procedure migracja_mapa_wstaw(p_nazwa_tabeli varchar2, 
                              p_nazwa_kolumny varchar2, 
                              p_typ_kolumny varchar2, 
                              p_wartosc_zrodlo varchar2, 
                              p_wartosc_cel varchar2,
                              p_nazwa_kolumny_2 varchar2 default null, 
                              p_typ_kolumny_2 varchar2 default null, 
                              p_wartosc_zrodlo_2 varchar2 default null, 
                              p_wartosc_cel_2 varchar2 default null)
is
begin
 insert into PPT_MIGRACJA_MAPA
 (MMAP_NAZWA_TABELI, 
	MMAP_NAZWA_KOLUMNY, 
	MMAP_TYP_KOLUMNY, 
	MMAP_WARTOSC_ZRODLO, 
	MMAP_WARTOSC_CEL,
  MMAP_NAZWA_KOLUMNY_2, 
	MMAP_TYP_KOLUMNY_2, 
	MMAP_WARTOSC_ZRODLO_2, 
	MMAP_WARTOSC_CEL_2)
 values
 (p_nazwa_tabeli, 
  p_nazwa_kolumny, 
  p_typ_kolumny, 
  p_wartosc_zrodlo, 
  p_wartosc_cel,
  p_nazwa_kolumny_2, 
  p_typ_kolumny_2, 
  p_wartosc_zrodlo_2, 
  p_wartosc_cel_2);  
end migracja_mapa_wstaw; 

/*
 Wstawienie zapisu do tabeli logu migracji
*/
procedure sm(p_nazwa_procedury VARCHAR2 default null,
             p_komunikat VARCHAR2 DEFAULT NULL,
             p_komunikat_rodzaj VARCHAR2 DEFAULT 'I')
as
PRAGMA AUTONOMOUS_TRANSACTION;
begin

INSERT INTO ppt_log
(
 MLOG_NAZWA_PROCEDURY, 
 MLOG_KOMUNIKAT, 
 MLOG_KOMUNIKAT_RODZAJ
) 
VALUES 
(
 p_nazwa_procedury,
 p_komunikat,
 p_komunikat_rodzaj
);
COMMIT;

exception when others then 
  null;
end sm;



/*
 Zarzadzanie danymi tabeli EAT_PROFILE
 Obs�uga zdarze�:
 1. Dodaj nowy profil
 2. Modyfikuj profi
*/
procedure profile_dodaj_modyfikuj
is
begin
 null;
end profile_dodaj_modyfikuj;


/*
 Przeniesienie profili do bazy docelowej
*/
procedure profile_dodaj
is
c_nazwa_procedury varchar2(30):=c_package_name||'.'||'profile_dodaj';
cursor c_profile is
        --Lista "nowych" Profili przypisanych do Portalu a nie przeniesionych
        --do bazy konfiguracyjnej
        select
        prf.PRF_NAZWA,
        prf.PRF_INS_ID,
        prf.PRF_FRM_ID,
        prf.PRF_F_EGERIA,
        prf.PRF_F_KOKPITY,
        prf.PRF_F_PORTAL,
        prf.PRF_F_CONTROLLING,
        prf.PRF_F_GENERATOR,
        prf.PRF_F_EDUKACJA,
        prf.PRF_OPIS
        from EAT_PROFILE prf
        where prf.PRF_F_PORTAL='T'
        and prf.PRF_ID NOT IN (select
                               TO_NUMBER(mmap.MMAP_WARTOSC_ZRODLO)
                               from PPT_MIGRACJA_MAPA mmap
                               where mmap.MMAP_NAZWA_TABELI='EAT_PROFILE'
                               and mmap.MMAP_NAZWA_KOLUMNY='PRF_ID' 
                               and mmap.MMAP_STATUS='A'
                              )
        -- PO TESTACH USUNAC                      
        and ((prf.PRF_NAZWA like 'PPP_TEST_COMARCH%') or (prf.PRF_NAZWA='PPP_DELZ_DYR_KOM') or (prf.PRF_NAZWA='PPP_DELZ_BAD') or (prf.PRF_NAZWA='PPP_DELZ_BDG') or (prf.PRF_NAZWA='PPP_OCE_SEKRETARIAT') or (prf.PRF_NAZWA='EK_WA_BAD') or (prf.PRF_NAZWA='EK_WA_BDG') or (prf.PRF_NAZWA='EK_WA_SEKRETARIAT'))
        --
        MINUS
        select
        prf.PRF_NAZWA,
        prf.PRF_INS_ID,
        prf.PRF_FRM_ID,
        prf.PRF_F_EGERIA,
        prf.PRF_F_KOKPITY,
        prf.PRF_F_PORTAL,
        prf.PRF_F_CONTROLLING,
        prf.PRF_F_GENERATOR,
        prf.PRF_F_EDUKACJA,
        prf.PRF_OPIS
        from EAT_PROFILE@conf prf
        ;
p_prf_id_zrodlo number(10,0);
p_komunikat varchar(100);
v_rec eap_obj_profil.t_profil@conf;
p_prf_rowid_cel ROWID;
p_error boolean:=false;
begin
 sm(c_nazwa_procedury,'Start...');
 for p_prf in c_profile loop
  p_error:=false;
  p_komunikat:='Import profilu '||p_prf.PRF_NAZWA;
  sm(c_nazwa_procedury,p_komunikat||' - Start...');
  --Pobranie identyfikatora - �r�d�o
  select
  prf.prf_id
  into p_prf_id_zrodlo
  from EAADM.EAT_PROFILE prf
  where prf.prf_nazwa=p_prf.prf_nazwa;
  sm(c_nazwa_procedury,p_komunikat||' - identyfikator �r�d�o: '||p_prf_id_zrodlo);
  --Wygenerowanie identyfikatora - cel
  v_rec.prf_id:=eap_utl.pobierz_numer@conf('EAS_PRF');
  sm(c_nazwa_procedury,p_komunikat||' - identyfikator cel: '||v_rec.prf_id);
  
  --Przygotowanie rekordu profilu do wstawienia w bazie docelowej
  v_rec.prf_ins_id := p_prf.prf_ins_id;
  v_rec.prf_frm_id := p_prf.prf_frm_id;
  v_rec.prf_nazwa := p_prf.prf_nazwa;
  v_rec.prf_opis := p_prf.prf_opis;
  v_rec.prf_f_egeria :=p_prf.prf_f_egeria;
  v_rec.prf_f_kokpity :=p_prf.prf_f_kokpity;
  v_rec.prf_f_portal :=p_prf.prf_f_portal;
  v_rec.prf_f_controlling :=p_prf.prf_f_controlling;
  v_rec.prf_f_edukacja := p_prf.prf_f_edukacja;
  v_rec.prf_f_generator := p_prf.prf_f_generator;
  
  --Dodanie nowego profilu w bazie docelowej
  begin
   p_prf_rowid_cel:=eap_obj_profil.wstaw@conf(v_rec);
   sm(c_nazwa_procedury,p_komunikat||' - dodany do bazy docelowej');
  exception
   when others then
   p_error:=true;
   sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
  end;  
  
  --Dodanie wpisu w tabli mapy w bazie �r�d�owej
  if not(p_error) then
    begin
     migracja_mapa_wstaw('EAT_PROFILE','PRF_ID','NUMBER',p_prf_id_zrodlo, v_rec.prf_id);
     sm(c_nazwa_procedury,p_komunikat||' - utworzono rekord mapy');
    exception
     when others then
      p_error:=true;
      sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
    end;  
  end if;  
  
  --Zatwierdzenie lub wycofanie zmian    
  case not(p_error)   
   when true then commit;   
   else rollback;
  end case;   
  
  case not(p_error)
   when true then sm(c_nazwa_procedury,p_komunikat||' - Zako�czony sukcesem');
   else sm(c_nazwa_procedury,p_komunikat||' - Zako�czony b��dem');
  end case;
  
  sm(c_nazwa_procedury,p_komunikat||' - Koniec');
 end loop;
 sm(c_nazwa_procedury,'Stop...');
end profile_dodaj;

/*
 Modyfikacja profili
*/
procedure profile_modyfikuj
is
c_nazwa_procedury varchar2(100):=c_package_name||'.'||'profile_modyfikuj';

cursor c_profile is
--Lista zmienionych Profili przypisanych do Portalu 
select
prf.PRF_NAZWA,
prf.PRF_INS_ID,
prf.PRF_FRM_ID,
prf.PRF_F_EGERIA,
prf.PRF_F_KOKPITY,
prf.PRF_F_PORTAL,
prf.PRF_F_CONTROLLING,
prf.PRF_F_GENERATOR,
prf.PRF_F_EDUKACJA,
prf.PRF_OPIS,
mmap.MMAP_ID
from EAT_PROFILE prf,
     PPT_MIGRACJA_MAPA mmap
where prf.PRF_ID=TO_NUMBER(mmap.MMAP_WARTOSC_ZRODLO)
and mmap.MMAP_NAZWA_TABELI='EAT_PROFILE'
and mmap.MMAP_NAZWA_KOLUMNY='PRF_ID' 
and mmap.MMAP_STATUS='A'
-- PO TESTACH USUNAC
and ((prf.PRF_NAZWA like 'PPP_TEST_COMARCH%') or (prf.PRF_NAZWA='PPP_DELZ_DYR_KOM') or (prf.PRF_NAZWA='PPP_DELZ_BAD') or (prf.PRF_NAZWA='PPP_DELZ_BDG') or (prf.PRF_NAZWA='PPP_OCE_SEKRETARIAT') or (prf.PRF_NAZWA='EK_WA_BAD') or (prf.PRF_NAZWA='EK_WA_BDG') or (prf.PRF_NAZWA='EK_WA_SEKRETARIAT'))
--
MINUS
select
prf.PRF_NAZWA,
prf.PRF_INS_ID,
prf.PRF_FRM_ID,
prf.PRF_F_EGERIA,
prf.PRF_F_KOKPITY,
prf.PRF_F_PORTAL,
prf.PRF_F_CONTROLLING,
prf.PRF_F_GENERATOR,
prf.PRF_F_EDUKACJA,
prf.PRF_OPIS,
mmap.MMAP_ID
from EAT_PROFILE@conf prf,
     PPT_MIGRACJA_MAPA mmap   
where prf.PRF_ID=TO_NUMBER(mmap.MMAP_WARTOSC_CEL)
and mmap.MMAP_NAZWA_TABELI='EAT_PROFILE'
and mmap.MMAP_NAZWA_KOLUMNY='PRF_ID'   
and mmap.MMAP_STATUS='A'
;
p_prf_id_zrodlo number(10,0);
p_komunikat varchar(100);
v_rec eap_obj_profil.t_profil@conf;
p_prf_rowid_cel ROWID;
p_error boolean:=false;
begin
 sm(c_nazwa_procedury,'Start...');
 for p_prf in c_profile loop
  p_error:=false;
  p_komunikat:='Modyfikacja profilu '||p_prf.PRF_NAZWA;
  sm(c_nazwa_procedury,p_komunikat||' - Start...');
  begin
    --Pobranie wartosci identyfikatorow zrodla i celu dla danego profilu
    select
    mmap.MMAP_WARTOSC_ZRODLO,
    mmap.MMAP_WARTOSC_CEL
    into p_prf_id_zrodlo, v_rec.prf_id
    from PPT_MIGRACJA_MAPA mmap,
         EAT_PROFILE prf
    where prf.PRF_ID=mmap.MMAP_WARTOSC_ZRODLO
    and mmap.MMAP_NAZWA_TABELI='EAT_PROFILE'
    and mmap.MMAP_NAZWA_KOLUMNY='PRF_ID'
    and prf.PRF_NAZWA=p_prf.PRF_NAZWA;
  exception
     when others then
      p_error:=true;
      sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
  end;
  
  if not(p_error) then
    --Sprawdzenie czy Profil o podamym id istnieje w bazie docelowej
    --pobranie ROWID Profilu
    begin
      select
      ROWID
      into v_rec.prf_rowid
      from EAT_PROFILE@conf prf
      where prf.PRF_ID=v_rec.prf_id;
    exception
     when no_data_found then
      v_rec.prf_rowid:=null;
      p_error:=true;
      sm(c_nazwa_procedury,p_komunikat||' - brak w bazie docelowej Profilu','E');
     when others then
      sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
    end;
  end if;
  
  if not(p_error) then
    --Przygotowanie rekodu z danymi do modyfikacji  
    v_rec.prf_ins_id := p_prf.prf_ins_id;
    v_rec.prf_frm_id := p_prf.prf_frm_id;
    v_rec.prf_nazwa := p_prf.prf_nazwa;
    v_rec.prf_opis := p_prf.prf_opis;
    v_rec.prf_f_egeria :=p_prf.prf_f_egeria;
    v_rec.prf_f_kokpity :=p_prf.prf_f_kokpity;
    v_rec.prf_f_portal :=p_prf.prf_f_portal;
    v_rec.prf_f_controlling :=p_prf.prf_f_controlling;
    v_rec.prf_f_edukacja := p_prf.prf_f_edukacja;
    v_rec.prf_f_generator := p_prf.prf_f_generator;
    
    --Modyfikacja rekordu w bazie docelowej
    begin
     eap_obj_profil.modyfikuj@conf(v_rec);
     sm(c_nazwa_procedury,p_komunikat||' - zmodyfikowano rekord');
    exception
     when others then
      p_error:=true;
      sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
    end;  
  end if;
  
  if not(p_error) then
    --Zwi�kszenie licznika synchronizacji
    update ppt_migracja_mapa set
    mmap_liczba_synch=mmap_liczba_synch+1
    where mmap_id=p_prf.mmap_id; 
  end if;
  
  --Zatwierdzenie lub wycofanie zmian    
  case not(p_error)   
   when true then commit;   
   else rollback;
  end case;   
  
  case not(p_error)
   when true then sm(c_nazwa_procedury,p_komunikat||' - Zako�czony sukcesem');
   else sm(c_nazwa_procedury,p_komunikat||' - Zako�czony b��dem');
  end case;  
  
  sm(c_nazwa_procedury,p_komunikat||' - Koniec...');  
 end loop;
 
 sm(c_nazwa_procedury,'Stop');
end profile_modyfikuj; 

/*
Usuniecie profilu
*/
procedure profile_usun
is
c_nazwa_procedury varchar(100):=c_package_name||'.'||'profile_usun';
cursor c_profile is
--
-- Lista profili usunietych w bazie zrodlowej ale istniejacych w bazie docelowej
-- LISTA PROFILI W TABELI DECELOWEJ
--
select
mmap.MMAP_ID,
mmap.MMAP_NAZWA_TABELI,
mmap.MMAP_NAZWA_KOLUMNY,
mmap.MMAP_WARTOSC_ZRODLO,
mmap.MMAP_WARTOSC_CEL
from EAT_PROFILE@conf prf,
     PPT_MIGRACJA_MAPA mmap
where prf.PRF_ID=TO_NUMBER(mmap.MMAP_WARTOSC_CEL)
and mmap.MMAP_NAZWA_TABELI='EAT_PROFILE'
and mmap.MMAP_NAZWA_KOLUMNY='PRF_ID'
and mmap.MMAP_STATUS='A'
-- PO TESTACH USUNAC
and ((prf.PRF_NAZWA like 'PPP_TEST_COMARCH%') or (prf.PRF_NAZWA='PPP_DELZ_DYR_KOM') or (prf.PRF_NAZWA='PPP_DELZ_BAD') or (prf.PRF_NAZWA='PPP_DELZ_BDG') or (prf.PRF_NAZWA='PPP_OCE_SEKRETARIAT') or (prf.PRF_NAZWA='EK_WA_BAD') or (prf.PRF_NAZWA='EK_WA_BDG') or (prf.PRF_NAZWA='EK_WA_SEKRETARIAT'))
--
MINUS
--LISTA PROFILI W TABELI ZRODLOWEJ
select
mmap.MMAP_ID,
mmap.MMAP_NAZWA_TABELI,
mmap.MMAP_NAZWA_KOLUMNY,
mmap.MMAP_WARTOSC_ZRODLO,
mmap.MMAP_WARTOSC_CEL
from PPT_MIGRACJA_MAPA mmap
join EAT_PROFILE prf on prf.PRF_ID=TO_NUMBER(mmap.MMAP_WARTOSC_ZRODLO)
                    and mmap.MMAP_NAZWA_TABELI='EAT_PROFILE'
                    and mmap.MMAP_NAZWA_KOLUMNY='PRF_ID'
                    and mmap.MMAP_STATUS='A'                    
--PO TESTACH USUNAC
and ((prf.PRF_NAZWA like 'PPP_TEST_COMARCH%') or (prf.PRF_NAZWA='PPP_DELZ_DYR_KOM') or (prf.PRF_NAZWA='PPP_DELZ_BAD') or (prf.PRF_NAZWA='PPP_DELZ_BDG') or (prf.PRF_NAZWA='PPP_OCE_SEKRETARIAT') or (prf.PRF_NAZWA='EK_WA_BAD') or (prf.PRF_NAZWA='EK_WA_BDG') or (prf.PRF_NAZWA='EK_WA_SEKRETARIAT'))
--
;

p_komunikat varchar(100);
p_error boolean:=false;
v_rec eap_obj_profil.t_profil@conf;
begin
 sm(c_nazwa_procedury,'Start...');
 for p_rec in c_profile loop
  p_error:=false;
  p_komunikat:='Usuni�cie profilu ';
  sm(c_nazwa_procedury,p_komunikat||' - Start...');
  --Pobranie nazwy kasowanego profilu
  begin
    select
    prf.prf_id,
    prf.prf_nazwa
    into v_rec.prf_id, v_rec.prf_nazwa
    from EAT_PROFILE@conf prf
    where prf.prf_id=p_rec.MMAP_WARTOSC_CEL;
    p_komunikat:='Usuni�cie profilu '||v_rec.prf_nazwa;
  exception
   when no_data_found then
    p_error:=true;
    sm(c_nazwa_procedury,'Nie istnieje profil w bazie docelowej','E');
   when others then 
    p_error:=true;
    sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');   
  end;  
  
  if not(p_error) then
    sm(c_nazwa_procedury,p_komunikat||' - Kasowanie w bazie docelowej...');
    --Kasowanie profilu w bazie docelowej
    begin
     EAP_OBJ_PROFIL.USUN@conf(v_rec.prf_id);
     sm(c_nazwa_procedury,p_komunikat||' - Wykonano');
    exception
     when others then
      p_error:=true;
      sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
    end;
  end if;
  
  if not(p_error) then
    sm(c_nazwa_procedury,p_komunikat||' - zmiana status rekordu w mapie na Usuni�ty...');
    --Oznaczenie rekordu jako skasowanego w mapie migracji i zwi�kszenie licznika synchronizacji   
    begin
      update ppt_migracja_mapa set
      mmap_status='U',
      mmap_liczba_synch=mmap_liczba_synch+1,
      mmap_info_kasowania='Kasowany profil: '||v_rec.prf_nazwa||'(ID: '||v_rec.prf_id||')'
      where mmap_id=p_rec.mmap_id; 
      sm(c_nazwa_procedury,p_komunikat||' - Wykonano');
    exception
     when others then
      p_error:=true;
      sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');      
    end;
  end if;
  
  --Zatwierdzenie lub wycofanie zmian    
  case not(p_error)   
   when true then commit;   
   else rollback;
  end case;   
  
  case not(p_error)
   when true then sm(c_nazwa_procedury,p_komunikat||' - Zako�czony sukcesem');
   else sm(c_nazwa_procedury,p_komunikat||' - Zako�czony b��dem');
  end case;  
  
  sm(c_nazwa_procedury,p_komunikat||' - Stop');
 end loop;
 sm(c_nazwa_procedury,'Koniec...');
end profile_usun;

/*
 Migrowanie profili
*/
procedure profile_synchronizacja
is
c_nazwa_procedury varchar2(100):=c_package_name||'.'||'profile_synchronizacja';
begin
sm(c_nazwa_procedury,'Start...');
 --Dodanie nowych profili
 begin
  profile_dodaj;
 exception
  when others then
   sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
 end;
 --Modyfikacja profili juz zaimportowanych
 begin
  profile_modyfikuj;
 exception
  when others then
   sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
 end;
 --Usuniecie profili juz zaimportowanych
 begin
  profile_usun;
 exception
  when others then
   sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
 end;
sm(c_nazwa_procedury,'Stop...');  
end profile_synchronizacja;

/*
 Dodanie profilu do drzewa jako podrz�dnego lub nadrzednego
*/
procedure profile_drzewo_dodaj(p_nadrzedny boolean default false)
is
c_nazwa_procedury constant varchar2(100):=c_package_name||'.'||'profile_drzewo_dodaj';
cursor c_prf_podrzedny is
--
--Lista profily podrzednych w stosunku do przes�anych do bazy docelowej
--W pierwszej kolejnosci synchronizujemy profile a potem mape profil podrzedny
--
select
prf2.prf_nazwa PRF_NAZWA_NAD,
NVL(prf3.prf_id,-1) CEL_PRF_ID_NAD,
prf4.prf_nazwa PRF_NAZWA_POD,
mmap.MMAP_WARTOSC_CEL CEL_PRF_ID_POD,
prf.PRF_PRF_ID_NAD ZRODLO_PRF_ID_NAD,
prf.PRF_PRF_ID_POD ZRODLO_PRF_ID_POD
from EAT_PROFILE_PROFILE prf,
     PPT_MIGRACJA_MAPA mmap,
     EAT_PROFILE prf2,
     EAT_PROFILE@conf prf3,
     EAT_PROFILE prf4
where prf.PRF_PRF_ID_POD=TO_NUMBER(mmap.MMAP_WARTOSC_ZRODLO)
and mmap.MMAP_NAZWA_TABELI='EAT_PROFILE'
and mmap.MMAP_NAZWA_KOLUMNY='PRF_ID'
and mmap.MMAP_STATUS='A'
and prf2.prf_id=prf.PRF_PRF_ID_NAD
and prf3.prf_nazwa(+)=prf2.prf_nazwa
and prf4.prf_id=prf.PRF_PRF_ID_POD
--PO TESTACH USUNAC
--and prf.PRF_PRF_ID_POD=300326
--and prf3.prf_id is null
--
;
p_error boolean:=false;
p_komunikat varchar2(255);
p_ile_nad number:=-1;
p_ile_pod number:=-1;
p_ile number:=-1;
p_licznik number:=0;
begin
 sm(c_nazwa_procedury,'Start...');
 for p_rec in c_prf_podrzedny loop
  p_error:=false;
  p_ile_nad:=-1;
  p_ile_pod:=-1;
  p_ile:=-1;  
  p_komunikat:='Import realcji profilii: '||p_rec.PRF_NAZWA_NAD||'->'||p_rec.PRF_NAZWA_POD;
  --Sprawdzenie czy PROFIL nadrzedny istnieje w bazie docelowej
  sm(c_nazwa_procedury,p_komunikat||' - Start...');
  select
  count(*)  
  into p_ile_nad
  from EAT_PROFILE@conf prf
  where prf.PRF_ID=p_rec.CEL_PRF_ID_NAD
  and prf.PRF_NAZWA=p_rec.PRF_NAZWA_NAD
  ;
  if p_ile_nad=0 then
   p_error:=true;
   sm(c_nazwa_procedury,p_komunikat||' - w bazie docelowej nie ma profilu: '||p_rec.PRF_NAZWA_NAD||' o ID '||p_rec.CEL_PRF_ID_NAD,'E');
  end if;

  --Sprawdzenie czy PROFIL podnadrzedny istnieje w bazie docelowej
  select
  count(*)
  into p_ile_pod
  from EAT_PROFILE@conf prf
  where prf.PRF_ID=p_rec.CEL_PRF_ID_POD
  and prf.PRF_NAZWA=p_rec.PRF_NAZWA_POD
  ;
  if p_ile_pod=0 then
   p_error:=true;
   sm(c_nazwa_procedury,p_komunikat||' - w bazie docelowej nie ma profilu: '||p_rec.PRF_NAZWA_POD||' o ID '||p_rec.CEL_PRF_ID_POD,'E');
  end if;

  --Sprawdzenie czy istnieje wpis w drzewie
  select
  count(*)
  into p_ile
  from EAT_PROFILE_PROFILE@conf prf
  where prf.PRF_PRF_ID_NAD=p_rec.CEL_PRF_ID_NAD
  and prf.PRF_PRF_ID_POD=p_rec.CEL_PRF_ID_POD
  ;
  
  if p_ile=0 and p_error then
     sm(c_nazwa_procedury,p_komunikat||' - nie mo�na doda� realcji profili','E');
  end if;

  if p_ile=0 and not(p_error) then
   begin
      sm(c_nazwa_procedury,p_komunikat||' - wgranie do bazy docelowej...');
      --Dodanie realacji do bazy docelowej
      insert into EAT_PROFILE_PROFILE@conf
      (PRF_PRF_ID_NAD, PRF_PRF_ID_POD)
      values
      (p_rec.CEL_PRF_ID_NAD, p_rec.CEL_PRF_ID_POD);
      sm(c_nazwa_procedury,p_komunikat||' - wgranie do bazy docelowej - wykonano');
   exception
    when others then
     p_error:=true;
     sm(c_nazwa_procedury,SUBSTR(p_komunikat||' - '||SQLCODE||' - '||SQLERRM,1,255),'E');
   end;
  end if;
  
  if p_ile=0 and not(p_error) then
   begin
     --Dodanie zapisu do tabeli mapy lokalnej
     sm(c_nazwa_procedury,p_komunikat||' - utworzenie wpisu w mapie powi�za�...');
     migracja_mapa_wstaw(p_nazwa_tabeli     =>'EAT_PROFILE_PROFILE', 
                         p_nazwa_kolumny    =>'PRF_PRF_ID_NAD', 
                         p_typ_kolumny      =>'NUMBER', 
                         p_wartosc_zrodlo   =>p_rec.ZRODLO_PRF_ID_NAD, 
                         p_wartosc_cel      =>p_rec.CEL_PRF_ID_NAD,   
                         p_nazwa_kolumny_2  =>'PRF_PRF_ID_POD', 
                         p_typ_kolumny_2    =>'NUMBER', 
                         p_wartosc_zrodlo_2 =>p_rec.ZRODLO_PRF_ID_POD, 
                         p_wartosc_cel_2    =>p_rec.CEL_PRF_ID_POD
                        ); 
     sm(c_nazwa_procedury,p_komunikat||' - utworzenie wpisu w mapie powi�za� - wykonano');                        
   exception
    when others then
     p_error:=true;
     sm(c_nazwa_procedury,SUBSTR(p_komunikat||' - '||SQLCODE||' - '||SQLERRM,1,255),'E');
   end;
  end if;
  
  if p_ile=0 and not(p_error) then
     p_licznik:=p_licznik+1;
  end if;
  
  
  if p_ile=1 and not(p_error) then
     sm(c_nazwa_procedury,p_komunikat||' - ju� zaimportowany');
  end if;

  --Zatwierdzenie lub wycofanie zmian    
  case not(p_error)   
   when true then commit;   
   else rollback;
  end case;   
  
  case not(p_error)
   when true then 
   if p_ile=0 then
      sm(c_nazwa_procedury,p_komunikat||' - Zako�czony sukcesem');
   end if;
   else sm(c_nazwa_procedury,p_komunikat||' - Zako�czony b��dem','E');
  end case;   
  
  sm(c_nazwa_procedury,p_komunikat||' - Stop');
 end loop;
 sm(c_nazwa_procedury,'Import realcji profilii - zaimportowano: '||p_licznik);
 sm(c_nazwa_procedury,'Koniec...');
end profile_drzewo_dodaj;

/*
 Usuniecie profilu z drzewa
*/
procedure profile_drzewo_usun
is 
c_nazwa_procedury varchar2(100):=c_package_name||'.'||'profile_drzewo_usun';
cursor c_profile is
--------------------------------------------------------------------------------
--LISTA RELACJI PROFILI DO USUNIECIA
--------------------------------------------------------------------------------
select
anl.MMAP_ID,
anl.MMAP_WARTOSC_CEL CEL_PRF_PRF_ID_NAD,
anl.MMAP_WARTOSC_CEL_2 CEL_PRF_PRF_ID_POD,
anl.PROFIL_NAZWA_NAD,
anl.PROFIL_NAZWA_POD
from 
(
select
(select
 count(*)
 from EAT_PROFILE_PROFILE@conf prf
 where prf.PRF_PRF_ID_NAD=mmap.MMAP_WARTOSC_CEL
 and prf.PRF_PRF_ID_POD=mmap.MMAP_WARTOSC_CEL_2
) ILE_CEL,
(select
 count(*)
 from EAT_PROFILE_PROFILE prf
 where prf.PRF_PRF_ID_NAD=mmap.MMAP_WARTOSC_ZRODLO
 and prf.PRF_PRF_ID_POD=mmap.MMAP_WARTOSC_ZRODLO_2
) ILE_ZRODLO,
(select
 prf.prf_nazwa
 from EAT_PROFILE@conf prf
 where prf.PRF_ID=mmap.MMAP_WARTOSC_CEL
) PROFIL_NAZWA_NAD,
(select
 prf.prf_nazwa
 from EAT_PROFILE@conf prf
 where prf.PRF_ID=mmap.MMAP_WARTOSC_CEL_2
) PROFIL_NAZWA_POD,
mmap.*
from PPT_MIGRACJA_MAPA mmap
where mmap.MMAP_NAZWA_TABELI='EAT_PROFILE_PROFILE'
and mmap.MMAP_STATUS='A'
and mmap.MMAP_NAZWA_KOLUMNY='PRF_PRF_ID_NAD'
and mmap.MMAP_NAZWA_KOLUMNY_2='PRF_PRF_ID_POD'
) anl
where anl.ILE_CEL>0 and anl.ILE_ZRODLO=0
;
p_error boolean:=false;
p_komunikat varchar2(255);
p_licznik number(10,0):=0;
begin
 sm(c_nazwa_procedury,'Start...');
 for v_rec in c_profile loop
  p_error:=false;
  p_komunikat:='Kasowanie relacji profilii '||v_rec.PROFIL_NAZWA_NAD||'->'||v_rec.PROFIL_NAZWA_POD;
  sm(c_nazwa_procedury,p_komunikat||' - Start...');
  --Usuniecie relacji w bazie docelowej
  begin
   sm(c_nazwa_procedury,p_komunikat||' - kasowanie realcji - Start...');
   delete from EAT_PROFILE_PROFILE@conf 
   where PRF_PRF_ID_NAD=v_rec.CEL_PRF_PRF_ID_NAD
   and PRF_PRF_ID_POD=v_rec.CEL_PRF_PRF_ID_POD;
   sm(c_nazwa_procedury,p_komunikat||' - kasowanie realcji - wykonano');
  exception
   when others then
    p_error:=true;
    sm(c_nazwa_procedury,SUBSTR(p_komunikat||' - '||SQLCODE||' - '||SQLERRM,1,255),'E');
  end;
  
  if not(p_error) then
  --Aktualizacja wpisu w tabeli z map�
  begin
    sm(c_nazwa_procedury,p_komunikat||' - aktualizacja danych w mapie migracji - Start...');
    update ppt_migracja_mapa set
    mmap_status='U',
    mmap_liczba_synch=mmap_liczba_synch+1,
    mmap_info_kasowania='Kasowana relacja profili: '||v_rec.PROFIL_NAZWA_NAD||'->'||v_rec.PROFIL_NAZWA_POD
    where mmap_id=v_rec.mmap_id; 
    sm(c_nazwa_procedury,p_komunikat||' - aktualizacja danych w mapie migracji - Wykonano');
  exception
   when others then
    p_error:=true;
    sm(c_nazwa_procedury,SUBSTR(p_komunikat||' - '||SQLCODE||' - '||SQLERRM,1,255),'E');
  end;
  end if;
  
  if not(p_error) then
     p_licznik:=p_licznik+1;
  end if;
  
  --Zatwierdzenie lub wycofanie zmian    
  case not(p_error)   
   when true then commit;   
   else rollback;
  end case;   
  
  case not(p_error)
   when true then 
      sm(c_nazwa_procedury,p_komunikat||' - Zako�czony sukcesem');
   else sm(c_nazwa_procedury,p_komunikat||' - Zako�czony b��dem','E');
  end case;   
  
  sm(c_nazwa_procedury,p_komunikat||' - Stop');
 end loop;
 sm(c_nazwa_procedury,'Kasowanie relacji profilii - usuni�to: '||p_licznik);
 sm(c_nazwa_procedury,'Koniec...');
end profile_drzewo_usun;

/*
 Synchronizacja drzewa zale�no�ci profili NADRZEDNY / PODRZEDNY
*/
procedure profile_drzewo_synchronizacja
is
c_nazwa_procedury varchar2(100):=c_package_name||'.'||'profile_drzewo_synchronizacja';
begin
sm(c_nazwa_procedury,'Start...');
 --Dodanie nowych relacji
 begin
  profile_drzewo_dodaj;
 exception
  when others then
   sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
 end;
 --Usuniecie profili z drzewa 
 begin
  profile_drzewo_usun;
 exception
  when others then
   sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
 end;
sm(c_nazwa_procedury,'Stop...');  

end profile_drzewo_synchronizacja;

/*
 G��wna procedura migurj�ca dane s�ownik�w 
*/
procedure migruj_konfiguracja
is
c_nazwa_procedury varchar2(100):=c_package_name||'.'||'migruj_konfiguracja';
begin
sm(c_nazwa_procedury,'Start...');
-- Migracja: Profile
   begin
    profile_synchronizacja;
   exception
    when others then
     sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
   end;
--Migracja: Drzewa zale�no�ci Profili
  begin 
   profile_drzewo_synchronizacja;
  exception
   when others then 
     sm(c_nazwa_procedury,SUBSTR(SQLCODE||' - '||SQLERRM,1,100),'E');
  end;

-- TODO: migracja danych z tabel w zakresie kontrolowanym przez modu�: Portal Pracowniczy
-- ZROBIONE - EAT_LISTY
-- ZROBIONE - EAT_FUNKCJE_UZYTKOWE
-- CSS_SLOWNIKI
-- CSS_WARTOSCI_SLOWNIKOW
-- PPT_ADM_SQLS
-- PPT_JPA_GENKEYS
-- TABELE KONFIGURACJI OBIEGOW DOKUEMNTOW

-- Migracja: Listy warto�ci
-- TODO

-- Migracja: Funkcje u�ytkowe
-- TODO
sm(c_nazwa_procedury,'Koniec...');
end migruj_konfiguracja;


END PPP_MIGRACJA;