CREATE TABLE "EGADM1"."OST_UMOWY" 
   (	"UMSZ_ID" NUMBER(10,0) NOT NULL ENABLE,
	"UMSZ_PRC_ID" NUMBER(10,0) NOT NULL ENABLE,
	"UMSZ_DATA_PODPISANIA" DATE,
	"UMSZ_WARTOSC" NUMBER(10,2),
	"UMSZ_CZY_UREGULOWANO" VARCHAR2(1) DEFAULT 'N' NOT NULL ENABLE,
	"UMSZ_UWAGI" VARCHAR2(2000),
	"UMSZ_AUDYT_UT" VARCHAR2(30) NOT NULL ENABLE,
	"UMSZ_AUDYT_DT" DATE NOT NULL ENABLE,
	"UMSZ_AUDYT_UM" VARCHAR2(30),
	"UMSZ_AUDYT_DM" DATE,
	"UMSZ_AUDYT_LM" VARCHAR2(100),
	"UMSZ_AUDYT_KM" VARCHAR2(100),
	"UMSZ_PRZEDMIOT" VARCHAR2(500) NOT NULL ENABLE,
	"UMSZ_DATA_OBOWIAZYWANIA" DATE
   )
   TABLESPACE EG_D;
   
   ALTER TABLE "EGADM1"."OST_UMOWY"  ADD CONSTRAINT "OST_UMOWY_PK" PRIMARY KEY ("UMSZ_ID") USING INDEX TABLESPACE EG_X ENABLE;
   
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_ID" IS 'Klucz g��wny tabeli';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_PRC_ID" IS 'Pracownik - klucz do tabeli EK_PRACOWNICY';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_DATA_PODPISANIA" IS 'Data podpisania umowy';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_WARTOSC" IS 'Warto�� zobowi�za� pracownika wynikaj�ca z umowy';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_CZY_UREGULOWANO" IS 'Uregulowano zobowi�zanie - checkbox';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_UWAGI" IS 'Uwagi';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_AUDYT_UT" IS 'U�ytkownik kt�ry utworzy� rekord ';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_AUDYT_DT" IS 'Kiedy utworzy� ';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_AUDYT_UM" IS 'Kto modyfikowa� rekord ';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_AUDYT_DM" IS 'Kiedy modyfikowa� ';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_AUDYT_LM" IS 'Liczba modyfikacji  ';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_AUDYT_KM" IS 'Komputer z kt�rego dokonano zmiany  ';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_PRZEDMIOT" IS 'Przedmiot umowy';
   COMMENT ON COLUMN "EGADM1"."OST_UMOWY"."UMSZ_DATA_OBOWIAZYWANIA" IS 'Data obowi�zywania umowy';

  CREATE OR REPLACE EDITIONABLE TRIGGER "EGADM1"."PPG_UMSZ_BIUR_AUDYT" 
  BEFORE INSERT or UPDATE ON EGADM1.OST_UMOWY
  FOR EACH ROW 
DECLARE
  v_pom VARCHAR2 (255);
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.UMSZ_AUDYT_UT, :NEW.UMSZ_AUDYT_DT, v_pom, :NEW.UMSZ_AUDYT_LM, :OLD.UMSZ_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.UMSZ_AUDYT_UT, :NEW.UMSZ_AUDYT_DT, v_pom, :NEW.UMSZ_AUDYT_LM, :OLD.UMSZ_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.UMSZ_AUDYT_UM, :NEW.UMSZ_AUDYT_DM, :NEW.UMSZ_AUDYT_KM, :NEW.UMSZ_AUDYT_LM, :OLD.UMSZ_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.UMSZ_AUDYT_UM, :NEW.UMSZ_AUDYT_DM, :NEW.UMSZ_AUDYT_KM, :NEW.UMSZ_AUDYT_LM, :OLD.UMSZ_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;

/
ALTER TRIGGER "EGADM1"."PPG_UMSZ_BIUR_AUDYT" ENABLE;
