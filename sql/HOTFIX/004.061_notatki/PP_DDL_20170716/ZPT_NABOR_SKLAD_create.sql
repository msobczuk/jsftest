   CREATE TABLE "EGADM1"."ZPT_NABOR_SKLAD" 
   (	"NSK_ID" NUMBER(10,0) NOT NULL ENABLE,
	"NSK_NAB_ID" NUMBER(10,0) NOT NULL ENABLE,
	"NSK_RODZAJ" NVARCHAR2(50) NOT NULL ENABLE,
	"NSK_PROCENT" NUMBER(3,2),
	"NSK_WARTOSC" NUMBER(10,2) NOT NULL ENABLE,
	"NSK_AUDYT_UT" VARCHAR2(30) NOT NULL ENABLE,
	"NSK_AUDYT_DT" DATE NOT NULL ENABLE,
	"NSK_AUDYT_UM" VARCHAR2(30),
	"NSK_AUDYT_DM" DATE,
	"NSK_AUDYT_LM" VARCHAR2(100),
	"NSK_AUDYT_KM" VARCHAR2(100)
   )
   TABLESPACE EG_D;
   
   ALTER TABLE "EGADM1"."ZPT_NABOR_SKLAD" ADD CONSTRAINT "ZPT_NABOR_SKLAD_PK" PRIMARY KEY ("NSK_ID") USING INDEX TABLESPACE EG_X ENABLE;
   
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_SKLAD"."NSK_ID" IS 'klucz g��wny tabeli';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_SKLAD"."NSK_NAB_ID" IS 'identyfikator naboru z ZPT_NABOR ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_SKLAD"."NSK_RODZAJ" IS 'Rodzaj sk�adnika , s�ownik REK_SKL_RODZAJ ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_SKLAD"."NSK_PROCENT" IS 'Procent sk�adnika wynagrodzenia';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_SKLAD"."NSK_WARTOSC" IS 'Warto�� sk�adnika wynagrodzenia ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_SKLAD"."NSK_AUDYT_UT" IS 'U�ytkownik kt�ry utworzy� rekord ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_SKLAD"."NSK_AUDYT_DT" IS 'Kiedy utworzy� ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_SKLAD"."NSK_AUDYT_UM" IS 'Kto modyfikowa� rekord ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_SKLAD"."NSK_AUDYT_DM" IS 'Kiedy modyfikowa� ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_SKLAD"."NSK_AUDYT_LM" IS 'Liczba modyfikacji  ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_SKLAD"."NSK_AUDYT_KM" IS 'Komputer z kt�rego dokonano zmiany  ';

  CREATE OR REPLACE EDITIONABLE TRIGGER "EGADM1"."PPG_NSK_BIUR_AUDYT" 
  BEFORE INSERT or UPDATE ON EGADM1.ZPT_NABOR_SKLAD
  FOR EACH ROW 
DECLARE
  v_pom VARCHAR2 (255);
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.NSK_AUDYT_UT, :NEW.NSK_AUDYT_DT, v_pom, :NEW.NSK_AUDYT_LM, :OLD.NSK_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.NSK_AUDYT_UT, :NEW.NSK_AUDYT_DT, v_pom, :NEW.NSK_AUDYT_LM, :OLD.NSK_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.NSK_AUDYT_UM, :NEW.NSK_AUDYT_DM, :NEW.NSK_AUDYT_KM, :NEW.NSK_AUDYT_LM, :OLD.NSK_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.NSK_AUDYT_UM, :NEW.NSK_AUDYT_DM, :NEW.NSK_AUDYT_KM, :NEW.NSK_AUDYT_LM, :OLD.NSK_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;

/
ALTER TRIGGER "EGADM1"."PPG_NSK_BIUR_AUDYT" ENABLE;