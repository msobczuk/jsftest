   CREATE TABLE "EGADM1"."OST_PLAN_INDYW" 
   (	"PIS_ID" NUMBER(10,0) NOT NULL ENABLE,
	"PIS_PRC_ID" NUMBER(10,0) NOT NULL ENABLE,
	"PIS_IPRZ_ID" NUMBER(10,0),
	"PIS_ROK" DATE NOT NULL ENABLE,
	"PIS_DATA_ZAPISU" DATE NOT NULL ENABLE,
	"PIS_TEMAT" VARCHAR2(200),
	"PIS_SZK_POWSZECH" VARCHAR2(30),
	"PIS_SSPE_OBSZAR_WIEDZY" VARCHAR2(30),
	"PIS_SSPE_FORMA" VARCHAR2(30) DEFAULT NULL,
	"PIS_AUDYT_UT" VARCHAR2(30) NOT NULL ENABLE,
	"PIS_AUDYT_DT" DATE NOT NULL ENABLE,
	"PIS_AUDYT_UM" VARCHAR2(30),
	"PIS_AUDYT_DM" DATE,
	"PIS_AUDYT_LM" VARCHAR2(100),
	"PIS_AUDYT_KM" VARCHAR2(100),
	"PIS_ZSZ_ID" NUMBER(10,0),
	"PIS_ORGANIZATOR" VARCHAR2(50),
	"PIS_MIEJSCE_SZKOL" VARCHAR2(200),
	"PIS_TERMIN_OD" DATE,
	"PIS_TERMIN_DO" DATE,
	"PIS_UZASADNIENIE" VARCHAR2(500)
   )
   TABLESPACE EG_D;
   
   ALTER TABLE "EGADM1"."OST_PLAN_INDYW"  ADD CONSTRAINT "OST_PLAN_INDYW_PK" PRIMARY KEY ("PIS_ID") USING INDEX TABLESPACE EG_X ENABLE;
   
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_ID" IS 'Klucz g��wny tabeli';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_PRC_ID" IS 'Pracownik ? klucz do tabeli EK_PRACOWNICY';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_IPRZ_ID" IS 'IPRZ, w kt�rym zdefiniowano szkolenia ZPT_IPRZ Pole wymagalne je�li PIS_ZRODLO = ?IP?';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_ROK" IS 'Rok kt�rego dotyczy planowanie ';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_DATA_ZAPISU" IS 'Data, kiedy dokonano zapisu na szkolenie ';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_TEMAT" IS 'Temat szkolenia  ';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_SZK_POWSZECH" IS 'Szkolenie powszechne ? s�ownik (';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_SSPE_OBSZAR_WIEDZY" IS 'Obszar wiedzy dla szkole� specjalistycznych ';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_SSPE_FORMA" IS 'Forma szkolenia  specjalistycznego';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_AUDYT_UT" IS 'U�ytkownik kt�ry utworzy� rekord ';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_AUDYT_DT" IS 'Kiedy utworzy� ';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_AUDYT_UM" IS 'Kto modyfikowa� rekord ';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_AUDYT_DM" IS 'Kiedy modyfikowa� ';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_AUDYT_LM" IS 'Liczba modyfikacji  ';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_AUDYT_KM" IS 'Komputer z kt�rego dokonano zmiany  ';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_ZSZ_ID" IS 'Plan szkoleniowy,  z kt�rym zwi�zane jest szkolenie';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_ORGANIZATOR" IS 'Podmiot organizuj�cy szkolenie';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_MIEJSCE_SZKOL" IS 'Miejsce szkolenia';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_TERMIN_OD" IS 'Termin szkolenia od';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_TERMIN_DO" IS 'Termin szkolenia do';
   COMMENT ON COLUMN "EGADM1"."OST_PLAN_INDYW"."PIS_UZASADNIENIE" IS 'Uzasadnienie';

  CREATE OR REPLACE EDITIONABLE TRIGGER "EGADM1"."PPG_PIS_BIUR_AUDYT" 
  BEFORE INSERT or UPDATE ON EGADM1.OST_PLAN_INDYW
  FOR EACH ROW 
DECLARE
  v_pom VARCHAR2 (255);
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.PIS_AUDYT_UT, :NEW.PIS_AUDYT_DT, v_pom, :NEW.PIS_AUDYT_LM, :OLD.PIS_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.PIS_AUDYT_UT, :NEW.PIS_AUDYT_DT, v_pom, :NEW.PIS_AUDYT_LM, :OLD.PIS_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.PIS_AUDYT_UM, :NEW.PIS_AUDYT_DM, :NEW.PIS_AUDYT_KM, :NEW.PIS_AUDYT_LM, :OLD.PIS_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.PIS_AUDYT_UM, :NEW.PIS_AUDYT_DM, :NEW.PIS_AUDYT_KM, :NEW.PIS_AUDYT_LM, :OLD.PIS_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;

/
ALTER TRIGGER "EGADM1"."PPG_PIS_BIUR_AUDYT" ENABLE;
