CREATE TABLE "EGADM1"."ZPT_IPRZ" 
   (	"IPRZ_ID" NUMBER(10,0) NOT NULL ENABLE,
	"IPRZ_PRC_ID" NUMBER(10,0) NOT NULL ENABLE,
	"IPRZ_PODSUMOWANIE" VARCHAR2(2000),
	"IPRZ_SCIEZKA" VARCHAR2(200) NOT NULL ENABLE,
	"IPRZ_PROGRAM" VARCHAR2(200) NOT NULL ENABLE,
	"IPRZ_TALENTY" VARCHAR2(1) NOT NULL ENABLE,
	"IPRZ_AUDYT_UT" VARCHAR2(30) NOT NULL ENABLE,
	"IPRZ_AUDYT_DT" DATE NOT NULL ENABLE,
	"IPRZ_AUDYT_UM" VARCHAR2(30),
	"IPRZ_AUDYT_DM" DATE,
	"IPRZ_AUDYT_LM" VARCHAR2(100),
	"IPRZ_AUDYT_KM" VARCHAR2(100)
   )
   TABLESPACE EG_D;
   
   ALTER TABLE "EGADM1"."ZPT_IPRZ" ADD CONSTRAINT "ZPT_IPRZ_PK" PRIMARY KEY ("IPRZ_ID") USING INDEX TABLESPACE EG_X ENABLE;
   
   COMMENT ON COLUMN "EGADM1"."ZPT_IPRZ"."IPRZ_ID" IS 'Klucz g��wny tabeli';
   COMMENT ON COLUMN "EGADM1"."ZPT_IPRZ"."IPRZ_PRC_ID" IS 'Pracownik ? klucz do tabeli EK_PRACOWNICY';
   COMMENT ON COLUMN "EGADM1"."ZPT_IPRZ"."IPRZ_PODSUMOWANIE" IS 'Podsumowanie realizacji dzia�a�';
   COMMENT ON COLUMN "EGADM1"."ZPT_IPRZ"."IPRZ_SCIEZKA" IS '�cie�ka Kariery ? s�ownik w CSS: IPRZ_SCIEZKA_KARIERY';
   COMMENT ON COLUMN "EGADM1"."ZPT_IPRZ"."IPRZ_PROGRAM" IS 'Program rozwojowy ? s�ownik w CSS: IPRZ_PROGRAM_ROZWOJOWY';
   COMMENT ON COLUMN "EGADM1"."ZPT_IPRZ"."IPRZ_TALENTY" IS 'Warto�� T/N ';
   COMMENT ON COLUMN "EGADM1"."ZPT_IPRZ"."IPRZ_AUDYT_UT" IS 'U�ytkownik kt�ry utworzy� rekord ';
   COMMENT ON COLUMN "EGADM1"."ZPT_IPRZ"."IPRZ_AUDYT_DT" IS 'Kiedy utworzy� ';
   COMMENT ON COLUMN "EGADM1"."ZPT_IPRZ"."IPRZ_AUDYT_UM" IS 'Kto modyfikowa� rekord ';
   COMMENT ON COLUMN "EGADM1"."ZPT_IPRZ"."IPRZ_AUDYT_DM" IS 'Kiedy modyfikowa� ';
   COMMENT ON COLUMN "EGADM1"."ZPT_IPRZ"."IPRZ_AUDYT_LM" IS 'Liczba modyfikacji  ';
   COMMENT ON COLUMN "EGADM1"."ZPT_IPRZ"."IPRZ_AUDYT_KM" IS 'Komputer z kt�rego dokonano zmiany  ';

  CREATE OR REPLACE EDITIONABLE TRIGGER "EGADM1"."PPG_IPRZ_BIUR_AUDYT" 
  BEFORE INSERT or UPDATE ON EGADM1.ZPT_IPRZ
  FOR EACH ROW 
DECLARE
  v_pom VARCHAR2 (255);
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.IPRZ_AUDYT_UT, :NEW.IPRZ_AUDYT_DT, v_pom, :NEW.IPRZ_AUDYT_LM, :OLD.IPRZ_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.IPRZ_AUDYT_UT, :NEW.IPRZ_AUDYT_DT, v_pom, :NEW.IPRZ_AUDYT_LM, :OLD.IPRZ_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.IPRZ_AUDYT_UM, :NEW.IPRZ_AUDYT_DM, :NEW.IPRZ_AUDYT_KM, :NEW.IPRZ_AUDYT_LM, :OLD.IPRZ_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.IPRZ_AUDYT_UM, :NEW.IPRZ_AUDYT_DM, :NEW.IPRZ_AUDYT_KM, :NEW.IPRZ_AUDYT_LM, :OLD.IPRZ_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;

/
ALTER TRIGGER "EGADM1"."PPG_IPRZ_BIUR_AUDYT" ENABLE;
