create or replace PACKAGE BODY PP_GENERATOR_RAPORTOW AS


FUNCTION generuj_pdf_start_fazal (p_par_fa_id NUMBER) RETURN NUMBER IS
 v_prc_id   number;
v_kg_id    number;
v_p_id     number;
v_par_rap  bop_parametry.parametry;
 
 BEGIN
eap_globals.ustaw_firme(1);
  eap_globals.ustaw_konsolidacje('N');

  --lp 12 bo parametr o nazwie P_FA_ID to 12. parametr raportu w tabeli konfiguracyjnej css_parametry_raporto
  v_par_rap(12).par_nazwa := 'P_FA_ID';
  v_par_rap(12).par_wartosc := p_par_fa_id;
  v_par_rap(12).par_opis := '';


  bop_korespondencja.p_wstaw_zadanie_kolejka (
    p_bka_id    => 1
  , p_typ       => 'FAZAL'
  , p_id        => v_p_id
  , p_opis      => 'test RANT'
  , p_format    => 'pdf'
  , p_parametry => v_par_rap
  , p_generacja => 'REPORTS'
  , p_prc_id    => v_prc_id
  , p_kg_id     => v_kg_id
  );
  -- to przenieść do API jako jawny parametr, kontekst użytkownika jest bardzo istotny
  -- na razie nie jest oprogramowany i raport wykonuje się z sesji egadm1
  update csst_kolejka_generacji set
    kg_uzytkownik = 'TEST'
  where kg_id = v_kg_id;
  
  --przepisuje parametry z css_parametry_raportow do kolejki  
  bop_parametry.p_przygotuj (p_bka_id =>1, p_typ => 'FAZAL', p_id => v_p_id, p_parametry => v_par_rap, p_rodzaj_gen => 'REPORTS');

  -- id zdania w kg (kolejce generacji)
  dbms(v_kg_id);
  --DBMS_OUTPUT.put_line(v_kg_id);

  commit;
  RETURN v_kg_id;
 END;


END PP_GENERATOR_RAPORTOW;