create or replace PROCEDURE WSTAW_WIADOMOSC_UZT 
(
  UZT IN VARCHAR2 
, TRESC IN VARCHAR2 
, DATA_WYSLANIA IN DATE default sysdate
)
AS 
   PRAGMA AUTONOMOUS_TRANSACTION;
   x_uzt VARCHAR2(50);
   err_code VARCHAR2(50);
   err_msg VARCHAR2(300);
BEGIN

  IF(UZT is not null) THEN

    BEGIN
      select uzt_nazwa into x_uzt from eat_uzytkownicy where uzt_nazwa = UZT;
    EXCEPTION
        WHEN no_data_found THEN
			BEGIN
			SELECT DDF_UZT_NAZWA into x_uzt
				FROM eat_dostepy_do_firm where DDF_UZT_NAZWA = UZT and ddf_frm_id = 1;
		  EXCEPTION
        when no_data_found THEN
          insert into PPT_ERR_LOG (ERR_TRESC_BLEDU) values ('Nie znaleziono użytkownika ' || UZT || ' w tabeli EAT_UZYTKOWNICY i EAT_DOSTEPY_DO_FIRM');
			commit;
		  END;
    END;

    IF(TRESC is not null) THEN
      insert into EAT_WIADOMOSCI (WDM_ID, WDM_FRM_ID, WDM_UZT_NAZWA, WDM_DATA_WSTAWIENIA, WDM_F_PRZECZYTANA, WDM_TYP, WDM_DATA_WYSLANIA, WDM_TRESC)
      values					  (EAADM.EAS_WDM.nextval, 1, x_uzt, sysdate, 'N', 'E', DATA_WYSLANIA, TRESC);
      commit;
      END IF;

END IF;
EXCEPTION
  WHEN OTHERS THEN
      err_code := SQLCODE;
      err_msg := SUBSTR(SQLERRM, 1, 200);

      INSERT INTO PPT_ERR_LOG (ERR_TRESC_BLEDU)
      VALUES (err_code || ' - ' || err_msg);
      rollback;
END WSTAW_WIADOMOSC_UZT;