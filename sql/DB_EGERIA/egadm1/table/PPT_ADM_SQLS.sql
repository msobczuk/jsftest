--------------------------------------------------------
--  DDL for Table PPT_ADM_SQLS
--------------------------------------------------------

  CREATE TABLE "EGADM1"."PPT_ADM_SQLS" 
   (	"PPSL_ID" VARCHAR2(50 BYTE), 
	"PPSL_QUERY" CLOB, 
	"PPSL_QUERY_INSERTED" CLOB, 
	"PPSL_QUERY_UPDATED" CLOB, 
	"PPSL_QUERY_DELETED" CLOB, 
	"PPSL_CACHEDURATION" NUMBER DEFAULT 30*60000, 
	"PPSL_DESCRIPTION" VARCHAR2(1024 BYTE), 
	"PPSL_SCOPE" NUMBER, 
	"PPSL_AUDYT_UT" VARCHAR2(30 BYTE), 
	"PPSL_AUDYT_DT" DATE, 
	"PPSL_AUDYT_UM" VARCHAR2(30 BYTE), 
	"PPSL_AUDYT_DM" DATE, 
	"PPSL_AUDYT_LM" VARCHAR2(100 BYTE), 
	"PPSL_AUDYT_KM" VARCHAR2(100 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "EG_D" 
 LOB ("PPSL_QUERY") STORE AS SECUREFILE (
  TABLESPACE "EG_D" ENABLE STORAGE IN ROW CHUNK 8192
  NOCACHE NOLOGGING  NOCOMPRESS  KEEP_DUPLICATES 
  STORAGE(INITIAL 106496 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) 
 LOB ("PPSL_QUERY_INSERTED") STORE AS SECUREFILE (
  TABLESPACE "EG_D" ENABLE STORAGE IN ROW CHUNK 8192
  NOCACHE NOLOGGING  NOCOMPRESS  KEEP_DUPLICATES 
  STORAGE(INITIAL 106496 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) 
 LOB ("PPSL_QUERY_UPDATED") STORE AS SECUREFILE (
  TABLESPACE "EG_D" ENABLE STORAGE IN ROW CHUNK 8192
  NOCACHE NOLOGGING  NOCOMPRESS  KEEP_DUPLICATES 
  STORAGE(INITIAL 106496 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) 
 LOB ("PPSL_QUERY_DELETED") STORE AS SECUREFILE (
  TABLESPACE "EG_D" ENABLE STORAGE IN ROW CHUNK 8192
  NOCACHE NOLOGGING  NOCOMPRESS  KEEP_DUPLICATES 
  STORAGE(INITIAL 106496 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)) ;

   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_ID" IS 'Identyfikator zapytania';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_QUERY" IS 'Tre�� zapytani SELEC';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_QUERY_INSERTED" IS 'Tre�� DML INSERT';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_QUERY_UPDATED" IS 'Tre�� DML UPDATE';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_QUERY_DELETED" IS 'Tre�� DML DELETE';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_CACHEDURATION" IS 'Czas cache''owania';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_DESCRIPTION" IS 'Opis zapytania';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_SCOPE" IS 'Zakres dla JSF (1=applicationscope, 1=sessionscope/viewscope...)';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."PPT_ADM_SQLS"."PPSL_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord.';
   COMMENT ON TABLE "EGADM1"."PPT_ADM_SQLS"  IS 'Tabela zawieraj�?ca zapytania/s�?owniki SQL wykorzystywane przez modu�? PP.';
