--------------------------------------------------------
--  DDL for Table DELT_POZYCJE_ROZLICZEN
--------------------------------------------------------

  CREATE TABLE "EGADM1"."DELT_POZYCJE_ROZLICZEN" 
   (	"PROZ_ID" NUMBER(10,0), 
	"PROZ_ROZD_ID" NUMBER(10,0), 
	"PROZ_TPOZ_ID" NUMBER(10,0), 
	"PROZ_WAL_ID" NUMBER(10,0), 
	"PROZ_AUDYT_DT" DATE, 
	"PROZ_AUDYT_UT" VARCHAR2(30 BYTE), 
	"PROZ_AUDYT_KT" VARCHAR2(100 BYTE), 
	"PROZ_AUDYT_LM" NUMBER, 
	"PROZ_LP" NUMBER(3,0), 
	"PROZ_F_WALUTA_KRAJOWA" VARCHAR2(1 BYTE), 
	"PROZ_KWOTA" NUMBER(17,2), 
	"PROZ_KWOTA_WALUTY" NUMBER(17,2), 
	"PROZ_F_OSOBA_TOWARZYSZACA" VARCHAR2(1 BYTE), 
	"PROZ_AUDYT_DM" DATE, 
	"PROZ_AUDYT_UM" VARCHAR2(30 BYTE), 
	"PROZ_AUDYT_KM" VARCHAR2(100 BYTE), 
	"PROZ_WZOR_Z_SZABLONU" VARCHAR2(3 BYTE), 
	"PROZ_OPIS" VARCHAR2(4000 BYTE), 
	"PROZ_ILOSC" NUMBER(14,6) DEFAULT 0, 
	"PROZ_KR_ID" NUMBER(10,0) DEFAULT Null, 
	"PROZ_STAWKA" NUMBER(14,6) DEFAULT 0
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "NZ_D" ;

   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_ID" IS 'ID pozycji rozliczenia';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_ROZD_ID" IS 'ID rozliczenia delegacji (klucz obcy do DELT_ROZLICZENIA_DELEGACJI)';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_TPOZ_ID" IS 'ID typu pozycji (klucz obcy do DELT_TYPY_POZYCJI)';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_WAL_ID" IS 'ID waluty (klucz obcy do CSS_WALUTY)';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_AUDYT_KT" IS 'Nazwa komputera, z kt�rego utworzono rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_LP" IS 'Liczba porz�dkowa';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_F_WALUTA_KRAJOWA" IS 'Flaga informuj�ca czy pozycja jest w walucie krajowej';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_KWOTA" IS 'Kwota przeliczona na walut� ksi�gow�';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_KWOTA_WALUTY" IS 'Warto�� pozycji w walucie';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_F_OSOBA_TOWARZYSZACA" IS 'Flaga informuj�ca czy pozycja dot. koszt�w osoby towarzysz�cej w trakcie delegacji';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_WZOR_Z_SZABLONU" IS 'Wz�r obliczaj�cy warto�� pozycji';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_OPIS" IS 'Dowolny opis';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_ILOSC" IS 'Ilo�� powi�zana z typem rozliczenia';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_KR_ID" IS 'ID kraju (klucz obcy z tabeli CSS_KRAJE)';
   COMMENT ON COLUMN "EGADM1"."DELT_POZYCJE_ROZLICZEN"."PROZ_STAWKA" IS 'Stawka';
   COMMENT ON TABLE "EGADM1"."DELT_POZYCJE_ROZLICZEN"  IS 'Pozycje rozlicze�';
