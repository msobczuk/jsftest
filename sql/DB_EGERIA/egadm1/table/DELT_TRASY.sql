--------------------------------------------------------
--  DDL for Table DELT_TRASY
--------------------------------------------------------

  CREATE TABLE "EGADM1"."DELT_TRASY" 
   (	"TRS_ID" NUMBER(10,0), 
	"TRS_KR_ID_OD" NUMBER(10,0), 
	"TRS_KR_ID_DO" NUMBER(10,0), 
	"TRS_ROZD_ID" NUMBER(10,0), 
	"TRS_AUDYT_DT" DATE, 
	"TRS_AUDYT_UT" VARCHAR2(30 BYTE), 
	"TRS_AUDYT_KT" VARCHAR2(100 BYTE), 
	"TRS_AUDYT_LM" NUMBER, 
	"TRS_LP" NUMBER(3,0), 
	"TRS_MIEJSCOWOSC_OD" VARCHAR2(50 BYTE), 
	"TRS_MIEJSCOWOSC_DO" VARCHAR2(50 BYTE), 
	"TRS_DATA_OD" DATE, 
	"TRS_DATA_DO" DATE, 
	"TRS_AUDYT_DM" DATE, 
	"TRS_AUDYT_UM" VARCHAR2(30 BYTE), 
	"TRS_AUDYT_KM" VARCHAR2(100 BYTE), 
	"TRS_ODLEGLOSC" NUMBER(17,2), 
	"TRS_OPIS" VARCHAR2(4000 BYTE), 
	"TRS_SRODEK_LOKOMOCJI" VARCHAR2(2 BYTE), 
	"TRS_KR_ID_DIETY" NUMBER(10,0)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "NZ_D" ;

   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_ID" IS 'ID trasy';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_KR_ID_OD" IS 'ID kraju wyjazdu (klucz obcy do CSS_KRAJE)';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_KR_ID_DO" IS 'ID kraju przyjazdu (klucz obcy do CSS_KRAJE)';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_ROZD_ID" IS 'ID rozliczenia delegacji (klucz obcy do DELT_ROZLICZENIA_DELEGACJI)';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_AUDYT_KT" IS 'Nazwa komputera, z kt�rego utworzono rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_LP" IS 'Liczba porz�dkowa';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_MIEJSCOWOSC_OD" IS 'Nazwa miejscowo�ci odjazdu';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_MIEJSCOWOSC_DO" IS 'Nazwa miejscowo�ci przyjazdu ';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_DATA_OD" IS 'Data i godzina (z dok�adno�ci� do minuty) przyjazdu';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_DATA_DO" IS 'Data i godzina (z dok�adno�ci� do minuty) odjazdu';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_ODLEGLOSC" IS 'Odleg�o�� pozycji trasy w km';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_OPIS" IS 'Opis trasy';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_SRODEK_LOKOMOCJI" IS '�rodek lokomocji wg s�ownika DEL_SRODEK_LOKOMOCJI (EGADM1.CSS_WARTOSCI_SLOWNIKOW / WSL_SL_NAZWA=DEL_SRODEK_LOKOMOCJI)';
   COMMENT ON COLUMN "EGADM1"."DELT_TRASY"."TRS_KR_ID_DIETY" IS 'Kraj wg kt�rego ustalana jest stawka diety w del. zagranicznych MF';
   COMMENT ON TABLE "EGADM1"."DELT_TRASY"  IS 'Trasy';
