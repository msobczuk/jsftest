--------------------------------------------------------
--  DDL for Table DELT_PRZYPISANE_SK
--------------------------------------------------------

  CREATE TABLE "EGADM1"."DELT_PRZYPISANE_SK" 
   (	"PSKO_CDEL_ID" NUMBER(10,0), 
	"PSKO_SK_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "NZ_D" ;

   COMMENT ON COLUMN "EGADM1"."DELT_PRZYPISANE_SK"."PSKO_CDEL_ID" IS 'ID celu (klucz obcy do DELT_CELE_DELEGACJI)';
   COMMENT ON COLUMN "EGADM1"."DELT_PRZYPISANE_SK"."PSKO_SK_ID" IS 'ID stanowiska koszt�w (klucz obcy do CSS_STANOWISKA_KOSZTOW)';
   COMMENT ON TABLE "EGADM1"."DELT_PRZYPISANE_SK"  IS 'Przypisane stanowiska koszt�w';
