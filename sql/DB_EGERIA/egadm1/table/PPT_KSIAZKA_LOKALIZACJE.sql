--------------------------------------------------------
--  DDL for Table PPT_KSIAZKA_LOKALIZACJE
--------------------------------------------------------

  CREATE TABLE "EGADM1"."PPT_KSIAZKA_LOKALIZACJE" 
   (	"KLOK_ID" NUMBER, 
	"KLOK_KOD" VARCHAR2(10 BYTE), 
	"KLOK_WARTOSC" VARCHAR2(100 BYTE), 
	"KLOK_KLOK_ID" NUMBER, 
	"KLOK_AUDYT_UT" VARCHAR2(30 BYTE), 
	"KLOK_AUDYT_DT" DATE, 
	"KLOK_AUDYT_UM" VARCHAR2(30 BYTE), 
	"KLOK_AUDYT_DM" DATE, 
	"KLOK_AUDYT_LM" VARCHAR2(100 BYTE), 
	"KLOK_AUDYT_KM" VARCHAR2(100 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "EG_D" ;

   COMMENT ON COLUMN "EGADM1"."PPT_KSIAZKA_LOKALIZACJE"."KLOK_ID" IS 'Identyfikator lokalizacji';
   COMMENT ON COLUMN "EGADM1"."PPT_KSIAZKA_LOKALIZACJE"."KLOK_KOD" IS 'Kod lokalizacji';
   COMMENT ON COLUMN "EGADM1"."PPT_KSIAZKA_LOKALIZACJE"."KLOK_WARTOSC" IS 'Nazwa lokalizacji';
   COMMENT ON COLUMN "EGADM1"."PPT_KSIAZKA_LOKALIZACJE"."KLOK_KLOK_ID" IS 'Identyfikator lokalizacji nadrz�?dnej';
   COMMENT ON COLUMN "EGADM1"."PPT_KSIAZKA_LOKALIZACJE"."KLOK_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."PPT_KSIAZKA_LOKALIZACJE"."KLOK_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."PPT_KSIAZKA_LOKALIZACJE"."KLOK_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."PPT_KSIAZKA_LOKALIZACJE"."KLOK_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."PPT_KSIAZKA_LOKALIZACJE"."KLOK_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."PPT_KSIAZKA_LOKALIZACJE"."KLOK_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord.';
   COMMENT ON TABLE "EGADM1"."PPT_KSIAZKA_LOKALIZACJE"  IS 'Tabela przechowuj�?ca s�?ownik lokalizacji.';
