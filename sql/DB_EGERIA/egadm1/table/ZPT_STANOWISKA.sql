--------------------------------------------------------
--  DDL for Table ZPT_STANOWISKA
--------------------------------------------------------

  CREATE TABLE "EGADM1"."ZPT_STANOWISKA" 
   (	"STN_ID" NUMBER(10,0), 
	"STN_ILOSC_ETATOW" NUMBER(6,2), 
	"STN_AUDYT_DT" DATE, 
	"STN_AUDYT_KT" VARCHAR2(100 BYTE), 
	"STN_AUDYT_UT" VARCHAR2(30 BYTE), 
	"STN_AUDYT_LM" NUMBER(5,0), 
	"STN_AUDYT_UM" VARCHAR2(30 BYTE), 
	"STN_AUDYT_DM" DATE, 
	"STN_AUDYT_KM" VARCHAR2(100 BYTE), 
	"STN_TYP" VARCHAR2(10 BYTE), 
	"STN_DATA_DO" DATE, 
	"STN_DATA_OD" DATE, 
	"STN_REF_URZAD" VARCHAR2(100 BYTE), 
	"STN_REF_KWOTA" NUMBER(8,2), 
	"STN_REF_OD" DATE, 
	"STN_REF_DO" DATE, 
	"STN_NAZWA" VARCHAR2(100 BYTE), 
	"STN_F_SZKODLIWE" VARCHAR2(1 BYTE) DEFAULT 'N', 
	"STN_F_SP" VARCHAR2(1 BYTE) DEFAULT 'T', 
	"STN_F_SPN" VARCHAR2(1 BYTE) DEFAULT 'T', 
	"STN_STAN_DEFINICJI" VARCHAR2(1 BYTE) DEFAULT 'U', 
	"STN_KOD" VARCHAR2(5 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "ZP_D" ;

   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_ID" IS 'Klucz g��wny tabeli';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_ILOSC_ETATOW" IS 'Ilosc etat�w w tej jednostce dl aptej funkcji';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_AUDYT_DT" IS 'Data utworzenia';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_AUDYT_KT" IS 'Nazwa komputera z kt�rego utworzono';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy�';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_AUDYT_LM" IS 'Liczba modyfikacji';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatni zmodyfikowa�';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_AUDYT_DM" IS 'Data ostantniej modyfikacji';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_AUDYT_KM" IS 'Nazwa komputera, kt�rego ostatnio zmodyfikowano';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_TYP" IS 'Typ stanowiska';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_DATA_DO" IS 'Data do';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_DATA_OD" IS 'Data od';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_REF_URZAD" IS 'Dane urz�du refinansuj�cego stanowisko';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_REF_KWOTA" IS 'Kwota refinansowania';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_REF_OD" IS 'Od kiedy stanowisko jest refinansowane';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_REF_DO" IS 'Do kiedy stanowisko jest refinansowane';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_NAZWA" IS 'Nazwa stanowiska';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_F_SZKODLIWE" IS 'Praca w warunkach szkodliwych';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_F_SP" IS 'Czy wy�wietla� na formatce "Umowa o prac�"?';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_F_SPN" IS 'Czy wy�wietla� na formatce "Stosunek pracy - nauczyciele"?';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_STAN_DEFINICJI" IS 'Stan definicji U uzywane A archiwalne';
   COMMENT ON COLUMN "EGADM1"."ZPT_STANOWISKA"."STN_KOD" IS 'Kod stanowiska';
   COMMENT ON TABLE "EGADM1"."ZPT_STANOWISKA"  IS 'Tabela przechowuje dane o stanowiskach';
