--------------------------------------------------------
--  DDL for Table DELT_PODZIAL_BUDZETOWY
--------------------------------------------------------

  CREATE TABLE "EGADM1"."DELT_PODZIAL_BUDZETOWY" 
   (	"PDB_ID" NUMBER(10,0), 
	"PDB_PZB_ID" NUMBER(10,0), 
	"PDB_WND_ID" NUMBER(10,0), 
	"PDB_KWOTA" NUMBER(12,2), 
	"PDB_LP" NUMBER(3,0), 
	"PDB_AUDYT_DT" DATE, 
	"PDB_AUDYT_UT" VARCHAR2(30 BYTE), 
	"PDB_AUDYT_KT" VARCHAR2(100 BYTE), 
	"PDB_AUDYT_LM" NUMBER, 
	"PDB_AUDYT_DM" DATE, 
	"PDB_AUDYT_UM" VARCHAR2(30 BYTE), 
	"PDB_AUDYT_KM" VARCHAR2(100 BYTE), 
	"PDB_KR_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "NZ_D" ;

   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_ID" IS 'Identyfikator rekordu w tabeli DELT_PODZIAL_BUDZETOWY.';
   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_PZB_ID" IS 'Klucz obcy do tabeli DELT_POZYCJE_BUDZETOWE.';
   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_WND_ID" IS 'Klucz obcy do tabeli DELT_WNIOSKI_DELEGACJI.';
   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_KWOTA" IS 'Kwota podzia�u bud�etowego';
   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_LP" IS 'Kolejno�� przypisania podzia�u bud�etowego.';
   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_AUDYT_KT" IS 'Nazwa komputera, z kt�rego utworzono rekord.';
   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord.';
   COMMENT ON COLUMN "EGADM1"."DELT_PODZIAL_BUDZETOWY"."PDB_KR_ID" IS 'Klucz obcy do tabeli CSS_KRAJE.';
   COMMENT ON TABLE "EGADM1"."DELT_PODZIAL_BUDZETOWY"  IS 'Podzia� bud�etowy';
