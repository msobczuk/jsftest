--------------------------------------------------------
--  DDL for Table ZPT_PLAN_ZIW
--------------------------------------------------------

  CREATE TABLE "EGADM1"."ZPT_PLAN_ZIW" 
   (	"PZW_ID" NUMBER(10,0), 
	"PZW_NUMER" NUMBER(10,0), 
	"PZW_DATA_UTWORZENIA" DATE, 
	"PZW_DATA_OD" DATE, 
	"PZW_DATA_DO" DATE, 
	"PZW_F_ZATWIERDZONY" VARCHAR2(1 BYTE) DEFAULT 'N', 
	"PZW_AUDYT_UT" VARCHAR2(30 BYTE), 
	"PZW_AUDYT_DT" DATE, 
	"PZW_AUDYT_UM" VARCHAR2(30 BYTE), 
	"PZW_AUDYT_DM" DATE, 
	"PZW_AUDYT_KT" VARCHAR2(100 BYTE), 
	"PZW_AUDYT_KM" VARCHAR2(100 BYTE), 
	"PZW_AUDYT_LM" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ZP_D" ;

   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_ID" IS 'Identyfikator rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_NUMER" IS 'Numer planu';
   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_DATA_UTWORZENIA" IS 'Data utworzenia planu';
   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_DATA_OD" IS 'Data od obowiazywania planu';
   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_DATA_DO" IS 'Data do obowiazywania planu';
   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_F_ZATWIERDZONY" IS 'Czy plan zatwierdzony?';
   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_AUDYT_KT" IS 'Nazwa komputera, z kt�rego utworzono rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_PLAN_ZIW"."PZW_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON TABLE "EGADM1"."ZPT_PLAN_ZIW"  IS 'Planowanie zatrudnienia i wynagordzen';
