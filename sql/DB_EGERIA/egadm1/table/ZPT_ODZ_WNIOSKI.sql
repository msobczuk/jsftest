--------------------------------------------------------
--  DDL for Table ZPT_ODZ_WNIOSKI
--------------------------------------------------------

  CREATE TABLE "EGADM1"."ZPT_ODZ_WNIOSKI" 
   (	"ODW_ID" NUMBER(10,0), 
	"ODW_PRC_ID" NUMBER(10,0), 
	"ODW_NUMER" VARCHAR2(25 BYTE), 
	"ODW_DATA" DATE, 
	"ODW_F_ZATW" VARCHAR2(1 BYTE), 
	"ODW_DOK_ID" NUMBER(10,0), 
	"ODW_AUDYT_UT" VARCHAR2(30 BYTE), 
	"ODW_AUDYT_UM" VARCHAR2(30 BYTE), 
	"ODW_AUDYT_DT" DATE, 
	"ODW_AUDYT_DM" DATE, 
	"ODW_AUDYT_KT" VARCHAR2(100 BYTE), 
	"ODW_AUDYT_KM" VARCHAR2(100 BYTE), 
	"ODW_AUDYT_LM" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ZP_D" ;

   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_ID" IS 'Identyfikator wniosku';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_PRC_ID" IS 'Identyfikator pracownika';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_NUMER" IS 'Numer wniosku';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_DATA" IS 'Data wniosku';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_F_ZATW" IS 'Czy wniosek jest zatwierdzony?';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_DOK_ID" IS 'Klucz obcy do tabeli KGT_DOKUMENTY';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_AUDYT_KT" IS 'Nazwa komputera, z kt�rego utworzono rekord.';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord.';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_WNIOSKI"."ODW_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON TABLE "EGADM1"."ZPT_ODZ_WNIOSKI"  IS 'Wnioski o wydanie odzie�y';
