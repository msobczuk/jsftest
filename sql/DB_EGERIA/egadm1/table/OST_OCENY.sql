--------------------------------------------------------
--  DDL for Table OST_OCENY
--------------------------------------------------------

  CREATE TABLE "EGADM1"."OST_OCENY" 
   (	"OC_LSZ_ID" NUMBER(10,0), 
	"OC_KO_ID" NUMBER(10,0), 
	"OC_NOTA" VARCHAR2(30 BYTE), 
	"OC_AUDYT_UT" VARCHAR2(30 BYTE), 
	"OC_AUDYT_DT" DATE, 
	"OC_AUDYT_KT" VARCHAR2(100 BYTE), 
	"OC_AUDYT_KM" VARCHAR2(100 BYTE), 
	"OC_AUDYT_LM" NUMBER(5,0), 
	"OC_AUDYT_UM" VARCHAR2(30 BYTE), 
	"OC_AUDYT_DM" DATE, 
	"OC_OPIS" VARCHAR2(1000 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "OS_D" ;

   COMMENT ON COLUMN "EGADM1"."OST_OCENY"."OC_LSZ_ID" IS 'Klucz obcy do tabeli OST_LISTY_SZKOLEN';
   COMMENT ON COLUMN "EGADM1"."OST_OCENY"."OC_KO_ID" IS 'Klucz obcy do tabeli OST_KRYTERIA_OCENY';
   COMMENT ON COLUMN "EGADM1"."OST_OCENY"."OC_NOTA" IS 'Ocena wg przyjetej skali';
   COMMENT ON COLUMN "EGADM1"."OST_OCENY"."OC_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy�';
   COMMENT ON COLUMN "EGADM1"."OST_OCENY"."OC_AUDYT_DT" IS 'Data utworzenia';
   COMMENT ON COLUMN "EGADM1"."OST_OCENY"."OC_AUDYT_KT" IS 'Nazwa komputera z kt�rego utworzono';
   COMMENT ON COLUMN "EGADM1"."OST_OCENY"."OC_AUDYT_KM" IS 'Nazwa komputera, kt�rego ostatnio zmodyfikowano';
   COMMENT ON COLUMN "EGADM1"."OST_OCENY"."OC_AUDYT_LM" IS 'Liczba modyfikacji';
   COMMENT ON COLUMN "EGADM1"."OST_OCENY"."OC_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatni zmodyfikowa�';
   COMMENT ON COLUMN "EGADM1"."OST_OCENY"."OC_AUDYT_DM" IS 'Data ostantniej modyfikacji';
   COMMENT ON COLUMN "EGADM1"."OST_OCENY"."OC_OPIS" IS 'Informacje opisowe';
   COMMENT ON TABLE "EGADM1"."OST_OCENY"  IS 'Oceny, wg kryteri�w';
