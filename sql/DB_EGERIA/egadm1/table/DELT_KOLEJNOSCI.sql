--------------------------------------------------------
--  DDL for Table DELT_KOLEJNOSCI
--------------------------------------------------------

  CREATE TABLE "EGADM1"."DELT_KOLEJNOSCI" 
   (	"KOL_ID" NUMBER(10,0), 
	"KOL_RDEL_ID" NUMBER(10,0), 
	"KOL_STWD_ID" NUMBER(10,0), 
	"KOL_AUDYT_DT" DATE, 
	"KOL_AUDYT_UT" VARCHAR2(30 BYTE), 
	"KOL_AUDYT_KT" VARCHAR2(100 BYTE), 
	"KOL_AUDYT_LM" NUMBER, 
	"KOL_LP" NUMBER(3,0), 
	"KOL_F_ZATWIERDZONY" VARCHAR2(1 BYTE), 
	"KOL_ETAP" VARCHAR2(3 BYTE), 
	"KOL_AUDYT_DM" DATE, 
	"KOL_AUDYT_UM" VARCHAR2(30 BYTE), 
	"KOL_AUDYT_KM" VARCHAR2(100 BYTE), 
	"KOL_F_GENERACJA_NUMERU" VARCHAR2(1 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "NZ_D" ;

   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_ID" IS 'ID kolejno�ci';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_RDEL_ID" IS 'ID rodzaju delegacji (klucz obcy do DELT_RODZAJE_DELEGACJI)';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_STWD_ID" IS 'ID statusu (klucz obcy do DELT_STATUSY)';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_AUDYT_KT" IS 'Nazwa komputera, z kt�rego utworzono rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_LP" IS 'Numer kolejny statusu w obiegu wybranego rodzaju delegacji ';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_F_ZATWIERDZONY" IS 'Flaga informuj�ca czy kolejno�� jest zatwierdzona';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_ETAP" IS 'Etap przetwarzania na jakim znajduje si� wniosek';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_KOLEJNOSCI"."KOL_F_GENERACJA_NUMERU" IS 'Czy generowac numer wniosku delegacji przy przej�ciu na t� kolejno�c';
   COMMENT ON TABLE "EGADM1"."DELT_KOLEJNOSCI"  IS 'Kolejno�ci';
