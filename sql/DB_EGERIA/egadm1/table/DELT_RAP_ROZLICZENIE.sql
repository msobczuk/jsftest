--------------------------------------------------------
--  DDL for Table DELT_RAP_ROZLICZENIE
--------------------------------------------------------

  CREATE TABLE "EGADM1"."DELT_RAP_ROZLICZENIE" 
   (	"ROZ_ID" NUMBER, 
	"ROZ_ROZD_ID" NUMBER, 
	"ROZ_ID_SESJI" NUMBER, 
	"ROZ_TYP" VARCHAR2(10 BYTE), 
	"ROZ_LP" NUMBER, 
	"ROZ_OPIS" VARCHAR2(100 BYTE), 
	"ROZ_PRZELICZENIE" VARCHAR2(1000 BYTE), 
	"ROZ_WALUTA1" NUMBER, 
	"ROZ_WALUTA2" NUMBER, 
	"ROZ_WALUTA3" NUMBER, 
	"ROZ_PRZELICZ_PLN" NUMBER, 
	"ROZ_AUDYT_UT" VARCHAR2(240 BYTE), 
	"ROZ_AUDYT_DT" DATE, 
	"ROZ_AUDYT_KT" VARCHAR2(100 BYTE), 
	"ROZ_AUDYT_LM" NUMBER, 
	"ROZ_AUDYT_UM" VARCHAR2(240 BYTE), 
	"ROZ_AUDYT_DM" DATE, 
	"ROZ_AUDYT_KM" VARCHAR2(100 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "NZ_D" ;

   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_ID" IS 'ID rekordu - klucz g��wny';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_ROZD_ID" IS 'ID istniej�cego rozliczenia';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_ID_SESJI" IS 'ID sesji';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_TYP" IS 'Typ pozycji rozliczenia';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_LP" IS 'Lp.';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_OPIS" IS 'Opis pozycji';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_PRZELICZENIE" IS 'Przeliczenie';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_WALUTA1" IS 'Warto�� w walucie 1';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_WALUTA2" IS 'Warto�� w walucie 2';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_WALUTA3" IS 'Warto�� w walucie 3';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_PRZELICZ_PLN" IS 'Warto�� przeliczona na PLN';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_AUDYT_KT" IS 'Nazwa komputera, z kt�rego utworzono rekord.';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."DELT_RAP_ROZLICZENIE"."ROZ_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord';
   COMMENT ON TABLE "EGADM1"."DELT_RAP_ROZLICZENIE"  IS 'Raporty rozliczenie';
