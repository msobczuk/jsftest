--------------------------------------------------------
--  DDL for Table ZPT_NABOR_ZESPOLY
--------------------------------------------------------

  CREATE TABLE "EGADM1"."ZPT_NABOR_ZESPOLY" 
   (	"NZE_ID" NUMBER(10,0), 
	"NZE_PRC_ID" NUMBER(10,0), 
	"NZE_NAB_WN5_ID" NUMBER(10,0), 
	"NZE_PRZEWODNICZACY" VARCHAR2(1 BYTE), 
	"NZE_AUDYT_UT" VARCHAR2(30 BYTE), 
	"NZE_AUDYT_DT" DATE, 
	"NZE_AUDYT_UM" VARCHAR2(30 BYTE), 
	"NZE_AUDYT_DM" DATE, 
	"NZE_AUDYT_LM" VARCHAR2(100 BYTE), 
	"NZE_AUDYT_KM" VARCHAR2(100 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "EG_D" ;

   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_ZESPOLY"."NZE_ID" IS 'klucz g��wny tabeli';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_ZESPOLY"."NZE_PRC_ID" IS 'id pracownika';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_ZESPOLY"."NZE_NAB_WN5_ID" IS 'id naboru z ZPT_NAB_WN5';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_ZESPOLY"."NZE_PRZEWODNICZACY" IS 'Czy przewodnicz�cy';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_ZESPOLY"."NZE_AUDYT_UT" IS 'U�ytkownik kt�ry utworzy� rekord ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_ZESPOLY"."NZE_AUDYT_DT" IS 'Kiedy utworzy� ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_ZESPOLY"."NZE_AUDYT_UM" IS 'Kto modyfikowa� rekord ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_ZESPOLY"."NZE_AUDYT_DM" IS 'Kiedy modyfikowa� ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_ZESPOLY"."NZE_AUDYT_LM" IS 'Liczba modyfikacji  ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NABOR_ZESPOLY"."NZE_AUDYT_KM" IS 'Komputer z kt�rego dokonano zmiany  ';
