--------------------------------------------------------
--  DDL for Table OST_KRYTERIA_OCENY
--------------------------------------------------------

  CREATE TABLE "EGADM1"."OST_KRYTERIA_OCENY" 
   (	"KO_ID" NUMBER(10,0), 
	"KO_F_ILOSCIOWE" VARCHAR2(1 BYTE), 
	"KO_NAZWA" VARCHAR2(50 BYTE), 
	"KO_AUDYT_KT" VARCHAR2(100 BYTE), 
	"KO_RODZAJ" VARCHAR2(50 BYTE), 
	"KO_AUDYT_UT" VARCHAR2(30 BYTE), 
	"KO_AUDYT_DT" DATE, 
	"KO_AUDYT_LM" NUMBER(5,0), 
	"KO_AUDYT_KM" VARCHAR2(100 BYTE), 
	"KO_AUDYT_UM" VARCHAR2(30 BYTE), 
	"KO_AUDYT_DM" DATE, 
	"KO_OPIS" VARCHAR2(200 BYTE)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "OS_D" ;

   COMMENT ON COLUMN "EGADM1"."OST_KRYTERIA_OCENY"."KO_ID" IS 'Klucz g��wny tabeli';
   COMMENT ON COLUMN "EGADM1"."OST_KRYTERIA_OCENY"."KO_F_ILOSCIOWE" IS 'Czy kryterium jest jako�ciowe czy ilo�ciowe';
   COMMENT ON COLUMN "EGADM1"."OST_KRYTERIA_OCENY"."KO_NAZWA" IS 'Nazwa kryterium';
   COMMENT ON COLUMN "EGADM1"."OST_KRYTERIA_OCENY"."KO_AUDYT_KT" IS 'Nazwa komputera z kt�rego utworzono';
   COMMENT ON COLUMN "EGADM1"."OST_KRYTERIA_OCENY"."KO_RODZAJ" IS 'Rodzaj kryterium';
   COMMENT ON COLUMN "EGADM1"."OST_KRYTERIA_OCENY"."KO_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy�';
   COMMENT ON COLUMN "EGADM1"."OST_KRYTERIA_OCENY"."KO_AUDYT_DT" IS 'Data utworzenia';
   COMMENT ON COLUMN "EGADM1"."OST_KRYTERIA_OCENY"."KO_AUDYT_LM" IS 'Liczba modyfikacji';
   COMMENT ON COLUMN "EGADM1"."OST_KRYTERIA_OCENY"."KO_AUDYT_KM" IS 'Nazwa komputera, kt�rego ostatnio zmodyfikowano';
   COMMENT ON COLUMN "EGADM1"."OST_KRYTERIA_OCENY"."KO_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatni zmodyfikowa�';
   COMMENT ON COLUMN "EGADM1"."OST_KRYTERIA_OCENY"."KO_AUDYT_DM" IS 'Data ostantniej modyfikacji';
   COMMENT ON COLUMN "EGADM1"."OST_KRYTERIA_OCENY"."KO_OPIS" IS 'Dodatkowe informacje o kryterium';
   COMMENT ON TABLE "EGADM1"."OST_KRYTERIA_OCENY"  IS 'Zdefiniowane kryteria pceny wg kt�rych b�dzie szkolenie oceniane';
