--------------------------------------------------------
--  DDL for Table ZPT_WYMIARY_PRC
--------------------------------------------------------

  CREATE TABLE "EGADM1"."ZPT_WYMIARY_PRC" 
   (	"WYP_ID" NUMBER(10,0), 
	"WYP_PRC_ID" NUMBER(10,0), 
	"WYP_KOD" VARCHAR2(50 BYTE), 
	"WYP_WYMIAR" VARCHAR2(50 BYTE), 
	"WYP_AUDYT_UT" VARCHAR2(30 BYTE), 
	"WYP_AUDYT_UM" VARCHAR2(30 BYTE), 
	"WYP_AUDYT_DT" DATE, 
	"WYP_AUDYT_DM" DATE, 
	"WYP_AUDYT_KT" VARCHAR2(100 BYTE), 
	"WYP_AUDYT_KM" VARCHAR2(100 BYTE), 
	"WYP_AUDYT_LM" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "ZP_D" ;

   COMMENT ON COLUMN "EGADM1"."ZPT_WYMIARY_PRC"."WYP_ID" IS 'Identyfikator rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_WYMIARY_PRC"."WYP_PRC_ID" IS 'Identyfikator pracownika z tabeli ZPT_WYMIARY';
   COMMENT ON COLUMN "EGADM1"."ZPT_WYMIARY_PRC"."WYP_KOD" IS 'Kod wymiaru pracownika pobierany ze s�ownika ZP_WYMIARY_PRC';
   COMMENT ON COLUMN "EGADM1"."ZPT_WYMIARY_PRC"."WYP_WYMIAR" IS 'Warto�� wymiaru';
   COMMENT ON COLUMN "EGADM1"."ZPT_WYMIARY_PRC"."WYP_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_WYMIARY_PRC"."WYP_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_WYMIARY_PRC"."WYP_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_WYMIARY_PRC"."WYP_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_WYMIARY_PRC"."WYP_AUDYT_KT" IS 'Nazwa komputera, z kt�rego utworzono rekord.';
   COMMENT ON COLUMN "EGADM1"."ZPT_WYMIARY_PRC"."WYP_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord.';
   COMMENT ON COLUMN "EGADM1"."ZPT_WYMIARY_PRC"."WYP_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON TABLE "EGADM1"."ZPT_WYMIARY_PRC"  IS 'Wymiary pracy';
