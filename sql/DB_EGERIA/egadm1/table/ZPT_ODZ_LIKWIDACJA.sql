--------------------------------------------------------
--  DDL for Table ZPT_ODZ_LIKWIDACJA
--------------------------------------------------------

  CREATE TABLE "EGADM1"."ZPT_ODZ_LIKWIDACJA" 
   (	"ODL_ID" NUMBER(10,0), 
	"ODL_PRC_ID" NUMBER(10,0), 
	"ODL_NUMER" VARCHAR2(25 BYTE), 
	"ODL_DATA" DATE, 
	"ODL_F_ZATW" VARCHAR2(1 BYTE), 
	"ODL_ODW_ID" NUMBER(10,0), 
	"ODL_AUDYT_UT" VARCHAR2(30 BYTE), 
	"ODL_AUDYT_UM" VARCHAR2(30 BYTE), 
	"ODL_AUDYT_DT" DATE, 
	"ODL_AUDYT_DM" DATE, 
	"ODL_AUDYT_KT" VARCHAR2(100 BYTE), 
	"ODL_AUDYT_KM" VARCHAR2(100 BYTE), 
	"ODL_AUDYT_LM" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "ZP_D" ;

   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_ID" IS 'Identyfikator protoko�u likwidacyjnego';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_PRC_ID" IS 'Identyfikator pracownika';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_NUMER" IS 'Numer protoko�u likwidacyjnego';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_DATA" IS 'Data protoko�u likwidacyjnego';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_F_ZATW" IS 'Czy protok� jest zatwierdzony?';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_ODW_ID" IS 'Klucz obcy do tabeli ZPT_ODZ_WNIOSKI';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_AUDYT_KT" IS 'Nazwa komputera, z kt�rego utworzono rekord.';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord.';
   COMMENT ON COLUMN "EGADM1"."ZPT_ODZ_LIKWIDACJA"."ODL_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON TABLE "EGADM1"."ZPT_ODZ_LIKWIDACJA"  IS 'Protoko�y likwidacji odzie�y';
