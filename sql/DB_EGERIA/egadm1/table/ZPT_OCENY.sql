--------------------------------------------------------
--  DDL for Table ZPT_OCENY
--------------------------------------------------------

  CREATE TABLE "EGADM1"."ZPT_OCENY" 
   (	"OCE_ID" NUMBER(10,0), 
	"OCE_PRC_ID" NUMBER(10,0), 
	"OCE_PRC_ID_OCENIAJACY" NUMBER(10,0), 
	"OCE_OCENIAJACY" VARCHAR2(100 BYTE), 
	"OCE_DATA_OD" DATE, 
	"OCE_DATA_DO" DATE, 
	"OCE_DATA_OCENY" DATE, 
	"OCE_DATA_KOLEJNEJ_OCENY" DATE, 
	"OCE_OPIS" VARCHAR2(2400 BYTE), 
	"OCE_OPIS2" VARCHAR2(2400 BYTE), 
	"OCE_AUDYT_UT" VARCHAR2(30 BYTE),
	"OCE_AUDYT_DT" DATE,
	"OCE_AUDYT_UM" VARCHAR2(30 BYTE),
	"OCE_AUDYT_DM" DATE,
	"OCE_AUDYT_KT" VARCHAR2(100 BYTE), 
	"OCE_AUDYT_KM" VARCHAR2(100 BYTE), 
	"OCE_AUDYT_LM" NUMBER, 
	"OCE_MAX_DATA_OCENY" DATE, 
	"OCE_TYP_OCENY" VARCHAR2(10 BYTE), 
	"OCE_SREDNIA" NUMBER(5,3), 
	"OCE_CZY_WN_KOL_STOP" VARCHAR2(1 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "EK_D" ;

   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_ID" IS 'Klucz g��wny tabeli';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_PRC_ID" IS 'Klucz obcy do tabeli EK_PRACOWNICY';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_PRC_ID_OCENIAJACY" IS 'Powi�zanie z tabel� EK_PRACOWNICY - id oceniaj�cego';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_OCENIAJACY" IS 'Oceniaj�cy';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_DATA_OD" IS 'Data od';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_DATA_DO" IS 'Data do';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_DATA_OCENY" IS 'Data oceny';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_DATA_KOLEJNEJ_OCENY" IS 'Data kolejnej oceny';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_OPIS" IS 'Opis';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_OPIS2" IS 'Opis dodatkowy';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_AUDYT_KT" IS 'Nazwa komputera, z kt�rego utworzono rekord.';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord.';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_MAX_DATA_OCENY" IS 'Termin sporz�dzenia oceny';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_TYP_OCENY" IS 'Warto�� z listy OCE_TYP_OCENY';
   COMMENT ON COLUMN "EGADM1"."ZPT_OCENY"."OCE_CZY_WN_KOL_STOP" IS 'Czy za��czono wniosek o przyznanie kolejnego stopnia s�u�bowego';
   COMMENT ON TABLE "EGADM1"."ZPT_OCENY"  IS 'Tabela przechowuje dane o ocenach pracownik�w';
