
  CREATE TABLE "PPADM"."PPT_DEL_WARTOSCI_STAWEK" 
   (	"WST_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"WST_KR_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"WST_STD_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"WST_AUDYT_DT" DATE NOT NULL ENABLE, 
	"WST_AUDYT_UT" VARCHAR2(30 BYTE) NOT NULL ENABLE, 
	"WST_AUDYT_KT" VARCHAR2(100 BYTE) NOT NULL ENABLE, 
	"WST_AUDYT_LM" NUMBER NOT NULL ENABLE, 
	"WST_OBOWIAZUJE_OD" DATE NOT NULL ENABLE, 
	"WST_F_KWOTA" VARCHAR2(1 BYTE) NOT NULL ENABLE, 
	"WST_WAL_ID" NUMBER(10,0), 
	"WST_STD_ID_OBL" NUMBER(10,0), 
	"WST_AUDYT_DM" DATE, 
	"WST_AUDYT_UM" VARCHAR2(30 BYTE), 
	"WST_AUDYT_KM" VARCHAR2(100 BYTE), 
	"WST_KWOTA" NUMBER(21,6), 
	"WST_PROCENT" NUMBER(5,2), 
	 CONSTRAINT "PPC_WST_PK" PRIMARY KEY ("WST_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 NOLOGGING 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "EG_D"  ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "EG_D" ;

   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_ID" IS 'ID wartości stawki';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_KR_ID" IS 'ID kraju (klucz obcy do CSS_KRAJE)';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_STD_ID" IS 'ID stawki delegacji (klucz obcy do DELT_STAWKI_DELEGACJI)';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_AUDYT_UT" IS 'Użytkownik, który utworzył rekord';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_AUDYT_KT" IS 'Nazwa komputera, z którego utworzono rekord.';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_OBOWIAZUJE_OD" IS 'Data od której wartość obowiązuje';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_F_KWOTA" IS 'Flaga informująca czy wartość stawki definiowana jest jako kwota, czy jako procent kwoty określonej w innej stawce';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_WAL_ID" IS 'ID waluty stawki (klucz obcy do CSS_WALUTY)';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_STD_ID_OBL" IS 'ID bazowej stawki delegacji (klucz obcy do DELT_STAWKI_DELEGACJI)';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_AUDYT_UM" IS 'Użytkownik, który ostatnio modyfikował rekord';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_AUDYT_KM" IS 'Nazwa komputera, z którego modyfikowano rekord.';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_KWOTA" IS 'Kwota jeżeli wartość stawki jest zdefiniowana jako kwota';
   COMMENT ON COLUMN "PPADM"."PPT_DEL_WARTOSCI_STAWEK"."WST_PROCENT" IS 'Procent jeżeli wartość stawki jest zdefiniowana jako procent stawki';
