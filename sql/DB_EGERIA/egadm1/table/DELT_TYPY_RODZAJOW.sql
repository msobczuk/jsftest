--------------------------------------------------------
--  DDL for Table DELT_TYPY_RODZAJOW
--------------------------------------------------------

  CREATE TABLE "EGADM1"."DELT_TYPY_RODZAJOW" 
   (	"TRODZ_RDEL_ID" NUMBER(10,0), 
	"TRODZ_TPOZ_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "NZ_D" ;

   COMMENT ON COLUMN "EGADM1"."DELT_TYPY_RODZAJOW"."TRODZ_RDEL_ID" IS 'ID rodzaju delegacji (klucz obcy do DELT_RODZAJE_DELEGACJI)';
   COMMENT ON COLUMN "EGADM1"."DELT_TYPY_RODZAJOW"."TRODZ_TPOZ_ID" IS 'ID typu pozycji (klucz obcy do DELT_TYPY_POZYCJI)';
   COMMENT ON TABLE "EGADM1"."DELT_TYPY_RODZAJOW"  IS 'Typy rodzaj�w';
