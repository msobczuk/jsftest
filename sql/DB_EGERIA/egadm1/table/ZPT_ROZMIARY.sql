--------------------------------------------------------
--  DDL for Table ZPT_ROZMIARY
--------------------------------------------------------

  CREATE TABLE "EGADM1"."ZPT_ROZMIARY" 
   (	"RO_ID" NUMBER(10,0), 
	"RO_WY_ID" NUMBER(10,0), 
	"RO_KOD" VARCHAR2(50 BYTE), 
	"RO_AUDYT_UT" VARCHAR2(30 BYTE), 
	"RO_AUDYT_UM" VARCHAR2(30 BYTE), 
	"RO_AUDYT_DT" DATE, 
	"RO_AUDYT_DM" DATE, 
	"RO_AUDYT_KT" VARCHAR2(100 BYTE), 
	"RO_AUDYT_KM" VARCHAR2(100 BYTE), 
	"RO_AUDYT_LM" NUMBER
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "ZP_D" ;

   COMMENT ON COLUMN "EGADM1"."ZPT_ROZMIARY"."RO_ID" IS 'Identyfikator rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_ROZMIARY"."RO_WY_ID" IS 'Identyfikator wymiaru z tabeli ZPT_WYMIARY';
   COMMENT ON COLUMN "EGADM1"."ZPT_ROZMIARY"."RO_KOD" IS 'Kod rozmiaru odzie�y';
   COMMENT ON COLUMN "EGADM1"."ZPT_ROZMIARY"."RO_AUDYT_UT" IS 'U�ytkownik, kt�ry utworzy� rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_ROZMIARY"."RO_AUDYT_UM" IS 'U�ytkownik, kt�ry ostatnio modyfikowa� rekord';
   COMMENT ON COLUMN "EGADM1"."ZPT_ROZMIARY"."RO_AUDYT_DT" IS 'Data utworzenia rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_ROZMIARY"."RO_AUDYT_DM" IS 'Data modyfikacji rekordu';
   COMMENT ON COLUMN "EGADM1"."ZPT_ROZMIARY"."RO_AUDYT_KT" IS 'Nazwa komputera, z kt�rego utworzono rekord.';
   COMMENT ON COLUMN "EGADM1"."ZPT_ROZMIARY"."RO_AUDYT_KM" IS 'Nazwa komputera, z kt�rego modyfikowano rekord.';
   COMMENT ON COLUMN "EGADM1"."ZPT_ROZMIARY"."RO_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
   COMMENT ON TABLE "EGADM1"."ZPT_ROZMIARY"  IS 'Wymiary pracownika';
