--------------------------------------------------------
--  DDL for Table DELT_PRZYPISANE_ZF
--------------------------------------------------------

  CREATE TABLE "EGADM1"."DELT_PRZYPISANE_ZF" 
   (	"PZF_CDEL_ID" NUMBER(10,0), 
	"PZF_ZF_ID" NUMBER(10,0)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "NZ_D" ;

   COMMENT ON COLUMN "EGADM1"."DELT_PRZYPISANE_ZF"."PZF_CDEL_ID" IS 'ID celu (klucz obcy do DELT_CELE_DELEGACJI)';
   COMMENT ON COLUMN "EGADM1"."DELT_PRZYPISANE_ZF"."PZF_ZF_ID" IS 'ID �r�d�a finansowania (klucz obcy do PRJT_ZRODLA_FINANSOWANIA)';
   COMMENT ON TABLE "EGADM1"."DELT_PRZYPISANE_ZF"  IS 'Przypisane �r�d�a finansowania';
