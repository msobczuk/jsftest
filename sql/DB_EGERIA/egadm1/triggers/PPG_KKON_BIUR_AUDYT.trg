CREATE OR REPLACE TRIGGER PPG_KKON_BIUR_AUDYT
  BEFORE INSERT or UPDATE ON PPT_KSIAZKA_KONTAKTOWA
  FOR EACH ROW
DECLARE
  v_pom VARCHAR2 (255);
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.KKON_AUDYT_UT, :NEW.KKON_AUDYT_DT, v_pom, :NEW.KKON_AUDYT_LM, :OLD.KKON_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.KKON_AUDYT_UT, :NEW.KKON_AUDYT_DT, v_pom, :NEW.KKON_AUDYT_LM, :OLD.KKON_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.KKON_AUDYT_UM, :NEW.KKON_AUDYT_DM, :NEW.KKON_AUDYT_KM, :NEW.KKON_AUDYT_LM, :OLD.KKON_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.KKON_AUDYT_UM, :NEW.KKON_AUDYT_DM, :NEW.KKON_AUDYT_KM, :NEW.KKON_AUDYT_LM, :OLD.KKON_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;
/

