CREATE OR REPLACE TRIGGER PPG_URPL_BIUR_AUDYT
  BEFORE INSERT or UPDATE ON PPT_URLOPY_PLANOWANE
  FOR EACH ROW
DECLARE
  v_pom VARCHAR2 (255);
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.URPL_AUDYT_UT, :NEW.URPL_AUDYT_DT, v_pom, :NEW.URPL_AUDYT_LM, :OLD.URPL_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.URPL_AUDYT_UT, :NEW.URPL_AUDYT_DT, v_pom, :NEW.URPL_AUDYT_LM, :OLD.URPL_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.URPL_AUDYT_UM, :NEW.URPL_AUDYT_DM, :NEW.URPL_AUDYT_KM, :NEW.URPL_AUDYT_LM, :OLD.URPL_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.URPL_AUDYT_UM, :NEW.URPL_AUDYT_DM, :NEW.URPL_AUDYT_KM, :NEW.URPL_AUDYT_LM, :OLD.URPL_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;
/

