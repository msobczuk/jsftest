create or replace TRIGGER PPG_OCE_WIADOMOSC 
AFTER INSERT OR UPDATE ON PPT_OCE_OCENY 
  FOR EACH ROW 
DECLARE
--x varchar2(50);
BEGIN
  IF inserting THEN
       --x := null;
       --select PRC_UZYTKOWNIK into x from EGADM1.EK_PRACOWNICY where PRC_ID = :NEW.OCE_PRC_ID;
       wstaw_wiadomosc(:NEW.OCE_PRC_ID,'Zbliża się termin oceny.', :NEW.OCE_DATA_OCENY - 14);
       --select PRC_UZYTKOWNIK into x from EGADM1.EK_PRACOWNICY where PRC_ID = :NEW.OCE_PRC_ID_OCENIAJACY;
       wstaw_wiadomosc(:NEW.OCE_PRC_ID_OCENIAJACY,'Zbliża się termin oceny.', :NEW.OCE_DATA_OCENY - 14);
  ELSE
    IF (:OLD.OCE_DATA_OCENY <> :NEW.OCE_DATA_OCENY) or (:OLD.OCE_MAX_DATA_OCENY <> :NEW.OCE_MAX_DATA_OCENY) or (:OLD.OCE_PRC_ID <> :NEW.OCE_PRC_ID) or (:OLD.OCE_PRC_ID_OCENIAJACY <> :NEW.OCE_PRC_ID_OCENIAJACY) THEN
       --x := null;
       --select PRC_UZYTKOWNIK into x from EGADM1.EK_PRACOWNICY where PRC_ID = :NEW.OCE_PRC_ID;
       wstaw_wiadomosc(:NEW.OCE_PRC_ID,'Zbliża się termin oceny.', :NEW.OCE_DATA_OCENY - 14);
       --select PRC_UZYTKOWNIK into x from EGADM1.EK_PRACOWNICY where PRC_ID = :NEW.OCE_PRC_ID_OCENIAJACY;
       wstaw_wiadomosc(:NEW.OCE_PRC_ID_OCENIAJACY,'Zbliża się termin oceny.',:NEW.OCE_DATA_OCENY - 14);
    END IF;
  END IF;
END;