CREATE OR REPLACE TRIGGER PPG_IPRZ_MODYFIKACJA 
AFTER UPDATE ON PPT_IPR_IPRZ 
FOR EACH ROW 
BEGIN
  wstaw_wiadomosc(:NEW.IPRZ_PRC_ID,'Twój indywidualny program rozwoju zawodowego został zmieniony .', sysdate);
END;