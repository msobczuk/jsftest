CREATE OR REPLACE TRIGGER PPG_KLOK_BIUR_AUDYT
  BEFORE INSERT or UPDATE ON PPT_KSIAZKA_LOKALIZACJE
  FOR EACH ROW
DECLARE
  v_pom VARCHAR2 (255);
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.KLOK_AUDYT_UT, :NEW.KLOK_AUDYT_DT, v_pom, :NEW.KLOK_AUDYT_LM, :OLD.KLOK_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.KLOK_AUDYT_UT, :NEW.KLOK_AUDYT_DT, v_pom, :NEW.KLOK_AUDYT_LM, :OLD.KLOK_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.KLOK_AUDYT_UM, :NEW.KLOK_AUDYT_DM, :NEW.KLOK_AUDYT_KM, :NEW.KLOK_AUDYT_LM, :OLD.KLOK_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.KLOK_AUDYT_UM, :NEW.KLOK_AUDYT_DM, :NEW.KLOK_AUDYT_KM, :NEW.KLOK_AUDYT_LM, :OLD.KLOK_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;
/

