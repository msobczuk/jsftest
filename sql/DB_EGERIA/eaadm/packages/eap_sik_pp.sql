CREATE OR REPLACE 
PACKAGE EAP_SIK_PP AS 

  c_pakiet    CONSTANT VARCHAR2 (30) := 'EAP_SIK_PP';
  
  --
  -- Procedura sprawdza czy nie ma w systemie nierozliczonych umów lojalnościowych
  --
  PROCEDURE nierozl_umowy_lojal;

END EAP_SIK_PP;