create or replace PACKAGE BODY Eap_Sik_Pp
AS

  v_komunikat             VARCHAR2 (4000);


PROCEDURE nierozl_umowy_lojal
  IS
    c_procedura    CONSTANT VARCHAR2 (61) := c_pakiet || '.NIEROZL_UMOWY_LOJAL';
    CURSOR cu_prc
    IS
     select distinct prc_numer,
                     prc_imie,
                     prc_nazwisko,
                     zat_data_zwolnienia,
                     umsz_data_obowiazywania
            from ek_pracownicy,
                 ek_zatrudnienie,
                 PPADM.ppt_szk_umowy
     where zat_typ_umowy = 0 
        and zat_data_zwolnienia is not null
        and zat_prc_id = prc_id
        and umsz_czy_uregulowano = 'N'
        and UMSZ_DATA_OBOWIAZYWANIA > zat_data_zwolnienia
        and UMSZ_PRC_ID = prc_id
        and UMSZ_PRC_ID = zat_prc_id;
  --
  BEGIN
    FOR r_p IN cu_prc
    LOOP
      v_komunikat  :=
           'Umowa lojalnościowa'
        || '. Pracownik o numerze '
        || TO_CHAR (r_p.prc_numer)
        || ' ('
        || r_p.prc_imie
        || ' '
        || r_p.prc_nazwisko
        || ')'
        || ' zwolniony w dniu '
        || TO_CHAR (r_p.zat_data_zwolnienia, 'DD-MM-YYYY')
        || ' ma nierozliczoną umowę lojalnościową obowiązującą do dnia '
        || TO_CHAR (r_p.umsz_data_obowiazywania, 'DD-MM-YYYY')
        || '.'
        ;
      --
      Eap_Obj_Zadanie.wyslij_komunikat ('Nierozliczone umowy lojalnościowe',
                                        nvl( eap_globals.odczytaj_firme,1),
                                        v_komunikat
                                       );
    END LOOP;
  --
  EXCEPTION
    WHEN OTHERS
    THEN
      --
      -- Zgłoszenie błędu
      --
      Eap_Blad.zglos (p_procedura => c_procedura);
  END;
END Eap_Sik_Pp;