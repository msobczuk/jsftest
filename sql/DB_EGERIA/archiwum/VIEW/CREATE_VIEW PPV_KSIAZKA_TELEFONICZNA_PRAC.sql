--------------------------------------------------------
--  File created - �roda-listopada-02-2016   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for View PPV_KSIAZKA_TELEFONICZNA_PRAC
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "EGADM1"."PPV_KSIAZKA_TELEFONICZNA_PRAC" ("KTP_PKP_POKOJ_ID", "KTP_PKP_PIETRO_ID", "KTP_PKP_BUDYNEK_ID", "KTP_PKP_MIASTO_ID", "KTP_PKP_KRAJ_ID", "KTP_PRC_ID", "KTP_PRC_IMIE", "KTP_PRC_NAZWISKO", "KTP_TL_NUMER_EMAIL", "KTP_TL_NUMER_TELEFON", "KTP_TL_NUMER_FAX", "KTP_OB_JO_ID", "KTP_OB_JEDNOSTKA_ORGANIZACYJNA", "KTP_PKP_STANOWISKO", "KTP_PRC_CZY_AKTYWNY", "KTP_PKP_KRAJ", "KTP_PKP_MIASTO", "KTP_PKP_BUDYNEK", "KTP_PKP_PIETRO", "KTP_PKP_POKOJ", "KTP_PKP_UWAGI") AS 
  select
(select PLO_ID from PP_KSIAZKA_LOKALIZACJE where PLO_KOD='POKOJ' and KTP_PKP_POKOJ like '%' || PLO_WARTOSC || '%' and plo_plo_id = KTP_PKP_PIETRO_ID ) KTP_PKP_POKOJ_ID
,V3."KTP_PKP_PIETRO_ID",V3."KTP_PKP_BUDYNEK_ID",V3."KTP_PKP_MIASTO_ID",V3."KTP_PKP_KRAJ_ID",V3."KTP_PRC_ID",V3."KTP_PRC_IMIE",V3."KTP_PRC_NAZWISKO",V3."KTP_TL_NUMER_EMAIL",V3."KTP_TL_NUMER_TELEFON",V3."KTP_TL_NUMER_FAX",V3."KTP_OB_JO_ID",V3."KTP_OB_JEDNOSTKA_ORGANIZACYJNA",V3."KTP_PKP_STANOWISKO",V3."KTP_PRC_CZY_AKTYWNY",V3."KTP_PKP_KRAJ",V3."KTP_PKP_MIASTO",V3."KTP_PKP_BUDYNEK",V3."KTP_PKP_PIETRO",V3."KTP_PKP_POKOJ",V3."KTP_PKP_UWAGI"
from
(
select 
(select PLO_ID from PP_KSIAZKA_LOKALIZACJE where PLO_KOD='PIETRO' and KTP_PKP_PIETRO like '%' || PLO_WARTOSC || '%' and plo_plo_id = KTP_PKP_BUDYNEK_ID ) KTP_PKP_PIETRO_ID
,V2.*
from
(
select

    --(select PLO_ID from PP_KSIAZKA_LOKALIZACJE where plo_kod='BUDYNEK' and KTP_PKP_BUDYNEK = PLO_WARTOSC and plo_plo_id = KTP_PKP_MIASTO_ID ) KTP_PKP_BUDYNEK_ID 
    (select PLO_ID from PP_KSIAZKA_LOKALIZACJE where plo_kod='BUDYNEK' and KTP_PKP_BUDYNEK like '%' || PLO_WARTOSC || '%' and plo_plo_id = KTP_PKP_MIASTO_ID ) KTP_PKP_BUDYNEK_ID
     -- !!!!TODO: -- dane w tabeli PP_KSAZKA_LOKALIZACJE s� niesp�jne z danymi z kolumny KTP_PKP_BUDYNEK!!!! DLACZEGO?
    , V1.* 
from  
(
    select
        (select PLO_ID from PP_KSIAZKA_LOKALIZACJE where plo_kod='MIASTO' and plo_wartosc = KTP_PKP_MIASTO and plo_plo_id = KTP_PKP_KRAJ_ID ) KTP_PKP_MIASTO_ID
        ,V0.*
    from  
    (
        select distinct
           (select PLO_ID from PP_KSIAZKA_LOKALIZACJE where plo_wartosc = pkp_kraj) as KTP_PKP_KRAJ_ID,
        
        
           prc_id KTP_PRC_ID ,
          INITCAP(prc_imie) KTP_prc_imie,
          INITCAP(prc_nazwisko) KTP_prc_nazwisko,
          (SELECT tl_numer from ckk_telefony where prc_id = tl_prc_id and TL_F_DOMYSLNY = 'T' and TL_TYP = 'E') KTP_tl_numer_email,
          (SELECT tl_numer from ckk_telefony where prc_id = tl_prc_id and TL_F_DOMYSLNY = 'T' and TL_TYP = 'T') KTP_tl_numer_telefon,
          (SELECT tl_numer from ckk_telefony where prc_id = tl_prc_id and TL_F_DOMYSLNY = 'T' and TL_TYP = 'F') KTP_tl_numer_fax,
          zat_ob_id KTP_ob_jo_id,
          concat(zat_ob_id, concat(' - ', ob_nazwa)) KTP_ob_jednostka_organizacyjna,
          PKP_STANOWISKO KTP_PKP_stanowisko,
          prc_aktywny KTP_prc_czy_aktywny,
          PKP_KRAJ KTP_PKP_KRAJ,
          

          
          PKP_MIASTO KTP_PKP_MIASTO,
          PKP_BUDYNEK KTP_PKP_BUDYNEK,
          PKP_PIETRO KTP_PKP_PIETRO,
          PKP_POKOJ KTP_PKP_POKOJ,
          PKP_UWAGI KTP_PKP_UWAGI
          
        from ek_pracownicy,
              ek_zatrudnienie,
               css_obiekty_w_przedsieb,
               css_stanowiska_kosztow,
              pp_ksiazka_kontaktowa
               
        WHERE zat_prc_id=prc_id
           AND zat_ob_id=ob_id
           AND zat_sk_id=sk_id
           AND prc_id = PKP_PRC_ID (+)
             and exists (select 1
             FROM EK_ZATRUDNIENIE WHERE
                     zat_prc_id=prc_id
                     AND zat_typ_umowy = 0
                     AND zat_prc_id = prc_id
                                  AND zat_data_zmiany <= LAST_DAY (sysdate)
                                  AND NVL (zat_data_do, sysdate) >= sysdate)
	

       ) V0
    )V1
  )V2
)V3;
