

--TODO: Wgrac prawdziwy widok

CREATE OR REPLACE VIEW EGADM1.PPV_KSIAZKA_TELEFONICZNA_PRAC as

select 

      EK_PRACOWNICY.PRC_ID          KTP_PRC_ID
    , EK_PRACOWNICY.PRC_IMIE        KTP_PRC_IMIE
    , EK_PRACOWNICY.PRC_NAZWISKO    KTP_PRC_NAZWISKO
    , EK_ADRESY.ADR_MAIL            KTP_TL_NUMER_EMAIL
    , null KTP_TL_NUMER_TELEFON
    , null KTP_TL_NUMER_FAX
    , TO_CHAR( DZL.DZLLIST )        KTP_OB_JEDNOSTKA_ORGANIZACYJNA
    , null KTP_PKP_STANOWISKO
    , null KTP_PKP_KRAJ
    , null KTP_PKP_KRAJ_ID
    , null KTP_PKP_MIASTO
    , null KTP_PKP_MIASTO_ID
    , null KTP_PKP_BUDYNEK
    , null KTP_PKP_BUDYNEK_ID
    , null KTP_PKP_PIETRO
    , null KTP_PKP_PIETRO_ID
    , null KTP_PKP_POKOJ
    , null KTP_PKP_POKOJ_ID
    
from  EGADM1.EK_PRACOWNICY
     , EGADM1.EK_ADRESY
     , (    select Z.ZAT_PRC_ID,  wm_concat(distinct Z.ZAT_OB_ID) DZLLIST
            from EGADM1.EK_ZATRUDNIENIE Z
            group by Z.ZAT_PRC_ID
        ) DZL
where EK_PRACOWNICY.PRC_ID = EK_ADRESY.ADR_PRC_ID(+)
    and 'S' = EK_ADRESY.ADR_TYP(+)
    and EK_PRACOWNICY.PRC_ID = DZL.ZAT_PRC_ID
;



