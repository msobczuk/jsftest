CREATE OR REPLACE TRIGGER EGADM1.DELG_WND_BIUR_NUMER
      BEFORE UPDATE OR INSERT ON EGADM1.DELT_WNIOSKI_DELEGACJI
      FOR EACH ROW  
DECLARE
   PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN

    if (:NEW.WND_NUMER is null OR :NEW.WND_NUMER = 'Z' OR :NEW.WND_NUMER = 'K' ) then
        select 
             nvl(:NEW.WND_NUMER,'K') || '/' || TO_CHAR( :NEW.WND_DATA_POWROTU , 'YYYY') 
                || '/'||substr('0000' ||(to_number(substr('0000'||max(WND.WND_NUMER),-4))+1),-4)
            into :NEW.WND_NUMER 
        from  EGADM1.DELT_WNIOSKI_DELEGACJI WND 
        where WND.WND_NUMER like nvl(:NEW.WND_NUMER,'K')||'/'||TO_CHAR( :NEW.WND_DATA_POWROTU , 'YYYY')||'/%';
        
    end if;
END;
/
