create or replace TRIGGER   EGADM1.ZAKTUALIZUJ_PORTAL
AFTER INSERT OR UPDATE OR DELETE ON EGADM1.ek_urlopy_planowane
FOR EACH ROW
DECLARE
ctx NUMBER;
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
   SELECT COUNT (*) INTO CTX FROM urlopy_context WHERE ctx = 0 ;
   if ctx > 0 then
 update urlopy_context set ctx = 1;
 commit;
  IF INSERTING  THEN
    INSERT INTO EGADM1.PPT_URLOPY_PLANOWANE (URPL_ID,URPL_PRC_ID, URPL_DATA_OD, URPL_DATA_DO) VALUES (EK_SEQ_URP.NEXTVAL,:NEW.URP_PRC_ID, :NEW.URP_DATA_OD, :NEW.URP_DATA_DO );
   END IF;
     IF UPDATING  THEN
        UPDATE EGADM1.PPT_URLOPY_PLANOWANE SET (URPL_DATA_OD, URPL_DATA_DO) = (SELECT :NEW.URP_DATA_OD, :NEW.URP_DATA_DO FROM DUAL) WHERE URPL_PRC_ID = :NEW.URP_PRC_ID 
        AND URPL_DATA_OD = :NEW.URP_DATA_OD AND URPL_DATA_DO = :NEW.URP_DATA_DO;
      END IF;
   IF DELETING THEN
        DELETE  FROM EGADM1.PPT_URLOPY_PLANOWANE WHERE URPL_PRC_ID = :OLD.URP_PRC_ID 
        AND URPL_DATA_OD = :OLD.URP_DATA_OD AND URPL_DATA_DO = :OLD.URP_DATA_DO;
      END IF;
         update urlopy_context set ctx = 0;
         commit;
      end if;
END;

