ALTER TABLE EGADM1.PP_JPA_KEYGEN
 DROP PRIMARY KEY CASCADE;

DROP TABLE EGADM1.PP_JPA_KEYGEN CASCADE CONSTRAINTS;


create table EGADM1.PP_JPA_KEYGEN
(
  PP_TABLE   VARCHAR2(255) not null,    -- nazwa tabeli dla ktorej generujemy klucze via JPA/HIBERNATE
  PP_IDVALUE NUMBER,                    -- aktualna wartosc dla kol. id w/w tabeli
  constraint PP_JPA_KEYGEN_PK  PRIMARY KEY (PP_TABLE)
);



insert into EGADM1.PP_JPA_KEYGEN
values ( 'PP_URLOPY_PLANOWANE'
        , nvl((  select max( T.PP_URLPLAN_ID ) from  PP_URLOPY_PLANOWANE T  ), 1 )
		);

		
insert into EGADM1.PP_JPA_KEYGEN
values ( 'PP_URLOPY_HARMONOGRAMY_ROCZNE'
        , nvl((  select max( T.PP_URLHR_ID ) from  PP_URLOPY_HARMONOGRAMY_ROCZNE T  ), 1 )
		);		
		
COMMIT;


