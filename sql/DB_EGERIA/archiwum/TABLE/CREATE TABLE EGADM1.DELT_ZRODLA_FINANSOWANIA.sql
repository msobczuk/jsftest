--select * from  sys.all_tab_columns where   column_name like 'PKZF%';

DROP TABLE EGADM1.DELT_ZRODLA_FINANSOWANIA CASCADE CONSTRAINTS;

------------------------------------------------------------------------------------------------------------------

CREATE TABLE EGADM1.DELT_ZRODLA_FINANSOWANIA
(
  PKZF_ID       NUMBER(10)                      NOT NULL,
  PKZF_PKL_ID   NUMBER(10)                      NOT NULL,
  PKZF_SK_ID    NUMBER(10)                      ,
  PKZF_KWOTA    NUMBER(5,2)                     ,
  PKZF_PROCENT  NUMBER(5,2)                     ,
  PKZF_OPIS     VARCHAR2(4000 BYTE)
);

------------------------------------------------------------------------------------------------------------------

ALTER TABLE EGADM1.DELT_ZRODLA_FINANSOWANIA 
ADD ( CONSTRAINT DELC_PKZF_PK PRIMARY KEY (PKZF_ID) );
 
-----------------------------------------------------------------------------------------------------------------

declare 

    v_owner varchar2(50) := 'EGADM1';
    v_table varchar2(50) := 'DELT_ZRODLA_FINANSOWANIA';
    v_prefix varchar(5)  := 'PKZF';
      
      PROCEDURE tworz_kolumny_audytowe1(p_owner IN VARCHAR2, p_tabela IN VARCHAR2, p_prefix OUT VARCHAR2) IS
      v_prefix  VARCHAR2(30);
      v_alter   VARCHAR2(200);
      v_update  VARCHAR2(240);
      v_alter_modify   VARCHAR2(240);
      v_comment VARCHAR2(200);
      
      BEGIN
      
         IF (p_owner is null) THEN 

          SELECT SUBSTR (column_name, 1, INSTR (column_name, '_')) prefix
            INTO v_prefix
            FROM dba_tab_columns 
           WHERE table_name = UPPER (p_tabela)               AND ROWNUM = 1;
           
            v_alter   :=  'ALTER TABLE '       ||p_tabela||' ADD ' || v_prefix;
            v_update  :=  'UPDATE '            ||p_tabela||' SET ' || v_prefix;
            v_alter_modify   :=  'ALTER TABLE '||p_tabela||' MODIFY ' || v_prefix;
            v_comment :=  'COMMENT ON COLUMN ' ||p_tabela||'.'|| v_prefix;
         
         ELSE
      
          SELECT SUBSTR (column_name, 1, INSTR (column_name, '_')) prefix
            INTO v_prefix
            FROM dba_tab_columns 
           WHERE  owner||'.'||table_name = UPPER (p_owner||'.'||p_tabela) AND ROWNUM = 1;
              
            v_alter   :=  'ALTER TABLE '       ||p_owner||'.'||p_tabela||' ADD '|| v_prefix;
            v_update  :=  'UPDATE '            ||p_owner||'.'||p_tabela||' SET '|| v_prefix;
            v_alter_modify   :=  'ALTER TABLE '||p_owner||'.'||p_tabela||' MODIFY '|| v_prefix;
            v_comment :=  'COMMENT ON COLUMN ' ||p_owner||'.'||p_tabela||'.'|| v_prefix;

         END IF;
                
        p_prefix := replace(v_prefix, '_', '');
        --dbms(p_prefix);

        dbms(v_alter||'AUDYT_UT     VARCHAR2 (30) not null');
        
--      EXECUTE IMMEDIATE v_alter           ||'AUDYT_UT VARCHAR2 (30)  not null';  
        EXECUTE IMMEDIATE v_alter           ||'AUDYT_UT VARCHAR2 (30)';
        EXECUTE IMMEDIATE v_update          ||'AUDYT_UT = ''-'' ';
        EXECUTE IMMEDIATE v_alter_modify    ||'AUDYT_UT NOT NULL';
        EXECUTE IMMEDIATE v_comment||'AUDYT_UT IS ''U�ytkownik kt�ry utworzy� rekord '' ';
                
--      EXECUTE IMMEDIATE v_alter||'AUDYT_DT     DATE     not  null';
        EXECUTE IMMEDIATE v_alter           ||'AUDYT_DT DATE';
        EXECUTE IMMEDIATE v_update          ||'AUDYT_DT = SYSDATE ';
        EXECUTE IMMEDIATE v_alter_modify    ||'AUDYT_DT NOT NULL';
        EXECUTE IMMEDIATE v_comment||'AUDYT_DT IS ''Kiedy utworzy� '' ';

        EXECUTE IMMEDIATE v_alter||'AUDYT_UM     VARCHAR2 (30)  null';
        EXECUTE IMMEDIATE v_comment||'AUDYT_UM IS ''Kto modyfikowa� rekord '' ';

        EXECUTE IMMEDIATE v_alter||'AUDYT_DM     DATE           null';
        EXECUTE IMMEDIATE v_comment||'AUDYT_DM IS ''Kiedy modyfikowa� '' ';

        EXECUTE IMMEDIATE v_alter||'AUDYT_LM     VARCHAR2 (100) null';
        EXECUTE IMMEDIATE v_comment||'AUDYT_LM IS ''Liczba modyfikacji  '' ';

        EXECUTE IMMEDIATE v_alter||'AUDYT_KM     VARCHAR2 (100) null';
        EXECUTE IMMEDIATE v_comment||'AUDYT_KM IS ''Komputer z kt�rego dokonano zmiany  '' ';

        COMMIT;
        
      END tworz_kolumny_audytowe1;


begin

  TWORZ_KOLUMNY_AUDYTOWE1 ('EGADM1', v_table, v_prefix);
  eaadm.eap_audyt.utworz_wyzwalacz_audytowy('EGADM1', v_table, v_prefix, 'WEB', true);

end;

--ALTER TABLE EGADM1.PPT_KSIAZKA_KONTAKTOWA ADD  KKON_AUDYT_UT     VARCHAR2 (30) not null

--drop trigger PPG_PKEY__BIUR_AUDYT;

--eap_audyt.utworz_wyzwalacz_audytowy
--wczesniej 

-- 1. zarejestrowac tabele w EA
-- 2. utworzyc trigger


------------------------------------------------------------------------------------------------------------------

DROP SEQUENCE EGADM1.DELS_PKZF;

CREATE SEQUENCE EGADM1.DELS_PKZF
  START WITH 1
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

DROP PUBLIC SYNONYM DELS_PKZF;

CREATE PUBLIC SYNONYM DELS_PKZF FOR EGADM1.DELS_PKZF;

GRANT ALTER, SELECT ON EGADM1.DELS_PKZF TO EG01_UZYTKOWNIK;


