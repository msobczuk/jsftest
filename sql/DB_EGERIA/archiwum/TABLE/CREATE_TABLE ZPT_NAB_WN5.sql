
  CREATE TABLE "EGADM1"."ZPT_NAB_WN5" 
   (	"NWN_ID" NUMBER(10,0) NOT NULL ENABLE, 
	"NWN_STN_ID" NUMBER(10,0), 
	"NWN_OB_ID" NUMBER(10,0), 
	"NWN_WYMIAR_ETATU" VARCHAR2(20 BYTE), 
	"NWN_RODZAJ_NABORU" VARCHAR2(20 BYTE), 
	"NWN_SYMB_OPIS_STN" VARCHAR2(20 BYTE), 
	"NWN_DEC_DYR_GEN" VARCHAR2(500 BYTE), 
	"NWN_UWAGI" VARCHAR2(500 BYTE), 
	"NWN_AUDYT_UT" VARCHAR2(30 BYTE) NOT NULL ENABLE, 
	"NWN_AUDYT_DT" DATE NOT NULL ENABLE, 
	"NWN_AUDYT_UM" VARCHAR2(30 BYTE), 
	"NWN_AUDYT_DM" DATE, 
	"NWN_AUDYT_LM" VARCHAR2(100 BYTE), 
	"NWN_AUDYT_KM" VARCHAR2(100 BYTE), 
	 CONSTRAINT "ZPT_NAB_WN5_PK" PRIMARY KEY ("NWN_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS NOLOGGING 
  TABLESPACE "EG_D"  ENABLE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  TABLESPACE "EG_D" ;

   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_ID" IS 'klucz główny tabeli';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_STN_ID" IS 'id stanowiska - klucz do ZPT_STANOWISKA';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_OB_ID" IS 'id jednostki organizacyjnej - CSS_OBIEKTY_W_PRZEDSIEB';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_WYMIAR_ETATU" IS 'wymiar etatu - wartość słownikowa z TYP ETATU';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_RODZAJ_NABORU" IS 'rodzaj naboru - wartość słownikowa z REK_RODZAJ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_SYMB_OPIS_STN" IS 'symbol opisu stanowiska - ?';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_DEC_DYR_GEN" IS 'decyzja Dyrektora Generalnego';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_UWAGI" IS 'uwagi';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_AUDYT_UT" IS 'Użytkownik który utworzył rekord ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_AUDYT_DT" IS 'Kiedy utworzył ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_AUDYT_UM" IS 'Kto modyfikował rekord ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_AUDYT_DM" IS 'Kiedy modyfikował ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_AUDYT_LM" IS 'Liczba modyfikacji  ';
   COMMENT ON COLUMN "EGADM1"."ZPT_NAB_WN5"."NWN_AUDYT_KM" IS 'Komputer z którego dokonano zmiany  ';

  CREATE OR REPLACE EDITIONABLE TRIGGER "EGADM1"."PPG_NWN_BIUR_AUDYT" 
  BEFORE INSERT or UPDATE ON EGADM1.ZPT_NAB_WN5
  FOR EACH ROW 
DECLARE
  v_pom VARCHAR2 (255);
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.NWN_AUDYT_UT, :NEW.NWN_AUDYT_DT, v_pom, :NEW.NWN_AUDYT_LM, :OLD.NWN_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.NWN_AUDYT_UT, :NEW.NWN_AUDYT_DT, v_pom, :NEW.NWN_AUDYT_LM, :OLD.NWN_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.NWN_AUDYT_UM, :NEW.NWN_AUDYT_DM, :NEW.NWN_AUDYT_KM, :NEW.NWN_AUDYT_LM, :OLD.NWN_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.NWN_AUDYT_UM, :NEW.NWN_AUDYT_DM, :NEW.NWN_AUDYT_KM, :NEW.NWN_AUDYT_LM, :OLD.NWN_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;

/
ALTER TRIGGER "EGADM1"."PPG_NWN_BIUR_AUDYT" ENABLE;
