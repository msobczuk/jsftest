ALTER TABLE delt_wnioski_delegacji
  ADD wnd_wnd_id NUMBER(10,0);
  
ALTER TABLE delt_wnioski_delegacji
  ADD WND_F_CZY_GRUPOWA VARCHAR2(1 BYTE);
  
ALTER TABLE delt_wnioski_delegacji
  ADD WND_PRYW_DATA_WYJAZDU DATE;
  
ALTER TABLE delt_wnioski_delegacji
  ADD WND_PRYW_DATA_POWROTU DATE;
  
  ALTER TABLE delt_wnioski_delegacji
  ADD WND_GR_UZASADNIENIE VARCHAR2(4000);
  
alter table EGADM1.DELT_WNIOSKI_DELEGACJI 
	add (WND_F_PRYW VARCHAR2(1 Byte))  
	
COMMENT ON COLUMN EGADM1.DELT_WNIOSKI_DELEGACJI.WND_F_PRYW IS 'Czy wyjazd prywatny / Powiązana podróż prywatna (w del. MF)'
  
  
ALTER TABLE delt_wnioski_delegacji ADD wnd_f_czy_szkol VARCHAR2(1 BYTE);
ALTER TABLE delt_wnioski_delegacji ADD wnd_trasa_wyjazdu VARCHAR2(4000);
ALTER TABLE delt_wnioski_delegacji ADD wnd_trasa_powrotu VARCHAR2(4000);
ALTER TABLE delt_wnioski_delegacji ADD wnd_uwagi VARCHAR2(4000);
ALTER TABLE DELT_WNIOSKI_DELEGACJI ADD WND_F_BEZKOSZTOWA VARCHAR2(1);
ALTER TABLE DELT_WNIOSKI_DELEGACJI ADD WND_F_PROJEKTOWA VARCHAR2(1);
ALTER TABLE DELT_WNIOSKI_DELEGACJI ADD WND_F_KONFERENCYJNA VARCHAR2(1);

ALTER TABLE DELT_WNIOSKI_DELEGACJI MODIFY WND_DATA_WNIOSKU null;


DROP INDEX delc_slok_uk;

DROP CONSTRAINT delc_slok_uk;
