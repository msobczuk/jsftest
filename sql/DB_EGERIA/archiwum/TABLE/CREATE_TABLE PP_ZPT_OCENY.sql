--ALTER TABLE EGADM1.ZPT_OCENY
-- DROP PRIMARY KEY CASCADE;

--DROP TABLE EGADM1.ZPT_OCENY CASCADE CONSTRAINTS;

CREATE TABLE EGADM1.ZPT_OCENY
(
  OCE_ID                   NUMBER(10)           NOT NULL,
  OCE_PRC_ID               NUMBER(10)           NOT NULL,
  OCE_PRC_ID_OCENIAJACY    NUMBER(10),
  OCE_OCENIAJACY           VARCHAR2(100 BYTE),
  OCE_DATA_OD              DATE,
  OCE_DATA_DO              DATE,
  OCE_DATA_OCENY           DATE,
  OCE_DATA_KOLEJNEJ_OCENY  DATE,
  OCE_OPIS                 VARCHAR2(2400 BYTE),
  OCE_OPIS2                VARCHAR2(2400 BYTE),
  OCE_AUDYT_UT         VARCHAR2(30 BYTE)    NOT NULL,
  OCE_AUDYT_DT       DATE                 NOT NULL,
  OCE_AUDYT_UM      VARCHAR2(30 BYTE),
  OCE_AUDYT_DM    DATE,
  OCE_AUDYT_KT             VARCHAR2(100 BYTE),
  OCE_AUDYT_KM             VARCHAR2(100 BYTE),
  OCE_AUDYT_LM             NUMBER,
  OCE_MAX_DATA_OCENY       DATE,
  OCE_TYP_OCENY            VARCHAR2(10 BYTE),
  OCE_SREDNIA 			   NUMBER(5,3)
)
TABLESPACE EK_D
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON TABLE EGADM1.ZPT_OCENY IS 'Tabela przechowuje dane o ocenach pracowników';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_MAX_DATA_OCENY IS 'Termin sporządzenia oceny';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_TYP_OCENY IS 'Wartość z listy OCE_TYP_OCENY';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_ID IS 'Klucz główny tabeli';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_PRC_ID IS 'Klucz obcy do tabeli EK_PRACOWNICY';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_PRC_ID_OCENIAJACY IS 'Powiązanie z tabelą EK_PRACOWNICY - id oceniającego';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_OCENIAJACY IS 'Oceniający';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_DATA_OD IS 'Data od';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_DATA_DO IS 'Data do';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_DATA_OCENY IS 'Data oceny';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_DATA_KOLEJNEJ_OCENY IS 'Data kolejnej oceny';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_OPIS IS 'Opis';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_OPIS2 IS 'Opis dodatkowy';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_AUDYT_UT IS 'Użytkownik, który utworzył rekord';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_AUDYT_DT IS 'Data utworzenia rekordu';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_AUDYT_UM IS 'Użytkownik, który ostatnio modyfikował rekord';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_AUDYT_DM IS 'Data modyfikacji rekordu';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_AUDYT_KT IS 'Nazwa komputera, z którego utworzono rekord.';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_AUDYT_KM IS 'Nazwa komputera, z którego modyfikowano rekord.';

COMMENT ON COLUMN EGADM1.ZPT_OCENY.OCE_AUDYT_LM IS 'Liczba modyfikacji rekordu';


CREATE UNIQUE INDEX EGADM1.ZPC_OCE_PK ON EGADM1.ZPT_OCENY
(OCE_ID)
NOLOGGING
TABLESPACE EK_X
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE OR REPLACE TRIGGER EGADM1.ZPG_OCE_BIUR_AUDYT
  BEFORE INSERT or UPDATE ON EGADM1.ZPT_OCENY   FOR EACH ROW
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.OCE_AUDYT_UT, :NEW.OCE_AUDYT_DT, :NEW.OCE_AUDYT_KT, :NEW.OCE_AUDYT_LM, :OLD.OCE_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.OCE_AUDYT_UT, :NEW.OCE_AUDYT_DT, :NEW.OCE_AUDYT_KT, :NEW.OCE_AUDYT_LM, :OLD.OCE_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN 
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.OCE_AUDYT_UM, :NEW.OCE_AUDYT_DM, :NEW.OCE_AUDYT_KM, :NEW.OCE_AUDYT_LM, :OLD.OCE_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.OCE_AUDYT_UM, :NEW.OCE_AUDYT_DM, :NEW.OCE_AUDYT_KM, :NEW.OCE_AUDYT_LM, :OLD.OCE_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;
END;
/


CREATE OR REPLACE TRIGGER EGADM1.ZPG_OCE_BIUD_OBSERWATOR
      BEFORE UPDATE OR INSERT OR DELETE ON EGADM1.ZPT_OCENY 
BEGIN
     IF SYS_CONTEXT('EA_GLOBALS','OBSERWATOR') = 'T' THEN
       eap_blad.zglos(p_procedura => 'EGADM1.ZPG_OCE_BIUD_OBSERWATOR',
                        p_nazwa     => 'ROLA_OBSERWATORA');
     END IF;
    END;
/


DROP PUBLIC SYNONYM ZPT_OCENY;

CREATE PUBLIC SYNONYM ZPT_OCENY FOR EGADM1.ZPT_OCENY;


ALTER TABLE EGADM1.ZPT_OCENY ADD (
  CONSTRAINT ZPC_OCE_PK
 PRIMARY KEY
 (OCE_ID)
    USING INDEX 
    TABLESPACE EK_X
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                PCTINCREASE      0
               ));

GRANT DELETE, INSERT, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE ON EGADM1.ZPT_OCENY TO EG01_UZYTKOWNIK;
