grant execute on utl_http to ppadm;
grant execute on dbms_lock to ppadm;
 
BEGIN
  DBMS_NETWORK_ACL_ADMIN.create_acl (
    acl          => 'ppadm_acl_file.xml', 
    description  => 'ACL dla PPADM',
    principal    => 'PPADM',
    is_grant     => TRUE, 
    privilege    => 'connect',
    start_date   => SYSTIMESTAMP,
    end_date     => NULL);
end;
/

begin
  DBMS_NETWORK_ACL_ADMIN.assign_acl (
    acl         => 'ppadm_acl_file.xml',
    host        => '10.132.216.41', 
    lower_port  => 8080,
    upper_port  => NULL);    
end; 
/


begin
  DBMS_NETWORK_ACL_ADMIN.assign_acl (
    acl         => 'ppadm_acl_file.xml',
    host        => '10.132.55.10', 
    lower_port  => 8080,
    upper_port  => NULL);    
end; 
/



--testowe wczytanie stronki z portalu (docelowo chodzi raczej o serwlet i np. wrzucanie/pingowanie tam komunikatów parametrem w url)
--testowe wczytanie stronki z portalu (docelowo chodzi raczej o serwlet i np. wrzucanie/pingowanie tam komunikatów parametrem w url)
declare
  v_url varchar2(100) := 'http://10.132.216.41:8080/PortalPracowniczy/alert.xhtml?id=123456'; --id =  z eat_wiadomosci...
  v_req  utl_http.req;
  v_resp utl_http.resp;
  v_text varchar2(32767);
begin
  v_req  := utl_http.begin_request(v_url);
  v_resp := utl_http.get_response(v_req);
  begin
   loop
     utl_http.read_text(v_resp, v_text, 32766);
     dbms_output.put_line(v_text);
   end loop;
  exception
   when utl_http.end_of_body then
     utl_http.end_response(v_resp);
  end;
end;
/



create or replace procedure PPADM.MSG(P_URL varchar2, P_WDM_ID NUMBER, P_SEVERITY VARCHAR2 DEFAULT 'INFO') as
PRAGMA AUTONOMOUS_TRANSACTION;
  -- P_URL: np. 'http://10.132.216.41:8080/PortalPracowniczy/'
  -- P_SEVERITY:jeden z INFO WARN ERROR
  -- P_WDM_ID jeden z    select wdm_id from eat_wiadomosci;
  --v_url varchar2(100) := 'http://10.132.216.41:8080/PortalPracowniczy/alert.xhtml?id=123456'; 
  v_req  utl_http.req;
  v_resp utl_http.resp;
  v_text varchar2(32767);
BEGIN
  utl_http.end_response(v_resp);
  v_req  := utl_http.begin_request(p_url); -- 'http://10.132.216.41:8080/PortalPracowniczy/msg.xhtml?id=123456,severity=ERROR
  v_resp := utl_http.get_response(v_req);
  begin
   loop
     utl_http.read_text(v_resp, v_text, 32766);
     dbms_output.put_line(v_text);
   end loop;
  exception
   when utl_http.end_of_body then
     utl_http.end_response(v_resp);
  end;
END;
/


begin 

 --PPADM.MSG('http://10.132.216.41:8080/PortalPracowniczy/msg.xhtml', 123456); -- moja maszyna developerska
 PPADM.MSG('http://10.132.55.10:20032/PortalPracowniczy/msg.xhtml?wdm_id=123456789', 123456); --http://vm.bc.krakow.comarch:20032/PortalPracowniczy/Delegacje 
 
end;
/

select UTL_HTTP.get_detailed_sqlerrm from dual;









