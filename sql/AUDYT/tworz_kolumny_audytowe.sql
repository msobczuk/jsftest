declare 
      PROCEDURE tworz_kolumny_audytowe1(p_tabela IN VARCHAR2) IS
      v_prefix  VARCHAR2(30);
      v_alter   VARCHAR2(200);
      v_update  VARCHAR2(240);
      v_alter_modify   VARCHAR2(240);
      v_comment VARCHAR2(200);
      
      BEGIN
          SELECT SUBSTR (column_name, 1, INSTR (column_name, '_')) prefix
            INTO v_prefix
            FROM dba_tab_columns 
           WHERE  (owner || '.' || table_name = UPPER (p_tabela) AND ROWNUM = 1)
               OR (                table_name = UPPER (p_tabela) AND ROWNUM = 1) ;
                
            v_alter   :=  'ALTER TABLE '|| p_tabela ||' ADD ' || v_prefix;
            v_update  :=  'UPDATE '     || p_tabela ||' SET ' || v_prefix;
            v_alter_modify   :=  'ALTER TABLE '|| p_tabela ||' MODIFY ' || v_prefix;
            v_comment :=  'COMMENT ON COLUMN '|| p_tabela||'.'|| v_prefix;

        dbms(v_alter||'AUDYT_UT     VARCHAR2 (30) not null');
        
--      EXECUTE IMMEDIATE v_alter           ||'AUDYT_UT VARCHAR2 (30)  not null';  
        EXECUTE IMMEDIATE v_alter           ||'AUDYT_UT VARCHAR2 (30)';
        EXECUTE IMMEDIATE v_update          ||'AUDYT_UT = ''-'' ';
        EXECUTE IMMEDIATE v_alter_modify    ||'AUDYT_UT NOT NULL';
        EXECUTE IMMEDIATE v_comment||'AUDYT_UT IS ''U�ytkownik kt�ry utworzy� rekord '' ';
                
--      EXECUTE IMMEDIATE v_alter||'AUDYT_DT     DATE     not  null';
        EXECUTE IMMEDIATE v_alter           ||'AUDYT_DT DATE';
        EXECUTE IMMEDIATE v_update          ||'AUDYT_DT = SYSDATE ';
        EXECUTE IMMEDIATE v_alter_modify    ||'AUDYT_DT NOT NULL';
        EXECUTE IMMEDIATE v_comment||'AUDYT_DT IS ''Kiedy utworzy� '' ';

        EXECUTE IMMEDIATE v_alter||'AUDYT_UM     VARCHAR2 (30)  null';
        EXECUTE IMMEDIATE v_comment||'AUDYT_UM IS ''Kto modyfikowa� rekord '' ';

        EXECUTE IMMEDIATE v_alter||'AUDYT_DM     DATE           null';
        EXECUTE IMMEDIATE v_comment||'AUDYT_DM IS ''Kiedy modyfikowa� '' ';

        EXECUTE IMMEDIATE v_alter||'AUDYT_LM     VARCHAR2 (100) null';
        EXECUTE IMMEDIATE v_comment||'AUDYT_LM IS ''Liczba modyfikacji  '' ';

        EXECUTE IMMEDIATE v_alter||'AUDYT_KM     VARCHAR2 (100) null';
        EXECUTE IMMEDIATE v_comment||'AUDYT_KM IS ''Komputer z kt�rego dokonano zmiany  '' ';

        COMMIT;
        
      END tworz_kolumny_audytowe1;

begin

TWORZ_KOLUMNY_AUDYTOWE1 ('EGADM1.PPT_KSIAZKA_KONTAKTOWA');

--eaadm.eap_audyt.utworz_wyzwalacz_audytowy('EGADM1', 'PPT_KSIAZKA_KONTAKTOWA', 'KKON', 'PP');
eaadm.eap_audyt.utworz_wyzwalacz_audytowy('EGADM1', 'PPT_KSIAZKA_KONTAKTOWA', 'KKON', 'PP', true);

end;

--ALTER TABLE EGADM1.PPT_KSIAZKA_KONTAKTOWA ADD  KKON_AUDYT_UT     VARCHAR2 (30) not null