--ilosc dni z nieobecnosci UW w obrebie roku 2017, ale bez nieobecnosci na przelomie roku. kal....

select sum(AB.AB_DNI_WYKORZYSTANE) 
from EGADM1.EK_ABSENCJE ab
where  AB.AB_RDA_ID = 5
    AND AB.AB_PRC_ID = 126677
    and EXTRACT(YEAR FROM AB.AB_DATA_OD) = 2017
    and EXTRACT(YEAR FROM AB.AB_DATA_DO) = 2017
;




-- obliczanie wykorzystanego zasu nieobecnosci wg dat - np. od pocztku roku do wybranego dnia (lub od wybranego dnia do konca roku)
declare 

 v_ret number :=1; 
 p_prc_id NUMBER := 126677; 
 p_rda_id NUMBER := 5;-- 5 = Urlop Wypoczynkowy;
 p_data_od  DATE := TO_DATE('2017-01-03', 'yyyy/MM/dd');
 p_data_do  DATE := TO_DATE('2017-01-04', 'yyyy/MM/dd');
 po_godziny  NUMBER;
 po_dni     NUMBER;
 po_blad    VARCHAR2(4000 byte); 

begin 
 v_ret := EKP_API_INTRANET.ZWROC_DNI_GODZINY_ABSENCJI(p_prc_id, p_rda_id, p_data_od, p_data_do, po_dni, po_godziny, po_blad);
 DBMS_OUTPUT.PUT_LINE('v_ret: ' || v_ret || '; po_blad: ' || po_blad || '; po_godziny: ' || po_godziny || '; po_dni:' || po_dni);
end;




--TODO - doliczyc? 
   and EXTRACT(YEAR FROM AB.AB_DATA_OD) = 2016
   and EXTRACT(YEAR FROM AB.AB_DATA_DO) = 2017


--TODO - doliczyc

   and EXTRACT(YEAR FROM AB.AB_DATA_OD) = 2017
   and EXTRACT(YEAR FROM AB.AB_DATA_DO) = 2018

;