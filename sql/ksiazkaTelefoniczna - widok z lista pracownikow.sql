
  CREATE OR REPLACE FORCE VIEW "EGADM1"."PPV_KSIAZKA_TELEFONICZNA_PRAC" ("KTP_PRC_ID", "KTP_PRC_IMIE", "KTP_PRC_NAZWISKO", "KTP_TL_NUMER_EMAIL", "KTP_TL_NUMER_TELEFON", "KTP_TL_NUMER_FAX", "KTP_OB_JEDNOSTKA_ORGANIZACYJNA", "KTP_PKP_STANOWISKO", "KTP_PRC_CZY_AKTYWNY", "KTP_PKP_KRAJ", "KTP_PKP_MIASTO", "KTP_PKP_BUDYNEK", "KTP_PKP_PIETRO", "KTP_PKP_POKOJ") AS 
  select
  distinct prc_id KTP_PRC_ID ,
  INITCAP(prc_imie) KTP_prc_imie,
  INITCAP(prc_nazwisko) KTP_prc_nazwisko,
  (SELECT tl_numer from ckk_telefony where prc_id = tl_prc_id and TL_F_DOMYSLNY = 'T' and TL_TYP = 'E') KTP_tl_numer_email,
  (SELECT tl_numer from ckk_telefony where prc_id = tl_prc_id and TL_F_DOMYSLNY = 'T' and TL_TYP = 'T') KTP_tl_numer_telefon,
  (SELECT tl_numer from ckk_telefony where prc_id = tl_prc_id and TL_F_DOMYSLNY = 'T' and TL_TYP = 'F') KTP_tl_numer_fax,
  concat(zat_ob_id, concat(' - ', ob_nazwa)) KTP_ob_jednostka_organizacyjna,
  PKP_STANOWISKO KTP_PKP_stanowisko,
  prc_aktywny KTP_prc_czy_aktywny,
  PKP_KRAJ KTP_PKP_KRAJ,
  PKP_MIASTO KTP_PKP_MIASTO,
  PKP_BUDYNEK KTP_PKP_BUDYNEK,
  PKP_PIETRO KTP_PKP_PIETRO,
  PKP_POKOJ KTP_PKP_POKOJ
from ek_pracownicy,
      ek_zatrudnienie,
       css_obiekty_w_przedsieb,
       css_stanowiska_kosztow,
       pp_ksiazka_kontaktowa
WHERE zat_prc_id=prc_id
   AND zat_ob_id=ob_id
   AND zat_sk_id=sk_id
   AND prc_id = PKP_PRC_ID (+)
     and exists (select 1
     FROM EK_ZATRUDNIENIE WHERE
             zat_prc_id=prc_id
             AND zat_typ_umowy = 0
             AND zat_prc_id = prc_id
                          AND zat_data_zmiany <= LAST_DAY (sysdate)
                          AND NVL (zat_data_do, sysdate) >= sysdate);
 
