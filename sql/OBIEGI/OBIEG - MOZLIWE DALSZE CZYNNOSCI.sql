-- cssp_obiegi.ROZPOCZNIJ_OBIEG
-- obieg - ustaw kontekst dla wybranej oceny okresowej i wczytaj mozliwe wg obiegu dalsze czynnosci (dla oceny): 

select SYS_CONTEXT ('EA_GLOBALS', 'FRM_ID') from  dual;






-----------------------------------------------------------------------------------------------------------------

-- ZAININCJOWAC SESJE WEB ...
declare 
    v_komunikat varchar2(500);
    v_ret varchar2(100);
begin
    v_ret := eap_web.inicjuj( 'ADMINISTRATOR_PORTAL','CC3542648A3FE5A9A8B50A7FE844D69F', v_komunikat, 'PP');
end;
/
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
--USTAW KONTEKST 
declare 
    v_komunikat varchar2(500);
    v_ret varchar2(100);
begin
    v_ret := eap_web.ustaw_kontekst( 'CC3542648A3FE5A9A8B50A7FE844D69F', v_komunikat);
end;
/


--USUN/WYLOGUJ SESJE Z BAZY 
begin
  eap_web.czysc_kontekst( 'CC3542648A3FE5A9A8B50A7FE844D69F');
end;
/

-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------
-- W NIEZALEZNYM BLOKU USTAW KTG I ID ENCJI DLA OBIEGU
declare 
    v_komunikat varchar2(500);
    v_ret varchar2(100);
begin
    cssp_obiegi_utl.ustaw_kategorie('WN_OPIS_STAN');
    cssp_obiegi_utl.ustaw_id_encji(2); --143 to identyfikator oceny
end;
/
-----------------------------------------------------------------------------------------------------------------
--UWAGA w SQLDEVELOPER lub TOAD: 
--USTAW FIRME (W RAZIE PROBLEMÓW JESLI W RAMACH SESJI NA SQLDEVELOPER MASZ NP. DWA OTWARTE POLACZENIA)
-- ALBO ZMIEN INDENTYFIKATOR SESJI DLA KAZDEEGO POLACZENIA NA INNY
begin
    eap_globals.ustaw_firme(1);--?????!!!!!!
end;
/
-----------------------------------------------------------------------------------------------------------------








select cdw_id, cdw_nazwa from cssv_czynnosci_do_wykonania; --czynnosci jakie mozna wykonac


--czynnosci jakie mozna wykonac + info czy wskazany user 'test' moze je wykonywac
select CSSP_OBIEGI_UTL.MA_UPRAWNIENIE_COB(cdw_id, 'TEST') MA_UPRAWNIENIE_COB, C.* from cssv_czynnosci_do_wykonania C;
--  select * from  csst_uprawnienia_obiegow;



--select eaadm.eap_globals.odczytaj_uzytkownika from  dual;
--begin
--    eaadm.eap_globals.ustaw_uzytkownika('test');
--end;
--select eaadm.eap_globals.odczytaj_uzytkownika from  dual;


-- WYKONANIE CZYNNOSCI:
begin
    --eaadm.eap_globals.ustaw_uzytkownika('TEST');
    cssp_obiegi_utl.ustaw_kategorie('WN_OCENA_OKRES');
    cssp_obiegi_utl.ustaw_id_encji(143); --143 to identyfikator oceny
    CSSP_OBIEGI.WYKONAJ_CZYNNOSC(100200, 'opis...');
    --CSSP_OBIEGI.WYCOFAJ_CZYNNOSC(100200,'opis...');
end;


-- WYCOFANIE CZYNNOSCI:
begin
    --eaadm.eap_globals.ustaw_uzytkownika('TEST');
    cssp_obiegi_utl.ustaw_kategorie('WN_OCENA_OKRES');
    cssp_obiegi_utl.ustaw_id_encji(143); --143 to identyfikator oceny
    --CSSP_OBIEGI.WYKONAJ_CZYNNOSC(100200, 'opis...');
    CSSP_OBIEGI.WYCOFAJ_CZYNNOSC(100200,'opis...');
end;



begin 

CSSP_OBIEGI.ROZPOCZNIJ_OBIEG(100080, 143  ); --100100 , 41
end;

select * from  CSST_HISTORIA_OBIEGOW;




-- odblokowanie/zatwierdzenie obiegu (jeśli w egerii się nie da... bo jest cykl)
UPDATE csst_obiegi 
set obg_stan_definicji = 'Z'
WHERE obg_tenc_id = 100080 --p_tenc_id
 AND obg_stan_definicji = 'N';
 
 
 
 
--Egeria CSS->Admin->Obiegi->Definicje Obiegów->zakładka: Definicje akcji
--przykladowa akcja (np. Zwolnienie pracownika ze Służby Przygotowawczej)
DECLARE
v_id VARCHAR2(100):=:1;
v_par1 VARCHAR2(100):=:2;
v_par3 VARCHAR2(100):=:3;
v_par4 VARCHAR2(100):=:4;
v_par5 VARCHAR2(100):=:5;
BEGIN

update EGADM1.ZPT_SLUZBA_PRZYG SLPR 
set SLPR.SLPR_DEC_ZWOL ='T' 
WHERE SLPR.SLPR_ID = v_id
   and (' '|| v_par1 || v_par3 || v_par4  || v_par5 >= ' ') ;

END;






-- USUN DANE OBIEGU (parametry kategoria i id encji ):
begin
 cssp_obiegi_utl.USUN_DANE ('WN_SLPR', 42); 
 commit;
end;