--obieg WN_DELKv2 POZIOM10->POZIOM20 obieg WN_DELZv2 POZIOM27->POZIOM30 POZIOM27->POZIOM40
DECLARE
   c_procedura       VARCHAR2 (200)  := 'Akcja na zdarzeniu: [MF] DEL WST - uzupełnij realizacje dla dok.';
   v_id              NUMBER          := :ID;
   v_typ             VARCHAR2 (100)  := :p_typ;
   v_cob_id          NUMBER          := :p_cob_id;
   v_akc_id          NUMBER          := :p_akc_id;
   v_opis            VARCHAR2 (4000) := :p_opis;
   v_kob_kod         VARCHAR2 (30)   := cssp_obiegi_utl.odczytaj_kategorie;
BEGIN

	IF v_typ = 'W' THEN
		-- usuwam realizacje dla generacji
		pbp_realizacje_wdr.wycofaj_wnd(	p_id => v_id, 
										p_kal_id => null, 
										p_wszystkie => FALSE,
										p_gen_lp => 1 );
		-- wstawiam realizacje dla generacji								
		pbp_realizacje_wdr.wstaw_wnd  ( p_id => v_id, 
										p_kal_id => null, 
										p_gen_lp => 1 );

	ELSIF  v_typ = 'C' THEN  
		-- usuwam realizacje dla ostatniej generacji
		pbp_realizacje_wdr.wycofaj_wnd(	p_id => v_id, 
										p_kal_id => null, 
										p_wszystkie => FALSE);

	END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      eap_blad.zglos (p_procedura => c_procedura ||' p_cob_id=' || v_cob_id);
END;