declare
  c_procedura  VARCHAR2 (200) 	:= 'Akcja na zdarzeniu: [MF] DEL ROZ - weryfikacja wypełnienia części III - kalkulacji rozliczającej';
  v_id       NUMBER := :ID;
  v_typ      VARCHAR2 (1) := :TYP;
  v_cob_id   NUMBER := :COB_ID;
  v_akc_id   NUMBER := :AKC_ID;
  v_opis     VARCHAR2 (4000) := :OPIS;
  
  v_id_kalkulacji NUMBER;
  v_trasy_count NUMBER;
  v_f_bezkosztowa VARCHAR2(1);
  v_pozycje_kal_count NUMBER;

begin
  v_trasy_count := 0;
  v_pozycje_kal_count := 0;
  
  select kal_id
    into v_id_kalkulacji
    from PPADM.PPT_DEL_KALKULACJE
   where kal_wnd_id = v_id
     and kal_rodzaj = 'ROZ';
  
  IF v_id_kalkulacji IS NOT NULL
  THEN
    select count(trs_id)
      into v_trasy_count
      from PPADM.PPT_DEL_TRASY
     where trs_kal_id = v_id_kalkulacji;
    
    select wnd_f_bezkosztowa
      into v_f_bezkosztowa
      from PPADM.PPT_DEL_WNIOSKI_DELEGACJI
     where wnd_id = v_id;
    
    IF v_f_bezkosztowa = 'T'
    THEN
      v_pozycje_kal_count := 1;
    ELSE
      select count(pkl_id)
        into v_pozycje_kal_count
        from PPADM.PPT_DEL_POZYCJE_KALKULACJI
       where pkl_kal_id = v_id_kalkulacji;
    END IF;
    
    IF v_trasy_count = 0 OR v_pozycje_kal_count = 0
    THEN
      ppadm.msg
               (eguser,
                'KONTROLA KALKULACJI ROZLICZAJĄCEJ',
                '<font color="red"><strong>'
                || 'UWAGA'
                || '</strong></font>'
                || CHR (10)
                || 'Delegacja numer '
                || v_id
                || '. Część III - kalkulacja rozliczająca nie została wypełniona. Operacja została przerwana.',
                NULL,
                'ERROR'
               );
      ROLLBACK;
    END IF;
  ELSE
    ppadm.msg
            (eguser,
             'KONTROLA KALKULACJI ROZLICZAJĄCEJ',
             '<font color="red"><strong>'
             || 'UWAGA'
             || '</strong></font>'
             || CHR (10)
             || 'Nie wstawiono rozliczenia dla delegacji numer '
             || v_id
             || '. Operacja została przerwana.'
             || SQLERRM,
             NULL,
             'ERROR'
            );

    ROLLBACK;
  END IF;
  
  EXCEPTION
    WHEN OTHERS
    THEN
      eap_blad.zglos (p_procedura => c_procedura|| ' p_cob_id='|| v_cob_id);
end;