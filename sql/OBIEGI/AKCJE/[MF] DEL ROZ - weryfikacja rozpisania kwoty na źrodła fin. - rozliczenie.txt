--obieg WN_DELKv2 POZIOM50->POZIOM60 obieg WN_DELZv2 POZIOM80->POZIOM90
DECLARE
   c_procedura           VARCHAR2 (200) 	:= 'Akcja na zdarzeniu: [MF] DEL ROZ - weryfikacja rozpisania kwoty na źrodła fin. - rozliczenie';
   v_id                  NUMBER             := :ID;
   v_typ                 VARCHAR2 (100)     := :P_TYP;
   v_cob_id              NUMBER             := :P_COB_ID;
   v_akc_id              NUMBER             := :P_AKC_ID;
   v_opis                VARCHAR2 (4000)    := :P_OPIS;
   v_kob_kod             VARCHAR2 (30)  	:= cssp_obiegi_utl.odczytaj_kategorie;
   v_suma_psk            NUMBER;
   v_kwota_do_podzialu   NUMBER;
   v_wnd_numer           ppadm.ppt_del_wnioski_delegacji.wnd_numer%TYPE;
BEGIN
   IF v_typ = 'W'
   THEN
      v_wnd_numer := wdrp_delegacje_mf.wnd_numer (v_id);

      BEGIN
         SELECT NVL (SUM (NVL (pkl_kwota_mf, 0)), 0)
           INTO v_kwota_do_podzialu
           FROM wdrv_kg_delegacje
          WHERE wnd_id = v_id AND kal_rodzaj = 'ROZ';

         SELECT NVL (SUM (NVL (ang_kwota, 0)), 0)
           INTO v_suma_psk
           FROM wdrv_kg_del_podzial_sk_rozl
          WHERE wnd_id = v_id;

         IF v_kwota_do_podzialu = 0
         THEN
            ppadm.msg
               (eguser,
                'KONTROLA ROZPISANIA ROZLICZENIA NA ŹRODŁA FIN.',
                '<font color="red"><strong>'
                || 'UWAGA'
                || '</strong></font>'
                || CHR (10)
                || 'Kwota rozliczenia do podziału na źrodła finansowania dla delegacji numer '
                || v_wnd_numer
                || ' wynosi '
                || TO_CHAR
                      (v_kwota_do_podzialu,
                       wdrp_zak_kontrola_budzetowa_mf.c_format_liczby_do_odczytu
                      )
                || '. Operacja została przerwana.',
                NULL,
                'ERROR'
               );
			/*eap_blad.zglos (	p_procedura => c_procedura,
                                p_nazwa 	=> 'WDR_BLAD',
                                p_p1  		=> 'Kwota kalkulacji do podziału na źrodła finansowania dla delegacji numer '||v_wnd_numer||' wynosi '||to_char(v_kwota_do_podzialu,wdrp_zak_kontrola_budzetowa_mf.c_format_liczby_do_odczytu)||'. Operacja została przerwana.',
                                p_p2        => '',
                                p_p3        => '' );*/
            ROLLBACK;
         ELSIF v_kwota_do_podzialu <> 0 AND v_suma_psk <> v_kwota_do_podzialu
         THEN
            ppadm.msg
               (eguser,
                'KONTROLA ROZPISANIA ROZLICZENIA NA ŹRODŁA FIN.',
                '<font color="red"><strong>'
                || 'UWAGA'
                || '</strong></font>'
                || CHR (10)
                || 'Delegacja numer '
                || v_wnd_numer
                || '. Kwota rozliczenia nie została rozpisana w całości na źrodła finansowania. Operacja została przerwana.'
                || CHR (10)
                || 'Kwota kalkulacji: '
                || TO_CHAR
                      (v_kwota_do_podzialu,
                       wdrp_zak_kontrola_budzetowa_mf.c_format_liczby_do_odczytu
                      )
                || CHR (10)
                || 'Kwota przypisana do źrodeł fin.: '
                || TO_CHAR
                      (v_suma_psk,
                       wdrp_zak_kontrola_budzetowa_mf.c_format_liczby_do_odczytu
                      ),
                NULL,
                'ERROR'
               );
            /*eap_blad.zglos (	p_procedura => c_procedura,
                                p_nazwa     => 'WDR_BLAD',
                                p_p1        => 'Delegacja numer '||v_wnd_numer||'. Kwota kalkulacji nie została rozpisana w całości na źrodła finansowania. Operacja została przerwana.'||chr(10)||'Kwota kalkulacji: '||to_char(v_kwota_do_podzialu,wdrp_zak_kontrola_budzetowa_mf.c_format_liczby_do_odczytu)||CHR(10)||
											   'Kwota przypisana do źrodeł fin.: '||to_char(v_suma_psk,wdrp_zak_kontrola_budzetowa_mf.c_format_liczby_do_odczytu ),
                                p_p2        => '',
                                p_p3        => '' );*/
            ROLLBACK;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            ppadm.msg
                    (eguser,
                     'KONTROLA ROZPISANIA ROZLICZENIA NA ŹRODŁA FIN.',
                     '<font color="red"><strong>'
                     || 'UWAGA'
                     || '</strong></font>'
                     || CHR (10)
                     || 'Nie wstawiono rozliczenia dla delegacji numer '
                     || v_wnd_numer
                     || '. Operacja została przerwana.'
                     || SQLERRM,
                     NULL,
                     'ERROR'
                    );
            /*eap_blad.zglos (	p_procedura => c_procedura,
                                p_nazwa     => 'WDR_BLAD',
                                p_p1        => 'Nie wstawiono kalkulacji dla delegacji numer '||v_wnd_numer||'. Operacja została przerwana.'||sqlerrm,
                                p_p2        => '',
                                p_p3        => '' );*/
            ROLLBACK;
         WHEN OTHERS
         THEN
            eap_blad.zglos (p_procedura => c_procedura);
      END;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      eap_blad.zglos (p_procedura => c_procedura|| ' p_cob_id='|| v_cob_id);
END;