--obieg WN_DELKv2 POZIOM80->POZIOM70
declare 
    v_ret varchar2(100);
    v_id NUMBER :=:ID;
    v_numer varchar2(100);
    v_pracownik varchar2(500);
    v_termin varchar2(500);
    v_miejscowosci varchar2(500);
    v_zapraszajacy varchar2(500);
    v_koszt NUMBER;

   v_typ      VARCHAR2 (1) := :TYP;
   v_cob_id   NUMBER := :COB_ID;
   v_akc_id   NUMBER := :AKC_ID;
   v_opis     VARCHAR2 (4000) := :OPIS;
begin

      SELECT 
        WND.WND_NUMER numer,
        PRC.PRC_IMIE ||' '||PRC.PRC_NAZWISKO prac,
        to_char(Wnd.Wnd_Data_Wyjazdu,'DD.MM.YYYY') || ' - ' || to_char(Wnd.Wnd_Data_Powrotu,'DD.MM.YYYY') termin,
        ( Select 
            LISTAGG(Cdel_Miejscowosc, '; ') WITHIN GROUP (ORDER BY Cdel_Miejscowosc) 
            From PPADM.Ppt_Del_Cele_Delegacji
          Where Cdel_Wnd_Id=v_id
        ) miejscowosci,
        (Select 
            LISTAGG(ZAPR_OPIS, '; ') WITHIN GROUP (ORDER BY ZAPR_OPIS) 
            From PPADM.Ppt_Del_Zapraszajacy
          Where zapr_wnd_id = v_id
        )zapraszajacy,
        PPADM.KOSZTY_DELEGACJI(v_id, 'ROZ') koszt
      into  v_numer, v_pracownik, v_termin, v_miejscowosci, v_zapraszajacy, v_koszt 
      FROM PPADM.PPT_DEL_WNIOSKI_DELEGACJI WND
      join EGADM1.EK_PRACOWNICY PRC on PRC.PRC_ID=WND.WND_PRC_ID
      WHERE WND_ID=v_id
      ;
      
      
PPADM.WIADOMOSC_UZT('MDUBLEWSKA',
              'Zwrot rozliczenia do korekty ' || CHR(13)||CHR(10)||CHR(13)||CHR(10)|| ppadm.get_adres()|| 'Delegacje?ppl_del_lista_delegacji='|| v_id
              ||CHR(13)||CHR(10)||CHR(13)||CHR(10)|| V_Opis,
              'KOREKTA ROZLICZENIA KOSZTÓW PODRÓŻY NR ' || V_Numer,
              P_WDM_TYP=>'M',
              P_WDM_ADRES=>'malgorzata.dublewska@mf.gov.pl');
end;