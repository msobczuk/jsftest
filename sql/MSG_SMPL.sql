-- Komunikacja bazy z użytkownikiem (komunikaty systemowe/techniczne + wiadomości/powiadomienia):
-- DB/Egeria --dbms_alert--> Tomcat --websocket--> Client 
-- istotne pakiety i pliki w kolejności przetwarzania: 
-- PPADM.MSG (procedura PL/SQL  wysyłająca komunikaty / pomocnicze procedury to MSG_AUTONOMOUS_TRANSACTION oraz MSG_ON_COMMIT) ...
-- MsgPipeListener.java - odbiera w/w komunikaty (na serwerze app.) i rozdziela je do własciwych sesji użytkowników PP (m.in. zapisuje je w sesyjnych User.java)...
-- Push.java - przesyła komunikat techniczny do określonego klienta, aby zmusić go do reakcji własnym requestem na ten komunikat (potrzebny jest dedykowany  request aby pobrać treść lub wykonac zlecona na srv. akcję; colowo nie przesłamy tędy całej otrzymanej wiadomości, bo takie działanie z użyciem websocket'ów powoduje często błedy typu malformed exc)....
-- EgrTemplateMainMenu.xhtml klient po odebraniu komunikatu techn. (websocket) wywołuje w js: rcProcessNewMsgs (-> ...  action="#{msgsBean.processNewMsgs()}" ...). 
-- msgsBean.processNewMsgs() decyduje o tym w jaki spoób z poziomu request'a danej sesji zostanie przetorzony dany komunikat. Taka reakcją jest np. pobranie powiadomienia, wyświetlenie komunikatu systemowego lub wylogowanie... 
-- MsgsBean.java + Msgs.xhtml (gdzie wyłaczono longpolling) - wczytuje komunikaty systemowe i wyświetla je użytkownikówi ...
-- PracownikPowiadomieniaSqlBean.java + Powiadomienia.xhtml  - wczytuje powiadomienia ...


-- Przykłady wysyłania komunikatów i powiadomień z bazy do użytkownika PP:



--komunikat systemowy (trafia bezpośrednio na ekran zalogowanego użytkownika PP)
begin 

PPADM.MSG(
'ADMINISTRATOR_PORTAL', --'MDZIOBEK', --user
'Temat: Osiół Kłąpoóchy - wyliczanka na dzień ' || SYSDATE, 
'<b>Tekst: (0123456789 ąęćłńóśźż ĄĘĆŁŃÓŚŹŻ)</b>
<font color=''red''>Ala ma kota.</font>
<font color=''green''>Ela ma psa.</font>
<font color=''blue''>Ola ma rybki.</font>
<i>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. Quisque semper justo at risus. Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci. Nam congue, pede vitae dapibus aliquet, elit magna vulputate arcu, vel tempus metus leo non est. Etiam sit amet lectus quis est congue mollis. Phasellus congue lacus eget neque. Phasellus ornare, ante vitae consectetuer consequat, purus sapien ultricies dolor, et mollis pede metus eget nisi. Praesent sodales velit quis augue. Cras suscipit, urna at aliquam rhoncus, urna quam viverra nisi, in interdum massa nibh nec erat.
</i>

<a href="https://www.w3schools.com/html/html_formatting.asp"><font color=''blue''>HTML - formatowanie tekstu</font></a> ',
null, --wdm_id
'ERROR' --severity --  'ERROR', 'WARN', 'INFO'
);
end;
/



--wiadomość (+powiadomienie) - w PP jako dymek z pocz. tematem + możliwość odczytania przez moduł powiadomień...
begin 
  PPADM.WSTAW_WIADOMOSC_UZT('ADMINISTRATOR_PORTAL', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. P');
  commit;
end;
/





--WYLOGUJ/USUŃ SESJĘ PP Z BAZY (prawidłową reakcją jest wylogowanie uzytkownika w PP)
begin
  eap_web.czysc_kontekst( '923738862092FBEDA80F4F1C06EAD57B');
end;
/


--pozorne wylogowanie sesji klienta (takie wywołanie służy tylko do testowania czy i jak reaguje system na taki komunikat. Na eat_sesje_zewnetrzne jest trigger, ktory rozsyła powiadomienia, więc aby produkcyjnie wylogować sesję z bazy używamy eap_web.czysc_kontekst... a ten przy kasowaniu danych z eat_sesje_zewnetrzne wysyła taki komunikat)
begin 
PPADM.MSG(
'ADMINISTRATOR_PORTAL',  --user
'923738862092FBEDA80F4F1C06EAD57B',  --session_id
p_severity => 'LOGOUT' --severity --  'LOGOUT', 'ERROR', 'WARN', 'INFO'
);
end;
/


select * from eaadm.eat_sesje_zewnetrzne;
/

