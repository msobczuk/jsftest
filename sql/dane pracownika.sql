 select 
    prc_id              pp_prcid, 
    prc_imie            pp_imie, 
    prc_nazwisko        pp_nazwisko,
    P.PRC_IMIE_MATKI    pp_imiematki,
    P.PRC_IMIE_OJCA     pp_imieojca,
    P.PRC_DATA_UR       pp_dataurodzenia,
    P.PRC_MIEJSCE_UR    pp_miejsceurodzenia, 
    P.PRC_PESEL         pp_pesel, 
    P.PRC_PASZPORT      pp_paszport, 
    -- TODO:  pp_datawzanoscipaszportu
    P.PRC_DOWOD_OSOB            pp_dowodosobisty, 
    P.PRC_DATA_WYDANIA_DOWODU   pp_datatwydaniadowodu,    
    P.PRC_DATA_WAZNOSCI_DOWODU  pp_datawaznoscidowodu,  
    P.PRC_ORGAN_WYDAJACY        pp_organwydajacy,
    P.PRC_OBYWATELSTWO          pp_obywatelstwo, 
    
    
    --P.PRC_USK_ID --> urzad skarbowy
    US.KL_NAZWA                         pp_urzadskarbowy,
    ek_pck_adresy.adres (KL_ADR_ID)     pp_us_adres,
    --    US.KL_ADR_MIEJSCOWOSC       pp_us_miescowosc,
    --    US.KL_ADR_ULICA             pp_us_ulica,
    --    US.KL_ADR_NUMER_DOMU        pp_us_nrdomu,
    --    US.KL_ADR_NUMER_LOKALU      pp_us_nrlokalu, 
    --    US.KL_ADR_KOD_POCZTOWY      pp_us_kodpocztowy,
    --    US.KL_ADR_POCZTA            pp_us_poczta,
    

    -- adres sta�y (zameldowana)
   (select KR_NAZWA from  CSS_KRAJE where KR_ID = ADR.ADR_KR_ID)            pp_adreskraj,
   (select WOJ_NAZWA from  CSS_WOJEWODZTWA where WOJ_ID = ADR.ADR_WOJ_ID)   pp_adreswojwodztwo,
   ADR.ADR_POWIAT                                                           pp_adrespowiat, 
   ADR.ADR_GMINA                                                            pp_adresgmina,
   ek_pck_adresy.adres (ADR.adr_id)                                         pp_adres,
    ---- albo w pelnej normalizacji:    
    --   ADR.ADR_MIEJSCOWOSC, 
    --   ADR.ADR_ULICA,
    --   ADR.ADR_NUMER_DOMU, 
    --   ADR_NUMER_LOKALU
    --   ADR.ADR_KOD_POCZTOWY,
    --   ADR.ADR_POCZTA, 
   

    -- adres Odb. (tymczasowy)
   (select KR_NAZWA from  CSS_KRAJE where KR_ID = ADR_O.ADR_KR_ID)            pp_adrestymczkraj,
   (select WOJ_NAZWA from  CSS_WOJEWODZTWA where WOJ_ID = ADR_O.ADR_WOJ_ID)   pp_adrestymczwojwodztwo,
   ADR_O.ADR_POWIAT                                                           pp_adrestymczpowiat, 
   ADR_O.ADR_GMINA                                                            pp_adrestymczgmina,
   ek_pck_adresy.adres (ADR_O.adr_id)                                         pp_adrestymcz,
    ---- albo w pelnej normalizacji:    
    --   ADR.ADR_MIEJSCOWOSC, 
    --   ADR.ADR_ULICA,
    --   ADR.ADR_NUMER_DOMU, 
    --   ADR_NUMER_LOKALU
    --   ADR.ADR_KOD_POCZTOWY,
    --   ADR.ADR_POCZTA,    
   
  

    -- adres Korespondencyjny
   (select KR_NAZWA from  CSS_KRAJE where KR_ID = ADR_O.ADR_KR_ID)            pp_adreskorespkraj,
   (select WOJ_NAZWA from  CSS_WOJEWODZTWA where WOJ_ID = ADR_O.ADR_WOJ_ID)   pp_adreskorespwojwodztwo,
   ADR_O.ADR_POWIAT                                                           pp_adreskoresppowiat, 
   ADR_O.ADR_GMINA                                                            pp_adreskorespgmina,
   ek_pck_adresy.adres (ADR_O.adr_id)                                         pp_adreskoresp
    ---- albo w pelnej normalizacji:    
    --   ADR.ADR_MIEJSCOWOSC, 
    --   ADR.ADR_ULICA,
    --   ADR.ADR_NUMER_DOMU, 
    --   ADR_NUMER_LOKALU
    --   ADR.ADR_KOD_POCZTOWY,
    --   ADR.ADR_POCZTA,    
 
 -- > TODO: konta bankowe jako osobny select
    
 from   EGADM1.EK_PRACOWNICY P, 
        EGADM1.CKK_KLIENCI_AKT US, 
        EGADM1.CKK_ADRESY ADR,
        
        EGADM1.CKK_ADRESY ADR_O, 
        
        EGADM1.CKK_ADRESY ADR_K
    
 where prc_numer = 10
    
    and P.PRC_USK_ID = US.KL_KOD
    
    and P.PRC_ID = ADR.adr_prc_id (+)
    and 'T' = ADR.Adr_zatwierdzony (+)
    and 'S' = ADR.adr_typ (+)
 
    and P.PRC_ID = ADR_O.adr_prc_id (+)
    and 'T' = ADR_O.Adr_zatwierdzony (+)
    and 'O' = ADR_O.adr_typ (+)
 
    and P.PRC_ID = ADR_K.adr_prc_id (+)
    and 'T' = ADR_K.Adr_zatwierdzony (+)
    and 'O' = ADR_K.adr_typ (+)
 
 -- prc_id = 100151
-- prc_numer = 10
