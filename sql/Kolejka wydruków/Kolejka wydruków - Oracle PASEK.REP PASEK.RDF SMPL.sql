
declare
  vn_rap_id number;
  v_wartosc varchar2(2000);
  v_kg_id number;
begin
  eap_globals.ustaw_firme(1);
  eap_globals.ustaw_konsolidacje('N');
  
  v_kg_id:=cssp_genrap.fn_wstaw_raport_kolejka('pasek','REPORTS');
  
  --dbms_output.put_line('wynik : '||v_kg_id);                                                            
  
  if nvl(v_kg_id,0)>0 then
  
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_TRP_ID','106653');    
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_data_wypelnienia', to_date('23-03-2018','dd-mm-yyyy'));    
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_podsumowanie', 'T');   
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_rok', '2017');     
    cssp_genrap.p_wstaw_parametr(v_kg_id,'P_FRM_ID', '1');      
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_angaz', '2017');       
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_grupowanie', 'BRAK');       
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_dk_kod', 'PASEK');     
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_oswiadczenie', 'T');     
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_tresc_pod', ' ');     
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_rodzaj_listy', 'LPLAC');       
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_sk_id', '2017');     
    cssp_genrap.p_wstaw_parametr(v_kg_id,'P_JEZ_ID', 1);     
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_typ_daty_listy', 'ZA_MIESIAC');       
    cssp_genrap.p_wstaw_parametr(v_kg_id,'p_miesiac', '2');     
      
  end if;
  
  commit; 
end;   
/
