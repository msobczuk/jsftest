ALTER TABLE EGADM1.PPT_URLOPY_PLANOWANE_DNI
 DROP PRIMARY KEY CASCADE;

DROP TABLE EGADM1.PPT_URLOPY_PLANOWANE_DNI CASCADE CONSTRAINTS;

----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------
-- select * from  EGADM1.PP_URLOPY_HARMONOGRAMY_ROCZNE
CREATE TABLE EGADM1.PPT_URLOPY_PLANOWANE_DNI
(
  URPD_ID    NUMBER,
  URPD_URPL_ID  NUMBER,
  URPD_PRC_ID   NUMBER,
  URPD_KAL_ID   NUMBER,
  URPD_ORG_ID   NUMBER,
  URPD_ROK   NUMBER,
  URPD_DATA  DATE,
  URPD_TYP   VARCHAR2(50 BYTE),
  URPD_OPIS  VARCHAR2(250 BYTE)
)
TABLESPACE EG_D;


ALTER TABLE EGADM1.PPT_URLOPY_PLANOWANE_DNI 
ADD CONSTRAINT "PPC_URPD_PK" PRIMARY KEY ("URPD_ID")
USING INDEX TABLESPACE "EG_X" ENABLE;


ALTER TABLE EGADM1.PPT_URLOPY_PLANOWANE_DNI ADD (
  CONSTRAINT PPC_URPD_URPL_FK 
 FOREIGN KEY (URPD_URPL_ID) 
 REFERENCES EGADM1.PPT_URLOPY_PLANOWANE (URPL_ID)
    ON DELETE CASCADE);



--insert into EGADM1.PPT_URLOPY_PLANOWANE_DNI 
--    (   URPD_ID,        URPD_URPL_ID,   URPD_PRC_ID, URPD_KAL_ID, URPD_ORG_ID, URPD_ROK, URPD_DATA, URPD_TYP, URPD_OPIS) 
--select  PP_URLHR_ID,    null,           100220,      PP_KAL_ID, PP_ORG_ID, PP_URLHR_ROK, PP_URLHR_DATA, PP_URLHR_TYP, PP_URLHR_OPIS 
--from  EGADM1.PP_URLOPY_HARMONOGRAMY_ROCZNE ;
