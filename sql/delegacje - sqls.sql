select * from  EGADM1.DELT_WNIOSKI_DELEGACJI WND;
--    WND_ID --PK
--    WND_NUMER -- numer delegacji
--    WND_OPIS -- uwagi
--    WND_PRC_ID_DEL -- pracownik delegowany i jednoczesnie przewodniczący dla del. wieloosobowych

select * from EGADM1.DELT_CELE_DELEGACJI CDEL;

select * from  EGADM1.DELT_ROZLICZENIA_DELEGACJI ROZD;
select * from  EGADM1.DELT_TRASY TRS;

select * from  EGADM1.DELT_SRODKI_LOKOMOCJI SLOK;


select * from  EGADM1.DELT_PODZIAL_BUDZETOWY PDB;

select * from  EGADM1.DELT_PODZIALY_KOSZTOW PKS;

select * from  EGADM1.ZKT_ZALACZNIKI ZAL;


select * from  EGADM1.DELT_WNISKI_DELEGACJI

--select * from  EGADM1.DELT_UPRAWNIENIA UPR;

;
--stawki w delegacjach
select  WST.WST_KR_ID, WST.WST_KWOTA, WST.WST_WAL_ID
, std.*
, wst.* 
from EGADM1.DELT_STAWKI_DELEGACJI std
    left join EGADM1.DELT_WARTOSCI_STAWEK WST on STD.STD_ID = WST.WST_STD_ID
where  1=1 
    --and STD.STD_KOD_STAWKI = 'KO1'  
;


--select * from   EGADM1.DELT_POZYCJE_KALKULACJI --PKL_KR_ID; PKL_WAL_ID; PKL_STAWKA

--stawki dla pozycji delegacji
select * 
from  EGADM1.DELT_TYPY_POZYCJI TPOZ
    join EGADM1.DELT_STAWKI_DELEGACJI STD on TPOZ.TPOZ_STD_ID = STD.STD_ID
    join EGADM1.DELT_WARTOSCI_STAWEK WST on STD.STD_ID = WST.WST_STD_ID 
;

-- tylko wartosci stawek dla pozycji
select WST.WST_KWOTA, WST.WST_PROCENT, wst.* 
from  EGADM1.DELT_TYPY_POZYCJI TPOZ
    --join EGADM1.DELT_STAWKI_DELEGACJI STD on TPOZ.TPOZ_STD_ID = STD.STD_ID
    join EGADM1.DELT_WARTOSCI_STAWEK WST on TPOZ.TPOZ_STD_ID = WST.WST_STD_ID
where  1=1
    and WST.WST_KR_ID = 8
    --and WST.WST_OBOWIAZUJE_OD <= sysdate
;




PAKIETY:

EGADM1.DELP_OBJ_WNIOSEK
EGADM1.DELP_ALGORYTMY
EGADM1.DELP_RAPORTY
EGADM1.DELP_OBJ_UPR
EGADM1.DELP_OBJ_RDEL
