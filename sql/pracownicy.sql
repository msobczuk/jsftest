    select --distinct
          PRC.PRC_ID
        , PRC.PRC_IMIE
        , PRC.PRC_NAZWISKO

        , PZAT_STANOWISKO STN_NAZWA
        , PZAT.PZAT_OB_ID || ' - ' || PZAT.PZAT_OB_NAZWA JED_ORG

        , PZAT.PZAT_OB_NAZWA
        , PZAT.PZAT_OB_ID
        , ZAT.ZAT_PRC_ID_SZEF

        --, PZAT.*
        --, ZAT.*
    from EK_PRACOWNICY PRC
        left join EGADM1.EK_PIK_ZATRUDNIENIE PZAT on PRC.PRC_ID = PZAT.PZAT_PRC_ID and PZAT.PZAT_TYP_UMOWY=0 and sysdate <= NVL(PZAT.PZAT_OKRES_DO,sysdate)
        --left join EGADM1.EK_PIK_ZATRUDNIENIE PZAT on PRC.PRC_ID = PZAT.PZAT_PRC_ID and PZAT.PZAT_TYP_UMOWY=0 and sysdate <= NVL(PZAT.PZAT_DATA_DO,sysdate)
        left join EGADM1.EK_ZATRUDNIENIE ZAT on PZAT.PZAT_ZAT_ID=ZAT.ZAT_ID
    order by PRC.PRC_NAZWISKO, PRC.PRC_IMIE  