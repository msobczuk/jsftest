---------------------------------------------------------------
-- aktualne zatrudnienie dla pracowników/wybranego pracownika
---------------------------------------------------------------
SELECT *
FROM
  ek_zatrudnienie
WHERE   zat_typ_umowy = 0
  AND trunc(SYSDATE) BETWEEN zat_data_zmiany AND nvl(zat_data_do, trunc(SYSDATE+(365*1000)))
;

---------------------------------------------------------------
-- aktualne zatrudnienie dla pracowników/wybranego pracownika
---------------------------------------------------------------
SELECT *
FROM  ek_zatrudnienie
WHERE    zat_typ_umowy = 0
  AND    trunc(SYSDATE) >= zat_data_zmiany
  AND    trunc(SYSDATE) <= nvl(zat_data_do,trunc(SYSDATE) )
;


    select * 
    from EGADM1.EK_ZATRUDNIENIE zat
    where zat.ZAT_PRC_ID = p_prc_id 
        AND    zat_data_zmiany <=   trunc(p_na_dzien)
        AND                         trunc(p_na_dzien)  <=  nvl(zat_data_do, trunc(SYSDATE+(365*1000)) )
        AND rownum = 1;