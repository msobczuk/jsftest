select
 
   czr_prc_id           pp_prc_id , 
   czr_pesel            pp_czr_pesel ,
   czr_imie             pp_czr_imie ,
   czr_nazwisko         pp_czr_nazwisko , 
   rv_meaning           pp_czr_pokrewienstwo , 
   to_char(czr_uzysk_ub, 'DD-MM-YYYY')  pp_czr_uzysk_ub , 
   to_char(czr_utrata_ub,'DD-MM-YYYY')  pp_czr_utrata_ub ,  
   (SELECT ek_pck_adresy.adres(adr_id) FROM CKK_ADRESY WHERE adr_typ='S' AND adr_czr_id = czr_id AND Adr_zatwierdzony = 'T' AND Adr_f_aktualne = 'T') pp_czr_adres
       
from ek_czlonkowie_rodziny, ek_ref_codes

where 1=1 
    and czr_prc_id = 100220 --?
    and rv_low_value(+) = czr_pokrewienstwo
    and rv_domain(+) = 'STOPIEN_POKREWIENSTWA' 

order by  czr_prc_id