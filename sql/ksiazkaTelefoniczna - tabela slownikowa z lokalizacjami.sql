
  CREATE TABLE "EGADM1"."PP_KSIAZKA_LOKALIZACJE" 
   (	"PLO_ID" NUMBER NOT NULL ENABLE, 
	"PLO_KOD" VARCHAR2(10 BYTE) NOT NULL ENABLE, 
	"PLO_WARTOSC" VARCHAR2(100 BYTE) NOT NULL ENABLE, 
	"PLO_PLO_ID" NUMBER, 
	"PRC_AUDYT_UT" VARCHAR2(30 BYTE),
	"PRC_AUDYT_DT" DATE,
	"PRC_AUDYT_UM" VARCHAR2(30 BYTE),
	"PRC_AUDYT_DM" DATE,
	"PRC_AUDYT_KT" VARCHAR2(100 BYTE), 
	"PRC_AUDYT_KM" VARCHAR2(100 BYTE), 
	"PRC_AUDYT_LM" NUMBER, 
	 CONSTRAINT "PP_SLOWNIKI_PK" PRIMARY KEY ("PLO_ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "KG_D"  ENABLE
   ) PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT)
  TABLESPACE "KG_D" ;
 

   COMMENT ON COLUMN "EGADM1"."PP_KSIAZKA_LOKALIZACJE"."PLO_ID" IS 'ID';
 
   COMMENT ON COLUMN "EGADM1"."PP_KSIAZKA_LOKALIZACJE"."PLO_KOD" IS 'Kod elementu';
 
   COMMENT ON COLUMN "EGADM1"."PP_KSIAZKA_LOKALIZACJE"."PLO_WARTOSC" IS 'Wartość elementu';
 
   COMMENT ON COLUMN "EGADM1"."PP_KSIAZKA_LOKALIZACJE"."PLO_PLO_ID" IS 'ID rodzica';
 
   COMMENT ON COLUMN "EGADM1"."PP_KSIAZKA_LOKALIZACJE"."PRC_AUDYT_UT" IS 'Użytkownik, który utworzył rekord';
 
   COMMENT ON COLUMN "EGADM1"."PP_KSIAZKA_LOKALIZACJE"."PRC_AUDYT_DT" IS 'Data utworzenia rekordu';
 
   COMMENT ON COLUMN "EGADM1"."PP_KSIAZKA_LOKALIZACJE"."PRC_AUDYT_UM" IS 'Użytkownik, który ostatnio modyfikował rekord';
 
   COMMENT ON COLUMN "EGADM1"."PP_KSIAZKA_LOKALIZACJE"."PRC_AUDYT_DM" IS 'Data modyfikacji rekordu';
 
   COMMENT ON COLUMN "EGADM1"."PP_KSIAZKA_LOKALIZACJE"."PRC_AUDYT_KT" IS 'Nazwa komputera, z którego utworzono rekord.';
 
   COMMENT ON COLUMN "EGADM1"."PP_KSIAZKA_LOKALIZACJE"."PRC_AUDYT_KM" IS 'Nazwa komputera, z którego modyfikowano rekord.';
 
   COMMENT ON COLUMN "EGADM1"."PP_KSIAZKA_LOKALIZACJE"."PRC_AUDYT_LM" IS 'Liczba modyfikacji rekordu';
 
