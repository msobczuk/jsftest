select distinct 
      to_char(lst_data_obliczen, 'mm-yyyy') lst_mc, 
      lst_drl_kod,
      drl_nazwa, 
      lst_data_obliczen

from ek_skladniki, ek_listy, ek_def_rodzajow_list 

where sk_lst_id = lst_id and
      drl_kod = lst_drl_kod
      and sk_prc_id = 100220
      and lst_zatwierdzona = 'T'

order by lst_data_obliczen desc 