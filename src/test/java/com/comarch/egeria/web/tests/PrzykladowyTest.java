package com.comarch.egeria.web.tests;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;

public class PrzykladowyTest {
        private static int pointOfLife;

        @BeforeClass
        public static void initTests() {
                pointOfLife = 42;
        }
        @Test
        public void testZero() {
        	
                assertEquals("Point of life must be 42", pointOfLife, 42);
        }
}
