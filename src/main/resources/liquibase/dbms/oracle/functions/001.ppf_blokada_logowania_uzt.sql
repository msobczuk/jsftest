--liquibase formatted sql
--changeset PP:PPF_BLOKADA_LOGOWANIA_UZT runOnChange:false
--preconditions onFail:WARN onError:WARN
create or replace function PPF_BLOKADA_LOGOWANIA_UZT(P_UZT_NAZWA_ZEWN VARCHAR2) return VARCHAR2 is
begin
    -- ��期���
    -- wersja std nie przewidujemy zadnych blokad; w wersji spersonalizowanej mozna zastosowac wlasny kod
    -- Je�eli u�ytkownik nie ma �adnej blokady to funkcja zwraca null.
    -- W przypadku blokady zwrocony zostaje komunikat z powodem blokady.
    --
    return null; --jesli nie ma blokady
end;
/
