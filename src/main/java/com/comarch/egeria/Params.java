package com.comarch.egeria;

import java.util.HashMap;

public class Params extends HashMap<String, Object> {


    public Params (){}


    public Params (String key, Object value){
        this.put(key,value);
    }


    public Params (String key1, Object value1,
                   String key2, Object value2){
        this.put(key1,value1);
        this.put(key2,value2);
    }

    public Params (String key1, Object value1,
                   String key2, Object value2,
                   String key3, Object value3){
        this.put(key1,value1);
        this.put(key2,value2);
        this.put(key3,value3);
    }


    public Params (String key1, Object value1,
                   String key2, Object value2,
                   String key3, Object value3,
                   String key4, Object value4
    ){
        this.put(key1,value1);
        this.put(key2,value2);
        this.put(key3,value3);
        this.put(key4,value4);
    }


    public Params (String key1, Object value1,
                   String key2, Object value2,
                   String key3, Object value3,
                   String key4, Object value4,
                   String key5, Object value5
    ){
        this.put(key1,value1);
        this.put(key2,value2);
        this.put(key3,value3);
        this.put(key4,value4);
        this.put(key5,value5);
    }



    public Params add(String key, Object value){
        this.put(key,value);
        return  this;
    }

}
