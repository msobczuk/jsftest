package com.comarch.egeria.web.mail;

public interface MailDao {

  MailParameters getMailParameters() throws MailConfigException;
}
