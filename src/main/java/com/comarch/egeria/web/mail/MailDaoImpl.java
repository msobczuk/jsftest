package com.comarch.egeria.web.mail;

import com.comarch.egeria.pp.data.JdbcUtils;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Named
@Scope("singleton")
@Component
public class MailDaoImpl implements MailDao {

  private static final Logger logger = LoggerFactory.getLogger(MailDaoImpl.class);

  @Override
  public MailParameters getMailParameters() throws MailConfigException {

    String pusKod = "EGERIA_MAILER";

    logger.info("Pobieranie parametr�w us�ugi {} z bazy danych...", pusKod);

    String sql = "DECLARE\r\n"
        + "  PROCEDURE pobierz_parametry(p_kod IN VARCHAR2, p_uzytkownik OUT VARCHAR2, p_haslo OUT VARCHAR2, p_smtp_host OUT VARCHAR2, p_smtp_port OUT VARCHAR2, p_smtp_from OUT VARCHAR2) IS\r\n"
        + "    r_pus eap_parametry_uslug_sieciowych.t_parametr_uslug_sieciowych;\r\n"
        + "  BEGIN\r\n"
        + "    eap_parametry_uslug_sieciowych.odczytaj(p_kod, NULL, r_pus);\r\n"
        + "    p_uzytkownik := r_pus.pus_uzytkownik;\r\n"
        + "    p_haslo := eap_parametry_uslug_sieciowych.odszyfruj(r_pus.pus_haslo);\r\n"
        + "    p_smtp_host := r_pus.pus_param1_wartosc;\r\n"
        + "    p_smtp_port := r_pus.pus_param2_wartosc;\r\n"
        + "    p_smtp_from := r_pus.pus_param3_wartosc;\r\n" + "  END;\r\n" + "BEGIN \r\n"
        + "  pobierz_parametry(?, ?, ?, ?, ?, ?);\r\n" + "END;";

    MailParameters params = new MailParameters();

    try (Connection conn = JdbcUtils.getConnection(false); CallableStatement stmt = conn.prepareCall(sql)) {
      stmt.setString(1, pusKod);
      stmt.registerOutParameter(2, java.sql.Types.VARCHAR);
      stmt.registerOutParameter(3, java.sql.Types.VARCHAR);
      stmt.registerOutParameter(4, java.sql.Types.VARCHAR);
      stmt.registerOutParameter(5, java.sql.Types.VARCHAR);
      stmt.registerOutParameter(6, java.sql.Types.VARCHAR);

      stmt.execute();

      params.setUsername(stmt.getString(2));
      params.setPassword(stmt.getString(3));
      params.setSmtpHost(stmt.getString(4));
      params.setSmtpPort(stmt.getString(5));
      params.setSmtpFrom(stmt.getString(6));
    } catch (Exception e) {
      throw new MailConfigException("Problem z odczytaniem konfiguracji konta mail", e);
    }

    return params;
  }

}
