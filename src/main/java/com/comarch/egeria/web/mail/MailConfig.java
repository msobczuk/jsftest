package com.comarch.egeria.web.mail;

import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.web.common.utils.constants.Const;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.annotation.EnableAsync;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableAsync
public class MailConfig {

  private static final Logger log = LoggerFactory.getLogger(MailConfig.class);

  @Inject
  MailDao mailDao;

  @Resource(mappedName = Const.JNDI_CONNECTION_NAME)
  private DataSource ds;

  @Bean
  public JavaMailSender getJavaMailSender() {
    try {
      return createJavaMailSender();
    } catch (MailConfigException e) {
      log.error("Problem z wczytaniem konfiguracji maila");
      return createJavaMailSenderForErrorConfig();
    }
  }

  private JavaMailSender createJavaMailSender() throws MailConfigException {
    DataSource ds_ = JdbcUtils.ds;
    if (ds_==null) JdbcUtils.ds = this.ds;

    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    MailParameters mailParameters = mailDao.getMailParameters();
    mailSender.setHost(mailParameters.getSmtpHost());
    mailSender.setPort(Integer.parseInt(mailParameters.getSmtpPort()));

    mailSender.setUsername(mailParameters.getUsername());
    mailSender.setPassword(mailParameters.getPassword());

    Properties props = mailSender.getJavaMailProperties();
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.timeout", 60000);
    switch (mailParameters.getSmtpPort()) {

      case "465":
        props.put("mail.smtp.ssl.enable", "true");
        break;
      case "25":
      case "587":
      default:
        props.put("mail.smtp.starttls.enable", "true");
    }
    props.put("mail.smtp.from", mailParameters.getUsername());
    props.put("mail.debug", "false");
    return mailSender;
  }

  private JavaMailSender createJavaMailSenderForErrorConfig() {
    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    Properties props = mailSender.getJavaMailProperties();
    props.put("mail.smtp.timeout", 1);
    return mailSender;
  }
}
