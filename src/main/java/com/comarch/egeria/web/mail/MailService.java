package com.comarch.egeria.web.mail;

import javax.inject.Named;
import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;

@Named
@Scope("singleton")
public class MailService {

  private static final Logger log = LoggerFactory.getLogger(MailService.class);
  private static final String MAIL_FROM_ADDRESS = "portal.support@comarch.pl";

  @Autowired
  public JavaMailSender mailSender;

  @Async
  public void send(String mailAddres, String mailSubject, String mailBody) {
    try {
      MimeMessagePreparator preparator = createMailPreparator(mailAddres, mailSubject, mailBody);
      mailSender.send(preparator);
    } catch (Exception e) {
      log.error("Problem z wysłaniem maila, adres użytkownika: " + mailAddres, e);
    }
  }

  private MimeMessagePreparator createMailPreparator(String mailAddres, String mailSubject,
      String mailBody) {
    return mimeMessage -> {
      mimeMessage.setRecipient(Message.RecipientType.TO,
          new InternetAddress(mailAddres.toLowerCase()));
      InternetAddress from = new InternetAddress(MAIL_FROM_ADDRESS);
      from.setPersonal("Portal Pracowniczy");
      mimeMessage.setFrom(from);
      mimeMessage.setSubject(mailSubject);
      mimeMessage.setText(mailBody);
    };
  }
}
