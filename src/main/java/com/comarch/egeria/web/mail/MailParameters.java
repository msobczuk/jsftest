package com.comarch.egeria.web.mail;

public class MailParameters {

	private String username;
	private String password;
	private String smtpHost;
	private String smtpPort;
	private String smtpFrom;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getSmtpHost() {
		return smtpHost;
	}

	public void setSmtpHost(String smtpHost) {
		this.smtpHost = smtpHost;
	}

	public String getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getSmtpFrom() {
		return smtpFrom;
	}

	public void setSmtpFrom(String smtpFrom) {
		this.smtpFrom = smtpFrom;
	}

	@Override
	public String toString() {
		return "MailParameters [username=" + username + ", smtpHost=" + smtpHost + ", smtpPort=" + smtpPort
				+ ", smtpFrom=" + smtpFrom + "]";
	}
}
