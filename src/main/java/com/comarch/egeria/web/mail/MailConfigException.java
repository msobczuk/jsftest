package com.comarch.egeria.web.mail;

public class MailConfigException extends Exception {

  public MailConfigException(String message, Throwable cause) {
    super(message, cause);
  }
}
