package com.comarch.egeria.web.common.utils.auth;

import org.apache.shiro.authc.UsernamePasswordToken;

public class EgrAuthToken extends UsernamePasswordToken {
  private EgrAuthMethod egrAuthMethod = EgrAuthMethod.ORACLE;

  public EgrAuthMethod getEgrAuthMethod() {
    return egrAuthMethod;
  }

  public void setEgrAuthMethod(
      EgrAuthMethod egrAuthMethod) {
    this.egrAuthMethod = egrAuthMethod;
  }

  public enum EgrAuthMethod {
    ORACLE, EGR
  }
}
