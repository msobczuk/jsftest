package com.comarch.egeria.web.common.hibernate;

import com.comarch.egeria.pp.Administrator.model.PptAdmListyTabele;
import com.comarch.egeria.pp.Administrator.model.PptAdmListyWartosci;
import com.comarch.egeria.pp.Administrator.model.PptAdmSlowniki;
import com.comarch.egeria.pp.Administrator.model.PptAdmWartosciSlownikow;
import com.comarch.egeria.pp.Bhp.model.EkSzkolenia;
import com.comarch.egeria.pp.Obiegi.model.*;
import com.comarch.egeria.pp.OcenaOkresowa.model.*;
import com.comarch.egeria.pp.OpisStanowiska.model.*;
import com.comarch.egeria.pp.Rekrutacja.model.*;
import com.comarch.egeria.pp.WniosekOkulary.model.PptWniosekOkulary;
import com.comarch.egeria.pp.WniosekOkulary.model.WniosekOkulary;
import com.comarch.egeria.pp.aktualizacjaDanych.model.PptWnioskiAktualizDanych;
import com.comarch.egeria.pp.aktualizacjaDanych.model.PptWnioskiAktualizPozycje;
import com.comarch.egeria.pp.aktualizacjaDanych.model.WniosekAktualizacjiDanych;
import com.comarch.egeria.pp.aktualizacjaDanych.model.WniosekAktualizacjiPozycja;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.EatLista;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.PptAdmSql;
import com.comarch.egeria.pp.data.model.*;
import com.comarch.egeria.pp.data.premie.model.PptWnpkPremieKwoty;
import com.comarch.egeria.pp.data.premie.model.PptWnpwPremieWniosek;
import com.comarch.egeria.pp.delegacje.model.*;
import com.comarch.egeria.pp.delegacjeStd.model.*;
import com.comarch.egeria.pp.erd.model.*;
import com.comarch.egeria.pp.iprz.model.PptIprLukiKompetencyjne;
import com.comarch.egeria.pp.iprz.model.ZptIprz;
import com.comarch.egeria.pp.kontrolaUprawnien.model.WebUprawnienia;
import com.comarch.egeria.pp.ksiazka.ustawienia.PptKsiazkaKontaktowa;
import com.comarch.egeria.pp.menu.model.PptAdmMenu;
import com.comarch.egeria.pp.menu.model.PptAdmMenuFu;
import com.comarch.egeria.pp.raporty.domain.CsstRejestrWydrukowPliki;
import com.comarch.egeria.pp.resouceFiles.model.PptResourceFilesRpl;
import com.comarch.egeria.pp.sluzbaprzygotowawcza.domain.ZptSlprBloki;
import com.comarch.egeria.pp.sluzbaprzygotowawcza.domain.ZptSluzbaPrzyg;
import com.comarch.egeria.pp.szkolenia.model.*;
import com.comarch.egeria.pp.translacja.model.PptAdmTranslacje;
import com.comarch.egeria.pp.urlopy.model.*;
import com.comarch.egeria.pp.wnioskiZaswiadczenia.model.PptWnioskiZaswiadczenia;
import com.comarch.egeria.pp.zfss.model.*;
import com.comarch.egeria.pp.zrodlaFinansowania.model.Angaze;
import com.comarch.egeria.pp.zrodlaFinansowania.model.ZrodlaFinansowania;
import com.comarch.egeria.web.common.utils.constants.Const;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Metamodel;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.internal.SessionImpl;
import org.hibernate.jdbc.Work;

import javax.persistence.Query;
import javax.persistence.metamodel.Type;
import java.io.Closeable;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

//import com.comarch.egeria.pp.Administrator.model.PpvListyWartosci;
//import com.comarch.egeria.pp.Rekrutacja.model.ZptNabWn5;
//import com.comarch.egeria.pp.testy.model.PptCrud;
//import com.comarch.egeria.web.common.session.EgrUserSessionBean;

//import model.PpMenu;

public class HibernateContext implements Closeable {

	private static final Logger log = LogManager.getLogger();

	private static Configuration configuration = null;
	private static SessionFactory sessionFactory;
	private Session session;
	private int jdbcBatchSize = 35;
	private final int jdbcFetchSize = 1000;

	public HibernateContext() {
		if (configuration == null || sessionFactory == null) {
			
			EgrHibernateInterceptor interceptor = new EgrHibernateInterceptor();
			
			
			configuration = new Configuration()
					.setInterceptor(interceptor)

					.addAnnotatedClass(DbObject.class)
					.addAnnotatedClass(DbObjSlownik.class)
					.addAnnotatedClass(DbObjListaWartosci.class)
					.addAnnotatedClass(DbObjView.class)
					.addAnnotatedClass(DbObjProcedure.class)
					.addAnnotatedClass(DbObjFunction.class)
					.addAnnotatedClass(DbObjPackage.class)
					.addAnnotatedClass(DbObjPackageBody.class)
					.addAnnotatedClass(DbObjTrigger.class)
					.addAnnotatedClass(PptObject.class)
					.addAnnotatedClass(PptAllSource.class)


					.addAnnotatedClass(PptResourceFilesRpl.class)

					// .addAnnotatedClass(PpMenu.class) //dodac klasy!
					.addAnnotatedClass(PptAdmSql.class)
					// .addAnnotatedClass(PpKsiazkaKontaktowa.class)
					.addAnnotatedClass(PptKsiazkaKontaktowa.class)

					.addAnnotatedClass(PptWspPolaDefiniowalne.class)

					// .addAnnotatedClass(PpUrlopyPlanowane.class)
					// .addAnnotatedClass(PpUrlopyHarmonogramyRoczne.class)
					.addAnnotatedClass(PptUrlopyPlanowane.class).addAnnotatedClass(PptUrlopyPlanowaneDni.class)

					.addAnnotatedClass(PptUrlopyWnioskowane.class)

					// Administrator
					// .addAnnotatedClass(PpvListyWartosci.class)
					.addAnnotatedClass(PptAdmListyWartosci.class)
					.addAnnotatedClass(PptAdmListyTabele.class)

					.addAnnotatedClass(PptAdmSlowniki.class).addAnnotatedClass(PptAdmWartosciSlownikow.class)
					// .addAnnotatedClass(CssSlowniki.class)
					// .addAnnotatedClass(CssWartosciSlownikow.class)
					// .addAnnotatedClass(CssWartosciSlownikowPK.class)

					// Translacja
					.addAnnotatedClass(PptAdmTranslacje.class)

					// Szkolenia
					.addAnnotatedClass(OstUmowy.class).addAnnotatedClass(OstListySzkolen.class)
					.addAnnotatedClass(PptSzkAnkietyPozycje.class).addAnnotatedClass(OstSzkolenia.class)
					.addAnnotatedClass(OstPlanIndyw.class).addAnnotatedClass(PptSzkWnSkierowanieStudia.class)
					.addAnnotatedClass(OstZapisySzkolen.class).addAnnotatedClass(PptSzkSzkolenia.class)
					.addAnnotatedClass(PptOfertySzkolen.class).addAnnotatedClass(PptKosztySzkolen.class)
					.addAnnotatedClass(PptUczestnicySzkolen.class).addAnnotatedClass(PptSzkWykladowcy.class)
					.addAnnotatedClass(PptSzkWykladowcySzkolenie.class)
					.addAnnotatedClass(PptSzkBudzet.class)
					.addAnnotatedClass(PptSzkBudzetPozycje.class)
					.addAnnotatedClass(PptSzkWnRefundacje.class)
					

					// Opisy stanowisk
					.addAnnotatedClass(PptOpisStanowiska.class).addAnnotatedClass(PptOpsGlZadania.class)
					.addAnnotatedClass(PptOpsNwDane.class).addAnnotatedClass(PptOpsObOdpow.class).addAnnotatedClass(PptOpsPrc.class)
					.addAnnotatedClass(PptOpsWsDane.class)
					.addAnnotatedClass(PptOpsNwWymogi.class)

					// Urlopy
					.addAnnotatedClass(EkAbsencjeDane.class).addAnnotatedClass(EkZwolnienia.class)

					// Premie
					.addAnnotatedClass(PptWnpwPremieWniosek.class).addAnnotatedClass(PptWnpkPremieKwoty.class)

					// TW_ZZZ
					// //Rozwoj
					// .addAnnotatedClass(ZptKorektaOceny.class)
					// //Wniosek o rekrutacje
					// .addAnnotatedClass(ZptProcesRekrutacji.class)

					// Generator Raportow
					.addAnnotatedClass(CsstRejestrWydrukowPliki.class)

					// Ocena okresowa
					.addAnnotatedClass(ZptOceny.class).addAnnotatedClass(ZptOcKryteria.class)

					// Ocena okresowa RCGW
					.addAnnotatedClass(PptOceny.class)
					.addAnnotatedClass(Ocena.class)

					.addAnnotatedClass(PptOcenyWartosci.class)
					.addAnnotatedClass(OcenaWartosc.class)


					// Sluzba Przygotowawcza
					.addAnnotatedClass(ZptSluzbaPrzyg.class).addAnnotatedClass(ZptSlprBloki.class)

					// IPRZ
					.addAnnotatedClass(ZptIprz.class)
					.addAnnotatedClass(PptIprLukiKompetencyjne.class)

					.addAnnotatedClass(EatLista.class)

					// Rekrutacja
					.addAnnotatedClass(ZptNabory.class)
					.addAnnotatedClass(ZptNaborKomisja.class)
					.addAnnotatedClass(ZptKandydaci.class)
					.addAnnotatedClass(ZptNaborSklad.class)
					// .addAnnotatedClass(ZptNabWn5.class)
					.addAnnotatedClass(ZptNaborZespoly.class)
					.addAnnotatedClass(ZptNabSymb.class)
					.addAnnotatedClass(PptRekAdresy.class)
					.addAnnotatedClass(PptUdostepnieniaDanych.class)
					.addAnnotatedClass(PptRekProjekty.class)
					.addAnnotatedClass(PptRekRezerwyCelowe.class)
					.addAnnotatedClass(PptRekEtapy.class)
					.addAnnotatedClass(PptRekKandydatWamagania.class)

					// test dziedziczenia
					// .addAnnotatedClass(ZptNaboryLB.class)

					// Dane adresowe
					.addAnnotatedClass(CkkAdresy.class)

					// BHP
					.addAnnotatedClass(PptWniosekOkulary.class)
					.addAnnotatedClass(WniosekOkulary.class)

					// Delegacje MF
					.addAnnotatedClass(DeltWnioskiDelegacji.class)
					.addAnnotatedClass(DeltCeleDelegacji.class)
					.addAnnotatedClass(DeltTrasy.class)
					.addAnnotatedClass(DeltSrodkiLokomocji.class)
					.addAnnotatedClass(DeltZapraszajacy.class)
					.addAnnotatedClass(DeltProjekty.class)
					.addAnnotatedClass(DeltKalkulacje.class)
					.addAnnotatedClass(DeltPozycjeKalkulacji.class)
//					.addAnnotatedClass(PozycjaKalkulacjiMF.class)
                    .addAnnotatedClass(DeltPklWyplNz.class)
					.addAnnotatedClass(DeltZrodlaFinansowania.class)
					.addAnnotatedClass(DelCzynnosciSluzbowe.class)
					.addAnnotatedClass(WniosekDelegacjiZagr.class)
					.addAnnotatedClass(WniosekDelegacjiKraj.class)
					
					
					// Delegacje STD
					.addAnnotatedClass(Delegacja.class)
					.addAnnotatedClass(DelegacjaKraj.class)
					.addAnnotatedClass(DelegacjaZagr.class)
					.addAnnotatedClass(Kalkulacja.class)
					.addAnnotatedClass(KalkulacjaRozlicz.class)
					.addAnnotatedClass(KalkulacjaWniosek.class)
					.addAnnotatedClass(KalkulacjaWstepna.class)
					.addAnnotatedClass(PozycjaKalkulacji.class)
					.addAnnotatedClass(PptDelCeleDelegacji.class)
					.addAnnotatedClass(PptDelCzynnosciSluzbowe.class)
					.addAnnotatedClass(PptDelKalkulacje.class)
					.addAnnotatedClass(PptDelOsobyDodatkowe.class)
					.addAnnotatedClass(PptDelPozycjeKalkulacji.class)
					.addAnnotatedClass(PptDelProjekty.class)
					.addAnnotatedClass(PptDelSrodkiLokomocji.class)
					.addAnnotatedClass(PptDelTrasy.class)
					.addAnnotatedClass(PptDelWnioskiDelegacji.class)
					.addAnnotatedClass(PptDelZaliczki.class)
					.addAnnotatedClass(PptDelZapraszajacy.class)
					.addAnnotatedClass(PptDelZrodlaFinansowania.class)

					
					
					
					

					// KontrolaUprawnien
					.addAnnotatedClass(WebUprawnienia.class)

					.addAnnotatedClass(DelStawkiDelegacji.class).addAnnotatedClass(DelTypyPozycji.class)
					.addAnnotatedClass(DelWartosciStawek.class)

					.addAnnotatedClass(PptAdmMenu.class).addAnnotatedClass(PptAdmMenuFu.class)

					.addAnnotatedClass(DelZaliczki.class).addAnnotatedClass(DelAngaze.class)

					.addAnnotatedClass(Angaze.class).addAnnotatedClass(ZrodlaFinansowania.class)

//					.addAnnotatedClass(PptCrud.class)

					// OBIEGI
					.addAnnotatedClass(Obieg.class).addAnnotatedClass(Stan.class).addAnnotatedClass(StanDefinicja.class)
					.addAnnotatedClass(Czynnosc.class).addAnnotatedClass(HistoriaObiegow.class).addAnnotatedClass(EncjaWStanie.class)
					.addAnnotatedClass(KategoriaObiegu.class).addAnnotatedClass(TypEncji.class)
					
					// BHP
					.addAnnotatedClass(EkSzkolenia.class)
					
					
					// WnioskiAktualizacjaDanych
					.addAnnotatedClass(PptWnioskiAktualizDanych.class)
					.addAnnotatedClass(WniosekAktualizacjiDanych.class)
					.addAnnotatedClass(PptWnioskiAktualizPozycje.class)
					.addAnnotatedClass(WniosekAktualizacjiPozycja.class)
					
					// WnioskiAktualizacjaDanych
					.addAnnotatedClass(PptWnioskiZaswiadczenia.class)
					
					// ZFSS
					.addAnnotatedClass(PptZfssWniosek.class)
					.addAnnotatedClass(PptZfssRodzina.class)
					.addAnnotatedClass(PptZfssPoreczyciele.class)
					.addAnnotatedClass(PptZfssBilety.class)
					.addAnnotatedClass(OswiadczenieZFSS.class)
					.addAnnotatedClass(WniosekZFSSPozyczka.class)
					.addAnnotatedClass(WniosekZFSSWypoczynekZimowoWiosenny.class)
					.addAnnotatedClass(WniosekZFSSBezzwrotnaPomoc.class)
					.addAnnotatedClass(WniosekZFSSMultiSport.class)
					.addAnnotatedClass(WniosekZFSSBiletyKarnety.class)
					.addAnnotatedClass(WniosekZFSSUniwersalny.class)
					.addAnnotatedClass(WniosekZFSSWypoczynek.class)
					
					
					
					
					
					
					
					// Indeks i ERD
					.addAnnotatedClass(PptTable.class)
					.addAnnotatedClass(PptTabCol.class)
					.addAnnotatedClass(PptErd.class)
					.addAnnotatedClass(PptErdElem.class)
					.addAnnotatedClass(Tab.class)
					.addAnnotatedClass(TabCol.class)
					.addAnnotatedClass(Erd.class)
					.addAnnotatedClass(ErdElem.class)

					.addAnnotatedClass(PptDataSource.class)
					.addAnnotatedClass(DataSourceOracle.class)
					.addAnnotatedClass(DataSourcePostgres.class)


//					.setProperty("hibernate.dialect", "org.hibernate.dialect.OracleDialect")
					//https://www.javatpoint.com/dialects-in-hibernate
					.setProperty("hibernate.dialect", AppBean.app.getDataSource().getHibernateDialect())

					.setProperty("hibernate.connection.datasource", Const.JNDI_CONNECTION_NAME)
					.setProperty("hibernate.jdbc.batch_size", "" + this.jdbcBatchSize)
					.setProperty("hibernate.jdbc.fetch_size ", "" + this.jdbcFetchSize)
					.setProperty("hibernate.order_updates", "true")
					.setProperty("hibernate.order_inserts", "true")
					.setProperty("hibernate.jdbc.batch_versioned_data", "true")
					.setProperty("hibernate.show_sql", "false")

//???
//			.setProperty("hibernate.connection.CharSet", "utf8")
//			.setProperty("hibernate.connection.characterEncoding", "utf8")
//			.setProperty("hibernate.connection.useUnicode", "true")
			;

			sessionFactory = configuration.buildSessionFactory();
		}

		if (sessionFactory==null){
			System.out.println("sessionFactory is null!!!!");
		}
			
		session = sessionFactory.openSession();
		session.doWork(new Work() {
			@Override
			public void execute(Connection connection) throws SQLException {
				JdbcUtils.addActiveConnInfo(connection);
				try {
					JdbcUtils.setContext(connection);	
				} catch (Exception e) {
					session.close();
					if (e.getMessage().contains("Sesja zakończona przez Administratora"))
						log.debug(e.getMessage());
					else
						log.error(e.getMessage(), e);
					throw e;
				}
			}
			
		});
	}

	@Override
	public void close() {
		// System.out.println("Hibernate / otwarto sesję i połączenie <- ... ");
		// StackTraceElement[] ste = Thread.currentThread().getStackTrace();
		// for (int i=0; i<10; i++){
		// if (ste.length < (i-1)) break;
		// System.out.println(" <- " + ste[i].getClassName() + "." + ste[i].getMethodName() );
		// }
		session.close();

	}

	public static List query(String hql) {
		try (HibernateContext h = new HibernateContext()) {
			return query(h, hql, null);
		}
	}

	
	public static List query(String hql,  Object... args) {
		try (HibernateContext h = new HibernateContext()) {
			return query(h, hql, args);
		}
	}
	
	
	
	public static List query(HibernateContext h, String hql,  Object... args) {
//		String methodname = "HibernateContext.query / hql: " + hql + " / " + args;
//		long t0 = AppBean.logMethod(methodname);
		List ret = null;
		Session s = h.getSession();
		Query q = s.createQuery(hql);
		
		if (args!=null){
			for (int i = 0; i < args.length;  i++){
				q.setParameter(i, args[i]);
			}
		}
		
		ret = q.getResultList();
//		AppBean.logMethod(methodname, t0);
		return ret;
	}
	
	
	public static Object save(Object entity) {
		return HibernateContext.save(entity, true);
	}

	public static Object save(Object entity, boolean refresh) {
		String methodname = "HibernateContext.save / "+entity.getClass().getSimpleName() + ": " + entity + " / refresh: " + refresh;
		long t0 = AppBean.logMethod(methodname);
		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			entity = s.merge(entity);
			s.saveOrUpdate(entity);
			s.beginTransaction().commit();
			if (refresh)
				s.refresh(entity);// zmiany z triggerów
		}
		AppBean.logMethod(methodname, t0);
		return entity;
	}

	
	public static Object refresh(Object entity) {
		if (entity==null)
			return null;

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
//			entity = s.merge(entity);
			s.saveOrUpdate(entity);//UWAGA - saveOrUpdtae jest uzyty aby przejsc do stanu persistent. Jesli zrobimy merge, lub nic nie zrobimy to są kłopoty z odswiezaniem zagniezdzonych encji;
			//UWAGA: celowo też bez transakcji i commit'a (te zmiany nie są zapisywane do bazy, chyba ze dodamy jeszcze beginTransaction().commit();)
			s.refresh(entity);// zmiany z triggerów
		}

		return entity;
	}

	public static Object refresh(HibernateContext h, Object entity) {
		if (entity==null)
			return null;

		Session s = h.getSession();
		s.refresh(entity);

		return entity;
	}
	
	
	
	public static void saveOrUpdate(Object entity) {
		saveOrUpdate(entity, true);
	}
	
	public static void saveOrUpdate(Object entity, boolean refresh) {
		try (HibernateContext h = new HibernateContext()) {
			saveOrUpdate(h, entity, refresh);	
		} 
	}
	
	public static void saveOrUpdate(HibernateContext h, Object entity, boolean refresh) {
		Session s = h.getSession(); 
		s.saveOrUpdate(entity);
		s.beginTransaction().commit();
		if (refresh)
			s.refresh(entity); // zmiany z triggerów
	}


	public static void saveOrUpdate(HibernateContext h, Transaction trx, Object entity, boolean refresh) {
		Session s = h.getSession();
		s.saveOrUpdate(entity);
//		s.beginTransaction().commit();
		if (refresh)
			s.refresh(entity); // zmiany z triggerów
	}


	public static void delete(Object entity) {
		try (HibernateContext h = new HibernateContext()) {
			delete(h, entity);
		}
	}
	
	
	public static void delete(HibernateContext h, Object entity) {
		Session s = h.getSession();
		entity = s.merge(entity);
		s.delete(entity);
		s.beginTransaction().commit();
	}



	public static boolean isDirty(Object entity) {
		if (entity==null) return false;

		boolean ret = false;
		try (HibernateContext h = new HibernateContext()) {
			ret  = isDirty(h, entity);
		}
		return ret;
	}

	public static boolean isDirty(HibernateContext h, Object entity) {
		if (entity == null) return false;

		boolean ret = false;
		Session s = h.getSession();
		entity = s.merge(entity);
		ret = s.isDirty();
		return ret;
	}


	
	public static Object get(Class c, Serializable id) {
		if (id==null) return null;
//		return get(c, Long.parseLong(""+id));
		try (HibernateContext h = new HibernateContext()) {

			Metamodel metamodel = h.getSessionFactory().getMetamodel();
			Type idType = metamodel.entity(c).getIdType();
			Class javaType = idType.getJavaType();

			if (!id.getClass().equals(javaType) && (long.class.equals(javaType) || Long.class.equals(javaType))){
				long lngId = Long.parseLong("" + id);
				return h.getSession().get(c, lngId);
			}

			return h.getSession().get(c, id);
		}
	}
	
	public static Object get(Class c, Long id) {
		try (HibernateContext h = new HibernateContext()) {
			return get (h, c, id);
		}
	}
	
	public static Object get(HibernateContext h, Class c, Long id) {
		return h.getSession().get(c, id);
	}

	public Configuration getConfiguration() {
		return configuration;
	}

	public void setConfiguration(Configuration configuration) {
		HibernateContext.configuration = configuration;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		HibernateContext.sessionFactory = sessionFactory;
	}

	public Session getSession() {
		return session;
	}

	public Connection getConnection(){
		return ((SessionImpl)getSession()).connection();
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public int getJdbcBatchSize() {
		return jdbcBatchSize;
	}

	public void setJdbcBatchSize(int jdbcBatchSize) {
		this.jdbcBatchSize = jdbcBatchSize;
	}

}
