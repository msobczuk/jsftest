package com.comarch.egeria.web.common.utils.reports;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import javax.annotation.Resource;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.web.common.utils.constants.Const;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;

/**
 * Klasa do obsługi generacji raportów.
 * 
 * @see com.comarch.egeria.ctl.common.utils.reports.bo.CtlReportsUtils#Dokumentacja
 */

@Named
public class CtlReportsUtils {

//	zzzzzzz@Resource(mappedName = Const.zzzzzzzzzJNDI_CONNECTION_NAME)
//	zzzzzprivate DataSource dataSourcezzzzzzzzz;

	/**
	 * Generuje raport i wyświetla go w nowym oknie przegldarki w postaci pliku
	 * PDF.
	 * 
	 * @param repName
	 *            Nazwa raportu, który ma zostać wygenerowany.
	 * @param parameters
	 *            Parametry raportu w postaci Mapy - <'NAZWA_PARAMETRU', Obiekt>
	 * 
	 * @throws SQLException,
	 *             JRException, IOException
	 */
	public void displayReportAsPdf(String repName, Map<String, Object> parameters)
			throws SQLException, JRException, IOException {

		String reportSrcFile = getRealReportSrcPath(repName);

		try (Connection conn = JdbcUtils.getConnection()) {
			// First, compile jrxml file.
			JasperReport jasperReport = JasperCompileManager.compileReport(reportSrcFile);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);

			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.setHeader("Content-Type", "application/pdf");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".pdf" + "\"");
			ServletOutputStream stream = response.getOutputStream();

			// JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
			exporter.exportReport();

			FacesContext.getCurrentInstance().responseComplete();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void displayReportAsPdf2(String repName, Map<String, Object> parameters)
			throws SQLException, JRException, IOException {

		String reportSrcFile = getRealReportSrcPath(repName);

		try (Connection conn = JdbcUtils.getConnection()) {
			// First, compile jrxml file.
			JasperReport jasperReport = JasperCompileManager.compileReport(reportSrcFile);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);

			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.setHeader("Content-Type", "application/pdf");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".pdf" + "\"");
			ServletOutputStream stream = response.getOutputStream();

			// JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
			exporter.exportReport();

			FacesContext.getCurrentInstance().responseComplete();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generuje raport i wyświetla go w nowym oknie przegldarki w postaci pliku
	 * PDF.
	 * 
	 * @param repName
	 *            Nazwa raportu, który ma zostać wygenerowany.
	 * @param parameters
	 *            Parametry raportu w postaci Mapy - <'NAZWA_PARAMETRU', Obiekt>
	 * 
	 * @throws SQLException,
	 *             JRException, IOException
	 */
	public void displayReportAsXlsx(String repName, Map<String, Object> parameters)
			throws SQLException, JRException, IOException {

		String reportSrcFile = getRealReportSrcPath(repName);

		try (Connection conn = JdbcUtils.getConnection()) {
			// First, compile jrxml file.
			JasperReport jasperReport = JasperCompileManager.compileReport(reportSrcFile);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);

			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".xlsx" + "\"");
			ServletOutputStream stream = response.getOutputStream();

			JRXlsxExporter exporter = new JRXlsxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
			exporter.exportReport();
			// JasperExportManager.exportReportToPdfStream(jasperPrint, stream);

			FacesContext.getCurrentInstance().responseComplete();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generuje raport i wyświetla go w nowym oknie przegldarki w postaci pliku
	 * PDF.
	 * 
	 * @param repName
	 *            Nazwa raportu, który ma zostać wygenerowany.
	 * @param parameters
	 *            Parametry raportu w postaci Mapy - <'NAZWA_PARAMETRU', Obiekt>
	 * 
	 * @throws SQLException,
	 *             JRException, IOException
	 */
	public void displayReportAsHtml(String repName, Map<String, Object> parameters)
			throws SQLException, JRException, IOException {

		String reportSrcFile = getRealReportSrcPath(repName);

		try (Connection conn = JdbcUtils.getConnection()) {
			// First, compile jrxml file.
			JasperReport jasperReport = JasperCompileManager.compileReport(reportSrcFile);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);

			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.setHeader("Content-Type", "	text/html");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".html" + "\"");
			ServletOutputStream stream = response.getOutputStream();

			HtmlExporter exporter = new HtmlExporter();

			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));

			exporter.setExporterOutput(new SimpleHtmlExporterOutput(stream));
			exporter.exportReport();
			// JasperExportManager.exportReportToPdfStream(jasperPrint, stream);

			FacesContext.getCurrentInstance().responseComplete();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Generuje raport i wyświetla go w nowym oknie przegldarki w postaci pliku
	 * PDF.
	 * 
	 * @param repName
	 *            Nazwa raportu, który ma zostać wygenerowany.
	 * @param parameters
	 *            Parametry raportu w postaci Mapy - <'NAZWA_PARAMETRU', Obiekt>
	 * 
	 * @throws SQLException,
	 *             JRException, IOException
	 */
	public void displayReportAsRtf(String repName, Map<String, Object> parameters)
			throws SQLException, JRException, IOException {

		String reportSrcFile = getRealReportSrcPath(repName);

		try (Connection conn = JdbcUtils.getConnection()) {
			// First, compile jrxml file.
			JasperReport jasperReport = JasperCompileManager.compileReport(reportSrcFile);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);

			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.setHeader("Content-Type", "application/rtf");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".rtf" + "\"");
			ServletOutputStream stream = response.getOutputStream();

			JRRtfExporter exporter = new JRRtfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(stream));
			exporter.exportReport();
			// JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
			// new SimpleOutputStreamExporterOutput(stream)
			FacesContext.getCurrentInstance().responseComplete();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void displayReportAsDocx(String repName, Map<String, Object> parameters)
			throws SQLException, JRException, IOException {

		String reportSrcFile = getRealReportSrcPath(repName);

		try (Connection conn = JdbcUtils.getConnection()) {
			// First, compile jrxml file.
			JasperReport jasperReport = JasperCompileManager.compileReport(reportSrcFile);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);

			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.setHeader("Content-Type",
					"application/vnd.openxmlformats-officedocument.wordprocessingml.document");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".docx" + "\"");
			ServletOutputStream stream = response.getOutputStream();

			JRDocxExporter exporter = new JRDocxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
			exporter.exportReport();
			// JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
			// new SimpleOutputStreamExporterOutput(stream)
			FacesContext.getCurrentInstance().responseComplete();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void displayReportAsCsv(String repName, Map<String, Object> parameters)
			throws SQLException, JRException, IOException {

		String reportSrcFile = getRealReportSrcPath(repName);

		try (Connection conn = JdbcUtils.getConnection()) {
			// First, compile jrxml file.
			JasperReport jasperReport = JasperCompileManager.compileReport(reportSrcFile);

			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, conn);

			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
					.getResponse();
			response.setHeader("Content-Type", "text/csv");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".csv" + "\"");
			ServletOutputStream stream = response.getOutputStream();

			JRCsvExporter exporter = new JRCsvExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(stream));
			exporter.exportReport();
			// JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
			// new SimpleOutputStreamExporterOutput(stream)
			FacesContext.getCurrentInstance().responseComplete();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private String getRealReportSrcPath(String repName) {
		return FacesContext.getCurrentInstance().getExternalContext().getRealPath("/WEB-INF/protectedElements/Reports/" + repName + ".jrxml");
	}
	/*
	 * Ta funkcja generuje pdf po stronie klienta. Może sie przyda dlatego
	 * zostawiam. Jakub Szczech.
	 */
	/*
	 * public void generatePDF() throws SQLException, JRException {
	 * 
	 * //String reportSrcFile =
	 * "C:\\JRs\\MiesiecznyPlanKosztowDlaCentrow.jrxml"; String reportSrcFile =
	 * "/reports/MiesiecznyPlanKosztowDlaCentrow.jrxml"; JasperReport
	 * jasperReport = JasperCompileManager.compileReport(reportSrcFile);
	 * 
	 * // Connection conn = CtlConnectionUtils.getConnection(); Connection conn
	 * = dataSource.getConnection();
	 * 
	 * // Parameters for report Map<String, Object> parameters = new
	 * HashMap<String, Object>();
	 * 
	 * JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
	 * parameters, conn);
	 * 
	 * // Make sure the output directory exists. File outDir = new
	 * File("C:/jasperoutput"); outDir.mkdirs();
	 * 
	 * // PDF Exportor. JRPdfExporter exporter = new JRPdfExporter();
	 * 
	 * ExporterInput exporterInput = new SimpleExporterInput(jasperPrint); //
	 * ExporterInput exporter.setExporterInput(exporterInput);
	 * 
	 * // ExporterOutput OutputStreamExporterOutput exporterOutput = new
	 * SimpleOutputStreamExporterOutput(
	 * "C:/jasperoutput/MiesiecznyPlanKosztowDlaCentrow.pdf"); // Output
	 * exporter.setExporterOutput(exporterOutput);
	 * 
	 * // SimplePdfExporterConfiguration configuration = new
	 * SimplePdfExporterConfiguration();
	 * exporter.setConfiguration(configuration); exporter.exportReport();
	 * 
	 * }
	 */
}
