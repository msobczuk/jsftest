package com.comarch.egeria.web.common.utils.auth.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the PPT_HASLO_RESET_TOKEN database table.
 */
@Entity
@Table(name = "PPT_HASLO_RESET_TOKEN", schema = "PPADM")
@NamedQuery(name = "PasswordResetToken.findAll", query = "SELECT z FROM PasswordResetToken z")
public class PasswordResetToken {

  @Id
  @Column(name = "HT_TOKEN")
  private String token;

  @Column(name = "HT_UZT_NAZWA")
  private String username;

  @Column(name = "HT_CZAS_WYGASNIECIA")
  private Date expiryDate;

  @Column(name = "HT_CZY_WYGASLE")
  private String expired;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Date getExpiryDate() {
    return expiryDate;
  }

  public void setExpiryDate(Date expiryDate) {
    this.expiryDate = expiryDate;
  }

  public String getExpired() {
    return expired;
  }

  public void setExpired(String expired) {
    this.expired = expired;
  }
}
