package com.comarch.egeria.web.common.utils.constants;

public class CtlUtils {

	/**
	 * Metoda zwraca kwotę zlecenia na formatkę Zlecenia z odpowiednim znakiem
	 * zależącym od Rodzaju i Typu zlecenia
	 * 
	 * @return - Kwota z odpowiednim znakiem +/-
	 * 
	 */
	public static Double kwotaZlecZZnakiem(Double rwKwota, String rwRodzaj, String rwTyp) {
		Double retValue = rwKwota;

		if (retValue != 0) {
			if (rwTyp.equals(Const.ZCLEC_NR)) {

				if (rwRodzaj.equals(Const.WLASNE_W_TOKU) || rwRodzaj.equals(Const.WLASNE_ODEBRANE)) {
					retValue = -retValue;
				}
			} else if (rwTyp.equals(Const.ZCLEC_PK)) {

				if (rwRodzaj.equals(Const.OBCE_W_TOKU) || rwRodzaj.equals(Const.OBCE_ZAKONCZONE)) {
					retValue = -retValue;
				}
			}
		}
		  
		return retValue;
	}
}
