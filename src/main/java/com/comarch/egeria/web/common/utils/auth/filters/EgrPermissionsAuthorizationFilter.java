package com.comarch.egeria.web.common.utils.auth.filters;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.PermissionsAuthorizationFilter;

import com.comarch.egeria.web.common.utils.constants.Const;
import org.pac4j.cas.credentials.extractor.TicketAndLogoutRequestExtractor;
import org.pac4j.core.client.IndirectClient;
import org.pac4j.core.context.WebContext;
import org.pac4j.core.logout.handler.LogoutHandler;

public class EgrPermissionsAuthorizationFilter extends PermissionsAuthorizationFilter {

	/*
	 * Prośba o wpisywanie reguł w podobnej kolejności jak występują w menu. Na
	 * przyszłośc będzie sie lepeiej szukać.
	 *
	 */

	@Override
	public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
		Subject subject = getSubject(request, response);
		// String[] perms = (String[]) mappedValue;
		WebContext web;
		LogoutHandler a;
		TicketAndLogoutRequestExtractor b;
		String url = ((HttpServletRequest) request).getRequestURL().toString();
		// LOG.debug(url);
		IndirectClient indirectClient;
//		org.keycloak
		boolean isPermitted = true;

		if (url.endsWith(Const.HOME_REDIRECT_URL + "/Administracja/Uprawnienia")
				&& !(subject.isPermitted("MENU_UPR"))) {
			isPermitted = false;
		}

		if (url.endsWith(Const.HOME_REDIRECT_URL + "/uprawnienia.xhtml") && !(subject.isPermitted("MENU_UPR2"))) {
			isPermitted = false;
		}

		if (url.endsWith(Const.HOME_REDIRECT_URL + "/Administracja/Wizard") && !(subject.isPermitted("MENU_WIZ"))) {
			isPermitted = false;
		}

		return isPermitted;
	}

}

// if (perms != null && perms.length > 0) {
// if (perms.length == 1) {
// if (!subject.isPermitted(perms[0])) {
// isPermitted = false;
// }
// } else {
// if (!subject.isPermittedAll(perms)) {
// isPermitted = false;
// }
// }
// }
