package com.comarch.egeria.web.common.help.bo;

import org.primefaces.model.file.UploadedFile;

import com.comarch.egeria.web.common.help.exceptions.HelpAccessException;
import com.comarch.egeria.web.common.help.exceptions.HelpConversionException;
import com.comarch.egeria.web.common.help.exceptions.HelpIOException;
import com.comarch.egeria.web.common.help.exceptions.HelpNotFoundException;
import com.comarch.egeria.web.common.help.exceptions.HelpPersistenceException;

public interface HelpBo {

	String saveFile(UploadedFile file, String modul, String region)
			throws HelpPersistenceException, HelpConversionException, HelpIOException;

	String getHtmlFilename(String modul, String region)
			throws HelpNotFoundException, HelpAccessException, HelpConversionException, HelpIOException;

}
