package com.comarch.egeria.web.common.utils.auth.utils;

public class CtLoginData {
	private String username;
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "CtLoginData [username=" + username + ", password=" + password + "]";
	}

}
