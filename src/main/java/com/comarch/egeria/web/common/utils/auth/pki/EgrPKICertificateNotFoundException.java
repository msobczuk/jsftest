package com.comarch.egeria.web.common.utils.auth.pki;

import java.security.cert.CertificateException;

public class EgrPKICertificateNotFoundException extends CertificateException {

	private static final long serialVersionUID = 1L;

	public EgrPKICertificateNotFoundException() {
		this("No matching certificates found");
	}

	public EgrPKICertificateNotFoundException(String message) {
		super(message);
	}
}
