package com.comarch.egeria.web.common.utils.auth.pki;

import java.security.cert.CertificateException;

public class EgrPKICertificateWrongIssuerException extends CertificateException {

	private static final long serialVersionUID = 1L;

	public EgrPKICertificateWrongIssuerException() {
		this("Wrong Certyficate Issuer");
	}

	public EgrPKICertificateWrongIssuerException(String message) {
		super(message);
	}
}
