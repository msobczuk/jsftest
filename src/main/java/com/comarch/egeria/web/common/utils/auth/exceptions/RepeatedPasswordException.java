package com.comarch.egeria.web.common.utils.auth.exceptions;

public class RepeatedPasswordException extends PasswordValidationException {

  public RepeatedPasswordException() {
    super();
  }

}
