package com.comarch.egeria.web.common.help;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.help.bo.HelpBo;
import com.comarch.egeria.web.common.help.exceptions.HelpAccessException;
import com.comarch.egeria.web.common.help.exceptions.HelpConversionException;
import com.comarch.egeria.web.common.help.exceptions.HelpIOException;
import com.comarch.egeria.web.common.help.exceptions.HelpNotFoundException;
import com.comarch.egeria.web.common.help.exceptions.HelpPersistenceException;

@Named
@Scope("view")
public class HelpBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(HelpBean.class);
	
	private String src;
	private String modul;
	private String region;
	private boolean permittedToModify;
	private String komunikat;

	@Inject
	private HelpBo helpBo;
	
	@Inject
	private User user;

	@Inject
	SessionBean sessionBean;

	@PostConstruct
	private void init() {
		permittedToModify = user.hasPermission("PP_MODYFIKACJA_POMOCY");
	}

	private void initModul() {
		log.debug("Ustalanie nazwy modułu");
		
		// modul = FacesContext.getCurrentInstance().getViewRoot().getViewId();
		modul = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("referer");
		int i = modul.lastIndexOf("/");
		
		if (modul.endsWith(".xhtml")) {
			modul = modul.substring(i + 1, modul.length() - 6);
		} else {
			modul = modul.substring(i + 1);
		}

		if (modul==null || modul.isEmpty())
			modul = "PP";

		if (modul.equals("SzablonLOV")){
			String lwshname = sessionBean.getMenuLWValue();
			if (lwshname != null)
				modul = lwshname;
		}

		if (modul.length() > 30)
			modul = modul.substring(0, 30);

		log.debug("Nazwa modułu: {}", modul);
	}

	@SuppressWarnings("unused")
	private String toUpperUnderscore(String str) {
		StringBuilder sb = new StringBuilder(str);

		for (int i = 1; i < sb.length(); i++)
			if (Character.isUpperCase(sb.charAt(i)))
				sb = sb.insert(i++, '_');

		return sb.toString().toUpperCase();
	}

	public void setContext() {
		log.debug("Ustawianie kontekstu pomocy");
		
		try {
			modul = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("modul");
			region = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("region");

			if (modul == null || modul.equals(""))
				initModul();
			
			log.debug("Kontekst pomocy [moduł=}{}, obszar={}]", modul, region == null ? "brak" : region);
			
			src = helpBo.getHtmlFilename(modul, region);
		} catch (HelpNotFoundException e) {
			src = null;
			komunikat = "BRAK POMOCY";
			log.debug("Brak karty pomocy");
		} catch (HelpAccessException | HelpConversionException | HelpIOException e) {
			e.printStackTrace();
			src = null;
			komunikat = "BŁĄD";
			log.error(e.getMessage(), e);
		}
	}

	public void uploadFile(FileUploadEvent event) {
		log.debug("Upload nowego pliku DOC(X) karty pomocy");
		
		try {
			UploadedFile uploadedFile = event.getFile();
			src = helpBo.saveFile(uploadedFile, modul, region);
		} catch (HelpPersistenceException | HelpConversionException | HelpIOException e) {
			src = null;
			komunikat = "BŁĄD";
			log.error(e.getMessage(), e);
		}
	}

	public String getModul() {
		return modul;
	}

	public void setModul(String modul) {
		this.modul = modul;
	}

	public String getRegion() {
		return region == null || region.equals("") ? "-" : region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public boolean isNoHelp() {
		return src == null || src.equals("");
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public boolean isPermittedToModify() {
		return permittedToModify;
	}

	public String getKomunikat() {
		return komunikat;
	}

	public void setKomunikat(String komunikat) {
		this.komunikat = komunikat;
	}
}