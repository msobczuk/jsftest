package com.comarch.egeria.web.common.utils.auth.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CtlAuthUtils {

	public static String encodeMD5(String password) {
		String encPass = null;
		MessageDigest md;

		try {
			md = MessageDigest.getInstance("MD5");
			md.update(password.getBytes());
			byte byteData[] = md.digest();

			// convert the byte to hex forma
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}

			encPass = sb.toString().toUpperCase();

			encPass = removeOddScratch(encPass);

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return encPass;
	}

	public static String encodUppereMD5(String password) {
		String encPass = null;
		MessageDigest md;

		try {
			md = MessageDigest.getInstance("MD5");
			md.update(password.toUpperCase().getBytes());
			byte byteData[] = md.digest();

			// convert the byte to hex forma
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}

			encPass = sb.toString().toUpperCase();

			while (encPass.startsWith("0"))
				encPass = encPass.substring(1, encPass.length());

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return encPass;
	}

	// W celu zachowania spójności z autoryzacją w CTL_STANDARD
	// funkcja usuwa 0 znajdujące się na nieparzystej pozycji
	// Jest to wynik błędu implementacji funkcji haszującej w CTL_STANDARD
	private static String removeOddScratch(String pass) {
		StringBuffer retPass = new StringBuffer();
		for (int i = 0; i < pass.length(); i++) {
			if (!(i % 2 == 0 && pass.charAt(i) == '0')) {
				retPass.append(pass.charAt(i));
			}
		}
		return retPass.toString();
	}
}
