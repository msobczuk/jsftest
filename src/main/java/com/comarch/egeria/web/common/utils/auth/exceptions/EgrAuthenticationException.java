package com.comarch.egeria.web.common.utils.auth.exceptions;


public class EgrAuthenticationException extends Exception {

	private static final long serialVersionUID = 1L;

	public EgrAuthenticationException() {
		this("Could not authenticate user.");
	}

	public EgrAuthenticationException(String message) {
		super(message);
	}
}
