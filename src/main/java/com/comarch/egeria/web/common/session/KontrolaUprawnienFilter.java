package com.comarch.egeria.web.common.session;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.web.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class KontrolaUprawnienFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession(false);
//		System.out.println("doFilter......" + request.getRequestURL());
        User user = (session != null) ? (User) session.getAttribute("user") : null;

		//https://zenidas.wordpress.com/recipes/encoding-issues-while-uploading-files-with-primefaces/
		request.setCharacterEncoding("UTF-8");//kodowanie zanaków po wczytywanoiu plików przez p:fileUpload nie powinno sie zmieniać

		if (user!=null && !user.isLoggedIn && !response.isCommitted()){
			//sesja wylogowana

			String uri = request.getRequestURI();
			String contextPath = request.getContextPath();
			String uri1 = uri.substring(contextPath.length());

			if (!uri1.contains("logout.xhtml") && !uri1.contains("login.xhtml")) { // && !uri1.equals("/login/") && !uri1.contains("javax.faces.resource")
				response.sendRedirect(request.getContextPath() + "/logout.xhtml?kufltr=lgt");
			}
		}

        if (user!=null && !user.isAdmin()){
        	String uri = request.getRequestURI();
    		String contextPath = request.getContextPath();
    		String uri1 = uri.substring(contextPath.length());
//    		System.out.println(user.getDbLogin() + " -> "+ uri1);
    		
    		if (		!"/Obiegi".equals(uri1)
					&& !"/DiagramObiegu".equals(uri1)
					&& !"/Powiadomienia".equals(uri1)
					&& !"/Msgs".equals(uri1)
					&& !"/push".equals(uri1)
					&& !"/index.xhtml".equals(uri1)
					&& !"/".equals(uri1)
					&& !"/UnexpError".equals(uri1)
			) {
	    		String xhtml = AppBean.getXhtml(uri1);
	    		if (xhtml!=null && !user.hasUserAnyPermissionToView(xhtml)){
    				System.out.println(user.getDbLogin() + " / brak uprawnień do: " + uri1);
    				response.sendRedirect(request.getContextPath() + "/ku.xhtml?p="+uri1);
    				return;	    			
	    		}
    		}
        }



		chain.doFilter(req, res);
    }

	@Override
	public void destroy() {
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}

}
