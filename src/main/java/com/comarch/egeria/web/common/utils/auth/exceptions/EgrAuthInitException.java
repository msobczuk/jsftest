package com.comarch.egeria.web.common.utils.auth.exceptions;


public class EgrAuthInitException extends Exception {

	private static final long serialVersionUID = 1L;

	public EgrAuthInitException() {
		this("Could not initialize database session.");
	}

	public EgrAuthInitException(String message) {
		super(message);
	}
}
