package com.comarch.egeria.web.common.view;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

@Named
@Scope("view")
public class HeaderBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String username;

	@PostConstruct
	private void init() {

	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}


}
