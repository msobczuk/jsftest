package com.comarch.egeria.web.common.utils.auth.exceptions;

public class PasswordValidationException extends Exception {

  public PasswordValidationException() {
    super();
  }

}
