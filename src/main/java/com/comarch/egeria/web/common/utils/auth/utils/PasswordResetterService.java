package com.comarch.egeria.web.common.utils.auth.utils;

import com.comarch.egeria.Utils;
import com.comarch.egeria.web.common.utils.auth.bo.AuthBo;
import com.comarch.egeria.web.common.utils.auth.exceptions.PasswordValidationException;
import com.comarch.egeria.web.common.utils.auth.model.PasswordResetToken;
import com.comarch.egeria.web.mail.MailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Named
@Scope("singleton")
public class PasswordResetterService {

  private static final Logger log = LoggerFactory.getLogger(PasswordResetterService.class);

  private static final String CHANGE_PASSWORD_URL = "changePassword.xhtml";
  //private static final String PORTAL_PRACOWNICZY_APP_URL = "PortalPracowniczy";
  private static final String RESET_PASSWORD_MAIL_TITLE = "Zmiana hasła";
  //private static final String RESET_PASSWORD_MAIL_START = "Link do zresetowania hasła: ";
  //private static final String RESET_PASSWORD_MAIL_END = "";
  private static final String DEFAULT_CHANGE_ERROR_MSG = "Problem ze zmianą hasła";
  //private static final String REPEATED_PWD_ERROR_CODE = "ORA-28007:";
  private static final int DEFAULT_RESET_PASSWORD_TOKEN_LIFETIME = 60 * 2;

  @Inject
  AuthBo authBo;

  @Inject
  MailService mailService;

  public void createPasswordResetTokenAndSendToUser(String username, String mail) {
    String passwordResetToken = createPasswordResetToken();
    storePasswordResetToken(passwordResetToken, username);
    sendMailWithPasswordResetToken(mail.toLowerCase(), passwordResetToken);
  }

  public Optional<PasswordResetToken> getValidPasswordResetToken(String passwordResetTokenParam) {
    Optional<PasswordResetToken> passwordResetToken = authBo
        .readPasswordResetToken(passwordResetTokenParam);
    if (passwordResetToken.isPresent()) {
      if ((new Date()).after(passwordResetToken.get().getExpiryDate())) {
        passwordResetToken = Optional.empty();
      }
    }
    return passwordResetToken;
  }

  public boolean canUserChangePassword(String username) {
    String checkUser;
    try {
      String nazwaZewnetrzna = authBo.odczytajNazweZewnetrzna(username);
      checkUser = authBo.checkPpfBlokadaLogowaniaUzt(nazwaZewnetrzna);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      return false;
    }
    return checkUser == null;
  }

  private void sendMailWithPasswordResetToken(String mail, String passwordResetToken) {
    try {
      String mailBody = createResetPasswordMail(passwordResetToken, mail);
      mailService.send(mail, RESET_PASSWORD_MAIL_TITLE, mailBody);
    } catch (Exception e) {
      throw new PasswordResetException(DEFAULT_CHANGE_ERROR_MSG, e);
    }
  }

  private String createResetPasswordMail(String passwordResetToken, String userMail) {
    String mailContent = "Zostało wysłane żądanie zmiany hasła do programu Portal Pracowniczy dla użytkownika: ";
    mailContent += userMail;
    mailContent += "\nW celu zmiany hasła należy skorzystać z poniższego linka.\n"
        + " \n"
        + "Link jest ważny przez 2 godziny od czasu wygenerowania.\n";
    mailContent += generateResetPasswordURL(passwordResetToken);
    mailContent +=
        "\nNowe hasło musi spełniać wymagania procedury DBW PPB-14_P Polityka haseł, dostępnej w intranet (http://dbw.comarch/procedury ). Bezpieczne hasło znacznie ogranicza możliwość nieautoryzowanego skorzystania z danego konta.\n"
            + "Jeśli nie chcesz zmieniać hasła, zignoruj tego maila.\n"
            + " \n"
            + "Mail ten został wygenerowany automatycznie, proszę na niego nie odpisywać.\n"
            + " \n"
            + "Portal Pracowniczy\n"
            + "Dział Informatycznych Systemów Finansowych\n"
            + " \n"
            + "portal.support@comarch.pl \n";
    return mailContent;
  }

  private String generateResetPasswordURL(String passwordResetToken) {
    HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
        .getExternalContext().getRequest();
    String requestURL = request.getRequestURL().toString();
    request.getContextPath();
    int indexOfContextPath = requestURL.indexOf('/', requestURL.indexOf("://") + 3);
    String domain = requestURL.substring(0, indexOfContextPath);
    return domain + request.getContextPath() + "/" + CHANGE_PASSWORD_URL + "?token="
        + passwordResetToken;
  }

  private void storePasswordResetToken(String passwordResetTokenValue, String username) {
    PasswordResetToken passwordResetToken = new PasswordResetToken();
    passwordResetToken.setUsername(username);
    passwordResetToken.setToken(passwordResetTokenValue);
    passwordResetToken.setExpiryDate(calculateExpirationTime());
    authBo.saveNewPasswordToken(passwordResetToken);
  }

  private Date calculateExpirationTime() {
    int tokenLifetime = getTokenLifetimeInMinutes();
    Calendar now = Calendar.getInstance();
    now.add(Calendar.MINUTE, tokenLifetime);
    return now.getTime();
  }

  private int getTokenLifetimeInMinutes() {
    try {
      String tokenLifetimeFromConfigurationStr = Utils
          .konfiguracjaWartoscZewn("PP_RESET_MAIL_TOKEN_LIFETIME");
      return Integer.parseInt(tokenLifetimeFromConfigurationStr);
    } catch (Exception e) {
      return DEFAULT_RESET_PASSWORD_TOKEN_LIFETIME;
    }
  }

  private String createPasswordResetToken() {
    return UUID.randomUUID().toString();
  }

  public void changePassword(PasswordResetToken passwordResetToken, String password)
      throws PasswordValidationException, SQLException {
    authBo.changePassword(passwordResetToken.getUsername(), password);
    authBo.deleteOldPasswordTokens(passwordResetToken.getUsername());
  }

}
