package com.comarch.egeria.web.common.hibernate;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.CallbackException;
import org.hibernate.EmptyInterceptor;
import org.hibernate.Transaction;
import org.hibernate.type.Type;

import com.comarch.egeria.pp.data.SqlDataCache;

public class EgrHibernateInterceptor extends EmptyInterceptor {
	private static final Logger log = LogManager.getLogger();
	
	//https://www.baeldung.com/hibernate-interceptor

	private static final long serialVersionUID = 1L;
	
	
	
	
	
//	@Override
//	public boolean onLoad(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
//		System.out.println("EgrHibernateInterceptor.onLoad... \n entity: "+ entity + "\n state: "+ state + "\n propertyNames: "+ propertyNames + "\n types: "+ types);
//		return super.onLoad(entity, id, state, propertyNames, types); 
//	}
	
//	@Override
//	public boolean onFlushDirty(Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types) {
//		boolean ret = super.onFlushDirty(entity, id, currentState, previousState, propertyNames, types);
//		return ret;
//	}
	
	
	/**
	 * 
	 */
	

	@Override
	public String onPrepareStatement(String sql) {
		String tb = null;
		
		if (sql.startsWith("update ")) { 			//update PPADM.PPT_TTTTTTT set  ....
			tb = sql.split(" ")[1] ; 
			log.debug("EgrHibernateInterceptor.onPrepareStatement: "+sql);
		}else if(sql.startsWith("insert ") 		//insert into PPADM.PPT_TTTTTTT (...
				|| sql.startsWith("delete ")) {      //delete from PPADM.PPT_TTTTTTT where ...
			tb =  sql.split(" ")[2] ;
			log.debug("EgrHibernateInterceptor.onPrepareStatement: "+sql);
		}
			
		if (tb!=null) {
			SqlDataCache.htbtsCHM.put(tb, System.currentTimeMillis()); //System.nanoTime()
		}

		
		
		return super.onPrepareStatement(sql);
	}
	
//	@Override nie dziala
//	public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
//		System.out.println("EgrHibernateInterceptor.onSave... \n entity: "+ entity + "\n state: "+ state + "\n propertyNames: "+ propertyNames + "\n types: "+ types);
//		return super.onSave(entity, id, state, propertyNames, types);
//	}
	
//	@Override
//	public void afterTransactionCompletion(Transaction tx) {
//		super.afterTransactionCompletion(tx);
//	}
	 
}
