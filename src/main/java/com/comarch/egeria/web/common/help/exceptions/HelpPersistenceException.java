package com.comarch.egeria.web.common.help.exceptions;

public class HelpPersistenceException extends Exception {

	private static final long serialVersionUID = 2429979446828838910L;

	public HelpPersistenceException(String message, Throwable cause) {
		super(message, cause);
	}

	public HelpPersistenceException(String message) {
		super(message);
	}

	public HelpPersistenceException(Throwable cause) {
		super(cause);
	}
}
