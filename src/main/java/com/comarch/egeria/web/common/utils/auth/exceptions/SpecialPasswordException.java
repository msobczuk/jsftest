package com.comarch.egeria.web.common.utils.auth.exceptions;

public class SpecialPasswordException extends PasswordValidationException {

  public SpecialPasswordException() {
    super();
  }

}
