package com.comarch.egeria.web.common.view.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Named;

import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.web.common.view.dao.SqlGlobalneDao;
import com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu.domain.Firma;
import com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu.domain.Oddzial;

@Named
public class SqlGlobalneDaoImpl implements SqlGlobalneDao {
	
	@Override
	public Firma sqlUstawionaFirmaUzytkownika() throws SQLException {
		String sql = "select FRM_ID, FRM_NAZWA from EAT_FIRMY where FRM_ID = eap_globals.odczytaj_firme";
		Firma ret = null;
		
		try (Connection conn = JdbcUtils.getConnection(false);
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);) {
			
			if(rs.next()) {
				ret = new Firma();
				ret.setId(rs.getLong("FRM_ID"));
				ret.setNazwa(rs.getString("FRM_NAZWA"));
			}
		}

		return ret;
	}

	@Override
	public List<Firma> sqlFirmyUzytkownika() throws SQLException {
		String sql = "select FUW_FRM_ID, FUW_FRM_NAZWA from EAV_FIRMY_UZYTKOWNIKA_WEB";
		List<Firma> list = new LinkedList<Firma>();
		
		try (Connection conn = JdbcUtils.getConnection(false);
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);) {
			
			while(rs.next()) {
				Firma frm = new Firma();
				frm.setId(rs.getLong("FUW_FRM_ID"));
				frm.setNazwa(rs.getString("FUW_FRM_NAZWA"));
				list.add(frm);
			}
		}

		return list;
	}

	@Override
	public Oddzial sqlUstawionyOddzialUzytkownika() throws SQLException {
		String sql = "select ODZ_ID, ODZ_NAZWA from EAT_ODDZIALY where ODZ_ID = eap_globals.odczytaj_oddzial";
		Oddzial ret = null;
		
		try (Connection conn = JdbcUtils.getConnection(false);
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);) {
			
			if(rs.next()) {
				ret = new Oddzial();
				ret.setId(rs.getLong("ODZ_ID"));
				ret.setNazwa(rs.getString("ODZ_NAZWA"));
			}
		}

		return ret;
	}

	@Override
	public List<Oddzial> sqlOddzialyUzytkownika() throws SQLException {
		String sql = "select OUF_ODZ_ID, OUF_ODZ_NAZWA from EAV_ODDZIALY_UZT_W_FIRMIE";
		List<Oddzial> list = new LinkedList<Oddzial>();
		
		try (Connection conn = JdbcUtils.getConnection(false);
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);) {
			
			while(rs.next()) {
				Oddzial odz = new Oddzial();
				odz.setId(rs.getLong("OUF_ODZ_ID"));
				odz.setNazwa(rs.getString("OUF_ODZ_NAZWA"));
				list.add(odz);
			}
		}

		return list;
	}

	@Override
	public void ustawFirme(Firma selectedFirma) throws SQLException {
		String sql = "{call eap_web.ustaw_firme(?)}";
		
		try (Connection conn = JdbcUtils.getConnection(false);
				CallableStatement cs = conn.prepareCall(sql)) {
			
			cs.setLong("p_frm_id", selectedFirma.getId());
			cs.execute();
		}
	}

	@Override
	public void ustawOddzial(Oddzial selectedOddzial) throws SQLException {
		String sql = "{call eap_web.ustaw_oddzial(?)}";
		
		try (Connection conn = JdbcUtils.getConnection(false);
				CallableStatement cs = conn.prepareCall(sql)) {
			
			cs.setLong("p_odz_id", selectedOddzial.getId());
			cs.execute();
		}
	}
}
