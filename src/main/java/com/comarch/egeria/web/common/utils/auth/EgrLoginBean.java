package com.comarch.egeria.web.common.utils.auth;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.RodoUtils;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.session.EgrUserSessionBean;
import com.comarch.egeria.web.common.utils.auth.EgrAuthToken.EgrAuthMethod;
import com.comarch.egeria.web.common.utils.auth.bo.AuthBo;
import com.comarch.egeria.web.common.utils.auth.exceptions.EgrAuthInitException;
import com.comarch.egeria.web.common.utils.auth.exceptions.EgrAuthenticationException;
import com.comarch.egeria.web.common.utils.auth.pki.EgrPKIAuth;
import com.comarch.egeria.web.common.utils.auth.pki.EgrPKICertificateEmailNotFoundException;
import com.comarch.egeria.web.common.utils.auth.pki.EgrPKICertificateNotFoundException;
import com.comarch.egeria.web.common.utils.auth.pki.EgrPKICertificateWrongIssuerException;
import com.comarch.egeria.web.common.utils.auth.utils.EgrPreAuthData;
import com.comarch.egeria.web.common.utils.auth.utils.PasswordResetException;
import com.comarch.egeria.web.common.utils.auth.utils.PasswordResetterService;
import com.comarch.egeria.web.common.utils.constants.Const;
import com.comarch.egeria.web.common.utils.lang.LanguageBean;
import com.comarch.egeria.web.common.view.dao.SqlGlobalneDao;
import com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu.domain.Firma;
import com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu.domain.Oddzial;
import io.buji.pac4j.subject.Pac4jPrincipal;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;
import org.pac4j.core.profile.CommonProfile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.CachedRowSet;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.comarch.egeria.Utils.eq;
import static com.comarch.egeria.Utils.nz;

@Named
@Scope("session")
public class EgrLoginBean {

  private static final Logger log = LoggerFactory.getLogger(EgrLoginBean.class);

  @Inject
  private EgrUserSessionBean egrUserSessionBean;

  @Inject
  private User user;

  @Inject
  private AuthBo authBo;
  @Inject
  private LanguageBean languageBean;
  @Inject
  private SqlGlobalneDao sqlGlobalneDao;
  @Inject
  PasswordResetterService passwordResetterService;

  @PreDestroy
  private void predestroy() {
    this.egrUserSessionBean = null;
    this.user = null;
    this.languageBean = null;
    this.authBo = null;
    this.sqlGlobalneDao = null;
  }

  public int prc_id = 0;
  public String prc_imie;
  public String prc_nazwisko;

  private String username;
  private String dbUsername;
  private String password;
  private Boolean rememberMe;

  private Boolean disableLoginButton;
  private Boolean loginDisabled;
  private Boolean passwordRendered;
  private Boolean passwordRequired;
  private String loginButtonLabel;

  // authMethod - CAS/Egeria
  private static AuthenticationMethod authMethod = AuthenticationMethod.STANDARD;
  private Boolean applicationStarted;

  private SavedRequest savedRequest;
  private String contentRedirectUrlFlash = null;

  private static Boolean checkPKI = false;
  private Boolean resetPasswordRendered = false;

  public void doLogin() {
    if (!testBlokadaLogowaniaPrzyLogowaniu()) return;

    if (!AuthenticationMethod.STANDARD.equals(authMethod)) {
      initDatabaseContext();
    } else {
      dbAuthentication();
    }
    if (applicationStarted) {
      inicjujFirmeOdzial();
      user.setFrmId(       egrUserSessionBean.getWybranaFirma().getId().intValue());
      user.setDefaultFrmId(egrUserSessionBean.getWybranaFirma().getId().intValue());
      user.setDefaultPrcId(user.getPrcId());

      RodoUtils.dsZapamietajSesje(user);
    }
  }

  public void doLogout() {
//    String sessionId = FacesContext.getCurrentInstance().getExternalContext().getSessionId(false);
//    AppBean.removeFromSessionsUsers(sessionId);
//    clearDbContext(sessionId);
//    Push.push(user.getDbLogin(), "-","-", sessionId + ".LOGOUT");//komunikat - wszystkie/pozostałe okna w ramach tej samej sesji
    //user.isLoggedIn = false;
    //redirectToLogoutPage();

    user.logout(true);
  }

  public void resetPassword() {
    if (!testBlokadaLogowania()) return;

    try {
      if (checkPKI && username != null) {
        resetPasswordForMailUsername();
        facesInfo(LanguageBean.translate("INF_reset_password_mail_sent"));
      }
    } catch (PasswordResetException e) {
      log.error(e.getMessage(), e.getCause());
      facesError(LanguageBean.translate("ERR_reset_password_problem"));
    }
  }

  private void resetPasswordForMailUsername() {
    String mail = null;
    String usernameFromMail = null;

    if (pkiUztNazwy!=null && pkiUztNazwy.size() > 1 && !eq(username.toUpperCase(), nz(pkiUztNazwy.get(0)).toUpperCase()) ) {

      usernameFromMail = username.toUpperCase(); //M2
      mail = nz(pkiUztNazwy.get(0)).toUpperCase(); //...@COMARCH.COM

    } else {

      mail = username.toUpperCase();//...@COMARCH.COM
      usernameFromMail = authBo.odczytajNazweUzytkownikaZNazwyZewnetrznej(mail, "EGR"); //M3

    }
    passwordResetterService
        .createPasswordResetTokenAndSendToUser(usernameFromMail, mail);

  }


  @PostConstruct
  private void init() {
    applicationStarted = false;
  }

  public void preViewConstruct() {
    initAuthentication();
    checkAndInitSavedRequest();
    if (SecurityUtils.getSubject().isAuthenticated()) {
      if (applicationStarted && user.isLoggedIn) {
        redirectToHomepage();
      } else {
        if (!AuthenticationMethod.STANDARD.equals(authMethod)) {
          initUsernameInNonStandardAuth();
        }
      }
    } else if (checkPKI) {
      validatePKI();
    }
    languageBean.changeLanguage(languageBean.getLocale().toString());
    disableLoginButton = false;
    setLoginPageFieldsProperties();


    testBlokadaLogowania();
  }



  private boolean testBlokadaLogowania(){
    if (nz(username).isEmpty()) return true;
    String checkBlokadaUzt = authBo.checkPpfBlokadaLogowaniaUzt(username);
    if (checkBlokadaUzt != null){
      log.error("{}: {}", username, checkBlokadaUzt);
      facesError(checkBlokadaUzt);
      disableLoginButton = true;
      resetPasswordRendered = false;
      return false;
    }
    return true;//brak blokady
  }

  private boolean testBlokadaLogowaniaPrzyLogowaniu(){
    if (nz(username).isEmpty()) return true;
    String checkBlokadaUzt = authBo.checkPpfBlokadaLogowaniaUzt(username);
    if (checkBlokadaUzt != null){
      log.error("{}: {}", username, checkBlokadaUzt);
      facesError(checkBlokadaUzt);
      return false;
    }
    return true;//brak blokady
  }


  private void validatePKI() {
    try {
      username = EgrPKIAuth.checkComarchX509Certificate();
    } catch (EgrPKICertificateNotFoundException | CertificateExpiredException | CertificateNotYetValidException
        | EgrPKICertificateWrongIssuerException | CertificateParsingException
        | EgrPKICertificateEmailNotFoundException e) {
      facesError(LanguageBean.translate("LBL10105_login_blad_certyfikatu"));
      disableLoginButton = true;
      resetPasswordRendered = false;
      logValidatePKIMessage(e);
    }
  }

  private void logValidatePKIMessage(CertificateException e) {
    String errorMessage;
    errorMessage = e.getMessage() != null ? e.getMessage() : e.getClass().getSimpleName();
    log.error("validatePKI, user: {}: {}", username, errorMessage);
  }

  private void checkAndInitSavedRequest() {
    savedRequest = WebUtils
        .getAndClearSavedRequest(
            (ServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest());
  }

  private void initUsernameInNonStandardAuth() {
    final Pac4jPrincipal principal = SecurityUtils.getSubject().getPrincipals()
        .oneByType(Pac4jPrincipal.class);
    if (principal != null && principal.getProfile() != null) {
      CommonProfile profile = principal.getProfile();
      if (AuthenticationMethod.CAS.equals(authMethod)) {
        username = profile.getId();
      } else if (AuthenticationMethod.KEYCLOAK.equals(authMethod)) {
        username = (String) profile.getAttribute("preferred_username");
      }
    }
  }

  private static void initAuthentication() {
    initCheckPKI();
    initAuthMethod();
  }

  private static void initCheckPKI() {
    try {
      checkPKI = (Boolean) (new InitialContext()).lookup("java:/comp/env/check_pki");
    } catch (NamingException e) {
      checkPKI = false;
    }
  }

  private static void initAuthMethod() {
    try {
      String authString = "" + (new InitialContext()).lookup("java:/comp/env/auth");
      AuthenticationMethod newAuthMethod = AuthenticationMethod.valueOf(authString.toUpperCase());
      if (!authMethod.equals(newAuthMethod)) {
        authMethod = newAuthMethod;
        log.debug("auth-> {} ", authString);
      }
    } catch (NamingException e) {
      log.error(e.getMessage(), e);
    }
  }

  private void redirectToHomepage() {
    redirectToPage(Const.HOME_REDIRECT_URL);
  }

  private void initDatabaseContext() {
    try {
      EgrPreAuthData egrPreAuthData = authBo.odczytajUzytkownika(getUsername());
      initEgrUsrSessionBean(egrPreAuthData);

      // inicjacja globali
      String sessionId = FacesContext.getCurrentInstance().getExternalContext().getSessionId(false);
      authBo.init(egrUserSessionBean.getDbUsername(), sessionId);

      initUser(egrPreAuthData, authBo.odczytajFunkcjeUzytkowe(egrPreAuthData.getDbUsername()));

      log.debug("zalogowano: {}  [{}]", user.getLogin(), user.getDbLogin());
      log.debug("user.permissions: {}", user.getPermissions());

      applicationStarted = true;
      if (savedRequest == null) {
        redirectToHomepage();
      } else {
        redirectToRequestedURL();
      }
    } catch (SQLException ex) {
      facesError(LanguageBean.translate("INF_database_error") + ": " + ex.getMessage());
      log.error(ex.getMessage(), ex);
    } catch (EgrAuthenticationException | EgrAuthInitException ex) {
      facesError(ex.getMessage());
      log.error(ex.getMessage(), ex);
    }
  }

  private void dbAuthentication() {
    Subject subject = SecurityUtils.getSubject();

    EgrAuthToken authToken = new EgrAuthToken();
    try {
      EgrPreAuthData egrPreAuthData = authBo.odczytajUzytkownika(getUsername());
      authToken.setUsername(egrPreAuthData.getDbUsername());
      authToken.setPassword(getPassword().toCharArray());

      if ("ORACLE".equals(egrPreAuthData.getAuthMethod())) {
        authToken.setEgrAuthMethod(EgrAuthMethod.ORACLE);
      } else if ("EGR".equals(egrPreAuthData.getAuthMethod())) {
        authToken.setEgrAuthMethod(EgrAuthMethod.EGR);
      } else {
        throw new EgrAuthenticationException(
            LanguageBean.translate("INF_unsupported_auth_method", egrPreAuthData.getAuthMethod()));
      }

      subject.login(authToken);

      initEgrUsrSessionBean(egrPreAuthData);

      // inicjacja globali
      String sessionId = FacesContext.getCurrentInstance().getExternalContext().getSessionId(false);
      authBo.init(egrUserSessionBean.getDbUsername(), sessionId);

      initUser(egrPreAuthData, authBo.odczytajFunkcjeUzytkowe(egrPreAuthData.getDbUsername()));//...

      log.debug("zalogowano: {}  [{}]", user.getLogin(), user.getDbLogin());
      log.debug("user.permissions: {}", user.getPermissions());

      // ustawienie flagi ze zalogowany
      applicationStarted = true;

      if (savedRequest == null) {
        FacesContext.getCurrentInstance().getExternalContext().redirect(Const.HOME_REDIRECT_URL);
      } else {
        redirectToRequestedURL();
      }

    } catch (EgrAuthenticationException | AuthenticationException ex) {
      facesError(ex.getMessage());
    } catch (IOException | EgrAuthInitException | SQLException ex) {
      facesError(LanguageBean.translate("INF_login_error"));
      log.error(ex.getMessage(), ex);
    } finally {
      authToken.clear();
    }
  }

  private void initUser(EgrPreAuthData egrPreAuthData, List<String> userPermissions) throws EgrAuthenticationException {

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    try (final Connection conn = JdbcUtils.getConnection(false)) {

      String sql = "" +
          "select ddf_prc_id, uzt_imie, uzt_nazwisko1 from (\n" +
          "    \n" +
          "    select * \n" +
          "    from eat_uzytkownicy uzt " +
          "        join eat_dostepy_do_firm ddf on uzt_nazwa = ddf_uzt_nazwa \n" +
          "        left join eat_parametry_instalacji pi on PI_UZT_NAZWA = ddf_uzt_nazwa  \n" +
          "            and PI_TPI_NAZWA = 'FRM_ID' \n" +
          "            and PI_JZK_ID = 1 \n" + //-- jezyk PL
          "            and PI_INS_ID = 1 \n" + //--1=EG; 0=EA (0 parametry logowania do EA nas nie interesuja)
          "    where uzt.uzt_nazwa = ? \n" + //--np. 'ADMINISTRATOR_PORTAL'
          "    order by \n" +
          "        nvl(pi.pi_audyt_dm, pi.pi_audyt_dt) desc, pi_id desc, \n" + //--ostatnio modyfikowany/utworzony pi; ostatnio wstawiony jesli audyt nie dziala
          "        ddf.ddf_frm_id \n" + //--jesli nigdy sie nie logowal i nie ma zadnego PI
          "    \n" +
          ") where rownum = 1 ";

      CachedRowSet crs = JdbcUtils.sqlExecQuery(conn, sql, egrPreAuthData.getDbUsername());
      crs.first();
      egrPreAuthData.setPrc_id(crs.getInt("ddf_prc_id"));
      egrPreAuthData.setPrc_imie(crs.getString("uzt_imie"));
      egrPreAuthData.setPrc_nazwisko(crs.getString("uzt_nazwisko1"));

    } catch (Exception e) {
      log.error("!!! Nie ustalono danych pracownika !!! {}", e.toString());
      throw new EgrAuthenticationException(LanguageBean.translate("INF_login_error"));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    this.prc_id = egrPreAuthData.getPrc_id();
    this.prc_imie = egrPreAuthData.getPrc_imie();
    this.prc_nazwisko = egrPreAuthData.getPrc_nazwisko();
    user.setLogin(this.getUsername());
    user.setDbLogin(egrPreAuthData.getDbUsername());
    this.setDbUsername(user.getDbLogin());
    user.setPrcId(prc_id);
    user.setNazwisko(egrPreAuthData.getPrc_nazwisko());
    user.setImie(egrPreAuthData.getPrc_imie());
    user.setPermissions(userPermissions);
    user.postLoginInit();
  }

  private void redirectToRequestedURL() {
    // ustawic docelowy context i przekierowac na home...
    HttpServletRequest baseRequest = (HttpServletRequest) FacesContext.getCurrentInstance()
        .getExternalContext().getRequest();

    String baseUrl = baseRequest.getRequestURL()
        .substring(0, baseRequest.getRequestURL().indexOf((baseRequest).getContextPath()))
        + (baseRequest).getContextPath();

    contentRedirectUrlFlash = savedRequest.getRequestUrl().substring(
        savedRequest.getRequestUrl().indexOf(baseRequest.getContextPath()) + baseRequest
            .getContextPath().length());
    redirectToPage(baseUrl);
  }


  public ArrayList<String> getPkiUztNazwy() {
    return pkiUztNazwy;
  }

  public void setPkiUztNazwy(ArrayList<String> pkiUztNazwy) {
    this.pkiUztNazwy = pkiUztNazwy;
  }

  ArrayList<String> pkiUztNazwy = new ArrayList<>();



  private void setLoginPageFieldsProperties() {
    if (!AuthenticationMethod.STANDARD.equals(authMethod)) {
      loginDisabled = true;
      passwordRequired = false;
      passwordRendered = false;
      loginButtonLabel = "LBL_start";
    } else if (checkPKI) {
      loginDisabled = true;
      passwordRequired = true;
      passwordRendered = true;
      resetPasswordRendered = true;
      loginButtonLabel = "LBL_login";
    } else {
      loginDisabled = false;
      passwordRequired = true;
      passwordRendered = true;
      loginButtonLabel = "LBL_login";
    }


    if (checkPKI) {
      pkiUztNazwy = wczytajUztNazwy( this.username );
    }

//    if (checkPKI) {
//      try(Connection conn = JdbcUtils.getConnection(false)){
//        BigDecimal cnt = (BigDecimal) JdbcUtils.sqlExecScalarQuery(conn,"select count(*) from eat_uzytkownicy where  uzt_f_portal = 'T' and lower(uzt_nazwa_zewn) = ?", nz(username).toLowerCase());
//        if (cnt.intValue()>0){
//          loginDisabled = false; // pozwalamy zmieniac login dla tych, ktorzy mają wg PKI dostep z kilku weirszy EAT_UZYTKOWNICY z roznymi UZT_METODA_AUTENTYKACJI
//        }
//      } catch (Exception e) {
//        e.printStackTrace();
//      }
//    }

  }



  private ArrayList<String> wczytajUztNazwy(String pkiLogin) {
    if (!this.pkiUztNazwy.isEmpty())
      return this.pkiUztNazwy;//zabezpieczenie przed nadmiernym obciazaniem bazy

    ArrayList<String> ret = new ArrayList<>();
    ret.add(pkiLogin); //login / mail z PKI

    try(Connection conn = JdbcUtils.getConnection(false)){

      final CachedRowSet crs = JdbcUtils.sqlExecQuery(conn,
              "select uzt_nazwa " +
                      "from eat_uzytkownicy " +
                      "where  uzt_f_portal = 'T' " +
                      " and lower(uzt_opis) =  ? " +
                      "order by uzt_nazwa_zewn ", nz(pkiLogin).toLowerCase());
      while (crs.next()) {
        ret.add(crs.getString("uzt_nazwa"));
      }

    } catch (Exception e) {
      log.error(e.getMessage(), e);
    }

    return ret;
  }




  // ReAuth START

  void reAuth(String username) {
    String sessionId = FacesContext.getCurrentInstance().getExternalContext().getSessionId(false);
    clearContextOnReAuth(username, sessionId);
    try {
      setContextOnReAuth(sessionId);
      log.debug("przelogowano: {}  [{}]", user.getLogin(), user.getDbLogin());
      log.debug("user.permissions: {}", user.getPermissions());
    } catch (Exception ex) {
      log.error(ex.getMessage(), ex);
      handleError(ex);
      FacesContext.getCurrentInstance().getExternalContext().getFlash().put("errMsg",
          "Próba zalogowania jako " + username + " się nie powiodła. Przyczyna: " + ex
              .getMessage());
      doLogout();
      return;
    }
    redirectToPage(Const.HOME_REDIRECT_URL);
  }

  private void redirectToPage(String redirectUrl) {
    try {
      FacesContext.getCurrentInstance().getExternalContext().redirect(redirectUrl);
    } catch (IOException e) {
      handleError(e);
    }
  }

  private void handleError(Exception ex) {
    handleError(ex, ex.getMessage());
  }

  private void handleError(Exception ex, String errMessage) {
    facesError(errMessage);
    log.error(errMessage, ex);
  }

  private void setContextOnReAuth(String sessionId)
      throws SQLException, EgrAuthenticationException, EgrAuthInitException {
    EgrPreAuthData egrPreAuthData = authBo.odczytajUzytkownika(getUsername());
    initEgrUsrSessionBean(egrPreAuthData);
    initGlobals(sessionId, egrPreAuthData);
    initUser(egrPreAuthData, authBo.odczytajFunkcjeUzytkowe(egrPreAuthData.getDbUsername()));
    inicjujFirmeOdzial();
    RodoUtils.dsZapamietajSesje(user);
  }

  private void clearContextOnReAuth(String username, String sessionId) {
    AppBean.removeFromSessionsUsers(sessionId);
    clearDbContext(sessionId);
    initUserOnReAuth(username);
  }

  private void clearDbContext(String sessionId) {
    authBo.clearContext(sessionId);
  }

  private void initUserOnReAuth(String username) {
    user.clean();
    setUsername(username);
    setPassword("NOT_USED");
  }

  private void initEgrUsrSessionBean(EgrPreAuthData egrPreAuthData) {
    egrUserSessionBean.setUsername(getUsername());
    egrUserSessionBean.setUzId(10L);
    egrUserSessionBean.setDbUsername(egrPreAuthData.getDbUsername());
  }

  private void initGlobals(String sessionId, EgrPreAuthData egrPreAuthData)
      throws SQLException, EgrAuthInitException {
    authBo.init(egrPreAuthData.getDbUsername(), sessionId);
  }

  // ReAuth END

  private void facesError(String message) {
    FacesContext.getCurrentInstance()
        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
  }

  @SuppressWarnings("unused")
  private void facesInfo(String message) {
    FacesContext.getCurrentInstance()
        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
  }

  private void inicjujFirmeOdzial() {
    try {
      Firma ustawionaFirma = sqlGlobalneDao.sqlUstawionaFirmaUzytkownika();
      egrUserSessionBean.setWybranaFirma(ustawionaFirma);

      Oddzial ustawionyOddzial = sqlGlobalneDao.sqlUstawionyOddzialUzytkownika();
      egrUserSessionBean.setWybranyOddzial(ustawionyOddzial);
      user.setFrmId(Math.toIntExact( nz(ustawionaFirma.getId()) ));
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
    }
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String login) {
    this.username = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String senha) {
    this.password = senha;
  }

  public Boolean getRememberMe() {
    return rememberMe;
  }

  public void setRememberMe(Boolean lembrar) {
    this.rememberMe = lembrar;
  }

  public Boolean getDisableLoginButton() {
    return disableLoginButton;
  }

  public void setDisableLoginButton(Boolean disableLoginButton) {
    this.disableLoginButton = disableLoginButton;
  }

  public Boolean getLoginDisabled() {
    return loginDisabled;
  }

  public void setLoginDisabled(Boolean loginDisabled) {
    this.loginDisabled = loginDisabled;
  }

  public Boolean getPasswordRendered() {
    return passwordRendered;
  }

  public void setPasswordRendered(Boolean passwordRendered) {
    this.passwordRendered = passwordRendered;
  }

  public Boolean getPasswordRequired() {
    return passwordRequired;
  }

  public void setPasswordRequired(Boolean passwordRequired) {
    this.passwordRequired = passwordRequired;
  }

  public String getLoginButtonLabel() {
    return LanguageBean.translate(loginButtonLabel);
  }

  public void setLoginButtonLabel(String loginButtonLabel) {
    this.loginButtonLabel = loginButtonLabel;
  }

  public String getDbUsername() {
    return dbUsername;
  }

  public void setDbUsername(String dbUsername) {
    this.dbUsername = dbUsername;
  }

  public String getContentRedirectUrlFlash() {
    return contentRedirectUrlFlash;
  }

  public void setContentRedirectUrlFlash(String contentRedirectUrlFlash) {
    this.contentRedirectUrlFlash = contentRedirectUrlFlash;
  }

  public Boolean getResetPasswordRendered() {
    return resetPasswordRendered;
  }

  public enum AuthenticationMethod {
    STANDARD,
    CAS,
    KEYCLOAK
  }

}