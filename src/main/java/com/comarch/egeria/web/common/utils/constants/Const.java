package com.comarch.egeria.web.common.utils.constants;

public final class Const {

	
	// symbol aplikacji (dla EAP_WEB.INICJIUJ)
	public static final String APP_SYMBOL = "PP";
	
	// nazwa aplikacji
	public static final String APP_NAME = "Egeria Portal Pracownika";
	
	// nazwa aplikacji do komunikacji z bazą danych
	public static final String NAZWA_KOMPONENTU_CRU = "PORTAL";
	
	
	// style formularzy
	public final static String DT_STYLE_STANDARD = "STANDARD";
	public final static String DT_STYLE_COMPACT = "COMPACT";


	//adresy
	public static final String LOGIN_PAGE_URL = "login.xhtml";
	public static final String LOGOUT_PAGE_URL = "logout.xhtml";	
	public static String HOME_REDIRECT_URL = null; // AppBean.init() ... -> "/PortalPracowniczy" na PROD albo "/pp_war_exploded" na DEV
	public static final String UNEXPECTED_ERROR_URL = "/WEB-INF/protectedElements/Errors/UnexpError.xhtml";
	/**
	 * Nazwa JNDI połaczenia do bazy zdefiniowana na serwerze.
	 */
	
	public static final String JNDI_CONNECTION_NAME = "java:comp/env/jdbc/pp_db";    //tomcat / -> mf_dev
	//	public static final String JNDI_CONNECTION_NAME = "java:jboss/pp_db"; //wildfly
	
	//public static final String JNDI_CONNECTION_NAME = "java:jboss/____ctl_db";
	 

	// rodzaje zleceń - przykladowy formularz
	public final static String WLASNE_W_TOKU = "WT";
	public final static String WLASNE_ODEBRANE = "WO";
	public final static String OBCE_W_TOKU = "OT";
	public final static String OBCE_ZAKONCZONE = "OZ";

	// typy zlecen - przykladowy formularz
	public final static String ZCLEC_NR = "NR";
	public final static String ZCLEC_PK = "PK";
	
	
	public static final boolean JDBC_ACTIVE_CONN_INFO = false;
	//JDBC_ACTIVE_CONN_INFO decyduje czy w JdbcDAO.addActiveConnInfo jest przechowywana informacja nt. otwartych polaczen do bazy; 
	//JDBC_ACTIVE_CONN_INFO w razie problemów lub wyciekow z puli polaczeñ mozna testowac to z widoku http://localhost:8080/PortalPracowniczy/test_conn.xhtml
	
}