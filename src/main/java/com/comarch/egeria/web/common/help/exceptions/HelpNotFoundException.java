package com.comarch.egeria.web.common.help.exceptions;

public class HelpNotFoundException extends Exception {

	private static final long serialVersionUID = 4907334415648367644L;

	public HelpNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public HelpNotFoundException(String message) {
		super(message);
	}

	public HelpNotFoundException(Throwable cause) {
		super(cause);
	}
}
