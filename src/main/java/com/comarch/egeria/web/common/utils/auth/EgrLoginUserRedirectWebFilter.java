package com.comarch.egeria.web.common.utils.auth;

import java.io.IOException;

import javax.faces.application.ResourceHandler;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;

import com.comarch.egeria.web.common.utils.constants.Const;

/**
 * Filtr przekierowujacy ze strony logowania na glowna zalogowanych uzytkownikow
 */
public class EgrLoginUserRedirectWebFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        if(!request.getRequestURI().startsWith(request.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER ) &&
        		request.getRequestURI().equals(request.getContextPath()+ "/" + Const.LOGIN_PAGE_URL) &&
        		SecurityUtils.getSubject().isAuthenticated()) {
        	response.sendRedirect(request.getContextPath()+"/");
        }
        else {
        	chain.doFilter(req, res);
        }
    }

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}
}
