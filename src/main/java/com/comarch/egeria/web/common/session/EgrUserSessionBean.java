package com.comarch.egeria.web.common.session;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.commons.lang.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import com.comarch.egeria.web.common.utils.constants.Const;
import com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu.domain.Firma;
import com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu.domain.Oddzial;

@Named
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS) 
public class EgrUserSessionBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LogManager.getLogger(EgrUserSessionBean.class);

	private Long uzId;
	private String username;
	private String usernameUpperCase;
	private String cUsername;
	private String dbUsername; //uzt_nazwa w eat_uzytkownicy


	private String dtStyle;
	private boolean menuShow;
	
	private Firma wybranaFirma;
	private Oddzial wybranyOddzial;

	/*
	 *
	 *
	 * Private functions
	 *
	 *
	 */

	@PostConstruct
	private void init() {
		
//		try {
//			FacesContext.getCurrentInstance().getExternalContext().getSession(true);//bywa, że zaraz po wylogowaniu sesja po str. klienta jakaś sesja jest jednak potrzebna...	
//		} catch (Exception e) {
//			System.out.println();
//		}
//		
		log.debug("############################################ EGRUSERSESSIONBEAN INIT() ############################################");
		uzId = 0L;

		dtStyle = Const.DT_STYLE_STANDARD;
		menuShow = true;
	}

	/*
	 *
	 *
	 * Getters & Setters
	 *
	 *
	 */

	public Long getUzId() {
		return uzId;
	}

	public void setUzId(Long uzId) {
		this.uzId = uzId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
		this.setUsernameUpperCase(this.username.toUpperCase());
		this.cUsername = WordUtils.capitalize(username);
	}

	public String getcUsername() {
		return cUsername;
	}

	public String getDbUsername() {
		return dbUsername;
	}

	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}
	
	public String getDtStyle() {
		return dtStyle;
	}

	public void setDtStyle(String dtStyle) {
		this.dtStyle = dtStyle;
	}

	public boolean isStyleStandard() {
		return this.dtStyle.equals(Const.DT_STYLE_STANDARD);
	}

	public boolean isStyleCompact() {
		return this.dtStyle.equals(Const.DT_STYLE_COMPACT);
	}

	public boolean isMenuShow() {
		return menuShow;
	}

	public void setMenuShow(boolean menuShow) {
		this.menuShow = menuShow;
	}

	public String getUsernameUpperCase() {
		return usernameUpperCase;
	}

	public void setUsernameUpperCase(String usernameUpperCase) {
		this.usernameUpperCase = usernameUpperCase;
	}	
	
	public Firma getWybranaFirma() {
		return wybranaFirma;
	}

	public void setWybranaFirma(Firma wybranaFirma) {
		this.wybranaFirma = wybranaFirma;
	}

	public Oddzial getWybranyOddzial() {
		return wybranyOddzial;
	}

	public void setWybranyOddzial(Oddzial wybranyOddzial) {
		this.wybranyOddzial = wybranyOddzial;
	}

	public String getSessionId() {
		return FacesContext.getCurrentInstance().getExternalContext().getSessionId(false);
	}
}
