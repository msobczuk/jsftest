package com.comarch.egeria.web.common.reports;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.utils.reports.CtlReportBean;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

@ManagedBean // JBossTools
@Named
@Scope("session")

/**
* pobranie nazwy i parametrów raportu i wywołanie jego generowania w danym formacie.
*/
public class JasperReportsUtils implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LogManager.getLogger(CtlReportBean.class);
	
	private HashMap<String, Object> reportParamsHM = new HashMap<String, Object>();
	private String selectedReport = "";
	private List<JRParameter> reportParameters = new ArrayList<>();


	@Inject
	protected ReportGenerator ReportGenerator;
	
	//TEST
	private String rapName="SzablonPortal";
	
	public List<String> getReportsNames() {
		List<String> results = new ArrayList<String>();
		String folderPath = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath("/WEB-INF/protectedElements/Reports/sourceReports/");

		File[] files = new File(folderPath).listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null. 

		for (File file : files) {
		    if (file.isFile() && !file.getName().startsWith("sub")) {
		        results.add(FilenameUtils.getBaseName(file.getName()));
		    }
		}
		return results;
	}
	
	public List<JRParameter> getReportParameters(String repName) throws JRException {
		reportParamsHM.clear();
		reportParameters.clear();
//		List<JRParameter> result = new ArrayList<JRParameter>();
		if("".equals(repName))
			return reportParameters;
		
		String filePath = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath("/WEB-INF/protectedElements/Reports/compiledReports/" + repName + ".jasper");
		JasperReport jasperReport = (JasperReport)JRLoader.loadObject(new File(filePath));
		JRParameter[] params = jasperReport.getParameters();

		
		for(JRParameter param : params) {
		  if(!param.isSystemDefined() && param.isForPrompting()){
			  reportParameters.add(param);
//		     param.getName();
//		     param.getDescription();
//		     param.getDefaultValueExpression();
//		     param.getNestedTypeName();
		  }
		}
		for (JRParameter jrParameter : reportParameters) {
			if(jrParameter.getValueClass().equals(Long.class))
				reportParamsHM.put(jrParameter.getName(), null);
			else if(jrParameter.getValueClass().equals(String.class))
				reportParamsHM.put(jrParameter.getName(), "");
			else
				reportParamsHM.put(jrParameter.getName(), null);
		}
		return reportParameters;
	}
	
	public void generateReportPdf() {
		try {
			HashMap<String, Object> param = prepareReportParameters();
			String rapName = getRapName();
			
			ReportGenerator.displayReportAsPDF(param, rapName);

		} catch (JRException | SQLException | IOException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
	}
	
	public void generateReportRtf() {
		try {
			HashMap<String, Object> param = prepareReportParameters();
			String rapName = getRapName();

			ReportGenerator.displayReportAsRTF(param, rapName);

		} catch (JRException | SQLException | IOException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
	}
	
	public void generateReportDocx() {
		try {
			HashMap<String, Object> param = prepareReportParameters();
			String rapName = getRapName();

			ReportGenerator.displayReportAsDOCX(param, rapName);

		} catch (JRException | SQLException | IOException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
	}
	
	public void generateReportOdt() {
		try {
			HashMap<String, Object> param = prepareReportParameters();
			String rapName = getRapName();


			ReportGenerator.displayReportAsODT(param, rapName);

		} catch (JRException | SQLException | IOException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
	}

	@PostConstruct
	protected void init() {
	}
//wstawiamy parametry raportu zwykle oznaczaja klucz głowny tabeli
	public HashMap<String, Object> prepareReportParameters() {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("reportTitle", new String("Tytuł raportu Szablon"));
		return params;
	}

	public String getRapName() {
		return rapName;
	}

	public void setRapName(String rapName) {
		this.rapName = rapName;
	}
	
	
	public HashMap<String, Object> getReportParamsHM() {
		return reportParamsHM;
	}

	public void setReportParamsHM(HashMap<String, Object> reportParamsHM) {
		this.reportParamsHM = reportParamsHM;
	}

	public HashMap<String, Object> getCopyReportParams() throws JRException {
		//Kopia, ponieważ JR dodaje jeszcze do niej elementy, a my korzystamy z reportParams w widoku
		HashMap<String, Object> ret = new HashMap<String, Object>(reportParamsHM);
		
		//Potrzebne jest rzutowanie ze String na Long, ponieważ komponenty Primefaces ustawiają jako typ String
		//Bez tego rzucany jest wyjątek ClassCastException ze String na Long
		
		for (String key : ret.keySet()) {
			for (JRParameter param : reportParameters) {
				if(key.equals(param.getName())) {
					Class<?> valueClass = param.getValueClass();
					if(String.class.equals(valueClass)) {
						ret.put(key, ret.get(key).toString());
					}
					else if(Long.class.equals(valueClass)) {
						ret.put(key, Long.parseLong((String) ret.get(key)));
						;
					}
				}
			}
		}
		
		return ret;
	}

	public String getSelectedReport() {
		return selectedReport;
	}

	public void setSelectedReport(String selectedReport) {
		this.selectedReport = selectedReport;
	}
	
	
}
