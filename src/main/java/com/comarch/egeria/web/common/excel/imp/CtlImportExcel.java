package com.comarch.egeria.web.common.excel.imp;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
//import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.file.UploadedFile;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.web.common.utils.lang.LanguageBean;

/**
 * @author CA
 *
 */
@Named
@Scope("view")
public class CtlImportExcel implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger log = LogManager.getLogger(CtlImportExcel.class);

	private enum ImportType {
		IMPORT_PODZIELNIKA, IMPORT_ZLECEN
	}

	// Do obsługi pliku
	private Workbook workbook;

	// Do obsługi dialogu
	private String dlgName;
	private ImportType importType;
	private boolean afterDownloadF = false;
	private boolean correctFormatF = false;
	private boolean resultsF = false;

	// Do tabeli importu
	private Map<Integer, String> sheetsMap = new TreeMap<Integer, String>();
	private int selectedSheet = 0;
	private List<String> headerList = new ArrayList<String>();
	private List<List<String>> bodyList = new ArrayList<List<String>>();
	private String headerText = LanguageBean.translate("LBL10261_zrodlo");

	// Do błędów
	List<String> selectedRow;
	int lineCounter = 0;

	// Do podzielnika
	//private List<DoImportExport> dataToImport;
	//private Long osId, spId, mpkId, mpkId3, ceId, okId, rpdId;

	@PostConstruct
	public void init() {
		dlgName = LanguageBean.translate("LBL10166_import_zlecen");
		importType = ImportType.IMPORT_PODZIELNIKA;
	}

	public void prepareImportZlecen() {
		headerList.clear();
		bodyList.clear();
		sheetsMap.clear();
		dlgName = LanguageBean.translate("LBL10166_import_zlecen");
		importType = ImportType.IMPORT_ZLECEN;
		headerText = LanguageBean.translate("LBL10261_zrodlo");
	}

	public void prepareImportPodzielnika() {
		// Tymczasowe zmienne pomocnicze
		afterDownloadF = false;
		correctFormatF = false;
		resultsF = false;
		headerList.clear();
		bodyList.clear();
		sheetsMap.clear();
		selectedSheet = 0;
		lineCounter = 0;
		dlgName = LanguageBean.translate("LBL10166_import_zlecen");
		importType = ImportType.IMPORT_PODZIELNIKA;
		headerText = LanguageBean.translate("LBL10261_zrodlo");
	}

	// Obsługa przesłania pliku excel
	// Odczytywne są dostępne arkusze
	public void handleFileUpload(FileUploadEvent event) {

		afterDownloadF = false;
		try {
			UploadedFile excelFile = event.getFile();
			String fileName = excelFile.getFileName();
			String fileFormat = fileName.substring(fileName.indexOf("."));
			sheetsMap.clear();

			InputStream fileIS = excelFile.getInputStream();

			if (fileFormat.equals(".xls")) {
				workbook = new HSSFWorkbook(fileIS);
			} else if (fileFormat.equals(".xlsx")) {
				workbook = new XSSFWorkbook(fileIS);
			} else
				workbook = null;

			if (workbook != null) {
				afterDownloadF = true;
				for (Integer i = 0; i < workbook.getNumberOfSheets(); i++) {
					sheetsMap.put(i, workbook.getSheetName(i));
				}
				com.comarch.egeria.Utils.update("formDlgImportExcel:sheetSelect");
				onSheetChange();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// Obsługa wyboru arkusza
	// Odczytywane są wszystkie dane
	public void onSheetChange() {
		getSheetHeader(workbook.getSheetAt(selectedSheet));
		getSheetBody(workbook.getSheetAt(selectedSheet));

		// Walidacja formatu tabeli w arkuszu
		correctFormatF = validateColumns(headerList);
		if (!correctFormatF) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_WARN, "Info", "Niepoprawny układ kolumn."));
		}
		com.comarch.egeria.Utils.update("formDlgImportExcel:tblImport");
		com.comarch.egeria.Utils.executeScript("PF('dlgImportExcel').initPosition();");
	}

	public void executeImport() {

		if (correctFormatF) {

			switch (importType) {
//			case IMPORT_PODZIELNIKA:
//				// Zapisanie danych w postaci obiektu
//				dataToImport = convertData(bodyList);
//
//				// Pobranie dodatkowych danych z Bean'a potrzebnych do importu
//				ceId = podzielnikBean.getCentrumFiltr();
//				okId = podzielnikBean.getOkresFiltr();
//				rpdId = podzielnikBean.getRodzajFiltr();
//
//				// Zapis danych
//				try {
//					dekretyCzasuPracyBo.usunDekretyPodzielnika(ceId, okId, rpdId);
//				} catch (SQLException e) {
//					log.error("Błąd usuwania");
//					e.printStackTrace();
//				}
//				lineCounter = 0;
//				for (DoImportExport temp : dataToImport) {
//					// Na podstawie importowanych nazw wyszukanie id do
//					// importu
//					if (temp.getError().equals("")) {
//						if (mapData(temp))
//							try {
//								dekretyCzasuPracyBo.wstawDekret(rpdId, mpkId, temp.getIloscDni(), temp.getCalyCzas(),
//										temp.getDzienWolny(), osId, spId, okId, ceId, mpkId3);
//							} catch (SQLException e) {
//								log.error("Błąd wstawiania");
//								temp.setError("Błąd Oracle");
//								e.printStackTrace();
//							}
//					}
//				}
//
//				// Pozostawienie tylko błędnych wpisów
//				headerText = LanguageBean.translate("LBL10262_bledy");
//				dataToImport = dataToImport.stream().filter(x -> !x.getError().equalsIgnoreCase(""))
//						.collect(Collectors.toList());
//				List<Long> wrongId = new ArrayList<Long>();
//				for (DoImportExport temp : dataToImport) {
//					wrongId.add(temp.getId());
//				}
//				bodyList = bodyList.stream().filter(x -> wrongId.contains(Long.valueOf(x.get(0))))
//						.collect(Collectors.toList());
//
//				resultsF = true;
//
//				break;
//			case IMPORT_ZLECEN:
//				// TODO
//				break;
			default:

				break;
			}
		}
		//podzielnikBean.reset();
		com.comarch.egeria.Utils.update("frmDekrety:tblDekrety");
	}

	// Metoda zwracajaca nagłówki kolumn z danego arkusza
	private List<String> getSheetHeader(Sheet sheet) {

		headerList.clear();
		Row row = sheet.getRow(0);
		Cell cell;
		Iterator<Cell> cells = row.cellIterator();
		while (cells.hasNext()) {
			cell = cells.next();
			if (cell.getCellType() == CellType.STRING) {
				headerList.add(cell.getStringCellValue());
			} else if (cell.getCellType() == CellType.NUMERIC) {
				headerList.add(String.valueOf(cell.getNumericCellValue()));
			} else {
				log.warn("Formuły nie są obsługiwane");
			}

		}
		return headerList;
	}

	// Metoda zwracająca zawartośc tabeli z danego arkusza
	private List<List<String>> getSheetBody(Sheet sheet) {

		bodyList.clear();
		int colCount = 0;
		Row row;
		Cell cell;
		Iterator<Row> rows = sheet.rowIterator();
		// Pominięcie pierwszego wiersza z nazwami kolumn
		if (rows.hasNext()) {
			row = rows.next();
			colCount = row.getLastCellNum();
		}
		while (rows.hasNext()) {
			List<String> rowList = new ArrayList<String>();
			row = rows.next();
			rowList.add(Integer.toString(row.getRowNum()));
			for (int i = 0; i < colCount; i++) {
				cell = row.getCell(i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
				if (cell == null) {
					rowList.add(null);
				} else if (cell.getCellType() == CellType.STRING) {
					rowList.add(cell.getStringCellValue());
				} else if (cell.getCellType() == CellType.NUMERIC) {
					rowList.add(String.valueOf(cell.getNumericCellValue()));
				} else {
					rowList.add(null);
				}
			}
			bodyList.add(rowList);
		}
		return bodyList;
	}

	// Meotda sprawdzajaca poprawność kolumn
	private boolean validateColumns(List<String> columns) {
		boolean flag = false;
		switch (importType) {
		case IMPORT_PODZIELNIKA:
			if (columns.get(0).equalsIgnoreCase(LanguageBean.translate("LBL10050_spolka"))
					&& columns.get(1).equalsIgnoreCase(LanguageBean.translate("LBL10017_imie"))
					&& columns.get(2).equalsIgnoreCase(LanguageBean.translate("LBL10034_nazwisko"))
					&& columns.get(3).equalsIgnoreCase(LanguageBean.translate("LBL10021_kod_RD"))
					&& columns.get(4).equalsIgnoreCase(LanguageBean.translate("LBL10187_kod_RD_2"))
					&& columns.get(5).equalsIgnoreCase(LanguageBean.translate("LBL10188_ilosc_dni"))
					&& columns.get(6).equalsIgnoreCase(LanguageBean.translate("LBL10189_dzien_wolny"))
					&& columns.get(7).equalsIgnoreCase(LanguageBean.translate("LBL10190_caly_czas")))
				flag = true;
			else {
				log.error("Niepoprawny układ kolumn");
				flag = false;
			}
			break;
		case IMPORT_ZLECEN:
			// TODO
			flag = false;
			break;
		default:
			flag = false;
			break;
		}
		return flag;
	}

	// --------------------------------------------------------------
	// Do wydzielenia jako osobna klasa
	// --------------------------------------------------------------

	// Metoda parsująca dane do odpowiedniego formatu
//	private List<DoImportExport> convertData(List<List<String>> rawData) {
//		List<DoImportExport> data = new ArrayList<DoImportExport>();
//		for (List<String> temp : rawData) {
//			DoImportExport record = new DoImportExport();
//			if (isNumeric(temp.get(6))) {
//				record.setId(Long.valueOf(temp.get(0)));
//				record.setSpolka(temp.get(1));
//				record.setImie(temp.get(2));
//				record.setNazwisko(temp.get(3));
//				record.setMpkKod(temp.get(4));
//				record.setMpkKod3(temp.get(5));
//				if (temp.get(6) != null)
//					record.setIloscDni(Double.parseDouble(temp.get(6)));
//				else
//					record.setIloscDni(0D);
//				if (temp.get(7) != null)
//					record.setDzienWolny(temp.get(7));
//				else
//					record.setDzienWolny("N");
//				if (temp.get(8) != null)
//					record.setCalyCzas(temp.get(8));
//				else
//					record.setCalyCzas("N");
//				record.setError("");
//			} else {
//				record.setId(Long.valueOf(temp.get(0)));
//				record.setError("Błąd formatu danych");
//			}
//			data.add(record);
//		}
//		return data;
//	}
//
//	// Metoda mapująca nazwy na id i sprawdzajaca poprawnosc danych
//	private boolean mapData(DoImportExport record) {
//
//		lineCounter++;
//		boolean fCorrectData = true;
//		List<DoPodzielnika> przydzialy;
//		DoPodzielnika przydzialyS;
//		List<MpkPrzypisane> mpkPrzypisane;
//		MpkPrzypisane mpkS;
//		List<Mpk3> mpk3;
//		Mpk3 mpk3S;
//
//		if (record.getSpolka() != null && record.getNazwisko() != null && record.getImie() != null) {
//			przydzialy = podzielnikBean.getPrzydzialy();
//			przydzialyS = przydzialy.stream()
//					.filter(x -> x.getSpNazwa().equalsIgnoreCase(record.getSpolka())
//							&& x.getOsNazwisko().equalsIgnoreCase(record.getNazwisko())
//							&& x.getOsImie().equalsIgnoreCase(record.getImie()))
//					.findFirst().orElse(null);
//			if (przydzialyS != null) {
//				osId = przydzialyS.getOsId();
//				spId = przydzialyS.getSpId();
//				try {
//					// Czy podano MPK
//					if (record.getMpkKod() != null) {
//						mpkPrzypisane = podzielnikBo.sqlMpkPrzypisane(ceId, okId, rpdId, spId);
//
//						mpkS = mpkPrzypisane.stream().filter(x -> x.getMpkKod().equalsIgnoreCase(record.getMpkKod()))
//								.findFirst().orElse(null);
//						// Czy dane MPK istnieje
//						if (mpkS != null) {
//							mpkId = mpkS.getMpkId();
//							// Czy podano MPK3 / jest przepisane z MPK
//							if (record.getMpkKod3() != null && record.getMpkKod3() != record.getMpkKod()) {
//								mpk3 = dekretyCzasuPracyBo.sqlMpk3(ceId, mpkId);
//								Mpk3 temp = mpk3.stream().filter(x -> x.getMpkId() == 0).findFirst().orElse(null);
//								if (temp != null) {
//									mpk3.remove(temp);
//								}
//								// Czy istnieje lista MPK3
//								if (mpk3.size() > 0) {
//									mpk3S = mpk3.stream()
//											.filter(x -> x.getMpkKod().equalsIgnoreCase(record.getMpkKod3()))
//											.findFirst().orElse(null);
//									// Czy dane MPK3 istnieje
//									if (mpk3S != null) {
//										mpkId3 = mpk3S.getMpkId();
//									} else if (record.getMpkKod3().equalsIgnoreCase(record.getMpkKod())) {
//										mpkId3 = null;
//									} else {
//										log.error("Nie znaleziono MPK3");
//										fCorrectData = false;
//									}
//								} else {
//									log.error("Nie znaleziono listy MPK3");
//									fCorrectData = false;
//								}
//							} else {
//								mpkId3 = null;
//							}
//						} else {
//							log.error("Nie znaleziono MPK lub pusty rekord");
//							fCorrectData = false;
//						}
//					} else {
//						log.error("Nie podano MPK");
//						fCorrectData = false;
//					}
//				} catch (SQLException e) {
//					log.error("Błąd pobierania mpk podczas importu.");
//					e.printStackTrace();
//				}
//			} else {
//				log.error("Nie znaleziono Pracownika Centrum");
//				fCorrectData = false;
//			}
//		} else {
//			log.error("Nie podano Pracownika Centrum");
//			fCorrectData = false;
//		}
//
//		if (record.getMpkKod() != null)
//			if (record.getMpkKod().indexOf("RD") == 0) {
//				record.setMpkKod(record.getMpkKod().substring(2));
//			}
//
//		if (record.getMpkKod3() != null)
//			if (record.getMpkKod3().indexOf("RD") == 0) {
//				record.setMpkKod3(record.getMpkKod3().substring(2));
//			}
//
//		if (record.getIloscDni() == 0) {
//			log.error("Nie mozna zadekretować 0 dni.");
//			fCorrectData = false;
//			record.setError("0 dni do dekretacji");
//		} else if (osId == null) {
//			record.setError("Nie znaleziono pracownika");
//		} else if (spId == null) {
//			record.setError("Nie znaleziono spółki");
//		} else if (mpkId == null) {
//			record.setError("Nie znaleziono rodzaju działalności");
//		}
//		return fCorrectData;
//	}

	public void onRowSelect(SelectEvent event) {
//		if (resultsF) {
//			String error = dataToImport.stream().filter(x -> x.getId() == Long.valueOf((selectedRow.get(0))))
//					.findFirst().orElse(null).getError();
//			if (!error.equals(""))
//				FacesContext.getCurrentInstance().addMessage(null,
//						new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", error));
//		}
	}

	// Po odznaczeniu wiersza w przydziałach zgaś flagę
	public void onRowUnselect(UnselectEvent event) {

	}

	/* Getters & Setters */
	public boolean isAfterDownloadF() {
		return afterDownloadF;
	}

	public void setAfterDownloadF(boolean afterDownloadF) {
		this.afterDownloadF = afterDownloadF;
	}

	public boolean isCorrectFormatF() {
		return correctFormatF;
	}

	public void setCorrectFormatF(boolean correctFormatF) {
		this.correctFormatF = correctFormatF;
	}

	public Map<Integer, String> getSheetsMap() {
		return sheetsMap;
	}

	public void setSheetsMap(Map<Integer, String> sheetsMap) {
		this.sheetsMap = sheetsMap;
	}

	public int getSelectedSheet() {
		return selectedSheet;
	}

	public void setSelectedSheet(int selectedSheet) {
		this.selectedSheet = selectedSheet;
	}

	public List<String> getHeaderList() {
		return headerList;
	}

	public void setHeaderList(List<String> headerList) {
		this.headerList = headerList;
	}

	public List<List<String>> getBodyList() {
		return bodyList;
	}

	public void setBodyList(List<List<String>> bodyList) {
		this.bodyList = bodyList;
	}

	public String getDlgName() {
		return dlgName;
	}

	public void setDlgName(String dlgName) {
		this.dlgName = dlgName;
	}

	public boolean isResultsF() {
		return resultsF;
	}

	public void setResultsF(boolean resultsF) {
		this.resultsF = resultsF;
	}

	public List<String> getSelectedRow() {
		return selectedRow;
	}

	public void setSelectedRow(List<String> selectedRow) {
		this.selectedRow = selectedRow;
	}

	public String getHeaderText() {
		return headerText;
	}

	public void setHeaderText(String headerText) {
		this.headerText = headerText;
	}

}
