package com.comarch.egeria.web.common.utils.reports;

import java.io.Serializable;

import javax.inject.Named;

import org.springframework.context.annotation.Scope;

@Named
@Scope("session")
public class CtlGridReportUtils implements Serializable {

	private static final long serialVersionUID = 1L;

//	public void generateGridReportPdf(List<RapFormSzczegoly> daneCentrum)  {
//
//		try {
//			JasperReportBuilder report = DynamicReports.report();
//
//			report.columns(Columns.column("Okres", "okMiesiacLong", DataTypes.stringType()),
//					Columns.column("Data", "dnData", DataTypes.dateType()),
//					Columns.column("Kwota", "dnKwota", DataTypes.doubleType()),
//					Columns.column("Waluta", "walSymbol", DataTypes.stringType()))
//					.title(Components.text("SimpleReportExample")).pageFooter(Components.pageXofY())
//					.setDataSource(new JRBeanCollectionDataSource(daneCentrum));
//
//			JasperPrint jasperPrint;
//
//			jasperPrint = report.toJasperPrint();
//
//			HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
//					.getResponse();
//			response.setHeader("Content-Type", "application/pdf");
//			response.setHeader("Content-Disposition", "inline;filename=\"" + "repName" + ".pdf" + "\"");
//			ServletOutputStream stream = response.getOutputStream();
//
//			// JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
//			JRPdfExporter exporter = new JRPdfExporter();
//			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
//			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
//			exporter.exportReport();
//
//			FacesContext.getCurrentInstance().responseComplete();
//
//		} catch (JRException | IOException | DRException e) {
//			e.printStackTrace();
//		}
//
//	}

	public void generateGridReportXlsx() {
		// TODO 
	}

	public void generateGridReportHtml() {
		// TODO 
	}

	public void generateGridReportRtf() {
		// TODO 
	}

	public void generateGridReportDocx() {
		// TODO 
	}

	public void generateGridReportCsv() {
		// TODO 
	}



}