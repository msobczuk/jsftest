package com.comarch.egeria.web.common.help.dao.impl;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Properties;

import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.web.common.help.dao.HelpDAO;
import com.comarch.egeria.web.common.help.exceptions.HelpAccessException;
import com.comarch.egeria.web.common.help.exceptions.HelpIOException;
import com.comarch.egeria.web.common.help.exceptions.HelpNotFoundException;
import com.comarch.egeria.web.common.help.exceptions.HelpPersistenceException;
import com.comarch.egeria.web.common.utils.file.FileUtils;

import oracle.jdbc.internal.OracleTypes;

@Named
@Transactional
public class HelpDAOImpl implements HelpDAO {

	private static final Logger log = LoggerFactory.getLogger(HelpDAOImpl.class);


	private static String readFileAsString(String filePath, String charset)
	{
		StringBuilder contentBuilder = new StringBuilder();
		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(
						new FileInputStream(filePath), StandardCharsets.UTF_8)))
		{
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null)
			{
				contentBuilder.append(sCurrentLine).append("\n");
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return contentBuilder.toString();
	}




	@Override
	public int persist(String modul, String region, String title, String docFilename, String htmlFilename,
			List<String> imgFilenames, String charset) throws HelpPersistenceException, HelpIOException {

		Connection conn = null;
		File docFile = new File(docFilename);
		File htmlFile = new File(htmlFilename);
		int version;

		try {
			try {
				conn = JdbcUtils.getConnection();
				conn.setAutoCommit(false);

				try (InputStream docFis = new FileInputStream(docFile);
						InputStream htmlFis = new FileInputStream(htmlFile);
						Reader rdr = new InputStreamReader(new FileInputStream(htmlFile), charset);
						CallableStatement cs = conn
								.prepareCall("{? = call eap_web_pomoc.wstaw_karte_pomocy(?, ?, ?, ?, ?, ?, ?)}")) {

					String[] docFileNameAndExt = FileUtils.extractFileNameAndExt(docFile.getName());

					log.debug("Zapisywanie karty pomocy w bazie danych [moduł={}, obszar={}, tytuł={}, nazwaPliku={}]",
							modul, region == null ? "brak" : region, title, docFileNameAndExt[0]);

					cs.registerOutParameter(1, OracleTypes.NUMBER);
					cs.setString(2, modul);

					if (region != null)
						cs.setString(3, region);
					else
						cs.setNull(3, OracleTypes.VARCHAR);
					cs.setString(4, title);
					cs.setString(5, docFileNameAndExt[0]);
					cs.setString(6, docFileNameAndExt[1]);
					cs.setBinaryStream(7, docFis, docFile.length());
					//cs.setCharacterStream(8, rdr, htmlFile.length());
					String txt = readFileAsString(htmlFilename, charset);
					cs.setString(8, txt);

					cs.execute();

					version = cs.getInt(1);
				}
				if (!imgFilenames.isEmpty()) {
					try (CallableStatement cs = conn
							.prepareCall("{call eap_web_pomoc.wstaw_plik_karty_pomocy(?, ?, ?, ?)}")) {
						cs.setString(1, title);

						log.debug("Zapisywanie plików karty pomocy w bazie danych w liczbie {}", imgFilenames.size());

						for (String imgFilename : imgFilenames) {
							File imgFile = new File(imgFilename);

							String[] fileNameAndExt = FileUtils.extractFileNameAndExt(imgFile.getName());
							cs.setString(2, fileNameAndExt[0]);
							cs.setString(3, fileNameAndExt[1]);

							try (InputStream imgFis = new FileInputStream(imgFile)) {
								cs.setBinaryStream(4, imgFis, imgFile.length());
								cs.execute();
							}
						}
					}
				}

				conn.commit();
			} catch (SQLException e) {
				conn.rollback();
				throw new HelpPersistenceException("Błąd przy zapisywaniu karty pomocy w bazie danych", e);
			} catch (IOException e) {
				conn.rollback();
				throw new HelpIOException("Błąd przy odczytywaniu plików karty pomocy z dysku", e);
			} finally {
				conn.close();
			}
		} catch (SQLException e) {
			throw new HelpPersistenceException("Krytyczny błąd związany z bazą danych", e);
		}

		log.debug("Zapisano kartę pomocy w bazie danych [wersja={}]", version);

		return version;
	}

	@Override
	public String getHtmlFilename(String modul, String region, String dir, String imgDir, String charset)
			throws HelpAccessException, HelpIOException {

		int version;
		String title;
		File helpHtmlFile;

		try (Connection conn = JdbcUtils.getConnection()) {
			try (CallableStatement cs = conn
					.prepareCall("{? = call eap_web_pomoc.pobierz_karte_pomocy(?, ?, ?, ?, ?)}")) {

				log.debug("Pobieranie pliku HTML karty pomocy z bazy danych [moduł={}, obszar={}]", modul,
						region == null ? "brak" : region);

				cs.registerOutParameter(1, OracleTypes.NUMBER);
				cs.setString(2, modul);

				if (region != null)
					cs.setString(3, region);
				else
					cs.setNull(3, OracleTypes.VARCHAR);

				cs.registerOutParameter(4, OracleTypes.VARCHAR);
				cs.registerOutParameter(5, OracleTypes.VARCHAR);
				cs.registerOutParameter(6, OracleTypes.CLOB);
				cs.execute();

				version = cs.getInt(1);

				if (version == 0) return null;

				title = cs.getString(4);
				helpHtmlFile = new File(dir, cs.getString(5) + ".html");

				try (OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(helpHtmlFile), charset);
						Reader reader = cs.getCharacterStream(6)) {

					log.debug("Zapisywanie pliku HTML karty pomocy na dysku]");

					int c;
					while ((c = reader.read()) != -1)
						writer.write(c);

				}

				Properties prop = new Properties();
				try (OutputStream out = new FileOutputStream(new File(dir, "help.properties"))) {

					log.debug("Tworzenie pliku 'help.properties' karty pomocy na dysku]");

					prop.setProperty("title", title);
					prop.setProperty("version", String.valueOf(version));
					prop.store(out, null);
				}
			}

			try (CallableStatement cs = conn.prepareCall("{? = call eap_web_pomoc.pobierz_pliki_karty_pomocy(?, ?)}")) {

				log.debug("Pobieranie plików karty pomocy z bazy danych [tytułKarty={}]", title);

				cs.registerOutParameter(1, OracleTypes.NUMBER);
				cs.setString(2, title);
				cs.registerOutParameter(3, OracleTypes.CURSOR);
				cs.execute();

				int imgCount = cs.getInt(1);

				try (ResultSet rs = (ResultSet) cs.getObject(3)) {
					int counter = 0;

					while (rs.next()) {
						try (InputStream in = rs.getBinaryStream("kpp_plik");
								FileOutputStream fos = new FileOutputStream(new File(imgDir,
										rs.getString("kpp_nazwa") + "." + rs.getString("kpp_rozszerzenie")))) {

							counter++;
							log.debug("Pobieranie i zapisywanie {}. pliku karty pomocy na dysku", counter);

							int b;
							while ((b = in.read()) != -1)
								fos.write(b);
						}
					}

					if (imgCount != counter) {
						// TODO
					}
				}
			}
		} catch (SQLException e) {
			throw new HelpAccessException("Błąd przy pobieraniu karty pomocy z bazy danych", e);
		} catch (IOException e) {
			throw new HelpIOException(
					"Błąd przy zapisywaniu pliku HTML karty pomocy na dysku podczas pobierania z bazy danych", e);
		}

		log.debug("Pobrano plik HTML karty pomocy z bazy danych [ścieżkaDoPliku={}]", helpHtmlFile.getPath());

		return helpHtmlFile.getPath();
	}

	@Override
	public int getCurrentVersion(String title) throws HelpAccessException {
		log.debug("Pobieranie wersji karty pomocy z bazy danych[tytuł={}]", title);

		int version;

		try (Connection conn = JdbcUtils.getConnection();
				CallableStatement cs = conn.prepareCall("{? = call eap_web_pomoc.pobierz_wersje_karty_pomocy(?)}")) {
			cs.registerOutParameter(1, OracleTypes.NUMBER);
			cs.setString(2, title);

			cs.execute();
			version = cs.getInt(1);
		} catch (SQLException e) {
			throw new HelpAccessException("Błąd przy pobieraniu wersji karty pomocy z bazy danych", e);
		}

		log.debug("Pobrano wersję karty pomocy z bazy danych [wersja={}]", version);

		return version;
	}

	@Override
	public int getCurrentVersion(String title, String modul, String region) throws HelpAccessException {
		log.debug("Pobieranie wersji karty pomocy z bazy danych[tytuł={}, moduł={}, obszar={}]", title, modul,
				region == null ? "brak" : region);

		int version;

		try (Connection conn = JdbcUtils.getConnection();
				CallableStatement cs = conn
						.prepareCall("{? = call eap_web_pomoc.pobierz_wersje_karty_pomocy(?, ?, ?)}")) {
			cs.registerOutParameter(1, OracleTypes.NUMBER);
			cs.setString(2, title);
			cs.setString(3, modul);

			if (region != null)
				cs.setString(4, region);
			else
				cs.setNull(4, OracleTypes.VARCHAR);

			cs.execute();
			version = cs.getInt(1);
		} catch (SQLException e) {
			throw new HelpAccessException("Błąd przy pobieraniu wersji karty pomocy z bazy danych", e);
		}

		log.debug("Pobrano wersję karty pomocy z bazy danych [wersja={}]", version);

		return version;
	}
}