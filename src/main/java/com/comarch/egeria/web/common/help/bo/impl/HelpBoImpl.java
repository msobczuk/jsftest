package com.comarch.egeria.web.common.help.bo.impl;

import com.comarch.egeria.ApachePOIUtils;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.web.common.help.bo.HelpBo;
import com.comarch.egeria.web.common.help.dao.HelpDAO;
import com.comarch.egeria.web.common.help.exceptions.*;
import com.comarch.egeria.web.common.utils.constants.Const;
import com.comarch.egeria.web.common.utils.file.FileUtils;
import fr.opensagres.poi.xwpf.converter.core.ImageManager;
import fr.opensagres.poi.xwpf.converter.core.XWPFConverterException;
import fr.opensagres.poi.xwpf.converter.xhtml.XHTMLConverter;
import fr.opensagres.poi.xwpf.converter.xhtml.XHTMLOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.primefaces.model.file.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HelpBoImpl implements HelpBo {

	private static final Logger log = LoggerFactory.getLogger(HelpBoImpl.class);

	private static final String MAIN_DIR = "help";
	private static final String IMAGES_DIR = "images";
	private static final String CHARSET = "UTF-8";
	private static final String APP_CONTEXT = Const.HOME_REDIRECT_URL.substring(1);// "/PortalPracowniczy" - > "PortalPracowniczy";
	public static final String DOC_RECOURCES_DIR = "/WEB-INF/protectedElements/DOC";

	@Inject
	private HelpDAO helpDAO;

	private String helpLocation;

	@Override
	public String saveFile(UploadedFile file, String modul, String region)
			throws HelpPersistenceException, HelpConversionException, HelpIOException {
		
		log.debug("Zapisywanie nowej karty pomocy");

		File htmlFile;

		try {
			String baseDirPath = createBaseDirPath(modul, region);
			String imgDirPath = createImgDirPath(baseDirPath);
			deleteFileAndImages(baseDirPath, imgDirPath);

			File docFile = saveAsDocFile(baseDirPath, file);
			String fileName = FileUtils.extractFileNameAndExt(file.getFileName())[0];

			htmlFile = convertToHtmlFile(docFile, fileName, baseDirPath);
			
			List<String>  imgFilenames = Files.list(Paths.get(imgDirPath))
					.map(p -> p.toAbsolutePath().toString()).collect(Collectors.toList());

			String title = fileName;
			if (title.length() > 30)
				title = title.substring(0, 30);

			int version = helpDAO.persist(modul, region, title, docFile.getPath(), htmlFile.getPath(), imgFilenames,
					CHARSET);

			createHelpPropertiesFile(baseDirPath, title, version);

			docFile.delete();
			addCSS(htmlFile.getPath());
		} catch (IOException e) {
			throw new HelpIOException("Błąd I/O podczas zapisywania karty pomocy", e);
		}

		log.debug("Zapisano nową kartę pomocy [ścieżkaDoPlikuHTML={}]", htmlFile.getPath());

		return createURL(modul, region, htmlFile.getName());
	}

	@Override
	public String getHtmlFilename(String modul, String region)
			throws HelpNotFoundException, HelpAccessException, HelpConversionException, HelpIOException {
		
		log.debug("Pobieranie ścieżki do pliku HTML karty pomocy");
		System.out.println("getHtmlFilename...");

		String htmlFilename = null;

		try {
			String baseDirPath = createBaseDirPath(modul, region);
			String imgDirPath = createImgDirPath(baseDirPath);

//			htmlFilename = findHtmlFile(modul, region, baseDirPath, imgDirPath);

			if (htmlFilename == null) {
				htmlFilename = obtainHtmlFilename(modul, region, baseDirPath, imgDirPath);
			}
		} catch (IOException e) {
			throw new HelpIOException("Błąd I/O podczas pobierania karty pomocy", e);
		}

		log.debug("Pobrano ścieżkę do pliku HTML karty pomocy [ścieżka={}]", htmlFilename);

		return createURL(modul, region, htmlFilename);
	}
	
	private String createBaseDirPath(String modul, String region) {
		return (region != null ? Paths.get(helpLocation, APP_CONTEXT, modul, region)
				: Paths.get(helpLocation, APP_CONTEXT, modul)).toString();
	}
	
	private String createImgDirPath(String baseDirPath) throws IOException {
		return Files.createDirectories(Paths.get(baseDirPath, IMAGES_DIR)).toString();
	}

	private void deleteFileAndImages(String baseDirPath, String imgDirPath) throws HelpIOException {
		log.debug("Usuwanie plików karty pomocy z dysku");

		try {
			Stream.concat(Files.walk(Paths.get(baseDirPath)), Files.walk(Paths.get(imgDirPath)))
					.filter(Files::isRegularFile).map(Path::toFile).forEach(File::delete);
		} catch (IOException e) {
			throw new HelpIOException("Błąd I/O przy usuwaniu starych plików pomocy", e);
		}
	}
	
	private File saveAsDocFile(String dir, UploadedFile file) throws IOException {
		File docFile = new File(dir, file.getFileName());
		try (InputStream in = file.getInputStream(); FileOutputStream fos = new FileOutputStream(docFile)) {
			int b;
			while ((b = in.read()) != -1)
				fos.write(b);
		}

		return docFile;
	}
	
	private File convertToHtmlFile(File docFile, String fileName, String baseDirPath) throws HelpConversionException {
		
		log.debug("Konwersja dokumentu DOC(X) do pliku HTML");

		File htmlFile;

		try {
			XWPFDocument document = new XWPFDocument(new FileInputStream(docFile));
			XHTMLOptions options = XHTMLOptions.create().setImageManager( new ImageManager( new File(baseDirPath), IMAGES_DIR ) );
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			XHTMLConverter.getInstance().convert(document, out, options);
			
			htmlFile = new File(baseDirPath, fileName + ".html");
			try (Writer writer = new PrintWriter(htmlFile, CHARSET)) {
				writer.write(parseHTML(out.toString()));
			}
		} catch (XWPFConverterException | IOException e) {
			throw new HelpConversionException("Błąd podczas konwersji DOC(X)->HTML", e);
		}

		return htmlFile;
	}
	
	private String parseHTML(String html) {
		log.debug("Parsowanie tekstu HTML");
		
		Document doc = Jsoup.parse(html);
		doc.outputSettings().syntax(Document.OutputSettings.Syntax.xml).charset(CHARSET);
		doc.head().appendElement("meta").attr("charset", CHARSET);
		doc.select("div").first().attr("style",
				"margin-bottom:12.0pt;margin-top:11.0pt;margin-left:11.0pt;margin-right:11.0pt;");
		
		return doc.toString();
	}
	
	private void createHelpPropertiesFile(String dirPath, String title, int version) throws IOException {
		Properties props = new Properties();
		try (OutputStream fos = new FileOutputStream(new File(dirPath, "help.properties"))) {
			props.setProperty("title", title);
			props.setProperty("version", String.valueOf(version));
			props.store(fos, null);
		}
	}
	
	private void addCSS(String htmlFilename) throws HelpIOException {
		log.debug("Dodawanie arkusza stylów CSS do pliku HTML");

		try {
			File htmlFile = new File(htmlFilename);
			Document doc = Jsoup.parse(htmlFile, CHARSET);
			doc.head().appendElement("link").attr("type", "text/css").attr("rel", "stylesheet").attr("href",
					"/" + APP_CONTEXT + "/javax.faces.resource/egr-help-style.css.xhtml?ln=css");

			try (Writer writer = new PrintWriter(htmlFile, CHARSET)) {
				writer.write(doc.html());
			}
		} catch (IOException e) {
			throw new HelpIOException("Błąd I/O podczas dodawania arkusza stylów CSS do pliku HTML", e);
		}
	}

	private String createURL(String modul, String region, String fileName) {
		return (region != null ? Paths.get("..", MAIN_DIR, APP_CONTEXT, modul, region, fileName)
				: Paths.get("..", MAIN_DIR, APP_CONTEXT, modul, fileName)).toString();
	}

	private String findHtmlFile(String modul, String region, String baseDirPath, String imgDirPath)
			throws HelpAccessException, HelpIOException {
		
		log.debug("Szukanie pliku HTML karty pomocy w systemie plików");
		
		String htmlFilename = null;
		
		try {
			htmlFilename = Files.list(Paths.get(baseDirPath)).filter(path -> path.toString().endsWith(".html"))
					.findFirst().get().getFileName().toString();
			
			try (InputStream in = new FileInputStream(new File(baseDirPath, "help.properties"))) {
				Properties prop = new Properties();
				prop.load(in);
				
				if (Integer.parseInt(prop.getProperty("version", "-1")) != helpDAO
						.getCurrentVersion(prop.getProperty("title"), modul, region)) {
					log.debug("Plik HTML istnieje, ale jest nieaktualny");
					deleteFileAndImages(baseDirPath, imgDirPath);
				}
			} catch (IOException e) {
				deleteFileAndImages(baseDirPath, imgDirPath);
				htmlFilename = null;
			}
		} catch (IOException e) {
			log.error("Wystąpił błąd podczas odczytywania pliku pomocy", e);
			deleteFileAndImages(baseDirPath, imgDirPath);
		} catch (NoSuchElementException e) {
			log.debug("Nie znaleziono pliku HTML karty pomocy");
		}
		
		return htmlFilename;
	}
	
	private String obtainHtmlFilename(String modul, String region, String baseDirPath, String imgDirPath)
			throws HelpNotFoundException, HelpAccessException, HelpIOException, HelpConversionException {
//		String htmlFilename = helpDAO.getHtmlFilename(modul, region, baseDirPath, imgDirPath, CHARSET);
		String htmlFilename = null;

		if(htmlFilename == null) {
			htmlFilename = getHtmlFilenameFromResources(modul, region, baseDirPath);
		}

		addCSS(htmlFilename);
		return Paths.get(htmlFilename).getFileName().toString();
	}

	private String getHtmlFilenameFromResources(String modul, String region, String baseDirPath)
			throws HelpNotFoundException, HelpIOException, HelpConversionException {

		try {

			System.out.println("getHtmlFilenameFromResources...");

			String fileName = modul + (region != null ? "_" + region : "");
			Path docsPath = Paths.get(AppBean.app.getApplicationContextFolder().getAbsolutePath(), DOC_RECOURCES_DIR);
			Path pathInstrukcjaPPdocx = Paths.get(AppBean.app.getApplicationContextFolder().getAbsolutePath(), DOC_RECOURCES_DIR, "Instrukcja PP.docx");


			Path docFilePath = Files.walk(docsPath)
					.filter(f -> Files.isRegularFile(f) && f.toFile().getName().startsWith(fileName + ".docx"))
					.findFirst()
					.orElse(null);

			if (docFilePath == null) { //pliku docx pomocy kontekstwej nie ma
				ApachePOIUtils.testApachePOIInstrukcjaPPSplitBodyElements(pathInstrukcjaPPdocx, fileName);
				docFilePath = Paths.get(AppBean.app.getApplicationContextFolder().getAbsolutePath(), DOC_RECOURCES_DIR, fileName + ".docx");
			} else { //plik docx jest ale pytanie czy aktualny?
				long lmInstrukcjaPP = pathInstrukcjaPPdocx.toFile().lastModified();
				long lmPomocKontekstowa = docFilePath.toFile().lastModified();
				if(lmPomocKontekstowa < lmInstrukcjaPP) {
					ApachePOIUtils.testApachePOIInstrukcjaPPSplitBodyElements(pathInstrukcjaPPdocx, fileName);
				}
			}


//					.orElseThrow(() -> new HelpNotFoundException("Nie znaleziono karty pomocy"));


			File htmlFile = convertToHtmlFile(docFilePath.toFile(), fileName, baseDirPath);
			System.out.println("...getHtmlFilenameFromResources");
			return htmlFile.getPath();
		} catch (IOException e) {
			throw new HelpIOException("Błąd I/O podczas wyszukiwania pliku DOC(X) w zasobach", e);
		}

	}

	public String getHelpLocation() {
		return helpLocation;
	}

	public void setHelpLocation(String helpLocation) {
		this.helpLocation = helpLocation;
	}
}
