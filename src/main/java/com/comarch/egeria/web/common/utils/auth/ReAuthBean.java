package com.comarch.egeria.web.common.utils.auth;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.menu.EgrMenuBean;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@Scope("view")
public class ReAuthBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 8281063341812990283L;
	private static final Logger log = LogManager.getLogger();
	
	@Inject
	EgrLoginBean egrLoginBean;
	
	@Inject
	EgrMenuBean egrMenuBean;
	
	@Inject
	User user;
	
	@PostConstruct
	public void init(){
	}

	public void reAuth(DataRow usersDR) {
		if (usersDR == null)
			return;
		
		String username = usersDR.get("uzt_nazwa_zewn").toString();
		reAuth(username);
	}

	public void reAuth(String username) {
		
		if (!user.isAdmin() && !user.hasPermission("PP_SU")){
			User.warn("Do wykonania tej operacji konieczne są uprawnienia administratora systemu (albo funkcja użytkowa PP_SU).");
			return;
		}
		
		if (username==null)
			return;

		com.comarch.egeria.Utils.executeScript("window.top.location.reload(true);");
		egrLoginBean.reAuth(username);
		egrMenuBean.reloadMenuModel();
		com.comarch.egeria.Utils.executeScript("parent.initWebsocket();");
	}
	
}
