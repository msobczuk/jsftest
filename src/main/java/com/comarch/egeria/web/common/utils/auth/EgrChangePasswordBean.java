package com.comarch.egeria.web.common.utils.auth;

import com.comarch.egeria.web.common.utils.auth.exceptions.PasswordValidationException;
import com.comarch.egeria.web.common.utils.auth.exceptions.RepeatedPasswordException;
import com.comarch.egeria.web.common.utils.auth.exceptions.SpecialPasswordException;
import com.comarch.egeria.web.common.utils.auth.model.PasswordResetToken;
import com.comarch.egeria.web.common.utils.auth.pki.EgrPKIAuth;
import com.comarch.egeria.web.common.utils.auth.pki.EgrPKICertificateEmailNotFoundException;
import com.comarch.egeria.web.common.utils.auth.pki.EgrPKICertificateNotFoundException;
import com.comarch.egeria.web.common.utils.auth.pki.EgrPKICertificateWrongIssuerException;
import com.comarch.egeria.web.common.utils.auth.utils.PasswordResetterService;
import com.comarch.egeria.web.common.utils.constants.Const;
import com.comarch.egeria.web.common.utils.lang.LanguageBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;
import java.sql.SQLException;
import java.util.Optional;

@Named
@Scope("view")
public class EgrChangePasswordBean {

  private static final Logger log = LoggerFactory.getLogger(EgrChangePasswordBean.class);

  @Inject
  private PasswordResetterService passwordResetterService;

  private Boolean checkPKI;

  private String password;
  private String repeatedPassword;

  private String passwordResetTokenParam;

  private PasswordResetToken passwordResetToken;

  private boolean showPasswordComplexityInfo = false;

  public void changePassword() {
    showPasswordComplexityInfo = false;
    try {
      passwordResetterService.changePassword(passwordResetToken, password);
      addPasswordChangeMessage();
      redirectToHomePage();
    } catch (RepeatedPasswordException e) {
      facesError("Wybrałeś hasło, które było użyte wcześniej.");
    } catch (SpecialPasswordException e) {
      facesError("Wybrałeś hasło zastrzeżone.");
    } catch (PasswordValidationException e) {
      showPasswordComplexityInfo = true;
    } catch (SQLException e) {
      log.error("Problem przy zmianie hasła. Użytkownik: " + passwordResetToken.getUsername(), e);
      facesError(LanguageBean.translate("ERR_reset_password_problem"));
    }
  }

  private void addPasswordChangeMessage() {
    FacesContext.getCurrentInstance()
        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,
            LanguageBean.translate("INF_password_changed"), null));
    FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
  }

  public void preViewConstruct() {
    initCheckPKI();
    if (checkPKI) {
      validatePKI();
    }
    validateToken();
    checkIsUserBlocked();
  }

  private void validateToken() {
    if (passwordResetTokenParam == null) {
      redirectToHomePage();
    } else {
      Optional<PasswordResetToken> optionalPasswordResetToken = passwordResetterService
          .getValidPasswordResetToken(passwordResetTokenParam);
      if (optionalPasswordResetToken.isPresent()) {
        passwordResetToken = optionalPasswordResetToken.get();
      } else {
        log.warn(
            "Próba dostania się pod adres " + Const.HOME_REDIRECT_URL + "/changePassword z błędnym tokenem: {}",
            passwordResetTokenParam);
        redirectToHomePage();
      }
    }
  }

  private void checkIsUserBlocked() {
    if (passwordResetterService == null || passwordResetToken == null || !passwordResetterService
        .canUserChangePassword(passwordResetToken.getUsername())) {
      redirectToHomePage();
    }
  }

  private void validatePKI() {
    try {
      EgrPKIAuth.checkComarchX509Certificate();
    } catch (EgrPKICertificateNotFoundException | CertificateExpiredException | CertificateNotYetValidException
        | EgrPKICertificateWrongIssuerException | CertificateParsingException
        | EgrPKICertificateEmailNotFoundException e) {
      redirectToHomePage();
    }
  }

  private void redirectToHomePage() {
    try {
      FacesContext.getCurrentInstance().getExternalContext().redirect(Const.HOME_REDIRECT_URL);
    } catch (Exception ex){
      if (! (ex instanceof  IllegalStateException)) {
        log.error(ex.getMessage(), ex);
      }
    }
  }

  private void initCheckPKI() {
    try {
      checkPKI = (Boolean) (new InitialContext()).lookup("java:/comp/env/check_pki");
    } catch (NamingException e) {
      checkPKI = false;
    }
  }

  private void facesError(String message) {
    FacesContext.getCurrentInstance()
        .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, null));
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String senha) {
    this.password = senha;
  }

  public String getRepeatedPassword() {
    return repeatedPassword;
  }

  public void setRepeatedPassword(String repeatedPassword) {
    this.repeatedPassword = repeatedPassword;
  }

  public String getPasswordResetTokenParam() {
    return passwordResetTokenParam;
  }

  public void setPasswordResetTokenParam(String passwordResetTokenParam) {
    this.passwordResetTokenParam = passwordResetTokenParam;
  }

  public boolean isShowPasswordComplexityInfo() {
    return showPasswordComplexityInfo;
  }

  public void setShowPasswordComplexityInfo(boolean showPasswordComplexityInfo) {
    this.showPasswordComplexityInfo = showPasswordComplexityInfo;
  }
}