package com.comarch.egeria.web.common.utils.reports;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Centrum;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Mpk;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Okres2;

import net.sf.jasperreports.engine.JRException;

abstract public class CtlReportBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LogManager.getLogger(CtlReportBean.class);

	@Inject
	protected CtlReportsUtils ctlReportUtils;

	protected String pParametr;

	private List<Centrum> centraF;
	protected Long centrumFiltr;
	protected Boolean wszystkieCeF;

	private List<Okres2> okresyF;
	protected Long okresOdFiltr;
	protected Long okresDoFiltr;

//	protected List<Waluta> walutyF;
//	protected Long walutyFiltr;
//	protected Boolean wWalSysF;
	// private Double kursWal;

	protected List<Mpk> zadaniaF;
	protected Long zadanieFiltr;
	protected Boolean wszystkieRDF;
	protected Boolean przypRDDoCeF;

	protected Boolean zNarzutemF;
	protected Boolean zRozlWewnF;
	protected Boolean zTowaramiF;
	protected Boolean GKF;

	public void generateReportPdf() {
		try {
			Map<String, Object> param = prepareReportParameters();
			String rapName = getReportName();

			ctlReportUtils.displayReportAsPdf(rapName, param);

		} catch (JRException | SQLException | IOException e) {
			e.printStackTrace();
		}
	}

	public void generateReportXlsx() {
		try {
			Map<String, Object> param = prepareReportParameters();
			String rapName = getReportName();

			ctlReportUtils.displayReportAsXlsx(rapName, param);

		} catch (JRException | SQLException | IOException e) {
			e.printStackTrace();
		}
	}

	public void generateReportHtml() {
		try {
			Map<String, Object> param = prepareReportParameters();
			String rapName = getReportName();

			ctlReportUtils.displayReportAsHtml(rapName, param);

		} catch (JRException | SQLException | IOException e) {
			e.printStackTrace();
		}
	}

	public void generateReportRtf() {
		try {
			Map<String, Object> param = prepareReportParameters();
			String rapName = getReportName();

			ctlReportUtils.displayReportAsRtf(rapName, param);

		} catch (JRException | SQLException | IOException e) {
			e.printStackTrace();
		}
	}

	public void generateReportDocx() {
		try {
			Map<String, Object> param = prepareReportParameters();
			String rapName = getReportName();

			ctlReportUtils.displayReportAsDocx(rapName, param);

		} catch (JRException | SQLException | IOException e) {
			e.printStackTrace();
		}
	}

	public void generateReportCsv() {
		try {
			Map<String, Object> param = prepareReportParameters();
			String rapName = getReportName();

			ctlReportUtils.displayReportAsCsv(rapName, param);

		} catch (JRException | SQLException | IOException e) {
			e.printStackTrace();
		}
	}

	@PostConstruct
	protected void init() {
		wszystkieCeF = false;
//		wWalSysF = false;
		wszystkieRDF = false;
		przypRDDoCeF = false;

		zNarzutemF = false;
		zRozlWewnF = false;
		zTowaramiF = false;
		GKF = false;

//		try {
//			centraF = sqlGlobalneBo.sqlDostepneCentra(ctlUserSessionBean.getUzId());
//			okresyF = sqlGlobalneBo.sqlOkresy2();
//			walutyF = definicjeBudzetowBo.sqlWaluty(60611, 1); // TODO
//
//			log.debug(walutyF);
//			walutyFiltr = walutyF.stream().filter(wal -> wal.getWalFSystemowa().equals("T")).findFirst().get()
//					.getWalId();
//
//			okresOdFiltr = ctlUserSessionBean.getAktualnyOkresId();
//			okresDoFiltr = ctlUserSessionBean.getAktualnyOkresId();
//			centrumFiltr = ctlUserSessionBean.getAktywneCentrum().getCeId();
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
	}

	protected Centrum getCentrumDetails(Long pCeId) {
		return centraF.stream().filter(centrum -> centrum.getCeId().equals(pCeId)).findFirst().get();
	}

	protected Okres2 getOkresDetails(Long pOkId) {
		return okresyF.stream().filter(okres -> okres.getOkId().equals(pOkId)).findFirst().get();
	}

	abstract protected Map<String, Object> prepareReportParameters();

	abstract protected String getReportName();

	public String getpParametr() {
		return pParametr;
	}

	public void setpParametr(String pParametr) {
		this.pParametr = pParametr;
	}

	public List<Centrum> getCentraF() {
		return centraF;
	}

	public void setCentraF(List<Centrum> centraF) {
		this.centraF = centraF;
	}

	public Long getCentrumFiltr() {
		return centrumFiltr;
	}

	public void setCentrumFiltr(Long centrumFiltr) {
		this.centrumFiltr = centrumFiltr;
	}

	public List<Okres2> getOkresyF() {
		return okresyF;
	}

	public void setOkresyF(List<Okres2> okresyF) {
		this.okresyF = okresyF;
	}

	public Long getOkresOdFiltr() {
		return okresOdFiltr;
	}

	public void setOkresOdFiltr(Long okresOdFiltr) {
		this.okresOdFiltr = okresOdFiltr;
	}

	public Long getOkresDoFiltr() {
		return okresDoFiltr;
	}

	public void setOkresDoFiltr(Long okresDoFiltr) {
		this.okresDoFiltr = okresDoFiltr;
	}

	public Boolean getWszystkieCeF() {
		return wszystkieCeF;
	}

	public void setWszystkieCeF(Boolean wszystkieCeF) {
		this.wszystkieCeF = wszystkieCeF;
	}

	public Boolean getzNarzutemF() {
		return zNarzutemF;
	}

	public void setzNarzutemF(Boolean zNarzutemF) {
		this.zNarzutemF = zNarzutemF;
	}

	public Boolean getzRozlWewnF() {
		return zRozlWewnF;
	}

	public void setzRozlWewnF(Boolean zRozlWewnF) {
		this.zRozlWewnF = zRozlWewnF;
	}

	public Boolean getzTowaramiF() {
		return zTowaramiF;
	}

	public void setzTowaramiF(Boolean zTowaramiF) {
		this.zTowaramiF = zTowaramiF;
	}

	public Boolean getGKF() {
		return GKF;
	}

	public void setGKF(Boolean gKF) {
		GKF = gKF;
	}

	public List<Mpk> getZadaniaF() {
		return zadaniaF;
	}

	public void setZadaniaF(List<Mpk> zadaniaF) {
		this.zadaniaF = zadaniaF;
	}

	public Long getZadanieFiltr() {
		return zadanieFiltr;
	}

	public void setZadanieFiltr(Long zadanieFiltr) {
		this.zadanieFiltr = zadanieFiltr;
	}

	public Boolean getWszystkieRDF() {
		return wszystkieRDF;
	}

	public void setWszystkieRDF(Boolean wszystkieRDF) {
		this.wszystkieRDF = wszystkieRDF;
	}

	public Boolean getPrzypRDDoCeF() {
		return przypRDDoCeF;
	}

	public void setPrzypRDDoCeF(Boolean przypRDDoCeF) {
		this.przypRDDoCeF = przypRDDoCeF;
	}
	
}