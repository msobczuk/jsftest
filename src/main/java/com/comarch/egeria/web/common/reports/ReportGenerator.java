package com.comarch.egeria.web.common.reports;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.Administrator.model.PptAdmListyWartosci;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import net.sf.jasperreports.components.table.BaseColumn;
import net.sf.jasperreports.components.table.StandardColumn;
import net.sf.jasperreports.components.table.StandardTable;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JRDesignComponentElement;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRRtfExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.nz;

@ManagedBean
@Named
@Scope("session")

public class ReportGenerator implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private static final Logger log = LogManager.getLogger();

	
	
	@Inject
	public AppBean appBean;
	
	@Inject
	User user;
	
	public static void displayReport(String repName, HashMap<String, Object> params) throws SQLException, JRException, IOException{
		displayReport("pdf", repName, params);
	}
	
	public static void displayReport(String outputType, String repName, HashMap<String, Object> params) throws SQLException, JRException, IOException{
		
		if (params == null) 
			params = new HashMap<String, Object>();
		
		if ("docx".equals((""+outputType).toLowerCase()))
			displayReportAsDOCX(params, repName);
		else if ("rtf".equals((""+outputType).toLowerCase()))
			displayReportAsRTF(params, repName);
		else if ("xls".equals((""+outputType).toLowerCase()))
			displayReportAsXLS(params, repName);
		else if ("odt".equals((""+outputType).toLowerCase()))
			displayReportAsODT(params, repName);					
		else 
			displayReportAsPDF(params, repName);
		
	}
	
	/**
	 * Generuje raport i wyświetla go w nowym oknie przegldarki w postaci pliku
	 * PDF. repName - Nazwa raportu, który ma zostać wygenerowany. parameters -
	 * Parametry raportu w postaci Mapy - <'NAZWA_PARAMETRU', Obiekt>
	 */
	public static void displayReportAsPDF(HashMap<String, Object> params, String repName)
			throws SQLException, JRException, IOException {
		// pobranie ścieżki do skompliowanego wczesniej raportu(format .jasper)
		String jasperFilePath = "/WEB-INF/protectedElements/Reports/compiledReports/" + repName + ".jasper";
		String jasperFileRealPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath(jasperFilePath);

		String jrxmlFilePath = "/WEB-INF/protectedElements/Reports/sourceReports/" + repName + ".jrxml";
		String jrxmlFileRealPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath(jrxmlFilePath);

		File fileJasper = new File(jasperFileRealPath);
		File fileJrxml = new File(jrxmlFileRealPath);
		long lmjasper = fileJasper.lastModified();
		long lmjrxml = fileJrxml.lastModified();
		if (
				(!fileJasper.exists() && fileJrxml.exists())//brak pliku jasper ale jest plik jrxml
				|| lmjasper < lmjrxml //data modyfikacji jrxml > daty mpodyfikacji jasper'a
		){
			JasperCompileManager.compileReportToFile(jrxmlFileRealPath, jasperFileRealPath);
		}

		RodoUtils.dslogUruchomionoRaport(jasperFilePath, "jasper");

		try (Connection conn = JdbcUtils.getConnection()) {

			DefaultJasperReportsContext.getInstance().setProperty("net.sf.jasperreports.text.pattern.date", "dd.MM.yyyy' r.'");
//          DefaultJasperReportsContext.getInstance().setProperty("net.sf.jasperreports.text.pattern.date", "dd.MM.yyyy");
//          DefaultJasperReportsContext.getInstance().setProperty("net.sf.jasperreports.text.pattern.date", "yyyy.MM.dd");

			
			//JasperReport jasperReport = JasperCompileManager.compileReport(jasperFileRealPath);
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperFileRealPath, params, conn);

			// //podglad raportu w JasperVierwer
			// JasperViewer.viewReport(jasperPrint,false);

			// DO NOWEGO OKNA W PRZEGLADARCE
			ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
			HttpServletResponse response = (HttpServletResponse) ectx.getResponse();

			response.setHeader("Content-Type", "application/pdf");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".pdf" + "\"");
			response.setHeader("pragma", "public");
			ServletOutputStream stream = response.getOutputStream();

			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
			exporter.exportReport();

			FacesContext.getCurrentInstance().responseComplete();

			// Do pliku na dysku
			// JasperExportManager.exportReportToPdfFile(jasperPrint,
			// "C:/reports/simple_report.pdf");
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			return;
		}
		
	}
	
	
	
	public static void displayReportNaboryAsPDF(HashMap<String, Object> params, String repName)
			throws SQLException, JRException, IOException {
		// pobranie ścieżki do skompliowanego wczesniej raportu(format .jasper)
		String path = "/WEB-INF/protectedElements/Reports/sourceReports/" + repName + ".jrxml";
		String file = FacesContext.getCurrentInstance().getExternalContext().getRealPath(path);
		RodoUtils.dslogUruchomionoRaport(path, "jasper");
		
		
		JasperDesign jd = JRXmlLoader.load(file);

		JRField[] fields = jd.getFields();
		
		JRBand jrBandDetails = jd.getAllBands()[3];//details
	
		JRDesignComponentElement ctb = (JRDesignComponentElement) jrBandDetails.getElements()[0];//tabela
		StandardTable tb = (StandardTable) ctb.getComponent();
		
		for (BaseColumn basecol : tb.getColumns()) {
			
			StandardColumn col  = (StandardColumn) basecol;
			System.out.println(col.getWidth());
//			col.setWidth(60);
			
		}

		
		JasperReport jr = JasperCompileManager.compileReport(jd);
		
		
		
		try (Connection conn = JdbcUtils.getConnection()) {

			JasperPrint jasperPrint = JasperFillManager.fillReport(jr, params, conn);
			
//			JasperPrint jasperPrint = JasperFillManager.fillReport(file, params, conn);

			ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
			HttpServletResponse response = (HttpServletResponse) ectx.getResponse();

			response.setHeader("Content-Type", "application/pdf");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".pdf" + "\"");
			response.setHeader("pragma", "public");
			ServletOutputStream stream = response.getOutputStream();

			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
			exporter.exportReport();

			FacesContext.getCurrentInstance().responseComplete();

		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			return;
		}
		
	}

	/**
	 * Generuje raport i wyświetla go w nowym oknie przegldarki w postaci pliku
	 * RTF. repName - Nazwa raportu, który ma zostać wygenerowany. parameters -
	 * Parametry raportu w postaci Mapy - <'NAZWA_PARAMETRU', Obiekt>
	 */
	public static void displayReportAsRTF(HashMap<String, Object> params, String repName)
			throws SQLException, JRException, IOException {
		// pobranie ścieżki do raportu
		String path = "/WEB-INF/protectedElements/Reports/compiledReports/" + repName + ".jasper";
		String file = FacesContext.getCurrentInstance().getExternalContext().getRealPath(path);
		RodoUtils.dslogUruchomionoRaport(path, "jasper");

		try (Connection conn = JdbcUtils.getConnection()) {

			//JasperReport jasperReport = JasperCompileManager.compileReport(file);
			JasperPrint jasperPrint = JasperFillManager.fillReport(file, params, conn);

			// //podglad raportu w JasperVierwer
			// JasperViewer.viewReport(jasperPrint,false);

			// DO NOWEGO OKNA W PRZEGLADARCE
			ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
			HttpServletResponse response = (HttpServletResponse) ectx.getResponse();

			response.setHeader("Content-Type", "application/rtf");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".rtf" + "\"");
			response.setHeader("pragma", "public");
			ServletOutputStream stream = response.getOutputStream();

			JRRtfExporter exporter = new JRRtfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(stream));
			exporter.exportReport();

			FacesContext.getCurrentInstance().responseComplete();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Generuje raport i wyświetla go w nowym oknie przegldarki w postaci pliku
	 * RTF. repName - Nazwa raportu, który ma zostać wygenerowany. parameters -
	 * Parametry raportu w postaci Mapy - <'NAZWA_PARAMETRU', Obiekt>
	 */
	public static void displayReportAsDOCX(HashMap<String, Object> params, String repName)
			throws SQLException, JRException, IOException {
		// pobranie ścieżki do raportu
		String path = "/WEB-INF/protectedElements/Reports/compiledReports/" + repName + ".jasper";
		String file = FacesContext.getCurrentInstance().getExternalContext().getRealPath(path);
		RodoUtils.dslogUruchomionoRaport(path, "jasper");
		
		try (Connection conn = JdbcUtils.getConnection()) {

			//JasperReport jasperReport = JasperCompileManager.compileReport(file);
			JasperPrint jasperPrint = JasperFillManager.fillReport(file, params, conn);

			// //podglad raportu w JasperVierwer
			// JasperViewer.viewReport(jasperPrint,false);

			// DO NOWEGO OKNA W PRZEGLADARCE
			ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
			HttpServletResponse response = (HttpServletResponse) ectx.getResponse();

			response.setHeader("Content-Type", "application/doc");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".docx" + "\"");
			response.setHeader("pragma", "public");
			ServletOutputStream stream = response.getOutputStream();

			JRDocxExporter exporter = new JRDocxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
			exporter.exportReport();

			FacesContext.getCurrentInstance().responseComplete();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	/**
	 * Generuje raport i wyświetla go w nowym oknie przegldarki w postaci pliku
	 * ODT. repName - Nazwa raportu, który ma zostać wygenerowany. parameters -
	 * Parametry raportu w postaci Mapy - <'NAZWA_PARAMETRU', Obiekt>
	 */
	public static void displayReportAsODT(HashMap<String, Object> params, String repName)
			throws SQLException, JRException, IOException {
		// pobranie ścieżki do raportu
		String path = "/WEB-INF/protectedElements/Reports/compiledReports/" + repName + ".jasper";
		String file = FacesContext.getCurrentInstance().getExternalContext().getRealPath(path);
		RodoUtils.dslogUruchomionoRaport(path, "jasper");
		
		try (Connection conn = JdbcUtils.getConnection()) {
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(file, params, conn);
			ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
			HttpServletResponse response = (HttpServletResponse) ectx.getResponse();

			response.setHeader("Content-Type", "application/doc");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".odt" + "\"");
			response.setHeader("pragma", "public");
			ServletOutputStream stream = response.getOutputStream();

			JROdtExporter exporter = new JROdtExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
			exporter.exportReport();

			FacesContext.getCurrentInstance().responseComplete();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public static void displayReportAsXLS(HashMap<String, Object> params, String repName)
			throws SQLException, JRException, IOException {
		
		
		params.put("IS_IGNORE_PAGINATION", true);
		

		String path = "/WEB-INF/protectedElements/Reports/compiledReports/" + repName + ".jasper";
		String file = FacesContext.getCurrentInstance().getExternalContext().getRealPath(path);
		RodoUtils.dslogUruchomionoRaport(path, "jasper");
		
		try (Connection conn = JdbcUtils.getConnection()) {

			JasperPrint jasperPrint = JasperFillManager.fillReport(file, params, conn);

			ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
			HttpServletResponse response = (HttpServletResponse) ectx.getResponse();

			response.setHeader("Content-Type", "application/vnd.ms-excel");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".xlsx" + "\"");
			response.setHeader("pragma", "public");
			ServletOutputStream stream = response.getOutputStream();

			
			JRXlsxExporter xlsExporter = new JRXlsxExporter();
			xlsExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			xlsExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
			xlsExporter.exportReport();

			FacesContext.getCurrentInstance().responseComplete();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}


	public static PipedInputStream pdfAsStream(String repName) throws SQLException, IOException, JRException {
		return pdfAsStream(repName, new HashMap<String, Object> ());
	}

	public static PipedInputStream pdfAsStream(String repName, HashMap<String, Object> params)
			throws SQLException, JRException, IOException {
		
		
		String applicationContextFolder = AppBean.app.getApplicationContextFolder().toString().replace("\\", "/");
		String path = "/WEB-INF/protectedElements/Reports/compiledReports/" + repName + ".jasper";
		//String file = FacesContext.getCurrentInstance().getExternalContext().getRealPath(path);
		
		String file = applicationContextFolder.replace("\\", "/") + path;
		
		RodoUtils.dslogUruchomionoRaport(path, "jasper");

		try (Connection conn = JdbcUtils.getConnection()) {

			DefaultJasperReportsContext.getInstance().setProperty("net.sf.jasperreports.text.pattern.date", "dd.MM.yyyy' r.'");
			JasperPrint jasperPrint = JasperFillManager.fillReport(file, params, conn);


			PipedInputStream in = new PipedInputStream();
			PipedOutputStream out = new PipedOutputStream(in);


			Thread thread = new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						JasperExportManager.exportReportToPdfStream(jasperPrint, out);
						out.close();
					} catch (JRException | IOException e) {
						log.error("An error has occured while exporting report", e);
					}
				}
			} );

			thread.start();

			return in;

		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			return null;
		}

	}
	
	
	public static PipedInputStream externalTaskPdfAsStream(OutputStream out, String repName, HashMap<String, Object> params)
			throws SQLException, JRException, IOException {
		
		
		String applicationContextFolder = AppBean.app.getApplicationContextFolder().toString().replace("\\", "/");
		String path = "/WEB-INF/protectedElements/Reports/compiledReports/" + repName + ".jasper";
		//String file = FacesContext.getCurrentInstance().getExternalContext().getRealPath(path);
		
		String file = applicationContextFolder.replace("\\", "/") + path;
		
		//AppBean.dslogUruchomionoRaport(path, "jasper"); TODO - przechwyc usera

		try (Connection conn = JdbcUtils.getConnection(false)) {

			DefaultJasperReportsContext.getInstance().setProperty("net.sf.jasperreports.text.pattern.date", "dd.MM.yyyy' r.'");
			JasperPrint jasperPrint = JasperFillManager.fillReport(file, params, conn);


			PipedInputStream in = new PipedInputStream();
			//PipedOutputStream out = new PipedOutputStream(in);


			Thread thread = new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						JasperExportManager.exportReportToPdfStream(jasperPrint, out);
						out.close();
					} catch (JRException | IOException e) {
						log.error("An error has occured while exporting report", e);
					}
				}
			} );

			thread.start();

			return in;

		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			return null;
		}

	}

	


	@Deprecated //TODO zla kolejnosc parametró - do redukcji po zamianie wywolan
	public static PipedInputStream pdfAsStream(HashMap<String, Object> params, String repName)
			throws SQLException, JRException, IOException {

		String path = "/WEB-INF/protectedElements/Reports/compiledReports/" + repName + ".jasper";
		String file = FacesContext.getCurrentInstance().getExternalContext().getRealPath(path);
		RodoUtils.dslogUruchomionoRaport(path, "jasper");

		try (Connection conn = JdbcUtils.getConnection()) {

			DefaultJasperReportsContext.getInstance().setProperty("net.sf.jasperreports.text.pattern.date", "dd.MM.yyyy' r.'");
			JasperPrint jasperPrint = JasperFillManager.fillReport(file, params, conn);


			PipedInputStream in = new PipedInputStream();
			PipedOutputStream out = new PipedOutputStream(in);


			Thread thread = new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						JasperExportManager.exportReportToPdfStream(jasperPrint, out);
						out.close();
					} catch (JRException | IOException e) {
						log.error("An error has occured while exporting report", e);
					}
				}
			} );


			thread.start();

			return in;

		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			return null;
		}

	}
	
	
//	private void createExporterThread(final JasperPrint jasperPrint, final PipedOutputStream out) {
//		
//		Thread thread = new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				try {
//					JasperExportManager.exportReportToPdfStream(jasperPrint, out);
//					out.close();
//				} catch (JRException | IOException e) {
//					log.error("An error has occured while exporting report", e);
//				}
//			}
//		} );
//		
//		thread.start();
//	}
	
	
	public static void displayReporutAsPDF(HashMap<String, Object> params, String repName)
			throws SQLException, JRException, IOException {
		// pobranie ścieżki do skompliowanego wczesniej raportu(format .jasper)
		String path = "/WEB-INF/protectedElements/Reports/compiledReports/" + repName + ".jasper";
		String file = FacesContext.getCurrentInstance().getExternalContext().getRealPath(path);
		RodoUtils.dslogUruchomionoRaport(path, "jasper");

		try (Connection conn = JdbcUtils.getConnection()) {

			DefaultJasperReportsContext.getInstance().setProperty("net.sf.jasperreports.text.pattern.date", "dd.MM.yyyy' r.'");
//          DefaultJasperReportsContext.getInstance().setProperty("net.sf.jasperreports.text.pattern.date", "dd.MM.yyyy");
//          DefaultJasperReportsContext.getInstance().setProperty("net.sf.jasperreports.text.pattern.date", "yyyy.MM.dd");

			
			//JasperReport jasperReport = JasperCompileManager.compileReport(file);
			
			JasperPrint jasperPrint = JasperFillManager.fillReport(file, params, conn);

			// //podglad raportu w JasperVierwer
			// JasperViewer.viewReport(jasperPrint,false);

			// DO NOWEGO OKNA W PRZEGLADARCE
			ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
			HttpServletResponse response = (HttpServletResponse) ectx.getResponse();

			response.setHeader("Content-Type", "application/pdf");
			response.setHeader("Content-Disposition", "inline;filename=\"" + repName + ".pdf" + "\"");
			response.setHeader("pragma", "public");
			ServletOutputStream stream = response.getOutputStream();

			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(stream));
			exporter.exportReport();

			
			FacesContext.getCurrentInstance().responseComplete();

			// Do pliku na dysku
			// JasperExportManager.exportReportToPdfFile(jasperPrint,
			// "C:/reports/simple_report.pdf");
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			return;
		}
		
	}
	
	public static PipedInputStream getLwAsPdfStream(SqlDataSelectionsHandler lw) throws IOException, DocumentException {
		byte[] b = ReportGenerator.asPdfData(lw);
		return Utils.asPipedInputStream(b);
	}



	public static byte[] asPdfData(SqlDataSelectionsHandler lw) throws IOException, DocumentException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Document pdf = new Document();
		PdfWriter.getInstance(pdf, byteArrayOutputStream);
		pdf.open();
		lw.preProcessPDF(pdf);
		appendPdfLW(pdf, lw, 100f, Element.ALIGN_CENTER);
		pdf.close();
		return byteArrayOutputStream.toByteArray();
	}





	public static byte[] asPdfData(List<SqlDataSelectionsHandler> lws) throws IOException, DocumentException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Document pdf = new Document();
		PdfWriter.getInstance(pdf, byteArrayOutputStream);
		pdf.open();

		appendPdfHeader(pdf);

		for (SqlDataSelectionsHandler lw : lws) {
			appendPdfLwTitle(pdf, lw, "");
			appendPdfLW(pdf, lw, 100f, Element.ALIGN_CENTER);
			pdf.add(new Paragraph(" "));
		}

		pdf.close();
		return byteArrayOutputStream.toByteArray();
	}

	
	
    public static StreamedContent generujSlElToPdfStream(String slownikEl2PdfNazwa) throws IOException, DocumentException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
    	return generujSlElToPdfStream(slownikEl2PdfNazwa, null);
    }

    public static StreamedContent generujSlElToPdfStream(String slownikEl2PdfNazwa, String pdffilename) throws IOException, DocumentException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
    	if (pdffilename == null)
    		pdffilename = slownikEl2PdfNazwa+".pdf";
    	else if (!pdffilename.toLowerCase().endsWith(".pdf"))
    		pdffilename+=".pdf";
    	
    	User.getCurrentUser().getSqlDataCache().loadTBTS();
        byte[] bytes = ReportGenerator.generujSlElToPdfData(slownikEl2PdfNazwa);
        return Utils.asStreamedContent(bytes, "application/pdf", "testLwListRpt.pdf");
    }
	
	
	public static byte[] generujSlElToPdfData(String slNazwa) throws IOException, DocumentException {
		SqlDataSelectionsHandler sl = Utils.getSL(slNazwa);
		sl.resetTs();
		return generujSlElToPdfData(sl);
	}


	
	public static byte[] generujSlElToPdfData(SqlDataSelectionsHandler sl) throws IOException, DocumentException {
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		Document pdf = new Document();
		PdfWriter.getInstance(pdf, byteArrayOutputStream);
		pdf.open();

		appendPdfHeader(pdf);

		for (DataRow r : sl.getData()) {

			String wslOpis = nz((String) r.get("WSL_OPIS"));
			String wslAlias2UC = " "+nz((String) r.get("WSL_ALIAS2")).toUpperCase() + " ";

			Object x = Utils.evaluateExpressionGet(wslOpis);
			if (x==null) continue;

			if (x instanceof SqlDataSelectionsHandler){
				
				SqlDataSelectionsHandler lw = (SqlDataSelectionsHandler) x;
				lw.resetTs();
				if (!nz(lw.getDefLst().getLstOpis()).isEmpty()  && !wslAlias2UC.contains(" OPIS-N ") && !wslAlias2UC.contains(" T-NONE ")) {
					appendPdfLwTitle(pdf, lw, wslAlias2UC);
				}

				float tbWidth = calcWidthByCssClass(wslAlias2UC);
				int horizontalAlignment = getHorizontalAligmentByCssClass(wslAlias2UC);

				appendPdfLW(pdf, lw, tbWidth, horizontalAlignment);


			} else if (x instanceof String) {
				Font font = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
				setFontSize(font, wslAlias2UC); //font.setSize(10);
				//font.setColor(Color.decode(facetFontColor)); //TODO rozbic wslAlias2UC na klasy  css ...

				Paragraph p = new Paragraph((String) x, font);

				if (wslAlias2UC.contains(" CENTER "))
					p.setAlignment(Element.ALIGN_CENTER);
				else if (wslAlias2UC.contains(" RIGHT "))
					p.setAlignment(Element.ALIGN_RIGHT);

				setSpacingBeforeByCssClass(p, wslAlias2UC);
				setSpacingAfterByCssClass(p, wslAlias2UC);
				pdf.add(p);

			}
		}

		if (sl.getName()!=null && !sl.getName().isEmpty())
			pdf.addTitle(sl.getName());

		pdf.close();
		return byteArrayOutputStream.toByteArray();
	}





	public static void setSpacingBeforeByCssClass(Paragraph p, String wslAlias2UC){
		if (wslAlias2UC.contains(" MT0 ")) p.setSpacingBefore(0f);
		else if (wslAlias2UC.contains(" MT1 ")) p.setSpacingBefore(1f);
		else if (wslAlias2UC.contains(" MT2 ")) p.setSpacingBefore(2f);
		else if (wslAlias2UC.contains(" MT3 ")) p.setSpacingBefore(3f);
		else if (wslAlias2UC.contains(" MT4 ")) p.setSpacingBefore(4f);
		else if (wslAlias2UC.contains(" MT5 ")) p.setSpacingBefore(5f);
		else if (wslAlias2UC.contains(" MT6 ")) p.setSpacingBefore(6f);
		else if (wslAlias2UC.contains(" MT7 ")) p.setSpacingBefore(7f);
		else if (wslAlias2UC.contains(" MT8 ")) p.setSpacingBefore(8f);
		else if (wslAlias2UC.contains(" MT9 ")) p.setSpacingBefore(9f);
		else if (wslAlias2UC.contains(" MT10 ")) p.setSpacingBefore(10f);
		else if (wslAlias2UC.contains(" MT11 ")) p.setSpacingBefore(11f);
		else if (wslAlias2UC.contains(" MT12 ")) p.setSpacingBefore(12f);
		else if (wslAlias2UC.contains(" MT13 ")) p.setSpacingBefore(13f);
		else if (wslAlias2UC.contains(" MT14 ")) p.setSpacingBefore(14f);
		else if (wslAlias2UC.contains(" MT15 ")) p.setSpacingBefore(15f);
		else if (wslAlias2UC.contains(" MT16 ")) p.setSpacingBefore(16f);
		else if (wslAlias2UC.contains(" MT17 ")) p.setSpacingBefore(17f);
		else if (wslAlias2UC.contains(" MT18 ")) p.setSpacingBefore(18f);
		else if (wslAlias2UC.contains(" MT19 ")) p.setSpacingBefore(19f);
		else if (wslAlias2UC.contains(" MT20 ")) p.setSpacingBefore(20f);
		else p.setSpacingBefore(0f);
	}


	public static void setSpacingAfterByCssClass(Paragraph p, String wslAlias2UC){
		if (wslAlias2UC.contains(" MB0 ")) p.setSpacingAfter(0f);
		else if (wslAlias2UC.contains(" MB1 ")) p.setSpacingAfter(1f);
		else if (wslAlias2UC.contains(" MB2 ")) p.setSpacingAfter(2f);
		else if (wslAlias2UC.contains(" MB3 ")) p.setSpacingAfter(3f);
		else if (wslAlias2UC.contains(" MB4 ")) p.setSpacingAfter(4f);
		else if (wslAlias2UC.contains(" MB5 ")) p.setSpacingAfter(5f);
		else if (wslAlias2UC.contains(" MB6 ")) p.setSpacingAfter(6f);
		else if (wslAlias2UC.contains(" MB7 ")) p.setSpacingAfter(7f);
		else if (wslAlias2UC.contains(" MB8 ")) p.setSpacingAfter(8f);
		else if (wslAlias2UC.contains(" MB9 ")) p.setSpacingAfter(9f);
		else if (wslAlias2UC.contains(" MB10 ")) p.setSpacingAfter(10f);
		else if (wslAlias2UC.contains(" MB11 ")) p.setSpacingAfter(11f);
		else if (wslAlias2UC.contains(" MB12 ")) p.setSpacingAfter(12f);
		else if (wslAlias2UC.contains(" MB13 ")) p.setSpacingAfter(13f);
		else if (wslAlias2UC.contains(" MB14 ")) p.setSpacingAfter(14f);
		else if (wslAlias2UC.contains(" MB15 ")) p.setSpacingAfter(15f);
		else if (wslAlias2UC.contains(" MB16 ")) p.setSpacingAfter(16f);
		else if (wslAlias2UC.contains(" MB17 ")) p.setSpacingAfter(17f);
		else if (wslAlias2UC.contains(" MB18 ")) p.setSpacingAfter(18f);
		else if (wslAlias2UC.contains(" MB19 ")) p.setSpacingAfter(19f);
		else if (wslAlias2UC.contains(" MB20 ")) p.setSpacingAfter(20f);
		else p.setSpacingAfter(10f);
	}


	private static float calcWidthByCssClass(String wslAlias2UC) {
		float tbWidth = 100f;
		if      (wslAlias2UC.contains(" W10P ")) tbWidth = 10f;
		else if (wslAlias2UC.contains(" W20P ")) tbWidth = 20f;
		else if (wslAlias2UC.contains(" W30P ")) tbWidth = 30f;
		else if (wslAlias2UC.contains(" W40P ")) tbWidth = 40f;
		else if (wslAlias2UC.contains(" W50P ")) tbWidth = 50f;
		else if (wslAlias2UC.contains(" W60P ")) tbWidth = 60f;
		else if (wslAlias2UC.contains(" W70P ")) tbWidth = 70f;
		else if (wslAlias2UC.contains(" W80P ")) tbWidth = 80f;
		else if (wslAlias2UC.contains(" W90P ")) tbWidth = 90f;
		return tbWidth;
	}

	private static int getHorizontalAligmentByCssClass(String wslAlias2UC) {
		int horizontalAlignment = Element.ALIGN_LEFT;
		if (wslAlias2UC.contains(" LEFT ")) horizontalAlignment = Element.ALIGN_LEFT;
		else if (wslAlias2UC.contains(" RIGHT "))  horizontalAlignment = Element.ALIGN_RIGHT;
		else if (wslAlias2UC.contains(" CENTER "))  horizontalAlignment = Element.ALIGN_CENTER;
		return horizontalAlignment;
	}

	private static int getLwTitleHorizontalAligmentByCssClass(String wslAlias2UC) {
		int horizontalAlignment = Element.ALIGN_CENTER;
		if (wslAlias2UC.contains(" T-LEFT ")) horizontalAlignment = Element.ALIGN_LEFT;
		else if (wslAlias2UC.contains(" T-RIGHT "))  horizontalAlignment = Element.ALIGN_RIGHT;
		else if (wslAlias2UC.contains(" T-CENTER "))  horizontalAlignment = Element.ALIGN_CENTER;
		return horizontalAlignment;
	}

	private static void setLwTitleFontSize(Font font, String wslAlias2UC) {
		     if (wslAlias2UC.contains(" T-FS20 ")) font.setSize(20);
		else if (wslAlias2UC.contains(" T-FS19 ")) font.setSize(19);
		else if (wslAlias2UC.contains(" T-FS18 ")) font.setSize(18);
		else if (wslAlias2UC.contains(" T-FS17 ")) font.setSize(17);
		else if (wslAlias2UC.contains(" T-FS16 ")) font.setSize(16);
		else if (wslAlias2UC.contains(" T-FS15 ")) font.setSize(15);
		else if (wslAlias2UC.contains(" T-FS14 ")) font.setSize(14);
		else if (wslAlias2UC.contains(" T-FS13 ")) font.setSize(13);
		else if (wslAlias2UC.contains(" T-FS12 ")) font.setSize(12);
		else if (wslAlias2UC.contains(" T-FS11 ")) font.setSize(11);
		else if (wslAlias2UC.contains(" T-FS10 ")) font.setSize(10);
		else if (wslAlias2UC.contains(" T-FS9 ")) font.setSize(9);
		else if (wslAlias2UC.contains(" T-FS8 ")) font.setSize(8);
		else if (wslAlias2UC.contains(" T-FS7 ")) font.setSize(7);
		else if (wslAlias2UC.contains(" T-FS6 ")) font.setSize(6);
		else if (wslAlias2UC.contains(" T-FS5 ")) font.setSize(5);
		else if (wslAlias2UC.contains(" T-FS4 ")) font.setSize(4);
		else font.setSize(14);
	}


	private static void setFontSize(Font font, String wslAlias2UC) {
		     if (wslAlias2UC.contains(" FS20 ")) font.setSize(20);
		else if (wslAlias2UC.contains(" FS19 ")) font.setSize(19);
		else if (wslAlias2UC.contains(" FS18 ")) font.setSize(18);
		else if (wslAlias2UC.contains(" FS17 ")) font.setSize(17);
		else if (wslAlias2UC.contains(" FS16 ")) font.setSize(16);
		else if (wslAlias2UC.contains(" FS15 ")) font.setSize(15);
		else if (wslAlias2UC.contains(" FS14 ")) font.setSize(14);
		else if (wslAlias2UC.contains(" FS13 ")) font.setSize(13);
		else if (wslAlias2UC.contains(" FS12 ")) font.setSize(12);
		else if (wslAlias2UC.contains(" FS11 ")) font.setSize(11);
		else if (wslAlias2UC.contains(" FS10 ")) font.setSize(10);
		else if (wslAlias2UC.contains(" FS9 ")) font.setSize(9);
		else if (wslAlias2UC.contains(" FS8 ")) font.setSize(8);
		else if (wslAlias2UC.contains(" FS7 ")) font.setSize(7);
		else if (wslAlias2UC.contains(" FS6 ")) font.setSize(6);
		else if (wslAlias2UC.contains(" FS5 ")) font.setSize(5);
		else if (wslAlias2UC.contains(" FS4 ")) font.setSize(4);
		else font.setSize(10);
	}


	private static void appendPdfTitle(Document pdf, String title) throws DocumentException {
		if (title==null || title.isEmpty()) return;

		Font font14 = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
		font14.setSize(14);

		Paragraph paragraph = new Paragraph(title, font14);
		paragraph.setAlignment(Element.ALIGN_CENTER);
		pdf.add(paragraph);
		pdf.add(new Paragraph(" ", font14));

	}


	private static void appendPdfLwTitle(Document pdf, SqlDataSelectionsHandler lw, String wslAlias2) throws DocumentException {
		Font lwOpisFont14 = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
		setLwTitleFontSize(lwOpisFont14, wslAlias2);//T-FS20 //lwOpisFont14.setSize(14);

		Font font6 = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
		font6.setSize(6);

		Paragraph paragraph = new Paragraph(nz(lw.getName()), lwOpisFont14);
		paragraph.setAlignment(getLwTitleHorizontalAligmentByCssClass(wslAlias2)); //Element.ALIGN_CENTER);

		pdf.add(paragraph);
		pdf.add(new Paragraph(" ", font6));
	}


	private static void appendPdfLW(Document pdf, SqlDataSelectionsHandler lw, float width, int horizontalAligment) throws DocumentException {
		boolean czyAgregacja = false;
	    int i = 0;
		List<PptAdmListyWartosci> pdfCols = lw.getLst().stream().filter(c -> nz(c.getLstDlugosc800600()) > 0L).collect(Collectors.toList());
		PdfPTable tb = new PdfPTable(pdfCols.size());
		Font cellFont = FontFactory.getFont(FontFactory.TIMES, "cp1250", 10);
		Font facetFont = FontFactory.getFont(FontFactory.TIMES, "cp1250", 10, Font.BOLD);

		float[] widths = new float[pdfCols.size()];
		for (PptAdmListyWartosci col : pdfCols) {
			widths[i] = nz(col.getLstDlugosc800600());
			if (lw.agregationClass(col.getLstNazwaPola()) !=null)
			    czyAgregacja=true;
			i++;
		}
		tb.setWidths(widths);
		tb.setWidthPercentage(width);
		tb.setHorizontalAlignment(horizontalAligment);

		for (PptAdmListyWartosci col : pdfCols) {
			PdfPCell celll = new PdfPCell(new Phrase(new Paragraph(col.getLstEtykieta(), facetFont)));
			tb.addCell(celll);
		}

		for (DataRow r : lw.getData()) {
			for (PptAdmListyWartosci col : pdfCols) {

				String colName = col.getLstNazwaPola();
				Object x = r.get(colName);

				String txt = r.getFormattedValue(colName);
				PdfPCell cell = new PdfPCell(new Phrase(new Paragraph(txt, cellFont)));
				cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

				if (x instanceof Number)
					cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
				tb.addCell(cell);
			}
		}

		if (czyAgregacja)
		for (PptAdmListyWartosci col : pdfCols) {

			String aggregation = nz(lw.agregationClass(col.getLstNazwaPola()));
			Object aggValue = lw.agregation(col.getLstNazwaPola());

			PdfPCell cell = new PdfPCell();

			if (aggValue != null) {

				if ("min".equals(aggregation)) aggregation = "min.:";
				else if ("max".equals(aggregation)) aggregation = "maks.:";
				else if ("sum".equals(aggregation)) aggregation = "suma:";
				else if ("avg".equals(aggregation)) aggregation = "śr.:";
				else if ("count".equals(aggregation)) aggregation = "zlicz:";

				Paragraph p1 = new Paragraph(aggregation, cellFont);
				p1.setAlignment(Element.ALIGN_LEFT);

				Paragraph p2 = new Paragraph("" + aggValue, facetFont);
				p2.setAlignment(Element.ALIGN_RIGHT);

				cell.addElement(p1);
				cell.addElement(p2);

			}

			tb.addCell(cell);
		}

		pdf.add(tb);
	}



	public static void appendPdfHeader(Document document) throws IOException, DocumentException {
		Document pdf = document;
		pdf.setPageSize(PageSize.A4);//A4 PORTRAIT
		//pdf.setPageSize(PageSize.A4.rotate());//A4 LANDSCAPE
		// header/footer musi być przed pdf.open() żeby wyświetlił się też na pierwszej stronie

		Font font6 = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
		font6.setSize(6);

		Font font8 = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
		font8.setSize(8);

		Font font8gray = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
		font8gray.setSize(8);
		font8gray.setColor(Color.GRAY);

		Font font14 = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
		font14.setSize(14);


		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();

		String webappsFolder = AppBean.app.getApplicationContextFolder().getParent();
		String egeriaImagesFolder = webappsFolder + File.separator + "egeria"+ File.separator + "images"+ File.separator;

		String logo = egeriaImagesFolder + "logoRpt_"+User.getCurrentUser().getFrmId()+".png";
		if (!(new File(logo)).exists()) {
			logo = egeriaImagesFolder + "logoRpt_" + User.getCurrentUser().getFrmId() + ".jpg";
		}
		if (!(new File(logo)).exists()){
			logo = externalContext.getRealPath("") + File.separator + "resources" + File.separator + "images" + File.separator + "MF_logo3.png";
		}

		Image img = Image.getInstance(logo);
		img.scaleToFit(100, 60);


		float[] widths = {70f, 30f};
		Table tb = new Table(2, 1);//l kolumn, l wierszy
		tb.setWidths(widths);

		Cell cell1 = new Cell();
		cell1.add(img);
		cell1.setBorder(Rectangle.NO_BORDER);

		User user = User.getCurrentUser();
		Paragraph paragraphHeaderRight1 = new Paragraph("Wykonano: " + Utils.format(new Date(),"dd-MM-yyyy HH:mm:ss"), font8);
		Paragraph paragraphHeaderRight2 = new Paragraph("Przez: " + user.getEkPracownik().format("%prc_imie$s %prc_nazwisko$s"), font8);
		Paragraph paragraphHeaderRight3 = new Paragraph("Użytkownik: " + user.getDbLogin(), font8gray);

		Cell cell2 = new Cell();
		cell2.setBorder(Rectangle.NO_BORDER);
		cell2.add(paragraphHeaderRight1);
		cell2.add(paragraphHeaderRight2);
		cell2.add(paragraphHeaderRight3);
		//cell1.setBackgroundColor(Color.GREEN);
		tb.addCell(cell1);
		tb.addCell(cell2);
		tb.setWidth(100f);//100%
		tb.setBorder(Rectangle.NO_BORDER);



		Paragraph paragraphHeader = new Paragraph();
		paragraphHeader.add(tb);
		HeaderFooter header = new HeaderFooter(paragraphHeader, false);
		header.setBorder(Rectangle.NO_BORDER);

//		Phrase phraseFooter = new Phrase( String.format("Str. ", font8gray));// "%1$s %2$s / Strona ", lwPageInfo(), filterInfo() ), font8);
//		HeaderFooter footer = new HeaderFooter(phraseFooter, true);
//		footer.setAlignment(Element.ALIGN_RIGHT);
//		footer.setBorder(Rectangle.NO_BORDER);
//		pdf.setFooter(footer);

		pdf.open();
		pdf.add(tb); //pdf.add(img);
	}
		
	public static void lWtoJRXML(String sqlId, Map<String, Object> sqlNamedParams) throws IOException, JRException {
		
		List<PptAdmListyWartosci> lwKolumny;
		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			Query query = s.createQuery("from PptAdmListyWartosci where lstNazwa = :lstNazwa");
			query.setParameter("lstNazwa", sqlId);
			lwKolumny = query.getResultList();
			s.close();
		}
		String select = lwKolumny.stream().filter(c -> nz(c.getLstQuery())!="" ).collect(Collectors.toList()).get(0).getLstQuery();
		lwKolumny = lwKolumny.stream().filter(c -> nz(c.getLstDlugosc800600()) > 0L).collect(Collectors.toList());
		long szerokoscCala = lwKolumny.stream().mapToLong( x -> x.getLstDlugosc800600()).sum();
		Date dataModyfikacji = lwKolumny.stream()
				//.filter(x -> x.getLstLp()!=1)
				.map(u -> u.getLstAudytDm()!=null ? u.getLstAudytDm() : u.getLstAudytDt() )
				.max(Date::compareTo).get();
		        	
		String jrxmlFileRealPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/WEB-INF/protectedElements/Reports/sourceReports/lwToRaport.jrxml.tmpl");
		File file = new File(jrxmlFileRealPath);
		
		if (!file.exists()) {
			System.err.println("Brak dostepu do szabonu raportu lwToRaport.jrxml.");
			return;
		}
		
		String jrxmlFileRealPathLW = FacesContext.getCurrentInstance().getExternalContext()
				.getRealPath("/WEB-INF/protectedElements/Reports/sourceReports/"+sqlId+".jrxml");
		File fileLW = new File(jrxmlFileRealPathLW);
					
		if(!fileLW.exists() || (fileLW.lastModified() - file.lastModified()<0) || (fileLW.lastModified() - AppBean.app.getStartupTime().getTime()<0)) {
	
		
				List<String> lines = Files.readAllLines( Paths.get(  file.getAbsolutePath()  ) );
				String txt = String.join("\n", lines);
				
				List<String> al = new ArrayList<>();
				
				for (String line : lines) {
					
			    	if(line.trim().startsWith("!parameter!")) {//parametry
			            for (Map.Entry par : sqlNamedParams.entrySet()) {
				    		//<parameter name="Parameter_1" class="java.lang.String"/>//tutaj nie wiem czy jest sens sprawdzac jaki to jest typ i do niego mapowac
			            															  //czy zostawic tak jak jest teraz czyli zawsze do String
			            	
			            	al.add("<parameter name=\""  +par.getKey()+  "\" class=\"java.lang.String\"/>"+"\n");
			            }
		
			    	} else if(line.trim().startsWith("!SELECT!")) {//zapytanie
			    		
			            for (Map.Entry par : sqlNamedParams.entrySet()) {//podmiana w select parametrów
			            	select = select.replace(":" + par.getKey(), "$P{"+par.getKey()+"}");
			            }
			    		
			    		al.add(select);
			    		
			    	} else if(line.trim().startsWith("!field!")) {
			    		
			            for (PptAdmListyWartosci kolumna : lwKolumny) {
			            	//<field name="POLE1" class="java.lang.String">
			        		//<property name="com.jaspersoft.studio.field.label" value="POLE1"/>
			        		//</field>
			            	
			            	al.add("<field name=\""+kolumna.getLstNazwaPola()+"\" class=\"java.lang.String\">" +"\n");
			            	al.add("<property name=\"com.jaspersoft.studio.field.label\" value=\""+kolumna.getLstNazwaPola()+"\"/>"+"\n");
			            	al.add("</field>"+"\n");
			            }
			            
			    	} else if(line.trim().startsWith("!textField!")) {
			    		 int szerokosc = 0;
			    		 int sumaSzerokosci = 5;
			    		 for (PptAdmListyWartosci kolumna : lwKolumny) {
							//<textField>
							//<reportElement x="150" y="0" width="150" height="30"/>
			 				//<box>
							//<topPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
							//<leftPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
							//<bottomPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
							//<rightPen lineWidth="1.0" lineStyle="Solid" lineColor="#000000"/>
							//</box>
							//<textFieldExpression><![CDATA[$F{POLE2}]]></textFieldExpression>
							//</textField>
			    			 
			    			szerokosc = (int) (kolumna.getLstDlugosc800600() * 510 / szerokoscCala);
			    			 
			    			al.add("<textField>"+"\n");
			    			al.add("<reportElement x=\"" +sumaSzerokosci+ "\" y=\"0\" width=\"" +szerokosc+ "\" height=\"30\"/>" +"\n");
			    			al.add("<box>"+"\n");
			    			al.add("<topPen lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>"+"\n");
			    			al.add("<leftPen lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>"+"\n");
			    			al.add("<bottomPen lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>"+"\n");
			    			al.add("<rightPen lineWidth=\"1.0\" lineStyle=\"Solid\" lineColor=\"#000000\"/>"+"\n");
			    			al.add("</box>"+"\n");
			    			al.add("<textFieldExpression><![CDATA[$F{"+kolumna.getLstNazwaPola()+"}]]></textFieldExpression>"+"\n");
			    			al.add("</textField>"+"\n");
			    			
			    			sumaSzerokosci = sumaSzerokosci + szerokosc;
			    		 }
			    	} else {
			    		al.add(line +"\r\n");
			    	}	
				}
				
				if (al.size()!=lines.size()) {//zapis pliku przygotowanego nazwa_listy_wartosci.jrxml
					String ntxt = String.join("", al);
					BufferedWriter writer = new BufferedWriter(new FileWriter(jrxmlFileRealPathLW, false));
					writer.write(ntxt);			
					writer.close();
				}
		
		}
		
		
		
		//kompilowanie jrxml to jasper
		String jasperFilePathLW = "/WEB-INF/protectedElements/Reports/compiledReports/" + sqlId + ".jasper";
		String jasperFileRealPathLW = FacesContext.getCurrentInstance().getExternalContext().getRealPath(jasperFilePathLW);
		File fileJasper = new File(jasperFileRealPathLW);
		File fileJrxml = new File(jrxmlFileRealPathLW);
		long lmjasper = fileJasper.lastModified();
		long lmjrxml = fileJrxml.lastModified();
		if (
				(!fileJasper.exists() && fileJrxml.exists())//brak pliku jasper ale jest plik jrxml
				|| lmjasper < lmjrxml //data modyfikacji jrxml > daty mpodyfikacji jasper'a
		){
			JasperCompileManager.compileReportToFile(jrxmlFileRealPathLW, jasperFileRealPathLW);
		}
		
	}
	
	
	
	public static void lWStoJRXML(LinkedHashMap<String, Map<String, Object>> listyWartosci) throws IOException {
		
		listyWartosci.forEach((key,value) -> {
		    
		    try {
				ReportGenerator.lWtoJRXML(key,value);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JRException e) {
				e.printStackTrace();
			}
		    
		});
		
		
		
		String jrxmlFileRealPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/WEB-INF/protectedElements/Reports/sourceReports/lwStoRaport.jrxml.tmpl");
		File file = new File(jrxmlFileRealPath);
		
		if (!file.exists()) {
			System.err.println("Brak dostepu do szabonu raportu lwStoRaport.jrxml.tmpl.");
			return;
		}
		
		List<String> lines = Files.readAllLines( Paths.get(  file.getAbsolutePath()  ) );
		String txt = String.join("\n", lines);
		
		List<String> al = new ArrayList<>();
		
		for (String line : lines) {
	    	if(line.trim().startsWith("!subreport!")) {//subreport'y

	    		for (Map.Entry lw : listyWartosci.entrySet()) {
	    		    //*<band height="30" splitType="Stretch">
	    		    //* <property name="com.jaspersoft.studio.unit.height" value="px"/>
	    			//<subreport>
					//	<reportElement positionType="Float" x="0" y="0" width="521" height="10" uuid="01c60e7d-6ee6-4746-acc1-8aca0ec8752b"/>
					//	<subreportParameter name="POLE_1">
					//		<subreportParameterExpression><![CDATA[101236]]></subreportParameterExpression>
					//	</subreportParameter>
					//	<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
					//	<subreportExpression><![CDATA["subTesty.jasper"]]></subreportExpression>
					//</subreport>
	    		    //* </band>

	    			al.add("<band height=\"10\" splitType=\"Stretch\">"+"\n");
	    			al.add("<property name=\"com.jaspersoft.studio.unit.height\" value=\"px\"/>"+"\n");
	    			al.add("<subreport>"+"\n");
	    			al.add("<reportElement positionType=\"Float\" x=\"0\" y=\"0\" width=\"521\" height=\"10\"/>"+"\n");
	    			for (Map.Entry lwPar : ((HashMap<String, Object>) lw.getValue()).entrySet()) {
	    				//<subreportParameter name="POLE_1">
							//<subreportParameterExpression><![CDATA[101236]]></subreportParameterExpression>
						//</subreportParameter>
	    				
	    				al.add("<subreportParameter name=\"" + lwPar.getKey() + "\">"+"\n");
	    				al.add("<subreportParameterExpression><![CDATA[\"" + lwPar.getValue() + "\"]]></subreportParameterExpression>"+"\n");
	    				al.add("</subreportParameter>"+"\n");
	    				
	    			}
	    			
	    			
	    			al.add("<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>"+"\n");
	    			al.add("<subreportExpression><![CDATA[\"" + lw.getKey() + ".jasper\"]]></subreportExpression>"+"\n");
	    			al.add("</subreport>"+"\n");
	    			al.add("</band>"+"\n");
	    		}

	    	} else {
	    		al.add(line +"\r\n");
	    	}
		}
			
		
		String nazwaPliku = new Random().nextInt(10) + "" + new Date().getTime(); //wygenerowanie losowej nazy, ona jest bez znaczenia bo i tak zaraz ten plik
																				  //bedzie usunięty
    	jrxmlFileRealPath = jrxmlFileRealPath.replace("lwStoRaport.jrxml.tmpl", nazwaPliku + ".jrxml");
		if (al.size()!=lines.size()) {//zapis pliku przygotowanego lwStoRaport.jrxml
			String ntxt = String.join("", al);
			BufferedWriter writer = new BufferedWriter(new FileWriter(jrxmlFileRealPath, false));
			writer.write(ntxt);			
			writer.close();
		}
			

		try {//drukowanie raportu
			ReportGenerator.displayReportAsPDF(null, nazwaPliku);
		} catch (JRException | SQLException | IOException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
			
		//usuniecie jrxml
		File f= new File(jrxmlFileRealPath);
		f.delete(); 
		
		//usuniecie jasper
		String jasperFileRealPath = jrxmlFileRealPath.replace("sourceReports", "compiledReports");
		jasperFileRealPath = jasperFileRealPath.replace(".jrxml", ".jasper");
		f= new File(jasperFileRealPath);
		f.delete(); 
			
	}
		

}