package com.comarch.egeria.web.common.view;

import com.comarch.egeria.pp.danepracownika.pracownik.PracownikPowiadomieniaSqlBean;

import javax.faces.FacesException;
import javax.faces.application.ViewHandler;
import javax.faces.application.ViewHandlerWrapper;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import java.io.IOException;

public class EgrViewHandlerWrapper extends ViewHandlerWrapper{
	
	private ViewHandler wrapped;

	
	public EgrViewHandlerWrapper(final ViewHandler wrapped) {
		this.wrapped = wrapped;
	}
	
	
	@Override
	public UIViewRoot restoreView(final FacesContext context, final String viewId) {
//		System.out.println(Utils.strTS(false) + " restoreView.................. " + viewId + "......");
		UIViewRoot root = wrapped.restoreView(context, viewId);
//		System.out.println(Utils.strTS(true) + " restoreView.................. " + viewId + " DONE");//kilka sekud na del zagr???

		if (root == null) {
			
			if ("/faces/Powiadomienia/Powiadomienia.xhtml".equals(viewId) ){//dla tego widoku sztucznie utrzymujemy stan przy zyciu (w sesyjnym beanie)  nawet jesli zostanie on wyrzucony przez przekorczenie ustawienia javax.faces.STATE_SAVING_METHOD
				//root = createView(context, viewId);
				PracownikPowiadomieniaSqlBean powiadomieniaBean = (PracownikPowiadomieniaSqlBean) context.getExternalContext().getSessionMap().get("pracownikPowiadomieniaSqlBean");
				root = powiadomieniaBean.viewState;//uwaga na mozliwosc otwierania wielokrotnie w  roznych zakøadkach przegladarki - np. stan pól do filtroania nie zostaje zsynchronizowany miédzy zakladkami 
			}
			else if ("/faces/Powiadomienia/Msgs.xhtml".equals(viewId) ){
				root = createView(context, viewId);
			}
			
			//else 
			//   root = createView(context, viewId); //tak mozna sie tez niekiedy pozbyc problemu ViewExpiredException
			
			//FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Please avoid using browser back-button","The application was not able to serve the last request. Please make sure that you are using the designated navigaton-links only.");
			//context.addMessage(null, fm);
		} 
		
		return root;
	}




	@Override
	public void renderView(FacesContext context, UIViewRoot viewToRender) throws IOException, FacesException {
//		System.out.println(Utils.strTS(false) + " renderView.................. " + viewToRender.getViewId() );
		super.renderView(context, viewToRender);
//		System.out.println(Utils.strTS(true) + " renderView.................. " + viewToRender.getViewId()+ " DONE" );
	}






	@Override
	public void writeState(FacesContext context) throws IOException {
//		System.out.println(Utils.strTS(false) + " writeState.................. " );
		String viewId = context.getViewRoot().getViewId();
		if ("/faces/Powiadomienia/Powiadomienia.xhtml".equals(viewId) ){
			PracownikPowiadomieniaSqlBean powiadomieniaBean = (PracownikPowiadomieniaSqlBean) context.getExternalContext().getSessionMap().get("pracownikPowiadomieniaSqlBean");
			powiadomieniaBean.viewState = context.getViewRoot(); 
		}
		//String clientId = context.getViewRoot().getClientId();
		super.writeState(context);
//		System.out.println(Utils.strTS(true) + " writeState.................. DONE");
	}
	
	@Override
	public ViewHandler getWrapped() {
		return wrapped;
	}
	

}

