package com.comarch.egeria.web.common.utils.file;

public class FileUtils {
	public static String[] extractFileNameAndExt(String filename) {
		String[] fileNameAndExt = new String[2];
		int lastIndex = filename.lastIndexOf(".");

		fileNameAndExt[0] = filename.substring(0, lastIndex);
		fileNameAndExt[1] = filename.substring(lastIndex + 1);

		return fileNameAndExt;
	}
}
