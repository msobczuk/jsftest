package com.comarch.egeria.web.common.components;

import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Iterator;

import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.context.annotation.Scope;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;

@Named
@Scope("session")
public class CtlContextMenuBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LogManager.getLogger(CtlContextMenuBean.class);

	public void postProcessXLS(Object document) {
		setCellsFormatXLS(document);
	}

	public void postProcessXLSX(Object document) {
		setCellsFormatXLSX(document);
	}

	public void preProcessPDF(Object document) throws IOException, BadElementException, DocumentException {
		Document pdf = (Document) document;
		pdf.open();
		pdf.setPageSize(PageSize.A0);
	}

	private void setCellsFormatXLS(Object document) {
		try {
			HSSFWorkbook wbn = (HSSFWorkbook) document;
			HSSFSheet sheetn = wbn.getSheetAt(0);

			Iterator<Cell> cellIt;

			int lastRowIndex = sheetn.getLastRowNum();

			if (lastRowIndex > 0) {

				CellStyle borderCellStyle = wbn.createCellStyle();
				borderCellStyle.setBorderTop(BorderStyle.THIN);
				borderCellStyle.setBorderRight(BorderStyle.THIN);
				borderCellStyle.setBorderBottom(BorderStyle.THIN);
				borderCellStyle.setBorderLeft(BorderStyle.THIN);

				Font boldFont = wbn.createFont();
				boldFont.setBold(true);

				CellStyle headerCellStyle = wbn.createCellStyle();
				headerCellStyle.setBorderTop(BorderStyle.THIN);
				headerCellStyle.setBorderRight(BorderStyle.THIN);
				headerCellStyle.setBorderBottom(BorderStyle.THIN);
				headerCellStyle.setBorderLeft(BorderStyle.THIN);
				headerCellStyle.setFont(boldFont);

				CellStyle currencyCellStyle = wbn.createCellStyle();
				currencyCellStyle.setDataFormat(wbn.getCreationHelper().createDataFormat().getFormat("#,##0.00"));
				currencyCellStyle.setBorderTop(BorderStyle.THIN);
				currencyCellStyle.setBorderRight(BorderStyle.THIN);
				currencyCellStyle.setBorderBottom(BorderStyle.THIN);
				currencyCellStyle.setBorderLeft(BorderStyle.THIN);

				CellStyle footerCellStyle = wbn.createCellStyle();
				footerCellStyle.setBorderBottom(BorderStyle.THIN);

				CellStyle footerCurrencyCellStyle = wbn.createCellStyle();
				footerCurrencyCellStyle.setDataFormat(wbn.getCreationHelper().createDataFormat().getFormat("#,##0.00"));
				footerCurrencyCellStyle.setBorderTop(BorderStyle.THIN);
				footerCurrencyCellStyle.setBorderRight(BorderStyle.THIN);
				footerCurrencyCellStyle.setBorderBottom(BorderStyle.THIN);
				footerCurrencyCellStyle.setBorderLeft(BorderStyle.THIN);
				footerCurrencyCellStyle.setFont(boldFont);

				CellStyle footerLeftCellStyle = wbn.createCellStyle();
				footerLeftCellStyle.setBorderBottom(BorderStyle.THIN);
				footerLeftCellStyle.setBorderLeft(BorderStyle.THIN);

				CellStyle footerRightCellStyle = wbn.createCellStyle();
				footerRightCellStyle.setBorderRight(BorderStyle.THIN);
				footerRightCellStyle.setBorderBottom(BorderStyle.THIN);

				CellStyle dateCellStyle = wbn.createCellStyle();
				dateCellStyle.setDataFormat(wbn.getCreationHelper().createDataFormat().getFormat("yyyy-mm-dd"));
				dateCellStyle.setAlignment(HorizontalAlignment.CENTER);
				dateCellStyle.setBorderTop(BorderStyle.THIN);
				dateCellStyle.setBorderRight(BorderStyle.THIN);
				dateCellStyle.setBorderBottom(BorderStyle.THIN);
				dateCellStyle.setBorderLeft(BorderStyle.THIN);

				SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");

				for (int i = 0; i <= lastRowIndex; i++) {
					cellIt = sheetn.getRow(i).cellIterator();

					int cellIndex = 0;
					while (cellIt.hasNext()) {
						Cell cell = cellIt.next();
						sheetn.autoSizeColumn(cellIndex);

						if (i == 0) {
							cell.setCellStyle(headerCellStyle);
						} else if (i == lastRowIndex) {
							if (cellIndex == 0)
								cell.setCellStyle(footerLeftCellStyle);
							else if (!cellIt.hasNext())
								cell.setCellStyle(footerRightCellStyle);
							else
								cell.setCellStyle(footerCellStyle);
						} else {
							cell.setCellStyle(borderCellStyle);
						}

						// wyciaganie wartosci tymczasowych
						String cellValueString = cell.getStringCellValue();
						double cellValueDouble = -1;

						// sprawdzanie, czy wartosc to kwota albo data
						if (cellValueString.matches("^-?\\d{1,3}(,\\d{3})*\\.\\d{2}$"))
							cellValueDouble = Double.parseDouble(cellValueString.replaceAll(",", ""));
						else if (cellValueString.matches("^-?\\d{1,3}(\\h\\d{3})*,\\d{2}$"))
							cellValueDouble = Double
									.parseDouble(cellValueString.replaceAll("\\h", "").replace(',', '.'));
						else if (cellValueString.matches("^\\d{4}(-\\d{2}){2}$")) {
							cell.setCellStyle(dateCellStyle);
							try {
								cell.setCellValue(format.parse(cellValueString));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						// jesli jest kwota
						if (cellValueDouble != -1) {
							// usuwanie komorki
							sheetn.getRow(i).removeCell(cell);

							// tworzenie komorki od nowa, z typem numerycznym
							// i poprawnym formatowaniem
							cell = sheetn.getRow(i).createCell(cellIndex, CellType.NUMERIC);

							if (i == lastRowIndex)
								cell.setCellStyle(footerCurrencyCellStyle);
							else
								cell.setCellStyle(currencyCellStyle);

							// wstawianie wartosci jako numeryczna
							cell.setCellValue(cellValueDouble);
						}

						cellIndex++;
					}
				}
			}
			wbn.close();
		} catch (IOException e1) {
			System.err.println("Blad metody ContextMenuBean.whichColumnsAreNumeric()");
			e1.printStackTrace();
		}
	}

	private void setCellsFormatXLSX(Object document) {
		
		try {
			SXSSFWorkbook wbn = (SXSSFWorkbook) document;
			SXSSFSheet sheetn = wbn.getSheetAt(0);

			int lastRowIndex = sheetn.getLastRowNum();

			if (lastRowIndex > 0) {

				CellStyle borderCellStyle = wbn.createCellStyle();
				borderCellStyle.setBorderTop(BorderStyle.THIN);
				borderCellStyle.setBorderRight(BorderStyle.THIN);
				borderCellStyle.setBorderBottom(BorderStyle.THIN);
				borderCellStyle.setBorderLeft(BorderStyle.THIN);

				Font boldFont = wbn.createFont();
				boldFont.setBold(true);

				CellStyle headerCellStyle = wbn.createCellStyle();
				headerCellStyle.setBorderTop(BorderStyle.THIN);
				headerCellStyle.setBorderRight(BorderStyle.THIN);
				headerCellStyle.setBorderBottom(BorderStyle.THIN);
				headerCellStyle.setBorderLeft(BorderStyle.THIN);
				headerCellStyle.setFont(boldFont);

				CellStyle currencyCellStyle = wbn.createCellStyle();
				currencyCellStyle.setDataFormat(wbn.getCreationHelper().createDataFormat().getFormat("#,##0.00"));
				currencyCellStyle.setBorderTop(BorderStyle.THIN);
				currencyCellStyle.setBorderRight(BorderStyle.THIN);
				currencyCellStyle.setBorderBottom(BorderStyle.THIN);
				currencyCellStyle.setBorderLeft(BorderStyle.THIN);

				CellStyle footerCellStyle = wbn.createCellStyle();
				footerCellStyle.setBorderBottom(BorderStyle.THIN);

				CellStyle footerCurrencyCellStyle = wbn.createCellStyle();
				footerCurrencyCellStyle.setDataFormat(wbn.getCreationHelper().createDataFormat().getFormat("#,##0.00"));
				footerCurrencyCellStyle.setBorderTop(BorderStyle.THIN);
				footerCurrencyCellStyle.setBorderRight(BorderStyle.THIN);
				footerCurrencyCellStyle.setBorderBottom(BorderStyle.THIN);
				footerCurrencyCellStyle.setBorderLeft(BorderStyle.THIN);
				footerCurrencyCellStyle.setFont(boldFont);

				CellStyle footerLeftCellStyle = wbn.createCellStyle();
				footerLeftCellStyle.setBorderBottom(BorderStyle.THIN);
				footerLeftCellStyle.setBorderLeft(BorderStyle.THIN);

				CellStyle footerRightCellStyle = wbn.createCellStyle();
				footerRightCellStyle.setBorderRight(BorderStyle.THIN);
				footerRightCellStyle.setBorderBottom(BorderStyle.THIN);

				CellStyle dateCellStyle = wbn.createCellStyle();
				dateCellStyle.setDataFormat(wbn.getCreationHelper().createDataFormat().getFormat("yyyy-mm-dd"));
				dateCellStyle.setAlignment(HorizontalAlignment.CENTER);
				dateCellStyle.setBorderTop(BorderStyle.THIN);
				dateCellStyle.setBorderRight(BorderStyle.THIN);
				dateCellStyle.setBorderBottom(BorderStyle.THIN);
				dateCellStyle.setBorderLeft(BorderStyle.THIN);

				SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");

				log.debug("sheetn.getLastRowNum() " + sheetn.getLastRowNum());
				log.debug("sheetn.getRow(0) " + sheetn.getRow(0));
				log.debug("sheetn.getRow(1) " + sheetn.getRow(1));
				log.debug("sheetn.getDefaultRowHeight()) " + sheetn.getDefaultRowHeight());

				int numberOfCells = 0;
				if (lastRowIndex > 0) {
					log.debug("sheetn.getRow(0) " + sheetn.getRow(0) + " |");
					numberOfCells = sheetn.getRow(0).getPhysicalNumberOfCells();
				}
				for (int i = 0; i <= lastRowIndex; i++) {
					// log.debug("lastRowIndex " + lastRowIndex);

					for (int cellIndex = 0; cellIndex < numberOfCells; cellIndex++) {

						Cell cell = sheetn.getRow(i).getCell(cellIndex);
						// log.debug(" sheetn.getRow(" + i +
						// ").getCell(cellIndex) " + cell);

						if (i == 0) {
							cell.setCellStyle(headerCellStyle);
						} else if (i == lastRowIndex) {
							if (cellIndex == 0)
								cell.setCellStyle(footerLeftCellStyle);
							// else if (!cellIt.hasNext())
							// cell.setCellStyle(footerRightCellStyle);
							else
								cell.setCellStyle(footerCellStyle);
						} else {
							cell.setCellStyle(borderCellStyle);
						}

						// wyciaganie wartosci tymczasowych
						String cellValueString = cell.getStringCellValue();
						double cellValueDouble = -1;

						// sprawdzanie, czy wartosc to kwota albo data
						if (cellValueString.matches("^-?\\d{1,3}(,\\d{3})*\\.\\d{2}$"))
							cellValueDouble = Double.parseDouble(cellValueString.replaceAll(",", ""));
						else if (cellValueString.matches("^-?\\d{1,3}(\\h\\d{3})*,\\d{2}$"))
							cellValueDouble = Double
									.parseDouble(cellValueString.replaceAll("\\h", "").replace(',', '.'));
						else if (cellValueString.matches("^\\d{4}(-\\d{2}){2}$")) {
							cell.setCellStyle(dateCellStyle);
							try {
								cell.setCellValue(format.parse(cellValueString));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}

						// jesli jest kwota
						if (cellValueDouble != -1) {
							// usuwanie komorki
							sheetn.getRow(i).removeCell(cell);

							// tworzenie komorki od nowa, z typem numerycznym
							// i poprawnym formatowaniem
							cell = sheetn.getRow(i).createCell(cellIndex, CellType.NUMERIC);

							if (i == lastRowIndex)
								cell.setCellStyle(footerCurrencyCellStyle);
							else
								cell.setCellStyle(currencyCellStyle);

							// wstawianie wartosci jako numeryczna
							cell.setCellValue(cellValueDouble);
						}

					}
				}
			}
			log.debug("wbn.close(); P");
			// wbn.close();
			log.debug("wbn.close(); K");
		} catch (Exception e1) {
			System.err.println("Blad metody ContextMenuBean.whichColumnsAreNumeric()");
			e1.printStackTrace();
		}
	}

}
