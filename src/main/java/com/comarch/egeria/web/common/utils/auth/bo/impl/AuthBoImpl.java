package com.comarch.egeria.web.common.utils.auth.bo.impl;

import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.utils.auth.EgeriaErrorMapper;
import com.comarch.egeria.web.common.utils.auth.exceptions.PasswordValidationException;
import com.comarch.egeria.web.common.utils.auth.bo.AuthBo;
import com.comarch.egeria.web.common.utils.auth.exceptions.EgrAuthInitException;
import com.comarch.egeria.web.common.utils.auth.exceptions.EgrAuthenticationException;
import com.comarch.egeria.web.common.utils.auth.exceptions.EgrContextException;
import com.comarch.egeria.web.common.utils.auth.exceptions.RepeatedPasswordException;
import com.comarch.egeria.web.common.utils.auth.exceptions.SpecialPasswordException;
import com.comarch.egeria.web.common.utils.auth.model.PasswordResetToken;
import com.comarch.egeria.web.common.utils.auth.utils.EgrPreAuthData;
import com.comarch.egeria.web.common.utils.auth.utils.PasswordResetException;
import com.comarch.egeria.web.common.utils.constants.Const;
import com.comarch.egeria.web.common.utils.lang.LanguageBean;
import oracle.jdbc.OracleTypes;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.CachedRowSet;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

/**
 * Implementacja interfejsu AuthBo
 *
 * @author Comarch SA
 */
@Named
public class AuthBoImpl implements AuthBo {

  private static final Logger log = LoggerFactory.getLogger(AuthBoImpl.class);

  /**
   * Sprawdza użytkownika w eat_uzytkownicy.
   *
   * @param username login użytkownika (nazwa zewnętrzna użytkownika w eat_uzytkownicy)
   * @return EgrPreAuthData
   * @throws SQLException, EgrAuthenticationException
   */
  public EgrPreAuthData odczytajUzytkownika(String username)
      throws EgrAuthenticationException {
    EgrPreAuthData egrPreAuthData = new EgrPreAuthData();
    String ret;
    String poKomunikat;

    egrPreAuthData.setUsername(username);

    try (Connection conn = JdbcUtils.getConnection(false)) {
      CallableStatement cs = conn
          .prepareCall("{? = call eap_web.odczytaj_uzytkownika( ?, ?, ?, ?, ?, ? )}");
      cs.registerOutParameter(1, OracleTypes.VARCHAR);
      cs.registerOutParameter(2, OracleTypes.VARCHAR);
      cs.setString(2, username);
      cs.setString(3, Const.NAZWA_KOMPONENTU_CRU);
      cs.setString(4, "N");
      cs.registerOutParameter(5, OracleTypes.VARCHAR);
      cs.registerOutParameter(6, OracleTypes.VARCHAR);
      cs.registerOutParameter(7, OracleTypes.VARCHAR);
      cs.execute();
      ret = cs.getString(1);

      if ("T".equals(ret)) {
        egrPreAuthData.setDbUsername(cs.getString(5));
        egrPreAuthData.setAuthMethod(cs.getString(6));
        egrPreAuthData.setUsername(cs.getString(2));

      } else {
        poKomunikat = cs.getString(7);
        log.error(poKomunikat);
        String errorCode = EgeriaErrorMapper.mapToErrorCode(poKomunikat);
        throw new EgrAuthenticationException(LanguageBean.translate(errorCode));
      }
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
      throw new EgrAuthenticationException(LanguageBean.translate("INF_login_error"));
    }
    log.debug("return " + egrPreAuthData.toString());
    return egrPreAuthData;
  }

  /**
   * Próbuje połączyć się z bazą za pomocą podanego użytkownika/hasła. Wyrzuca błąd w przypadku
   * błędu logowania
   *
   * @param dbUsername login uzytkownika
   * @param dbPassword hasło
   */
  public void authOracle(String dbUsername, String dbPassword) {
    try (Connection conn = JdbcUtils.getConnection(false)) {
      String url = conn.getMetaData().getURL();

      Class.forName("oracle.jdbc.driver.OracleDriver");
      Connection connection = null;
      connection = DriverManager.getConnection(url, dbUsername, dbPassword);
      connection.close();
      log.debug(url);
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
      if (e.getMessage() != null && e.getMessage().contains("ORA-01017")) {
        throw new IncorrectCredentialsException(e);
      } else {
        throw new AuthenticationException(e);
      }
    } catch (ClassNotFoundException e) {
      log.error(e.getMessage(), e);
      throw new AuthenticationException(e);
    }
    log.debug("Udana próba logowania do bazy: {}", dbUsername);
  }

  /**
   * Próbuje zautentykować użytkownika zewnętrznego w bazie metodą EGR
   *
   * @param dbUsername nazwa użytkownika
   * @param dbPassword hasło
   * @return token
   */
  @Override
  public String authEgr(String dbUsername, String dbPassword) {
    String ret;
    String poKomunikat;

    try (Connection conn = JdbcUtils.getConnection(false)) {
      CallableStatement cs = conn
          .prepareCall("{? = call eap_api_autentykacja_egr.autentykuj_uzytkownika_egr(?, ?, ?)}");
      cs.registerOutParameter(1, OracleTypes.VARCHAR);
      cs.setString(2, dbUsername);
      cs.setString(3, dbPassword);
      cs.registerOutParameter(4, OracleTypes.VARCHAR);
      cs.execute();
      ret = cs.getString(1);

      if ("0".equals(ret)) {
        poKomunikat = cs.getString(4);
        if (poKomunikat == null) {
          log.error("Nieprawidłowa nazwa użytkownika/hasło: {}", dbUsername);
          throw new IncorrectCredentialsException("Nieprawidłowa nazwa użytkownika/hasło");
        }
        // TODO - tutaj mapowanie błędów mna Exceptions - np. konto zablokowane
        log.error(poKomunikat);
        throw new AuthenticationException(poKomunikat);
      }
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
      throw new AuthenticationException(e);
    }
    log.debug("Udana autentykascja EGR w bazie");

    return ret;
  }

  /**
   * Inicjuje globale sesji na bazie danych
   *
   * @param dbUsername login użytkownika
   * @param sessionId ID sesji
   */
  @Override
  public void init(String dbUsername, String sessionId) throws SQLException, EgrAuthInitException {
    String ret;
    String poKomunikat;

    try (Connection conn = JdbcUtils.getConnection(false)) {
      CallableStatement cs = conn.prepareCall("{? = call eap_web.inicjuj( ?, ?, ?, ? )}");
      cs.registerOutParameter(1, OracleTypes.VARCHAR);
      cs.setString(2, dbUsername);
      cs.setString(3, sessionId);
      cs.registerOutParameter(4, OracleTypes.VARCHAR);
      cs.setString(5, Const.APP_SYMBOL); //PP

      cs.execute();
      ret = cs.getString(1);

      if ("N".equals(ret)) {
        poKomunikat = cs.getString(4);
        log.error(poKomunikat);
        throw new EgrAuthInitException(poKomunikat);
      }

      HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance()
          .getExternalContext().getRequest();
      JdbcUtils.sqlSPCall(conn, "EAP_GLOBALS.ustaw_nazwe_pc",
          "" + request.getRemoteHost());      //request.getRemoteAddr();

    } catch (SQLException e) {

      if (e.getMessage().contains(
          "PLS-00306")) { //PLS-00306: niepoprawna liczba lub typy argumentów w wywołaniu "INICJUJ"

        try (Connection conn = JdbcUtils.getConnection(false)) {
          CallableStatement cs = conn.prepareCall("{? = call eap_web.inicjuj( ?, ?, ? )}");
          cs.registerOutParameter(1, OracleTypes.VARCHAR);
          cs.setString(2, dbUsername);
          cs.setString(3, sessionId);
          cs.registerOutParameter(4, OracleTypes.VARCHAR);
          cs.execute();
          ret = cs.getString(1);
          if ("N".equals(ret)) {
            poKomunikat = cs.getString(4);
            log.error(poKomunikat);
            throw new EgrAuthInitException(poKomunikat);
          }
        }

      } else {
        log.error(e.getMessage(), e);
        throw e;
      }

    }
    log.debug("Zainicjowano globale");
  }

  /**
   * Ustawia kontekst użytkownika w bazie danych
   *
   * @param sessionId ID sesji
   * @param conn Connection do bazy
   */
  @Override
  public void setContext(String sessionId, Connection conn) {
    try {
      CallableStatement cs = conn.prepareCall("{? = call eap_web.ustaw_kontekst(?, ?)}");

      cs.registerOutParameter(1, OracleTypes.VARCHAR);
      cs.setString(2, sessionId);
      cs.registerOutParameter(3, OracleTypes.VARCHAR);
      cs.execute();

      if (cs.getString(1).equals("N")) {
        throw new EgrContextException(cs.getString(3));
      }
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
      throw new EgrContextException(LanguageBean.translate("INF_database_error"));
    }
    log.debug("Ustawiono kontekst");
  }

  /**
   * Ustawia kontekst użytkownika w bazie danych
   *
   * @param sessionId ID sesji
   */
  @Override
  public void setContext(String sessionId) {
    try (Connection conn = JdbcUtils.getConnection(false)) {
      setContext(sessionId, conn);
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
    }
  }

  /**
   * Czyści kontekst użytkownika w bazie danych
   *
   * @param sessionId ID sesji
   */
  @Override
  public void clearContext(String sessionId) {
    try (Connection conn = JdbcUtils.getConnection()) {
      CallableStatement cs = conn.prepareCall("{call eap_web.czysc_kontekst(?)}");

      cs.setString(1, sessionId);
      cs.execute();
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
    }
    log.debug("Wyczyszczono kontekst");
  }

  @Override
  public void changePassword(String username, String password)
      throws SQLException, PasswordValidationException {
    try (Connection conn = JdbcUtils.getConnection(false)) {
      CallableStatement cs = conn.prepareCall(
          "{? = call eap_web.zmien_haslo_z_walidacja(p_uzt_nazwa => ?, p_haslo => ?)}");
      cs.registerOutParameter(1, OracleTypes.VARCHAR);
      cs.setString(2, username);
      cs.setString(3, password);
      cs.execute();

      String passwordValidationError = cs.getString(1);
      if ("WYBRANO_HASLO_ZASTRZEZONE".equals(passwordValidationError)) {
        throw new SpecialPasswordException();
      } else if ("HASLO_NIE_MOZE_SIE_POWTARZAC".equals(passwordValidationError)
          || "HASLO_NIE_MOZE_SIE_POWTARZAC_DNI".equals(passwordValidationError)
          || "HASLO_NIE_MOZE_SIE_POWTARZAC_LICZNIK".equals(passwordValidationError)) {
        throw new RepeatedPasswordException();
      } else if ("WERYFIKACJA_HASLA".equals(passwordValidationError) || "SLABE_HASLO"
          .equals(passwordValidationError)) {
        throw new PasswordValidationException();
      }
    } catch (SQLException e) {
      log.error(e.getMessage(), e);
      throw e;
    }
    log.info("Zmieniono haslo");
  }


  @Override
  public Optional<PasswordResetToken> readPasswordResetToken(String passwordResetTokenValue) {
    Optional<PasswordResetToken> passwordResetTokenOptional;
    try (Connection conn = JdbcUtils.getConnection(false)) {

      String sql = ""
          + " SELECT HT_TOKEN, HT_UZT_NAZWA, HT_CZAS_WYGASNIECIA "
          + " FROM PPT_HASLO_RESET_TOKEN "
          + " where HT_TOKEN = ? "
          + " order by HT_AUDYT_UT desc ";

      CachedRowSet crs = JdbcUtils.sqlExecQuery(conn, sql, passwordResetTokenValue);
      if (crs.next()) {
        PasswordResetToken passwordResetToken = new PasswordResetToken();
        passwordResetToken.setToken(crs.getString(1));
        passwordResetToken.setUsername(crs.getString(2));
        passwordResetToken.setExpiryDate(crs.getDate(3));
        passwordResetTokenOptional = Optional.of(passwordResetToken);
      } else {
        passwordResetTokenOptional = Optional.empty();
      }
    } catch (Exception e) {
      passwordResetTokenOptional = Optional.empty();
    }
    return passwordResetTokenOptional;
  }


  @Override
  public List<String> odczytajFunkcjeUzytkowe(String dbUsername) {
    return User.odczytajFunkcjeUzytkowe(dbUsername);
  }

  @Override
  public void saveNewPasswordToken(PasswordResetToken passwordResetToken) {

    String sqlInsert = "INSERT INTO PPT_HASLO_RESET_TOKEN (HT_TOKEN, HT_UZT_NAZWA, HT_CZAS_WYGASNIECIA) VALUES (?,?,?)";

    try (Connection conn = JdbcUtils.getConnection(false)) {
      deleteOldPasswordTokens(conn, passwordResetToken.getUsername());
      JdbcUtils.sqlExecUpdate(conn, sqlInsert, passwordResetToken.getToken(),
          passwordResetToken.getUsername(),
          new java.sql.Date(passwordResetToken.getExpiryDate().getTime()));
    } catch (Exception e) {
      throw new PasswordResetException("Problem ze zresetowaniem hasła", e);
    }
  }

  @Override
  public void deleteOldPasswordTokens(String username) {
    try (Connection conn = JdbcUtils.getConnection(false)) {
      deleteOldPasswordTokens(conn, username);
    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }

  @Override
  public String odczytajNazweUzytkownikaZNazwyZewnetrznej(String username,
      String metodaAutentykacji) {
    String dbUsername;
    try (Connection conn = JdbcUtils.getConnection(false)) {
      String sql = ""
          + " SELECT uzt_nazwa "
          + " FROM eat_uzytkownicy "
          + " where uzt_nazwa_zewn = ? "
          + " and uzt_metoda_autentykacji = ?";

      CachedRowSet crs = JdbcUtils.sqlExecQuery(conn, sql, username, metodaAutentykacji);
      crs.next();
      dbUsername = crs.getString(1);
    } catch (Exception e) {
      throw new PasswordResetException("Problem z odczytaniem użytkownika z nazwy zewnętrznej", e);
    }
    return dbUsername;
  }

  @Override
  public String checkPpfBlokadaLogowaniaUzt(String uztNazwaZewnetrzna) {
    String ret;
    try (Connection conn = JdbcUtils.getConnection(false)) {
      CallableStatement cs = conn
          .prepareCall("{? = call PPADM.PPF_BLOKADA_LOGOWANIA_UZT(?)}");
      cs.registerOutParameter(1, OracleTypes.VARCHAR);
      cs.setString(2, uztNazwaZewnetrzna);
      cs.execute();
      ret = cs.getString(1);
    } catch (Exception e) {
      log.error(e.getMessage(), e);
      ret = "Problem przy sprawdzaniu aktualnych umów pracownika";
    }
    return ret;
  }

  @Override
  public String odczytajNazweZewnetrzna(String username) {
    String nazwaZewnetrzna;
    try (Connection conn = JdbcUtils.getConnection(false)) {
      String sql = ""
          + " SELECT uzt_nazwa_zewn "
          + " FROM eat_uzytkownicy "
          + " where lower(uzt_nazwa) = lower(?) ";

      CachedRowSet crs = JdbcUtils.sqlExecQuery(conn, sql, username);
      crs.next();
      nazwaZewnetrzna = crs.getString(1);
    } catch (Exception e) {
      throw new PasswordResetException("Problem z odczytaniem nazwy zewnetrznej użytkownika", e);
    }
    return nazwaZewnetrzna;
  }

  private void deleteOldPasswordTokens(Connection conn, String username)
      throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
    String sqlDelete = "delete from ppt_haslo_reset_token where ht_uzt_nazwa = ?";
    JdbcUtils.sqlExecUpdate(conn, sqlDelete, username);
  }

//	@Override
//	public List<String> odczytajFunkcjeUzytkowe(String dbUsername) {
//		List<String> listaFunkcji = new ArrayList<>();
//		// TODO - odczytywanie z bazy
//		//Connection conn = DataSourceUtils.getConnection(dataSource);
//		String sql = "select fu_nazwa from eav_upr_fu_apl where upr_uzt_nazwa = ? and apl_symbol = ?";
//		try (Connection conn = jdbc.getConnection(); PreparedStatement stmt = conn.prepareStatement(sql)) {
//			stmt.setString(1, dbUsername);
//			stmt.setString(2, "PP");
//			try(ResultSet rs = stmt.executeQuery()) {
//				while(rs.next()) {
//					listaFunkcji.add(rs.getString(1));
//				}
//			} catch ( SQLException e) {
//				throw e;
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		} 
////		finally {
////			DataSourceUtils.releaseConnection(conn, dataSource);
////		}
//		System.out.println("Funkcje uzytkowe:");
//		for(String funkcja : listaFunkcji)
//			System.out.println("Funkcja: " + funkcja);
//		
//		listaFunkcji.add("MENU_UPR");
//		listaFunkcji.add("MENU_WIZ");
//		listaFunkcji.add("MENU_ADM");
//		listaFunkcji.add("MENU_POMOC");
//		listaFunkcji.add("MENU_DZIENNIK");
//		listaFunkcji.add("MENU_ABOUT");
//		return listaFunkcji;
//	}


}
