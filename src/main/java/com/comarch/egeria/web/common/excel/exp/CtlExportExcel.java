package com.comarch.egeria.web.common.excel.exp;

import java.io.Serializable;
import java.text.SimpleDateFormat;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.web.common.excel.imp.CtlImportExcel;

@Named
@Scope("view")
public class CtlExportExcel implements Serializable {

	protected CellStyle standardCellStyle;
	protected CellStyle dateCellStyle;
	protected CellStyle currencyCellStyle;

	protected CellStyle boldStandardCellStyle;
	protected CellStyle boldCurrencyCellStyle;
	protected CellStyle boldDateCellStyle;

	protected SimpleDateFormat dateFormat;

	private static final long serialVersionUID = 1L;

	private static final Logger log = LogManager.getLogger(CtlImportExcel.class);

	private static final String EXPORT_PODZIELNIKA = "EPDL";

	private String exportType;

	// Do podzielnika
	// private List<DoImportExport> dataToExport;
	// private Long ceId, okId, rpdId;

	@PostConstruct
	public void init() {
		exportType = EXPORT_PODZIELNIKA;
	}

	public void prepareExportPodzielnika() {
		log.debug("prepareExportPodzielnika()");
		exportType = EXPORT_PODZIELNIKA;
		handleFileDownload();
	}

	public void handleFileDownload() {
		if (exportType.equals(EXPORT_PODZIELNIKA)) {
			// exportPodzielnik();

		} else {
			FacesMessage message = new FacesMessage("Succesful", "Błąd exportu pliku");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	// Export dla Podzielnika
	// private void exportPodzielnik() throws IOException {
	// try {
	// ceId = podzielnikBean.getCentrumFiltr();
	// okId = podzielnikBean.getOkresFiltr();
	// rpdId = podzielnikBean.getRodzajFiltr();
	// dataToExport = podzielnikBo.doImportExport(rpdId, ceId, okId, 0L);
	// } catch (SQLException e) {
	// log.error("Błąd pobrania danych do eksportu.");
	// e.printStackTrace();
	// }
	// if (dataToExport != null) {
	//
	// String format = "xlsx";
	// Workbook workbook;
	// if (format.equals("xlsx")) {
	// workbook = new XSSFWorkbook();
	// POIXMLProperties xmlProps = ((POIXMLDocument) workbook).getProperties();
	// POIXMLProperties.CoreProperties coreProps = xmlProps.getCoreProperties();
	// coreProps.setCreator(ctlUserSessionBean.getUsername());
	// coreProps.setTitle(LanguageBean.translate("LBL10158_podzielnik"));
	// coreProps.setKeywords("CTL");
	// }
	// else {
	// workbook = new HSSFWorkbook();
	// SummaryInformation summaryInfo = ((POIDocument)
	// workbook).getSummaryInformation();
	// summaryInfo.setAuthor(ctlUserSessionBean.getUsername());
	// summaryInfo.setTitle(LanguageBean.translate("LBL10158_podzielnik"));
	// summaryInfo.setKeywords("CTL");
	// }
	// setCellDefStyle(workbook);
	// Sheet sheet = workbook.createSheet(ceId + "_" +
	// podzielnikBean.getOkres().replace("/", "_"));
	// Row row = sheet.createRow(0);
	// Cell cell = row.createCell(0);
	// cell.setCellStyle(boldStandardCellStyle);
	// cell.setCellValue(LanguageBean.translate("LBL10050_spolka"));
	// cell = row.createCell(1);
	// cell.setCellStyle(boldStandardCellStyle);
	// cell.setCellValue(LanguageBean.translate("LBL10017_imie"));
	// cell = row.createCell(2);
	// cell.setCellStyle(boldStandardCellStyle);
	// cell.setCellValue(LanguageBean.translate("LBL10034_nazwisko"));
	// cell = row.createCell(3);
	// cell.setCellStyle(boldStandardCellStyle);
	// cell.setCellValue(LanguageBean.translate("LBL10021_kod_RD"));
	// cell = row.createCell(4);
	// cell.setCellStyle(boldStandardCellStyle);
	// cell.setCellValue(LanguageBean.translate("LBL10187_kod_RD_2"));
	// cell = row.createCell(5);
	// cell.setCellStyle(boldStandardCellStyle);
	// cell.setCellValue(LanguageBean.translate("LBL10188_ilosc_dni"));
	// cell = row.createCell(6);
	// cell.setCellStyle(boldStandardCellStyle);
	// cell.setCellValue(LanguageBean.translate("LBL10189_dzien_wolny"));
	// cell = row.createCell(7);
	// cell.setCellStyle(boldStandardCellStyle);
	// cell.setCellValue(LanguageBean.translate("LBL10190_caly_czas"));
	// int i = 1;
	// for (DoImportExport temp : dataToExport) {
	// row = sheet.createRow(i);
	// cell = row.createCell(0);
	// cell.setCellStyle(standardCellStyle);
	// cell.setCellValue(temp.getSpolka());
	// cell = row.createCell(1);
	// cell.setCellStyle(standardCellStyle);
	// cell.setCellValue(temp.getImie());
	// cell = row.createCell(2);
	// cell.setCellStyle(standardCellStyle);
	// cell.setCellValue(temp.getNazwisko());
	// cell = row.createCell(3);
	// cell.setCellStyle(standardCellStyle);
	// cell.setCellValue(temp.getMpkKod());
	// cell = row.createCell(4);
	// cell.setCellStyle(standardCellStyle);
	// cell.setCellValue(temp.getMpkKod3());
	// cell = row.createCell(5);
	// cell.setCellStyle(standardCellStyle);
	// cell.setCellValue(temp.getIloscDni());
	// cell = row.createCell(6);
	// cell.setCellStyle(standardCellStyle);
	// cell.setCellValue(temp.getDzienWolny());
	// cell = row.createCell(7);
	// cell.setCellStyle(standardCellStyle);
	// cell.setCellValue(temp.getCalyCzas());
	// i++;
	// }
	//
	// FacesContext facesContext = FacesContext.getCurrentInstance();
	// ExternalContext externalContext = facesContext.getExternalContext();
	// externalContext.setResponseContentType("application/vnd.ms-excel");
	// externalContext.setResponseHeader("Expires", "0");
	// externalContext.setResponseHeader("Cache-Control", "must-revalidate,
	// post-check=0, pre-check=0");
	// externalContext.setResponseHeader("Pragma", "public");
	// externalContext.setResponseHeader("Content-Disposition",
	// "attachment; filename=\"" + LanguageBean.translate("LBL10158_podzielnik")
	// + ceId + "_"
	// + podzielnikBean.getOkres().replace("/", "_") + "." + format + "\"");
	// externalContext.addResponseCookie(Constants.DOWNLOAD_COOKIE, "true",
	// Collections.<String, Object> emptyMap());
	//
	// workbook.write(externalContext.getResponseOutputStream());
	// externalContext.responseFlushBuffer();
	// facesContext.responseComplete();
	// workbook.close();
	//
	// }
	// }

	public void setCellDefStyle(Workbook wbn) {

		standardCellStyle = wbn.createCellStyle();
		standardCellStyle.setBorderTop(BorderStyle.THIN);
		standardCellStyle.setBorderRight(BorderStyle.THIN);
		standardCellStyle.setBorderBottom(BorderStyle.THIN);
		standardCellStyle.setBorderLeft(BorderStyle.THIN);

		Font boldFont = wbn.createFont();
		boldFont.setBold(true);

		boldStandardCellStyle = wbn.createCellStyle();
		boldStandardCellStyle.setBorderTop(BorderStyle.THIN);
		boldStandardCellStyle.setBorderRight(BorderStyle.THIN);
		boldStandardCellStyle.setBorderBottom(BorderStyle.THIN);
		boldStandardCellStyle.setBorderLeft(BorderStyle.THIN);
		boldStandardCellStyle.setFont(boldFont);

		currencyCellStyle = wbn.createCellStyle();
		currencyCellStyle.setDataFormat(wbn.getCreationHelper().createDataFormat().getFormat("#,##0.00"));
		currencyCellStyle.setBorderTop(BorderStyle.THIN);
		currencyCellStyle.setBorderRight(BorderStyle.THIN);
		currencyCellStyle.setBorderBottom(BorderStyle.THIN);
		currencyCellStyle.setBorderLeft(BorderStyle.THIN);

		boldCurrencyCellStyle = wbn.createCellStyle();
		boldCurrencyCellStyle.setDataFormat(wbn.getCreationHelper().createDataFormat().getFormat("#,##0.00"));
		boldCurrencyCellStyle.setBorderTop(BorderStyle.THIN);
		boldCurrencyCellStyle.setBorderRight(BorderStyle.THIN);
		boldCurrencyCellStyle.setBorderBottom(BorderStyle.THIN);
		boldCurrencyCellStyle.setBorderLeft(BorderStyle.THIN);
		boldCurrencyCellStyle.setFont(boldFont);

		dateCellStyle = wbn.createCellStyle();
		dateCellStyle.setDataFormat(wbn.getCreationHelper().createDataFormat().getFormat("yyyy-mm-dd"));
		dateCellStyle.setAlignment(HorizontalAlignment.CENTER);
		dateCellStyle.setBorderTop(BorderStyle.THIN);
		dateCellStyle.setBorderRight(BorderStyle.THIN);
		dateCellStyle.setBorderBottom(BorderStyle.THIN);
		dateCellStyle.setBorderLeft(BorderStyle.THIN);

		boldDateCellStyle = wbn.createCellStyle();
		boldDateCellStyle.setDataFormat(wbn.getCreationHelper().createDataFormat().getFormat("yyyy-mm-dd"));
		boldDateCellStyle.setAlignment(HorizontalAlignment.CENTER);
		boldDateCellStyle.setBorderTop(BorderStyle.THIN);
		boldDateCellStyle.setBorderRight(BorderStyle.THIN);
		boldDateCellStyle.setBorderBottom(BorderStyle.THIN);
		boldDateCellStyle.setBorderLeft(BorderStyle.THIN);
		boldDateCellStyle.setFont(boldFont);

		dateFormat = new SimpleDateFormat("yyyy-mm-dd");
	}

}
