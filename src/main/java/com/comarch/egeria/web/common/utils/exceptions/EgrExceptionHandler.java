package com.comarch.egeria.web.common.utils.exceptions;

import com.comarch.egeria.Utils;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.utils.auth.exceptions.EgrContextException;
import com.comarch.egeria.web.common.utils.constants.Const;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.shiro.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.FacesException;
import javax.faces.application.FacesMessage;
import javax.faces.application.ViewExpiredException;
import javax.faces.component.UIComponent;
import javax.faces.context.*;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.PhaseId;
import java.io.IOException;
import java.util.Iterator;

public class EgrExceptionHandler extends ExceptionHandlerWrapper {
    private ExceptionHandler wrapped;

    private static final Logger log = LoggerFactory.getLogger(EgrExceptionHandler.class);

    public EgrExceptionHandler(ExceptionHandler wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public ExceptionHandler getWrapped() {
        return wrapped;
    }

    @Override
    public void handle() throws FacesException {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            if (context == null || context.getResponseComplete()) return;

            Iterable<ExceptionQueuedEvent> exceptionQueuedEvents = this.getUnhandledExceptionQueuedEvents();
            if (exceptionQueuedEvents == null || exceptionQueuedEvents.iterator() == null) return;

            Iterator<ExceptionQueuedEvent> iterator = this.getUnhandledExceptionQueuedEvents().iterator();
            if (!iterator.hasNext()) return;

            Throwable throwable = iterator.next().getContext().getException();
            Throwable rootCause = ExceptionUtils.getRootCause(throwable);
            iterator.remove();

            if (rootCause instanceof ViewExpiredException) {

                System.out.println("!!!EgrExceptionHandler.handle ---- ViewExpiredException ----> Poprzedni stan widoku został unieważniony / " + ((ViewExpiredException)rootCause).getViewId());
                User.wsWarn("Poprzedni stan widoku został unieważniony.");
//                handleRedirect(rootCause, "/UnexpError");
                handleRedirect(rootCause, "/home.xhtml" ); // "/" + Const.LOGOUT_PAGE_URL;

            } else if (rootCause instanceof EgrContextException) {

                log.debug(rootCause.getMessage(), rootCause);
                SecurityUtils.getSubject().logout();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, rootCause.getMessage(), null));
                handleRedirect(rootCause, "/" + Const.LOGOUT_PAGE_URL);

            } else {

				log.error(throwable.getMessage(), throwable);
                handleRedirect(rootCause, "/UnexpError");

            }

            while (iterator.hasNext()) {
                iterator.next();
                iterator.remove();
            }

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        } finally {
            getWrapped().handle();
        }
    }

    void handleRedirect(Throwable rootCause, String errorPage) throws IOException {

        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext externalContext = context.getExternalContext();
        String url = externalContext.getRequestContextPath() + errorPage;

        String errorUserViewInfo = Utils.getUserSessionViewInfoHeader();
        UIComponent cc = UIComponent.getCurrentComponent(context);
        if (cc!=null) {
            errorUserViewInfo +=  " c.: " + cc + " clientId: " + cc.getClientId();
        }
        errorUserViewInfo += " ph.: " + context.getCurrentPhaseId();

        context.getExternalContext().getSessionMap().put("errorUserViewInfo", errorUserViewInfo);
        context.getExternalContext().getSessionMap().put("errorDetails",  rootCause.getClass().getCanonicalName() + ": " + rootCause.getMessage());
		context.getExternalContext().getSessionMap().put("errorStackTrace", Utils.getStackTraceAsString(rootCause));

//        context.getAttributes().put(ExceptionInfo.ATTRIBUTE_NAME, rootCause);
//        externalContext.getSessionMap().put(ExceptionInfo.ATTRIBUTE_NAME, rootCause);

        PartialResponseWriter writer = context.getPartialViewContext().getPartialResponseWriter();

        boolean responseResetted = false;
        if (context.getPartialViewContext().isAjaxRequest() && context.getCurrentPhaseId().equals(PhaseId.RENDER_RESPONSE) && !externalContext.isResponseCommitted()) {
            if (writer != null) {
                writer.flush();
                writer.endCDATA();
                writer.endInsert();
                writer.endUpdate();
                writer.startError("");
                writer.endError();
                writer.getWrapped().endElement("changes");
                writer.getWrapped().endElement("partial-response");
            }

            String characterEncoding = externalContext.getResponseCharacterEncoding();
            externalContext.responseReset();
            externalContext.setResponseCharacterEncoding(characterEncoding);
            responseResetted = true;
        }

        if (responseResetted && context.getPartialViewContext().isAjaxRequest()) {
//			writer = context.getPartialViewContext().getPartialResponseWriter();
            externalContext.addResponseHeader("Content-Type", "text/xml; charset=" + externalContext.getResponseCharacterEncoding());
            externalContext.addResponseHeader("Cache-Control", "no-cache");
            externalContext.setResponseContentType("text/xml");
            writer.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
            writer.startElement("partial-response", null);
            writer.startElement("redirect", null);
            writer.writeAttribute("url", url, null);
            writer.endElement("redirect");
            writer.endElement("partial-response");
        } else if (externalContext.isResponseCommitted() && !context.getPartialViewContext().isAjaxRequest()) {
            writer = context.getPartialViewContext().getPartialResponseWriter();
            writer.startElement("script", null);
            writer.write("window.location.href = '" + url + "';");
            writer.endElement("script");
            writer.getWrapped().endDocument();
        } else {
            externalContext.redirect(url);
        }

        context.responseComplete();
    }


}
