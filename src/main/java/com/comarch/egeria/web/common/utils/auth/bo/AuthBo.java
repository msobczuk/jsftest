package com.comarch.egeria.web.common.utils.auth.bo;

import com.comarch.egeria.web.common.utils.auth.exceptions.PasswordValidationException;
import com.comarch.egeria.web.common.utils.auth.model.PasswordResetToken;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import com.comarch.egeria.web.common.utils.auth.exceptions.EgrAuthInitException;
import com.comarch.egeria.web.common.utils.auth.exceptions.EgrAuthenticationException;
import com.comarch.egeria.web.common.utils.auth.utils.EgrPreAuthData;
import java.util.Optional;
import org.apache.shiro.authc.AuthenticationException;

/**
 * Interefejs dla klasy AuthBoImpl
 * @author Comarch SA
 */
public interface AuthBo {

	/**
	 * Sprawdza użytkownika w eat_uzytkownicy. 
	 * @param username login użytkownika (nazwa zewnętrzna użytkownika w eat_uzytkownicy)
	 * @return EgrPreAuthData
	 * @throws SQLException, EgrAuthenticationException
	 */
	EgrPreAuthData odczytajUzytkownika(String username) throws EgrAuthenticationException;

	/**
	 * Próbuje połączyć się z bazą za pomocą podanego użytkownika/hasła. Wyrzuca błąd w przypadku błędu logowania 
	 * @param dbUsername login uzytkownika bazy
	 * @param dbPassword hasło
	 * @throws AuthenticationException AuthenticationException
	 */
	void authOracle(String dbUsername, String dbPassword) throws AuthenticationException;

	/** 
	 * Próbuje zautentykować użytkownika zewnętrznego w bazie metodą EGR
	 * @param dbUsername nazwa użytkownika 
	 * @param dbPassword hasło
	 * @return token 
	 * @throws AuthenticationException AuthenticationException
	 */
	String authEgr(String dbUsername, String dbPassword) throws AuthenticationException;
	
	/**
	 * Inicjuje globale sesji na bazie danych
	 * @param dbUsername login uzytkownika
	 * @param sessionId ID sesji
	 * @throws SQLException SQLException
	 * @throws EgrAuthInitException EgrAuthInitException
	 */
	void init(String dbUsername, String sessionId) throws SQLException, EgrAuthInitException;
	
	/**
	 * Ustawia kontekst użytkownika w bazie danych
	 * @param sessionId ID sesji
	 * @param conn Connection do bazy
	 */
	void setContext(String sessionId, Connection conn);
	
	/**
	 * Ustawia kontekst użytkownika w bazie danych
	 * @param sessionId ID sesji
	 */
	void setContext(String sessionId);
	
	/**
	 * Czyści kontekst użytkownika w bazie danych
	 * @param sessionId ID sesji
	 */
	void clearContext(String sessionId);

	void changePassword(String username, String password) throws SQLException, PasswordValidationException;

	Optional<PasswordResetToken> readPasswordResetToken(String passwordResetToken);

	List<String> odczytajFunkcjeUzytkowe(String dbUsername);

	void saveNewPasswordToken(PasswordResetToken passwordResetToken);

	void deleteOldPasswordTokens(String username);

  String odczytajNazweUzytkownikaZNazwyZewnetrznej(String username, String metodaAutentykacji);

  String checkPpfBlokadaLogowaniaUzt(String nazwaZewnetrzna);

	String odczytajNazweZewnetrzna(String username);
}