package com.comarch.egeria.web.common.help.exceptions;

import java.io.IOException;

public class HelpIOException extends IOException {

	private static final long serialVersionUID = -2293939615023732398L;

	public HelpIOException(String message, Throwable cause) {
		super(message, cause);
	}

	public HelpIOException(String message) {
		super(message);
	}

	public HelpIOException(Throwable cause) {
		super(cause);
	}
}
