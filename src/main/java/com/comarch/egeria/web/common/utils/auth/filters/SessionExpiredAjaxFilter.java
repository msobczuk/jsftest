package com.comarch.egeria.web.common.utils.auth.filters;

import com.comarch.egeria.web.User;

import javax.faces.application.ResourceHandler;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SessionExpiredAjaxFilter implements Filter {

  private static final String AJAX_REDIRECT_XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
      + "<partial-response><redirect url=\"%s\"></redirect></partial-response>";

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
      throws ServletException, IOException {
    HttpServletRequest request = (HttpServletRequest) req;
    HttpServletResponse response = (HttpServletResponse) res;

    boolean ajaxRequest = "partial/ajax".equals(request.getHeader("Faces-Request"));

    if (ajaxRequest) {
      doFilterForAjaxRequest(request, response, chain);
    } else {
      chain.doFilter(req, res);
    }
  }

  private void doFilterForAjaxRequest(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    HttpSession session = request.getSession(false);
    String loginURL = request.getContextPath() + "/login.xhtml";
    String changePasswordURL = request.getContextPath() + "/changePassword.xhtml";

    User user = (session != null) ? (User) session.getAttribute("user") : null;
    boolean loggedIn = user != null && user.isLoggedIn;
    boolean loginRequest = request.getRequestURI().equals(loginURL);
    boolean resourceRequest = request.getRequestURI()
        .startsWith(request.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER + "/");

    if (!request.getRequestURI().equals(changePasswordURL) //PT260023
            && !loggedIn && !loginRequest && !resourceRequest) {
      response.setContentType("text/xml");
      response.setCharacterEncoding("UTF-8");
      response.getWriter().printf(AJAX_REDIRECT_XML, request.getContextPath() + "/logout.xhtml");
    } else {
      chain.doFilter(request, response);
    }
  }


  @Override
  public void destroy() {
  }

  @Override
  public void init(FilterConfig arg0) throws ServletException {
  }

}