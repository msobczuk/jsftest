package com.comarch.egeria.web.common.utils.auth.utils;

public class PasswordResetException extends RuntimeException {

  public PasswordResetException(String message, Throwable cause) {
    super(message, cause);
  }
}
