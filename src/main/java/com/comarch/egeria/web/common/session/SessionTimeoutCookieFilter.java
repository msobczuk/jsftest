package com.comarch.egeria.web.common.session;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.xml.sax.SAXException;

public class SessionTimeoutCookieFilter implements Filter {

	int defaultSessionTimeoutInSeconds;
	
	
	@Override
	public void init(FilterConfig config) throws ServletException {
		int defaultSessionTimeoutInMinutes = 30;
		try {
			defaultSessionTimeoutInMinutes = Integer.parseInt(XPathFactory.newInstance().newXPath().compile("web-app/session-config/session-timeout").evaluate(DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream("/WEB-INF/web.xml"))));
		} catch (NumberFormatException | XPathExpressionException | SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
		defaultSessionTimeoutInSeconds = defaultSessionTimeoutInMinutes * 60;
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException, ServletException {
		HttpServletResponse httpResp = (HttpServletResponse) resp;
		HttpServletRequest httpReq = (HttpServletRequest) req;		

		String servletPath = httpReq.getServletPath(); //((HttpServletRequest) request).getRequestURI();
		if (servletPath!=null && servletPath.startsWith("/rest")) {
			//dla webservice'ów rest/msg nie robimy żadnych ciasteczek
			filterChain.doFilter(req, resp);

		} else { 
		
			long currTime = System.currentTimeMillis();
			
			String expiryTime = Long.toString(currTime + defaultSessionTimeoutInSeconds * 1000);
			Cookie cookie = new Cookie("serverTime", Long.toString(currTime));
			cookie.setPath("/");
			httpResp.addCookie(cookie);
			
			cookie = new Cookie("sessionExpiry", expiryTime);
			cookie.setPath("/");
			httpResp.addCookie(cookie);
			
			filterChain.doFilter(req, resp);
			
//			if (httpReq.getAttribute("isRequestByPoll") == null)
//				resetMaxInactiveInterval(httpReq.getSession());//czy na pewno tu chcemy tworzyc nowe sesję http?
			
			HttpSession session = httpReq.getSession(false);
			if (session!=null && httpReq.getAttribute("isRequestByPoll") == null)
				resetMaxInactiveInterval(session);
		
		}
	}

	private void resetMaxInactiveInterval(HttpSession session) {
		session.setMaxInactiveInterval(defaultSessionTimeoutInSeconds);
		
	}

	@Override
	public void destroy() {
		
	}
}
