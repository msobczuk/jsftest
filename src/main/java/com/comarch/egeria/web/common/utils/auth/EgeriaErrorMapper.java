package com.comarch.egeria.web.common.utils.auth;

public class EgeriaErrorMapper {

  private static final String USER_IS_LOCKED_REGEX = "^Użytkownik .* jest zablokowany$";

  public static String mapToErrorCode(String egrError) {
    if (egrError.matches(USER_IS_LOCKED_REGEX)) {
      return "INF_locked_account";
    } else {
      return "INF_login_error";
    }
  }
}
