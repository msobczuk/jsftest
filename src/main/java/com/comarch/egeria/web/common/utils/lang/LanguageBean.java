package com.comarch.egeria.web.common.utils.lang;

import java.io.Serializable;
import java.text.NumberFormat;
import java.time.Month;
import java.util.Locale;
import java.util.MissingResourceException;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

@Named
@Scope("session")
public class LanguageBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final Logger log = LoggerFactory.getLogger(LanguageBean.class);

	private Locale locale;
	
	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	
	public boolean isChoosen(String language) {
		return locale.equals(new Locale(language));
	}

	public void changeLanguage(String language) {
		locale = new Locale(language);
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);

		log.debug("Language changed to: " + locale.toString());
	}

	public String getMonthName(Month month) {
		String label = "EGR701";
		if (month.getValue() < 10)
			label += "0";
		label += month.getValue();
		return translate(label);
	}

	public static String translate(String key, String... args) {
		String translated = "";
		String[] params = args;
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			translated = context.getApplication().getResourceBundle(context, "dic").getString(key);
			
			if (params.length > 0) {
				for (int i = 0; i < params.length; i++) {
					translated = translated.replaceAll("@" + (i + 1), params[i]);
				}
			}
		} catch (MissingResourceException e) {
			log.error("Cannot retrieve {} from dictionary.", key);
		}
		return translated;
	}

	public static String formatNumber(double number) {
		return number < 0 ? "-" + formatNumberToString(-number) : formatNumberToString(number);
	}

	private static String formatNumberToString(double number) {
		Locale viewLocale = FacesContext.getCurrentInstance().getViewRoot().getLocale();
		return NumberFormat.getCurrencyInstance(viewLocale).format(number).substring(1).trim();
	}

	@PostConstruct
	private void init() {
		locale = new Locale(FacesContext.getCurrentInstance().getApplication().getDefaultLocale().getLanguage());

		log.debug("Language: " + locale.toString());
	}
}