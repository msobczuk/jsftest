package com.comarch.egeria.web.common.utils.auth.exceptions;

public class EgrContextException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	public EgrContextException() {
		super("Context Exception");
	}
	
	public EgrContextException(String message) {
		super(message);
	}
}
