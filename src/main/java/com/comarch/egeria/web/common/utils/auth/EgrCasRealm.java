package com.comarch.egeria.web.common.utils.auth;

import com.comarch.egeria.web.common.utils.auth.bo.AuthBo;
import io.buji.pac4j.realm.Pac4jRealm;
import io.buji.pac4j.subject.Pac4jPrincipal;
import javax.inject.Inject;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;

public class EgrCasRealm extends Pac4jRealm {

  @Inject
  AuthBo authBo;


  public EgrCasRealm() {
    super();
  }

  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(final PrincipalCollection principals) {
    SimpleAuthorizationInfo simpleAuthorizationInfo = (SimpleAuthorizationInfo) super
        .doGetAuthorizationInfo(principals);
    Pac4jPrincipal principal = principals.oneByType(Pac4jPrincipal.class);
    if (principal != null) {
      // TODO - czy na pewno getId() - tak jest w loginBeanie
      simpleAuthorizationInfo
          .addStringPermissions(authBo.odczytajFunkcjeUzytkowe(principal.getProfile().getId()));
    }
    return simpleAuthorizationInfo;
  }

}
