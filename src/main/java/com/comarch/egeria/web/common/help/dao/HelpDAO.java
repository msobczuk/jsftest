package com.comarch.egeria.web.common.help.dao;

import java.util.List;

import com.comarch.egeria.web.common.help.exceptions.HelpAccessException;
import com.comarch.egeria.web.common.help.exceptions.HelpIOException;
import com.comarch.egeria.web.common.help.exceptions.HelpNotFoundException;
import com.comarch.egeria.web.common.help.exceptions.HelpPersistenceException;

public interface HelpDAO {
	int persist(String modul, String region, String title, String docFilename, String htmlFilename,
			List<String> imgFilenames, String charset) throws HelpPersistenceException, HelpIOException;

	String getHtmlFilename(String modul, String region, String dir, String imgDir, String charset)
			throws HelpAccessException, HelpIOException;

	int getCurrentVersion(String title) throws HelpAccessException;

	int getCurrentVersion(String title, String modul, String region) throws HelpAccessException;
}
