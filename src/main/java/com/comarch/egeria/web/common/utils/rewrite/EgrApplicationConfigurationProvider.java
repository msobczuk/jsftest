package com.comarch.egeria.web.common.utils.rewrite;

import com.comarch.egeria.pp.data.AppBean;
import org.ocpsoft.rewrite.annotation.RewriteConfiguration;
import org.ocpsoft.rewrite.config.Configuration;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.config.ConfigurationRuleBuilder;
import org.ocpsoft.rewrite.config.Direction;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;
import org.ocpsoft.rewrite.servlet.config.Lifecycle;
import org.ocpsoft.rewrite.servlet.config.Response;
import org.ocpsoft.rewrite.servlet.config.rule.Join;

import javax.servlet.ServletContext;
import java.util.Map.Entry;

@RewriteConfiguration
public class EgrApplicationConfigurationProvider extends HttpConfigurationProvider {
	
	
	
	
	public EgrApplicationConfigurationProvider() throws Exception{
		AppBean.addUrlMapping("/UnexpError", "/WEB-INF/protectedElements/Errors/UnexpError.xhtml");

		//Ksiazka telefoniczna MF
		AppBean.addUrlMapping("/TelefonyMF", "/faces/KsiazkaTelefoniczna/TelefonyMF.xhtml");
		AppBean.addUrlMapping("/TelefonyMF2", "/faces/KsiazkaTelefoniczna/TelefonyMF2.xhtml");

		// DaneKadrowe
		AppBean.addUrlMapping("/DaneKadrowe", "/faces/Pracownik/Pracownik.xhtml"); // index.xhtml
		AppBean.addUrlMapping("/DaneKadroweZatrudnienie", "/faces/Pracownik/PracownikZatrudnienie.xhtml");
		AppBean.addUrlMapping("/DaneKadroweRodzina", "/faces/Pracownik/PracownikRodzina.xhtml");
		AppBean.addUrlMapping("/DaneKadroweBadania", "/faces/Pracownik/PracownikBadania.xhtml");
		AppBean.addUrlMapping("/DaneKadroweBadaniaSzkBHP", "/faces/Pracownik/PracownikBadaniaSzkBHP.xhtml");
		AppBean.addUrlMapping("/DaneKadroweWyksztalcenie", "/faces/Pracownik/PracownikWyksztalcenie.xhtml");
//		testowe wartosci na staz
//		AppBean.addUrlMapping("/Kwalifikacje", "/faces/Pracownik/PracownikKwalifikacje.xhtml");
		AppBean.addUrlMapping("/Jezyki", "/faces/Pracownik/PracownikJezyki.xhtml");

		// DanePlacowe
		AppBean.addUrlMapping("/DanePlacoweListyPlac", "/faces/DanePlacowe/ListyPlac.xhtml");
		AppBean.addUrlMapping("/DanePlacoweListyPlacKI", "/faces/DanePlacowe/ListyPlacKI.xhtml");

		AppBean.addUrlMapping("/DanePlacoweKontaBankowe", "/faces/DanePlacowe/KontaBakowe.xhtml");
		AppBean.addUrlMapping("/DanePlacoweListaPIT11", "/faces/DanePlacowe/ListaPIT11.xhtml");
		AppBean.addUrlMapping("/DanePlacoweListaRACHUCP", "/faces/DanePlacowe/ListaRACHUCP.xhtml");

		// Urlopy
		AppBean.addUrlMapping("/UrlopyNieobecnosci", "/faces/Urlopy/Nieobecnosci.xhtml");
		AppBean.addUrlMapping("/UrlopyRocznyPlanOLD", "/faces/Urlopy/RocznyPlanUrlopowy.xhtml");
		AppBean.addUrlMapping("/WnioskiUrlopowe", "/faces/Urlopy/WnioskiUrlopowe.xhtml");
		AppBean.addUrlMapping("/WniosekUrlopowy", "/faces/Urlopy/WniosekUrlopowy.xhtml");
		AppBean.addUrlMapping("/ListaWnioskowUrlopowych", "/faces/Urlopy/ListaWnioskowUrlopowych.xhtml");
		AppBean.addUrlMapping("/ListaWnioskowPlanowanych", "/faces/Urlopy/ListaWnioskowPlanowanych.xhtml");
		
		AppBean.addUrlMapping("/UrlopyRocznyPlan", "/faces/Urlopy/RocznyPlanUrlopowyNew.xhtml");
		AppBean.addUrlMapping("/UrlopyRocznyPlanStd", "/faces/Urlopy/RocznyPlanUrlopowyNewSTD.xhtml");
		
		AppBean.addUrlMapping("/WniosekUrlopowy2", "/faces/Urlopy/WniosekUrlopowy2.xhtml");
		
		
		// Opis stanowiska
		AppBean.addUrlMapping("/NowyOpis", "/faces/OpisStanowiska/NowyOpis.xhtml");
		AppBean.addUrlMapping("/NowyOpisSC", "/faces/OpisStanowiska/NowyOpisSC.xhtml");
		AppBean.addUrlMapping("/ListaOpisow", "/faces/OpisStanowiska/OpisyLista.xhtml");
		AppBean.addUrlMapping("/StanowiskaRaporty", "/faces/OpisStanowiska/StanowiskaRaporty.xhtml");
		AppBean.addUrlMapping("/StrukturaPodleglosci", "/faces/OpisStanowiska/StrukturaPodleglosci.xhtml");

		// SrodkiTrwale
		AppBean.addUrlMapping("/SrodkiTrwale", "/faces/SrodkiTrwale/SrodkiTrwale.xhtml");
		AppBean.addUrlMapping("/ListaStPrc", "/faces/SrodkiTrwale/ListaSrodkowTrwalychPrc.xhtml");
		AppBean.addUrlMapping("/ListaStWnPrc", "/faces/SrodkiTrwale/ListaWnioskowStPrc.xhtml");

		// Szkolenia
		AppBean.addUrlMapping("/RejestrSzkolen", "/faces/Szkolenia/RejestrSzkolen.xhtml");
		AppBean.addUrlMapping("/NowaOfertaSzkolen", "/faces/Szkolenia/OfertaSzkolen.xhtml");
		// TW_ZZZ
		// AppBean.addUrlMapping("/SzkoleniaTabelaSzkolen", "/faces/Szkolenia/TabelaSzkolen.xhtml");

		//AppBean.addUrlMapping("/SzkoleniaAdm", "/faces/Szkolenia/ZZZSzkoleniaAdministrator.xhtml");

		AppBean.addUrlMapping("/ListaUmowLojalnosciowych", "/faces/Szkolenia/ListaUmowLoj.xhtml");
		AppBean.addUrlMapping("/UmowaLojal", "/faces/Szkolenia/UmowaLojal.xhtml");

		
		AppBean.addUrlMapping("/ZapisySzkolenia", "/faces/Szkolenia/Zapisy.xhtml");
		AppBean.addUrlMapping("/WniosekStudia", "/faces/Szkolenia/WniosekStudia.xhtml");
		AppBean.addUrlMapping("/MojeSzkolenia", "/faces/Szkolenia/MojeSzkolenia.xhtml");
		AppBean.addUrlMapping("/AnkietySzkoleniowe", "/faces/Szkolenia/Ankiety.xhtml");
		AppBean.addUrlMapping("/AnkietaSzkoleniowa", "/faces/Szkolenia/AnkietaSzkoleniowa.xhtml");
		AppBean.addUrlMapping("/AnkietyNiewypelnione", "/faces/Szkolenia/AnkietyNiewypelnioneZZZ.xhtml");
		AppBean.addUrlMapping("/AnkietyWszystkie", "/faces/Szkolenia/AnkietyWszystkie.xhtml");
		AppBean.addUrlMapping("/MojPlanSzkoleniowy", "/faces/Szkolenia/MojPlanSzkoleniowy.xhtml");
		AppBean.addUrlMapping("/NierozliczoneUmowyLojalnosciowe", "/faces/Szkolenia/NierozliczoneUmowyLojalnosciowe.xhtml");
		AppBean.addUrlMapping("/RozliczoneUmowyLojalnosciowe", "/faces/Szkolenia/RozliczoneUmowyLojalnosciowe.xhtml");
		AppBean.addUrlMapping("/StudiaWnioskiWObiegu", "/faces/Szkolenia/StudiaWnioskiWObieguZZZ.xhtml");
		AppBean.addUrlMapping("/StudiaWszystkieWnioski", "/faces/Szkolenia/StudiaWszystkieWnioski.xhtml");
		AppBean.addUrlMapping("/SzkoleniaDoAkceptacji", "/faces/Szkolenia/SzkoleniaDoAkceptacji.xhtml");
		AppBean.addUrlMapping("/StudiaDoAkceptacji", "/faces/Szkolenia/StudiaDoAkceptacji.xhtml");
		// AppBean.addUrlMapping("/ZapisySzkolenie", "/faces/Szkolenia/SzkolenieZapisy.xhtml");
		AppBean.addUrlMapping("/ZapisySzkolenie", "/faces/Szkolenia/ZapisNaSzkolenie.xhtml");
		AppBean.addUrlMapping("/SzkolenieListaZapisow", "/faces/Szkolenia/SzkoleniaListaZapisow.xhtml");
		AppBean.addUrlMapping("/WniosekRefundacja", "/faces/Szkolenia/WniosekRefundacja.xhtml");
		AppBean.addUrlMapping("/ListaWnioskowRefundacje", "/faces/Szkolenia/ListaWnioskowRefundacje.xhtml");

		AppBean.addUrlMapping("/BudzetSzkoleniowy", "/faces/Szkolenia/BudzetSzkoleniowy.xhtml");
		
		
		AppBean.addUrlMapping("/SzkoleniaWykladowcy", "/faces/Szkolenia/SzkoleniaListaWykladowcow.xhtml");
		
		// Rozwój
		AppBean.addUrlMapping("/RozwojWniosekRekrutacje", "/faces/Rozwoj/WniosekRekrutacjeZZZ.xhtml");
		AppBean.addUrlMapping("/RozwojOdwolanieOdOceny", "/faces/Rozwoj/OdwolanieOdOcenyZZZ.xhtml");

		// Bilans czasu pracy
		// AppBean.addUrlMapping("/BilansCzasuPracyZlecenieNadgodzin", "/faces/BilansCzasuPracy/ZlecenieNadgodzin.xhtml");
		// AppBean.addUrlMapping("/BilansCzasuPracyOdbiorNadgodzin", "/faces/BilansCzasuPracy/OdbiorNadgodzin.xhtml");
		AppBean.addUrlMapping("/WnioskiRozCzasuPracy", "/faces/BilansCzasuPracy/WnioskiRozCzasuPracy.xhtml");
		AppBean.addUrlMapping("/WniosekRozCzasuPracy", "/faces/BilansCzasuPracy/WniosekRozCzasuPracy.xhtml");
		AppBean.addUrlMapping("/PlanPracy", "/faces/BilansCzasuPracy/PlanPracy.xhtml");
		
		// Czas pracy
		AppBean.addUrlMapping("/CzasPracy", "/faces/CzasPracy/CzasPracy.xhtml");
		AppBean.addUrlMapping("/BilansCzasuPracy", "/faces/CzasPracy/BilansCzasuPracy.xhtml");
		AppBean.addUrlMapping("/GrafikPracownika", "/faces/CzasPracy/GrafikPracownika.xhtml");
		AppBean.addUrlMapping("/KartaPracy", "/faces/CzasPracy/KartaPracy.xhtml");
		AppBean.addUrlMapping("/PlanowaneGrafiki", "/faces/CzasPracy/PlanowaneGrafiki.xhtml");
		AppBean.addUrlMapping("/PlanowanieAbsencji", "/faces/CzasPracy/PlanowanieAbsencji.xhtml");
		AppBean.addUrlMapping("/RejestrCzasuPracy", "/faces/CzasPracy/RejestrCzasuPracy.xhtml");
		AppBean.addUrlMapping("/WnioskiOZatwierdzenieGodzin", "/faces/CzasPracy/WnioskiOZatwierdzenieGodzin.xhtml");
		
		// Test Raportow
		AppBean.addUrlMapping("/RaportyTest", "/faces/TESTraportow/TestRaportow.xhtml");

		// Raporty
		AppBean.addUrlMapping("/Raporty", "/faces/Raporty/Raporty.xhtml");
		AppBean.addUrlMapping("/HistoriaRaportow", "/faces/Raporty/HistoriaRaportow.xhtml");
		AppBean.addUrlMapping("/RaportyJasper", "/faces/Raporty/RaportyJasperTest.xhtml");


		// Ocena okresowa
		//AppBean.addUrlMapping("/OcenaOkresowaAdm", "/faces/OcenaOkresowa/ZZZOcenaOkresowaAdministrator.xhtml");
		//AppBean.addUrlMapping("/ListaOcen", "/faces/OcenaOkresowa/ZZZListaOcen.xhtml");
		AppBean.addUrlMapping("/ListaPlanowanychOcen", "/faces/OcenaOkresowa/ListaPlanowanychOcen.xhtml");
		AppBean.addUrlMapping("/ListaZaplanowanychOcen", "/faces/OcenaOkresowa/ListaZaplanowanychOcen.xhtml");
		AppBean.addUrlMapping("/ListaPlanowanychOcenwObiegu", "/faces/OcenaOkresowa/ListaPlanowanychOcenwObiegu.xhtml");
		AppBean.addUrlMapping("/ListaZaplanowanychOcenwObiegu", "/faces/OcenaOkresowa/ListaZaplanowanychOcenwObiegu.xhtml");
		AppBean.addUrlMapping("/OcenaOkresowaRaporty", "/faces/OcenaOkresowa/OcenaOkresowaRaporty.xhtml");
		AppBean.addUrlMapping("/OcenaOkresowaZaplanuj", "/faces/OcenaOkresowa/OcenaOkresowaZaplanuj.xhtml");
		AppBean.addUrlMapping("/OcenaOkresowa", "/faces/OcenaOkresowa/OcenaOkresowa.xhtml");
		AppBean.addUrlMapping("/ListaOcenOceniany", "/faces/OcenaOkresowa/ListaOcenOceniany.xhtml");
		AppBean.addUrlMapping("/ListaOcenOceniajacy", "/faces/OcenaOkresowa/ListaOcenOceniajacy.xhtml");
		AppBean.addUrlMapping("/ListaOcenWTrakcie", "/faces/OcenaOkresowa/ListaOcenWTrakcie.xhtml");
		AppBean.addUrlMapping("/OcenaRaport", "/faces/OcenaOkresowa/OcenaRaport.xhtml");

		// Ocena okresowa RCGW
		AppBean.addUrlMapping("/OcenaRCGW", "/faces/OcenyRCGW/OcenaRCGW.xhtml");
		AppBean.addUrlMapping("/ListaOcenRCGW", "/faces/OcenyRCGW/ListaOcenRCGW.xhtml");



		// Sluzba Przygotowawcza
		AppBean.addUrlMapping("/ListaArkuszy", "/faces/SluzbaPrzygotowawcza/ListaArkuszy.xhtml");
		AppBean.addUrlMapping("/WszystkieArkusze", "/faces/SluzbaPrzygotowawcza/WszystkieArkusze.xhtml");
		AppBean.addUrlMapping("/WynikSluzbyPrzygotowawczej", "/faces/SluzbaPrzygotowawcza/ListaArkuszy2.xhtml");
		AppBean.addUrlMapping("/ListaArkuszyWyniku", "/faces/SluzbaPrzygotowawcza/ListaArkuszy3.xhtml");
		AppBean.addUrlMapping("/ListaArkuszyzWynikami", "/faces/SluzbaPrzygotowawcza/ListaArkuszy4.xhtml");
		AppBean.addUrlMapping("/ListaArkuszyWObiegu", "/faces/SluzbaPrzygotowawcza/ListaArkuszyWObiegu.xhtml");
		AppBean.addUrlMapping("/ArkuszSluzbyPrzygotowawczej", "/faces/SluzbaPrzygotowawcza/ArkuszSluzbyPrzygotowawczej.xhtml");
		AppBean.addUrlMapping("/ArkuszSluzbyPrzygotowawczej2", "/faces/SluzbaPrzygotowawcza/ArkuszSluzbyPrzygotowawczej2.xhtml");
		AppBean.addUrlMapping("/ArkuszSluzbyPrzygotowawczej3", "/faces/SluzbaPrzygotowawcza/ArkuszSluzbyPrzygotowawczej3.xhtml");
		AppBean.addUrlMapping("/ArkuszSluzbyPrzygotowawczej4", "/faces/SluzbaPrzygotowawcza/ArkuszSluzbyPrzygotowawczej4.xhtml");

		// IPRZ
		AppBean.addUrlMapping("/ListaArkuszyIPRZ", "/faces/IPRZ/ListaArkuszyIPRZ.xhtml");
		AppBean.addUrlMapping("/ListaArkuszyIPRZFiltr", "/faces/IPRZ/ListaArkuszyIPRZFiltrowanie.xhtml");
		AppBean.addUrlMapping("/ArkuszIPRZ", "/faces/IPRZ/ArkuszIPRZ.xhtml");
		AppBean.addUrlMapping("/ListaArkuszyIPRZWObiegu", "/faces/IPRZ/ListaArkuszyIPRZWObiegu.xhtml");
		AppBean.addUrlMapping("/IPRZRaporty", "/faces/IPRZ/IPRZRaporty.xhtml");
		AppBean.addUrlMapping("/RaportowanieIPRZ", "/faces/IPRZ/RaportowanieIPRZ.xhtml");

		// Wydruki TEST
		AppBean.addUrlMapping("/WydrukiTEST", "/faces/wydrukiTEST/wydrukiTEST.xhtml");



		// Rekrutacja
//		AppBean.addUrlMapping("/KartotekaKandydata", "/faces/Rekrutacja/KartotekaKandydata.xhtml");
		AppBean.addUrlMapping("/KartotekaRekrutacji", "/faces/Rekrutacja/KartotekaRekrutacji.xhtml");
		AppBean.addUrlMapping("/KartotekaRekrutacjiWno5", "/faces/Rekrutacja/KartotekaRekrutacjiWno5.xhtml");
//		AppBean.addUrlMapping("/RekrutacjaRaporty", "/faces/Rekrutacja/RekrutacjaRaporty.xhtml");
		AppBean.addUrlMapping("/RekrutacjaWniosek", "/faces/Rekrutacja/RekrutacjaWniosek.xhtml");
		AppBean.addUrlMapping("/RekrutacjaOchronaDanych", "/faces/Rekrutacja/RekrutacjaOchronaDanych.xhtml");

		
		
		AppBean.addUrlMapping("/RekrutacjaWnioskiWObiegu", "/faces/Rekrutacja/RekrutacjaWnioskiWObiegu.xhtml");
		AppBean.addUrlMapping("/ListaKartotek", "/faces/Rekrutacja/ListaKartotek.xhtml");
		AppBean.addUrlMapping("/NaborRekrutacjaRaport", "/faces/Rekrutacja/NaborRekrutacjaRaport.xhtml");
		AppBean.addUrlMapping("/ListaWnioskowRekrutacje", "/faces/Rekrutacja/ListaWnioskowORekrutacje.xhtml");
		AppBean.addUrlMapping("/MojeWnioskiORekrutacje", "/faces/Rekrutacja/ListaWnioskowORekrutacjeMoje.xhtml");
		AppBean.addUrlMapping("/RekrutacjaWniosek1", "/faces/Rekrutacja/RekrutacjaWniosek1.xhtml");
		AppBean.addUrlMapping("/RekrutacjaWniosek2", "/faces/Rekrutacja/RekrutacjaWniosek2.xhtml");
		AppBean.addUrlMapping("/RekrutacjaWniosek3", "/faces/Rekrutacja/RekrutacjaWniosek3.xhtml");
		AppBean.addUrlMapping("/RekrutacjaWniosek4", "/faces/Rekrutacja/RekrutacjaWniosek4.xhtml");
		AppBean.addUrlMapping("/RekrutacjaWniosek5", "/faces/Rekrutacja/RekrutacjaWniosek5.xhtml");
		
		AppBean.addUrlMapping("/TestDziedziczenia", "/faces/Rekrutacja/TestDziedziczenieRekrutacjaWniosek1.xhtml");
		AppBean.addUrlMapping("/TestDziedziczeniaLista", "/faces/Rekrutacja/TestDziedziczeniaListaWnioskowORekrutacje.xhtml");
//		WniosekOkulary
		AppBean.addUrlMapping("/WniosekOkulary", "/faces/WniosekOkulary/WniosekOkulary.xhtml");
		AppBean.addUrlMapping("/ListaWnioskowOkulary", "/faces/WniosekOkulary/ListaWnioskowOkulary.xhtml");
		
//		Stan magazynowy
		AppBean.addUrlMapping("/StanMagazynowyWyszukiwarka", "/faces/StanMagazynowy/StanMagazynowy.xhtml");
		AppBean.addUrlMapping("/StanMagazynowy", "/faces/StanMagazynowy/StanMagazynowyWyszukane.xhtml");
		AppBean.addUrlMapping("/StanMagazynowyBiblKsiazki", "/faces/StanMagazynowy/StanMagazynowyBiblKsiazWyszukaneZZZ.xhtml");
		AppBean.addUrlMapping("/StanMagazynowyBiblCzasopisma", "/faces/StanMagazynowy/StanMagazynowyBiblCzasopWyszukaneZZZ.xhtml");
		

		//SzczegolneWarunkiPracy
		AppBean.addUrlMapping("/RejestrCzynnikowSzkodliwych", "/faces/SzczegolneWarunkiPracy/RejestrCzynnikowSzkodliwych.xhtml");
		AppBean.addUrlMapping("/RejestrCzynnikowUciazliwych", "/faces/SzczegolneWarunkiPracy/RejestrCzynnikowUciazliwych.xhtml");

		
		//Premie
		AppBean.addUrlMapping("/PremieListaWnioskow", "/faces/Premie/PremieListaWnioskow.xhtml");
		AppBean.addUrlMapping("/PremieWniosek", "/faces/Premie/PremieWniosek.xhtml");
		
		
//		Panel administratora
		AppBean.addUrlMapping("/ListyWartosci", "/faces/Administrator/ListaWartosciLista.xhtml");
		AppBean.addUrlMapping("/SzczegolyListyWartosci", "/faces/Administrator/SzczegolyListyWartosci.xhtml");
		AppBean.addUrlMapping("/DaneListyWartosci", "/faces/Administrator/DaneListyWartosci.xhtml");
//		AppBean.addUrlMapping("/ListyWartosciPP", "/faces/Administrator/ListyWartosciPPZZZ.xhtml");
		AppBean.addUrlMapping("/ListaSlownikow", "/faces/Administrator/ListaSlownikow.xhtml");
		AppBean.addUrlMapping("/SzczegolySlownika", "/faces/Administrator/SzczegolySlownika.xhtml");
		AppBean.addUrlMapping("/ListaTranslacji", "/faces/Administrator/ListaTranslacji.xhtml");
		AppBean.addUrlMapping("/DaneTranslacji", "/faces/Administrator/DaneTranslacji.xhtml");
		AppBean.addUrlMapping("/KonfiguracjaMenu", "/faces/Administrator/KonfiguracjaMenu.xhtml");
		AppBean.addUrlMapping("/ZmianaUzytkownikaLista", "/faces/Administrator/ZmianaUzytkownikaLista.xhtml");
		AppBean.addUrlMapping("/KontrolaUprawnien", "/faces/KontrolaUprawnien/KontrolaUprawnien.xhtml");
		AppBean.addUrlMapping("/ListaTabel", "/faces/Administrator/ListaTabel.xhtml");
		AppBean.addUrlMapping("/ListyObiektowBD", "/faces/Administrator/ListyObiektowBD.xhtml");
		AppBean.addUrlMapping("/DiffDbCodeVsPPCode", "/faces/Administrator/DiffDbCodeVsPPCode.xhtml");

		// Delegacje Zagraniczne
		AppBean.addUrlMapping("/DelegacjaZagraniczna", "/faces/DelegacjeMF/DelegacjaMFZagr.xhtml");
//		AppBean.addUrlMapping("/DelZagrRozliczenia", "/faces/DelegacjeZagraniczne/DelZagrRozliczenia.xhtml");
//		AppBean.addUrlMapping("/DelZagrRozliczeniaWObiegu", "/faces/DelegacjeZagraniczne/DelZagrRozliczeniaWObiegu.xhtml");
//		AppBean.addUrlMapping("/DelZagrRozliczenie", "/faces/DelegacjeZagraniczne/DelZagrRozliczenie.xhtml");
//		AppBean.addUrlMapping("/DelZagrWniosekWyjazdowy", "/faces/DelegacjeZagraniczne/DelZagrWniosekWyjazdowyZZZ.xhtml");
//		AppBean.addUrlMapping("/DelZagrWnioski", "/faces/DelegacjeZagraniczne/DelZagrWnioskiZZZ.xhtml");
//		AppBean.addUrlMapping("/DelZagrWnioskiWObiegu", "/faces/DelegacjeZagraniczne/DelZagrWnioskiWObieguZZZ.xhtml");
//		AppBean.addUrlMapping("/DelZagrRaporty", "/faces/DelegacjeKrajowe/DelZagrRaporty.xhtml");

		// Delegacje Krajowe
		AppBean.addUrlMapping("/DelegacjaKrajowa", "/faces/DelegacjeMF/DelegacjaMFKraj.xhtml");
//		AppBean.addUrlMapping("/DelKrajRozliczenia", "/faces/DelegacjeKrajowe/DelKrajRozliczenia.xhtml");
//		AppBean.addUrlMapping("/DelKrajRozliczeniaWObiegu", "/faces/DelegacjeKrajowe/DelKrajRozliczeniaWObiegu.xhtml");
//		AppBean.addUrlMapping("/DelKrajRozliczenie", "/faces/DelegacjeKrajowe/DelKrajRozliczenie.xhtml");
//		AppBean.addUrlMapping("/DelKrajPoleceniePodrozy", "/faces/DelegacjeKrajowe/DelKrajPoleceniePodrozyZZZ.xhtml");
//		AppBean.addUrlMapping("/DelKrajPolecenia", "/faces/DelegacjeKrajowe/DelKrajPolecenia.xhtml");
//		AppBean.addUrlMapping("/DelKrajPoleceniaWObiegu", "/faces/DelegacjeKrajowe/DelKrajPoleceniaWObiegu.xhtml");
//		AppBean.addUrlMapping("/DelKrajRaporty", "/faces/DelegacjeKrajowe/DelKrajRaporty.xhtml");

		// DelegacjeMF
//		AppBean.addUrlMapping("/Delegacja", "/faces/DelegacjeMF/DelegacjaMF.xhtml");
		AppBean.addUrlMapping("/Delegacje", "/faces/DelegacjeMF/ListaDelegacjiMF.xhtml");
		AppBean.addUrlMapping("/DelegacjeKrajowe", "/faces/DelegacjeMF/ListaDelegacjiKrajMF.xhtml");
		AppBean.addUrlMapping("/DelegacjeZagraniczne", "/faces/DelegacjeMF/ListaDelegacjiZagrMF.xhtml");
		AppBean.addUrlMapping("/DelegacjeWgCzynnosci", "/faces/DelegacjeMF/ListaDelegacjiWgCzynnosci.xhtml");
		
		AppBean.addUrlMapping("/DelegacjeBudzet", "/faces/DelegacjeMF/BudzetDelegacje.xhtml");		
		
		
		//Delegacje MF
		AppBean.addUrlMapping("/TypyPozycjiDelegacjiLista", "/faces/DelegacjeMF/TypyPozycjiDelegacjiLista.xhtml");
		AppBean.addUrlMapping("/TypyPozycjiDelegacji", "/faces/DelegacjeMF/TypyPozycjiDelegacji.xhtml");
		AppBean.addUrlMapping("/StawkiDelegacji", "/faces/DelegacjeMF/StawkiDelegacjiLista.xhtml");
		AppBean.addUrlMapping("/StawkiDelegacjiEdycja", "/faces/DelegacjeMF/StawkiDelegacjiEdycja.xhtml");

		//Delegacje STD
		AppBean.addUrlMapping("/ListaDelegacji", "/faces/DelegacjeStd/ListaDelegacjiStd.xhtml");
		AppBean.addUrlMapping("/DelKraj", "/faces/DelegacjeStd/DelegacjaKrajStd.xhtml");
		AppBean.addUrlMapping("/DelZagr", "/faces/DelegacjeStd/DelegacjaZagrStd.xhtml");
		AppBean.addUrlMapping("/TypyPozycjiDelegacjiListaSTD", "/faces/DelegacjeStd/TypyPozycjiDelegacjiLista.xhtml");
		AppBean.addUrlMapping("/TypyPozycjiDelegacjiSTD", "/faces/DelegacjeStd/TypyPozycjiDelegacji.xhtml");
		AppBean.addUrlMapping("/StawkiDelegacjiSTD", "/faces/DelegacjeStd/StawkiDelegacjiLista.xhtml");
		AppBean.addUrlMapping("/StawkiDelegacjiEdycjaSTD", "/faces/DelegacjeStd/StawkiDelegacjiEdycja.xhtml");

		
		
		// BHP
		AppBean.addUrlMapping("/ListaSzkolenBhp", "/faces/BHP/ListaSzkolenBhp.xhtml");
		
		//POWIADOMIENIA / IFRAME
		AppBean.addUrlMapping("/Powiadomienia", "/faces/Powiadomienia/Powiadomienia.xhtml");
		AppBean.addUrlMapping("/Msgs", "/faces/Powiadomienia/Msgs.xhtml");
		AppBean.addUrlMapping("/Dlg", "/faces/Dlg/Dlg.xhtml");		
		
		AppBean.addUrlMapping("/KontrolaUprawnienMList", "/faces/KontrolaUprawnien/KontrolaUprawnien.xhtml");
		
		
		AppBean.addUrlMapping("/AdmDelegacje", "/faces/Administrator/ADM-Delegacje.xhtml");
		
		//Wnioski o aktualizacje danych Tarnów
		AppBean.addUrlMapping("/WnioskiAktualizacje", "/faces/AktualizacjeDanych/ListaWnioskiAktualzacjeDanych.xhtml");
		AppBean.addUrlMapping("/WnioskiAktualizacjeOpinie", "/faces/AktualizacjeDanych/ListaWnioskiDoZaopiniowania.xhtml");
		AppBean.addUrlMapping("/WniosekAktualizacja", "/faces/AktualizacjeDanych/WniosekAktualizacjaDanych.xhtml");
		
		//Wnioski o zaświadczenia Tarnów
		AppBean.addUrlMapping("/WnioskiZaswiadczenia", "/faces/WnioskiZaswiadczenia/ListaWnioskiZaswiadczenia.xhtml");
		AppBean.addUrlMapping("/WniosekZaswiadczenie", "/faces/WnioskiZaswiadczenia/WniosekZaswiadczenie.xhtml");
		
		//Wnioski ZFŚS Tarnów
		AppBean.addUrlMapping("/ListaWnioskowZFSS", "/faces/ZFSS/ListaWnioskowZFSS.xhtml");

		AppBean.addUrlMapping("/WniosekZFSSPozyczka", "/faces/ZFSS/WniosekZFSSPozyczka.xhtml");
		AppBean.addUrlMapping("/WniosekZFSSZimowoWiosenny", "/faces/ZFSS/WniosekZFSSZimowoWiosenny.xhtml");
		AppBean.addUrlMapping("/WniosekZFSSWypoczynek", "/faces/ZFSS/WniosekZFSSWypoczynek.xhtml");
		AppBean.addUrlMapping("/WniosekZFSSBezzwrotnaPomoc", "/faces/ZFSS/WniosekZFSSBezzwrotnaPomoc.xhtml");
		AppBean.addUrlMapping("/WniosekZFSSMultiSport", "/faces/ZFSS/WniosekZFSSMultiSport.xhtml");
		AppBean.addUrlMapping("/WniosekZFSSBiletyiKarnety", "/faces/ZFSS/WniosekZFSSBiletyiKarnety.xhtml");
		AppBean.addUrlMapping("/WniosekZFSSUniwersalnyPozostale", "/faces/ZFSS/WniosekZFSSUniwersalnyPozostale.xhtml");
		
		AppBean.addUrlMapping("/ListaOswiadczenZFSS", "/faces/ZFSS/ListaOswiadczenZFSS.xhtml");
		AppBean.addUrlMapping("/OswiadczenieZFSS", "/faces/ZFSS/OswiadczenieZFSS.xhtml");
		
	
		AppBean.addUrlMapping("/ListaObecnosciMC", "/faces/ListyObecnosci/ListaObecnosciMC.xhtml");


		// Obiegi
		AppBean.addUrlMapping("/Obiegi", "/faces/Obiegi/Obiegi.xhtml");
		AppBean.addUrlMapping("/ListaObiegow", "/faces/Obiegi/ListaObiegow.xhtml");
		AppBean.addUrlMapping("/DiagramObiegu", "/faces/Obiegi/DiagramObiegu.xhtml");
		AppBean.addUrlMapping("/AkcjaObiegu", "/faces/Obiegi/AkcjaObiegu.xhtml");
	}
	

	@Override
	public Configuration getConfiguration(ServletContext context) {


		ConfigurationRuleBuilder ret = ConfigurationBuilder.begin()
				.addRule().when(Direction.isInbound().and(Response.isCommitted())).perform(Lifecycle.abort())
				.addRule((Join.path("/test").to("/faces/test.xhtml")))
				//pozpostałe mapowania wyżej w konstruktorze
				;
				

		for (Entry<String, String> m : AppBean.urlMappings.entrySet()) {
			ret.addRule((Join.path(m.getKey()).to(m.getValue())));
		}
		
		return ret;

	}

	@Override
	public int priority() {
		return 0;
	}

}