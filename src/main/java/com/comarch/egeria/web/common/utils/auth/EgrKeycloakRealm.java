package com.comarch.egeria.web.common.utils.auth;

import com.comarch.egeria.web.common.utils.auth.bo.AuthBo;
import io.buji.pac4j.realm.Pac4jRealm;
import io.buji.pac4j.subject.Pac4jPrincipal;
import javax.inject.Inject;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.subject.PrincipalCollection;

public class EgrKeycloakRealm extends Pac4jRealm {

  @Inject
  AuthBo authBo;

  public EgrKeycloakRealm() {
    super();
  }

  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(final PrincipalCollection principals) {
    SimpleAuthorizationInfo simpleAuthorizationInfo = (SimpleAuthorizationInfo) super.doGetAuthorizationInfo(principals);
    Pac4jPrincipal principal = principals.oneByType(Pac4jPrincipal.class);
    if (principal != null) {
      simpleAuthorizationInfo.addStringPermissions(authBo.odczytajFunkcjeUzytkowe(
          getUsernameFromProfile(principal)));
    }
    return simpleAuthorizationInfo;
  }

  private String getUsernameFromProfile(Pac4jPrincipal principal) {
    return (String) principal.getProfile().getAttribute("preferred_username");
  }

}
