package com.comarch.egeria.web.common.utils.auth.pki;

import java.security.cert.CertificateException;

public class EgrPKICertificateEmailNotFoundException extends CertificateException {

	private static final long serialVersionUID = 1L;

	public EgrPKICertificateEmailNotFoundException() {
		this("No matching Email found in Certificate's SubjectAlternativeNames");
	}

	public EgrPKICertificateEmailNotFoundException(String message) {
		super(message);
	}
}
