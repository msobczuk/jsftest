package com.comarch.egeria.web.common.utils.auth.pki;

import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

public class EgrPKIAuth {

	/**
	 * Funkcja zwraca wybrany przez użytkownika certyfikat PKI.
	 * 
	 * @return - X509Certificate
	 * @throws EgrPKICertificateNotFoundException
	 */
	public static X509Certificate getX509Certificate() throws EgrPKICertificateNotFoundException {
		// pobieram obiekt HttpServletRequest
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		// pobieram Certyfikat zaimportowany do przeglądarki
		X509Certificate certChain[] = (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");

		// sprawdzam czy poprawnie pobrano certyfikat
		if (certChain == null || certChain.length == 0) {
			throw new EgrPKICertificateNotFoundException();
		}
		// zwracam pierwszy certyfikat z zwróconej tablicy - tablica zawsze
		// powinna być jednoelementowa
		return certChain[0];
	}

	/**
	 * Funkcja sprawdza czy przekazany certyfikat spełnia wymagania certyfikatu
	 * PKI w Comarch
	 * 
	 * @param X509Certificate
	 *            - walidwany certyfikat
	 * 
	 * @return X509Certificate
	 * 
	 * @throws CertificateExpiredException,
	 * @throws CertificateNotYetValidException,
	 * @throws EgrPKICertificateWrongIssuerException,
	 * @throws CertificateParsingException
	 * @throws EgrPKICertificateEmailNotFoundException
	 * @throws EgrPKICertificateNotFoundException
	 */
	public static String checkComarchX509Certificate()
			throws CertificateExpiredException, CertificateNotYetValidException, EgrPKICertificateWrongIssuerException,
      CertificateParsingException, EgrPKICertificateEmailNotFoundException, EgrPKICertificateNotFoundException {

		// pobieram obiekt HttpServletRequest
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		// pobieram Certyfikat zaimportowany do przeglądarki (Powinien byc pobrany PKI)
		X509Certificate certChain[] = (X509Certificate[]) request.getAttribute("javax.servlet.request.X509Certificate");

		// sprawdzam czy poprawnie pobrano certyfikat
		if (certChain == null || certChain.length == 0) {
			throw new EgrPKICertificateNotFoundException();
		}

		//Issuer: CN=GK COMARCH PERSONAL CA, OU=PERSONAL, O=COMARCH
		X509Certificate personalCert = certChain[0];
		//Issuer: CN=GK COMARCH ROOT CA, O=COMARCH
		X509Certificate comarchCert = certChain[certChain.length -1];
		
		//printCertyficateInfo(personalCert);
		//printCertyficateInfo(comarchCert);

		// Spracdzam ważnośc wybranego certyfikatu
		personalCert.checkValidity();
			
		// Spracdzam czy certyfikat ma poprawnego wystawcę
		if (!((comarchCert.getIssuerDN().toString().equals("CN=COMARCH ROOT CA, O=COMARCH"))
				|| (comarchCert.getIssuerDN().toString().equals("CN=GK COMARCH ROOT CA, O=COMARCH")))) {

			throw new EgrPKICertificateWrongIssuerException("Wrong Certyficate Issuer - IssuerDN: " + comarchCert.getIssuerDN());
		}

		// Spracdzam czy w alternativs names certyfikatu znajduje sie adres
		// email z koncówką "@comarch.com"
		Collection<List<?>> c = personalCert.getSubjectAlternativeNames();
		
		String userEmail = null;
		if (c != null) {			
			for (Iterator<List<?>> ii = c.iterator(); ii.hasNext();) {
				Object o = ii.next();
				if (o != null) {
					String subjectAlternativeName = o.toString().toLowerCase();

					if (subjectAlternativeName.length() > 3 && subjectAlternativeName.contains("@comarch.com")) {
						userEmail = subjectAlternativeName.substring(4, subjectAlternativeName.length() - 1);
						break;
					}
				}
			}
		}
		
		if (userEmail == null) {
			throw new EgrPKICertificateEmailNotFoundException();
		}
		
		return userEmail;
	}

	private static void printCertyficateInfo(X509Certificate crt) {

		System.err.println("crt.getSubjectDN().toString(): " + crt.getSubjectDN().toString());
		System.err.println("crt.getIssuerDN().toString(): " + crt.getIssuerDN().toString());
		System.err.println("crt.getIssuerDN().getName().toString(): " + crt.getIssuerDN().getName().toString());
		System.err.println("crt.getIssuerX500Principal().toString(): " + crt.getIssuerX500Principal().toString());
		System.err.println("crt.toString(): " + crt.toString());
		try {
			System.err.println("crt.getSubjectAlternativeNames(): " + crt.getSubjectAlternativeNames());
		} catch (CertificateParsingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	private static String getNameFromEmail(String email){
		String login = email.substring(0, email.indexOf("@"));
		
		String name = login.substring(0, login.indexOf("."));
		String surname = login.substring(login.lastIndexOf(".") + 1);
		
		name = name.substring(0, 1).toUpperCase() + name.substring(1);
		surname = surname.substring(0, 1).toUpperCase() + surname.substring(1);
		
		return name + " " + surname;
	}

}