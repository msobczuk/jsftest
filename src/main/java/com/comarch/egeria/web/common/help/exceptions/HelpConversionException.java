package com.comarch.egeria.web.common.help.exceptions;

public class HelpConversionException extends Exception {

	private static final long serialVersionUID = -533271981734261689L;

	public HelpConversionException(String message, Throwable cause) {
		super(message, cause);
	}

	public HelpConversionException(String message) {
		super(message);
	}

	public HelpConversionException(Throwable cause) {
		super(cause);
	}
}
