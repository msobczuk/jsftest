package com.comarch.egeria.web.common.view;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.WordUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.web.common.session.EgrUserSessionBean;

@Named
@Scope("view")
public class IndexBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(IndexBean.class);

	private String username;

	@Inject
	private EgrUserSessionBean egrUserSessionBean;

	@PostConstruct
	private void init() {

	}

	public String getUsername() {
		return WordUtils.capitalize(username);
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
