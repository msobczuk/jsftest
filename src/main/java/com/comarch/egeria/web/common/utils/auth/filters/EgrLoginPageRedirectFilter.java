package com.comarch.egeria.web.common.utils.auth.filters;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;

import com.comarch.egeria.web.common.session.EgrUserSessionBean;
import com.comarch.egeria.web.common.utils.constants.Const;

/**
 * Filtr przekierowujący na stronę logowania, jeśli użytkownik jest
 * zautentykowany, ale nie został wciśnięty przycisk "Uruchom" (dotyczy
 * logowania CAS)
 */
public class EgrLoginPageRedirectFilter implements Filter {

	@Inject
	EgrUserSessionBean userSessionBean;

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) res;

		if (SecurityUtils.getSubject().isAuthenticated() && userSessionBean.getDbUsername() == null) {
			response.sendRedirect(request.getContextPath() + "/" + Const.LOGIN_PAGE_URL);
		} else {
			chain.doFilter(req, res);
		}
	}

	@Override
	public void destroy() {
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}
}
