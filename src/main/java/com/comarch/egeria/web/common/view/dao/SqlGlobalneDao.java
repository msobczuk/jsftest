package com.comarch.egeria.web.common.view.dao;

import java.sql.SQLException;
import java.util.List;

import com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu.domain.Firma;
import com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu.domain.Oddzial;

/**
 * Interefejs określający co może robić klasa SqlGlobalneBoImpl
 * 
 * @author g2.12
 * @version 12-11-2015
 * @see com.comarch.egeria.ctl.common.view.bo.impl.SqlGlobalneBoImpl#implementacja
 */

public interface SqlGlobalneDao {

	/**
	 * Pobiera aktualnie ustawioną firmę użytkownika w eap_globals
	 * 
	 * @return <Firma>
	 */
	Firma sqlUstawionaFirmaUzytkownika() throws SQLException;
	
	/**
	 * Pobiera listę Firm do których użytkownik jest przypisany
	 * 
	 * @return List<Firmy>
	 */
	public List<Firma> sqlFirmyUzytkownika() throws SQLException;
	
	/**
	 * Pobiera aktualnie ustawiony oddział użytkownika w eap_globals
	 * 
	 * @return <Firma>
	 */
	Oddzial sqlUstawionyOddzialUzytkownika() throws SQLException;

	/**
	 * Pobiera listę Oddziałów do których użytkownik ma dostęp w aktualnie wybranej firmie
	 * 
	 * @return List<Oddzialy>
	 */
	public List<Oddzial> sqlOddzialyUzytkownika() throws SQLException;

	void ustawFirme(Firma selectedFirma) throws SQLException;
	
	void ustawOddzial(Oddzial selectedOddzial) throws SQLException;
}

