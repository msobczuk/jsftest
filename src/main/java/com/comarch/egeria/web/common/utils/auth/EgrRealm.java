package com.comarch.egeria.web.common.utils.auth;

import com.comarch.egeria.web.common.utils.auth.EgrAuthToken.EgrAuthMethod;
import com.comarch.egeria.web.common.utils.auth.bo.AuthBo;
import com.comarch.egeria.web.common.utils.lang.LanguageBean;
import javax.inject.Inject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

public class EgrRealm extends AuthorizingRealm {

  private static final Logger log = LogManager.getLogger(EgrRealm.class);

  @Inject
  AuthBo authBo;

  @Override
  protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authToken)
      throws AuthenticationException {
    final EgrAuthToken egrAuthToken = (EgrAuthToken) authToken;
    try {
      if (EgrAuthMethod.ORACLE.equals(egrAuthToken.getEgrAuthMethod())) {
        authBo.authOracle(egrAuthToken.getUsername(), new String(egrAuthToken.getPassword()));
      } else if (EgrAuthMethod.EGR.equals(egrAuthToken.getEgrAuthMethod())) {
        authBo.authEgr(egrAuthToken.getUsername(), new String(egrAuthToken.getPassword()));
      }
    } catch (IncorrectCredentialsException ex) {
      throw new IncorrectCredentialsException(LanguageBean.translate("INF_incorrect_passw"), ex);
    } catch (LockedAccountException ex) {
      throw new LockedAccountException(LanguageBean.translate("INF_locked_account"), ex);
    } catch (AuthenticationException ex) {
      throw new AuthenticationException(LanguageBean.translate("INF_login_error"), ex);
    }
    return new SimpleAuthenticationInfo(egrAuthToken.getUsername(),
        new String(egrAuthToken.getPassword()),
        getName());
  }

  @Override
  protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {

    SimpleAuthorizationInfo authInfo = new SimpleAuthorizationInfo();
    // authInfo.addRole("PrzykladowaRola");
    // authInfo.addRole("PRACOWNICY");
    // authInfo.addRole("NARZĘDZIA");
    authInfo.addRole("KSIAZKATEL");

    authInfo.addStringPermissions(
        authBo.odczytajFunkcjeUzytkowe(principals.getPrimaryPrincipal().toString()));

    return authInfo;
  }

}
