package com.comarch.egeria.web.common.utils.auth;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.PermissionsAuthorizationFilter;

import com.comarch.egeria.web.common.utils.constants.Const;

public class EgrPermissionsAuthorizationFilter extends PermissionsAuthorizationFilter {

	private static final Logger LOG = LogManager.getLogger(EgrPermissionsAuthorizationFilter.class);

	/*
	 * Prośba o wpisywanie reguł w podobnej kolejności jak występują w menu. Na
	 * przyszłośc będzie sie lepeiej szukać.
	 *
	 */

	@Override
	public boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
		Subject subject = getSubject(request, response);
		// String[] perms = (String[]) mappedValue;

		String url = ((HttpServletRequest) request).getRequestURL().toString();
//		LOG.debug(url);

		boolean isPermitted = true;

		// Rozliczenia
		if (url.endsWith(Const.HOME_REDIRECT_URL + "/PrzykladowyFormularz")
				&& !(subject.hasRole("PrzykladowaRola"))) {
			isPermitted = false;
		}
		
		else if (url.endsWith(Const.HOME_REDIRECT_URL + "/Pracownicy")
				&& !(subject.hasRole("PRACOWNICY"))) {
			isPermitted = false;
		}
		else if (url.endsWith(Const.HOME_REDIRECT_URL + "/Data")
				&& !(subject.hasRole("PRACOWNICY"))) {
			isPermitted = false;
		}
		//Ksiazka telefoniczna
		else if (url.endsWith(Const.HOME_REDIRECT_URL + "/KsiazkaTelefoniczna")
				&& !(subject.hasRole("KSIAZKATEL"))) {
			isPermitted = false;
		}
		else if (url.endsWith(Const.HOME_REDIRECT_URL + "/KsiazkaTelefonicznaUlubione")
				&& !(subject.hasRole("KSIAZKATEL"))) {
			isPermitted = false;
		}
		else if (url.endsWith(Const.HOME_REDIRECT_URL + "/KsiazkaTelefonicznaKontaktySpecjalne")
				&& !(subject.hasRole("KSIAZKATEL"))) {
			isPermitted = false;
		}
		else if (url.endsWith(Const.HOME_REDIRECT_URL + "/KsiazkaTelefonicznaStrukturaOrganizacyjna")
				&& !(subject.hasRole("KSIAZKATEL"))) {
			isPermitted = false;
		}
		else if (url.endsWith(Const.HOME_REDIRECT_URL + "/KsiazkaTelefonicznaUstawienia")
				&& !(subject.hasRole("KSIAZKATEL"))) {
			isPermitted = false;
		}
		
		
		
		return isPermitted;
	}

}

// if (perms != null && perms.length > 0) {
// if (perms.length == 1) {
// if (!subject.isPermitted(perms[0])) {
// isPermitted = false;
// }
// } else {
// if (!subject.isPermittedAll(perms)) {
// isPermitted = false;
// }
// }
// }
