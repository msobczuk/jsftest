package com.comarch.egeria.web.common.help.exceptions;

public class HelpAccessException extends Exception {

	private static final long serialVersionUID = -533271981734261689L;

	public HelpAccessException(String message, Throwable cause) {
		super(message, cause);
	}

	public HelpAccessException(String message) {
		super(message);
	}

	public HelpAccessException(Throwable cause) {
		super(cause);
	}
}
