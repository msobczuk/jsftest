package com.comarch.egeria.web.common.utils.exceptions;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Arrays;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.comarch.egeria.web.common.utils.lang.LanguageBean;



/**
 * Klasa do obsługi komunikatów aplikacji.
 * 
 * @see com.comarch.egeria.ctl.common.utils.exceptions.bo.CtlMessageUtils#Dokumentacja
 */
public class CtlMessageUtils {

	/**
	 * TODO
	 * 
	 * @return TODO
	 */
	public static FacesContext getFacesContext() {
		return FacesContext.getCurrentInstance();
	}

	/**
	 * TODO
	 * 
	 * @return TODO
	 */
	public static void addErrorMessage(final String messageCode, final Object... arguments) {
		addMessage(null, FacesMessage.SEVERITY_ERROR, "MSG_ERROR", messageCode, arguments);
	}

	/**
	 * TODO
	 * 
	 * @return TODO
	 */
	public static void addWarningMessage(final String messageCode, final Object... arguments) {
		addMessage(null, FacesMessage.SEVERITY_WARN, "MSG_WARN", messageCode, arguments);
	}

	/**
	 * TODO
	 * 
	 * @return TODO
	 */
	public static void addInfoMessage(final String messageCode, final Object... arguments) {
		addMessage(null, FacesMessage.SEVERITY_INFO, "MSG_INFO", messageCode, arguments);
	}

	/**
	 * TODO
	 * 
	 * @return TODO
	 */
	public static void addFatalMessage(final String messageCode, final Object... arguments) {
		addMessage(null, FacesMessage.SEVERITY_FATAL, "MSG_FATAL", messageCode, arguments);
	}

	/**
	 * TODO
	 * 
	 * @return TODO
	 */
	private static void addMessage(final String clientId, final FacesMessage.Severity severity,
			final String summaryCode, final String messageCode, final Object... arguments) {
		getFacesContext().addMessage(clientId, new FacesMessage(severity, LanguageBean.translate(summaryCode),
				MessageFormat.format(LanguageBean.translate(messageCode), arguments)));
	}

	/**
	 * Funkcja zwraca kod komunikatu z odpowiadajacy zawartości plików
	 * dictionary (MSGXXXX), który ma zostać wyswietlony użytkownikowi.
	 * 
	 * @param e
	 *            Obsługiwany wyjatek
	 * @return - Kod komunikatu, który ma zostać wyswietlony użytkownikowi
	 */
	public static String getErrorMsgCode(SQLException e) {
		return "MSG" + getErrorCode(e);
	}

	/**
	 * TODO
	 * 
	 * @return TODO
	 */
	public static String getErrorCode(SQLException e) {
		return e.getMessage().substring(11, 15);
	}

	/**
	 * Funkcja sprawdza czy kod obsługiwanego wyjątku (rzuconego z pakietu
	 * bazodanowego) odpowiada jednemu z kodów przekazanych w kolejnych
	 * argumentach
	 * 
	 * @param e
	 *            Obsługiwany wyjatek
	 * @param codes
	 *            Kolejne kody wyjątku które chcemy obsłużyc
	 * 
	 * @return true lub false w zależności od tego czy kod obsługiwanego wyjatku
	 *         odpowiada jednemu z kodów przekazanych w kolejnych argumentach
	 */
	public static boolean isErrorCodeIn(SQLException e, final String... codes) {
		return (Arrays.asList(codes).contains(getErrorCode(e)));
	}
}
