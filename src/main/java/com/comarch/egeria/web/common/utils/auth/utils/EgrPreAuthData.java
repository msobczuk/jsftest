package com.comarch.egeria.web.common.utils.auth.utils;

public class EgrPreAuthData {
	private String username;
	private String dbUsername;
	private String authMethod;
	private int prc_id;
	private String prc_imie;
	private String prc_nazwisko;

	public String getPrc_imie() {
		return prc_imie;
	}

	public void setPrc_imie(String prc_imie) {
		this.prc_imie = prc_imie;
	}

	public String getPrc_nazwisko() {
		return prc_nazwisko;
	}

	public void setPrc_nazwisko(String prc_nazwisko) {
		this.prc_nazwisko = prc_nazwisko;
	}

	public int getPrc_id() {
		return prc_id;
	}

	public void setPrc_id(int prc_id) {
		this.prc_id = prc_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDbUsername() {
		return dbUsername;
	}

	public void setDbUsername(String dbUsername) {
		this.dbUsername = dbUsername;
	}

	public String getAuthMethod() {
		return authMethod;
	}

	public void setAuthMethod(String authMethod) {
		this.authMethod = authMethod;
	}
	
	@Override
	public String toString() {
		return "EgrPreAuthData [username=" + username + ", dbUsername=" + dbUsername + ", authMethod=" + authMethod + "]";
	}

}
