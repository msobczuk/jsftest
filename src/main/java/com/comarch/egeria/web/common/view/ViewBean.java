package com.comarch.egeria.web.common.view;

public abstract class ViewBean {
	
	public abstract void refreshOnCenterChange();
	
}
