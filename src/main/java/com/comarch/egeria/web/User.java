package com.comarch.egeria.web;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.data.msg.Msg;
import com.comarch.egeria.pp.data.msg.Push;
import com.comarch.egeria.pp.kontrolaUprawnien.KontrolaUprawnien;
import com.comarch.egeria.pp.kontrolaUprawnien.model.WebUprawnienia;
import com.comarch.egeria.web.common.utils.constants.Const;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.springframework.context.annotation.Scope;

import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.sql.rowset.CachedRowSet;
import java.io.IOException;
import java.sql.Connection;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.konfiguracjaWartoscZewn;
import static com.comarch.egeria.Utils.nz;

//import org.primefaces.context.RequestContext;

@ManagedBean
@Named
@Scope("session")
public class User extends SqlBean {
	private static final Logger log = LogManager.getLogger();

//	@Inject
//	AppBean app;

	private int defaultPrcId;
	private int defaultFrmId;
	private int prcId;
	private int frmId;

	private String login; //cas: XAAHE / std: ADMINISTRATOR_PORTAL - pole UZT_NAZWA_ZEWN z EAT_UZYTKOWNICY
	
	private String dbLogin; //cas: MDZIOBEK  / std: ADMINISTRATOR_PORTAL - pole UZT_NAZWA z EAT_UZYTKOWNICY
	
	private String imie; // imie z EAT_UZYTKOWNICY
	
	private String nazwisko;  // nazwisko z EAT_UZYTKOWNICY
	
	private String profil = null;
	
	private String pracownikLabel;
	
	
	
	private String urlHref; 
	private String urlHostname; 
	private String urlPort; 
	private String urlPathname; 
	private String urlProtocol;
	private String currentWindowName = null;
	
	
	private List<String> permissions = new ArrayList<String>();
	private HashMap<String, PermissionSelHolder> permissionsEnalbledHM = new HashMap<>();

	String userSessionGUID = UUID.randomUUID().toString();//niezalezny identyfikator dla sesji uzytkownika, który nie jest traktowany w localStorage jako informacja wrażliwa
	public String getUserSessionGUID() {
		return userSessionGUID;
	}

	
//	private HashMap<String, String> wartosciDomyslne  = new HashMap<>();
	
	//elementy przekazywane w ramach sesji pomiedzy kontrolerami (Bean), których scope to view lub request...  
	//uwaga te elementy są czyszczone na pocz. kazdego full-request'a (z EgrTempl) 
	public ConcurrentHashMap<String, Object> tmpUserObjectsCHM = new ConcurrentHashMap<>();

	private List<Msg> msgs =  Collections.synchronizedList( new ArrayList<>() );
	
	public ConcurrentLinkedQueue<Msg> newMsgs = new ConcurrentLinkedQueue<>();
	
	
	private Msg currentMsg = new Msg();
	
	private Long szwId = null;
	
	Boolean ku = null; 
	
	Boolean renderKU = false; //domyslnie nie renderujemy btnKU


    @PreDestroy
	private void predestroy() {
		this.tmpUserObjectsCHM.clear();
		this.clearSqlDataCache();
	}


	public String editRowPage = null; //doFilterAndSelectRowByUrlParams
	public String editRowPageEntityId = null;//doFilterAndSelectRowByUrlParams
	public DataRow editRowPageDataRow = null;//doFilterAndSelectRowByUrlParams

	public void doEditRowPage() throws IOException { //doFilterAndSelectRowByUrlParams
		if (editRowPageEntityId==null) return;
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", editRowPageEntityId);
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", editRowPageDataRow);
		editRowPageEntityId = null;
		editRowPageDataRow = null;
		FacesContext.getCurrentInstance().getExternalContext().redirect(editRowPage); //??? a to zapętla się.
		editRowPage = null;
		return;
	}

	public void clean() {
		prcId = 0;
		frmId = 0;
		login = null;
		dbLogin = null;
		imie = null;
		nazwisko = null;
		profil = null;
		pracownikLabel = null;
		permissions = new ArrayList<String>();
		permissionsEnalbledHM.clear();
//		wartosciDomyslne  = new HashMap<>();
		tmpUserObjectsCHM = new ConcurrentHashMap<>();
		msgs =  Collections.synchronizedList( new ArrayList<>() );
		newMsgs = new ConcurrentLinkedQueue<>();
		currentMsg = new Msg();
		szwId = null;
		ku = null;

		//SqlDataSelectionsHandler lw = this.getSql("user_ek_pracownik");
		//if (lw!=null) lw.resetTs();
        this.clearSqlDataCache();
        this.sessionId = null;
	}
	
	public boolean isKU(){

		if (this.ku == null)
			ku = "true".equals((""+FacesContext.getCurrentInstance().getExternalContext().getInitParameter("KontrolaUprawnien")).toLowerCase());
		
		if (
				   (""+login  ).toLowerCase().contains("svc-egr-")
				|| (""+dbLogin).toLowerCase().contains("svc-egr-") 
		    ){
			return false;
		}else{
			return ku;
		}
		
	}
	
	
	public boolean isRenderKU(){
		if (!this.isAdmin()) return false;
		return renderKU;
	}
	
	public void setRenderKU(boolean x){
		this.renderKU = x;
	}
	
	
	public void update(String id){
		log.debug("js / u.p.d.a.t.e.C.m.d / " + id);
		com.comarch.egeria.Utils.update(id);
	}
	
	
	
//	public static void reloadAndUpdate(String sqlId, Object rowId){
//		System.out.println("reloadAndUpdate / " + sqlId + " / " + rowId);
//		SqlBean.reloadAndUpdate(sqlId, rowId);
//	}

	public String clearTmpUserObjectsCHM(){
		this.tmpUserObjectsCHM.clear();
		return "";
	}
	
	
	
	public void postLoginInit()  {//throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException
		//TODO: kod wczytujacy dane / ustawienia itp. po zalogowaniu
		this.sessionId = AppBean.getSessionId();
	}
	
	public Boolean allowCreateOcenaOkresowa(){
		Boolean ret = false;
		if (hasAnyPermission("PP_OCE_OCENIAJACY","PP_OCE_SEKRETARIAT","PP_OCE_ADMINISTRATOR")){
			ret = true;
		}
		return ret;
	}	
	
	
	public boolean isAdmin(){
		//		if(1==1) return false;
		//		if(1==1) return true;

//		boolean ret = "test2".equals(login) || "TEST2".equals(login);
//		return ret;

		return "administrator_portal".equals(login)
				|| hasPermission("PP_ADMINISTRATOR_PORTAL")
				|| hasPermission("PP_ADMINISTRATOR");
	}
	
	
	public static void info(String msg, Object... args){
		msg = msg.replace("\\r","\n").replace("\\n","\r\n");
		if (args.length>0)
			msg = String.format(msg, args);
			msg = msg.replaceAll("\n", "<br/>");
		
		AppBean.addMessage(msg);
	}
	
	public static void warn(String msg, Object... args){
		msg = msg.replace("\\r","\n").replace("\\n","\r\n");
		if (args.length>0)
			msg = String.format(msg, args);
			msg = msg.replaceAll("\n", "<br/>");
		
		AppBean.addMessage(FacesMessage.SEVERITY_WARN, msg);
	}
	
	
//	public static void alertme(String msg, Object... args){
//		alert(msg, args);
//	}

	
	
	public static void alert(Exception e) {

		User user = User.getCurrentUser();
		if (user==null){
			log.error(e.getMessage(), e);
			return;
		}

		SqlDataSelectionsHandler slErrMsgMappings = user.getSlownik("PPS_MAPOWANIE_BLEDOW");

		//mapowanie po typie błędów (stosować wpis wsl_wartosc zgodny z nazwa typu tylko jesli mamy okreslony własny rodzaj bledu)
		final DataRow row = slErrMsgMappings.find(e.getClass().getCanonicalName());
		if (row!=null){
			String lvl  = (row.getAsString("RV_TYPE") + "").toUpperCase();
			String txt = nz(row.getAsString("RV_MEANING"));

			if (eq(lvl, "INFO")) {
				User.info(txt);
			}else if (eq(lvl, "WARN")) {
				User.warn(txt);
			} else {
				log.error(e.getMessage(), e);
				User.alert(txt);
			}
			return;
		}


		//mapowanie po fragmencie opisu (w opisie lub zrzucie stosu szukamy tekstu harakterystycznego z WSL_OPIS).
		String exceptionDetails = Utils.getExceptionDetails(e);
		for (DataRow r : slErrMsgMappings.getData()) {
			String wslOpis = nz(r.getAsString("RV_MEANING"));
			if (wslOpis.isEmpty()) continue;
			if ((exceptionDetails+"").contains(wslOpis)) {

				String lvl  = (r.getAsString("RV_TYPE") + "").toUpperCase();
				String txt  = (r.getAsString("RV_ABBREVIATION") + "");
				if (txt.isEmpty()) txt += wslOpis;
			
				if (eq(lvl, "INFO")) {
					User.info(txt);
				}else if (eq(lvl, "WARN")) {
					User.warn(txt);
				} else {
					User.alert(txt);
				}
				
				return;
			}
		}

		log.error(e.getMessage(), e);

		User.alert(exceptionDetails);
	}


	public static void alert(String msg, Object... args){
		msg = msg.replace("\\r","\n").replace("\\n","\r\n");
		if (args.length>0)
			msg = String.format(msg, args);

		msg = msg.replaceAll("\n", "<br/>");
		
		AppBean.addMessage(FacesMessage.SEVERITY_ERROR, msg);
	}
	
	public static void alert(String msg, Throwable cause, Object... args){
		msg = msg.replace("\\r","\n").replace("\\n","\r\n");
		if (args.length>0)
			msg = String.format(msg, args);

		msg += "<br/><div style='font-size:80%; color:red'>" + cause.getMessage();
		for (int i=0; i<10; i++){
			cause = cause.getCause();
			if (cause==null) break;
			msg += ""+ cause.getMessage() + "<br/>";
		}
		msg+="</div>";
		
		AppBean.addMessage(FacesMessage.SEVERITY_ERROR, msg);
	}
	
	public static void alert(ArrayList<String> msgs, String delimiter){
		if (msgs == null || msgs.isEmpty()) return;
		String join = String.join(delimiter, msgs);
		alert(join);
	}
	public static void alert(ArrayList<String> msgs){
		alert(msgs, "<br/><br/>");
	}

	
	public static void showMessage(FacesMessage.Severity severity, String title, String msg, Object... args){
		if (args.length>0)
			msg = String.format(msg, args);
			msg = msg.replaceAll("\n", "<br/>");
		
		AppBean.addMessage(severity, title, msg);
	}



	public static void wsInfo(String message, Object... args){
		User u = User.getCurrentUser();
		if (u!=null) u.push("INFO 0", Utils.strNow(), message, args);
	}

	public static void wsWarn(String message, Object... args){
		User u = User.getCurrentUser();
		if (u!=null) u.push("WARN 1", Utils.strNow(), message, args);
	}

	public static void wsAlert(String message, Object... args){
		User u = User.getCurrentUser();
		if (u!=null) u.push("ERROR 2", Utils.strNow(), message, args);
	}

	public void push(String severity, String title, String message, Object... args){
		String btn = "<div class=\"copyLink msgseverity p0 right\" onclick=\"copyToClipboard( $(this).prev()[0] );\"><button class=\"ui-button p0\">Kopiuj treść</button></div>";
		Msg msg = new Msg();
		msg.setSeverity(""+severity);
		msg.setSessionId(msg.getUsername());
		msg.setUsername(this.getDbLogin());
		msg.setTytul("<i class='material-icons growl'>error_outline</i>" + title);
		msg.setTekst("<div style=\"width:100%; margin:0!important; max-height:300px!important; overflow:auto;\">"+message+"</div>"
				+ btn.replace("msgserverity","error"));
		Push.processEncB64(msg);
	}




	public boolean hasAnyPermission(String... permission){
		for (String r : permission){
			if ( this.getPermissionsEnabled().contains(r)) return true;
		}
		return false;
	}
	
	public  boolean hasUserAnyPermissionToView(String xhtml){
		if (xhtml == null) return false;


		if ("/faces/Administrator/ZmianaUzytkownikaLista.xhtml".equals(xhtml) && this.hasPermission("PP_SU")) return true;

		List<WebUprawnienia> wupr = KontrolaUprawnien.uprawnieniaHM.get(xhtml);
		if (wupr==null || wupr.isEmpty()) {
			return false;
		}
		
		for (WebUprawnienia u : wupr) {
			if (     ("T".equals( u.getWuprCzyWyswietlac()) || "T".equals(u.getWuprCzyEdytowalne()) )
				  && (u.getWuprFunkcjaUzytkowa()==null || hasPermission(u.getWuprFunkcjaUzytkowa()))
				){
					return true;
			}				
		}
		
		return false;
	}
	
	
	public boolean hasPermission(String permission){
		boolean ret = permissions.contains(permission);

		if (permissionsEnalbledHM.get(permission)!=null && !"T".equals(permissionsEnalbledHM.get(permission).enabled ))
				ret = false;
		
		return ret;
	}
	
	public List<String> getPermissionsEnabled() {
		return permissions.stream().filter(f ->  "T".equals(getPermissionsEnalbledHM().get(f).enabled)).collect(Collectors.toList());
	}

	public List<String> getPermissionsDisabled() {
		return permissions.stream().filter(f -> !"T".equals(getPermissionsEnalbledHM().get(f).enabled)).collect(Collectors.toList());
	}

	public List<String> getPermissions() {
		return permissions;
	}
	public void setPermissions(List<String> roles) {
		this.permissions = roles;
	}

	
	public String getTogglePermissionsEnalbedToglle(){
		if (this.getPermissionsEnabled().size() == this.permissions.size())
			return "T";
		else 
			return "N";
	}
	
	public void setTogglePermissionsEnalbedToglle(String toggle){
		if (!(UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()) instanceof SelectBooleanCheckbox))
			return; //bug... przy kaædym kliknieciu w HOME czyszcza sie permissionsEnalbledHM
//		System.out.println("setTogglePermissionsEnalbedTogll - " + toggle );
		for (String fu : this.permissions) {
			this.permissionsEnalbledHM.get(fu).setEnabled(toggle);
		}
	}

	
	
	public int getPrcId() {
		return prcId;
	}


	public void setPrcId(int prcId) {
		this.prcId = prcId;
	}

	public int getFrmId() {
		return frmId;
	}

	public void setFrmId(int frmId) {
		this.frmId = frmId;
	}

	public Long getPrcIdLong() {
		return 0L + prcId;
	}

	public int getDefaultPrcId() {
		return defaultPrcId;
	}

	public void setDefaultPrcId(int defaultPrcId) {
		this.defaultPrcId = defaultPrcId;
	}

	public int getDefaultFrmId() {
		return defaultFrmId;
	}

	public void setDefaultFrmId(int defaultFrmId) {
		this.defaultFrmId = defaultFrmId;
	}

	public String getLogin() {
		return login;
	}



	public void setLogin(String login) {
		this.login = login;
	}



	public String getImie() {
		return imie;
	}



	public void setImie(String imie) {
		this.imie = imie;
	}



	public String getNazwisko() {
		return nazwisko;
	}



	public void setNazwisko(String nazwisko) {
		this.nazwisko = nazwisko;
	}


	public String getDbLogin() {
		return dbLogin;
	}


	public void setDbLogin(String dbLogin) {
		this.dbLogin = dbLogin;
	}




	public DataRow getPrc(){ //krotki alias dla getEkPracownik
		return getEkPracownik();
	}

	public DataRow getEkPracownik() {

		SqlDataSelectionsHandler lw = this.getSql("user_ek_pracownik");
		if (lw==null) {
			this.cacheLoadSqlData("user_ek_pracownik", "select * from  EGADM1.EK_PRACOWNICY where PRC_ID = ?", this.prcId);
			lw = this.getSql("user_ek_pracownik");

			try {
				lw.getDependencyTablesCHM().put("EGADM1.EK_PRACOWNICY", "-");
			} catch (Exception e) {
				log.debug(e.getMessage(), e);
			}
		}

		if (!lw.getData().isEmpty())
			return lw.getData().get(0);
		else {
			DataRow dr = new DataRow(lw);
			dr.add(this.prcId); //dr.getData().set(0, this.prcId);
			return dr;
		}
	}



	
	public String getPracownikLabel() {
		if (pracownikLabel == null) {
			try (Connection conn = JdbcUtils.getConnection())  {
				pracownikLabel = JdbcUtils.sqlFnVarchar(conn, "PPADM.PPF_ZALOGOWANY_ETYKIETA");
			}catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		return pracownikLabel;
	}
	
	public Boolean isWieleKontekstowPracy() {
		int size = getLW("PPL_KONTEKST_PRACY").getData().size();
		return size >= 2;
	}
	
	public void zmienKontekst(DataRow kontekst) {
		if (kontekst == null) {
			return;
		}
		setPrcId(kontekst.getInteger("id_pracownika"));
		setFrmId(kontekst.getInteger("id_firmy"));
		try (Connection conn = JdbcUtils.getConnection())  {
			JdbcUtils.sqlSPCall(conn, "EAP_WEB.ustaw_firme", frmId);
		}catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		this.permissionsEnalbledHM.clear();
		this.setPermissions(User.odczytajFunkcjeUzytkowe(this.getDbLogin()));

		clearSqlDataCache();
		pracownikLabel = null;
		info("Zmieniono kontekst pracy.");
		com.comarch.egeria.Utils.executeScript("window.top.location.reload(true);");
	}
	
	
	
	

//	public void setEkPracownik(DataRow ekPracownik) {
//		this.ekPracownik = ekPracownik;
//	}

//	public String getWartoscDomyslna(String key) {
//		return wartosciDomyslne.get(key);
//	}





	public Long getSzwId() {
		return szwId;
	}





	public void setSzwId(Long szwId) {
		this.szwId = szwId;
	}





	public List<Msg> getMsgs() {
		return msgs;
	}





	public void setMsgs(List<Msg> msgs) {
		this.msgs = msgs;
	}





	public Msg getCurrentMsg() {
		return currentMsg;
	}





	public void setCurrentMsg(Msg currentMsg) {
		this.currentMsg = currentMsg;
	}

	public HashMap<String, PermissionSelHolder> getPermissionsEnalbledHM() {
		if (this.permissionsEnalbledHM.isEmpty()){
			for (String fu : this.permissions) {
				this.permissionsEnalbledHM.put(fu, new PermissionSelHolder(fu));
			}
		}
		return permissionsEnalbledHM;
	}

	public void setPermissionsEnalbledHM(HashMap<String, PermissionSelHolder> permissionsEnalbledHM) {
		this.permissionsEnalbledHM = permissionsEnalbledHM;
	}


	
	
	public String getProfil() {
		if (profil==null){
			try {
				profil = ""+ JdbcUtils.sqlExecScalarQuery(""
						+ "select prf_nazwa from  EAT_DOSTEPY_DO_FIRM "
						+ " join eat_uzytkownicy on ddf_uzt_nazwa=uzt_nazwa "
						+ " join EAADM.eat_profile on ddf_prf_id=prf_id "
						+ "where uzt_nazwa = ? ", this.dbLogin);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		
		return profil;
	}

	public void setProfil(String profil) {
		this.profil = profil;

// TODO - mozna umozliwoc testowanie uprawnien przez wybór profilu z LW		
//		--FUNKJCE WYBRANEGO PROFILU (i PODPROFILI)
//		select distinct 
//		    --prf_prf_id_nad, 
//		    --prf_prf_id_pod, 
//		    prf_nazwa, fu_nazwa
//		from EAT_PROFILE_PROFILE 
//		    join EAADM.eat_profile on prf_prf_id_nad = prf_id or prf_prf_id_pod = prf_id
//		    join EAADM.EAT_UZYCIA_W_PROFILACH on prf_id=uwp_prf_id
//		    join eat_funkcje_uzytkowe on uwp_fu_id = fu_id
//		START WITH prf_prf_id_nad = ? --300784=PPP_PRZELOZONY
//		    CONNECT BY prf_prf_id_nad = prf_prf_id_pod
//		order by fu_nazwa, prf_nazwa		
	}


	
	
	
	public String genWindowNameGUID() {
		UUID uuid = UUID.randomUUID();
		if (getCurrentWindowName()!=null) {
//			System.out.println("SPA = Single Page Application / przeładowanie głównego okna aplikacji " +getCurrentWindowName() + " ----> "  + uuid);
			log.debug("SPA = Single Page Application / przeładowanie głównego okna aplikacji " +getCurrentWindowName() + " ----> "  + uuid);
		}
		setCurrentWindowName(uuid.toString());
		return getCurrentWindowName();
	}
	
	public void setClinetLocationInfo(String href, String protocol, String hostname,  String port, String pathname){
		this.setUrlHref(href);
		this.setUrlProtocol(protocol);
		this.setUrlHostname(hostname);
		this.setUrlPort(port);
		this.setUrlPathname(pathname);
//		System.out.println("href: "+href);
//		System.out.println("protocol: "+protocol);
//		System.out.println("hostname: "+hostname);
//		System.out.println("port: "+port);
//		System.out.println("pathname: "+pathname); // np /PortalPracowniczy/
//		System.out.println("urlHost: "+this.getUrlHost()); // -> http://localhost:8080/
	}
	




	public String getUrlHref() {
		return urlHref;
	}


	public void setUrlHref(String urlHref) {
		this.urlHref = urlHref;
	}





	public String getUrlHostname() {
		return urlHostname;
	}


	public void setUrlHostname(String urlHostname) {
		this.urlHostname = urlHostname;
	}





	public String getUrlPort() {
		return urlPort;
	}


	public void setUrlPort(String urlPort) {
		this.urlPort = urlPort;
	}





	public String getUrlPathname() {
		return urlPathname;
	}


	public void setUrlPathname(String urlPathname) {
		this.urlPathname = urlPathname;
	}





	public String getUrlProtocol() {
		return urlProtocol;
	}

	public void setUrlProtocol(String urlProtocol) {
		this.urlProtocol = urlProtocol;
	}


	public boolean isHttps() {
		return ("https".equals(this.urlProtocol));
	}

	
	public String getUrlHost() {
		String ret = this.urlProtocol + "//"+ urlHostname;
		if (urlPort!=null && !"".equals( urlPort) )
			ret +=":"+urlPort;
		ret +="/";
		return ret;
	}





	public String getCurrentWindowName() {
		return currentWindowName;
	}


	public void setCurrentWindowName(String currentWindowName) {
		this.currentWindowName = currentWindowName;
	}





	public class PermissionSelHolder{
		private String permission = null;
		private String enabled = "T";

		public PermissionSelHolder(String permission) {
			this.setPermission(permission);
		}
		
		public String getEnabled() {
			return enabled;
		}

		public void setEnabled(String enabled) {
			if (!(UIComponent.getCurrentComponent(FacesContext.getCurrentInstance()) instanceof SelectBooleanCheckbox))
				return; //bug... przy kaædym kliknieciu w HOME czyszcza sie permissionsEnalbledHM
						
//			System.out.println("setEnabled: " + enabled +  " / " + this.permission);
			this.enabled = enabled; 
		}

		String pgetPermission() {
			return permission;
		}

		void setPermission(String permission) {
			this.permission = permission;
		} 
		
	}

	public static User getCurrentUser(){
		FacesContext ctx = FacesContext.getCurrentInstance();
		if (ctx==null) return null;

		ExternalContext ectx = ctx.getExternalContext();
		if (ectx==null) return null;


		Map<String, Object> sessionMap = ectx.getSessionMap();
		if (sessionMap==null) return null;

		User user = (User) sessionMap.get("user");
		return user;
	}


	public DataRow getFrm(){
		return getLW("PPL_FIRMA", this.frmId).first();
	}

	public String getFrmOpis(){
		return (String) getLW("PPL_FIRMA", this.frmId).first("frm_opis");
	}

	public String getFrmNazwa(){
		return (String) getLW("PPL_FIRMA", this.frmId).first("frm_nazwa");
	}



	Integer sessionTimeOut = null;
	public int getSessionTimeout(){
		if (sessionTimeOut==null) {
			try {
				sessionTimeOut = Integer.parseInt(konfiguracjaWartoscZewn("PP_SESSION_TIMEOUT"));
			} catch (Exception ex) {
				log.error(ex.getMessage(), ex);
				sessionTimeOut = 30;
			}
		}

        sessionTimeOut = nz(sessionTimeOut);
		return sessionTimeOut;
	}







	public long activityTS = System.currentTimeMillis(); //ts ozn. ostatnia aktyanowsc usera

	public void testActivityTS(){
//		if ( (System.currentTimeMillis() - activityTS) > 15000 ){//	System.out.println("TIMEOUT / WYLOGOWAC WSZYSTKIE OKNA!!!!!!");
		if ( (System.currentTimeMillis() - activityTS) > this.getSessionTimeout() * 60000 ){//	System.out.println("TIMEOUT / WYLOGOWAC WSZYSTKIE OKNA!!!!!!");
			com.comarch.egeria.Utils.executeScript("document.title = 'PP - sesja wygasła'");
			Push.push(this.getDbLogin(), "-","-", getUserSessionGUID() + ".TIMEOUT");//komunikat - wszystkie/pozostałe okna
			this.logout(false);//wylogowanie aktualnego okna
			com.comarch.egeria.Utils.executeScript("window.location.replace('" + this.getUrlHref() + "logout.xhtml?timeout=1');");//aktualne okno (gdby wwbsocket już nie działał).
		}
	}





	public void logout(){
		this.logout(true);
	}

	public void logout(boolean redirectToLogoutPage){
		if (this.login==null) return;

		String sessionId = this.getSessionId();
		if (sessionId==null) return;

		Push.push(this.getDbLogin(), "-","-", getUserSessionGUID() + ".LOGOUT");//komunikat - wszystkie/pozostałe okna w ramach tej samej sesji (o ile okna przegladarki nie sa w uspieniu)

		AppBean.removeFromSessionsUsers(sessionId);//skasowanie sesji z AppBean CHM
		JdbcUtils.czyscKontekst(sessionId);//usuniecie sesji z serwera bazy danych (eat_sesje_zewnetrzne)

		try {
			SecurityUtils.getSubject().logout();
			ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
			externalContext.invalidateSession();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}


		try {
			if (redirectToLogoutPage){
				com.comarch.egeria.Utils.executeScript("if (localStorage.getItem(usGUID) != null){ localStorage.removeItem(usGUID); } " +
						" window.location.replace('" + this.getUrlHref() + "logout.xhtml?lgt=0'); ");
				FacesContext.getCurrentInstance().getExternalContext().redirect(Const.LOGOUT_PAGE_URL);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		this.isLoggedIn = false;
	}





	public void kontynuujEatSesjaZewn(){
		if ( (System.currentTimeMillis() - activityTS) < 60000 ) return;//raz na minute wystarczy

		try (final Connection conn = JdbcUtils.getConnection(true)) {//sztuczne utrzymanie sesji DB
			log.debug("... db ping ...");//nic nie robimy; aby podtrzymac sesje db wystarczy pobranie polaczenia i ustawienie na nim kontekstu
		} catch (Exception e) {
			log.error(e);
			User.alert(e);
		}
	}







	public boolean isLoggedIn = true;




	public static List<String> odczytajFunkcjeUzytkowe(String dbUsername) {
		if (dbUsername == null) {
			dbUsername = "";
		}

		List<String> ret = new ArrayList<>();

		try (Connection conn = JdbcUtils.getConnection(false)) {

			String sql = ""
					+ " SELECT distinct FU.FU_NAZWA "
					+ " FROM eat_uprawnienia upr "
					+ "    join eat_funkcje_uzytkowe fu on fu.FU_ID = upr_fu_id "
					+ "    join eat_aplikacje apl on apl.APL_ID = fu.FU_APL_ID "
					+ " where APL.APL_SYMBOL = ? "
					+ "  and UPR.UPR_UZT_NAZWA = ? "
					+ "  and ( nvl(upr.upr_frm_id, eap_globals.odczytaj_firme) = eap_globals.odczytaj_firme  or  eap_globals.odczytaj_firme is null ) "
					+ " order by FU.FU_NAZWA ";

			CachedRowSet crs = JdbcUtils.sqlExecQuery(conn, sql, "PP", dbUsername.toUpperCase());
			while (crs.next()) {
				ret.add(crs.getString(1));
			}

		} catch (Exception ex) {
			log.error(ex.toString());
		}

		return ret;
	}


}
