package com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;

//import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.web.common.session.EgrUserSessionBean;
import com.comarch.egeria.web.common.view.dao.SqlGlobalneDao;
import com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu.domain.Firma;
import com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu.domain.Oddzial;

@Named
@Scope("session")
public class ZmianaFirmaOddzialBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(ZmianaFirmaOddzialBean.class);

	@Inject
	private EgrUserSessionBean userSessionBean;

	@Inject
	private SqlGlobalneDao sqlGlobalneDao;

	@PreDestroy
	private void predestroy() {
		this.userSessionBean = null;
		this.sqlGlobalneDao = null;
	}
	
	private List<Firma> firmy = new ArrayList<Firma>();

	private Firma selectedFirma;

	private List<Oddzial> oddzialy = new ArrayList<Oddzial>();

	private Oddzial selectedOddzial;

	private boolean showOddzialyDialogOnStart;

	private boolean zatwierdzOddzialDisabled;

	@PostConstruct
	public void init() {
		reoladLists();
		zatwierdzOddzialDisabled = true;

		selectedFirma = userSessionBean.getWybranaFirma();
		selectedOddzial = userSessionBean.getWybranyOddzial();

		updateOddzialyButtonState();
	}

	private void updateOddzialyButtonState() {
		zatwierdzOddzialDisabled = null == selectedOddzial;
	}

	public void zatwierdzWyborFirmy() {
		if (!userSessionBean.getWybranaFirma().equals(selectedFirma)) {
			try {
				sqlGlobalneDao.ustawFirme(selectedFirma);
				userSessionBean.setWybranaFirma(selectedFirma);
				log.debug("Zmieniono firme");
				updateOddzialy();
				// shiroBean.refreshCache();
				com.comarch.egeria.Utils.executeScript("location.reload();");
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
				// TODO
			}
		} else {
			log.debug("Nie zmienono firmy");
		}
	}

	private void updateOddzialy() {
		reloadOddzialy();
		selectedOddzial = null;
		if (oddzialy.size() == 1) {
			try {
				selectedOddzial = oddzialy.get(0);
				sqlGlobalneDao.ustawOddzial(selectedOddzial);
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
				// TODO
			}
		} else if (!oddzialy.isEmpty()) {
			showOddzialyDialogOnStart = true;
		}
		userSessionBean.setWybranyOddzial(selectedOddzial);
		updateOddzialyButtonState();
	}

	public void zatwierdzWyborOddzialu() {
		showOddzialyDialogOnStart = false;
		if (null != selectedOddzial && !selectedOddzial.equals(userSessionBean.getWybranyOddzial())) {
			try {
				sqlGlobalneDao.ustawOddzial(selectedOddzial);
				userSessionBean.setWybranyOddzial(selectedOddzial);
				log.debug("Zmieniono oddzial");
			} catch (SQLException e) {
				log.error(e.getMessage(), e);
				// TODO
			}
		} else
			log.debug("Nie zmieniono oddzialu");
	}

	public void anulujWyborOddzialu() {
		selectedOddzial = userSessionBean.getWybranyOddzial();
	}

	public void oddzialyRowSelect(SelectEvent event) {
		updateOddzialyButtonState();
	}

	private void reoladLists() {
		reoladFirmy();
		reloadOddzialy();
	}

	private void reloadOddzialy() {
		try {
			oddzialy = sqlGlobalneDao.sqlOddzialyUzytkownika();
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			// TODO
		}
	}

	private void reoladFirmy() {
		try {
			firmy = sqlGlobalneDao.sqlFirmyUzytkownika();
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			// TODO
		}
	}

	public int getFirmySize() {
		return firmy.size();
	}

	public int getOddzialySize() {
		return oddzialy.size();
	}

	public List<Firma> getFirmy() {
		return firmy;
	}

	public void setFirmy(List<Firma> firmy) {
		this.firmy = firmy;
	}

	public Firma getSelectedFirma() {
		return selectedFirma;
	}

	public void setSelectedFirma(Firma selectedFirma) {
		this.selectedFirma = selectedFirma;
	}

	public List<Oddzial> getOddzialy() {
		return oddzialy;
	}

	public void setOddzialy(List<Oddzial> oddzialy) {
		this.oddzialy = oddzialy;
	}

	public Oddzial getSelectedOddzial() {
		return selectedOddzial;
	}

	public void setSelectedOddzial(Oddzial selectedOddzial) {
		this.selectedOddzial = selectedOddzial;
	}

	public boolean isZatwierdzDisabled() {
		return zatwierdzOddzialDisabled;
	}

	public void setZatwierdzDisabled(boolean zatwierdzDisabled) {
		this.zatwierdzOddzialDisabled = zatwierdzDisabled;
	}

	public boolean isFirmaDialogDisabled() {
		return getFirmySize() <= 1;
	}

	public boolean isOddzialDialogDisabled() {
		return getOddzialySize() <= 1;
	}

	public boolean isShowOddzialyDialogOnStart() {
		return showOddzialyDialogOnStart;
	}

	public void setShowOddzialyDialogOnStart(boolean showOddzialyDialogOnStart) {
		this.showOddzialyDialogOnStart = showOddzialyDialogOnStart;
	}
}
