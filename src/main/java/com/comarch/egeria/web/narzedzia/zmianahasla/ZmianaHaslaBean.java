package com.comarch.egeria.web.narzedzia.zmianahasla;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

@Named
@Scope("view")
public class ZmianaHaslaBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LogManager.getLogger(ZmianaHaslaBean.class);

	private String stareHaslo;

	private String noweHaslo;

	private String potwNoweHaslo;

	@PostConstruct
	private void init() {
	}

	/*
	 *
	 * Funkcje publiczne
	 *
	 */
	public void zmienHaslo() {
		log.debug("Zmiana hasła");
	}
	/*
	 *
	 * Getters & Setters
	 *
	 */

	public String getStareHaslo() {
		return stareHaslo;
	}

	public void setStareHaslo(String stareHaslo) {
		this.stareHaslo = stareHaslo;
	}

	public String getNoweHaslo() {
		return noweHaslo;
	}

	public void setNoweHaslo(String noweHaslo) {
		this.noweHaslo = noweHaslo;
	}

	public String getPotwNoweHaslo() {
		return potwNoweHaslo;
	}

	public void setPotwNoweHaslo(String potwNoweHaslo) {
		this.potwNoweHaslo = potwNoweHaslo;
	}

}
