package com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu.domain;

public class Firma {
	Long id;
	String nazwa;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	
	@Override
	public String toString() {
		return "Firma [id=" + id + ", nazwa=" + nazwa + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		if (id == null)
			return false;
		Firma compFirma = (Firma) obj;
		return id.equals(compFirma.getId());
	}
}
