package com.comarch.egeria.web.narzedzia.zmianafirmyoddzialu.domain;

public class Oddzial {
	Long id;
	String nazwa;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	
	@Override
	public String toString() {
		return "Oddzial [id=" + id + ", nazwa=" + nazwa + "]";
	}
}
