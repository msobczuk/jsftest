package com.comarch.egeria;

import com.comarch.egeria.web.User;
import org.apache.poi.ooxml.POIXMLDocumentPart;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMarkupRange;
import org.w3c.dom.Node;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.eq;

/**
 * Utilities - metody statyczne/narzędziowe;
 * <p>
 * W klasach korzystających najlepiej stosować
 * import static com.comarch.egeria.Utils;
 * <p>
 * W klasach wewn. pakietu com.comarch.egeria.pp wystarczy wywołanie Utils.<metoda....>.
 * W wybranych klasach bazowych i kontrolerach (np. SqlBean) można reimplementować sygnatury n/w metod jako niestatyczne
 */
public class ApachePOIUtils {

    public static void testApachePOIInstrukcjaPPSplitBodyElements(Path pthInstrukcjaUzytkownika,  String bookmarkName) throws IOException {
        //POC - wyciąganie ze wskazanego dokuemntu DOCX częsci oznaczonej zakładką (np. o nazwie "obiegi")
        //docelowo sposób generowania pomocy kontekstowej, gdzie nazwa zakładki to jednoczesnie mapowanie okreslonego pliku xhtml

        //System.out.println("testApachePOIInstrukcjaPPSplitBodyElements: " + "pthInstrukcjaUzytkownika: " + pthInstrukcjaUzytkownika + " ,bookmarkName: " +bookmarkName+"...");

        if (bookmarkName==null || bookmarkName.isEmpty()) return;
//		bookmarkName = "obiegi";

        Path pthPlikPomocKontekstowa = Paths.get(pthInstrukcjaUzytkownika.getParent().toString(), bookmarkName + ".docx");
        //System.out.println("pthPlikPomocKontekstowa: " + pthPlikPomocKontekstowa.toString());



        XWPFDocument doc = null;
        CTBookmark bookmarkStart = null;
        Object bookmarkEnd = null;
        IBodyElement firstBookmarkedBe = null;
        IBodyElement lastBookmarkedBe = null;
        List<IBodyElement> bookmarkedBodyElements = new ArrayList<>();


        List<Integer> bodyElementsPositiosToRemove = new ArrayList<>();

        try {
            //System.out.println(61);
            doc = new XWPFDocument(new FileInputStream(pthInstrukcjaUzytkownika.toFile()));

            List<IBodyElement> bodyElemetsToSave = new ArrayList<>();
            List<IBodyElement> bodyElementsToRemove = new ArrayList<>();

            //////////////////////////////////////wyznaczanie pozycji oraz selekcja el.:////////////////////////////////
            int pos = 0;
            for (IBodyElement be : doc.getBodyElements()) {

                if (be instanceof XWPFParagraph) {
                    XWPFParagraph p = (XWPFParagraph) be;
                    if (bookmarkStart==null) bookmarkStart = findBookamrkStart(p, bookmarkName);
                    if (bookmarkEnd==null) bookmarkEnd = findBookamrkEnd(p, bookmarkStart);
                } else if (be instanceof XWPFTable) {
                    XWPFTable tb = (XWPFTable) be;
                    if (bookmarkStart==null) bookmarkStart = findBookamrkStart(tb, bookmarkName);
                    if (bookmarkEnd==null) bookmarkEnd = findBookamrkEnd(tb, bookmarkStart);
                }


                if (firstBookmarkedBe==null && bookmarkStart!=null) firstBookmarkedBe = be;

                if (firstBookmarkedBe!=null && lastBookmarkedBe==null){
                    bodyElemetsToSave.add(be);
                    bookmarkedBodyElements.add(be);
                } else {
                    bodyElementsToRemove.add(be);
                    bodyElementsPositiosToRemove.add(0,pos);
                }

                if (lastBookmarkedBe==null && bookmarkEnd!=null) lastBookmarkedBe = be;

                pos+=1;
            }
            //System.out.println(95 + "" + firstBookmarkedBe + ", " + lastBookmarkedBe + ", " + pos);
            if (lastBookmarkedBe==null) return;


            //////////////////////////////////////kasowanie zbednych czesci dokuemntu://////////////////////////////////
            for (Integer i : bodyElementsPositiosToRemove) {
                doc.removeBodyElement(i);
            }


            //System.out.println(105);
            //////////////////////////////////////czyszczenie poczatkowego i koncowego paragrafu:///////////////////////
            if (firstBookmarkedBe instanceof XWPFParagraph) {
                final XWPFParagraph par = (XWPFParagraph) firstBookmarkedBe;
                removeAllRunsBeforeBookmark(par, bookmarkStart);
                if (par.getRuns().isEmpty()){
                    doc.removeBodyElement(doc.getPosOfParagraph(par));
                }
            }
            //System.out.println(114);
            if (lastBookmarkedBe instanceof XWPFParagraph) {
                final XWPFParagraph par = (XWPFParagraph) lastBookmarkedBe;

                if (bookmarkEnd instanceof CTMarkupRange)
                    removeAllRunsAfterBookmark(par, (CTMarkupRange) bookmarkEnd);

                if (par.getRuns().isEmpty()){
                    doc.removeBodyElement(doc.getPosOfParagraph(par));
                }
            }

            //System.out.println(123);
            //////////////////////////////////////czyszczenie naglowkow i stopek:///////////////////////////////////////
            clearDocxHeadersAndFooters(doc);


            //////////////////////////////////////kasowanie spisu tresci itp....:///////////////////////////////////////
            doc.getDocument().getBody().getSdtList().clear();  //ew. doc.enforceUpdateFields();->aktualizacja spisu treści (i formul)


            //System.out.println(132);


            ////////////////////////////////zbieranie info o obrazkach, ktore sa uzywane:///////////////////////////////
            List<String> usedPicturesNames = new ArrayList<>();
            List<String> usedPicturesEmbedRelationsIds = new ArrayList<>();
            List<PackagePart> usedPuctersPackageParts = new ArrayList<>();

            for (XWPFParagraph p : doc.getParagraphs()) {
                for (XWPFRun run : p.getRuns()) {
                    final List<XWPFPicture> embeddedPictures = run.getEmbeddedPictures();
                    for (XWPFPicture pic : embeddedPictures) {

                        final String embed = pic.getCTPicture().getBlipFill().getBlip().getEmbed(); //np. rId60 - id relationship
                        usedPicturesEmbedRelationsIds.add(embed);

                        final PackagePart packagePart = doc.getRelationById(embed).getPackagePart();
                        usedPuctersPackageParts.add(packagePart);

                        final String fileName = pic.getPictureData().getFileName();
                        usedPicturesNames.add(fileName);
                    }
                }
            }

            //System.out.println(157);
            //////////////////////////////// kasowanie obrazków, które nie są używane://////////////////////////////////
            final List<PackagePart> parts = doc.getPackage().getParts().stream().collect(Collectors.toList());
            for (PackagePart part : parts) {
                final String contentType = part.getContentType();
                if (contentType.startsWith("image") && !usedPuctersPackageParts.contains(part)){
                    if (!part.toString().contains("image1.jpeg")) {//z jakiegos powodu image1.jpeg jest potrzebny (ale jest bardzo mały...)
                        doc.getPackage().removePart(part);
                    }
                }
            }
            //System.out.println(168);


            ///////////////////////////////zapis do pliku//////////////////////////////////////////
            final FileOutputStream os = new FileOutputStream(pthPlikPomocKontekstowa.toString());
            doc.write(os);
            os.flush();
            os.close();

            //System.out.println("zapisano plik: " + pthPlikPomocKontekstowa.toString());

        } catch (Exception e) {
            User.alert(e);
            //System.out.println("bląd : "  + e.getMessage());
        } finally {
            //System.out.println("Finally");
            if (doc != null) doc.close();
        }
    }


    public static CTBookmark findBookamrkStart(XWPFParagraph paragraph, String bookmarkName) {
        for (CTBookmark ctBookmark : paragraph.getCTP().getBookmarkStartList()) {
            if (eq(bookmarkName,ctBookmark.getName())){
                return ctBookmark;
            }
        }
        return null;
    }

    public static CTBookmark findBookamrkStart(XWPFTable table, String bookmarkName) {
        for (CTBookmark ctBookmark : table.getCTTbl().getBookmarkStartList()) {
            if (eq(bookmarkName,ctBookmark.getName())){
                return ctBookmark;
            }
        }
        return null;
    }

    public static Object findBookamrkEnd(XWPFParagraph paragraph, CTBookmark bookmark) {
        if (bookmark==null || paragraph==null) return null;

        for (CTMarkupRange x : paragraph.getCTP().getBookmarkEndList()) {
            if ( eq(bookmark.getId().intValue(), x.getId().intValue()) ){
                return x;
            }
        }

        //czy koniec zakładki nie jest miedzy tabelą a nastepnym be np.: [...</w:tbl><w:bookmarkEnd w:id="3"/><w:bookmarkEnd w:id="4"/><w:p ...]
        final String id = "" + bookmark.getId().intValue();
        Node nn = paragraph.getCTP().getDomNode().getNextSibling();
        while (nn!=null && "w:bookmarkEnd".equals(nn.getNodeName())
        ){
            if (id.equals(nn.getAttributes().getNamedItem("w:id").getNodeValue())) {
                return nn;
            }
        }

        return null;
    }

    public static Object findBookamrkEnd(XWPFTable paragraph, CTBookmark bookmark) {
        if (bookmark==null || paragraph==null) return null;

        for (CTMarkupRange x : paragraph.getCTTbl().getBookmarkEndList()) {
            if ( eq(bookmark.getId().intValue(), x.getId().intValue()) ){
                return x;
            }
        }

        //czy koniec zakładki nie jest miedzy paragrafami/tabelkami np.: [...</w:p><w:bookmarkEnd w:id="3"/><w:bookmarkEnd w:id="4"/><w:p w14:paraId="...]
        final String id = "" + bookmark.getId().intValue();
        Node nn = paragraph.getCTTbl().getDomNode().getNextSibling();
        while (nn!=null && "w:bookmarkEnd".equals(nn.getNodeName())
        ){
            if (id.equals(nn.getAttributes().getNamedItem("w:id").getNodeValue())) {
                return nn;
            }
        }

        return null;
    }

    public static void clearDocxHeadersAndFooters(XWPFDocument doc) {

        Set<PackagePart> packagePartsToRemove = new HashSet<>();

        for (XWPFHeader header : doc.getHeaderList()) {
            for (POIXMLDocumentPart.RelationPart r : header.getRelationParts()) {
                final PackagePart packagePart = r.getDocumentPart().getPackagePart();//kasowanie obrazków
                packagePartsToRemove.add(packagePart);
            }
            header.clearHeaderFooter();
        }

        for (XWPFFooter footer : doc.getFooterList()) {
            for (POIXMLDocumentPart.RelationPart r : footer.getRelationParts()) {
                final PackagePart packagePart = r.getDocumentPart().getPackagePart();//kasowanie obrazków
                packagePartsToRemove.add(packagePart);
            }
            footer.clearHeaderFooter();
        }

        for (PackagePart packagePart : packagePartsToRemove) {
            doc.getPackage().removePart(packagePart); //kasowanie obrazków używanych w nagłówkach i stopkach
        }


    }


    public static void removeAllRunsBeforeBookmark(XWPFParagraph p, CTBookmark bookmarkStart) {
        List<Node> nodesToRemove = new ArrayList<>();

        Node pn = bookmarkStart.getDomNode();
        while (pn!=null){
            nodesToRemove.add(pn);
            pn = pn.getPreviousSibling();
        }

        final List<XWPFRun> runs = p.getRuns().stream().collect(Collectors.toList());
        for (XWPFRun run : runs) {
            final Node n = run.getCTR().getDomNode();
            if (nodesToRemove.contains(n)) {
                p.removeRun(p.getRuns().indexOf(run));
            }
        }
    }

    public static void removeAllRunsAfterBookmark(XWPFParagraph p, CTMarkupRange bookmarkEnd) {
        List<Node> nodesToRemove = new ArrayList<>();

        Node nn = bookmarkEnd.getDomNode();
        while (nn!=null){
            nodesToRemove.add(nn);
            nn = nn.getNextSibling();
        }

        final List<XWPFRun> runs = p.getRuns().stream().collect(Collectors.toList());
        for (XWPFRun run : runs) {
            final Node n = run.getCTR().getDomNode();
            if (nodesToRemove.contains(n)) {
                p.removeRun(p.getRuns().indexOf(run));
            }
        }
    }


}
