package com.comarch.egeria.pp.Bhp;

import com.comarch.egeria.pp.Bhp.model.EkSzkolenia;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import java.math.BigDecimal;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.comarch.egeria.Utils.plusDays;

//import org.primefaces.context.RequestContext;

@ManagedBean
@Named
@Scope("view")
public class SzkoleniaBhpBean extends SqlBean {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	private EkSzkolenia szkolenieBhp = new EkSzkolenia();
	private Long selectedSzkolenieBhpId = null;
	private Long selectedPrcId = -1L;
	
	private DataRow pracownik;
	private String checkString = "";
//	private Map<String,String> nazwySzkolenBhp = new HashMap<String, String>();

	@PostConstruct
	public void init() {
		this.szkolenieBhp.setSqlBean(this);
		
//		nazwySzkolenBhp = new HashMap<String, String>();
//		
//		nazwySzkolenBhp.put("Szkolenie wstępne BHP (bez ważnego szkolenia BHP)",
//				"Szkolenie wstępne BHP (bez ważnego szkolenia BHP)");
//		nazwySzkolenBhp.put("Szkolenie BHP okresowe/wstępne (ważne szkolenie BHP z poprzedniego zatrudnienia)",
//				"Szkolenie BHP okresowe/wstępne (ważne szkolenie BHP z poprzedniego zatrudnienia)");
	
	}
	
	public void dodajSzkolenieBhp() {
		this.szkolenieBhp = new EkSzkolenia();
		this.szkolenieBhp.setSqlBean(this);
		this.getLW("ppl_eg_rodzaj_szkol_bhp").setCurrentRow(null);
	}
	
	public void edytujSzkolenieBhp(Long szkId) {		
		this.szkolenieBhp = (EkSzkolenia) HibernateContext.get(EkSzkolenia.class, szkId);
		this.szkolenieBhp.setSqlBean(this);
		DataRow r = this.getLW("ppl_eg_rodzaj_szkol_bhp").find(szkolenieBhp.getSzkRodzaj());
		this.getLW("ppl_eg_rodzaj_szkol_bhp").setCurrentRow(r);
		
		checkString = getCheckString();
	}
	
	public void zapisz() {
		if (!validateBeforeSave())
			return;
		
		try {
			pobierzKwalifikacjePrc();
			
			this.szkolenieBhp = (EkSzkolenia) HibernateContext.save(this.szkolenieBhp);
			this.szkolenieBhp.setSqlBean(this);
			
			if (szkolenieBhp.getSzkDataWaznosci()!=null){
				wstawKomunikat();
			}
			
			User.info("Zapisano szkolenie BHP [#%1$s]", this.szkolenieBhp.getSzkId());
			reloadAndUpdate("ppl_szk_bhp_pracownika", szkolenieBhp.getSzkId() );
			
//			RequestContext context = RequestContext.getCurrentInstance();
//			context.execute("PF('dlgBhpZapis').hide();");
			com.comarch.egeria.Utils.executeScript("PF('dlgBhpZapis').hide();");
			
		} catch (Exception e) {
			User.alert("Błąd w zapisie szkolenia BHP!\n" + e);
			log.error(e.getMessage(), e);
		}
	}

	private void wstawKomunikat() {
		if(szkolenieBhp.getSzkDataWaznosci() == null || pracownik == null)
			return;
		
		String komunikat = String.format(
				"Termin ważności szkolenia. Pracownikowi o numerze \"%s\" (%s) "
						+ "zbliża się termin wygaśnięcia ważności szkolenia %s, które odbywało się w terminie %s-%s. "
						+ "Termin upływa dnia %s.",
				"" + pracownik.get("prc_numer"), pracownik.get("pp_pracownik"), this.szkolenieBhp.getSzkNazwa(),
				formatDate(this.szkolenieBhp.getSzkDataOd()), formatDate(this.szkolenieBhp.getSzkDataDo()),
				"" + this.szkolenieBhp.getSzkDataWaznosci());

		try (Connection conn = JdbcUtils.getConnection()) {
			JdbcUtils.sqlSPCall(conn, "wstaw_wiadomosc", pracownik.get("prc_id"),
					komunikat, plusDays(this.szkolenieBhp.getSzkDataWaznosci(), -1));
			JdbcUtils.sqlSPCall(conn, "wstaw_wiadomosc", pracownik.get("prc_id"),
					komunikat, plusDays(this.szkolenieBhp.getSzkDataWaznosci(), -5));
		} catch (Exception e) {
			User.alert("Nieudane wstawienie komunikatu ważności szkolenia.");
			log.error(e.getMessage(), e);
			return;
		}		
	}
	
	public void usun(DataRow row) {
		this.szkolenieBhp = (EkSzkolenia) HibernateContext.get(EkSzkolenia.class, ((BigDecimal) row.get(0)).longValue());
		this.szkolenieBhp.setSqlBean(this);
		
		try {
			HibernateContext.delete(szkolenieBhp);
			User.info("Usunięto szkolenie BHP");
			reloadAndUpdate("ppl_szk_bhp_pracownika", row.getIdAsLong());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.warn("Błąd w usuwaniu szkolenie BHP.");
		}
	}
	
	private void pobierzKwalifikacjePrc() {
		try (Connection conn = JdbcUtils.getConnection()) {
			this.szkolenieBhp.setSzkKwaId(JdbcUtils.sqlFnLong(conn, "PPP_UTILITIES.pobierz_kwalifikacje", getSelectedPrcId()));
			
		} catch (Exception e) {
			User.alert("Błąd w pobieraniu kwalifikacji pracownika");
			log.error(e.getMessage(), e);
			return;
		}
	}
	
	private boolean validateBeforeSave() {
		ArrayList<String> inv = new ArrayList<String>();
		
		String currentCheckString = this.getCheckString();

		if (this.checkString.equals(currentCheckString))
			inv.add("Brak zmian do zapisania.");

		if (this.szkolenieBhp.getSzkRodzaj() == null)
			inv.add("Wybierz rodzaj szkolenia.");
		
//		if (this.szkolenieBhp.getSzkNazwa() == null || this.szkolenieBhp.getSzkNazwa() == "")
//			inv.add("Wybierz nazwę szkolenia.");
		
		if (this.szkolenieBhp.getSzkDataOd() == null)
			inv.add("Data rozpoczęcia szkolenia jest polem wymaganym.");

		if (this.szkolenieBhp.getSzkDataDo() == null)
			inv.add("Data zakończenia szkolenia jest polem wymaganym.");

		if (this.szkolenieBhp.getSzkDataOd() != null && this.szkolenieBhp.getSzkDataDo() != null
				&& this.szkolenieBhp.getSzkDataOd().after(this.szkolenieBhp.getSzkDataDo()))
			inv.add("Data zakończenia szkolenia nie może poprzedzać daty rozpoczęcia.");

		for (String msg : inv) {
			User.alert(msg);
		}
		
		return inv.isEmpty();
	}
	
	public String getCheckString() {
		String ret = "";

		if (this.szkolenieBhp != null) {
			ret = ret + AppBean.concatValues(this.szkolenieBhp.getSzkRodzaj(), this.szkolenieBhp.getSzkNazwa(),
					this.szkolenieBhp.getSzkDataOd(), this.szkolenieBhp.getSzkDataDo(), this.szkolenieBhp.getSzkDataWaznosci());
		}

		return ret;
	}

	private String formatDate(Date data) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yy/MM/dd");
		return dateFormat.format(data);
	}
	
	
	public Long getSelectedSzkolenieBhpId() {
		return selectedSzkolenieBhpId;
	}

	public void setSelectedSzkolenieBhpId(Long selectedSzkolenieBhpId) {
		this.selectedSzkolenieBhpId = selectedSzkolenieBhpId;
	}

	public EkSzkolenia getSzkolenieBhp() {
		return szkolenieBhp;
	}

	public void setSzkolenieBhp(EkSzkolenia szkolenieBhp) {
		this.szkolenieBhp = szkolenieBhp;
		this.szkolenieBhp.setSqlBean(this);
	}
	
	public void setSzkolenieBhp() {
		this.szkolenieBhp = (EkSzkolenia) HibernateContext.get(EkSzkolenia.class, selectedSzkolenieBhpId);
		this.szkolenieBhp.setSqlBean(this);
	}

	public DataRow getPracownik() {
		return pracownik;
	}

	public void setPracownik(DataRow pracownik) {
		this.pracownik = pracownik;
	}

	public Long getSelectedPrcId() {
		return selectedPrcId;
	}

	public void setSelectedPrcId(Long selectedPrcId) {
		this.selectedPrcId = selectedPrcId;
		this.pracownik = this.getLW("pp_prc_bhp").find(selectedPrcId);
	}

//	public Map<String,String> getNazwySzkolenBhp() {
//		return nazwySzkolenBhp;
//	}
//
//	public void setNazwySzkolenBhp(Map<String,String> nazwySzkolenBhp) {
//		this.nazwySzkolenBhp = nazwySzkolenBhp;
//	}

}
