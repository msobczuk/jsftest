package com.comarch.egeria.pp.Bhp.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.data.model.ModelBase;



/**
 * The persistent class for the EK_SZKOLENIA database table.
 * 
 */
@Entity
@Table(name = "EK_SZKOLENIA")
@NamedQuery(name = "EkSzkolenia.findAll", query = "SELECT p FROM EkSzkolenia p")
public class EkSzkolenia extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name = "ek_seq_szk", sequenceName = "ek_seq_szk", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="ek_seq_szk")
	@Column(name = "SZK_ID")
	private Long szkId;

	@Column(name = "SZK_KWA_ID")
	private Long szkKwaId;

	@Column(name = "SZK_NAZWA")
	private String szkNazwa;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "SZK_DATA_OD")
	private Date szkDataOd;

	@Temporal(TemporalType.DATE)
	@Column(name = "SZK_DATA_DO")
	private Date szkDataDo;

	@Temporal(TemporalType.DATE)
	@Column(name = "SZK_DATA_WAZNOSCI")
	private Date szkDataWaznosci;

	@Column(name = "SZK_RODZAJ")
	private Long szkRodzaj;
	

	public EkSzkolenia() {
	}

	public Long getSzkId() {
		return this.szkId;
	}

	public void setSzkId(Long szkId) {
		this.szkId = szkId;
	}

	public Long getSzkKwaId() {
		return this.szkKwaId;
	}

	public void setSzkKwaId(Long szkKwaId) {
		this.szkKwaId = szkKwaId;
	}

	public Date getSzkDataOd() {
		return this.szkDataOd;
	}

	public void setSzkDataOd(Date szkDataOd) {
		this.szkDataOd = szkDataOd;
		
//		if (this.getSzkDataDo() == null || (this.getSzkDataDo() != null && this.getSzkDataDo().before(szkDataOd)))
		if (szkDataOd != null && (this.getSzkDataDo() == null || this.getSzkDataDo().before(szkDataOd)))
			setSzkDataDo(szkDataOd);
	}
	
	public Date getSzkDataDo() {
		return this.szkDataDo;
	}

	public void setSzkDataDo(Date szkDataDo) {
		if (!eq(this.szkDataDo, szkDataDo)){
			setDataWaznosciByRodzaj(szkDataDo);
		}
		
		this.szkDataDo = szkDataDo;
		
//		if (this.getSzkDataOd() == null || (this.getSzkDataOd() != null && this.getSzkDataOd().after(szkDataDo)))
		if (szkDataDo != null && (this.getSzkDataOd() == null || this.getSzkDataOd().after(szkDataDo)))
			setSzkDataOd(szkDataDo);
	}

	public Date getSzkDataWaznosci() {
		return szkDataWaznosci;
	}

	public void setSzkDataWaznosci(Date szkDataWaznosci) {
		this.szkDataWaznosci = szkDataWaznosci;
	}
	
	public String getSzkNazwa() {
		return this.szkNazwa;
	}

	public void setSzkNazwa(String szkNazwa) {
		this.szkNazwa = szkNazwa;
	}

	public Long getSzkRodzaj() {
		return this.szkRodzaj;
	}

	public void setSzkRodzaj(Long szkRodzaj) {
		if (!eq(this.szkRodzaj, szkRodzaj)){
			setNazwaByRodzaj(szkRodzaj);
		}
		this.szkRodzaj = szkRodzaj;
	}

	public void setNazwaByRodzaj(Long szkRodzaj) {
		SqlDataSelectionsHandler lw = getLW("ppl_eg_rodzaj_szkol_bhp");
		if (lw==null)
			return;
		
		DataRow r = lw.find(szkRodzaj);
		if (r == null)
			return;
		
		this.szkNazwa = ""+ r.get("wsl_opis");
	}
	
	public void setDataWaznosciByRodzaj(Date szkDataDo) {
		SqlDataSelectionsHandler lw = getLW("ppl_eg_rodzaj_szkol_bhp");
		if (lw==null)
			return;
		
		DataRow r = lw.find(this.szkRodzaj);
		if (r == null)
			return;
		
		Date dataWaznosci = null;
		
		switch (r.get("wsl_wartosc").toString()) {
		case "13": case "19": case "49":
			dataWaznosci = addYears(szkDataDo, 5);
			dataWaznosci = addDays(dataWaznosci, -1);
			this.szkDataWaznosci = dataWaznosci;
			break;
		case "43":
			dataWaznosci = addYears(szkDataDo, 3);
			dataWaznosci = addDays(dataWaznosci, -1);
			this.szkDataWaznosci = dataWaznosci;
			break;
		case "42":
			dataWaznosci = addYears(szkDataDo, 6);
			dataWaznosci = addDays(dataWaznosci, -1);
			this.szkDataWaznosci = dataWaznosci;
			break;
		default:
			break;
		}
	}
	
}