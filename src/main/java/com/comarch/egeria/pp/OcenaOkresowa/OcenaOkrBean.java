package com.comarch.egeria.pp.OcenaOkresowa;

import static com.comarch.egeria.Utils.*;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.OcenaOkresowa.model.ZptOcKryteria;
import com.comarch.egeria.pp.OcenaOkresowa.model.ZptOceny;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean // JBossTools
@Named
@Scope("view")
public class OcenaOkrBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	@Inject
	User user;

	@Inject
	ObiegBean obieg;

	@Inject
	SessionBean sessionBean;

	private Long selectedOecenaId = null;
	private ZptOceny ocena = null;// uwaga dla ocena.kryteria przestawiono w

	private double sredniaWymagana = 0;
	private Boolean czyZalaczonoWniosek = false;

	private Object wybranaCzynnosc;

	String referer = "/index.xhtml";

	String checkCzI = "";
	String checkCzII = "";

	
	private String stanObiegu4Update;
	
	
	private SqlDataSelectionsHandler pracownicyPodlegliWybranemu = new SqlDataSelectionsHandler();
	
	private String aktualnyStopienSluzbowy;

	@PostConstruct
	public void init() throws IOException {
		referer = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("referer"); // http://localhost:8080/PortalPracowniczy/ListaZaplanowanychOcen

		obieg.setKategoriaObiegu("WN_OCENA_OKRES");

		Object id =
		FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		if (id == null || "".equals(id)) {
			this.setOcena(new ZptOceny());
			this.ocena.setSqlBean(this);
			this.ocena.setZptOcKryterias(new ArrayList<>());
			this.ocena.setOcePrcIdOceniajacy(user.getPrcIdLong());
			
		} else {
			wczytajOcene(id);
		}
		
		//jest to aktualnie wyliczane w obiegu
		//wyliczOkresOdOkresDo();
	}

	public void raportPdf1() {
		if (ocena != null) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("id_oceny", ocena.getOceId());
				if ("KSC".equals(ocena.getOceTypOceny()))
				{
					ReportGenerator.displayReportAsPDF(params, "Ocena_okresowa_1_kier");
				}
				else
				{
				ReportGenerator.displayReportAsPDF(params, "Ocena_okresowa_1");
				}

			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
	}
	
	public void raportPdf2() {
		if (ocena != null) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("id_oceny", ocena.getOceId());
				if ("KSC".equals(ocena.getOceTypOceny()))
				{
					ReportGenerator.displayReportAsPDF(params, "Ocena_okresowa_2_kier");
				}
				else
				{
				ReportGenerator.displayReportAsPDF(params, "Ocena_okresowa_2");
				}

			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
	}
	
	public void raportPdf3() {
		if (ocena != null) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("id_oceny", ocena.getOceId());
				
					ReportGenerator.displayReportAsPDF(params, "Ocena_okresowa_3");
				

			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
	}

	public void zapisz(boolean addSelectedKryteria) {// na formularzu OcenaOkresowaZaplanuj możemy wybrać kryteria dodatkowe
		long id = ocena.getOceId();

		SqlDataSelectionsHandler sqlKryteriaObowiazkowe = this.getLW("PPL_OCE_KRYT_OBOWIAZ", this.ocena.getOceTypOceny(), this.ocena.getOceTypOceny());
		if (sqlKryteriaObowiazkowe != null)// istotne tylko przy planowaniu
											// oceny
			dodajUsunKryteria("O", sqlKryteriaObowiazkowe.getData());

		SqlDataSelectionsHandler kryteraDodatkoweAllRows = this.getLW("PPL_OCE_KRYT_DODATKOWE", this.ocena.getOceTypOceny(), this.ocena.getOceTypOceny());
		if (addSelectedKryteria && kryteraDodatkoweAllRows != null) {
			List<DataRow> selectedRows = kryteraDodatkoweAllRows.getSelectedRows();
			dodajUsunKryteria("D", selectedRows);
		}

		if (!validateBeforeSaveOcena(addSelectedKryteria))
			return;

		ocena.setOceAudytLm(ocena.getOceAudytLm() != null ? ocena.getOceAudytLm() + 1 : 1);

		try {
			ocena = (ZptOceny) HibernateContext.save(ocena);
			User.info("Zapisano planowaną ocenę");
		} catch (Exception e) {
			log.debug(e);
			return;
		}

		if (id == 0)
			sessionBean.setViewEntityId(ocena.getOceId());// obsluga odsw.. F5

		wczytajOcene(ocena.getOceId());

	}

	public void usun()
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, IOException {

		long id = this.ocena.getOceId();
		if (id == 0L) {
			User.alert("Ta ocena nie została jeszcze zapisana w bazie.");
			return;
		}


		try (Connection conn = JdbcUtils.getConnection(false)) {
			obieg.usunDaneObiegu(); // reloadPage
			JdbcUtils.sqlExecNoQuery(conn, " delete from  EGADM1.ZKT_ZALACZNIKI ZAL where ZAL.ZAL_OCE_ID = ? ", id);
		}

		try{
			HibernateContext.delete(ocena);
		} catch (Exception e) {
			System.err.println(e);
			User.alert(e.getMessage());
			return;
		}

		FacesContext.getCurrentInstance().getExternalContext().redirect(referer);
	}


	private void wczytajOcene(Object id) {

		setSelectedOecenaId(Long.parseLong("" + id));

		obieg.setIdEncji(this.getSelectedOecenaId());

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			this.setOcena(s.get(ZptOceny.class, getSelectedOecenaId()));
			this.ocena.setSqlBean(this);

			if (this.ocena.getOceCzyWnKolStop() != null && this.ocena.getOceCzyWnKolStop().equals("T")) {
				czyZalaczonoWniosek = true;
			}
			
			SqlDataSelectionsHandler sq = this.getLW("PPL_AKTUALNY_ST_SLUZBOWY", this.ocena.getOcePrcId());
			if (!sq.getData().isEmpty())
				this.aktualnyStopienSluzbowy = (String) sq.getData().get(0).get("akt_stopien_sluzbowy");
		}
		checkCzI = getCheckString_CzI();
		checkCzII = getCheckString_CzII();

	}

	public void onTypOcenyChange() {
		System.out.println("onTypOcenyChange... czyszczenie kryteriów i ocen cząstkowych...");
		System.out.println(this.getOcenaKryteriaKeySet());

		if (ocena.getZptOcKryterias() == null || ocena.getZptOcKryterias().isEmpty())
			return;
		
		for (ZptOcKryteria k : ocena.getZptOcKryterias()) {
			k.setZptOceny(null);
		}
		ocena.getZptOcKryterias().clear();

		User.warn(
				"Po zmianie typu oceny zostaną usunięte wcześniej wprowadzone kryteria i oceny cząstkowe. Nie zapisuj zmian jeśli chcesz zachować te informacje.");
	}

	// f. skleja wartosci, ktore na cz I moze teoretycznie próbowac modyfikowac
	// uzyszkodnik.... jesli zrobi to bez uprawnien to ponowne obliczenie i
	// porownanie przy zapisie pozwoli zablokowac takie zmiany
	private String getCheckString_CzI() {
		String ret = AppBean.concatValues(ocena.getOceMaxDataOceny(), ocena.getOcePrcId(),
				ocena.getOcePrcIdOceniajacy(), ocena.getOceTypOceny(), this.getOcenaKryteriaKeySet());

		System.out.println(this.checkCzI);
		System.out.println(ret);
		return ret;
	}

	private String getCheckString_CzII() {
		String ret = AppBean.concatValues(ocena.getOceDataKolejnejOceny(), ocena.getOceDataOceny(), ocena.getOceDataOd(), ocena.getOceDataDo(),
				ocena.getOceOpis(), ocena.getOceOpis2(), ocena.getOceCzyWnKolStop(), ocena.getOceWnKolStopien());

		for (ZptOcKryteria k : ocena.getZptOcKryterias()) {
			ret += k.getKrytKryterium() + "-" + k.getKrytOcena() + "; ";
		}

		System.out.println(this.checkCzII);
		System.out.println(ret);
		return ret;
	}

	public Boolean allowUpdateCzI() {
		System.out.print("allowUpdateCzI ? / " + user.getLogin() + " / permissions: " + user.getPermissions()
				+ "; st.obiegu: " + obieg.stanObiegu());

		Boolean ret = false;
		if (user.hasAnyPermission("PP_OCE_OCENIAJACY", "PP_OCE_SEKRETARIAT", "PP_OCE_ADMINISTRATOR")) {
			ret = (user.hasPermission("PP_OCE_ADMINISTRATOR") || "START".equals(obieg.stanObiegu())
					|| "".equals(obieg.stanObiegu()) // nowa encja
			);
		}
		System.out.println(" -> " + ret);
		return ret;
	}

	public Boolean allowUpdateCzII() {
		System.out.print("allowUpdateCzII ? / " + user.getLogin() + " / permissions: " + user.getPermissions()
				+ "; st.obiegu: " + obieg.stanObiegu());

		Boolean ret = false;
		if (user.hasAnyPermission("PP_OCE_OCENIAJACY", "PP_OCE_SEKRETARIAT", "PP_OCE_ADMINISTRATOR")) {
			ret = (user.hasPermission("PP_OCE_ADMINISTRATOR") || "START".equals(obieg.stanObiegu())
					|| "POSIOM 2".equals(obieg.stanObiegu()) || "".equals(obieg.stanObiegu()) // nowa
																								// encja
			);
		}
		System.out.println(" -> " + ret);
		return ret;
	}


	public String zaznaczWczytaneDodatkowe() {
		HashSet<String> ocenaKryteriaKeySet = this.getOcenaKryteriaKeySet();
		
		SqlDataSelectionsHandler dodatkoweRows = getLW("PPL_OCE_KRYT_DODATKOWE", this.ocena.getOceTypOceny(), this.ocena.getOceTypOceny());
		dodatkoweRows.clearSelection();
		for (DataRow dr : dodatkoweRows.getData()) {
			if (ocenaKryteriaKeySet.contains(dr.get("WSL_WARTOSC"))) {
				dodatkoweRows.setRowSelection(true, dr);

			}
		}
		return "";
	}

	public Double wyliczSrednia() {
		double licznik = 0.0;
		double mianownik = 0.0;

		for (ZptOcKryteria k : this.ocena.getZptOcKryterias()) {
			String o = k.getKrytOcena();
			if (o != null && !"".equals(o)) {
				licznik += Integer.valueOf(o);
				mianownik++;
			}

		}

		if (mianownik == 0)
			return null;
		else
			return (double) Math.round((licznik / mianownik) * 100.0) / 100.0;
	}
	
	public boolean isAnyKrytOcenaZnaczniePonizejOczekiwan() {
		ArrayList<DataRow> sqlData = this.getLW("PPL_OCE_OCENY_CZASTKOWE").getData();
		
		DataRow znaczniePonizejOczekiwanDR = sqlData.get(0);
		int znaczniePonizejWymaganWartoscMax;
		
		if(znaczniePonizejOczekiwanDR != null)
			znaczniePonizejWymaganWartoscMax = Integer.valueOf((String) znaczniePonizejOczekiwanDR.get("WSL_WARTOSC_MAX"));
		else
			return true;
		
		for (ZptOcKryteria k : this.ocena.getZptOcKryterias()) {
			String o = k.getKrytOcena();
			
			if(o != null) {
				if(Integer.valueOf(o) <= znaczniePonizejWymaganWartoscMax )
					return true;
			}
		}
		
		return false;
	}

	public String poziomSpelnienia() {
		String ret = "-/-";
		Double srednia = this.wyliczSrednia();
		if (srednia == null)
			return ret;

		ArrayList<DataRow> sqlData = this.getLW("PPL_OCE_OCENY_CZASTKOWE").getData();
		for (DataRow r : sqlData) {
			Integer p = Integer.valueOf("" + r.get("WSL_WARTOSC"));
			if (srednia <= p)
				break;
			else
				ret = "" + r.get("WSL_OPIS");
		}
		return ret;
	}

	private void wyliczSredniaWymagana() {
		ArrayList<DataRow> sqlData = this.getLW("PPL_OCE_OCENY_CZASTKOWE").getData();
/*		for (DataRow r : sqlData) {
			setSredniaWymagana(getSredniaWymagana() + Integer.valueOf("" + r.get("WSL_WARTOSC_MAX")));
		}
		if (getSredniaWymagana() > 0)
			setSredniaWymagana( (getSredniaWymagana() / sqlData.size()) -1); //zgodnie z Rozporządzeniem Prezesa Rady Ministrów z dnia 4 kwietnia 2016 par. 11
*/	
		setSredniaWymagana(sqlData.stream().mapToInt(r -> Integer.valueOf("" + r.get("WSL_WARTOSC_MAX"))).average().getAsDouble()-1);
	}

	public String ocenaWynikowa() {
		if (!czyWszystkieOceny())
			return "-/-";

		if (getSredniaWymagana() == 0)
			wyliczSredniaWymagana();

		Double srednia = this.wyliczSrednia();
		Double sredniaWymagana = getSredniaWymagana();
		boolean iskryteriumPonizejOczekiwan = isAnyKrytOcenaZnaczniePonizejOczekiwan();

		String ret = "";
		
		if (srednia == null){
			return "-/-";
		}else if ( sredniaWymagana >= srednia || iskryteriumPonizejOczekiwan ){ //zgodnie z Rozporządzeniem Prezesa Rady Ministrów z dnia 4 kwietnia 2016 par. 11
			ret += "NEGATYWNA ";
			if( sredniaWymagana  >= srednia)  //zgodnie z Rozporządzeniem Prezesa Rady Ministrów z dnia 4 kwietnia 2016 par. 11
				ret += String.format("\n(średnia wymagana powyżej: %1$s)", this.getSredniaWymagana());
			if(iskryteriumPonizejOczekiwan)
				ret += "\n(Co najmniej 1 ocena cząstkowa znacznie poniżej wymagań)";
		}else{
			ret = "POZYTYWNA";
		}
		
		return ret;
	}

	private void dodajUsunKryteria(String rodzaj, List<DataRow> kryteriaRows) {
		if (kryteriaRows != null) {
			HashSet<String> kryteriaOcenaKeySet = getOcenaKryteriaKeySet();
			HashSet<String> kryteriaRowsKeySet = new HashSet<String>();

			for (DataRow dr : kryteriaRows) {
				String kryterium = "" + dr.get("WSL_WARTOSC");
				kryteriaRowsKeySet.add(kryterium);
				if (!kryteriaOcenaKeySet.contains(kryterium)) {
					addOcenaNoweKryteria(kryterium, rodzaj);
				}
			}

			ArrayList<ZptOcKryteria> toRemove = new ArrayList<ZptOcKryteria>();
			for (ZptOcKryteria k : this.ocena.getZptOcKryterias()) {
				if (rodzaj.equals(k.getKrytRodzaj()) && !kryteriaRowsKeySet.contains(k.getKrytKryterium())) {
					toRemove.add(k);
				}
			}

			toRemove.forEach(k -> {
				this.ocena.removeZptOcKryteria(k);
			});
		}
	}

	private Boolean validateBeforeSaveOcena(boolean addSelectedKryteria) {
		// walidacja - do dalszego refaktoringu
		ArrayList<String> inval = new ArrayList<String>();

		//PT 238494
		/*if ("T".equals( this.ocena.getOceCzyWnKolStop()) && (nz(ocena.getOceWnKolStopien()).isEmpty()) )
			inval.add("Przyznawany stopień jest polem wymaganym.");*/

		if ("T".equals( this.ocena.getOceCzyWnKolStop()) && (nz(ocena.getOceWnKolStUmot()).isEmpty()) )
			inval.add("Umotywowanie wniosku jest polem wymaganym.");

		if (ocena.getOceMaxDataOceny() == null)
			inval.add("Termin sporządzenia oceny jest polem wymaganym.");

		if (ocena.getOceDataDo() != null && ocena.getOceDataDo() != null
				&& ocena.getOceDataDo().before(ocena.getOceDataOd()))
			inval.add("Pole 'Okres - od' jest mniejsze niż 'Okres do'.");

		if (ocena.getOcePrcId() == null || ocena.getOcePrcId() == 0)
			inval.add("Pracownik oceniany jest polem wymaganym.");

		if (ocena.getOcePrcIdOceniajacy() == null || ocena.getOcePrcIdOceniajacy() == 0)
			inval.add("Pracownik oceniający jest polem wymaganym.");

		if (ocena.getOceTypOceny() == null)
			inval.add("Typ oceny jest polem wymaganym.");

		if (getKryteriaDodatkowe().size() > 3) 
			inval.add("Powinno być najwyżej trzy kryteria dodatkowe.");

		if (ocena.getOcePrcId() != null && ocena.getOcePrcIdOceniajacy() != null
				&& ocena.getOcePrcId().equals(ocena.getOcePrcIdOceniajacy()))
			inval.add("Pracownik oceniany nie moze być jednocześnie oceniającym.");

		if (ocena.getOceCzyWnKolStop() != null && ocena.getOceCzyWnKolStop() == "T"
				&& this.ocenaWynikowa() != "POZYTYWNA")
			inval.add(
					"Ocena wynikowa nie jest pozytywna, więc nie można dołączyć wniosku o przyznanie kolejnego stopnia służbowego.");
		
		if (!addSelectedKryteria && ocena.getOceDataOceny()==null) 
			inval.add("Data rozmowy nie może być pusta.");
		
		SqlDataSelectionsHandler s = this.getLW("PPL_OCE_TERMIN_OCENY", ocena.getOcePrcId(),ocena.getOceId() ,ocena.getOcePrcId());
		Date d = new Date();
		if(!s.getData().isEmpty()){
			d=  (Date) s.getData().get(0).get("data_poprzedniej");
			d=DateUtils.addMonths(d, -18);
		}
		
		if( d.after(new Date()) ) 
			User.warn("Nie upłynęło sześć miesięcy od poprzedniej oceny!");

		if (!inval.isEmpty()) {
			for (String msg : inval) {
				User.alert(msg);
			}
		}
		return inval.isEmpty();
	}

//	private Boolean validatePermissions() {
//		ArrayList<String> inval = new ArrayList<String>();
//
//		// test czy są zmiany
//		String currentCheckString_CzI = this.getCheckString_CzI();
//		String currentCheckString_CzII = this.getCheckString_CzII();
//
//		System.out.println();
//		System.out.println(checkCzI);
//		System.out.println(currentCheckString_CzI);
//		System.out.println();
//		System.out.println(checkCzII);
//		System.out.println(currentCheckString_CzII);
//
//		// if ( this.checkCzI.equals( currentCheckString_CzI )
//		// && this.checkCzII.equals( currentCheckString_CzII ) )
//		// inval.add("Brak zmian do zapisania.");
//
//		if (!user.allowCreateOcenaOkresowa() && this.selectedOecenaId == null)
//			inval.add("Nieautoryzowana próba utworzenia nowej oceny.");
//
//		if (!this.allowUpdateCzI() && !this.checkCzI.equals(currentCheckString_CzI))
//			inval.add("Nieautoryzowana próba modyfikacji zawartości części I oceny.");
//
//		if (!this.allowUpdateCzII() && !this.checkCzII.equals(currentCheckString_CzII))
//			inval.add("Nieautoryzowana próba modyfikacji zawartości części II oceny.");
//
//		for (String txt : inval) {
//			// FacesContext.getCurrentInstance().addMessage(null, new
//			// FacesMessage(FacesMessage.SEVERITY_ERROR, "BŁĄD","" + txt));
//			User.alert(txt);
//		}
//		return inval.isEmpty();
//	}
	

	private List<ZptOcKryteria> getKryteria(String rodzaj) {
		ArrayList<ZptOcKryteria> ret = new ArrayList<ZptOcKryteria>();
		for (ZptOcKryteria k : ocena.getZptOcKryterias()) {
			if (rodzaj.equals(k.getKrytRodzaj()))
				ret.add(k);
		}
		return ret;
	}

	private HashSet<String> getOcenaKryteriaKeySet() {
		HashSet<String> zptKrKeySet = new HashSet<String>();
		if (ocena != null && ocena.getZptOcKryterias() != null)
			for (ZptOcKryteria k : ocena.getZptOcKryterias()) {
				zptKrKeySet.add(k.getKrytKryterium());
			}
		return zptKrKeySet;
	}

	private void addOcenaNoweKryteria(String kryterium, String rodzaj) {
		ZptOcKryteria k = new ZptOcKryteria();
		k.setKrytKryterium(kryterium);
		k.setKrytRodzaj(rodzaj);

		if (ocena.getZptOcKryterias() == null)
			ocena.setZptOcKryterias(new ArrayList<>());

		ocena.addZptOcKryteria(k);
		System.out.println("ocena / dodawanie kryterium " + k.getKrytKryterium() + " /rodzaj: " + k.getKrytRodzaj());
	}
	
	//narazie jest to wyliczane w obiegu
	public void wyliczOkresOdOkresDo() {
		if(ocena == null) return; 
		
		if(ocena.getOceDataOd() == null || ocena.getOceDataDo() == null) {
			SqlDataSelectionsHandler s = this.getLW("PPL_OCE_OKRES_OD_DO", ocena.getOceId());
			if (!s.getData().isEmpty()) {
			
				if(ocena.getOceDataOd() == null && s.getData().get(0).get("POZ30_Data")!=null) {
					ocena.setOceDataOd( (Date) s.getData().get(0).get("POZ30_Data") );
				} 
				
				if(ocena.getOceDataDo() == null && s.getData().get(0).get("POZ40_Data")!=null) {
					ocena.setOceDataDo( (Date) s.getData().get(0).get("POZ40_Data") );
				} 
			}
		}
	}


	public ZptOceny getOcena() {
		return ocena;
	}

	public void setOcena(ZptOceny ocena) {
		this.ocena = ocena;
	}

	public Long getTestVal() {
		return this.ocena.getOcePrcIdOceniajacy();
	}

	public void setTestVal(Long id) {
		System.out.println("setTestVal.....!!!....");
		this.ocena.setOcePrcId(id);
	}

	public List<ZptOcKryteria> getKryteriaObowiazkowe() {
		return getKryteria("O");
	}

	public List<ZptOcKryteria> getKryteriaDodatkowe() {
		return getKryteria("D");
	}

	public double getSredniaWymagana() {
		return sredniaWymagana;
	}

	public void setSredniaWymagana(double sredniaWymagana) {
		this.sredniaWymagana = sredniaWymagana;
	}

	public Boolean czyWszystkieOceny() {

		for (ZptOcKryteria k : this.ocena.getZptOcKryterias()) {
			if (k.getKrytOcena() == null || "".equals(k.getKrytOcena()))
				return false;
		}

		return true;
	}

	public Object getWybranaCzynnosc() {
		return wybranaCzynnosc;
	}

	public void setWybranaCzynnosc(Object wybranaCzynnosc) {
		this.wybranaCzynnosc = wybranaCzynnosc;
	}

	public Boolean getCzyZalaczonoWniosek() {
		return czyZalaczonoWniosek;
	}

	public void setCzyZalaczonoWniosek(Boolean czyZalaczonoWniosek) {
		this.czyZalaczonoWniosek = czyZalaczonoWniosek;
	}

	public Long getSelectedOecenaId() {
		return selectedOecenaId;
	}

	public void setSelectedOecenaId(Long selectedOecenaId) {
		this.selectedOecenaId = selectedOecenaId;
	}

	public SqlDataSelectionsHandler getPracownicyPodlegliWybranemu() {
		return pracownicyPodlegliWybranemu;
	}

	public void setPracownicyPodlegliWybranemu(SqlDataSelectionsHandler pracownicyPodlegliWybranemu) {
		this.pracownicyPodlegliWybranemu = pracownicyPodlegliWybranemu;
	}

	public String getAktualnyStopienSluzbowy() {
		return aktualnyStopienSluzbowy;
	}

	public void setAktualnyStopienSluzbowy(String aktualnyStopienSluzbowy) {
		this.aktualnyStopienSluzbowy = aktualnyStopienSluzbowy;
	}

	public String getStanObiegu4Update() {
		return stanObiegu4Update;
	}

	public void setStanObiegu4Update(String stanObiegu4Update) {
		
		if (this.stanObiegu4Update!=null && !eq(this.stanObiegu4Update, stanObiegu4Update)
				&& ("POZIOM30".equals( stanObiegu4Update ) || "POZIOM40".equals( stanObiegu4Update ) ) ){
			this.ocena = (ZptOceny) HibernateContext.refresh(this.ocena);
		}
		
		this.stanObiegu4Update = stanObiegu4Update;
	}

}
