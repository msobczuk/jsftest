package com.comarch.egeria.pp.OcenaOkresowa;

import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.OcenaOkresowa.model.Ocena;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

@ManagedBean // JBossTools
@Named
@Scope("view")
public class ListaOcenRCGWBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();


	public void nowaOcena(DataRow r) throws IOException {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("onag_rodzaj", r.getIdAsString());
		FacesContext.getCurrentInstance().getExternalContext().redirect("OcenaRCGW");

//		String typ = r.getIdAsString();
//		String url = mapujTypWnioskuToUrl(typ);
//
//		try {
//			if (url!=null) {
//				System.out.println(typ + " -> " + url);
//				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", null);
//				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", null);
//				FacesContext.getCurrentInstance().getExternalContext().redirect(url);
//			}
//		} catch (Exception e) {
//			log.error(e.getMessage(), e);
//		}

	}



	public void edytuj(DataRow row){

		System.out.println();

//		System.out.println();
//
//		//		System.out.println(row);
//		if (row==null) return;
//
//		String typ = (String) row.get("zfs_typ");
//		String url = "test" ;//mapujTypWnioskuToUrl(typ);
//
//		try {
//			if (url!=null) {
//				System.out.println(typ + " -> " + url);
//				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", row);
//				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", row.getIdAsLong());
//				FacesContext.getCurrentInstance().getExternalContext().redirect(url);
//			}
//		} catch (Exception e) {
//			log.error(e.getMessage(), e);
//		}

	}


}
