package com.comarch.egeria.pp.OcenaOkresowa.model;

import com.comarch.egeria.web.User;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Entity
public class OcenaWartosc extends PptOcenyWartosci {



    public Ocena getOcena(){
        return (Ocena) this.pptOceny;
    }

    public String getKryteriumOpis(){
        if (this.getOcena()==null) return null;
        return this.getOcena().getSlKryteria().findAndFormat(this.owartKryterium, "%rv_meaning$s");
    }


    @Override
    public void setOwartWartoscE2(BigDecimal owartWartoscE2) {
        boolean eq = eq(this.owartWartoscE2, owartWartoscE2);
        super.setOwartWartoscE2(owartWartoscE2);
        if (!eq){
            ustawZalogowanyUserJakoPrzelozony();
        }
    }

    @Override
    public void setOwartWartoscE3(BigDecimal owartWartoscE3) {
        boolean eq = eq(this.owartWartoscE3, owartWartoscE3);
        super.setOwartWartoscE3(owartWartoscE3);
        if (!eq){
            ustawZalogowanyUserJakoPrzelozony();

            Ocena ocena = this.getOcena();
            if (ocena !=null){
                ocena.setOnagPremia(ocena.getWynikE3());
            }
        }
    }

    private void ustawZalogowanyUserJakoPrzelozony() {
        if (this.getOcena() != null && this.getOcena().getOnagPrcIdPrz() == null && User.getCurrentUser() != null) {
            this.getOcena().setOnagPrcIdPrz(User.getCurrentUser().getPrcIdLong());
        }
    }

    public BigDecimal getWynikE3(){
        if (this.getOwartWartoscE3()==null || this.getOwartWaga()==null) return null;
        return this.getOwartWaga().multiply(this.getOwartWartoscE3()).setScale(2, RoundingMode.HALF_UP);
    }

}
