package com.comarch.egeria.pp.OcenaOkresowa.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_OCE_OCENY database table.
 * 
 */
@Entity
@Table(name="PPT_OCE_OCENY", schema="PPADM")
@NamedQuery(name="ZptOceny.findAll", query="SELECT z FROM ZptOceny z")
public class ZptOceny implements Serializable {
	private static final long serialVersionUID = 1L;

	@Transient
	private SqlBean sqlBean;
	
	@Id
//	@SequenceGenerator(name="ZPT_OCENY_OCEID_GENERATOR", sequenceName="ZPS_OCE")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ZPT_OCENY_OCEID_GENERATOR")
	@TableGenerator(name="TABLE_KEYGEN_PPT_OCE_OCENY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OCE_OCENY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_OCE_OCENY")	
	@Column(name="OCE_ID")
	private long oceId;

	@Column(name="OCE_AUDYT_KM")
	private String oceAudytKm;

	@Column(name="OCE_AUDYT_KT")
	private String oceAudytKt;

	@Column(name="OCE_AUDYT_LM")
	private Long oceAudytLm;

	@Temporal(TemporalType.DATE)
	@Column(name="OCE_DATA_DO")
	private Date oceDataDo;

	@Temporal(TemporalType.DATE)
	@Column(name="OCE_DATA_KOLEJNEJ_OCENY")
	private Date oceDataKolejnejOceny = new Date();

	@Temporal(TemporalType.DATE)
	@Column(name="OCE_DATA_OCENY")
	private Date oceDataOceny;

	@Temporal(TemporalType.DATE)
	@Column(name="OCE_DATA_OD")
	private Date oceDataOd;

	@Temporal(TemporalType.DATE)
	@Column(name="OCE_AUDYT_DM")
	private Date oceAudytDM;

	@Temporal(TemporalType.DATE)
	@Column(name="OCE_AUDYT_DT")
	private Date oceAudytDT;

	@Column(name="OCE_AUDYT_UM")
	private String oceAudytUM;

	@Column(name="OCE_AUDYT_UT")
	private String oceAudytUT;

	@Temporal(TemporalType.DATE)
	@Column(name="OCE_MAX_DATA_OCENY")
	private Date oceMaxDataOceny = new Date();

	@Column(name="OCE_OCENIAJACY")
	private String oceOceniajacy;

	@Column(name="OCE_OPIS")
	private String oceOpis;

	@Column(name="OCE_OPIS2")
	private String oceOpis2;

	@Column(name="OCE_PRC_ID")
	private Long ocePrcId;

	@Column(name="OCE_PRC_ID_OCENIAJACY")
	private Long ocePrcIdOceniajacy;

	@Column(name="OCE_TYP_OCENY")
	private String oceTypOceny;

	@Column(name="OCE_SREDNIA")
	private Double oceSrednia;
	
	@Column(name="OCE_CZY_WN_KOL_STOP")
	private String oceCzyWnKolStop;
	
	
	@Column(name="OCE_WN_KOL_STOPIEN")
	private String oceWnKolStopien;
	
	@Column(name="OCE_WN_KOL_ST_UMOT")
	private String oceWnKolStUmot;
	
	//bi-directional many-to-one association to ZptOcKryteria
	
	@OneToMany(mappedBy="zptOceny", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ZptOcKryteria> zptOcKryterias = new ArrayList<>();

	public ZptOceny() {
	}

	public long getOceId() {
		return this.oceId;
	}

	public void setOceId(long oceId) {
		this.oceId = oceId;
	}

	public String getOceAudytKm() {
		return this.oceAudytKm;
	}

	public void setOceAudytKm(String oceAudytKm) {
		this.oceAudytKm = oceAudytKm;
	}

	public String getOceAudytKt() {
		return this.oceAudytKt;
	}

	public void setOceAudytKt(String oceAudytKt) {
		this.oceAudytKt = oceAudytKt;
	}

	public Long getOceAudytLm() {
		return this.oceAudytLm;
	}

	public void setOceAudytLm(Long oceAudytLm) {
		this.oceAudytLm = oceAudytLm;
	}

	public Date getOceDataDo() {
		return this.oceDataDo;
	}

	public void setOceDataDo(Date oceDataDo) {
		this.oceDataDo = oceDataDo;
	}

	public Date getOceDataKolejnejOceny() {
		return this.oceDataKolejnejOceny;
	}

	public void setOceDataKolejnejOceny(Date oceDataKolejnejOceny) {
		this.oceDataKolejnejOceny = oceDataKolejnejOceny;
	}

	public Date getOceDataOceny() {
		return this.oceDataOceny;
	}

	public void setOceDataOceny(Date oceDataOceny) {
		this.oceDataOceny = oceDataOceny;
	}

	public Date getOceDataOd() {
		return this.oceDataOd;
	}

	public void setOceDataOd(Date oceDataOd) {
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! setOceDataOd -> " + oceDataOd);
		this.oceDataOd = oceDataOd;
	}

	public Date getOceAudytDM() {
		return this.oceAudytDM;
	}

	public void setOceAudytDM(Date oceAudytDM) {
		this.oceAudytDM = oceAudytDM;
	}

	public Date getOceAudytDT() {
		return this.oceAudytDT;
	}

	public void setOceAudytDT(Date oceAudytDT) {
		this.oceAudytDT = oceAudytDT;
	}

	public String getOceAudytUM() {
		return this.oceAudytUM;
	}

	public void setOceAudytUM(String oceAudytUM) {
		this.oceAudytUM = oceAudytUM;
	}

	public String getOceAudytUT() {
		return this.oceAudytUT;
	}

	public void setOceAudytUT(String oceAudytUT) {
		this.oceAudytUT = oceAudytUT;
	}

	public Date getOceMaxDataOceny() {
		return this.oceMaxDataOceny;
	}

	public void setOceMaxDataOceny(Date oceMaxDataOceny) {
		System.out.println("setOceMaxDataOceny....-> " + oceMaxDataOceny);
		this.oceMaxDataOceny = oceMaxDataOceny;
	}

	public String getOceOceniajacy() {
		return this.oceOceniajacy;
	}

	public void setOceOceniajacy(String oceOceniajacy) {
		this.oceOceniajacy = oceOceniajacy;
	}

	public String getOceOpis() {
		return this.oceOpis;
	}

	public void setOceOpis(String oceOpis) {
		System.out.println("ZptOceny.setOceOpis...." + oceOpis);
		this.oceOpis = oceOpis;
	}

	public String getOceOpis2() {
		return this.oceOpis2;
	}

	public void setOceOpis2(String oceOpis2) {
		this.oceOpis2 = oceOpis2;
	}

	public Long getOcePrcId() {
		return this.ocePrcId;
	}

	public void setOcePrcId(Long ocePrcId) {
		if(sqlBean!=null){
			if(ocePrcId!=null){
				SqlDataSelectionsHandler s = sqlBean.getLW("PPL_OCE_TERMIN_OCENY", ocePrcId, oceId, ocePrcId);
				if(!s.getData().isEmpty()){
					this.setOceMaxDataOceny(  (Date) s.getData().get(0).get("data_poprzedniej")  );
				}
			}
		}
		this.ocePrcId = ocePrcId;
	}

	public Long getOcePrcIdOceniajacy() {
//		System.out.println("ZptOceny.getOcePrcIdOceniajacy. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . ."+ocePrcIdOceniajacy);
		return this.ocePrcIdOceniajacy;
	}

	public void setOcePrcIdOceniajacy(Long ocePrcIdOceniajacy) {
		System.out.println("ZptOceny.setOcePrcIdOceniajacy....................................................................................."+ocePrcIdOceniajacy);
		this.ocePrcIdOceniajacy = ocePrcIdOceniajacy;
	}

	public String getOceTypOceny() {
		return this.oceTypOceny;
	}

	public void setOceTypOceny(String oceTypOceny) {
		this.oceTypOceny = oceTypOceny;
	}

	public List<ZptOcKryteria> getZptOcKryterias() {
		return this.zptOcKryterias;
	}

	public void setZptOcKryterias(List<ZptOcKryteria> zptOcKryterias) {
		this.zptOcKryterias = zptOcKryterias;
	}

	public ZptOcKryteria addZptOcKryteria(ZptOcKryteria zptOcKryteria) {
		getZptOcKryterias().add(zptOcKryteria);
		zptOcKryteria.setZptOceny(this);

		return zptOcKryteria;
	}

	public ZptOcKryteria removeZptOcKryteria(ZptOcKryteria zptOcKryteria) {
		getZptOcKryterias().remove(zptOcKryteria);
		zptOcKryteria.setZptOceny(null);

		return zptOcKryteria;
	}

	public Double getOceSrednia() {
		return oceSrednia;
	}

	public void setOceSrednia(Double oceSrednia) {
		this.oceSrednia = oceSrednia;
	}

	public String getOceCzyWnKolStop() {
		return oceCzyWnKolStop;
	}

	public void setOceCzyWnKolStop(String oceCzyWnKolStop) {
		this.oceCzyWnKolStop = oceCzyWnKolStop;
	}

	public String getOceWnKolStopien() {
		return oceWnKolStopien;
	}

	public void setOceWnKolStopien(String oceWnKolStopien) {
		this.oceWnKolStopien = oceWnKolStopien;
	}

	public String getOceWnKolStUmot() {
		return oceWnKolStUmot;
	}

	public void setOceWnKolStUmot(String oceWnKolStUmot) {
		this.oceWnKolStUmot = oceWnKolStUmot;
	}

	public SqlBean getSqlBean() {
		return sqlBean;
	}

	public void setSqlBean(SqlBean sqlBean) {
		this.sqlBean = sqlBean;
	}
	
}