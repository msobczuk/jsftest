package com.comarch.egeria.pp.OcenaOkresowa;

import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.OcenaOkresowa.model.Ocena;
import com.comarch.egeria.pp.OcenaOkresowa.model.ZptOcKryteria;
import com.comarch.egeria.pp.OcenaOkresowa.model.ZptOceny;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

import static com.comarch.egeria.Utils.nz;

@ManagedBean // JBossTools
@Named
@Scope("view")
public class OcenaRCGWBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	@Inject
	User user;

	@Inject
	ObiegBean obieg;

	@Inject
	SessionBean sessionBean;

	Ocena ocena = null;


	@PostConstruct
	public void init() {

		System.out.println("12345");
		//obieg.setKategoriaObiegu("WN_OCENA_OKRES");

		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		if (id == null || "".equals(id)) {
			this.setOcena(new Ocena());
			String onagRodzaj = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("onag_rodzaj");
			this.ocena.setOnagRodzaj(onagRodzaj);
		} else {
				this.setOcena((Ocena) HibernateContext.get(Ocena.class, (Serializable) id));
		}

	}


	public void usun(){
		try {

			FacesContext.getCurrentInstance().getExternalContext().redirect("ListaOcenRCGW");
			HibernateContext.delete(this.ocena);

		} catch (Exception e){
			User.alert(e);
		}
	}


	public Ocena getOcena() {
		return ocena;
	}

	public void setOcena(Ocena ocena) {
		this.ocena = ocena;
	}

}
