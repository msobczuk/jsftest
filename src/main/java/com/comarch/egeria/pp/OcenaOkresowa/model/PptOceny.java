package com.comarch.egeria.pp.OcenaOkresowa.model;

import java.io.Serializable;
import javax.persistence.*;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.model.ModelBase;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_OCENY database table.
 * 
 */
@Entity
@Table(name="PPT_OCENY")
@DiscriminatorFormula("'Ocena'")
@NamedQuery(name="PptOceny.findAll", query="SELECT p FROM PptOceny p")
public class PptOceny extends ModelBase implements Serializable {
	static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_OCENY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OCENY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_OCENY")
	@Column(name="ONAG_ID")
	long onagId;

//	@Temporal(TemporalType.DATE)
//	@Column(name="ONAG_DATA")
//	Date onagData = Utils.today();

	@Column(name="ONAG_PRC_ID")
	Long onagPrcId;

	@Column(name="ONAG_PRC_ID_PRZ")
	Long onagPrcIdPrz;

	@Column(name="ONAG_PREMIA")
	BigDecimal onagPremia;

	@Column(name="ONAG_RODZAJ")
	String onagRodzaj;


	@Column(name="ONAG_ROK")
	Integer onagRok;

	@Column(name="ONAG_KWARTAL")
	Integer onagKwartal;


	//bi-directional many-to-one association to PptOcenyWartosci
	@OneToMany(mappedBy="pptOceny", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	List<PptOcenyWartosci> pptOcenyWartoscis = new ArrayList<>();

	public PptOceny() {
	}

	public long getOnagId() {
		this.onRXGet("onagId");
		return this.onagId;
	}

	public void setOnagId(long onagId) {
		this.onRXSet("onagId", this.onagId, onagId);
		this.onagId = onagId;
	}

//	public Date getOnagData() {
//		this.onRXGet("onagData");
//		return this.onagData;
//	}
//
//	public void setOnagData(Date onagData) {
//		this.onRXSet("onagData", this.onagData, onagData);
//		this.onagData = onagData;
//	}

	public Long getOnagPrcId() {
		this.onRXGet("onagPrcId");
		return this.onagPrcId;
	}

	public void setOnagPrcId(Long onagPrcId) {
		this.onRXSet("onagPrcId", this.onagPrcId, onagPrcId);
		this.onagPrcId = onagPrcId;
	}

	public Long getOnagPrcIdPrz() {
		this.onRXGet("onagPrcIdPrz");
		return this.onagPrcIdPrz;
	}

	public void setOnagPrcIdPrz(Long onagPrcIdPrz) {
		this.onRXSet("onagPrcIdPrz", this.onagPrcIdPrz, onagPrcIdPrz);
		this.onagPrcIdPrz = onagPrcIdPrz;
	}

	public BigDecimal getOnagPremia() {
		this.onRXGet("onagPremia");
		return this.onagPremia;
	}

	public void setOnagPremia(BigDecimal onagPremia) {
		this.onRXSet("onagPremia", this.onagPremia, onagPremia);
		this.onagPremia = onagPremia;
	}

	public List<PptOcenyWartosci> getPptOcenyWartoscis() {
		this.onRXGet("pptOcenyWartoscis");
		return this.pptOcenyWartoscis;
	}

	public void setPptOcenyWartoscis(List<PptOcenyWartosci> pptOcenyWartoscis) {
		this.onRXSet("pptOcenyWartoscis", this.pptOcenyWartoscis, pptOcenyWartoscis);
		this.pptOcenyWartoscis = pptOcenyWartoscis;
	}

	public PptOcenyWartosci addPptOcenyWartosci(PptOcenyWartosci pptOcenyWartosci) {
		this.onRXSet("pptOcenyWartoscis", null, this.pptOcenyWartoscis);
		getPptOcenyWartoscis().add(pptOcenyWartosci);
		pptOcenyWartosci.setPptOceny(this);

		return pptOcenyWartosci;
	}

	public PptOcenyWartosci removePptOcenyWartosci(PptOcenyWartosci pptOcenyWartosci) {
		this.onRXSet("pptOcenyWartoscis", null, this.pptOcenyWartoscis);
		getPptOcenyWartoscis().remove(pptOcenyWartosci);
		pptOcenyWartosci.setPptOceny(null);

		return pptOcenyWartosci;
	}


	public String getOnagRodzaj() {
		this.onRXGet("onagRodzaj");
		return onagRodzaj;
	}

	public void setOnagRodzaj(String onagRodzaj) {
		this.onRXSet("onagRodzaj", this.onagRodzaj, onagRodzaj);
		this.onagRodzaj = onagRodzaj;
	}

	public Integer getOnagRok() {
		this.onRXGet("onagRok");
		return onagRok;
	}

	public void setOnagRok(Integer onagRok) {
		this.onRXSet("onagRok", this.onagRok, onagRok);
		this.onagRok = onagRok;
	}

	public Integer getOnagKwartal() {
		this.onRXGet("onagKwartal");
		return onagKwartal;
	}

	public void setOnagKwartal(Integer onagKwartal) {
		this.onRXSet("onagKwartal", this.onagKwartal, onagKwartal);
		this.onagKwartal = onagKwartal;
	}


}
