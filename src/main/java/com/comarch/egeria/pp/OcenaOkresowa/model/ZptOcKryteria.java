package com.comarch.egeria.pp.OcenaOkresowa.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PPT_OCE_KRYTERIA database table.
 * 
 */
@Entity
@Table(name="PPT_OCE_KRYTERIA", schema="PPADM")
@NamedQuery(name="ZptOcKryteria.findAll", query="SELECT z FROM ZptOcKryteria z")
public class ZptOcKryteria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@SequenceGenerator(name="ZPT_OC_KRYTERIA_KRYTID_GENERATOR", sequenceName="ZPS_OC_KRYT")
//	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ZPT_OC_KRYTERIA_KRYTID_GENERATOR")
	
	@TableGenerator(name="TABLE_KEYGEN_PPT_OCE_KRYTERIA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OCE_KRYTERIA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_OCE_KRYTERIA")		
	@Column(name="KRYT_ID")
	private long krytId;

	@Temporal(TemporalType.DATE)
	@Column(name="KRYT_AUDYT_DM")
	private Date krytAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="KRYT_AUDYT_DT")
	private Date krytAudytDt;

	@Column(name="KRYT_AUDYT_KM")
	private String krytAudytKm;

	@Column(name="KRYT_AUDYT_LM")
	private String krytAudytLm;

	@Column(name="KRYT_AUDYT_UM")
	private String krytAudytUm;

	@Column(name="KRYT_AUDYT_UT")
	private String krytAudytUt;

	@Column(name="KRYT_KRYTERIUM")
	private String krytKryterium;

	@Column(name="KRYT_OCENA")
	private String krytOcena;

	@Column(name="KRYT_RODZAJ")
	private String krytRodzaj;

	//bi-directional many-to-one association to ZptOceny 
	//@ManyToOne(cascade={CascadeType.ALL}) // NIGDY NIE ROBIMY KSKAD OD DZIECKA DO RODZICA cascade={CascadeType.ALL} (bo próba kasowania dziecka konczy sie proba skasowania rodzica)
	@ManyToOne
	@JoinColumn(name="KRYT_OCE_ID")
	private ZptOceny zptOceny;

	public ZptOcKryteria() {
	}

	public long getKrytId() {
		return this.krytId;
	}

	public void setKrytId(long krytId) {
		this.krytId = krytId;
	}

	public Date getKrytAudytDm() {
		return this.krytAudytDm;
	}

	public void setKrytAudytDm(Date krytAudytDm) {
		this.krytAudytDm = krytAudytDm;
	}

	public Date getKrytAudytDt() {
		return this.krytAudytDt;
	}

	public void setKrytAudytDt(Date krytAudytDt) {
		this.krytAudytDt = krytAudytDt;
	}

	public String getKrytAudytKm() {
		return this.krytAudytKm;
	}

	public void setKrytAudytKm(String krytAudytKm) {
		this.krytAudytKm = krytAudytKm;
	}

	public String getKrytAudytLm() {
		return this.krytAudytLm;
	}

	public void setKrytAudytLm(String krytAudytLm) {
		this.krytAudytLm = krytAudytLm;
	}

	public String getKrytAudytUm() {
		return this.krytAudytUm;
	}

	public void setKrytAudytUm(String krytAudytUm) {
		this.krytAudytUm = krytAudytUm;
	}

	public String getKrytAudytUt() {
		return this.krytAudytUt;
	}

	public void setKrytAudytUt(String krytAudytUt) {
		this.krytAudytUt = krytAudytUt;
	}

	public String getKrytKryterium() {
		return this.krytKryterium;
	}

	public void setKrytKryterium(String krytKryterium) {
		this.krytKryterium = krytKryterium;
	}

	public String getKrytOcena() {
		return this.krytOcena;
	}

	public void setKrytOcena(String krytOcena) {
		this.krytOcena = krytOcena;
	}

	public String getKrytRodzaj() {
		return this.krytRodzaj;
	}

	public void setKrytRodzaj(String krytRodzaj) {
		this.krytRodzaj = krytRodzaj;
	}

	public ZptOceny getZptOceny() {
		return this.zptOceny;
	}

	public void setZptOceny(ZptOceny zptOceny) {
		this.zptOceny = zptOceny;
	}

}