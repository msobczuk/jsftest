package com.comarch.egeria.pp.OcenaOkresowa;

import static com.comarch.egeria.Utils.nz;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import net.sf.jasperreports.engine.JRException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

@ManagedBean
@Named
@Scope("session")
public class OcenaRaportBean extends SqlBean {

  private static final Logger log = LogManager.getLogger(OcenaRaportBean.class);

  //Widoczne kolumny:
  private Boolean pokazNrOceny = false;
  private Boolean pokazNrOcenianego = false;
  private Boolean pokazOcenianego = false;
  private Boolean pokazNrOceniajacego = false;
  private Boolean pokazOceniajacego = false;
  private Boolean pokazStanowisko = false;
  private Boolean pokazRodzajUmowy = false;
  private Boolean pokazTerminOceny = false;
  private Boolean pokazPoziomOceny = false;
  private Boolean pokazDatePoprzedniejOceny = false;
  private Boolean pokazPoziomPoprzedniejOceny = false;
  private Boolean pokazPoprzedniStopienSluzbowy = false;
  private Boolean pokazDatePrzyznaniaOstatniegoStopnia = false;


  //filtruj wg:
  private Date terminOcenyOd = null;
  private Date terminOcenyDo = null;

  private Date okresOcenyOd = null;
  private Date okresOcenyDo = null;

  private Long jednostkaId;
  private Long wydzialId;

  private SortujWedlug sortujWedlug = SortujWedlug.NUMER_OCENY;
  private String sortujRosnacoMalejaco = "SortujRosnaco";

  public SortujWedlug[] getSortujWedlugValues() {
    return SortujWedlug.values();
  }

  private Boolean anyMarked() {
    return pokazNrOceny || pokazNrOcenianego || pokazOcenianego || pokazNrOceniajacego
        || pokazOceniajacego || pokazStanowisko || pokazRodzajUmowy || pokazPoziomOceny
        || pokazTerminOceny || pokazDatePoprzedniejOceny || pokazPoziomPoprzedniejOceny
        || pokazPoprzedniStopienSluzbowy || pokazDatePrzyznaniaOstatniegoStopnia;
  }

  private HashMap<String, Object> prepareReportParams() {
    HashMap<String, Object> params = new HashMap<>();

    params.put("pokazNrOceny", pokazNrOceny);
    params.put("pokazNrOcenianego", pokazNrOcenianego);
    params.put("pokazOcenianego", pokazOcenianego);
    params.put("pokazNrOceniajacego", pokazNrOceniajacego);
    params.put("pokazOceniajacego", pokazOceniajacego);
    params.put("pokazStanowisko", pokazStanowisko);
    params.put("pokazRodzajUmowy", pokazRodzajUmowy);
    params.put("pokazPoziomOceny", pokazPoziomOceny);
    params.put("pokazTerminOceny", pokazTerminOceny);
    params.put("pokazDatePoprzedniejOceny", pokazDatePoprzedniejOceny);
    params.put("pokazPoziomPoprzedniejOceny", pokazPoziomPoprzedniejOceny);
    params.put("pokazPoprzedniStopienSluzbowy", pokazPoprzedniStopienSluzbowy);
    params.put("pokazDatePrzyznaniaOstatniegoStopnia", pokazDatePrzyznaniaOstatniegoStopnia);

    params.put("terminOcenyOd", nz(format(terminOcenyOd)));
    params.put("terminOcenyDo", nz(format(terminOcenyDo)));

    params.put("okresOcenyOd", nz(format(okresOcenyOd)));
    params.put("okresOcenyDo", nz(format(okresOcenyDo)));

    params.put("sortujWg", createSortValue());

    params.put("departamentId", jednostkaId);
    params.put("wydzialId", wydzialId);

    return params;
  }

  private String createSortValue() {
    String sortValue = sortujWedlug.getField();

    if ("SortujMalejaco".equals(sortujRosnacoMalejaco)) {
      sortValue = sortValue + " desc";
    }

    return sortValue;
  }

  public void raportPDF() {
    if (!anyMarked()) {
      User.alert("Raport musi zawierać przynajmniej jedną kolumnę!");
      return;
    }

    try {
      ReportGenerator.displayReportAsPDF(prepareReportParams(), "Ocena_okresowa");
    } catch (JRException | SQLException | IOException e) {
      log.error(e.getMessage(), e);
      User.alert(e.getMessage());
    }
  }

  public void raportXLS() {
    if (!anyMarked()) {
      User.alert("Raport musi zawierać przynajmniej jedną kolumnę!");
      return;
    }
    try {
      ReportGenerator.displayReportAsXLS(prepareReportParams(), "Ocena_okresowa");
    } catch (JRException | SQLException | IOException e) {
      log.error(e.getMessage(), e);
      User.alert(e.getMessage());
    }
  }

  public void komOrgRowSelectAction() {
    wydzialId = null;
  }

  public enum SortujWedlug {
    NUMER_OCENY("Numer oceny", "nr_oceny"),
    TERMIN_OCENY("Termin oceny", "termin_oceny"),
    OCENIANY("Oceniany", "prc_oceniany"),
    OCENIAJACY("Oceniający", "prc_oceniajacy");

    private String label;
    private String field;

    SortujWedlug(String label, String field) {
      this.label = label;
      this.field = field;
    }

    public String getLabel() {
      return label;
    }

    public String getField() {
      return field;
    }
  }

  public String getPokazNrOceny() {
    return boolToTNstr(pokazNrOceny);
  }

  public void setPokazNrOceny(String pokazNrOceny) {
    this.pokazNrOceny = strTNtoBool(pokazNrOceny);
  }

  public String getPokazNrOcenianego() {
    return boolToTNstr(pokazNrOcenianego);
  }

  public void setPokazNrOcenianego(String pokazNrOcenianego) {
    this.pokazNrOcenianego = strTNtoBool(pokazNrOcenianego);
  }

  public String getPokazOcenianego() {
    return boolToTNstr(pokazOcenianego);
  }

  public void setPokazOcenianego(String pokazOcenianego) {
    this.pokazOcenianego = strTNtoBool(pokazOcenianego);
  }

  public String getPokazNrOceniajacego() {
    return boolToTNstr(pokazNrOceniajacego);
  }

  public void setPokazNrOceniajacego(String pokazNrOceniajacego) {
    this.pokazNrOceniajacego = strTNtoBool(pokazNrOceniajacego);
  }

  public String getPokazOceniajacego() {
    return boolToTNstr(pokazOceniajacego);
  }

  public void setPokazOceniajacego(String pokazOceniajacego) {
    this.pokazOceniajacego = strTNtoBool(pokazOceniajacego);
  }

  public String getPokazStanowisko() {
    return boolToTNstr(pokazStanowisko);
  }

  public void setPokazStanowisko(String pokazStanowisko) {
    this.pokazStanowisko = strTNtoBool(pokazStanowisko);
  }

  public String getPokazRodzajUmowy() {
    return boolToTNstr(pokazRodzajUmowy);
  }

  public void setPokazRodzajUmowy(String pokazRodzajUmowy) {
    this.pokazRodzajUmowy = strTNtoBool(pokazRodzajUmowy);
  }

  public Boolean getPokazPoziomOceny() {
    return pokazPoziomOceny;
  }

  public void setPokazPoziomOceny(Boolean pokazPoziomOceny) {
    this.pokazPoziomOceny = pokazPoziomOceny;
  }

  public String getPokazTerminOceny() {
    return boolToTNstr(pokazTerminOceny);
  }

  public void setPokazTerminOceny(String pokazTerminOceny) {
    this.pokazTerminOceny = strTNtoBool(pokazTerminOceny);
  }

  public String getPokazDatePoprzedniejOceny() {
    return boolToTNstr(pokazDatePoprzedniejOceny);
  }

  public void setPokazDatePoprzedniejOceny(String pokazDatePoprzedniejOceny) {
    this.pokazDatePoprzedniejOceny = strTNtoBool(pokazDatePoprzedniejOceny);
  }

  public String getPokazPoziomPoprzedniejOceny() {
    return boolToTNstr(pokazPoziomPoprzedniejOceny);
  }

  public void setPokazPoziomPoprzedniejOceny(String pokazPoziomPoprzedniejOceny) {
    this.pokazPoziomPoprzedniejOceny = strTNtoBool(pokazPoziomPoprzedniejOceny);
  }

  public String getPokazPoprzedniStopienSluzbowy() {
    return boolToTNstr(pokazPoprzedniStopienSluzbowy);
  }

  public void setPokazPoprzedniStopienSluzbowy(String pokazPoprzedniStopienSluzbowy) {
    this.pokazPoprzedniStopienSluzbowy = strTNtoBool(pokazPoprzedniStopienSluzbowy);
  }

  public String getPokazDatePrzyznaniaOstatniegoStopnia() {
    return boolToTNstr(pokazDatePrzyznaniaOstatniegoStopnia);
  }

  public void setPokazDatePrzyznaniaOstatniegoStopnia(
      String pokazDatePrzyznaniaOstatniegoStopnia) {
    this.pokazDatePrzyznaniaOstatniegoStopnia = strTNtoBool(pokazDatePrzyznaniaOstatniegoStopnia);
  }

  public Date getTerminOcenyOd() {
    return terminOcenyOd;
  }

  public void setTerminOcenyOd(Date terminOcenyOd) {
    this.terminOcenyOd = terminOcenyOd;
  }

  public Date getTerminOcenyDo() {
    return terminOcenyDo;
  }

  public void setTerminOcenyDo(Date terminOcenyDo) {
    this.terminOcenyDo = terminOcenyDo;
  }

  public Date getOkresOcenyOd() {
    return okresOcenyOd;
  }

  public void setOkresOcenyOd(Date okresOcenyOd) {
    this.okresOcenyOd = okresOcenyOd;
  }

  public Date getOkresOcenyDo() {
    return okresOcenyDo;
  }

  public void setOkresOcenyDo(Date okresOcenyDo) {
    this.okresOcenyDo = okresOcenyDo;
  }

  public SortujWedlug getSortujWedlug() {
    return sortujWedlug;
  }

  public void setSortujWedlug(
      SortujWedlug sortujWedlug) {
    this.sortujWedlug = sortujWedlug;
  }

  public String getSortujRosnacoMalejaco() {
    return sortujRosnacoMalejaco;
  }

  public void setSortujRosnacoMalejaco(String sortujRosnacoMalejaco) {
    this.sortujRosnacoMalejaco = sortujRosnacoMalejaco;
  }


  public Long getJednostkaId() {
    return jednostkaId;
  }

  public void setJednostkaId(Long jednostkaId) {
    this.jednostkaId = jednostkaId;
  }

  public Long getWydzialId() {
    return wydzialId;
  }

  public void setWydzialId(Long wydzialId) {
    this.wydzialId = wydzialId;
  }

  private static Boolean strTNtoBool(String val) {
    return val.equals("T");
  }

  private static String boolToTNstr(Boolean val) {
    if (val == null) {
      return "N";
    }

    if (val) {
      return "T";
    } else {
      return "N";
    }
  }
}
