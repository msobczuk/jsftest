package com.comarch.egeria.pp.OcenaOkresowa.model;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

import javax.persistence.Entity;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.getQarterValue;

@Entity
public class Ocena extends PptOceny {




    public List<OcenaWartosc> getOcenaWaroscis(){
        return (List) super.getPptOcenyWartoscis();
    }



    public String getRodzajWslOpis(){
        return this.getSlownik("OCE_RODZAJE").findAndFormat(this.getOnagRodzaj(),"%rv_meaning$s");
    }

    public SqlDataSelectionsHandler getSlKryteria(){
        String kryteriaSlNazwa = this.getSlownik("OCE_RODZAJE").findAndFormat(this.getOnagRodzaj(), "%rv_high_value$s");
        SqlDataSelectionsHandler ret = this.getSlownik(kryteriaSlNazwa);
        return ret;
    }


    @Override
    public void setOnagRodzaj(String onagRodzaj) {
        boolean eq = eq(this.onagRodzaj, onagRodzaj);
        super.setOnagRodzaj(onagRodzaj);
        if (!eq){

            clearOcenaWartoscis();

            for (DataRow r : this.getSlKryteria().getData()) {
                OcenaWartosc w = new OcenaWartosc();
                w.setOwartKryterium(r.getIdAsString());
                double rv_high_value = Double.parseDouble(r.getAsString("rv_high_value").replace(",","."));
                w.setOwartWaga(BigDecimal.valueOf(rv_high_value));
                this.addPptOcenyWartosci(w);
            }

            ustawAktualnyRokKwartal(onagRodzaj);
        }
    }

    private void ustawAktualnyRokKwartal(String onagRodzaj) {

        LocalDate now = LocalDate.now();
        int rok = now.getYear();
        int mc = now.getMonthValue();
        int q = getQarterValue(now);

        if ("K".equals(onagRodzaj) && this.getOnagKwartal()==null){// ustaw ostatni kwartal
            this.setOnagRok(rok);
            this.setOnagKwartal(q);
        }else if ("R".equals(onagRodzaj) && this.getOnagKwartal()==null){ //ostatni rok
            if (mc < 2) rok-=1;
            this.setOnagRok(rok);
        }
    }


    public void mvRok(int step){
        Integer r = this.getOnagRok();
        r+=step;
        this.setOnagRok(r);
    }

    public void mvKwartal(int step){

        Integer r = this.getOnagRok();
        Integer q = this.getOnagKwartal();

        q += step;

        if (q>4){
            q = 1;
            r +=1;
        } else if(q<1){
            q = 4;
            r -=1;
        }

        this.setOnagKwartal(q);
        this.setOnagRok(r);
    }


    public LocalDate getDataOd(){
        if ("R".equals(this.getOnagRodzaj())){
            return LocalDate.of(this.getOnagRok(),1,1);
        } else if ("K".equals(this.getOnagRodzaj())) {
            if (this.getOnagKwartal()==null) return null;
            int m = (this.getOnagKwartal()-1)*3+1;
            return LocalDate.of(this.getOnagRok(),m,1);
        }
        return null;
    }


    public LocalDate getDataDo(){
        if (this.getOnagRok()==null) return null;
        if ("R".equals(this.getOnagRodzaj())){
            return LocalDate.of(this.getOnagRok(),12,31);
        } else if ("K".equals(this.getOnagRodzaj())) {
            if (this.getOnagKwartal()==null) return null;
            return this.getDataOd().plusMonths(3).minusDays(1);
        }
        return null;
    }


    public void clearOcenaWartoscis() {
        for (OcenaWartosc o : this.getOcenaWaroscis().stream().collect(Collectors.toList())) {
            this.removePptOcenyWartosci(o);
        }
    }

    public BigDecimal getWynikE3(){
        BigDecimal ret = BigDecimal.valueOf(0.0);
        for (OcenaWartosc ow : this.getOcenaWaroscis()) {
            if (ow.getWynikE3()==null) return  null;
            ret = ret.add(ow.getWynikE3());
        }
        return ret.setScale(2, RoundingMode.HALF_UP);
    }



}
