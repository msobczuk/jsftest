package com.comarch.egeria.pp.OcenaOkresowa.model;

import java.io.Serializable;
import javax.persistence.*;

import com.comarch.egeria.pp.data.model.ModelBase;
import org.hibernate.annotations.DiscriminatorFormula;

import java.math.BigDecimal;


/**
 * The persistent class for the PPT_OCENY_WARTOSCI database table.
 * 
 */
@Entity
@Table(name="PPT_OCENY_WARTOSCI")
@DiscriminatorFormula("'OcenaWartosc'")
@NamedQuery(name="PptOcenyWartosci.findAll", query="SELECT p FROM PptOcenyWartosci p")
public class PptOcenyWartosci extends ModelBase implements Serializable {
	static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_OCENY_WARTOSCI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OCENY_WARTOSCI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_OCENY_WARTOSCI")
	@Column(name="OWART_ID")
	long owartId;

	@Column(name="OWART_KRYTERIUM")
	String owartKryterium;

	@Column(name="OWART_WAGA")
	BigDecimal owartWaga = BigDecimal.valueOf(1.0);

	@Column(name="OWART_WARTOSC_E1")
	BigDecimal owartWartoscE1;

	@Column(name="OWART_WARTOSC_E2")
	BigDecimal owartWartoscE2;

	@Column(name="OWART_WARTOSC_E3")
	BigDecimal owartWartoscE3;

	//bi-directional many-to-one association to PptOceny
	@ManyToOne
	@JoinColumn(name="OWART_ONAG_ID")
	PptOceny pptOceny;

	public PptOcenyWartosci() {
	}

	public long getOwartId() {
		this.onRXGet("owartId");
		return this.owartId;
	}

	public void setOwartId(long owartId) {
		this.onRXSet("owartId", this.owartId, owartId);
		this.owartId = owartId;
	}

	public String getOwartKryterium() {
		this.onRXGet("owartKryterium");
		return this.owartKryterium;
	}

	public void setOwartKryterium(String owartKryterium) {
		this.onRXSet("owartKryterium", this.owartKryterium, owartKryterium);
		this.owartKryterium = owartKryterium;
	}

	public BigDecimal getOwartWartoscE1() {
		this.onRXGet("owartWartoscE1");
		return this.owartWartoscE1;
	}

	public void setOwartWartoscE1(BigDecimal owartWartoscE1) {
		this.onRXSet("owartWartoscE1", this.owartWartoscE1, owartWartoscE1);
		this.owartWartoscE1 = owartWartoscE1;
	}

	public BigDecimal getOwartWartoscE2() {
		this.onRXGet("owartWartoscE2");
		return this.owartWartoscE2;
	}

	public void setOwartWartoscE2(BigDecimal owartWartoscE2) {
		this.onRXSet("owartWartoscE2", this.owartWartoscE2, owartWartoscE2);
		this.owartWartoscE2 = owartWartoscE2;
	}

	public BigDecimal getOwartWartoscE3() {
		this.onRXGet("owartWartoscE3");
		return this.owartWartoscE3;
	}

	public void setOwartWartoscE3(BigDecimal owartWartoscE3) {
		this.onRXSet("owartWartoscE3", this.owartWartoscE3, owartWartoscE3);
		this.owartWartoscE3 = owartWartoscE3;
	}

	public PptOceny getPptOceny() {
		this.onRXGet("pptOceny");
		return this.pptOceny;
	}

	public void setPptOceny(PptOceny pptOceny) {
		this.onRXSet("pptOceny", this.pptOceny, pptOceny);
		this.pptOceny = pptOceny;
	}

	public BigDecimal getOwartWaga() {
		this.onRXGet("owartWaga");
		return owartWaga;
	}

	public void setOwartWaga(BigDecimal owartWaga) {
		this.onRXSet("owartWaga", this.owartWaga, owartWaga);
		this.owartWaga = owartWaga;
	}


}
