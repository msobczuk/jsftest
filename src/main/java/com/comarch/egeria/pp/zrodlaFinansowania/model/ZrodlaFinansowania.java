package com.comarch.egeria.pp.zrodlaFinansowania.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.zrodlaFinansowania.ZrodlaFinansowaniaBean;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import static com.comarch.egeria.Utils.*;


@Entity
@Table(name="PPT_ZRODLA_FINANSOWANIA")
@NamedQuery(name="ZrodlaFinansowania.findAll", query="SELECT p FROM ZrodlaFinansowania p")
public class ZrodlaFinansowania implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ZRODLA_FINANSOWANIA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ZRODLA_FINANSOWANIA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ZRODLA_FINANSOWANIA")	
	@Column(name="ZFI_ID")
	private long zfiId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ZFI_AUDYT_DM")
	private Date zfiAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ZFI_AUDYT_DT")
	private Date zfiAudytDt;

	@Column(name="ZFI_AUDYT_KM")
	private String zfiAudytKm;

	@Column(name="ZFI_AUDYT_LM")
	private String zfiAudytLm;

	@Column(name="ZFI_AUDYT_UM")
	private String zfiAudytUm;

	@Column(name="ZFI_AUDYT_UT")
	private String zfiAudytUt;

	@Column(name="ZFI_ENCJA_ID")
	private Long zfiEncjaId;

	@Column(name="ZFI_KATEGORIA")
	private String zfiKategoria;

	@Column(name="ZFI_KWOTA")
	private Double zfiKwota;

	@Column(name="ZFI_OPIS")
	private String zfiOpis;

	@Column(name="ZFI_PROCENT")
	private Double zfiProcent;

	@Column(name="ZFI_SK_ID")
	private Long zfiSkId;

	//bi-directional many-to-one association to PptAngaze
	@OneToMany(mappedBy="zrodlaFinansowania", cascade={CascadeType.ALL} , orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("ZFA_ID ASC")
	private List<Angaze> angazes = new ArrayList<>();


	@Transient
	private ZrodlaFinansowaniaBean zfBean; // m.in. kwota do podziału między ZF

	public ZrodlaFinansowaniaBean getZfBean() {

		if (this.zfBean==null){
			this.zfBean = (ZrodlaFinansowaniaBean) AppBean.getViewScopeBean("zrodlaFinansowaniaBean");
		}

		return zfBean;
	}

	public void setZfBean(ZrodlaFinansowaniaBean zfBean) {
		this.zfBean = zfBean;
	}



	public ZrodlaFinansowania() {
	}

	public long getZfiId() {
		return this.zfiId;
	}

	public void setZfiId(long zfiId) {
		this.zfiId = zfiId;
	}

	public Date getZfiAudytDm() {
		return this.zfiAudytDm;
	}

	public void setZfiAudytDm(Date zfiAudytDm) {
		this.zfiAudytDm = zfiAudytDm;
	}

	public Date getZfiAudytDt() {
		return this.zfiAudytDt;
	}

	public void setZfiAudytDt(Date zfiAudytDt) {
		this.zfiAudytDt = zfiAudytDt;
	}

	public String getZfiAudytKm() {
		return this.zfiAudytKm;
	}

	public void setZfiAudytKm(String zfiAudytKm) {
		this.zfiAudytKm = zfiAudytKm;
	}

	public String getZfiAudytLm() {
		return this.zfiAudytLm;
	}

	public void setZfiAudytLm(String zfiAudytLm) {
		this.zfiAudytLm = zfiAudytLm;
	}

	public String getZfiAudytUm() {
		return this.zfiAudytUm;
	}

	public void setZfiAudytUm(String zfiAudytUm) {
		this.zfiAudytUm = zfiAudytUm;
	}

	public String getZfiAudytUt() {
		return this.zfiAudytUt;
	}

	public void setZfiAudytUt(String zfiAudytUt) {
		this.zfiAudytUt = zfiAudytUt;
	}

	public Long getZfiEncjaId() {
		return this.zfiEncjaId;
	}

	public void setZfiEncjaId(Long zfiEncjaId) {
		this.zfiEncjaId = zfiEncjaId;
	}

	public String getZfiKategoria() {
		return this.zfiKategoria;
	}

	public void setZfiKategoria(String zfiKategoria) {
		this.zfiKategoria = zfiKategoria;
	}


	private void przeliczAngaze() {
		for (Angaze a : this.angazes) {
			double procent = nz(a.getZfaProcent());
			double kwotaAng = round((nz(this.zfiKwota) * procent) / 100, 2);
			a.setZfaKwota(kwotaAng);
		}
	}

	public Double getZfiKwota() {
		return this.zfiKwota;
	}

	public void setZfiKwota(Double zfiKwota) {
		boolean eq = eq(this.zfiKwota, zfiKwota);
		this.zfiKwota = zfiKwota;

		if(!eq && this.getZfBean()!=null){
			double kwotaZFTotal = nz(this.zfBean.getDoWykorzystaniaNaZrodla());
			double procentZF = round(nz(zfiKwota) / kwotaZFTotal * 100, 2);

			if (Double.isFinite(procentZF))
			this.zfiProcent = procentZF;

			przeliczAngaze();
		}
	}

	public void setZfiProcent(Double zfiProcent) {
		boolean eq = eq(this.zfiProcent, zfiProcent);
		this.zfiProcent = zfiProcent;
		if (!eq && this.getZfBean()!=null){
			double kwotaZFTotal = nz(this.getZfBean().getDoWykorzystaniaNaZrodla());
			double kwotaZF = round(((nz(zfiProcent) * kwotaZFTotal) / 100), 2);

			if (Double.isFinite(kwotaZF))
				this.zfiKwota = kwotaZF;
			else
				this.zfiKwota = 0.0;

			przeliczAngaze();
		}

	}


	public String getZfiOpis() {
		return this.zfiOpis;
	}

	public void setZfiOpis(String zfiOpis) {
		this.zfiOpis = zfiOpis;
	}

	public Double getZfiProcent() {
		return this.zfiProcent;
	}
	public Long getZfiSkId() {
		return this.zfiSkId;
	}

	public void setZfiSkId(Long zfiSkId) {
		this.zfiSkId = zfiSkId;
	}

	public List<Angaze> getAngazes() {
		return this.angazes;
	}

	public void setAngazes(List<Angaze> angazes) {
		this.angazes = angazes;
	}
	
	public void addNewAngaze() {
		this.addAngaze(new Angaze());
	}

	public Angaze addAngaze(Angaze angaz) {

		double sumProcent = getSumAngazeProcent();
		double sumKwota = getSumAngazeKwota();


		this.getAngazes().add(angaz);
		angaz.setZrodlaFinansowania(this);

		double procent = 100d - sumProcent;
		if (procent<0d) procent=0d;
		angaz.setZfaProcent(procent);

		double kwota = nz(this.zfiKwota) - sumKwota;
		if (kwota<0d) kwota = 0d;
		angaz.setZfaKwota(kwota);

		return angaz;
	}

	public double getSumAngazeKwota() {
		return this.getAngazes().stream().mapToDouble(a -> a.getZfaKwota()).sum();
	}

	public double getSumAngazeProcent() {
		return this.getAngazes().stream().mapToDouble(a -> a.getZfaProcent()).sum();
	}

	public Angaze removeAngaze(Angaze angaze) {
		getAngazes().remove(angaze);
		angaze.setZrodlaFinansowania(null);

		return angaze;
	}
	



}
