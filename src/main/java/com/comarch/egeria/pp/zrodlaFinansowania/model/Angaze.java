package com.comarch.egeria.pp.zrodlaFinansowania.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import static com.comarch.egeria.Utils.*;


@Entity
@Table(name="PPT_ANGAZE")
@NamedQuery(name="Angaze.findAll", query="SELECT p FROM Angaze p")
public class Angaze implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ANGAZE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ANGAZE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ANGAZE")	
	@Column(name="ZFA_ID")
	private long zfaId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ZFA_AUDYT_DM")
	private Date zfaAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ZFA_AUDYT_DT")
	private Date zfaAudytDt;

	@Column(name="ZFA_AUDYT_KM")
	private String zfaAudytKm;

	@Column(name="ZFA_AUDYT_LM")
	private String zfaAudytLm;

	@Column(name="ZFA_AUDYT_UM")
	private String zfaAudytUm;

	@Column(name="ZFA_AUDYT_UT")
	private String zfaAudytUt;

	@Column(name="ZFA_KWOTA")
	private Double zfaKwota;

	@Column(name="ZFA_OPIS")
	private String zfaOpis;

	@Column(name="ZFA_PROCENT")
	private Double zfaProcent;

	@Column(name="ZFA_WNR_ID")
	private Long zfaWnrId;

	//bi-directional many-to-one association to PptZrodlaFinansowania
	@ManyToOne
	@JoinColumn(name="ZFA_ZFI_ID")
	private ZrodlaFinansowania zrodlaFinansowania;

	public Angaze() {
	}

	public long getZfaId() {
		return this.zfaId;
	}

	public void setZfaId(long zfaId) {
		this.zfaId = zfaId;
	}

	public Date getZfaAudytDm() {
		return this.zfaAudytDm;
	}

	public void setZfaAudytDm(Date zfaAudytDm) {
		this.zfaAudytDm = zfaAudytDm;
	}

	public Date getZfaAudytDt() {
		return this.zfaAudytDt;
	}

	public void setZfaAudytDt(Date zfaAudytDt) {
		this.zfaAudytDt = zfaAudytDt;
	}

	public String getZfaAudytKm() {
		return this.zfaAudytKm;
	}

	public void setZfaAudytKm(String zfaAudytKm) {
		this.zfaAudytKm = zfaAudytKm;
	}

	public String getZfaAudytLm() {
		return this.zfaAudytLm;
	}

	public void setZfaAudytLm(String zfaAudytLm) {
		this.zfaAudytLm = zfaAudytLm;
	}

	public String getZfaAudytUm() {
		return this.zfaAudytUm;
	}

	public void setZfaAudytUm(String zfaAudytUm) {
		this.zfaAudytUm = zfaAudytUm;
	}

	public String getZfaAudytUt() {
		return this.zfaAudytUt;
	}

	public void setZfaAudytUt(String zfaAudytUt) {
		this.zfaAudytUt = zfaAudytUt;
	}

	public Double getZfaKwota() {
		return this.zfaKwota;
	}

	public void setZfaKwota(Double zfaKwota) {
		boolean eq = eq(this.zfaKwota, zfaKwota);
		this.zfaKwota = zfaKwota;

		if (!eq && this.zrodlaFinansowania != null){
			double kwotaZF = nz(this.zrodlaFinansowania.getZfiKwota());
			double angProcent = round(nz(zfaKwota) / kwotaZF * 100, 2);
			if (Double.isFinite(angProcent))
			    this.zfaProcent = angProcent;
		}
		
	}

	public String getZfaOpis() {
		return this.zfaOpis;
	}

	public void setZfaOpis(String zfaOpis) {
		this.zfaOpis = zfaOpis;
	}

	public Double getZfaProcent() {
		return this.zfaProcent;
	}

	public void setZfaProcent(Double zfaProcent) {
		boolean eq = eq(this.zfaProcent, zfaProcent);
		this.zfaProcent = zfaProcent;

		if (!eq && this.zrodlaFinansowania != null){
			double kwotaZF = nz(this.zrodlaFinansowania.getZfiKwota());
			double angkwota = round(kwotaZF * nz(zfaProcent) / 100, 2);

			if (Double.isFinite(angkwota))
			    this.zfaKwota = angkwota;
			else
			    this.zfaKwota = 0.0;
		}
	}

	public Long getZfaWnrId() {
		return this.zfaWnrId;
	}

	public void setZfaWnrId(Long zfaWnrId) {
		this.zfaWnrId = zfaWnrId;
	}

	public ZrodlaFinansowania getZrodlaFinansowania() {
		return this.zrodlaFinansowania;
	}

	public void setZrodlaFinansowania(ZrodlaFinansowania zrodlaFinansowania) {
		this.zrodlaFinansowania = zrodlaFinansowania;
	}
	
	

}
