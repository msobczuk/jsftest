package com.comarch.egeria.pp.zrodlaFinansowania;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.delegacje.model.DelAngaze;
import com.comarch.egeria.pp.delegacje.model.DeltCeleDelegacji;
import com.comarch.egeria.pp.delegacje.model.DeltPozycjeKalkulacji;
import com.comarch.egeria.pp.delegacje.model.DeltSrodkiLokomocji;
import com.comarch.egeria.pp.delegacje.model.DeltZrodlaFinansowania;
import com.comarch.egeria.pp.zrodlaFinansowania.model.Angaze;
import com.comarch.egeria.pp.zrodlaFinansowania.model.ZrodlaFinansowania;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

import static com.comarch.egeria.Utils.nz;
import static com.comarch.egeria.Utils.round;


/**
 * @author Mikolaj.Golda
 * Klasa którą nalezy dodać poprzez Inject do klasy z której korzysta widok w którym chcemy korzystac z kontrolki cZrodlaFinansowania
 */
@ManagedBean
@Scope("view")
@Named
public class ZrodlaFinansowaniaBean extends SqlBean {


    private String kategoria = null;
    private Long idEncji = null;
    private Double doWykorzystaniaNaZrodla = 0.0;

    private List<ZrodlaFinansowania> zrodlaFinansowaniaList = new ArrayList<>();
    private Set<ZrodlaFinansowania> zrodlaFinansowaniaDoUsunieciaSet = new HashSet<>();

    @PostConstruct
    public void init() {

    }


    public String getKategoria() {
        return kategoria;
    }

    public String setKategoria(String kategoria) {
        this.kategoria = kategoria;
        return "";
    }

    public Long getIdEncji() {
        return idEncji;
    }

    public String setIdEncji(Long idEncji) {
        this.idEncji = idEncji;
        return "";
    }


    public String wczytajZrodlaFinansowaniaLista(String kategoria, Long idEncji, Double doWykorzystaniaNaZrodla) {

        if (this.zrodlaFinansowaniaList == null || !kategoria.equals(this.getKategoria()) || (idEncji != null && !idEncji.equals(this.getIdEncji()))) {

            this.setKategoria(kategoria);
            this.setIdEncji(idEncji);
            this.setDoWykorzystaniaNaZrodla(doWykorzystaniaNaZrodla);

            String hql = String.format(" FROM ZrodlaFinansowania zf WHERE zf.zfiKategoria = '%1$s' and zf.zfiEncjaId = %2$s ORDER BY zf.zfiId", kategoria, idEncji); //ORDER BY E.firstName DESC, E.salary DESC
            List ret = HibernateContext.query(hql);
            this.zrodlaFinansowaniaList = ret;

            if (this.zrodlaFinansowaniaList.isEmpty()){ //jesli lista jest pusta - dodaj pierwszy element
                addZFI();
                ZrodlaFinansowania zf = this.zrodlaFinansowaniaList.get(0);
                zf.setZfiProcent(100d);
            }

        }


        return "";
    }

    public void addZFI() {
        ZrodlaFinansowania zf = new ZrodlaFinansowania();
        //        zf.setZfBean(this);//... lazy getter

        zf.setZfiEncjaId(idEncji);
        zf.setZfiKategoria(kategoria);
        zf.setZfiId(0L);

        double sumProcentZf = this.zrodlaFinansowaniaList.stream().mapToDouble(z -> z.getZfiProcent()).sum();
        double zfiProcent = 100d - sumProcentZf;
        if (zfiProcent<0d)zfiProcent=0d;
        zf.setZfiProcent(zfiProcent);

        double sumKwotyZf = this.zrodlaFinansowaniaList.stream().mapToDouble(z -> z.getZfiKwota()).sum();
        double zfiKwota = nz(this.doWykorzystaniaNaZrodla) - sumKwotyZf;
        if (zfiKwota<0d) zfiKwota=0d;
        zf.setZfiKwota(zfiKwota);

        this.zrodlaFinansowaniaList.add(zf);

        zf.addNewAngaze();
    }

    public void removeZrodlaFinansowania(ZrodlaFinansowania zf) {
        this.zrodlaFinansowaniaList.remove(zf);
        this.zrodlaFinansowaniaDoUsunieciaSet.add(zf);
    }

    public Boolean validateBeforeSave() {

        Set<String> inval = new HashSet<String>();
        Double wykorzystano = 0.0;
        Double wykorzystanoNaAngaze = 0.0;
        Double wykorzystanoProcent = 0.0;
        Double wykorzystanoNaAngazeProcent = 0.0;

        for (ZrodlaFinansowania zf : zrodlaFinansowaniaList) {

            wykorzystanoNaAngaze = zf.getAngazes().stream().mapToDouble(a->a.getZfaKwota()).sum();
            wykorzystanoNaAngazeProcent = zf.getAngazes().stream().mapToDouble(a->a.getZfaProcent()).sum();

            if (zf.getZfiKwota() != null && zf.getZfiKwota().doubleValue() > wykorzystanoNaAngaze.doubleValue() && wykorzystanoNaAngazeProcent.doubleValue() != 100.0) {
                inval.add("Nie rozdzielono całej kwoty ze źródła finansowania na angaże.");
            }

            if (zf.getZfiKwota() != null && zf.getZfiKwota().doubleValue() < wykorzystanoNaAngaze.doubleValue() && wykorzystanoNaAngazeProcent.doubleValue() != 100.0) {
                inval.add("Na angaże rozdzielono większą kwote niż przydzielono do danego źródła finansowania.");
            }
        }



        wykorzystano = this.zrodlaFinansowaniaList.stream().mapToDouble(z->z.getZfiKwota()).sum();
        wykorzystanoProcent = this.zrodlaFinansowaniaList.stream().mapToDouble(z->z.getZfiProcent()).sum();


        if (doWykorzystaniaNaZrodla != null && doWykorzystaniaNaZrodla.doubleValue() > wykorzystano.doubleValue() && wykorzystanoProcent.doubleValue() != 100.0) {
            inval.add("Nie rozdzielono całej kwoty na źródła finansowania.");
        }

        if (doWykorzystaniaNaZrodla != null && doWykorzystaniaNaZrodla.doubleValue() < wykorzystano.doubleValue() && wykorzystanoProcent.doubleValue() != 100.0) {
            inval.add("Na źródła finansowania rozdzielono za dużą kwote.");
        }


        if (!inval.isEmpty()) {
            for (String msg : inval) {
                User.alert(msg);
            }
        }

        return inval.isEmpty();
    }


    /**
     * Metode zapisz należy wykonać po głównej metodzie zapisz w danym Beanie, można to wykonać w następujący sposób:
     * 1. dodajemy klase ZrodlaFinansowaniaBean jako pole w ten sposób:
     * np. w klasie TerminySzkolenBean
     *
     * @Inject ZrodlaFinansowaniaBean zrodlaFinansowaniaBean;
     * 2. w klasie TerminySzkolenBean wywołujemy metode ZrodlaFinansowaniaBean.zapisz() na końcu jej metody zapisz
     */
    public void zapisz(Long idEncji) {

        if (!validateBeforeSave())
            return;

        setIdEncji(idEncji);
        for (ZrodlaFinansowania zf : zrodlaFinansowaniaList) {
            zf.setZfiEncjaId(idEncji);
        }

        try (HibernateContext h = new HibernateContext()) {
            Session s = h.getSession();

            Transaction t = s.beginTransaction();

            for (ZrodlaFinansowania zf : zrodlaFinansowaniaDoUsunieciaSet) {
                s.delete(zf);
            }

            for (ZrodlaFinansowania zf : zrodlaFinansowaniaList) {
                Object entity = zf;
                entity = s.merge(entity);
                s.saveOrUpdate(entity);
            }
            t.commit();

            zrodlaFinansowaniaDoUsunieciaSet.clear();

            this.zrodlaFinansowaniaList = null;
            wczytajZrodlaFinansowaniaLista(this.kategoria, this.idEncji, this.doWykorzystaniaNaZrodla);

        }


    }




//    public Double sumaKwotAngazy(ZrodlaFinansowania zf) {
//        Double suma = 0.0;
//        for (Angaze a : zf.getAngazes()) {
//            suma += a.getZfaKwota();
//        }
//        return round(suma, 2);
//    }
//
//    public Double sumaKwotZf() {
//        Double suma = 0.0;
//        for (ZrodlaFinansowania z : zrodlaFinansowaniaList) {
//            suma += z.getZfiKwota();
//        }
//        return round(suma, 2);
//    }

//    public double ustawKwote(Double kwotaWej, ZrodlaFinansowania zf) {
//        Double ret = procentNaKwote(zf.getZfiProcent(), kwotaWej);
//        for (Angaze a : zf.getAngazes()) {
//            //a.setZfaKwota(procentNaKwote(a.getZfaProcent(), ret));
//        }
//        return ret;
//    }
//
//    public double ustawProcent(Double kwotaWej, ZrodlaFinansowania zf) {
//        for (Angaze a : zf.getAngazes()) {
//            a.setZfaKwota(procentNaKwote(a.getZfaProcent(), zf.getZfiKwota()));
//        }
//        return kwotaNaProcent(zf.getZfiKwota(), kwotaWej);
//    }

//    public Double procentNaKwote(Double procent, Double kwotaWej) {
//        return round(((procent * kwotaWej) / 100), 2);
//    }
//
//    public Double kwotaNaProcent(Double kwotaAkt, Double kwotaWej) {
//        return round(((100 * kwotaAkt) / kwotaWej), 2);
//    }

//    public static double round(double value, int places) {
//        if (places < 0)
//            throw new IllegalArgumentException();
//
//        long factor = (long) Math.pow(10, places);
//        value = value * factor;
//        long tmp = Math.round(value);
//        return (double) tmp / factor;
//    }

    public List<ZrodlaFinansowania> getZrodlaFinansowaniaLista() {
        return zrodlaFinansowaniaList;
    }

    public void setZrodlaFinansowaniaLista(List<ZrodlaFinansowania> zrodlaFinansowaniaLista) {
        this.zrodlaFinansowaniaList = zrodlaFinansowaniaLista;
    }

    public Double getDoWykorzystaniaNaZrodla() {
        return doWykorzystaniaNaZrodla;
    }

    public void setDoWykorzystaniaNaZrodla(Double doWykorzystaniaNaZrodla) {
        doWykorzystaniaNaZrodla = nz(doWykorzystaniaNaZrodla);
        this.doWykorzystaniaNaZrodla = doWykorzystaniaNaZrodla;

        if (!(this.getZrodlaFinansowaniaLista() == null))
            for (ZrodlaFinansowania zf : this.getZrodlaFinansowaniaLista()) {
                double zfiProcent = nz(zf.getZfiProcent());
                double kwotaZF = round(((nz(zfiProcent) * doWykorzystaniaNaZrodla) / 100), 2);
                zf.setZfiKwota(kwotaZF);
            }

    }


    //    /**
//     * Narazie wszedzie gdzie jest usuwany rodzic danego zrodła finansowania niestety musi zostac wywołana ta metoda
//     */
//    public static void usun(Long id, String kategoria) {
//        try (HibernateContext h = new HibernateContext()) {
//
//            Session s = h.getSession();
//            //Query queryDelAng = s.createQuery(String.format( "DELETE FROM Angaze a WHERE a.zfiKategoria = '%1$s' and zf.zfiEncjaId = %2$s" , kategoria, id));
//            Query queryDelZF = s.createQuery(String.format("DELETE FROM ZrodlaFinansowania zf WHERE zf.zfiKategoria = '%1$s' and zf.zfiEncjaId = %2$s", kategoria, id));
//            Transaction t = s.beginTransaction();
//            int delZF = queryDelZF.executeUpdate();
//            t.commit();
//
//
//            User.info("Usunięto źródła finansowania termiu szkolenia.");
//
//        } catch (Exception e) {
//            User.alert("Błąd w usuwaniu źródła finansowania terminu szkolenia.");
//            e.printStackTrace();
//        }
//
//    }
}
