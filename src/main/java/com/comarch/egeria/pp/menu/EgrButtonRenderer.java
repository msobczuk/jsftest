package com.comarch.egeria.pp.menu;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

import org.primefaces.component.button.Button;
import org.primefaces.util.HTML;

public class EgrButtonRenderer extends org.primefaces.component.button.ButtonRenderer {

	
	
	@Override
    public void encodeMarkup(FacesContext context, Button button) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
		String clientId = button.getClientId(context);
        String value = (String) button.getValue();
        String icon = button.getIcon();

		writer.startElement("button", button);
		writer.writeAttribute("id", clientId, "id");
		writer.writeAttribute("name", clientId, "name");
        writer.writeAttribute("type", "button", null);
		writer.writeAttribute("class", button.resolveStyleClass(), "styleClass");

		renderPassThruAttributes(context, button, HTML.BUTTON_ATTRS, HTML.CLICK_EVENT);

        if(button.isDisabled()) 
            writer.writeAttribute("disabled", "disabled", "disabled");
        
		writer.writeAttribute("onclick", buildOnclick(context, button), null);

		//icon
        if(!isValueBlank(icon)) {
            String defaultIconClass = button.getIconPos().equals("left") ? HTML.BUTTON_LEFT_ICON_CLASS : HTML.BUTTON_RIGHT_ICON_CLASS; 
            String iconClass = defaultIconClass + " " + icon;
            
            if ((""+icon).startsWith("fa") || "ctl-menu-icon".equals(icon)){
	            writer.startElement("i", null);
	            //writer.writeAttribute("class", iconClass, null);
                writer.writeAttribute("class", icon, null);
	            writer.endElement("i");
            }else if ((""+icon).startsWith("mdi-")){
                writer.startElement("i", null);
                writer.writeAttribute("class", "mdi "+icon, null);
                writer.endElement("i");
            }else{
				//PP: icon material-icons: <i class="material-icons">home</i>
				writer.startElement("i", null);
				writer.writeAttribute("class", "material-icons pp-commandbutton-icon", null);
				writer.writeText(icon!=null?icon:"folder_open", null);
				writer.endElement("i");
            }
        }
        
        //text
        writer.startElement("span", null);
        writer.writeAttribute("class", HTML.BUTTON_TEXT_CLASS, null);
        
        if(value == null) {
            writer.write("ui-button");
        }
        else {
            if(button.isEscape())
                writer.writeText(value, "value");
            else
                writer.write(value);
        }
        
        writer.endElement("span");
			
		writer.endElement("button");
    }
}
