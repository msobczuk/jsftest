package com.comarch.egeria.pp.menu.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the PPT_ADM_MENU database table.
 * 
 */
@Entity
@Table(name = "PPT_ADM_MENU")
@NamedQuery(name = "PptAdmMenu.findAll", query = "SELECT p FROM PptAdmMenu p")
public class PptAdmMenu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "TABLE_KEYGEN_PPT_ADM_MENU", table = "PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ADM_MENU", valueColumnName = "PKEY_VALUE", initialValue = 0, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "TABLE_KEYGEN_PPT_ADM_MENU")
	@Column(name = "MNU_ID")
	private long mnuId;

	@Temporal(TemporalType.DATE)
	@Column(name = "MNU_AUDYT_DM")
	private Date mnuAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name = "MNU_AUDYT_DT")
	private Date mnuAudytDt;

	@Column(name = "MNU_AUDYT_KM")
	private String mnuAudytKm;

	@Column(name = "MNU_AUDYT_LM")
	private String mnuAudytLm;

	@Column(name = "MNU_AUDYT_UM")
	private String mnuAudytUm;

	@Column(name = "MNU_AUDYT_UT")
	private String mnuAudytUt;

	@Column(name = "MNU_ACTION")
	private String mnuAction;

	@Column(name = "MNU_ICON")
	private String mnuIcon;

	@Column(name = "MNU_KOD")
	private String mnuKod;

	@Column(name = "MNU_LABEL")
	private String mnuLabel;

	@Column(name = "MNU_LOV")
	private String mnuLov;

	@Column(name = "MNU_LOV_EDITROWPAGE")
	private String mnuLovEditrowpage;

	@Column(name = "MNU_LOV_ROWSELECTEDACTON")
	private String mnuLovRowselectedacton;

	@Column(name = "MNU_LP")
	private Long mnuLp;

	@Column(name = "MNU_RENDERED")
	private String mnuRendered;

	@Column(name = "MNU_AJAX")
	private String mnuAjax;

	@Column(name = "MNU_URL")
	private String mnuUrl;

	@Column(name = "MNU_KAFELEK_LP")
	private Long mnuKafelekLp;

	@Column(name = "MNU_KAFELEK_LABEL")
	private String mnuKafelekLabel;

	@Column(name = "MNU_KAFELEK_BIG_SIZE")
	private String mnuKafelekBigSize;

	@Column(name = "MNU_KAFELEK_ICON")
	private String mnuKafelekIcon;

	// bi-directional many-to-one association to PptAdmMenu
	@ManyToOne
	@JoinColumn(name = "MNU_MNU_ID")
	private PptAdmMenu pptAdmMenu;

	// bi-directional many-to-one association to PptAdmMenu
	@OneToMany(mappedBy = "pptAdmMenu", cascade = { CascadeType.ALL }, orphanRemoval = true)
	@OrderBy("mnuLp ASC")
	@BatchSize(size = 20)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<PptAdmMenu> pptAdmMenus = new ArrayList<>();

	// bi-directional many-to-one association to PptAdmMenu
	// @OrderBy("mnuLp ASC")
	@OneToMany(mappedBy = "pptAdmMenu", cascade = { CascadeType.ALL }, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 20)
	private List<PptAdmMenuFu> pptAdmMenuFus = new ArrayList<>();

	public PptAdmMenu() {
	}

	public long getMnuId() {
		return this.mnuId;
	}

	public void setMnuId(long mnuId) {
		this.mnuId = mnuId;
	}

	public Date getMnuAudytDm() {
		return this.mnuAudytDm;
	}

	public void setMnuAudytDm(Date mnuAudytDm) {
		this.mnuAudytDm = mnuAudytDm;
	}

	public Date getMnuAudytDt() {
		return this.mnuAudytDt;
	}

	public void setMnuAudytDt(Date mnuAudytDt) {
		this.mnuAudytDt = mnuAudytDt;
	}

	public String getMnuAudytKm() {
		return this.mnuAudytKm;
	}

	public void setMnuAudytKm(String mnuAudytKm) {
		this.mnuAudytKm = mnuAudytKm;
	}

	public String getMnuAudytLm() {
		return this.mnuAudytLm;
	}

	public void setMnuAudytLm(String mnuAudytLm) {
		this.mnuAudytLm = mnuAudytLm;

		if (this.pptAdmMenu != null) {

			String audytLM = this.pptAdmMenu.getMnuAudytLm();

			if (audytLM.length() + 1 >= 99) {
				this.pptAdmMenu.setMnuAudytLm(audytLM.trim() + " ");
			} else {
				this.pptAdmMenu.setMnuAudytLm(audytLM + " ");
			}
		}

	}

	public String getMnuAudytUm() {
		return this.mnuAudytUm;
	}

	public void setMnuAudytUm(String mnuAudytUm) {
		this.mnuAudytUm = mnuAudytUm;
	}

	public String getMnuAudytUt() {
		return this.mnuAudytUt;
	}

	public void setMnuAudytUt(String mnuAudytUt) {
		this.mnuAudytUt = mnuAudytUt;
	}

	public String getMnuAction() {
		return this.mnuAction;
	}

	public void setMnuAction(String mnuAction) {
		this.mnuAction = mnuAction;
	}

	public String getMnuIcon() {
		return this.mnuIcon;
	}

	public void setMnuIcon(String mnuIcon) {
		this.mnuIcon = mnuIcon;
	}

	public String getMnuKod() {
		return this.mnuKod;
	}

	public void setMnuKod(String mnuKod) {
		this.mnuKod = mnuKod;
	}

	public String getMnuLabel() {
		return this.mnuLabel;
	}

	public void setMnuLabel(String mnuLabel) {
		this.mnuLabel = mnuLabel;
	}

	public String getMnuLov() {
		return this.mnuLov;
	}

	public void setMnuLov(String mnuLov) {
		this.mnuLov = mnuLov;
	}

	public String getMnuLovEditrowpage() {
		return this.mnuLovEditrowpage;
	}

	public void setMnuLovEditrowpage(String mnuLovEditrowpage) {
		this.mnuLovEditrowpage = mnuLovEditrowpage;
	}

	public String getMnuLovRowselectedacton() {
		return this.mnuLovRowselectedacton;
	}

	public void setMnuLovRowselectedacton(String mnuLovRowselectedacton) {
		this.mnuLovRowselectedacton = mnuLovRowselectedacton;
	}

	public Long getMnuLp() {
		return this.mnuLp;
	}

	public void setMnuLp(Long mnuLp) {
		this.mnuLp = mnuLp;
	}

	public String getMnuRendered() {
		return this.mnuRendered;
	}

	public void setMnuRendered(String mnuRendered) {
		this.mnuRendered = mnuRendered;
	}

	public String getMnuAjax() {
		return this.mnuAjax;
	}

	public void setMnuAjax(String mnuAjax) {
		this.mnuAjax = mnuAjax;
	}

	public String getMnuUrl() {
		return this.mnuUrl;
	}

	public void setMnuUrl(String mnuUrl) {
		this.mnuUrl = mnuUrl;
	}

	public Long getMnuKafelekLp() { return mnuKafelekLp; }

	public void setMnuKafelekLp(Long mnuKafelekLp) { this.mnuKafelekLp = mnuKafelekLp; }

	public String getMnuKafelekLabel() { return mnuKafelekLabel; }

	public void setMnuKafelekLabel(String mnuKafelekLabel) { this.mnuKafelekLabel = mnuKafelekLabel; }

	public String getMnuKafelekBigSize() { return mnuKafelekBigSize; }

	public void setMnuKafelekBigSize(String mnuKafelekBigSize) { this.mnuKafelekBigSize = mnuKafelekBigSize; }

	public String getMnuKafelekIcon() { return mnuKafelekIcon; }

	public void setMnuKafelekIcon(String mnuKafelekIcon) { this.mnuKafelekIcon = mnuKafelekIcon; }

	public PptAdmMenu getPptAdmMenu() {
		return this.pptAdmMenu;
	}

	public void setPptAdmMenu(PptAdmMenu pptAdmMenu) {
		this.pptAdmMenu = pptAdmMenu;
	}

	public List<PptAdmMenu> getPptAdmMenus() {
		return this.pptAdmMenus;
	}

	public void setPptAdmMenus(List<PptAdmMenu> pptAdmMenus) {
		this.pptAdmMenus = pptAdmMenus;
	}

	public PptAdmMenu addPptAdmMenus(PptAdmMenu pptAdmMenus) {
		getPptAdmMenus().add(pptAdmMenus);
		pptAdmMenus.setPptAdmMenu(this);

		return pptAdmMenus;
	}

	public PptAdmMenu removePptAdmMenus(PptAdmMenu pptAdmMenus) {
		getPptAdmMenus().remove(pptAdmMenus);
		pptAdmMenus.setPptAdmMenu(null);

		return pptAdmMenus;
	}

	public List<PptAdmMenuFu> getPptAdmMenuFus() {
		return this.pptAdmMenuFus;
	}

	public void setPptAdmMenuFus(List<PptAdmMenuFu> pptAdmMenuFus) {
		this.pptAdmMenuFus = pptAdmMenuFus;
	}

	public PptAdmMenuFu addPptAdmMenuFus(PptAdmMenuFu pptAdmMenuFus) {
		getPptAdmMenuFus().add(pptAdmMenuFus);
		pptAdmMenuFus.setPptAdmMenu(this);

		return pptAdmMenuFus;
	}

	public PptAdmMenuFu removePptAdmMenuFus(PptAdmMenuFu pptAdmMenuFus) {
		getPptAdmMenuFus().remove(pptAdmMenuFus);
		pptAdmMenuFus.setPptAdmMenu(null);

		return pptAdmMenuFus;
	}
	
	
	public String getPozycjaInfo(){
		String ret = this.mnuLabel;
		if (this.pptAdmMenu!=null && this.pptAdmMenu.mnuLabel!=null)
			ret = this.pptAdmMenu.getPozycjaInfo() + " -> "+ ret;
		return ret;
	}

}