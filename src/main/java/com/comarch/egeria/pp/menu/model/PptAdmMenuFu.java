package com.comarch.egeria.pp.menu.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the PPT_ADM_MENU_FU database table.
 * 
 */
@Entity
@Table(name="PPT_ADM_MENU_FU")
@NamedQuery(name="PptAdmMenuFu.findAll", query="SELECT p FROM PptAdmMenuFu p")
public class PptAdmMenuFu implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ADM_MENU_FU",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ADM_MENU_FU", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ADM_MENU_FU")	
	@Column(name="MNFU_ID")
	private long mnfuId;
	
	@Temporal(TemporalType.DATE)
	@Column(name="MNFU_AUDYT_DM")
	private Date mnfuAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="MNFU_AUDYT_DT")
	private Date mnfuAudytDt;

	@Column(name="MNFU_AUDYT_KM")
	private String mnfuAudytKm;

	@Column(name="MNFU_AUDYT_LM")
	private String mnfuAudytLm;

	@Column(name="MNFU_AUDYT_UM")
	private String mnfuAudytUm;

	@Column(name="MNFU_AUDYT_UT")
	private String mnfuAudytUt;

	@Column(name="MNFU_NAZWA_FU")
	private String mnfuNazwaFu;
	
	//bi-directional many-to-one association to PptAdmMenu
	@ManyToOne
	@JoinColumn(name="MNFU_MNU_ID")
	private PptAdmMenu pptAdmMenu;

	public PptAdmMenuFu() {
	}

	public long getMnfuId() {
		return this.mnfuId;
	}

	public void setMnfuId(long mnfuId) {
		this.mnfuId = mnfuId;
	}
	
	public Date getMnfuAudytDm() {
		return this.mnfuAudytDm;
	}

	public void setMnfuAudytDm(Date mnfuAudytDm) {
		this.mnfuAudytDm = mnfuAudytDm;
	}

	public Date getMnfuAudytDt() {
		return this.mnfuAudytDt;
	}

	public void setMnfuAudytDt(Date mnfuAudytDt) {
		this.mnfuAudytDt = mnfuAudytDt;
	}

	public String getMnfuAudytKm() {
		return this.mnfuAudytKm;
	}

	public void setMnfuAudytKm(String mnfuAudytKm) {
		this.mnfuAudytKm = mnfuAudytKm;
	}

	public String getMnfuAudytLm() {
		return this.mnfuAudytLm;
	}

	public void setMnfuAudytLm(String mnfuAudytLm) {
		this.mnfuAudytLm = mnfuAudytLm;
	}

	public String getMnfuAudytUm() {
		return this.mnfuAudytUm;
	}

	public void setMnfuAudytUm(String mnfuAudytUm) {
		this.mnfuAudytUm = mnfuAudytUm;
	}

	public String getMnfuAudytUt() {
		return this.mnfuAudytUt;
	}

	public void setMnfuAudytUt(String mnfuAudytUt) {
		this.mnfuAudytUt = mnfuAudytUt;
	}
	
	public String getMnfuNazwaFu() {
		return this.mnfuNazwaFu;
	}

	public void setMnfuNazwaFu(String mnfuNazwaFu) {
		this.mnfuNazwaFu = mnfuNazwaFu;
	}

	public PptAdmMenu getPptAdmMenu() {
		return this.pptAdmMenu;
	}

	public void setPptAdmMenu(PptAdmMenu pptAdmMenu) {
		this.pptAdmMenu = pptAdmMenu;
	}

}