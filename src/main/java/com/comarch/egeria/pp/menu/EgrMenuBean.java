package com.comarch.egeria.pp.menu;

import static com.comarch.egeria.Utils.*;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.primefaces.context.RequestContext;
import org.primefaces.event.NodeCollapseEvent;
import org.primefaces.event.NodeExpandEvent;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.TreeDragDropEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.Submenu;
import org.springframework.context.annotation.Scope;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.kontrolaUprawnien.KontrolaUprawnien;
import com.comarch.egeria.pp.kontrolaUprawnien.model.WebUprawnienia;
import com.comarch.egeria.pp.menu.model.PptAdmMenu;
import com.comarch.egeria.pp.menu.model.PptAdmMenuFu;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@ManagedBean
@Named
@Scope("session")
public class EgrMenuBean extends SqlBean {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	private DefaultMenuModel menuModel = null;
	

	@Inject
	private SessionBean sessionBean;

	@Inject
	User user;
	
	@PreDestroy
	private void predestroy() {
		this.sessionBean = null;
		this.user = null;
	}
	
	

	private TreeNode menuTreeRoot = null;
	private PptAdmMenu rootMenu = null;
	
	private TreeNode currentNode = null;
	private PptAdmMenu currentMenu = null;

	@PostConstruct
	public void init() {
		this.setRootMenu((PptAdmMenu) HibernateContext.get(PptAdmMenu.class, 1L));
	}
	
	

	public void onDragDrop(TreeDragDropEvent event) {
		TreeNode dragNode = event.getDragNode();
		TreeNode dropNode = event.getDropNode();
		PptAdmMenu newParent = (PptAdmMenu) dropNode.getData();
		PptAdmMenu mnu = (PptAdmMenu) dragNode.getData();
		if (newParent==null || mnu==null)
			return;

		
		PptAdmMenu oldParent = mnu.getPptAdmMenu();
		if (oldParent!=null){
			oldParent.removePptAdmMenus(mnu);
		}
		newParent.addPptAdmMenus(mnu);
		
		long dropIndex = event.getDropIndex() + 1;
		mnu.setMnuLp(dropIndex);
		for (PptAdmMenu pam : newParent.getPptAdmMenus()) {
			if (pam.getMnuId() != mnu.getMnuId()  && nz(pam.getMnuLp()) >= dropIndex) {
				pam.setMnuLp(pam.getMnuLp() + 1L);
			}
		}
//		System.out.println(oldParent.getMnuLabel() + " -/- " + mnu.getMnuLabel());
//		System.out.println(newParent.getMnuLabel() + " --> " + mnu.getMnuLabel() + "; lp: " + dropIndex);
		this.setCurrentNode(dragNode);
//		this.setCurrentNode(dragNode.getParent());
	}
	
	
	public void upateCurrentMnu(){
		if (this.currentNode==null)
			return;
		com.comarch.egeria.Utils.update("frmMenuData:mnuTree");
	}
	

	public void onNodeSelect(NodeSelectEvent event) {
		if (this.currentNode!=null && event.getTreeNode()!=null)
			this.currentNode.setSelected(false);
		
		event.getTreeNode().setSelected(true);
		
		this.setCurrentNode(event.getTreeNode());//		this.currentMenu = (PptAdmMenu) this.currentNode.getData();

	}
	
	public void onNodeUnselect(NodeSelectEvent event) {
		event.getTreeNode().setSelected(false);
	}

	public void onNodeExpand(NodeExpandEvent event) {
		event.getTreeNode().setExpanded(true);
	}
	
	public void onNodeCollapse(NodeCollapseEvent event) {
		event.getTreeNode().setExpanded(false);
	}
	
	
	
	public void dodajNode() {
		if ( !nz(this.currentMenu.getMnuUrl()).isEmpty() || !nz(this.currentMenu.getMnuAction()).isEmpty() ){
			User.alert("Nie można dodać podmenu dla podmenu");
			return;
		}
		
		PptAdmMenu parentMnu = this.currentMenu;
		PptAdmMenu newMnu = new PptAdmMenu();
		newMnu.setMnuLabel("Nowe podmenu");
		newMnu.setMnuRendered("T");
		Long lp = parentMnu.getPptAdmMenus().stream().map(x->nz(x.getMnuLp())).max( (a,b)-> a.compareTo(b) ).orElse(0L) + 1;
		newMnu.setMnuLp(lp);
		this.currentMenu.addPptAdmMenus(newMnu);
		TreeNode newNode = addsubTreeNodeToTreeMenu(newMnu, this.currentNode);
		this.setCurrentNode(newNode);
	}


	public void zapisz() {

		try {
			HibernateContext.saveOrUpdate(this.currentMenu);
			User.info("Udana modyfikacja menu.");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
			return;
		}

	}

	
	
	public void usun(){
		if (this.currentNode==null || this.currentMenu==null)
			return;
		
		TreeNode pNode = this.currentNode.getParent();
		PptAdmMenu pMnu = this.currentMenu.getPptAdmMenu();

		if (!this.currentMenu.getPptAdmMenus().isEmpty()){
			User.alert("Nie można skasować - ten el. menu nie jest pusty.");
			return;
		}

		if ( this.rootMenu.equals( this.currentMenu ) || pNode==null || pMnu==null ){
			User.alert("Korzeń - nie można skasować głównego elemntu.");
			return;
		}
		
		
		try {
			pMnu.removePptAdmMenus(this.currentMenu);
			
			if (this.currentMenu.getMnuId()==0){ //instancja jeszcze nie zapisana
				User.info("Usunięto niezapisaną pozycję.");
			} else {
				HibernateContext.saveOrUpdate(pMnu);
				User.info("Udana modyfikacja struktury menu.");
			}
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
			return;
		}

		pNode.getChildren().remove(this.currentNode);
		this.setCurrentNode(pNode);
	}
	

	
	
	
	public void redirectToSzablonLOV(String nazwaLW, String editRowPage) {
		sessionBean.setMenuLWValue(nazwaLW);
		sessionBean.setMenuLWEditRowPage(editRowPage);
		com.comarch.egeria.Utils.executeScript("setIframeContentSrc('SzablonLOV.xhtml');");
	}
	
	

	public void generatePanelMenu() {

		List<PptAdmMenu> subMenus = rootMenu.getPptAdmMenus();

		for (PptAdmMenu subMenuItem : subMenus) {
			if ( user.isAdmin() || hasUserAnyPermission(subMenuItem)) {
				addsubMenuToPanelMenu(subMenuItem);
			}
		}
	}

	public void generateTreeMenu() {
		if (menuTreeRoot != null) {
			return;
		}

		menuTreeRoot = new DefaultTreeNode(rootMenu, null);
		if (this.currentNode==null)
			this.currentNode = menuTreeRoot;

		List<PptAdmMenu> subMenus = rootMenu.getPptAdmMenus();

		for (PptAdmMenu subMenuItem : subMenus) {
			if (subMenuItem != null)
				addsubTreeNodeToTreeMenu(subMenuItem, menuTreeRoot); // menuTree -
																	// root
																	// drzewa
		}
	}

	public TreeNode addsubTreeNodeToTreeMenu(PptAdmMenu menuNode, TreeNode parentNode) {
		TreeNode n = new DefaultTreeNode(menuNode, parentNode);
		for (PptAdmMenu item : menuNode.getPptAdmMenus()) {
			addsubTreeNodeToTreeMenu(item, n);
		}
		
		new DefaultTreeNode(null, n);//konieczne aby pokazał sie tez node expander jesli n nie ma dzieci (a chcemy miec możliwośc drag&drop)
		
		if (menuNode.getPptAdmMenus().isEmpty())
			n.setExpanded(true);
		
		return n;
	}

	private void addmenuItem(Submenu submenu, PptAdmMenu menuItem) {
		if (menuItem.getMnuRendered().equals("N"))
			return;

		if (user.isAdmin() || hasUserAnyPermission(menuItem)) {

			String label = menuItem.getMnuLabel() + "---" + menuItem.getMnuId();
			DefaultMenuItem item = new DefaultMenuItem(label, menuItem.getMnuIcon());

			String action = menuItem.getMnuAction();
			if (action != null) {
				item.setCommand(action);
			}

			String ajax = menuItem.getMnuAjax();
			if (ajax != null) {
				boolean ajaxBool = ajax.equals("T");
				item.setAjax(ajaxBool);
			}


			String nazwaLW = menuItem.getMnuLov();
			String editRowPage = menuItem.getMnuLovEditrowpage();

			if (nazwaLW != null && !nazwaLW.isEmpty()) {
				item.setCommand("#{egrMenuBean.redirectToSzablonLOV('" + nazwaLW + "', '" + editRowPage + "')}");
			}
			
			//statycznego EgrTemplate i EgrMenu
			String xhtml = menuItem.getMnuUrl();
			String urlParams = "";
			if (xhtml!=null && xhtml.contains("?")){ // np. /faces/DelegacjeMF/DelegacjaMFZagr.xhtml?new
				urlParams = xhtml.substring(xhtml.indexOf("?"));
				xhtml = xhtml.substring(0, xhtml.indexOf("?"));
			}
			
			String mappingUrl = AppBean.xhtmlMappings.get(""+xhtml);
			
			String url =  mappingUrl!=null ? mappingUrl + urlParams : xhtml;
			if (url!=null && url.startsWith("/"))
				url = url.substring(1);
			
			if (url!=null && !url.isEmpty()){
				item.setOnclick("setIframeContentSrc('" + url + "');");	
			}
			
			item.setAjax(true);
			submenu.getElements().add(item);
			
		}
	}

	private void addsubMenu(Submenu submenuParent, PptAdmMenu subMenuChild) {
		if (subMenuChild.getMnuRendered().equals("N"))
			return; // Przerwanie rekurencji gdy ten element ma nie być
					// renderowany => jego dzieci też nie będą

		if (user.isAdmin() || hasUserAnyPermission(subMenuChild)) {

			Submenu subMenuModel = new DefaultSubMenu(subMenuChild.getMnuLabel(), subMenuChild.getMnuIcon());

			submenuParent.getElements().add(subMenuModel);

			for (PptAdmMenu item : subMenuChild.getPptAdmMenus()) {
				if (item.getPptAdmMenus().isEmpty()) {
					addmenuItem(subMenuModel, item);
				} else {
					addsubMenu(subMenuModel, item);
				}
			}
		}
	}

	private void addsubMenuToPanelMenu(PptAdmMenu submenuItem) {
		if (submenuItem.getMnuRendered().equals("N"))
			return; // Przerwanie rekurencji gdy ten element ma nie być
					// renderowany => jego dzieci też nie będą

		if (user.isAdmin() || hasUserAnyPermission(submenuItem)) {

			Submenu subMenu = new DefaultSubMenu(submenuItem.getMnuLabel(), submenuItem.getMnuIcon());

			menuModel.addElement(subMenu);

			if (submenuItem.getPptAdmMenus() != null)
				for (PptAdmMenu item : submenuItem.getPptAdmMenus()) {
					if (item.getPptAdmMenus().isEmpty()) {
						addmenuItem(subMenu, item);
					} else {
						addsubMenu(subMenu, item);
					}
				}
		}
	}

	public void dodajFunkcjeUzytkowa() {
		PptAdmMenuFu menuFu = new PptAdmMenuFu();
		currentMenu.addPptAdmMenuFus(menuFu);
	}

	public void usunFunkcjeUzytkowa(PptAdmMenuFu menuFu) {
		if (currentMenu.getPptAdmMenuFus() != null && currentMenu.getPptAdmMenuFus().contains(menuFu)) {
			currentMenu.removePptAdmMenuFus(menuFu);
			// currentMenu.getPptAdmMenuFus().remove(menuFu);

		}
	}

	private boolean hasUserAnyPermission(PptAdmMenu pptAdmMenu) {
//		System.out.println("\n" + pptAdmMenu.getMnuLabel() + " ...");
				
		for (PptAdmMenuFu fu : pptAdmMenu.getPptAdmMenuFus()) {
			if (user.hasPermission(fu.getMnfuNazwaFu()))
				return true;
		}
		
		String xhtml = AppBean.getXhtml(pptAdmMenu.getMnuUrl());
		if (xhtml!=null){
			List<WebUprawnienia> wupr = KontrolaUprawnien.uprawnieniaHM.get(xhtml);
			
			if (wupr != null){
				for (WebUprawnienia upr : wupr) {
					if ( upr.getWuprFunkcjaUzytkowa() == null || user.hasAnyPermission(upr.getWuprFunkcjaUzytkowa()) ) {
//						if ( upr.getWuprZestawPolId() == null || ("" + kuid).equals( upr.getWuprZestawPolId() ) ) {
//							if ( upr.getWuprStanObiegu() == null || ("" + getStanObiegu()).equals( upr.getWuprStanObiegu())) {
								if("T".equals(upr.getWuprCzyWyswietlac()) || "T".equals(upr.getWuprCzyEdytowalne())) {
//									System.out.println(pptAdmMenu.getMnuLabel() + " -> xhtml: " + xhtml + " -> upr: " + upr.getWuprZestawPolId() + " / " + upr.getWuprOpis() );
									return true;	
								}
//							}
//						}
					}
				}
			}
		}
		
		//możliwe, że któreś podmenu ma coś dodane w PPT_WEB_UPRAWNIENIA albo w ppt_adm_menu_fu ...
		for (PptAdmMenu childMenu : pptAdmMenu.getPptAdmMenus()) {
			if (hasUserAnyPermission(childMenu))
				return true;
		}
		
		return false;
	}
	
	
	public void clearMenuModel(){
		this.menuModel = null;
	}
	
	public void reloadMenuModel() {
		this.menuModel = new DefaultMenuModel();
		generatePanelMenu();
	}

	
	
	public DefaultMenuModel getMenuModel() {
		if (this.menuModel == null) {
			reloadMenuModel();
		}
		return menuModel;
	}

	public void setMenuModel(DefaultMenuModel menuModel) {
		this.menuModel = menuModel;
	}

	public TreeNode getMenuTree() {
		return menuTreeRoot;
	}

	public void setMenuTree(TreeNode menuTree) {
		this.menuTreeRoot = menuTree;
	}

	public PptAdmMenu getRootMenu() {
		return rootMenu;
	}

	public void setRootMenu(PptAdmMenu rootMenu) {
		this.rootMenu = rootMenu;
		this.currentMenu = rootMenu;
	}

	TreeNode getCurrentNode() {
		return currentNode;
	}

	void setCurrentNode(TreeNode currentNode) {
		if (this.currentNode!=null)
			this.currentNode.setSelected(false);
		
		this.currentNode = currentNode;
		this.setCurrentMenu( (PptAdmMenu) this.currentNode.getData() );
		this.currentNode.setSelected(true);

		TreeNode p = this.currentNode.getParent();
		while (p!=null){
			p.setExpanded(true);
			p=p.getParent();
		}
	}

	public PptAdmMenu getCurrentMenu() {
		return currentMenu;
	}

	public void setCurrentMenu(PptAdmMenu currentMenu) {
		this.currentMenu = currentMenu;
	}

}
