package com.comarch.egeria.pp.menu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.FacesException;
import javax.faces.component.UIComponent;
import javax.faces.component.UIForm;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

import org.primefaces.component.api.AjaxSource;
import org.primefaces.component.api.UIOutcomeTarget;
import org.primefaces.component.menu.AbstractMenu;
import org.primefaces.component.menu.BaseMenuRenderer;
import org.primefaces.component.menu.Menu;
import org.primefaces.component.panelmenu.PanelMenu;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuItem;
import org.primefaces.model.menu.Submenu;
import org.primefaces.util.ComponentTraversalUtils;
import org.primefaces.util.WidgetBuilder;



public class EgrPanelMenuRenderer extends BaseMenuRenderer {
	

	@Override
	protected void encodeScript(FacesContext context, AbstractMenu abstractMenu) throws IOException {
	    PanelMenu menu = (PanelMenu) abstractMenu;
	    String clientId = menu.getClientId(context);
	    WidgetBuilder wb = getWidgetBuilder(context);
	    wb.initWithDomReady("PanelMenu", menu.resolveWidgetVar(), clientId).attr("stateful", menu.isStateful());
	    wb.finish();
	}

	
	@Override
	protected void encodeMarkup(FacesContext context, AbstractMenu abstractMenu) throws IOException {
	    ResponseWriter writer = context.getResponseWriter();
	    PanelMenu menu = (PanelMenu) abstractMenu;
	    String clientId = menu.getClientId(context);
	    String style = menu.getStyle();
	    String styleClass = menu.getStyleClass();
	    styleClass = styleClass == null ? PanelMenu.CONTAINER_CLASS : PanelMenu.CONTAINER_CLASS + " " + styleClass;
	
	    writer.startElement("div", menu);
	    writer.writeAttribute("id", clientId, "id");
	    writer.writeAttribute("class", styleClass, "styleClass");
	    if (style != null) {
	        writer.writeAttribute("style", style, "style");
	    }
	    writer.writeAttribute("role", "menu", null);
	
	    if (menu.getElementsCount() > 0) {
	        List<MenuElement> elements = menu.getElements();
	
	        for (MenuElement element : elements) {
	            if (element.isRendered() && element instanceof Submenu) {
	                encodeRootSubmenu(context, menu, (Submenu) element);
	            }
	        }
	    }
	
	    writer.endElement("div");
	}

	
	
	
	String submenulbl;
	
	
	protected void encodeRootSubmenu(FacesContext context, PanelMenu menu, Submenu submenu) throws IOException {
		
		
		    ResponseWriter writer = context.getResponseWriter();
		    String style = submenu.getStyle();
		    String icon = submenu.getIcon();
		    String styleClass = submenu.getStyleClass();
		    styleClass = styleClass == null ? PanelMenu.PANEL_CLASS : PanelMenu.PANEL_CLASS + " " + styleClass;
		    boolean expanded = submenu.isExpanded();
		    String headerClass = expanded ? PanelMenu.ACTIVE_HEADER_CLASS : PanelMenu.INACTIVE_HEADER_CLASS;
//		    String headerIconClass = expanded ? PanelMenu.ACTIVE_TAB_HEADER_ICON_CLASS : PanelMenu.INACTIVE_TAB_HEADER_ICON_CLASS;
//		    System.out.println("headerIconClass = " + headerIconClass);
//		    System.out.println("icon = " + icon);
//		    boolean hasIcon = (icon != null);
		    String contentClass = expanded ? PanelMenu.ACTIVE_ROOT_SUBMENU_CONTENT : PanelMenu.INACTIVE_ROOT_SUBMENU_CONTENT;
		
		    submenulbl = submenu.getLabel();
		    
		    //wrapper
		    writer.startElement("div", null);
		    writer.writeAttribute("class", styleClass, null);
		    if (style != null) {
		        writer.writeAttribute("style", style, null);
		    }
		
		    //header
		    writer.startElement("h3", null);
		    writer.writeAttribute("class", headerClass, null);
		    writer.writeAttribute("role", "tab", null);
		    writer.writeAttribute("tabindex", "0", null);
		
		    
	    
	    
			//PP: icon material-icons: <i class="material-icons">home</i>
//			writer.startElement("i", null);
//			writer.writeAttribute("class", "material-icons pp-submenu-icon", null);
//			writer.writeText(icon!=null?icon:"folder_open", null);
//			writer.endElement("i");

			if ((""+icon).startsWith("mdi-")){
				writer.startElement("i", null);
				writer.writeAttribute("class", "mdi " + icon + " pp-submenu-icon", null);
				writer.endElement("i");
			}else{
				//PP: icon material-icons: <i class="material-icons">home</i>
				writer.startElement("i", null);
				writer.writeAttribute("class", "material-icons pp-submenu-icon", null);
				writer.writeText(icon!=null?icon:"folder_open", null);
				writer.endElement("i");
			}

			
			
			//submenu.setId(submenu.getLabel().replaceAll(" ", "-"));
			

			//Kontrola Uprawnien 
			/*UIComponent uic = (UIComponent) submenu.getParent();
			String renderKU = ""+uic.getAttributes().get("renderKU");
			if ("true".equals(renderKU)){				//ikonka kontroli uprawnien
				writer.startElement("div", null);//prevent click bubbling DOM / div is noncliceakable
				writer.writeAttribute("class", "mnuBtnKu", null);
				writer.writeAttribute("onclick", "event.stopPropagation(); return false;", null);
				writer.writeAttribute("style", "z-index:99999; position:absolute; top:10px; right:0px;  opacity:0.4", null);
					writer.startElement("i", null);
						writer.writeAttribute("style", "font-size:100%!important", null);
						writer.writeAttribute("class", "material-icons pp-menuitem-icon", null);
						String kuid = KontrolaUprawnien.getKuid(submenu); //("menu_"+ submenu.getLabel()).replaceAll(" ", "-");
						writer.writeAttribute("onclick", "  showDlgKU( [ {name:'kuid' , value:'"+ kuid + "'}, {name:'class' , value:'"+ submenu.getClass().getName() + "'} ] );  ", null); //PF('dlgKU').show() 
						writer.writeText("settings", null);
					writer.endElement("i");
				writer.endElement("div");  
			}*/
			
			
			
			
			
			
			writer.startElement("a", null);
			writer.writeAttribute("href", "#", null);
			writer.writeAttribute("tabindex", "-1", null);
			writer.writeText(submenu.getLabel(), null);
			writer.endElement("a");
	
			writer.endElement("h3");
	
			// content
			writer.startElement("div", null);
			writer.writeAttribute("class", contentClass, null);
			writer.writeAttribute("role", "tabpanel", null);
			writer.writeAttribute("id", menu.getClientId(context) + "_" + submenu.getId(), null);
			writer.writeAttribute("tabindex", "0", null);
	
		    if (submenu.getElementsCount() > 0) {
		        List<MenuElement> elements = submenu.getElements();
		
		        writer.startElement("ul", null);
		        writer.writeAttribute("class", PanelMenu.LIST_CLASS, null);
		
		        //for (MenuElement element : elements) {
		        for (Object oElement : elements) {
		        	if (!(oElement instanceof MenuElement))
		        		continue;
		        	
		        	MenuElement element = (MenuElement) oElement;
		            if (element.isRendered()) {
		                if (element instanceof MenuItem) {
		                    MenuItem menuItem = (MenuItem) element;
		                    String containerStyle = menuItem.getContainerStyle();
		                    String containerStyleClass = menuItem.getContainerStyleClass();
		                    containerStyleClass = (containerStyleClass == null) ? Menu.MENUITEM_CLASS : Menu.MENUITEM_CLASS + " " + containerStyleClass;
		
		                    writer.startElement("li", null);
		                    writer.writeAttribute("class", containerStyleClass, null);
		                    if (containerStyle != null) {
		                        writer.writeAttribute("style", containerStyle, null);
		                    }
		                    encodeMenuItem(context, menu, menuItem);
		                    writer.endElement("li");
		                } else if (element instanceof Submenu) {
		                    encodeDescendantSubmenu(context, menu, (Submenu) element);
		                }
		            }
		        }
		
		        writer.endElement("ul");
		    }
		
		    writer.endElement("div");   //content
		
		    writer.endElement("div");   //wrapper
	}


	
	
	

	@Override
	protected void encodeMenuItemContent(FacesContext context, AbstractMenu menu, MenuItem menuitem) throws IOException {
	    ResponseWriter writer = context.getResponseWriter();
	    String icon = menuitem.getIcon();
	    Object value = menuitem.getValue();

	    String valueText = getLabelFromValue(String.valueOf(value));

	    
		// icon material-icons: <i class="material-icons">home</i>
//		writer.startElement("i", null);
//		writer.writeAttribute("class", "material-icons pp-menuitem-icon", null);
//		writer.writeText(icon!=null?icon:"null", null);
//		writer.endElement("i");

		if ((""+icon).startsWith("mdi-")){
			writer.startElement("i", null);
			writer.writeAttribute("class", "mdi " + icon + " pp-menuitem-icon", null);
			writer.endElement("i");
		}else{
			//PP: icon material-icons: <i class="material-icons">home</i>
			writer.startElement("i", null);
			writer.writeAttribute("class", "material-icons pp-menuitem-icon", null);
			writer.writeText(icon!=null?icon:"null", null);
			writer.endElement("i");
		}

		
	    writer.startElement("span", null);
	    writer.writeAttribute("class", AbstractMenu.MENUITEM_TEXT_CLASS, null);
	    
	    if(value != null) {
	        if (menuitem.isEscape()) {
				writer.writeText(valueText, "value");
			} else {
				writer.write(valueText);
			}
	    }
	    else if (menuitem.shouldRenderChildren()) {
	        renderChildren(context, (UIComponent) menuitem);
	    }
	
	    writer.endElement("span");
	    
		//Kontrola Uprawnien
		/*UIComponent mainmenu = ((UIComponent) menuitem).getParent().getParent();
		String renderKU = ""+mainmenu.getAttributes().get("renderKU");
		if ("true".equals(renderKU)){				//ikonka kontroli uprawnien			
			writer.startElement("div", null);//prevent click bubbling DOM / div is noncliceakable
			writer.writeAttribute("class", "mnuBtnKu", null);
			writer.writeAttribute("onclick", "event.stopPropagation(); return false;", null);
			writer.writeAttribute("style", "z-index:99999; position:absolute; top:10px; right:0px; opacity:0.4", null);
				writer.startElement("i", null);
					writer.writeAttribute("style", "font-size:100%!important", null);
					writer.writeAttribute("class", "material-icons pp-menuitem-icon", null);
					
//					UIComponent submenu = ((UIComponent) menuitem).getParent();
					String kuid = KontrolaUprawnien.getKuid(menuitem); // ("menu_"+ submenu.getId() + "_" + menuitem.getValue()).replaceAll(" ", "-");
//					if (submenu instanceof Submenu)
//						kuid = ("menu_"+ ((Submenu) submenu).getLabel() + "_" + menuitem.getValue()).replaceAll(" ", "-");
					writer.writeAttribute("onclick", "  showDlgKU( [ {name:'kuid' , value:'"+ kuid + "'}, {name:'class' , value:'"+ menuitem.getClass().getName() + "'} ] );  ", null); 
					writer.writeText("settings", null);
				writer.endElement("i");
			writer.endElement("div");  
		}*/
	}	


	private String getLabelFromValue(String menuItemValue) {
		return menuItemValue.substring(0, menuItemValue.indexOf("---"));
	}

	private String getIdFromValue(String menuItemValue) {
		return menuItemValue.substring(menuItemValue.indexOf("---") + 3);
	}
	
	

	protected void encodeDescendantSubmenu(FacesContext context, PanelMenu menu, Submenu submenu) throws IOException {
	    ResponseWriter writer = context.getResponseWriter();
	    String icon = submenu.getIcon();
	    String style = submenu.getStyle();
	    String styleClass = submenu.getStyleClass();
	    styleClass = styleClass == null ? PanelMenu.DESCENDANT_SUBMENU_CLASS : PanelMenu.DESCENDANT_SUBMENU_CLASS + " " + styleClass;
	    boolean expanded = submenu.isExpanded();
//	    String toggleIconClass = expanded ? PanelMenu.DESCENDANT_SUBMENU_EXPANDED_ICON_CLASS : PanelMenu.DESCENDANT_SUBMENU_COLLAPSED_ICON_CLASS;
	    String listClass = expanded ? PanelMenu.DESCENDANT_SUBMENU_EXPANDED_LIST_CLASS : PanelMenu.DESCENDANT_SUBMENU_COLLAPSED_LIST_CLASS;
	    boolean hasIcon = (icon != null);
	    String linkClass = (hasIcon) ? PanelMenu.MENUITEM_LINK_WITH_ICON_CLASS : PanelMenu.MENUITEM_LINK_CLASS;
	
	    writer.startElement("li", null);
	    writer.writeAttribute("id", submenu.getClientId(), null);
	    writer.writeAttribute("class", styleClass, null);
	    if (style != null) {
	        writer.writeAttribute("style", style, null);
	    }
	
	    writer.startElement("a", null);
	    writer.writeAttribute("class", linkClass, null);
	
	    //toggle icon
//	    writer.startElement("span", null);
//	    writer.writeAttribute("class", toggleIconClass, null);
//	    writer.endElement("span");
	
	    //user icon
//	    if (hasIcon) {
//	        writer.startElement("span", null);
//	        writer.writeAttribute("class", "ui-icon " + icon, null);
//	        writer.endElement("span");
//	    }
		//PP: icon material-icons: <i class="material-icons">home</i>
//		writer.startElement("i", null);
//		writer.writeAttribute("class", "material-icons pp-submenu-icon", null);
//		writer.writeText(icon!=null?icon:"folder_open", null);
//		writer.endElement("i");
		writer.startElement("i", null);
		writer.writeAttribute("class", "material-icons pp-menuitem-icon", null);
		writer.writeText(icon!=null?icon:"null", null);
		writer.endElement("i");  
	
	    //submenu label
	    writer.startElement("span", null);
	    writer.writeAttribute("class", PanelMenu.MENUITEM_TEXT_CLASS, null);
	    writer.writeText(submenu.getLabel(), null);
	    writer.endElement("span");
	
	    writer.endElement("a");
	
	    //submenu children
	    if (submenu.getElementsCount() > 0) {
	        List<MenuElement> elements = submenu.getElements();
	
	        writer.startElement("ul", null);
	        writer.writeAttribute("class", listClass, null);
	
	        for (MenuElement element : elements) {
	            if (element.isRendered()) {
	                if (element instanceof MenuItem) {
	                    writer.startElement("li", null);
	                    writer.writeAttribute("class", Menu.MENUITEM_CLASS, null);
	                    encodeMenuItem(context, menu, (MenuItem) element);
	                    writer.endElement("li");
	                } else if (element instanceof Submenu) {
	                    encodeDescendantSubmenu(context, menu, (Submenu) element);
	                }
	            }
	        }
	
	        writer.endElement("ul");
	    }
	
	    writer.endElement("li");
	}
	
	
	
	
	
	@Override
	protected void encodeMenuItem(FacesContext context, AbstractMenu menu, MenuItem menuitem) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String title = menuitem.getTitle();
        String style = menuitem.getStyle();
        boolean disabled = menuitem.isDisabled();
        String rel = menuitem.getRel();

	    String viewId = context.getViewRoot().getViewId();
	    //System.out.println("encodeMenuItemContent / viewId: " + viewId);
	    String url = menuitem.getUrl();


	    String id  = menuitem.getValue().toString();
	    
	    if ("/index.xhtml".equals(viewId))
	    	//if (context.getExternalContext().getSession(false)!=null)
	    	context.getExternalContext().getSessionMap().put("activeMnuId", "");
	    	
	    else if (viewId.equals(url)){
	    	//if (context.getExternalContext().getSession(false)!=null)
	    	context.getExternalContext().getSessionMap().put("activeMnuId", id);
	    }        

        writer.startElement("a", null);

        
        writer.writeAttribute("tabindex", "-1", null);

        writer.writeAttribute("menucode", (submenulbl + "_"+ getLabelFromValue(id)).replaceAll(" ", "-").trim(), null);

		String customId = getIdFromValue(id);

		writer.writeAttribute("id", customId, null);
//        if(shouldRenderId(menuitem)) {
//            writer.writeAttribute("id", menuitem.getClientId(), null);
//        }
        if (title != null) {
            writer.writeAttribute("title", title, null);
        }

        String styleClass = this.getLinkStyleClass(menuitem);
        if (disabled) {
            styleClass = styleClass + " ui-state-disabled";
        }

        
        if (context.getExternalContext().getSessionMap().get("activeMnuId").equals(id))
        	styleClass += " pp-menuitem-active";
        	
        writer.writeAttribute("class", styleClass, null);

        if(style != null) {
            writer.writeAttribute("style", style, null);
        }
        
        if(rel != null) {
            writer.writeAttribute("rel", rel, null);
        }

        if(disabled) {
            writer.writeAttribute("href", "#", null);
            writer.writeAttribute("onclick", "return false;", null);
        }
        else {
            setConfirmationScript(context, menuitem);
            String onclick = menuitem.getOnclick();

            //GET
            if(menuitem.getUrl() != null || menuitem.getOutcome() != null) {                
                String targetURL = getTargetURL(context, (UIOutcomeTarget) menuitem);
                writer.writeAttribute("href", targetURL, null);

                if(menuitem.getTarget() != null) {
                    writer.writeAttribute("target", menuitem.getTarget(), null);
                }
            }
            //POST
            else {
                writer.writeAttribute("href", "#", null);

                UIForm form = ComponentTraversalUtils.closestForm(context, menu);
                if(form == null) {
                    throw new FacesException("MenuItem must be inside a form element");
                }

                String command;
                if(menuitem.isDynamic()) {
                    String menuClientId = menu.getClientId(context);
                    Map<String,List<String>> params = menuitem.getParams();
                    if(params == null) {
                        params = new LinkedHashMap<String, List<String>>();
                    }
                    List<String> idParams = new ArrayList<String>();
                    idParams.add(menuitem.getId());
                    params.put(menuClientId + "_menuid", idParams);

                    command = menuitem.isAjax() ? buildAjaxRequest(context, menu, (AjaxSource) menuitem, form, params) : buildNonAjaxRequest(context, menu, form, menuClientId, params, true);
                } 
                else {
                    command = menuitem.isAjax() ? buildAjaxRequest(context, (UIComponent & AjaxSource) menuitem, form) : buildNonAjaxRequest(context, ((UIComponent) menuitem), form, ((UIComponent) menuitem).getClientId(context), true);
                }

                onclick = (onclick == null) ? command : onclick + ";" + command;
            }
            
            //onclick = onclick + "; alert(1)"; //TODO dodac ajaxa do ustawienia dynamicznie activeMenuId jesli menuitem nie ma zadnego url...

            if(onclick != null) {
                if(menuitem.requiresConfirmation()) {
                    writer.writeAttribute("data-pfconfirmcommand", onclick, null);
                    writer.writeAttribute("onclick", menuitem.getConfirmationScript(), "onclick");
                }
                else {
                    writer.writeAttribute("onclick", onclick, null);
                }
            }
        }

        encodeMenuItemContent(context, menu, menuitem);

        writer.endElement("a");
	}
	
}