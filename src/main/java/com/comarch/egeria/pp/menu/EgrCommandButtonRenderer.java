package com.comarch.egeria.pp.menu;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.commandbutton.CommandButtonRenderer;
import org.primefaces.util.HTML;

public class EgrCommandButtonRenderer extends CommandButtonRenderer {

	
	
	
	@Override
	protected void encodeMarkup(FacesContext context, CommandButton button) throws IOException {
		ResponseWriter writer = context.getResponseWriter();
		String clientId = button.getClientId(context);
		String type = button.getType();
        boolean pushButton = (type.equals("reset")||type.equals("button"));
        Object value = button.getValue();
        String icon = button.getIcon();
        String title = button.getTitle();
        String onclick = null;
        
        if (!button.isDisabled()) {
            String request = pushButton ? null : buildRequest(context, button, clientId);        
            onclick = buildDomEvent(context, button, "onclick", "click", "action", request);
        }

		writer.startElement("button", button);
		writer.writeAttribute("id", clientId, "id");
		writer.writeAttribute("name", clientId, "name");
        writer.writeAttribute("class", button.resolveStyleClass(), "styleClass");

		if(onclick != null) {
            if(button.requiresConfirmation()) {
                writer.writeAttribute("data-pfconfirmcommand", onclick, null);
                writer.writeAttribute("onclick", button.getConfirmationScript(), "onclick");
            }
            else
                writer.writeAttribute("onclick", onclick, "onclick");
		}
		
		renderPassThruAttributes(context, button, HTML.BUTTON_ATTRS, HTML.CLICK_EVENT);

        if(button.isDisabled()) writer.writeAttribute("disabled", "disabled", "disabled");

        
        //icon
        if(!isValueBlank(icon)) {
            String defaultIconClass = button.getIconPos().equals("left") ? HTML.BUTTON_LEFT_ICON_CLASS : HTML.BUTTON_RIGHT_ICON_CLASS; 
            String iconClass = defaultIconClass + " " + icon;
            
            if ((""+icon).startsWith("fa") || "ctl-menu-icon".equals(icon)) {

                writer.startElement("i", null);
                writer.writeAttribute("class", icon, null);
                writer.endElement("i");
            }else if ((""+icon).startsWith("mdi-")){
                writer.startElement("i", null);
                writer.writeAttribute("class", "mdi "+icon, null);
                writer.endElement("i");
            }else{

            	writer.startElement("i", null);
				writer.writeAttribute("class", "material-icons pp-commandbutton-icon", null);
				
				if("perm_data_setting".equals(icon)) //Jeżeli command button z tagu egr:ku
					writer.writeAttribute("style", "font-size:15px", null);

				Object iconSize = button.getAttributes().get("iconSize");
				if( !("".equals(""+iconSize)))
					writer.writeAttribute("style", "font-size:"+iconSize, null);
				
				writer.writeText(icon!=null?icon:"folder_open", null);
				writer.endElement("i");
            }
        }

        
        
//        //Kontrola Uprawnien
//        boolean renderKU = false;
//
//        UIComponent x = FacesContext.getCurrentInstance().getViewRoot().findComponent("templateBody");
//        
//        if (x!=null && "true".equals( ""+x.getAttributes().get("renderKU") ))
//        	renderKU = true;
//
//        if (renderKU  && ("clear".equals(icon) || "close".equals("icon") || "perm_data_setting".equals(icon) || clientId.endsWith("_WUPRbtn"))) 
//        	renderKU=false;
//        
//        if (renderKU){
//        	String kuid = KontrolaUprawnien.getKuid(button);  //("btn_"+ button.getClientId() + "_" + button.getValue()).replaceAll(" ", "-");
//        	if (kuid!=null && !"".equals(kuid) && !"ku-e".equals(kuid) && !kuid.startsWith("DLG_dlgKU")) {
//    			writer.startElement("div", null);//prevent click bubbling DOM / div is noncliceakable
//    			writer.writeAttribute("class", "mnuBtnKu", null);
//    			writer.writeAttribute("onclick", "event.stopPropagation(); return false;", null);
//    			writer.writeAttribute("style", "z-index:99999; position:absolute; top:0px; right:-2px; opacity:0.2", null);
//    				writer.startElement("i", null);
//    					writer.writeAttribute("style", "font-size:6px!important; margin:0!important; cursor:crosshair; ", null);
//    					writer.writeAttribute("class", "material-icons pp-menuitem-icon", null);
//    				  	writer.writeAttribute("onclick", "try { showDlgKU( [ {name:'kuid' , value:'"+ kuid + "'}, {name:'class' , value:'" + button.getClass().getName() +"'} ] ); } catch(err){}  ", null); 
//    					writer.writeText("settings", null);
//    				writer.endElement("i");
//    			writer.endElement("div");  
//        	}
//		}        
        
        
        
        
        
        
        //text
        writer.startElement("span", null);
        writer.writeAttribute("class", HTML.BUTTON_TEXT_CLASS, null);
        
        if(value == null) {
            //For ScreenReader
            String text = (title != null) ? title: "ui-button";
            
            writer.write(text);
        }
        else {
            if(button.isEscape())
                writer.writeText(value, "value");
            else
                writer.write(value.toString());
        }
        
        writer.endElement("span");
			
		writer.endElement("button");
	}
	
}
