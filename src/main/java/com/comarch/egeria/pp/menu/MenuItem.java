package com.comarch.egeria.pp.menu;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.comarch.egeria.pp.data.DataRow;

/*public*/abstract class MenuItem {

	private Long id; 
	private Long parentId;
	private String type;
	private String label;
	private String icon;
	private String url;
	private String action;
	private String nazwaLW;
	
	public MenuItem(DataRow r) {
		//this.type = "menuitem";
//		this.id =  ((BigDecimal) r.get("MNU_ID")).longValue() ;
//		this.parentId = ((BigDecimal) r.get("MNU_MNU_ID")).longValue() ;
		BigDecimal id = (BigDecimal) r.get("MNU_ID");
		BigDecimal parentId = (BigDecimal) r.get("MNU_MNU_ID");
		if(id != null)
			this.id = id.longValue();
		if(parentId != null)
			this.parentId = parentId.longValue();
		
		this.label = (String) r.get("MNU_LABEL");
		this.icon = (String) r.get("MNU_ICON");
		this.url =  (String) r.get("MNU_URL");
		this.setAction((String) r.get("MNU_ACTION"));
		this.nazwaLW = (String) r.get("MNU_LOV");
	}
	
	
	
	private ArrayList<MenuItem> menuItems  = new ArrayList<>();

	
	
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public ArrayList<MenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(ArrayList<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getNazwaLW() {
		return nazwaLW;
	}

	public void setNazwaLW(String nazwaLW) {
		this.nazwaLW = nazwaLW;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	
	
}
