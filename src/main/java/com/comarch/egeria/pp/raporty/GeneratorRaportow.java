package com.comarch.egeria.pp.raporty;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.raporty.domain.CsstRejestrWydrukowPliki;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@Named
@Scope("session")
public class GeneratorRaportow {

	
//	@Inject
//	JdbcDAO jdbc;
	
	//
	// TYMCZASOWE METODY DO TESTOW
	public void uruchomOcenaOKresowaCzescI(int zpt_oce_id) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("zpt_oce_id", zpt_oce_id);
		
		uruchomRaportOcean("Ocena okresowa czesc 1", params);
	}
	public void uruchomOcenaOKresowaCzescII(int zpt_oce_id) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("zpt_oce_id", zpt_oce_id);

		uruchomRaportOcean("Ocena okresowa czesc 2", params);
	}
	public void uruchomArkuszSluzbyPrzygotowawczej(int id_sluzby) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("id_sluzby", id_sluzby);

		uruchomRaportOcean("Arkusz sluzby przygotowawczej", params);
	}
	
	//RTF
		public void uruchomOcenaOKresowaCzescIRTF(int zpt_oce_id) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("zpt_oce_id", zpt_oce_id);
			
			uruchomRaportOceanRTF("Ocena okresowa czesc 1", params);
		}
		public void uruchomOcenaOKresowaCzescIIRTF(int zpt_oce_id) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("zpt_oce_id", zpt_oce_id);

			uruchomRaportOceanRTF("Ocena okresowa czesc 2", params);
		}
		public void uruchomArkuszSluzbyPrzygotowawczejRTF(int id_sluzby) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("id_sluzby", id_sluzby);

			uruchomRaportOceanRTF("Arkusz sluzby przygotowawczej", params);
		}
		public void uruchomUmowaLojalnosciowaRTF(int id_umowy) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("id_umowy", id_umowy);

			uruchomRaportOceanRTF("Umowa lojalnosciowa", params);
		}
	//
	//
	
	
	
	
	
	
	
	public void uruchomRaportOcean(String nazwa, HashMap<String, Object> params){
		System.out.println("--- URUCHAMIAM RAPORT: "+nazwa+" ---");
		try (Connection conn = JdbcUtils.getConnection()){//iCloseable->potencjalny rollback przed close
			JdbcUtils.sqlSPCall(conn, "eap_globals.ustaw_konsolidacje", "N");
			
			int ret=JdbcUtils.sqlFnInteger(conn, "cssp_genrap.fn_wstaw_raport_kolejka", nazwa, "OCEAN",null,"","PDF");
			if(ret>0){
				
				params.forEach((k,v) -> {
					try {
						JdbcUtils.sqlSPCall(conn, "cssp_genrap.p_wstaw_parametr", ret, k, v);
					} catch (SQLException e) {
						System.out.println("NIE MOZNA WSTAWIC PARAMETRU DLA RAPORTU " + nazwa + "[key,value] - ["+k+","+v+"]");
						e.printStackTrace();
					}
				});
				
				System.out.println("URUCHOMIONY RAPORT "+nazwa+" NUMER W KOLEJCE: "+ret);
				openFile(pobierzRaport(ret, ".pdf")); //monitorowanie kolejki
			}else{
				System.out.println("NIE MOZNA WSTAWIC RAPORTU "+nazwa+" DO KOLEJKI - ret:"+ret);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();// TODO
		} 
		
		
	}
	
	public void uruchomRaportOceanRTF(String nazwa, HashMap<String, Object> params){
		System.out.println("--- URUCHAMIAM RAPORT: "+nazwa+" ---");
		try (Connection conn = JdbcUtils.getConnection()){//iCloseable->potencjalny rollback przed close
			JdbcUtils.sqlSPCall(conn, "eap_globals.ustaw_konsolidacje", "N");
			
			
			int ret=JdbcUtils.sqlFnInteger(conn, "cssp_genrap.fn_wstaw_raport_kolejka", nazwa, "OCEAN",null,"","RTF");
			if(ret>0){
				
				params.forEach((k,v) -> {
					try {
						JdbcUtils.sqlSPCall(conn, "cssp_genrap.p_wstaw_parametr", ret, k, v);
					} catch (SQLException e) {
						System.out.println("NIE MOZNA WSTAWIC PARAMETRU DLA RAPORTU " + nazwa + "[key,value] - ["+k+","+v+"]");
						e.printStackTrace();
					}
				});
				
				System.out.println("URUCHOMIONY RAPORT "+nazwa+" NUMER W KOLEJCE: "+ret);
				downloadFile(pobierzRaport(ret,".rtf")); 
			}else{
				System.out.println("NIE MOZNA WSTAWIC RAPORTU "+nazwa+" DO KOLEJKI - ret:"+ret);
				com.comarch.egeria.Utils.executeScript("window.open('','_self').close();");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();// TODO
		} 
		
		
	}
	
	
	
	public void uruchomRaportOceanRTF_UOKIK(String nazwa, HashMap<String, Object> params){
		System.out.println("--- URUCHAMIAM RAPORT: "+nazwa+" ---");
		try (Connection conn = JdbcUtils.getConnection()){//iCloseable->potencjalny rollback przed close
			JdbcUtils.sqlSPCall(conn, "eap_globals.ustaw_konsolidacje", "N");
			
			
			int ret=JdbcUtils.sqlFnInteger(conn, "cssp_genrap.fn_wstaw_raport_kolejka", nazwa, "REPORTS",null,"","PDF");
			if(ret>0){
				
				params.forEach((k,v) -> {
					try {
						JdbcUtils.sqlSPCall(conn, "cssp_genrap.p_wstaw_parametr", ret, k, v);
					} catch (SQLException e) {
						System.out.println("NIE MOZNA WSTAWIC PARAMETRU DLA RAPORTU " + nazwa + "[key,value] - ["+k+","+v+"]");
						e.printStackTrace();
					}
				});
				
				System.out.println("URUCHOMIONY RAPORT "+nazwa+" NUMER W KOLEJCE: "+ret);
				downloadFile(pobierzRaport(ret,".pdf")); 
			}else{
				System.out.println("NIE MOZNA WSTAWIC RAPORTU "+nazwa+" DO KOLEJKI - ret:"+ret);
				com.comarch.egeria.Utils.executeScript("window.open('','_self').close();");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();// TODO
		} 
		
		
	}
	
	
	public void uruchomRaportOceanRTF_PDF(String nazwa, HashMap<String, Object> params){
		try (Connection conn = JdbcUtils.getConnection()){//iCloseable->potencjalny rollback przed close
			JdbcUtils.sqlSPCall(conn, "eap_globals.ustaw_konsolidacje", "N");
						
			int ret=JdbcUtils.sqlFnInteger(conn, "cssp_genrap.fn_wstaw_raport_kolejka", nazwa, "REPORTS",null,"","PDF");
			if(ret>0){
				
				params.forEach((k,v) -> {
					try {
						JdbcUtils.sqlSPCall(conn, "cssp_genrap.p_wstaw_parametr", ret, k, v);
					} catch (SQLException e) {
						System.out.println("NIE MOZNA WSTAWIC PARAMETRU DLA RAPORTU " + nazwa + "[key,value] - ["+k+","+v+"]");
						e.printStackTrace();
					}
				});
				
				System.out.println("URUCHOMIONY RAPORT "+nazwa+" NUMER W KOLEJCE: "+ret);
				openFile(pobierzRaport(ret,".pdf")); 
			}else{
				System.out.println("NIE MOZNA WSTAWIC RAPORTU "+nazwa+" DO KOLEJKI - ret:"+ret);
				com.comarch.egeria.Utils.executeScript("window.open('','_self').close();");
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();// TODO
		} 
		
		
	}
		

	
	/*public void uruchomOcenaOKresowaCzescI(int zpt_oce_id) {
		System.out.println("--- URUCHAMIAM RAPORT OCENA OKRESOWA CZESC I ---");
		try (Connection conn = jdbc.getConnection()){//iCloseable->potencjalny rollback przed close
			JdbcDAO.sqlSPCall(conn, "eap_globals.ustaw_konsolidacje", "N");
			
			int ret=JdbcDAO.sqlFnInteger(conn, "cssp_genrap.fn_wstaw_raport_kolejka", "Ocena okresowa czesc 1", "OCEAN");
			if(ret>0){
				JdbcDAO.sqlSPCall(conn, "cssp_genrap.p_wstaw_parametr", ret, "zpt_oce_id", zpt_oce_id);
				System.out.println("URUCHOMIONY RAPORT W KOLEJCE: "+ret);
				openFile(pobierzRaport(ret));
			}else{
				System.out.println("NIE MOZNA WSTAWIC RAPORT DO KOLEJKI: "+ret);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();// TODO
		}
	}*/
	
	

	/**
	 * Metoda sprawdza okresowo czy raport został już wygenerowany a następnie pobiera plik z bazy zapisując jako plik tymczasowy
	 * @param queueId
	 * @return
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws InterruptedException 
	 */
	public File pobierzRaport(Integer queueId, String rozszerzenie) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException, InterruptedException {

		
//		boolean isGenerated = false;
//		List<?> result = null;
//		int mnoznik = 1;
		int counter = 0;
		int sekundy = 0;
		Object ret = null;
			
		while (ret==null) {
			
			try(Connection conn = JdbcUtils.getConnection()){
				ret = JdbcUtils.sqlExecScalarQuery(conn, " select rw_id from CSST_REJESTR_WYDRUKOW where rw_kg_id = ? ", queueId);
				if (ret != null) break;
			}
			
			counter ++;
			sekundy += counter;
	
			if (sekundy > 5*60){ //3 minuty minęła
				User.warn("Wydruk nie został jeszcze zrealizowany. Sprawdź historię wydruków aby odczytać wynik później.");
				return null;
			}

			Thread.sleep(1000 * counter );
		}			
			
		
		
		
//			while(!isGenerated){
//				counter++;
			
//		Query query = h.getSession().createSQLQuery(
//				"select rw_id from CSST_REJESTR_WYDRUKOW where rw_kg_id = :p_kg_id")
//				.setParameter("p_kg_id", queueId); //queueId
//				 result = query.list();
		
//				 if(counter >5){
//					 if(counter >15){
//						 mnoznik=5;
//					 }else{
//						 mnoznik=2;
//					 }
//				 }
//				 //System.out.println("CZY JEST PLIK: " + isGenerated + " - " + counter);
//				if(result.isEmpty()){
//					try {
//						Thread.sleep(1000 * mnoznik);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//					
//				}else{
//					isGenerated=true;
//					try {
//						Thread.sleep(1500);
//					} catch (InterruptedException e) {
//						e.printStackTrace();
//					}
//					
//				}
				
//		}
		
		System.out.println("---POBIERAM RAPORT---");
		//System.out.println(result.toString()); //id pliku w CsstRejestrWydrukowPliki

//		CsstRejestrWydrukowPliki raport = (CsstRejestrWydrukowPliki) h.getSession().get(CsstRejestrWydrukowPliki.class, Long.parseLong(result.get(0).toString()));
		File tempPdfFile = null;
		
		try(HibernateContext h = new HibernateContext()){
			CsstRejestrWydrukowPliki raport = h.getSession().get(CsstRejestrWydrukowPliki.class, Long.parseLong(ret.toString()));
			
			System.out.println(raport.getRwpRwPlik());
			
			String[] nazwa = raport.getRwpRwPlik().split("\\\\");
			nazwa = nazwa[nazwa.length-1].split("_");
			System.out.println("--NAZWA: " + nazwa[0]);
			
			
			try {
				tempPdfFile = File.createTempFile(nazwa[0] +"-", rozszerzenie);
				FileOutputStream fileOuputStream = new FileOutputStream(tempPdfFile);
				fileOuputStream.write(raport.getRwpPlik());
				fileOuputStream.close();
				
			} catch (IOException e) {
				e.printStackTrace();
			}

		}//		h.close();
		
		return tempPdfFile;
		
	}

	/**
	 * Metoda otwiera plik .pdf w aplikacji przegladarki
	 * @param file
	 * @throws IOException
	 */
	public void openFile(File file) throws IOException {
		
		
		
		FacesContext fc = FacesContext.getCurrentInstance();
	    ExternalContext ec = fc.getExternalContext();

	    OutputStream output = ec.getResponseOutputStream();
		String fileName = file.getName();
		String contentType = ec.getMimeType(fileName); 
		int contentLength = (int) file.length();
	    
	    ec.responseReset(); 
	    ec.setResponseContentType(contentType); 
	    ec.setResponseContentLength(contentLength); 
	    ec.setResponseHeader("Content-type", "application/pdf");
	    ec.setResponseHeader("Content-disposition", "inline; filename="+file.getName());
	    ec.setResponseHeader("pragma", "public");
		
		Files.copy(file.toPath(), output);
		
		fc.responseComplete(); 
	}
	
	public void downloadFile(File file) throws IOException {
		FacesContext fc = FacesContext.getCurrentInstance();
	    ExternalContext ec = fc.getExternalContext();

	    OutputStream output = ec.getResponseOutputStream();
		String fileName = file.getName();
		String contentType = ec.getMimeType(fileName); 
		int contentLength = (int) file.length();
	    
	    ec.responseReset(); 
	    ec.setResponseContentType(contentType); 
	    ec.setResponseContentLength(contentLength); 
	    ec.setResponseHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
		
		Files.copy(file.toPath(), output);
		
		fc.responseComplete(); 
	}

}
