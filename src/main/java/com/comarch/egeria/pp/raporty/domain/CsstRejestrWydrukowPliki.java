package com.comarch.egeria.pp.raporty.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the CSST_REJESTR_WYDRUKOW_PLIKI database table.
 * 
 */
@Entity
@Table(name="CSST_REJESTR_WYDRUKOW_PLIKI", schema="EGADM1")
@NamedQuery(name="CsstRejestrWydrukowPliki.findAll", query="SELECT c FROM CsstRejestrWydrukowPliki c")
public class CsstRejestrWydrukowPliki implements Serializable {
	private static final long serialVersionUID = 1L;

	@Lob
	@Column(name="RWP_PLIK")
	private byte[] rwpPlik;

	@Id
	@Column(name="RWP_RW_ID")
	private long rwpRwId;

	@Column(name="RWP_RW_PLIK")
	private String rwpRwPlik;

	public CsstRejestrWydrukowPliki() {
	}

	public byte[] getRwpPlik() {
		return this.rwpPlik;
	}

	public void setRwpPlik(byte[] rwpPlik) {
		this.rwpPlik = rwpPlik;
	}

	public long getRwpRwId() {
		return this.rwpRwId;
	}

	public void setRwpRwId(long rwpRwId) {
		this.rwpRwId = rwpRwId;
	}

	public String getRwpRwPlik() {
		return this.rwpRwPlik;
	}

	public void setRwpRwPlik(String rwpRwPlik) {
		this.rwpRwPlik = rwpRwPlik;
	}

}