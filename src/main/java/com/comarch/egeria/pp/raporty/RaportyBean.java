package com.comarch.egeria.pp.raporty;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean // JBossTools
@Named
@Scope("view")
public class RaportyBean extends SqlBean implements Serializable {

	@Inject
	protected ReportGenerator ReportGenerator;

	private Long idRaportu;
	private List<RapParameter> parametry = new ArrayList<RapParameter>();
	private String format;

	@PostConstruct
	public void init() {
		format = "PDF";
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public Long getIdRaportu() {
		return idRaportu;
	}

	public void setIdRaportu(Long idRaportu) {
		this.idRaportu = idRaportu;
	}

	public List<RapParameter> getParametry() {
		return parametry;
	}

	public void setParametry(List<RapParameter> parametry) {
		this.parametry = parametry;
	}

	public void drukuj() {

		if (!validate())
			return;

		HashMap<String, Object> params = new HashMap<String, Object>();

		System.out.println(">>>>>>>>>>>>Parametry uruchamianego raportu:");
		parametry.forEach(p -> {
			if (p.getNazwa().equals("iprz_id"))
				params.put("IPRZ_ID", Long.parseLong(String.valueOf(p.getValue())));
			System.out.println(p);
		});
		System.out.println(">>>>>>>>>>>>");

		try {
			if (parametry.get(0).getRapNazwa().toString().equals("Indywidualny program rozwoju zawodowego"))
				ReportGenerator.displayReportAsPDF(params, "IPRZ");

		} catch (JRException | SQLException | IOException e) {
			e.printStackTrace();
			User.alert(e.getMessage());
		}

		/*
		 * if(format.equals("PDF")){
		 * genrap.uruchomRaportOcean(parametry.get(0).getRapNazwa().toString(),
		 * params); }else if(format.equals("RTF")) {
		 * genrap.uruchomRaportOceanRTF(parametry.get(0).getRapNazwa().toString(
		 * ), params); }else{ User.alert("Niepoprawny format pliku."); }
		 */

	}

	public boolean validate() {
		if (idRaportu == null) {
			User.alert("Nie wybrano raportu.");
			return false;
		}
		for (RapParameter param : parametry) {
			if (param.getValue() == null) {
				User.alert("Nie wszystkie parametry raportu zostały uzupełnione.");
				return false;
			}
		}
		return true;
	}

	public void ustawParametry() {

		parametry.clear();

		this.getLW("PPL_IPR_PARAMETRY_RAPORTY", idRaportu).getData().forEach(dr -> {
			RapParameter tmp = new RapParameter();

			tmp.setId(Long.parseLong(dr.get("pp_rap_id").toString()));
			if (dr.get("pp_par_rap_id") != null)
				tmp.setRapId(Long.parseLong(dr.get("pp_par_rap_id").toString()));

			if (dr.get("pp_rap_nazwa") != null)
				tmp.setRapNazwa(dr.get("pp_rap_nazwa").toString());

			if (dr.get("pp_par_nazwa") != null)
				tmp.setNazwa(dr.get("pp_par_nazwa").toString());

			if (dr.get("pp_par_wartosc") != null)
				tmp.setWartosc(dr.get("pp_par_wartosc").toString());

			if (dr.get("pp_par_lst_id") != null)
				tmp.setLovId(Long.parseLong(dr.get("pp_par_lst_id").toString()));

			if (dr.get("pp_lst_nazwa") != null)
				tmp.setLovName(dr.get("pp_lst_nazwa").toString());

			if (dr.get("pp_par_interfejs") != null)
				tmp.setInterfejs(dr.get("pp_par_interfejs").toString());

			if (dr.get("pp_par_etykieta") != null)
				tmp.setEtykieta(dr.get("pp_par_etykieta").toString());

			if (dr.get("pp_par_opis") != null)
				tmp.setOpis(dr.get("pp_par_opis").toString());

			parametry.add(tmp);
		});

		/*
		 * System.out.println(">>>>>Parametry raportu:"); parametry.forEach(par
		 * -> { System.out.println(par); });
		 */

	}

}
