package com.comarch.egeria.pp.raporty;

import net.sf.jasperreports.engine.JRDefaultScriptlet;

public class HtmlConverterScriplet extends JRDefaultScriptlet {

	private final String UNORDERED_LIST_TAG = "ul";
//	private final String ORDERED_LIST_TAG = "ol";
//	private final String LIST_EL_TAG = "li";
	private final String DEFAULT_EMPTY_LIST = "………………………………………………………………\n………………………………………………………………";

	public String convertStringToUnorderedList(String string) {
		return convertStringToUnorderedList(string, "");
	}

	public String convertStringToUnorderedList(String string, String type) {
		return convertStringToList(string, UNORDERED_LIST_TAG, type);
	}

	public String convertStringToUnorderedListOrEmpty(String string) {
		String stringList = convertStringToList(string, UNORDERED_LIST_TAG, "");
		if (stringList.isEmpty() || stringList.equals("<br/>")) {
			stringList = convertStringToUnorderedList(DEFAULT_EMPTY_LIST);
		}
		return stringList;
	}

	private String convertStringToList(String string, String tag, String type) {
		String listString = "";
		if (string != null && !string.isEmpty()) {
			listString = createListString(string, tag, type);
		}
		return listString;
	}

	private String createListString(String string, String listTag, String type) {
		String lines[] = string.split("\\r?\\n");
		StringBuilder strBld = new StringBuilder();
//		String startTag;
//		if (type.equals(""))
//			startTag = createTagStart(listTag);
//		else
//			startTag = createTagStart(listTag, "type", type);
		// strBld.append(startTag);
		for (String line : lines) {
			if (!line.equals("")) {
				createLiStart(strBld);
				// strBld.append(createTagStart(LIST_EL_TAG));
				strBld.append(line);
				// strBld.append(createTagEnd(LIST_EL_TAG));
				strBld.append("<br/>");
			}
		}
		// strBld.append(createTagEnd(listTag));
		return strBld.toString();
	}

	private void createLiStart(StringBuilder strBld) {
		strBld.append("&nbsp ");
		strBld.append("&nbsp ");
		strBld.append('•');
		strBld.append("&nbsp ");
	}

//	private String createTagStart(String listTag, String paramName, String paramValue) {
//		StringBuilder strBld = new StringBuilder();
//		strBld.append('<');
//		strBld.append(listTag);
//		strBld.append(' ');
//		strBld.append(paramName);
//		strBld.append('=');
//		strBld.append('\"');
//		strBld.append(paramValue);
//		strBld.append('\"');
//		strBld.append('>');
//		return strBld.toString();
//	}
//
//	private String createTagStart(String tag) {
//		StringBuilder strBld = new StringBuilder();
//		strBld.append('<');
//		strBld.append(tag);
//		strBld.append('>');
//		return strBld.toString();
//	}
//
//	private String createTagEnd(String tag) {
//		StringBuilder strBld = new StringBuilder();
//		strBld.append('<');
//		strBld.append('/');
//		strBld.append(tag);
//		strBld.append('>');
//		return strBld.toString();
//	}
}