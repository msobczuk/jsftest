package com.comarch.egeria.pp.raporty;

public class RapParameter {
	
	private Long id;
	private Long rapId;
	private String rapNazwa;
	private String nazwa;
	private String wartosc;
	private Long lovId;
	private String lovName="";
	private String interfejs;
	private String etykieta;
	private String opis;
	private Object value;
	
	
	
	
	public String getRapNazwa() {
		return rapNazwa;
	}
	public void setRapNazwa(String rapNazwa) {
		this.rapNazwa = rapNazwa;
	}
	public String getLovName() {
		return lovName;
	}
	public void setLovName(String lovName) {
		this.lovName = lovName;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getRapId() {
		return rapId;
	}
	public void setRapId(Long rapId) {
		this.rapId = rapId;
	}
	public String getNazwa() {
		return nazwa;
	}
	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}
	public String getWartosc() {
		return wartosc;
	}
	public void setWartosc(String wartosc) {
		this.wartosc = wartosc;
	}
	public Long getLovId() {
		return lovId;
	}
	public void setLovId(Long lovId) {
		this.lovId = lovId;
	}
	public String getInterfejs() {
		return interfejs;
	}
	public void setInterfejs(String interfejs) {
		this.interfejs = interfejs;
	}
	public String getEtykieta() {
		return etykieta;
	}
	public void setEtykieta(String etykieta) {
		this.etykieta = etykieta;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	@Override
	public String toString() {
		return "RapParameter [id=" + id + ", rapId=" + rapId + ", rapNazwa=" + rapNazwa + ", nazwa=" + nazwa
				+ ", wartosc=" + wartosc + ", lovId=" + lovId + ", lovName=" + lovName + ", interfejs=" + interfejs
				+ ", etykieta=" + etykieta + ", opis=" + opis + ", value=" + value + "]";
	}

	
	

}
