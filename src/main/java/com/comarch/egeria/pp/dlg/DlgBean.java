package com.comarch.egeria.pp.dlg;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.szkolenia.RejestrSzkolenBean;
import com.comarch.egeria.web.User;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

//import org.primefaces.context.RequestContext;

/**
 * Kontroler / proxy do wymany danych między różnymi kontrolerami o zasiégu (scope) na poziomie view dla widoku głównego oraz ifreame w dlgDv
 *
 */
@ManagedBean 
@Named
@Scope("view")
public class DlgBean extends SqlBean {

	
	@Inject
	User user;
	
	
	private RejestrSzkolenBean rejestrSzkolenBean = null;

	private String test;
	
	@PostConstruct
	public void init(){
		//TODO kod tego kontrolera działa wewn. iframe - wczytac kontrolery z widoku głównego ...
		this.rejestrSzkolenBean = (RejestrSzkolenBean) user.tmpUserObjectsCHM.get("RejestrSzkolenBean");
		//user.tmpUserObjectsCHM.remove("RejestrSzkolenBean");
		
//		System.out.println();
	}
	
	
	
	
	
	
	//id dialogu oraz id obiektu do wyswietlenia/edycji wewn. dlg...
	 public void showDlg(final String dlgid, final String id) {
		System.out.println("showDlg / " + dlgid + " -> " + id);
		
		//TODO: if dlgid -> rejestrSzkolen...
		this.rejestrSzkolenBean.setSelectedOfertaId(Long.parseLong(id));
		
//        RequestContext context = RequestContext.getCurrentInstance();
//        context.update(dlgid);
//        context.execute("PF('" + dlgid + "').show();");
		com.comarch.egeria.Utils.update("PF('" + dlgid + "').show();");
	 }
	
	
	public RejestrSzkolenBean getRejestrSzkolenBean() {
		return rejestrSzkolenBean;
	}

	public void setRejestrSzkolenBean(RejestrSzkolenBean rejestrSzkolenBean) {
		this.rejestrSzkolenBean = rejestrSzkolenBean;
	}






	public String getTest() {
		return test;
	}






	public void setTest(String test) {
		this.test = test;
	} 
	
}
