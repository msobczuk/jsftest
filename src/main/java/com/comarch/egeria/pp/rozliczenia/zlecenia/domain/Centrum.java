package com.comarch.egeria.pp.rozliczenia.zlecenia.domain;

import java.sql.Date;

public class Centrum {

	private Long ceId;
	private String ceKod;
	private String ceNazwa;
	private String ceKierownik;
	private String ceFPokazywac;
	private Date ceDataOtwarcia;
	private Date ceDataZamkniecia;
	private String ceTyp;
	private Long ceSort;
	private String ceHsort;

	@Override
	public String toString() {
		return "Centra [ceId=" + ceId + ", ceKod=" + ceKod + ", ceNazwa=" + ceNazwa + ", ceKierownik=" + ceKierownik
				+ ", ceFPokazywac=" + ceFPokazywac + ", cdDataOtwarcia=" + ceDataOtwarcia + ", cdDataZamkniecia="
				+ ceDataZamkniecia + ", ceTyp=" + ceTyp + ", ceSort=" + ceSort + ", ceHsort=" + ceHsort + "]";
	}

	public Long getCeId() {
		return ceId;
	}

	public void setCeId(Long ceId) {
		this.ceId = ceId;
	}

	public String getCeKod() {
		return ceKod;
	}

	public void setCeKod(String ceKod) {
		this.ceKod = ceKod;
	}

	public String getCeNazwa() {
		return ceNazwa;
	}

	public void setCeNazwa(String ceNazwa) {
		this.ceNazwa = ceNazwa;
	}

	public String getCeKierownik() {
		return ceKierownik;
	}

	public void setCeKierownik(String ceKierownik) {
		this.ceKierownik = ceKierownik;
	}

	public String getCeFPokazywac() {
		return ceFPokazywac;
	}

	public void setCeFPokazywac(String ceFPokazywac) {
		this.ceFPokazywac = ceFPokazywac;
	}

	public Date getCeDataOtwarcia() {
		return ceDataOtwarcia;
	}

	public void setCeDataOtwarcia(Date ceDataOtwarcia) {
		this.ceDataOtwarcia = ceDataOtwarcia;
	}

	public Date getCeDataZamkniecia() {
		return ceDataZamkniecia;
	}

	public void setCeDataZamkniecia(Date ceDataZamkniecia) {
		this.ceDataZamkniecia = ceDataZamkniecia;
	}

	public String getCeTyp() {
		return ceTyp;
	}

	public void setCeTyp(String ceTyp) {
		this.ceTyp = ceTyp;
	}

	public Long getCeSort() {
		return ceSort;
	}

	public void setCeSort(Long ceSort) {
		this.ceSort = ceSort;
	}

	public String getCeHsort() {
		return ceHsort;
	}

	public void setCeHsort(String ceHsort) {
		this.ceHsort = ceHsort;
	}

}