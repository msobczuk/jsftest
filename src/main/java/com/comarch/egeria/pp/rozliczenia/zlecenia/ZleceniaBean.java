package com.comarch.egeria.pp.rozliczenia.zlecenia;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.rozliczenia.zlecenia.bo.GlobalMockBo;
import com.comarch.egeria.pp.rozliczenia.zlecenia.bo.ZleceniaBo;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Centrum;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Mpk;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Okres2;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.PozycjaUmow;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.TypyZlecen;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Umowa;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Zlecenie;
import com.comarch.egeria.web.common.utils.constants.Const;
import com.comarch.egeria.web.common.utils.exceptions.CtlMessageUtils;
import com.comarch.egeria.web.common.utils.lang.LanguageBean;
import com.comarch.egeria.web.common.view.ViewBean;



@Named
@Scope("view")
public class ZleceniaBean extends ViewBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(ZleceniaBean.class);
	
	private enum DlgMZleceniaType {
		PROPONUJ_KWOTE, USUN_ZLECENIE, ZGLOSZENIE_DO_ANULOWANIA, ZGODA_NA_ANULOWANIE, ODRZUCENIE_ANULOWANIA, PRZEPNIJ_ZLECENIE, ODBIOR_ZLECENIA, ANULOWANIE_ODBIORU, ZGLOSZENIE_ODBIORU, PRZYJECIE_ZLECENIA, AKCEPTUJ_ZLECENIE, ODRZUC_ZLECENIE, AKCEPTUJ_KWOTE, ANULUJ_KWOTE
	}
	
	private Long getAktualnyOkresId() {
		return 1L;
	}

	@Inject
	private ZleceniaBo zleceniaBo;
	@Inject
	private GlobalMockBo globalMockBo;

	private List<Zlecenie> zlecenia;
	private Zlecenie selectedZlecenie;
	private Zlecenie noweZlecenieWyk = new Zlecenie();
	private List<String> wybraneZlecenia;
	private List<Okres2> okresy = new ArrayList<Okres2>();
	private Long okresOdFiltr;
	private Long okresDoFiltr;
	private short wyborDaty;

	// Dodatkowy opis Rodzaju Zlecenia
	private String rodzajZleceniaOpis;

	// Obsługa wielu centrów
	private List<Centrum> aktywneCentra;

	// Listy
	private List<Mpk> mpkKosztRd;
	private List<Centrum> adresaciCentra;
	private List<Mpk> mpkPrzychodRd;
	private List<Umowa> umowy;
	private List<PozycjaUmow> pozycjeUmow;
	private List<TypyZlecen> typyZlecen;

	// Dialog modyfikacji
	private String dlgTytul;
	private String dlgTekst;
	private String dlgTekstPK;
	private DlgMZleceniaType dlgTyp;
	private boolean dlgRenderedTekst;
	private boolean dlgRenderedTekstPK;
	private boolean dlgRenderedKwota;
	private boolean dlgRenderedKwotaProponowana;
	private boolean dlgRenderedCentrum;
	private boolean dlgRenderedOkresPM;

	private Double dlgKwota;
	private Long dlgCeDoF;
	private Boolean dlgZLPM;
	private List<Okres2> dlgOkresyPM = new ArrayList<Okres2>();
	private Long dlgOkPMIdF;

	// Do przycisków w dialogach
	private String confirm;
	private String cancel;

	// Do wyboru okresu
	private String q1, q2, q3, q4;

	public void onRowSelect() {
		// log.debug("selectedZlecenie " + selectedZlecenie);

		if (selectedZlecenie.getRodzaj().equals(Const.WLASNE_W_TOKU)) {
			rodzajZleceniaOpis = LanguageBean.translate("LBL10206_zlecenie_wt");
		} else if (selectedZlecenie.getRodzaj().equals(Const.WLASNE_ODEBRANE)) {
			rodzajZleceniaOpis = LanguageBean.translate("LBL10207_zlecenie_wo");
		} else if (selectedZlecenie.getRodzaj().equals(Const.OBCE_W_TOKU)) {
			rodzajZleceniaOpis = LanguageBean.translate("LBL10208_zlecenie_ot");
		} else if (selectedZlecenie.getRodzaj().equals(Const.OBCE_ZAKONCZONE)) {
			rodzajZleceniaOpis = LanguageBean.translate("LBL10209_zlecenie_oz");
		}
		setButtonsVisibility();
	}

	public void onRowUnselect() {
		selectedZlecenie = null;
		setButtonsVisibility();
	}

	public void wybierzZlecenia() {

		Okres2 okOd = new Okres2();
		okOd.setOkId(okresOdFiltr);
		Okres2 okDo = new Okres2();
		okDo.setOkId(okresDoFiltr);

		try {
			zlecenia = new ArrayList<Zlecenie>();
			Centrum ce = new Centrum();
			List<Zlecenie> zleceniaZCe = zleceniaBo.sqlZleceniaWszystkie(ce, okOd, okDo, wyborDaty).stream()
					.filter(z -> wybraneZlecenia.contains(z.getRodzaj())).collect(Collectors.toList());

				zlecenia.addAll(zleceniaZCe);

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void przygotujDodanieZleceniaWyk() {
		noweZlecenieWyk = new Zlecenie();
		initListyWyboru();
		pobierzListeOkresyPM();
		if (selectedZlecenie != null)
			noweZlecenieWyk.setCeIdK(selectedZlecenie.getKontekstCentrum().getCeId());
		else
			noweZlecenieWyk.setCeIdK(aktywneCentra.get(0).getCeId());
		noweZlecenieWyk.setRwDataWykonaniaUtl(new java.util.Date());
		noweZlecenieWyk.setRodzaj(Const.ZCLEC_NR);
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10094_zatwierdz");
		cancel = LanguageBean.translate("LBL10067_anuluj");
	}

	public void przygotujDodanieZleceniaPK() {
		noweZlecenieWyk = new Zlecenie();
		initListyWyboru();
		pobierzListeOkresyPM();
		if (selectedZlecenie != null)
			noweZlecenieWyk.setCeIdK(selectedZlecenie.getKontekstCentrum().getCeId());
		else
			noweZlecenieWyk.setCeIdK(aktywneCentra.get(0).getCeId());
		noweZlecenieWyk.setRwDataWykonaniaUtl(new java.util.Date());
		noweZlecenieWyk.setRodzaj(Const.ZCLEC_PK);
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10094_zatwierdz");
		cancel = LanguageBean.translate("LBL10067_anuluj");
	}

	public void przygotujDlgPropKwote() {
		dlgKwota = 0.0;
		dlgTyp = DlgMZleceniaType.PROPONUJ_KWOTE;
		przygotujElementyWyswietlanie(dlgTyp);
		dlgTytul = LanguageBean.translate("LBL10196_prop_kwote");
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10094_zatwierdz");
		cancel = LanguageBean.translate("LBL10067_anuluj");
	}

	public void przygotujDlgAkceptPropKwote() {
		dlgTyp = DlgMZleceniaType.AKCEPTUJ_KWOTE;
		przygotujElementyWyswietlanie(dlgTyp);
		dlgTytul = LanguageBean.translate("LBL10242_akcept_kwote_prop");
		dlgTekst = LanguageBean.translate("MSG4171");
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10095_tak");
		cancel = LanguageBean.translate("LBL10096_nie");
	}

	public void przygotujDlgAnulujPropKwote() {
		dlgTyp = DlgMZleceniaType.ANULUJ_KWOTE;
		przygotujElementyWyswietlanie(dlgTyp);
		dlgTytul = LanguageBean.translate("LBL10243_anuluj_kwote_prop");
		dlgTekst = LanguageBean.translate("MSG4173");
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10095_tak");
		cancel = LanguageBean.translate("LBL10096_nie");
	}

	public void przygotujDlgUsunZleceniaWyk() {
		dlgTyp = DlgMZleceniaType.USUN_ZLECENIE;
		przygotujElementyWyswietlanie(dlgTyp);
		dlgTytul = LanguageBean.translate("LBL10244_usun_zlec");
		dlgTekst = LanguageBean.translate("MSG4137");
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10095_tak");
		cancel = LanguageBean.translate("LBL10096_nie");
	}

	public void przygotujDlgZglDoAnulowania() {
		dlgTyp = DlgMZleceniaType.ZGLOSZENIE_DO_ANULOWANIA;
		przygotujElementyWyswietlanie(dlgTyp);
		dlgTytul = LanguageBean.translate("LBL10245_zglos_do_anul");

		// Anulowac mozna tylko własne w toku lub obce w toku
		if (selectedZlecenie.getRodzaj().equals(Const.WLASNE_W_TOKU)) {
			dlgTekst = LanguageBean.translate("MSG4139");
		} else {
			dlgTekst = LanguageBean.translate("MSG4134");
		}
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10095_tak");
		cancel = LanguageBean.translate("LBL10096_nie");
	}

	public void przygotujDlgZgodaNaAnulowanie() {
		dlgTyp = DlgMZleceniaType.ZGODA_NA_ANULOWANIE;
		przygotujElementyWyswietlanie(dlgTyp);
		dlgTytul = LanguageBean.translate("LBL10246_zgoda_na_anul");
		dlgTekst = LanguageBean.translate("MSG4136");
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10095_tak");
		cancel = LanguageBean.translate("LBL10096_nie");
	}

	public void przygotujDlgOdrzucenieaAnulowania() {
		dlgTyp = DlgMZleceniaType.ODRZUCENIE_ANULOWANIA;
		przygotujElementyWyswietlanie(dlgTyp);
		dlgTytul = LanguageBean.translate("LBL10247_odrzuc_anul");
		dlgTekst = LanguageBean.translate("MSG4135");
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10095_tak");
		cancel = LanguageBean.translate("LBL10096_nie");
	}

	public void przygotujDlgPrzepnij() {
		dlgTyp = DlgMZleceniaType.PRZEPNIJ_ZLECENIE;
		przygotujElementyWyswietlanie(dlgTyp);
		dlgTytul = LanguageBean.translate("LBL10248_przep_zlec");
		pobierzListeAdresatow(selectedZlecenie.getKontekstCentrum().getCeId());
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10094_zatwierdz");
		cancel = LanguageBean.translate("LBL10067_anuluj");
	}

	public void przygotujDlgOdbiorZlec() {
		dlgKwota = 0.0;
		dlgTyp = DlgMZleceniaType.ODBIOR_ZLECENIA;
		przygotujElementyWyswietlanie(dlgTyp);
		dlgTytul = LanguageBean.translate("LBL10249_odbior_zlec");
		// ? 4138
		pobierzListeAdresatow(selectedZlecenie.getKontekstCentrum().getCeId());
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10094_zatwierdz");
		cancel = LanguageBean.translate("LBL10067_anuluj");
	}

	public void przygotujDlgAnulowanieOdbioru() {
		dlgTyp = DlgMZleceniaType.ANULOWANIE_ODBIORU;
		przygotujElementyWyswietlanie(dlgTyp);
		dlgTytul = LanguageBean.translate("LBL10254_anuluj_odbior");
		dlgTekst = LanguageBean.translate("MSG4159");
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10095_tak");
		cancel = LanguageBean.translate("LBL10096_nie");
	}

	public void przygotujDlgZglDoOdbioru() {
		dlgTyp = DlgMZleceniaType.ZGLOSZENIE_ODBIORU;
		przygotujElementyWyswietlanie(dlgTyp);
		pobierzListeOkresyPM();
		dlgTytul = LanguageBean.translate("LBL10250_zglosz_odbioru");
		dlgTekst = LanguageBean.translate("MSG4133");
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10095_tak");
		cancel = LanguageBean.translate("LBL10096_nie");
	}

	public void przygotujDlgPrzyjZlecenia() {
		dlgTyp = DlgMZleceniaType.PRZYJECIE_ZLECENIA;
		przygotujElementyWyswietlanie(dlgTyp);
		pobierzListeOkresyPM();
		dlgKwota = selectedZlecenie.getRwKwota();
		dlgTytul = LanguageBean.translate("LBL10251_przyj_zlec");
		dlgTekst = LanguageBean.translate("INF1004");
		dlgTekstPK = LanguageBean.translate("INF1005");
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10095_tak");
		cancel = LanguageBean.translate("LBL10096_nie");
	}

	public void przygotujDlgAkceptuj() {
		dlgTyp = DlgMZleceniaType.AKCEPTUJ_ZLECENIE;
		przygotujElementyWyswietlanie(dlgTyp);
		dlgTytul = LanguageBean.translate("LBL10252_akcept_zlec");
		dlgTekst = LanguageBean.translate("MSG4132");
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10095_tak");
		cancel = LanguageBean.translate("LBL10096_nie");
	}

	public void przygotujDlgOdrzuc() {
		dlgTyp = DlgMZleceniaType.ODRZUC_ZLECENIE;
		przygotujElementyWyswietlanie(dlgTyp);
		dlgTytul = LanguageBean.translate("LBL10253_odrzuc_zlec");
		dlgTekst = LanguageBean.translate("MSG4131");
		dlgZLPM = false;
		confirm = LanguageBean.translate("LBL10095_tak");
		cancel = LanguageBean.translate("LBL10096_nie");
	}

	public void dlgMZlZatwierdz() {
		switch (dlgTyp) {
		case PROPONUJ_KWOTE:
			log.debug("" + DlgMZleceniaType.PROPONUJ_KWOTE);

			try {
				zleceniaBo.wprowadzKwoteProponowana(selectedZlecenie, dlgKwota);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2033", "2563")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case USUN_ZLECENIE:
			log.debug("" + DlgMZleceniaType.USUN_ZLECENIE);

			try {
				zleceniaBo.usun(selectedZlecenie);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2350", "2351", "2107", "2016")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case ZGLOSZENIE_DO_ANULOWANIA:
			log.debug("" + DlgMZleceniaType.ZGLOSZENIE_DO_ANULOWANIA);

			try {
				zleceniaBo.zgloszenieAnulowania(selectedZlecenie);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2360", "2361", "2278", "2362", "2107")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case ZGODA_NA_ANULOWANIE:
			log.debug("" + DlgMZleceniaType.ZGODA_NA_ANULOWANIE);

			try {
				zleceniaBo.zgodaNaAnulowanie(selectedZlecenie);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2363", "2364", "2365", "2278", "2366", "2107")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case ODRZUCENIE_ANULOWANIA:
			log.debug("" + DlgMZleceniaType.ODRZUCENIE_ANULOWANIA);

			try {
				zleceniaBo.odrzucenieAnulowania(selectedZlecenie);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2363", "2278", "2107")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case PRZEPNIJ_ZLECENIE:
			log.debug("" + DlgMZleceniaType.PRZEPNIJ_ZLECENIE);

			try {
				zleceniaBo.przepnijZlecenie(selectedZlecenie, dlgCeDoF);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2033", "2560")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case ODBIOR_ZLECENIA:
			log.debug("" + DlgMZleceniaType.ODBIOR_ZLECENIA);

			try {
				zleceniaBo.odbierz(selectedZlecenie, dlgKwota, dlgZLPM ? "T" : "N", dlgOkPMIdF);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2370", "2278", "2371", "2372", "2373", "2374", "2324",
						"2533", "2107")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case ANULOWANIE_ODBIORU:
			log.debug("" + DlgMZleceniaType.ANULOWANIE_ODBIORU);

			try {
				zleceniaBo.anulowanieOdbioru(selectedZlecenie);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2504", "2278", "2505")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case ZGLOSZENIE_ODBIORU:
			log.debug("" + DlgMZleceniaType.ZGLOSZENIE_ODBIORU);

			try {
				zleceniaBo.zglosDoOdbioru(selectedZlecenie);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2367", "2368", "2369", "2107")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case PRZYJECIE_ZLECENIA:
			log.debug("" + DlgMZleceniaType.PRZYJECIE_ZLECENIA);

			log.debug(selectedZlecenie);

			try {
				if (selectedZlecenie.getRwTyp().equals(Const.ZCLEC_PK)) {
					zleceniaBo.przyjmijZleceniePk(selectedZlecenie, dlgZLPM ? "T" : "N", dlgOkPMIdF);
				} else {
					zleceniaBo.przyjmij(selectedZlecenie, dlgZLPM ? "T" : "N", dlgOkPMIdF);
				}
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2354", "2355", "2356", "2357", "2358", "2359", "2345",
						"2346", "2359", "2107", "2377", "2359", "2378", "2533")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case AKCEPTUJ_ZLECENIE:
			log.debug("" + DlgMZleceniaType.AKCEPTUJ_ZLECENIE);

			try {
				zleceniaBo.akceptuj(selectedZlecenie);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2348", "2349", "2107")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case ODRZUC_ZLECENIE:
			log.debug("" + DlgMZleceniaType.ODRZUC_ZLECENIE);

			try {
				zleceniaBo.odrzuc(selectedZlecenie);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2352", "2353", "2107")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case AKCEPTUJ_KWOTE:
			log.debug("" + DlgMZleceniaType.ODRZUC_ZLECENIE);

			try {
				zleceniaBo.akceptujKwoteProponowana(selectedZlecenie);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2033", "2564", "2565", "2566", "2567")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		case ANULUJ_KWOTE:
			log.debug("" + DlgMZleceniaType.ODRZUC_ZLECENIE);

			try {
				zleceniaBo.anulujKwoteProponowana(selectedZlecenie);
				com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
				wybierzZlecenia();
			} catch (SQLException e) {
				if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2033", "2564")) {
					CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

				} else {
					CtlMessageUtils.addErrorMessage("INF1006");
				}
			}

			break;
		default:
			com.comarch.egeria.Utils.executeScript("PF('dlgMZlecenieWlasne').hide();");
			wybierzZlecenia();
			break;
		}
	}

	public void otZLWystawcaChanged() {
		pobierzListeKosztRD(noweZlecenieWyk.getCeIdK());
		if(mpkKosztRd != null && !mpkKosztRd.isEmpty()){
			noweZlecenieWyk.setMpkIdK(mpkKosztRd.get(0).getMpkId());
			pobierzListeUmowa(noweZlecenieWyk.getCeIdK(), noweZlecenieWyk.getMpkIdK());
		} else {
			noweZlecenieWyk.setMpkIdK(null);
			umowy = null;
		}
		pobierzListeAdresatow(noweZlecenieWyk.getCeIdK());
		if(adresaciCentra != null && !adresaciCentra.isEmpty()){
			noweZlecenieWyk.setCeIdP(adresaciCentra.get(0).getCeId());
		} else {
			noweZlecenieWyk.setCeIdP(null);
		}
		pobierzListePrzychodRD(noweZlecenieWyk.getCeIdK());
		if(mpkPrzychodRd != null && !mpkPrzychodRd.isEmpty()){
//			noweZlecenieWyk.setMpkIdP(mpkPrzychodRd.get(0).getMpkId());
			noweZlecenieWyk.setMpkIdP(noweZlecenieWyk.getMpkIdK());
		} else {
			noweZlecenieWyk.setMpkIdP(null);
		}
		if(umowy != null && !umowy.isEmpty()){
			noweZlecenieWyk.setUmId(umowy.get(0).getUmwId());
			pobierzListePozycje(noweZlecenieWyk.getCeIdK(), noweZlecenieWyk.getUmId());
		} else {
			noweZlecenieWyk.setUmId(null);
			pozycjeUmow = null;
		}
		if(pozycjeUmow != null && !pozycjeUmow.isEmpty()){
		noweZlecenieWyk.setPumId(pozycjeUmow.get(0).getPumId());
		} else {
			noweZlecenieWyk.setPumId(null);
		}
		pobierzListeTypZlecen();

//		System.out.println("Zmiana Centrum");
//		System.out.println("CE ID: " + noweZlecenieWyk.getCeIdK());
//		System.out.println("MPK ID: " + noweZlecenieWyk.getMpkIdK());
//		System.out.println("UMW ID: " + noweZlecenieWyk.getUmId());
//		System.out.println("PUM ID: " + noweZlecenieWyk.getPumId());

	}

	public void otZLWKosztRDChanged() {
		pobierzListeUmowa(noweZlecenieWyk.getCeIdK(), noweZlecenieWyk.getMpkIdK());
		if(umowy != null && !umowy.isEmpty()){
			noweZlecenieWyk.setUmId(umowy.get(0).getUmwId());
			pobierzListePozycje(noweZlecenieWyk.getCeIdK(), noweZlecenieWyk.getUmId());
		} else {
			noweZlecenieWyk.setUmId(null);
			pozycjeUmow = null;
		}
		if(pozycjeUmow != null && !pozycjeUmow.isEmpty()){
			noweZlecenieWyk.setPumId(pozycjeUmow.get(0).getPumId());
		} else {
			noweZlecenieWyk.setPumId(null);
		}
		noweZlecenieWyk.setMpkIdP(noweZlecenieWyk.getMpkIdK());

//		System.out.println("Zmiana RD");
//		System.out.println("CE ID: " + noweZlecenieWyk.getCeIdK());
//		System.out.println("MPK ID: " + noweZlecenieWyk.getMpkIdK());
//		System.out.println("UMW ID: " + noweZlecenieWyk.getUmId());
//		System.out.println("PUM ID: " + noweZlecenieWyk.getPumId());
	}

	public void otZLUmowaChanged() {
		pobierzListePozycje(noweZlecenieWyk.getCeIdK(), noweZlecenieWyk.getUmId());
		if(pozycjeUmow != null && !pozycjeUmow.isEmpty()){
			noweZlecenieWyk.setPumId(pozycjeUmow.get(0).getPumId());
		}
		else {
			noweZlecenieWyk.setPumId(null);
		}
//		System.out.println("Zmiana umowy");
//		System.out.println("CE ID: " + noweZlecenieWyk.getCeIdK());
//		System.out.println("MPK ID: " + noweZlecenieWyk.getMpkIdK());
//		System.out.println("UMW ID: " + noweZlecenieWyk.getUmId());
//		System.out.println("PUM ID: " + noweZlecenieWyk.getPumId());
	}

	public void dodajZlecenieWykonania() {

		Long newRwId = 0L;
		log.debug("dodajZlecenieWykonania() noweZlecenieWyk " + noweZlecenieWyk);

		try {
			newRwId = zleceniaBo.wystaw(noweZlecenieWyk);
		} catch (SQLException e) {
			if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2341", "2342", "2345", "2346", "2444", "2445")) {
				CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

			} else {
				CtlMessageUtils.addErrorMessage("INF1006");
			}
		}

		if (newRwId != 0L) {
			com.comarch.egeria.Utils.executeScript("PF('dlgDZlecenieWykonania').hide();");
			wybierzZlecenia();
			// selectedZlecenie = zlecenia.stream().filter(z ->
			// z.getRwId().equals(newRwId) ).findFirst().get();
		}

	}

	public void dodajZleceniePK() {

		Long newRwId = 0L;
		log.debug("dodajZleceniePK() noweZlecenieWyk " + noweZlecenieWyk);

		try {
			newRwId = zleceniaBo.wystaw(noweZlecenieWyk);
		} catch (SQLException e) {
			if (CtlMessageUtils.isErrorCodeIn(e, "2001", "2341", "2342", "2345", "2346", "2444", "2445")) {
				CtlMessageUtils.addErrorMessage(CtlMessageUtils.getErrorMsgCode(e));

			} else {
				CtlMessageUtils.addErrorMessage("INF1006");
			}
		}

		if (newRwId != 0L) {
			com.comarch.egeria.Utils.executeScript("PF('dlgDZleceniePrzjeciaKosztow').hide();");
			wybierzZlecenia();
		}

	}

	public void ustawOkresy(final String period) {
		if (period.equals("BM")) {
			okresOdFiltr = getAktualnyOkresId();
			okresDoFiltr = okresOdFiltr;
		} else if (period.equals("OM")) {
			okresOdFiltr = previousMonth(1);
			okresDoFiltr = okresOdFiltr;
		} else if (period.equals("BOM")) {
			okresOdFiltr = previousMonth(1);
			okresDoFiltr = getAktualnyOkresId();
		} else if (period.equals("Q4")) {
			quarter(getAktualnyOkresId());
		} else if (period.equals("Q3")) {
			quarter(previousMonth(3));
		} else if (period.equals("Q2")) {
			quarter(previousMonth(6));
		} else if (period.equals("Q1")) {
			quarter(previousMonth(9));
		}
	}

	private Long previousMonth(int count) {
		Okres2 current, previous;
		current = okresy.stream().filter(x -> x.getOkId().equals(getAktualnyOkresId())).findFirst()
				.orElse(null);
		previous = okresy.stream().filter(x -> x.getNumerMiesiaca().equals(current.getNumerMiesiaca() - count))
				.findFirst().orElse(null);
		return previous.getOkId();
	}

	private void quarter(Long id) {
		Okres2 current;
		Long m;
		current = okresy.stream().filter(x -> x.getOkId().equals(id)).findFirst().orElse(null);
		m = current.getNumerMiesiaca() % 12;
		if (m >= 1 && m <= 3) {
			okresOdFiltr = okresy.stream().filter(x -> x.getNumerMiesiaca().equals(current.getOkRok() * 12 + 1))
					.findFirst().orElse(null).getOkId();
			okresDoFiltr = okresy.stream().filter(x -> x.getNumerMiesiaca().equals(current.getOkRok() * 12 + 3))
					.findFirst().orElse(null).getOkId();
		} else if (m >= 4 && m <= 6) {
			okresOdFiltr = okresy.stream().filter(x -> x.getNumerMiesiaca().equals(current.getOkRok() * 12 + 4))
					.findFirst().orElse(null).getOkId();
			okresDoFiltr = okresy.stream().filter(x -> x.getNumerMiesiaca().equals(current.getOkRok() * 12 + 6))
					.findFirst().orElse(null).getOkId();
		} else if (m >= 7 && m <= 9) {
			okresOdFiltr = okresy.stream().filter(x -> x.getNumerMiesiaca().equals(current.getOkRok() * 12 + 7))
					.findFirst().orElse(null).getOkId();
			okresDoFiltr = okresy.stream().filter(x -> x.getNumerMiesiaca().equals(current.getOkRok() * 12 + 9))
					.findFirst().orElse(null).getOkId();
		} else if (m >= 10 && m <= 12) {
			okresOdFiltr = okresy.stream().filter(x -> x.getNumerMiesiaca().equals(current.getOkRok() * 12 + 10))
					.findFirst().orElse(null).getOkId();
			okresDoFiltr = okresy.stream().filter(x -> x.getNumerMiesiaca().equals(current.getOkRok() * 12 + 12))
					.findFirst().orElse(null).getOkId();
		}
	}

	public void refreshOnCenterChange() {
		wybierzZlecenia();
	}

	/*
	 * Private Functions
	 */

	@PostConstruct
	private void init() {
		log.error("Sample Log error");
		log.info("Sample Log info");

		okresy = new ArrayList<>();
		Okres2 okres = new Okres2();
		okres.setOkId(1L);
		okres.setMiesiac("08");
		okresy.add(okres);

		wybraneZlecenia = new LinkedList<String>();
		wybraneZlecenia.addAll(Arrays.asList(Const.WLASNE_W_TOKU, Const.WLASNE_ODEBRANE, Const.OBCE_W_TOKU,
				Const.OBCE_ZAKONCZONE));

		// Do wyboru okresu
		Calendar c = Calendar.getInstance();
		int month = c.get(Calendar.MONTH);
		if (month >= Calendar.JANUARY && month <= Calendar.MARCH) {
			q4 = "Q1 " + c.get(Calendar.YEAR);
			q3 = "Q4 " + (c.get(Calendar.YEAR) - 1);
			q2 = "Q3 " + (c.get(Calendar.YEAR) - 1);
			q1 = "Q2 " + (c.get(Calendar.YEAR) - 1);
		} else if (month >= Calendar.APRIL && month <= Calendar.JUNE) {
			q4 = "Q2 " + c.get(Calendar.YEAR);
			q3 = "Q1 " + c.get(Calendar.YEAR);
			q2 = "Q4 " + (c.get(Calendar.YEAR) - 1);
			q1 = "Q3 " + (c.get(Calendar.YEAR) - 1);
		} else if (month >= Calendar.JULY && month <= Calendar.SEPTEMBER) {
			q4 = "Q3 " + c.get(Calendar.YEAR);
			q3 = "Q2 " + c.get(Calendar.YEAR);
			q2 = "Q1 " + c.get(Calendar.YEAR);
			q1 = "Q4 " + (c.get(Calendar.YEAR) - 1);
		} else if (month >= Calendar.OCTOBER && month <= Calendar.DECEMBER) {
			q4 = "Q4 " + c.get(Calendar.YEAR);
			q3 = "Q3 " + c.get(Calendar.YEAR);
			q2 = "Q2 " + c.get(Calendar.YEAR);
			q1 = "Q1 " + c.get(Calendar.YEAR);
		}

		wyborDaty = 1;

		okresOdFiltr = getAktualnyOkresId();
		okresDoFiltr = getAktualnyOkresId();

		aktywneCentra = new ArrayList<>();
		Centrum centrum = new Centrum();
		centrum.setCeId(1L);
		aktywneCentra.add(centrum);
		
		
		wybierzZlecenia();

		initListyWyboru();
	}

	private void pobierzListeKosztRD(Long ceId) {
		try {
			mpkKosztRd = zleceniaBo.sqlMpkKoszt(ceId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void pobierzListeAdresatow(Long ceId) {
		this.adresaciCentra = globalMockBo.sqlAdresat();
	}

	private void pobierzListePrzychodRD(Long ceId) {
		try {
			mpkPrzychodRd = zleceniaBo.sqlMpkPrzychod(ceId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void pobierzListeUmowa(Long ceId, Long kosztRD) {
		this.umowy = globalMockBo.sqlUmowa();
	}

	private void pobierzListePozycje(Long ceId, Long umwId) {
		this.pozycjeUmow = globalMockBo.sqlPozycja();
	}

	private void pobierzListeTypZlecen() {
		this.typyZlecen = globalMockBo.sqlTypZlecen();
	}

	private void pobierzListeOkresyPM() {
		try {
			dlgOkresyPM = zleceniaBo.sqlOkresyPm();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	private void initListyWyboru() {
		Long ceIdF = getAktywneCentra().get(0).getCeId();
		aktywneCentra = getAktywneCentra();
		noweZlecenieWyk.setCeIdK(ceIdF);
		pobierzListeKosztRD(ceIdF);
		if(mpkKosztRd != null && !mpkKosztRd.isEmpty()){
			noweZlecenieWyk.setMpkIdK(mpkKosztRd.get(0).getMpkId());
		} else {
			noweZlecenieWyk.setMpkIdK(null);
		}
		pobierzListeAdresatow(ceIdF);
		if(adresaciCentra != null && !adresaciCentra.isEmpty()){
			noweZlecenieWyk.setCeIdP(adresaciCentra.get(0).getCeId());
		} else {
			noweZlecenieWyk.setCeIdP(null);
		}
		pobierzListePrzychodRD(ceIdF);
		if(mpkPrzychodRd != null && !mpkPrzychodRd.isEmpty()){
			noweZlecenieWyk.setMpkIdP(mpkPrzychodRd.get(0).getMpkId());
			pobierzListeUmowa(ceIdF, noweZlecenieWyk.getMpkIdK());
		} else {
			noweZlecenieWyk.setMpkIdP(null);
		}
		if(umowy != null && !umowy.isEmpty()){
			noweZlecenieWyk.setUmId(umowy.get(0).getUmwId());
			pobierzListePozycje(ceIdF, noweZlecenieWyk.getUmId());
		} else {
			noweZlecenieWyk.setUmId(null);
		}
		if(pozycjeUmow != null && !pozycjeUmow.isEmpty()){
			noweZlecenieWyk.setPumId(pozycjeUmow.get(0).getPumId());
		} else {
			noweZlecenieWyk.setPumId(null);
		}
		pobierzListeTypZlecen();
		if(typyZlecen != null && !typyZlecen.isEmpty()){
			noweZlecenieWyk.setTzlId(typyZlecen.get(0).getTzlId());
		} else {
			noweZlecenieWyk.setTzlId(null);
		}
	}

	private void przygotujElementyWyswietlanie(DlgMZleceniaType typ) {
		dlgRenderedTekst = false;
		dlgRenderedTekstPK = false;
		dlgRenderedKwota = false;
		dlgRenderedKwotaProponowana = false;
		dlgRenderedCentrum = false;
		dlgRenderedOkresPM = false;

		switch (typ) {
		case PROPONUJ_KWOTE:
			dlgRenderedKwota = true;
			break;
		case USUN_ZLECENIE:
			dlgRenderedTekst = true;
			break;
		case ZGLOSZENIE_DO_ANULOWANIA:
			dlgRenderedTekst = true;
			break;
		case ZGODA_NA_ANULOWANIE:
			dlgRenderedTekst = true;
			break;
		case ODRZUCENIE_ANULOWANIA:
			dlgRenderedTekst = true;
			break;
		case PRZEPNIJ_ZLECENIE:
			dlgRenderedCentrum = true;
			break;
		case ODBIOR_ZLECENIA:
			dlgRenderedKwota = true;
			dlgRenderedOkresPM = true;
			break;
		case ANULOWANIE_ODBIORU:
			dlgRenderedTekst = true;
			break;
		case ZGLOSZENIE_ODBIORU:
			dlgRenderedTekst = true;
			break;
		case PRZYJECIE_ZLECENIA:
			dlgRenderedTekst = true;
			if (selectedZlecenie.getRwTyp().equals(Const.ZCLEC_PK)) {
				dlgRenderedTekstPK = true;
			}
			dlgRenderedOkresPM = true;
			break;
		case AKCEPTUJ_ZLECENIE:
			dlgRenderedTekst = true;
			break;
		case ODRZUC_ZLECENIE:
			dlgRenderedTekst = true;
			break;
		case AKCEPTUJ_KWOTE:
			dlgRenderedTekst = true;
			dlgRenderedKwotaProponowana = true;
			break;
		case ANULUJ_KWOTE:
			dlgRenderedTekst = true;
			dlgRenderedKwotaProponowana = true;
			break;
		default:
			break;
		}
	}

	/*
	 * Getters & Setters
	 */

	/*
	 * Funkcja wymagana w LiveScroll
	 */
	public int getSize() {
		return zlecenia.size();
	}

	public List<Zlecenie> getZlecenia() {
		return zlecenia;
	}

	public Zlecenie getSelectedZlecenie() {
		return selectedZlecenie;
	}

	public void setSelectedZlecenie(Zlecenie selectedZlecenie) {
		this.selectedZlecenie = selectedZlecenie;
	}

	public Zlecenie getNoweZlecenieWyk() {
		return noweZlecenieWyk;
	}

	public void setNoweZlecenieWyk(Zlecenie noweZlecenieWyk) {
		this.noweZlecenieWyk = noweZlecenieWyk;
	}

	public List<Okres2> getOkresy() {
		return okresy;
	}

	public void setOkresy(List<Okres2> okresy) {
		this.okresy = okresy;
	}

	public Long getOkresOdFiltr() {
		return okresOdFiltr;
	}

	public void setOkresOdFiltr(Long okresOdFiltr) {
		this.okresOdFiltr = okresOdFiltr;
	}

	public Long getOkresDoFiltr() {
		return okresDoFiltr;
	}

	public void setOkresDoFiltr(Long okresDoFiltr) {
		this.okresDoFiltr = okresDoFiltr;
	}

	public short getWyborDaty() {
		return wyborDaty;
	}

	public void setWyborDaty(short wyborDaty) {
		this.wyborDaty = wyborDaty;
	}

	public List<String> getWybraneZlecenia() {
		return wybraneZlecenia;
	}

	public void setWybraneZlecenia(List<String> wybraneZlecenia) {
		this.wybraneZlecenia = wybraneZlecenia;
	}

	public String getRwKwotaSuma(Integer fromIndex, Integer toIndex) {
		double suma = zlecenia.subList(fromIndex, toIndex).stream().mapToDouble(o -> o.getRwKwotaZnak()).sum();

		return LanguageBean.formatNumber(suma);
	}

	public String getRwKwotaZaplaconaSuma(Integer fromIndex, Integer toIndex) {
		double suma = zlecenia.subList(fromIndex, toIndex).stream().mapToDouble(o -> o.getRwKwotaZaplaconaZnak()).sum();

		return LanguageBean.formatNumber(suma);
	}

	public String getRwKwotaProponowanaSuma(Integer fromIndex, Integer toIndex) {
		double suma = zlecenia.subList(fromIndex, toIndex).stream().mapToDouble(o -> o.getRwKwotaProponowanaZnak())
				.sum();

		return LanguageBean.formatNumber(suma);
	}

	public List<Centrum> getAktywneCentra() {
		if(aktywneCentra == null)
			aktywneCentra = globalMockBo.sqlAdresat();
		return aktywneCentra;
	}

	public void setAktywneCentra(List<Centrum> aktywneCentra) {
		this.aktywneCentra = aktywneCentra;
	}

	public List<Mpk> getMpkKosztRd() {
		return mpkKosztRd;
	}

	public void setMpkKosztRd(List<Mpk> mpkKosztRd) {
		this.mpkKosztRd = mpkKosztRd;
	}

	public String getDlgTytul() {
		return dlgTytul;
	}

	public void setDlgTytul(String dlgTytul) {
		this.dlgTytul = dlgTytul;
	}

	public List<Centrum> getAdresaciCentra() {
		return adresaciCentra;
	}

	public void setAdresaciCentra(List<Centrum> adresaciCentra) {
		this.adresaciCentra = adresaciCentra;
	}

	public List<Mpk> getMpkPrzychodRd() {
		return mpkPrzychodRd;
	}

	public void setMpkPrzychodRd(List<Mpk> mpkPrzychodRd) {
		this.mpkPrzychodRd = mpkPrzychodRd;
	}

	public List<Umowa> getUmowy() {
		return umowy;
	}

	public void setUmowy(List<Umowa> umowy) {
		this.umowy = umowy;
	}

	public String getDlgTekst() {
		return dlgTekst;
	}

	public void setDlgTekst(String dlgTekst) {
		this.dlgTekst = dlgTekst;
	}

	public List<PozycjaUmow> getPozycjeUmow() {
		return pozycjeUmow;
	}

	public void setPozycjeUmow(List<PozycjaUmow> pozycjeUmow) {
		this.pozycjeUmow = pozycjeUmow;
	}

	public List<TypyZlecen> getTypyZlecen() {
		return typyZlecen;
	}

	public void setTypyZlecen(List<TypyZlecen> typyZlecen) {
		this.typyZlecen = typyZlecen;
	}

	// Obsługa wyswietlania przycisków
	private boolean renderedPUsun;
	private boolean disabledPUsun;
	private boolean renderedPPropKwote;
	private boolean disabledPPropKwote;
	private boolean renderedPAnulujKwote;
	private boolean disabledPAnulujKwote;
	private boolean renderedPAkceptKwote;
	private boolean disabledPAkceptKwote;
	private boolean renderedPZglAnulacji;
	private boolean disabledPZglAnulacji;
	private boolean renderedPZgodaAnulacji;
	private boolean disabledPZgodaAnulacji;
	private boolean renderedPOdrzucAnulacji;
	private boolean disabledPOdrzucAnulacji;
	private boolean renderedPOdbior;
	private boolean disabledPOdbior;
	private boolean renderedPPrzepnij;
	private boolean disabledPPrzepnij;
	private boolean renderedPAnulowOdbior;
	private boolean disabledPAnulowOdbior;
	private boolean renderedPAkceptuj;
	private boolean disabledPAkceptuj;
	private boolean renderedPOdrzuc;
	private boolean disabledPOdrzuc;
	private boolean renderedPPrzyjZlec;
	private boolean disabledPPrzyjZlec;
	private boolean renderedPZglosDoOdbioru;
	private boolean disabledPZglosDoOdbioru;

	private void setButtonsVisibility() {

		renderedPUsun = false;
		disabledPUsun = true;
		renderedPPropKwote = false;
		disabledPPropKwote = true;
		renderedPAkceptKwote = false;
		disabledPAkceptKwote = true;
		renderedPAnulujKwote = false;
		disabledPAnulujKwote = true;
		renderedPZglAnulacji = false;
		disabledPZglAnulacji = true;
		renderedPZgodaAnulacji = false;
		disabledPZgodaAnulacji = true;
		renderedPOdrzucAnulacji = false;
		disabledPOdrzucAnulacji = true;
		renderedPOdbior = false;
		disabledPOdbior = true;
		renderedPPrzepnij = false;
		disabledPPrzepnij = true;
		renderedPAnulowOdbior = false;
		disabledPAnulowOdbior = true;
		renderedPAkceptuj = false;
		disabledPAkceptuj = true;
		renderedPOdrzuc = false;
		disabledPOdrzuc = true;
		renderedPPrzyjZlec = false;
		disabledPPrzyjZlec = true;
		renderedPZglosDoOdbioru = false;
		disabledPZglosDoOdbioru = true;
		if (selectedZlecenie != null) {
			if (selectedZlecenie.getRodzaj().equals(Const.WLASNE_W_TOKU)) {

				renderedPUsun = true;
				disabledPUsun = !(selectedZlecenie.getRwStan().equals("WS") || selectedZlecenie.getRwStan().equals("ZA")
						|| selectedZlecenie.getRwStan().equals("OW") || selectedZlecenie.getRwStan().equals("OZ"));
				renderedPPropKwote = true;
				disabledPPropKwote = !(selectedZlecenie.getRwStan().equals("PZ"));
				renderedPAkceptKwote = true;
				disabledPAkceptKwote = !(selectedZlecenie.getRwStan().equals("MZ"));
				renderedPAnulujKwote = true;
				disabledPAnulujKwote = !(selectedZlecenie.getRwStan().equals("MZ")
						|| selectedZlecenie.getRwStan().equals("MW"));
				renderedPZglAnulacji = true;
				disabledPZglAnulacji = !(selectedZlecenie.getRwStan().equals("PZ")
						|| selectedZlecenie.getRwStan().equals("WK"));
				renderedPZgodaAnulacji = true;
				disabledPZgodaAnulacji = !(selectedZlecenie.getRwStan().equals("AZ"));
				renderedPOdrzucAnulacji = true;
				disabledPOdrzucAnulacji = !(selectedZlecenie.getRwStan().equals("AW")
						|| selectedZlecenie.getRwStan().equals("AZ"));
				renderedPOdbior = true;
				disabledPOdbior = !(selectedZlecenie.getRwStan().equals("PZ")
						|| selectedZlecenie.getRwStan().equals("WK"));
				renderedPPrzepnij = true;
				disabledPPrzepnij = false;

			} else if (selectedZlecenie.getRodzaj().equals(Const.WLASNE_ODEBRANE)) {

				renderedPAnulowOdbior = true;
				disabledPAnulowOdbior = false;

			} else if (selectedZlecenie.getRodzaj().equals(Const.OBCE_W_TOKU)) {

				renderedPAkceptuj = true;
				disabledPAkceptuj = !(selectedZlecenie.getRwStan().equals("WS"));
				renderedPOdrzuc = true;
				disabledPOdrzuc = !(selectedZlecenie.getRwStan().equals("WS")
						|| selectedZlecenie.getRwStan().equals("ZA"));
				renderedPPrzyjZlec = true;
				disabledPPrzyjZlec = !(selectedZlecenie.getRwStan().equals("ZA"));
				renderedPPropKwote = true;
				disabledPPropKwote = !(selectedZlecenie.getRwStan().equals("PZ"));
				renderedPAkceptKwote = true;
				disabledPAkceptKwote = !(selectedZlecenie.getRwStan().equals("MW"));
				renderedPAnulujKwote = true;
				disabledPAnulujKwote = !(selectedZlecenie.getRwStan().equals("MZ")
						|| selectedZlecenie.getRwStan().equals("MW"));
				renderedPZglosDoOdbioru = true;
				disabledPZglosDoOdbioru = !(selectedZlecenie.getRwStan().equals("PZ"));
				renderedPZglAnulacji = true;
				disabledPZglAnulacji = !(selectedZlecenie.getRwStan().equals("PZ")
						|| selectedZlecenie.getRwStan().equals("WK"));
				renderedPOdrzucAnulacji = true;
				disabledPOdrzucAnulacji = !(selectedZlecenie.getRwStan().equals("AW")
						|| selectedZlecenie.getRwStan().equals("AZ"));
				renderedPZgodaAnulacji = true;
				disabledPZgodaAnulacji = !(selectedZlecenie.getRwStan().equals("AW"));

			}
		}

	}

	public boolean isRenderedPUsun() {
		return renderedPUsun;
	}

	public void setRenderedPUsun(boolean renderedPUsun) {
		this.renderedPUsun = renderedPUsun;
	}

	public boolean isDisabledPUsun() {
		return disabledPUsun;
	}

	public void setDisabledPUsun(boolean disabledPUsun) {
		this.disabledPUsun = disabledPUsun;
	}

	public boolean isRenderedPPropKwote() {
		return renderedPPropKwote;
	}

	public void setRenderedPPropKwote(boolean renderedPPropKwote) {
		this.renderedPPropKwote = renderedPPropKwote;
	}

	public boolean isDisabledPPropKwote() {
		return disabledPPropKwote;
	}

	public void setDisabledPPropKwote(boolean disabledPPropKwote) {
		this.disabledPPropKwote = disabledPPropKwote;
	}

	public boolean isRenderedPAnulujKwote() {
		return renderedPAnulujKwote;
	}

	public void setRenderedPAnulujKwote(boolean renderedPAnulujKwote) {
		this.renderedPAnulujKwote = renderedPAnulujKwote;
	}

	public boolean isDisabledPAnulujKwote() {
		return disabledPAnulujKwote;
	}

	public void setDisabledPAnulujKwote(boolean disabledPAnulujKwote) {
		this.disabledPAnulujKwote = disabledPAnulujKwote;
	}

	public boolean isRenderedPAkceptKwote() {
		return renderedPAkceptKwote;
	}

	public void setRenderedPAkceptKwote(boolean renderedPAkceptKwote) {
		this.renderedPAkceptKwote = renderedPAkceptKwote;
	}

	public boolean isDisabledPAkceptKwote() {
		return disabledPAkceptKwote;
	}

	public void setDisabledPAkceptKwote(boolean disabledPAkceptKwote) {
		this.disabledPAkceptKwote = disabledPAkceptKwote;
	}

	public boolean isRenderedPZglAnulacji() {
		return renderedPZglAnulacji;
	}

	public void setRenderedPZglAnulacji(boolean renderedPZglAnulacji) {
		this.renderedPZglAnulacji = renderedPZglAnulacji;
	}

	public boolean isDisabledPZglAnulacji() {
		return disabledPZglAnulacji;
	}

	public void setDisabledPZglAnulacji(boolean disabledPZglAnulacji) {
		this.disabledPZglAnulacji = disabledPZglAnulacji;
	}

	public boolean isRenderedPZgodaAnulacji() {
		return renderedPZgodaAnulacji;
	}

	public void setRenderedPZgodaAnulacji(boolean renderedPZgodaAnulacji) {
		this.renderedPZgodaAnulacji = renderedPZgodaAnulacji;
	}

	public boolean isDisabledPZgodaAnulacji() {
		return disabledPZgodaAnulacji;
	}

	public void setDisabledPZgodaAnulacji(boolean disabledPZgodaAnulacji) {
		this.disabledPZgodaAnulacji = disabledPZgodaAnulacji;
	}

	public boolean isRenderedPOdrzucAnulacji() {
		return renderedPOdrzucAnulacji;
	}

	public void setRenderedPOdrzucAnulacji(boolean renderedPOdrzucAnulacji) {
		this.renderedPOdrzucAnulacji = renderedPOdrzucAnulacji;
	}

	public boolean isDisabledPOdrzucAnulacji() {
		return disabledPOdrzucAnulacji;
	}

	public void setDisabledPOdrzucAnulacji(boolean disabledPOdrzucAnulacji) {
		this.disabledPOdrzucAnulacji = disabledPOdrzucAnulacji;
	}

	public boolean isRenderedPOdbior() {
		return renderedPOdbior;
	}

	public void setRenderedPOdbior(boolean renderedPOdbior) {
		this.renderedPOdbior = renderedPOdbior;
	}

	public boolean isDisabledPOdbior() {
		return disabledPOdbior;
	}

	public void setDisabledPOdbior(boolean disabledPOdbior) {
		this.disabledPOdbior = disabledPOdbior;
	}

	public boolean isRenderedPPrzepnij() {
		return renderedPPrzepnij;
	}

	public void setRenderedPPrzepnij(boolean renderedPPrzepnij) {
		this.renderedPPrzepnij = renderedPPrzepnij;
	}

	public boolean isDisabledPPrzepnij() {
		return disabledPPrzepnij;
	}

	public void setDisabledPPrzepnij(boolean disabledPPrzepnij) {
		this.disabledPPrzepnij = disabledPPrzepnij;
	}

	public boolean isRenderedPAnulowOdbior() {
		return renderedPAnulowOdbior;
	}

	public void setRenderedPAnulowOdbior(boolean renderedPAnulowOdbior) {
		this.renderedPAnulowOdbior = renderedPAnulowOdbior;
	}

	public boolean isDisabledPAnulowOdbior() {
		return disabledPAnulowOdbior;
	}

	public void setDisabledPAnulowOdbior(boolean disabledPAnulowOdbior) {
		this.disabledPAnulowOdbior = disabledPAnulowOdbior;
	}

	public boolean isRenderedPAkceptuj() {
		return renderedPAkceptuj;
	}

	public void setRenderedPAkceptuj(boolean renderedPAkceptuj) {
		this.renderedPAkceptuj = renderedPAkceptuj;
	}

	public boolean isDisabledPAkceptuj() {
		return disabledPAkceptuj;
	}

	public void setDisabledPAkceptuj(boolean disabledPAkceptuj) {
		this.disabledPAkceptuj = disabledPAkceptuj;
	}

	public boolean isRenderedPOdrzuc() {
		return renderedPOdrzuc;
	}

	public void setRenderedPOdrzuc(boolean renderedPOdrzuc) {
		this.renderedPOdrzuc = renderedPOdrzuc;
	}

	public boolean isDisabledPOdrzuc() {
		return disabledPOdrzuc;
	}

	public void setDisabledPOdrzuc(boolean disabledPOdrzuc) {
		this.disabledPOdrzuc = disabledPOdrzuc;
	}

	public boolean isRenderedPPrzyjZlec() {
		return renderedPPrzyjZlec;
	}

	public void setRenderedPPrzyjZlec(boolean renderedPPrzyjZlec) {
		this.renderedPPrzyjZlec = renderedPPrzyjZlec;
	}

	public boolean isDisabledPPrzyjZlec() {
		return disabledPPrzyjZlec;
	}

	public void setDisabledPPrzyjZlec(boolean disabledPPrzyjZlec) {
		this.disabledPPrzyjZlec = disabledPPrzyjZlec;
	}

	public boolean isRenderedPZglosDoOdbioru() {
		return renderedPZglosDoOdbioru;
	}

	public void setRenderedPZglosDoOdbioru(boolean renderedPZglosDoOdbioru) {
		this.renderedPZglosDoOdbioru = renderedPZglosDoOdbioru;
	}

	public boolean isDisabledPZglosDoOdbioru() {
		return disabledPZglosDoOdbioru;
	}

	public void setDisabledPZglosDoOdbioru(boolean disabledPZglosDoOdbioru) {
		this.disabledPZglosDoOdbioru = disabledPZglosDoOdbioru;
	}

	public boolean isDlgRenderedTekst() {
		return dlgRenderedTekst;
	}

	public void setDlgRenderedTekst(boolean dlgRenderedTekst) {
		this.dlgRenderedTekst = dlgRenderedTekst;
	}

	public boolean isDlgRenderedKwota() {
		return dlgRenderedKwota;
	}

	public void setDlgRenderedKwota(boolean dlgRenderedKwota) {
		this.dlgRenderedKwota = dlgRenderedKwota;
	}

	public boolean isDlgRenderedCentrum() {
		return dlgRenderedCentrum;
	}

	public void setDlgRenderedCentrum(boolean dlgRenderedCentrum) {
		this.dlgRenderedCentrum = dlgRenderedCentrum;
	}

	public boolean isDlgRenderedOkresPM() {
		return dlgRenderedOkresPM;
	}

	public void setDlgRenderedOkresPM(boolean dlgRenderedOkresPM) {
		this.dlgRenderedOkresPM = dlgRenderedOkresPM;
	}

	public Double getDlgKwota() {
		return dlgKwota;
	}

	public void setDlgKwota(Double dlgKwota) {
		this.dlgKwota = dlgKwota;
	}

	public Long getDlgCeDoF() {
		return dlgCeDoF;
	}

	public void setDlgCeDoF(Long dlgCeDoF) {
		this.dlgCeDoF = dlgCeDoF;
	}

	public Boolean getDlgZLPM() {
		return dlgZLPM;
	}

	public void setDlgZLPM(Boolean dlgZLPM) {
		this.dlgZLPM = dlgZLPM;
	}

	public List<Okres2> getDlgOkresyPM() {
		return dlgOkresyPM;
	}

	public void setDlgOkresyPM(List<Okres2> dlgOkresyPM) {
		this.dlgOkresyPM = dlgOkresyPM;
	}

	public Long getDlgOkPMIdF() {
		return dlgOkPMIdF;
	}

	public void setDlgOkPMIdF(Long dlgOkPMIdF) {
		this.dlgOkPMIdF = dlgOkPMIdF;
	}

	public String getDlgTekstPK() {
		return dlgTekstPK;
	}

	public void setDlgTekstPK(String dlgTekstPK) {
		this.dlgTekstPK = dlgTekstPK;
	}

	public boolean isDlgRenderedTekstPK() {
		return dlgRenderedTekstPK;
	}

	public void setDlgRenderedTekstPK(boolean dlgRenderedTekstPK) {
		this.dlgRenderedTekstPK = dlgRenderedTekstPK;
	}

	public String getRodzajZleceniaOpis() {
		return rodzajZleceniaOpis;
	}

	public void setRodzajZleceniaOpis(String rodzajZleceniaOpis) {
		this.rodzajZleceniaOpis = rodzajZleceniaOpis;
	}

	public String getQ1() {
		return q1;
	}

	public void setQ1(String q1) {
		this.q1 = q1;
	}

	public String getQ2() {
		return q2;
	}

	public void setQ2(String q2) {
		this.q2 = q2;
	}

	public String getQ3() {
		return q3;
	}

	public void setQ3(String q3) {
		this.q3 = q3;
	}

	public String getQ4() {
		return q4;
	}

	public void setQ4(String q4) {
		this.q4 = q4;
	}

	public DlgMZleceniaType getDlgTyp() {
		return dlgTyp;
	}

	public void setDlgTyp(DlgMZleceniaType dlgTyp) {
		this.dlgTyp = dlgTyp;
	}

	public boolean isDlgRenderedKwotaProponowana() {
		return dlgRenderedKwotaProponowana;
	}

	public void setDlgRenderedKwotaProponowana(boolean dlgRenderedKwotaProponowana) {
		this.dlgRenderedKwotaProponowana = dlgRenderedKwotaProponowana;
	}

	public String getConfirm() {
		return confirm;
	}

	public void setConfirm(String confirm) {
		this.confirm = confirm;
	}

	public String getCancel() {
		return cancel;
	}

	public void setCancel(String cancel) {
		this.cancel = cancel;
	}

}
