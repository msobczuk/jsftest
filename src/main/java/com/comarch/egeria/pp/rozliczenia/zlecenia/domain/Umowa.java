package com.comarch.egeria.pp.rozliczenia.zlecenia.domain;

public class Umowa {

	private Long umwId;
	private String umwNumer;
	private Long mppMpkId;

	public void setUmwId(Long umwId) {
		this.umwId = umwId;
	}

	public Long getUmwId() {
		return umwId;
	}

	public void setUmwNumer(String umwNumer) {
		this.umwNumer = umwNumer;
	}

	public String getUmwNumer() {
		return umwNumer;
	}

	public void setMppMpkId(Long mppMpkId) {
		this.mppMpkId = mppMpkId;
	}

	public Long getMppMpkId() {
		return mppMpkId;
	}

}
