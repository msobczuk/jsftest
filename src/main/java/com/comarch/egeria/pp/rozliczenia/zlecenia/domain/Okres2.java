package com.comarch.egeria.pp.rozliczenia.zlecenia.domain;

import java.sql.Date;

public class Okres2 {

	private Long okId;
	private String miesiac;
	private Long numerMiesiaca;
	private String okMiesiacLong;
	private String okZh001;
	private String okZh002;
	private String okZh003;
	private String okFPokazywac;
	private Long minionyOkres;
	private Long okRok;
	private Date okPierwszyDzien;
	private Date okOstatniDzien;

	@Override
	public String toString() {
		return "Okres2 [okId=" + okId + ", miesiac=" + miesiac + ", numerMiesiaca=" + numerMiesiaca + ", okMiesiacLong="
				+ okMiesiacLong + ", okZh001=" + okZh001 + ", okZh002=" + okZh002 + ", okZh003=" + okZh003
				+ ", okFPokazywac=" + okFPokazywac + ", minionyOkres=" + minionyOkres + ", okRok=" + okRok
				+ ", okPierwszyDzien=" + okPierwszyDzien + ", okOstatniDzien=" + okOstatniDzien + "]";
	}

	public void setOkId(Long okresOdFiltr) {
		this.okId = okresOdFiltr;
	}

	public Long getOkId() {
		return okId;
	}

	public void setMiesiac(String miesiac) {
		this.miesiac = miesiac;
	}

	public String getMiesiac() {
		return miesiac;
	}

	public void setNumerMiesiaca(Long numerMiesiaca) {
		this.numerMiesiaca = numerMiesiaca;
	}

	public Long getNumerMiesiaca() {
		return numerMiesiaca;
	}

	public void setOkMiesiacLong(String okMiesiacLong) {
		this.okMiesiacLong = okMiesiacLong;
	}

	public String getOkMiesiacLong() {
		return okMiesiacLong;
	}

	public void setOkZh001(String okZh001) {
		this.okZh001 = okZh001;
	}

	public String getOkZh001() {
		return okZh001;
	}

	public void setOkZh002(String okZh002) {
		this.okZh002 = okZh002;
	}

	public String getOkZh002() {
		return okZh002;
	}

	public void setOkZh003(String okZh003) {
		this.okZh003 = okZh003;
	}

	public String getOkZh003() {
		return okZh003;
	}

	public void setOkFPokazywac(String okFPokazywac) {
		this.okFPokazywac = okFPokazywac;
	}

	public String getOkFPokazywac() {
		return okFPokazywac;
	}

	public void setMinionyOkres(Long minionyOkres) {
		this.minionyOkres = minionyOkres;
	}

	public Long getMinionyOkres() {
		return minionyOkres;
	}

	public void setOkRok(Long okRok) {
		this.okRok = okRok;
	}

	public Long getOkRok() {
		return okRok;
	}

	public void setOkPierwszyDzien(Date okPierwszyDzien) {
		this.okPierwszyDzien = okPierwszyDzien;
	}

	public Date getOkPierwszyDzien() {
		return okPierwszyDzien;
	}

	public void setOkOstatniDzien(Date okOstatniDzien) {
		this.okOstatniDzien = okOstatniDzien;
	}

	public Date getOkOstatniDzien() {
		return okOstatniDzien;
	}

}
