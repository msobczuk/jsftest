package com.comarch.egeria.pp.rozliczenia.zlecenia.domain;

public class TypyZlecen {

	private Long tzlId;
	private String tzlNazwa;
	private String tzlFPodlegaUprawnieniu;
	private Long tzlRodKosztId;
	private Long tzlRodPrzychId;
	private String tzlFPokazywac;
	private String rodKoszNazwa;
	private String rodPrzychNazwa;

	public void setTzlId(Long tzlId) {
		this.tzlId = tzlId;
	}

	public Long getTzlId() {
		return tzlId;
	}

	public void setTzlNazwa(String tzlNazwa) {
		this.tzlNazwa = tzlNazwa;
	}

	public String getTzlNazwa() {
		return tzlNazwa;
	}

	public void setTzlFPodlegaUprawnieniu(String tzlFPodlegaUprawnieniu) {
		this.tzlFPodlegaUprawnieniu = tzlFPodlegaUprawnieniu;
	}

	public String getTzlFPodlegaUprawnieniu() {
		return tzlFPodlegaUprawnieniu;
	}

	public void setTzlRodKosztId(Long tzlRodKosztId) {
		this.tzlRodKosztId = tzlRodKosztId;
	}

	public Long getTzlRodKosztId() {
		return tzlRodKosztId;
	}

	public void setTzlRodPrzychId(Long tzlRodPrzychId) {
		this.tzlRodPrzychId = tzlRodPrzychId;
	}

	public Long getTzlRodPrzychId() {
		return tzlRodPrzychId;
	}

	public void setTzlFPokazywac(String tzlFPokazywac) {
		this.tzlFPokazywac = tzlFPokazywac;
	}

	public String getTzlFPokazywac() {
		return tzlFPokazywac;
	}

	public void setRodKoszNazwa(String rodKoszNazwa) {
		this.rodKoszNazwa = rodKoszNazwa;
	}

	public String getRodKoszNazwa() {
		return rodKoszNazwa;
	}

	public void setRodPrzychNazwa(String rodPrzychNazwa) {
		this.rodPrzychNazwa = rodPrzychNazwa;
	}

	public String getRodPrzychNazwa() {
		return rodPrzychNazwa;
	}
	
}

