package com.comarch.egeria.pp.rozliczenia.zlecenia.domain;

import java.sql.Date;

public class Mpk {

	private Long mpkId;
	private String mpkOznaczenie;
	private Date mpkKiedyOtwarte;
	private Date mpkKiedyZamkniete;
	private String mpkKod;
	private Long mpkCeId;
	private Long mpkSort;
	private String mpkFPrzychodowy;
	private String mpkFPokazywac;
	private String mpkFPodzielnik;
	private String mpkFRwWymagaPum;
	private String mpkOpis;
	private String mpkHsort;

	@Override
	public String toString() {
		return "PikMpk [mpkId=" + mpkId + ", mpkOznaczenie=" + mpkOznaczenie + ", mpkKiedyOtwarte=" + mpkKiedyOtwarte
				+ ", mpkKiedyZamkniete=" + mpkKiedyZamkniete + ", mpkKod=" + mpkKod + ", mpkCeId=" + mpkCeId
				+ ", mpkSort=" + mpkSort + ", mpkFPrzychodowy=" + mpkFPrzychodowy + ", mpkFPokazywac=" + mpkFPokazywac
				+ ", mpkFPodzielnik=" + mpkFPodzielnik + ", mpkFRwWymagaPum=" + mpkFRwWymagaPum + ", mpkOpis=" + mpkOpis
				+ ", mpkHsort=" + mpkHsort + "]";
	}

	public Long getMpkId() {
		return mpkId;
	}

	public void setMpkId(Long mpkId) {
		this.mpkId = mpkId;
	}

	public String getMpkOznaczenie() {
		return mpkOznaczenie;
	}

	public void setMpkOznaczenie(String mpkOznaczenie) {
		this.mpkOznaczenie = mpkOznaczenie;
	}

	public Date getMpkKiedyOtwarte() {
		return mpkKiedyOtwarte;
	}

	public void setMpkKiedyOtwarte(Date mpkKiedyOtwarte) {
		this.mpkKiedyOtwarte = mpkKiedyOtwarte;
	}

	public Date getMpkKiedyZamkniete() {
		return mpkKiedyZamkniete;
	}

	public void setMpkKiedyZamkniete(Date mpkKiedyZamkniete) {
		this.mpkKiedyZamkniete = mpkKiedyZamkniete;
	}

	public String getMpkKod() {
		return mpkKod;
	}

	public void setMpkKod(String mpkKod) {
		this.mpkKod = mpkKod;
	}

	public Long getMpkCeId() {
		return mpkCeId;
	}

	public void setMpkCeId(Long mpkEeId) {
		this.mpkCeId = mpkEeId;
	}

	public Long getMpkSort() {
		return mpkSort;
	}

	public void setMpkSort(Long mpkSort) {
		this.mpkSort = mpkSort;
	}

	public String getMpkFPrzychodowy() {
		return mpkFPrzychodowy;
	}

	public void setMpkFPrzychodowy(String mpkFPrzychodowy) {
		this.mpkFPrzychodowy = mpkFPrzychodowy;
	}

	public String getMpkFPokazywac() {
		return mpkFPokazywac;
	}

	public void setMpkFPokazywac(String mpkFPokazywac) {
		this.mpkFPokazywac = mpkFPokazywac;
	}

	public String getMpkFPodzielnik() {
		return mpkFPodzielnik;
	}

	public void setMpkFPodzielnik(String mpkFPodzielnik) {
		this.mpkFPodzielnik = mpkFPodzielnik;
	}

	public String getMpkFRwWymagaPum() {
		return mpkFRwWymagaPum;
	}

	public void setMpkFRwWymagaPum(String mpkFRwWymagaPum) {
		this.mpkFRwWymagaPum = mpkFRwWymagaPum;
	}

	public String getMpkOpis() {
		return mpkOpis;
	}

	public void setMpkOpis(String mpkOpis) {
		this.mpkOpis = mpkOpis;
	}

	public String getMpkHsort() {
		return mpkHsort;
	}

	public void setMpkHsort(String mpkHsort) {
		this.mpkHsort = mpkHsort;
	}

}
