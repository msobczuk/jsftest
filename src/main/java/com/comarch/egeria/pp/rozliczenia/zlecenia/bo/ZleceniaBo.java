package com.comarch.egeria.pp.rozliczenia.zlecenia.bo;

import java.sql.SQLException;
import java.util.List;

import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Centrum;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Mpk;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Okres2;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Zlecenie;



public interface ZleceniaBo {

	/**
	 * Wywołanie metody pik_pck_zlecenia.sqlZlecenia(...)
	 * 
	 * @param -
	 *            Centrum
	 * @param -
	 *            Zlecenie
	 * @return - Lista zleceń
	 * @throws -
	 *             SQLException
	 */
	public List<Zlecenie> sqlZlecenia(Centrum ce, Zlecenie zlec) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.sqlZleceniaWToku(...)
	 * 
	 * @param -
	 *            Centrum
	 * @return - Lista zleceń
	 * @throws -
	 *             SQLException
	 */
	public List<Zlecenie> sqlZleceniaWToku(Centrum ce) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.sqlMpkKoszt(...)
	 * 
	 * @param -
	 *            Long
	 * @return - Lista RD
	 * @throws -
	 *             SQLException
	 */
	public List<Mpk> sqlMpkKoszt(Long ceId) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.sqlMpkPrzychod(...)
	 * 
	 * @param -
	 *            Long
	 * @return - Lista RD
	 * @throws -
	 *             SQLException
	 */
	public List<Mpk> sqlMpkPrzychod(Long ceId) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.sqlOkresyPm(...)
	 * 
	 * @return - Lista Okresów
	 * @throws -
	 *             SQLException
	 */
	public List<Okres2> sqlOkresyPm() throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.sqlZleceniaOdebrane(...)
	 * 
	 * @return - Lista Okresów
	 * @throws -
	 *             SQLException
	 */
	public List<Zlecenie> sqlZleceniaOdebrane(Centrum ce, Okres2 okOd, Okres2 okDo, short wgDaty) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.sqlZleceniaWykonane(...)
	 * 
	 * @return - Lista Okresów
	 * @throws -
	 *             SQLException
	 */
	public List<Zlecenie> sqlZleceniaWykonane(Centrum ce, Okres2 okOd, Okres2 okDo, short wgDaty) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.sqlZleceniaWszystkie(...)
	 * 
	 * @return - Lista Okresów
	 * @throws -
	 *             SQLException
	 */
	public List<Zlecenie> sqlZleceniaWszystkie(Centrum ce, Okres2 okOd, Okres2 okDo, short wgDaty) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.wystaw(...)
	 * 
	 * @param -
	 *            Zlecenie (Wypełnione jedynie pola wymagane do wstawienia
	 *            rekordu)
	 * @return - Long - Numer wystawionego zlecenia
	 * @throws -
	 *             SQLException
	 */
	public Long wystaw(Zlecenie zlec) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.akceptuj(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * 
	 * @throws -
	 *             SQLException
	 */
	public void akceptuj(Zlecenie zlec) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.usun(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * 
	 * @throws -
	 *             SQLException
	 */
	public void usun(Zlecenie zlec) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.odrzuc(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * 
	 * @throws -
	 *             SQLException
	 */
	public void odrzuc(Zlecenie zlec) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.przyjmij(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * 
	 * @throws -
	 *             SQLException
	 */
	public void przyjmij(Zlecenie zlec, String zlecPM, Long zlecPMOkresId) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.zgloszenieAnulowania(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * 
	 * @throws -
	 *             SQLException
	 */
	public void zgloszenieAnulowania(Zlecenie zlec) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.zgodaNaAnulowanie(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * 
	 * @throws -
	 *             SQLException
	 */
	public void zgodaNaAnulowanie(Zlecenie zlec) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.odrzucenieAnulowania(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * 
	 * @throws -
	 *             SQLException
	 */
	public void odrzucenieAnulowania(Zlecenie zlec) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.zglosDoOdbioru(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * 
	 * @throws -
	 *             SQLException
	 */
	public void zglosDoOdbioru(Zlecenie zlec) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.odbierz(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * 
	 * @throws -
	 *             SQLException
	 */
	public void odbierz(Zlecenie zlec, Double kwota, String zlecPM, Long zlecPMOkresId) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.przyjmijZleceniePk(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * 
	 * @throws -
	 *             SQLException
	 */
	public void przyjmijZleceniePk(Zlecenie zlec, String zlecPM, Long zlecPMOkresId) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.anulowanieOdbioru(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * 
	 * @throws -
	 *             SQLException
	 */
	public void anulowanieOdbioru(Zlecenie zlec) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.przepnijZlecenie(...)
	 * 
	 * @param -
	 *            Przepinane zlecenie Zlecenie
	 * @param -
	 *            Id centrum do którego zostanie przepięte zlecenie
	 * @throws -
	 *             SQLException
	 */
	public void przepnijZlecenie(Zlecenie zlec, Long ceIdDo) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.wprowadzKwoteProponowana(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * @param -
	 *            proponowana kwota
	 * @throws -
	 *             SQLException
	 */
	public void wprowadzKwoteProponowana(Zlecenie zlec, Double kwotaProp) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.akceptujKwoteProponowana(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * @throws -
	 *             SQLException
	 */
	public void akceptujKwoteProponowana(Zlecenie zlec) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.anulujKwoteProponowana(...)
	 * 
	 * @param -
	 *            Modyfikowane Zlecenie
	 * @throws -
	 *             SQLException
	 */
	public void anulujKwoteProponowana(Zlecenie zlec) throws SQLException;

	/**
	 * Wywołanie metody pik_pck_zlecenia.wystawZlecenieZarzadu(...)
	 * 
	 * @param -
	 *            Zlecenie
	 * @return - Long - Numer wystawionego zlecenia
	 * @throws -
	 *             SQLException
	 */
	public Long wystawZlecenieZarzadu(Zlecenie zlec) throws SQLException;
}
