package com.comarch.egeria.pp.rozliczenia.zlecenia.domain;

public class PozycjaUmow {

	private Long pumId;
	private String pumOznaczenie;
	private String pumNazwa;
	private Long pumUmwId;

	public void setPumId(Long pumId) {
		this.pumId = pumId;
	}

	public Long getPumId() {
		return pumId;
	}

	public void setPumOznaczenie(String pumOznaczenie) {
		this.pumOznaczenie = pumOznaczenie;
	}

	public String getPumOznaczenie() {
		return pumOznaczenie;
	}

	public void setPumNazwa(String pumNazwa) {
		this.pumNazwa = pumNazwa;
	}

	public String getPumNazwa() {
		return pumNazwa;
	}

	public void setPumUmwId(Long pumUmwId) {
		this.pumUmwId = pumUmwId;
	}

	public Long getPumUmwId() {
		return pumUmwId;
	}
}

