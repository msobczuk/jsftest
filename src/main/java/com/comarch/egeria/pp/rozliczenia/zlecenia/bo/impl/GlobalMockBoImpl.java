package com.comarch.egeria.pp.rozliczenia.zlecenia.bo.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import com.comarch.egeria.pp.rozliczenia.zlecenia.bo.GlobalMockBo;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Centrum;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.PozycjaUmow;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.TypyZlecen;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Umowa;

@Named
public class GlobalMockBoImpl implements GlobalMockBo {
	private static List<Umowa> umowy;
	private static List<TypyZlecen> typyZlecen;
	private static List<PozycjaUmow> pozycjeUmow;
	private static List<Centrum> adresaci;

	@Override
	public List<Umowa> sqlUmowa() {
		if(umowy == null) {
			umowy = new ArrayList<>();
			Umowa umowa = new Umowa();
			umowa.setUmwNumer("XDDD");
			umowa.setMppMpkId(1L);
			umowa.setUmwId(1L);
			umowy.add(umowa);
		}
		return umowy;
	}

	@Override
	public List<TypyZlecen> sqlTypZlecen() {
		if(typyZlecen == null) {
			typyZlecen = new ArrayList<>();
			TypyZlecen typyZlecen = new TypyZlecen();
			typyZlecen.setTzlId(1L);
			typyZlecen.setTzlRodKosztId(1L);
			typyZlecen.setTzlRodPrzychId(1L);
			typyZlecen.setRodKoszNazwa("costam");
			GlobalMockBoImpl.typyZlecen.add(typyZlecen);
		}
		return typyZlecen;
	}

	@Override
	public List<PozycjaUmow> sqlPozycja() {
		if(pozycjeUmow == null) {
			pozycjeUmow = new ArrayList<>();
			PozycjaUmow pozycjaUmow = new PozycjaUmow();
			pozycjaUmow.setPumId(1L);
			pozycjaUmow.setPumUmwId(1L);
			pozycjaUmow.setPumOznaczenie("XD");
			pozycjaUmow.setPumNazwa("pumNazwa");
			pozycjeUmow.add(pozycjaUmow);
		}
		return pozycjeUmow;
	}

	@Override
	public List<Centrum> sqlAdresat() {
		if(adresaci == null) {
			adresaci = new ArrayList<>();
			Centrum adresat = new Centrum();
			adresat.setCeId(1L);
			adresat.setCeNazwa("Śledzące Centrum Biurowe");
			adresaci.add(adresat);
		}
		return adresaci;
	}

}
