package com.comarch.egeria.pp.rozliczenia.zlecenia.domain;

import java.sql.Date;

import com.comarch.egeria.web.common.utils.constants.CtlUtils;



public class Zlecenie {
	private Long rwId;
	private Date rwDataWystawienia;
	private Date rwDataWykonania;
	private String rwTyp;
	private Double rwKwota;
	private String rwStan;
	private String centrumP;
	private String centrumK;
	private String centrumPnazwa;
	private String centrumKnazwa;
	private String rwOpis;
	private String mpkP;
	private String mpkK;
	private String mpkKopis;
	private String mpkPopis;
	private String opisSzerzej;
	private Date rwDataOdbioru;
	private Double rwKwotaZaplacona;
	private Double rwKwotaProponowana;
	private String tzlNazwa;
	private Long tzlId;

	private Long mpkIdK;
	private Long mpkIdP;
	private Long ceIdK;
	private Long ceIdP;
	private Boolean zleceniePm;
	private Long okIdPm;
	private Long umId;
	private Long pumId;
	private java.util.Date rwDataWykonaniaUtl;

	private String rodzaj;
	private Centrum kontekstCentrum;

	public Long getRwId() {
		return rwId;
	}

	public void setRwId(Long rwId) {
		this.rwId = rwId;
	}

	public Date getRwDataWystawienia() {
		return rwDataWystawienia;
	}

	public void setRwDataWystawienia(Date rwDataWystawienia) {
		this.rwDataWystawienia = rwDataWystawienia;
	}

	public Date getRwDataWykonania() {
		return rwDataWykonania;
	}

	public void setRwDataWykonania(Date rwDataWykonania) {
		this.rwDataWykonania = rwDataWykonania;
	}

	public String getRwTyp() {
		return rwTyp;
	}

	public void setRwTyp(String rwTyp) {
		this.rwTyp = rwTyp;
	}

	public Double getRwKwota() {
		return rwKwota;
	}

	public Double getRwKwotaZnak() {
		return CtlUtils.kwotaZlecZZnakiem(this.rwKwota, this.rodzaj, this.rwTyp);
	}

	public void setRwKwota(Double rwKwota) {
		this.rwKwota = rwKwota;
	}

	public String getRwStan() {
		return rwStan;
	}

	public void setRwStan(String rwStan) {
		this.rwStan = rwStan;
	}

	public String getCentrumP() {
		return centrumP;
	}

	public void setCentrumP(String centrumP) {
		this.centrumP = centrumP;
	}

	public String getCentrumK() {
		return centrumK;
	}

	public void setCentrumK(String centrumK) {
		this.centrumK = centrumK;
	}

	public String getCentrumPnazwa() {
		return centrumPnazwa;
	}

	public void setCentrumPnazwa(String centrumPnazwa) {
		this.centrumPnazwa = centrumPnazwa;
	}

	public String getCentrumKnazwa() {
		return centrumKnazwa;
	}

	public void setCentrumKnazwa(String centrumKnazwa) {
		this.centrumKnazwa = centrumKnazwa;
	}

	public String getRwOpis() {
		return rwOpis;
	}

	public void setRwOpis(String rwOpis) {
		this.rwOpis = rwOpis;
	}

	public String getMpkP() {
		return mpkP;
	}

	public void setMpkP(String mpkP) {
		this.mpkP = mpkP;
	}

	public String getMpkK() {
		return mpkK;
	}

	public void setMpkK(String mpkK) {
		this.mpkK = mpkK;
	}

	public String getMpkKopis() {
		return mpkKopis;
	}

	public void setMpkKopis(String mpkKopis) {
		this.mpkKopis = mpkKopis;
	}

	public String getMpkPopis() {
		return mpkPopis;
	}

	public void setMpkPopis(String mpkPopis) {
		this.mpkPopis = mpkPopis;
	}

	public String getOpisSzerzej() {
		return opisSzerzej;
	}

	public void setOpisSzerzej(String opisSzerzej) {
		this.opisSzerzej = opisSzerzej;
	}

	public Date getRwDataOdbioru() {
		return rwDataOdbioru;
	}

	public void setRwDataOdbioru(Date rwDataOdbioru) {
		this.rwDataOdbioru = rwDataOdbioru;
	}

	public Double getRwKwotaZaplacona() {
		return rwKwotaZaplacona;
	}
	
	public Double getRwKwotaZaplaconaZnak() {
		return CtlUtils.kwotaZlecZZnakiem(this.rwKwotaZaplacona, this.rodzaj, this.rwTyp);
	}

	public void setRwKwotaZaplacona(Double rwKwotaZaplacona) {
		this.rwKwotaZaplacona = rwKwotaZaplacona;
	}

	public Double getRwKwotaProponowana() {
		return rwKwotaProponowana;
	}

	public Double getRwKwotaProponowanaZnak() {
		return CtlUtils.kwotaZlecZZnakiem(this.rwKwotaProponowana, this.rodzaj, this.rwTyp);
	}
	
	public void setRwKwotaProponowana(Double rwKwotaProponowana) {
		this.rwKwotaProponowana = rwKwotaProponowana;
	}

	public String getTzlNazwa() {
		return tzlNazwa;
	}

	public void setTzlNazwa(String tzlNazwa) {
		this.tzlNazwa = tzlNazwa;
	}

	public Long getTzlId() {
		return tzlId;
	}

	public void setTzlId(Long tzlId) {
		this.tzlId = tzlId;
	}

	public Long getMpkIdK() {
		return mpkIdK;
	}

	public void setMpkIdK(Long mpkIdK) {
		this.mpkIdK = mpkIdK;
	}

	public Long getMpkIdP() {
		return mpkIdP;
	}

	public void setMpkIdP(Long mpkIdP) {
		this.mpkIdP = mpkIdP;
	}

	public Long getCeIdK() {
		return ceIdK;
	}

	public void setCeIdK(Long ceIdK) {
		this.ceIdK = ceIdK;
	}

	public Long getCeIdP() {
		return ceIdP;
	}

	public void setCeIdP(Long ceIdP) {
		this.ceIdP = ceIdP;
	}

	public Boolean getZleceniePm() {
		return zleceniePm;
	}

	public void setZleceniePm(Boolean zleceniePm) {
		this.zleceniePm = zleceniePm;
	}

	public Long getOkIdPm() {
		return okIdPm;
	}

	public void setOkIdPm(Long okIdPm) {
		this.okIdPm = okIdPm;
	}

	public Long getPumId() {
		return pumId;
	}

	public void setPumId(Long pumId) {
		this.pumId = pumId;
	}

	public String getRodzaj() {
		return rodzaj;
	}

	public void setRodzaj(String rodzaj) {
		this.rodzaj = rodzaj;
	}

	public Centrum getKontekstCentrum() {
		return kontekstCentrum;
	}

	public void setKontekstCentrum(Centrum kontekstCentrum) {
		this.kontekstCentrum = kontekstCentrum;
	}

	public Long getUmId() {
		return umId;
	}

	public void setUmId(Long umId) {
		this.umId = umId;
	}

	public java.util.Date getRwDataWykonaniaUtl() {
		return rwDataWykonaniaUtl;
	}

	public void setRwDataWykonaniaUtl(java.util.Date rwDataWykonaniaUtl) {
		this.rwDataWykonania = new java.sql.Date(rwDataWykonaniaUtl.getTime());
		this.rwDataWykonaniaUtl = rwDataWykonaniaUtl;
	}

	public int getRwRowKey() {
		return this.hashCode();
	}

	@Override
	public String toString() {
		return "Zlecenie [rwId=" + rwId + ", rwDataWystawienia=" + rwDataWystawienia + ", rwDataWykonania="
				+ rwDataWykonania + ", rwTyp=" + rwTyp + ", rwKwota=" + rwKwota + ", rwStan=" + rwStan + ", centrumP="
				+ centrumP + ", centrumK=" + centrumK + ", centrumPnazwa=" + centrumPnazwa + ", centrumKnazwa="
				+ centrumKnazwa + ", rwOpis=" + rwOpis + ", mpkP=" + mpkP + ", mpkK=" + mpkK + ", mpkKopis=" + mpkKopis
				+ ", mpkPopis=" + mpkPopis + ", opisSzerzej=" + opisSzerzej + ", rwDataOdbioru=" + rwDataOdbioru
				+ ", rwKwotaZaplacona=" + rwKwotaZaplacona + ", rwKwotaProponowana=" + rwKwotaProponowana
				+ ", tzlNazwa=" + tzlNazwa + ", tzlId=" + tzlId + ", mpkIdK=" + mpkIdK + ", mpkIdP=" + mpkIdP
				+ ", ceIdK=" + ceIdK + ", ceIdP=" + ceIdP + ", zleceniePm=" + zleceniePm + ", okIdPm=" + okIdPm
				+ ", umId=" + umId + ", pumId=" + pumId + ", rwDataWykonaniaUtl=" + rwDataWykonaniaUtl + ", rodzaj="
				+ rodzaj + ", kontekstCentrum=" + kontekstCentrum + "]";
	}

}
