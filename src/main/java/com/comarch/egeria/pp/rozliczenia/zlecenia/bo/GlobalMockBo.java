package com.comarch.egeria.pp.rozliczenia.zlecenia.bo;

import java.util.List;

import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Centrum;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.PozycjaUmow;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.TypyZlecen;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Umowa;

public interface GlobalMockBo {
	List<Umowa> sqlUmowa();
	List<TypyZlecen> sqlTypZlecen();
	List<PozycjaUmow> sqlPozycja();
	List<Centrum> sqlAdresat();
}
