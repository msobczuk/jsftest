package com.comarch.egeria.pp.rozliczenia.zlecenia.bo.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.comarch.egeria.pp.rozliczenia.zlecenia.bo.ZleceniaBo;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Centrum;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Mpk;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Okres2;
import com.comarch.egeria.pp.rozliczenia.zlecenia.domain.Zlecenie;

@Named
public class ZleceniaBoImpl implements ZleceniaBo {
	private static List<Zlecenie> zleceniaMock;
	private static List<Mpk> mpkMock;
	private static List<Okres2> okres2Mock;

	private List<Zlecenie> getMock() {
		if(zleceniaMock == null) {
			zleceniaMock = new ArrayList<>();
			Zlecenie zlecenie = new Zlecenie();
			zlecenie.setMpkIdP(1L);
			zlecenie.setCeIdP(1L);
			zlecenie.setOkIdPm(1L);
			zlecenie.setRwId(1L);
			zlecenie.setRwKwotaProponowana(16.66D);
			zlecenie.setRwOpis("super opis");
			zleceniaMock.add(zlecenie);
		}
		return zleceniaMock;
	}

	private List<Mpk> getMpkMock() {
		if(mpkMock == null) {
			mpkMock = new ArrayList<>();
			Mpk mpk = new Mpk();
			mpk.setMpkCeId(1L);
			mpk.setMpkId(1L);
			mpkMock.add(mpk);
		}
		return mpkMock;
	}

	private List<Okres2> getOkres2Mock() {
		if(okres2Mock == null) {
			okres2Mock = new ArrayList<>();
			Okres2 okres2 = new Okres2();
			okres2.setOkId(1L);
			okres2.setMiesiac("luty");
			okres2Mock.add(okres2);
		}
		return okres2Mock;
	}

	Logger log = LogManager.getLogger(ZleceniaBoImpl.class);

	@Override
	public List<Zlecenie> sqlZlecenia(Centrum ce, Zlecenie zlec) throws SQLException {
		return getMock();
	}

	@Override
	public List<Zlecenie> sqlZleceniaWToku(Centrum ce) throws SQLException {
		return getMock();
	}

	@Override
	public List<Mpk> sqlMpkKoszt(Long ceId) throws SQLException {
		return getMpkMock();
	}

	@Override
	public List<Mpk> sqlMpkPrzychod(Long ceId) throws SQLException {
		return getMpkMock();
	}

	@Override
	public List<Okres2> sqlOkresyPm() throws SQLException {
		return getOkres2Mock();
	}

	@Override
	public List<Zlecenie> sqlZleceniaOdebrane(Centrum ce, Okres2 okOd, Okres2 okDo, short wgDaty) throws SQLException {
		return getMock();
	}

	@Override
	public List<Zlecenie> sqlZleceniaWykonane(Centrum ce, Okres2 okOd, Okres2 okDo, short wgDaty) throws SQLException {
		return getMock();
	}

	@Override
	public List<Zlecenie> sqlZleceniaWszystkie(Centrum ce, Okres2 okOd, Okres2 okDo, short wgDaty) throws SQLException {
		return getMock();
	}

	@Override
	public Long wystaw(Zlecenie zlec) throws SQLException {
		zlec.setRwId(10L);
		getMock().add(zlec);
		return 10L;
	}

	@Override
	public void akceptuj(Zlecenie zlec) throws SQLException {
	}

	@Override
	public void usun(Zlecenie zlec) throws SQLException {
		getMock().remove(zlec);
	}

	@Override
	public void odrzuc(Zlecenie zlec) throws SQLException {
	}

	@Override
	public void przyjmij(Zlecenie zlec, String zlecPM, Long zlecPMOkresId) throws SQLException {
	}

	@Override
	public void zgloszenieAnulowania(Zlecenie zlec) throws SQLException {
	}

	@Override
	public void zgodaNaAnulowanie(Zlecenie zlec) throws SQLException {
	}

	@Override
	public void odrzucenieAnulowania(Zlecenie zlec) throws SQLException {
	}

	@Override
	public void zglosDoOdbioru(Zlecenie zlec) throws SQLException {
	}

	@Override
	public void odbierz(Zlecenie zlec, Double kwota, String zlecPM, Long zlecPMOkresId) throws SQLException {
	}

	@Override
	public void przyjmijZleceniePk(Zlecenie zlec, String zlecPM, Long zlecPMOkresId) throws SQLException {
	}

	@Override
	public void anulowanieOdbioru(Zlecenie zlec) throws SQLException {
	}

	@Override
	public void przepnijZlecenie(Zlecenie zlec, Long ceIdDo) throws SQLException {
	}

	@Override
	public void wprowadzKwoteProponowana(Zlecenie zlec, Double kwotaProp) throws SQLException {
	}

	@Override
	public void akceptujKwoteProponowana(Zlecenie zlec) throws SQLException {
	}

	@Override
	public void anulujKwoteProponowana(Zlecenie zlec) throws SQLException {
	}

	@Override
	public Long wystawZlecenieZarzadu(Zlecenie zlec) throws SQLException {
		return wystaw(zlec);
	}
}
