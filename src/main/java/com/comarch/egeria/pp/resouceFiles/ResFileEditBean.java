package com.comarch.egeria.pp.resouceFiles;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.resouceFiles.model.PptResourceFilesRpl;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.InputStream;

import static com.comarch.egeria.Utils.nz;


@ManagedBean
@Named
@Scope("view")
public class ResFileEditBean {
    private static final Logger log = LogManager.getLogger();//public bo wiekszosc beanow dziedziczy po tym

    PptResourceFilesRpl resfile;

    @PostConstruct
    public void init(){
        final String srscfid = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("rscfid");
        if (srscfid!=null && !"0".equals(srscfid)){
            resfile = (PptResourceFilesRpl) HibernateContext.get(PptResourceFilesRpl.class, Long.parseLong(srscfid));
        }else{
            resfile = new PptResourceFilesRpl();
            String sfnm = nz(FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("f"));
            if (!sfnm.isEmpty()){
                resfile.setRscfPlikSciezka(sfnm);
                try {

                    InputStream is = FacesContext.getCurrentInstance().getExternalContext().getResourceAsStream(sfnm);
                    resfile.setRscfPlikDane(Utils.getBytes(is));

////                    InputStream is = AppBean.app.getAppContext().getResource(sfnm).getInputStream();
////                    if (AppBean.app.getAppContext().getResource(sfnm).isReadable()) {
////                        resfile.setRscfPlikDane(Utils.getBytes(is));
////                    }
////
//////                    String plikPth = plikPth = AppBean.app.getApplicationContextFolder() + sfnm;
//////                    final File file = new File(plikPth);
//////                    if (file.exists()) {
//////                        resfile.setRscfPlikDane(Utils.getBytes(new FileInputStream(file)));
//////                    }
////
                } catch (Exception ex) {
                    log.error(ex.getMessage(), ex);
                }
            }
        }
    }


    public PptResourceFilesRpl getResfile() {
        return resfile;
    }

    public void setResfile(PptResourceFilesRpl resfile) {
        this.resfile = resfile;
    }






}
