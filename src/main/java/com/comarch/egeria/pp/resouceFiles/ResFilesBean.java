package com.comarch.egeria.pp.resouceFiles;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.resouceFiles.model.PptResourceFilesRpl;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.file.UploadedFile;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.*;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import static com.comarch.egeria.Utils.*;

@ManagedBean
@Named
@Scope("view")
public class ResFilesBean extends SqlBean {


    public String rsfcViewId = null;
    public String uploadingFile = null;
    PptResourceFilesRpl rscfFile = null;


    @PostConstruct
    public void init(){
        rsfcViewId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("v");
    }


    public void test(){
        System.out.println(this);
    }


    public void preparePublishResFile(String plik){
        this.uploadingFile = plik;
        final List lst = HibernateContext.query("from PptResourceFilesRpl where RSCF_PLIK_SCIEZKA = ?0", plik);
        if (lst.isEmpty()){
            this.rscfFile = new PptResourceFilesRpl();
            this.rscfFile.setRscfPlikSciezka(plik);
        }else {
            this.rscfFile = (PptResourceFilesRpl) lst.get(0);
        }

    }
    public void uploadFile(FileUploadEvent event) {
        try {

            UploadedFile uploadedFile = event.getFile();
            UploadedFile ufile = event.getFile();

            byte[] bytes = new byte[(int) ufile.getSize()];
            ufile.getInputStream().read(bytes);

            this.rscfFile.setRscfPlikDane(bytes);
            HibernateContext.saveOrUpdate(this.rscfFile);
            this.getViewFiles().put(this.uploadingFile, this.uploadingFile+".rplc");

            User.info("Wczytywano plik \r\n"+ ufile.getFileName()+ "\r\n" +
                    "\r\n(" + ufile.getSize()+"b)");

            this.getLW("PPL_RESOURCE_FILES_RPL").resetTs();

        } catch (Exception ex){
            User.alert(ex);
        }

    }


    public Object rscfId(String plik){
        DataRow r1 = getLW("PPL_RESOURCE_FILES_RPL").getAllData().stream().filter(r -> eq(r.get("rscf_plik_sciezka"), plik)).findFirst().orElse(null);
        if (r1!=null)
            return r1.get("rscf_id");
        else
            return 0;
    }



    public StreamedContent getResFile(String plik) throws FileNotFoundException {
        String plikPth = null;
        if (nz(this.getViewFiles().get(plik)).isEmpty()){
            plikPth = AppBean.app.getApplicationContextFolder() + plik;
        } else {
            plikPth = AppBean.app.getApplicationContextFolder() + plik + ".rplc";
        }

        final File file = new File((plikPth));
        final FileInputStream is = new FileInputStream(file);
        return new DefaultStreamedContent(is, "text/plain", file.getName().replace(".rplc",""));
    }


    public void delteResFile(String plik) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        JdbcUtils.sqlExecNoQuery("delete from PPADM.PPT_RESOURCE_FILES_RPL where RSCF_PLIK_SCIEZKA = ?", plik);
        this.getViewFiles().put(plik,"");
        User.info("Usunięto wdrożeniową wersję pliku \r\n"+ plik);
        getLW("PPL_RESOURCE_FILES_RPL").resetTs();
    }


    public void delete(DataRow r) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        if (r==null) return;
        JdbcUtils.sqlExecNoQuery("delete from PPADM.PPT_RESOURCE_FILES_RPL where rscf_id = ?", r.getIdAsLong());
        User.info("Usunięto wdrożeniową wersję pliku \r\n"+ r.get("rscf_plik_sciezka"));
        getLW("PPL_RESOURCE_FILES_RPL").resetTs();
    }

    public void deleteZaznaczone() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        SqlDataSelectionsHandler lw = getLW("PPL_RESOURCE_FILES_RPL");
        if (lw==null || lw.getSelectedRows().isEmpty()){
            return;
        }

        for (DataRow r : lw.getSelectedRows()) {
            delete(r);
        }
    }

    public String getRsfcViewId() {
        return rsfcViewId;
    }

    public void setRsfcViewId(String rsfcViewId) {
        this.rsfcViewId = rsfcViewId;
    }


    public ConcurrentHashMap<String, String> getViewFiles() {
        if (this.rsfcViewId==null)
            return new ConcurrentHashMap<>();

        return AppBean.app.getViewResourceFilesCHM(this.rsfcViewId);
    }

    public List<String> getViewFilesNames() {
        return this.getViewFiles().keySet().stream().sorted().collect(Collectors.toList());
    }

    public String getRplcFile(String fileName){
        return this.getViewFiles().get(fileName);
    }



    public StreamedContent dmpAllResFilesSource() throws IOException {
        SqlDataSelectionsHandler lw = getLW("PPL_RESOURCE_FILES_RPL");
        if (lw==null || lw.getSelectedRows().isEmpty()){
            return null;
        }

        if (lw.getSelectedRows().size()==1) {
            DataRow r = lw.getSelectedRows().get(0);
            Long rscfId = r.getAsLong("rscf_id");
            PptResourceFilesRpl resfile = (PptResourceFilesRpl) HibernateContext.get(PptResourceFilesRpl.class, rscfId);
            return  new DefaultStreamedContent(resfile.getPlikDaneStream(), "text/plain", resfile.getPlikNazwa());
        }

        if (lw.getSelectedRows().size()>1) {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(baos);

            for (DataRow r : lw.getSelectedRows()) {
                Long rscfId = r.getAsLong("rscf_id");
                PptResourceFilesRpl resfile = (PptResourceFilesRpl) HibernateContext.get(PptResourceFilesRpl.class, rscfId);

                if (resfile.getRscfPlikDane()!=null) {
                    Utils.addToZipFile(resfile.getRscfPlikDane(), resfile.getPlikNazwa(), zos);
                }else{
                    Utils.addToZipFile("".getBytes(), resfile.getPlikNazwa(), zos);
                }
            }

            zos.close();
            ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray() );

            String now = Utils.format(new Date(), "yyyy-MM-dd.HH.mm.ss");
            String fname = "res_files("+lw.getSelectedRows().size()+")." + now + ".zip";
            return new DefaultStreamedContent(bis , "text/plain", fname);

        }

        return null;
    }



}
