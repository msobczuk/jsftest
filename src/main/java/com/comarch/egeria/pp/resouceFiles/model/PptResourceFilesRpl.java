package com.comarch.egeria.pp.resouceFiles.model;

import com.comarch.egeria.pp.data.model.ModelBase;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.*;
import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Date;


/**
 * The persistent class for the PPT_RESOURCE_FILES_RPL database table.
 * 
 */
@Entity
@Table(name="PPT_RESOURCE_FILES_RPL", schema="PPADM")
@NamedQuery(name="PptResourceFilesRpl.findAll", query="SELECT p FROM PptResourceFilesRpl p")
//@DiscriminatorFormula("'.....'")
public class PptResourceFilesRpl extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_RESOURCE_FILES_RPL",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_RESOURCE_FILES_RPL", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_RESOURCE_FILES_RPL")
	@Column(name="RSCF_ID")
	Long rscfId = 0L;

	@Temporal(TemporalType.DATE)
	@Column(name="RSCF_AUDYT_DM")
	Date rscfAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="RSCF_AUDYT_DT")
	Date rscfAudytDt;

	@Column(name="RSCF_AUDYT_LM")
	String rscfAudytLm;

	@Column(name="RSCF_AUDYT_UM")
	String rscfAudytUm;

	@Column(name="RSCF_AUDYT_UT")
	String rscfAudytUt;


	
	@Column(name="RSCF_PLIK_SCIEZKA")
	String rscfPlikSciezka;

	@Lob
	@Column(name="RSCF_PLIK_DANE", columnDefinition="BLOB")
	byte[] rscfPlikDane;

	@Column(name="RSCF_PLIK_TYP")
	String rscfPlikTyp;



	public PptResourceFilesRpl() {
	}


	public Long getRscfId() {
		this.onRXGet("rscfId");
		return rscfId;
	}

	public void setRscfId(Long rscfId) {
		this.onRXSet("rscfId", this.rscfId, rscfId);
		this.rscfId = rscfId;
	}

	public Date getRscfAudytDm() {
		this.onRXGet("rscfAudytDm");
		return rscfAudytDm;
	}

	public void setRscfAudytDm(Date rscfAudytDm) {
		this.onRXSet("rscfAudytDm", this.rscfAudytDm, rscfAudytDm);
		this.rscfAudytDm = rscfAudytDm;
	}

	public Date getRscfAudytDt() {
		this.onRXGet("rscfAudytDt");
		return rscfAudytDt;
	}

	public void setRscfAudytDt(Date rscfAudytDt) {
		this.onRXSet("rscfAudytDt", this.rscfAudytDt, rscfAudytDt);
		this.rscfAudytDt = rscfAudytDt;
	}

	public String getRscfAudytLm() {
		this.onRXGet("rscfAudytLm");
		return rscfAudytLm;
	}

	public void setRscfAudytLm(String rscfAudytLm) {
		this.onRXSet("rscfAudytLm", this.rscfAudytLm, rscfAudytLm);
		this.rscfAudytLm = rscfAudytLm;
	}

	public String getRscfAudytUm() {
		this.onRXGet("rscfAudytUm");
		return rscfAudytUm;
	}

	public void setRscfAudytUm(String rscfAudytUm) {
		this.onRXSet("rscfAudytUm", this.rscfAudytUm, rscfAudytUm);
		this.rscfAudytUm = rscfAudytUm;
	}

	public String getRscfAudytUt() {
		this.onRXGet("rscfAudytUt");
		return rscfAudytUt;
	}

	public void setRscfAudytUt(String rscfAudytUt) {
		this.onRXSet("rscfAudytUt", this.rscfAudytUt, rscfAudytUt);
		this.rscfAudytUt = rscfAudytUt;
	}

	public String getRscfPlikSciezka() {
		this.onRXGet("rscfPlikSciezka");
		return rscfPlikSciezka;
	}

	public void setRscfPlikSciezka(String rscfPlikSciezka) {
		this.onRXSet("rscfPlikSciezka", this.rscfPlikSciezka, rscfPlikSciezka);
		this.rscfPlikSciezka = rscfPlikSciezka;
	}

	public byte[] getRscfPlikDane() {
		this.onRXGet("rscfPlikDane");
		return rscfPlikDane;
	}

	public void setRscfPlikDane(byte[] rscfPlikDane) {
		this.onRXSet("rscfPlikDane", this.rscfPlikDane, rscfPlikDane);
		this.rscfPlikDane = rscfPlikDane;
	}

	public String getDaneAsString() throws UnsupportedEncodingException {
		if (getRscfPlikDane()==null)
			return "";
		else
        	return new String( rscfPlikDane, StandardCharsets.UTF_8);
    }

    public void setDaneAsString(String daneAsString) throws UnsupportedEncodingException {
	    if (daneAsString==null)
            this.setRscfPlikDane(null);
	    else
            this.setRscfPlikDane(daneAsString.getBytes(StandardCharsets.UTF_8));
    }


	public String getRscfPlikTyp() {
		this.onRXGet("rscfPlikTyp");
		return rscfPlikTyp;
	}

	public void setRscfPlikTyp(String rscfPlikTyp) {
		this.onRXSet("rscfPlikTyp", this.rscfPlikTyp, rscfPlikTyp);
		this.rscfPlikTyp = rscfPlikTyp;
	}



	public String getPlikNazwa() {
		String[] arr = ("/"+this.getRscfPlikSciezka()).split("/");
		return arr[arr.length-1];
	}

	public ByteArrayInputStream getPlikDaneStream() {
		ByteArrayInputStream is;
		if ( this.getRscfPlikDane() != null){
			is = new ByteArrayInputStream(  this.getRscfPlikDane() );
		} else {
			is = new ByteArrayInputStream(  "".getBytes() );
		}
		return is;
	}


}
