package com.comarch.egeria.pp.stanMagazynowy;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
//import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.TabChangeEvent;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean
@Named
@Scope("session")
public class StanMagazynowyBean extends SqlBean implements Serializable {
	

	private String p_tow_id = "0";
	private String p_tow_id_bibl = "0";
	private String p_mag_ob_id = null;
	private String p_mag_ob_id_bibl = null;
	private String p_kls_id = "-1";
	private String p_kls_id_bibl = "-1";
	private String p_kk_id = "-1";
	private String p_kk_id_bibl = "-1";
	private String p_kls;
	private String p_kls_bibl;
	private String p_prt_kod = null;
	private String p_kls_id_ksiazki;
	private String p_kk_id_ksiazki;
	private String p_kls_id_czasopisma; 
	private String p_kk_id_czasopisma; 
	
	private Integer activeTabIndex =0;
	
	private static final Logger log = LogManager.getLogger();
	
	
	@PostConstruct
	public void init() {
		try {
			this.p_mag_ob_id_bibl = (String) this.execScalarQuery("select css_pck_public.get_default ('BIBLIOTEKA_MAGAZYN' , gmp_app_css.get_app_id )\r\n" + 
					"  from dual\r\n");
			this.p_kls_id_ksiazki =  (this.execScalarQuery("select kk_kl_id\r\n" + 
					"					  from css_kody_klasyfikacji\r\n" + 
					"					  where kk_id = css_pck_public.get_default ('BIBLIOTEKA_GRUPA_KSIAZKI', gmp_app_css.get_app_id )")).toString();
			
			this.p_kk_id_ksiazki =  (this.execScalarQuery("select css_pck_public.get_default ('BIBLIOTEKA_GRUPA_KSIAZKI', gmp_app_css.get_app_id )\r\n" + 
					"  from dual\r\n")).toString();
			
			this.p_kls_id_czasopisma = (this.execScalarQuery("select kk_kl_id\r\n" + 
					"  from css_kody_klasyfikacji\r\n" + 
					" where kk_id = css_pck_public.get_default ('BIBLIOTEKA_GRUPA_CZASOPISMA', gmp_app_css.get_app_id )\r\n")).toString();
					
			this.p_kk_id_czasopisma = (this.execScalarQuery("select kk_kl_id\r\n" + 
					"  from css_kody_klasyfikacji\r\n" + 
					" where kk_id = css_pck_public.get_default ('BIBLIOTEKA_GRUPA_CZASOPISMA', gmp_app_css.get_app_id )\r\n")).toString();
			
			

		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	
	public void raport(){
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				if(this.p_mag_ob_id==null || "".equals(this.p_mag_ob_id) || "-1".equals(this.p_mag_ob_id)) 
				{
					params.put("p_mag_ob_id", "-1");
					params.put("p_tow_id", this.p_tow_id);
					params.put("p_kls_id", this.p_kls_id);
					params.put("p_kk_id", this.p_kk_id);
					params.put("stanNa", this.getDate());
					
					ReportGenerator.displayReportAsPDF(params, "StanMagazynowy");
				}
				else 
				{
					params.put("p_mag_ob_id", this.p_mag_ob_id);
					params.put("p_tow_id", this.p_tow_id);
					params.put("p_kls_id", this.p_kls_id);
					params.put("p_kk_id", this.p_kk_id);
					params.put("stanNa", this.getDate());
	
					
					ReportGenerator.displayReportAsPDF(params, "StanMagazynowyWybrany");
				}
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}		
	}
	
	public String getP_tow_id_bibl() {
		return p_tow_id_bibl;
	}

	public void setP_tow_id_bibl(String p_tow_id_bibl) {
		this.p_tow_id_bibl = p_tow_id_bibl;
	}

	public String getP_mag_ob_id_bibl() {
		return p_mag_ob_id_bibl;
	}

	public void setP_mag_ob_id_bibl(String p_mag_ob_id_bibl) {
		if ("null".equals(p_mag_ob_id_bibl))
			p_mag_ob_id_bibl = null;
		this.p_mag_ob_id_bibl = p_mag_ob_id_bibl;
	}

	public String getP_kls_id_bibl() {
		return p_kls_id_bibl;
	}

	public void setP_kls_id_bibl(String p_kls_id_bibl) {
		this.p_kls_id_bibl = p_kls_id_bibl;
	}

	public String getP_kk_id_bibl() {
		return p_kk_id_bibl;
	}

	public void setP_kk_id_bibl(String p_kk_id_bibl) {
		this.p_kk_id_bibl = p_kk_id_bibl;
	}

	public String getP_kls_bibl() {
		return p_kls_bibl;
	}

	public void setP_kls_bibl(String p_kls_bibl) {
		this.p_kls_bibl = p_kls_bibl;
	}

	public String getP_tow_id() {
		return p_tow_id;
	}

	public void setP_tow_id(String p_tow_id) {
		this.p_tow_id = p_tow_id;
	}

	public String getP_mag_ob_id() {
		return p_mag_ob_id;
	}

	public void setP_mag_ob_id(String p_mag_ob_id) {
		if ("null".equals(p_mag_ob_id))
				p_mag_ob_id = null;
		this.p_mag_ob_id = p_mag_ob_id;
	}

	public String getP_kls_id() {
		return p_kls_id;
	}

	public void setP_kls_id(String p_kls_id) {
		this.p_kls_id = p_kls_id;
	}

	public String getP_kk_id() {
		return p_kk_id;
	}

	public void setP_kk_id(String p_kk_id) {
		this.p_kk_id = p_kk_id;
	}




	public Integer getActiveTabIndex() {
		return activeTabIndex;
	}

	public void setActiveTabIndex(Integer activeTabIndex) {
		this.activeTabIndex = activeTabIndex;
	}

	public void tabChanged(TabChangeEvent event) {
		TabView tv = (TabView) event.getComponent();
//		this.activeTabIndex = tv.getActiveIndex();
		this.activeTabIndex = tv.getChildren().indexOf(event.getTab());
	}
	
	public void przejdzDoWyniku() {
		if(activeTabIndex == 0) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("StanMagazynowy");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(activeTabIndex == 1 ) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("StanMagazynowyBiblKsiazki");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(activeTabIndex == 2 ) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("StanMagazynowyBiblCzasopisma");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String getP_kls() {
		return p_kls;
	}

	public void setP_kls(String p_kls) {
		this.p_kls = p_kls;
	}

	public String getP_prt_kod() {
		return p_prt_kod;
	}

	public void setP_prt_kod(String p_prt_kod) {
		this.p_prt_kod = p_prt_kod;
	}

	public String getP_kls_id_ksiazki() {
		return p_kls_id_ksiazki;
	}

	public void setP_kls_id_ksiazki(String p_kls_id_ksiazki) {
		this.p_kls_id_ksiazki = p_kls_id_ksiazki;
	}

	public String getP_kk_id_ksiazki() {
		return p_kk_id_ksiazki;
	}

	public void setP_kk_id_ksiazki(String p_kk_id_ksiazki) {
		this.p_kk_id_ksiazki = p_kk_id_ksiazki;
	}

	public String getDate() {
		Date dNow = new Date();
		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String formatted = format1.format(dNow);
		return formatted;
	}
	
	public void resetujFiltr() {
		p_tow_id = "0";
		p_mag_ob_id = null;
		p_kls_id = "-1";
		p_kk_id = "-1";
//		p_kls;
		p_prt_kod = null;
//		p_kls_id_ksiazki;
//		p_kk_id_ksiazki;
		
		
		
		
		
		
		p_tow_id_bibl = "0";
//		p_mag_ob_id_bibl = null;
		p_kls_id_bibl = "-1";
		p_kk_id_bibl = "-1";
//		p_kls;
//		p_prt_kod = null;
//		p_kls_id_ksiazki;
//		p_kk_id_ksiazki;
	}
	
	public boolean isBtnDisabled() {
		if(activeTabIndex == 0) {
			if(p_mag_ob_id != null && Strings.isNotEmpty(p_mag_ob_id)) 
				return false;
		}
		else if(activeTabIndex == 1 || activeTabIndex == 2) {
			if(p_mag_ob_id_bibl != null && Strings.isNotEmpty(p_mag_ob_id_bibl)) 
				return false;
		}
		
		
		return true;
	}

	public String getP_kls_id_czasopisma() {
		return p_kls_id_czasopisma;
	}

	public void setP_kls_id_czasopisma(String p_kls_id_czasopisma) {
		this.p_kls_id_czasopisma = p_kls_id_czasopisma;
	}

	public String getP_kk_id_czasopisma() {
		return p_kk_id_czasopisma;
	}

	public void setP_kk_id_czasopisma(String p_kk_id_czasopisma) {
		this.p_kk_id_czasopisma = p_kk_id_czasopisma;
	}
}
