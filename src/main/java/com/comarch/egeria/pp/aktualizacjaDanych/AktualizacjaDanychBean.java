package com.comarch.egeria.pp.aktualizacjaDanych;

import com.comarch.egeria.pp.aktualizacjaDanych.model.PptWnioskiAktualizPozycje;
import com.comarch.egeria.pp.aktualizacjaDanych.model.WniosekAktualizacjiDanych;
import com.comarch.egeria.pp.aktualizacjaDanych.model.WniosekAktualizacjiPozycja;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.comarch.egeria.Utils.asDate;
import static com.comarch.egeria.Utils.asDouble;

@ManagedBean // JBossTools
@Named
@Scope("view")
public class AktualizacjaDanychBean extends SqlBean implements Serializable {
	
//	@Inject
//	ObiegBean obieg;
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	
	private WniosekAktualizacjiDanych wniosek = new WniosekAktualizacjiDanych();

	@PostConstruct
	public void init() {
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		if (id == null) {
			
			WniosekAktualizacjiDanych w = new WniosekAktualizacjiDanych();
			w.setWadPrcId( AppBean.getUser().getPrcIdLong() );
			this.setWniosek(w);
			
		} else {
			
			this.setWniosek((WniosekAktualizacjiDanych) HibernateContext.get(WniosekAktualizacjiDanych.class, (Serializable) id));
			
		}
		
	}
	

 	
	public void zapisz() {

		if (!validateBeforeSaveAktualizacjeDanych())
			return;

		try {
			HibernateContext.saveOrUpdate(this.getWniosek());
			User.info("Udany zapis wniosku o aktualizacje danych.");
//			obieg.setIdEncji(this.getWniosek().getWadId());
		} catch (Exception e) {
			User.alert(e); //"Błąd w zapisie wniosku o aktualizacje danych!");
			log.error(e.getMessage(), e);
		}

	}

	public void usun() {

		try {
			HibernateContext.delete(getWniosek());
			FacesContext.getCurrentInstance().getExternalContext().redirect("WnioskiAktualizacje");
		} catch (Exception e) {
			User.alert("Błąd w usuwaniu wniosku!");
			log.error(e.getMessage(), e);
		}

	}




	private boolean validateBeforeSaveAktualizacjeDanych() {
		
		ArrayList<String> inval = new ArrayList<String>();

		if (!"T".equals(wniosek.getWadFZgodaRegulamin())){



			DataRow r = getSlownik("WN_AKT_DANYCH_KONFIGURACJA").find("URL_REGULAMIN");
			if (r!=null && r.get("rv_meaning")!=null){
				inval.add("Wymagana jest zgoda w polu '"+ r.get("rv_abbreviation") +"'" );
			}
		}
		
		if (wniosek.getWadRodzaj() == null || "".equals(wniosek.getWadRodzaj())) {
			inval.add("Pola rodzaj aktualizowanych danych nie może być puste!");
		}
		
		if(wniosek.getPptWnioskiAktualizPozycjes().isEmpty() && !"INNE".equals( wniosek.getWadRodzaj())) {
			inval.add("Musi być dodana co najmniej jedna pozycja aktualizacji!");
		}
		
		
		
		if(eq("INNE", wniosek.getWadRodzaj()) &&  eq("",wniosek.getWadOpis()) )  {
			inval.add("Brak opisu / uzasadnienia.");
		}
		
		
		for (PptWnioskiAktualizPozycje pozycjaWniosku : wniosek.getPptWnioskiAktualizPozycjes()) {
			//				if ( (pozycjaWniosku.getWadpWartosc() == null || "".equals( pozycjaWniosku.getWadpWartosc()) ) ) 
			//					inval.add("Należy wpisać nową wartość wybranego pola!"); //TODO - Nie zawsz -  tylko jesli pole jest wymagane w bazie...
		}
		

		DataRow rRodzajWniosku = this.getSL("WN_AKT_DANYCH").find( wniosek.getWadRodzaj()  );
		if (rRodzajWniosku!=null 
				&& rRodzajWniosku.get("wsl_wartosc_max")!=null //wymagane dodatkowe pole wadInfId
				&& (this.wniosek.getWadInfId()==null || "".equals( this.wniosek.getWadInfId() )) 
				) {
			
			inval.add("Pole \"" + rRodzajWniosku.get("wsl_opis3") + "\" jest wymagane."); //jesli rodzaj ma podany LW w wsl_wartosc_max to wymagamy wad_inf_id
		}
		
		for (String msg : inval) {
			User.alert(msg);
		}

		return inval.isEmpty();
	}



	public void dodajPozycje() {
		WniosekAktualizacjiDanych w = this.getWniosek();
		SqlDataSelectionsHandler lw = this.getLW("PPL_AKT_DANYCH_WSL", w.getWadPrcId(), w.getWadInfId(), w.getWadRodzaj());
		if (lw==null) return;

		List<DataRow> selected = lw.getSelectedRows();

		for (DataRow dr : selected) {
				
				WniosekAktualizacjiPozycja  p = new WniosekAktualizacjiPozycja();
				p.setWadpRodzaj( ""+dr.get("wsl_wartosc"));
				
				String strAktWartosc = (String) dr.get("aktualna_wartosc");
				
				if ("DATE".equals(dr.get("wsl_typ"))) {
					
					p.setWadpWartoscDate(asDate( strAktWartosc, "yyyy-MM-dd HH:mm:ss" ));
					
				} else if ("NUMBER".equals(dr.get("wsl_typ"))) {

					p.setWadpWartoscNumber(asDouble( strAktWartosc) );
					
				} else {
					
					p.setWadpWartosc(strAktWartosc);
					
				}
					 

				w.addPptWnioskiAktualizPozycje(p );
//				lw.hiddenRowsPK.add( p.getWadpRodzaj() ); nie potrzeba jesli kolekcja hideRowsPK na LW jest reaktywna
//				System.out.println(""+dr.get("wsl_wartosc"));
				
		}
		
		lw.clearSelection();
	}



	public WniosekAktualizacjiDanych getWniosek() {
		return wniosek;
	}

	public void setWniosek(WniosekAktualizacjiDanych wniosek) {
		this.wniosek = wniosek;
	}

}



//public boolean czyDisableWnkRodzaj() {
//boolean ret = false; 
//
//List<PptWnioskiAktualizPozycje> lista = this.wniosek.getPptWnioskiAktualizPozycjes().stream()
//.filter(p-> p.getWadpRodzaj()!= null)
//.filter( p -> !(p.getWadpRodzaj().isEmpty())).collect(Collectors.toList());
//if(lista != null)
//	ret = lista.size() > 0 ? true:false;
//
//return ret;
//}







//private boolean sprawdzTypy() {
//	SqlDataSelectionsHandler sqlDH = this.getLW("PPL_WN_AKTU_TYPY");
//	if(sqlDH == null)
//		return false;
//	
//	String typ = "";
//	String wartosc = "";
//	
//	for(PptWnioskiAktualizPozycje pozycja : wniosek.getPptWnioskiAktualizPozycjes()) {
//		if(pozycja.getPptWnioskiAktualizPozycjes() == null || pozycja.getPptWnioskiAktualizPozycjes().isEmpty()) {
//			
//			wartosc = pozycja.getWadpWartosc();
//			if(pozycja.getWadpRodzaj()==null || "".equals(pozycja.getWadpRodzaj()) ) return false;
//			typ = ""+sqlDH.get(pozycja.getWadpRodzaj()).get("typ");
//			
//			if(!sprawdzTypy(typ, wartosc)) return false;
//		}else {
//			
//			for(PptWnioskiAktualizPozycje pozycjaPozycji : pozycja.getPptWnioskiAktualizPozycjes()) {
//				
//				wartosc = pozycjaPozycji.getWadpWartosc();
//				if(pozycjaPozycji.getWadpRodzaj()==null || "".equals(pozycjaPozycji.getWadpRodzaj()) ) return false;
//				typ = ""+sqlDH.get(pozycjaPozycji.getWadpRodzaj()).get("typ");
//				
//				if(!sprawdzTypy(typ, wartosc)) return false;
//			}
//			
//		}
//	}
//	
//	
//	return true;
//}

//private boolean sprawdzTypy(String typ, String wartosc) {
//	
//	switch (typ) {
//    case "VARCHAR2": 
//    	return true;
//    case "NUMBER":
//    	return wartosc.chars().allMatch( Character::isDigit );
//	//case "DATE":
//	//break;*TODO
//}
//	
//	
//	return true;
//}

//public boolean czySL(String wadpRodzaj) {
//
//if(wadpRodzaj.isEmpty())
//	return false;
//
//List<DataRow> collect = getSL("PPL_SLOWNIK").getData()
//	.stream()
//	.filter(s -> wadpRodzaj.equals(""+s.get("wsl_wartosc_zewn")))
//	.collect(Collectors.toList());
//
//if(!collect.isEmpty()) {
//	return "SL".equals(collect.get(0).get("wsl_typ").toString());
//}
//	
//return false;
//}