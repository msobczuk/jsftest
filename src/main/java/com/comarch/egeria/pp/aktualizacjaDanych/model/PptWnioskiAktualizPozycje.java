package com.comarch.egeria.pp.aktualizacjaDanych.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.comarch.egeria.pp.data.model.ModelBase;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_WNIOSKI_AKTUALIZ_POZYCJE database table.
 * 
 */
@Entity
@Table(name="PPT_WNIOSKI_AKTUALIZ_POZYCJE", schema="PPADM")
@NamedQuery(name="PptWnioskiAktualizPozycje.findAll", query="SELECT p FROM PptWnioskiAktualizPozycje p")
@DiscriminatorFormula("'WniosekAktualizacjiPozycja'")
public class PptWnioskiAktualizPozycje extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_WNIOSKI_AKTUALIZ_POZYCJE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_WNIOSKI_AKTUALIZ_POZYCJE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_WNIOSKI_AKTUALIZ_POZYCJE")
	@Column(name="WADP_ID")
	private long wadpId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="WADP_AUDYT_DM")
	private Date wadpAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="WADP_AUDYT_DT")
	private Date wadpAudytDt;

	@Column(name="WADP_AUDYT_LM")
	private String wadpAudytLm;

	@Column(name="WADP_AUDYT_UM")
	private String wadpAudytUm;

	@Column(name="WADP_AUDYT_UT")
	private String wadpAudytUt;

	@Column(name="WADP_OPIS")
	private String wadpOpis;

	@Column(name="WADP_RODZAJ")
	private String wadpRodzaj;

	@Column(name="WADP_WARTOSC")
	private String wadpWartosc;
	
	@Column(name="WADP_WARTOSC_NUMBER")
	private Double wadpWartoscNumber;
	
	@Column(name="WADP_WARTOSC_DATE")
	private Date wadpWartoscDate;

	//bi-directional many-to-one association to PptWnioskiAktualizDanych
	@ManyToOne
	@JoinColumn(name="WADP_WAD_ID")
	private PptWnioskiAktualizDanych pptWnioskiAktualizDanych;

	//bi-directional many-to-one association to PptWnioskiAktualizPozycje
	@ManyToOne
	@JoinColumn(name="WADP_WADP_ID")
	private PptWnioskiAktualizPozycje pptWnioskiAktualizPozycje;

	//bi-directional many-to-one association to PptWnioskiAktualizPozycje
	@OneToMany(mappedBy="pptWnioskiAktualizPozycje", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 20)
//	@OrderBy("WADP_ID ASC")
	private List<PptWnioskiAktualizPozycje> pptWnioskiAktualizPozycjes = new ArrayList<>();

	public PptWnioskiAktualizPozycje() {
	}

	public long getWadpId() {
		this.onRXGet("wadpId");
		return this.wadpId;
	}

	public void setWadpId(long wadpId) {
		this.onRXSet("wadpId", this.wadpId, wadpId);
		this.wadpId = wadpId;
	}

	public Date getWadpAudytDm() {
		this.onRXGet("wadpAudytDm");
		return this.wadpAudytDm;
	}

	public void setWadpAudytDm(Date wadpAudytDm) {
		this.onRXSet("wadpAudytDm", this.wadpAudytDm, wadpAudytDm);
		this.wadpAudytDm = wadpAudytDm;
	}

	public Date getWadpAudytDt() {
		this.onRXGet("wadpAudytDt");
		return this.wadpAudytDt;
	}

	public void setWadpAudytDt(Date wadpAudytDt) {
		this.onRXSet("wadpAudytDt", this.wadpAudytDt, wadpAudytDt);
		this.wadpAudytDt = wadpAudytDt;
	}

	public String getWadpAudytLm() {
		this.onRXGet("wadpAudytLm");
		return this.wadpAudytLm;
	}

	public void setWadpAudytLm(String wadpAudytLm) {
		this.onRXSet("wadpAudytLm", this.wadpAudytLm, wadpAudytLm);
		this.wadpAudytLm = wadpAudytLm;
	}

	public String getWadpAudytUm() {
		this.onRXGet("wadpAudytUm");
		return this.wadpAudytUm;
	}

	public void setWadpAudytUm(String wadpAudytUm) {
		this.onRXSet("wadpAudytUm", this.wadpAudytUm, wadpAudytUm);
		this.wadpAudytUm = wadpAudytUm;
	}

	public String getWadpAudytUt() {
		this.onRXGet("wadpAudytUt");
		return this.wadpAudytUt;
	}

	public void setWadpAudytUt(String wadpAudytUt) {
		this.onRXSet("wadpAudytUt", this.wadpAudytUt, wadpAudytUt);
		this.wadpAudytUt = wadpAudytUt;
	}

	public String getWadpOpis() {
		this.onRXGet("wadpOpis");
		return this.wadpOpis;
	}

	public void setWadpOpis(String wadpOpis) {
		this.onRXSet("wadpOpis", this.wadpOpis, wadpOpis);
		this.wadpOpis = wadpOpis;
	}

	public String getWadpRodzaj() {
		this.onRXGet("wadpRodzaj");
		return this.wadpRodzaj;
	}

	public void setWadpRodzaj(String wadpRodzaj) {
		this.onRXSet("wadpRodzaj", this.wadpRodzaj, wadpRodzaj); 
		this.wadpRodzaj = wadpRodzaj;
		this.setWadpWartosc("");
	}

	public String getWadpWartosc() {
		this.onRXGet("wadpWartosc");
		return this.wadpWartosc;
	}

	public void setWadpWartosc(String wadpWartosc) {
		this.onRXSet("wadpWartosc", this.wadpWartosc, wadpWartosc);
		this.wadpWartosc = wadpWartosc;
	}

	public PptWnioskiAktualizDanych getPptWnioskiAktualizDanych() {
		this.onRXGet("pptWnioskiAktualizDanych");
		return this.pptWnioskiAktualizDanych;
	}

	public void setPptWnioskiAktualizDanych(PptWnioskiAktualizDanych pptWnioskiAktualizDanych) {
		this.onRXSet("pptWnioskiAktualizDanych", this.pptWnioskiAktualizDanych, pptWnioskiAktualizDanych);
		this.pptWnioskiAktualizDanych = pptWnioskiAktualizDanych;
	}

	public PptWnioskiAktualizPozycje getPptWnioskiAktualizPozycje() {
		this.onRXGet("pptWnioskiAktualizPozycje");
		return this.pptWnioskiAktualizPozycje;
	}

	public void setPptWnioskiAktualizPozycje(PptWnioskiAktualizPozycje pptWnioskiAktualizPozycje) {
		this.onRXSet("pptWnioskiAktualizPozycje", this.pptWnioskiAktualizPozycje, pptWnioskiAktualizPozycje);
		this.pptWnioskiAktualizPozycje = pptWnioskiAktualizPozycje;
	}

	public List<PptWnioskiAktualizPozycje> getPptWnioskiAktualizPozycjes() {
		this.onRXGet("pptWnioskiAktualizPozycjes");
		return this.pptWnioskiAktualizPozycjes;
	}

	public void setPptWnioskiAktualizPozycjes(List<PptWnioskiAktualizPozycje> pptWnioskiAktualizPozycjes) {
		this.onRXSet("pptWnioskiAktualizPozycjes", this.pptWnioskiAktualizPozycjes, pptWnioskiAktualizPozycjes);
		this.pptWnioskiAktualizPozycjes = pptWnioskiAktualizPozycjes;
	}

	public PptWnioskiAktualizPozycje addPptWnioskiAktualizPozycje(PptWnioskiAktualizPozycje pptWnioskiAktualizPozycje) {
		getPptWnioskiAktualizPozycjes().add(pptWnioskiAktualizPozycje);
		pptWnioskiAktualizPozycje.setPptWnioskiAktualizPozycje(this);

		return pptWnioskiAktualizPozycje;
	}
	
	public void addNewPptWnioskiAktualizPozycje() {
		PptWnioskiAktualizPozycje poz = new PptWnioskiAktualizPozycje();
		this.addPptWnioskiAktualizPozycje(poz);
	}

	public PptWnioskiAktualizPozycje removePptWnioskiAktualizPozycje(PptWnioskiAktualizPozycje pptWnioskiAktualizPozycje) {
		getPptWnioskiAktualizPozycjes().remove(pptWnioskiAktualizPozycje);
		pptWnioskiAktualizPozycje.setPptWnioskiAktualizPozycje(null);

		return pptWnioskiAktualizPozycje;
	}

	public Double getWadpWartoscNumber() {
		return wadpWartoscNumber;
	}

	public void setWadpWartoscNumber(Double wadpWartoscNumber) {
		this.wadpWartoscNumber = wadpWartoscNumber;
	}

	public Date getWadpWartoscDate() {
		return wadpWartoscDate;
	}

	public void setWadpWartoscDate(Date wadpWartoscDate) {
		this.wadpWartoscDate = wadpWartoscDate;
	}

}

