package com.comarch.egeria.pp.aktualizacjaDanych.model;

import com.comarch.egeria.pp.data.model.ModelBase;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_WNIOSKI_AKTUALIZ_DANYCH database table.
 * 
 */
@Entity
@Table(name="PPT_WNIOSKI_AKTUALIZ_DANYCH", schema="PPADM")
@NamedQuery(name="PptWnioskiAktualizDanych.findAll", query="SELECT p FROM PptWnioskiAktualizDanych p")
@DiscriminatorFormula("'WniosekAktualizacjiDanych'")
public class PptWnioskiAktualizDanych extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_WNIOSKI_AKTUALIZ_DANYCH",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_WNIOSKI_AKTUALIZ_DANYCH", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_WNIOSKI_AKTUALIZ_DANYCH")
	@Column(name="WAD_ID")
	private long wadId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="WAD_AUDYT_DM")
	private Date wadAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="WAD_AUDYT_DT")
	private Date wadAudytDt;

	@Version
	@Column(name="WAD_AUDYT_LM")
	private Long wadAudytLm;

	public Long getWadAudytLm() {
		this.onRXGet("wadAudytLm");
		return this.wadAudytLm;
	}

	public void setWadAudytLm(Long wadAudytLm) {
		this.onRXSet("wadAudytLm", this.wadAudytLm, wadAudytLm);
		this.wadAudytLm = wadAudytLm;
	}


	@Column(name="WAD_AUDYT_UM")
	private String wadAudytUm;

	@Column(name="WAD_AUDYT_UT")
	private String wadAudytUt;

	@Column(name="WAD_OPIS")
	private String wadOpis;

	@Column(name="WAD_PRC_ID")
	private Long wadPrcId;

	@Column(name="WAD_RODZAJ")
	String wadRodzaj;
	
	@Column(name="WAD_INF_ID")
	String wadInfId;

	@Column(name="WAD_F_ZGODA_REGULAMIN")
	String wadFZgodaRegulamin = "N";

	//bi-directional many-to-one association to PptWnioskiAktualizPozycje
	@OneToMany(mappedBy="pptWnioskiAktualizDanych",cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 20)
//	@OrderBy("WADP_ID ASC")
	private List<PptWnioskiAktualizPozycje> pptWnioskiAktualizPozycjes = new ArrayList<>();
	

	public PptWnioskiAktualizDanych() {
	}

	public long getWadId() {
		this.onRXGet("wadId");
		return this.wadId;
	}

	public void setWadId(long wadId) {
		this.onRXSet("wadId", this.wadId, wadId);
		this.wadId = wadId;
	}

	public Date getWadAudytDm() {
		this.onRXGet("wadAudytDm");
		return this.wadAudytDm;
	}

	public void setWadAudytDm(Date wadAudytDm) {
		this.onRXSet("wadAudytDm", this.wadAudytDm, wadAudytDm);
		this.wadAudytDm = wadAudytDm;
	}

	public Date getWadAudytDt() {
		this.onRXGet("wadAudytDt");
		return this.wadAudytDt;
	}

	public void setWadAudytDt(Date wadAudytDt) {
		this.onRXSet("wadAudytDt", this.wadAudytDt, wadAudytDt);
		this.wadAudytDt = wadAudytDt;
	}


	public String getWadAudytUm() {
		this.onRXGet("wadAudytUm");
		return this.wadAudytUm;
	}

	public void setWadAudytUm(String wadAudytUm) {
		this.onRXSet("wadAudytUm", this.wadAudytUm, wadAudytUm);
		this.wadAudytUm = wadAudytUm;
	}

	public String getWadAudytUt() {
		this.onRXGet("wadAudytUt");
		return this.wadAudytUt;
	}

	public void setWadAudytUt(String wadAudytUt) {
		this.onRXSet("wadAudytUt", this.wadAudytUt, wadAudytUt);
		this.wadAudytUt = wadAudytUt;
	}

	public String getWadOpis() {
		this.onRXGet("wadOpis");
		return this.wadOpis;
	}

	public void setWadOpis(String wadOpis) {
		this.onRXSet("wadOpis", this.wadOpis, wadOpis);
		this.wadOpis = wadOpis;
	}

	public Long getWadPrcId() {
		this.onRXGet("wadPrcId");
		return this.wadPrcId;
	}

	public void setWadPrcId(Long wadPrcId) {
		this.onRXSet("wadPrcId", this.wadPrcId, wadPrcId);
		this.wadPrcId = wadPrcId;
	}

	public String getWadRodzaj() {
		this.onRXGet("wadRodzaj");
		return this.wadRodzaj;
	}

	public void setWadRodzaj(String wadRodzaj) {
		this.onRXSet("wadRodzaj", this.wadRodzaj, wadRodzaj);
		this.wadRodzaj = wadRodzaj;
	}

	public List<PptWnioskiAktualizPozycje> getPptWnioskiAktualizPozycjes() {
		this.onRXGet("pptWnioskiAktualizPozycjes");
		return this.pptWnioskiAktualizPozycjes;
	}

	public void setPptWnioskiAktualizPozycjes(List<PptWnioskiAktualizPozycje> pptWnioskiAktualizPozycjes) {
		this.onRXSet("pptWnioskiAktualizPozycjes", this.pptWnioskiAktualizPozycjes, pptWnioskiAktualizPozycjes);
		this.pptWnioskiAktualizPozycjes = pptWnioskiAktualizPozycjes;
	}

	public PptWnioskiAktualizPozycje addPptWnioskiAktualizPozycje(PptWnioskiAktualizPozycje pptWnioskiAktualizPozycje) {
		this.onRXSet("pptWnioskiAktualizPozycjes", null, this.pptWnioskiAktualizPozycjes);
		getPptWnioskiAktualizPozycjes().add(pptWnioskiAktualizPozycje);
		pptWnioskiAktualizPozycje.setPptWnioskiAktualizDanych(this);

		return pptWnioskiAktualizPozycje;
	}
	
	public void addNewPptWnioskiAktualizPozycje () {
		PptWnioskiAktualizPozycje poz = new PptWnioskiAktualizPozycje();
		this.addPptWnioskiAktualizPozycje(poz);
	}

	public PptWnioskiAktualizPozycje removePptWnioskiAktualizPozycje(PptWnioskiAktualizPozycje pptWnioskiAktualizPozycje) {
		this.onRXSet("pptWnioskiAktualizPozycjes", null, this.pptWnioskiAktualizPozycjes);
		getPptWnioskiAktualizPozycjes().remove(pptWnioskiAktualizPozycje);
		pptWnioskiAktualizPozycje.setPptWnioskiAktualizDanych(null);

		return pptWnioskiAktualizPozycje;
	}

	public String getWadInfId() {
		return wadInfId;
	}

	public void setWadInfId(String wadInfId) {
		this.wadInfId = wadInfId;
	}



	public String getWadFZgodaRegulamin() {
		return wadFZgodaRegulamin;
	}

	public void setWadFZgodaRegulamin(String wadFZgodaRegulamin) {
		this.wadFZgodaRegulamin = wadFZgodaRegulamin;
	}

}

