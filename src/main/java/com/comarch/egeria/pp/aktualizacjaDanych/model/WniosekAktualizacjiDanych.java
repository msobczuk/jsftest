package com.comarch.egeria.pp.aktualizacjaDanych.model;

import javax.persistence.Entity;

import static com.comarch.egeria.Utils.*;

import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

@Entity
public class WniosekAktualizacjiDanych extends PptWnioskiAktualizDanych{
	
	private static final long serialVersionUID = 1L;
	public static final Logger log = LogManager.getLogger();
	
	
	public WniosekAktualizacjiDanych() {
		this.wadInfId = "";
		this.wadRodzaj = "KTEL";	
	}
	
//	@Override
//	public void setWadRodzaj(String wadRodzaj) {
//		boolean changed = !eq(this.wadRodzaj, wadRodzaj);
//		super.setWadRodzaj(wadRodzaj);
//		
//		if (changed)  {
//			removeAllPozycje();
//
//			if ( wadRodzaj!=null &&  wadRodzaj.startsWith(  "ADRES_") ) {	//jesli wybrano adres to dodajemy wszystkie pola adresu..		
//				SqlDataSelectionsHandler lw = this.getLW("PPL_AKT_DANYCH_WSL", this.getWadPrcId(), wadRodzaj);
//				for (DataRow dr : lw.getData()) {
//					WniosekAktualizacjiPozycja p = new WniosekAktualizacjiPozycja();
//					p.setWadpRodzaj( ""+dr.get("wsl_wartosc"));
//					p.setWadpWartosc((String) dr.get("aktualna_wartosc"));
//					this.addPptWnioskiAktualizPozycje(p );
//				}
//			}
//		}
//				
//	}



	public void setWadOpisDR(DataRow r) {
		this.setWadOpis((String) r.get("wsl_opis"));
	}


	@Override
	public void setWadInfId(String wadInfId) {
		boolean changed = !eq(this.wadInfId, wadInfId);
		super.setWadInfId(wadInfId);
		
		if (changed)  {
			removeAllPozycje(); 

			if ( wadRodzaj!=null &&  wadRodzaj.startsWith(  "ADRES") ) {	//jesli wybrano adres to dodajemy wszystkie pola adresu..		
				SqlDataSelectionsHandler lw = this.getLW("PPL_AKT_DANYCH_WSL", this.getWadPrcId(), this.getWadInfId(),  this.getWadRodzaj());
				lw.getHiddenRowsPK().clear();
				ArrayList<DataRow> data = lw.getData();
				for (DataRow dr : data) {
					WniosekAktualizacjiPozycja p = new WniosekAktualizacjiPozycja();
					p.setWadpRodzaj( ""+dr.get("wsl_wartosc"));
					p.setWadpWartosc((String) dr.get("aktualna_wartosc"));
					this.addPptWnioskiAktualizPozycje(p );
				}
			}
		}
	}


	public void removeAllPozycje() {
		for (Object object : this.getPptWnioskiAktualizPozycjes().toArray()) {
			this.removePptWnioskiAktualizPozycje((PptWnioskiAktualizPozycje) object);
		}
	}
	
	
	

}
