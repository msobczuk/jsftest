package com.comarch.egeria.pp.ksiazka.ustawienia;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PPT_KAD_KSIAZKA_KONTAKTOWA database table.
 * 
 */
@Entity
@Table(name="PPT_KAD_KSIAZKA_KONTAKTOWA", schema="PPADM")
@NamedQuery(name="PptKsiazkaKontaktowa.findAll", query="SELECT p FROM PptKsiazkaKontaktowa p")
public class PptKsiazkaKontaktowa implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id//UWAGA tu nie stosujemy póki co generatora - kopiujemy pole KKON_ID = KKON_PRC_ID... może w innehj bazie bedzie generator
	@Column(name="KKON_ID")
	private long kkonId;

	@Column(name="KKON_BUDYNEK_ID")
	private Long kkonBudynekId;

	@Column(name="KKON_DODATKOWY_1")
	private String kkonDodatkowy1;

	@Column(name="KKON_DODATKOWY_2")
	private String kkonDodatkowy2;

	@Column(name="KKON_FAKS")
	private String kkonFaks;

	@Column(name="KKON_KIERUNKOWY")
	private String kkonKierunkowy;

	@Column(name="KKON_KOMORKOWY")
	private String kkonKomorkowy;

	@Column(name="KKON_KRAJ_ID")
	private Long kkonKrajId;

	@Column(name="KKON_MIASTO_ID")
	private Long kkonMiastoId;

	@Column(name="KKON_PIETRO_ID")
	private Long kkonPietroId;

	@Column(name="KKON_POKOJ_ID")
	private Long kkonPokojId;

	@Column(name="KKON_PRC_ID")
	private Long kkonPrcId;

	@Column(name="KKON_STACJONARNY")
	private String kkonStacjonarny;

	@Column(name="KKON_STANOWISKO")
	private String kkonStanowisko;

	@Column(name="KKON_STATUS_EDYCJI")
	private Long kkonStatusEdycji;

	@Column(name="KKON_UWAGI")
	private String kkonUwagi;

	@Column(name="KKON_WEWNETRZNY")
	private String kkonWewnetrzny;

	public PptKsiazkaKontaktowa() {
	}

	public long getKkonId() {
		return this.kkonId;
	}

	public void setKkonId(long kkonId) {
		this.kkonId = kkonId;
	}

	public Long getKkonBudynekId() {
		return this.kkonBudynekId;
	}

	public void setKkonBudynekId(Long kkonBudynekId) {
		if (this.kkonBudynekId!=kkonBudynekId){
			this.kkonPietroId = null;
			this.kkonPokojId = null;
		}
		this.kkonBudynekId = kkonBudynekId;
	}

	public String getKkonDodatkowy1() {
		return this.kkonDodatkowy1;
	}

	public void setKkonDodatkowy1(String kkonDodatkowy1) {
		this.kkonDodatkowy1 = kkonDodatkowy1;
	}

	public String getKkonDodatkowy2() {
		return this.kkonDodatkowy2;
	}

	public void setKkonDodatkowy2(String kkonDodatkowy2) {
		this.kkonDodatkowy2 = kkonDodatkowy2;
	}

	public String getKkonFaks() {
		return this.kkonFaks;
	}

	public void setKkonFaks(String kkonFaks) {
		this.kkonFaks = kkonFaks;
	}

	public String getKkonKierunkowy() {
		return this.kkonKierunkowy;
	}

	public void setKkonKierunkowy(String kkonKierunkowy) {
		this.kkonKierunkowy = kkonKierunkowy;
	}

	public String getKkonKomorkowy() {
		return this.kkonKomorkowy;
	}

	public void setKkonKomorkowy(String kkonKomorkowy) {
		this.kkonKomorkowy = kkonKomorkowy;
	}

	public Long getKkonKrajId() {
		return this.kkonKrajId;
	}

	public void setKkonKrajId(Long kkonKrajId) {
		if (this.kkonKrajId!=kkonKrajId){
			this.kkonMiastoId = null;
			this.kkonBudynekId = null;
			this.kkonPietroId = null;
			this.kkonPokojId = null;
		}
		this.kkonKrajId = kkonKrajId;
	}

	public Long getKkonMiastoId() {
		return this.kkonMiastoId;
	}

	public void setKkonMiastoId(Long kkonMiastoId) {
		if (this.kkonMiastoId!=kkonMiastoId){
			this.kkonBudynekId = null;
			this.kkonPietroId = null;
			this.kkonPokojId = null;
		}
		this.kkonMiastoId = kkonMiastoId;
	}

	public Long getKkonPietroId() {
		return this.kkonPietroId;
	}

	public void setKkonPietroId(Long kkonPietroId) {
		if (this.kkonPietroId!=kkonPietroId){
			this.kkonPokojId = null;
		}
		this.kkonPietroId = kkonPietroId;
	}

	public Long getKkonPokojId() {
		return this.kkonPokojId;
	}

	public void setKkonPokojId(Long kkonPokojId) {
		this.kkonPokojId = kkonPokojId;
	}

	public Long getKkonPrcId() {
		return this.kkonPrcId;
	}

	public void setKkonPrcId(Long kkonPrcId) {
		this.kkonPrcId = kkonPrcId;
	}

	public String getKkonStacjonarny() {
		return this.kkonStacjonarny;
	}

	public void setKkonStacjonarny(String kkonStacjonarny) {
		this.kkonStacjonarny = kkonStacjonarny;
	}

	public String getKkonStanowisko() {
		return this.kkonStanowisko;
	}

	public void setKkonStanowisko(String kkonStanowisko) {
		this.kkonStanowisko = kkonStanowisko;
	}

	public Long getKkonStatusEdycji() {
		return this.kkonStatusEdycji;
	}

	public void setKkonStatusEdycji(Long kkonStatusEdycji) {
		this.kkonStatusEdycji = kkonStatusEdycji;
	}

	public String getKkonUwagi() {
		return this.kkonUwagi;
	}

	public void setKkonUwagi(String kkonUwagi) {
		this.kkonUwagi = kkonUwagi;
	}

	public String getKkonWewnetrzny() {
		return this.kkonWewnetrzny;
	}

	public void setKkonWewnetrzny(String kkonWewnetrzny) {
		this.kkonWewnetrzny = kkonWewnetrzny;
	}

}