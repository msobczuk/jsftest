package com.comarch.egeria.pp.ksiazka;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SqlBean;

@ManagedBean 
@Named
@Scope("session")
public class TelefonyMFBean extends SqlBean {

	private static final long serialVersionUID = -5806366986076184951L;
	private static final Logger log = LogManager.getLogger();//public bo wiekszosc beanow dziedziczy po tym

	@PostConstruct
	public void init(){
		this.getLW("PPL_KS_TEL_MF").setCacheDuration(5 * 60000); //5 minut
	}
	
}
