package com.comarch.egeria.pp.ksiazka.ustawienia;

import java.io.Serializable;
import java.sql.SQLData;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Transaction;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataHandler;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@ManagedBean
@Scope(value = "view")
@Named
public class UstawieniaBean extends SqlBean implements Serializable {

	@Inject
	private SessionBean sessionBean;
	
	private PptKsiazkaKontaktowa kk;
	private DataRow ks;
	
	private Long prcId;
	
	
	@PostConstruct public void init(){
		ustawPracownika();
	} 


	private void ustawPracownika() {
		
		setPrcId(sessionBean.getPrc_id_Long());
		
		DataRow selectedPracownik = sessionBean.getSelectedPracownik(); //Admin/ dane z KsTel - np: [125280, Paulina, Abramczyk, T, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null]
		if (selectedPracownik!=null){
			setPrcId( selectedPracownik.getIdAsLong() );
			sessionBean.setSelectedPracownik(null); //.. aby prawidłowo działał el. Ustawienia w menu (ma wczytywać dane zalogowanego usera)... 
		}
		
		if (kk==null){
				kk = (PptKsiazkaKontaktowa) HibernateContext.get(PptKsiazkaKontaktowa.class, getPrcId());
		}
		
		if (kk==null){
			kk = new PptKsiazkaKontaktowa();
			kk.setKkonId(getPrcId());
			kk.setKkonPrcId(getPrcId());//pole opcjonalne, moze jesli do KS bedziemy dawac nie tylko telefny pracowników ... 
		}
		
//		SqlDataSelectionsHandler sl = this.getSql("sqlUstawieniaDaneKs");
//		if (sl!=null && !sl.getData().isEmpty())
//			this.ks = sl.getData().get(0);
		
		
	}
	
	
	public void zapisz(){
		if (kk==null) return;
		
		try(HibernateContext h = new HibernateContext()){
			Transaction t = h.getSession().beginTransaction();
			h.getSession().saveOrUpdate(kk);
	//		System.out.println("zapisano kk -> PptKsiazkaKontaktowa # " + kk.getKkonId() );
			t.commit();
		}
		SqlDataHandler ksTel = getLW("PPL_KAD_KS_TEL");
		if (ksTel!=null) 
			SqlDataHandler.invalidate("PPL_KAD_KS_TEL", kk.getKkonId());
		else
			System.err.println("ksTel is null");
		
	}
	
	
	public List<DataRow> getKsLokalizacje(String parentId, String kod) {
		// System.out.println("getKsLokalizacje (" + parentId + ", " + kod + ")
		// ... ");
		ArrayList<DataRow> ret = new ArrayList<DataRow>();
		SqlDataSelectionsHandler sl = this.getSql("PPL_KTEL_LOKALIZACJE");
		if (sl == null)
			return null;
		ArrayList<DataRow> data = sl.getData();
		for (DataRow r : data) {
			// System.out.println("????" + r + "" + r.get("PL_KOD"));
			if ((kod != null && kod.equals("" + r.get("KLOK_KOD")))
					|| (parentId != null && parentId.equals("" + r.get("KLOK_KLOK_ID")))
					|| (kod == null && parentId == null)) {
				// System.out.println(">>>>" + r);
				ret.add(r);
			}
		}
		// System.out.println("getKsLokalizacje ...END / " + ret.size());
		return ret;
	}

	public PptKsiazkaKontaktowa getKk() {
//		if (kk==null) ustawPracownika();
		return kk;
	}

	public void setKk(PptKsiazkaKontaktowa kk) {
		this.kk = kk;
	}


	public DataRow getKs() {
		
		if (ks==null){
			SqlDataSelectionsHandler sl = this.getSql("PPL_KTEL_PRC_DANE");
			if (sl!=null && !sl.getData().isEmpty())
				this.ks = sl.getData().get(0);
		}
		
		return ks;
	}


	public void setKs(DataRow ks) {
		this.ks = ks;
	}


	public Long getPrcId() {
		return prcId;
	}


	public void setPrcId(Long prcId) {
		this.prcId = prcId;
	}
	
	
	
}
