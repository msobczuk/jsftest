package com.comarch.egeria.pp.WniosekOkulary;

import static com.comarch.egeria.Utils.*;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.WniosekOkulary.model.WniosekOkulary;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@Scope("view")
@Named
public class WniosekOkularyBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Inject
	ObiegBean obieg;

	private static final Logger log = LogManager.getLogger();

	private WniosekOkulary wniosekOkulary = new WniosekOkulary();


	@PostConstruct
	public void init() {
		
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		if (id == null) {
			this.setWniosekOkulary(new WniosekOkulary());
			this.getWniosekOkulary().setWnoPrcId( AppBean.getUser().getPrcIdLong() );
			this.getWniosekOkulary().setWnoDataZlozenia( new Date() );
		} else {
			this.setWniosekOkulary((WniosekOkulary) HibernateContext.get(WniosekOkulary.class, (Serializable) id));
		}

		wniosekOkulary.getPplBhpOkulStanKosztow().getData();
	}


	public void zapisz() {

		if (!validateBeforeSaveWniosekOkulary())
			return;

		try {
			HibernateContext.saveOrUpdate(this.getWniosekOkulary());
			User.info("Udany zapis wniosku o okulary o numerze " + this.getWniosekOkulary().getWnoId());
			obieg.setIdEncji(this.getWniosekOkulary().getWnoId());
		} catch (Exception e) {
			User.alert(e);//User.alert("Błąd w zapisie wniosku o okulary!");
			log.error(e.getMessage(), e);
		}

	}

	public void usun() {

		try {
			HibernateContext.delete(getWniosekOkulary());
//			User.info("Usunięto wniosek o rekrutację");
			FacesContext.getCurrentInstance().getExternalContext().redirect("ListaWnioskowOkulary");
		} catch (Exception e) {
			User.alert("Błąd w usuwaniu wniosku!");
			log.error(e.getMessage(), e);
		}

	}


	private boolean validateBeforeSaveWniosekOkulary() {

		ArrayList<String> inval = new ArrayList<String>();

		String dateInval = "";

		Calendar cal = Calendar.getInstance();

		Boolean ret = true;

		if (getWniosekOkulary().getWnoKodBudzZad() == null || "".equals(getWniosekOkulary().getWnoKodBudzZad())) {
			inval.add("Kod budżetu zadaniowego jest polem wymaganym.");
		}
		if (getWniosekOkulary().getWnoSkId() == null || "".equals(getWniosekOkulary().getWnoSkId())) {
			inval.add("Źródło finansowania jest polem wymaganym.");
		}
		if (getWniosekOkulary().getWnoKoszt() == null || getWniosekOkulary().getWnoKoszt() <= 0.0) {
			inval.add("Koszt jest polem wymaganym.");
		}

		if (getWniosekOkulary().getWnoDataZlozenia() != null) {

			if (getWniosekOkulary().getWnoAudytDt() != null) {
				Date resetTime = trunc(getWniosekOkulary().getWnoAudytDt());
				if (getWniosekOkulary().getWnoDataZlozenia().before(resetTime)) {
					dateInval += "Data złożenia wniosku nie może być wcześniejsza niż data bieżąca.";
				}
			} else if (asLocalDate(getWniosekOkulary().getWnoDataZlozenia()).isBefore(asLocalDate(cal.getTime()))) {
				dateInval += "Data złożenia wniosku nie może być wcześniejsza niż data bieżąca. ";
			} 

		}
		else {
			inval.add("Data złożenia wniosku nie może być pusta.");
		}

		String txt = "";
		for (String s : inval) {
			txt += s + "</br></br>";
		}

		if (!inval.isEmpty()) {
			ret = false;
			User.warn(txt);
		}
		if (!dateInval.isEmpty()) {
			ret = false;
			User.warn(dateInval);
		}

		return ret;
	}


	
	public void raport() {
		if (this.getWniosekOkulary() == null)
			return;
			
		Long idAsLong = this.getWniosekOkulary().getWnoId() ; //wniosekOkularyDR.getIdAsLong();
		
		if (idAsLong != null && idAsLong > 0) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("id_wniosku", idAsLong);
				ReportGenerator.displayReportAsPDF(params, "Okulary_wniosek");
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
	}


	public WniosekOkulary getWniosekOkulary() {
		return wniosekOkulary;
	}


	public void setWniosekOkulary(WniosekOkulary wniosekOkulary) {
		this.wniosekOkulary = wniosekOkulary;
	}


	
	



}
