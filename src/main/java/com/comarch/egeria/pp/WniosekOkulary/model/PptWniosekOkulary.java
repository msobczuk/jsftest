package com.comarch.egeria.pp.WniosekOkulary.model;

import com.comarch.egeria.pp.data.model.ModelBase;
import org.hibernate.annotations.DiscriminatorFormula;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the PPT_KAD_WN_OKULARY database table.
 * 
 */
@Entity
@Table(name="PPT_KAD_WN_OKULARY", schema="PPADM")
@NamedQuery(name="PptWniosekOkulary.findAll", query="SELECT p FROM PptWniosekOkulary p")
@DiscriminatorFormula("'WniosekOkulary'")
public class PptWniosekOkulary extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_KAD_WN_OKULARY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_KAD_WN_OKULARY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_KAD_WN_OKULARY")	
	@Column(name="WNO_ID")
	protected Long wnoId = 0L;

	@Temporal(TemporalType.DATE)
	@Column(name="WNO_AUDYT_DM")
	protected Date wnoAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="WNO_AUDYT_DT")
	protected Date wnoAudytDt;

	@Column(name="WNO_AUDYT_KM")
	protected String wnoAudytKm;

	@Version  
	@Column(name="WNO_AUDYT_LM")
	protected Long wnoAudytLm;

	@Column(name="WNO_AUDYT_UM")
	protected String wnoAudytUm;

	@Column(name="WNO_AUDYT_UT")
	protected String wnoAudytUt;

	@Temporal(TemporalType.DATE)
	@Column(name="WNO_DATA_ZLOZENIA")
	protected Date wnoDataZlozenia = new Date();

	@Column(name="WNO_KOD_BUDZ_ZAD")
	protected String wnoKodBudzZad;

	@Column(name="WNO_KOSZT")
	protected Double wnoKoszt;

	@Column(name="WNO_PRC_ID")
	protected Long wnoPrcId;


	@Column(name="WNO_SK_ID")
	protected Long wnoSkId;

	@Column(name="WNO_WNR_ID")
	protected Long wnoWnrId;



	@Column(name="WNO_NR_WNIOSKU")
	protected String wnoNrWniosku;

	@Column(name="WNO_NR_Wydatku")
	protected String wnoNrWydatku;






	public PptWniosekOkulary() {
	}

	public Long getWnoId() {
		this.onRXGet("wnoId");
		return this.wnoId;
	}

	public void setWnoId(Long wnoId) {
		this.onRXSet("wnoId", this.wnoId, wnoId);
		this.wnoId = wnoId;
	}

	public Date getWnoAudytDm() {
		this.onRXGet("wnoAudytDm");
		return this.wnoAudytDm;
	}

	public void setWnoAudytDm(Date wnoAudytDm) {
		this.onRXSet("wnoAudytDm", this.wnoAudytDm, wnoAudytDm);
		this.wnoAudytDm = wnoAudytDm;
	}

	public Date getWnoAudytDt() {
		this.onRXGet("wnoAudytDt");
		return this.wnoAudytDt;
	}

	public void setWnoAudytDt(Date wnoAudytDt) {
		this.onRXSet("wnoAudytDt", this.wnoAudytDt, wnoAudytDt);
		this.wnoAudytDt = wnoAudytDt;
	}

	public String getWnoAudytKm() {
		this.onRXGet("wnoAudytKm");
		return this.wnoAudytKm;
	}

	public void setWnoAudytKm(String wnoAudytKm) {
		this.onRXSet("wnoAudytKm", this.wnoAudytKm, wnoAudytKm);
		this.wnoAudytKm = wnoAudytKm;
	}

	public Long  getWnoAudytLm() {
		this.onRXGet("wnoAudytLm");
		return this.wnoAudytLm;
	}

	public void setWnoAudytLm(Long  wnoAudytLm) {
		this.onRXSet("wnoAudytLm", this.wnoAudytLm, wnoAudytLm);
		this.wnoAudytLm = wnoAudytLm;
	}

	public String getWnoAudytUm() {
		this.onRXGet("wnoAudytUm");
		return this.wnoAudytUm;
	}

	public void setWnoAudytUm(String wnoAudytUm) {
		this.onRXSet("wnoAudytUm", this.wnoAudytUm, wnoAudytUm);
		this.wnoAudytUm = wnoAudytUm;
	}

	public String getWnoAudytUt() {
		this.onRXGet("wnoAudytUt");
		return this.wnoAudytUt;
	}

	public void setWnoAudytUt(String wnoAudytUt) {
		this.onRXSet("wnoAudytUt", this.wnoAudytUt, wnoAudytUt);
		this.wnoAudytUt = wnoAudytUt;
	}

	public Date getWnoDataZlozenia() {
		this.onRXGet("wnoDataZlozenia");
		return this.wnoDataZlozenia;
	}

	public void setWnoDataZlozenia(Date wnoDataZlozenia) {
		this.onRXSet("wnoDataZlozenia", this.wnoDataZlozenia, wnoDataZlozenia);
		this.wnoDataZlozenia = wnoDataZlozenia;
	}

	public String getWnoKodBudzZad() {
		this.onRXGet("wnoKodBudzZad");
		return this.wnoKodBudzZad;
	}

	public void setWnoKodBudzZad(String wnoKodBudzZad) {
		this.onRXSet("wnoKodBudzZad", this.wnoKodBudzZad, wnoKodBudzZad);
		this.wnoKodBudzZad = wnoKodBudzZad;
	}

	public Double getWnoKoszt() {
		this.onRXGet("wnoKoszt");
		return this.wnoKoszt;
	}

	public void setWnoKoszt(Double wnoKoszt) {
		this.onRXSet("wnoKoszt", this.wnoKoszt, wnoKoszt);
		this.wnoKoszt = wnoKoszt;
	}

	public Long getWnoPrcId() {
		this.onRXGet("wnoPrcId");
		return this.wnoPrcId;
	}

	public void setWnoPrcId(Long wnoPrcId) {
		this.onRXSet("wnoPrcId", this.wnoPrcId, wnoPrcId);
		this.wnoPrcId = wnoPrcId;
	}

	public Long getWnoSkId() {
		this.onRXGet("wnoSkId");
		return this.wnoSkId;
	}

	public void setWnoSkId(Long wnoSkId) {
		this.onRXSet("wnoSkId", this.wnoSkId, wnoSkId);
		this.wnoSkId = wnoSkId;
	}




	public Long getWnoWnrId() {
		this.onRXGet("wnoWnrId");
		return wnoWnrId;
	}

	public void setWnoWnrId(Long wnoWnrId) {
		this.onRXSet("wnoWnrId", this.wnoWnrId, wnoWnrId);
		this.wnoWnrId = wnoWnrId;
	}



	public String getWnoNrWniosku() {
		this.onRXGet("wnoNrWniosku");
		return wnoNrWniosku;
	}

	public void setWnoNrWniosku(String wnoNrWniosku) {
		this.onRXSet("wnoNrWniosku", this.wnoNrWniosku, wnoNrWniosku);
		this.wnoNrWniosku = wnoNrWniosku;
	}


	public String getWnoNrWydatku() {
		this.onRXGet("wnoNrWydatku");
		return wnoNrWydatku;
	}

	public void setWnoNrWydatku(String wnoNrWydatku) {
		this.onRXSet("wnoNrWydatku", this.wnoNrWydatku, wnoNrWydatku);
		this.wnoNrWydatku = wnoNrWydatku;
	}

}
