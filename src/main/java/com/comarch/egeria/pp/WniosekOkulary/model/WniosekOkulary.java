package com.comarch.egeria.pp.WniosekOkulary.model;

import static com.comarch.egeria.Utils.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import javax.persistence.Entity;

import com.comarch.egeria.Params;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;

@Entity
public class WniosekOkulary extends PptWniosekOkulary {

	private static final long serialVersionUID = 1L;

	public void setWnoPrcId(Long wnoPrcId) {
		boolean zm = !eq(this.wnoPrcId, wnoPrcId);
		super.setWnoPrcId(wnoPrcId);
		if (zm){
			aktualizujIdZrodla();	
		}
	}
	
	public void setWnoDataZlozenia(Date wnoDataZlozenia) {
		boolean zm = !eq(this.wnoDataZlozenia, wnoDataZlozenia);
		super.setWnoDataZlozenia(wnoDataZlozenia); //onRXSet...
		if (zm){
			aktualizujIdZrodla();	
		}
	}
	
	
	public void setWnoSkId(Long wnoSkId) {
		boolean zm = !eq(this.wnoSkId, wnoSkId);
		super.setWnoSkId(wnoSkId); //onRXSet...
		if (zm){
			aktualizujKodBudzetu();	
		}
	}
	
	public void aktualizujIdZrodla() {
		ArrayList<DataRow> stanKosztLW = getPplBhpOkulStanKosztow().getData();
		if (!stanKosztLW.isEmpty()) {
			if(stanKosztLW.get(0)!=null){
				DataRow dr = stanKosztLW.get(0);
				this.setWnoSkId(  (dr.get("sk_id") == null) ? null : ((BigDecimal) dr.get("sk_id")).longValue() );
			}
		}
	}

	public SqlDataSelectionsHandler getPplBhpOkulStanKosztow() {

		SqlDataSelectionsHandler ret = this.getLW("PPL_BHP_OKUL_STAN_KOSZTOW",

				new Params(
						"P_PRC_ID", this.getWnoPrcId(),
						"P_DATA_WNO", this.getWnoDataZlozenia()
				)

		);

		ret.getData();
		return ret;
	}


	public String getKodZrodlaFinansowania() {
		DataRow r = this.getLW("PPL_BHP_OKU_STN_KSZT_WSZ").find(getWnoSkId());
		if (r!=null){
			return (String) r.get("sk_kod");
		}
		return null;
	}
	
	
	public String getOpisZrodlaFinansowania() {
		DataRow r = this.getLW("PPL_BHP_OKU_STN_KSZT_WSZ").find(getWnoSkId());
		if (r!=null){
			return (String) r.get("sk_opis");
		}
		return null;
	}
	
	
	public void aktualizujKodBudzetu() {
		DataRow r = this.getLW("PPL_BHP_OKU_STN_KSZT_WSZ").find(getWnoSkId());
		if (r!=null){
			this.setWnoKodBudzZad(""+r.get("sk_wymiar14"));
			if ("00.00.00.00".equals( this.getWnoKodBudzZad() )){
				User.warn("Dla tego pracownika nie udalo sie ustalić źródła finansowania właściwego na dzień %1$s"
						+ " (kod budżetu zadaniowego ustawiono na '00.00.00.00').", 
						asString( this.getWnoDataZlozenia() ));
			}
		}

	}
	
	
}
