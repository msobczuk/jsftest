package com.comarch.egeria.pp.pracownik;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import org.springframework.context.annotation.Scope;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Scope("view")
@Named
public class RodzinaBean extends SqlBean implements Serializable {

	String obecnaData = null;
	private SqlDataSelectionsHandler czRodzSlownik = null;
//	ArrayList<DataRow> listaSrodkowDruk;
	

	@Inject
	private SessionBean sessionBean;
	

	public void selectRodzina() {
		
		System.out.println(
				"=================================RodzinaBean.selectRodzina===================================");
		SqlDataSelectionsHandler p = this.getSql("PP_KAD_RODZINA");
		if (p != null) {
			System.out.println(p.getName() + " / currentRow: " + p.getCurrentRow());

			sessionBean.setSelectedRodzina(p.getCurrentRow());
			


		}
	}
	
	
	public ArrayList<DataRow> getCzlonkowieRodziny() {
		if (this.czRodzSlownik == null) {
			this.setCzlonkowieRodziny(this.getSql("PP_KAD_RODZINA"));
		}
		return this.czRodzSlownik.getData();
	}

	
	public void setCzlonkowieRodziny(SqlDataSelectionsHandler czRodzSlownik) {
		this.czRodzSlownik = czRodzSlownik;
	}
	
	public String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		this.obecnaData = dateFormat.format(date);
		return this.obecnaData;
	}

}
