package com.comarch.egeria.pp.pracownik;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.inject.Named;
import java.io.Serializable;
import java.sql.SQLException;

@Named
@Scope("view")
public class RachunkiUCPBean extends EkpDokumentyBase implements Serializable {
  public static final Logger log = LogManager.getLogger(RachunkiUCPBean.class);

  @Override
  void markDocumentAsReleased(Long docId) throws SQLException {
    super.markDocumentAsReleased(docId);
    this.getSql("PPL_KAD_RACHUCP").resetTs();
  }
}
