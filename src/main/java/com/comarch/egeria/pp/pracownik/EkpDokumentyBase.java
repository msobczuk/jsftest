package com.comarch.egeria.pp.pracownik;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;

import javax.inject.Named;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;

@Named
@Scope("view")
public class EkpDokumentyBase extends SqlBean implements Serializable {
  public static final Logger log = LogManager.getLogger();

  public StreamedContent pobierz(DataRow dr) {
    if (dr==null) return null;

    StreamedContent document = null;
    try {
      String nazwa_pliku = dr.getAsString("nazwa_pliku");
      if (nazwa_pliku==null){
        User.alert("W bazie brak pliku do pobrania.");
        return null;
      }

      document = blob2streamEkpDokumentyPliki(dr.getIdAsLong(), "application/pdf", nazwa_pliku);//downloadDocumentFromDB(dr.getIdAsLong(), nazwa_pliku);

      if (shouldBeMarkedAsReleased(dr)) {
        markDocumentAsReleased(dr.getIdAsLong());
      }
    } catch (Exception e) {
      log.error("Problem z pobraniem dokumentu", e);
    }

    return document;
  }


  public static DefaultStreamedContent blob2streamEkpDokumentyPliki(Long docId, String contentType, String filename) throws SQLException, IllegalAccessException, ClassNotFoundException, InstantiationException {
    try (Connection conn = JdbcUtils.getConnection()) {
      Object oBlob = JdbcUtils.sqlExecScalarQuery(conn, "select ekp_dokumenty.zwroc_plik(?) from dual", docId);
      if (oBlob == null) {
        User.alert("Dokument " + docId+ " nie istnieje");
        return null;
      }
      Blob blob = (Blob) oBlob;
      return new DefaultStreamedContent(blob.getBinaryStream(), contentType, filename);
    }
  }


  boolean shouldBeMarkedAsReleased(DataRow dr) {
    return isUserDocumentsOwner(dr) && !isMarkedAsReleased(dr);
  }

  boolean isUserDocumentsOwner(DataRow dr) {
    return AppBean.getUser().getPrcId() == ((BigDecimal) dr.get("dok_prc_id")).intValue();
  }

  boolean isMarkedAsReleased(DataRow dr) {
    return "T".equals(dr.get("czy_wydano"));
  }

  void markDocumentAsReleased(Long docId) throws SQLException {
    try (Connection conn = JdbcUtils.getConnection()) {
      JdbcUtils.sqlSPCall(conn, "EKP_DOKUMENTY.USTAW_DATE_WYDANIA", docId, new Date(System.currentTimeMillis()));
//      this.getSql("PPL_KAD_PIT11").resetTs(); //polimofricznie
    }
  }




}
