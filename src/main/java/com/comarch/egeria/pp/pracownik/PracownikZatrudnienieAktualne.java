package com.comarch.egeria.pp.pracownik;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.inject.Named;

//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

@Scope("view")
@Named
public class PracownikZatrudnienieAktualne extends SqlBean implements Serializable {

	private String obecnaData = null;
	private Integer pracownikZatrTabViewActiveIndex = 0;
	private DataRow selectedHistoriaUmow = null;
	private DataRow selectedWynagrodzenie = null;
	private DataRow HistoriaSelectedZatrudnienie = null;
	private String pracownikZatrTabViewFile = "PracownikZatrudnienieTab1.xhtml";

//	private SqlDataSelectionsHandler prcZatr1 = null;
//	private SqlDataSelectionsHandler prcZatr2 = null;
//	private SqlDataSelectionsHandler prcZatr3 = null;
	
	
	private int activeIndex = 0;
	
//	public ArrayList<DataRow> getListaZatr1() {
//		if (this.prcZatr1 == null) {
//			this.setListaZatr1(this.getSql("PPL_KAD_ZATR_AKT"));
//		}
//		return this.prcZatr1.getData();
//	}
//
//	public void setListaZatr1(SqlDataSelectionsHandler prcZatr1) {
//		this.prcZatr1 = prcZatr1;
//	}

//	public ArrayList<DataRow> getListaZatr2() {
//		if (this.prcZatr2 == null) {
//			this.setListaZatr2(this.getSql("PPL_KAD_PRC_FORM_ZATR"));
//		}
//		return this.prcZatr2.getData();
//	}
//
//	public void setListaZatr2(SqlDataSelectionsHandler prcZatr2) {
//		this.prcZatr2 = prcZatr2;
//	}

//	public ArrayList<DataRow> getListaZatr3() {
//		if (this.prcZatr3 == null) {
//			this.setListaZatr3(this.getLW("PPL_PRC_HIST_ZATR"));
//		}
//		return this.prcZatr3.getData();
//	}
//
//	public void setListaZatr3(SqlDataSelectionsHandler prcZatr3) {
//		this.prcZatr3 = prcZatr3;
//	}

	public String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		this.obecnaData = dateFormat.format(date);
		return this.obecnaData;
	}

	public Object getAktualneZatrId() {
		SqlDataSelectionsHandler slZatr = this.getSql("PPL_KAD_ZATR_AKT");
		if (slZatr == null || slZatr.getData().isEmpty())
			return null;
		DataRow r = slZatr.getData().get(0);
		return r.get("PP_Z_ID");
	}

	public Object getPoprzednieZatrId() {
		if (getHistoriaSelectedZatrudnienie() == null || getHistoriaSelectedZatrudnienie().isEmpty())
			return null;
		DataRow r = getHistoriaSelectedZatrudnienie();
		System.out.println(r);
		System.out.println(r.get("PP_Z_ID"));
		return r.get("PP_Z_ID");
	}


	public void setPracownikZatrTab(Integer indexTabView, String xhtmlFileName) {
		this.setPracownikZatrTabViewActiveIndex(indexTabView);
		this.setPracownikZatrTabViewFile(xhtmlFileName);
	}

	public Object get(String columnName) {
		SqlDataSelectionsHandler sl = this.getSql("PPL_KAD_ZATR_AKT");
		if (sl == null)
			return null;
		ArrayList<DataRow> data = sl.getData();
		if (data == null || data.isEmpty())
			return null;
		DataRow r = data.get(0);
		return r.get(columnName);
	}

	public Integer getPracownikZatrTabViewActiveIndex() {
		return pracownikZatrTabViewActiveIndex;
	}

	public void setPracownikZatrTabViewActiveIndex(Integer pracownikZatrTabViewActiveIndex) {
		this.pracownikZatrTabViewActiveIndex = pracownikZatrTabViewActiveIndex;
	}

	public String getPracownikZatrTabViewFile() {
		return pracownikZatrTabViewFile;
	}

	public void setPracownikZatrTabViewFile(String pracownikZatrTabViewFile) {
		this.pracownikZatrTabViewFile = pracownikZatrTabViewFile;
	}

	public DataRow getSelectedHistoriaUmow() {
		return selectedHistoriaUmow;
	}

	public void setSelectedHistoriaUmow(DataRow selectedHistoriaUmow) {
		this.selectedHistoriaUmow = selectedHistoriaUmow;
	}

	public DataRow getSelectedWynagrodzenie() {
		return selectedWynagrodzenie;
	}

	public void setSelectedWynagrodzenie(DataRow selectedWynagrodzenie) {
		this.selectedWynagrodzenie = selectedWynagrodzenie;
	}

	public DataRow getHistoriaSelectedZatrudnienie() {
		return HistoriaSelectedZatrudnienie;
	}

	public void setHistoriaSelectedZatrudnienie(DataRow selectedPremia) {
		this.HistoriaSelectedZatrudnienie = selectedPremia;
	}

	public void test() {
		System.out.println("Historia zatrudnienia");
	}

	public int getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
//		com.comarch.egeria.Utils.update("prntPnl");
	}

}
