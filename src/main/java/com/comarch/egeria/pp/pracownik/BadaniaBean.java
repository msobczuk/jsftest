package com.comarch.egeria.pp.pracownik;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import org.springframework.context.annotation.Scope;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Scope("view")
@Named
public class BadaniaBean extends SqlBean implements Serializable {

	String obecnaData = null;
	private SqlDataSelectionsHandler rodzBadSlownik = null;
//	ArrayList<DataRow> listaSrodkowDruk;
	

	@Inject
	private SessionBean sessionBean;
	

public void selectBadanie() {
		
		System.out.println(
				"=================================BadaniaBean.selectBadanie===================================");
		SqlDataSelectionsHandler p = this.getSql("PPL_KAD_BADANIA");
		if (p != null) {
			System.out.println(p.getName() + " / currentRow: " + p.getCurrentRow());

			sessionBean.setSelectedBadanie(p.getCurrentRow());
			

		}
	}
	

	public ArrayList<DataRow> getBadania() {
		if (this.rodzBadSlownik == null) {
			this.setBadania(this.getSql("PPL_KAD_BADANIA"));
		}
		return this.rodzBadSlownik.getData();
	}

	
	public void setBadania(SqlDataSelectionsHandler rodzBadSlownik) {
		this.rodzBadSlownik = rodzBadSlownik;
	}
	
	public String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		this.obecnaData = dateFormat.format(date);
		return this.obecnaData;
	}

}
