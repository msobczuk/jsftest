package com.comarch.egeria.pp.pracownik;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.inject.Named;
import java.io.Serializable;
import java.sql.SQLException;

@Named
@Scope("view")
public class Pit11Bean extends EkpDokumentyBase implements Serializable {
  public static final Logger log = LogManager.getLogger();

  @Override
  void markDocumentAsReleased(Long docId) throws SQLException {
    super.markDocumentAsReleased(docId);
    this.getSql("PPL_KAD_PIT11").resetTs(); //polimofricznie
  }

//  public StreamedContent pobierz(DataRow dr) {
//    StreamedContent document = null;
//    try {
//      String nazwa_pliku = (String) dr.get("nazwa_pliku");
//      if (nazwa_pliku==null){
//        User.alert("W bazie brak pliku do pobrania.");
//        return null;
//      }
//
//      document = JdbcUtils.blob2streamEkpDokumentyPliki(dr.getIdAsLong(), "application/pdf", nazwa_pliku);; //downloadDocumentFromDB(dr.getIdAsLong(), nazwa_pliku);
//      if (shouldBeMarkedAsReleased(dr)) {
//        markDocumentAsReleased(dr.getIdAsLong());
//      }
//    } catch (Exception e) {
//      log.error("Problem z pobraniem dokumentu", e);
//    }
//
//    return document;
//  }
//
//  private boolean shouldBeMarkedAsReleased(DataRow dr) {
//    return isUserDocumentsOwner(dr) && !isMarkedAsReleased(dr);
//  }
//
//  private boolean isUserDocumentsOwner(DataRow dr) {
//    return AppBean.getUser().getPrcId() == ((BigDecimal) dr.get("dok_prc_id")).intValue();
//  }
//
//  private boolean isMarkedAsReleased(DataRow dr) {
//    return "T".equals(dr.get("czy_wydano"));
//  }

//  private void markDocumentAsReleased(Long docId) throws SQLException {
//    try (Connection conn = JdbcUtils.getConnection()) {
//      JdbcUtils.sqlSPCall(conn, "EKP_DOKUMENTY.USTAW_DATE_WYDANIA", docId, new Date(System.currentTimeMillis()));
//      this.getSql("PPL_KAD_PIT11").resetTs();
//    }
//  }




}
