package com.comarch.egeria.pp.sluzbaprzygotowawcza;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Session;
import org.primefaces.component.tabview.TabView;
import org.primefaces.event.TabChangeEvent;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.sluzbaprzygotowawcza.domain.ZptSlprBloki;
import com.comarch.egeria.pp.sluzbaprzygotowawcza.domain.ZptSluzbaPrzyg;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean
@Named
@Scope("view")
public class ArkuszSluzbyPrzygBean extends SqlBean {


//	@Inject
//	SessionBean sessionBean;
	
//	@Inject
//	GeneratorRaportow genrap;

	private static final long serialVersionUID = 1L;

	@Inject
	ObiegBean obieg;
	
//	@Inject
//	JdbcDAO jdbc;
	
//	@Inject
//	protected ReportGenerator ReportGenerator;
	
	private DataRow arkuszDR = null;
	private ZptSluzbaPrzyg arkusz = new ZptSluzbaPrzyg();// uwaga ustawiono
															// fetch = EAGER

	private long prcId;
	private boolean wniosekZwol = false;
	private boolean wniosekZwolZeSluzbyPrzyg = true;
	private boolean czyNowy = false;
//	private boolean init = true;
	private int activeIndex = 0;
	
	String checkCzI = ""; 
	String checkCzII = "";
	String checkCzIII ="";
	String checkCzV ="";
	String checkCzVI ="";

	
	public String getDate(){
		return (new Date()) .toString();
	} 
	
 
	
	@PostConstruct
	public void init() {
		obieg.setKategoriaObiegu("WN_SLPR");
		
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		if (id == null) {
			czyNowy = true;
			return;
		}

		arkuszDR = (DataRow) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("row");

		Long idAsLong = arkuszDR.getIdAsLong();

		this.setArkusz( (ZptSluzbaPrzyg) HibernateContext.get(ZptSluzbaPrzyg.class, idAsLong) );

		if (arkusz.getSlprDecZwol() != null && arkusz.getSlprDecZwol().equals("T"))
			wniosekZwolZeSluzbyPrzyg = true;
		if (arkusz.getSlprDecZwol() != null && arkusz.getSlprDecZwol().equals("N"))
			wniosekZwolZeSluzbyPrzyg = false;

		if (arkusz.getSlprCzyZwol() != null && arkusz.getSlprCzyZwol().equals("T"))
			wniosekZwol = true;
		if (arkusz.getSlprCzyZwol() != null && arkusz.getSlprCzyZwol().equals("N"))
			wniosekZwol = false;
		
		
//		checkCzI = getCheckString_CzI();
//		checkCzII = getCheckString_CzII();
//		checkCzIII = getCheckString_CzIII();
//		checkCzV = getCheckString_CzV();
//		checkCzVI = getCheckString_CzVI();
//		init=false;

		obieg.setIdEncji(this.arkusz.getSlprId());
		
	}
	
	public void raport(String nazwa){
		
		if (arkusz!= null && arkusz.getSlprId()>0) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("SLPR_ID", arkusz.getSlprId());
			try {
				ReportGenerator.displayReportAsPDF(params, nazwa);
	
			} catch (JRException | SQLException | IOException e) {
				e.printStackTrace();
				User.alert(e.getMessage());
			}
		}
	}

	public int getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
	}
	
    public void onTabChange(TabChangeEvent event) 
    {   
        TabView tabView = (TabView) event.getComponent();
        activeIndex = tabView.getChildren().indexOf(event.getTab());
/*        System.out.println("tab id = " + event.getTab().getId());
        System.out.println("active index = " + activeIndex);*/
    }

	public void zapisz() {
		System.out.println("ArkuszSluzbyPrzygBean.zapisz() ... ");

		if (!validatePermissions())
			return;
		
		if (!validateBeforeSaveArkusz())
			return;
		
		
		przypiszBloki();

		try {
			arkusz = (ZptSluzbaPrzyg) HibernateContext.save(arkusz);
			User.info("Zapisano arkusz służby przygotowawczej o numerze %1$s", this.arkusz.getSlprId());
			czyNowy = false;
			obieg.setIdEncji(this.arkusz.getSlprId());
//			checkCzI = getCheckString_CzI();
//			checkCzII = getCheckString_CzII();
//			checkCzIII = getCheckString_CzIII();
//			checkCzV = getCheckString_CzV();
//			checkCzVI = getCheckString_CzVI();
		} catch (Exception e) {
			User.alert("Nieudany zapis służby przygotowawczej!\n" + e.getMessage());
			System.err.println(e);
		}
		
	}
	
	public void reloadPage() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("ArkuszSluzbyPrzygotowawczej");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void przypiszBloki(){
		
		if (!wniosekZwol) {

			if (arkusz.getZptSlprBlokis() == null) {
				List<ZptSlprBloki> zpb = new ArrayList<ZptSlprBloki>();
				arkusz.setZptSlprBlokis(zpb);
			}
			arkusz.getZptSlprBlokis().clear();

			SqlDataSelectionsHandler lw = this.getLW("PPL_SLP_BLOKI_TEMAT");
			for (DataRow dr : lw.getData()) {
				//if (this.getSqls().get("PPL_SLP_BLOKI_TEMAT").getSelectedRows().contains(dr)) {
				if (lw.getSelectedRows().contains(dr)) {
					ZptSlprBloki zsb = new ZptSlprBloki();
					zsb.setSlpbBlok((String) dr.get("pp_skrot_bloku"));
					zsb.setSlpbCzyWskazany("T");
					arkusz.addZptSlprBloki(zsb);
				}
				// if
				// (this.getSqls().get("sqlblokitematyczne").getSelectedRows().contains(dr))
				// {
				// ZptSlprBloki zsb = new ZptSlprBloki();
				// zsb.setSlpbBlok((String) dr.get("pp_skrot_bloku"));
				// zsb.setSlpbCzyWskazany("T");
				// arkusz.addZptSlprBloki(zsb);
				// } else {
				// ZptSlprBloki zsb = new ZptSlprBloki();
				// zsb.setSlpbBlok((String) dr.get("pp_skrot_bloku"));
				// zsb.setSlpbCzyWskazany("N");
				// arkusz.addZptSlprBloki(zsb);
				// }
			}
		} else {
			// jezeli wniosek o zwolnienie, to nie powinno byc do
			// uzupelnienia blokow
			if (arkusz.getZptSlprBlokis() != null) {
				arkusz.getZptSlprBlokis().clear();
			}
		}
	}

	private boolean validateBeforeSaveArkusz() {
		ArrayList<String> inv = new ArrayList<String>();

		if (this.arkusz.getSlprPrcId() == null || this.arkusz.getSlprPrcId() == 0L)
			inv.add("Nie ustawiono pracownika na Części I.");


//ODKOMENTOWAĆ PO TESTACH
//		if (this.arkusz.getSlprCzyZwol() == null || this.arkusz.getSlprCzyZwol().trim().isEmpty())
//			inv.add("Czy wniosek o zwolnienie pracownika z obowiązku odbywania służby przygotowawczej jest polem wymaganym.");
//
//		if (wniosekZwol) {
//			if (this.arkusz.getSlprUzasZwol() == null || this.arkusz.getSlprUzasZwol().trim().isEmpty()) {
//				inv.add("Uzasadnienie wniosku o zwolnienie pracownika z obowiązku odbywania służby przygotowawczej jest polem wymaganym.");
//			}
//		}
//		
//		
//
////		ustawZwolnienie();
//		if (this.arkusz.getSlprDecZwol() == null || this.arkusz.getSlprDecZwol().trim().isEmpty())
//			inv.add("Czy zwolniono ze służby przygotowawczej jest polem wymaganym.");
//
//		if (!wniosekZwolZeSluzbyPrzyg) {
//
//			if (this.arkusz.getSlprDataOd() == null || this.arkusz.getSlprDataOd().getTime() == 0L)
//				inv.add("Termin rozpoczęcia służby jest polem wymaganym.");
//
//			if (this.arkusz.getSlprDataDo() == null || this.arkusz.getSlprDataDo().getTime() == 0L)
//				inv.add("Termin zakończenia służby jest polem wymaganym.");
//			
//			if (arkusz.getSlprDataDo()!=null && arkusz.getSlprDataDo()!=null &&  arkusz.getSlprDataDo().before(arkusz.getSlprDataOd()))
//				inv.add("Termin rozpoczęcia służby powinien być mniejszy niż termin zakończenia służby");
//
//			if (this.arkusz.getSlprPrzygTeor() == null || this.arkusz.getSlprPrzygTeor().trim().isEmpty())
//				inv.add("Przygotowanie teoretyczne jest polem wymaganym.");
//
//		}
		
//		Warunki na pole Wyniku
		
//		if (this.arkusz.getSlprOpTeor() == null || this.arkusz.getSlprOpTeor().trim().isEmpty())
//			inv.add("Opinia dotycząca przebiegu jest polem wymaganym.");
//
//		if (this.arkusz.getSlprOcena() == null || this.arkusz.getSlprOcena().trim().isEmpty())
//			inv.add("Ocena jest polem wymaganym.");
//
//		if (this.arkusz.getSlprEgzamin() == null || this.arkusz.getSlprEgzamin().trim().isEmpty())
//			inv.add("Egzamin jest polem wymaganym.");

		for (String msg : inv) {
			User.alert(msg);
		}
		return inv.isEmpty();

	}
	
//	private String getCheckString_CzI() {
//		
//		String ret = AppBean.concatValues(
//				arkusz.getSlprPrcId()
//		);
//
//		return ret;
//	}
//	
//	private String getCheckString_CzII() {
//		
//		String ret = AppBean.concatValues(
//				arkusz.getSlprCzyZwol(),
//				arkusz.getSlprPoziomPrzy(),
//				arkusz.getSlprUzasZwol()
//		);
//		
//		
//		if(!init){
//		przypiszBloki();
//		}
//
//		if(arkusz.getZptSlprBlokis() != null){
//		for ( ZptSlprBloki b: arkusz.getZptSlprBlokis()){
//			ret += b.getSlpbBlok() + b.getSlpbCzyWskazany();
//		}}	
//
//		return ret;
//	}
//	
//	private String getCheckString_CzIII() {
//		String ret = AppBean.concatValues(
//				arkusz.getSlprDecZwol(),
//				arkusz.getSlprPrzygTeor(),
//				arkusz.getSlprDataOd(),
//				arkusz.getSlprDataDo()
//		);
//
//		return ret;
//	}
//	
//	private String getCheckString_CzV() {
//		String ret = AppBean.concatValues(
//				arkusz.getSlprOpTeor(),
//				arkusz.getSlprOcena()
//		);
//
//		return ret;
//	}
//	
//	private String getCheckString_CzVI() {
//		String ret = AppBean.concatValues(
//				arkusz.getSlprEgzamin(),
//				arkusz.getSlprEgzaminKoncowy()
//		);
//
//		return ret;
//	}
	
	private Boolean validatePermissions() {
		ArrayList<String> inval = new ArrayList<String>();
		
		//test czy są zmiany
//		String currentCheckString_CzI = this.getCheckString_CzI();
//		String currentCheckString_CzII = this.getCheckString_CzII();
//		String currentCheckString_CzIII = this.getCheckString_CzIII();
//		String currentCheckString_CzV = this.getCheckString_CzV();
//		String currentCheckString_CzVI = this.getCheckString_CzVI();
		
		
//		if (	   this.checkCzI.equals( currentCheckString_CzI) 
//				&& this.checkCzII.equals( currentCheckString_CzII)
//				&& this.checkCzIII.equals( currentCheckString_CzIII)
//				&& this.checkCzV.equals( currentCheckString_CzV)
//				&& this.checkCzVI.equals( currentCheckString_CzVI))
//			inval.add("Brak zmian do zapisania.");
		
		/*
		if (!user.allowCreateOcenaOkresowa() && this.selectedOecenaId == null)
			inval.add("Nieautoryzowana próba utworzenia nowej oceny.");

		if (!this.allowUpdateCzI() && !this.checkCzI.equals( currentCheckString_CzI ))
			inval.add("Nieautoryzowana próba modyfikacji zawartości części I oceny.");
	
		if (!this.allowUpdateCzII() && !this.checkCzII.equals( currentCheckString_CzII ))
			inval.add("Nieautoryzowana próba modyfikacji zawartości części II oceny.");*/
	
		
		for (String txt : inval) {
			User.alert(txt);
		}
		return inval.isEmpty();
	}
	

	public String zaznaczBloki() {
		HashSet<String> sluzbaBlokiKeySet = this.getSluzbaBlokiKeySet();
		SqlDataSelectionsHandler blokiTematyczneRows = getLW("PPL_SLP_BLOKI_TEMAT");
		for (DataRow dr : blokiTematyczneRows.getData()) {
			if (sluzbaBlokiKeySet.contains(dr.get("pp_skrot_bloku"))) {
				
				blokiTematyczneRows.setRowSelection(true, dr);
				
			}
		}
		return "";
	}

	private HashSet<String> getSluzbaBlokiKeySet() {
		HashSet<String> zptSpKeySet = new HashSet<String>();
		if (arkusz.getZptSlprBlokis() != null) {
			for (ZptSlprBloki k : arkusz.getZptSlprBlokis()) {
				zptSpKeySet.add(k.getSlpbBlok());
			}
		}
		return zptSpKeySet;
	}


	public void usunArkusz() {



		try (Connection conn = JdbcUtils.getConnection(false)) {
			obieg.usunDaneObiegu(); 

		} catch (SQLException e) {
			e.printStackTrace();
			User.alert("Nieudane usuniecie danych obiegu");
		}
		
		try (HibernateContext h = new HibernateContext()) {
			
			Session s = h.getSession();

			s.delete(this.arkusz); // << usuniecie rekordu
			s.beginTransaction().commit();
			
			User.info("Usunięto arkusz służby przygotowawczej");

			FacesContext.getCurrentInstance().getExternalContext().redirect("ListaArkuszy");
		} catch (Exception e) {
			User.alert("Nieudane usunięcie arkusza służby przygotowawczej!\n" + e);
			System.err.println(e);
		}

	}

//	public void ustawZwolnienie() {
//		if (wniosekZwolZeSluzbyPrzyg) {
//			arkusz.setSlprDecZwol("T");
//		} else {
//			arkusz.setSlprDecZwol("N");
//		}
//	}

	public void ustawWniosekZwolnienie() {
		if (wniosekZwol) {
			arkusz.setSlprCzyZwol("T");
		} else {
			arkusz.setSlprCzyZwol("N");
		}
	}

	public long getPrcId() {
		return prcId;
	}

	public void setPrcId(long prcId) {
		this.prcId = prcId;
	}

	public boolean isWniosekZwol() {

		return wniosekZwol;
	}

	public void setWniosekZwol(boolean wniosekZwol) {

		this.wniosekZwol = wniosekZwol;
	}

	public DataRow getArkuszDR() {
		return arkuszDR;
	}

	public void setArkuszDR(DataRow arkuszDR) {
		this.arkuszDR = arkuszDR;
	}

	public ZptSluzbaPrzyg getArkusz() {
		return arkusz;
	}

	public void setArkusz(ZptSluzbaPrzyg arkusz) {
		this.arkusz = arkusz;
	}

	public boolean isWniosekZwolZeSluzbyPrzyg() {
		return wniosekZwolZeSluzbyPrzyg;
	}

	public void setWniosekZwolZeSluzbyPrzyg(boolean wniosekZwolZeSluzbyPrzyg) {
		this.wniosekZwolZeSluzbyPrzyg = wniosekZwolZeSluzbyPrzyg;
	}

	public boolean isCzyNowy() {
		return czyNowy;
	}

	public void setCzyNowy(boolean czyNowy) {
		this.czyNowy = czyNowy;
	}

}
