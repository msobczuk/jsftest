package com.comarch.egeria.pp.sluzbaprzygotowawcza.domain;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_SLP_SLUZBA_PRZYG database table.
 * 
 */
@Entity
@Table(name="PPT_SLP_SLUZBA_PRZYG", schema="PPADM")
@NamedQuery(name="ZptSluzbaPrzyg.findAll", query="SELECT z FROM ZptSluzbaPrzyg z")
public class ZptSluzbaPrzyg implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SLP_SLUZBA_PRZYG",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SLP_SLUZBA_PRZYG", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SLP_SLUZBA_PRZYG")		@Column(name="SLPR_ID")
	private Long slprId = 0l;

	@Temporal(TemporalType.DATE)
	@Column(name="SLPR_AUDYT_DM")
	private Date slprAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="SLPR_AUDYT_DT")
	private Date slprAudytDt;

	@Column(name="SLPR_AUDYT_KM")
	private String slprAudytKm;

	@Column(name="SLPR_AUDYT_LM")
	private String slprAudytLm;

	@Column(name="SLPR_AUDYT_UM")
	private String slprAudytUm;

	@Column(name="SLPR_AUDYT_UT")
	private String slprAudytUt;

	@Column(name="SLPR_CZY_ZWOL")
	private String slprCzyZwol;

	@Temporal(TemporalType.DATE)
	@Column(name="SLPR_DATA_DO")
	private Date slprDataDo;

	@Temporal(TemporalType.DATE)
	@Column(name="SLPR_DATA_OD")
	private Date slprDataOd;

	@Column(name="SLPR_DEC_ZWOL")
	private String slprDecZwol;

	@Column(name="SLPR_DEC_ZWOL_OPIS")
	private String slprDecZwolOpis;

	@Column(name="SLPR_EGZAMIN")
	private String slprEgzamin;

	@Column(name="SLPR_OCENA")
	private String slprOcena;

	@Column(name="SLPR_OP_TEOR")
	private String slprOpTeor;

	@Column(name="SLPR_POZIOM_PRZY")
	private String slprPoziomPrzy;

	@Column(name="SLPR_PRC_ID")
	private Long slprPrcId;

	@Column(name="SLPR_PRZYG_TEOR")
	private String slprPrzygTeor;

	@Column(name="SLPR_UZAS_ZWOL")
	private String slprUzasZwol;

	@Column(name="SLPR_EGZAMIN_KONCOWY")
	private String slprEgzaminKoncowy;
	
//	@Column(name="SLPR_OP_KIEROWNIK")
//	private String slprOpKierownik;
//	
//	@Column(name="SLPR_OCENA_KIEROWNIK")
//	private String slprOcenaKierownik;

	//bi-directional many-to-one association to ZptSlprBloki
	@OneToMany(mappedBy="zptSluzbaPrzyg", cascade={CascadeType.ALL},orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ZptSlprBloki> zptSlprBlokis = new ArrayList<>();

	public ZptSluzbaPrzyg() {
	}

	public Long getSlprId() {
		return this.slprId;
	}

	public void setSlprId(Long slprId) {
		this.slprId = slprId;
	}

	public Date getSlprAudytDm() {
		return this.slprAudytDm;
	}

	public void setSlprAudytDm(Date slprAudytDm) {
		this.slprAudytDm = slprAudytDm;
	}

	public Date getSlprAudytDt() {
		return this.slprAudytDt;
	}

	public void setSlprAudytDt(Date slprAudytDt) {
		this.slprAudytDt = slprAudytDt;
	}

	public String getSlprAudytKm() {
		return this.slprAudytKm;
	}

	public void setSlprAudytKm(String slprAudytKm) {
		this.slprAudytKm = slprAudytKm;
	}

	public String getSlprAudytLm() {
		return this.slprAudytLm;
	}

	public void setSlprAudytLm(String slprAudytLm) {
		this.slprAudytLm = slprAudytLm;
	}

	public String getSlprAudytUm() {
		return this.slprAudytUm;
	}

	public void setSlprAudytUm(String slprAudytUm) {
		this.slprAudytUm = slprAudytUm;
	}

	public String getSlprAudytUt() {
		return this.slprAudytUt;
	}

	public void setSlprAudytUt(String slprAudytUt) {
		this.slprAudytUt = slprAudytUt;
	}

	public String getSlprCzyZwol() {
		return this.slprCzyZwol;
	}

	public void setSlprCzyZwol(String slprCzyZwol) {
		this.slprCzyZwol = slprCzyZwol;
	}

	public Date getSlprDataDo() {
		return this.slprDataDo;
	}

	public void setSlprDataDo(Date slprDataDo) {
		this.slprDataDo = slprDataDo;
	}

	public Date getSlprDataOd() {
		return this.slprDataOd;
	}

	public void setSlprDataOd(Date slprDataOd) {
		this.slprDataOd = slprDataOd;
	}

	public String getSlprDecZwol() {
		return this.slprDecZwol;
	}

	public void setSlprDecZwol(String slprDecZwol) {
		this.slprDecZwol = slprDecZwol;
	}

	public String getSlprEgzamin() {
		return this.slprEgzamin;
	}

	public void setSlprEgzamin(String slprEgzamin) {
		this.slprEgzamin = slprEgzamin;
	}

	public String getSlprOcena() {
		return this.slprOcena;
	}

	public void setSlprOcena(String slprOcena) {
		this.slprOcena = slprOcena;
	}

	public String getSlprOpTeor() {
		return this.slprOpTeor;
	}

	public void setSlprOpTeor(String slprOpTeor) {
		this.slprOpTeor = slprOpTeor;
	}

	public String getSlprPoziomPrzy() {
		return this.slprPoziomPrzy;
	}

	public void setSlprPoziomPrzy(String slprPoziomPrzy) {
		this.slprPoziomPrzy = slprPoziomPrzy;
	}

	public Long getSlprPrcId() {
		return this.slprPrcId;
	}

	public void setSlprPrcId(Long slprPrcId) {
		this.slprPrcId = slprPrcId;
	}

	public String getSlprPrzygTeor() {
		return this.slprPrzygTeor;
	}

	public void setSlprPrzygTeor(String slprPrzygTeor) {
		this.slprPrzygTeor = slprPrzygTeor;
	}

	public String getSlprUzasZwol() {
		return this.slprUzasZwol;
	}

	public void setSlprUzasZwol(String slprUzasZwol) {
		this.slprUzasZwol = slprUzasZwol;
	}

	public String getSlprEgzaminKoncowy() {
		return slprEgzaminKoncowy;
	}

	public void setSlprEgzaminKoncowy(String slprEgzaminKoncowy) {
		this.slprEgzaminKoncowy = slprEgzaminKoncowy;
	}

	public List<ZptSlprBloki> getZptSlprBlokis() {
		return this.zptSlprBlokis;
	}

	public void setZptSlprBlokis(List<ZptSlprBloki> zptSlprBlokis) {
		this.zptSlprBlokis = zptSlprBlokis;
	}




	public String getSlprDecZwolOpis() {
		return slprDecZwolOpis;
	}

	public void setSlprDecZwolOpis(String slprDecZwolOpis) {
		this.slprDecZwolOpis = slprDecZwolOpis;
	}

	public ZptSlprBloki addZptSlprBloki(ZptSlprBloki zptSlprBloki) {
		getZptSlprBlokis().add(zptSlprBloki);
		zptSlprBloki.setZptSluzbaPrzyg(this);

		return zptSlprBloki;
	}

	public ZptSlprBloki removeZptSlprBloki(ZptSlprBloki zptSlprBloki) {
		getZptSlprBlokis().remove(zptSlprBloki);
		zptSlprBloki.setZptSluzbaPrzyg(null);

		return zptSlprBloki;
	}
//	public String getSlprOpKierownik() {
//		return slprOpKierownik;
//	}
//
//	public void setSlprOpKierownik(String slprOpKierownik) {
//		this.slprOpKierownik = slprOpKierownik;
//	}
//	public String getSlprOcenaKierownik() {
//		return slprOcenaKierownik;
//	}
//
//	public void setSlprOcenaKierownik(String slprOcenaKierownik) {
//		this.slprOcenaKierownik = slprOcenaKierownik;
//	}


}