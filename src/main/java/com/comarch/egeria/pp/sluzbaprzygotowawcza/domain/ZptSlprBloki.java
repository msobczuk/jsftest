package com.comarch.egeria.pp.sluzbaprzygotowawcza.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_SLP_BLOKI database table.
 * 
 */
@Entity
@Table(name="PPT_SLP_BLOKI", schema="PPADM")
@NamedQuery(name="ZptSlprBloki.findAll", query="SELECT z FROM ZptSlprBloki z")
public class ZptSlprBloki implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SLP_BLOKI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SLP_BLOKI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SLP_BLOKI")
	@Column(name="SLPB_ID")
	private long slpbId;

	@Temporal(TemporalType.DATE)
	@Column(name="SLPB_AUDYT_DM")
	private Date slpbAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="SLPB_AUDYT_DT")
	private Date slpbAudytDt;

	@Column(name="SLPB_AUDYT_KM")
	private String slpbAudytKm;

	@Column(name="SLPB_AUDYT_LM")
	private String slpbAudytLm;

	@Column(name="SLPB_AUDYT_UM")
	private String slpbAudytUm;

	@Column(name="SLPB_AUDYT_UT")
	private String slpbAudytUt;

	@Column(name="SLPB_BLOK")
	private String slpbBlok;

	@Column(name="SLPB_CZY_WSKAZANY")
	private String slpbCzyWskazany;

	//bi-directional many-to-one association to ZptSluzbaPrzyg
	@ManyToOne
	@JoinColumn(name="SLPB_SLPR_ID")
	private ZptSluzbaPrzyg zptSluzbaPrzyg;

	public ZptSlprBloki() {
	}

	public long getSlpbId() {
		return this.slpbId;
	}

	public void setSlpbId(long slpbId) {
		this.slpbId = slpbId;
	}

	public Date getSlpbAudytDm() {
		return this.slpbAudytDm;
	}

	public void setSlpbAudytDm(Date slpbAudytDm) {
		this.slpbAudytDm = slpbAudytDm;
	}

	public Date getSlpbAudytDt() {
		return this.slpbAudytDt;
	}

	public void setSlpbAudytDt(Date slpbAudytDt) {
		this.slpbAudytDt = slpbAudytDt;
	}

	public String getSlpbAudytKm() {
		return this.slpbAudytKm;
	}

	public void setSlpbAudytKm(String slpbAudytKm) {
		this.slpbAudytKm = slpbAudytKm;
	}

	public String getSlpbAudytLm() {
		return this.slpbAudytLm;
	}

	public void setSlpbAudytLm(String slpbAudytLm) {
		this.slpbAudytLm = slpbAudytLm;
	}

	public String getSlpbAudytUm() {
		return this.slpbAudytUm;
	}

	public void setSlpbAudytUm(String slpbAudytUm) {
		this.slpbAudytUm = slpbAudytUm;
	}

	public String getSlpbAudytUt() {
		return this.slpbAudytUt;
	}

	public void setSlpbAudytUt(String slpbAudytUt) {
		this.slpbAudytUt = slpbAudytUt;
	}

	public String getSlpbBlok() {
		return this.slpbBlok;
	}

	public void setSlpbBlok(String slpbBlok) {
		this.slpbBlok = slpbBlok;
	}

	public String getSlpbCzyWskazany() {
		return this.slpbCzyWskazany;
	}

	public void setSlpbCzyWskazany(String slpbCzyWskazany) {
		this.slpbCzyWskazany = slpbCzyWskazany;
	}

	public ZptSluzbaPrzyg getZptSluzbaPrzyg() {
		return this.zptSluzbaPrzyg;
	}

	public void setZptSluzbaPrzyg(ZptSluzbaPrzyg zptSluzbaPrzyg) {
		this.zptSluzbaPrzyg = zptSluzbaPrzyg;
	}

}