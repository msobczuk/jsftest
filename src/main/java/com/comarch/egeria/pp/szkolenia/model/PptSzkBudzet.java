package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.*;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;


/**
 * The persistent class for the PPT_SZK_BUDZET database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_BUDZET", schema="PPADM")
@NamedQuery(name="PptSzkBudzet.findAll", query="SELECT p FROM PptSzkBudzet p")
public class PptSzkBudzet implements Serializable {
	private static final long serialVersionUID = 1L;


	
	
	public Double sumaPozycjePlan(){
		return  this.pptSzkBudzetPozycjes.stream().mapToDouble(p->p.getBspPlan()).sum();
	}
	

	public Double sumaPozycjePlan(SqlDataSelectionsHandler lw){
		List<Long> pkList = lw.getData().stream().map(r->r.getIdAsLong()).collect(Collectors.toList());
		
		return  this.pptSzkBudzetPozycjes.stream()
				.filter(p-> pkList.contains(p.getBspId()) )
				.mapToDouble(p->p.getBspPlan()).sum();
	}

	
	
	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_BUDZET",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_BUDZET", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_BUDZET")
	@Column(name="BSZ_ID")
	private long bszId;
	
	
	@Column(name="BSZ_ROK")
	private Integer bszRok; // = LocalDate.now().getYear()+1;

	@Temporal(TemporalType.DATE)
	@Column(name="BSZ_DATA_DO")
	private Date bszDataDo; // = SqlBean.dateSerial(bszRok, 12, 31);


	@Temporal(TemporalType.DATE)
	@Column(name="BSZ_DATA_OD")
	private Date bszDataOd; // = SqlBean.dateSerial(bszRok, 1, 1);

	@Column(name="BSZ_PLAN")
	private double bszPlan;



	//bi-directional many-to-one association to PptSzkBudzetPozycje
	@OneToMany(mappedBy="pptSzkBudzet",  cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<PptSzkBudzetPozycje> pptSzkBudzetPozycjes = new ArrayList<>();

	public PptSzkBudzet() {
		this.setBszRok(LocalDate.now().getYear()+1);
	}

	public long getBszId() {
		return this.bszId;
	}

	public void setBszId(long bszId) {
		this.bszId = bszId;
	}

	public Date getBszDataDo() {
		return this.bszDataDo;
	}

	public void setBszDataDo(Date bszDataDo) {
		this.bszDataDo = bszDataDo;
	}

	public Date getBszDataOd() {
		return this.bszDataOd;
	}

	public void setBszDataOd(Date bszDataOd) {
		this.bszDataOd = bszDataOd;
	}

	public double getBszPlan() {
		return this.bszPlan;
	}

	public void setBszPlan(double bszPlan) {
		this.bszPlan = bszPlan;
	}

	public Integer getBszRok() {
		return this.bszRok;
	}

	public void setBszRok(Integer bszRok) {
		this.bszRok = bszRok;
		this.setBszDataOd(Utils.dateSerial(bszRok, 1, 1));
		this.setBszDataDo(Utils.dateSerial(bszRok, 12, 31));
	}
	
	public List<BigDecimal> getPptSzkBudzetPozycjesBspObIdList() {
		return this.pptSzkBudzetPozycjes.stream().map(p->p.getBspObId()).collect(Collectors.toList()) ;
	}

	public List<PptSzkBudzetPozycje> getPptSzkBudzetPozycjes() {
		return this.pptSzkBudzetPozycjes;
	}

	public void setPptSzkBudzetPozycjes(List<PptSzkBudzetPozycje> pptSzkBudzetPozycjes) {
		this.pptSzkBudzetPozycjes = pptSzkBudzetPozycjes;
	}

	public PptSzkBudzetPozycje addPptSzkBudzetPozycje(PptSzkBudzetPozycje pptSzkBudzetPozycje) {
		getPptSzkBudzetPozycjes().add(pptSzkBudzetPozycje);
		pptSzkBudzetPozycje.setPptSzkBudzet(this);

		return pptSzkBudzetPozycje;
	}

	public PptSzkBudzetPozycje removePptSzkBudzetPozycje(PptSzkBudzetPozycje pptSzkBudzetPozycje) {
		getPptSzkBudzetPozycjes().remove(pptSzkBudzetPozycje);
		pptSzkBudzetPozycje.setPptSzkBudzet(null);

		return pptSzkBudzetPozycje;
	}

}