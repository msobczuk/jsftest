package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.LazyToOne;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_SZK_UCZESTNICY_SZKOLEN database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_UCZESTNICY_SZKOLEN", schema="PPADM")
@NamedQuery(name="PptUczestnicySzkolen.findAll", query="SELECT p FROM PptUczestnicySzkolen p")
public class PptUczestnicySzkolen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_UCZESTNICY_SZKOLEN",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_UCZESTNICY_SZKOLEN", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_UCZESTNICY_SZKOLEN")
	@Column(name="USZ_ID")
	private Long uszId;

	@Temporal(TemporalType.DATE)
	@Column(name="USZ_AUDYT_DM")
	private Date uszAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="USZ_AUDYT_DT")
	private Date uszAudytDt;

	@Column(name="USZ_AUDYT_KM")
	private String uszAudytKm;

	@Column(name="USZ_AUDYT_LM")
	private String uszAudytLm;

	@Column(name="USZ_AUDYT_UM")
	private String uszAudytUm;

	@Column(name="USZ_AUDYT_UT")
	private String uszAudytUt;

	@Column(name="USZ_CZY_ZALICZYL")
	private String uszCzyZaliczyl;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USZ_DATA_UMOWY")
	private Date uszDataUmowy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USZ_DATA_WYPELNIENIA")
	private Date uszDataWypelnienia;

//	@Column(name="USZ_SZK_ID")
//	private Long uszSzkId;

	@Column(name="USZ_KWOTA_ZOBOWIAZANIA")
	private BigDecimal uszKwotaZobowiazania;
	
	@Column(name="USZ_OBECNOSC")
	private String uszObecnosc;

	@Column(name="USZ_PRC_ID")
	private Long uszPrcId;
	
	@Column(name="USZ_REZ")
	private String uszRez;
	
	@Column(name="USZ_ZGLOSZ")
	private String uszZglosz;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USZ_ZOBOWIAZANIE_OD")
	private Date uszZobowiazanieOd;
	
	@Temporal(TemporalType.DATE)
	@Column(name="USZ_ZOBOWIAZANIE_DO")
	private Date uszZobowiazanieDo;

	@Column(name="USZ_KOSZT_PLAN")
	private Double uszKosztPlan;
	
	@Column(name="USZ_KOSZT_RZECZ")
	private Double uszKosztRzecz;
	
	//bi-directional many-to-one association to PptSzkolenia
	@ManyToOne
	@JoinColumn(name="USZ_SZK_ID")
	@LazyCollection(LazyCollectionOption.FALSE)
	private PptSzkSzkolenia pptSzkolenia;

	//bi-directional many-to-one association to OstAnkietyPozycje
	@OneToMany(mappedBy="pptUczestnicySzkolen", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<PptSzkAnkietyPozycje> pptAnkietyPozycjes = new ArrayList<>();
	
	public PptUczestnicySzkolen() {
	}

	public Long getUszId() {
		return this.uszId;
	}

	public void setUszId(Long uszId) {
		this.uszId = uszId;
	}

	public Date getUszAudytDm() {
		return this.uszAudytDm;
	}

	public void setUszAudytDm(Date uszAudytDm) {
		this.uszAudytDm = uszAudytDm;
	}

	public Date getUszAudytDt() {
		return this.uszAudytDt;
	}

	public void setUszAudytDt(Date uszAudytDt) {
		this.uszAudytDt = uszAudytDt;
	}

	public String getUszAudytKm() {
		return this.uszAudytKm;
	}

	public void setUszAudytKm(String uszAudytKm) {
		this.uszAudytKm = uszAudytKm;
	}

	public String getUszAudytLm() {
		return this.uszAudytLm;
	}

	public void setUszAudytLm(String uszAudytLm) {
		this.uszAudytLm = uszAudytLm;
	}

	public String getUszAudytUm() {
		return this.uszAudytUm;
	}

	public void setUszAudytUm(String uszAudytUm) {
		this.uszAudytUm = uszAudytUm;
	}

	public String getUszAudytUt() {
		return this.uszAudytUt;
	}

	public void setUszAudytUt(String uszAudytUt) {
		this.uszAudytUt = uszAudytUt;
	}
	
//	public Long getUszEkSzkId() {
//		return uszEkSzkId;
//	}
//
//	public void setUszEkSzkId(Long uszEkSzkId) {
//		this.uszEkSzkId = uszEkSzkId;
//	}
	
	public Long getUszPrcId() {
		return uszPrcId;
	}

	public void setUszPrcId(Long uszPrcId) {
		this.uszPrcId = uszPrcId;
	}

	public String getUszRez() {
		return this.uszRez;
	}

	public void setUszRez(String uszRez) {
		this.uszRez = uszRez;
		if (this.pptSzkolenia!=null)
			this.pptSzkolenia.recalcProporcjoanlnieKoszty();
	}
	
	public String getUszObecnosc() {
		return uszObecnosc;
	}

	public void setUszObecnosc(String uszObecnosc) {
		this.uszObecnosc = uszObecnosc;
	}

	public String getUszZglosz() {
		return this.uszZglosz;
	}

	public void setUszZglosz(String uszZglosz) {
		this.uszZglosz = uszZglosz;
	}

	public String getUszCzyZaliczyl() {
		return uszCzyZaliczyl;
	}

	public void setUszCzyZaliczyl(String uszCzyZaliczyl) {
		this.uszCzyZaliczyl = uszCzyZaliczyl;
	}

	public Date getUszDataUmowy() {
		return uszDataUmowy;
	}

	public void setUszDataUmowy(Date uszDataUmowy) {
		this.uszDataUmowy = uszDataUmowy;
	}

	public Date getUszDataWypelnienia() {
		return uszDataWypelnienia;
	}

	public void setUszDataWypelnienia(Date uszDataWypelnienia) {
		this.uszDataWypelnienia = uszDataWypelnienia;
	}

	public BigDecimal getUszKwotaZobowiazania() {
		return uszKwotaZobowiazania;
	}

	public void setUszKwotaZobowiazania(BigDecimal uszKwotaZobowiazania) {
		this.uszKwotaZobowiazania = uszKwotaZobowiazania;
	}

	public Date getUszZobowiazanieOd() {
		return uszZobowiazanieOd;
	}

	public void setUszZobowiazanieOd(Date uszZobowiazanieOd) {
		this.uszZobowiazanieOd = uszZobowiazanieOd;
	}

	public Date getUszZobowiazanieDo() {
		return uszZobowiazanieDo;
	}

	public void setUszZobowiazanieDo(Date uszZobowiazanieDo) {
		this.uszZobowiazanieDo = uszZobowiazanieDo;
	}
	
	public PptSzkSzkolenia getPptSzkolenia() {
		return this.pptSzkolenia;
	}

	public void setPptSzkolenia(PptSzkSzkolenia pptSzkolenia) {
		this.pptSzkolenia = pptSzkolenia;
	}
	
	public List<PptSzkAnkietyPozycje> getPptAnkietyPozycjes() {
		return this.pptAnkietyPozycjes;
	}

	public void setPptAnkietyPozycjes(List<PptSzkAnkietyPozycje> pptAnkietyPozycjes) {
		this.pptAnkietyPozycjes = pptAnkietyPozycjes;
	}

	public PptSzkAnkietyPozycje addPptAnkietyPozycje(PptSzkAnkietyPozycje pptAnkietyPozycje) {
		getPptAnkietyPozycjes().add(pptAnkietyPozycje);
		pptAnkietyPozycje.setPptUczestnicySzkolen(this);

		return pptAnkietyPozycje;
	}

	public PptSzkAnkietyPozycje removePptAnkietyPozycje(PptSzkAnkietyPozycje pptAnkietyPozycje) {
		getPptAnkietyPozycjes().remove(pptAnkietyPozycje);
		pptAnkietyPozycje.setPptUczestnicySzkolen(null);

		return pptAnkietyPozycje;
	}

	public Double getUszKosztPlan() {
		return uszKosztPlan;
	}

	public void setUszKosztPlan(Double uszKosztPlan) {
		this.uszKosztPlan = uszKosztPlan;
	}

	public Double getUszKosztRzecz() {
		return uszKosztRzecz;
	}

	public void setUszKosztRzecz(Double uszKosztRzecz) {
		this.uszKosztRzecz = uszKosztRzecz;
	}


}