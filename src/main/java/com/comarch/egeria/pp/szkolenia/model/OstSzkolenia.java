package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the OST_SZKOLENIA database table.
 * 
 */
@Entity
@Table(name="OST_SZKOLENIA", schema="EGADM1")
@NamedQuery(name="OstSzkolenia.findAll", query="SELECT o FROM OstSzkolenia o")
public class OstSzkolenia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_OST_SZKOLENIA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "OST_SZKOLENIA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_OST_SZKOLENIA")
	@Column(name="SZK_ID")
	private long szkId;

	@Column(name="SZK_ADRES")
	private String szkAdres;

	@Temporal(TemporalType.DATE)
	@Column(name="SZK_AUDYT_DM")
	private Date szkAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="SZK_AUDYT_DT")
	private Date szkAudytDt;

	@Column(name="SZK_AUDYT_KM")
	private String szkAudytKm;

	@Column(name="SZK_AUDYT_KT")
	private String szkAudytKt;

	@Column(name="SZK_AUDYT_LM")
	private BigDecimal szkAudytLm;

	@Column(name="SZK_AUDYT_UM")
	private String szkAudytUm;

	@Column(name="SZK_AUDYT_UT")
	private String szkAudytUt;

	@Temporal(TemporalType.DATE)
	@Column(name="SZK_DATA_DO")
	private Date szkDataDo;

	@Temporal(TemporalType.DATE)
	@Column(name="SZK_DATA_OD")
	private Date szkDataOd;

	@Column(name="SZK_KWOTA")
	private BigDecimal szkKwota;

	@Column(name="SZK_LOKALIZACJA")
	private String szkLokalizacja;

	@Column(name="SZK_MAX_UCZESTNIKOW")
	private Long szkMaxUczestnikow;

	@Column(name="SZK_MIN_UCZESTNIKOW")
	private Long szkMinUczestnikow;

	@Column(name="SZK_NAZWA")
	private String szkNazwa;

	@Column(name="SZK_NAZWA_OPIS")
	private String szkNazwaOpis;

	@Column(name="SZK_NR_UPRAWNIENIA")
	private String szkNrUprawnienia;

	@Column(name="SZK_OF_ID")
	private BigDecimal szkOfId;

	@Column(name="SZK_OPIS")
	private String szkOpis;

	@Column(name="SZK_PROWADZACY")
	private String szkProwadzacy;

	@Column(name="SZK_RODZAJ")
	private String szkRodzaj;

	@Column(name="SZK_STATUS")
	private String szkStatus;

	@Column(name="SZK_ZAKRES")
	private String szkZakres;

	//bi-directional many-to-one association to OstListySzkolen
	@OneToMany(mappedBy="ostSzkolenia")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<OstListySzkolen> ostListySzkolens = new ArrayList<>();

	public OstSzkolenia() {
	}

	public long getSzkId() {
		return this.szkId;
	}

	public void setSzkId(long szkId) {
		this.szkId = szkId;
	}

	public String getSzkAdres() {
		return this.szkAdres;
	}

	public void setSzkAdres(String szkAdres) {
		this.szkAdres = szkAdres;
	}

	public Date getSzkAudytDm() {
		return this.szkAudytDm;
	}

	public void setSzkAudytDm(Date szkAudytDm) {
		this.szkAudytDm = szkAudytDm;
	}

	public Date getSzkAudytDt() {
		return this.szkAudytDt;
	}

	public void setSzkAudytDt(Date szkAudytDt) {
		this.szkAudytDt = szkAudytDt;
	}

	public String getSzkAudytKm() {
		return this.szkAudytKm;
	}

	public void setSzkAudytKm(String szkAudytKm) {
		this.szkAudytKm = szkAudytKm;
	}

	public String getSzkAudytKt() {
		return this.szkAudytKt;
	}

	public void setSzkAudytKt(String szkAudytKt) {
		this.szkAudytKt = szkAudytKt;
	}

	public BigDecimal getSzkAudytLm() {
		return this.szkAudytLm;
	}

	public void setSzkAudytLm(BigDecimal szkAudytLm) {
		this.szkAudytLm = szkAudytLm;
	}

	public String getSzkAudytUm() {
		return this.szkAudytUm;
	}

	public void setSzkAudytUm(String szkAudytUm) {
		this.szkAudytUm = szkAudytUm;
	}

	public String getSzkAudytUt() {
		return this.szkAudytUt;
	}

	public void setSzkAudytUt(String szkAudytUt) {
		this.szkAudytUt = szkAudytUt;
	}

	public Date getSzkDataDo() {
		return this.szkDataDo;
	}

	public void setSzkDataDo(Date szkDataDo) {
		this.szkDataDo = szkDataDo;
	}

	public Date getSzkDataOd() {
		return this.szkDataOd;
	}

	public void setSzkDataOd(Date szkDataOd) {
		this.szkDataOd = szkDataOd;
	}

	public BigDecimal getSzkKwota() {
		return this.szkKwota;
	}

	public void setSzkKwota(BigDecimal szkKwota) {
		this.szkKwota = szkKwota;
	}

	public String getSzkLokalizacja() {
		return this.szkLokalizacja;
	}

	public void setSzkLokalizacja(String szkLokalizacja) {
		this.szkLokalizacja = szkLokalizacja;
	}

	public Long getSzkMaxUczestnikow() {
		return this.szkMaxUczestnikow;
	}

	public void setSzkMaxUczestnikow(Long szkMaxUczestnikow) {
		this.szkMaxUczestnikow = szkMaxUczestnikow;
	}

	public Long getSzkMinUczestnikow() {
		return this.szkMinUczestnikow;
	}

	public void setSzkMinUczestnikow(Long szkMinUczestnikow) {
		this.szkMinUczestnikow = szkMinUczestnikow;
	}

	public String getSzkNazwa() {
		return this.szkNazwa;
	}

	public void setSzkNazwa(String szkNazwa) {
		this.szkNazwa = szkNazwa;
	}

	public String getSzkNazwaOpis() {
		return this.szkNazwaOpis;
	}

	public void setSzkNazwaOpis(String szkNazwaOpis) {
		this.szkNazwaOpis = szkNazwaOpis;
	}

	public String getSzkNrUprawnienia() {
		return this.szkNrUprawnienia;
	}

	public void setSzkNrUprawnienia(String szkNrUprawnienia) {
		this.szkNrUprawnienia = szkNrUprawnienia;
	}

	public BigDecimal getSzkOfId() {
		return this.szkOfId;
	}

	public void setSzkOfId(BigDecimal szkOfId) {
		this.szkOfId = szkOfId;
	}

	public String getSzkOpis() {
		return this.szkOpis;
	}

	public void setSzkOpis(String szkOpis) {
		this.szkOpis = szkOpis;
	}

	public String getSzkProwadzacy() {
		return this.szkProwadzacy;
	}

	public void setSzkProwadzacy(String szkProwadzacy) {
		this.szkProwadzacy = szkProwadzacy;
	}

	public String getSzkRodzaj() {
		return this.szkRodzaj;
	}

	public void setSzkRodzaj(String szkRodzaj) {
		this.szkRodzaj = szkRodzaj;
	}

	public String getSzkStatus() {
		return this.szkStatus;
	}

	public void setSzkStatus(String szkStatus) {
		this.szkStatus = szkStatus;
	}

	public String getSzkZakres() {
		return this.szkZakres;
	}

	public void setSzkZakres(String szkZakres) {
		this.szkZakres = szkZakres;
	}

	public List<OstListySzkolen> getOstListySzkolens() {
		return this.ostListySzkolens;
	}

	public void setOstListySzkolens(List<OstListySzkolen> ostListySzkolens) {
		this.ostListySzkolens = ostListySzkolens;
	}

	public OstListySzkolen addOstListySzkolen(OstListySzkolen ostListySzkolen) {
		getOstListySzkolens().add(ostListySzkolen);
		ostListySzkolen.setOstSzkolenia(this);

		return ostListySzkolen;
	}

	public OstListySzkolen removeOstListySzkolen(OstListySzkolen ostListySzkolen) {
		getOstListySzkolens().remove(ostListySzkolen);
		ostListySzkolen.setOstSzkolenia(null);

		return ostListySzkolen;
	}

}