package com.comarch.egeria.pp.szkolenia;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.zrodlaFinansowania.model.ZrodlaFinansowania;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.szkolenia.model.PptSzkWnRefundacje;
import com.comarch.egeria.pp.zrodlaFinansowania.ZrodlaFinansowaniaBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

import static com.comarch.egeria.Utils.nz;

@Named
@Scope("view")
public class RefundacjaWniosekBean extends SqlBean {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	@Inject
	User user;

	@Inject
	ZrodlaFinansowaniaBean zrodlaFinansowaniaBean;


	public DataRow wniosekRow = null;
	public PptSzkWnRefundacje wniosek = null;


	@PostConstruct
	public void init() {
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		if (id != null) {
			wniosekRow = (DataRow) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("row");
		}

		if (id == null) {
			this.wniosek = new PptSzkWnRefundacje();
			this.wniosek.setWnrFCzyPokrycie("T");
			wniosek.setWnrPrcId(user.getPrcIdLong());

//			zrodlaFinansowaniaBean.addZFI();
//			zrodlaFinansowaniaBean.getZrodlaFinansowaniaLista().get(0).addNewAngaze();

 		} else {
			this.setWniosek((PptSzkWnRefundacje) HibernateContext.get(PptSzkWnRefundacje.class, wniosekRow.getIdAsLong()));
		}

		String obKod = getPrcObKod();
		SqlDataSelectionsHandler pplSzkZF = this.getLW("PPL_SZK_STAN_KOSZTOW");
		pplSzkZF.setColFilter("sk_wymiar06", obKod, true);
		pplSzkZF.invalidateClientComponents();

		SqlDataSelectionsHandler pplSzkZfWnioski = this.getLW("PPL_SZK_ZAANGAZOWANIE");
		pplSzkZfWnioski.setColFilter("ob_kod", obKod, true);
		pplSzkZfWnioski.invalidateClientComponents();

		if (wniosek.getWnrFCzyPokrycie() == null) {
			wniosek.setWnrFCzyPokrycie("T");
		}
	}

	public String getPrcObKod() {
		return this.getLW("PPL_PRCjZATljOB").findAndFormat(wniosek.getWnrPrcId(), "%ob_kod$s");
	}

	public void zapisz() {

//		if (wniosek.getWnrPrcId() == null)
//			wniosek.setWnrPrcId(user.getPrcIdLong());

		if (!validateBeforeSave()) {
			return;
		}

		if (!zrodlaFinansowaniaBean.validateBeforeSave()) {
			return;
		}

		// zapis pól dodatkowych
//		FacesContext facesContext = FacesContext.getCurrentInstance();
//		UIViewRoot root = facesContext.getViewRoot();
//		CommandButton button = (CommandButton) root.findComponent("zapisyFormId:pdef:zapiszPolaDodId");
//		ActionEvent actionEvent = new ActionEvent(button);
//		actionEvent.queue();

		// zapis pól dodatkowych - jesli inject nie dziala...
		CPoleDefiniowalne cPoleDefiniowalne = (CPoleDefiniowalne) AppBean.getViewScopeBean("cPoleDefiniowalne");
		cPoleDefiniowalne.zapiszPolaDef();

		// zapis wniosku
		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			wniosek = (PptSzkWnRefundacje) s.merge(wniosek);
			s.saveOrUpdate(wniosek);
			s.beginTransaction().commit();
			s.refresh(this.wniosek);

			zrodlaFinansowaniaBean.zapisz(this.wniosek.getWnrId());
			
			User.info("Zapisano wniosek o numerze " + wniosek.getWnrId());

		} catch (Exception e) {
			log.error(e);
			if (wniosek.getWnrId() != 0) {
				User.alert("Błąd przy zapisie wniosku o numerze " + wniosek.getWnrId());
			} else {
				User.alert("Błąd przy zapisie wniosku");
			}
		}
		
	}

	private boolean validateBeforeSave() {
		try {

			ArrayList<String> inval = new ArrayList<>();
			String invalField = "";
			String invalDate = "";


			if (ileNiewypelnionychAnkiet()>0) {
				User.warn("Nie wszystkie ankiety szkoleniowe są wypełnione.");
			}

			if (nz(wniosek.getWnrNazwa()).isEmpty()){
				inval.add("'Nazwa' jest polem wymaganym.");
			}
			if (nz(wniosek.getWnrFCzyPokrycie()).isEmpty()){
				inval.add("'Pokrycie kosztów szkolenia' jest polem wymaganym.");
			}
			if (nz(wniosek.getWnrFCzyFaktura()).isEmpty()){
				inval.add("'Czy do wniosku została dołączona faktura' jest polem wymaganym.");
			}
			if (nz(wniosek.getWnrFCzyHarmonogram()).isEmpty()){
				inval.add("'Czy do wniosku został załączony harmonogram' jest polem wymaganym.");
			}
			if (nz(wniosek.getWnrFCzyIprz()).isEmpty()){
				inval.add("'Czy ujęte w IPRZ' jest polem wymaganym.");
			}


			if (wniosek.getWnrTematOrganizator() == null || wniosek.getWnrTematOrganizator().trim().isEmpty()) {
				invalField += "Temat/organizator nie może być pusty! ";
			}

			if (wniosek.getWnrTerminRozpoczecia()!=null && wniosek.getWnrTerminZakonczenia()!=null &&  wniosek.getWnrTerminZakonczenia().before(wniosek.getWnrTerminRozpoczecia())) {
				invalField += "Termin zakończenia szkolenia nie może być wcześniejszy niż termin jego rozpoczęcia! ";
			}
			if (wniosek.getWnrFCzyPokrycie() == null || wniosek.getWnrFCzyPokrycie().trim().isEmpty()) {
				invalField += "Należy określić pokrycie kosztów szkolenia! ";
			} else {
				if (wniosek.getWnrFCzyPokrycie().equals("N") && wniosek.getWnrKwotaPokrycia() == null) {
					invalField += "Należy określić kwotę pokrycia kosztów szkolenia! ";
				}
			}

			if (wniosek.getWnrInneKoszty() != null && !wniosek.getWnrInneKoszty().trim().isEmpty()) {
				if (wniosek.getWnrKwotaInneKoszty() == null) {
					invalField += "Wskazując rodzaj innych kosztów należy podać ich kwotę! ";
				}
			}

			if (wniosek.getWnrUrlopBezplOd() != null) {
				if (wniosek.getWnrUrlopBezplDo() != null) {
					if (wniosek.getWnrUrlopBezplDo().before(wniosek.getWnrUrlopBezplOd())) {
						invalDate += "Data końca urlopu bezpłatnego nie może być wcześniejsza od daty początkowej! ";
					}
				} else {
					invalDate += "Bezpłatny urlop musi mieć koniec! ";
				}
			} else {
				if (wniosek.getWnrUrlopBezplDo() != null) {
					invalDate += "Bezpłatny urlop musi mieć początek! ";
				}
			}

			if (wniosek.getWnrUrlopSzkolOd() != null) {
				if (wniosek.getWnrUrlopSzkolDo() != null) {
					if (wniosek.getWnrUrlopSzkolDo().before(wniosek.getWnrUrlopSzkolOd())) {
						invalDate += "Data końca urlopu szkoleniowego nie może być wcześniejsza od daty początkowej! ";
					}
				} else {
					invalDate += "Urlop szkoleniowy musi mieć koniec! ";
				}
			} else {
				if (wniosek.getWnrUrlopBezplDo() != null) {
					invalDate += "Urlop szkoleniowy musi mieć początek! ";
				}
			}

			if (!invalField.isEmpty()) {
				inval.add(invalField);
			}
			if (!invalDate.isEmpty()) {
				inval.add(invalDate);
			}

			for (String msg : inval) {
				User.alert(msg);
			}

			return inval.isEmpty();

		} catch (Exception e){
			log.error(e.getMessage(), e);
			User.alert(e);
			return false; // są błędy w kodzie
		}
	}

	private int ileNiewypelnionychAnkiet() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		Number x = (Number) JdbcUtils.sqlExecScalarQuery("select count(*) from PPT_SZK_UCZESTNICY_SZKOLEN where usz_prc_id = ? and USZ_DATA_WYPELNIENIA is null", wniosek.getWnrPrcId());
		return x.intValue();
	}


	public void usun() {

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			for (ZrodlaFinansowania zf : zrodlaFinansowaniaBean.getZrodlaFinansowaniaLista()) {
				s.delete(zf);
			}
			s.delete(wniosek);
			s.beginTransaction().commit();
			User.info("Usunięto wniosek o numerze " + wniosek.getWnrId());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e);
		}

		try {
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			FacesContext.getCurrentInstance().getExternalContext().redirect("ListaWnioskowRefundacje");
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			User.alert(e);
		}
	}

	public PptSzkWnRefundacje getWniosek() {
		return wniosek;
	}

	public void setWniosek(PptSzkWnRefundacje wniosek) {
		this.wniosek = wniosek;
	}

}
