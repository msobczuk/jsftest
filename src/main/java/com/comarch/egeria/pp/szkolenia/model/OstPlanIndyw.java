package com.comarch.egeria.pp.szkolenia.model;

import static com.comarch.egeria.Utils.*;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.iprz.model.ZptIprz;
import com.comarch.egeria.web.User;


/**
 * The persistent class for the PPT_SZK_PLAN_INDYW database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_PLAN_INDYW", schema="PPADM")
@NamedQuery(name="OstPlanIndyw.findAll", query="SELECT o FROM OstPlanIndyw o")
public class OstPlanIndyw implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	
	public OstPlanIndyw clone() throws CloneNotSupportedException{
        return (OstPlanIndyw) super.clone();
    }

		
	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_PLAN_INDYW",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_PLAN_INDYW", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_PLAN_INDYW")
	@Column(name="PIS_ID")
	private long pisId;

	@Temporal(TemporalType.DATE)
	@Column(name="PIS_AUDYT_DM")
	private Date pisAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="PIS_AUDYT_DT")
	private Date pisAudytDt;

	@Column(name="PIS_AUDYT_KM")
	private String pisAudytKm;

	@Column(name="PIS_AUDYT_LM")
	private String pisAudytLm;

	@Column(name="PIS_AUDYT_UM")
	private String pisAudytUm;

	@Column(name="PIS_AUDYT_UT")
	private String pisAudytUt;

	@Temporal(TemporalType.DATE)
	@Column(name="PIS_DATA_ZAPISU")
	private Date pisDataZapisu =  new Date(System.currentTimeMillis());

	@Column(name="PIS_PRC_ID")
	private Long pisPrcId;

	@Temporal(TemporalType.DATE)
	@Column(name="PIS_ROK")
	private Date pisRok = new Date(System.currentTimeMillis());

	@Column(name="PIS_SSPE_FORMA")
	private String pisSspeForma;

	@Column(name="PIS_SSPE_OBSZAR_WIEDZY")
	private String pisSspeObszarWiedzy;

	@Column(name="PIS_SZK_POWSZECH")
	private String pisSzkPowszech;

	@Column(name="PIS_TEMAT")
	private String pisTemat;

	@Column(name="PIS_OF_ID")
	private Long pisOfId;
	
	@Column(name="PIS_ORGANIZATOR")
	private String pisOrganizator;
	
	@Column(name="PIS_MIEJSCE_SZKOL")
	private String pisMiejsceSzkol;
	
	@Temporal(TemporalType.DATE)
	@Column(name="PIS_TERMIN_OD")
	private Date pisTerminOd;

	@Temporal(TemporalType.DATE)
	@Column(name="PIS_TERMIN_DO")
	private Date pisTerminDo;
	
	@Column(name="PIS_UZASADNIENIE")
	private String pisUzasadnienie;
	
	//bi-directional many-to-one association to ZptIprz
	@ManyToOne
	@JoinColumn(name="PIS_IPRZ_ID")
	private ZptIprz zptIprz;

	//bi-directional many-to-one association to OstZapisySzkolen
	@ManyToOne
	@JoinColumn(name="PIS_ZSZ_ID")
	private OstZapisySzkolen ostZapisySzkolen;
	
	
	//bi-directional many-to-one association to PptSzkSzkolenia
	@ManyToOne
	@JoinColumn(name="PIS_SZK_ID")
	private PptSzkSzkolenia pptSzkSzkolenia;
	
	@Column(name="PIS_F_CZY_REJESTR")
	private String pisFCzyRejestr = "N";

	@Column(name="PIS_F_CZY_NIE_SC")
	private String pisFCzyNieSc = "N";
	
	@Column(name="PIS_F_CZY_IPRZ")
	private String pisFCzyIprz;
	
	@Column(name="PIS_KOSZT")
	private Double pisKoszt;
	
	public String getPisFCzyRejestr() {
		return pisFCzyRejestr;
	}

	public void setPisFCzyRejestr(String pisFCzyRejestr) {
		this.pisFCzyRejestr = pisFCzyRejestr;
	}

	public OstPlanIndyw() {
	}

	public long getPisId() {
		return this.pisId;
	}

	public void setPisId(long pisId) {
		this.pisId = pisId;
	}

	public Date getPisAudytDm() {
		return this.pisAudytDm;
	}

	public void setPisAudytDm(Date pisAudytDm) {
		this.pisAudytDm = pisAudytDm;
	}

	public Date getPisAudytDt() {
		return this.pisAudytDt;
	}

	public void setPisAudytDt(Date pisAudytDt) {
		this.pisAudytDt = pisAudytDt;
	}

	public String getPisAudytKm() {
		return this.pisAudytKm;
	}

	public void setPisAudytKm(String pisAudytKm) {
		this.pisAudytKm = pisAudytKm;
	}

	public String getPisAudytLm() {
		return this.pisAudytLm;
	}

	public void setPisAudytLm(String pisAudytLm) {
		this.pisAudytLm = pisAudytLm;
	}

	public String getPisAudytUm() {
		return this.pisAudytUm;
	}

	public void setPisAudytUm(String pisAudytUm) {
		this.pisAudytUm = pisAudytUm;
	}

	public String getPisAudytUt() {
		return this.pisAudytUt;
	}

	public void setPisAudytUt(String pisAudytUt) {
		this.pisAudytUt = pisAudytUt;
	}

	public Date getPisDataZapisu() {
		return this.pisDataZapisu;
	}

	public void setPisDataZapisu(Date pisDataZapisu) {
		this.pisDataZapisu = pisDataZapisu;
	}

	public Long getPisPrcId() {
		return this.pisPrcId;
	}

	public void setPisPrcId(Long pisPrcId) {
		this.pisPrcId = pisPrcId;
	}

	public Date getPisRok() {
		return this.pisRok;
	}

	public void setPisRok(Date pisRok) {
		this.pisRok = pisRok;
	}

	public String getPisSspeForma() {
		return this.pisSspeForma;
	}

	public void setPisSspeForma(String pisSspeForma) {
		this.pisSspeForma = pisSspeForma;
	}

	public String getPisSspeObszarWiedzy() {
		return this.pisSspeObszarWiedzy;
	}

	public void setPisSspeObszarWiedzy(String pisSspeObszarWiedzy) {
		this.pisSspeObszarWiedzy = pisSspeObszarWiedzy;
	}

	public String getPisSzkPowszech() {
		return this.pisSzkPowszech;
	}

	public void setPisSzkPowszech(String pisSzkPowszech) {
		this.pisSzkPowszech = pisSzkPowszech;
	}

	public String getPisTemat() {
		return this.pisTemat;
	}

	public void setPisTemat(String pisTemat) {
		this.pisTemat = pisTemat;
	}

	public Long getPisOfId() {
		return pisOfId;
	}

	public void setPisOfId(Long pisOfId) {
		boolean b = !nz(this.pisOfId).equals(nz(pisOfId));
//		System.out.println(this.pisOfId + "->" + pisOfId);
		this.pisOfId = pisOfId;
		
		if (b && this.getZptIprz()!=null){
			for (String errMsg : this.getZptIprz().validationOstPlanIndywsErrors()) {
				User.warn(errMsg);
			}
		}
	}

	public String getPisOrganizator() {
		return pisOrganizator;
	}

	public void setPisOrganizator(String pisOrganizator) {
		this.pisOrganizator = pisOrganizator;
	}

	public String getPisMiejsceSzkol() {
		return pisMiejsceSzkol;
	}

	public void setPisMiejsceSzkol(String pisMiejsceSzkol) {
		this.pisMiejsceSzkol = pisMiejsceSzkol;
	}

	public Date getPisTerminOd() {
		return pisTerminOd;
	}

	public void setPisTerminOd(Date pisTerminOd) {
		this.pisTerminOd = pisTerminOd;
	}

	public Date getPisTerminDo() {
		return pisTerminDo;
	}

	public void setPisTerminDo(Date pisTerminDo) {
		this.pisTerminDo = pisTerminDo;
	}

	public String getPisUzasadnienie() {
		return pisUzasadnienie;
	}

	public void setPisUzasadnienie(String pisUzasadnienie) {
		this.pisUzasadnienie = pisUzasadnienie;
	}

	public ZptIprz getZptIprz() {
		return this.zptIprz;
	}

	public void setZptIprz(ZptIprz zptIprz) {
		this.zptIprz = zptIprz;
	}

	public OstZapisySzkolen getOstZapisySzkolen() {
		return this.ostZapisySzkolen;
	}

	public void setOstZapisySzkolen(OstZapisySzkolen ostZapisySzkolen) {
		this.ostZapisySzkolen = ostZapisySzkolen;
	}

	public PptSzkSzkolenia getPptSzkSzkolenia() {
		return this.pptSzkSzkolenia;
	}

	public void setPptSzkSzkolenia(PptSzkSzkolenia pptSzkSzkolenia) {
		this.pptSzkSzkolenia = pptSzkSzkolenia;
	}

	public String getPisFCzyNieSc() {
		return pisFCzyNieSc;
	}

	public void setPisFCzyNieSc(String pisFCzyNieSc) {
		this.pisFCzyNieSc = pisFCzyNieSc;
	}

	public String getPisFCzyIprz() {
		return pisFCzyIprz;
	}

	public void setPisFCzyIprz(String pisFCzyIprz) {
		this.pisFCzyIprz = pisFCzyIprz;
	}

	public Double getPisKoszt() {
		return pisKoszt;
	}

	public void setPisKoszt(Double pisKoszt) {
		this.pisKoszt = pisKoszt;
	}
	
}