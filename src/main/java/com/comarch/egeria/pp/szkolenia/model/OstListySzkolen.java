package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_SZK_LISTY_SZKOLEN database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_LISTY_SZKOLEN", schema="PPADM")
@NamedQuery(name="OstListySzkolen.findAll", query="SELECT o FROM OstListySzkolen o")
public class OstListySzkolen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_LISTY_SZKOLEN",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_LISTY_SZKOLEN", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_LISTY_SZKOLEN")	
	@Column(name="LSZ_ID")
	private long lszId;

	@Temporal(TemporalType.DATE)
	@Column(name="LSZ_AUDYT_DM")
	private Date lszAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="LSZ_AUDYT_DT")
	private Date lszAudytDt;

	@Column(name="LSZ_AUDYT_KM")
	private String lszAudytKm;

	@Column(name="LSZ_AUDYT_KT")
	private String lszAudytKt;

	@Column(name="LSZ_AUDYT_LM")
	private BigDecimal lszAudytLm;

	@Column(name="LSZ_AUDYT_UM")
	private String lszAudytUm;

	@Column(name="LSZ_AUDYT_UT")
	private String lszAudytUt;

	@Column(name="LSZ_BRAK_ZAGAD")
	private String lszBrakZagad;

	@Column(name="LSZ_CZY_NABYL_UPRAWNIENIA")
	private String lszCzyNabylUprawnienia;

	@Column(name="LSZ_CZY_REZERWOWA")
	private String lszCzyRezerwowa;

	@Column(name="LSZ_CZY_ZALICZYL")
	private String lszCzyZaliczyl;

	@Temporal(TemporalType.DATE)
	@Column(name="LSZ_DATA_ROZPOCZECIA")
	private Date lszDataRozpoczecia;

	@Temporal(TemporalType.DATE)
	@Column(name="LSZ_DATA_UKONCZENIA")
	private Date lszDataUkonczenia;

	@Temporal(TemporalType.DATE)
	@Column(name="LSZ_DATA_UMOWY")
	private Date lszDataUmowy;

	@Temporal(TemporalType.DATE)
	@Column(name="LSZ_DATA_WAZNOSCI")
	private Date lszDataWaznosci;

	@Temporal(TemporalType.DATE)
	@Column(name="LSZ_DATA_WYPELNIENIA")
	private Date lszDataWypelnienia;

	@Column(name="LSZ_KWOTA_ZOBOWIAZANIA")
	private BigDecimal lszKwotaZobowiazania;

	@Column(name="LSZ_NUMER_NA_LISCIE")
	private Long lszNumerNaLiscie;

	@Column(name="LSZ_NUMER_ZASWIADCZENIA")
	private String lszNumerZaswiadczenia;

	@Column(name="LSZ_OB_ID")
	private BigDecimal lszObId;

	@Column(name="LSZ_OCENA")
	private String lszOcena;

	@Column(name="LSZ_OGOLNE_UWAGI")
	private String lszOgolneUwagi;

	@Column(name="LSZ_PRC_ID")
	private Long lszPrcId;

	@Column(name="LSZ_RODZAJ_ZGLOSZENIA")
	private String lszRodzajZgloszenia;

	@Column(name="LSZ_STATUS")
	private String lszStatus;

	@Column(name="LSZ_STATUS_WAZNOSCI")
	private String lszStatusWaznosci;

	@Column(name="LSZ_TEMAT_PRZYSZL")
	private String lszTematPrzyszl;

	@Column(name="LSZ_UWAGI")
	private String lszUwagi;

	@Temporal(TemporalType.DATE)
	@Column(name="LSZ_ZOBOWIAZANIE_DO")
	private Date lszZobowiazanieDo;

	@Temporal(TemporalType.DATE)
	@Column(name="LSZ_ZOBOWIAZANIE_OD")
	private Date lszZobowiazanieOd;

	//bi-directional many-to-one association to OstSzkolenia
	@ManyToOne
	@JoinColumn(name="LSZ_SZK_ID")
	private OstSzkolenia ostSzkolenia;

	//bi-directional many-to-one association to OstAnkietyPozycje
//	@OneToMany(mappedBy="ostListySzkolen")
//	@LazyCollection(LazyCollectionOption.FALSE)
//	private List<PptSzkAnkietyPozycje> ostAnkietyPozycjes;

	public OstListySzkolen() {
	}

	public long getLszId() {
		return this.lszId;
	}

	public void setLszId(long lszId) {
		this.lszId = lszId;
	}

	public Date getLszAudytDm() {
		return this.lszAudytDm;
	}

	public void setLszAudytDm(Date lszAudytDm) {
		this.lszAudytDm = lszAudytDm;
	}

	public Date getLszAudytDt() {
		return this.lszAudytDt;
	}

	public void setLszAudytDt(Date lszAudytDt) {
		this.lszAudytDt = lszAudytDt;
	}

	public String getLszAudytKm() {
		return this.lszAudytKm;
	}

	public void setLszAudytKm(String lszAudytKm) {
		this.lszAudytKm = lszAudytKm;
	}

	public String getLszAudytKt() {
		return this.lszAudytKt;
	}

	public void setLszAudytKt(String lszAudytKt) {
		this.lszAudytKt = lszAudytKt;
	}

	public BigDecimal getLszAudytLm() {
		return this.lszAudytLm;
	}

	public void setLszAudytLm(BigDecimal lszAudytLm) {
		this.lszAudytLm = lszAudytLm;
	}

	public String getLszAudytUm() {
		return this.lszAudytUm;
	}

	public void setLszAudytUm(String lszAudytUm) {
		this.lszAudytUm = lszAudytUm;
	}

	public String getLszAudytUt() {
		return this.lszAudytUt;
	}

	public void setLszAudytUt(String lszAudytUt) {
		this.lszAudytUt = lszAudytUt;
	}

	public String getLszBrakZagad() {
		return this.lszBrakZagad;
	}

	public void setLszBrakZagad(String lszBrakZagad) {
		this.lszBrakZagad = lszBrakZagad;
	}

	public String getLszCzyNabylUprawnienia() {
		return this.lszCzyNabylUprawnienia;
	}

	public void setLszCzyNabylUprawnienia(String lszCzyNabylUprawnienia) {
		this.lszCzyNabylUprawnienia = lszCzyNabylUprawnienia;
	}

	public String getLszCzyRezerwowa() {
		return this.lszCzyRezerwowa;
	}

	public void setLszCzyRezerwowa(String lszCzyRezerwowa) {
		this.lszCzyRezerwowa = lszCzyRezerwowa;
	}

	public String getLszCzyZaliczyl() {
		return this.lszCzyZaliczyl;
	}

	public void setLszCzyZaliczyl(String lszCzyZaliczyl) {
		this.lszCzyZaliczyl = lszCzyZaliczyl;
	}

	public Date getLszDataRozpoczecia() {
		return this.lszDataRozpoczecia;
	}

	public void setLszDataRozpoczecia(Date lszDataRozpoczecia) {
		this.lszDataRozpoczecia = lszDataRozpoczecia;
	}

	public Date getLszDataUkonczenia() {
		return this.lszDataUkonczenia;
	}

	public void setLszDataUkonczenia(Date lszDataUkonczenia) {
		this.lszDataUkonczenia = lszDataUkonczenia;
	}

	public Date getLszDataUmowy() {
		return this.lszDataUmowy;
	}

	public void setLszDataUmowy(Date lszDataUmowy) {
		this.lszDataUmowy = lszDataUmowy;
	}

	public Date getLszDataWaznosci() {
		return this.lszDataWaznosci;
	}

	public void setLszDataWaznosci(Date lszDataWaznosci) {
		this.lszDataWaznosci = lszDataWaznosci;
	}

	public Date getLszDataWypelnienia() {
		return this.lszDataWypelnienia;
	}

	public void setLszDataWypelnienia(Date lszDataWypelnienia) {
		this.lszDataWypelnienia = lszDataWypelnienia;
	}

	public BigDecimal getLszKwotaZobowiazania() {
		return this.lszKwotaZobowiazania;
	}

	public void setLszKwotaZobowiazania(BigDecimal lszKwotaZobowiazania) {
		this.lszKwotaZobowiazania = lszKwotaZobowiazania;
	}

	public Long getLszNumerNaLiscie() {
		return this.lszNumerNaLiscie;
	}

	public void setLszNumerNaLiscie(Long lszNumerNaLiscie) {
		this.lszNumerNaLiscie = lszNumerNaLiscie;
	}

	public String getLszNumerZaswiadczenia() {
		return this.lszNumerZaswiadczenia;
	}

	public void setLszNumerZaswiadczenia(String lszNumerZaswiadczenia) {
		this.lszNumerZaswiadczenia = lszNumerZaswiadczenia;
	}

	public BigDecimal getLszObId() {
		return this.lszObId;
	}

	public void setLszObId(BigDecimal lszObId) {
		this.lszObId = lszObId;
	}

	public String getLszOcena() {
		return this.lszOcena;
	}

	public void setLszOcena(String lszOcena) {
		this.lszOcena = lszOcena;
	}

	public String getLszOgolneUwagi() {
		return this.lszOgolneUwagi;
	}

	public void setLszOgolneUwagi(String lszOgolneUwagi) {
		this.lszOgolneUwagi = lszOgolneUwagi;
	}

	public Long getLszPrcId() {
		return this.lszPrcId;
	}

	public void setLszPrcId(Long lszPrcId) {
		this.lszPrcId = lszPrcId;
	}

	public String getLszRodzajZgloszenia() {
		return this.lszRodzajZgloszenia;
	}

	public void setLszRodzajZgloszenia(String lszRodzajZgloszenia) {
		this.lszRodzajZgloszenia = lszRodzajZgloszenia;
	}

	public String getLszStatus() {
		return this.lszStatus;
	}

	public void setLszStatus(String lszStatus) {
		this.lszStatus = lszStatus;
	}

	public String getLszStatusWaznosci() {
		return this.lszStatusWaznosci;
	}

	public void setLszStatusWaznosci(String lszStatusWaznosci) {
		this.lszStatusWaznosci = lszStatusWaznosci;
	}

	public String getLszTematPrzyszl() {
		return this.lszTematPrzyszl;
	}

	public void setLszTematPrzyszl(String lszTematPrzyszl) {
		this.lszTematPrzyszl = lszTematPrzyszl;
	}

	public String getLszUwagi() {
		return this.lszUwagi;
	}

	public void setLszUwagi(String lszUwagi) {
		this.lszUwagi = lszUwagi;
	}

	public Date getLszZobowiazanieDo() {
		return this.lszZobowiazanieDo;
	}

	public void setLszZobowiazanieDo(Date lszZobowiazanieDo) {
		this.lszZobowiazanieDo = lszZobowiazanieDo;
	}

	public Date getLszZobowiazanieOd() {
		return this.lszZobowiazanieOd;
	}

	public void setLszZobowiazanieOd(Date lszZobowiazanieOd) {
		this.lszZobowiazanieOd = lszZobowiazanieOd;
	}

	public OstSzkolenia getOstSzkolenia() {
		return this.ostSzkolenia;
	}

	public void setOstSzkolenia(OstSzkolenia ostSzkolenia) {
		this.ostSzkolenia = ostSzkolenia;
	}

//	public List<PptSzkAnkietyPozycje> getOstAnkietyPozycjes() {
//		return this.ostAnkietyPozycjes;
//	}
//
//	public void setOstAnkietyPozycjes(List<PptSzkAnkietyPozycje> ostAnkietyPozycjes) {
//		this.ostAnkietyPozycjes = ostAnkietyPozycjes;
//	}

//	public PptSzkAnkietyPozycje addOstAnkietyPozycje(PptSzkAnkietyPozycje ostAnkietyPozycje) {
//		getOstAnkietyPozycjes().add(ostAnkietyPozycje);
//		ostAnkietyPozycje.setOstListySzkolen(this);
//
//		return ostAnkietyPozycje;
//	}

//	public PptSzkAnkietyPozycje removeOstAnkietyPozycje(PptSzkAnkietyPozycje ostAnkietyPozycje) {
//		getOstAnkietyPozycjes().remove(ostAnkietyPozycje);
//		ostAnkietyPozycje.setOstListySzkolen(null);
//
//		return ostAnkietyPozycje;
//	}

}