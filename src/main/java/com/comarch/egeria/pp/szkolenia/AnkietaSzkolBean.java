package com.comarch.egeria.pp.szkolenia;

import static com.comarch.egeria.Utils.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.szkolenia.model.PptSzkAnkietyPozycje;
import com.comarch.egeria.pp.szkolenia.model.PptUczestnicySzkolen;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean
@Named
@Scope("view")
public class AnkietaSzkolBean extends SqlBean {

	@Inject
	SessionBean sessionBean;
	
	@Inject
	protected ReportGenerator ReportGenerator;
	
	private DataRow uszDR;
	private PptUczestnicySzkolen usz;
	
	private int liczbaPoz = 0;
	
	@PostConstruct
	public void init() {
		
		String id = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		DataRow row = (DataRow) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("row");
		
		this.setUszDR(row);
		
		this.loadSqlData("nazwyOcenBezWykl",
				"select K.WSL_WARTOSC, K.WSL_OPIS, K.WSL_TYP, K.WSL_WARTOSC_ZEWN " + 
				"from  ppt_adm_wartosci_slownikow K " +
				"where K.WSL_SL_NAZWA = \'OS_ANKIETA_TYP_OCENY\' " +
				"and WSL_WARTOSC like \'O%\' " +
				"order by LPAD(K.WSL_WARTOSC, 10)", new ArrayList<Object>());
		
		this.loadSqlData("nazwyOcenWykl",
				"select K.WSL_WARTOSC, K.WSL_OPIS, K.WSL_TYP, K.WSL_WARTOSC_ZEWN " + 
				"from  ppt_adm_wartosci_slownikow K " +
				"where K.WSL_SL_NAZWA = \'OS_ANKIETA_TYP_OCENY\' " +
				"and WSL_WARTOSC like \'W%\' " +
				"order by LPAD(K.WSL_WARTOSC, 10)", new ArrayList<Object>());
		
		if (id == null || "".equals(id)) {
			
			setUsz(new PptUczestnicySzkolen());
			
		} else {
			
			setUsz((PptUczestnicySzkolen) HibernateContext.get(PptUczestnicySzkolen.class, Long.parseLong("" + id)));
			naprawaEncji();
			
		}		
		
		for (PptSzkAnkietyPozycje poz : usz.getPptAnkietyPozycjes()) {
			if (poz.getAnspSwykId() == null)
				liczbaPoz++;
		}
		
	}
	public void raport() {
		
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("usz_id", usz.getUszId());
			ReportGenerator.displayReportAsPDF(params, "Ankieta_szkoleniowa");

		} catch (JRException | SQLException | IOException e) {
			User.alert(e.getMessage());
		}
	
}

	private void naprawaEncji() {
		// naprawa ocen szkolenia
		if (this.getSql("nazwyOcenBezWykl") != null) {
			if (liczbaPoz < this.getSql("nazwyOcenBezWykl").getData().size()) {
				this.getSql("nazwyOcenBezWykl").getData().forEach(p -> {
					PptSzkAnkietyPozycje poz = null;
					Optional<PptSzkAnkietyPozycje> findFirst = usz.getPptAnkietyPozycjes()
						.stream()
						.filter(pp-> pp.getAnspTypOceny().equals(p.get("WSL_WARTOSC"))).findFirst();
					if (findFirst.isPresent()) {
						poz = findFirst.get();
					} else {
						poz = new PptSzkAnkietyPozycje();
						
						poz.setAnspTypOceny("" + p.get("WSL_WARTOSC"));
						
						usz.addPptAnkietyPozycje(poz);
					}
				});
				
			}
		}
		
		// naprawa ocen wykładowcy
		for (DataRow r : this.getLW("PPL_SZK_WYKLADOWCY", usz.getPptSzkolenia().getSzkId() ).getData()) {
			
			BigDecimal bdSwykId = (BigDecimal) r.get("pp_wyk_id");
			Long swykId = nz(bdSwykId.longValue());
			
			Optional<PptSzkAnkietyPozycje> findFirst = usz.getPptAnkietyPozycjes()
					.stream()
					.filter(p->swykId.equals(p.getAnspSwykId()))
					.findFirst();
			
						
			if (!findFirst.isPresent()) {
				this.getSql("nazwyOcenWykl").getData().forEach(p -> {
					PptSzkAnkietyPozycje pozTemp = null;
					Optional<PptSzkAnkietyPozycje> findFirstW = usz.getPptAnkietyPozycjes()
						.stream()
						.filter(pp-> pp.getAnspTypOceny().equals(p.get("WSL_WARTOSC")) && swykId.equals(pp.getAnspSwykId() ) ).findFirst();
					
					if (findFirstW.isPresent()) {
						pozTemp = findFirstW.get();
					} else {
						pozTemp = new PptSzkAnkietyPozycje();
						
						pozTemp.setAnspTypOceny("" + p.get("WSL_WARTOSC"));
						pozTemp.setAnspSwykId(swykId);
						usz.addPptAnkietyPozycje(pozTemp);
					}
				});
			}
		}
		
		int size = usz.getPptAnkietyPozycjes().size();
		usz.getPptAnkietyPozycjes().sort((o1, o2) ->o1.getAnspTypOceny().compareTo(o2.getAnspTypOceny()));
	}
	
//	private void naprawaUszkodzonejEncji() {
//		//todo - dogenerowac brakujace pozycje wg PPL_SZK_WYKLADOWCY (lub kryteriow dla ocen samego szkolenia)
//		String warnings = "";
//		
//		for (DataRow r : this.getLW("PPL_SZK_WYKLADOWCY", usz.getPptSzkolenia().getSzkId() ).getData()) {
//			
//			BigDecimal bdSwykId = (BigDecimal) r.get("pp_wyk_id");
//			Long swykId = nz(bdSwykId.longValue());
//			
//			
//			PptSzkAnkietyPozycje poz = null;
//			
//			Optional<PptSzkAnkietyPozycje> findFirst = usz.getPptAnkietyPozycjes()
//					.stream()
//					.filter(p->swykId.equals(p.getAnspSwykId()))
//					.findFirst();
//			
//			
//			
//			if (findFirst.isPresent()){
//				poz = findFirst.get();
//			}else{
//				poz = new PptSzkAnkietyPozycje();
//				
//				//poz.setAnspTypOceny("WDYS");
//				//poz.setAnspTypOceny("WKON");
//				
//				usz.addPptAnkietyPozycje(poz);
//				warnings += " Dodano brakujacą ocenę dla " + r;
//			}
//			
////			this.ocenyWykladowcyHM.put(r, poz);
//		}
//
//		
//		if (!warnings.isEmpty())
//			User.warn(warnings);
//	}	
	
	public void zapisz() {
		
		if (!validateBeforeSaveAnkieta())
			return;
		
		try (HibernateContext h = new HibernateContext()) {

			Session s = h.getSession();
			
			this.usz.setUszDataWypelnienia(new Date());
			this.usz = (PptUczestnicySzkolen) s.merge(this.usz);
			s.saveOrUpdate(this.usz);
			s.beginTransaction().commit();
			s.refresh(this.usz);
			

			User.info("Zapisano ankietę.");
//			FacesContext.getCurrentInstance().getExternalContext().redirect("AnkietySzkoleniowe");

		} catch (Exception e) {
			User.alert("Błąd w zapisie ankiety!", e);
			e.printStackTrace();
		}
	}

	private boolean validateBeforeSaveAnkieta() {
		Boolean ret = true;
		ArrayList<String> inval = new ArrayList<String>();
		
		ArrayList<String> alOcenySzk = new ArrayList<>();
		HashMap<String, ArrayList<String>> hmOcenyWyk = new HashMap<>();
		
		for (PptSzkAnkietyPozycje poz : usz.getPptAnkietyPozycjes()) {
			if ((poz.getAnspOcena() == null || poz.getAnspOcena().equals("")) 
					&& (poz.getAnspOcenaOpis() == null || poz.getAnspOcenaOpis().equals(""))){
				
				ret = false;
			
				SqlDataSelectionsHandler lw = this.getLW("PPL_SZK_WYKLADOWCY", usz.getPptSzkolenia().getSzkId());
				DataRow rWykl = lw.find(poz.getAnspSwykId());
				
				DataRow rNazwaOceny = this.getSql("nazwyOcenBezWykl").find(poz.getAnspTypOceny());
				if (rNazwaOceny==null)
					rNazwaOceny = this.getSql("nazwyOcenWykl").find(poz.getAnspTypOceny());
//				System.out.println(w);
//				System.out.println(no);
				if (rNazwaOceny==null){
					User.alert("Brak nazwy oceny dla symbolu " +poz.getAnspTypOceny());
					continue;
				}
				
				if (rWykl==null)
					alOcenySzk.add(""+rNazwaOceny.get("WSL_OPIS"));
				else{
					String w = nz((String)rWykl.get("pp_tytul"))+" " +nz((String)rWykl.get("pp_wyk_osoba"));
					ArrayList<String> al = hmOcenyWyk.get(w);
					if (al == null) al = new ArrayList<>();
					al.add(""+rNazwaOceny.get("WSL_OPIS"));
					hmOcenyWyk.put(w, al);
				}
			}
		}
		
		
		if (!ret) {
			
			String s = "Nie wypełniono wszystkich pozycji ankiety!<br/>";
			
			if (!alOcenySzk.isEmpty())
				s += "<br/><b>Pozycje w zakładce 'Ocena szkolenia':</b> <br/><br/>" + String.join("<br/>", alOcenySzk) + "<br/>";
			
			if (!hmOcenyWyk.isEmpty()){
					s += "<br/><b>Pozycje w zakładce 'Oceny wykładowców':</b><br/><br/>";
				
				for (String w : hmOcenyWyk.keySet()) {
					s += "<b><ins>"+ w + "</ins></b><br/>" + String.join("<br/>", String.join("<br/>", hmOcenyWyk.get(w)));
				}
			}
			
			inval.add( s );
		}
		
		for (Object txt : inval) {
			User.alert("" +txt);
		}
		
		return ret;
	}
	
	
	public DataRow getUszDR() {
		return uszDR;
	}

	public void setUszDR(DataRow uszDR) {
		this.uszDR = uszDR;
	}

	public PptUczestnicySzkolen getUsz() {
		return usz;
	}

	public void setUsz(PptUczestnicySzkolen usz) {
		this.usz = usz;
	}	

}