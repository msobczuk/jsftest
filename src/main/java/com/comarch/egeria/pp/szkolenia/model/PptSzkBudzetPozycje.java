package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the PPT_SZK_BUDZET_POZYCJE database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_BUDZET_POZYCJE")
@NamedQuery(name="PptSzkBudzetPozycje.findAll", query="SELECT p FROM PptSzkBudzetPozycje p")
public class PptSzkBudzetPozycje implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_BUDZET_POZYCJE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_BUDZET_POZYCJE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_BUDZET_POZYCJE")
	@Column(name="BSP_ID")
	private long bspId;

	@Column(name="BSP_OB_ID")
	private BigDecimal bspObId;

	@Column(name="BSP_PLAN")
	private double bspPlan;

	//bi-directional many-to-one association to PptSzkBudzet
	@ManyToOne
	@JoinColumn(name="BSP_BSZ_ID")
	private PptSzkBudzet pptSzkBudzet;

	public PptSzkBudzetPozycje() {
	}

	public long getBspId() {
		return this.bspId;
	}

	public void setBspId(long bspId) {
		this.bspId = bspId;
	}

	public BigDecimal getBspObId() {
		return this.bspObId;
	}

	public void setBspObId(BigDecimal bspObId) {
		this.bspObId = bspObId;
	}

	public double getBspPlan() {
		return this.bspPlan;
	}

	public void setBspPlan(double bspPlan) {
		this.bspPlan = bspPlan;
	}

	public PptSzkBudzet getPptSzkBudzet() {
		return this.pptSzkBudzet;
	}

	public void setPptSzkBudzet(PptSzkBudzet pptSzkBudzet) {
		this.pptSzkBudzet = pptSzkBudzet;
	}

}