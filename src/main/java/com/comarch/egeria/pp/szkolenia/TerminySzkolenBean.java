package com.comarch.egeria.pp.szkolenia;

import static com.comarch.egeria.Utils.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.szkolenia.model.OstPlanIndyw;
import com.comarch.egeria.pp.szkolenia.model.PptKosztySzkolen;
import com.comarch.egeria.pp.szkolenia.model.PptOfertySzkolen;
import com.comarch.egeria.pp.szkolenia.model.PptSzkAnkietyPozycje;
import com.comarch.egeria.pp.szkolenia.model.PptSzkSzkolenia;
import com.comarch.egeria.pp.szkolenia.model.PptSzkWykladowcy;
import com.comarch.egeria.pp.szkolenia.model.PptSzkWykladowcySzkolenie;
import com.comarch.egeria.pp.szkolenia.model.PptUczestnicySzkolen;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean
@Named
@Scope("view")
public class TerminySzkolenBean extends SqlBean {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	
	@Inject
	RejestrSzkolenBean rejestrSzkolen;

//	@Inject
//	protected ReportGenerator ReportGenerator;

//	@Inject
//	ZrodlaFinansowaniaBean zrodlaFinansowaniaBean;

	PptSzkSzkolenia terminSzkolenia = new PptSzkSzkolenia();

	DataRow terminSzkoleniaDR;

	String ofertaSzkolen = "";

	Long selectedTerminSzkoleniaId = null;

//	List<PptSzkSzkolenia> listaTerminowSzkolen = null;

	List<PptSzkAnkietyPozycje> listaAnkietyPozycje = new ArrayList<>();
	
	List<PptSzkAnkietyPozycje> listaWykladowcyPozycje = new ArrayList<>();

	List<PptSzkWykladowcy> listaWykladowcow = new ArrayList<>();

	public HashMap<DataRow, PptUczestnicySzkolen> pis2uszHM = new HashMap<>();
	
	
	@PostConstruct
	public void init(){

	}
	
	
	public void wczytajTerminSzkoleniaDR(DataRow dr) {
		this.pis2uszHM.clear();
		this.terminSzkoleniaDR = dr;

		if (terminSzkoleniaDR == null) {
			// nowy termin szkolenia ...
			this.setTerminSzkolenia(null);
			this.terminSzkolenia = new PptSzkSzkolenia();
			this.terminSzkolenia.setPptUczestnicySzkolens(new ArrayList<PptUczestnicySzkolen>());
			this.terminSzkolenia.setPptKosztySzkolens(new ArrayList<PptKosztySzkolen>());
		} else {
			long id = Long.parseLong("" + dr.getIdAsString());
			this.setTerminSzkolenia((PptSzkSzkolenia) HibernateContext.get(PptSzkSzkolenia.class, id));
		}

		com.comarch.egeria.Utils.update("rejestrSzkolenTermin");
		com.comarch.egeria.Utils.update("dlgLov");

		przygotujPozycjeAnkiety();
		lwUkryjDodanychWykladowcow();
		lwUkryjDodanychUczestnikow();
	}

	
	
	public void nowyTerminSzkolenia(Long szkId, Long ofId) {
		PptOfertySzkolen of =  (PptOfertySzkolen) HibernateContext.get(PptOfertySzkolen.class, ofId);
		PptSzkSzkolenia szk =  new PptSzkSzkolenia();
		szk.setPptOfertySzkolen(of);
		wczytajTerminSzkolenia(szk);
	}
	
	public void wczytajTerminSzkolenia(Long szkId) {
		PptSzkSzkolenia szk = (PptSzkSzkolenia) HibernateContext.get(PptSzkSzkolenia.class, szkId);
		wczytajTerminSzkolenia(szk);
	}
	
	public void wczytajTerminSzkolenia(DataRow r) {
		this.terminSzkoleniaDR = r;
		
		PptSzkSzkolenia szk = (PptSzkSzkolenia) HibernateContext.get(PptSzkSzkolenia.class, r.getIdAsLong());
		wczytajTerminSzkolenia(szk);
	}
	
	public void wczytajTerminSzkolenia(PptSzkSzkolenia o) {
		this.pis2uszHM.clear();
		
		this.setTerminSzkolenia(o);
		
		com.comarch.egeria.Utils.update("rejestrSzkolenTermin");
		com.comarch.egeria.Utils.update("dlgLov");

		przygotujPozycjeAnkiety();
		
		lwUkryjDodanychWykladowcow();
		lwUkryjDodanychUczestnikow();
	}


	public void lwUkryjDodanychWykladowcow() {
		hideLwRows("ppl_szk_wykladowcy_lista", this.terminSzkolenia.wykladowcyToSwykIdList());
	}

	
	
	
	public void przygotujPozycjeAnkiety() {
		List nazwyPozycjiAnkiety = new ArrayList<>();

		SqlDataSelectionsHandler p = this.getSql("nazwyOcenBezWykl");
		if (p != null) {
			for (DataRow dr : p.getData()) {
				PptSzkAnkietyPozycje oap = new PptSzkAnkietyPozycje();
				oap.setAnspTypOceny((String) dr.get("WSL_WARTOSC"));
				oap.setAnspOcena("");
				nazwyPozycjiAnkiety.add(oap);
			}
			listaAnkietyPozycje = nazwyPozycjiAnkiety;
		}

		List<PptSzkWykladowcySzkolenie> wykladowcySzkolenia = this.terminSzkolenia.getPptSzkWykladowcySzkolenies();
		listaWykladowcow = new ArrayList<>();

		if (wykladowcySzkolenia != null) {

			for (PptSzkWykladowcySzkolenie wszk : wykladowcySzkolenia) {
				listaWykladowcow.add(wszk.getPptSzkWykladowcy());
			}
		}

		p = this.getSql("nazwyOcenWykl");
		List listaPozycjiWykladowcow = new ArrayList<>();
		if (p != null) {
			for (DataRow dr : p.getData()) {
				PptSzkAnkietyPozycje oap = new PptSzkAnkietyPozycje();
				oap.setAnspTypOceny((String) dr.get("WSL_WARTOSC"));
				oap.setAnspOcena("");
				listaPozycjiWykladowcow.add(oap);
			}
			listaWykladowcyPozycje = listaPozycjiWykladowcow;
		}
	}

	
	public void usun() {
		Long szkId = this.terminSzkolenia.getSzkId();
		if (szkId == null || szkId==0L) return;
		
		try {
			PptOfertySzkolen of = this.terminSzkolenia.getPptOfertySzkolen();
			if (of!=null){
				of.removePptSzkolenia(this.terminSzkolenia);
				HibernateContext.save(of);//cascade!!! + orphan removal skasuja termin szkolenia
			}else {
				HibernateContext.delete(this.terminSzkolenia);
			}
			
			User.info("Usunięto termin szkolenia.");
			reloadAndUpdate("ppl_szk_terminy_szkolenia", szkId);
		} catch (Exception e) {
			User.alert("Błąd w usuwaniu terminu szkolenia: " + e.getMessage());
			log.error(e.getMessage(), e);
		}
	}
	
	
	public void usun(DataRow r){
		wczytajTerminSzkolenia(r);
		usun();
	}	

	public void usun(PptSzkSzkolenia o) {
		Long szkId = o.getSzkId();
		if (szkId == null || szkId==0L) return;
		
		try {
			PptOfertySzkolen of = o.getPptOfertySzkolen();
			if (of!=null){
				of.removePptSzkolenia(o);
				HibernateContext.save(of);//cascade!!! + orphan removal skasuja termin szkolenia
			}else {
				HibernateContext.delete(this.terminSzkolenia);
			}
			User.info("Usunięto termin szkolenia.");
			reloadAndUpdate("ppl_szk_terminy_szkolenia", szkId);
		} catch (Exception e) {
			User.alert("Błąd w usuwaniu terminu szkolenia: " + e.getMessage());
			log.error(e.getMessage(), e);
		}
	}
	
	
	
	public void dodajZaznaczonychUczestnikow() {
		SqlDataSelectionsHandler lwPis2Szk = getLW("ppl_szk_pis4szk");
		SqlDataSelectionsHandler lw = getLW("PP_PRC");
		
		if (lw == null) 
			return;
		
		List<Long> selectedPrcIdList =  lw.getSelectedRows().stream().map(r -> ((BigDecimal) r.get("prc_id")).longValue()).collect(Collectors.toList());
		selectedPrcIdList.addAll(lwPis2Szk.getSelectedRows().stream().map(r -> ((BigDecimal) r.get("prc_id")).longValue()).collect(Collectors.toList()));
		
		Long maxUczestnikow = nz( terminSzkolenia.getSzkMaxUczestnikow() );

		List<Long> uczestnicyPrcIdList = terminSzkolenia.getPptUczestnicySzkolens().stream().map(usz->  usz.getUszPrcId() ).collect(Collectors.toList());
		for (DataRow r : lwPis2Szk.getSelectedRows()) {
			if (!uczestnicyPrcIdList.contains( r.getLong("prc_id") ) ){
				PptUczestnicySzkolen usz = new PptUczestnicySzkolen();
				usz.setUszPrcId(r.getLong("prc_id"));
				usz.setUszRez(terminSzkolenia.getPptUczestnicyNierezerwowi().size() >= maxUczestnikow ? "T" : "N");
				this.terminSzkolenia.addPptUczestnicySzkolen(usz);
				this.pis2uszHM.put(r, usz);
				uczestnicyPrcIdList.add(r.getLong("prc_id"));
			}else{
				User.warn("%1$s %2$s jest już uczestnikiem tego szkolenia."
						+ "W planie szkoleniowym pozycja zapisu: %3$s / %4$s nie zostanie przypisana do tego szkolenia. ", 
						r.get("prc_imie"), r.get("prc_nazwisko"), r.get("pis_id"),  r.get("of_nazwa") );
			}
		}
		
		//uczestnicyPrcIdList = terminSzkolenia.getPptUczestnicySzkolens().stream().map(usz->  usz.getUszPrcId() ).collect(Collectors.toList());
		for (DataRow r : lw.getSelectedRows()) {
			if (!uczestnicyPrcIdList.contains( r.getLong("prc_id") ) ){
				PptUczestnicySzkolen usz = new PptUczestnicySzkolen();
				usz.setUszPrcId(r.getLong("prc_id"));
				usz.setUszRez(terminSzkolenia.getPptUczestnicyNierezerwowi().size() >= maxUczestnikow ? "T" : "N");
				this.terminSzkolenia.addPptUczestnicySzkolen(usz);
				uczestnicyPrcIdList.add(r.getLong("prc_id"));
			}else{
				User.warn("%1$s %2$s jest już uczestnikiem tego szkolenia.", 
						 r.get("prc_imie"), r.get("prc_nazwisko") );
			}
		}
		
		
		this.terminSzkolenia.recalcProporcjoanlnieKoszty();
		lwPis2Szk.clearSelection();
		lw.clearSelection();
		
		lwUkryjDodanychUczestnikow();
	}	


	public void lwUkryjDodanychUczestnikow() {
		List<Long> uczestnicyToPrcIdList = this.terminSzkolenia.uczestnicyToPrcIdList();
		hideLwRows("PP_PRC", uczestnicyToPrcIdList);
		
		SqlDataSelectionsHandler lw = findLW("ppl_szk_pis4szk");
		if (lw==null) return;
		
		//lw.hiddenRowsPK.clear(); -> lw.getOriginalData() 
		
		List<Long> pisIdListToHide = lw.getOriginalData() 
		.stream()
		.filter(r-> uczestnicyToPrcIdList.contains( r.getLong("prc_id") ))
		.map(r->r.getLong("pis_id"))
		.collect(Collectors.toList());
		
		hideLwRows("ppl_szk_pis4szk", pisIdListToHide);
		
	}
	
	public void raport() {
		HashMap<String, Object> params = new HashMap<String, Object>();
		long id = this.terminSzkolenia.getSzkId();
		params.put("szkol_id", id);

		try {
			ReportGenerator.displayReportAsXLS(params, "oceny_szkolenia");
//			ReportGenerator.displayReportAsPDF(params, "oceny_szkolenia");
		} catch (SQLException | JRException | IOException e) {
			log.error(e.getMessage(), e);
		}
	}


	public void zapisz() {

		if (!validateBeforeSave())
			return;
		
//		if (!zrodlaFinansowaniaBean.validateBeforeSave()) {
//			return;
//		}

		// jesli nowe dziecko to dodajemy je do rodzica
		if (this.terminSzkolenia.getSzkId() == null || this.terminSzkolenia.getSzkId() == 0L) {
			this.rejestrSzkolen.getOferta().addPptSzkolenia(this.terminSzkolenia);
		}

		
		//po modyfikacji listy uczestnikow moga byc nieaktualne zapisy (pis->szk gdzie pis_prcId nie jest juz uczestnikiem szk)
		List<Long> uczestnicyPrcIdList = this.terminSzkolenia.getPptUczestnicySzkolens().stream().map(u -> u.getUszPrcId()).collect(Collectors.toList());
		List<OstPlanIndyw> listaPozycjiZapisowDoOdwiazania = this.terminSzkolenia.getPptSzkPlanIndyws().stream()
				.filter(pis -> !uczestnicyPrcIdList.contains( pis.getPisPrcId()  ) )
				.collect(Collectors.toList());
		listaPozycjiZapisowDoOdwiazania.stream().forEach(pis -> this.terminSzkolenia.removePptSzkPlanIndyw(pis));
		
					
			try (HibernateContext h = new HibernateContext()) {
				
				Session s = h.getSession();

				listaPozycjiZapisowDoOdwiazania.stream().forEach(pis -> s.saveOrUpdate(pis)); 
				
				
				//pozycje zapisow do zwiazana z terminem
				for (DataRow r : pis2uszHM.keySet()) {
					OstPlanIndyw pis = s.get(OstPlanIndyw.class,  r.getLong("pis_id"));
					terminSzkolenia.addPptSzkPlanIndyw(pis);
					s.saveOrUpdate(pis);
				}
				
				
				terminSzkolenia = (PptSzkSzkolenia) s.merge(terminSzkolenia);
				s.saveOrUpdate(terminSzkolenia);
				s.beginTransaction().commit();
				s.refresh(terminSzkolenia);
			
//				zrodlaFinansowaniaBean.zapisz(this.terminSzkolenia.getSzkId());

				User.info("Zapisano termin szkolenia.");
				reloadAndUpdate("ppl_szk_terminy_szkolenia", this.terminSzkolenia.getSzkId());

		} catch (Exception e) {
			User.alert("Błąd w zapisie terminu szkolenia");
			e.printStackTrace();
		}
	}
	
	
	
	
	
	

	private boolean validateBeforeSave() {
		ArrayList<String> inv = new ArrayList<String>();

		if (this.terminSzkolenia.getSzkDataOd() == null)
			inv.add("Data rozpoczęcia szkolenia jest polem wymaganym.");

		if (this.terminSzkolenia.getSzkDataDo() == null)
			inv.add("Data zakończenia szkolenia jest polem wymaganym.");

		if (this.terminSzkolenia.getSzkDataOd().after(this.terminSzkolenia.getSzkDataDo()))
			inv.add("Data zakończenia szkolenia nie może poprzedzać daty rozpoczęcia.");

		if (this.terminSzkolenia.getSzkStatus() == null || this.terminSzkolenia.getSzkStatus() == "") {
			inv.add("Status szkolenia jest polem wymaganym.");
		} else {
			if (this.terminSzkolenia.getSzkStatus().length() > 10) {
				inv.add("Status szkolenia nie może mieć więcej znaków niż 10!");
			}
		}

		if (this.terminSzkolenia.getPptUczestnicySzkolens() != null
				&& !this.terminSzkolenia.getPptUczestnicySzkolens().isEmpty()) {
			if (this.terminSzkolenia.getSzkMaxUczestnikow() != null) {

				if (Long.valueOf(this.terminSzkolenia.getPptUczestnicySzkolens().size()) > this.terminSzkolenia
						.getSzkMaxUczestnikow()) {
					Long liczbaRezerwowych = 0L;

					for (PptUczestnicySzkolen ucz : this.terminSzkolenia.getPptUczestnicySzkolens()) {
						if (ucz.getUszRez() != null && !ucz.getUszRez().isEmpty() && ucz.getUszRez().equals("T")) {
							liczbaRezerwowych++;
						}
					}

					Long liczbaZapisywanychUczestnikow = Long
							.valueOf(this.terminSzkolenia.getPptUczestnicySzkolens().size()) - liczbaRezerwowych;

					if (liczbaZapisywanychUczestnikow > this.terminSzkolenia.getSzkMaxUczestnikow()) {
						inv.add("Liczba uczestników większa od maksymalnej! Część z nich musi zostać zapisana na listę rezerwową.");
					}

				}
			}
		}

		for (String msg : inv) {
			User.alert(msg);
		}
		return inv.isEmpty();
	}




	public Long getSelectedTerminSzkoleniaId() {
		return selectedTerminSzkoleniaId;
	}

	public void setSelectedTerminSzkoleniaId(Long selectedTerminSzkoleniaId) {
		this.selectedTerminSzkoleniaId = selectedTerminSzkoleniaId;
		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			setTerminSzkolenia(s.get(PptSzkSzkolenia.class, selectedTerminSzkoleniaId));
			selectedTerminSzkoleniaId = null;
		}
	}



	public String getZaznWszystkieObecnosci() {
		if (this.terminSzkolenia.getPptUczestnicySzkolens() == null
				|| this.terminSzkolenia.getPptUczestnicySzkolens().isEmpty())
			return "N";

		String ret = "T";
		for (PptUczestnicySzkolen u : this.terminSzkolenia.getPptUczestnicySzkolens()) {
			if (!"T".equals(u.getUszObecnosc())) {
				ret = "N";
				break;
			}
		}
		return ret;
	}

	public void setZaznWszystkieObecnosci(String zaznWszystkieObecnosci) {
		if (this.terminSzkolenia.getPptUczestnicySzkolens() == null
				|| this.terminSzkolenia.getPptUczestnicySzkolens().isEmpty())
			return;

		for (PptUczestnicySzkolen u : this.terminSzkolenia.getPptUczestnicySzkolens()) {
			if ("N".equals(u.getUszRez()))
				u.setUszObecnosc(zaznWszystkieObecnosci);
			else
				u.setUszObecnosc("N");
		}
	}

	public void dodajWykladowcow() {
		SqlDataSelectionsHandler lw = findLW("ppl_szk_wykladowcy_lista");
		if (lw==null) return;
		
		List<DataRow> wykladowcyKeySet = lw.getSelectedRows();

		for (DataRow dr : wykladowcyKeySet) {
			if (this.terminSzkolenia.getPptSzkWykladowcySzkolenies().stream().filter(
					u -> u.getPptSzkWykladowcy().getSwykId() == ((BigDecimal) dr.get("pp_wyk_id")).longValue())
					.collect(Collectors.toList()).size() == 0) {
				PptSzkWykladowcySzkolenie w = new PptSzkWykladowcySzkolenie();
				w.setPptSzkWykladowcy((PptSzkWykladowcy) HibernateContext.get(PptSzkWykladowcy.class,
						((BigDecimal) dr.get("pp_wyk_id")).longValue()));

				this.terminSzkolenia.addPptSzkWykladowcySzkoleny(w);
				lw.getHiddenRowsPK().add(w.getWszkId());
			}
		}
		
		lw.clearSelection();
		lwUkryjDodanychWykladowcow();
	}
	
	public PptSzkSzkolenia getTerminSzkolenia() {
		return terminSzkolenia;
	}

	public void setTerminSzkolenia(PptSzkSzkolenia terminSzkolenia) {
		this.terminSzkolenia = terminSzkolenia;
	}
	
	public List<PptSzkWykladowcy> getListaWykladowcow() {
		return listaWykladowcow;
	}

	public void setListaWykladowcow(List<PptSzkWykladowcy> listaWykladowcow) {
		this.listaWykladowcow = listaWykladowcow;
	}

	public List<PptSzkAnkietyPozycje> getListaAnkietyPozycje() {
		return listaAnkietyPozycje;
	}

	public void setListaAnkietyPozycje(List<PptSzkAnkietyPozycje> listaAnkietyPozycje) {
		this.listaAnkietyPozycje = listaAnkietyPozycje;
	}

	public List<PptSzkAnkietyPozycje> getListaWykladowcyPozycje() {
		return listaWykladowcyPozycje;
	}

	public void setListaWykladowcyPozycje(List<PptSzkAnkietyPozycje> listaWykladowcyPozycje) {
		this.listaWykladowcyPozycje = listaWykladowcyPozycje;
	}

	
}
