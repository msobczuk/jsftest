package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PPT_SZK_UMOWY database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_UMOWY", schema="PPADM")
@NamedQuery(name="OstUmowy.findAll", query="SELECT o FROM OstUmowy o")
public class OstUmowy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
//	@GeneratedValue(strategy=GenerationType.TABLE)
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_UMOWY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_UMOWY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_UMOWY")	
	@Column(name="UMSZ_ID")
	private long umszId;

	@Temporal(TemporalType.DATE)
	@Column(name="UMSZ_AUDYT_DM")
	private Date umszAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="UMSZ_AUDYT_DT")
	private Date umszAudytDt;

	@Column(name="UMSZ_AUDYT_KM")
	private String umszAudytKm;

	@Column(name="UMSZ_AUDYT_LM")
	private String umszAudytLm;

	@Column(name="UMSZ_AUDYT_UM")
	private String umszAudytUm;

	@Column(name="UMSZ_AUDYT_UT")
	private String umszAudytUt;

	@Column(name="UMSZ_CZY_UREGULOWANO")
	private String umszCzyUregulowano;

	@Temporal(TemporalType.DATE)
	@Column(name="UMSZ_DATA_OBOWIAZYWANIA")
	private Date umszDataObowiazywania;

	@Temporal(TemporalType.DATE)
	@Column(name="UMSZ_DATA_PODPISANIA")
	private Date umszDataPodpisania;

	@Column(name="UMSZ_PRC_ID")
	private Long umszPrcId;

	@Column(name="UMSZ_PRZEDMIOT")
	private String umszPrzedmiot;

	@Column(name="UMSZ_UWAGI")
	private String umszUwagi;

	@Column(name="UMSZ_WARTOSC")
	private Long umszWartosc;

	@Column(name="UMSZ_F_CZY_DYPLOM")
	private String umszFCzyDyplom;

	public OstUmowy() {
	}

	public long getUmszId() {
		return this.umszId;
	}

	public void setUmszId(long umszId) {
		this.umszId = umszId;
	}

	public Date getUmszAudytDm() {
		return this.umszAudytDm;
	}

	public void setUmszAudytDm(Date umszAudytDm) {
		this.umszAudytDm = umszAudytDm;
	}

	public Date getUmszAudytDt() {
		return this.umszAudytDt;
	}

	public void setUmszAudytDt(Date umszAudytDt) {
		this.umszAudytDt = umszAudytDt;
	}

	public String getUmszAudytKm() {
		return this.umszAudytKm;
	}

	public void setUmszAudytKm(String umszAudytKm) {
		this.umszAudytKm = umszAudytKm;
	}

	public String getUmszAudytLm() {
		return this.umszAudytLm;
	}

	public void setUmszAudytLm(String umszAudytLm) {
		this.umszAudytLm = umszAudytLm;
	}

	public String getUmszAudytUm() {
		return this.umszAudytUm;
	}

	public void setUmszAudytUm(String umszAudytUm) {
		this.umszAudytUm = umszAudytUm;
	}

	public String getUmszAudytUt() {
		return this.umszAudytUt;
	}

	public void setUmszAudytUt(String umszAudytUt) {
		this.umszAudytUt = umszAudytUt;
	}

	public String getUmszCzyUregulowano() {
		return this.umszCzyUregulowano;
	}

	public void setUmszCzyUregulowano(String umszCzyUregulowano) {
		this.umszCzyUregulowano = umszCzyUregulowano;
	}

	public Date getUmszDataObowiazywania() {
		return this.umszDataObowiazywania;
	}

	public void setUmszDataObowiazywania(Date umszDataObowiazywania) {
		this.umszDataObowiazywania = umszDataObowiazywania;
	}

	public Date getUmszDataPodpisania() {
		return this.umszDataPodpisania;
	}

	public void setUmszDataPodpisania(Date umszDataPodpisania) {
		this.umszDataPodpisania = umszDataPodpisania;
	}

	public Long getUmszPrcId() {
		return this.umszPrcId;
	}

	public void setUmszPrcId(Long umszPrcId) {
		this.umszPrcId = umszPrcId;
	}

	public String getUmszPrzedmiot() {
		return this.umszPrzedmiot;
	}

	public void setUmszPrzedmiot(String umszPrzedmiot) {
		this.umszPrzedmiot = umszPrzedmiot;
	}

	public String getUmszUwagi() {
		return this.umszUwagi;
	}

	public void setUmszUwagi(String umszUwagi) {
		this.umszUwagi = umszUwagi;
	}

	public Long getUmszWartosc() {
		return this.umszWartosc;
	}

	public void setUmszWartosc(Long umszWartosc) {
		this.umszWartosc = umszWartosc;
	}

	public String getUmszFCzyDyplom() {
		return umszFCzyDyplom;
	}

	public void setUmszFCzyDyplom(String umszFCzyDyplom) {
		this.umszFCzyDyplom = umszFCzyDyplom;
	}

}