package com.comarch.egeria.pp.szkolenia;

import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.szkolenia.model.OstPlanIndyw;
import com.comarch.egeria.pp.szkolenia.model.OstZapisySzkolen;
import com.comarch.egeria.pp.szkolenia.model.PptSzkSzkolenia;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import net.sf.jasperreports.engine.JRException;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.rowset.CachedRowSet;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;

//import org.primefaces.context.RequestContext;

@ManagedBean
@Named
@Scope("view")
public class ZapisySzkoleniaBean extends SqlBean {
	private static final long serialVersionUID = 1L;

	@Inject
	SessionBean sessionBean;

	@Inject
	User user;

	@Inject
	ObiegBean obieg;

	@Inject
	protected ReportGenerator ReportGenerator;

	private OstZapisySzkolen zapis;
	OstPlanIndyw currentPlan = null;
	private OstPlanIndyw currentPlanTmp = null;

	@PostConstruct
	public void init() {
		this.loadEgSlownik("OS_SZKOLENIA_POWSZECHNE");
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		zapis = new OstZapisySzkolen();

		if (id == null) {
			zapis.setZszPrcId(user.getPrcIdLong());
			newSzkolSpec();
		} else {
			wczytajZapis(id);
		}
	}

	public void raport(long pisId) {
			if (pisId == 0) {
				User.info("Nie wybrano żadnego szkolenia", FacesMessage.SEVERITY_WARN);
				return;
			}
			
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("PIS_ID", pisId);
				com.comarch.egeria.web.common.reports.ReportGenerator.displayReportAsDOCX(params, "Szkolenie_indywidualne");
			} catch (JRException | SQLException | IOException e) {
				User.alert(e.getMessage());
			}
		}

	
	public void spozaRejestruValueChangeListtener(ValueChangeEvent event){
		//System.out.println("spozaRejestruValueChangeListtener: " + event.getOldValue() + " -> " + newValue);
		Boolean newValue = (Boolean) event.getNewValue();
		if (newValue) { 
			newSzkolSpec();
			com.comarch.egeria.Utils.executeScript("PF('accOf').select(1);");
		} else {
			com.comarch.egeria.Utils.executeScript("PF('accOf').select(0);");
		}
	}

	public void zapisz() {
		saveSelectedPowszechneSzkol();

		if (!validateBeforeSave())
			return;

		try {
			this.zapis = (OstZapisySzkolen) HibernateContext.save(this.zapis);
//			User.info("Zapisano plan szkoleniowy [#%1$s]", this.zapis.getZszId());
			User.info("Dodano nową pozycję zapisu na szkolenie.");
		} catch (Exception e) {
			User.alert("Błąd w zapisie planu szkoleniowego!\n" + e);
			System.err.println(e);
		}
		long id = zapis.getZszId();

		if (id == 0)
			sessionBean.setViewEntityId(zapis.getZszId());// obsluga odsw.. F5

		wczytajZapis(zapis.getZszId());
	}

	public void usun() {
		try (Connection conn = JdbcUtils.getConnection(false)) {
			obieg.usunDaneObiegu(); // reloadPage

		} catch (SQLException e) {
			e.printStackTrace();
			User.alert("Nieudane usuniecie danych obiegu");
		}
		HibernateContext.delete(this.zapis);
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("MojeSzkolenia");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void zaznaczWczytaneSzkolenia() {
		HashSet<String> szkoleniaKeySet = this.getSzkoleniaKeySet();
		SqlDataSelectionsHandler powszechneRows = getSql("OS_SZKOLENIA_POWSZECHNE");
		powszechneRows.clearSelection();
		for (DataRow dr : powszechneRows.getData()) {
			if (szkoleniaKeySet.contains(dr.get(0))) {
				powszechneRows.setRowSelection(true, dr);
			}
		}
	}

	private HashSet<String> getSzkoleniaKeySet() {
		HashSet<String> szkoleniaKeySet = new HashSet<String>();
		if (zapis != null && zapis.getOstPlanIndyws() != null)
			for (OstPlanIndyw k : zapis.getOstPlanIndyws()) {
				szkoleniaKeySet.add(k.getPisSzkPowszech());
			}
		return szkoleniaKeySet;
	}

	public void newSzkolSpec() {
		this.currentPlan = null;
		this.currentPlanTmp = new OstPlanIndyw();
		this.currentPlanTmp.setPisPrcId(user.getPrcIdLong());
	}

	public void setCurrentSzkolSpec(OstPlanIndyw currentSzkolSpec) throws CloneNotSupportedException {
		this.currentPlan = currentSzkolSpec;
		this.currentPlanTmp = currentSzkolSpec.clone();
	}

	public void usunSzkolSpec(OstPlanIndyw ost) {
		zapis.removeOstPlanIndyw(ost);
	}

	private void wczytajZapis(Object id) {
		long lId = Long.parseLong(id + "");
		this.zapis = (OstZapisySzkolen) HibernateContext.get(OstZapisySzkolen.class, lId);
		obieg.setIdEncji(zapis.getZszId());
	}

	public void onCurrentTmpPisOfIdChanged() {
		if (this.currentPlanTmp == null)
			return;

		DataRow dr = getLW("PPL_OFERTY_SZKOL_NADCH").get(currentPlanTmp.getPisOfId());
		currentPlanTmp.setPisTemat((String) dr.get("of_nazwa"));
		currentPlanTmp.setPisTerminDo((Date) dr.get("szk_data_do"));
		currentPlanTmp.setPisTerminOd((Date) dr.get("szk_data_od"));
		currentPlanTmp.setPisMiejsceSzkol((String) dr.get("szk_lokalizacja"));
		currentPlanTmp.setPisSspeObszarWiedzy((String) dr.get("of_obszar_wiedzy"));
		currentPlanTmp.setPisSspeForma((String) dr.get("of_forma_org"));
		currentPlanTmp.setPisSzkPowszech((String) dr.get("of_czy_powszechne"));

		BigDecimal bdSzkId = (BigDecimal) dr.get("szk_id");
		if (bdSzkId != null) {
			this.currentPlanTmp.setPptSzkSzkolenia(
					(PptSzkSzkolenia) HibernateContext.get(PptSzkSzkolenia.class, bdSzkId.longValue()));
			// currentPlanTmp.setPisTemat(currentPlanTmp.getPptSzkSzkolenia().getPptOfertySzkolen().getOfNazwa());
			// currentPlanTmp.setPisTerminOd(currentPlanTmp.getPptSzkSzkolenia().getSzkDataOd());
			// currentPlanTmp.setPisTerminDo(currentPlanTmp.getPptSzkSzkolenia().getSzkDataDo());
		} else {
			this.currentPlanTmp.setPptSzkSzkolenia(null);
		}
	}

	public void zatwierdzCurrentTmp() {
		if (currentPlanTmp == null)
			return;
		
		if (!validateIPRZBeforeSave())
			return;
		
		if (currentPlanTmp.getPisFCzyRejestr() == "T" && !validateBeforeSaveSpozaRejestru())
			return;

		if (this.currentPlan == null) {
			this.zapis.addOstPlanIndyw(this.currentPlanTmp);
		} else {
			int indexOf = this.zapis.getOstPlanIndyws().indexOf(this.currentPlan);
			this.zapis.getOstPlanIndyws().set(indexOf, this.currentPlanTmp);
		}
		this.currentPlan = this.currentPlanTmp;
		if (currentPlanTmp.getPisFCzyRejestr() == "N") {
			validateCzyPlatneSzkolenie();
			validateSzkolenieSpecjalistyczne();
		} else {
			validateSpozaRejestru();
		}
		
		com.comarch.egeria.Utils.executeScript("PF('zapisNaSzkolenieSzczeg').hide();");
	}
	
	private boolean validateBeforeSaveSpozaRejestru() {
		ArrayList<String> inval = new ArrayList<String>();

		if (currentPlanTmp.getPisKoszt() == null) {
			inval.add("Koszt szkolenia jest polem wymaganym.");
		}

		for (String msg : inval) {
			User.alert(msg);
		}
		return inval.isEmpty();
	}
	
	private boolean validateIPRZBeforeSave() {
		ArrayList<String> inval = new ArrayList<String>();

		if (currentPlanTmp.getPisFCzyIprz() == null || currentPlanTmp.getPisFCzyIprz() == "") {
			inval.add("Czy szkolenie IPRZ jest polem wymaganym.");
		}

		for (String msg : inval) {
			User.alert(msg);
		}
		return inval.isEmpty();
	}
	
	private void validateCzyPlatneSzkolenie() {
		SqlDataSelectionsHandler szkolenia = this.getLW("PPL_SZK_OFERTY_SZKOLEN");
		DataRow wybraneSzkolenie = szkolenia.getCurrentRow();
		
		if (wybraneSzkolenie==null)
			return;
		
		if ("T".equals(wybraneSzkolenie.get("of_f_platne"))) {
			User.warn("Wybrano szkolenie płatne. Zapisz je, a następnie dodaj w załączniku ofertę szkolenia zawierającą program oraz ewentualne koszty szkolenia.");
		}
	}
	
	private void validateSpozaRejestru() {
		if (currentPlanTmp.getPisFCzyRejestr() == "T") {
			User.warn("Wybrano szkolenie spoza rejestru. Zapisz je, a następnie dodaj w załączniku ofertę szkolenia zawierającą program oraz ewentualne koszty szkolenia.");
		}
	}
	
	private void validateSzkolenieSpecjalistyczne() {
		SqlDataSelectionsHandler szkolenia = this.getLW("PPL_SZK_OFERTY_SZKOLEN");
		DataRow wybraneSzkolenie = szkolenia.getCurrentRow();
		
		if (wybraneSzkolenie==null)
			return;
		
		if ("N".equals(wybraneSzkolenie.get("of_czy_powszechne"))) {
			User.warn("Zapis na szkolenie spesjalistyczne powinien wpłynąć do BDG najpóżniej 3 tyg. przed rozpoczęciem szkolenia, w celu zapewnienia zabezpieczenia środków na ten cel.");
			if ("N".equals(wybraneSzkolenie.get("of_f_platne"))) {
				User.warn("Proszę o dodanie oferty szkolenia zawierającej program oraz ewentualne koszty szkolenia.");
			}
		}
	}

	private void saveSelectedPowszechneSzkol() {
		SqlDataSelectionsHandler szkoleniaAllRows = this.getSql("OS_SZKOLENIA_POWSZECHNE");
		if (szkoleniaAllRows != null) {
			List<DataRow> szokleniaSelectedRows = szkoleniaAllRows.getSelectedRows();
			HashSet<String> szkoleniaKeySet = getSzkoleniaKeySet();
			HashSet<String> szkoleniaRowsKeySet = new HashSet<String>();

			for (DataRow dr : szokleniaSelectedRows) {
				String szkolenie = "" + dr.get(0);
				szkoleniaRowsKeySet.add(szkolenie);
				if (!szkoleniaKeySet.contains(szkolenie)) {
					OstPlanIndyw k = new OstPlanIndyw();
					k.setPisSzkPowszech(szkolenie);
					k.setPisPrcId(zapis.getZszPrcId());
					Date dataRok = new Date(System.currentTimeMillis());
					k.setPisRok(dataRok);
					k.setPisDataZapisu(dataRok);

					zapis.addOstPlanIndyw(k);
				}
			}
		}
	}

	private boolean validateBeforeSave() {
		ArrayList<String> inval = new ArrayList<String>();

		try (Connection conn = JdbcUtils.getConnection()) {
			List<Object> params = new ArrayList<>();

			params.add(zapis.getZszPrcId());
			CachedRowSet crs = JdbcUtils.sqlExecQuery(conn,
					"select usz_id from PPT_SZK_UCZESTNICY_SZKOLEN where usz_prc_id = ? and USZ_DATA_WYPELNIENIA is null",
					params);

			if (crs != null) {
				if (crs.next()) {
					User.warn("Posiadasz niewypełnione ankiety szkoleniowe.");
				}
			}
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
			inval.add(e.getMessage());
		}

		if (zapis.getOstPlanIndyws() != null) {
			if (zapis.getOstPlanIndyws().isEmpty()) {
				inval.add("Nie wybrano żadnych szkoleń");
			} else {
				for (OstPlanIndyw opi : zapis.getOstPlanIndyws()) {
					if (opi.getPisSzkPowszech() == null || opi.getPisSzkPowszech().trim().equals("")) {
						validateSzkolIndyw(opi, inval);
					}
				}
			}
		}
		for (String msg : inval) {
			User.alert(msg);
		}
		return inval.isEmpty();
	}

	private void validateSzkolIndyw(OstPlanIndyw opi, ArrayList<String> inval) {
		if (opi.getPisFCzyRejestr().equals("N")) {
			if (opi.getPisSspeForma() == null || opi.getPisSspeForma().trim().equals("")) {
				inval.add("Szkolenie specjalne musi mieć określoną formę. ");
			}

			if (opi.getPisSspeObszarWiedzy() == null || opi.getPisSspeObszarWiedzy().trim().equals("")) {
				inval.add("Szkolenie specjalne musi mieć określony obszar wiedzy. ");
			}
		}

		if (opi.getPisUzasadnienie() != null && opi.getPisUzasadnienie().length() > 500) {
			inval.add("Uzasadnienie nie może przekraczać 500 znaków. ");
		}
	}

	public OstZapisySzkolen getZapis() {
		return zapis;
	}

	public void setZapis(OstZapisySzkolen zapis) {
		this.zapis = zapis;
	}

	public OstPlanIndyw getCurrentPlanTmp() {
		return currentPlanTmp;
	}

	public void setCurrentPlanTmp(OstPlanIndyw currentPlanTmp) {
		this.currentPlanTmp = currentPlanTmp;
	}

	
	
	public int getAccIndex() {
		try {

			if (this.currentPlan==null
				|| "T".equals( this.currentPlan.getPisFCzyRejestr() ) 
				|| this.currentPlan.getPisId()==0 )
				return 0;
			else
				return 1;
			
		} catch (Exception e) {
			throw e;
		}
	}

	public void setAccIndex(int accIndex) {
		//nic
	}

	//<!-- activeIndex="#{zapisySzkoleniaBean.ustawIndex(zapisySzkoleniaBean.currentPlanTmp.pisFCzyRejestr,zapisySzkoleniaBean.currentPlanTmp.pisId)}" -->
//	public int ustawIndex (Object czyRejestr, Object pisId) {
//		int tabIndex = 0;
//		if(!nz((String)czyRejestr).equals("T") && nz((Long)pisId) != 0) {
//			tabIndex = 1;
//		}
//		return tabIndex;
//	}
	
}
