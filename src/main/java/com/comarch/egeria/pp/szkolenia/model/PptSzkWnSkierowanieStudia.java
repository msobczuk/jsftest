package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_SZK_WN_SKIEROWANIE_STUDIA database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_WN_SKIEROWANIE_STUDIA", schema="PPADM")
@NamedQuery(name="PptSzkWnSkierowanieStudia.findAll", query="SELECT o FROM PptSzkWnSkierowanieStudia o")
public class PptSzkWnSkierowanieStudia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_WN_SKIEROWANIE_STUDIA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_WN_SKIEROWANIE_STUDIA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_WN_SKIEROWANIE_STUDIA")
	@Column(name="SKST_ID")
	private long skstId;

	@Temporal(TemporalType.DATE)
	@Column(name="SKST_AUDYT_DM")
	private Date skstAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="SKST_AUDYT_DT")
	private Date skstAudytDt;

	@Column(name="SKST_AUDYT_KM")
	private String skstAudytKm;

	@Column(name="SKST_AUDYT_LM")
	private String skstAudytLm;

	@Column(name="SKST_AUDYT_UM")
	private String skstAudytUm;

	@Column(name="SKST_AUDYT_UT")
	private String skstAudytUt;

	@Column(name="SKST_F_CZY_POKRYCIE")
	private String skstFCzyPokrycie;

	@Column(name="SKST_FORMA")
	private String skstForma;

	@Column(name="SKST_INNE_KOSZTY")
	private String skstInneKoszty;

	@Column(name="SKST_KOSZT")
	private BigDecimal skstKoszt;

	@Column(name="SKST_KWOTA_INNE_KOSZTY")
	private BigDecimal skstKwotaInneKoszty;

	@Column(name="SKST_KWOTA_POKRYCIE")
	private BigDecimal skstKwotaPokrycie;

	@Column(name="SKST_OPINIA")
	private String skstOpinia;

	@Column(name="SKST_ORGANIZATOR")
	private String skstOrganizator;

	@Column(name="SKST_PRC_ID")
	private Long skstPrcId;

	@Column(name="SKST_TEMAT")
	private String skstTemat;

	@Temporal(TemporalType.DATE)
	@Column(name="SKST_URLOP_BEZPL_DO")
	private Date skstUrlopBezplDo;

	@Temporal(TemporalType.DATE)
	@Column(name="SKST_URLOP_BEZPL_OD")
	private Date skstUrlopBezplOd;

	@Column(name="SKST_URLOP_PRACA")
	private Long skstUrlopPraca;

	@Column(name="SKST_URLOP_PRZYGOT")
	private Long skstUrlopPrzygot;

	@Temporal(TemporalType.DATE)
	@Column(name="SKST_URLOP_SZKOL_DO")
	private Date skstUrlopSzkolDo;

	@Temporal(TemporalType.DATE)
	@Column(name="SKST_URLOP_SZKOL_OD")
	private Date skstUrlopSzkolOd;

	@Column(name="SKST_URLOP_ZAJECIA")
	private Long skstUrlopZajecia;

	@Column(name="SKST_UZASADNIANIE")
	private String skstUzasadnianie;

	public PptSzkWnSkierowanieStudia() {
	}

	public long getSkstId() {
		return this.skstId;
	}

	public void setSkstId(long skstId) {
		this.skstId = skstId;
	}

	public Date getSkstAudytDm() {
		return this.skstAudytDm;
	}

	public void setSkstAudytDm(Date skstAudytDm) {
		this.skstAudytDm = skstAudytDm;
	}

	public Date getSkstAudytDt() {
		return this.skstAudytDt;
	}

	public void setSkstAudytDt(Date skstAudytDt) {
		this.skstAudytDt = skstAudytDt;
	}

	public String getSkstAudytKm() {
		return this.skstAudytKm;
	}

	public void setSkstAudytKm(String skstAudytKm) {
		this.skstAudytKm = skstAudytKm;
	}

	public String getSkstAudytLm() {
		return this.skstAudytLm;
	}

	public void setSkstAudytLm(String skstAudytLm) {
		this.skstAudytLm = skstAudytLm;
	}

	public String getSkstAudytUm() {
		return this.skstAudytUm;
	}

	public void setSkstAudytUm(String skstAudytUm) {
		this.skstAudytUm = skstAudytUm;
	}

	public String getSkstAudytUt() {
		return this.skstAudytUt;
	}

	public void setSkstAudytUt(String skstAudytUt) {
		this.skstAudytUt = skstAudytUt;
	}

	public String getSkstFCzyPokrycie() {
		return this.skstFCzyPokrycie;
	}

	public void setSkstFCzyPokrycie(String skstFCzyPokrycie) {
		this.skstFCzyPokrycie = skstFCzyPokrycie;
	}

	public String getSkstForma() {
		return this.skstForma;
	}

	public void setSkstForma(String skstForma) {
		this.skstForma = skstForma;
	}

	public String getSkstInneKoszty() {
		return this.skstInneKoszty;
	}

	public void setSkstInneKoszty(String skstInneKoszty) {
		this.skstInneKoszty = skstInneKoszty;
	}

	public BigDecimal getSkstKoszt() {
		return this.skstKoszt;
	}

	public void setSkstKoszt(BigDecimal skstKoszt) {
		this.skstKoszt = skstKoszt;
	}

	public BigDecimal getSkstKwotaInneKoszty() {
		return this.skstKwotaInneKoszty;
	}

	public void setSkstKwotaInneKoszty(BigDecimal skstKwotaInneKoszty) {
		this.skstKwotaInneKoszty = skstKwotaInneKoszty;
	}

	public BigDecimal getSkstKwotaPokrycie() {
		return this.skstKwotaPokrycie;
	}

	public void setSkstKwotaPokrycie(BigDecimal skstKwotaPokrycie) {
		this.skstKwotaPokrycie = skstKwotaPokrycie;
	}

	public String getSkstOpinia() {
		return this.skstOpinia;
	}

	public void setSkstOpinia(String skstOpinia) {
		this.skstOpinia = skstOpinia;
	}

	public String getSkstOrganizator() {
		return this.skstOrganizator;
	}

	public void setSkstOrganizator(String skstOrganizator) {
		this.skstOrganizator = skstOrganizator;
	}

	public Long getSkstPrcId() {
		return this.skstPrcId;
	}

	public void setSkstPrcId(Long skstPrcId) {
		this.skstPrcId = skstPrcId;
	}

	public String getSkstTemat() {
		return this.skstTemat;
	}

	public void setSkstTemat(String skstTemat) {
		this.skstTemat = skstTemat;
	}

	public Date getSkstUrlopBezplDo() {
		return this.skstUrlopBezplDo;
	}

	public void setSkstUrlopBezplDo(Date skstUrlopBezplDo) {
		this.skstUrlopBezplDo = skstUrlopBezplDo;
	}

	public Date getSkstUrlopBezplOd() {
		return this.skstUrlopBezplOd;
	}

	public void setSkstUrlopBezplOd(Date skstUrlopBezplOd) {
		this.skstUrlopBezplOd = skstUrlopBezplOd;
	}

	public Long getSkstUrlopPraca() {
		return this.skstUrlopPraca;
	}

	public void setSkstUrlopPraca(Long skstUrlopPraca) {
		this.skstUrlopPraca = skstUrlopPraca;
	}

	public Long getSkstUrlopPrzygot() {
		return this.skstUrlopPrzygot;
	}

	public void setSkstUrlopPrzygot(Long skstUrlopPrzygot) {
		this.skstUrlopPrzygot = skstUrlopPrzygot;
	}

	public Date getSkstUrlopSzkolDo() {
		return this.skstUrlopSzkolDo;
	}

	public void setSkstUrlopSzkolDo(Date skstUrlopSzkolDo) {
		this.skstUrlopSzkolDo = skstUrlopSzkolDo;
	}

	public Date getSkstUrlopSzkolOd() {
		return this.skstUrlopSzkolOd;
	}

	public void setSkstUrlopSzkolOd(Date skstUrlopSzkolOd) {
		this.skstUrlopSzkolOd = skstUrlopSzkolOd;
	}

	public Long getSkstUrlopZajecia() {
		return this.skstUrlopZajecia;
	}

	public void setSkstUrlopZajecia(Long skstUrlopZajecia) {
		this.skstUrlopZajecia = skstUrlopZajecia;
	}

	public String getSkstUzasadnianie() {
		return this.skstUzasadnianie;
	}

	public void setSkstUzasadnianie(String skstUzasadnianie) {
		this.skstUzasadnianie = skstUzasadnianie;
	}

}