package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_SZK_KOSZTY_SZKOLEN database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_KOSZTY_SZKOLEN", schema="PPADM")
@NamedQuery(name="PptKosztySzkolen.findAll", query="SELECT p FROM PptKosztySzkolen p")
public class PptKosztySzkolen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_KOSZTY_SZKOLEN",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_KOSZTY_SZKOLEN", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_KOSZTY_SZKOLEN")
	@Column(name="PKS_ID")
	private Long pksId;

	@Temporal(TemporalType.DATE)
	@Column(name="PKS_AUDYT_DM")
	private Date pksAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="PKS_AUDYT_DT")
	private Date pksAudytDt;

	@Column(name="PKS_AUDYT_KM")
	private String pksAudytKm;

	@Column(name="PKS_AUDYT_LM")
	private String pksAudytLm;

	@Column(name="PKS_AUDYT_UM")
	private String pksAudytUm;

	@Column(name="PKS_AUDYT_UT")
	private String pksAudytUt;

	@Column(name="PKS_FORMA")
	private String pksForma;

	@Column(name="PKS_KOSZT")
	private BigDecimal pksKoszt;

	@Column(name="PKS_RODZAJ")
	private String pksRodzaj;

	//bi-directional many-to-one association to PptSzkolenia
	@ManyToOne
	@JoinColumn(name="PKS_SZK_ID")
	private PptSzkSzkolenia pptSzkolenia;

	public PptKosztySzkolen() {
	}

	public Long getPksId() {
		return this.pksId;
	}

	public void setPksId(Long pksId) {
		this.pksId = pksId;
	}

	public Date getPksAudytDm() {
		return this.pksAudytDm;
	}

	public void setPksAudytDm(Date pksAudytDm) {
		this.pksAudytDm = pksAudytDm;
	}

	public Date getPksAudytDt() {
		return this.pksAudytDt;
	}

	public void setPksAudytDt(Date pksAudytDt) {
		this.pksAudytDt = pksAudytDt;
	}

	public String getPksAudytKm() {
		return this.pksAudytKm;
	}

	public void setPksAudytKm(String pksAudytKm) {
		this.pksAudytKm = pksAudytKm;
	}

	public String getPksAudytLm() {
		return this.pksAudytLm;
	}

	public void setPksAudytLm(String pksAudytLm) {
		this.pksAudytLm = pksAudytLm;
	}

	public String getPksAudytUm() {
		return this.pksAudytUm;
	}

	public void setPksAudytUm(String pksAudytUm) {
		this.pksAudytUm = pksAudytUm;
	}

	public String getPksAudytUt() {
		return this.pksAudytUt;
	}

	public void setPksAudytUt(String pksAudytUt) {
		this.pksAudytUt = pksAudytUt;
	}

	public String getPksForma() {
		return this.pksForma;
	}

	public void setPksForma(String pksForma) {
		this.pksForma = pksForma;
	}

	public BigDecimal getPksKoszt() {
		return this.pksKoszt;
	}

	public void setPksKoszt(BigDecimal pksKoszt) {
		this.pksKoszt = pksKoszt;
	}

	public String getPksRodzaj() {
		return this.pksRodzaj;
	}

	public void setPksRodzaj(String pksRodzaj) {
		this.pksRodzaj = pksRodzaj;
	}

	public PptSzkSzkolenia getPptSzkolenia() {
		return this.pptSzkolenia;
	}

	public void setPptSzkolenia(PptSzkSzkolenia pptSzkolenia) {
		this.pptSzkolenia = pptSzkolenia;
	}

}