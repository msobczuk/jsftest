package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


/**
 * The persistent class for the PPT_SZK_OFERTY_SZKOLEN database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_OFERTY_SZKOLEN", schema="PPADM")
@NamedQuery(name="PptOfertySzkolen.findAll", query="SELECT p FROM PptOfertySzkolen p")
public class PptOfertySzkolen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_OFERTY_SZKOLEN",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_OFERTY_SZKOLEN", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_OFERTY_SZKOLEN")
	@Column(name="OF_ID")
	private Long ofId;

	@Temporal(TemporalType.DATE)
	@Column(name="OF_AUDYT_DM")
	private Date ofAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="OF_AUDYT_DT")
	private Date ofAudytDt;

	@Column(name="OF_AUDYT_KM")
	private String ofAudytKm;

	@Column(name="OF_AUDYT_LM")
	private String ofAudytLm;

	@Column(name="OF_AUDYT_UM")
	private String ofAudytUm;

	@Column(name="OF_AUDYT_UT")
	private String ofAudytUt;

	@Column(name="OF_OBSZAR_WIEDZY")
	private String ofObszarWiedzy;
	
	@Column(name="OF_F_WEW")
	private String ofFWew;

	@Column(name="OF_FORMA_ORG")
	private String ofFormaOrg;

	@Column(name="OF_FORMA_ZAL")
	private String ofFormaZal;

	@Column(name="OF_ILOSC_DNI")
	private Long ofIloscDni;

	@Column(name="OF_ILOSC_GODZIN")
	private Long ofIloscGodzin;

	@Column(name="OF_KATEGORIA")
	private String ofKategoria;

	@Column(name="OF_NAZWA")
	private String ofNazwa;

	@Column(name="OF_OPIS")
	private String ofOpis;

	@Column(name="OF_RODZAJ")
	private String ofRodzaj;
	
	@Column(name="OF_SKROT")
	private String ofSkrot;

	@Column(name="OF_TYP")
	private String ofTyp;
	
	@Column(name="OF_CZY_POWSZECHNE")
	private String ofCzyPowszechne;
	
	@Column(name="OF_F_CZY_NIE_SC")
	private String ofFCzyNieSc = "N";
	
	@Column(name="OF_F_PLATNE")
	private String ofFCzyPlatne;

	//bi-directional many-to-one association to PptSzkolenia
	@OneToMany(mappedBy="pptOfertySzkolen", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<PptSzkSzkolenia> pptSzkolenias = new ArrayList<>();

	public PptOfertySzkolen() {
	}

	public Long getOfId() {
		return this.ofId;
	}

	public void setOfId(Long ofId) {
		this.ofId = ofId;
	}

	public Date getOfAudytDm() {
		return this.ofAudytDm;
	}

	public void setOfAudytDm(Date ofAudytDm) {
		this.ofAudytDm = ofAudytDm;
	}

	public Date getOfAudytDt() {
		return this.ofAudytDt;
	}

	public void setOfAudytDt(Date ofAudytDt) {
		this.ofAudytDt = ofAudytDt;
	}

	public String getOfAudytKm() {
		return this.ofAudytKm;
	}

	public void setOfAudytKm(String ofAudytKm) {
		this.ofAudytKm = ofAudytKm;
	}

	public String getOfAudytLm() {
		return this.ofAudytLm;
	}

	public void setOfAudytLm(String ofAudytLm) {
		this.ofAudytLm = ofAudytLm;
	}

	public String getOfAudytUm() {
		return this.ofAudytUm;
	}

	public void setOfAudytUm(String ofAudytUm) {
		this.ofAudytUm = ofAudytUm;
	}

	public String getOfAudytUt() {
		return this.ofAudytUt;
	}

	public void setOfAudytUt(String ofAudytUt) {
		this.ofAudytUt = ofAudytUt;
	}

	public String getOfObszarWiedzy() {
		return ofObszarWiedzy;
	}

	public void setOfObszarWiedzy(String ofObszarWiedzy) {
		this.ofObszarWiedzy = ofObszarWiedzy;
	}
	
	public String getOfFWew() {
		return this.ofFWew;
	}

	public void setOfFWew(String ofFWew) {
		this.ofFWew = ofFWew;
	}

	public String getOfFormaOrg() {
		return this.ofFormaOrg;
	}

	public void setOfFormaOrg(String ofFormaOrg) {
		this.ofFormaOrg = ofFormaOrg;
	}

	public String getOfFormaZal() {
		return this.ofFormaZal;
	}

	public void setOfFormaZal(String ofFormaZal) {
		this.ofFormaZal = ofFormaZal;
	}

	public Long getOfIloscDni() {
		return this.ofIloscDni;
	}

	public void setOfIloscDni(Long ofIloscDni) {
		this.ofIloscDni = ofIloscDni;
	}

	public Long getOfIloscGodzin() {
		return this.ofIloscGodzin;
	}

	public void setOfIloscGodzin(Long ofIloscGodzin) {
		this.ofIloscGodzin = ofIloscGodzin;
	}

	public String getOfKategoria() {
		return this.ofKategoria;
	}

	public void setOfKategoria(String ofKategoria) {
		this.ofKategoria = ofKategoria;
	}

	public String getOfNazwa() {
		return this.ofNazwa;
	}

	public void setOfNazwa(String ofNazwa) {
		this.ofNazwa = ofNazwa;
	}

	public String getOfOpis() {
		return this.ofOpis;
	}

	public void setOfOpis(String ofOpis) {
		this.ofOpis = ofOpis;
	}

	public String getOfRodzaj() {
		return this.ofRodzaj;
	}

	public void setOfRodzaj(String ofRodzaj) {
		this.ofRodzaj = ofRodzaj;
	}
	
	public String getOfSkrot() {
		return ofSkrot;
	}

	public void setOfSkrot(String ofSkrot) {
		this.ofSkrot = ofSkrot;
	}

	public String getOfTyp() {
		return this.ofTyp;
	}

	public void setOfTyp(String ofTyp) {
		this.ofTyp = ofTyp;
	}

	public List<PptSzkSzkolenia> getPptSzkolenias() {
		return this.pptSzkolenias;
	}

	public void setPptSzkolenias(List<PptSzkSzkolenia> pptSzkolenias) {
		this.pptSzkolenias = pptSzkolenias;
	}

	public PptSzkSzkolenia addPptSzkolenia(PptSzkSzkolenia pptSzkolenia) {
		getPptSzkolenias().add(pptSzkolenia);
		pptSzkolenia.setPptOfertySzkolen(this);

		return pptSzkolenia;
	}

	public PptSzkSzkolenia removePptSzkolenia(PptSzkSzkolenia pptSzkolenia) {
		getPptSzkolenias().remove(pptSzkolenia);
		pptSzkolenia.setPptOfertySzkolen(null);

		return pptSzkolenia;
	}
	
	public String getOfCzyPowszechne() {
		return ofCzyPowszechne;
	}

	public void setOfCzyPowszechne(String ofCzyPowszechne) {
		this.ofCzyPowszechne = ofCzyPowszechne;
	}

	public String getOfFCzyNieSc() {
		return ofFCzyNieSc;
	}

	public void setOfFCzyNieSc(String ofFCzyNieSc) {
		this.ofFCzyNieSc = ofFCzyNieSc;
	}

	public String getOfFCzyPlatne() {
		return ofFCzyPlatne;
	}

	public void setOfFCzyPlatne(String ofFCzyPlatne) {
		this.ofFCzyPlatne = ofFCzyPlatne;
	}


}