package com.comarch.egeria.pp.szkolenia.model;

import static com.comarch.egeria.Utils.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;



/**
 * The persistent class for the PPT_SZK_SZKOLENIA database table.
 * 
 */
@Entity
@Table(name = "PPT_SZK_SZKOLENIA")
@NamedQuery(name = "PptSzkSzkolenia.findAll", query = "SELECT p FROM PptSzkSzkolenia p")
public class PptSzkSzkolenia implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	public List<Long> wykladowcyToSwykIdList(){
		return getPptSzkWykladowcySzkolenies().stream().map(w-> w.getPptSzkWykladowcy().getSwykId()).collect(Collectors.toList());
	}
	
	
	public List<Long> uczestnicyToPrcIdList(){
		return  this.getPptUczestnicySzkolens().stream().map(u-> u.getUszPrcId()).collect(Collectors.toList());
	}
	
	
	
	public void recalcProporcjoanlnieKoszty(){
		calcProporcjonalnieKosztRzeczywisty();
		calcProporcjonalnieKosztPlanowany();
	}
	
	
	public void calcProporcjonalnieKosztRzeczywisty(){
		Double kosztRzeczywistyTotal = nz(this.getSzkKwota()).doubleValue();
		
		this.getPptUczestnicySzkolens().stream().forEach(u -> u.setUszKosztRzecz(null));

		if (this.getPptUczestnicyNierezerwowi().isEmpty())
			return;
		
		int ileUczestnikow = this.getPptUczestnicyNierezerwowi().size();
		Double kosztRzeczywistyPrc = round(kosztRzeczywistyTotal / ileUczestnikow, 2);
		
		
		
		this.getPptUczestnicyNierezerwowi().stream().forEach(u -> u.setUszKosztRzecz(kosztRzeczywistyPrc));
		
		Double resztka = kosztRzeczywistyTotal - (kosztRzeczywistyPrc * ileUczestnikow);
		if (resztka==0.0){
			return;
		}else {
			PptUczestnicySzkolen pechowiec = this.getPptUczestnicyNierezerwowi().get(ileUczestnikow-1);
			pechowiec.setUszKosztRzecz(pechowiec.getUszKosztRzecz() + resztka);
		}
	}
	
	
	public void calcProporcjonalnieKosztPlanowany(){
		Double kosztPlanowanyTotal = nz(this.getSzkKosztPlJPod()).doubleValue() + nz(this.getSzkKosztPlDost()).doubleValue();
		
		this.getPptUczestnicySzkolens().stream().forEach(u -> u.setUszKosztPlan(null));

		if (this.getPptUczestnicyNierezerwowi().isEmpty())
			return;
		
		int ileUczestnikow = this.getPptUczestnicyNierezerwowi().size();
		Double kosztPlanowanyPrc = round(kosztPlanowanyTotal / ileUczestnikow, 2);
		
		this.getPptUczestnicyNierezerwowi().stream().forEach(u -> u.setUszKosztPlan(kosztPlanowanyPrc));
		
		Double resztka = kosztPlanowanyTotal - (kosztPlanowanyPrc * ileUczestnikow);
		if (resztka==0.0){
			return;
		}else {
			PptUczestnicySzkolen pechowiec = this.getPptUczestnicyNierezerwowi().get(ileUczestnikow-1);
			pechowiec.setUszKosztPlan(pechowiec.getUszKosztPlan() + resztka);
		}
	}	
	

	
	
	
	@Id
	@TableGenerator(name = "TABLE_KEYGEN_PPT_SZK_SZKOLENIA", table = "PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_SZKOLENIA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "TABLE_KEYGEN_PPT_SZK_SZKOLENIA")
	@Column(name = "SZK_ID")
	private Long szkId;

	@Column(name = "SZK_ADRES")
	private String szkAdres;

	@Temporal(TemporalType.DATE)
	@Column(name = "SZK_AUDYT_DM")
	private Date szkAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name = "SZK_AUDYT_DT")
	private Date szkAudytDt;

	@Column(name = "SZK_AUDYT_KM")
	private String szkAudytKm;

	@Column(name = "SZK_AUDYT_LM")
	private String szkAudytLm;

	@Column(name = "SZK_AUDYT_UM")
	private String szkAudytUm;

	@Column(name = "SZK_AUDYT_UT")
	private String szkAudytUt;

	@Temporal(TemporalType.DATE)
	@Column(name = "SZK_DATA_DO")
	private Date szkDataDo;

	@Temporal(TemporalType.DATE)
	@Column(name = "SZK_DATA_OD")
	private Date szkDataOd;

	@Temporal(TemporalType.DATE)
	@Column(name = "SZK_DATA_WAZNOSCI")
	private Date szkDataWaznosci;
	
	@Column(name = "SZK_FORMA_ROZL")
	private String szkFormaRozl;

	@Column(name = "SZK_KOSZT_PL_DOST")
	private BigDecimal szkKosztPlDost;

	@Column(name = "SZK_KOSZT_PL_J_POD")
	private BigDecimal szkKosztPlJPod;

	@Column(name = "SZK_KWOTA")
	private BigDecimal szkKwota;

	@Column(name = "SZK_LOKALIZACJA")
	private String szkLokalizacja;

	@Column(name = "SZK_MAX_UCZESTNIKOW")
	private Long szkMaxUczestnikow;

	@Column(name = "SZK_MIN_UCZESTNIKOW")
	private Long szkMinUczestnikow;

	@Column(name = "SZK_NAZWA")
	private String szkNazwa;

	@Column(name = "SZK_NAZWA_OPIS")
	private String szkNazwaOpis;

	@Column(name = "SZK_OPIS")
	private String szkOpis;

	@Column(name = "SZK_PROWADZACY")
	private String szkProwadzacy;

	@Column(name = "SZK_RODZAJ")
	private String szkRodzaj;

	@Column(name = "SZK_STATUS")
	private String szkStatus;
	
	@Column(name = "SZK_KL_KOD")
	private Long szkKlKod;
	
	@Column(name = "SZK_SZK_KOD")
	private String szkSzkKod;

	// bi-directional many-to-one association to PptSzkWykladowcySzkolenie
	@OneToMany(mappedBy = "pptSzkSzkolenia", cascade = { CascadeType.ALL }, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<PptSzkWykladowcySzkolenie> pptSzkWykladowcySzkolenies = new ArrayList<>();

	// bi-directional many-to-one association to PptKosztySzkolen
	@OneToMany(mappedBy = "pptSzkolenia", cascade = { CascadeType.ALL }, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<PptKosztySzkolen> pptKosztySzkolens = new ArrayList<>();

	// bi-directional many-to-one association to PptUczestnicySzkolen
	@OneToMany(mappedBy = "pptSzkolenia", cascade = { CascadeType.ALL }, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("USZ_ID")
	private List<PptUczestnicySzkolen> pptUczestnicySzkolens = new ArrayList<>();

	// bi-directional many-to-one association to PptOfertySzkolen
	@ManyToOne
	@JoinColumn(name = "SZK_OF_ID")
	private PptOfertySzkolen pptOfertySzkolen;
	
	
	
	//bi-directional many-to-one association to PptSzkPlanIndyw
	@OneToMany(mappedBy="pptSzkSzkolenia",  cascade = { CascadeType.ALL }) // , orphanRemoval = true
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<OstPlanIndyw> pptSzkPlanIndyws = new ArrayList<>();

	public PptSzkSzkolenia() {
	}

	public Long getSzkId() {
		return this.szkId;
	}

	public void setSzkId(Long szkId) {
		this.szkId = szkId;
	}

	public String getSzkAdres() {
		return this.szkAdres;
	}

	public void setSzkAdres(String szkAdres) {
		this.szkAdres = szkAdres;
	}

	public Date getSzkAudytDm() {
		return this.szkAudytDm;
	}

	public void setSzkAudytDm(Date szkAudytDm) {
		this.szkAudytDm = szkAudytDm;
	}

	public Date getSzkAudytDt() {
		return this.szkAudytDt;
	}

	public void setSzkAudytDt(Date szkAudytDt) {
		this.szkAudytDt = szkAudytDt;
	}

	public String getSzkAudytKm() {
		return this.szkAudytKm;
	}

	public void setSzkAudytKm(String szkAudytKm) {
		this.szkAudytKm = szkAudytKm;
	}

	public String getSzkAudytLm() {
		return this.szkAudytLm;
	}

	public void setSzkAudytLm(String szkAudytLm) {
		this.szkAudytLm = szkAudytLm;
	}

	public String getSzkAudytUm() {
		return this.szkAudytUm;
	}

	public void setSzkAudytUm(String szkAudytUm) {
		this.szkAudytUm = szkAudytUm;
	}

	public String getSzkAudytUt() {
		return this.szkAudytUt;
	}

	public void setSzkAudytUt(String szkAudytUt) {
		this.szkAudytUt = szkAudytUt;
	}

	public Date getSzkDataDo() {
		return this.szkDataDo;
	}

	public void setSzkDataDo(Date szkDataDo) {
		this.szkDataDo = szkDataDo;
	}

	public Date getSzkDataOd() {
		return this.szkDataOd;
	}

	public void setSzkDataOd(Date szkDataOd) {
		this.szkDataOd = szkDataOd;
	}

	public Date getSzkDataWaznosci() {
		return szkDataWaznosci;
	}

	public void setSzkDataWaznosci(Date szkDataWaznosci) {
		this.szkDataWaznosci = szkDataWaznosci;
	}
	
	public String getSzkFormaRozl() {
		return this.szkFormaRozl;
	}

	public void setSzkFormaRozl(String szkFormaRozl) {
		this.szkFormaRozl = szkFormaRozl;
	}

	public BigDecimal getSzkKosztPlDost() {
		return this.szkKosztPlDost;
	}

	public void setSzkKosztPlDost(BigDecimal szkKosztPlDost) {
		this.szkKosztPlDost = szkKosztPlDost;
	}

	public BigDecimal getSzkKosztPlJPod() {
		return this.szkKosztPlJPod;
	}

	public void setSzkKosztPlJPod(BigDecimal szkKosztPlJPod) {
		this.szkKosztPlJPod = szkKosztPlJPod;
	}

	public BigDecimal getSzkKwota() {
		return this.szkKwota;
	}

	public void setSzkKwota(BigDecimal szkKwota) {
		this.szkKwota = szkKwota;
	}

	public String getSzkLokalizacja() {
		return this.szkLokalizacja;
	}

	public void setSzkLokalizacja(String szkLokalizacja) {
		this.szkLokalizacja = szkLokalizacja;
	}

	public Long getSzkMaxUczestnikow() {
		return this.szkMaxUczestnikow;
	}

	public void setSzkMaxUczestnikow(Long szkMaxUczestnikow) {
		this.szkMaxUczestnikow = szkMaxUczestnikow;
	}

	public Long getSzkMinUczestnikow() {
		return this.szkMinUczestnikow;
	}

	public void setSzkMinUczestnikow(Long szkMinUczestnikow) {
		this.szkMinUczestnikow = szkMinUczestnikow;
	}

	public String getSzkNazwa() {
		return this.szkNazwa;
	}

	public void setSzkNazwa(String szkNazwa) {
		this.szkNazwa = szkNazwa;
	}

	public String getSzkNazwaOpis() {
		return this.szkNazwaOpis;
	}

	public void setSzkNazwaOpis(String szkNazwaOpis) {
		this.szkNazwaOpis = szkNazwaOpis;
	}

	public PptOfertySzkolen getPptOfertySzkolen() {
		return this.pptOfertySzkolen;
	}

	public void setPptOfertySzkolen(PptOfertySzkolen pptOfertySzkolen) {
		this.pptOfertySzkolen = pptOfertySzkolen;
	}

	public String getSzkOpis() {
		return this.szkOpis;
	}

	public void setSzkOpis(String szkOpis) {
		this.szkOpis = szkOpis;
	}

	public String getSzkProwadzacy() {
		return this.szkProwadzacy;
	}

	public void setSzkProwadzacy(String szkProwadzacy) {
		this.szkProwadzacy = szkProwadzacy;
	}

	public String getSzkRodzaj() {
		return this.szkRodzaj;
	}

	public void setSzkRodzaj(String szkRodzaj) {
		this.szkRodzaj = szkRodzaj;
	}

	public String getSzkStatus() {
		return this.szkStatus;
	}

	public void setSzkStatus(String szkStatus) {
		this.szkStatus = szkStatus;
	}
	
	public Long getSzkKlKod() {
		return this.szkKlKod;
	}

	public void setSzkKlKod(Long szkKlKod) {
		this.szkKlKod = szkKlKod;
	}
	
	public String getSzkSzkKod() {
		return szkSzkKod;
	}

	public void setSzkSzkKod(String szkSzkKod) {
		this.szkSzkKod = szkSzkKod;
	}

	public List<PptSzkWykladowcySzkolenie> getPptSzkWykladowcySzkolenies() {
		return this.pptSzkWykladowcySzkolenies;
	}

	public void setPptSzkWykladowcySzkolenies(List<PptSzkWykladowcySzkolenie> pptSzkWykladowcySzkolenies) {
		this.pptSzkWykladowcySzkolenies = pptSzkWykladowcySzkolenies;
	}

	public PptSzkWykladowcySzkolenie addPptSzkWykladowcySzkoleny(PptSzkWykladowcySzkolenie pptSzkWykladowcySzkoleny) {
		getPptSzkWykladowcySzkolenies().add(pptSzkWykladowcySzkoleny);
		pptSzkWykladowcySzkoleny.setPptSzkSzkolenia(this);

		return pptSzkWykladowcySzkoleny;
	}

	public PptSzkWykladowcySzkolenie removePptSzkWykladowcySzkoleny(
			PptSzkWykladowcySzkolenie pptSzkWykladowcySzkoleny) {
		getPptSzkWykladowcySzkolenies().remove(pptSzkWykladowcySzkoleny);
		pptSzkWykladowcySzkoleny.setPptSzkSzkolenia(null);

		return pptSzkWykladowcySzkoleny;
	}

	public List<PptKosztySzkolen> getPptKosztySzkolens() {
		return this.pptKosztySzkolens;
	}

	public void setPptKosztySzkolens(List<PptKosztySzkolen> pptKosztySzkolens) {
		this.pptKosztySzkolens = pptKosztySzkolens;
	}

	public PptKosztySzkolen addPptKosztySzkolen(PptKosztySzkolen pptKosztySzkolen) {
		getPptKosztySzkolens().add(pptKosztySzkolen);
		pptKosztySzkolen.setPptSzkolenia(this);

		return pptKosztySzkolen;
	}

	public PptKosztySzkolen removePptKosztySzkolen(PptKosztySzkolen pptKosztySzkolen) {
		getPptKosztySzkolens().remove(pptKosztySzkolen);
		pptKosztySzkolen.setPptSzkolenia(null);

		return pptKosztySzkolen;
	}

	public List<PptUczestnicySzkolen> getPptUczestnicySzkolens() {
		return this.pptUczestnicySzkolens;
	}
	
	
	public List<PptUczestnicySzkolen> getPptUczestnicyNierezerwowi() {
		return this.pptUczestnicySzkolens.stream()
				.filter(u -> !("T".equals( u.getUszRez() ) ) )
				.sorted( (u1, u2) -> nz(u1.getUszId()).compareTo(nz(u2.getUszId()) ) )
				.collect(Collectors.toList());
	}

	public List<PptUczestnicySzkolen> getPptUczestnicyRezerwowi() {
		return this.pptUczestnicySzkolens.stream()
				.filter(u -> ("T".equals( u.getUszRez() ) ) )
				.sorted( (u1, u2) -> nz(u1.getUszId()).compareTo(nz(u2.getUszId()) ) )
				.collect(Collectors.toList());
	}

	public List<PptUczestnicySzkolen> getPptUczestnicyObecni() {
		return this.pptUczestnicySzkolens.stream()
				.filter(u -> ("T".equals( u.getUszObecnosc() ) ) )
				.sorted( (u1, u2) -> nz(u1.getUszId()).compareTo(nz(u2.getUszId()) ) )
				.collect(Collectors.toList());
	}

	
	
	public void setPptUczestnicySzkolens(List<PptUczestnicySzkolen> pptUczestnicySzkolens) {
		this.pptUczestnicySzkolens = pptUczestnicySzkolens;
	}

	public PptUczestnicySzkolen addPptUczestnicySzkolen(PptUczestnicySzkolen pptUczestnicySzkolen) {
		getPptUczestnicySzkolens().add(pptUczestnicySzkolen);
		pptUczestnicySzkolen.setPptSzkolenia(this);

		calcProporcjonalnieKosztPlanowany();
		return pptUczestnicySzkolen;
	}

	public PptUczestnicySzkolen removePptUczestnicySzkolen(PptUczestnicySzkolen pptUczestnicySzkolen) {
		
//		//modyfikujemyplan szkoleniowy / pozycje
//		List<OstPlanIndyw> collect = this.getPptSzkPlanIndyws().stream()
//		.filter(p -> pptUczestnicySzkolen.getUszPrcId().equals(  p.getPisPrcId()  )  )
//		.collect(Collectors.toList());
//		collect.stream()
//		.forEach(p -> this.removePptSzkPlanIndyw(p));
			
		getPptUczestnicySzkolens().remove(pptUczestnicySzkolen);
		pptUczestnicySzkolen.setPptSzkolenia(null);
		
		recalcProporcjoanlnieKoszty();
		return pptUczestnicySzkolen;
	}

	
	
	
	
	public List<OstPlanIndyw> getPptSzkPlanIndyws() {
		return this.pptSzkPlanIndyws;
	}

	public void setPptSzkPlanIndyws(List<OstPlanIndyw> pptSzkPlanIndyws) {
		this.pptSzkPlanIndyws = pptSzkPlanIndyws;
	}

	
	public OstPlanIndyw addPptSzkPlanIndyw(OstPlanIndyw pptSzkPlanIndyw) {
		getPptSzkPlanIndyws().add(pptSzkPlanIndyw);
		pptSzkPlanIndyw.setPptSzkSzkolenia(this);

		return pptSzkPlanIndyw;
	}

	public OstPlanIndyw removePptSzkPlanIndyw(OstPlanIndyw pptSzkPlanIndyw) {
		getPptSzkPlanIndyws().remove(pptSzkPlanIndyw);
		pptSzkPlanIndyw.setPptSzkSzkolenia(null);

		return pptSzkPlanIndyw;
	}
	
}