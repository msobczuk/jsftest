package com.comarch.egeria.pp.szkolenia;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.szkolenia.model.PptSzkAnkietyPozycje;
import com.comarch.egeria.pp.szkolenia.model.PptUczestnicySzkolen;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@ManagedBean
@Named
@Scope("view")
public class MojeSzkoleniaBean extends SqlBean {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SessionBean sessionBean;
	@Inject
	JdbcUtils jdbc;

	public void editSelectedRow() {
		System.out.println(
				"=================================MojeSzkoleniaBean.editSelectedRow===================================");

		SqlDataSelectionsHandler p = this.getSql("PP_SZKOL_ANKIETY");
		if (p != null) {
			System.out.println(p.getName() + " / currentRow: " + p.getCurrentRow());

			sessionBean.setSelectedListaSzkolenie(p.getCurrentRow());

			sessionBean.setLista_szkolenie_id(p.getCurrentRow().get("usz_id"));

			sessionBean.setSzkolenie_id(p.getCurrentRow().get("usz_szk_id"));

			p = this.getSql("nazwyOcenBezWykl");
			List nazwyPozycjiAnkiety = new ArrayList<>();

			if (p != null) {
				for (DataRow dr : p.getData()) {
					PptSzkAnkietyPozycje oap = new PptSzkAnkietyPozycje();
					oap.setAnspTypOceny((String) dr.get("WSL_WARTOSC"));
					oap.setAnspOcena("");
					nazwyPozycjiAnkiety.add(oap);
				}
				sessionBean.setListaPozycjiAnkiet(nazwyPozycjiAnkiety);
			}

			p = this.getSql("nazwyOcenWykl");
			List listaPozycjiWykladowcow = new ArrayList<>();
			if (p != null) {
				for (DataRow dr : p.getData()) {
					PptSzkAnkietyPozycje oap = new PptSzkAnkietyPozycje();
					oap.setAnspTypOceny((String) dr.get("WSL_WARTOSC"));
					oap.setAnspOcena("");
					listaPozycjiWykladowcow.add(oap);
				}
				sessionBean.setListaPozycjiWykladowcow(listaPozycjiWykladowcow);
			}

			// Przekierowanie do formularza z wybranym szkoleniem
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("AnkietaSzkoleniowa");
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		p = this.getSql("PPL_SZK_ANKIETY_WSZYSTKIE");
		if (p != null) {

			sessionBean.setSelectedListaSzkolenie(p.getCurrentRow());

			sessionBean.setSzkolenie_id(p.getCurrentRow().get("usz_szk_id"));

			Object uszId = p.getCurrentRow().get("usz_id");

			sessionBean.setLista_szkolenie_id(uszId);

			PptUczestnicySzkolen ucz = (PptUczestnicySzkolen) HibernateContext.get(PptUczestnicySzkolen.class,
					((BigDecimal) uszId).longValue());

			List<PptSzkAnkietyPozycje> listaPozycjiAnkiety = ucz.getPptAnkietyPozycjes();

			if (listaPozycjiAnkiety != null && !listaPozycjiAnkiety.isEmpty()) {

				List<PptSzkAnkietyPozycje> listaPozycjiWykladowcow = new ArrayList<>();

				listaPozycjiAnkiety.stream().filter((oap) -> oap.getAnspTypOceny().startsWith("W"))
						.forEach(listaPozycjiWykladowcow::add);

				sessionBean.setListaPozycjiWykladowcow(listaPozycjiWykladowcow);

				List<PptSzkAnkietyPozycje> nazwyPozycjiAnkiety = new ArrayList<>();

				listaPozycjiAnkiety.stream().filter((oap) -> !oap.getAnspTypOceny().startsWith("W"))
						.forEach(nazwyPozycjiAnkiety::add);

				sessionBean.setListaPozycjiAnkiet(nazwyPozycjiAnkiety);
			} else {
				List nazwyPozycjiAnkiety = new ArrayList<>();

				p = this.getSql("nazwyOcenBezWykl");
				if (p != null) {
					for (DataRow dr : p.getData()) {
						PptSzkAnkietyPozycje oap = new PptSzkAnkietyPozycje();
						oap.setAnspTypOceny((String) dr.get("WSL_WARTOSC"));
						oap.setAnspOcena("");
						nazwyPozycjiAnkiety.add(oap);
					}
					sessionBean.setListaPozycjiAnkiet(nazwyPozycjiAnkiety);
				}

				p = this.getSql("nazwyOcenWykl");
				List listaPozycjiWykladowcow = new ArrayList<>();
				if (p != null) {
					for (DataRow dr : p.getData()) {
						PptSzkAnkietyPozycje oap = new PptSzkAnkietyPozycje();
						oap.setAnspTypOceny((String) dr.get("WSL_WARTOSC"));
						oap.setAnspOcena("");
						listaPozycjiWykladowcow.add(oap);
					}
					sessionBean.setListaPozycjiWykladowcow(listaPozycjiWykladowcow);
				}
			}
			// Przekierowanie do formularza z wybranym szkoleniem
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("AnkietaSzkoleniowa");
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		
		p = this.getSql("PPL_SZK_WYKLADOWCY_LISTA");
		if (p != null) {
			
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("idWykladowcy",
					p.getCurrentRow().getIdAsString());
			// sessionBean.getIdHM().put(vRootId, r.getIdAsString());

			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", p.getCurrentRow());
			
			
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("WykladowcaSzczegoly");
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		System.out.println(
				"============================================================================================");
	}

	public void zapisNaSzkol() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("ZapisySzkolenie");
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void dodajWykladowce() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("WykladowcaSzczegoly");
			return;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void doAnkiety() {
		System.out.println(
				"=================================MojeSzkoleniaBean.doAnkiety===================================");

		// Przekierowanie do formularza z wybranym szkoleniem
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("AnkietaSzkoleniowa");
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println(
				"============================================================================================");
	}
	
//	public void edytujIprzAlboSzkolenie(DataRow row) {
//		
//		System.out.println(row);
//		
//	}
//	

}
