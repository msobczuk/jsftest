package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


/**
 * The persistent class for the PPT_SZK_ZAPISY_SZKOLEN database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_ZAPISY_SZKOLEN", schema="PPADM")
@NamedQuery(name="OstZapisySzkolen.findAll", query="SELECT o FROM OstZapisySzkolen o")
public class OstZapisySzkolen implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_ZAPISY_SZKOLEN",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_ZAPISY_SZKOLEN", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_ZAPISY_SZKOLEN")
	@Column(name="ZSZ_ID")
	private Long zszId;

	@Temporal(TemporalType.DATE)
	@Column(name="ZSZ_AUDYT_DM")
	private Date zszAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="ZSZ_AUDYT_DT")
	private Date zszAudytDt;

	@Column(name="ZSZ_AUDYT_KM")
	private String zszAudytKm;

	@Column(name="ZSZ_AUDYT_LM")
	private String zszAudytLm;

	@Column(name="ZSZ_AUDYT_UM")
	private String zszAudytUm;

	@Column(name="ZSZ_AUDYT_UT")
	private String zszAudytUt;

	@Column(name="ZSZ_PRC_ID")
	private Long zszPrcId;

	//bi-directional many-to-one association to OstPlanIndyw
	@OneToMany(mappedBy="ostZapisySzkolen", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<OstPlanIndyw> ostPlanIndyws = new ArrayList<>();

	public OstZapisySzkolen() {
	}

	public Long getZszId() {
		return this.zszId;
	}

	public void setZszId(Long zszId) {
		this.zszId = zszId;
	}

	public Date getZszAudytDm() {
		return this.zszAudytDm;
	}

	public void setZszAudytDm(Date zszAudytDm) {
		this.zszAudytDm = zszAudytDm;
	}

	public Date getZszAudytDt() {
		return this.zszAudytDt;
	}

	public void setZszAudytDt(Date zszAudytDt) {
		this.zszAudytDt = zszAudytDt;
	}

	public String getZszAudytKm() {
		return this.zszAudytKm;
	}

	public void setZszAudytKm(String zszAudytKm) {
		this.zszAudytKm = zszAudytKm;
	}

	public String getZszAudytLm() {
		return this.zszAudytLm;
	}

	public void setZszAudytLm(String zszAudytLm) {
		this.zszAudytLm = zszAudytLm;
	}

	public String getZszAudytUm() {
		return this.zszAudytUm;
	}

	public void setZszAudytUm(String zszAudytUm) {
		this.zszAudytUm = zszAudytUm;
	}

	public String getZszAudytUt() {
		return this.zszAudytUt;
	}

	public void setZszAudytUt(String zszAudytUt) {
		this.zszAudytUt = zszAudytUt;
	}

	public Long getZszPrcId() {
		return this.zszPrcId;
	}

	public void setZszPrcId(Long zszPrcId) {
		this.zszPrcId = zszPrcId;
	}

	public List<OstPlanIndyw> getOstPlanIndyws() {
		return this.ostPlanIndyws;
	}

	public void setOstPlanIndyws(List<OstPlanIndyw> ostPlanIndyws) {
		this.ostPlanIndyws = ostPlanIndyws;
	}

	public OstPlanIndyw addOstPlanIndyw(OstPlanIndyw ostPlanIndyw) {
		getOstPlanIndyws().add(ostPlanIndyw);
		ostPlanIndyw.setOstZapisySzkolen(this);

		return ostPlanIndyw;
	}

	public OstPlanIndyw removeOstPlanIndyw(OstPlanIndyw ostPlanIndyw) {
		getOstPlanIndyws().remove(ostPlanIndyw);
		ostPlanIndyw.setOstZapisySzkolen(null);

		return ostPlanIndyw;
	}

}