package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_SZK_WYKLADOWCY database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_WYKLADOWCY")
@NamedQuery(name="PptSzkWykladowcy.findAll", query="SELECT p FROM PptSzkWykladowcy p")
public class PptSzkWykladowcy implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_WYKLADOWCY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_WYKLADOWCY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_WYKLADOWCY")
	@Column(name="SWYK_ID")
	private Long swykId;

	@Temporal(TemporalType.DATE)
	@Column(name="SWYK_AUDYT_DM")
	private Date swykAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="SWYK_AUDYT_DT")
	private Date swykAudytDt;

	@Column(name="SWYK_AUDYT_KM")
	private String swykAudytKm;

	@Column(name="SWYK_AUDYT_LM")
	private String swykAudytLm;

	@Column(name="SWYK_AUDYT_UM")
	private String swykAudytUm;

	@Column(name="SWYK_AUDYT_UT")
	private String swykAudytUt;

	@Column(name="SWYK_IMIE")
	private String swykImie;

	@Column(name="SWYK_NAZWISKO")
	private String swykNazwisko;

	@Column(name="SWYK_STOPIEN_NAUK")
	private String swykStopienNauk;

	@Column(name="SWYK_SZK_ID")
	private BigDecimal swykSzkId;

	@Column(name="SWYK_TYTUL")
	private String swykTytul;

	//bi-directional many-to-one association to PptSzkWykladowcySzkolenie
	@OneToMany(mappedBy="pptSzkWykladowcy")
	private List<PptSzkWykladowcySzkolenie> pptSzkWykladowcySzkolenies;

	public PptSzkWykladowcy() {
	}

	public Long getSwykId() {
		return this.swykId;
	}

	public void setSwykId(Long swykId) {
		this.swykId = swykId;
	}

	public Date getSwykAudytDm() {
		return this.swykAudytDm;
	}

	public void setSwykAudytDm(Date swykAudytDm) {
		this.swykAudytDm = swykAudytDm;
	}

	public Date getSwykAudytDt() {
		return this.swykAudytDt;
	}

	public void setSwykAudytDt(Date swykAudytDt) {
		this.swykAudytDt = swykAudytDt;
	}

	public String getSwykAudytKm() {
		return this.swykAudytKm;
	}

	public void setSwykAudytKm(String swykAudytKm) {
		this.swykAudytKm = swykAudytKm;
	}

	public String getSwykAudytLm() {
		return this.swykAudytLm;
	}

	public void setSwykAudytLm(String swykAudytLm) {
		this.swykAudytLm = swykAudytLm;
	}

	public String getSwykAudytUm() {
		return this.swykAudytUm;
	}

	public void setSwykAudytUm(String swykAudytUm) {
		this.swykAudytUm = swykAudytUm;
	}

	public String getSwykAudytUt() {
		return this.swykAudytUt;
	}

	public void setSwykAudytUt(String swykAudytUt) {
		this.swykAudytUt = swykAudytUt;
	}

	public String getSwykImie() {
		return this.swykImie;
	}

	public void setSwykImie(String swykImie) {
		this.swykImie = swykImie;
	}

	public String getSwykNazwisko() {
		return this.swykNazwisko;
	}

	public void setSwykNazwisko(String swykNazwisko) {
		this.swykNazwisko = swykNazwisko;
	}

	public String getSwykStopienNauk() {
		return this.swykStopienNauk;
	}

	public void setSwykStopienNauk(String swykStopienNauk) {
		this.swykStopienNauk = swykStopienNauk;
	}

	public BigDecimal getSwykSzkId() {
		return this.swykSzkId;
	}

	public void setSwykSzkId(BigDecimal swykSzkId) {
		this.swykSzkId = swykSzkId;
	}

	public String getSwykTytul() {
		return this.swykTytul;
	}

	public void setSwykTytul(String swykTytul) {
		this.swykTytul = swykTytul;
	}

	public List<PptSzkWykladowcySzkolenie> getPptSzkWykladowcySzkolenies() {
		return this.pptSzkWykladowcySzkolenies;
	}

	public void setPptSzkWykladowcySzkolenies(List<PptSzkWykladowcySzkolenie> pptSzkWykladowcySzkolenies) {
		this.pptSzkWykladowcySzkolenies = pptSzkWykladowcySzkolenies;
	}

	public PptSzkWykladowcySzkolenie addPptSzkWykladowcySzkoleny(PptSzkWykladowcySzkolenie pptSzkWykladowcySzkoleny) {
		getPptSzkWykladowcySzkolenies().add(pptSzkWykladowcySzkoleny);
		pptSzkWykladowcySzkoleny.setPptSzkWykladowcy(this);

		return pptSzkWykladowcySzkoleny;
	}

	public PptSzkWykladowcySzkolenie removePptSzkWykladowcySzkoleny(PptSzkWykladowcySzkolenie pptSzkWykladowcySzkoleny) {
		getPptSzkWykladowcySzkolenies().remove(pptSzkWykladowcySzkoleny);
		pptSzkWykladowcySzkoleny.setPptSzkWykladowcy(null);

		return pptSzkWykladowcySzkoleny;
	}

}