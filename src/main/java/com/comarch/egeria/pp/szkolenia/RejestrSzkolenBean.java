package com.comarch.egeria.pp.szkolenia;

import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.szkolenia.model.PptOfertySzkolen;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@ManagedBean
@Named
@Scope("view")
public class RejestrSzkolenBean extends SqlBean {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LogManager.getLogger();

//	@Inject
//	User user;
	
	
	private PptOfertySzkolen oferta = new PptOfertySzkolen();
	private Long selectedOfertaId = null;

	@PostConstruct
	public void init() {

		if (FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id") != null) {

			setSelectedOfertaId((Long) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id"));
			setOferta();
		}
		
//		user.tmpUserObjectsCHM.put(this.getClass().getSimpleName(), this);	
	}

	public void newOferta() {
		oferta = new PptOfertySzkolen();
	}

	public void zapiszOferte() {
		if (validateOfferBeforeSave()) {
			try {
				oferta = (PptOfertySzkolen) HibernateContext.save(oferta);
				User.info("Zapisano ofertę szkoleń.");
				reloadAndUpdate("ppl_szk_oferty_szkolen", oferta.getOfId());
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				User.alert("Błąd w zapisie oferty szkoleń.");
			}
			com.comarch.egeria.Utils.executeScript("PF('dlgOfertaSzkolenSzczeg').hide();");
		}
	}
	private boolean validateOfferBeforeSave() {
		ArrayList<String> inval = new ArrayList<String>();
		if(oferta.getOfNazwa() == null || oferta.getOfNazwa().trim().equals("")) {
			inval.add("Nazwa oferty jest polem wymaganym");
		}
		if(oferta.getOfObszarWiedzy() == null || oferta.getOfObszarWiedzy().trim().equals("")) {
			inval.add("Obszar wiedzy jest polem wymaganym");
		}
		if(oferta.getOfRodzaj() == null || oferta.getOfRodzaj().trim().equals("")) {
			inval.add("Rodzaj szkolenia jest polem wymaganym");
		}
		if(oferta.getOfKategoria() == null || oferta.getOfKategoria().trim().equals("")) {
			inval.add("Kategoria jest polem wymagaanym");
		}

		for (String msg : inval) {
			User.alert(msg);
		}
		return inval.isEmpty();
	}

	public void usunOferte(DataRow row) {
		setSelectedOfertaId(Long.parseLong("" + row.get(0)));
		usunOferte();
	}

	public void usunOferte() {

		if (!oferta.getPptSzkolenias().isEmpty()) {
			User.warn("Nie można skasować oferty szkolenia, dla której wprowadzono terminy szkoleń.");
			resetDatasourceAndUpdateAllComponents("ppl_szk_terminy_szkolenia", null);
			com.comarch.egeria.Utils.executeScript("PF('accTerm').select(1)");
			return;
		}

		Long ofId = oferta.getOfId();

		try {
			HibernateContext.delete(oferta);
			User.info("Usunięto ofertę szkoleń");
			reloadAndUpdate("ppl_szk_oferty_szkolen", ofId); // this.getSql("PPL_SZK_OFERTY_SZKOLEN").resetTs();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.warn("Błąd w usuwaniu oferty szkoleń.");
		}
	}

	public void zapisz() {

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();

			oferta = (PptOfertySzkolen) s.merge(oferta);

			s.saveOrUpdate(oferta);
			s.beginTransaction().commit();

			User.info("Zapisano szkolenie.");
			reloadAndUpdate("ppl_szk_oferty_szkolen", oferta.getOfId());

		} catch (Exception e) {
			User.alert("Błąd w zapisie szkolenia.");
			log.error(e.getMessage(), e);
		}

	}

	public void setSelectedOfertaId(Long selectedOfertaId) {
		this.selectedOfertaId = selectedOfertaId;
		setOferta();
	}

	public Long getSelectedOfertaId() {
		return selectedOfertaId;
	}

	public PptOfertySzkolen getOferta() {
		return oferta;
	}

	public void setOferta(PptOfertySzkolen oferta) {
		this.oferta = oferta;
	}

	public void setOferta() {
		this.oferta = (PptOfertySzkolen) HibernateContext.get(PptOfertySzkolen.class, selectedOfertaId);
	}

}
