package com.comarch.egeria.pp.szkolenia;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.szkolenia.model.OstUmowy;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@Named
@Scope("view")
public class UmowaLojalBean extends SqlBean {
	private static final long serialVersionUID = 1L;

	@Inject
	SessionBean sessionBean;

	@Inject
	protected ReportGenerator ReportGenerator;

	private static final Logger log = LogManager.getLogger();
	
	private DataRow umowaDR = null;
	private OstUmowy umowa;
	private boolean czyUregulowano = false;
	private long prcId;
	
	@PostConstruct
	public void init() {
		System.out.println("UmowaLojalBean.init().........................");
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
//		umowaDR = sessionBean.getSelectedUmowaLoj();
		if (id != null) {
			umowaDR = (DataRow) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("row");
		}
		if(umowaDR == null) {
			umowa = new OstUmowy();
			return;
		}
		
		
		try(HibernateContext h = new HibernateContext()){
			Session s = h.getSession();
			this.setUmowa(s.get(OstUmowy.class, umowaDR.getIdAsLong()));
			if(umowa.getUmszCzyUregulowano().equals("T")) {
				czyUregulowano = true;
			}
		}
		
		
	}
	public void raport(){
//		if(umowaDR != null){
//			HashMap<String, Object> params = new HashMap<String, Object>();
//			params.put("id_umowy", umowaDR.getIdAsLong());
//
//			genrap.uruchomRaportOceanRTF("Umowa lojalnosciowa", params);
//			}
		
		if(umowa!=null && umowa.getUmszId() >0) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("id_umowy", umowa.getUmszId());
				ReportGenerator.displayReportAsRTF(params, "Umowa_lojalnosciowa");
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
	}

	public void zapisz() {
		
		if (!validateBeforeSaveUmowa()) return;

		
		/*//TODO: zapisanie encji OSTumowy
		if(czyUregulowano) {
			umowa.setUmszCzyUregulowano("T");
		} else {
			umowa.setUmszCzyUregulowano("N");
		}*/

		System.out.println("UmowaLojalBean.zapisz() ... ");

		try (HibernateContext h = new HibernateContext()){

			Session s = h.getSession();			
			umowa = (OstUmowy) s.merge(umowa);
			s.saveOrUpdate(umowa);
			s.beginTransaction().commit();
			User.info("Zapisano umowę lojalnościową o numerze " + umowa.getUmszId());
			//FacesContext.getCurrentInstance().getExternalContext().redirect("ListaUmowLojalnosciowych");
		} catch (Exception e) {
			if (umowa.getUmszId() != 0) {
				User.alert("Nieudany zapis umowy lojalnościowej o numerze " + umowa.getUmszId() + "\n" + e.getMessage());
			} else {
				User.alert("Nieudany zapis umowy lojalnościowej\n" + e.getMessage());
			}
			System.err.println(e);
		}
		
		// JESLI PO ZAPISANIU ENCJI NIE CHCEMY PRZELADOWAC CALEJ STRONY, TO
				// KONIECZNIE TRZEBA PRZELADOWAC PRZYNAJMNEJ ENCJE (BO SA TAM POLA
				// USTAWIANE TRIGGERIAMI)!
				long id = umowa.getUmszId();
				this.setUmowa((OstUmowy) HibernateContext.get(OstUmowy.class, id));
	}

	private boolean validateBeforeSaveUmowa() {
		Boolean ret=true;
//		ArrayList<String> inval = new ArrayList<String>();
		String inval = new String();
		
		if (umowa.getUmszPrcId()==null || umowa.getUmszPrcId()==0) 
			inval += "\nPracownik jest polem wymaganym.";
		
		if (umowa.getUmszPrzedmiot()==null || umowa.getUmszPrzedmiot().trim().equals("")) 
			inval += "\nPrzedmiot jest polem wymaganym.";
		
		if (umowa.getUmszDataPodpisania()==null) 
			inval += "\nData podpisania umowy jest polem wymaganym.";
		
		if (umowa.getUmszDataObowiazywania()==null) 
			inval += "\nData obowiązywania umowy jest polem wymaganym.";
		
		if (umowa.getUmszDataObowiazywania()!=null && umowa.getUmszDataObowiazywania()!=null &&  umowa.getUmszDataObowiazywania().before(umowa.getUmszDataPodpisania()))
			inval += "Termin podpisania umowy powinien być mniejszy niż termin obowiązywania umowy.";
		
		if (umowa.getUmszWartosc()==null) 
			inval += "\nWartość zobowiązań jest polem wymaganym.";
		
		if (umowa.getUmszCzyUregulowano()==null || umowa.getUmszCzyUregulowano().trim().equals("")) 
			inval += "\nInformacja czy uregulowano zobowiązanie jest polem wymaganym.";

		if (!inval.isEmpty()){
			ret = false;
				User.alert(inval);
		}
		return ret;
	}

	public void usunUmowe(){


		try (HibernateContext h = new HibernateContext()) {
			
			Session s = h.getSession();
			
			s.delete(this.umowa); // << usuniecie rekordu
			s.beginTransaction().commit();

			User.info("Usunięto umowę lojalnościową o numerze " + umowa.getUmszId());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
			FacesContext.getCurrentInstance().getExternalContext().redirect("ListaUmowLojalnosciowych");
		} catch (Exception e) {
			System.err.println(e);
		}
		
	}

	public DataRow getUmowaDR() {
		return umowaDR;
	}

	public void setUmowaDR(DataRow umowaDR) {
		this.umowaDR = umowaDR;
	}

	public OstUmowy getUmowa() {
		return umowa;
	}

	public void setUmowa(OstUmowy umowa) {
		this.umowa = umowa;
	}

	public long getPrcId() {
		return prcId;
	}

	public void setPrcId(long prcId) {
		this.prcId = prcId;
	}
	
	public boolean isCzyUregulowano() {
		return czyUregulowano;
	}

	public void setCzyUregulowano(boolean czyUregulowano) {
		this.czyUregulowano = czyUregulowano;
	}
	
}