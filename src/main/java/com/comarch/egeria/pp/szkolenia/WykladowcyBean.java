package com.comarch.egeria.pp.szkolenia;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.rowset.CachedRowSet;

import org.hibernate.Session;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.szkolenia.model.PptSzkWykladowcy;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@Named
@Scope("view")
public class WykladowcyBean extends SqlBean {
	private static final long serialVersionUID = 1L;
	
	@Inject
	private SessionBean sessionBean;
	@Inject
	JdbcUtils jdbc;

	@Inject
	private TerminySzkolenBean terminySzkolenBean;

	private PptSzkWykladowcy wykladowca;

	private DataRow wykladowcaDR;

	private Long wykladowcaID;

	@PostConstruct
	private void init() {
		Long id = ((Long) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("idWykladowcy"));

//		System.out.println("WykladowcyBean.init().........................");

		if (id != null) {
			setWykladowcaID(id);
		} else {
			this.wykladowca = new PptSzkWykladowcy();
		}
	}

	public void zapisz() {

		if (!validateBeforeSave()) {
			
			try  {
				
				wykladowca = (PptSzkWykladowcy) HibernateContext.save(wykladowca);
				User.info("Zapisano wykładowcę");
				reloadAndUpdate("PPL_SZK_WYKLADOWCY_LISTA", wykladowca.getSwykId());
				com.comarch.egeria.Utils.executeScript("PF('dlgWykladowcaNowy').hide()");
				
				
//				if (terminySzkolenBean.getTerminSzkolenia().getPptOfertySzkolen() != null) {
//
//					SqlDataSelectionsHandler ssh = terminySzkolenBean.getSql("PPL_SZK_WYKLADOWCY_LISTA");
//					List<DataRow> wykladowcyList = new ArrayList<DataRow>();
//					List<DataRow> wykladowcyTempList = new ArrayList<DataRow>();
//					List<String> wykladowcyTempListId = new ArrayList<String>();
//					if (ssh != null) {
//						wykladowcyList = ssh.getSelectedRows();
//
//						for (DataRow dr : wykladowcyList) {
//							wykladowcyTempListId.add(dr.getIdAsString());
//						}
//					}
//					terminySzkolenBean.getLW("PPL_SZK_WYKLADOWCY_LISTA").reLoadData();
//					if (wykladowcyTempListId != null && !wykladowcyTempListId.isEmpty()) {
//
//						for (String str : wykladowcyTempListId) {
//							wykladowcyTempList
//									.add((DataRow) terminySzkolenBean.getSql("PPL_SZK_WYKLADOWCY_LISTA").find(str));
//						}
//						terminySzkolenBean.getSql("PPL_SZK_WYKLADOWCY_LISTA").getSelectedRows()
//								.addAll(wykladowcyTempList);
//					}
//
//					wykladowca = new PptSzkWykladowcy();
//
//				}
				
			} catch (Exception ex) {
				User.alert(ex.getMessage());
				User.alert("Nieudany zapis wykładowcy");
			}

		}
	}

	public void usun(Long wykladowcaID) {
		setWykladowcaID(wykladowcaID);
		usun();
	}
	public void usun() {
		if (!validateBeforeDelete()) {

			try (HibernateContext h = new HibernateContext()) {
				Session s = h.getSession();
				s.delete(wykladowca);
				s.beginTransaction().commit();
				User.info("Usunięto wykładowcę");
				this.getLW("PPL_SZK_WYKLADOWCY_LISTA").reLoadData();
				com.comarch.egeria.Utils.update("frmCntr");

			} catch (Exception ex) {
				User.alert(ex.getMessage());
				User.alert("Nieudane usunięcie wykładowcy");
			}

		}
	}

	public boolean validateBeforeSave() {

		List<String> inval = new ArrayList<>();

		String brakDanych = "";

		if (this.wykladowca.getSwykImie() == null || this.wykladowca.getSwykImie().isEmpty()) {
			brakDanych = brakDanych + "Imię wykładowcy; ";
		}

		if (this.wykladowca.getSwykNazwisko() == null || this.wykladowca.getSwykNazwisko().isEmpty()) {
			brakDanych = brakDanych + "Nazwisko wykładowcy; ";
		}

		if (!brakDanych.isEmpty()) {
			inval.add("Następujące pola nie zostały wypełnione: " + brakDanych);
		}

		for (String msg : inval) {
			User.alert(msg);
		}

		return !inval.isEmpty();
	}

	public boolean validateBeforeDelete() {
		List<String> inval = new ArrayList<>();

		try (Connection conn = JdbcUtils.getConnection()) {
			CachedRowSet crs = JdbcUtils.sqlExecQuery(conn,
					"select * from PPT_SZK_ANKIETY_POZYCJE where ANSP_SWYK_ID = ?", wykladowca.getSwykId());

			if (crs.next()) {
				inval.add("Istnieją ankiety dotyczące usuwanego wykładowcy.");
			}

		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
		
			e.printStackTrace();
		}

		for (String msg : inval) {
			User.alert(msg);
		}

		return !inval.isEmpty();
	}

	public PptSzkWykladowcy getWykladowca() {
		return wykladowca;
	}

	public void setWykladowca() {
		this.wykladowca = (PptSzkWykladowcy) HibernateContext.get(PptSzkWykladowcy.class, wykladowcaID);
	}

	public void newWykladowca() {
		this.wykladowca = new PptSzkWykladowcy();
	}

	public void setWykladowcaID(Long selectedWykladowcaID) {
		wykladowcaID = selectedWykladowcaID;
		setWykladowca();
	}
}
