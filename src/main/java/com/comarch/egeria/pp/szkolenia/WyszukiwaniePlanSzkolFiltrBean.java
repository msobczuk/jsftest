package com.comarch.egeria.pp.szkolenia;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

@ManagedBean
@Scope("view")
@Named
public class WyszukiwaniePlanSzkolFiltrBean extends SqlBean implements Serializable {

	private String filterPracownik = "";
	private String filterNazwa = "";
	private String filterTypSzkolenia = "";
	private Integer filterRok = null;
	private String filterZrodlo = "";

	private SqlDataSelectionsHandler planSzkoleniowy = null;
	
	private ArrayList<DataRow> listaZapisow = new ArrayList<>();

	@PostConstruct
	public void init() {

		setPlanSzkoleniowy(this.getLW("PPL_SZK_PLAN_SZKOLENIOWY"));
		listaZapisow = this.planSzkoleniowy.getData();
	}

	public void filtruj() {
		this.planSzkoleniowy
				.setFilteredData(filtruj(this.planSzkoleniowy.getOriginalData(), "prc_id", this.filterPracownik));
		this.planSzkoleniowy
				.setFilteredData(filtruj(this.planSzkoleniowy.getFilteredData(), "szkolenie", this.filterNazwa));
		this.planSzkoleniowy
				.setFilteredData(filtruj(this.planSzkoleniowy.getFilteredData(), "pp_typ", this.filterTypSzkolenia));
		this.planSzkoleniowy
				.setFilteredData(filtruj(this.planSzkoleniowy.getFilteredData(), "pis_rok", this.filterRok));
		this.planSzkoleniowy
				.setFilteredData(filtruj(this.planSzkoleniowy.getFilteredData(), "pp_zrodlo", this.filterZrodlo));
		
	}

	public ArrayList<DataRow> filtruj(ArrayList<DataRow> in, String kolumna, Object filterExpression) {
		ArrayList<DataRow> ret = in;
		if (filterExpression == null) {
			return ret;
		}

		String filterExpressionStr = "" + filterExpression;

		if (filterExpressionStr.isEmpty()) {
			return ret;
		}

		String filterExpressionL = (filterExpressionStr).toLowerCase();

		List<DataRow> l = ret.stream().filter(r -> ("" + r.get(kolumna)).toLowerCase().contains(filterExpressionL))
				.collect(Collectors.toList());

		ret = new ArrayList<>();
		ret.addAll(l);

		return ret;
	}

	public void deleteFilters() {
		this.planSzkoleniowy.setTextFilter("");
		setFilterNazwa("");
		setFilterPracownik("");
		setFilterRok(null);
		setFilterTypSzkolenia("");
		setFilterZrodlo("");
	}

	public void openPage(DataRow dr) {

		Object backId = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		String id = dr.get("pp_encja_id").toString();
		
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", id);

		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", dr);

		String typ = (String) dr.get("pp_zrodlo");

		if (typ.equals("SZK"))
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("ZapisySzkolenie");
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}

		if (typ.equals("IPRZ"))
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("ArkuszIPRZ");
				return;
			} catch (IOException e) {
				e.printStackTrace();
			}

		return;
	}

	public String getFilterPracownik() {
		return filterPracownik;
	}

	public void setFilterPracownik(String filterPracownik) {

		if (!("" + filterPracownik).equals("" + this.filterPracownik)) {
			this.filterPracownik = filterPracownik;
			filtruj();
		}
	}

	public String getFilterNazwa() {
		return filterNazwa;
	}

	public void setFilterNazwa(String filterNazwa) {

		if (!("" + filterNazwa).equals("" + this.filterNazwa)) {
			this.filterNazwa = filterNazwa;
			filtruj();
		}
	}

	public String getFilterTypSzkolenia() {
		return filterTypSzkolenia;
	}

	public void setFilterTypSzkolenia(String filterTypSzkolenia) {

		if (!("" + filterTypSzkolenia).equals("" + this.filterTypSzkolenia)) {
			this.filterTypSzkolenia = filterTypSzkolenia;
			filtruj();
		}
	}

	public Integer getFilterRok() {
		return filterRok;
	}

	public void setFilterRok(Integer filterRok) {

		if (!("" + filterRok).equals("" + this.filterRok)) {
			this.filterRok = filterRok;
			filtruj();
		}
	}

	public String getFilterZrodlo() {
		return filterZrodlo;
	}

	public void setFilterZrodlo(String filterZrodlo) {

		if (!("" + filterZrodlo).equals("" + this.filterZrodlo)) {
			this.filterZrodlo = filterZrodlo;
			filtruj();
		}
	}

	public SqlDataSelectionsHandler getPlanSzkoleniowy() {
		return planSzkoleniowy;
	}

	public void setPlanSzkoleniowy(SqlDataSelectionsHandler przypisaniaTelefonow) {
		this.planSzkoleniowy = przypisaniaTelefonow;
	}

	public void odswiezPlan() {
		if(this.planSzkoleniowy == null) {
			setPlanSzkoleniowy(this.getLW("PPL_SZK_PLAN_SZKOLENIOWY"));
		} else {
			setPlanSzkoleniowy(this.planSzkoleniowy);
		}
		
		listaZapisow = this.planSzkoleniowy.getData();
	}
	
	public void wyczyscPracownika() {
		filterPracownik = "";
		filtruj();
	}
	
	public ArrayList<DataRow> getListaZapisow() {
		return listaZapisow;
	}

	public void setListaZapisow(ArrayList<DataRow> listaZapisow) {
		this.listaZapisow = listaZapisow;
	}


	/*
	 * public boolean isFilterRodzajKom() { return filterRodzajKom; }
	 * 
	 * public void setFilterRodzajKom(boolean filterRodzajKom) {
	 * this.filterRodzajKom = filterRodzajKom; }
	 * 
	 * public boolean isFilterRodzajSta() { return filterRodzajSta; }
	 * 
	 * public void setFilterRodzajSta(boolean filterRodzajSta) {
	 * this.filterRodzajSta = filterRodzajSta; }
	 */

}
