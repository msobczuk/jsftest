package com.comarch.egeria.pp.szkolenia;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.szkolenia.model.PptSzkBudzet;
import com.comarch.egeria.pp.szkolenia.model.PptSzkBudzetPozycje;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@ManagedBean 
@Named
@Scope("view")
public class BudzetSzkoleniowyBean extends SqlBean {
	private static final long serialVersionUID = 1;


//	public static final Logger log = LogManager.getLogger(SqlBean.class);//public bo wiekszosc beanow dziedziczy po tym
	private static final Logger log = LogManager.getLogger();//public bo wiekszosc beanow dziedziczy po tym
	
//	DataRow okres = null;
//	
//	private Integer rok = LocalDate.now().getYear();
//	private ArrayList<Integer> lata = new ArrayList<>();
	
	
	private PptSzkBudzet szkBudzet = new PptSzkBudzet();
	
	
	@PostConstruct
	public void init(){
		SqlDataSelectionsHandler lwJO = this.getLW("ppl_szk_budzet_jo");
		SqlDataSelectionsHandler lwOkresy = this.getLW("PPL_SZK_BUDZET");
		if (lwOkresy.getData().isEmpty()){
			User.info("Lista okresów budżetowych jest pusta (dodaj nowy okres budżetowy).");
			com.comarch.egeria.Utils.executeScript("PF('dlgSzkBudzetListaOkresow').show()");
			return;
		}

		wczytajSzkBudzet(lwOkresy.getData().get(0));
	}

	
	private DataRow pozycja = null;
	
	public void pozycjaPokazUczestnikow(DataRow r){
		this.setPozycja(r);
		SqlDataSelectionsHandler lw = this.getLW("PPL_SZK_BUDZET_POZ2USZ", r.get("bsp_bsz_id"), r.getIdAsLong());
		this.updateAndShowDialog("dlgSzkBudzetPozycjaPokazUczestnikow");
//		RequestContext.getCurrentInstance().update("dlgSzkBudzetPozycjaPokazUczestnikow");
//		RequestContext.getCurrentInstance().execute("PF('dlgSzkBudzetPozycjaPokazUczestnikow').show()");
	} 
	
	public void pozaBudzetemPokazUczestnikow(){
		//this.setPozycja(r);
		SqlDataSelectionsHandler lw = this.getLW("PPL_SZK_BUDZET_POZ2USZ",szkBudzet.getBszId(), -999999999L);
		this.updateAndShowDialog("dlgSzkBudzetPozycjaPokazUczestnikow");
//		RequestContext.getCurrentInstance().update("dlgSzkBudzetPozycjaPokazUczestnikow");
//		RequestContext.getCurrentInstance().execute("PF('dlgSzkBudzetPozycjaPokazUczestnikow').show()");
	} 
	
	
	public PptSzkBudzetPozycje getPozycja(DataRow r){
		PptSzkBudzetPozycje ret = this.szkBudzet.getPptSzkBudzetPozycjes().stream().filter(p->r.getIdAsLong().equals( p.getBspId() )).findFirst().orElse(null);
		return ret;
	}
	
	
	public void ustawSeryjniePlanDlaZaznaczonych(){
		SqlDataSelectionsHandler lw = this.getLW("PPL_SZK_BUDZET_POZYCJE");
		lw.getSelectedRows();
	}
	
	
	/**
	 * wczytuje okrees budzetowy ...
	 * @param r
	 */
	private void wczytajSzkBudzet(DataRow r){
		
		if (r==null)
			setSzkBudzet(new PptSzkBudzet());
		else{
			setSzkBudzet((PptSzkBudzet) HibernateContext.get(PptSzkBudzet.class, r.getIdAsLong()));
			
			String tytulPdf = String.format("Pomocniczy budżet szkoleniowy na okres %1$s do %2$1s (r.b. %3$s)"
								, this.szkBudzet.getBszDataOd(), this.szkBudzet.getBszDataDo(), this.szkBudzet.getBszRok());
			this.getLW("PPL_SZK_BUDZET_POZYCJE", r.getIdAsLong()).setName(tytulPdf);
		}
		lwUkryjDodanePozycje();
		
		 
	}

	
	
	
	public void listaOkresowRowSelectAction(){
		DataRow r = this.getLW("PPL_SZK_BUDZET").getCurrentRow();
		wczytajSzkBudzet(r);
	}

	
	public void listaOkresowRowEditAction(DataRow r){
		wczytajSzkBudzet(r);
		com.comarch.egeria.Utils.update("dlgSzkBudzetEdycjaOkresu");
		com.comarch.egeria.Utils.executeScript("PF('dlgSzkBudzetEdycjaOkresu').show()");
	}

	public void listaOkresowRowDeleteAction(DataRow r){
		wczytajSzkBudzet(r);
		HibernateContext.delete(szkBudzet);
		wczytajSzkBudzet(null);
		reloadAndUpdate("PPL_SZK_BUDZET", r.getIdAsLong());
		com.comarch.egeria.Utils.executeScript("hgoff();");
	}
	

	
	private Double wartoscPlanDlaPozycji = 0.0d;
	
	
	public void dlgSzkBudzetPozycjeDodajZapiszPozycje(){
	

		List<DataRow> selectedRows = this.getLW("ppl_szk_budzet_jo").getSelectedRows();
		
		for (DataRow r : selectedRows) {
			PptSzkBudzetPozycje p = new PptSzkBudzetPozycje();
			p.setBspPlan(this.wartoscPlanDlaPozycji);
			BigDecimal bspObId = (BigDecimal) r.get("ob_id");
			p.setBspObId(bspObId);
			this.szkBudzet.addPptSzkBudzetPozycje(p);
		}

		
		szkBudzet =	(PptSzkBudzet) HibernateContext.save(this.szkBudzet);
		
		lwUkryjDodanePozycje();
		resetDatasourceAndUpdateAllComponents("PPL_SZK_BUDZET_POZYCJE", null);
		com.comarch.egeria.Utils.executeScript("PF('dlgSzkBudzetPozycjeDodaj').hide(); hgoff();");
		
		this.getLW("ppl_szk_budzet_jo").clearSelection();
		
		updatePodsumowanie();
		
		//RequestContext.getCurrentInstance().update("frmCntr_dlgSzkBudzetPozycjeDodaj");
		
	}

	public void updatePodsumowanie() {
		int size = 0;
		this.getLW("PPL_SZK_BUDZET_POZA").resetTs();
		size = this.getLW("PPL_SZK_BUDZET_POZA").getData().size();
		com.comarch.egeria.Utils.update("frmCntr:acc:podsBudz");
		
		this.getLW("PPL_SZK_BUDZET_POZYCJE").resetTs();
		size = this.getLW("PPL_SZK_BUDZET_POZYCJE").getData().size();
		com.comarch.egeria.Utils.update("frmCntr:acc:pozaPlanem");
	}
	
	
	public void lwUkryjDodanePozycje() {
		//		hideLwRows(sqlid, this.szkBudzet.getPptSzkBudzetPozycjesBspObIdList());
		List<BigDecimal> pptSzkBudzetPozycjesBspObIdList = this.szkBudzet.getPptSzkBudzetPozycjesBspObIdList();
		this.getLW("ppl_szk_budzet_jo").getHiddenRowsPK().clear();
		this.getLW("ppl_szk_budzet_jo").hideAll(pptSzkBudzetPozycjesBspObIdList);
		resetDatasourceAndUpdateAllComponents("ppl_szk_budzet_jo", null);
	}
	

	public void listaPozycjiDeleteRowAction(DataRow r) throws SQLException{
		if (r==null)
			return;

		try  {
			PptSzkBudzetPozycje p = this.szkBudzet.getPptSzkBudzetPozycjes().stream().filter(poz->r.getIdAsLong().equals( poz.getBspId() ) ).findFirst().orElse(null);
			if (p!=null){
				this.szkBudzet.removePptSzkBudzetPozycje(p);
				this.szkBudzet = (PptSzkBudzet) HibernateContext.save(this.szkBudzet);
				reloadAndUpdate("PPL_SZK_BUDZET_POZYCJE", r.getIdAsLong());
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
		com.comarch.egeria.Utils.executeScript("hgoff();");
		
		lwUkryjDodanePozycje();
		
		updatePodsumowanie();
		
	}

	
	
	
	public void zapiszOkres(){
		szkBudzet = (PptSzkBudzet) HibernateContext.save(szkBudzet);
		User.info("Zapisano.");
		reloadAndUpdate("PPL_SZK_BUDZET", szkBudzet.getBszId());
		this.getLW("PPL_SZK_BUDZET_POZYCJE").resetTs();
		com.comarch.egeria.Utils.executeScript("PF('dlgSzkBudzetEdycjaOkresu').hide(); hgoff();");
	}


	public PptSzkBudzet getSzkBudzet() {
		return szkBudzet;
	}


	public void setSzkBudzet(PptSzkBudzet szkBudzet) {
		this.szkBudzet = szkBudzet;
	}






	public Double getWartoscPlanDlaPozycji() {
		return wartoscPlanDlaPozycji;
	}

	
	public void setWartoscPlanDlaPozycji(Double WartoscPlanDlaPozycji) {
		this.wartoscPlanDlaPozycji = WartoscPlanDlaPozycji;
	}


	public DataRow getPozycja() {
		return pozycja;
	}


	public void setPozycja(DataRow pozycja) {
		this.pozycja = pozycja;
	}

	
}
