package com.comarch.egeria.pp.szkolenia;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.rowset.CachedRowSet;

import com.comarch.egeria.pp.data.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.primefaces.component.commandbutton.CommandButton;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.szkolenia.model.PptSzkWnSkierowanieStudia;
import com.comarch.egeria.pp.zrodlaFinansowania.ZrodlaFinansowaniaBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@Named
@Scope("view")
public class SzkoleniaWniosekStudiaBean extends SqlBean {
	private static final long serialVersionUID = 1L;

	@Inject
	User user;

	@Inject
	private SessionBean sessionBean;

	@Inject
	protected ReportGenerator ReportGenerator;
	
	@Inject
	ZrodlaFinansowaniaBean zrodlaFinansowaniaBean;

	private Double trainingValue;
	private Double wKwocie;
	private Date startDate;
	private Date endDate;

	private static final Logger log = LogManager.getLogger();

	private String prowadzoneSystemem = "";
	private List<Object> formyOrganizacji = new ArrayList<>();

	private String radioBox;

	private Boolean renderedWKwocie = true;

	private List<String> radioItems = new LinkedList<>();

	public DataRow wniosekDr = null;

	public PptSzkWnSkierowanieStudia wniosek = null;

	private Boolean czyPokrycie = false;

	@PostConstruct
	public void init() {
		// radioItems.add("W całości");
		// radioItems.add("W części");
		//
		// wniosek = new OstWnSkierowanieStudia();

		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		if (id != null) {
			wniosekDr = (DataRow) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("row");

		}

		if (wniosekDr == null) {
			this.wniosek = new PptSzkWnSkierowanieStudia();
			this.wniosek.setSkstFCzyPokrycie("T");
			return;
		}

		this.setWniosek((PptSzkWnSkierowanieStudia) HibernateContext.get(PptSzkWnSkierowanieStudia.class,
				wniosekDr.getIdAsLong()));

		if (wniosek.getSkstFCzyPokrycie() == null) {
			wniosek.setSkstFCzyPokrycie("T");
		}

		// s.close();
	}

	public void setCzyPokrycie(Boolean czyPokrycie) {
		this.czyPokrycie = czyPokrycie;
	}

	public void raport() {
		if (wniosekDr.getIdAsLong() != null) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("id_wniosku", wniosekDr.getIdAsLong());
				com.comarch.egeria.web.common.reports.ReportGenerator.displayReportAsPDF(params, "Wniosek_studia");
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}

		}
	}

	public void zapisz() {

		if (wniosek.getSkstPrcId() == null)
			wniosek.setSkstPrcId(sessionBean.getPrc_id_Long());

		if (!validateBeforeSave()) {
			return;
		}
		
		if (!zrodlaFinansowaniaBean.validateBeforeSave()) {
			return;
		}
		


//		// zapis pol dodatkowych
//		FacesContext facesContext = FacesContext.getCurrentInstance();
//		UIViewRoot root = facesContext.getViewRoot();
//		// TODO dynamiczne ID, do zmiany
//		CommandButton button = (CommandButton) root.findComponent("zapisyFormId:pdef:zapiszPolaDodId");
//		ActionEvent actionEvent = new ActionEvent(button);
//		actionEvent.queue();

		// zapis pól dodatkowych - jesli inject nie dziala...
		CPoleDefiniowalne cPoleDefiniowalne = (CPoleDefiniowalne) AppBean.getViewScopeBean("cPoleDefiniowalne");
		cPoleDefiniowalne.zapiszPolaDef();


		// zapis wniosku
		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			wniosek = (PptSzkWnSkierowanieStudia) s.merge(wniosek); // MERGE
																	// jest
																	// ultrawazny,
																	// bo
																	// dopiero
																	// teraz w
																	// nowej/niezaleznej
																	// sesji
																	// zarządzamy
																	// obiektem
																	// ocena (to
																	// nowa
																	// zarzadzalna
																	// ocena;
																	// linijke
																	// wczesniej
																	// ocena
																	// byla
																	// skojarzona
																	// jako
																	// niezarzadzalna/detached)
			s.saveOrUpdate(wniosek);
			s.beginTransaction().commit();
			s.refresh(this.wniosek);
			
			zrodlaFinansowaniaBean.zapisz(this.wniosek.getSkstId());
			
		} catch (Exception e) {
			System.err.println(e);
		}

		User.info("Zapisano wniosek o wysłanie na studia");
	}

	private boolean validateBeforeSave() {
		ArrayList<String> inval = new ArrayList<String>();
		String invalField = "";
		String invalDate = "";

		try (Connection conn = JdbcUtils.getConnection()) {
			List<Object> params = new ArrayList<>();

			params.add(wniosek.getSkstPrcId());
			CachedRowSet crs = JdbcUtils.sqlExecQuery(conn,
					"select usz_id from PPT_SZK_UCZESTNICY_SZKOLEN where usz_prc_id = ? and USZ_DATA_WYPELNIENIA is null",
					params);

			if (crs != null) {
				if (crs.next()) {
					User.warn("Posiadasz niewypełnione ankiety szkoleniowe.");
				}
			}
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
			inval.add(e.getMessage());
		}

		if (wniosek.getSkstTemat() == null || wniosek.getSkstTemat().trim().isEmpty()) {
			invalField += "Temat nie może być pusty! ";
		}

		if (wniosek.getSkstFCzyPokrycie() == null || wniosek.getSkstFCzyPokrycie().trim().isEmpty()) {
			invalField += "Należy określić pokrycie kosztów szkolenia! ";
		} else {
			if (wniosek.getSkstFCzyPokrycie().equals("N") && wniosek.getSkstKwotaPokrycie() == null) {
				invalField += "Należy określić kwotę pokrycia kosztów szkolenia! ";
			}
		}

		if (wniosek.getSkstInneKoszty() != null && !wniosek.getSkstInneKoszty().trim().isEmpty()) {
			if (wniosek.getSkstKwotaInneKoszty() == null) {
				invalField += "Wskazując rodzaj innych kosztów należy podać ich kwotę! ";
			}
		}

		if (wniosek.getSkstUrlopBezplOd() != null) {
			if (wniosek.getSkstUrlopBezplDo() != null) {
				if (wniosek.getSkstUrlopBezplDo().before(wniosek.getSkstUrlopBezplOd())) {
					invalDate += "Data końca urlopu bezpłatnego nie może być wcześniejsza od daty początkowej! ";
				}
			} else {
				invalDate += "Bezpłatny urlop musi mieć koniec! ";
			}
		} else {
			if (wniosek.getSkstUrlopBezplDo() != null) {
				invalDate += "Bezpłatny urlop musi mieć początek! ";
			}
		}

		if (wniosek.getSkstUrlopSzkolOd() != null) {
			if (wniosek.getSkstUrlopSzkolDo() != null) {
				if (wniosek.getSkstUrlopSzkolDo().before(wniosek.getSkstUrlopSzkolOd())) {
					invalDate += "Data końca urlopu szkoleniowego nie może być wcześniejsza od daty początkowej! ";
				}
			} else {
				invalDate += "Urlop szkoleniowy musi mieć koniec! ";
			}
		} else {
			if (wniosek.getSkstUrlopBezplDo() != null) {
				invalDate += "Urlop szkoleniowy musi mieć początek! ";
			}
		}

		if (!invalField.isEmpty()) {
			inval.add(invalField);
		}
		if (!invalDate.isEmpty()) {
			inval.add(invalDate);
		}

		for (String msg : inval) {
			User.alert(msg);
		}

		return inval.isEmpty();
	}

	public void usunWniosek() {

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			s.delete(wniosek);
			s.beginTransaction().commit();
			User.info("Usunięto wniosek o szkolenie");
		} catch (Exception e) {
			User.alert("Błąd w usuwaniu wniosku!");
			e.printStackTrace();
		}

		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("StudiaWszystkieWnioski");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void zmianaPokrycia() {
        czyPokrycie = !wniosek.getSkstFCzyPokrycie().equals("T");
	}

	public Double getTrainingValue() {
		return trainingValue;
	}

	public void setTrainingValue(Double trainingValue) {
		this.trainingValue = trainingValue;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getRenderedWKwocie() {
		System.out.println("getRenderedWKwocie");
		if (renderedWKwocie == true) {
			renderedWKwocie = false;
			return false;
		} else {
			renderedWKwocie = true;
			return true;

		}
	}

	public void setRenderedWKwocie(Boolean renderedWKwocie) {
		this.renderedWKwocie = renderedWKwocie;
	}

	public String getRadioBox() {
		return radioBox;
	}

	public void setRadioBox(String radioBox) {
		System.out.println(radioBox);
		this.radioBox = radioBox;
	}

	public List<String> getRadioItems() {
		return radioItems;
	}

	public void setRadioItems(List<String> radioItems) {
		this.radioItems = radioItems;
	}

	public String getProwadzoneSystemem() {
		return prowadzoneSystemem;
	}

	public void setProwadzoneSystemem(String prowadzoneSystemem) {
		// SqlDataSelectionsHandler ssh = this.getSql("OS_FORMA_ORGANIZACJI");
		// if(prowadzoneSystemem != null && !prowadzoneSystemem.isEmpty()) {
		// for(DataRow dr : ssh.getData()) {
		// if(dr.get(0).equals(prowadzoneSystemem)) {
		// this.prowadzoneSystemem = (String) dr.get(0);
		// return;
		// }
		// }
		// }
		this.prowadzoneSystemem = prowadzoneSystemem;
	}

	public List<Object> getFormyOrganizacji() {
		if (formyOrganizacji.isEmpty()) {
			SqlDataSelectionsHandler ssh = this.getSql("OS_FORMA_ORGANIZACJI");
			for (DataRow dr : ssh.getData())
				formyOrganizacji.add(dr.get(1));
		}
		return formyOrganizacji;
	}

	public void setFormyOrganizacji(List<Object> formyOrganizacji) {
		this.formyOrganizacji = formyOrganizacji;
	}

	public Double getwKwocie() {
		return wKwocie;
	}

	public void setwKwocie(Double wKwocie) {
		this.wKwocie = wKwocie;
	}

	public PptSzkWnSkierowanieStudia getWniosek() {
		return wniosek;
	}

	public void setWniosek(PptSzkWnSkierowanieStudia wniosek) {
		this.wniosek = wniosek;
	}

	public Boolean getCzyPokrycie() {
		return czyPokrycie;
	}

}
