package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_SZK_ANKIETY_POZYCJE database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_ANKIETY_POZYCJE", schema="PPADM")
@NamedQuery(name="PptSzkAnkietyPozycje.findAll", query="SELECT o FROM PptSzkAnkietyPozycje o")
public class PptSzkAnkietyPozycje implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_ANKIETY_POZYCJE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_ANKIETY_POZYCJE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_ANKIETY_POZYCJE")
	@Column(name="ANSP_ID")
	private long anspId;

	@Temporal(TemporalType.DATE)
	@Column(name="ANSP_AUDYT_DM")
	private Date anspAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="ANSP_AUDYT_DT")
	private Date anspAudytDt;

	@Column(name="ANSP_AUDYT_KM")
	private String anspAudytKm;

	@Column(name="ANSP_AUDYT_LM")
	private String anspAudytLm;

	@Column(name="ANSP_AUDYT_UM")
	private String anspAudytUm;

	@Column(name="ANSP_AUDYT_UT")
	private String anspAudytUt;

	@Column(name="ANSP_OCENA")
	private String anspOcena;

	@Column(name="ANSP_OCENA_NR")
	private Long anspOcenaNr;
	
	@Column(name="ANSP_OCENA_OPIS")
	private String anspOcenaOpis;
	
	
	@Column(name="ANSP_SWYK_ID")
	private Long anspSwykId;

	@Column(name="ANSP_TYP_OCENY")
	private String anspTypOceny;

	//bi-directional many-to-one association to OstListySzkolen
	@ManyToOne
	@JoinColumn(name="ANSP_USZ_ID")
	private PptUczestnicySzkolen pptUczestnicySzkolen;

	public PptSzkAnkietyPozycje() {
	}

	public long getAnspId() {
		return this.anspId;
	}

	public void setAnspId(long anspId) {
		this.anspId = anspId;
	}

	public Date getAnspAudytDm() {
		return this.anspAudytDm;
	}

	public void setAnspAudytDm(Date anspAudytDm) {
		this.anspAudytDm = anspAudytDm;
	}

	public Date getAnspAudytDt() {
		return this.anspAudytDt;
	}

	public void setAnspAudytDt(Date anspAudytDt) {
		this.anspAudytDt = anspAudytDt;
	}

	public String getAnspAudytKm() {
		return this.anspAudytKm;
	}

	public void setAnspAudytKm(String anspAudytKm) {
		this.anspAudytKm = anspAudytKm;
	}

	public String getAnspAudytLm() {
		return this.anspAudytLm;
	}

	public void setAnspAudytLm(String anspAudytLm) {
		this.anspAudytLm = anspAudytLm;
	}

	public String getAnspAudytUm() {
		return this.anspAudytUm;
	}

	public void setAnspAudytUm(String anspAudytUm) {
		this.anspAudytUm = anspAudytUm;
	}

	public String getAnspAudytUt() {
		return this.anspAudytUt;
	}

	public void setAnspAudytUt(String anspAudytUt) {
		this.anspAudytUt = anspAudytUt;
	}

	public String getAnspOcena() {
		return this.anspOcena;
	}

	public void setAnspOcena(String anspOcena) {
		this.anspOcena = anspOcena;
	}

	public Long getAnspSwykId() {
		return this.anspSwykId;
	}

	public void setAnspSwykId(Long anspSwykId) {
		this.anspSwykId = anspSwykId;
	}

	public String getAnspTypOceny() {
		return this.anspTypOceny;
	}

	public void setAnspTypOceny(String anspTypOceny) {
		this.anspTypOceny = anspTypOceny;
	}

	public PptUczestnicySzkolen getPptUczestnicySzkolen() {
		return this.pptUczestnicySzkolen;
	}

	public void setPptUczestnicySzkolen(PptUczestnicySzkolen pptUczestnicySzkolen) {
		this.pptUczestnicySzkolen = pptUczestnicySzkolen;
	}

	public Long getAnspOcenaNr() {
		return anspOcenaNr;
	}

	public void setAnspOcenaNr(Long anspOcenaNr) {
		this.anspOcenaNr = anspOcenaNr;
	}

	public String getAnspOcenaOpis() {
		return anspOcenaOpis;
	}

	public void setAnspOcenaOpis(String anspOcenaOpis) {
		this.anspOcenaOpis = anspOcenaOpis;
	}

}