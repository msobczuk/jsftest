package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PPT_SZK_WYKLADOWCY_SZKOLENIE database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_WYKLADOWCY_SZKOLENIE")
@NamedQuery(name="PptSzkWykladowcySzkolenie.findAll", query="SELECT p FROM PptSzkWykladowcySzkolenie p")
public class PptSzkWykladowcySzkolenie implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_WYKLADOWCY_SZKOLENIE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_WYKLADOWCY_SZKOLENIE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_WYKLADOWCY_SZKOLENIE")
	@Column(name="WSZK_ID")
	private Long wszkId;

	@Temporal(TemporalType.DATE)
	@Column(name="WSZK_AUDYT_DM")
	private Date wszkAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="WSZK_AUDYT_DT")
	private Date wszkAudytDt;

	@Column(name="WSZK_AUDYT_KM")
	private String wszkAudytKm;

	@Column(name="WSZK_AUDYT_LM")
	private String wszkAudytLm;

	@Column(name="WSZK_AUDYT_UM")
	private String wszkAudytUm;

	@Column(name="WSZK_AUDYT_UT")
	private String wszkAudytUt;

	//bi-directional many-to-one association to PptSzkSzkolenia
	@ManyToOne
	@JoinColumn(name="WSZK_SZK_ID")
	private PptSzkSzkolenia pptSzkSzkolenia;

	//bi-directional many-to-one association to PptSzkWykladowcy
	@ManyToOne
	@JoinColumn(name="WSZK_SWYK_ID")
	private PptSzkWykladowcy pptSzkWykladowcy;

	public PptSzkWykladowcySzkolenie() {
	}

	public Long getWszkId() {
		return this.wszkId;
	}

	public void setWszkId(Long wszkId) {
		this.wszkId = wszkId;
	}

	public Date getWszkAudytDm() {
		return this.wszkAudytDm;
	}

	public void setWszkAudytDm(Date wszkAudytDm) {
		this.wszkAudytDm = wszkAudytDm;
	}

	public Date getWszkAudytDt() {
		return this.wszkAudytDt;
	}

	public void setWszkAudytDt(Date wszkAudytDt) {
		this.wszkAudytDt = wszkAudytDt;
	}

	public String getWszkAudytKm() {
		return this.wszkAudytKm;
	}

	public void setWszkAudytKm(String wszkAudytKm) {
		this.wszkAudytKm = wszkAudytKm;
	}

	public String getWszkAudytLm() {
		return this.wszkAudytLm;
	}

	public void setWszkAudytLm(String wszkAudytLm) {
		this.wszkAudytLm = wszkAudytLm;
	}

	public String getWszkAudytUm() {
		return this.wszkAudytUm;
	}

	public void setWszkAudytUm(String wszkAudytUm) {
		this.wszkAudytUm = wszkAudytUm;
	}

	public String getWszkAudytUt() {
		return this.wszkAudytUt;
	}

	public void setWszkAudytUt(String wszkAudytUt) {
		this.wszkAudytUt = wszkAudytUt;
	}

	public PptSzkSzkolenia getPptSzkSzkolenia() {
		return this.pptSzkSzkolenia;
	}

	public void setPptSzkSzkolenia(PptSzkSzkolenia pptSzkSzkolenia) {
		this.pptSzkSzkolenia = pptSzkSzkolenia;
	}

	public PptSzkWykladowcy getPptSzkWykladowcy() {
		return this.pptSzkWykladowcy;
	}

	public void setPptSzkWykladowcy(PptSzkWykladowcy pptSzkWykladowcy) {
		this.pptSzkWykladowcy = pptSzkWykladowcy;
	}

}