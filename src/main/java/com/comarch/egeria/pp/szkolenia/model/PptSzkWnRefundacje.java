package com.comarch.egeria.pp.szkolenia.model;

import java.io.Serializable;

import java.math.RoundingMode;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.comarch.egeria.pp.data.model.ModelBase;

import static com.comarch.egeria.Utils.nz;
import static com.comarch.egeria.Utils.round;


/**
 * The persistent class for the PPT_SZK_WN_REFUNDACJE database table.
 * 
 */
@Entity
@Table(name="PPT_SZK_WN_REFUNDACJE", schema="PPADM")
@NamedQuery(name="PptSzkWnRefundacje.findAll", query="SELECT p FROM PptSzkWnRefundacje p")
public class PptSzkWnRefundacje extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_SZK_WN_REFUNDACJE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_SZK_WN_REFUNDACJE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_SZK_WN_REFUNDACJE")
	@Column(name="WNR_ID")
	private long wnrId;

	@Column(name="WNR_F_CZY_FAKTURA")
	private String wnrFCzyFaktura;

	@Column(name="WNR_F_CZY_HARMONOGRAM")
	private String wnrFCzyHarmonogram;

	@Column(name="WNR_F_CZY_IPRZ")
	private String wnrFCzyIprz;

	@Column(name="WNR_F_CZY_POKRYCIE")
	private String wnrFCzyPokrycie;

	@Column(name="WNR_INNE_KOSZTY")
	private String wnrInneKoszty;

	@Column(name="WNR_KOSZT")
	private Double wnrKoszt;

	@Column(name="WNR_KWOTA_INNE_KOSZTY")
	private Double wnrKwotaInneKoszty;

	@Column(name="WNR_KWOTA_POKRYCIA")
	private Double wnrKwotaPokrycia;

	@Column(name="WNR_NAZWA")
	private String wnrNazwa;

	@Column(name="WNR_OPINIA")
	private String wnrOpinia;

	@Column(name="WNR_PRC_ID")
	private Long wnrPrcId;

	@Column(name="WNR_TEMAT_ORGANIZATOR")
	private String wnrTematOrganizator;

	@Temporal(TemporalType.DATE)
	@Column(name="WNR_TERMIN_ROZPOCZECIA")
	private Date wnrTerminRozpoczecia;

	@Temporal(TemporalType.DATE)
	@Column(name="WNR_TERMIN_ZAKONCZENIA")
	private Date wnrTerminZakonczenia;

	@Temporal(TemporalType.DATE)
	@Column(name="WNR_URLOP_BEZPL_DO")
	private Date wnrUrlopBezplDo;

	@Temporal(TemporalType.DATE)
	@Column(name="WNR_URLOP_BEZPL_OD")
	private Date wnrUrlopBezplOd;

	@Column(name="WNR_URLOP_PRACA")
	private Double wnrUrlopPraca;

	@Column(name="WNR_URLOP_PRZYGOT")
	private Double wnrUrlopPrzygot;

	@Temporal(TemporalType.DATE)
	@Column(name="WNR_URLOP_SZKOL_DO")
	private Date wnrUrlopSzkolDo;

	@Temporal(TemporalType.DATE)
	@Column(name="WNR_URLOP_SZKOL_OD")
	private Date wnrUrlopSzkolOd;

	@Column(name="WNR_URLOP_ZAJECIA")
	private Double wnrUrlopZajecia;

	@Column(name="WNR_UWAGI")
	private String wnrUwagi;

	@Column(name="WNR_UZASADNIANIE")
	private String wnrUzasadnianie;
	
	@Column(name="WNR_PROCENT_POKRYCIA")
	private Double wnrProcentPokrycia;

	public PptSzkWnRefundacje() {
	}

	public long getWnrId() {
//		this.onRXGet("wnrId");
		return this.wnrId;
	}

	public void setWnrId(long wnrId) {
//		this.onRXSet("wnrId", this.wnrId, wnrId);
		this.wnrId = wnrId;
	}

	public String getWnrFCzyFaktura() {
		return this.wnrFCzyFaktura;
	}

	public void setWnrFCzyFaktura(String wnrFCzyFaktura) {
		this.wnrFCzyFaktura = wnrFCzyFaktura;
	}

	public String getWnrFCzyHarmonogram() {
		return this.wnrFCzyHarmonogram;
	}

	public void setWnrFCzyHarmonogram(String wnrFCzyHarmonogram) {
		this.wnrFCzyHarmonogram = wnrFCzyHarmonogram;
	}

	public String getWnrFCzyIprz() {
		return this.wnrFCzyIprz;
	}

	public void setWnrFCzyIprz(String wnrFCzyIprz) {
		this.wnrFCzyIprz = wnrFCzyIprz;
	}

	public String getWnrFCzyPokrycie() {
//		this.onRXGet("wnrFCzyPokrycie");
		return this.wnrFCzyPokrycie;
	}

	public void setWnrFCzyPokrycie(String wnrFCzyPokrycie) {
//		this.onRXSet("wnrFCzyPokrycie", this.wnrFCzyPokrycie, wnrFCzyPokrycie);
		this.wnrFCzyPokrycie = wnrFCzyPokrycie;
	}

	public String getWnrInneKoszty() {
		return this.wnrInneKoszty;
	}

	public void setWnrInneKoszty(String wnrInneKoszty) {
		this.wnrInneKoszty = wnrInneKoszty;
	}


	public Double getWnrKwotaInneKoszty() {
//		this.onRXGet("wnrKwotaInneKoszty");
		return this.wnrKwotaInneKoszty;
	}

	public void setWnrKwotaInneKoszty(Double wnrKwotaInneKoszty) {
//		this.onRXSet("wnrKwotaInneKoszty", this.wnrKwotaInneKoszty, wnrKwotaInneKoszty);
		this.wnrKwotaInneKoszty = wnrKwotaInneKoszty;
	}


	public String getWnrNazwa() {
		return this.wnrNazwa;
	}

	public void setWnrNazwa(String wnrNazwa) {
		this.wnrNazwa = wnrNazwa;
	}

	public String getWnrOpinia() {
		return this.wnrOpinia;
	}

	public void setWnrOpinia(String wnrOpinia) {
		this.wnrOpinia = wnrOpinia;
	}

	public Long getWnrPrcId() {
		return this.wnrPrcId;
	}

	public void setWnrPrcId(Long wnrPrcId) {
		this.wnrPrcId = wnrPrcId;
	}

	public String getWnrTematOrganizator() {
		return this.wnrTematOrganizator;
	}

	public void setWnrTematOrganizator(String wnrTematOrganizator) {
		this.wnrTematOrganizator = wnrTematOrganizator;
	}

	public Date getWnrTerminRozpoczecia() {
		return this.wnrTerminRozpoczecia;
	}

	public void setWnrTerminRozpoczecia(Date wnrTerminRozpoczecia) {
		this.wnrTerminRozpoczecia = wnrTerminRozpoczecia;
	}

	public Date getWnrTerminZakonczenia() {
		return wnrTerminZakonczenia;
	}

	public void setWnrTerminZakonczenia(Date wnrTerminZakonczenia) {
		this.wnrTerminZakonczenia = wnrTerminZakonczenia;
	}

	public Date getWnrUrlopBezplDo() {
		return this.wnrUrlopBezplDo;
	}

	public void setWnrUrlopBezplDo(Date wnrUrlopBezplDo) {
		this.wnrUrlopBezplDo = wnrUrlopBezplDo;
	}

	public Date getWnrUrlopBezplOd() {
		return this.wnrUrlopBezplOd;
	}

	public void setWnrUrlopBezplOd(Date wnrUrlopBezplOd) {
		this.wnrUrlopBezplOd = wnrUrlopBezplOd;
	}

	public Double getWnrUrlopPraca() {
		return this.wnrUrlopPraca;
	}

	public void setWnrUrlopPraca(Double wnrUrlopPraca) {
		this.wnrUrlopPraca = wnrUrlopPraca;
	}

	public Double getWnrUrlopPrzygot() {
		return this.wnrUrlopPrzygot;
	}

	public void setWnrUrlopPrzygot(Double wnrUrlopPrzygot) {
		this.wnrUrlopPrzygot = wnrUrlopPrzygot;
	}

	public Date getWnrUrlopSzkolDo() {
		return this.wnrUrlopSzkolDo;
	}

	public void setWnrUrlopSzkolDo(Date wnrUrlopSzkolDo) {
		this.wnrUrlopSzkolDo = wnrUrlopSzkolDo;
	}

	public Date getWnrUrlopSzkolOd() {
		return this.wnrUrlopSzkolOd;
	}

	public void setWnrUrlopSzkolOd(Date wnrUrlopSzkolOd) {
		this.wnrUrlopSzkolOd = wnrUrlopSzkolOd;
	}

	public Double getWnrUrlopZajecia() {
		return this.wnrUrlopZajecia;
	}

	public void setWnrUrlopZajecia(Double wnrUrlopZajecia) {
		this.wnrUrlopZajecia = wnrUrlopZajecia;
	}

	public String getWnrUwagi() {
		return this.wnrUwagi;
	}

	public void setWnrUwagi(String wnrUwagi) {
		this.wnrUwagi = wnrUwagi;
	}

	public String getWnrUzasadnianie() {
		return this.wnrUzasadnianie;
	}

	public void setWnrUzasadnianie(String wnrUzasadnianie) {
		this.wnrUzasadnianie = wnrUzasadnianie;
	}






	public Double getWnrKwotaPokrycia() {
//		this.onRXGet("wnrKwotaPokrycia");
		return this.wnrKwotaPokrycia;
	}

	public void setWnrKwotaPokrycia(Double wnrKwotaPokrycia) {
		boolean eq = eq(wnrKwotaPokrycia, this.wnrKwotaPokrycia);
//		this.onRXSet("wnrKwotaPokrycia", this.wnrKwotaPokrycia, wnrKwotaPokrycia);
		this.wnrKwotaPokrycia = wnrKwotaPokrycia;
		if (!eq){
			if (this.wnrKoszt ==null || this.wnrKoszt.equals(0D))
				this.wnrProcentPokrycia = 0D;
			else
				this.wnrProcentPokrycia = round(nz(this.wnrKwotaPokrycia) / this.wnrKoszt * 100, 2);
		}
	}




	public Double getWnrProcentPokrycia() {
//		this.onRXGet("wnrProcentPokrycia");
		return this.wnrProcentPokrycia;
	}

	public void setWnrProcentPokrycia(Double wnrProcentPokrycia) {
		boolean eq = eq(wnrProcentPokrycia, this.wnrProcentPokrycia);

//		this.onRXSet("wnrProcentPokrycia", this.wnrProcentPokrycia, wnrProcentPokrycia);
		this.wnrProcentPokrycia = wnrProcentPokrycia;

		if(!eq){
			this.wnrKwotaPokrycia = round(nz(this.wnrProcentPokrycia) / 100d * nz(this.wnrKoszt), 2);
		}



//		if (this.getWnrProcentPokrycia() == null || this.getWnrKoszt() == null)
//			this.setWnrKwotaPokrycia(0d);
//		else {
//			Double kwota = round(this.getWnrProcentPokrycia() * this.getWnrKoszt() / 100,2);
//
//			this.setWnrKwotaPokrycia(round(kwota, 2));
//		}
	}





	public Double getWnrKoszt() {
//		this.onRXGet("wnrKoszt");
		return this.wnrKoszt;
	}

	public void setWnrKoszt(Double wnrKoszt) {
//		this.onRXSet("wnrKoszt", this.wnrKoszt, wnrKoszt);
		this.wnrKoszt = wnrKoszt;

		if (this.getWnrKwotaPokrycia() == null || this.getWnrKoszt() == null)
			this.setWnrProcentPokrycia(0d);
		else {
			Double procent = this.getWnrKwotaPokrycia().doubleValue() / this.getWnrKoszt().doubleValue() * 100;
			this.setWnrProcentPokrycia( round(procent, 2));
		}
	}


}