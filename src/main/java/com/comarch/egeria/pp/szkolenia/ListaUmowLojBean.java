package com.comarch.egeria.pp.szkolenia;

import java.io.IOException;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

@Named
@Scope("view")
public class ListaUmowLojBean extends SqlBean {

	@Inject
	private SessionBean sessionBean;

	public void editSelectedRow() {
		System.out.println(
				"=================================ListaUmowLojBean.editSelectedRow===================================");
		SqlDataSelectionsHandler p = this.getSql("PP_SZKOL_LOJ");
		if(p==null) p=this.getSql("PP_SZKOL_LOJ_NROZ");
		if(p==null) p=this.getSql("PP_SZKOL_LOJ_ROZ");
		
		if (p != null) {
			System.out.println(p.getName() + " / currentRow: " + p.getCurrentRow());

//			sessionBean.setSelectedUmowaLoj(p.getCurrentRow());
			
			Object backId = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id",
					p.getCurrentRow().getIdAsString());
			String vRootId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", p.getCurrentRow());



			// Przekierowanie do edycji rekordu
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("UmowaLojal");
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		System.out.println(
				"============================================================================================");
	}
	
	

	public void dodajNowyArkusz() {
		try {
			sessionBean.setSelectedUmowaLoj(null);
			FacesContext.getCurrentInstance().getExternalContext().redirect("UmowaLojal");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
