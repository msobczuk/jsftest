package com.comarch.egeria.pp.szkolenia;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.szkolenia.model.OstPlanIndyw;
import com.comarch.egeria.pp.szkolenia.model.PptSzkSzkolenia;
import com.comarch.egeria.pp.szkolenia.model.PptUczestnicySzkolen;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;


@ManagedBean 
@Named
@Scope("view")
public class PlanSzkolenBean extends SqlBean{
	private static final Logger log = LogManager.getLogger();//public bo wiekszosc beanow dziedziczy po tym

	
	@Inject
	RejestrSzkolenBean rejestrSzkolenBean;
	
	DataRow terminSzkoleniaDR;
	
	private Integer rokPlanuSzk;
	
	private ArrayList<String> invZaznPrzypisanieTerminu = new ArrayList<>();
	
	
	@PostConstruct
	public void init(){
		this.invZaznPrzypisanieTerminu.add( "Nie zaznaczono żadnego zapisu." );
		setRokPlanuSzk(Calendar.getInstance().get(Calendar.YEAR));
		
		this.getLW("PPL_SZK_PLAN").clearSelection();
	};
	
	
	
	public void przypiszTermin(){
		ArrayList<String> msgW = new ArrayList<>();
		terminSzkoleniaDR = rejestrSzkolenBean.getLW("ppl_szk_terminy_szkolenia").getCurrentRow();

		List<DataRow> selectedRows = this.getLW("PPL_SZK_PLAN", this.rokPlanuSzk).getSelectedRows();
		if (selectedRows.isEmpty()){
			User.warn("Nie wskazano żadnego wiersza z zapisem na szkolenie.");
			return;
		}
		
		if (terminSzkoleniaDR==null){
			User.warn("Nie wskazano terminu szkolenia.");
			return;
		}
		
		
		try(HibernateContext h = new HibernateContext()){
			Session s = h.getSession();
			PptSzkSzkolenia szk = s.get(PptSzkSzkolenia.class, terminSzkoleniaDR.getIdAsLong());
			//List<Long> collect = szk.getPptSzkPlanIndyws().stream().map(p->p.getPisId()).collect(Collectors.toList());
			
			Long szkMaxUczestnikow = szk.getSzkMaxUczestnikow();
			if (szkMaxUczestnikow==null)
				szkMaxUczestnikow = 999L;
			
			int i = 0;
			for (Object oRow : selectedRows.toArray() ) {
				DataRow r = (DataRow) oRow;
				List<PptUczestnicySzkolen> uszNotRezerwowi = szk.getPptUczestnicySzkolens().stream().filter(u -> !"T".equals(u.getUszRez()) ).collect(Collectors.toList());
				if (szkMaxUczestnikow <= uszNotRezerwowi.size()){
					msgW.add(String.format("Przetworzono %1$s z %2$s wierszy. Faktyczna liczba uczestników ma nie przekraczać maksymalnej liczby uczestników dla terminu szkolenia.", i, selectedRows.size() ));
					this.resetDatasourceAndUpdateAllComponents("ppl_szk_plan", null);
					break;
				}
				
				i++;
				
				
				Long pisId = r.getLong("pis_id");
				Long prcId = r.getLong("pis_prc_id");

				//modyfikacja zapisu...
				OstPlanIndyw pis = szk.getPptSzkPlanIndyws().stream().filter(p->(pisId==p.getPisId())).findFirst().orElse(null);
				if (pis==null){
					pis = (OstPlanIndyw) s.get(OstPlanIndyw.class, pisId);
					
					PptSzkSzkolenia szkOld = pis.getPptSzkSzkolenia();
					if (szkOld!=null){
						if ("ZREAL".equals(szkOld.getSzkStatus())){
							msgW.add ( String.format("Modyfikacja zapisu #%1$s pominięta. "
									+ "Zapis dla pracownika %2$s jest powiązany ze szkoleniem %3$s (lokalizacja: %4$s; w dn. %5$s do %6$s), dla którego status ustawiono na zrealizowany.",
										pis.getPisId(), r.get("osoba"), r.get("pis_szk_id"), r.get("szk_lokalizacja"), r.get("szk_data_od"), r.get("szk_data_do") ).replace("00:00:00.0", ""));
							continue;
						}else {
 						    //	TODO: co jesi wczesniej zapisano juz uczetnika na inne szkolenie... moze tam robimy go jako rezerwowego - np:
							szkOld.getPptUczestnicySzkolens().stream().filter(p->p.getUszPrcId() == prcId).forEach(p->p.setUszRez("T"));
						}
					}
					
					szk.addPptSzkPlanIndyw(pis); //aktualizacja kolumny PPT_SZK_PLAN_INDYW.pis_szk_id 
					s.saveOrUpdate(pis);
					
				}
				
				//dodaj/zmien uczestnika szkolen: rezerwowy = N				
				PptUczestnicySzkolen usz = szk.getPptUczestnicySzkolens().stream().filter(p->eq(p.getUszPrcId(), prcId)).findFirst().orElse(null); 
				if (usz == null ){
					usz = new PptUczestnicySzkolen();
					szk.addPptUczestnicySzkolen(usz);
					usz.setUszPrcId(r.getLong("pis_prc_id"));
				} 
				
				usz.setUszRez("N");//N tylko jesi ni eprzekrocozno maks liczby uczestnikow!!!
			
			}
			
			s.saveOrUpdate(szk);
			s.beginTransaction().commit();
			

			for (String msg : msgW) {
				User.warn(msg);
			}

			if (msgW.isEmpty() && !selectedRows.isEmpty())
//				User.info("Zamiany zapisano.");
				User.info("Zapisano zmiany.");
			
			
		} catch (Exception e) {
			User.alert(""+e.getMessage());
			log.error(e.getMessage(), e);
		}
		this.getLW("PPL_SZK_PLAN").clearSelection();
		this.resetDatasourceAndUpdateAllComponents("ppl_szk_plan", null);
	}


	
	public void test(){
		this.resetDatasourceAndUpdateAllComponents("ppl_szk_plan", null);
		System.out.println( this.getLW("ppl_szk_plan", this.getRokPlanuSzk()).getSelectedRows() );
	}
	
	
	
	
	
	
	
	public void calcInvZaznPrzypisanieTerminu() {

		SqlDataSelectionsHandler lw = this.getLW("PPL_SZK_PLAN", this.getRokPlanuSzk());
		ArrayList<String>  ret = new ArrayList<>();
		
		if(lw.getSelectedRows().isEmpty()){
			ret.add(String.format( "Nie zaznaczono żadnego zapisu." ) );
		}
	
		for (DataRow r : lw.getSelectedRows() ) {
			String zrodlo = ""+ r.get("zrodlo");
			String szkStatus = ""+ r.get("szk_status");
			String dstnNazwa =""+ r.get("dstn_nazwa");
			Object pisId = r.get("pis_id");
			Object szkId = r.get("pis_szk_id");
			Object osoba = r.get("osoba");
			
			if ("SZK".equals(zrodlo)) {
				
//				if ("POZIOM10".equals( dstnNazwa )){
//					ret.add(String.format( "Zapis ( %1$s / %2$s ) nie został przekazany do akceptacji w dz. szkol.", pisId, osoba ) );
//				}
				
				if ("POZIOM10".equals( dstnNazwa ) || "POZIOM20".equals( dstnNazwa )){
					ret.add(String.format( "Zapis ( %1$s / %2$s ) nie został zaakceptowany w dz. szkol.", pisId, osoba ) );
				}				
				
				if ("ZREAL".equals( szkStatus )){
					ret.add(String.format( "Zapis ( %1$s / %2$s ) jest powiązany ze szkoleniem %3$s (lokalizacja: %4$s; w dn. %5$s do %6$s), dla którego status ustawiono na zrealizowany."
							, pisId, osoba, szkId, r.get("szk_lokalizacja"), r.get("szk_data_od"), r.get("szk_data_do") ).replace("00:00:00.0", "") );
				}
			}
		}
		
		
		String oldWarn = String.join("<br/><br/>", this.invZaznPrzypisanieTerminu);
		this.invZaznPrzypisanieTerminu = ret;
		String newWarn = String.join("<br/><br/>", this.invZaznPrzypisanieTerminu);
		if (!ret.isEmpty() && !oldWarn.equals(newWarn) && !"Nie zaznaczono żadnego zapisu.".equals(newWarn) ){
			User.warn(newWarn);
		}
				
//		return ret;
	}


	public ArrayList<String> getInvZaznPrzypisanieTerminu() {
		return invZaznPrzypisanieTerminu;
	}


	public void setInvZaznPrzypisanieTerminu(ArrayList<String> invZaznPrzypisanieTerminu) {
		this.invZaznPrzypisanieTerminu = invZaznPrzypisanieTerminu;
	}



	public Integer getRokPlanuSzk() {
		return rokPlanuSzk;
	}



	public void setRokPlanuSzk(Integer rokPlanuSzk) {
		this.rokPlanuSzk = rokPlanuSzk;
	}
	
	
}
