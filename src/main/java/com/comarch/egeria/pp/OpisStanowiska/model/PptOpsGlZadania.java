package com.comarch.egeria.pp.OpisStanowiska.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_OPS_GL_ZADANIA database table.
 * 
 */
@Entity
@Table(name="PPT_OPS_GL_ZADANIA", schema="PPADM")
@NamedQuery(name="PptOpsGlZadania.findAll", query="SELECT w FROM PptOpsGlZadania w")
public class PptOpsGlZadania implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_PPT_OPS_GL_ZADANIA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OPS_GL_ZADANIA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_PPT_OPS_GL_ZADANIA")
	@Column(name="GLZAD_ID")
	private long glzadId;

//	@Temporal(TemporalType.DATE)
//	@Column(name="GLZAD_AUDYT_DM")
//	private Date glzadAudytDm;
//
//	@Temporal(TemporalType.DATE)
//	@Column(name="GLZAD_AUDYT_DT")
//	private Date glzadAudytDt;
//
//	@Column(name="GLZAD_AUDYT_KM")
//	private String glzadAudytKm;
//
//	@Column(name="GLZAD_AUDYT_LM")
//	private String glzadAudytLm;
//
//	@Column(name="GLZAD_AUDYT_UM")
//	private String glzadAudytUm;
//
//	@Column(name="GLZAD_AUDYT_UT")
//	private String glzadAudytUt;

	@Column(name="GLZAD_LP")
	private Long glzadLp;

	@Column(name="GLZAD_OPIS")
	private String glzadOpis;

	@Column(name="GLZAD_RODZAJ")
	private String glzadRodzaj;

	//bi-directional many-to-one association to PptOpisStanowiska
	@ManyToOne
	@JoinColumn(name="GLZAD_WOSP_ID")
	private PptOpisStanowiska pptOpisStanowiska;

	public PptOpsGlZadania() {
	}

	public long getGlzadId() {
		return this.glzadId;
	}

	public void setGlzadId(long glzadId) {
		this.glzadId = glzadId;
	}

//	public Date getGlzadAudytDm() {
//		return this.glzadAudytDm;
//	}
//
//	public void setGlzadAudytDm(Date glzadAudytDm) {
//		this.glzadAudytDm = glzadAudytDm;
//	}
//
//	public Date getGlzadAudytDt() {
//		return this.glzadAudytDt;
//	}
//
//	public void setGlzadAudytDt(Date glzadAudytDt) {
//		this.glzadAudytDt = glzadAudytDt;
//	}
//
//	public String getGlzadAudytKm() {
//		return this.glzadAudytKm;
//	}
//
//	public void setGlzadAudytKm(String glzadAudytKm) {
//		this.glzadAudytKm = glzadAudytKm;
//	}
//
//	public String getGlzadAudytLm() {
//		return this.glzadAudytLm;
//	}
//
//	public void setGlzadAudytLm(String glzadAudytLm) {
//		this.glzadAudytLm = glzadAudytLm;
//	}
//
//	public String getGlzadAudytUm() {
//		return this.glzadAudytUm;
//	}
//
//	public void setGlzadAudytUm(String glzadAudytUm) {
//		this.glzadAudytUm = glzadAudytUm;
//	}
//
//	public String getGlzadAudytUt() {
//		return this.glzadAudytUt;
//	}
//
//	public void setGlzadAudytUt(String glzadAudytUt) {
//		this.glzadAudytUt = glzadAudytUt;
//	}

	public Long getGlzadLp() {
		return this.glzadLp;
	}

	public void setGlzadLp(Long glzadLp) {
		this.glzadLp = glzadLp;
	}

	public String getGlzadOpis() {
		return this.glzadOpis;
	}

	public void setGlzadOpis(String glzadOpis) {
		this.glzadOpis = glzadOpis;
	}

	public String getGlzadRodzaj() {
		return this.glzadRodzaj;
	}

	public void setGlzadRodzaj(String glzadRodzaj) {
		this.glzadRodzaj = glzadRodzaj;
	}

	public PptOpisStanowiska getPptOpisStanowiska() {
		return this.pptOpisStanowiska;
	}

	public void setPptOpisStanowiska(PptOpisStanowiska pptOpisStanowiska) {
		this.pptOpisStanowiska = pptOpisStanowiska;
	}

}