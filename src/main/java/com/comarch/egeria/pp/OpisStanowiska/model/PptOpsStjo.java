package com.comarch.egeria.pp.OpisStanowiska.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_OPS_STJO database table.
 * 
 */
@Entity
@Table(name="PPT_OPS_STJO", schema="PPADM")
@NamedQuery(name="PptOpsStjo.findAll", query="SELECT w FROM PptOpsStjo w")
public class PptOpsStjo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_PPT_OPS_STJO",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OPS_STJO", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_PPT_OPS_STJO")
	@Column(name="WOSST_ID")
	private long wosstId;

	@Temporal(TemporalType.DATE)
	@Column(name="WOSST_AUDYT_DM")
	private Date wosstAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="WOSST_AUDYT_DT")
	private Date wosstAudytDt;

	@Column(name="WOSST_AUDYT_KM")
	private String wosstAudytKm;

	@Column(name="WOSST_AUDYT_LM")
	private String wosstAudytLm;

	@Column(name="WOSST_AUDYT_UM")
	private String wosstAudytUm;

	@Column(name="WOSST_AUDYT_UT")
	private String wosstAudytUt;

	@Column(name="WOSST_STJO_ID")
	private Long wosstStjoId;

	@Column(name="WOSST_SYMBOL")
	private String wosstSymbol;

	//bi-directional many-to-one association to PptOpisStanowiska
	@ManyToOne
	@JoinColumn(name="WOSST_WOSP_ID")
	private PptOpisStanowiska pptOpisStanowiska;

	public PptOpsStjo() {
	}

	public long getWosstId() {
		return this.wosstId;
	}

	public void setWosstId(long wosstId) {
		this.wosstId = wosstId;
	}

	public Date getWosstAudytDm() {
		return this.wosstAudytDm;
	}

	public void setWosstAudytDm(Date wosstAudytDm) {
		this.wosstAudytDm = wosstAudytDm;
	}

	public Date getWosstAudytDt() {
		return this.wosstAudytDt;
	}

	public void setWosstAudytDt(Date wosstAudytDt) {
		this.wosstAudytDt = wosstAudytDt;
	}

	public String getWosstAudytKm() {
		return this.wosstAudytKm;
	}

	public void setWosstAudytKm(String wosstAudytKm) {
		this.wosstAudytKm = wosstAudytKm;
	}

	public String getWosstAudytLm() {
		return this.wosstAudytLm;
	}

	public void setWosstAudytLm(String wosstAudytLm) {
		this.wosstAudytLm = wosstAudytLm;
	}

	public String getWosstAudytUm() {
		return this.wosstAudytUm;
	}

	public void setWosstAudytUm(String wosstAudytUm) {
		this.wosstAudytUm = wosstAudytUm;
	}

	public String getWosstAudytUt() {
		return this.wosstAudytUt;
	}

	public void setWosstAudytUt(String wosstAudytUt) {
		this.wosstAudytUt = wosstAudytUt;
	}

	public Long getWosstStjoId() {
		return this.wosstStjoId;
	}

	public void setWosstStjoId(Long wosstStjoId) {
		this.wosstStjoId = wosstStjoId;
	}

	public String getWosstSymbol() {
		return this.wosstSymbol;
	}

	public void setWosstSymbol(String wosstSymbol) {
		this.wosstSymbol = wosstSymbol;
	}

	public PptOpisStanowiska getPptOpisStanowiska() {
		return this.pptOpisStanowiska;
	}

	public void setPptOpisStanowiska(PptOpisStanowiska pptOpisStanowiska) {
		this.pptOpisStanowiska = pptOpisStanowiska;
	}

}