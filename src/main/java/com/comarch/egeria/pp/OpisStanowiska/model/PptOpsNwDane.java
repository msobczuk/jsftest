package com.comarch.egeria.pp.OpisStanowiska.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


/**
 * The persistent class for the PPT_OPS_NW_DANE database table.
 * 
 */
@Entity
@Table(name="PPT_OPS_NW_DANE", schema="PPADM")
@NamedQuery(name="PptOpsNwDane.findAll", query="SELECT w FROM PptOpsNwDane w")
public class PptOpsNwDane implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_PPT_OPS_NW_DANE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OPS_NW_DANE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_PPT_OPS_NW_DANE")
	@Column(name="WOSPN_ID")
	private Long wospnId;

//	@Temporal(TemporalType.DATE)
//	@Column(name="WOSPN_AUDYT_DM")
//	private Date wospnAudytDm;
//
//	@Temporal(TemporalType.DATE)
//	@Column(name="WOSPN_AUDYT_DT")
//	private Date wospnAudytDt;

//	@Column(name="WOSPN_AUDYT_KM")
//	private String wospnAudytKm;
//
//	@Column(name="WOSPN_AUDYT_LM")
//	private String wospnAudytLm;
//
//	@Column(name="WOSPN_AUDYT_UM")
//	private String wospnAudytUm;
//
//	@Column(name="WOSPN_AUDYT_UT")
//	private String wospnAudytUt;

	@Column(name="WOSPN_CZSZ_INNE")
	private String wospnCzszInne;

	@Column(name="WOSPN_CZSZ_PRZYKLAD")
	private String wospnCzszPrzyklad;

	@Column(name="WOSPN_F_CZSZ_ATR_01")
	private String wospnFCzszAtr01;

	@Column(name="WOSPN_F_CZSZ_ATR_02")
	private String wospnFCzszAtr02;

	@Column(name="WOSPN_F_CZSZ_ATR_03")
	private String wospnFCzszAtr03;

	@Column(name="WOSPN_F_CZSZ_ATR_04")
	private String wospnFCzszAtr04;

	@Column(name="WOSPN_F_CZSZ_ATR_05")
	private String wospnFCzszAtr05;

	@Column(name="WOSPN_F_CZSZ_ATR_06")
	private String wospnFCzszAtr06;

	@Column(name="WOSPN_F_CZSZ_ATR_07")
	private String wospnFCzszAtr07;

	@Column(name="WOSPN_F_CZSZ_ATR_08")
	private String wospnFCzszAtr08;

	@Column(name="WOSPN_F_CZSZ_ATR_09")
	private String wospnFCzszAtr09;

	@Column(name="WOSPN_F_CZSZ_ATR_10")
	private String wospnFCzszAtr10;

	@Column(name="WOSPN_F_HCP_ATR_01")
	private String wospnFHcpAtr01;

	@Column(name="WOSPN_F_HCP_ATR_02")
	private String wospnFHcpAtr02;

	@Column(name="WOSPN_F_HCP_ATR_03")
	private String wospnFHcpAtr03;

	@Column(name="WOSPN_F_HCP_ATR_04")
	private String wospnFHcpAtr04;

	@Column(name="WOSPN_F_KZ_ATR_01")
	private String wospnFKzAtr01;

	@Column(name="WOSPN_F_KZ_ATR_02")
	private String wospnFKzAtr02;

	@Column(name="WOSPN_F_KZ_ATR_03")
	private String wospnFKzAtr03;

	@Column(name="WOSPN_F_KZ_ATR_04")
	private String wospnFKzAtr04;

	@Column(name="WOSPN_F_LP_ATR_01")
	private String wospnFLpAtr01;

	@Column(name="WOSPN_F_LP_ATR_02")
	private String wospnFLpAtr02;

	@Column(name="WOSPN_F_LP_ATR_03")
	private String wospnFLpAtr03;

	@Column(name="WOSPN_F_LP_ATR_04")
	private String wospnFLpAtr04;

	@Column(name="WOSPN_F_LP_ATR_05")
	private String wospnFLpAtr05;

	@Column(name="WOSPN_F_LP_ATR_06")
	private String wospnFLpAtr06;

	@Column(name="WOSPN_F_UIP_ATR_01")
	private String wospnFUipAtr01;

	@Column(name="WOSPN_F_UIP_ATR_02")
	private String wospnFUipAtr02;

	@Column(name="WOSPN_F_UIP_ATR_03")
	private String wospnFUipAtr03;

	@Column(name="WOSPN_F_UIP_ATR_04")
	private String wospnFUipAtr04;

	@Column(name="WOSPN_F_UIP_ATR_05")
	private String wospnFUipAtr05;

	@Column(name="WOSPN_F_UIP_ATR_06")
	private String wospnFUipAtr06;

	@Column(name="WOSPN_HCP_PRZYKLAD")
	private String wospnHcpPrzyklad;

	@Column(name="WOSPN_KZ_ATR_01_OPIS")
	private String wospnKzAtr01Opis;

	@Column(name="WOSPN_KZ_ATR_02_OPIS")
	private String wospnKzAtr02Opis;

	@Column(name="WOSPN_KZ_ATR_03_OPIS")
	private String wospnKzAtr03Opis;

	@Column(name="WOSPN_KZ_ATR_04_OPIS")
	private String wospnKzAtr04Opis;

	@Column(name="WOSPN_NSII_OPIS")
	private String wospnNsiiOpis;

	@Column(name="WOSPN_NSII_PRZYKLAD")
	private String wospnNsiiPrzyklad;

	@Column(name="WOSPN_UIP_INNE")
	private String wospnUipInne;

	@Column(name="WOSPN_WKID_ATR_01")
	private String wospnWkidAtr01;

	@Column(name="WOSPN_WKID_ATR_02")
	private String wospnWkidAtr02;

	@Column(name="WOSPN_WKID_ATR_03")
	private String wospnWkidAtr03;

	@Column(name="WOSPN_WKID_ATR_04")
	private String wospnWkidAtr04;

	@Column(name="WOSPN_WKID_ATR_05")
	private String wospnWkidAtr05;

	@Column(name="WOSPN_WKID_ATR_06")
	private String wospnWkidAtr06;

	@Column(name="WOSPN_WKID_ATR_07")
	private String wospnWkidAtr07;

	@Column(name="WOSPN_WKID_ATR_08")
	private String wospnWkidAtr08;

	@Column(name="WOSPN_WKID_ATR_09")
	private String wospnWkidAtr09;

	@Column(name="WOSPN_WKID_ATR_10")
	private String wospnWkidAtr10;

	@Column(name="WOSPN_WKID_ATR_11")
	private String wospnWkidAtr11;

	@Column(name="WOSPN_WKID_ATR_12")
	private String wospnWkidAtr12;

	@Column(name="WOSPN_ZIK_OPIS")
	private String wospnZikOpis;

	@Column(name="WOSPN_ZIK_PRZYKLAD")
	private String wospnZikPrzyklad;

	
	@ManyToOne
	@JoinColumn(name="WOSPN_WOSP_ID")
	private PptOpisStanowiska pptOpisStanowiska;

	public PptOpsNwDane() {
	}

	public Long getWospnId() {
		return this.wospnId;
	}

	public void setWospnId(Long wospnId) {
		this.wospnId = wospnId;
	}

//	public Date getWospnAudytDm() {
//		return this.wospnAudytDm;
//	}
//
//	public void setWospnAudytDm(Date wospnAudytDm) {
//		this.wospnAudytDm = wospnAudytDm;
//	}
//
//	public Date getWospnAudytDt() {
//		return this.wospnAudytDt;
//	}
//
//	public void setWospnAudytDt(Date wospnAudytDt) {
//		this.wospnAudytDt = wospnAudytDt;
//	}

//	public String getWospnAudytKm() {
//		return this.wospnAudytKm;
//	}
//
//	public void setWospnAudytKm(String wospnAudytKm) {
//		this.wospnAudytKm = wospnAudytKm;
//	}
//
//	public String getWospnAudytLm() {
//		return this.wospnAudytLm;
//	}
//
//	public void setWospnAudytLm(String wospnAudytLm) {
//		this.wospnAudytLm = wospnAudytLm;
//	}
//
//	public String getWospnAudytUm() {
//		return this.wospnAudytUm;
//	}
//
//	public void setWospnAudytUm(String wospnAudytUm) {
//		this.wospnAudytUm = wospnAudytUm;
//	}
//
//	public String getWospnAudytUt() {
//		return this.wospnAudytUt;
//	}
//
//	public void setWospnAudytUt(String wospnAudytUt) {
//		this.wospnAudytUt = wospnAudytUt;
//	}

	public String getWospnCzszInne() {
		return this.wospnCzszInne;
	}

	public void setWospnCzszInne(String wospnCzszInne) {
		this.wospnCzszInne = wospnCzszInne;
	}

	public String getWospnCzszPrzyklad() {
		return this.wospnCzszPrzyklad;
	}

	public void setWospnCzszPrzyklad(String wospnCzszPrzyklad) {
		this.wospnCzszPrzyklad = wospnCzszPrzyklad;
	}

	public String getWospnFCzszAtr01() {
		return this.wospnFCzszAtr01;
	}

	public void setWospnFCzszAtr01(String wospnFCzszAtr01) {
		this.wospnFCzszAtr01 = wospnFCzszAtr01;
	}

	public String getWospnFCzszAtr02() {
		return this.wospnFCzszAtr02;
	}

	public void setWospnFCzszAtr02(String wospnFCzszAtr02) {
		this.wospnFCzszAtr02 = wospnFCzszAtr02;
	}

	public String getWospnFCzszAtr03() {
		return this.wospnFCzszAtr03;
	}

	public void setWospnFCzszAtr03(String wospnFCzszAtr03) {
		this.wospnFCzszAtr03 = wospnFCzszAtr03;
	}

	public String getWospnFCzszAtr04() {
		return this.wospnFCzszAtr04;
	}

	public void setWospnFCzszAtr04(String wospnFCzszAtr04) {
		this.wospnFCzszAtr04 = wospnFCzszAtr04;
	}

	public String getWospnFCzszAtr05() {
		return this.wospnFCzszAtr05;
	}

	public void setWospnFCzszAtr05(String wospnFCzszAtr05) {
		this.wospnFCzszAtr05 = wospnFCzszAtr05;
	}

	public String getWospnFCzszAtr06() {
		return this.wospnFCzszAtr06;
	}

	public void setWospnFCzszAtr06(String wospnFCzszAtr06) {
		this.wospnFCzszAtr06 = wospnFCzszAtr06;
	}

	public String getWospnFCzszAtr07() {
		return this.wospnFCzszAtr07;
	}

	public void setWospnFCzszAtr07(String wospnFCzszAtr07) {
		this.wospnFCzszAtr07 = wospnFCzszAtr07;
	}

	public String getWospnFCzszAtr08() {
		return this.wospnFCzszAtr08;
	}

	public void setWospnFCzszAtr08(String wospnFCzszAtr08) {
		this.wospnFCzszAtr08 = wospnFCzszAtr08;
	}

	public String getWospnFCzszAtr09() {
		return this.wospnFCzszAtr09;
	}

	public void setWospnFCzszAtr09(String wospnFCzszAtr09) {
		this.wospnFCzszAtr09 = wospnFCzszAtr09;
	}

	public String getWospnFCzszAtr10() {
		return this.wospnFCzszAtr10;
	}

	public void setWospnFCzszAtr10(String wospnFCzszAtr10) {
		this.wospnFCzszAtr10 = wospnFCzszAtr10;
	}

	public String getWospnFHcpAtr01() {
		return this.wospnFHcpAtr01;
	}

	public void setWospnFHcpAtr01(String wospnFHcpAtr01) {
		this.wospnFHcpAtr01 = wospnFHcpAtr01;
	}

	public String getWospnFHcpAtr02() {
		return this.wospnFHcpAtr02;
	}

	public void setWospnFHcpAtr02(String wospnFHcpAtr02) {
		this.wospnFHcpAtr02 = wospnFHcpAtr02;
	}

	public String getWospnFHcpAtr03() {
		return this.wospnFHcpAtr03;
	}

	public void setWospnFHcpAtr03(String wospnFHcpAtr03) {
		this.wospnFHcpAtr03 = wospnFHcpAtr03;
	}

	public String getWospnFHcpAtr04() {
		return this.wospnFHcpAtr04;
	}

	public void setWospnFHcpAtr04(String wospnFHcpAtr04) {
		this.wospnFHcpAtr04 = wospnFHcpAtr04;
	}

	public String getWospnFKzAtr01() {
		return this.wospnFKzAtr01;
	}

	public void setWospnFKzAtr01(String wospnFKzAtr01) {
		this.wospnFKzAtr01 = wospnFKzAtr01;
	}

	public String getWospnFKzAtr02() {
		return this.wospnFKzAtr02;
	}

	public void setWospnFKzAtr02(String wospnFKzAtr02) {
		this.wospnFKzAtr02 = wospnFKzAtr02;
	}

	public String getWospnFKzAtr03() {
		return this.wospnFKzAtr03;
	}

	public void setWospnFKzAtr03(String wospnFKzAtr03) {
		this.wospnFKzAtr03 = wospnFKzAtr03;
	}

	public String getWospnFKzAtr04() {
		return this.wospnFKzAtr04;
	}

	public void setWospnFKzAtr04(String wospnFKzAtr04) {
		this.wospnFKzAtr04 = wospnFKzAtr04;
	}

	public String getWospnFLpAtr01() {
		return this.wospnFLpAtr01;
	}

	public void setWospnFLpAtr01(String wospnFLpAtr01) {
		this.wospnFLpAtr01 = wospnFLpAtr01;
	}

	public String getWospnFLpAtr02() {
		return this.wospnFLpAtr02;
	}

	public void setWospnFLpAtr02(String wospnFLpAtr02) {
		this.wospnFLpAtr02 = wospnFLpAtr02;
	}

	public String getWospnFLpAtr03() {
		return this.wospnFLpAtr03;
	}

	public void setWospnFLpAtr03(String wospnFLpAtr03) {
		this.wospnFLpAtr03 = wospnFLpAtr03;
	}

	public String getWospnFLpAtr04() {
		return this.wospnFLpAtr04;
	}

	public void setWospnFLpAtr04(String wospnFLpAtr04) {
		this.wospnFLpAtr04 = wospnFLpAtr04;
	}

	public String getWospnFLpAtr05() {
		return this.wospnFLpAtr05;
	}

	public void setWospnFLpAtr05(String wospnFLpAtr05) {
		this.wospnFLpAtr05 = wospnFLpAtr05;
	}

	public String getWospnFLpAtr06() {
		return this.wospnFLpAtr06;
	}

	public void setWospnFLpAtr06(String wospnFLpAtr06) {
		this.wospnFLpAtr06 = wospnFLpAtr06;
	}

	public String getWospnFUipAtr01() {
		return this.wospnFUipAtr01;
	}

	public void setWospnFUipAtr01(String wospnFUipAtr01) {
		this.wospnFUipAtr01 = wospnFUipAtr01;
	}

	public String getWospnFUipAtr02() {
		return this.wospnFUipAtr02;
	}

	public void setWospnFUipAtr02(String wospnFUipAtr02) {
		this.wospnFUipAtr02 = wospnFUipAtr02;
	}

	public String getWospnFUipAtr03() {
		return this.wospnFUipAtr03;
	}

	public void setWospnFUipAtr03(String wospnFUipAtr03) {
		this.wospnFUipAtr03 = wospnFUipAtr03;
	}

	public String getWospnFUipAtr04() {
		return this.wospnFUipAtr04;
	}

	public void setWospnFUipAtr04(String wospnFUipAtr04) {
		this.wospnFUipAtr04 = wospnFUipAtr04;
	}

	public String getWospnFUipAtr05() {
		return this.wospnFUipAtr05;
	}

	public void setWospnFUipAtr05(String wospnFUipAtr05) {
		this.wospnFUipAtr05 = wospnFUipAtr05;
	}

	public String getWospnFUipAtr06() {
		return this.wospnFUipAtr06;
	}

	public void setWospnFUipAtr06(String wospnFUipAtr06) {
		this.wospnFUipAtr06 = wospnFUipAtr06;
	}

	public String getWospnHcpPrzyklad() {
		return this.wospnHcpPrzyklad;
	}

	public void setWospnHcpPrzyklad(String wospnHcpPrzyklad) {
		this.wospnHcpPrzyklad = wospnHcpPrzyklad;
	}

	public String getWospnKzAtr01Opis() {
		return this.wospnKzAtr01Opis;
	}

	public void setWospnKzAtr01Opis(String wospnKzAtr01Opis) {
		this.wospnKzAtr01Opis = wospnKzAtr01Opis;
	}

	public String getWospnKzAtr02Opis() {
		return this.wospnKzAtr02Opis;
	}

	public void setWospnKzAtr02Opis(String wospnKzAtr02Opis) {
		this.wospnKzAtr02Opis = wospnKzAtr02Opis;
	}

	public String getWospnKzAtr03Opis() {
		return this.wospnKzAtr03Opis;
	}

	public void setWospnKzAtr03Opis(String wospnKzAtr03Opis) {
		this.wospnKzAtr03Opis = wospnKzAtr03Opis;
	}

	public String getWospnKzAtr04Opis() {
		return this.wospnKzAtr04Opis;
	}

	public void setWospnKzAtr04Opis(String wospnKzAtr04Opis) {
		this.wospnKzAtr04Opis = wospnKzAtr04Opis;
	}

	public String getWospnNsiiOpis() {
		return this.wospnNsiiOpis;
	}

	public void setWospnNsiiOpis(String wospnNsiiOpis) {
		this.wospnNsiiOpis = wospnNsiiOpis;
	}

	public String getWospnNsiiPrzyklad() {
		return this.wospnNsiiPrzyklad;
	}

	public void setWospnNsiiPrzyklad(String wospnNsiiPrzyklad) {
		this.wospnNsiiPrzyklad = wospnNsiiPrzyklad;
	}

	public String getWospnUipInne() {
		return this.wospnUipInne;
	}

	public void setWospnUipInne(String wospnUipInne) {
		this.wospnUipInne = wospnUipInne;
	}

	public String getWospnWkidAtr01() {
		return this.wospnWkidAtr01;
	}

	public void setWospnWkidAtr01(String wospnWkidAtr01) {
		this.wospnWkidAtr01 = wospnWkidAtr01;
	}

	public String getWospnWkidAtr02() {
		return this.wospnWkidAtr02;
	}

	public void setWospnWkidAtr02(String wospnWkidAtr02) {
		this.wospnWkidAtr02 = wospnWkidAtr02;
	}

	public String getWospnWkidAtr03() {
		return this.wospnWkidAtr03;
	}

	public void setWospnWkidAtr03(String wospnWkidAtr03) {
		this.wospnWkidAtr03 = wospnWkidAtr03;
	}

	public String getWospnWkidAtr04() {
		return this.wospnWkidAtr04;
	}

	public void setWospnWkidAtr04(String wospnWkidAtr04) {
		this.wospnWkidAtr04 = wospnWkidAtr04;
	}

	public String getWospnWkidAtr05() {
		return this.wospnWkidAtr05;
	}

	public void setWospnWkidAtr05(String wospnWkidAtr05) {
		this.wospnWkidAtr05 = wospnWkidAtr05;
	}

	public String getWospnWkidAtr06() {
		return this.wospnWkidAtr06;
	}

	public void setWospnWkidAtr06(String wospnWkidAtr06) {
		this.wospnWkidAtr06 = wospnWkidAtr06;
	}

	public String getWospnWkidAtr07() {
		return this.wospnWkidAtr07;
	}

	public void setWospnWkidAtr07(String wospnWkidAtr07) {
		this.wospnWkidAtr07 = wospnWkidAtr07;
	}

	public String getWospnWkidAtr08() {
		return this.wospnWkidAtr08;
	}

	public void setWospnWkidAtr08(String wospnWkidAtr08) {
		this.wospnWkidAtr08 = wospnWkidAtr08;
	}

	public String getWospnWkidAtr09() {
		return this.wospnWkidAtr09;
	}

	public void setWospnWkidAtr09(String wospnWkidAtr09) {
		this.wospnWkidAtr09 = wospnWkidAtr09;
	}

	public String getWospnWkidAtr10() {
		return this.wospnWkidAtr10;
	}

	public void setWospnWkidAtr10(String wospnWkidAtr10) {
		this.wospnWkidAtr10 = wospnWkidAtr10;
	}

	public String getWospnWkidAtr11() {
		return this.wospnWkidAtr11;
	}

	public void setWospnWkidAtr11(String wospnWkidAtr11) {
		this.wospnWkidAtr11 = wospnWkidAtr11;
	}

	public String getWospnWkidAtr12() {
		return this.wospnWkidAtr12;
	}

	public void setWospnWkidAtr12(String wospnWkidAtr12) {
		this.wospnWkidAtr12 = wospnWkidAtr12;
	}

	public String getWospnZikOpis() {
		return this.wospnZikOpis;
	}

	public void setWospnZikOpis(String wospnZikOpis) {
		this.wospnZikOpis = wospnZikOpis;
	}

	public String getWospnZikPrzyklad() {
		return this.wospnZikPrzyklad;
	}

	public void setWospnZikPrzyklad(String wospnZikPrzyklad) {
		this.wospnZikPrzyklad = wospnZikPrzyklad;
	}

	public PptOpisStanowiska getPptOpisStanowiska() {
		return this.pptOpisStanowiska;
	}

	public void setPptOpisStanowiska(PptOpisStanowiska pptOpisStanowiska) {
		this.pptOpisStanowiska = pptOpisStanowiska;
	}

}