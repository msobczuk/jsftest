package com.comarch.egeria.pp.OpisStanowiska;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.common.hibernate.HibernateContext;


@Named
@Scope("view")
public class ListaOpisowStanowiskBean extends SqlBean {

	@Inject
	private SessionBean sessionBean;

	public void editSelectedRow() throws SQLException, IOException {
		SqlDataSelectionsHandler lw = this.getLW("PPL_OPS_LISTA_OPISOW");
		
		if (lw == null)
			return;

		DataRow r = lw.getCurrentRow();
		
		if (r == null)
			return;
		
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id",r.getIdAsString());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", r);

		String rodzaj = (String) r.get("WOSP_RODZAJ_WN"); 
		
		if(rodzaj != null) {
			//wyższe stanowisko
			if (rodzaj.equals("WS")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("NowyOpisSC");
			}
			
			//nie będące wyższym stanowiskiem
			if (rodzaj.equals("NW")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("NowyOpis");
			}
		}
	}
	
}
