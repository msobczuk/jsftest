package com.comarch.egeria.pp.OpisStanowiska.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


/**
 * The persistent class for the PPT_OPS_NW_WYMOGI database table.
 * 
 */
@Entity
@Table(name="PPT_OPS_NW_WYMOGI", schema="PPADM")
@NamedQuery(name="PptOpsNwWymogi.findAll", query="SELECT p FROM PptOpsNwWymogi p")
public class PptOpsNwWymogi implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_OPS_NW_WYMOGI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OPS_NW_WYMOGI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_OPS_NW_WYMOGI")
	@Column(name="WOSPWM_ID")
	private Long wospwmId;

	@Column(name="WOSPWM_RODZAJ")
	private String wospwmRodzaj;

	@Column(name="WOSPWM_F_CZY_NIEZBEDNE")
	private String wospwmFCzyNiezbedne;

	@Column(name="WOSPWM_OPIS_01")
	private String wospwmOpis01;

	@Column(name="WOSPWM_OPIS_02")
	private String wospwmOpis02;

	@Column(name="WOSPWM_OPIS_03")
	private String wospwmOpis03;

	@Column(name="WOSPWM_F_CZY_INNE_01")
	private String wospwmFCzyInne01;

	@Column(name="WOSPWM_F_CZY_INNE_02")
	private String wospwmFCzyInne02;

	@Column(name="WOSPWM_F_CZY_INNE_03")
	private String wospwmFCzyInne03;

	@ManyToOne
	@JoinColumn(name="WOSPWM_WOSP_ID")
	private PptOpisStanowiska pptOpisStanowiska;
	
	public PptOpsNwWymogi() {
	}

	public Long getWospwmId() {
		return this.wospwmId;
	}
	
	public void setWospwmId(Long wospwmId) {
		this.wospwmId = wospwmId;
	}
	
	public String getWospwmRodzaj() {
		return this.wospwmRodzaj;
	}

	public void setWospwmRodzaj(String wospwmRodzaj) {
		this.wospwmRodzaj = wospwmRodzaj;
	}
	
	public String getWospwmFCzyNiezbedne() {
		return this.wospwmFCzyNiezbedne;
	}

	public void setWospwmFCzyNiezbedne(String wospwmFCzyNiezbedne) {
		this.wospwmFCzyNiezbedne = wospwmFCzyNiezbedne;
	}

	public String getWospwmOpis01() {
		return this.wospwmOpis01;
	}

	public void setWospwmOpis01(String wospwmOpis01) {
		this.wospwmOpis01 = wospwmOpis01;
	}

	public String getWospwmOpis02() {
		return this.wospwmOpis02;
	}

	public void setWospwmOpis02(String wospwmOpis02) {
		this.wospwmOpis02 = wospwmOpis02;
	}

	public String getWospwmOpis03() {
		return this.wospwmOpis03;
	}

	public void setWospwmOpis03(String wospwmOpis03) {
		this.wospwmOpis03 = wospwmOpis03;
	}

	public String getWospwmFCzyInne01() {
		return wospwmFCzyInne01;
	}

	public void setWospwmFCzyInne01(String wospwmFCzyInne01) {
		this.wospwmFCzyInne01 = wospwmFCzyInne01;
	}

	public String getWospwmFCzyInne02() {
		return wospwmFCzyInne02;
	}

	public void setWospwmFCzyInne02(String wospwmFCzyInne02) {
		this.wospwmFCzyInne02 = wospwmFCzyInne02;
	}

	public String getWospwmFCzyInne03() {
		return wospwmFCzyInne03;
	}

	public void setWospwmFCzyInne03(String wospwmFCzyInne03) {
		this.wospwmFCzyInne03 = wospwmFCzyInne03;
	}

	public PptOpisStanowiska getPptOpisStanowiska() {
		return pptOpisStanowiska;
	}

	public void setPptOpisStanowiska(PptOpisStanowiska pptOpisStanowiska) {
		this.pptOpisStanowiska = pptOpisStanowiska;
	}

}