package com.comarch.egeria.pp.OpisStanowiska.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_OPS_PRC database table.
 * 
 */
@Entity
@Table(name="PPT_OPS_PRC", schema="PPADM")
@NamedQuery(name="PptOpsPrc.findAll", query="SELECT w FROM PptOpsPrc w")
public class PptOpsPrc implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_PPT_OPS_PRC",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OPS_PRC", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_PPT_OPS_PRC")
	@Column(name="WOSPR_ID")
	private long wosprId;

	@Temporal(TemporalType.DATE)
	@Column(name="WOSPR_AUDYT_DM")
	private Date wosprAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="WOSPR_AUDYT_DT")
	private Date wosprAudytDt;

	@Column(name="WOSPR_AUDYT_KM")
	private String wosprAudytKm;

	@Column(name="WOSPR_AUDYT_LM")
	private String wosprAudytLm;

	@Column(name="WOSPR_AUDYT_UM")
	private String wosprAudytUm;

	@Column(name="WOSPR_AUDYT_UT")
	private String wosprAudytUt;

	@Column(name="WOSPR_PRC_ID")
	private Long wosprPrcId;

	//bi-directional many-to-one association to PptOpisStanowiska
	@ManyToOne
	@JoinColumn(name="WOSPR_WOSP_ID")
	private PptOpisStanowiska pptOpisStanowiska;

	public PptOpsPrc() {
	}

	public long getWosprId() {
		return this.wosprId;
	}

	public void setWosprId(long wosprId) {
		this.wosprId = wosprId;
	}

	public Date getWosprAudytDm() {
		return this.wosprAudytDm;
	}

	public void setWosprAudytDm(Date wosprAudytDm) {
		this.wosprAudytDm = wosprAudytDm;
	}

	public Date getWosprAudytDt() {
		return this.wosprAudytDt;
	}

	public void setWosprAudytDt(Date wosprAudytDt) {
		this.wosprAudytDt = wosprAudytDt;
	}

	public String getWosprAudytKm() {
		return this.wosprAudytKm;
	}

	public void setWosprAudytKm(String wosprAudytKm) {
		this.wosprAudytKm = wosprAudytKm;
	}

	public String getWosprAudytLm() {
		return this.wosprAudytLm;
	}

	public void setWosprAudytLm(String wosprAudytLm) {
		this.wosprAudytLm = wosprAudytLm;
	}

	public String getWosprAudytUm() {
		return this.wosprAudytUm;
	}

	public void setWosprAudytUm(String wosprAudytUm) {
		this.wosprAudytUm = wosprAudytUm;
	}

	public String getWosprAudytUt() {
		return this.wosprAudytUt;
	}

	public void setWosprAudytUt(String wosprAudytUt) {
		this.wosprAudytUt = wosprAudytUt;
	}

	public Long getWosprPrcId() {
		return this.wosprPrcId;
	}

	public void setWosprPrcId(Long wosprPrcId) {
		this.wosprPrcId = wosprPrcId;
	}

	public PptOpisStanowiska getPptOpisStanowiska() {
		return this.pptOpisStanowiska;
	}

	public void setPptOpisStanowiska(PptOpisStanowiska pptOpisStanowiska) {
		this.pptOpisStanowiska = pptOpisStanowiska;
	}

}