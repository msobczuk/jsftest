package com.comarch.egeria.pp.OpisStanowiska.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_OPS_OB_ODPOW database table.
 * 
 */
@Entity
@Table(name="PPT_OPS_OB_ODPOW", schema="PPADM")
@NamedQuery(name="PptOpsObOdpow.findAll", query="SELECT w FROM PptOpsObOdpow w")
public class PptOpsObOdpow implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_PPT_OPS_OB_ODPOW",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OPS_OB_ODPOW", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_PPT_OPS_OB_ODPOW")
	@Column(name="OBOD_ID")
	private long obodId;

//	@Temporal(TemporalType.DATE)
//	@Column(name="OBOD_AUDYT_DM")
//	private Date obodAudytDm;
//
//	@Temporal(TemporalType.DATE)
//	@Column(name="OBOD_AUDYT_DT")
//	private Date obodAudytDt;
//
//	@Column(name="OBOD_AUDYT_KM")
//	private String obodAudytKm;
//
//	@Column(name="OBOD_AUDYT_LM")
//	private String obodAudytLm;
//
//	@Column(name="OBOD_AUDYT_UM")
//	private String obodAudytUm;
//
//	@Column(name="OBOD_AUDYT_UT")
//	private String obodAudytUt;

	@Column(name="OBOD_OPIS")
	private String obodOpis;

	@Column(name="OBOD_RODZAJ")
	private String obodRodzaj;

	//bi-directional many-to-one association to PptOpisStanowiska
	@ManyToOne
	@JoinColumn(name="OBOD_WOSP_ID")
	private PptOpisStanowiska pptOpisStanowiska;

	public PptOpsObOdpow() {
	}

	public long getObodId() {
		return this.obodId;
	}

	public void setObodId(long obodId) {
		this.obodId = obodId;
	}

//	public Date getObodAudytDm() {
//		return this.obodAudytDm;
//	}
//
//	public void setObodAudytDm(Date obodAudytDm) {
//		this.obodAudytDm = obodAudytDm;
//	}
//
//	public Date getObodAudytDt() {
//		return this.obodAudytDt;
//	}
//
//	public void setObodAudytDt(Date obodAudytDt) {
//		this.obodAudytDt = obodAudytDt;
//	}
//
//	public String getObodAudytKm() {
//		return this.obodAudytKm;
//	}
//
//	public void setObodAudytKm(String obodAudytKm) {
//		this.obodAudytKm = obodAudytKm;
//	}
//
//	public String getObodAudytLm() {
//		return this.obodAudytLm;
//	}
//
//	public void setObodAudytLm(String obodAudytLm) {
//		this.obodAudytLm = obodAudytLm;
//	}
//
//	public String getObodAudytUm() {
//		return this.obodAudytUm;
//	}
//
//	public void setObodAudytUm(String obodAudytUm) {
//		this.obodAudytUm = obodAudytUm;
//	}
//
//	public String getObodAudytUt() {
//		return this.obodAudytUt;
//	}
//
//	public void setObodAudytUt(String obodAudytUt) {
//		this.obodAudytUt = obodAudytUt;
//	}

	public String getObodOpis() {
		return this.obodOpis;
	}

	public void setObodOpis(String obodOpis) {
		this.obodOpis = obodOpis;
	}

	public String getObodRodzaj() {
		return this.obodRodzaj;
	}

	public void setObodRodzaj(String obodRodzaj) {
		this.obodRodzaj = obodRodzaj;
	}

	public PptOpisStanowiska getPptOpisStanowiska() {
		return this.pptOpisStanowiska;
	}

	public void setPptOpisStanowiska(PptOpisStanowiska pptOpisStanowiska) {
		this.pptOpisStanowiska = pptOpisStanowiska;
	}

}