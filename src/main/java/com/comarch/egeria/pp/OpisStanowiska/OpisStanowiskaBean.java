package com.comarch.egeria.pp.OpisStanowiska;

import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.OpisStanowiska.model.*;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.zalaczniki.ZalacznikiBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import net.sf.jasperreports.engine.JRException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.comarch.egeria.Utils.asString;
import static com.comarch.egeria.Utils.nz;

@ManagedBean
@Scope("view")
@Named
public class OpisStanowiskaBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LogManager.getLogger();

	@Inject
	SessionBean sessionBean;

	@Inject
	ObiegBean obieg;

	@Inject
	ZalacznikiBean zalaczniki;

//	@Inject
//	protected ReportGenerator ReportGenerator;

//	private String checkString = "";

	private static final String WYKSZTALCENIE = "WYKSZ";
	private static final String UPRAWNIENIE = "UPRAW";
	private static final String JEZYK = "JEZYK";
	private static final String INNE = "INNE";
	private static final String DOSWIADCZENIE = "DOSW";
	
	String symbolStn = "";

	private PptOpisStanowiska pptOpisStanowiska = null;
	private List<PptOpsGlZadania> zadaniaDominujaceList = new ArrayList<>();
	private List<PptOpsGlZadania> zadaniaPozostaleList = new ArrayList<>();
	
	private List<PptOpsNwWymogi> wyksztalcenieNiezbedne = new ArrayList<>();
	private List<PptOpsNwWymogi> wyksztalcenieDodatkowe = new ArrayList<>();
	private List<PptOpsNwWymogi> uprawnienieNiezbedne = new ArrayList<>();
	private List<PptOpsNwWymogi> uprawnienieDodatkowe = new ArrayList<>();
	private List<PptOpsNwWymogi> jezykNiezbedny = new ArrayList<>();
	private List<PptOpsNwWymogi> jezykDodatkowy = new ArrayList<>();
	private List<PptOpsNwWymogi> inneNiezbedne = new ArrayList<>();
	private List<PptOpsNwWymogi> inneDodatkowe = new ArrayList<>();
	private List<PptOpsNwWymogi> doswiadczenieNiezbedne = new ArrayList<>();
	private List<PptOpsNwWymogi> doswiadczenieDodatkowe = new ArrayList<>();

	@PostConstruct
	public void init() {
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		obieg.setKategoriaObiegu("WN_OPIS_STAN");
		if (id == null) {
			this.pptOpisStanowiska = new PptOpisStanowiska();
			this.pptOpisStanowiska.sqlBean = this;

			if (isCurrentTabSC()) {
				pptOpisStanowiska.setWospRodzajWn("WS");
				if (pptOpisStanowiska.getPptOpsWsDanes().isEmpty())
					pptOpisStanowiska.addPptOpsWsDane(new PptOpsWsDane());

				if (pptOpisStanowiska.getPptOpsObOdpows().isEmpty()) {
					PptOpsObOdpow odpow1 = new PptOpsObOdpow();
					PptOpsObOdpow odpow2 = new PptOpsObOdpow();
					PptOpsObOdpow odpow3 = new PptOpsObOdpow();

					odpow1.setObodRodzaj("Zarządzanie ludźmi");
					odpow2.setObodRodzaj("Zarządzanie finansami");
					odpow3.setObodRodzaj("Reprezentacja / Kontakty zewnętrzne");

					pptOpisStanowiska.addPptOpsObOdpow(odpow1);
					pptOpisStanowiska.addPptOpsObOdpow(odpow2);
					pptOpisStanowiska.addPptOpsObOdpow(odpow3);
				}

			}
			if (FacesContext.getCurrentInstance().getViewRoot().getViewId().endsWith("NowyOpis.xhtml")) {
				pptOpisStanowiska.setWospRodzajWn("NW");
				if (pptOpisStanowiska.getPptOpsNwDanes().isEmpty()) {
					pptOpisStanowiska.addPptOpsNwDane(new PptOpsNwDane());
				}
				
				dodajWyksztalcenieNiezbedne();
				dodajWyksztalcenieDodatkowe();
				dodajUprawnienieNiezbedne();
				dodajUprawnienieDodatkowe();
				dodajJezykNiezbedny();
				dodajJezykDodatkowy();
				dodajInneNiezbedne();
				dodajInneDodatkowe();
				dodajDoswiadczenieNiezbedne();
				dodajDoswiadczenieDodatkowe();

			}

			return;
		} else {

			this.pptOpisStanowiska = (PptOpisStanowiska) HibernateContext.get(PptOpisStanowiska.class, Long.parseLong("" + id));
			this.pptOpisStanowiska.sqlBean = this;

			if (this.getPptOpisStanowiska().getPptOpsObOdpows() != null)
				this.getPptOpisStanowiska().sortPptOpsObOdpows();
			for (PptOpsGlZadania ogz : pptOpisStanowiska.getPptOpsGlZadanias()) {
				if (ogz.getGlzadRodzaj().equals("ZD")) {
					zadaniaDominujaceList.add(ogz);
				} else {
					zadaniaPozostaleList.add(ogz);
				}
			}
			
			for (PptOpsNwWymogi wym : pptOpisStanowiska.getPptOpsNwWymogi()) {
				if (wym.getWospwmRodzaj().equals(WYKSZTALCENIE)) {
					if ("T".equals(wym.getWospwmFCzyNiezbedne())) {
						wyksztalcenieNiezbedne.add(wym);
					} else {
						wyksztalcenieDodatkowe.add(wym);
					}
				} else if (wym.getWospwmRodzaj().equals(UPRAWNIENIE)) {
					if ("T".equals(wym.getWospwmFCzyNiezbedne())) {
						uprawnienieNiezbedne.add(wym);
					} else {
						uprawnienieDodatkowe.add(wym);
					}
				} else if (wym.getWospwmRodzaj().equals(JEZYK)) {
					if ("T".equals(wym.getWospwmFCzyNiezbedne())) {
						jezykNiezbedny.add(wym);
					} else {
						jezykDodatkowy.add(wym);
					}
				} else if (wym.getWospwmRodzaj().equals(INNE)) {
					if ("T".equals(wym.getWospwmFCzyNiezbedne())) {
						inneNiezbedne.add(wym);
					} else {
						inneDodatkowe.add(wym);
					}
				} else if (wym.getWospwmRodzaj().equals(DOSWIADCZENIE)) {
					if ("T".equals(wym.getWospwmFCzyNiezbedne())) {
						doswiadczenieNiezbedne.add(wym);
					} else {
						doswiadczenieDodatkowe.add(wym);
					}
				}
			}
			
			ustawPustePolaWymagan();

			obieg.setIdEncji(this.getPptOpisStanowiska().getWospId());
			zalaczniki.setZalacznikFkId(this.getPptOpisStanowiska().getWospId());
//			checkString = getCheckString();
		}
	}
	
	private void ustawPustePolaWymagan() {
		if (wyksztalcenieNiezbedne.isEmpty()) {
			dodajWyksztalcenieNiezbedne();
		}
		if (wyksztalcenieDodatkowe.isEmpty()) {
			dodajWyksztalcenieDodatkowe();
		}
		if (uprawnienieNiezbedne.isEmpty()) {
			dodajUprawnienieNiezbedne();
		}
		if (uprawnienieDodatkowe.isEmpty()) {
			dodajUprawnienieDodatkowe();
		}
		if (jezykNiezbedny.isEmpty()) {
			dodajJezykNiezbedny();
		}
		if (jezykDodatkowy.isEmpty()) {
			dodajJezykDodatkowy();
		}
		if (inneNiezbedne.isEmpty()) {
			dodajInneNiezbedne();
		}
		if (inneDodatkowe.isEmpty()) {
			dodajInneDodatkowe();
		}
		if (doswiadczenieNiezbedne.isEmpty()) {
			dodajDoswiadczenieNiezbedne();
		}
		if (doswiadczenieDodatkowe.isEmpty()) {
			dodajDoswiadczenieDodatkowe();
		}
	}

	public boolean isCurrentTabSC() {
		return FacesContext.getCurrentInstance().getViewRoot().getViewId().endsWith("NowyOpisSC.xhtml");
	}

	public void zapisz() {
		this.pptOpisStanowiska.getPptOpsGlZadanias().clear();

		// Usuwanie pustych zadan dominujacych
		List<PptOpsGlZadania> toRemoveZadaniaD = new ArrayList<>();
		for (PptOpsGlZadania zadanie : this.getZadaniaDominujaceList()) {
			if (zadanie.getGlzadOpis() == null || zadanie.getGlzadOpis().isEmpty()) {
				toRemoveZadaniaD.add(zadanie);
			}
		}
		this.getZadaniaDominujaceList().removeAll(toRemoveZadaniaD);

		// Usuwanie pustych pozostalych zadan
		List<PptOpsGlZadania> toRemoveZadaniaP = new ArrayList<>();
		for (PptOpsGlZadania zadanie : this.getZadaniaPozostaleList()) {
			if (zadanie.getGlzadOpis() == null || zadanie.getGlzadOpis().isEmpty()) {
				toRemoveZadaniaP.add(zadanie);
			}
		}
		this.getZadaniaPozostaleList().removeAll(toRemoveZadaniaP);

		for (PptOpsGlZadania zadanie : this.getZadaniaDominujaceList()) {
			this.getPptOpisStanowiska().addPptOpsGlZadania(zadanie);
		}

		for (PptOpsGlZadania zadanie : this.getZadaniaPozostaleList()) {
			this.getPptOpisStanowiska().addPptOpsGlZadania(zadanie);
		}
		
		przygotujWymaganiaDoZapisu();

		if (!validateBeforeSave())
			return;

		try {
			this.pptOpisStanowiska = (PptOpisStanowiska) HibernateContext.save(this.pptOpisStanowiska, false);
			this.pptOpisStanowiska.sqlBean = this;
			User.info("Udany zapis opisu stanowiska pracy o id " + this.pptOpisStanowiska.getWospId());
			reloadPplOpisListaOpisowCurrentDataRow();
		} catch (Exception e) {
			User.alert("Błąd w zapisie opisu stanowiska pracy!", e);
			ustawPustePolaWymagan();
			return;
		}

		obieg.setIdEncji(this.pptOpisStanowiska.getWospId());
		zalaczniki.setZalacznikFkId(this.pptOpisStanowiska.getWospId());
//		checkString = getCheckString();
	}

	public void reloadPplOpisListaOpisowCurrentDataRow() {
		this.getLW("PPL_OPS_LISTA_OPISOW").reLoadDataRow(""+this.pptOpisStanowiska.getWospId());
	}

	private void przygotujWymaganiaDoZapisu() {
		this.pptOpisStanowiska.getPptOpsNwWymogi().clear();

		List<PptOpsNwWymogi> toRemoveWyksztalcenieN = new ArrayList<>();
		for (PptOpsNwWymogi wyksztalcenieN : this.getWyksztalcenieNiezbedne()) {
			if (wyksztalcenieN.getWospwmOpis01() == null || wyksztalcenieN.getWospwmOpis01().isEmpty()) {
				toRemoveWyksztalcenieN.add(wyksztalcenieN);
			}
		}
		this.getWyksztalcenieNiezbedne().removeAll(toRemoveWyksztalcenieN);
		
		List<PptOpsNwWymogi> toRemoveWyksztalcenieD = new ArrayList<>();
		for (PptOpsNwWymogi wyksztalcenieD : this.getWyksztalcenieDodatkowe()) {
			if (wyksztalcenieD.getWospwmOpis01() == null || wyksztalcenieD.getWospwmOpis01().isEmpty()) {
				toRemoveWyksztalcenieD.add(wyksztalcenieD);
			}
		}
		this.getWyksztalcenieDodatkowe().removeAll(toRemoveWyksztalcenieD);

		///
		List<PptOpsNwWymogi> toRemoveUprawnienieN = new ArrayList<>();
		for (PptOpsNwWymogi uprawnienieN : this.getUprawnienieNiezbedne()) {
			if (uprawnienieN.getWospwmOpis01() == null || uprawnienieN.getWospwmOpis01().isEmpty()) {
				toRemoveUprawnienieN.add(uprawnienieN);
			}
		}
		this.getUprawnienieNiezbedne().removeAll(toRemoveUprawnienieN);
		
		List<PptOpsNwWymogi> toRemoveUprawnienieD = new ArrayList<>();
		for (PptOpsNwWymogi uprawnienieD : this.getUprawnienieDodatkowe()) {
			if (uprawnienieD.getWospwmOpis01() == null || uprawnienieD.getWospwmOpis01().isEmpty()) {
				toRemoveUprawnienieD.add(uprawnienieD);
			}
		}
		this.getUprawnienieDodatkowe().removeAll(toRemoveUprawnienieD);

		///
		List<PptOpsNwWymogi> toRemoveJezykN = new ArrayList<>();
		for (PptOpsNwWymogi jezykN : this.getJezykNiezbedny()) {
			if (jezykN.getWospwmOpis01() == null || jezykN.getWospwmOpis01().isEmpty()) {
				toRemoveJezykN.add(jezykN);
			}
		}
		this.getJezykNiezbedny().removeAll(toRemoveJezykN);
		
		List<PptOpsNwWymogi> toRemoveJezykD = new ArrayList<>();
		for (PptOpsNwWymogi jezykD : this.getJezykDodatkowy()) {
			if (jezykD.getWospwmOpis01() == null || jezykD.getWospwmOpis01().isEmpty()) {
				toRemoveJezykD.add(jezykD);
			}
		}
		this.getJezykDodatkowy().removeAll(toRemoveJezykD);

		///
		List<PptOpsNwWymogi> toRemoveInneN = new ArrayList<>();
		for (PptOpsNwWymogi inneN : this.getInneNiezbedne()) {
			if ((inneN.getWospwmOpis01() == null || inneN.getWospwmOpis01().isEmpty()) 
					&& (inneN.getWospwmOpis02() == null || inneN.getWospwmOpis02().isEmpty()) 
					&& (inneN.getWospwmOpis03() == null || inneN.getWospwmOpis03().isEmpty())) {
				toRemoveInneN.add(inneN);
			}
		}
		this.getInneNiezbedne().removeAll(toRemoveInneN);
		
		List<PptOpsNwWymogi> toRemoveInneD = new ArrayList<>();
		for (PptOpsNwWymogi inneD : this.getInneDodatkowe()) {
			if ((inneD.getWospwmOpis01() == null || inneD.getWospwmOpis01().isEmpty()) 
					&& (inneD.getWospwmOpis02() == null || inneD.getWospwmOpis02().isEmpty()) 
					&& (inneD.getWospwmOpis03() == null || inneD.getWospwmOpis03().isEmpty())) {
				toRemoveInneD.add(inneD);
			}
		}
		this.getInneDodatkowe().removeAll(toRemoveInneD);

		///
		List<PptOpsNwWymogi> toRemoveDoswiadczenieN = new ArrayList<>();
		for (PptOpsNwWymogi doswiadczenieN : this.getDoswiadczenieNiezbedne()) {
			if (doswiadczenieN.getWospwmOpis01() == null || doswiadczenieN.getWospwmOpis01().isEmpty()) {
				toRemoveDoswiadczenieN.add(doswiadczenieN);
			}
		}
		this.getDoswiadczenieNiezbedne().removeAll(toRemoveDoswiadczenieN);
		
		List<PptOpsNwWymogi> toRemoveDoswiadczenieD = new ArrayList<>();
		for (PptOpsNwWymogi doswiadczenieD : this.getDoswiadczenieDodatkowe()) {
			if (doswiadczenieD.getWospwmOpis01() == null || doswiadczenieD.getWospwmOpis01().isEmpty()) {
				toRemoveDoswiadczenieD.add(doswiadczenieD);
			}
		}
		this.getDoswiadczenieDodatkowe().removeAll(toRemoveDoswiadczenieD);
		
		
		for (PptOpsNwWymogi wymaganie : this.getWyksztalcenieNiezbedne()) {
			this.getPptOpisStanowiska().addPptOpsNwWymogi(wymaganie);
		}
		
		for (PptOpsNwWymogi wymaganie : this.getWyksztalcenieDodatkowe()) {
			this.getPptOpisStanowiska().addPptOpsNwWymogi(wymaganie);
		}
		
		for (PptOpsNwWymogi wymaganie : this.getUprawnienieNiezbedne()) {
			this.getPptOpisStanowiska().addPptOpsNwWymogi(wymaganie);
		}
		
		for (PptOpsNwWymogi wymaganie : this.getUprawnienieDodatkowe()) {
			this.getPptOpisStanowiska().addPptOpsNwWymogi(wymaganie);
		}
		
		for (PptOpsNwWymogi wymaganie : this.getJezykNiezbedny()) {
			this.getPptOpisStanowiska().addPptOpsNwWymogi(wymaganie);
		}
		
		for (PptOpsNwWymogi wymaganie : this.getJezykDodatkowy()) {
			this.getPptOpisStanowiska().addPptOpsNwWymogi(wymaganie);
		}
		
		for (PptOpsNwWymogi wymaganie : this.getInneNiezbedne()) {
			this.getPptOpisStanowiska().addPptOpsNwWymogi(wymaganie);
		}
		
		for (PptOpsNwWymogi wymaganie : this.getInneDodatkowe()) {
			this.getPptOpisStanowiska().addPptOpsNwWymogi(wymaganie);
		}
		
		for (PptOpsNwWymogi wymaganie : this.getDoswiadczenieNiezbedne()) {
			this.getPptOpisStanowiska().addPptOpsNwWymogi(wymaganie);
		}
		
		for (PptOpsNwWymogi wymaganie : this.getDoswiadczenieDodatkowe()) {
			this.getPptOpisStanowiska().addPptOpsNwWymogi(wymaganie);
		}
	}

	
	public void usun() throws IOException {
		try (Connection conn = JdbcUtils.getConnection(false)) {
			obieg.usunDaneObiegu(); // reloadPage
		} catch (SQLException e) {
			log.error(e.getMessage());
			User.alert("Nieudane usuniecie danych obiegu: " + e.getMessage());
			return;
		}

		try {
			ZalacznikiBean.deleteAll("ZAL_WOSP_ID", pptOpisStanowiska.getWospId());
		} catch (Exception e) {
			log.error(e.getMessage());
			User.alert("Błąd przy próbie usunięcia załączników: " + e.getMessage());
			return;
		}

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			pptOpisStanowiska = (PptOpisStanowiska) s.merge(pptOpisStanowiska);
			this.pptOpisStanowiska.sqlBean = this;
			s.delete(pptOpisStanowiska);
			s.beginTransaction().commit();
			User.info("Usunięto opis stanowiska pracy");
		} catch (Exception e) {
			User.alert("Błąd w usuwaniu opisu stanowiska pracy!");
			log.error(e.getMessage());
			return;
		}

		FacesContext.getCurrentInstance().getExternalContext().redirect("ListaOpisow");

	}

	public void raport(String nazwa) {
		raport(nazwa, "pdf");
	}

	public void raport(String nazwa, String type) {

		if (pptOpisStanowiska != null && pptOpisStanowiska.getWospId() > 0) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("WOSP_ID", pptOpisStanowiska.getWospId());
			try {
				switch (type) {
				case "docx":
					ReportGenerator.displayReportAsDOCX(params, nazwa);
					break;

				case "rtf": 
					ReportGenerator.displayReportAsRTF(params, nazwa);
					break;
				
				default:
				case "pdf":
					ReportGenerator.displayReportAsPDF(params, nazwa);
					break;
					
				}

			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
	}
	
	
	
	public void onCzIIIZatwierdzajacySelected(){
		this.pptOpisStanowiska.setWospCzIIIZatwStanowisko(null);
		
		SqlDataSelectionsHandler lw = this.getLW("PPL_KAD_PRACOWNICY");
		DataRow r = lw.getCurrentRow();
		if (lw==null || r == null)
			return;
		
		String stn = (String) r.get("STN_NAZWA");
		this.pptOpisStanowiska.setWospCzIIIZatwStanowisko(stn);
	}
	

	public void onCzIIIKierujacySelected(){
		this.pptOpisStanowiska.setWospCzIIIKierStanowisko(null);
		
		SqlDataSelectionsHandler lw = this.getLW("PPL_KAD_PRACOWNICY");
		DataRow r = lw.getCurrentRow();
		if (lw==null || r == null)
			return;
		
		String stn = (String) r.get("STN_NAZWA");
		this.pptOpisStanowiska.setWospCzIIIKierStanowisko(stn);
	}
	
	
	
	private boolean validateBeforeSave() {
		ArrayList<String> inval = new ArrayList<String>();

//		String currentCheckString = this.getCheckString();

//		if (this.checkString.equals(currentCheckString))
//			inval.add("Brak zmian do zapisania.");

		if (pptOpisStanowiska.getWospObId() == null || pptOpisStanowiska.getWospObId() == 0L) {
			inval.add("Jednostka organizacyjna nie może być pusta.");
		}

		if (nz(pptOpisStanowiska.getWospStjoId()) == 0L) {
			inval.add("Stanowisko nie może być puste.");
		}

		
		
		
		if (this.pptOpisStanowiska.getWospStoId()!=null){
			
			inval.add("Aktualny opis zapisano już do w systemie EGR/ZP. Jego dalsza modyfikacja jest możliwa w systemie EGR/ZP. ");
		
		} else {

			SqlDataSelectionsHandler lw = this.getLW("PPL_OPS_KOLIZJE_ZPT", this.pptOpisStanowiska.getWospStjoId(), this.pptOpisStanowiska.getWospDataOd(), this.pptOpisStanowiska.getWospDataDo() );
			for (DataRow r : lw.getData()) {
				java.sql.Timestamp dtOd = (java.sql.Timestamp) r.get("STO_DATA_OD");
				java.sql.Timestamp dtDo = (java.sql.Timestamp) r.get("STO_DATA_DO");
				inval.add("W systemie EGR/ZP istnieje analogiczny '" + r.get("STO_SYMBOL")  + "', rozpoczęty opis stanowiska na wskazny okres "
						+ "( "+ asString( dtOd ) + " - " + asString ( dtDo ) + " ). "
						+ "Przed dodaniem kolejnego opisu zakończ poprzedni.");
			}
			
			if (lw.size()==0){	
				SqlDataSelectionsHandler lw1 = this.getLW("PPL_OPS_KOLIZJE_PP",  this.pptOpisStanowiska.getWospId(), this.pptOpisStanowiska.getWospStjoId(), this.pptOpisStanowiska.getWospDataOd(), this.pptOpisStanowiska.getWospDataDo() );
				for (DataRow r : lw1.getData()) {
					java.sql.Timestamp dtOd = (java.sql.Timestamp) r.get("WOSP_DATA_OD");
					java.sql.Timestamp dtDo = (java.sql.Timestamp) r.get("WOSP_DATA_DO");
					inval.add("W systemie PP istnieje analogiczny '" + r.get("WOSP_SYMBOL")  + "', rozpoczęty opis stanowiska na wskazny okres "
							+ "( "+ asString( dtOd ) + " - " + asString ( dtDo ) + " ). "
							+ "Przed dodaniem kolejnego zakończ poprzedni.");
				}
			}
			
		}
		
		
//		Integer iloscOpisow = 0;
//		try (Connection conn = JdbcDAO.getConnection()) {
//			
//			iloscOpisow = JdbcDAO.sqlFnInteger(conn, "PPADM.PPP_UTILITIES.waliduj_opis_stanowiska",
//					this.pptOpisStanowiska.getWospStjoId(), asSqlDate(this.pptOpisStanowiska.getWospDataOd()),
//					asSqlDate(this.pptOpisStanowiska.getWospDataDo()));
//
//			if (iloscOpisow > 0)
//				inval.add("W Poratlu Pracowniczym lub Egerii istnieje co najmniej jeden, analogiczny (" +  symbolStn + "), rozpoczęty opis stanowiska na wskazny okres. Przed dodaniem kolejnego zakończ poprzedni.");
//				//	inval.add("Przedział czasu między datą początkową i końcową opisu stanowiska " + symbolStn + " nakłada się z innym przedziałem czasu opisu tego stanowiska.");
//			
//		} catch (Exception e) {
//			inval.add("Błąd walidacji ważności opisu stanowiska");
//			log.error(e.getMessage(), e);
//		}
		
		
		
		
		if (!inval.isEmpty())
			User.alert(	String.join( "<br/><br/>", inval) );
		
//		for (String msg : inval) {
//			User.alert(msg);
//		}

		return inval.isEmpty();
	}

//	public String getCheckString() {
//		String ret = "";
//		if (("" + pptOpisStanowiska.getWospRodzajWn()).equals("WS")) {
//			ret = AppBean.concatValues(pptOpisStanowiska.getWospRodzajWn(), pptOpisStanowiska.getWospObId(),
//					pptOpisStanowiska.getWospWydzialId(), pptOpisStanowiska.getWospStnId(),
//					pptOpisStanowiska.getWospStnIdNadBp(), pptOpisStanowiska.getWospStnIdNadPo(),
//					pptOpisStanowiska.getWospCelIstnienia(), pptOpisStanowiska.getWospDataOd(),
//					pptOpisStanowiska.getWospDataDo(), pptOpisStanowiska.getWospPrcId(),
//					pptOpisStanowiska.getWospEtat(), pptOpisStanowiska.getWospFZastepstwo(),
//					pptOpisStanowiska.getWospFUkonczony(), pptOpisStanowiska.getWospSymbol(),
//					getWsDane().getWospwWkidAtr01(), getWsDane().getWospwWkidAtr02(), getWsDane().getWospwWkidAtr03(),
//					getWsDane().getWospwWkidAtr04(), getWsDane().getWospwWkidAtr05(), getWsDane().getWospwWkidAtr06(),
//					getWsDane().getWospwWkidAtr07(), getWsDane().getWospwWkidAtr08(), getWsDane().getWospwWkidAtr09(),
//					getWsDane().getWospwWkidAtr10(), getWsDane().getWospwWkidAtr11(), getWsDane().getWospwWkidAtr12(),
//					getWsDane().getWospwFSfAtr01(), getWsDane().getWospwFSfAtr02(), getWsDane().getWospwFSfAtr03(),
//					getWsDane().getWospwFSfAtr04(), getWsDane().getWospwFSfAtr05(), getWsDane().getWospwFSfAtr06(),
//					getWsDane().getWospwFSfAtr07(), getWsDane().getWospwFSfAtr08());
//			for (PptOpsObOdpow odpow : pptOpisStanowiska.getPptOpsObOdpows()) {
//				ret += odpow.getObodRodzaj() + "-" + odpow.getObodOpis() + "; ";
//			}
//
//		}
//
//		if (("" + pptOpisStanowiska.getWospRodzajWn()).equals("NW")) {
//			ret = AppBean.concatValues(pptOpisStanowiska.getWospRodzajWn(), pptOpisStanowiska.getWospObId(),
//					pptOpisStanowiska.getWospWydzialId(), pptOpisStanowiska.getWospStnId(),
//					pptOpisStanowiska.getWospStnIdNadBp(), pptOpisStanowiska.getWospStnIdNadPo(),
//					pptOpisStanowiska.getWospCelIstnienia(), pptOpisStanowiska.getWospDataOd(),
//					pptOpisStanowiska.getWospDataDo(), pptOpisStanowiska.getWospPrcId(),
//					pptOpisStanowiska.getWospEtat(), pptOpisStanowiska.getWospFZastepstwo(),
//					pptOpisStanowiska.getWospFUkonczony(), pptOpisStanowiska.getWospSymbol(),
//					getNwDane().getWospnCzszInne(), getNwDane().getWospnCzszPrzyklad(),
//					getNwDane().getWospnFCzszAtr01(), getNwDane().getWospnFCzszAtr01(),
//					getNwDane().getWospnFCzszAtr02(), getNwDane().getWospnFCzszAtr03(),
//					getNwDane().getWospnFCzszAtr04(), getNwDane().getWospnFCzszAtr05(),
//					getNwDane().getWospnFCzszAtr06(), getNwDane().getWospnFCzszAtr07(),
//					getNwDane().getWospnFCzszAtr08(), getNwDane().getWospnFCzszAtr09(),
//					getNwDane().getWospnFCzszAtr10(), getNwDane().getWospnFHcpAtr01(), getNwDane().getWospnFKzAtr01(),
//					getNwDane().getWospnFKzAtr02(), getNwDane().getWospnFKzAtr03(), getNwDane().getWospnFKzAtr04(),
//					getNwDane().getWospnFLpAtr01(), getNwDane().getWospnFUipAtr01(), getNwDane().getWospnFUipAtr02(),
//					getNwDane().getWospnFUipAtr03(), getNwDane().getWospnFUipAtr04(), getNwDane().getWospnFUipAtr05(),
//					getNwDane().getWospnFUipAtr06(), getNwDane().getWospnKzAtr01Opis(),
//					getNwDane().getWospnKzAtr02Opis(), getNwDane().getWospnKzAtr03Opis(),
//					getNwDane().getWospnKzAtr04Opis(), getNwDane().getWospnNsiiOpis(),
//					getNwDane().getWospnNsiiPrzyklad(), getNwDane().getWospnUipInne(), getNwDane().getWospnWkidAtr01(),
//					getNwDane().getWospnWkidAtr02(), getNwDane().getWospnWkidAtr03(), getNwDane().getWospnWkidAtr04(),
//					getNwDane().getWospnWkidAtr05(), getNwDane().getWospnWkidAtr06(), getNwDane().getWospnWkidAtr07(),
//					getNwDane().getWospnWkidAtr08(), getNwDane().getWospnWkidAtr09(), getNwDane().getWospnWkidAtr10(),
//					getNwDane().getWospnWkidAtr11(), getNwDane().getWospnWkidAtr12(), getNwDane().getWospnZikOpis(),
//					getNwDane().getWospnZikPrzyklad());
//
//			for (PptOpsGlZadania zadanie : pptOpisStanowiska.getPptOpsGlZadanias()) {
//				ret += zadanie.getGlzadOpis() + "-" + zadanie.getGlzadRodzaj() + "; ";
//			}
//		}
//
//		return ret;
//	}

	public void dodajOdpowiedzialnoscStn() {
		if (pptOpisStanowiska.getPptOpsObOdpows().size() >= 11) {
			User.alert("Można dodać maksymalnie 8 dodatkowcyh obszarów odpowiedzialności!");
			return;
		}
		PptOpsObOdpow odpow = new PptOpsObOdpow();
		pptOpisStanowiska.addPptOpsObOdpow(odpow);
	}

	public void dodajNoweZadanieDominujace() {
		/** PT 239201 - sprawdzanie usunięte z powodu sprzecznych wymagań */
//		if (this.getZadaniaDominujaceList().size() > 10) {
//			User.alert("Można dodać maksymalnie 10 zadań dominujących.");
//			return;
//		}
		
		PptOpsGlZadania zadanie = new PptOpsGlZadania();
		zadanie.setGlzadRodzaj("ZD");
		this.zadaniaDominujaceList.add(zadanie);
		// pptOpisStanowiska.addPptOpsGlZadania(zadanie);
	}

	public void dodajNoweZadaniePozostale() {
		if (this.getZadaniaPozostaleList().size() >= 5) {
			User.alert("Można dodać maksymalnie 5 zadań pozostałych.");
			return;
		}
		PptOpsGlZadania zadanie = new PptOpsGlZadania();
		zadanie.setGlzadRodzaj("PZ");
		this.zadaniaPozostaleList.add(zadanie);
		// pptOpisStanowiska.addPptOpsGlZadania(zadanie);
	}

	public void setSymbolOpisu() {
		symbolStn = (String) getLW("PP_STAN_W_JEDN").getCurrentRow().get("PP_STN_SYMBOL");
		int lp = 0;

		String iloscPP_S = getLW("PP_STAN_W_JEDN").getCurrentRow().get("ilosc_opisow_PP").toString();
		int iloscPP = Integer.parseInt(iloscPP_S);
		String iloscEg_S = getLW("PP_STAN_W_JEDN").getCurrentRow().get("ilosc_opisow_ZP").toString();
		int iloscEg = Integer.parseInt(iloscEg_S);

		lp = iloscEg + iloscPP;

		String symbol = symbolStn.concat("/").concat(String.valueOf(lp + 1));
		pptOpisStanowiska.setWospSymbol(symbol);
	}

	public void usunWybranegoPracownika() {
		if (this.pptOpisStanowiska != null) {
			this.pptOpisStanowiska.setWospPrcId(null);
		}
	}

	public PptOpisStanowiska getPptOpisStanowiska() {
		return pptOpisStanowiska;
	}

	public void setPptOpisStanowiska(PptOpisStanowiska pptOpisStanowiska) {
		this.pptOpisStanowiska = pptOpisStanowiska;
		this.pptOpisStanowiska.sqlBean = this;
	}

	public PptOpsWsDane getWsDane() {
		return pptOpisStanowiska.getPptOpsWsDanes().get(0);
	}

	public PptOpsNwDane getNwDane() {
		return pptOpisStanowiska.getPptOpsNwDanes().get(0);
	}

	public List<PptOpsGlZadania> getZadaniaDominujaceList() {
		return zadaniaDominujaceList;
	}

	public void setZadaniaDominujaceList(List<PptOpsGlZadania> zadaniaDominujaceList) {
		this.zadaniaDominujaceList = zadaniaDominujaceList;
	}

	public List<PptOpsGlZadania> getZadaniaPozostaleList() {
		return zadaniaPozostaleList;
	}

	public void setZadaniaPozostaleList(List<PptOpsGlZadania> zadaniaPozostaleList) {
		this.zadaniaPozostaleList = zadaniaPozostaleList;
	}

	public List<PptOpsNwWymogi> getWyksztalcenieNiezbedne() {
		return wyksztalcenieNiezbedne;
	}

	public void setWyksztalcenieNiezbedne(List<PptOpsNwWymogi> wyksztalcenieNiezbedne) {
		this.wyksztalcenieNiezbedne = wyksztalcenieNiezbedne;
	}
	
	public void dodajWyksztalcenieNiezbedne() {
		PptOpsNwWymogi wyksztalcenie = new PptOpsNwWymogi();
		wyksztalcenie.setWospwmRodzaj(WYKSZTALCENIE);
		wyksztalcenie.setWospwmFCzyNiezbedne("T");
		this.wyksztalcenieNiezbedne.add(wyksztalcenie);
	}
	
	public void usunWyksztalcenieNiezbedne(PptOpsNwWymogi wyksztalcenie) {
		this.wyksztalcenieNiezbedne.remove(wyksztalcenie);
		
		if (this.wyksztalcenieNiezbedne.isEmpty())
			dodajWyksztalcenieNiezbedne();
	}

	public List<PptOpsNwWymogi> getWyksztalcenieDodatkowe() {
		return wyksztalcenieDodatkowe;
	}

	public void setWyksztalcenieDodatkowe(List<PptOpsNwWymogi> wyksztalcenieDodatkowe) {
		this.wyksztalcenieDodatkowe = wyksztalcenieDodatkowe;
	}
	
	public void dodajWyksztalcenieDodatkowe() {
		PptOpsNwWymogi wyksztalcenie = new PptOpsNwWymogi();
		wyksztalcenie.setWospwmRodzaj(WYKSZTALCENIE);
		wyksztalcenie.setWospwmFCzyNiezbedne("N");
		this.wyksztalcenieDodatkowe.add(wyksztalcenie);
	}
	
	public void usunWyksztalcenieDodatkowe(PptOpsNwWymogi wyksztalcenie) {
		this.wyksztalcenieDodatkowe.remove(wyksztalcenie);
		
		if (this.wyksztalcenieDodatkowe.isEmpty())
			dodajWyksztalcenieDodatkowe();
	}

	public List<PptOpsNwWymogi> getUprawnienieNiezbedne() {
		return uprawnienieNiezbedne;
	}

	public void setUprawnienieNiezbedne(List<PptOpsNwWymogi> uprawnienieNiezbedne) {
		this.uprawnienieNiezbedne = uprawnienieNiezbedne;
	}
	
	public void dodajUprawnienieNiezbedne() {
		PptOpsNwWymogi uprawnienie = new PptOpsNwWymogi();
		uprawnienie.setWospwmRodzaj(UPRAWNIENIE);
		uprawnienie.setWospwmFCzyNiezbedne("T");
		this.uprawnienieNiezbedne.add(uprawnienie);
	}
	
	public void usunUprawnienieNiezbedne(PptOpsNwWymogi uprawnienie) {
		this.uprawnienieNiezbedne.remove(uprawnienie);
		
		if (this.uprawnienieNiezbedne.isEmpty())
			dodajUprawnienieNiezbedne();
	}

	public List<PptOpsNwWymogi> getUprawnienieDodatkowe() {
		return uprawnienieDodatkowe;
	}

	public void setUprawnienieDodatkowe(List<PptOpsNwWymogi> uprawnienieDodatkowe) {
		this.uprawnienieDodatkowe = uprawnienieDodatkowe;
	}
	
	public void dodajUprawnienieDodatkowe() {
		PptOpsNwWymogi uprawnienie = new PptOpsNwWymogi();
		uprawnienie.setWospwmRodzaj(UPRAWNIENIE);
		uprawnienie.setWospwmFCzyNiezbedne("N");
		this.uprawnienieDodatkowe.add(uprawnienie);
	}
	
	public void usunUprawnienieDodatkowe(PptOpsNwWymogi uprawnienie) {
		this.uprawnienieDodatkowe.remove(uprawnienie);
		
		if (this.uprawnienieDodatkowe.isEmpty())
			dodajUprawnienieDodatkowe();
	}

	public List<PptOpsNwWymogi> getJezykNiezbedny() {
		return jezykNiezbedny;
	}

	public void setJezykNiezbedny(List<PptOpsNwWymogi> jezykNiezbedny) {
		this.jezykNiezbedny = jezykNiezbedny;
	}
	
	public void dodajJezykNiezbedny() {
		PptOpsNwWymogi jezyk = new PptOpsNwWymogi();
		jezyk.setWospwmRodzaj(JEZYK);
		jezyk.setWospwmFCzyNiezbedne("T");
		this.jezykNiezbedny.add(jezyk);
	}
	
	public void usunJezykNiezbedny(PptOpsNwWymogi jezyk) {
		this.jezykNiezbedny.remove(jezyk);
		
		if (this.jezykNiezbedny.isEmpty())
			dodajJezykNiezbedny();
	}

	public List<PptOpsNwWymogi> getJezykDodatkowy() {
		return jezykDodatkowy;
	}

	public void setJezykDodatkowy(List<PptOpsNwWymogi> jezykDodatkowy) {
		this.jezykDodatkowy = jezykDodatkowy;
	}
	
	public void dodajJezykDodatkowy() {
		PptOpsNwWymogi jezyk = new PptOpsNwWymogi();
		jezyk.setWospwmRodzaj(JEZYK);
		jezyk.setWospwmFCzyNiezbedne("N");
		this.jezykDodatkowy.add(jezyk);
	}
	
	public void usunJezykDodatkowy(PptOpsNwWymogi jezyk) {
		this.jezykDodatkowy.remove(jezyk);
		
		if (this.jezykDodatkowy.isEmpty())
			dodajJezykDodatkowy();
	}

	public List<PptOpsNwWymogi> getInneNiezbedne() {
		return inneNiezbedne;
	}

	public void setInneNiezbedne(List<PptOpsNwWymogi> inneNiezbedne) {
		this.inneNiezbedne = inneNiezbedne;
	}
	
	public void dodajInneNiezbedne() {
		PptOpsNwWymogi inne = new PptOpsNwWymogi();
		inne.setWospwmRodzaj(INNE);
		inne.setWospwmFCzyNiezbedne("T");
		this.inneNiezbedne.add(inne);
	}
	
	public void usunInneNiezbedne(PptOpsNwWymogi inne) {
		this.inneNiezbedne.remove(inne);
		
		if (this.inneNiezbedne.isEmpty())
			dodajInneNiezbedne();
	}

	public List<PptOpsNwWymogi> getInneDodatkowe() {
		return inneDodatkowe;
	}

	public void setInneDodatkowe(List<PptOpsNwWymogi> inneDodatkowe) {
		this.inneDodatkowe = inneDodatkowe;
	}
	
	public void dodajInneDodatkowe() {
		PptOpsNwWymogi inne = new PptOpsNwWymogi();
		inne.setWospwmRodzaj(INNE);
		inne.setWospwmFCzyNiezbedne("N");
		this.inneDodatkowe.add(inne);
	}
	
	public void usunInneDodatkowe(PptOpsNwWymogi inne) {
		this.inneDodatkowe.remove(inne);
		
		if (this.inneDodatkowe.isEmpty())
			dodajInneDodatkowe();
	}

	public List<PptOpsNwWymogi> getDoswiadczenieNiezbedne() {
		return doswiadczenieNiezbedne;
	}

	public void setDoswiadczenieNiezbedne(List<PptOpsNwWymogi> doswiadczenieNiezbedne) {
		this.doswiadczenieNiezbedne = doswiadczenieNiezbedne;
	}
	
	public void dodajDoswiadczenieNiezbedne() {
		PptOpsNwWymogi doswiadczenie = new PptOpsNwWymogi();
		doswiadczenie.setWospwmRodzaj(DOSWIADCZENIE);
		doswiadczenie.setWospwmFCzyNiezbedne("T");
		this.doswiadczenieNiezbedne.add(doswiadczenie);
	}
	
	public void usunDoswiadczenieNiezbedne(PptOpsNwWymogi doswiadczenie) {
		this.doswiadczenieNiezbedne.remove(doswiadczenie);
		
		if (this.doswiadczenieNiezbedne.isEmpty())
			dodajDoswiadczenieNiezbedne();
	}

	public List<PptOpsNwWymogi> getDoswiadczenieDodatkowe() {
		return doswiadczenieDodatkowe;
	}

	public void setDoswiadczenieDodatkowe(List<PptOpsNwWymogi> doswiadczenieDodatkowe) {
		this.doswiadczenieDodatkowe = doswiadczenieDodatkowe;
	}
	
	public void dodajDoswiadczenieDodatkowe() {
		PptOpsNwWymogi doswiadczenie = new PptOpsNwWymogi();
		doswiadczenie.setWospwmRodzaj(DOSWIADCZENIE);
		doswiadczenie.setWospwmFCzyNiezbedne("N");
		this.doswiadczenieDodatkowe.add(doswiadczenie);
	}
	
	public void usunDoswiadczenieDodatkowe(PptOpsNwWymogi doswiadczenie) {
		this.doswiadczenieDodatkowe.remove(doswiadczenie);
		
		if (this.doswiadczenieDodatkowe.isEmpty())
			dodajDoswiadczenieDodatkowe();
	}

}
