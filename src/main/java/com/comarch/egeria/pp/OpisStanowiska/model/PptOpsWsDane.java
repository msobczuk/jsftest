package com.comarch.egeria.pp.OpisStanowiska.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;


/**
 * The persistent class for the PPT_OPS_WS_DANE database table.
 * 
 */
@Entity
@Table(name="PPT_OPS_WS_DANE", schema="PPADM")
@NamedQuery(name="PptOpsWsDane.findAll", query="SELECT w FROM PptOpsWsDane w")
public class PptOpsWsDane implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_PPT_OPS_WS_DANE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OPS_WS_DANE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_PPT_OPS_WS_DANE")
	@Column(name="WOSPW_ID")
	private long wospwId;

//	@Temporal(TemporalType.DATE)
//	@Column(name="WOSPW_AUDYT_DM")
//	private Date wospwAudytDm;
//
//	@Temporal(TemporalType.DATE)
//	@Column(name="WOSPW_AUDYT_DT")
//	private Date wospwAudytDt;
//
//	@Column(name="WOSPW_AUDYT_KM")
//	private String wospwAudytKm;
//
//	@Column(name="WOSPW_AUDYT_LM")
//	private String wospwAudytLm;
//
//	@Column(name="WOSPW_AUDYT_UM")
//	private String wospwAudytUm;
//
//	@Column(name="WOSPW_AUDYT_UT")
//	private String wospwAudytUt;

	@Column(name="WOSPW_F_SF_ATR_01")
	private String wospwFSfAtr01;

	@Column(name="WOSPW_F_SF_ATR_02")
	private String wospwFSfAtr02;

	@Column(name="WOSPW_F_SF_ATR_03")
	private String wospwFSfAtr03;

	@Column(name="WOSPW_F_SF_ATR_04")
	private String wospwFSfAtr04;

	@Column(name="WOSPW_F_SF_ATR_05")
	private String wospwFSfAtr05;

	@Column(name="WOSPW_F_SF_ATR_06")
	private String wospwFSfAtr06;

	@Column(name="WOSPW_F_SF_ATR_07")
	private String wospwFSfAtr07;

	@Column(name="WOSPW_F_SF_ATR_08")
	private String wospwFSfAtr08;

	@Column(name="WOSPW_WKID_ATR_01")
	private String wospwWkidAtr01;

	@Column(name="WOSPW_WKID_ATR_02")
	private String wospwWkidAtr02;

	@Column(name="WOSPW_WKID_ATR_03")
	private String wospwWkidAtr03;

	@Column(name="WOSPW_WKID_ATR_04")
	private String wospwWkidAtr04;

	@Column(name="WOSPW_WKID_ATR_05")
	private String wospwWkidAtr05;

	@Column(name="WOSPW_WKID_ATR_06")
	private String wospwWkidAtr06;

	@Column(name="WOSPW_WKID_ATR_07")
	private String wospwWkidAtr07;

	@Column(name="WOSPW_WKID_ATR_08")
	private String wospwWkidAtr08;

	@Column(name="WOSPW_WKID_ATR_09")
	private String wospwWkidAtr09;

	@Column(name="WOSPW_WKID_ATR_10")
	private String wospwWkidAtr10;

	@Column(name="WOSPW_WKID_ATR_11")
	private String wospwWkidAtr11;

	@Column(name="WOSPW_WKID_ATR_12")
	private String wospwWkidAtr12;

	@ManyToOne
	@JoinColumn(name="WOSPW_WOSP_ID")
	private PptOpisStanowiska pptOpisStanowiska;
	
//	//bi-directional one-to-one association to PptOpisStanowiska
//	@OneToOne(mappedBy="pptOpsWsDane")
//	private PptOpisStanowiska pptOpisStanowiska;

	public PptOpsWsDane() {
	}

	public long getWospwId() {
		return this.wospwId;
	}

	public void setWospwId(long wospwId) {
		this.wospwId = wospwId;
	}

//	public Date getWospwAudytDm() {
//		return this.wospwAudytDm;
//	}
//
//	public void setWospwAudytDm(Date wospwAudytDm) {
//		this.wospwAudytDm = wospwAudytDm;
//	}
//
//	public Date getWospwAudytDt() {
//		return this.wospwAudytDt;
//	}
//
//	public void setWospwAudytDt(Date wospwAudytDt) {
//		this.wospwAudytDt = wospwAudytDt;
//	}
//
//	public String getWospwAudytKm() {
//		return this.wospwAudytKm;
//	}
//
//	public void setWospwAudytKm(String wospwAudytKm) {
//		this.wospwAudytKm = wospwAudytKm;
//	}
//
//	public String getWospwAudytLm() {
//		return this.wospwAudytLm;
//	}
//
//	public void setWospwAudytLm(String wospwAudytLm) {
//		this.wospwAudytLm = wospwAudytLm;
//	}
//
//	public String getWospwAudytUm() {
//		return this.wospwAudytUm;
//	}
//
//	public void setWospwAudytUm(String wospwAudytUm) {
//		this.wospwAudytUm = wospwAudytUm;
//	}
//
//	public String getWospwAudytUt() {
//		return this.wospwAudytUt;
//	}
//
//	public void setWospwAudytUt(String wospwAudytUt) {
//		this.wospwAudytUt = wospwAudytUt;
//	}

	public String getWospwFSfAtr01() {
		return this.wospwFSfAtr01;
	}

	public void setWospwFSfAtr01(String wospwFSfAtr01) {
		this.wospwFSfAtr01 = wospwFSfAtr01;
	}

	public String getWospwFSfAtr02() {
		return this.wospwFSfAtr02;
	}

	public void setWospwFSfAtr02(String wospwFSfAtr02) {
		this.wospwFSfAtr02 = wospwFSfAtr02;
	}

	public String getWospwFSfAtr03() {
		return this.wospwFSfAtr03;
	}

	public void setWospwFSfAtr03(String wospwFSfAtr03) {
		this.wospwFSfAtr03 = wospwFSfAtr03;
	}

	public String getWospwFSfAtr04() {
		return this.wospwFSfAtr04;
	}

	public void setWospwFSfAtr04(String wospwFSfAtr04) {
		this.wospwFSfAtr04 = wospwFSfAtr04;
	}

	public String getWospwFSfAtr05() {
		return this.wospwFSfAtr05;
	}

	public void setWospwFSfAtr05(String wospwFSfAtr05) {
		this.wospwFSfAtr05 = wospwFSfAtr05;
	}

	public String getWospwFSfAtr06() {
		return this.wospwFSfAtr06;
	}

	public void setWospwFSfAtr06(String wospwFSfAtr06) {
		this.wospwFSfAtr06 = wospwFSfAtr06;
	}

	public String getWospwFSfAtr07() {
		return this.wospwFSfAtr07;
	}

	public void setWospwFSfAtr07(String wospwFSfAtr07) {
		this.wospwFSfAtr07 = wospwFSfAtr07;
	}

	public String getWospwFSfAtr08() {
		return this.wospwFSfAtr08;
	}

	public void setWospwFSfAtr08(String wospwFSfAtr08) {
		this.wospwFSfAtr08 = wospwFSfAtr08;
	}

	public String getWospwWkidAtr01() {
		return this.wospwWkidAtr01;
	}

	public void setWospwWkidAtr01(String wospwWkidAtr01) {
		this.wospwWkidAtr01 = wospwWkidAtr01;
	}

	public String getWospwWkidAtr02() {
		return this.wospwWkidAtr02;
	}

	public void setWospwWkidAtr02(String wospwWkidAtr02) {
		this.wospwWkidAtr02 = wospwWkidAtr02;
	}

	public String getWospwWkidAtr03() {
		return this.wospwWkidAtr03;
	}

	public void setWospwWkidAtr03(String wospwWkidAtr03) {
		this.wospwWkidAtr03 = wospwWkidAtr03;
	}

	public String getWospwWkidAtr04() {
		return this.wospwWkidAtr04;
	}

	public void setWospwWkidAtr04(String wospwWkidAtr04) {
		this.wospwWkidAtr04 = wospwWkidAtr04;
	}

	public String getWospwWkidAtr05() {
		return this.wospwWkidAtr05;
	}

	public void setWospwWkidAtr05(String wospwWkidAtr05) {
		this.wospwWkidAtr05 = wospwWkidAtr05;
	}

	public String getWospwWkidAtr06() {
		return this.wospwWkidAtr06;
	}

	public void setWospwWkidAtr06(String wospwWkidAtr06) {
		this.wospwWkidAtr06 = wospwWkidAtr06;
	}

	public String getWospwWkidAtr07() {
		return this.wospwWkidAtr07;
	}

	public void setWospwWkidAtr07(String wospwWkidAtr07) {
		this.wospwWkidAtr07 = wospwWkidAtr07;
	}

	public String getWospwWkidAtr08() {
		return this.wospwWkidAtr08;
	}

	public void setWospwWkidAtr08(String wospwWkidAtr08) {
		this.wospwWkidAtr08 = wospwWkidAtr08;
	}

	public String getWospwWkidAtr09() {
		return this.wospwWkidAtr09;
	}

	public void setWospwWkidAtr09(String wospwWkidAtr09) {
		this.wospwWkidAtr09 = wospwWkidAtr09;
	}

	public String getWospwWkidAtr10() {
		return this.wospwWkidAtr10;
	}

	public void setWospwWkidAtr10(String wospwWkidAtr10) {
		this.wospwWkidAtr10 = wospwWkidAtr10;
	}

	public String getWospwWkidAtr11() {
		return this.wospwWkidAtr11;
	}

	public void setWospwWkidAtr11(String wospwWkidAtr11) {
		this.wospwWkidAtr11 = wospwWkidAtr11;
	}

	public String getWospwWkidAtr12() {
		return this.wospwWkidAtr12;
	}

	public void setWospwWkidAtr12(String wospwWkidAtr12) {
		this.wospwWkidAtr12 = wospwWkidAtr12;
	}

	
	public PptOpisStanowiska getPptOpisStanowiska() {
		return this.pptOpisStanowiska;
	}

	public void setPptOpisStanowiska(PptOpisStanowiska pptOpisStanowiska) {
		this.pptOpisStanowiska = pptOpisStanowiska;
	}
	


}