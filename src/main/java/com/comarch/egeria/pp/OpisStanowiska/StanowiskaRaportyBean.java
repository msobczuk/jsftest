package com.comarch.egeria.pp.OpisStanowiska;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean // JBossTools
@Named
@Scope("view")
public class StanowiskaRaportyBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LogManager.getLogger(StanowiskaRaportyBean.class);

	private String selectedReport = "";

	private String jednostkaOrganizacyjna = "";
	private List<String> wybraneJO;
	private Date stanNaDzien = new Date();

	private int joSize;

	private boolean isJoFull = false;

	@Inject
	protected ReportGenerator reportGenerator;

	@PostConstruct
	public void init() {
		wybraneJO = new ArrayList<>();
	}

	public void loadJednostki() {
		SqlDataSelectionsHandler lw = this.getLW("PP_JEDNOSTKI_2");
		if (lw!=null)
			joSize = lw.getData().size();
	}

	public void displayReportAsPDF() {
		try {
			insertJoIntoTempTable();
			HashMap<String, Object> params = createParams();
			reportGenerator.displayReportAsPDF(params, selectedReport);
		} catch (JRException | SQLException | IOException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
	}

	public void displayReportAsRTF() {
		try {
			insertJoIntoTempTable();
			HashMap<String, Object> params = createParams();
			reportGenerator.displayReportAsRTF(params, selectedReport);
		} catch (JRException | SQLException | IOException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
	}

	public void displayReportAsDOCX() {
		try {
			insertJoIntoTempTable();
			HashMap<String, Object> params = createParams();
			reportGenerator.displayReportAsDOCX(params, selectedReport);
		} catch (JRException | SQLException | IOException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
	}

	public void displayReportAsODT() {
		try {
			insertJoIntoTempTable();
			HashMap<String, Object> params = createParams();
			reportGenerator.displayReportAsODT(params, selectedReport);
		} catch (JRException | SQLException | IOException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
	}

	private void insertJoIntoTempTable() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		this.execNoQuery("delete from ppt_in_tmp");
		try (Connection conn = JdbcUtils.getConnection()) {

			if (wybraneJO.size() < joSize) {
				for (String jo : wybraneJO) {
					JdbcUtils.sqlExecUpdate(conn, "insert into ppt_in_tmp(str_val) values (?)", jo);
				}
				isJoFull = false;
			} else {
				isJoFull = true;
			}
		}
	}

	public void wyborJO() {
		List<DataRow> dr = getSql("PP_JEDNOSTKI_2").getSelectedRows();
		wybraneJO = getJOFromSelectedRows(dr);
	}

	private List<String> getJOFromSelectedRows(List<DataRow> dr) {
		List<String> jednostki = new ArrayList<>();
		dr.forEach(el -> jednostki.add((String) el.get("pp_ob_kod")));
		return jednostki;

	}

	private HashMap<String, Object> createParams() {
		HashMap<String, Object> params = new HashMap<>();
		params.put("P_OB_KOD", wybraneJO);
		params.put("P_DATA", new java.sql.Date(stanNaDzien.getTime()));

		if (isJoFull || null == wybraneJO || wybraneJO.isEmpty())
			params.put("P_IS_IGNORE_OB_KOD", true);
		return params;
	}

	public String getJednostkaOrganizacyjna() {
		return jednostkaOrganizacyjna;
	}

	public void setJednostkaOrganizacyjna(String jednostkaOrganizacyjna) {
		this.jednostkaOrganizacyjna = jednostkaOrganizacyjna;
	}

	public Date getStanNaDzien() {
		return stanNaDzien;
	}

	public void setStanNaDzien(Date stanNaDzien) {
		this.stanNaDzien = stanNaDzien;
	}

	public String getSelectedReport() {
		return selectedReport;
	}

	public void setSelectedReport(String selectedReport) {
		this.selectedReport = selectedReport;
	}

}
