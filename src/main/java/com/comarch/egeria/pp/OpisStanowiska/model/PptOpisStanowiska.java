package com.comarch.egeria.pp.OpisStanowiska.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the PPT_OPS_OPIS_STANOWISKA database table.
 * 
 */
@Entity
@Table(name = "PPT_OPS_OPIS_STANOWISKA", schema = "PPADM")
@NamedQuery(name = "PptOpisStanowiska.findAll", query = "SELECT w FROM PptOpisStanowiska w")
public class PptOpisStanowiska implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "TABLE_KEYGEN_PPT_OPS_OPIS_STANOWISKA", table = "PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OPS_OPIS_STANOWISKA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "TABLE_KEYGEN_PPT_OPS_OPIS_STANOWISKA")
	@Column(name = "WOSP_ID")
	private long wospId;

	
	@Transient
	public SqlBean sqlBean;

	@Temporal(TemporalType.DATE)
	@Column(name = "WOSP_DATA_OD")
	private Date wospDataOd;

	@Column(name = "WOSP_CEL_ISTNIENIA")
	private String wospCelIstnienia;

	@Column(name = "WOSP_OB_ID")
	private Long wospObId;

	@Column(name = "WOSP_RODZAJ_WN")
	private String wospRodzajWn;

	@Column(name = "WOSP_STN_ID")
	private Long wospStnId;
	
	@Column(name = "WOSP_STJO_ID")
	private Long wospStjoId;	

	@Column(name = "WOSP_STN_ID_NAD_BP")
	private Long wospStnIdNadBp;

	@Column(name = "WOSP_STN_ID_NAD_PO")
	private Long wospStnIdNadPo;
	
	@Column(name = "WOSP_WYDZIAL_ID")
	private Long wospWydzialId;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "WOSP_DATA_DO")
	private Date wospDataDo;
	
	@Column(name = "WOSP_PRC_ID")
	private Long wospPrcId;
	
	@Column(name = "WOSP_ETAT")
	private BigDecimal wospEtat;
	
	@Column(name = "WOSP_F_ZASTEPSTWO")
	private String wospFZastepstwo;
	
	@Column(name = "WOSP_F_UKONCZONY")
	private String wospFUkonczony;
	
	@Column(name = "WOSP_SYMBOL")
	private String wospSymbol;

	
	
	@Column(name = "WOSP_CZIII_KIERUJACY_JO")
	private String wospCzIIIKierujacyJo;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "WOSP_CZIII_KIER_DATA")
	private Date wospCzIIIKierData;

	@Column(name = "WOSP_CZIII_KIER_STANOWISKO")
	private String wospCzIIIKierStanowisko;


	
	@Column(name = "WOSP_CZIII_ZATWIERDZAJACY")
	private String wospCzIIIZatwierdzajacy;

	@Temporal(TemporalType.DATE)
	@Column(name = "WOSP_CZIII_ZATW_DATA")
	private Date wospCzIIIZatwData;

	@Column(name = "WOSP_CZIII_ZATW_STANOWISKO")
	private String wospCzIIIZatwStanowisko;

	
	/**
	 * Id rekordu w tabeli ZPT_STANOWISKA_OPIS
	 */
	@Column(name = "WOSP_STO_ID")
	private Long wospStoId;	

	// bi-directional one-to-one association to PptOpsNwDane
	// @OneToOne
	// @JoinColumn(name="WOSP_ID", referencedColumnName="WOSPN_WOSP_ID")
	// private PptOpsNwDane pptOpsNwDane;
	// bi-directional many-to-one association to PptOpsWsDane
	@OneToMany(mappedBy = "pptOpisStanowiska", cascade = { CascadeType.ALL }, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<PptOpsNwDane> pptOpsNwDanes = new ArrayList<>();

	// bi-directional many-to-one association to PptOpsWsDane
	@OneToMany(mappedBy = "pptOpisStanowiska", cascade = { CascadeType.ALL }, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<PptOpsWsDane> pptOpsWsDanes = new ArrayList<>();

	public PptOpisStanowiska() {
	}

	// bi-directional many-to-one association to PptOpsGlZadania
	@OneToMany(mappedBy = "pptOpisStanowiska", cascade = { CascadeType.ALL }, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<PptOpsGlZadania> pptOpsGlZadanias = new ArrayList<>();

	// bi-directional many-to-one association to PptOpsPrc
	@OneToMany(mappedBy = "pptOpisStanowiska")
	private List<PptOpsPrc> pptOpsPrcs = new ArrayList<>();

	// bi-directional many-to-one association to PptOpsObOdpow
	@OneToMany(mappedBy = "pptOpisStanowiska", cascade = { CascadeType.ALL }, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<PptOpsObOdpow> pptOpsObOdpows = new ArrayList<>();
	
	@OneToMany(mappedBy = "pptOpisStanowiska", cascade = { CascadeType.ALL }, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<PptOpsNwWymogi> pptOpsNwWymogi = new ArrayList<>();

	public long getWospId() {
		return this.wospId;
	}

	public void setWospId(long wospId) {
		this.wospId = wospId;
	}

	public String getWospCelIstnienia() {
		return this.wospCelIstnienia;
	}

	public void setWospCelIstnienia(String wospCelIstnienia) {
		this.wospCelIstnienia = wospCelIstnienia;
	}

	public Long getWospObId() {
		return this.wospObId;
	}

	public void setWospObId(Long wospObId) {
		this.wospObId = wospObId;
		this.wospWydzialId = null;
		this.wospStnId = null;
		this.wospStjoId = null;
		this.wospSymbol = null;
	}

	public String getWospRodzajWn() {
		return this.wospRodzajWn;
	}

	public void setWospRodzajWn(String wospRodzajWn) {
		this.wospRodzajWn = wospRodzajWn;
	}

	public Long getWospStnId() {
		if (this.wospStnId == null)
			setWospSymbol("");
		return this.wospStnId;
	}

	public void setWospStnId(Long wospStnId) {
		this.wospStnId = wospStnId;
	}

	public Long getWospStnIdNadBp() {
		return this.wospStnIdNadBp;
	}

	public void setWospStnIdNadBp(Long wospStnIdNadBp) {
		this.wospStnIdNadBp = wospStnIdNadBp;
	}

	public Long getWospStnIdNadPo() {
		return this.wospStnIdNadPo;
	}

	public void setWospStnIdNadPo(Long wospStnIdNadPo) {
		this.wospStnIdNadPo = wospStnIdNadPo;
	}

	public Date getWospDataOd() {
		return wospDataOd;
	}

	public void setWospDataOd(Date wospDataOd) {
		this.wospDataOd = wospDataOd;
	}

	public List<PptOpsGlZadania> getPptOpsGlZadanias() {
		return this.pptOpsGlZadanias;
	}

	public void setPptOpsGlZadanias(List<PptOpsGlZadania> pptOpsGlZadanias) {
		this.pptOpsGlZadanias = pptOpsGlZadanias;
	}

	public PptOpsGlZadania addPptOpsGlZadania(PptOpsGlZadania pptOpsGlZadania) {
		getPptOpsGlZadanias().add(pptOpsGlZadania);
		pptOpsGlZadania.setPptOpisStanowiska(this);

		return pptOpsGlZadania;
	}

	public PptOpsGlZadania removePptOpsGlZadania(PptOpsGlZadania pptOpsGlZadania) {
		getPptOpsGlZadanias().remove(pptOpsGlZadania);
		pptOpsGlZadania.setPptOpisStanowiska(null);

		return pptOpsGlZadania;
	}

	public List<PptOpsPrc> getPptOpsPrcs() {
		return this.pptOpsPrcs;
	}

	public void setPptOpsPrcs(List<PptOpsPrc> pptOpsPrcs) {
		this.pptOpsPrcs = pptOpsPrcs;
	}

	public PptOpsPrc addPptOpsPrc(PptOpsPrc pptOpsPrc) {
		getPptOpsPrcs().add(pptOpsPrc);
		pptOpsPrc.setPptOpisStanowiska(this);

		return pptOpsPrc;
	}

	public PptOpsPrc removePptOpsPrc(PptOpsPrc pptOpsPrc) {
		getPptOpsPrcs().remove(pptOpsPrc);
		pptOpsPrc.setPptOpisStanowiska(null);

		return pptOpsPrc;
	}

	public List<PptOpsObOdpow> getPptOpsObOdpows() {
		return this.pptOpsObOdpows;
	}

	public void sortPptOpsObOdpows() {
		Collections.sort(this.pptOpsObOdpows, new Comparator<PptOpsObOdpow>() {
			@Override
			public int compare(PptOpsObOdpow odpowiedzialnosc1, PptOpsObOdpow odpowiedzialnosc2) {
				// return
				// odpowiedzialnosc2.getObodId().compareTo(odpowiedzialnosc1.getObodId());
				return Long.compare(odpowiedzialnosc1.getObodId(), odpowiedzialnosc2.getObodId());
			}
		});
	}

	public void setPptOpsObOdpows(List<PptOpsObOdpow> pptOpsObOdpows) {
		this.pptOpsObOdpows = pptOpsObOdpows;
	}

	public PptOpsObOdpow addPptOpsObOdpow(PptOpsObOdpow pptOpsObOdpow) {
		getPptOpsObOdpows().add(pptOpsObOdpow);
		pptOpsObOdpow.setPptOpisStanowiska(this);
		return pptOpsObOdpow;
	}

	public PptOpsObOdpow removePptOpsObOdpow(PptOpsObOdpow pptOpsObOdpow) {
		getPptOpsObOdpows().remove(pptOpsObOdpow);
		pptOpsObOdpow.setPptOpisStanowiska(null);

		return pptOpsObOdpow;
	}

	public List<PptOpsWsDane> getPptOpsWsDanes() {
		return this.pptOpsWsDanes;
	}

	public void setPptOpsWsDanes(List<PptOpsWsDane> pptOpsWsDanes) {
		this.pptOpsWsDanes = pptOpsWsDanes;
	}

	public PptOpsWsDane addPptOpsWsDane(PptOpsWsDane pptOpsWsDane) {
		getPptOpsWsDanes().add(pptOpsWsDane);
		pptOpsWsDane.setPptOpisStanowiska(this);

		return pptOpsWsDane;
	}

	public PptOpsWsDane removePptOpsWsDane(PptOpsWsDane pptOpsWsDane) {
		getPptOpsWsDanes().remove(pptOpsWsDane);
		pptOpsWsDane.setPptOpisStanowiska(null);

		return pptOpsWsDane;
	}

	public List<PptOpsNwDane> getPptOpsNwDanes() {
		return this.pptOpsNwDanes;
	}

	public void setPptOpsNwDanes(List<PptOpsNwDane> pptOpsNwDanes) {
		this.pptOpsNwDanes = pptOpsNwDanes;
	}

	public PptOpsNwDane addPptOpsNwDane(PptOpsNwDane pptOpsNwDane) {
		getPptOpsNwDanes().add(pptOpsNwDane);
		pptOpsNwDane.setPptOpisStanowiska(this);

		return pptOpsNwDane;
	}

	public PptOpsNwDane removePptOpsNwDane(PptOpsNwDane pptOpsNwDane) {
		getPptOpsNwDanes().remove(pptOpsNwDane);
		pptOpsNwDane.setPptOpisStanowiska(null);

		return pptOpsNwDane;
	}

	public Long getWospWydzialId() {
		return wospWydzialId;
	}

	public void setWospWydzialId(Long wospWydzialId) {
		this.wospWydzialId = wospWydzialId;
		this.wospStnId = null;
	}

	public Date getWospDataDo() {
		return wospDataDo;
	}

	public void setWospDataDo(Date wospDataDo) {
		this.wospDataDo = wospDataDo;
	}

	public Long getWospPrcId() {
		return wospPrcId;
	}

	public void setWospPrcId(Long wospPrcId) {
		this.wospPrcId = wospPrcId;
	}

	public BigDecimal getWospEtat() {
		return wospEtat;
	}

	public void setWospEtat(BigDecimal wospEtat) {
		this.wospEtat = wospEtat;
	}

	public String getWospFZastepstwo() {
		return wospFZastepstwo;
	}

	public void setWospFZastepstwo(String wospFZastepstwo) {
		this.wospFZastepstwo = wospFZastepstwo;
	}

	public String getWospFUkonczony() {
		return wospFUkonczony;
	}

	public void setWospFUkonczony(String wospFUkonczony) {
		this.wospFUkonczony = wospFUkonczony;
	}

	public String getWospSymbol() {
		return wospSymbol;
	}

	public void setWospSymbol(String wospSymbol) {
		this.wospSymbol = wospSymbol;
	}

	public Long getWospStjoId() {
		return wospStjoId;
	}

	public void setWospStjoId(Long wospStjoId) {
		this.wospStjoId = wospStjoId;
		
		SqlDataSelectionsHandler lw = null;
		
		if (this.sqlBean!=null) 
			lw = this.sqlBean.getLW("PP_STAN_W_JEDN");
		
		if (lw!=null){
			DataRow r = lw.find(wospStjoId);
			if (r!=null)
				this.setWospStnId( r.getLong("pp_stn_id"));
		}
	}

	public Long getWospStoId() {
		return wospStoId;
	}

	public void setWospStoId(Long wospStoId) {
		this.wospStoId = wospStoId;
	}

	public String getWospCzIIIKierujacyJo() {
		return wospCzIIIKierujacyJo;
	}

	public void setWospCzIIIKierujacyJo(String wospCzIIIKierujacyJo) {
		this.wospCzIIIKierujacyJo = wospCzIIIKierujacyJo;
	}

	public Date getWospCzIIIKierData() {
		return wospCzIIIKierData;
	}

	public void setWospCzIIIKierData(Date wospCzIIIKierData) {
		this.wospCzIIIKierData = wospCzIIIKierData;
	}

	public String getWospCzIIIKierStanowisko() {
		return wospCzIIIKierStanowisko;
	}

	public void setWospCzIIIKierStanowisko(String wospCzIIIKierStanowisko) {
		this.wospCzIIIKierStanowisko = wospCzIIIKierStanowisko;
	}

	public String getWospCzIIIZatwierdzajacy() {
		return wospCzIIIZatwierdzajacy;
	}

	public void setWospCzIIIZatwierdzajacy(String wospCzIIIZatwierdzajacy) {
		this.wospCzIIIZatwierdzajacy = wospCzIIIZatwierdzajacy;
	}

	public Date getWospCzIIIZatwData() {
		return wospCzIIIZatwData;
	}

	public void setWospCzIIIZatwData(Date wospCzIIIZatwData) {
		this.wospCzIIIZatwData = wospCzIIIZatwData;
	}

	public String getWospCzIIIZatwStanowisko() {
		return wospCzIIIZatwStanowisko;
	}

	public void setWospCzIIIZatwStanowisko(String wospCzIIIZatwStanowisko) {
		this.wospCzIIIZatwStanowisko = wospCzIIIZatwStanowisko;
	}

	public List<PptOpsNwWymogi> getPptOpsNwWymogi() {
		return pptOpsNwWymogi;
	}

	public void setPptOpsNwWymogi(List<PptOpsNwWymogi> pptOpsNwWymogi) {
		this.pptOpsNwWymogi = pptOpsNwWymogi;
	}
	
	public PptOpsNwWymogi addPptOpsNwWymogi(PptOpsNwWymogi pptOpsNwWymogi) {
		getPptOpsNwWymogi().add(pptOpsNwWymogi);
		pptOpsNwWymogi.setPptOpisStanowiska(this);

		return pptOpsNwWymogi;
	}

	public PptOpsNwWymogi removePptOpsNwWymogi(PptOpsNwWymogi pptOpsNwWymogi) {
		getPptOpsNwWymogi().remove(pptOpsNwWymogi);
		pptOpsNwWymogi.setPptOpisStanowiska(null);

		return pptOpsNwWymogi;
	}
}