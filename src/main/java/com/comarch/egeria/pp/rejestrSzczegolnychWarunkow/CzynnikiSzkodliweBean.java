package com.comarch.egeria.pp.rejestrSzczegolnychWarunkow;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean
@Named
@Scope("view")
public class CzynnikiSzkodliweBean extends SqlBean {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	@Inject
	User user;

	@Inject
	SessionBean sessionBean;

	@Inject
	protected ReportGenerator ReportGenerator;

	
	
	public void raport() {
		
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("prc_id", user.getPrcIdLong());
				ReportGenerator.displayReportAsPDF(params, "Czynniki_uciazliwe");

			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		
	}

	
	
	
	public void raport(Long prcId) {
		
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("prc_id", prcId);
				ReportGenerator.displayReportAsPDF(params, "Czynniki_uciazliwe");

			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		
	}


}
