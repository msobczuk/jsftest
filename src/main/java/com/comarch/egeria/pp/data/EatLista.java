package com.comarch.egeria.pp.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "EAT_LISTY", schema="EAADM")
@NamedQuery(name = "EatLista.findAll", query = "SELECT e FROM EatLista e")
public class EatLista implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "LST_ID")
	private String rowID;

	@Column(name = "LST_NAZWA")
	private String NazwaListy;

	@Column(name = "LST_QUERY")
	private String Zapytanie;

	@Column(name = "LST_OPIS")
	private String Opis;
	
	@Column(name = "LST_LP")
	private Integer Lp;



	public String getZapytanie() {
		return Zapytanie;
	}

	public void setZapytanie(String zapytanie) {
		Zapytanie = zapytanie;
	}

	public String getOpis() {
		return Opis;
	}

	public void setOpis(String opis) {
		Opis = opis;
	}

	public String getNazwaListy() {
		return NazwaListy;
	}

	public void setNazwaListy(String nazwaListy) {
		NazwaListy = nazwaListy;
	}

	public Integer getLp() {
		return Lp;
	}

	public void setLp(Integer lp) {
		Lp = lp;
	}

}
