package com.comarch.egeria.pp.data;

public class TestObject extends RXObjectBase {

	private int id;
	private String name = "";
	private String description = "";
	private String version = "";
	
	
	private double quantity;
	private double price;
	private double value;
	
	
	
	public int getId() {
		this.onRXGet("id");
		return id;
	}

	public void setId(int id) {
		this.onRXSet("id", this.id, id);
		this.id = id;
	}

	public String getName() {
		this.onRXGet("name");
		return name;
	}

	public void setName(String name) {
		this.onRXSet("name", this.name, name);
		this.name = name;
	}

	public String getVersion() {
		this.onRXGet("version");
		return version;
	}

	public void setVersion(String version) {
		this.onRXSet("version", this.version, version);
		this.version = version;
	}

	
	
	
	public double getQuantity() {
		this.onRXGet("quantity");
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.onRXSet("quantity", this.quantity, quantity);
		this.quantity = quantity;
		this.setValue( this.quantity * this.price);
	}

	public double getPrice() {
		this.onRXGet("price");
		return price;
	}

	public void setPrice(double price) {
		this.onRXSet("price", this.price, price);
		this.price = price;
		this.setValue( this.quantity * this.price);
	}

	public double getValue() {
		this.onRXGet("value");
		return value;
	}

	public void setValue(double value) {
		this.onRXSet("value", this.value, value);
		this.value = value;
	}

	public String getDescription() {
		this.onRXGet("description");
		return description;
	}

	public void setDescription(String description) {
		this.onRXSet("description", this.description, description);
		this.description = description;
	}
	
	 
	
}
