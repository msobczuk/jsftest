package com.comarch.egeria.pp.data;

import com.sun.faces.facelets.component.UIRepeat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.extensions.component.layout.LayoutPane;

import javax.faces.component.UIComponent;
import javax.faces.component.UINamingContainer;
import javax.faces.component.UIPanel;
import javax.faces.component.html.HtmlBody;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import java.util.HashMap;
import java.util.HashSet;

import static com.comarch.egeria.Utils.eq;
import static com.comarch.egeria.Utils.updateComponent;


public class RXObjectBase {
    private static final Logger log = LogManager.getLogger();
//    SessionBean sb = (SessionBean) AppBean.getSessionScopeBean("sessionBean");

    //uwaga na komponenty z <i:if test="...property z onRXGet i onRXSet ..."; docelowo przy podejsciu RX nalezy unikac sotosowania pn xmlns:i="http://xmlns.jcp.org/jsp/jstl/core"
	//przykład problemów z cZrodlaFinansowania.xhtml gdzie jest m.in.:
	//  <i:if test="#{zrodlaFinansowaniaBean.wczytajZrodlaFinansowaniaLista(cc.attrs.kategoria, cc.attrs.idEncji, cc.attrs.doWykorzystaniaNaZrodla) eq 'xyz'}"></i:if>
	//jesli którykolwiek z cc.attrs.kategoria, cc.attrs.idEncji, cc.attrs.doWykorzystaniaNaZrodla jest RX to w/w instrukcja powoduje, że w komponencie nie zadziała prawidłowo dodawanie [+] ...
	//zaczyna dzialać po przeniesieniu wprost do html wywołania #{zrodlaFinansowaniaBean.wczytajZrodlaFinansowaniaLista(cc.attrs.kategoria, cc.attrs.idEncji, cc.attrs.doWykorzystaniaNaZrodla)}"
	
	HashMap<String, HashSet<String>> rxsubscriptions =  new HashMap<>();

	public void onRXSet(String name, Object oldValue, Object newValue){
		boolean eq = eq(oldValue, newValue);

//		if (sb!=null && sb.requestPhase >= 6) return; //za poźno na update
		if (eq || FacesContext.getCurrentInstance() == null || FacesContext.getCurrentInstance().getCurrentPhaseId() == PhaseId.RENDER_RESPONSE) {
			return; //brak zaminy lub za poźno na update
		}

		// uwaga onRxSet jest istotne m.in. przy dodawaniu i usuwaniu el. z kolekcji
		// dlatego w metodach add/remove modelu musi byc wywolanie z null'em jako oldValue:  this.onRXSet("<<name>>", null, this.<<newValue>> );

		//		if ("dkzFormaWyplaty".equals(name))
		//			System.out.println( name );

		HashSet<String> hashSet = rxsubscriptions.get(name);
		if (hashSet != null && !eq) {
			for (String clientId : hashSet) {
				updateComponent(clientId);
			}
		}

	}



	public void onRXGet(String name){
//		if (sb!=null && sb.requestPhase < 6) return;//... insteresuje nas tylko faza renderowania (tylko komponenty faktycznie renderowane) ???
		if (FacesContext.getCurrentInstance()==null || FacesContext.getCurrentInstance().getCurrentPhaseId() != PhaseId.RENDER_RESPONSE) return; //insteresuje nas tylko faza renderowania (tylko komponenty faktycznie renderowane) ???


		UIComponent component = UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());

		if (component==null || !component.initialStateMarked()) //		!!! if (!component.isRendered()) return; tak nie wolno
			return;

//        if (eq("wndNumer", name))
//         	System.out.println("onRXGet / name: " + name + "; component: " + component + "; clientId: "+ component.getClientId());



		HashSet<String> hashSet = rxsubscriptions.get(name);
		if (hashSet==null){
			hashSet = new HashSet<>();
			rxsubscriptions.put(name, hashSet);
		}



		if (component instanceof HtmlBody) {
			hashSet.add("lpContent"); //ograniczamy zasieg update'a do lpContent
			return;
		}

		
		UIComponent p = component;
		while (p!=null){
			
			if (hashSet.contains(p.getClientId())) {
                return;//nie ma chyba potrzby dodawać dziecka jesli rodzic takze jest subskrybentem
            }
			p = p.getParent();
		}
		
		p = component;
		while (p instanceof UIRepeat && p.getParent()!=null) { // https://stackoverflow.com/questions/17224365/primefaces-uirepeat-component-does-not-update
			p = p.getParent();
		}
		
		if (p instanceof UIPanel && p.getParent() instanceof UINamingContainer)
			p = p.getParent();//cc/div  zazwyczaj generuje dynamiczny UIPanel, który sie nie updateuj'e dynamicznie bo tez sam nie jest renderowany

		if (p!=null) {
			if (!(p instanceof LayoutPane)) {
				hashSet.add(p.getClientId());
			}
        }

	}



}
