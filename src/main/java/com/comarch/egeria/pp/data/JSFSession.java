package com.comarch.egeria.pp.data;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


@ManagedBean(name="ssess")
@ViewScoped
public class JSFSession  implements Serializable {

	
	private Object entityId;

	public Object getEntityId() {
		return entityId;
	}

	public void setEntityId(Object entityId) {
		this.entityId = entityId;
	}
	
	
}
