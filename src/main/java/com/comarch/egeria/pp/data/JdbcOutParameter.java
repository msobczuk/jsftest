package com.comarch.egeria.pp.data;

//import java.sql.SQLType;

import oracle.jdbc.OracleTypes;

public class JdbcOutParameter {

	private Object value;
	private int type;
	private int index;
	private String name;
	
	
	public JdbcOutParameter(int pType){
		this.type = pType;
	}
	
//	public JdbcOutParameter(Object pValue, int pType){
//		this.value = pValue;
//		this.type = pType;
//	}
	
	
	@Override
	public String toString() {
		return ""+this.value;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}



	public int getIndex() {
		return index;
	}



	public void setIndex(int index) {
		this.index = index;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}

	
	
}
