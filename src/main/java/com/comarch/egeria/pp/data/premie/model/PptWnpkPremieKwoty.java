package com.comarch.egeria.pp.data.premie.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the PPT_WNPK_PREMIE_KWOTY database table.
 * 
 */
@Entity
@Table(name="PPT_WNPK_PREMIE_KWOTY")
@NamedQuery(name="PptWnpkPremieKwoty.findAll", query="SELECT p FROM PptWnpkPremieKwoty p")
public class PptWnpkPremieKwoty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_WNPK_PREMIE_KWOTY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_WNPK_PREMIE_KWOTY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_WNPK_PREMIE_KWOTY")
	@Column(name="WNPK_ID")
	private long wnpkId;

	@Column(name="WNPK_KWOTA")
	private Double wnpkKwota;

	@Column(name="WNPK_STAWKA_PROCENT")
	private Double wnpkStawkaProcent;

	@Column(name="WNPK_ZAT_ID")
	private Long wnpkZatId;
	
	@Temporal(TemporalType.DATE)
	@Column(name="WNPK_AUDYT_DM")
	private Date wnpkAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="WNPK_AUDYT_DT")
	private Date wnpkAudytDt;

	@Column(name="WNPK_AUDYT_KM")
	private String wnpkAudytKm;

	@Column(name="WNPK_AUDYT_LM")
	private BigDecimal wnpkAudytLm;

	@Column(name="WNPK_AUDYT_UM")
	private String wnpkAudytUm;

	@Column(name="WNPK_AUDYT_UT")
	private String wnpkAudytUt;

	//bi-directional many-to-one association to PptWnpwPremieWniosek
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="WNPK_WNPW_ID")
	private PptWnpwPremieWniosek pptWnpwPremieWniosek;

	public PptWnpkPremieKwoty() {
	}

	public long getWnpkId() {
		return this.wnpkId;
	}

	public void setWnpkId(long wnpkId) {
		this.wnpkId = wnpkId;
	}

	public Double getWnpkKwota() {
		return this.wnpkKwota;
	}

	public void setWnpkKwota(Double wnpkKwota) {
		this.wnpkKwota = wnpkKwota;
	}

	public Double getWnpkStawkaProcent() {
		return this.wnpkStawkaProcent;
	}

	public void setWnpkStawkaProcent(Double wnpkStawkaProcent) {
		this.wnpkStawkaProcent = wnpkStawkaProcent;
	}

	public Long getWnpkZatId() {
		return this.wnpkZatId;
	}

	public void setWnpkZatId(Long wnpkZatId) {
		this.wnpkZatId = wnpkZatId;
	}

	public PptWnpwPremieWniosek getPptWnpwPremieWniosek() {
		return this.pptWnpwPremieWniosek;
	}

	public void setPptWnpwPremieWniosek(PptWnpwPremieWniosek pptWnpwPremieWniosek) {
		this.pptWnpwPremieWniosek = pptWnpwPremieWniosek;
	}

	public Date getWnpkAudytDm() {
		return wnpkAudytDm;
	}

	public void setWnpkAudytDm(Date wnpkAudytDm) {
		this.wnpkAudytDm = wnpkAudytDm;
	}

	public Date getWnpkAudytDt() {
		return wnpkAudytDt;
	}

	public void setWnpkAudytDt(Date wnpkAudytDt) {
		this.wnpkAudytDt = wnpkAudytDt;
	}

	public String getWnpkAudytKm() {
		return wnpkAudytKm;
	}

	public void setWnpkAudytKm(String wnpkAudytKm) {
		this.wnpkAudytKm = wnpkAudytKm;
	}

	public BigDecimal getWnpkAudytLm() {
		return wnpkAudytLm;
	}

	public void setWnpkAudytLm(BigDecimal wnpkAudytLm) {
		this.wnpkAudytLm = wnpkAudytLm;
	}

	public String getWnpkAudytUm() {
		return wnpkAudytUm;
	}

	public void setWnpkAudytUm(String wnpkAudytUm) {
		this.wnpkAudytUm = wnpkAudytUm;
	}

	public String getWnpkAudytUt() {
		return wnpkAudytUt;
	}

	public void setWnpkAudytUt(String wnpkAudytUt) {
		this.wnpkAudytUt = wnpkAudytUt;
	}

}