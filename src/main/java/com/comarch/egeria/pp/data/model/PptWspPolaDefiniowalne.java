package com.comarch.egeria.pp.data.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the PPT_WSP_POLA_DEFINIOWALNE database table.
 * 
 */
@Entity
@Table(name = "PPT_WSP_POLA_DEFINIOWALNE")
@NamedQuery(name = "PptWspPolaDefiniowalne.findAll", query = "SELECT p FROM PptWspPolaDefiniowalne p")
public class PptWspPolaDefiniowalne implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "TABLE_KEYGEN_PPT_PPT_WSP_POLA_DEFINIOWALNE", table = "PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "CKK_ADRESY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "TABLE_KEYGEN_PPT_PPT_WSP_POLA_DEFINIOWALNE")
	@Column(name = "WPDF_ID")
	private Long wpdfId;

	@Temporal(TemporalType.DATE)
	@Column(name = "WPDF_AUDYT_DM")
	private Date wpdfAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name = "WPDF_AUDYT_DT")
	private Date wpdfAudytDt;

	@Column(name = "WPDF_AUDYT_KM")
	private String wpdfAudytKm;

	@Column(name = "WPDF_AUDYT_LM")
	private String wpdfAudytLm;

	@Column(name = "WPDF_AUDYT_UM")
	private String wpdfAudytUm;

	@Column(name = "WPDF_AUDYT_UT")
	private String wpdfAudytUt;

	@Column(name = "WPDF_ENCJA_ID")
	private Long wpdfEncjaId;

	@Column(name = "WPDF_ETYKIETA")
	private String wpdfEtykieta;

	@Column(name = "WPDF_FORMULARZ")
	private String wpdfFormularz;

	@Column(name = "WPDF_RODZAJ_ENCJI")
	private String wpdfRodzajEncji;

	@Column(name = "WPDF_WARTOSC_TXT")
	private String wpdfWartoscTxt;

	public PptWspPolaDefiniowalne() {
	}

	public Long getWpdfId() {
		return this.wpdfId;
	}

	public void setWpdfId(Long wpdfId) {
		this.wpdfId = wpdfId;
	}

	public Date getWpdfAudytDm() {
		return this.wpdfAudytDm;
	}

	public void setWpdfAudytDm(Date wpdfAudytDm) {
		this.wpdfAudytDm = wpdfAudytDm;
	}

	public Date getWpdfAudytDt() {
		return this.wpdfAudytDt;
	}

	public void setWpdfAudytDt(Date wpdfAudytDt) {
		this.wpdfAudytDt = wpdfAudytDt;
	}

	public String getWpdfAudytKm() {
		return this.wpdfAudytKm;
	}

	public void setWpdfAudytKm(String wpdfAudytKm) {
		this.wpdfAudytKm = wpdfAudytKm;
	}

	public String getWpdfAudytLm() {
		return this.wpdfAudytLm;
	}

	public void setWpdfAudytLm(String wpdfAudytLm) {
		this.wpdfAudytLm = wpdfAudytLm;
	}

	public String getWpdfAudytUm() {
		return this.wpdfAudytUm;
	}

	public void setWpdfAudytUm(String wpdfAudytUm) {
		this.wpdfAudytUm = wpdfAudytUm;
	}

	public String getWpdfAudytUt() {
		return this.wpdfAudytUt;
	}

	public void setWpdfAudytUt(String wpdfAudytUt) {
		this.wpdfAudytUt = wpdfAudytUt;
	}

	public Long getWpdfEncjaId() {
		return this.wpdfEncjaId;
	}

	public void setWpdfEncjaId(Long wpdfEncjaId) {
		this.wpdfEncjaId = wpdfEncjaId;
	}

	public String getWpdfEtykieta() {
		return this.wpdfEtykieta;
	}

	public void setWpdfEtykieta(String wpdfEtykieta) {
		this.wpdfEtykieta = wpdfEtykieta;
	}

	public String getWpdfFormularz() {
		return this.wpdfFormularz;
	}

	public void setWpdfFormularz(String wpdfFormularz) {
		this.wpdfFormularz = wpdfFormularz;
	}

	public String getWpdfRodzajEncji() {
		return wpdfRodzajEncji;
	}

	public void setWpdfRodzajEncji(String wpdfRodzajEncji) {
		this.wpdfRodzajEncji = wpdfRodzajEncji;
	}

	public String getWpdfWartoscTxt() {
		return this.wpdfWartoscTxt;
	}

	public void setWpdfWartoscTxt(String wpdfWartoscTxt) {
		this.wpdfWartoscTxt = wpdfWartoscTxt;
	}

}