package com.comarch.egeria.pp.data;

import com.sun.faces.facelets.el.ContextualCompositeMethodExpression;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.comarch.egeria.Utils.isNumeric;
import static com.comarch.egeria.Utils.nz;

//import org.primefaces.context.RequestContext;

@ManagedBean
@SessionScoped // @SessionScoped w Dialog Framework
public class CLovJS {
	
	
	
	HashMap<String,Lov> dlgLovs2Lovs = new HashMap<>();
	HashMap<String,Lov> cLovs2Lovs = new HashMap<>();
	
	private ArrayList<String> dlgWidgetVars = new ArrayList<>();


	public String getDlgWidgetVar(String clid) {
		Lov lov = this.cLovs2Lovs.get(clid);
		return lov.getWidgetVar();
	}
	

	public void setDlgLovId(String key){ 
		
		Lov lov = this.getLov(key);
		if (lov==null)
			return;
		
		UIComponent c = UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());
		if (c==null)
			return;
		
		lov.setDialogId(c.getClientId());
	}

	
	
	
	
	public void updateDlgDataSourceOnCCRender(String ccClientId, SqlDataSelectionsHandler ds){
		ccClientId = asViewIdClientId(ccClientId);

		Lov lov = this.cLovs2Lovs.get(ccClientId);

		if (lov==null || lov.getDialogId() == null || lov.getDatasource() == null)
			return;
		
		lov.setDatasource(ds);
		
		long dlgTS = lov.getDlgTs();
		
		lov.getDatasource().getData();
		long dsTS = lov.getDatasource().getTs();
		
		if ( FacesContext.getCurrentInstance().getPartialViewContext().isPartialRequest() 
				&& dlgTS < dsTS
			){
			lov.setDlgTs((new Date()).getTime());
			String clkpId = lov.getDialogId().replace(":dlgLovJS", "") + ":frmDlgLov:clkptbl";
//			RequestContext.getCurrentInstance().update(clkpId); //"dlgLovJSs:1:frmDlgLov:clkptbl"
			com.comarch.egeria.Utils.executeScript("if (parent!=undefined) parent.update('"+clkpId+"'); else update('"+clkpId+"'); ");
		}
	}
	

	public SqlDataSelectionsHandler getDlgLovJSDatasource(String key){
		if (key==null || key.isEmpty())
			return null;

		//key = asViewIdClientId(key); tu nie wolno, bo key to widg.var dla dlg

		Lov lov = this.getLov(key);
		if (lov==null)
			return null;

		SqlDataSelectionsHandler ds = lov.getDatasource();
		
		if (ds!=null) {
			lov.setDlgTs(ds.getTs());
		}
		return ds; 
	}
	
	
	int lvCntr;
	
	//ContextualCompositeMethodExpression action
	public void addLovDlg(String clid, SqlDataSelectionsHandler ds, String title, String lovTextFormat, String lovValueFormat, Boolean lockInputText){
//		if (title.contains("Źródła finansowania"))
//			System.out.println();

		boolean invalidated = false;

		clid = asViewIdClientId(clid);

		Lov lov = this.cLovs2Lovs.get(clid);
		if (lov == null && ds!=null){
			 lov= new Lov(ds, title, lovTextFormat, lovValueFormat, lockInputText);
			 this.cLovs2Lovs.put(clid, lov);
			 this.dlgLovs2Lovs.put(lov.getWidgetVar(), lov);
			 
			 if (!this.dlgWidgetVars.contains(lov.getWidgetVar())){
			 	this.dlgWidgetVars.set(lvCntr, lov.getWidgetVar());

				String pnlGrpId = "dlgLovJSs:"+lvCntr+":pnl";
				com.comarch.egeria.Utils.executeScript("parent.invalidateComponent('"+pnlGrpId+"');");
				invalidated = true;
				lvCntr++;
			 }
        }

		if (!invalidated && ds!=null)
			ds.invalidateClientComponents();
	}

	
	public Lov getLov(String key){
		Lov ret = this.dlgLovs2Lovs.get(key);
		if (ret==null) {
			String vkey = asViewIdClientId(key);
			ret = this.cLovs2Lovs.get(vkey);
		}
		return ret;
	}

	private String asViewIdClientId(String key) {
		String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
		if (!key.startsWith(viewId))
			key = viewId+"_"+key;
		return key;
	}


	public ArrayList<String> getDlgWidgetVars() {
        if (this.dlgWidgetVars.isEmpty()){
            for (int i=0; i<100; i++){
                this.dlgWidgetVars.add("empty_"+i);
            }
        }
		return dlgWidgetVars;
	}

	public void setDlgWidgetVars(ArrayList<String> dlgWidgetVars) {
		this.dlgWidgetVars = dlgWidgetVars;
	}
	
	
	public void invokeRowSelectAction(String ccClientId){
//		Lov lov = this.getLov(ccClientId);
		
		FacesContext ctx = FacesContext.getCurrentInstance();
		UIComponent cc = ctx.getViewRoot().findComponent(ccClientId);
		if (cc==null) return;
		
		ContextualCompositeMethodExpression m = (ContextualCompositeMethodExpression) cc.getAttributes().get("rowSelectAction");
		if (m==null) return;
		
		m.invoke(ctx.getELContext(), null);
		
	}

	public String calcWidthUnits(String w, String styleClass){
		if (nz(w).isEmpty() || (styleClass!=null && styleClass.contains("ui-g-")))
			return "";

		if (isNumeric(w))
			return "width: " + w + "px";
		else 
			return w;
	}
}

