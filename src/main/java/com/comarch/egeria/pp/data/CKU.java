package com.comarch.egeria.pp.data;

import java.io.IOException;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.component.UINamingContainer;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

@FacesComponent(value="cCKU")
public class CKU extends UINamingContainer  implements PhaseListener    { //
//public class CKU extends UIComponentBase  implements PhaseListener    { //
	
	
	private Boolean disabled = (Boolean) getAttributes().get("disabled")!=null ? (boolean) getAttributes().get("disabled"):false;
	
	public CKU() {
//		System.out.println("new CKU() ....");
		FacesContext.getCurrentInstance().getViewRoot().addPhaseListener(this);
		getAttributes().put("disabled", disabled);
		
//		if (disabled == null)
//	    {
//		  disabled = false;
//	    }
	}
	
	
	
    @Override
    public String getFamily()
    {
		return  UINamingContainer.COMPONENT_FAMILY; // "javax.faces.NamingContainer"
    }

	
	@Override
	public void afterPhase(PhaseEvent event) {
//		System.out.println("CKU.afterPhase..." + event.getPhaseId());
	}

	@Override
	public void beforePhase(PhaseEvent event) {
//		System.out.println("CKU.beforePhase..." + event.getPhaseId());
		
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}



	public Boolean getDisabled() {
		return disabled;
	}



	public void setDisabled(Boolean disabled) {
//		System.out.println("CKU.setDisabled->"+disabled);
		this.disabled = disabled;
	}

	
//	@Override
//	public void encodeAll(FacesContext context) throws IOException {
//		// TODO Auto-generated method stub
//		super.encodeAll(context);
//	} 
	
	
//	@Override
//	public void encodeChildren(FacesContext context) throws IOException {
//		// TODO Auto-generated method stub
//		super.encodeChildren(context);
//	}

}
