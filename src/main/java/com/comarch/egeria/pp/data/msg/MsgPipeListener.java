package com.comarch.egeria.pp.data.msg;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.Date;

import static com.comarch.egeria.Utils.eq;


public class MsgPipeListener implements Runnable {
	public static final Logger log = LogManager.getLogger();
	public static DataSource ds;
	
	
	public MsgPipeListener(DataSource ds){
		MsgPipeListener.ds = ds;
	}
	
	
	@Override
	public void run() {
		
//		System.out.println("MsgPipeListener.run()... AppBean.getSessionId(): " + AppBean.getSessionId());
			
		int addsleep = 0;
		
		while (0==0){
			
			try ( Connection conn = ds.getConnection() ) {
				while (1==1){

//					log.debug("...MsgPipeListener... AppBean.getSessionId(): " + AppBean.getSessionId());
					
					String txt = JdbcUtils.sqlFnVarchar(conn, "PPP_MSG.FN_RECEIVE"); //przykładowoa nazwa "PP_MSG_PIPE_10.132.216.41"
					if (txt==null)
						continue;  //timeout co 60 sekund
					
					
					if (txt.contains("!!!MSGLOG!!!"))//komunikat techniczny do zapisania w logach Tomcata
						System.out.println((new Date()) + "...MsgPipeListener... -> " + txt);

					if(txt!=null)
						log.debug("...MsgPipeListener... -> \n" + txt+"\n\n\n");
					
					
					// sprawdzanie otwartych str:  select * from V$DB_PIPES where NAME like 'PP\_MSG\_PIPE%' ESCAPE '\'
					if (txt.startsWith("SLEEP5M")){
						System.out.println("SLEEP5M");
						addsleep = 5*60000;//5 minut - potrzebne na rekompilacje pakietow
						break;
						
					}else if (txt.startsWith("SLEEP1M")){
						System.out.println("SLEEP1M");
						addsleep = 1*60000;//5 minut - potrzebne na rekompilacje pakietow
						break;


					}else if (txt.startsWith("JASPERTOPDF_ZALACZNIK")){//polecenie aktualizacji pliku pdf zapisanego jako zalacznik w tabeli ZKT_ZALACZNIKI / w txt parametry
						MsgJasperToPdf testListener = new MsgJasperToPdf(ds,txt);
						Thread msgListenerThread = new Thread(testListener);
						msgListenerThread.start();


					
//					}else if (txt.startsWith("JASPER_TO_PDF_ZAL")){ to był tylko POC
//						MsgJasperToPdf testListener = new MsgJasperToPdf(ds,txt);
//						Thread msgListenerThread = new Thread(testListener);
//						msgListenerThread.start();
					}else if (txt.startsWith("JASPERTOPDF")){
						//TODO pełna implementacja serwera wydruków w tym dodanie tabel w PPADM podobnych do CSST_REJESTR_WYDRUKOW; CSST_PARAM_REJESTRU_WYDRUKOW; csst_rejestr_wydrukow_pliki...
						// albo realizacja w oparciu o te tabele tylko z własną flagą, że to nasze raporty...
						
					}else{
						Msg msg = new Msg(txt);
						//MsgsBean.collectNewMsgTh(msg); // wstaw msg do wlasciwej kolekcji...
						for (User u : AppBean.sessionsUsers.values()) {

							if ( 	eq("%", msg.getUsername()) 				 	//wiadomość do wszystkich zalogowanych (spam! ... i  nie wysyłać brzydkich słów!)
									|| eq(u.getDbLogin(), msg.getUsername()) 	// ... lub do konkretnego użytkownika
									|| eq( u.getSessionId(), msg.getUsername()) // ... lub do konkretnej sesji
							) {

								if (eq( u.getSessionId(), msg.getUsername())){
									msg.setSessionId(msg.getUsername());
									msg.setUsername(u.getDbLogin());
								}

								u.newMsgs.add(msg);    //u.getMsgs().add(msg);
							}


						}
						Push.process(msg);//powiadm klientów aby wczytali w/w msg
						
						//Push.process(txt);//jesli dzialaja websockets...
						//MsgPollBean.process(txt); //jesli zamiast websocketow stosujemy https / long polling
						//AppBean.sendPush(msg);//p:socket - w MF totalna katastrofa... logi po kilka MB na minute...
						//MsgsBean.collectNewMsgTh(msg); //long polling przez Msg.xhtml
					}
				}
				
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			} finally {
				System.out.println("MsgPipeListener.run() -> finally");		
			}
			
			try {
				log.debug((new Date()) + "sleep: " +  (5000 + addsleep));
				Thread.sleep(5000 + addsleep);
				log.debug((new Date()) + ".....running......");
				addsleep = 0;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	
	
	

	
	
	

	
}
