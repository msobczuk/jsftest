package com.comarch.egeria.pp.data;

import com.comarch.egeria.pp.Administrator.model.PptAdmSlowniki;
import com.comarch.egeria.pp.Administrator.model.PptAdmWartosciSlownikow;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

@ManagedBean
@Named
@Scope("view")
public class SzablonLovBean extends SqlBean {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	@Inject
	SessionBean sessionBean;


	String lwNazwa;

	String lwHeader;

	String lwFooter;



	@PostConstruct
	public void init(){
		lwNazwa = sessionBean.getMenuLWValue();
		lwHeader = getSL("SZABLONLOV_FOOTERS").findAndFormat(lwNazwa,"%wsl_opis2$s");
		lwFooter = getSL("SZABLONLOV_FOOTERS").findAndFormat(lwNazwa,"%wsl_opis$s");
	}


	public void saveHdrFtr(){
		DataRow rSlownik = getLW("ppl_adm_slowniki").getAllData().stream().filter(r -> eq("SZABLONLOV_FOOTERS", r.get("sl_nazwa"))).findFirst().orElse(null);

		PptAdmSlowniki sl = (PptAdmSlowniki) HibernateContext.get(PptAdmSlowniki.class, rSlownik.getIdAsLong());
		PptAdmWartosciSlownikow wsl = sl.getPptAdmWartosciSlownikows().stream().filter(w -> eq(lwNazwa, w.getWslWartosc())).findFirst().orElse(null);
		if (wsl==null){
			wsl = new PptAdmWartosciSlownikow();
			wsl.setWslWartosc(lwNazwa);
			wsl.setWslSlNazwa("SZABLONLOV_FOOTERS");//pole wymagane
			wsl.setWslTyp("UC");//pole wymagane
			sl.addPptAdmWartosciSlownikow(wsl);
		}

		wsl.setWslOpis(lwFooter);
		wsl.setWslOpis2(lwHeader);
		HibernateContext.saveOrUpdate(sl);
	}


	public void deleteHdrFtr(){
		DataRow rSlownik = getLW("ppl_adm_slowniki").getAllData().stream().filter(r -> eq("SZABLONLOV_FOOTERS", r.get("sl_nazwa"))).findFirst().orElse(null);

		PptAdmSlowniki sl = (PptAdmSlowniki) HibernateContext.get(PptAdmSlowniki.class, rSlownik.getIdAsLong());
		PptAdmWartosciSlownikow wsl = sl.getPptAdmWartosciSlownikows().stream().filter(w -> eq(lwNazwa, w.getWslWartosc())).findFirst().orElse(null);
		if (wsl!=null){
			sl.removePptAdmWartosciSlownikow(wsl);
			HibernateContext.saveOrUpdate(sl);
		}
	}



	public String getLwNazwa() {
		return lwNazwa;
	}

	public void setLwNazwa(String lwNazwa) {
		this.lwNazwa = lwNazwa;
	}



	public String getLwHeader() {
		return lwHeader;
	}

	public void setLwHeader(String lwHeader) {
		this.lwHeader = lwHeader;
	}



	public String getLwFooter() {
		return lwFooter;
	}

	public void setLwFooter(String lwFooter) {
		this.lwFooter = lwFooter;
	}


}



