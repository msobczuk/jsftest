package com.comarch.egeria.pp.data;

import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponent;
import javax.faces.component.UINamingContainer;

@FacesComponent
public class CRepeatTbl extends UINamingContainer {

	private UIComponent repeat;

	public UIComponent getRepeat() {
		return repeat;
	}

	public void setRepeat(UIComponent repeat) {
		this.repeat = repeat;
//		this.repeat.getAttributes().put("var", getAttributes().get("var"));
	}


    public void init() {
//		nie zapomnnij o
//		<cc:implementation>
//	       <f:event type="postAddToView" listener="#{cc.init}" />
//		    ...

//		final Iterator<UIComponent> facetsAndChildren = this.getFacetsAndChildren();
		this.setRepeat( this.findComponent("rptr") );
		repeat.getAttributes().put("var", getAttributes().get("var"));
    }

	
}
