package com.comarch.egeria.pp.data.model;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.Entity;
import javax.persistence.Transient;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Entity
public abstract class DataSourceBase extends PptDataSource{

    @Transient
    private DataSource ds;

    @Transient
    public static ConcurrentHashMap<String, DataSourceBase> dataSourceCHM = new ConcurrentHashMap<>();

    public static DataSourceBase getDS(String dsName){
        if(dataSourceCHM.isEmpty()){
            List<DataSourceBase> lst = HibernateContext.query("from PptDataSource");
            for (DataSourceBase ds : lst) {
                dataSourceCHM.put(ds.getDsnName(), ds);
            }
        }
        if(dataSourceCHM.isEmpty()){
            DataSourceBase ds = AppBean.app.getDataSource();
            dataSourceCHM.put(ds.getDsnName(), ds);
        }
        return dataSourceCHM.get(dsName);
    }


    public DataSource getDs(){
        if(ds!=null) return ds;

        try {
            this.ds = (DataSource) (new InitialContext()).lookup("java:comp/env/jdbc/"+this.getDsnName());
            return ds;
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setDs(DataSource ds){
        this.ds = ds;
    }

    public abstract String formatSqlToLimitRows(String sqlName, String sql, int limitToRows);

    public abstract String getSysdate();

    public abstract Connection getConnection(Boolean setContext) throws SQLException;

    public abstract String getHibernateDialect();


    @Transient
    private boolean egeria6DB = false;
    public boolean isEgeria6DB() {
        return egeria6DB;
    }
    public void setEgeria6DB(boolean egeria6DB) {
        this.egeria6DB = egeria6DB;
    }

}
