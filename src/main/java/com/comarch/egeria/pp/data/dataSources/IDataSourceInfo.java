package com.comarch.egeria.pp.data.dataSources;

public interface IDataSourceInfo {
	String getUsername();
	String getUrl();
	Integer getNumActive();
	Integer getNumIdle();
	Integer getMaxTotal();
}
