package com.comarch.egeria.pp.data.liquibase;

import com.comarch.egeria.Utils;

import javax.sql.rowset.CachedRowSet;
import java.sql.SQLException;

public class Crs2LqbColumn {

    public String colPrefix = null;

    public String columnName;
    public String dataType;
    public int dataLength;
    public int dataPecision;
    public String comments;

    public boolean nullable = true;

    public Crs2LqbColumn(CachedRowSet crs) throws SQLException {
        columnName = crs.getString("COLUMN_NAME");
        dataType = crs.getString("DATA_TYPE");
        dataLength = crs.getInt("DATA_LENGTH");
        dataPecision = crs.getInt("DATA_PRECISION");
        comments = crs.getString("COMMENTS");
        nullable = "Y".equals(crs.getString("NULLABLE"));
    }


    public String getLiquibaseDataType(){
        //Mapowanie typow Oracle'a na liquibase
        String ret = "";
        if ("VARCHAR2".equals(dataType)) ret = "varchar";
//        if ("NVARCHAR2".equals(dataType)) ret = "varchar";
        if ("CLOB".equals(dataType)) ret = "clob";
        if ("NUMBER".equals(dataType)) ret = "number";


        if (dataLength>0) {
            ret += "(" + dataLength;
            if (dataPecision>0) ret += ","+dataPecision;
            ret += ")";
        }
        if ("DATE".equals(dataType)) ret = "date";
        return ret;
    }

    @Override
    public String toString() {
        //        return "Crs2PptColumn{" +
        //                "columnName='" + columnName + '\'' +
        //                ", dataType='" + dataType + '\'' +
        //                ", dataLength=" + dataLength +
        //                ", dataPecision=" + dataPecision +
        //                '}';

        return Utils.rpad(columnName,35) + Utils.rpad(getLiquibaseDataType(),20);
    }

    public String getXmlAddColumn(){

        String colName = columnName;
        if (colPrefix!=null && colName.startsWith(colPrefix+"_")) {
            colName = columnName.substring(colPrefix.length()+1);
        }

        return String.format("<column name=\"%1$s\" type=\"%2$s\"       remarks=\"%3$s\"/>",
                getXmlColumnName(), getLiquibaseDataType(), comments);
    }


    public String getXmlColumnName(){
        String ret = columnName;
        if (colPrefix!=null && ret.startsWith(colPrefix+"_")) {
            ret = "${colprefix}_" + columnName.substring(colPrefix.length()+1);
        }
        return ret;
    }

}
