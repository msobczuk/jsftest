package com.comarch.egeria.pp.data;

import com.comarch.egeria.web.User;

import javax.faces.context.FacesContext;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;

public class RodoUtils {

    public static void dsZapamietajSesje(User user){
        String sessionId = FacesContext.getCurrentInstance().getExternalContext().getSessionId(false);
        AppBean.sessionsUsers.put(sessionId, user);

        try ( Connection conn = JdbcUtils.getConnection(false) ) {
             user.setSzwId( JdbcUtils.sqlFnLong(conn, "PPADM.PPP_DZIENNIK_SESJI.FN_ZAPAMIETAJ_SESJE", sessionId) );
        } catch (Exception e) {
            AppBean.log.error(e.getMessage(), e);
        }
    }

    public static void dsWczytajWidokiXhtmlCHM() {
        try {

            SqlDataSelectionsHandler sqlWidokiXhtml = AppBean.sqlBean.getSql("sqlWidokiXhtml", "select * from PPT_DX_WIDOKI_XHTML where DXW_WERSJA = ?", AppBean.version );
            for (DataRow r : sqlWidokiXhtml.data) {
                BigDecimal dxwId = (BigDecimal) r.get("DXW_ID");
                AppBean.widokiXhtml.put(""+r.get("DXW_PLIK"), dxwId.longValue());
            }

            System.out.println(String.format("... PPT_DX_WIDOKI_XHTML: %1$s widoków xhtml (dla wersji '%2$s') ...", AppBean.widokiXhtml.size(), AppBean.version ) );
        } catch (Exception e) {
            AppBean.log.error(e.getMessage(), e);
        }
    }

    public static Long dsWstawWidokXhtmlCHM(String viewId){

        Long dxwId = AppBean.widokiXhtml.get(viewId);

        if (dxwId == null){
            try (Connection conn = JdbcUtils.getConnection(false)){

                dxwId = JdbcUtils.sqlFnLong(conn, "PPADM.PPP_DZIENNIK_SESJI.WSTAW_WIDOK", viewId, AppBean.version);
                AppBean.widokiXhtml.put(viewId, dxwId);

            } catch (SQLException e) {
                AppBean.log.error(e.getMessage(), e);
            }
        }
        return dxwId;
    }

    public static void dsUruchomionoWidokXhtml(Long szwid, Long dxwid, Long dataRowId){
        if (dxwid == null || dxwid < 0)
            return; //nie wszystkie eidoki wymagają logowania - wystarczy zanegować w rej. id widoku aby go nie logować (wymagany restart romcata)

        try (Connection conn = JdbcUtils.getConnection(false)){
            JdbcUtils.sqlSPCall(conn, "PPADM.PPP_DZIENNIK_SESJI.URUCHOMIONO_WIDOK", szwid, dxwid, dataRowId);
        } catch (SQLException e) {
            AppBean.log.error(e.getMessage(), e);
        }
    }

    public static void dsUruchomionoWidokXhtml(User user, Long dxwid, Long dataRowId){
        if (user==null)
            return;

        dsUruchomionoWidokXhtml(user.getSzwId(), dxwid, dataRowId);
    }

    public static void dsWczytajRaportyCHM() {
        try {

            SqlDataSelectionsHandler sqlWidokiXhtml = AppBean.sqlBean.getSql("sqlRaporty", "select * from PPT_DX_RAPORTY where DXR_WERSJA = ?", AppBean.version );
            for (DataRow r : sqlWidokiXhtml.data) {
                BigDecimal dxwId = (BigDecimal) r.get("DXR_ID");
                AppBean.raporty.put(""+r.get("DXR_PLIK"), dxwId.longValue());
            }

            System.out.println(String.format("... PPT_DX_RAPORTY: %1$s raportów (dla wersji '%2$s') ...", AppBean.raporty.size(), AppBean.version ) );
        } catch (Exception e) {
            AppBean.log.error(e.getMessage(), e);
        }
    }

    public static Long dsWstawRaportCHM(String dxrPlik, String typRaportu){

        Long dxrid = AppBean.raporty.get(dxrPlik);

        if (dxrid == null){
            try (Connection conn = JdbcUtils.getConnection(false)){

                dxrid = JdbcUtils.sqlFnLong(conn, "PPADM.PPP_DZIENNIK_SESJI.WSTAW_RAPORT", dxrPlik, AppBean.version, typRaportu);
                AppBean.raporty.put(dxrPlik, dxrid);

            } catch (SQLException e) {
                AppBean.log.error(e.getMessage(), e);
            }
        }

        return dxrid;
    }

    public static void dsUruchomionoRaport(Long szwid, Long dxrid){
        if (dxrid == null || dxrid < 0)
            return; //nie wszystkie raporty wymagają logowania - wystarczy zanegować w rej. id raportu aby go nie logować (wymagany restart romcata)

        try (Connection conn = JdbcUtils.getConnection(false)){
            JdbcUtils.sqlSPCall(conn, "PPADM.PPP_DZIENNIK_SESJI.URUCHOMIONO_RAPORT", szwid, dxrid);
        } catch (SQLException e) {
            AppBean.log.error(e.getMessage(), e);
        }
    }

    public static void dsUruchomionoRaport(User user, Long dxrid){
        if (user==null)
            return;
        dsUruchomionoRaport(user.getSzwId(), dxrid);
    }

    public static void dsUruchomionoRaport(Long dxrid){
        User user  = AppBean.getUser();
        if (user==null)
            return; //niezalogowany...
        dsUruchomionoRaport(user.getSzwId(), dxrid);
    }

    public static void dslogUruchomionoRaport(String dxrPlik, String typRaportu){
        User user  = AppBean.getUser();

        if (user==null)
            return; //niezalogowany...

        Long dxrid = AppBean.raporty.get(dxrPlik);
        if (dxrid == null){
            dxrid = dsWstawRaportCHM(dxrPlik, typRaportu);
        }

        dsUruchomionoRaport(user.getSzwId(), dxrid);
    }
}
