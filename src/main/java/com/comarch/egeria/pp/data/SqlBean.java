package com.comarch.egeria.pp.data;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.Administrator.model.PptAdmListyWartosci;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import com.lowagie.text.DocumentException;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;
import org.springframework.core.io.Resource;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.PartialViewContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.sql.rowset.CachedRowSet;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@ManagedBean 
@Named
@Scope("view")
public class SqlBean  implements Serializable{
	
	private static final long serialVersionUID = -5806366986076184951L;
	private static final Logger log = LogManager.getLogger();//public bo wiekszosc beanow dziedziczy po tym
	
	@Inject
	public AppBean appBean;
	
	@Inject
	SqlDataCache sqlDataCache;
	
//	@Inject 
	User user;



	public String sessionId = null;

//	private HashMap<String, SqlDataSelectionsHandler> sqls = new HashMap<>();

	public ConcurrentHashMap<String, SqlDataSelectionsHandler> getSqls() {
//		return this.sqls;
		
		if (this.sqlDataCache==null && FacesContext.getCurrentInstance()!=null){
			Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			this.sqlDataCache = (SqlDataCache) sessionMap.get("sqlDataCache");
			//System.out.println(); // com.comarch.egeria.pp.data.CPracownikBasicInfoBean com.comarch.egeria.pp.delegacje.DelegacjaMFBean
		}
			
		if (this.sqlDataCache==null)
			this.sqlDataCache = new SqlDataCache(); //instancja AppBean.sqlBean 
		
		return this.sqlDataCache.sqls; 
		
	}

	public SqlDataCache getSqlDataCache() {
		return sqlDataCache;
	}

//	public void setSqlDataCache(SqlDataCache sqlDataCache) {
//		this.sqlDataCache = sqlDataCache;
//	}
	
	public void clearSqlDataCache(){
		this.getSqlDataCache().clear();
	}
	
	
	
	
	public String getSessionId(){
		String ctxSessionId = AppBean.getSessionId();

		if(ctxSessionId!=null && !eq(ctxSessionId, this.sessionId))
			this.sessionId = ctxSessionId;//leniuch, ale trzeba zapewnić możliwość odczytania sesji użytkownia z wątku MsgPipeListener'a

		return this.sessionId;
	}
	
	public void displayReport(String reportName, HashMap<String, Object> params){
		displayReport("pdf", reportName, params);
	}
	
	public void displayReport(String outputType, String reportName, HashMap<String, Object> params){
		if (params == null) 
				params = new HashMap<String, Object>();
		
		try {
			ReportGenerator.displayReport(outputType, reportName, params);
		} catch (JRException | SQLException | IOException e) {
			e.printStackTrace();
			User.alert(e.getMessage());
		}
	}
	
	
	
	public void updateAndShowDialog(String dlgWidgetVar){
		com.comarch.egeria.Utils.update(dlgWidgetVar);
		com.comarch.egeria.Utils.executeScript("PF('"+dlgWidgetVar+"').show()");
	}

	
	public String tmp(){
		String ret = this.getClass().getSimpleName();
		user.tmpUserObjectsCHM.put(ret, this);
		return ret;
	}
	
	
	public String tmp(Object x){
		if (x==null)
			return "";
		
		String ret = x.getClass().getSimpleName();
		user.tmpUserObjectsCHM.put(ret, x);
		return ret;
	}
	
	@PostConstruct
	public void initSqlBean(){// nie init() bo wiele po tym dziedziczy i mają juz rózne sygnatury metod init (public/protected... throws ... )
		user = AppBean.getUser();//statyczne wczytanie usera - nie przez  inject bo SqlBean bywa ładowany przed zalogowaniem sie 
		if (user != null){
			String simpleName = this.getClass().getSimpleName();
			user.tmpUserObjectsCHM.put(simpleName, this);
		}
	}
	

	public String clearTmpUserObjectsCHM(){
		PartialViewContext pvc = FacesContext.getCurrentInstance().getPartialViewContext();
		boolean partialRequest = pvc.isPartialRequest();
		if (user != null && !partialRequest) {
			user.clearTmpUserObjectsCHM();
		}
		return "";
	}
	
	
	/**
	 * Ukrywa część wierszy wpisując przekzane identyfikatory do kolekcji hiddenRowsPK
	 * Wiersze ukryte nie sa dostepne przez lw.getData() ale są dostepne przez lw.find(pk)
	 * Dzięki temu jesteśmy np. w stanie stowrzyć sytuację kiedy użyszkodnik nie może 
	 * z listy wartości dodawac już tych obiektów, które już są dodane w encji
	 * (Np. nie doda kolejny raz wykładowcy, który już jest na terminie szkolenia) 
	 * @param sqlid
	 * @param pklist
	 */
	public static void hideLwRows(String sqlid, Collection pklist) {
		hideLwRows(sqlid, pklist, false);
	}
	
	public static void hideLwRows(String sqlid, Collection pklist, boolean add) {
		SqlDataSelectionsHandler lw = findLW(sqlid);
		if (lw==null) return;
		
		if (!add) 
			lw.getHiddenRowsPK().clear();
		
		lw.hideAll(pklist);
		resetDatasourceAndUpdateAllComponents(sqlid, null);
	}
	
	public static void hideLwRows(String sqlid, Object pk) {
		hideLwRows(sqlid, pk, false);
	}
	
	public static void hideLwRows(String sqlid, Object pk, boolean add) {
		SqlDataSelectionsHandler lw = findLW(sqlid);
		if (lw==null) return;
		
		if (!add) 
			lw.getHiddenRowsPK().clear();
		
		lw.hide(pk);
		resetDatasourceAndUpdateAllComponents(sqlid, null);
	}
	
	
	
	
	public String mapujBySL(String sqlId, String key, String defaultvalue){
		SqlDataSelectionsHandler sl = this.getSlownik(sqlId);
		DataRow r = sl.find(key);
		if (r==null)
		 return defaultvalue;
		
		
		String ret = ""+r.get(1);
		return ret;
	} 
	
	
	public void resetTs(String sqlId){
		SqlDataSelectionsHandler sql = this.getSql(sqlId);
		if (sql!=null)
			sql.resetTs();
	}

	
	public String join(String delimiter, Object... args) {
		return Utils.join(delimiter, args);
	}

	public String join(Object... args) {
		return Utils.join(args);
	}
	
	public String join(Collection<?> collection) {
		return Utils.join(collection);
	}
	
	public String join(Collection<?> collection, String delimiter) {
		return Utils.join(delimiter, collection);
	}
	
	/**
	 * Przeszukuje rekurnecyjnie wszystkie komponenty począwszy od wskazanego roota i jesli znajdzie taki, który ma ustawiony datasource z shortname pasujacym do sqlId, to uniewazna jego datasource oraz wykonuje update komponentu
	 * @param sqlId
	 * @param root
	 */
	public static void resetDatasourceAndUpdateAllComponents(String sqlId, UIComponent root){
		if (sqlId==null || sqlId.isEmpty())
			return;
		sqlId = sqlId.toLowerCase();
		
		if (root==null)
			root = FacesContext.getCurrentInstance().getViewRoot();

		Iterator<UIComponent> children = root.getFacetsAndChildren();
		
		while (children.hasNext()) {
			UIComponent uic = children.next(); 
			Object ds = uic.getAttributes().get("datasource");
			if (ds!=null && ds instanceof SqlDataSelectionsHandler ){
				SqlDataSelectionsHandler sql = (SqlDataSelectionsHandler) ds;
				if (sqlId.equals(sql.getShortName())){
					
					sql.resetTs();
					com.comarch.egeria.Utils.update(uic.getClientId());
					
				}
			}
			resetDatasourceAndUpdateAllComponents(sqlId, uic);//rekurncja!!!
		}
	}
	

	
	public static void reloadAndUpdate(String sqlId, Object rowId){
		if (sqlId==null || rowId==null)
			return;
		
		SqlDataSelectionsHandler lw = SqlBean.findLW(sqlId);
		if (lw==null) return;
		
		DataRow rOld = lw.find(rowId);
		 
		String strOld = ""+rOld;
		
		List<UIComponent> uicList = getAllUIComponents(null)
				 .stream()
				 .filter(uic ->  lw.equals(uic.getAttributes().get("datasource")) )
				 .collect(Collectors.toList());		
		
		 
		for (UIComponent uic : uicList) {
			 //SqlDataHandler sql =  (SqlDataHandler) uic.getAttributes().get("datasource"); 
			 
			 lw.reLoadDataRow(""+rowId);
			 DataRow rNew = lw.find(rowId);
			
			 String strNew = ""+rNew;
			 if (!strOld.equals(strNew)){
				 
				if (rOld==null){
					 lw.clearFilter();
					 com.comarch.egeria.Utils.update(uic.getClientId());
				} else {
					DataTable tb = (DataTable) uic.findComponent("dane");
					String clientId = tb.getClientId();
					com.comarch.egeria.Utils.update(tb.getClientId());
					if (rNew==null && rOld!=null){//skasowano wiersz
						lw.setRowSelection(false, rOld);
						if (lw.getFilteredData()!=null)
							lw.getFilteredData().remove(rOld);
					}					
				}
			 }
			 
		 }
	}	
	
	
	public void reloadAndUpdate(DataRow r){
		if (r==null || r.getSqlData()==null)
			return;

		Object pk = r.get(0);
		if (r.getSqlData().pkColumnName!=null) 
			pk = r.get(r.getSqlData().pkColumnName);
		
		String strOld = ""+r; 
		r.reload();
		DataRow r1 = r.getSqlData().find(pk);
		String strNew = ""+r1;
		
		if (strOld.equals(strNew))
			return;//dane w bazie sa identyczne

		 //odswiez dane w tabelkach 
		 List<UIComponent> uicList = getAllUIComponents(null).stream().filter(uic -> r.getSqlData().equals( uic.getAttributes().get("datasource") )).collect(Collectors.toList());
		 for (UIComponent uic : uicList) {
			 DataTable tb = (DataTable) uic.findComponent("dane");
			 com.comarch.egeria.Utils.update(tb.getClientId());
		 }
	}	
	
	
	
	public static ArrayList<UIComponent> getAllUIComponents(UIComponent root){
		ArrayList<UIComponent> ret = new ArrayList<>();
		
		if (root==null)
			root = FacesContext.getCurrentInstance().getViewRoot();

		Iterator<UIComponent> children = root.getFacetsAndChildren();
		
		while (children.hasNext()) {
			UIComponent uic = children.next(); 
			ret.add(uic);
			ret.addAll( getAllUIComponents(uic) );
		}
		
		return ret;
	}	
	
	
	
	public void editLW(String lstNazwa) throws SQLException {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", null);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("lista", lstNazwa);
	
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("SzczegolyListyWartosci");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	
	
	
	/**
	 * Przeszukuje FacesContext.getCurrentInstance().getViewRoot().getViewMap()
	 * oraz zaladowane instancje typu SqlBean. Jeslli znajdzie w nich sql z
	 * sqlId to go zwraca. Jesli nie to null;
	 * @param sqlId
	 * @return
	 */
	public static SqlDataSelectionsHandler findLW(String sqlId){
		
		FacesContext ctx = FacesContext.getCurrentInstance();
		if (ctx!=null){

			Map<String, Object> viewMap = ctx.getViewRoot().getViewMap();
			
			for (String k : viewMap.keySet()) {
				Object x = viewMap.get(k);
				if (x instanceof SqlBean){
					SqlDataSelectionsHandler sql = ((SqlBean) x).getSql(sqlId);
					if (sql !=null)
						return sql;
				}
			}
		}
		
		return null;
	}

	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * W razie koniecznosci wczytuje i zwraca listę wartości jako obiekt SqlDataSelectionsHandler
	 * @param sqlId - id/nazwa listy wartosci zapytanie sql
	 * @param params  - varargs - parametry do sql
	 * @return SqlDataSelectionsHandler (lub null jeśli lista o sqlId nie istnieje w bazie)
	 */
	public SqlDataSelectionsHandler getLW(String sqlId, Object... params) {
		ArrayList<Object> pars = new ArrayList<Object>();
     	pars.addAll( Arrays.asList(params) );
		return getLW(sqlId, pars);
	}

	
	
	
	
	/**
	 * W razie koniecznosci wczytuje i zwraca listę wartości jako obiekt SqlDataSelectionsHandler
	 * @param sqlId - id/nazwa listy wartosci zapytanie sql
	 * @param params lista parametrów do sql
	 * @return SqlDataSelectionsHandler (lub null jeśli lista o sqlId nie istnieje w bazie)
	 */
	public SqlDataSelectionsHandler getLW(String sqlId, ArrayList<Object> params) {
		sqlId = sqlId.toLowerCase();
		SqlDataSelectionsHandler sqlData = this.getSqls().get(sqlId);
		
		if (sqlData == null || ! JdbcUtils.normalizeSqlParams(sqlData.getSqlParams()).toString()
						.equals( JdbcUtils.normalizeSqlParams(        params        ).toString() )) {
			loadEgListaWartosci(sqlId, params);
		}
		
		return this.getSqls().get(sqlId);
	}
	
	
	
	
	
	
	/**
	 * W razie koniecznosci wczytuje i zwraca listę wartości jako obiekt SqlDataSelectionsHandler
	 * @param sqlId - id/nazwa listy wartosci zapytanie sql
	 * @param namedParams mapa parametrów do sql
	 * @return SqlDataSelectionsHandler (lub null jeśli lista o sqlId nie istnieje w bazie)
	 */
	public  SqlDataSelectionsHandler getLW(String sqlId, HashMap<String,  Object> namedParams) {
		sqlId = sqlId.toLowerCase();
		SqlDataSelectionsHandler sqlData = this.getSqls().get(sqlId);
		
		if (sqlData == null  || ! JdbcUtils.normalizeSqlParams(sqlData.getSqlNamedParams()).toString()
				                .equals( JdbcUtils.normalizeSqlParams(       namedParams      ).toString() )) {
			
			loadEgListaWartosci(sqlId, namedParams);
		}
		
		return this.getSqls().get(sqlId);
	}	
	
	
	
	
	

	
	/**
	 * W razie koniecznosci wczytuje i zwraca listę wartości jako obiekt SqlDataSelectionsHandler
	 * @param sqlId - id/nazwa listy wartosci zapytanie sql
	 * @return SqlDataSelectionsHandler (lub null jeśli lista o sqlId nie istnieje w bazie)
	 */
	public SqlDataSelectionsHandler getLW(String sqlId) {

		sqlId = sqlId.toLowerCase();
		SqlDataSelectionsHandler sqlData = this.getSqls().get(sqlId);
		
		if (sqlData == null) {
			sqlData = findLW(sqlId);
			if (sqlData != null)
				this.getSqls().put(sqlId, sqlData);
		}
		
		if (sqlData == null) {
			loadEgListaWartosci(sqlId, null);
		}

		return this.getSqls().get(sqlId);
	}	
	
	
	
	
	
	
	
	/**
	 * W razie koniecznosci wczytuje listę wartości
	 * @param sqlId - id/nazwa listy wartosci zapytanie sql
	 * @param params lista parametrów do sql
	 */
	public void loadLW(String sqlId, ArrayList<Object> params) {
		sqlId = sqlId.toLowerCase();
		SqlDataSelectionsHandler sqlData = this.getSqls().get(sqlId);
		if (sqlData == null || !sqlData.getSqlParams().toString().equals(params.toString())) {
			loadEgListaWartosci(sqlId, params);
		}
	}
	
	
	
	
	/**
	 * W razie koniecznosci wczytuje listę wartości
	 * @param sqlId - id/nazwa listy wartosci zapytanie sql
	 * @param params  - varargs - parametry do sql
	 */
	public void loadLW(String sqlId, Object... params) {
		ArrayList<Object> pars = new ArrayList<Object>();
     	pars.addAll( Arrays.asList(params) );
		loadLW(sqlId, pars);
	}
	
	
	public SqlDataSelectionsHandler loadedLW(String sqlid){
		sqlid = sqlid.toLowerCase();
		return this.getSqls().get(sqlid);
	}

	
	
	
	public static boolean eq(Object x1, Object x2){
		return (x1!=null && x1.equals(x2)) || (x2!=null && x2.equals(x1) || (x1==null && x2==null));
	}
	
//	public static String nz(String x){
//		if (x==null) 
//				return "";
//		return x;
//	} 
//
//	public static Double nz(Double x){
//		if (x==null) 
//			return 0.00;
//		return x;
//	} 
//
//	public static Long nz(Long x){
//		if (x==null) 
//			return 0L;
//		return x;
//	} 
//	
//	public static BigDecimal nz(BigDecimal x){
//		if(x==null)
//			return new BigDecimal(0);
//		return x;
//	}
//
//	public static Integer nz(Integer x){
//		if (x==null) 
//			return 0;
//		return x;
//	} 

	public Double cdbl(Long x) {
		return Utils.cdbl(x);
	}
	public Double cdbl(Integer x) {
		return Utils.cdbl(x);
	}
	public Double cdbl(BigDecimal x) {
		return Utils.cdbl(x);
	}
	public Double cdbl(String x) {
		return Utils.cdbl(x);
	}






	public String format(Object x){
		return Utils.format(x);
	}

	public String format(Object x, String pattern){
		return Utils.format(x, pattern);
	}



	public String formatNzNumber(Number number){
		if (number==null) number = 0.0;
		return Utils.format(number);
	}

	public String formatNzNumber(Number number, String pattern){
		if (number==null) number = 0.0;
		return Utils.format(number, pattern);
	}



	public Object execScalarQuery(String sql, Object... args) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		return JdbcUtils.sqlExecScalarQuery(sql, args);
	}	
	

	public void execNoQuery(String sql, Object... args) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		JdbcUtils.sqlExecNoQuery(sql, args);
	}	
	
	
	
	public CachedRowSet execSqlQuery(String sql, Object... args) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		return JdbcUtils.sqlExecQuery(sql, args);
	}	
	
	
	
	
	public  String loadEgListaWartosci(String sqlId, Object params){
		if (sqlId==null) 
			return "";
		
		if ("ppl_adm_powiadomienia".equals(sqlId) && AppBean.getUser()==null){
			return "";
		}
		
		String methodname = "loadEgListaWartosci/"+sqlId+"/"+params;
		long t0 = AppBean.logMethod(methodname);

		sqlId = sqlId.toLowerCase();
		try {
			
			SqlDataSelectionsHandler sqlData = this.getSqls().get(sqlId);
			
			SqlDataSelectionsHandler lwDef = AppBean.listyWartosciHM.get(sqlId);
			if( lwDef == null ) { //|| ( timestampHelper.getTimestamp().after ( AppBean.listyWartosciTSHM.get(sqlId)) )
				lwDef = loadDBDefinitionOfLW(sqlId);
				sqlData = null;
			}
			
			if(sqlData == null) {//clone def
				sqlData = new SqlDataSelectionsHandler();
				//sqlData.ds = getDs(lwDef.getDefLst().getLstDataSource()); //getDs(); //(new AppBean()).getDs();
				sqlData.setSql(lwDef.getSql());
				sqlData.setShortName(lwDef.getShortName());
				sqlData.setName(lwDef.getName());
				sqlData.setColNames(lwDef.getColNames());
				sqlData.setColHeaders(lwDef.getColHeaders());
				sqlData.setColWidths(lwDef.getColWidths());
				sqlData.setColConvertPatternHM((HashMap<String, String>) lwDef.getColConvertPatternHM().clone());
				sqlData.setCacheDuration(15*60000); //cache'owanie na 15 min...
				sqlData.clearFilter();
				sqlData.setKonsolidacja(lwDef.getKonsolidacja());
				sqlData.setCzyPdf(lwDef.getCzyPdf());
				sqlData.setCzyXls(lwDef.getCzyXls());
				sqlData.setCzyCsv(lwDef.getCzyCsv());
				sqlData.setCzyXml(lwDef.getCzyXml());
				sqlData.getColWidths800600().addAll(lwDef.getColWidths800600());
				
				sqlData.setLst(lwDef.getLst());
				sqlData.setColumnsHM(lwDef.getColumnsHM());
				sqlData.pkColumnName = lwDef.pkColumnName;
				
				sqlData.setDependencyTablesCHM(lwDef.getDependencyTablesCHM());
				this.getSqls().put(sqlId, sqlData);
			}
			
			
			if (params instanceof Map) 
				sqlData.setSqlNamedParams((Map<String, Object>) params);
			else
				sqlData.setSqlParams((ArrayList<Object>) params);
			
		} catch(Exception ex){
			if (!ex.getMessage().contains("Sesja zakończona przez Administratora"))
				log.error(ex.getMessage(), ex);
			else {
				log.debug("loadEgListaWartosci / " + sqlId + " / -> Sesja zakończona przez Administratora ");
			}
			
			if (ex.getMessage().contains("Brak listy wartości o nazwie ")){
				throw new RuntimeException(ex.getMessage());
			}
		}
		AppBean.logMethod(methodname, t0);
		return "";
	}
	
	
	public PptAdmListyWartosci getLWDef(String sqlId){
		if (sqlId==null) return null;
		
		PptAdmListyWartosci ret = null;
		try(HibernateContext h = new HibernateContext()){
			List lst = HibernateContext.query(h, "from PptAdmListyWartosci where LST_LP=1 and upper(LST_NAZWA) = ?0", sqlId.toUpperCase());
			if(lst.isEmpty()) return null;
			ret = (PptAdmListyWartosci) lst.get(0);
			ret.getPptAdmListyWartoscis().size(); //LAZY!
			ret.getPptAdmListyTabele().size(); //LAZY!
		}
		return ret;
	}
	
	
	private SqlDataSelectionsHandler loadDBDefinitionOfLW(String sqlId) throws Exception {
		String sqlIdU = sqlId.toUpperCase();
		
		String sql = "";
		
		SqlDataSelectionsHandler sqlData = new SqlDataSelectionsHandler();
		//sqlData.ds = getDs();
		
		PptAdmListyWartosci lwDef = this.getLWDef(sqlIdU);
		if (lwDef==null) {  //proba doczytania brakujacej definicji z pliku lw.<NAZWA_LW>.sql próba dodania LW na podstawie skryptu sl.<NAZWA_LW>.sql wewn. pliku war (katalog /WEB-INF/protectedElements/SQL/dmp_lw/)
			Resource resource = Utils.getResource("/WEB-INF/protectedElements/SQL/dmp_lw/lw." + sqlIdU + ".sql");
			if (resource.exists()){
				JdbcUtils.executeSqlScript(resource, "Cp1250"); //				org.springframework.jdbc.datasource.init.ScriptUtils.executeSqlScript(conn,resource);
				User.warn("Listy wartości: wykonano skrypt sl." + sqlIdU + ".sql");
			}
			lwDef = this.getLWDef(sqlIdU);
		}else {
			//TODO: wypada sprawdzic wersje definicji lw i jesli w bazie lw jest starsza niz w pliku war to wysłac ostrzeżenie albo oznaczyc lw do aktualizacji ...
		}


		if (lwDef==null) {
			User.alert("Brak listy wartości o nazwie '" + sqlIdU + "'");
			throw new Exception("Brak listy wartości o nazwie '" + sqlIdU + "'");
		}
		
		List<PptAdmListyWartosci> lst = new ArrayList<>();// HibernateContext.query("from PptAdmListyWartosci where upper(LST_NAZWA) = ? order by LST_LP", sqlIdU );
		lst.addAll(lwDef.getPptAdmListyWartoscisEtykietyAll());
		
		sqlData.setLst(lst);
		PptAdmListyWartosci x = lst.get(0);
		
		sqlData.setName(x.getLstOpis());
		sql = replaceEgParamsPOLE_N(x.getLstQuery());
		sqlData.setSql(sql);
		sqlData.setKonsolidacja("T".equals(x.getLstKonsoliduj()));
		sqlData.setCzyPdf("T".equals(x.getLstCzyPDF()));
		sqlData.setCzyXls("T".equals(x.getLstCzyXLS()));
		sqlData.setCzyCsv("T".equals(x.getLstCzyCSV()));
		sqlData.setCzyXml("T".equals(x.getLstCzyXML()));
		sqlData.pkColumnName = x.getLstNazwaPola();
		
		for (PptAdmListyWartosci et : lst) {
			String cname = (""+et.getLstNazwaPola()).toLowerCase();
			
			if (!"T".equals(et.getLstFUkryte())){
				sqlData.getColNames().add(cname);
				sqlData.getColHeaders().add(et.getLstEtykieta());
				sqlData.getColWidths().add(""+et.getLstDlugosc());
				sqlData.getColWidths800600().add(""+et.getLstDlugosc800600());
			}
			
			if (et.getLstLbCodeEtykieta()!=null)
				sqlData.getColConvertPatternHM().put(cname, et.getLstLbCodeEtykieta());  //opcjonalny format wyswietlania danych - np. "#0.00" dla liczb dziesiętnych
			
			sqlData.getColHeadersHM().put(cname.toLowerCase(), et.getLstEtykieta());
			sqlData.getColumnsHM().put(cname, et);
		}

		
		sqlData.setShortName(sqlId);
		if (sqlData.getName()==null || sqlData.getName().equals("")) 
			sqlData.setName("SqlBean-"+sqlId);
		
		AppBean.listyWartosciHM.put(sqlId, sqlData);
		return sqlData;
	}
	
	
	
	
//	public String clob2string(Clob clb) throws IOException, SQLException {
//		return Utils.asString(clb);
//	}
	

	public String loadEgListaWartosciDefaultParams(String sqlId){
		sqlId = sqlId.toLowerCase();
		SqlDataSelectionsHandler sqlData = this.getSqls().get(sqlId);
		
		if (sqlData==null){
			String sql = "";
			
			sqlData = new SqlDataSelectionsHandler();
			//sqlData.ds = getDs();
			
			try (Connection conn = JdbcUtils.getConnection()){
				String sqlIdU = sqlId.toUpperCase();
				
				CachedRowSet crsDef = JdbcUtils.sqlExecQuery(conn, "select lst_query, lst_opis from PPV_ADM_LISTY_WARTOSCI where lst_lp = 1 and lst_nazwa = ?  ", sqlIdU);
				while (crsDef.next()) {
					Clob clob = crsDef.getClob(1);
					String string = clob.toString();
					sql = Utils.asString(clob) ; //crsDef.getString(1); od przejscia na CLOB nie stosujemy tu zwyklego stringa!!!!
					sqlData.setName(  (""+crsDef.getString(2)).trim() );
				}
				sql = replaceEgParamsPOLE_N(sql);

				CachedRowSet crs = JdbcUtils.sqlExecQuery(conn, "select lst_etykieta, lst_dlugosc, lst_nazwa_pola, lst_f_ukryte from PPV_ADM_LISTY_WARTOSCI where lst_nazwa = ? order by lst_lp", sqlIdU);
				sqlData.getColNames().clear();
				
				while (crs.next()){
					
					String header = crs.getString(1);
					String width = crs.getString(2);
					String cname = crs.getString(3);
					String ukryte = crs.getString(4);
					
					if (!"T".equals(ukryte)){
						sqlData.getColNames().add(cname);
						sqlData.getColHeaders().add(header);
						sqlData.getColWidths().add(width);
					}
				}
				
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
			
			sqlData.setSql(sql);
			int count = StringUtils.countMatches(sql, "?");
			ArrayList<Object> params = new ArrayList<Object>();
			for(int i=0; i<count; i++){
				params.add("%");
			}
			sqlData.setShortName(sqlId);
			if (sqlData.getName()==null || sqlData.getName().equals("")) sqlData.setName("SqlBean-"+sqlId);
			sqlData.setSqlParams(params);
			sqlData.reLoadData();
			sqlData.setCacheDuration(15*60000); //cache'owanie na 15 min...
			this.getSqls().put(sqlId.toLowerCase(), sqlData);
		}
 		
		return "";
	
	}
	
	
	public static String replaceEgParamsPOLE_N(String sql) {
		Pattern pattern = Pattern.compile("\\:POLE_+\\d*");
		Matcher m = pattern.matcher(sql);
		int numberOfQuestionMarks = 1;
		while (m.find()) {
			String s = m.group(0);
			sql = sql.replaceAll(s, new String(new char[numberOfQuestionMarks]).replace("\0", "?"));
		}
		return sql;
	}
	
	public static String replaceEgParamsPOLEN(String sql) {
		Pattern pattern = Pattern.compile("\\:POLE+\\d*");
		Matcher m = pattern.matcher(sql);
		int numberOfQuestionMarks = 1;
		while (m.find()) {
			String s = m.group(0);
			sql = sql.replaceAll(s, new String(new char[numberOfQuestionMarks]).replace("\0", "?"));
		}
		return sql;
	}




	public StreamedContent slAsPdf(String slNazwa) throws IOException, DocumentException {
		byte[] bytes = ReportGenerator.generujSlElToPdfData(slNazwa);
		return Utils.asStreamedContent(bytes, "application/pdf", "testLwListRpt.pdf");
	}


	public SqlDataSelectionsHandler getSL(String slownikId){
		slownikId = slownikId.toLowerCase();
		SqlDataHandler s = this.getSqls().get(slownikId);
		if (s==null){
			loadSlownik(slownikId);
			s = this.getSqls().get(slownikId);
		}
		return (SqlDataSelectionsHandler) s;
	}


	public String loadSlownik(String sqlId) { //, boolean pokazWartosc, boolean sortujWartosc){

		sqlId = (""+sqlId).toLowerCase();
		ArrayList<Object> params = new ArrayList<>();
		params.add(sqlId.toUpperCase());

		String sql = this.getLW("PPL_SLOWNIK", sqlId).getSql();

		loadSqlData(sqlId, sql, params);

		try {
			if (this.getLW(sqlId).getData().isEmpty()) {//jesli w bazie nie ma danych nt. wskazanego przez sqlId słownika->próba dodania słownika na podstawie skryptu sl.<NAZWA_SLOWNIKA>.sql wewn. pliku war (katalog /WEB-INF/protectedElements/SQL/dmp_sl/)
				Resource resource = Utils.getResource("/WEB-INF/protectedElements/SQL/dmp_sl/sl." + sqlId.toUpperCase() + ".sql");
				if (resource.exists()){
					JdbcUtils.executeSqlScript(resource, "Cp1250"); //				org.springframework.jdbc.datasource.init.ScriptUtils.executeSqlScript(conn,resource);
					User.warn("Słowniki: wykonano skrypt sl." + sqlId.toUpperCase() + ".sql");
				}
				loadSqlData(sqlId, sql, params);
			} else {
				//TODO: są dane ale wypada sprawdzic wersje danych slownika i jesli w bazie slownik jest starszy niz w pliku war to wysłac ostrzeżenie albo oznaczyc słownik do aktualizacji ...
			}
		} catch (Exception e) {
			User.alert(e);
//			log.error(e.getMessage(), e);
		}


		try {
			this.getLW(sqlId).setDependencyTablesCHM(this.getLW("PPL_SLOWNIK", sqlId).getDependencyTablesCHM());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}


//		loadSqlData(sqlId,
//				" select "
//						+ "   S.RV_LOW_VALUE      "
//						+ "	, S.RV_MEANING     "
//						+ " , S.RV_HIGH_VALUE"
//						+ " , S.RV_ABBREVIATION "
//						+ " , S.RV_TYPE "
//						+ " from  PPV_REF_CODES s"
//						+ " where lower(RV_DOMAIN) = ? "
//						+ (sortujWartosc ? " order by S.RV_LOW_VALUE " :" order by S.RV_MEANING ")
//				, params);


//		if (pokazWartosc){
//			this.getSql(sqlId).setColNames("rv_low_value, rv_meaning");
//			this.getSql(sqlId).setColHeaders("wartość, opis");
//			this.getSql(sqlId).setColWidths("150,  350");
//		}else{
//			this.getSql(sqlId).setColNames("rv_meaning");
//			this.getSql(sqlId).setColHeaders("opis");
//			this.getSql(sqlId).setColWidths("500");
//		}


		String sl_opis = wczytajEgSlownikOpis(sqlId);
		if (sl_opis!=null)
			this.getSql(sqlId).setName(sl_opis);

		SqlDataSelectionsHandler lwDef =AppBean.listyWartosciHM.get("PPL_SLOWNIK");
		if(lwDef==null) {
			try {
				lwDef = loadDBDefinitionOfLW("PPL_SLOWNIK");
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
		this.getSql(sqlId).setColNames(lwDef.getColNames());
		this.getSql(sqlId).setColHeaders(lwDef.getColHeaders());
		this.getSql(sqlId).setColWidths(lwDef.getColWidths());

		return "";
	}


	public SqlDataSelectionsHandler getSlownik(String sqlid) {
		return getSlownik(sqlid, true, false);
	}

	public SqlDataSelectionsHandler getSlownik(String sqlid, boolean pokazWartosc) {
		return getSlownik(sqlid, pokazWartosc, false);
	}
	
	public SqlDataSelectionsHandler getSlownik(String sqlId, boolean pokazWartosc, boolean sortujWartosc) {
		sqlId = sqlId.toLowerCase();
		SqlDataHandler s = this.getSqls().get(sqlId);
		if (s==null){
			loadEgSlownik(sqlId, pokazWartosc, sortujWartosc);
			s = this.getSqls().get(sqlId);
		}
		return (SqlDataSelectionsHandler) s;
	}
	
	
	public String loadEgSlownik(String sqlId){
		return loadEgSlownik(sqlId, true, false);
	}
	
	public String loadEgSlownik(String sqlid, boolean pokazWartosc) {
		return loadEgSlownik(sqlid, pokazWartosc, false);
	}
	
	public String loadEgSlownik(String sqlId, boolean pokazWartosc, boolean sortujWartosc){

		sqlId = (""+sqlId).toLowerCase();
		ArrayList<Object> params = new ArrayList<>();
		params.add(sqlId);
		loadSqlData(sqlId, 
				      " select "
				    + "   S.RV_LOW_VALUE      "
				    + "	, S.RV_MEANING     "
				    + " , S.RV_HIGH_VALUE"
					+ " , S.RV_ABBREVIATION "
					+ " , S.RV_TYPE "
					+ " from  PPV_REF_CODES s"
					+ " where lower(RV_DOMAIN) = ? "
					+ (sortujWartosc ? " order by S.RV_LOW_VALUE " :" order by S.RV_MEANING ")
					, params);
		

		if (pokazWartosc){
			this.getSql(sqlId).setColNames("rv_low_value, rv_meaning");
			this.getSql(sqlId).setColHeaders("wartość, opis");
			this.getSql(sqlId).setColWidths("150,  350");
		}else{
			this.getSql(sqlId).setColNames("rv_meaning"); 
			this.getSql(sqlId).setColHeaders("opis");
			this.getSql(sqlId).setColWidths("500");
		}		
		
		
		String sl_opis = wczytajEgSlownikOpis(sqlId);
		if (sl_opis!=null)
			this.getSql(sqlId).setName(sl_opis);
		
		return "";
	}
		

	
	private String wczytajEgSlownikOpis(String sqlId) {
		String sl_opis = null;
		try(Connection conn = JdbcUtils.getConnection()){
			sl_opis = ""+JdbcUtils.sqlExecScalarQuery(conn, "select sl_opis from  PPADM.PPT_ADM_SLOWNIKI where   sl_nazwa =  ? ", sqlId.toUpperCase());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return sl_opis;
	}

	
	public String loadSqlData(String sqlId, ArrayList<Object> params){
		sqlId = sqlId.toLowerCase();
		SqlDataSelectionsHandler sqlData = this.getSqls().get(sqlId);
		SqlDataHandler sqlDef = AppBean.slowniki.get(sqlId);
		if (sqlDef==null){
			System.err.println("AppBean.slowniki -> BRAK DEFINICJI DLA SQLID: " + sqlId);
			return "";
		}
		
		if (sqlData==null || !sqlData.getSqlParams().toString().equals(params.toString()) ){
			sqlData = new SqlDataSelectionsHandler();
			//sqlData.ds = getDs();
			sqlData.setSql(sqlDef.getSql());
			sqlData.setShortName(sqlId);
			sqlData.setName("SqlBean-"+sqlId);
			sqlData.setSqlParams(params);
			sqlData.reLoadData();
			sqlData.setCacheDuration(15*60000); //cache'owanie na 15 min...
			this.getSqls().put(sqlId.toLowerCase(), sqlData);
		}
 		
		sqlData.setSqlParams(params);
		sqlData.setSql(sqlDef.getSql());//def. mogla sie zmienic
		
		return "";
	}	
	
	
	
	public SqlDataSelectionsHandler getSql(String sqlId, String sql){
		loadSqlData(sqlId, sql, new ArrayList<>()) ;
		return this.getSql(sqlId);
	}
	
	public SqlDataSelectionsHandler getSql(String sqlId, String sql, Object... params){
		ArrayList<Object> params2 = new ArrayList<>();
		for (Object p : params) {
			params2.add(p);
		}
		loadSqlData(sqlId, sql, params2 ) ;
		return this.getSql(sqlId);
	}

	

	public static SqlDataHandler getSqlData(String sql, Object... params) {
		ArrayList<Object> pars = new ArrayList<Object>();
     	pars.addAll( Arrays.asList(params) );
     	return getSqlData(true, sql, params);
	}

	
	public static SqlDataHandler getSqlData(Boolean setContext, String sql,   Object... params) {
		ArrayList<Object> pars = new ArrayList<Object>();
     	pars.addAll( Arrays.asList(params) );
     	return getSqlData(setContext, sql, pars);
	}
		
	public static SqlDataHandler getSqlData(Boolean setContext, String sql,  ArrayList<Object> params) {
		SqlDataHandler s = new SqlDataHandler();
		//s.setShortName(shortName);
		s.setContextDependent(setContext);
		s.setSql(sql);
		s.setSqlParams(params);
		
		try {
			s.reLoadData();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		return s;
	}
	
	
	
	public String loadSqlData(String sqlId, String sql, Object... params) {
		ArrayList<Object> pars = new ArrayList<Object>();
     	pars.addAll( Arrays.asList(params) );
		return loadSqlData(sqlId, sql, pars);
	}
	
	
	public String loadSqlData(String sqlId, String sql, ArrayList<Object> params){
		if(params==null) params = new ArrayList<>();
		
		sqlId = sqlId.toLowerCase();
		SqlDataSelectionsHandler sqlData = this.getSqls().get(sqlId);
		
		if (sqlData==null || !sqlData.getSqlParams().toString().equals(params.toString()) ){
			sqlData = new SqlDataSelectionsHandler();
			//sqlData.ds = getDs(); //(new AppBean()).getDs();
			sqlData.setSql(sql);
			sqlData.setShortName(sqlId);
			sqlData.setName("SqlBean-"+sqlId);
			sqlData.setSqlParams(params);
			sqlData.reLoadData();
			sqlData.setCacheDuration(15*60000); //cache'owanie na 15 min...
			this.getSqls().put(sqlId.toLowerCase(), sqlData);
		}
 		
		sqlData.setSql(sql);
		sqlData.setSqlParams(params);

		return "";
	}

	public String cacheLoadSqlData(String sqlId, String sql, Object... params){
		ArrayList<Object> pars = new ArrayList<Object>();
     	pars.addAll( Arrays.asList(params) );
		cacheLoadSqlData(sqlId, sql, pars);
		return "";
	}
	
	public String cacheLoadSqlData(String sqlId, String sql, ArrayList<Object> params){
		sqlId = sqlId.toLowerCase();
		
		
		
		SqlDataHandler slApp = AppBean.slowniki.get(sqlId);
		if (slApp==null || !slApp.getSqlParams().toString().equals(params.toString()) ){
			slApp = new SqlDataHandler();
			//slApp.ds = getDs();
			slApp.setSql(sql);
			slApp.setShortName(sqlId);
			slApp.setName("SqlBean-"+sqlId);
			slApp.setSqlParams(params);
			slApp.reLoadData();
			slApp.setCacheDuration(30*60000); //cache'owanie na 10 min...
			AppBean.slowniki.put(sqlId, slApp);			
		}
		
		SqlDataSelectionsHandler sqlData = this.getSqls().get(sqlId);
		if (sqlData==null || !sqlData.getSqlParams().toString().equals(params.toString()) ){
			sqlData = new SqlDataSelectionsHandler();
			//sqlData.ds = getDs(); //(new AppBean()).getDs();
			sqlData.setSql(sql);
			sqlData.setShortName(sqlId);
			sqlData.setName("SqlBean-"+sqlId);
			sqlData.setSqlParams(params);

			sqlData.setTs(-1);
			sqlData.setData((ArrayList<DataRow>) slApp.getData().clone());		
			sqlData.setColumns(slApp.getColumns());
			sqlData.setColumnsTypes(slApp.getColumnsTypes());
			sqlData.pkIndexMap = slApp.pkIndexMap;
			sqlData.setCacheDuration(15*60000); //cache'owanie na 15 min...
			this.getSqls().put(sqlId.toLowerCase(), sqlData);
		}
 		
		sqlData.setSql(sql);
		sqlData.setSqlParams(params);

		return "";
	}
	
	
	public String cacheLoadSqlData(String sqlId){
		sqlId = sqlId.toLowerCase();
		SqlDataHandler sl = AppBean.slowniki.get(sqlId);
		return this.cacheLoadSqlData(sqlId, sl.getSql(), sl.getSqlParams());
	}
	
	
	
	public static Map<String, String> getFieldsetParams( Boolean disabled ) {
	    HashMap<String, String> params = new HashMap<String, String>();
	    if (!disabled) {
	        params.put("disabled", "disabled");
	    }
	    return params;
	}
	
	
	
	public SqlDataSelectionsHandler getSql(String sqlid){
		sqlid = sqlid.toLowerCase();
		return this.getSqls().get(sqlid);
	}
	
	
	public SqlDataSelectionsHandler removeSql(String sqlid){
		sqlid = sqlid.toLowerCase();
		return this.getSqls().remove(sqlid);
	}

	
	public ArrayList<DataRow> getSqlData(String sqlid){
		
		
		sqlid = sqlid.toLowerCase();
		SqlDataHandler sqlData = this.getSqls().get(sqlid);
		if (sqlData!=null){
			
			//System.out.println(sqlData.getData().size());
			
			return sqlData.getData();
		}
		return null;
	}

	

	public DataSource getDs() {
		return JdbcUtils.ds;
	}

	public DataSource getDs(String dataSourceName) {
		if(dataSourceName==null || dataSourceName.equals("")) return getDs();

		DataSource ds = null;
		try {
			ds = (DataSource) (new InitialContext()).lookup("java:comp/env/jdbc/"+dataSourceName);
		} catch (NamingException e) {
			e.printStackTrace();
		}
		return ds;
	}
	
	public boolean isNumeric(String str) {
		return Utils.isNumeric(str);
	}

//	public static double round(double value, int places) {
//		return Utils.round(value, places);
//	}


//	public static Date asDate(LocalDate localDate) {
//		return Utils.asDate(localDate);
//	}

//	public static Date asDate(LocalDateTime localDateTime) {
//		return Utils.asDate(localDateTime); 
//	}

//	public static LocalDate asLocalDate(Date date) {
//		return Utils.asLocalDate(date);
//	}

//	public static LocalDateTime asLocalDateTime(Date date) {
//		return Utils.asLocalDateTime(date);
//	}

//	public static Date trunc(Date date) {
//		return  Utils.trunc(date);
//	}
	
	
	
//	public static String asString(Date d){
//		return asString(d, "yyyy-MM-dd");
//	}
//
//	
//	public static String asString(Date d, String format){
//		if (d == null)
//			return "";
//		else
//			return new SimpleDateFormat(format).format(d); // format = "yyyy-MM-dd"
//	}
	

//	public static Timestamp asSqlTimestamp(Date date) {
//		return Utils.asSqlTimestamp(date);
//	}
	
	
//	public static java.sql.Date asSqlDate(LocalDate localDate) {
//		return Utils.asSqlDate(localDate);
//	}

//	public static java.sql.Date asSqlDate(LocalDateTime localDateTime) {
//		return Utils.asSqlDate(localDateTime);
//	}

//	public static java.sql.Date asSqlDate(Date date) {
//		return Utils.asSqlDate(date);
//	}	
//	
	
	
//	public static java.sql.Date plusDays(Date d, int days){
//		return Utils.plusDays(d, days);
//	}
	
//	public static java.sql.Date plusMonths(Date d, int m){
//		return Utils.plusMonths(d, m);
//	}

//	public static java.sql.Date plusYears(Date d, int y){
//		return Utils.plusYears(d, y);
//	}
	
	public static Date today(){
		return Utils.today();
	}
	
	public static Date now(){
		return Utils.now();
	}


	public LocalDate getNow(){
		return LocalDate.now();
	}

//	public static Date addYears(Date date, int years) {
//		return Utils.addYears(date, years);
//	}
	
//	public static Date addDays(Date date, int days) {
//		return Utils.addDays(date, days);
//	}
	
	
	public Date dateSerial(int year, int month, int day) {
		return Utils.dateSerial(year, month, day);
	}
	
	public  Double zaokraglenieKwotaWgWaluty(Double wartosc, Long walId){
		return Utils.dopelnijDoNominalWalutyNBP(wartosc, walId);
	}

	/**
	 * Zwraca zbiór komponentów bezpośrednio podrzędnych (dzieci) względem
	 * wskazanego. Docelowo taki zbiór jest dalej przeszukiwany rekurencyjnie. Z
	 * uwagi na cc (aby dało się przeitorwać także componenty użyte wewn. el.
	 * composite component ) - w wyniku standardowa kolekcja getFacetsAndChildren
	 * zostaje powiększona o ew. o dziec dla
	 * uic.getFacets().get("javax.faces.component.COMPOSITE_FACET_NAME").
	 * 
	 * @param uic
	 *            - rodzic dla którego zostanie wygenerowana kolekcja dzieci
	 * @return HashSet<UIComponent> : (uic.getFacetsAndChildren() +
	 *         (uic.getFacets().get("javax.faces.component.COMPOSITE_FACET_NAME")).getChildren())
	 */
	public static ArrayList<UIComponent> getFacetsAndChildrenSet(UIComponent uic) {
		ArrayList<UIComponent> uicSet = new ArrayList<UIComponent>();
	
		// jesli to composite component to wynik uic.getChildren() jest pusty,
		// ale za to n/w lista ma elementy
		UIComponent childrenHolderFacet = uic.getFacets().get("javax.faces.component.COMPOSITE_FACET_NAME");
		if (childrenHolderFacet != null) {
			uicSet.addAll(childrenHolderFacet.getChildren());
		}
	
		Iterator<UIComponent> facetsAndChildren = uic.getFacetsAndChildren();
		while (facetsAndChildren.hasNext()) {
			uicSet.add(facetsAndChildren.next());
		}
		return uicSet;
	}
		

	public String formatAsCommaSeparatedNumbersString(List<Long> list){
		return JdbcUtils.formatAsCommaSeparatedNumbersString(list);
	}

	
	
	public void seryjnieProceduraPLSQLDlaZaznaczonych(SqlDataSelectionsHandler lw, String plsqlProcedurename) throws SQLException {
		try(Connection conn = JdbcUtils.getConnection()){

			try {
				for (DataRow r : lw.getSelectedRows()) {
					System.out.println("seryjnieProceduraPLSQLDlaZaznaczonych / " + lw.getShortName() + " / " + plsqlProcedurename + " / " + r.getIdAsString());
					JdbcUtils.sqlSPCall(conn, plsqlProcedurename, r.getIdAsString());	
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
				conn.rollback();
			}
						
		} 
		
		lw.resetTs(); lw.getData();
	}


	public static void redirectTo(String redirectToPage) {
		Utils.redirectTo(redirectToPage);
	}

	
	
	  public static StreamedContent generujSlElToPdfStream(String slownikEl2PdfNazwa) throws IOException, DocumentException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
	    	return ReportGenerator.generujSlElToPdfStream(slownikEl2PdfNazwa, null);
	  }

	  public static StreamedContent generujSlElToPdfStream(String slownikEl2PdfNazwa, String pdffilename) throws IOException, DocumentException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
	    	return ReportGenerator.generujSlElToPdfStream(slownikEl2PdfNazwa, pdffilename);
	  }





	public Object nvl(Object x, Object y){
		if (x!=null) return x;
		return y;
	}


	public Object evaluateExpressionGet(String elExpression) {
		return Utils.evaluateExpressionGet(elExpression);
	}

	public String getStrTS(){
		return Utils.strTS();
	}
}
