package com.comarch.egeria.pp.data;

import com.comarch.egeria.Params;
import com.comarch.egeria.Utils;
import com.comarch.egeria.ApachePOIUtils;
import com.comarch.egeria.pp.data.liquibase.LiquibaseUtils;
import com.comarch.egeria.pp.data.msg.Msg;
import com.comarch.egeria.pp.data.msg.Push;
import com.comarch.egeria.pp.delegacjeStd.model.DelegacjaKraj;
import com.comarch.egeria.pp.zalaczniki.ZalacznikiBean;
import com.comarch.egeria.pp.zfss.model.PptZfssWniosek;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import com.lowagie.text.DocumentException;
import liquibase.exception.LiquibaseException;
import net.sf.jasperreports.engine.JRException;
import oracle.jdbc.OracleTypes;
import org.apache.poi.ooxml.POIXMLDocumentPart;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.*;
import org.hibernate.Metamodel;
import org.hibernate.metamodel.spi.MetamodelImplementor;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTBookmark;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTMarkupRange;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;
import org.w3c.dom.Node;

import javax.annotation.PostConstruct;
import javax.faces.application.ViewExpiredException;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.ManagedType;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

//import org.primefaces.context.RequestContext;

@ManagedBean
@Named
@Scope("view")
public class TestBean extends SqlBean {



	public void testLiquibaseUpdatePPDB() throws SQLException, LiquibaseException {
		LiquibaseUtils.updateDataBasePP();
	}


	public void dmpTableChangelog(String owner, String tablename) throws SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException, IOException {
		LiquibaseUtils.dmpOracleTableChangelog(owner, tablename);
	}



	public void testHQL(){
//		List lst = HibernateContext.query("from PptAdmListyWartosci where LST_LP=1 and upper(LST_NAZWA) = ?0 and '1' = ?1", "PPL_TEST", "0" );
//		List lst = HibernateContext.query("from PptWspPolaDefiniowalne where WPDF_ENCJA_ID=?0 and WPDF_RODZAJ_ENCJI = ?1 and WPDF_FORMULARZ = ?2", 1L, "X", "Y" );
//		List query = HibernateContext.query("from DbObject where OBJP_OWNER = ?0 and OBJP_OBJECT_TYPE = ?1 and OBJP_NAME = ?2 ", "PPADM", "TRIGGER", "PPG_ABL_BUR_LOG");

//		String hql = " FROM PptUrlopyWnioskowane x WHERE x.uprwPrcId = ?0 and ?1 BETWEEN trunc(x.uprwDataOd) and trunc(x.uprwDataDo)  ORDER BY x.uprwDataOd";
//		List lst = HibernateContext.query(hql, user.getPrcIdLong(), Utils.dateSerial(2020,01,31)) ;

		System.out.println();

	}
	
	
	private SqlDataSelectionsHandler currentLovData;
	
	private Object testVal = null;
	private Object testVal1 = "106234";
	private Object testVal2 = null;
	private Object testVal3 = null;
	private Object testVal4 = null;
	private Object testVal5 = null;
	private Object testVal6 = null;


	private Integer testInteger1 = 1;
	private Integer testInteger2 = 2;
	
	
	private Long testLong1;
	private Long testLong2;
	private Long testLong3;
	
	private Date data1;
	private Date data2;
	

	private String kategoriaObiegu;
	private Long idEncji;
	
	
	
	private TestObject testObject = new TestObject();
	
	private List<TestObject> testObjects = new ArrayList<>();

	
	@PostConstruct
	public void init() {
		
		for (int i=0; i<12; i++){
			TestObject o = new TestObject();
			o.setQuantity(i);
			o.setPrice(i*0.1);
			this.getTestObjects().add(o);
		}
		
		this.testObject = this.testObjects.get(0);
		
	}





	public CTBookmark findBookamrkStart(XWPFParagraph paragraph, String bookmarkName) {
		for (CTBookmark ctBookmark : paragraph.getCTP().getBookmarkStartList()) {
			if (eq(bookmarkName,ctBookmark.getName())){
				return ctBookmark;
			}
		}
		return null;
	}

	public CTBookmark findBookamrkStart(XWPFTable table, String bookmarkName) {
		for (CTBookmark ctBookmark : table.getCTTbl().getBookmarkStartList()) {
			if (eq(bookmarkName,ctBookmark.getName())){
				return ctBookmark;
			}
		}
		return null;
	}

	public CTMarkupRange findBookamrkEnd(XWPFParagraph paragraph, CTBookmark bookmark) {
		if (bookmark==null) return null;

		for (CTMarkupRange x : paragraph.getCTP().getBookmarkEndList()) {
			if ( eq(bookmark.getId().intValue(), x.getId().intValue()) ){
				return x;
			}
		}
		return null;
	}

	public CTMarkupRange findBookamrkEnd(XWPFTable paragraph, CTBookmark bookmark) {
		if (bookmark==null) return null;

		for (CTMarkupRange x : paragraph.getCTTbl().getBookmarkEndList()) {
			if ( eq(bookmark.getId().intValue(), x.getId().intValue()) ){
				return x;
			}
		}
		return null;
	}


	public void testApachePOIInstrukcjaPPSplitBodyElements(String bookmarkName) throws IOException {


		//POC - wyciąganie ze wskazanego dokuemntu DOCX częsci oznaczonej zakładką (np. o nazwie "obiegi")
		//docelowo sposób generowania pomocy kontekstowej, gdzie nazwa zakładki to jednoczesnie mapowanie okreslonego pliku xhtml

		if (bookmarkName==null || bookmarkName.isEmpty()) return;
//		bookmarkName = "obiegi";

		String folderDOC = "W:";//TODO - podmienic na docelowy
		Path pthInstrukcjaUzytkownika = Paths.get(folderDOC, "Instrukcja PP.docx");
		Path pthPlikPomocKontekstowa = Paths.get(folderDOC, "test.docx");
		ApachePOIUtils.testApachePOIInstrukcjaPPSplitBodyElements(pthInstrukcjaUzytkownika,bookmarkName);

		if(1==1) return;

		XWPFDocument doc = null;
		CTBookmark bookmarkStart = null;
		CTMarkupRange bookmarkEnd = null;
		IBodyElement firstBookmarkedBe = null;
		IBodyElement lastBookmarkedBe = null;
		List<IBodyElement> bookmarkedBodyElements = new ArrayList<>();


		List<Integer> bodyElementsPositiosToRemove = new ArrayList<>();

		try {

			doc = new XWPFDocument(new FileInputStream(pthInstrukcjaUzytkownika.toFile()));

			List<IBodyElement> bodyElemetsToSave = new ArrayList<>();
			List<IBodyElement> bodyElementsToRemove = new ArrayList<>();

			//////////////////////////////////////wyznaczanie pozycji oraz selekcja el.:////////////////////////////////
			int pos = 0;
			for (IBodyElement be : doc.getBodyElements()) {

				if (be instanceof XWPFParagraph) {
					XWPFParagraph p = (XWPFParagraph) be;
					if (bookmarkStart==null) bookmarkStart = findBookamrkStart(p, bookmarkName);
					if (bookmarkEnd==null) bookmarkEnd = findBookamrkEnd(p, bookmarkStart);
				} else if (be instanceof XWPFTable) {
					XWPFTable tb = (XWPFTable) be;
					if (bookmarkStart==null) bookmarkStart = findBookamrkStart(tb, bookmarkName);
					if (bookmarkEnd==null) bookmarkEnd = findBookamrkEnd(tb, bookmarkStart);
				}

				if (firstBookmarkedBe==null && bookmarkStart!=null) firstBookmarkedBe = be;

				if (firstBookmarkedBe!=null && lastBookmarkedBe==null){
					bodyElemetsToSave.add(be);
					bookmarkedBodyElements.add(be);
				} else {
					bodyElementsToRemove.add(be);
					bodyElementsPositiosToRemove.add(0,pos);
				}

				if (lastBookmarkedBe==null && bookmarkEnd!=null) lastBookmarkedBe = be;

				pos+=1;
			}

			if (lastBookmarkedBe==null) return;


			//////////////////////////////////////kasowanie zbednych czesci dokuemntu://////////////////////////////////
			for (Integer i : bodyElementsPositiosToRemove) {
				doc.removeBodyElement(i);
			}



			//////////////////////////////////////czyszczenie poczatkowego i koncowego paragrafu:///////////////////////
			if (firstBookmarkedBe instanceof XWPFParagraph) {
				final XWPFParagraph par = (XWPFParagraph) firstBookmarkedBe;
				removeAllRunsBeforeBookmark(par, bookmarkStart);
				if (par.getRuns().isEmpty()){
					doc.removeBodyElement(doc.getPosOfParagraph(par));
				}
			}

			if (lastBookmarkedBe instanceof XWPFParagraph) {
				final XWPFParagraph par = (XWPFParagraph) lastBookmarkedBe;
				removeAllRunsAfterBookmark(par, bookmarkEnd);
				if (par.getRuns().isEmpty()){
					doc.removeBodyElement(doc.getPosOfParagraph(par));
				}
			}


			//////////////////////////////////////czyszczenie naglowkow i stopek:///////////////////////////////////////
			clearDocxHeadersAndFooters(doc);


			//////////////////////////////////////kasowanie spisu tresci itp....:///////////////////////////////////////
			doc.getDocument().getBody().getSdtList().clear();  //ew. doc.enforceUpdateFields();->aktualizacja spisu treści (i formul)





			////////////////////////////////zbieranie info o obrazkach, ktore sa uzywane:///////////////////////////////
			List<String> usedPicturesNames = new ArrayList<>();
			List<String> usedPicturesEmbedRelationsIds = new ArrayList<>();
			List<PackagePart> usedPuctersPackageParts = new ArrayList<>();

			for (XWPFParagraph p : doc.getParagraphs()) {
				for (XWPFRun run : p.getRuns()) {
					final List<XWPFPicture> embeddedPictures = run.getEmbeddedPictures();
					for (XWPFPicture pic : embeddedPictures) {

						final String embed = pic.getCTPicture().getBlipFill().getBlip().getEmbed(); //np. rId60 - id relationship
						usedPicturesEmbedRelationsIds.add(embed);

						final PackagePart packagePart = doc.getRelationById(embed).getPackagePart();
						usedPuctersPackageParts.add(packagePart);

						final String fileName = pic.getPictureData().getFileName();
						usedPicturesNames.add(fileName);
					}
				}
			}


			//////////////////////////////// kasowanie obrazków, które nie są używane://////////////////////////////////
			final List<PackagePart> parts = doc.getPackage().getParts().stream().collect(Collectors.toList());
			for (PackagePart part : parts) {
				final String contentType = part.getContentType();
				if (contentType.startsWith("image") && !usedPuctersPackageParts.contains(part)){
					if (!part.toString().contains("image1.jpeg")) {//z jakiegos powodu image1.jpeg jest potrzebny (ale jest bardzo mały...)
						doc.getPackage().removePart(part);
					}
				}
			}



			///////////////////////////////zapis do pliku//////////////////////////////////////////
			final FileOutputStream os = new FileOutputStream(pthPlikPomocKontekstowa.toString());
			doc.write(os);
			os.flush();
			os.close();

		} catch (Exception e) {
			User.alert(e);
		} finally {
			if (doc != null) doc.close();
		}
	}















	public void removeAllRunsBeforeBookmark(XWPFParagraph p, CTBookmark bookmarkStart) {
		List<Node> nodesToRemove = new ArrayList<>();

		Node pn = bookmarkStart.getDomNode();
		while (pn!=null){
			nodesToRemove.add(pn);
			pn = pn.getPreviousSibling();
		}

		final List<XWPFRun> runs = p.getRuns().stream().collect(Collectors.toList());
		for (XWPFRun run : runs) {
			final Node n = run.getCTR().getDomNode();
			if (nodesToRemove.contains(n)) {
			 	p.removeRun(p.getRuns().indexOf(run));
			}
		}
	}

	public void removeAllRunsAfterBookmark(XWPFParagraph p, CTMarkupRange bookmarkEnd) {
		List<Node> nodesToRemove = new ArrayList<>();

		Node nn = bookmarkEnd.getDomNode();
		while (nn!=null){
			nodesToRemove.add(nn);
			nn = nn.getNextSibling();
		}

		final List<XWPFRun> runs = p.getRuns().stream().collect(Collectors.toList());
		for (XWPFRun run : runs) {
			final Node n = run.getCTR().getDomNode();
			if (nodesToRemove.contains(n)) {
				p.removeRun(p.getRuns().indexOf(run));
			}
		}
	}


	public void clearDocxHeadersAndFooters(XWPFDocument doc) {

		Set<PackagePart> packagePartsToRemove = new HashSet<>();

		for (XWPFHeader header : doc.getHeaderList()) {
			for (POIXMLDocumentPart.RelationPart r : header.getRelationParts()) {
				final PackagePart packagePart = r.getDocumentPart().getPackagePart();//kasowanie obrazków
				packagePartsToRemove.add(packagePart);
			}
			header.clearHeaderFooter();
		}

		for (XWPFFooter footer : doc.getFooterList()) {
			for (POIXMLDocumentPart.RelationPart r : footer.getRelationParts()) {
				final PackagePart packagePart = r.getDocumentPart().getPackagePart();//kasowanie obrazków
				packagePartsToRemove.add(packagePart);
			}
			footer.clearHeaderFooter();
		}

		for (PackagePart packagePart : packagePartsToRemove) {
			doc.getPackage().removePart(packagePart); //kasowanie obrazków używanych w nagłówkach i stopkach
		}


	}


	public String testViewExpiredException(){
		throw new ViewExpiredException();
	}


	LocalDateTime testLocalDateTime = LocalDateTime.now();

	public LocalDateTime getTestLocalDateTime() {
		return testLocalDateTime;
	}

	public void setTestLocalDateTime(LocalDateTime testLocalDateTime) {
		this.testLocalDateTime = testLocalDateTime;
	}



	Date testDate = new Date();// Utils.dateSerial(1990,1,31);

	public Date getTestDate() {
		return testDate;
	}

	public void setTestDate(Date testDate) {
		System.out.println("setTestDate .... "+testDate);
		this.testDate = testDate;
	}



	public void testGetLW(){
		SqlDataSelectionsHandler lw = this.getLW("PPL_TEST_1234567");
		int size = lw.getData().size();
		System.out.println();
	}

	public void testGetSL(){
		SqlDataSelectionsHandler sl = this.getSL("TEST");
		int size = sl.getData().size();
		System.out.println();
	}


	public void iterateHibernate(){
		HibernateContext h = new HibernateContext();//			ClassMetadata classMetadata = h.getSessionFactory().getClassMetadata(c);
		Metamodel metamodel = h.getSessionFactory().getMetamodel();

//		Set<EntityType<?>> entities = metamodel.getEntities(); //136
//		Set<EmbeddableType<?>> embeddables = metamodel.getEmbeddables(); //1 //Uwaga StanPK jest dostepny jako @Embeddable (to nie jest Entity)
//		Set<ManagedType<?>> managedTypes = metamodel.getManagedTypes(); // 137

		Set<EntityType<?>> entities = metamodel.getEntities();
		for (ManagedType<?> entity : entities) {
			Class<?> javaType = entity.getJavaType();
			iterateHibernateClassTableAndColumns(h, javaType);
		}

	}


	private void iterateHibernateClassTableAndColumns(HibernateContext h, Class<?> c) {
		System.out.println("\n\n\n");
//		HibernateContext h = new HibernateContext();//			ClassMetadata classMetadata = h.getSessionFactory().getClassMetadata(c);
		Metamodel metamodel = h.getSessionFactory().getMetamodel();


		AbstractEntityPersister abstractEntityPersister = (AbstractEntityPersister) ((MetamodelImplementor) metamodel).entityPersister(c);

		String tableName = abstractEntityPersister.getTableName();
		System.out.println(c.getCanonicalName()  + " <=> " + tableName + "\n--------------------------------------------------------------------");

		for (String propertyName : abstractEntityPersister.getPropertyNames()) {
			String[] fldColNames = abstractEntityPersister.getPropertyColumnNames(propertyName);
			if (fldColNames!=null && fldColNames.length>0) {
				System.out.println(propertyName + " <-> " + String.join("; " , fldColNames));
			}
		}
		System.out.println("\n\n\n");
	}










	public void testAddLW2JRXML() throws IOException {

		String sqlId = "PPL_KS_TEL_PWSZ";
		LinkedHashMap<String, Map<String, Object>> listyWartosci = new LinkedHashMap<String, Map<String, Object>>();

		//Map<String, Object> sqlNamedParams = new HashMap<>();
		//sqlNamedParams.put("POLE_1", 101236);
		//ReportGenerator.lWtoRaport(sqlId, sqlNamedParams);
		
		listyWartosci.put("sqlId", null);
		
		ReportGenerator.lWStoJRXML(listyWartosci);
	}





	public void testPush(){
		Push.push("ADMINISTRATOR_PORTAL",""+ LocalDateTime.now(),"OK","INFO");
		User.info("test");
	}


	public void testSetColFilter(){
		try {
			System.out.println(SqlDataSelectionsHandler.checkContainsWithRegex("{0}", "abc(12345)[6789]{0}"));
		} catch (Exception ex) {
			System.out.println(ex);
		}
	}


	public void testLwPodwojneDane(){
		SqlDataSelectionsHandler lw = this.getLW("PPL_KONTEKST_PRACY");
//		lw.resetTs();
//		System.out.println(lw.getData().size());

		new ThreadTestLwPodwojneDane(lw).start();
		new ThreadTestLwPodwojneDane(lw).start();
		new ThreadTestLwPodwojneDane(lw).start();

	}

	 class ThreadTestLwPodwojneDane extends Thread {

		 SqlDataSelectionsHandler lw;
		public ThreadTestLwPodwojneDane(SqlDataSelectionsHandler lw){
			this.lw = lw;
		}

		public  void run(){
			lw.resetTs();
			System.out.println(lw.getData().size());
		}
	 }






	public void testKursyWalut(){

		Date naDzien = Utils.dateSerial(2019, 10, 10);
		KursWalutySredniNBP kursWalutySredniNBP = new KursWalutySredniNBP(50000L, naDzien);
		KursWalutySprzedazyNBP kursWalutySprzedazyNBP = new KursWalutySprzedazyNBP(50000L, naDzien);

		System.out.println();
	}

	public void testExportXLSX(SqlDataSelectionsHandler lw) throws IOException {

		XSSFWorkbook wb = lw.exportDataAsXlsxWorkbook();

		//zapis dopliku:
		FileOutputStream fileOut = new FileOutputStream("w:\\"+lw.getShortName()+".xlsx");
		wb.write(fileOut);
		wb.close();
		fileOut.flush();
		fileOut.close();
		
		System.out.println();
	}
//
//	public static void writeXLSX(ExampleSet exampleSet, String sheetName, String dateFormat, String numberFormat,
//								 OutputStream outputStream, OperatorProgress opProg) throws WriteException, IOException,
//			ProcessStoppedException {
//		// .xlsx files can only store up to 16384 columns, so throw error in case of more
//		if (exampleSet.getAttributes().allSize() > 16384) {
//			throw new IllegalArgumentException(I18N.getMessage(I18N.getErrorBundle(),
//					"export.excel.excel_xlsx_file_exceeds_column_limit"));
//		}
//
//		try {
//			XSSFWorkbook workbook = new XSSFWorkbook();
//
//			Sheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName(sheetName));
//			dateFormat = dateFormat == null ? DEFAULT_DATE_FORMAT : dateFormat;
//
//			numberFormat = numberFormat == null ? "#.0" : numberFormat;
//
//			writeXLSXDataSheet(workbook, sheet, dateFormat, numberFormat, exampleSet, opProg);
//			workbook.write(outputStream);
//		} finally {
//			outputStream.flush();
//			outputStream.close();
//		}
//	}



	public StreamedContent dmpAllObjectsAllSource(){
		SqlDataSelectionsHandler lw = getLW("ALL_OBJECTS");
		if (lw==null || lw.getSelectedRows().isEmpty()){
			return null;
		}

		if (lw.getSelectedRows().size()==1) {
			DataRow r = lw.getSelectedRows().get(0);
			String owner = r.getAsString("owner");
			String objectName = r.getAsString("object_name");
			String objectType = r.getAsString("object_type");
			return dmpAllObjectsAllSource2SqlFile(owner, objectName, objectType);
 		}

		if (lw.getSelectedRows().size()>1) {
			return dmpAllObjectsAllSource2ZipFile(lw.getSelectedRows());
		}

		return null;
	}


	public StreamedContent dmpAllObjectsAllSource2ZipFile(List<DataRow> rows)  {

		try (Connection conn = JdbcUtils.getConnection()){

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(baos);

			for (DataRow r : rows) {

				String owner = r.getAsString("owner");
				String objectName = r.getAsString("object_name");
				String objectType = r.getAsString("object_type");

				String dmlTxt = JdbcUtils.getDbSourceCode(conn, owner, objectName, objectType, false, false, true, true);

				Utils.addToZipFile(dmlTxt.getBytes(), owner + "." + objectName + "." + objectType + ".sql", zos);

			}

			zos.close();
			ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray() );

			String now = Utils.format(new Date(), "yyyy-MM-dd.HH.mm.ss");
			String fname = "dmp("+rows.size()+")." + now + ".zip";
			return new DefaultStreamedContent(bis , "text/plain", fname);

		} catch (Exception e) {
			e.printStackTrace();
			User.alert(e);
		}

		return null;
	}


	public StreamedContent dmpAllObjectsAllSource2SqlFile(String owner, String objectName, String objectType){

		try (Connection conn = JdbcUtils.getConnection()){

			String dmlTxt = JdbcUtils.getDbSourceCode(conn, owner, objectName, objectType, false, false, true, true);
			String fname = owner + "." + objectName + "." + objectType + ".sql";
			ByteArrayInputStream is = new ByteArrayInputStream( dmlTxt.getBytes() );
			return  new DefaultStreamedContent(is, "text/plain", fname);

		} catch (Exception e) {
			e.printStackTrace();
			User.alert(e);
		}

		return null;
	}

















	public void testUpdateFindComponetsByClass(String cssClass){
//		Utils.updateCssClassComponents(null, cssClass); ZLE
	}


    public StreamedContent testGenerujSlElToPdfData(String slownikEl2PdfNazwa) throws IOException, DocumentException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        //SqlDataSelectionsHandler sl = this.getSL("TEST_EL2PDF");
        this.sqlDataCache.loadTBTS();
        byte[] bytes = ReportGenerator.generujSlElToPdfData(slownikEl2PdfNazwa);
        return Utils.asStreamedContent(bytes, "application/pdf", "testLwListRpt.pdf");
    }


    public StreamedContent testLwListAsPdf() throws IOException, DocumentException {
        //SqlDataSelectionsHandler sl = this.getSL("TEST_EL2PDF");
        byte[] bytes = ReportGenerator.generujSlElToPdfData("TEST_EL2PDF");
        return Utils.asStreamedContent(bytes, "application/pdf", "testLwListRpt.pdf");


//        String slOpis = sl.getName();
//
//        for (DataRow r : sl.getData()) {
//            String wslOpis = (String) r.get("WSL_OPIS");
//            Object x = Utils.evaluateExpressionGet(wslOpis);
//            System.out.println(x);
//        }


//        List<SqlDataSelectionsHandler> lws = new ArrayList<>();
//		//Object o = Utils.evaluateExpressionGet("#{testBean.now()}");
//		lws.add(this.getLW("PPL_TEST"));
//		lws.add(this.getLW("PPL_TEST_WB"));
//
//		lws.add(this.getLW("PPL_TEST"));
//		lws.add(this.getLW("PPL_TEST_WB"));
//
//		byte[] bytes = ReportGenerator.asPdfData(lws);
//		return Utils.asStreamedContent(bytes, "application/pdf", "testLwListRpt.pdf");
	}


	public void testExportPdf() throws IOException, DocumentException {

/*		SqlDataSelectionsHandler lw = this.getLW("PPL_TEST");
		Document pdf = new Document();

		com.lowagie.text.pdf.PdfWriter pdfWriter = PdfWriter.getInstance (pdf, new FileOutputStream("test_lw_export2.pdf"));
		pdf.open();

		lw.preProcessPDF(pdf);

    	int i=0;

        List<PptAdmListyWartosci> pdfCols = lw.getLst().stream().filter(c -> nz(c.getLstDlugosc800600()) > 0L).collect(Collectors.toList());
                
        PdfPTable tb = new PdfPTable(pdfCols.size());

        Font cellFont = FontFactory.getFont(FontFactory.TIMES, "cp1250", 10);
        Font facetFont = FontFactory.getFont(FontFactory.TIMES, "cp1250", 10, Font.BOLD);
    	
        float[] widths = new float[pdfCols.size()];
        for (PptAdmListyWartosci col : pdfCols){
            widths[i] = nz(col.getLstDlugosc800600());
            i++;
        }
    	tb.setWidths(widths);
    	tb.setWidthPercentage(100f);
           	   	
    	for (PptAdmListyWartosci col : pdfCols){
    		PdfPCell celll = new PdfPCell(new Phrase(new Paragraph(col.getLstEtykieta(), facetFont)));
    		tb.addCell(celll);
    	}
    	
    	
    	for (DataRow r : lw.getData()) {

          	for (PptAdmListyWartosci col : pdfCols){
          		
          		String colName = col.getLstNazwaPola();
          		
          		Object x = r.get(colName);
          		
				String txt = r.getFormattedValue(colName);
                //Cell cell = new Cell(new Phrase(txt));
                PdfPCell cell = new PdfPCell(new Phrase(new Paragraph(txt, cellFont)));
                 cell.setVerticalAlignment(Element.ALIGN_MIDDLE);

                 if (x instanceof  Number)
                     cell.setHorizontalAlignment(Element.ALIGN_RIGHT);                 
                 //cell.
                 tb.addCell(cell);
        	}

    		
    	}
    	
      	for (PptAdmListyWartosci col : pdfCols){
      		
            String aggregation = nz(lw.agregationClass(col.getLstNazwaPola()));
            Object aggValue = lw.agregation(col.getLstNazwaPola());

            PdfPCell cell = new PdfPCell();

            if (aggValue!=null) {

                if ("min".equals(aggregation)) aggregation="min.:";
                else if ("max".equals(aggregation)) aggregation="maks.:";
                else if ("sum".equals(aggregation)) aggregation="suma:";
                else if ("avg".equals(aggregation)) aggregation="śr.:";
                else if ("count".equals(aggregation)) aggregation="zlicz:";

                Paragraph p1 = new Paragraph( aggregation, cellFont);
                p1.setAlignment(Element.ALIGN_LEFT);

                Paragraph p2 = new Paragraph("" + aggValue, facetFont);
                p2.setAlignment(Element.ALIGN_RIGHT);

                cell.addElement(p1);
                cell.addElement(p2);

            }

            tb.addCell(cell);
    	}

        pdf.add(tb);	
		pdf.close();
		pdfWriter.close();*/
		
		//kod dodaje plik pdf z wynikiem listem wartosci jak załacznik do delegacji o id = 350
		SqlDataSelectionsHandler lw = this.getLW("PPL_TEST");
		ZalacznikiBean.LWJakoPdfZapiszZalacznik(lw, null, "ZAL_WND_ID", 350, "testLWtoPDF");
	}
	



	public void testRpt2Zal() throws SQLException, IOException, JRException {
		//wygenerowanie raportu wniosku i rozliczenia i podpięcia ich jako zal do wniosku
		Params params = new Params("wnd_id", 312L);//this.dodajRaportyDoZal();
		ZalacznikiBean.generujRaportPdfZapiszJakoZalacznik("Delegacja_krajowa_polecenie", params, "ZAL_OKUL_ID", 100 ,"raport polecenia");
	}



	public void testhibernaterefresh() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {

        Long id = 5l;

        PptZfssWniosek w = (PptZfssWniosek) HibernateContext.get(PptZfssWniosek.class, id);

        JdbcUtils.sqlExecNoQuery(" update ppt_zfss_wniosek set zfs_numer = null where zfs_id = " + id);
        System.out.println("lm wniosek: "+ JdbcUtils.sqlExecScalarQuery(" select zfs_audyt_lm from ppt_zfss_wniosek where zfs_id = " +id) );
        JdbcUtils.sqlExecNoQuery(" update ppt_zfss_poreczyciele set zfsp_opis = null where zfsp_id = 7 ");
        System.out.println("lm poreczyciel: " +  JdbcUtils.sqlExecScalarQuery(" select zfsp_audyt_lm from ppt_zfss_poreczyciele where zfsp_id = 7"));

        HibernateContext.refresh(w);

        System.out.println();




		System.out.println();
	}


	
	public double getTestObjectsTotalValue(){
		double ret = this.testObjects.stream().mapToDouble(x->x.getValue()).sum();
		return ret; 
	}
	
	
	
	public void ustawObieg(String kategoria, Long idEncji){

		try (Connection conn = JdbcUtils.getConnection()) {// iCloseable->potencjalny
			JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_kategorie", this.kategoriaObiegu);
			JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_id_encji", this.idEncji);
		} catch (Exception e) {
			e.printStackTrace();
			if (e.getMessage().contains("Użytkownik nie ma uprawnień"))
			User.alert("Brak uprawnień do usunięcia danych obiegu.");
			return;
		}
	}
	
	
	
	
	
	
	public List<Msg> getMsgList(){
		List<Msg> ret = new ArrayList<>();
		for (long i = 0; i<=5; i++){
			Msg p = new Msg();
			p.setWdm_id(""+i);
			p.setTekst("Ala ma kota "+ i);
			p.setTytul("Ala "+i);
			ret.add(p);
		}
		return ret;
		
	}
	
	
	private boolean testDisabled = true;
	
	
	
	private List<TestObject> testList =  new ArrayList<>();
		
	
	public void testInfo(){
		User.info("testsaf asldjdjas glas");
	}
	

	public void testWarn(){
		User.warn("testsaf asldjdjas glas");
	}
	
	public void testAlert(){
		User.alert("testsaf asldjdjas glas");
	}

	
	
//	public void testMSG() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
//
//		try (Connection con = JdbcUtils.getConnection()){
//
//			String sql = ""
//					+ "begin"
//					+ "  PPADM.MSG('http://10.132.216.41:8080/PortalPracowniczy/msg.xhtml', 'ADMINISTRATOR_PORTAL', 'Temat...', 'Tekst...',null, 'INFO');"
//					+ "end;";
//
//			JdbcUtils.sqlExecNoQuery(con, sql );
//
//		} catch(Exception ex){
//			System.out.println(ex.toString());
//		}
//
//	}
	
	
	//przepakowane z klasy do Beaná zeby dalo sie wyswietlic zawartosc
	public List<JdbcConnInfo> activeConneInfos(){
		return JdbcUtils.activeConnections;
	}



	////////////////////////////////////////////////////////////////////////////////////////////////////////
	DelegacjaKraj delegacja = null;

	public void test1DelegacjaKraj(){
		if (this.delegacja==null)
			this.delegacja = (DelegacjaKraj) HibernateContext.get(DelegacjaKraj.class, 340l);

        System.out.println(this.delegacja.gettersInfo(false));
	}
    ////////////////////////////////////////////////////////////////////////////////////////////////////////




//	public void test1() {
//
//
//
//		AppBean.listyWartosciHM.remove("ppl_del_zaangazowanie_zf");
//		this.sqlDataCache.sqls.remove("ppl_del_zaangazowanie_zf");
//
//		SqlDataSelectionsHandler lw = this.getLW("ppl_del_zaangazowanie_zf");
//		int size = lw.getData().size();
//		System.out.println(size);
//
//
//
////		System.out.println(SqlDataSelectionsHandler.checkContainsWithRegex("x*ki", "8 opolskie 2000-12-05 00:00:00.0 ")); //false?
////		System.out.println(SqlDataSelectionsHandler.checkContainsWithRegex("c*z", "Cesiu"));//false?
////		System.out.println(SqlDataSelectionsHandler.checkContainsWithRegex("c*z", "Czesławz")); //true?
////
////		System.out.println();
//	}
	

	
	
	public void testUpdateRow(DataRow r, Object rowIdx){
		System.out.println(rowIdx);
		
		 r.set("osoba", ""+new Date().getTime());
		
		 List<UIComponent> uicList = getAllUIComponents(null).stream().filter(uic -> r.getSqlData().equals( uic.getAttributes().get("datasource") )).collect(Collectors.toList());
		 
		 for (UIComponent uic : uicList) {
			 
			 DataTable tb = (DataTable) uic.findComponent("dane");
			 com.comarch.egeria.Utils.update(tb.getClientId());
			 
			 
//			 ArrayList<UIComponent> uicList1 = getAllUIComponents(tb);
			 
//			 String id = tb.getClientId()  + ":" + rowIdx + ":col:4.cell";
//			 System.out.println(id);
//			 
//			 RequestContext.getCurrentInstance().update( id ); //frmCenter:clkptbltest1:dane:3
//			 
//			 RequestContext.getCurrentInstance().update("frmCenter:clkptbltest1:dane:1:col:0:cell");
//			 RequestContext.getCurrentInstance().update("frmCenter:clkptbltest1:dane:1:col:1:cell");
//			 RequestContext.getCurrentInstance().update("frmCenter:clkptbltest1:dane:1:col:2:cell");
//			 RequestContext.getCurrentInstance().update("frmCenter:clkptbltest1:dane:1:col:3:cell");
//			 RequestContext.getCurrentInstance().update("frmCenter:clkptbltest1:dane:1:col:4:cell");
//			 RequestContext.getCurrentInstance().update("frmCenter:clkptbltest1:dane:1:col:5:cell");
//			 RequestContext.getCurrentInstance().update("frmCenter:clkptbltest1:dane:1:col:6:cell");
//			 RequestContext.getCurrentInstance().update("frmCenter:clkptbltest1:dane:1:col:7:cell");
//			                                           //frmCenter:clkptbltest1:dane:5:col:4.cell
			 
			 System.out.println();
		 }
		 
		 System.out.println();
	}
	
	public void testIterateAndUpdateByClass(UIComponent root){
		
		if (root==null)
			root = FacesContext.getCurrentInstance().getViewRoot();

		Iterator<UIComponent> children = root.getFacetsAndChildren();
		
		while (children.hasNext()) {
			UIComponent uic = children.next(); 
			
			if ("clkptbltest1".equals(uic.getId())){
	
				Map<String, Object> passThroughAttributes = uic.getPassThroughAttributes();
			
				Object datasourceObject = uic.getAttributes().get("datasource");
				
				System.out.println();
			}
			
			
			testIterateAndUpdateByClass(uic);
		}
	}
	
	
	
//	public ArrayList<UIComponent> getAllUIComponents(UIComponent root){
//		ArrayList<UIComponent> ret = new ArrayList<>();
//		
//		if (root==null)
//			root = FacesContext.getCurrentInstance().getViewRoot();
//
//		Iterator<UIComponent> children = root.getFacetsAndChildren();
//		
//		while (children.hasNext()) {
//			UIComponent uic = children.next(); 
//			ret.add(uic);
//			ret.addAll( getAllUIComponents(uic) );
//		}
//		
//		return ret;
//	}
	
	
	
	
	public void testJdbc(){
		
		
		JdbcOutParameter pInOut = new JdbcOutParameter(OracleTypes.VARCHAR);
		
		try (Connection conn = JdbcUtils.getConnection())  {
			
			Long valOfTest1 = JdbcUtils.sqlFnLong(conn, "test1", 10, 3, pInOut);
			System.out.println("fn returned: "+  valOfTest1);
			
			System.out.println("out param value: " + pInOut.getValue());
			
			
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		
		
	}
	
//	public void onTypDietChanged(javax.faces.event.ValueChangeEvent e){
//		this.setTestVal1("zmiana: " + e.getOldValue() + " -> " + e.getNewValue());
//		System.out.println("onTypDietChanged....");
//	}

	
	
	
	public void setupLov(){
		DataRow r = this.getSql("egSlowniki").getCurrentRow();
		System.out.println(r);
		String sl_nazwa = ""+ r.get("sl_nazwa");
		System.out.println(sl_nazwa);
		this.loadEgSlownik(sl_nazwa);
		this.setCurrentLovData(this.getSql(sl_nazwa));
	}
	
	public void showSelectedRow(){
		System.out.println("=================================TestBean.showSelectedRow===================================");
		SqlDataSelectionsHandler p = this.getSql("pracownicy");
		if (p!=null){
			System.out.println(p.getName() +  " / currentRow: " + p.getCurrentRow());
			System.out.println(p.getName() +  " / selectedRows" + p.getSelectedRows());
		}
		System.out.println("============================================================================================");
	}

	
	
	public String log(Object testVal) {
//		System.out.println("testBean.log: " + testVal);
		return "";
	}	
	
	public Object getTestVal() {
//		System.out.println("getTestVal:" + testVal);
		return testVal;
	}

	public void setTestVal(Object testVal) {
		System.out.println("setTestVal=" + testVal);
		this.testVal = testVal;
	}
	
	
	public String testActionFn(){
		System.out.println("...testActionFn...");
		return "";
	}
	
	public void testAction(){
		UIComponent component = UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());
		//	ValueExpression ve = component.getValueExpression("value");
		System.out.println("...testAction..." + component);

        User.alert("Ola ma rybki...<h3>Lorem ipsum dolor sit amet </h3>\n consectetur adipiscing elit.\nProin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. Quisque semper justo at risus. Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci. Nam congue, pede vitae dapibus aliquet, elit magna vulputate arcu, vel tempus metus leo non est. Etiam sit amet lectus quis est congue mollis. Phasellus congue lacus eget neque. Phasellus ornare, ante vitae consectetuer consequat, purus sapien ultricies dolor, et mollis pede metus eget nisi. Praesent sodales velit quis augue. Cras suscipit, urna at aliquam rhoncus, urna quam viverra nisi, in interdum massa nibh nec erat.");
        User.warn("ALA MA KOTA<h1>Lorem ipsum dolor sit amet</h1> sectetur adipiscing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. Quisque semper justo at risus. Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci. Nam congue, pede vitae dapibus aliquet, elit magna vulputate arcu, vel tempus metus leo non est. Etiam sit amet lectus quis est congue mollis. Phasellus congue lacus eget neque. Phasellus ornare, ante vitae consectetuer consequat, purus sapien ultricies dolor, et mollis pede metus eget nisi. Praesent sodales velit quis augue. Cras suscipit, urna at aliquam rhoncus, urna quam viverra nisi, in interdum massa nibh nec erat.");
        User.info("ELA MA PSA \n\n<h1>Lorem ipsum dolor sit amet </h1>\n consectetur adipiscing elit.\n\n Proin nibh augue, suscipit a, scelerisque sed, lacinia in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus pharetra nulla ac diam. Quisque semper justo at risus. Donec venenatis, turpis vel hendrerit interdum, dui ligula ultricies purus, sed posuere libero dui id orci. Nam congue, pede vitae dapibus aliquet, elit magna vulputate arcu, vel tempus metus leo non est. Etiam sit amet lectus quis est congue mollis. Phasellus congue lacus eget neque. Phasellus ornare, ante vitae consectetuer consequat, purus sapien ultricies dolor, et mollis pede metus eget nisi. Praesent sodales velit quis augue. Cras suscipit, urna at aliquam rhoncus, urna quam viverra nisi, in interdum massa nibh nec erat.");
	}
	
	public void testActionListener(ActionEvent event) {
		System.out.println("...testActionListener..." + event);
	}
	
	public String testAction(Object x){
		System.out.println("...testAction..." + x);


		if (x instanceof SqlDataSelectionsHandler){
			SqlDataSelectionsHandler lw = (SqlDataSelectionsHandler) x;
			System.out.println(lw.getColNames());
			for (String c : lw.getColNames()) {
				System.out.println(lw.getColHeader(c));
			}
		}

		return "";
	}
	
	
	public String testAction2(Object x){
		System.out.println("...testAction2..." + x);
		return "";
	}	
	
	public void testAction1(){
		
		this.testVal1 = (new Date()).getTime() - 1505381020000L - 21438465400L ;
		System.out.println("...testAction1..." + this.testVal1);
	}
	
	
	public static String show(Object x){
		System.out.println("testBean.show: " +  x );
		return "";
	}
	
	
	public static void show(SqlDataHandler data){
		if (data==null)
			return;
		
		System.out.println("showData: "+ data.getShortName());
		
		for (DataRow r : data.data) {
			System.out.println(""+ r);
		}
		
		System.out.println();
	}
	
	
	public void testValueChangeListener(ValueChangeEvent event){
		UIComponent component = event.getComponent();
		Object oldValue = event.getOldValue();
		Object newValue = event.getNewValue();
		System.out.println(	  "\n----------------------------------------------------------------------------------------------------"
	 						+ "\nTestBean.testValueChangeListener / " +  component 
	 						+ "\n" + oldValue + " -> "  + newValue 
	 						+ "\n----------------------------------------------------------------------------------------------------");
	}


	public SqlDataSelectionsHandler getCurrentLovData() {
		return currentLovData;
	}

	public void setCurrentLovData(SqlDataSelectionsHandler currentLovData) {
		this.currentLovData = currentLovData;
	}


	public long getTime(){
		return (new Date()).getTime();
	}
	
	
	
	
	
	
	
	public void iterateChildrenTest( UIComponent uic ) throws IOException{
		FacesContext ctx = FacesContext.getCurrentInstance();
		
		if (uic == null)
			uic = ctx.getViewRoot();
			//uic = UIComponent.getCurrentComponent(ctx);
		
		
		Map<String, Object> attributes = uic.getAttributes();
		for (Entry<String, Object> e : attributes.entrySet() ) {
			System.out.println(uic.getId() + " - ->  " +  e.getKey()  + " -> " + e.getValue()) ;
			if ("kuid".equals( e.getKey() )){
				
			}
			
		}
		
		
//		for (String k: uic.getAttributes().keySet()){
//			uic.attr
//			
//			System.out.println(k + " ---> " + uic.getAttributes().get(k));
//		}
		
		
	
//		if (uic instanceof LayoutPane && "lpContent".equals( uic.getId() )){
//			LayoutPane p = (LayoutPane) uic;
//			CommandButton btn = new CommandButton();
//			btn.setValue("....TODO: ku center pane test.....");
//			btn.setIcon("save");
//			btn.setStyle("");
//			btn.setOnclick("alert('12345')");
//			p.getChildren().add(btn);
//		}
		
		
	 
//		System.out.println("------------------------------------------------------------------\n"+uic.getClientId()+"\n------------------------------------------------------------------");
//		System.out.println("uic:" + uic);
//		System.out.println("------------------------------------------------------------------\n");
//		System.out.println();
//		System.out.println();
//		System.out.println();
//		System.out.println();
		
		HashSet<UIComponent> al = new HashSet<UIComponent>();
		//al.addAll(uic.getChildren());
		
//		UIComponent childrenHolderFacet = uic.getFacets().get("javax.faces.component.COMPOSITE_FACET_NAME");
//		if (childrenHolderFacet!=null){
//			al.addAll(childrenHolderFacet.getChildren());
//		}
				
//		if (uic instanceof Column){
		Iterator<UIComponent> facetsAndChildren = uic.getFacetsAndChildren();
		while (facetsAndChildren.hasNext()){
			al.add(facetsAndChildren.next());
		}
//		}

		for (UIComponent c:al){
			iterateChildrenTest(c);
		}
	}
	
	
	
	
	
	
	HashSet<UIComponent> kuComponents = new HashSet<>();
	
	public void iterateChildren( UIComponent uic ) throws IOException{
		FacesContext ctx = FacesContext.getCurrentInstance();
		
		if (uic == null)
			uic = ctx.getViewRoot();
		
		
		if (uic.getAttributes().get("kuid")!=null){
			kuComponents.add(uic);
		}
		
//		for (Entry<String, Object> e : uic.getAttributes().entrySet() ) {
//			//System.out.println(uic.getId() + " - ->  " +  e.getKey()  + " -> " + e.getValue()) ;
//			if ("kuid".equals( e.getKey() )						){
//				kuComponents.add(uic);
//			}
//		}
		
		HashSet<UIComponent> al = new HashSet<UIComponent>();
		Iterator<UIComponent> facetsAndChildren = uic.getFacetsAndChildren();
		while (facetsAndChildren.hasNext()){
			al.add(facetsAndChildren.next());
		}

		for (UIComponent c:al){
			iterateChildren(c);
		}
	}

	
	
	
	
	Date t0 = new Date();
	public void logTime(Object info){
		Date t1 = new Date();
		System.out.println( t1 + "; +" + (t1.getTime() - t0.getTime()) + " [ms] " + info );
		t0=t1; 
	}



	public boolean isTestDisabled() {
		return testDisabled;
	}



	public void setTestDisabled(boolean testDisabled) {
		this.testDisabled = testDisabled;
	}



	public List<TestObject> getTestList() {
		if (testList.isEmpty()){
			for (int i =0 ; i<5; i++){
				TestObject o = new TestObject();
				o.setName("TestObject-" +i);
				o.setId(i);
				o.setVersion("v."+i);
				testList.add(o);
			}
		}
		return testList;
	}



	public void setTestList(List<TestObject> testList) {
		this.testList = testList;
	}


	public Date getData1() {
		return data1;
	}


	public void setData1(Date data1) {
		System.out.println(this.data1 +" -> "+ data1);
		this.data1 = data1;
		
	}


	public Date getData2() {
		return data2;
	}


	public void setData2(Date data2) {
		System.out.println(this.data2 +" -> "+ data2);
		this.data2 = data2;
	}




	public String getKategoriaObiegu() {
		return kategoriaObiegu;
	}




	public void setKategoriaObiegu(String kategoriaObiegu) {
		this.kategoriaObiegu = kategoriaObiegu;
	}




	public Long getIdEncji() {
		return idEncji;
	}




	public void setIdEncji(Long idEncji) {
		this.idEncji = idEncji;
	}






	public TestObject getTestObject() {
		return testObject;
	}






	public void setTestObject(TestObject testObject) {
		this.testObject = testObject;
	}





	public List<TestObject> getTestObjects() {
		return testObjects;
	}





	public void setTestObjects(List<TestObject> testObjects) {
		this.testObjects = testObjects;
	}


	public Integer getTestInteger1() {
		System.out.println("getTestInteger1: "+testInteger1);
		return testInteger1;
	}


	public void setTestInteger1(Integer testInteger1) {
		System.out.println("setTestInteger1: " + this.testInteger1 + " -> " + testInteger1);
		this.testInteger1 = testInteger1;
	}


	public Integer getTestInteger2() {
		System.out.println("getTestInteger2: "+testInteger2);
		return testInteger2;
	}


	public void setTestInteger2(Integer testInteger2) {
		System.out.println("setTestInteger2: " + this.testInteger2 + " -> " + testInteger2);
		this.testInteger2 = testInteger2;
	}


	public Long getTestLong1() {
		return testLong1;
	}


	public void setTestLong1(Long testLong1) {
		System.out.println("setTestLong1: " + this.testLong1 + " -> " + testLong1);
		if (!eq(this.testLong1, testLong1)){
			this.setTestLong2(null);
		}
		this.testLong1 = testLong1;
	}


	public Long getTestLong2() {
		return testLong2;
	}


	public void setTestLong2(Long testLong2) {
		System.out.println("setTestLong2: " + this.testLong2 + " -> " + testLong2);
		if (!eq(this.testLong2, testLong2)){
			this.setTestLong3(null);
		}
		this.testLong2 = testLong2;
	}


	public Long getTestLong3() {
		return testLong3;
	}


	public void setTestLong3(Long testLong3) {
		System.out.println("setTestLong3: " + this.testLong3 + " -> " + testLong3);
		this.testLong3 = testLong3;
	}
	
//	public void showSelectedRow(DataRow r){
//		System.out.println("=================================TestBean.showSelectedRow===================================>" + r);
//	}

	public Object getTestVal1() {
		return testVal1;
	}

	public void setTestVal1(Object testVal1) {
		this.testVal1 = testVal1;
		System.out.println("setTestVal1:" + testVal1);
	}

	public Object getTestVal2() {
		return testVal2;
	}

	public void setTestVal2(Object testVal2) {
		this.testVal2 = testVal2;
		System.out.println("setTestVal2: "+ testVal2);
	}

	public Object getTestVal3() {
		return testVal3;
	}

	public void setTestVal3(Object testVal3) {
		this.testVal3 = testVal3;
		System.out.println("setTestVal3: " + testVal3);
	}

	public Object getTestVal4() {
		return testVal4;
	}

	public void setTestVal4(Object testVal4) {
		this.testVal4 = testVal4;
		System.out.println("setTestVal4: " + testVal4);
	}

	public Object getTestVal5() {
		return testVal5;
	}

	public void setTestVal5(Object testVal5) {
		this.testVal5 = testVal5;
		System.out.println("setTestVal5: " + testVal5);
	}

	public Object getTestVal6() {
		return testVal6;
	}

	public void setTestVal6(Object testVal6) {
		this.testVal6 = testVal6;
		System.out.println("setTestVal6: " + testVal6);
	}

}




//			for (IBodyElement be : doc.getBodyElements()) {
//				System.out.println(be);
//				if (!(be instanceof XWPFTable) && !(be instanceof XWPFParagraph)){
//				}
//			}






//			ArrayList<XWPFParagraph> parsToRemove = new ArrayList<>();
//
//			boolean bookmarked = false;
//			String bookMarkId = "";
//
//			for (XWPFParagraph p : doc.getParagraphs()) {
//
//				ArrayList<XWPFRun> runsToRemove = new ArrayList<>();
//
//				for (XWPFRun run : p.getRuns()) {
//
//					Node n = run.getCTR().getDomNode();
//
//					//czy poczatek zakladki:
//					Node pn = n.getPreviousSibling();
//					if (pn != null
//							&& "w:bookmarkStart".equals(pn.getNodeName())
//							&& bookmarkName.equals(pn.getAttributes().getNamedItem("w:name").getNodeValue()) // np. "Zalaczniki"
//					) {
//						bookMarkId = pn.getAttributes().getNamedItem("w:id").getNodeValue();
//						bookmarked = true;
//					}
//
//
//
//					if (bookmarked) {
//						System.out.println(" + " + run);
//					} else {
//						System.out.println(" - " + run);
//						runsToRemove.add(0, run);
//					}
//
//
//
//					//czy koniec:
//					Node nn = n.getNextSibling();
//					if (nn!=null
//							&& "w:bookmarkEnd".equals(nn.getNodeName())
//							&& bookMarkId.equals(nn.getAttributes().getNamedItem("w:id").getNodeValue())
//					){
//						bookmarked = false;
//					}
//
//				}
//
//				for (XWPFRun run : runsToRemove) {
//					CTR ctr = run.getCTR();
//					List<CTDrawing> lst = ctr.getDrawingList();
//					for (int ii = 0; ii < lst.size(); ii++) {
//						ctr.removeDrawing(ii);
//					}
//
//					run.getEmbeddedPictures().clear();
//					p.removeRun(p.getRuns().indexOf(run));
//				}
//
//				if (p.getRuns().isEmpty()){
//					parsToRemove.add(0,p);
//				}
//
//			}
//
//			for (XWPFParagraph p : parsToRemove) {
//				doc.removeBodyElement(doc.getPosOfParagraph(p));
//			}
//
//
//
//			czyscNiepotrzebneElementyDocx(doc);
//
//			final FileOutputStream os = new FileOutputStream("w:\\test.docx");
////			XWPFDocument d1 = new XWPFDocument();
////			for (XWPFParagraph p0 : doc.getParagraphs()) {
////				final XWPFParagraph p1 = d1.createParagraph();
////				cloneParagraph(p0,p1);
////			}
//
////			for (IBodyElement be : doc.getBodyElements()) {
////				System.out.println(be);
////				if (be instanceof XWPFParagraph){
////					final XWPFParagraph p1 = d1.createParagraph();
////					cloneParagraph((XWPFParagraph) be, p1);
////				}
////
////				if (be instanceof XWPFTable){
////					final XWPFTable t1 = d1.createTable();
////					copyTable((XWPFTable)be, t1);
////				}
////			}
//
//
//
//
////			final FileOutputStream os = new FileOutputStream("w:\\test.docx.html");
////			XHTMLConverter.getInstance().convert(doc, os, null);
//
////			final FileOutputStream os = new FileOutputStream("w:\\test.docx");


//		final int pages = doc.getProperties().getExtendedProperties().getUnderlyingProperties().getPages();
//		for (int i=0; i<=pages; i++){
//			XWPFHeader header = doc.getHeaderFooterPolicy().getHeader(i);
//			if (header!=null){
//
//				final List<POIXMLDocumentPart> relations = header.getRelations();
//				final List<POIXMLDocumentPart.RelationPart> relationParts = header.getRelationParts();
//
////				List<PackagePart> packagePartsToRemove = new ArrayList<>();
//
////				for (XWPFPictureData pic1 : header.getAllPictures()) {
////					System.out.println("hdr.picture: " + pic1.getFileName() + " " + pic1.getData().length );
////				}
//				for (XWPFPictureData pic3 : header.getAllPackagePictures()) {
//					System.out.println("hdr.p.picture: " + pic3.getFileName() + " " + pic3.getData().length );
////					packagePartsToRemove.add(pic3.getPackagePart());
////					doc.getPackage().removePart(pic3.getPackagePart());
//				}
//
//
//
////				final List<XWPFParagraph> ptrmv = header.getParagraphs().stream().collect(Collectors.toList());
////				for (XWPFParagraph p : ptrmv) {
////					final List<XWPFRun> runs = p.getRuns().stream().collect(Collectors.toList());
////
////					for (XWPFRun run : runs) {
//////							System.out.println(run.getEmbeddedPictures().size()); run.getEmbeddedPictures().get(0)
////
//////						CTR ctr = run.getCTR();
//////						List<CTDrawing> lst = ctr.getDrawingList();
//////						for (int ii = 0; ii < lst.size(); ii++) {
//////							ctr.removeDrawing(ii);
//////						}
////
//////						while (!run.getCTR().getPictList().isEmpty()) {
//////							run.getCTR().removePict(0);
//////						}
////
//////						run.getEmbeddedPictures().clear();
////						p.removeRun(p.getRuns().indexOf(run));
////					}
////
////					header.removeParagraph(p);
////				}
//
//				header.clearHeaderFooter();//ale obrazki zostają.. i dok. waży dalej prawie 2MB
//			}
//
//
//			final XWPFFooter footer = doc.getHeaderFooterPolicy().getFooter(i);
//			if (footer!=null){
//				final List<XWPFPictureData> allPictures1 = footer.getAllPictures();
//				for (XWPFPictureData pic1 : allPictures1) {
//					System.out.println("ftr.picture: " + pic1.getFileName() + " " + pic1.getData().length );
//				}
//				footer.clearHeaderFooter();
//			}
//		}

//TODO kasowanie niektórych tabel (które sa bezposrednio w body - np. spis tresci)
//		final XWPFTable tb = doc.getTables().get(0);
//		doc.removeBodyElement(doc.getPosOfTable(tb));


//			XWPFDocument d1 = new XWPFDocument();
//
//			for (IBodyElement be : doc.getBodyElements()) {
//				System.out.println(be);
//				if (be instanceof XWPFParagraph){
//					final XWPFParagraph p1 = d1.createParagraph();
//					cloneParagraph((XWPFParagraph) be, p1);
//				}
//
//				if (be instanceof XWPFTable){
//					final XWPFTable t1 = d1.createTable();
//					copyTable((XWPFTable)be, t1);
//				}
//			}
//			d1.write(os);

//			final FileOutputStream os1 = new FileOutputStream("w:\\test.docx.html");
//			XHTMLConverter.getInstance().convert(doc, os1, null);




//	public void testApachePOIInstrukcjaPPSplit(String bookmarkName) throws IOException {
//		if (bookmarkName==null || bookmarkName.isEmpty()) return;
//
////		bookmarkName = "obiegi";
//
//		Path pthTmpl = Paths.get("W:", "Instrukcja_PP.docx");
//
//		XWPFDocument doc = null;
//		XWPFParagraph pstart = null;
//		XWPFParagraph pstop = null;
//		CTBookmark bookmarkStart = null;
//		CTMarkupRange bookmarkEnd = null;
//		List<XWPFParagraph> bookmarkedParagraphs = new ArrayList<>();
//
//		try {
//
//			doc = new XWPFDocument(new FileInputStream(pthTmpl.toFile()));
//
//			List<XWPFParagraph> paragraphs = doc.getParagraphs().stream().collect(Collectors.toList());
//
//			for (XWPFParagraph p : paragraphs) {
//
//				for (CTBookmark ctBookmark : p.getCTP().getBookmarkStartList()) {
//					if (eq(bookmarkName,ctBookmark.getName())){
//						pstart = p;
//						bookmarkStart = ctBookmark;
//					}
//				}
//
//				if (pstart!=null && pstop==null){
//					bookmarkedParagraphs.add(p);
//				}
//
//				for (CTMarkupRange ctMarkupRange : p.getCTP().getBookmarkEndList()) {
//					if (bookmarkStart!=null && eq(bookmarkStart.getId().intValue(), ctMarkupRange.getId().intValue())){
//						pstop = p;
//						bookmarkEnd = ctMarkupRange;
//					}
//				}
//			}
//
//
//
//			if (pstop==null) return;
//
//			removeAllRunsBeforeBookmark(pstart, bookmarkStart);
//			removeAllRunsAfterBookmark(pstop, bookmarkEnd);
//
//			for (XWPFParagraph p : paragraphs) {
//				if (!bookmarkedParagraphs.contains(p) || p.getRuns().isEmpty()){
//					for (XWPFRun run : p.getRuns()) {
//						while (!run.getCTR().getPictList().isEmpty()) {
//							run.getCTR().removePict(0);
//						}
//					}
//					doc.removeBodyElement(doc.getPosOfParagraph(p));
//				}
//			}
//
//
//			clearDocxHeadersAndFooters(doc);
//
////			final List<POIXMLDocumentPart.RelationPart> relationParts1 = doc.getRelationParts();
////			final List<POIXMLDocumentPart> relations1 = doc.getRelations();
////
////			List<PackagePart> pkgpartsToRemove = new ArrayList<>();
////			final List<XWPFPictureData> allPackagePictures = doc.getAllPackagePictures();
////			for (XWPFPictureData pic : allPackagePictures) {
////				System.out.println(pic.getFileName());
////				final PackagePart packagePart = pic.getPackagePart();
////				pkgpartsToRemove.add(packagePart);
//////				final List<POIXMLDocumentPart> relations = pic.getRelations();
//////				final List<POIXMLDocumentPart.RelationPart> relationParts = pic.getRelationParts();
////
////			}
////
////			for (PackagePart pp : pkgpartsToRemove) {
////				doc.getPackage().removePart(pp);
////			}
//
//
//			final FileOutputStream os = new FileOutputStream("w:\\test.docx");
//			doc.write(os);
//			os.flush();
//			os.close();
//
//		} catch (Exception e) {
//			User.alert(e);
//		} finally {
//			if (doc != null) doc.close();
//		}
//	}



//	public void testApachePOIGenerujInstrUzytk(){
//		//TODO wykorzystać Apache POI aby skopiować i wstawić
//		// do pliku _instrukcja_uytkownika_tmpl.docx jako akapity dane plików pomocy kontekstowej
//		// Pliki pomocy kontekstowej mogą być importowane jako akapity wstawiane zamiast tagów postaci
//		//##NazwaPlikuPomocyKontekstowej.docx##
//
//
//		Path pthTmpl = Utils.getAbsolutePath("/WEB-INF/protectedElements/DOC/","_instrukcja_uytkownika_tmpl.docx");
//
//		try {
//
//			XWPFDocument doc = new XWPFDocument(new FileInputStream(pthTmpl.toFile()));
//			List<XWPFParagraph> paragraphs = doc.getParagraphs();
//
//
//			final ArrayList<XWPFParagraph> parsToReplace = new ArrayList<>();
//			for (XWPFParagraph p : paragraphs) {
//				String text = nz(p.getText());
//				if (text.startsWith("##")&&text.endsWith("##")){
//					System.out.println(text);
//					parsToReplace.add(p);
//				}
//			}
//
//
//			for (XWPFParagraph p : parsToReplace) {
//				String text = nz(p.getText());
//				String fname = text.substring(2, text.length() - 2);
//				try {
//					final Path pth1 = Utils.getAbsolutePath("/WEB-INF/protectedElements/DOC/", fname);
//					final XWPFDocument d0 = new XWPFDocument(new FileInputStream(pth1.toFile()));
//
//					for (IBodyElement be : d0.getBodyElements()) {
//						System.out.println(be);
//						final XmlCursor xmlCursor = p.getCTP().newCursor();
//
//						if (be instanceof XWPFParagraph){
//							final XWPFParagraph p1 = doc.insertNewParagraph(xmlCursor);
//							cloneParagraph((XWPFParagraph) be, p1);
//						}
//
//						if (be instanceof XWPFTable){
//							final XWPFTable t1 = doc.insertNewTbl(xmlCursor);
//							copyTable((XWPFTable)be, t1);
//						}
//					}
//
//				} catch (Exception ex) {
//					User.alert(ex);
//				}
//				doc.removeBodyElement(doc.getPosOfParagraph(p));//skasowanie starego paragrafu
//			}
//
//			doc.enforceUpdateFields();//aktualizacja spisu treści?
//			final FileOutputStream os = new FileOutputStream("w:\\instrukcja_uytkownika_test.docx");
//			doc.write(os);
//			os.flush();
//			os.close();
//
//		} catch (Exception e) {
//			User.alert(e);
//		}
//	}


//	//https://stackoverflow.com/questions/48322534/apache-poi-how-to-copy-tables-from-one-docx-to-another-docx
//	private void copyTable(XWPFTable source, XWPFTable target) throws IOException, InvalidFormatException {
//		final CTTblPr t1 = target.getCTTbl().getTblPr();
//		final CTTblPr t0 = source.getCTTbl().getTblPr();
//		target.getCTTbl().setTblPr(source.getCTTbl().getTblPr());
//		target.getCTTbl().setTblGrid(source.getCTTbl().getTblGrid());
//
////		target.getCTTbl().setTblPr(t0);
////
////		target.getCTTbl().setTblGrid(source.getCTTbl().getTblGrid());
////
//		//kopiowanie obramowań tabeli w Word / Projektowanie / Obramowania ...
//		if (t0.isSetTblBorders())t1.setTblBorders(t0.getTblBorders());
//
//		//kopiowanie obramowań ustawianych w Word / Narzędzia główne / Akapit / Obramowanie ...
//		if (t0.isSetTblStyle()) t1.setTblStyle(t0.getTblStyle());
//
////		//???
//		if (t0.isSetTblLayout()) t1.setTblLayout(t0.getTblLayout());
//
//		if (t0.isSetTblOverlap()) t1.setTblOverlap(t0.getTblOverlap());
//
//
//		//		//if ( t0.isSetTblPrChange() ) ... TODO reszta ustawien w stylu isSet...
//
//
//
//
//
//		for (int r = 0; r<source.getRows().size(); r++) {
//			XWPFTableRow targetRow = target.createRow();
//			XWPFTableRow row = source.getRows().get(r);
//			targetRow.getCtRow().setTrPr(row.getCtRow().getTrPr());
//			for (int c=0; c<row.getTableCells().size(); c++) {
//				//newly created row has 1 cell
//				XWPFTableCell targetCell = c==0 ? targetRow.getTableCells().get(0) : targetRow.createCell();
//				XWPFTableCell cell = row.getTableCells().get(c);
//				targetCell.getCTTc().setTcPr(cell.getCTTc().getTcPr());
//
//				XmlCursor cursor = targetCell.getParagraphArray(0).getCTP().newCursor();
//				for (int p = 0; p < cell.getBodyElements().size(); p++) {
//					IBodyElement elem = cell.getBodyElements().get(p);
//					if (elem instanceof XWPFParagraph) {
//						XWPFParagraph targetPar = targetCell.insertNewParagraph(cursor);
//						cursor.toNextToken();
//						XWPFParagraph par = (XWPFParagraph) elem;
//						cloneParagraph(par, targetPar);
//					} else if (elem instanceof XWPFTable) {
//						XWPFTable targetTable = targetCell.insertNewTbl(cursor);
//						XWPFTable table = (XWPFTable) elem;
//						copyTable(table, targetTable);
//						cursor.toNextToken();
//					}
//				}
//				//newly created cell has one default paragraph we need to remove
//				targetCell.removeParagraph(targetCell.getParagraphs().size()-1);
//			}
//		}
//		//newly created table has one row by default. we need to remove the default row.
//		target.removeRow(0);
//	}


//	public static void cloneParagraph(XWPFParagraph from, XWPFParagraph to) throws IOException, InvalidFormatException {
//		CTPPr pPr = to.getCTP().isSetPPr() ? to.getCTP().getPPr() : to.getCTP().addNewPPr();
//		pPr.set(from.getCTP().getPPr());
//		CTSpacing spacing = pPr.isSetSpacing()? pPr.getSpacing() : pPr.addNewSpacing();
//		//spacing.setAfter();
//
//		if (from.getCTP().getPPr()!=null) {
//			final CTSpacing spc = from.getCTP().getPPr().getSpacing();
//			pPr.setSpacing(spc);
//		}
//
//		for (XWPFRun r : from.getRuns()) {
//			XWPFRun nr = to.createRun();
//			cloneRun(r, nr);
//		}
//	}



//	public static void cloneParagraph(XWPFParagraph from, XWPFParagraph to) throws IOException, InvalidFormatException {
//		CTPPr pPr = to.getCTP().isSetPPr() ? to.getCTP().getPPr() : to.getCTP().addNewPPr();
//		pPr.set(from.getCTP().getPPr());
//		CTSpacing spacing = pPr.isSetSpacing()? pPr.getSpacing() : pPr.addNewSpacing();
//		//spacing.setAfter();
//
//		if (from.getCTP().getPPr()!=null) {
//			final CTSpacing spc = from.getCTP().getPPr().getSpacing();
//			pPr.setSpacing(spc);
//		}
//
//		for (XWPFRun r : from.getRuns()) {
//			XWPFRun nr = to.createRun();
//			cloneRun(r, nr);
//		}
//	}