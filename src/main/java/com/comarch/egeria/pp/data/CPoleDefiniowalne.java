package com.comarch.egeria.pp.data;

import com.comarch.egeria.pp.data.model.PptWspPolaDefiniowalne;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.hibernate.Transaction;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.sql.rowset.CachedRowSet;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@ManagedBean
@ViewScoped // @SessionScoped w Dialog Framework
public class CPoleDefiniowalne extends SqlBean {

	String clientId = "";

	PptWspPolaDefiniowalne nowePoleDef = null;

	Map idKlucz = new HashMap<Long, String>();
	Map wartoscEtykieta = new TreeMap<String, String>();
	List listaWartoscEtykieta = new ArrayList<>();


	String rodzajEncji = null;
	Long idEncji = null;
	String nazwaFormularza = null;



	@PostConstruct
	public void init() {

	}

	public static <T, S> List<Map.Entry<T, S>> mapToList(Map<T, S> map) {

		if (map == null) {
			return null;
		}

		List<Map.Entry<T, S>> list = new ArrayList<Map.Entry<T, S>>();
		list.addAll(map.entrySet());

		return list;
	}

	public void pobierzPola(String rodzajEncji, Long idEncji, String nazwaFormularza) {
		this.rodzajEncji = rodzajEncji;
		this.idEncji = idEncji;
		this.nazwaFormularza=nazwaFormularza;

		if (rodzajEncji != null && idEncji != null) {

			String etykieta;
			String wartoscTxt;
			Long idPola;

			try (HibernateContext h = new HibernateContext()) {
				CachedRowSet crs = this.execSqlQuery(
						"select wpdf_id, wpdf_etykieta, wpdf_wartosc_txt from ppt_wsp_pola_definiowalne where"
								+ " wpdf_rodzaj_encji = ? and wpdf_encja_id = ? and wpdf_formularz = ?",
						rodzajEncji, idEncji, nazwaFormularza);

				if (crs != null) {
					while (crs.next()) {
						etykieta = crs.getString("wpdf_etykieta");
						wartoscTxt = crs.getString("wpdf_wartosc_txt");
						idPola = crs.getLong("wpdf_id");
						wartoscEtykieta.put(etykieta, wartoscTxt);
						idKlucz.put(idPola, etykieta);
					}
				}

				listaWartoscEtykieta = mapToList(wartoscEtykieta);

			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			User.alert("Przed dodaniem pól definiowalnych należy zapisać encję");
		}
	}

	public void uzupelnijPola(String rodzajEncji, Long idEncji, String nazwaFormularza) {
		if (rodzajEncji != null && idEncji != null) {

			List<String> listaEtykiet = new ArrayList<String>();

			String etykieta;

			try (HibernateContext h = new HibernateContext()) {
				CachedRowSet crs = this.execSqlQuery(
						"select count(WPDF_ID), WPDF_FORMULARZ, wpdf_etykieta, wpdf_rodzaj_encji from ppt_wsp_pola_definiowalne "
								+ " having wpdf_formularz = ? and wpdf_rodzaj_encji = ? group by  wpdf_formularz, wpdf_etykieta, wpdf_rodzaj_encji",
						nazwaFormularza, rodzajEncji);

				if (crs != null) {
					while (crs.next()) {
						etykieta = crs.getString("wpdf_etykieta");
						listaEtykiet.add(etykieta);
					}
				}

				for (String str : listaEtykiet) {
					if (!wartoscEtykieta.containsKey(str)) {
						dodajPoleDef(rodzajEncji, idEncji, nazwaFormularza);
						nowePoleDef.setWpdfEtykieta(str);
						HibernateContext.saveOrUpdate(nowePoleDef);
						wartoscEtykieta.put(str, "");
						idKlucz.put(nowePoleDef.getWpdfId(), nowePoleDef.getWpdfEtykieta());
//						User.warn("Uzupełniono listę pól dodatkowych o definicję '%1$s'", str); nie zadziala - jest faza (6) renderowanie
					}
				}

				listaWartoscEtykieta = mapToList(wartoscEtykieta);

			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
		return map.entrySet().stream().filter(entry -> Objects.equals(entry.getValue(), value)).map(Map.Entry::getKey)
				.collect(Collectors.toSet());
	}

	public void dodajPoleDef(String rodzajEncji, Long idEncji, String nazwaFormularza) {
		PptWspPolaDefiniowalne poleDef = new PptWspPolaDefiniowalne();
		poleDef.setWpdfRodzajEncji(rodzajEncji);
		poleDef.setWpdfEncjaId(idEncji);
		poleDef.setWpdfFormularz(nazwaFormularza);
		nowePoleDef = poleDef;
	}

	public void dodajNowePoleDef() {
		if (nowePoleDef.getWpdfEtykieta()==null || nowePoleDef.getWpdfEtykieta().isEmpty()){
			User.alert("Etykieta musi być uzupełniona");
			return;
		}
		if (wartoscEtykieta.containsKey(nowePoleDef.getWpdfEtykieta())){
			User.alert("Pole z etykietą '%1$s' już jest dodane.", nowePoleDef.getWpdfEtykieta());
			return;
		}

		HibernateContext.saveOrUpdate(nowePoleDef);
		User.info("Udane dodanie nowego pola");
	}




	public void zapiszPolaDef() {
		try (HibernateContext h = new HibernateContext()) {
            Transaction transaction = h.getSession().beginTransaction();

		    List<PptWspPolaDefiniowalne> lst = HibernateContext.query(h, "from PptWspPolaDefiniowalne where WPDF_ENCJA_ID=?0 and WPDF_RODZAJ_ENCJI = ?1 and WPDF_FORMULARZ = ?2", this.idEncji, this.rodzajEncji, this.nazwaFormularza );
			for (PptWspPolaDefiniowalne p: lst ) {
				p.setWpdfWartoscTxt((String) wartoscEtykieta.get(p.getWpdfEtykieta()));
				h.getSession().saveOrUpdate(p);
			}

            transaction.commit();
			User.info("Udany zapis pól definiowalnych");
		}
	}

	public void usunPoleDef(String klucz, String nazwaFormularza, String rodzajEncji) {
		Iterator idKluczIt = getKeysByValue(idKlucz, klucz).iterator();
		if (idKluczIt.hasNext()) {
			Long kluczId = (Long) idKluczIt.next();

			idKlucz.remove(kluczId);

			try (HibernateContext h = new HibernateContext()) {
				CachedRowSet crs = this.execSqlQuery(
						"select WPDF_ID, WPDF_FORMULARZ, wpdf_etykieta, wpdf_rodzaj_encji, WPDF_ENCJA_ID from ppt_wsp_pola_definiowalne "
								+ " where wpdf_formularz = ? and wpdf_rodzaj_encji = ? and wpdf_etykieta = ?",
						nazwaFormularza, rodzajEncji, klucz);

				if (crs != null) {
					while (crs.next()) {

						Long idPola = crs.getLong("wpdf_id");
						PptWspPolaDefiniowalne poleDef = (PptWspPolaDefiniowalne) HibernateContext.get(PptWspPolaDefiniowalne.class,
								idPola);
						HibernateContext.delete(poleDef);
					}
				}
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}

		}
		wartoscEtykieta.remove(klucz);
	}

	public List getListaWartoscEtykieta() {
		return listaWartoscEtykieta;
	}

	public void setListaWartoscEtykieta(List listaWartoscEtykieta) {
		this.listaWartoscEtykieta = listaWartoscEtykieta;
	}

	public Map getWartoscEtykieta() {
		return wartoscEtykieta;
	}

	public void setWartoscEtykieta(Map wartoscEtykieta) {
		this.wartoscEtykieta = wartoscEtykieta;
	}

	public PptWspPolaDefiniowalne getNowePoleDef() {
		return nowePoleDef;
	}

	public void setNowePoleDef(PptWspPolaDefiniowalne nowePoleDef) {
		this.nowePoleDef = nowePoleDef;
	}


}
