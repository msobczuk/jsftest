package com.comarch.egeria.pp.data.liquibase;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import liquibase.Contexts;
import liquibase.LabelExpression;
import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.ResourceAccessor;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.sql.rowset.CachedRowSet;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import static com.comarch.egeria.Utils.nz;

public class LiquibaseUtils {


    public static void updateDataBasePP() throws LiquibaseException, SQLException {
        System.out.println("LiquibaseUtils.updateDataBasePP START ... ");
        try (Connection conn = AppBean.app.getDataSource().getConnection(false)) {
            ResourceAccessor ra = new ClassLoaderResourceAccessor();
            Database db = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(new JdbcConnection(conn));
            final Liquibase liquibase = new Liquibase("/liquibase/_changelog_master.xml", ra, db);
//			liquibase.setChangeLogParameter("PKEY","PVALUE");
            liquibase.update(new Contexts(), new LabelExpression());
        }
        System.out.println("... LiquibaseUtils.updateDataBasePP END");
    }



    public static String dmpOracleTableChangelog(String owner, String tablename) throws SQLException, IllegalAccessException, ClassNotFoundException, InstantiationException, IOException {

        String xmltb = Utils.getResourceAsString("/WEB-INF/classes/liquibase/_templates/_DMP_TABLE.xml");
        String xmlfktmplt = Utils.getResourceAsString("/WEB-INF/classes/liquibase/_templates/_DMP_TABLE_FK.xml");
        String xmluktmplt = Utils.getResourceAsString("/WEB-INF/classes/liquibase/_templates/_DMP_TABLE_UK.xml");
        String xmlnntmplt = Utils.getResourceAsString("/WEB-INF/classes/liquibase/_templates/_DMP_TABLE_NN.xml");
        String xmlixtmplt = Utils.getResourceAsString("/WEB-INF/classes/liquibase/_templates/_DMP_TABLE_IX.xml");

        String colPrefix = null;

        final HashMap<String, Crs2LqbColumn> colsHM = new HashMap<>();
        final ArrayList<Crs2LqbColumn> addColumns2XML = new ArrayList<>();

        try (Connection conn = JdbcUtils.getConnection()) {



            //wczytywanie def. kolumn z sys.all_tab_cols
            String sql = "" +
                    "select tc.COLUMN_NAME, tc.DATA_TYPE, tc.DATA_LENGTH, tc.DATA_PRECISION, cc.COMMENTS, tc.NULLABLE " +
                    "from SYS.all_tab_cols tc " +
                    " left join SYS.all_col_comments cc on tc.owner = cc.owner and tc.table_name = cc.table_name and tc.column_name = cc.column_name " +
                    "where tc.owner = ? and tc.table_name = ? " +
                    "order by tc.column_id ";

            CachedRowSet crs = JdbcUtils.sqlExecQuery(conn, sql, owner, tablename);
            while (crs.next()){
                final Crs2LqbColumn col = new Crs2LqbColumn(crs);

                if (col.columnName.matches(".+_AUDYT_[A-Z][A-Z]")) {
                    if (col.columnName.endsWith("_AUDYT_UT")){
                        colPrefix = col.columnName.replace("_AUDYT_UT","");
                    }
                } else {
                    addColumns2XML.add(col);
                }

                colPrefix = col.columnName.split("_")[0];
                colsHM.put(col.columnName, col);
            }

            if (addColumns2XML.isEmpty()) return "";


            if (colPrefix==null) colPrefix = addColumns2XML.get(0).columnName.split("_")[0];


            //nazwa i prefiks tabeli
            xmltb = xmltb.replace("_PODAJ_NAZWE_TABELI_", tablename);
            xmltb = xmltb.replace("_PODAJ_PREFIKS_KOLUMN_", colPrefix);



            //komentarz tabeli
            final String tbcomments = nz((String) JdbcUtils.sqlExecScalarQuery(conn, "select comments from SYS.all_tab_comments where owner = ? and table_name = ? ", owner, tablename));
            xmltb = xmltb.replace("@tb_remarks@", tbcomments);



            //wstawianie kolumn (bez audytowych)
            for (Crs2LqbColumn col : addColumns2XML) {
                col.colPrefix = colPrefix;
                xmltb = xmltb.replace("<!--@columns@-->", "            "+col.getXmlAddColumn()+"\r\n<!--@columns@-->");
            }
            xmltb = xmltb.replace("\r\n<!--@columns@-->","");




            //generowanie PK
            ArrayList<Crs2LqbColumn> pkColumnsLst = new ArrayList<>();
            String pkcolumns = "";
            sql =   " select cc.* \n" +
                    " from SYS.all_constraints ac \n" +
                    "  join sys.all_cons_columns cc on ac.constraint_name = cc.constraint_name and cc.table_name = ac.table_name \n" +
                    " where  1=1 \n" +
                    "  and ac.constraint_type = 'P' \n" + //--P = PRIMARY KEY
                    "  and cc.owner = ? and cc.table_name = ? \n" +
                    " order by cc.table_name, cc.position \n" +
                    " ";
            crs = JdbcUtils.sqlExecQuery(conn, sql, owner, tablename);
            while (crs.next()){
                String pkColumn = crs.getString("COLUMN_NAME");
                final Crs2LqbColumn col = colsHM.get(pkColumn);
//                System.out.println(col.getXmlColumnName());
                pkcolumns += ", "+col.getXmlColumnName();
                pkColumnsLst.add(col);
            }
            pkcolumns = pkcolumns.substring(2);
            xmltb = xmltb.replace("@pkcolname@", pkcolumns);







            //generowanie UK
            sql = " select * from sys.all_constraints where owner = ? and table_name = ? and constraint_type = 'U' ";
            final CachedRowSet crsTbUKConstraints = JdbcUtils.sqlExecQuery(conn, sql, owner, tablename);
            while (crsTbUKConstraints.next()){
                String constraintName = crsTbUKConstraints.getString("CONSTRAINT_NAME");
                String sqlConsColums = " select * from SYS.all_cons_columns where constraint_name = ? order by position ";
                final CachedRowSet crsUKColumns = JdbcUtils.sqlExecQuery(conn, sqlConsColums, constraintName);//fk columns

                String columnNames = "";
                while (crsUKColumns.next()){
                    final String columnName = crsUKColumns.getString("COLUMN_NAME");
                    final Crs2LqbColumn col = colsHM.get(columnName);
                    columnNames += ", " + col.getXmlColumnName();
                }
                columnNames = columnNames.substring(2);

                String xmluk = xmluktmplt;
                xmluk = xmluk.replace("@constraintName@", constraintName);
                xmluk = xmluk.replace("@columnNames@", columnNames);
                xmluk = xmluk.replace("@tableName@", tablename);

                xmltb = xmltb.replace("<!--@uk@-->", xmluk+"\r\n<!--@uk@-->");

            }
            xmltb = xmltb.replace("<!--@uk@-->","");




            //NOT NULL constraints
            for (Crs2LqbColumn col : addColumns2XML.stream().filter(c->!c.nullable && !pkColumnsLst.contains(c)).collect(Collectors.toList())) {

                String xmlnn = xmlnntmplt.replace("@columnName@", col.getXmlColumnName());
                xmltb = xmltb.replace("<!--@nn@-->", xmlnn+"\r\n<!--@nn@-->");
//                System.out.println(col.getXmlColumnName());
            }
            xmltb = xmltb.replace("<!--@nn@-->","");



            //generowanie IX (tylko te, których nazwa konczy sie na '%X')
            sql = " select * from SYS.all_indexes where table_owner = ? and table_name = ? and index_name like '%X' ";
            final CachedRowSet crsTbIX = JdbcUtils.sqlExecQuery(conn, sql, owner, tablename);
            while (crsTbIX.next()){
                String indexName = crsTbIX.getString("INDEX_NAME");
                String xmlix = xmlixtmplt;
                xmlix = xmlix.replace("@indexName@", indexName);
                String sqlIndexColums = " select * from SYS.all_ind_columns where index_name = ? order by column_position ";
                final CachedRowSet crsIXColumns = JdbcUtils.sqlExecQuery(conn, sqlIndexColums, indexName);//ix columns
                while (crsIXColumns.next()){

                    final String columnName = crsIXColumns.getString("COLUMN_NAME");
                    final Crs2LqbColumn col = colsHM.get(columnName);

                    xmlix = xmlix.replace("<!--@ixcolumn@-->", String.format("            <column name=\"%1$s\"/>\r\n<!--@ixcolumn@-->", col.getXmlColumnName()) );
                }
                xmlix = xmlix.replace("<!--@ixcolumn@-->","");
                xmltb = xmltb.replace("<!--@ix@-->", xmlix+"\r\n<!--@ix@-->");
            }
            xmltb = xmltb.replace("<!--@ix@-->","");










            //generowanie FK
            sql = " select * from sys.all_constraints where owner = ? and table_name = ? and constraint_type = 'R' ";
            final CachedRowSet crsTbFKConstraints = JdbcUtils.sqlExecQuery(conn, sql, owner, tablename);
            while (crsTbFKConstraints.next()){
                String constraintName = crsTbFKConstraints.getString("CONSTRAINT_NAME");
                String rConstraintName = crsTbFKConstraints.getString("R_CONSTRAINT_NAME");

                String sqlConsColums = " select * from SYS.all_cons_columns where constraint_name = ? order by position ";
                final CachedRowSet crsFKColumns = JdbcUtils.sqlExecQuery(conn, sqlConsColums, constraintName);//fk columns
                final CachedRowSet crsPKColumns = JdbcUtils.sqlExecQuery(conn, sqlConsColums, rConstraintName);//referenced columns

                String baseColumnNames = "";
                String referencedTableCatalogName = "";
                String referencedTableName = "";
                String referencedColumnNames = "";
//                String rColPrefix = "";

                while (crsFKColumns.next() && crsPKColumns.next()){
                    final String fkColumn = crsFKColumns.getString("COLUMN_NAME");
                    final String pkColumn = crsPKColumns.getString("COLUMN_NAME");
                    final Crs2LqbColumn col = colsHM.get(fkColumn);
                    baseColumnNames += ", " + col.getXmlColumnName();
                    referencedColumnNames +=  ", " + pkColumn;

                    referencedTableCatalogName = crsPKColumns.getString("OWNER");
                    referencedTableName = crsPKColumns.getString("TABLE_NAME");
//                    rColPrefix = pkColumn.split("_")[0];
                }
                baseColumnNames = baseColumnNames.substring(2);
                referencedColumnNames = referencedColumnNames.substring(2);

                String xmlfk = xmlfktmplt;
                xmlfk = xmlfk.replace("@constraintName@", constraintName);
                xmlfk = xmlfk.replace("@baseColumnNames@", baseColumnNames);
                xmlfk = xmlfk.replace("@referencedTableCatalogName@", referencedTableCatalogName);
                xmlfk = xmlfk.replace("@referencedTableName@", referencedTableName);
                xmlfk = xmlfk.replace("@referencedColumnNames@", referencedColumnNames);

//                System.out.println(xmlfk);
                xmltb = xmltb.replace("<!--@fk@-->", xmlfk+"\r\n<!--@fk@-->");
            }
            xmltb = xmltb.replace("<!--@fk@-->","");







//              xmltb = xmltb.replace("YYYYMMDD.HH:MI", Utils.format(Utils.now(),"yyyyMMdd.HH:mm" ));

            //wynik
//            System.out.println(xmltb);
//            System.out.println();
            return  xmltb;
        }

    }


    public static StreamedContent exportXmlFile(String tbOwner, String tbName) {
        StringBuilder sql = new StringBuilder();
        try (Connection conn = JdbcUtils.getConnection()) {

            final String xml = LiquibaseUtils.dmpOracleTableChangelog(tbOwner, tbName);
            sql.append(xml);

            String fname = tbName + ".xml";//

//            if (lw.getSelectedRows().size() == 1) {
//                fname = getDmlTableFileName(tableName, null, ".xml");
//            } else {
//                String now = Utils.format(new Date(), DATE_TIME_FORMAT);
//                fname = getDmlTablesFileName(lw.getSelectedRows().size(), now, ".xml");
//            }

//            ByteArrayInputStream bis = new ByteArrayInputStream(sql.toString().getBytes("Cp1250"));
            ByteArrayInputStream bis = new ByteArrayInputStream(sql.toString().getBytes(StandardCharsets.UTF_8));
            return new DefaultStreamedContent(bis, "text/plain", fname);

        } catch (Exception e) {
            User.alert(e);
        }

        return null;
    }

    public static StreamedContent exportXmlZip(SqlDataSelectionsHandler lw) {
        String tableName = null;

        if (lw == null || lw.getSelectedRows().isEmpty()) return null;

        try (Connection conn = JdbcUtils.getConnection()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(baos);

            for (DataRow r : lw.getSelectedRows()) {
                tableName = (String) r.get("tb_table_name");
                final String xml = LiquibaseUtils.dmpOracleTableChangelog(r.getAsString("tb_owner"), tableName);
                String filename = tableName + ".xml";
                Utils.addToZipFile(xml.getBytes(StandardCharsets.UTF_8), filename, zos);
            }

            zos.close();
            ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());

            String now = Utils.format(new Date(), "yyyy-MM-dd.HH.mm.ss");
            String fname = "pp.dmp.liquibase.tables.xml.(" + lw.getSelectedRows().size() + ")." + now + ".zip";

//            if (lw.getSelectedRows().size() == 1) {
//                fname = getDmlTableFileName(tableName, now, ZIP_FILE_EXTENSION);
//            } else {
//                fname = getDmlTablesFileName(lw.getSelectedRows().size(), now, ZIP_FILE_EXTENSION);
//            }

            return new DefaultStreamedContent(bis, "text/plain", fname);
        } catch (Exception e) {
            User.alert(e);
        }

        return null;
    }



}
