package com.comarch.egeria.pp.data;

import com.comarch.egeria.pp.Obiegi.ObiegBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.el.MethodExpression;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UINamingContainer;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.io.IOException;
import java.util.Map;


@FacesComponent
public class CObiegCzynnosci extends UINamingContainer {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();



	@Override //był problem z KU kiedy ObiegBean nie moze wczytac stanu obiegu przed renderowaniem przycisku cObiegCzynnosci ...
	public void setInView(boolean isInView) { //wystarczajaco pózno aby odczytac atryubty, ale jeszcze przed renderowaniem zawartosci  ...
		super.setInView(isInView);

		ObiegBean obieg = (ObiegBean) AppBean.getViewScopeBean("obiegBean");
		if (obieg!=null && obieg.getIdEncji()==null) {
			String kategoriaObiegu = (String) this.getAttributes().get("kategoriaObiegu");
			Long idEncji = (Long) this.getAttributes().get("idEncji");
			obieg.setIdEncji(idEncji);
			obieg.setKategoriaObiegu(kategoriaObiegu);
		}
	}
}
