package com.comarch.egeria.pp.data;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("mycnv")
public class MyConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
		return null;
	}   
    
	@Override
	public String getAsString(FacesContext ctx, UIComponent component, Object value) {
//		System.out.println(component.getClass().getSimpleName() +" [" +component.getId() + "] <- "+  value.getClass().getSimpleName() + " // " + value);
		
		if (value instanceof Date){
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date dt = (Date) value;
			return format.format(dt);
		}
		
		if (value instanceof String){
			String txt =  (String) value;
			if (txt.endsWith("00:00:00.0"))
				txt = txt.replace("00:00:00.0", "").trim();
				return txt;
		}	 

		return value.toString();
		
	}

}
