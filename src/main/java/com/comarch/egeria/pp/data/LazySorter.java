package com.comarch.egeria.pp.data;

import java.util.Comparator;

import org.primefaces.model.SortOrder;

public class LazySorter implements Comparator<DataRow> {
		 
	private String sortField;

	private SortOrder sortOrder;

	public LazySorter(String sortField, SortOrder sortOrder) {
		this.sortField = sortField;
		this.sortOrder = sortOrder;
	}

	public int compare(DataRow r1, DataRow r2) {
		try {
			Object value1 = r1.get(sortField); // DataRow.class.getField(this.sortField).get(car1);
			Object value2 = r2.get(sortField); // DataRow.class.getField(this.sortField).get(car2);
			
			int value = 0;

			if (value1 == null && value2 != null)
				value = -1;

			else if (value1 == null && value2 == null)
				value = 0;

			else if (value1 != null && value2 == null)
				value = 1;

//			else if (value1 instanceof String)
//				value = ((String) value1).compareToIgnoreCase((String) value2);
			
			else
				value = ((Comparable) value1).compareTo(value2);

			return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
			
		} catch (Exception e) {
			throw new RuntimeException();
		}
	}
	
	
}