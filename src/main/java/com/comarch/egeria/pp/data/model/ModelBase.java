package com.comarch.egeria.pp.data.model;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.RXObjectBase;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Transaction;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;


public class ModelBase extends RXObjectBase implements Serializable {
	private static final Logger log = LogManager.getLogger();

	public SqlBean sqlBean;

	public SqlBean getSqlBean() {
		return sqlBean;
	}

	public void setSqlBean(SqlBean sqlBean) {
		this.sqlBean = sqlBean;
	}


	public boolean validate(){
		return true;//POLIMORFICZNA (OPCJONALNA)
	}



	public void beforeCzynnoscObiegu(){
		//polimorficzna - wykonaj na rzecz modelu kod przed wykonaniem czynnosci...
	}

	public void afterCzynnoscObiegu(){
		//polimorficzna - wykonaj na rzecz modelu kod po wykonaniu czynnosci...
	}


	public void saveOrUpdate(boolean validate) {
		if (!validate()) return;
		try {
			HibernateContext.saveOrUpdate(this);
			User.info("Zmiany zapisano.");
		} catch (Exception ex) {
			User.alert(ex);
		}
	}

	public void saveOrUpdate() {
		this.saveOrUpdate(true);
	}


	public void saveOrUpdate(boolean validate, HibernateContext h){
		if (!validate) return;
		saveOrUpdate(h, true);
	}

	public void saveOrUpdate(HibernateContext h){
		saveOrUpdate(h, true);
	}


	public void saveOrUpdate(boolean validate, HibernateContext h, boolean refresh){
		if (!validate()) return;
		try {
			HibernateContext.saveOrUpdate(h, this, refresh);
			User.info("Zmiany zapisano.");
		} catch (Exception ex){
			User.alert(ex);
		}
	}

	public void saveOrUpdate(HibernateContext h, boolean refresh){
		saveOrUpdate(true, h, refresh);
	}


	public void saveOrUpdate(boolean validate, HibernateContext h, Transaction trx, boolean refresh){
		if (!validate()) return;
		try {
			HibernateContext.saveOrUpdate(h, trx, this, refresh);
			User.info("Zmiany zapisano.");
		} catch (Exception ex){
			User.alert(ex);
		}
	}

	public void saveOrUpdate(HibernateContext h, Transaction trx, boolean refresh){
		saveOrUpdate(true, h, trx, refresh);
	}



	public SqlDataSelectionsHandler getSql(String sqlId) {
		if (this.sqlBean==null)
			this.sqlBean = ((User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));

		return sqlBean.getSql(sqlId);
	}

	public SqlDataSelectionsHandler getLW(String sqlId) {
		return Utils.getLW(sqlId);
//		if (FacesContext.getCurrentInstance()==null)
//			return AppBean.sqlBean.getLW(sqlId);
//
//		//return ((User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user")).getLW(sqlId);
//		if (this.sqlBean==null)
//			this.sqlBean = ((User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));
//
//		if (sqlBean==null) return  null;
//		return sqlBean.getLW(sqlId);
	}

	public  SqlDataSelectionsHandler getLW(String sqlId, HashMap<String,  Object> namedParams){
		return Utils.getLW(sqlId, namedParams);
//		if (this.sqlBean==null)
//			this.sqlBean = ((User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));
//
//		return sqlBean.getLW(sqlId, namedParams);
	}

	public SqlDataSelectionsHandler getLW(String sqlId, Object... params) {
		return Utils.getLW(sqlId, params);
//		if (this.sqlBean==null)
//			this.sqlBean = ((User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));
//
//		return sqlBean.getLW(sqlId, params);
	}

	public SqlDataSelectionsHandler getLW(String sqlId, ArrayList<Object> params) {
		return Utils.getLW(sqlId, params);
//		if (this.sqlBean==null)
//			this.sqlBean = ((User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));
//
//		return sqlBean.getLW(sqlId, params);
	}


	
	public SqlDataSelectionsHandler getSlownik(String slNazwa) {
		return this.getSlownik(slNazwa, true, false);
	}
	
	public SqlDataSelectionsHandler getSlownik(String sqlid, boolean pokazWartosc) {
		return this.getSlownik(sqlid, pokazWartosc, false);
	}
	
	public SqlDataSelectionsHandler getSlownik(String slNazwa, boolean pokazWartosc, boolean sortujWartosc) {
		if (this.sqlBean==null)
			this.sqlBean = ((User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user"));

		return sqlBean.getSlownik(slNazwa, pokazWartosc, sortujWartosc);
	}

	public static boolean eq(Object x1, Object x2){
		//return (x1!=null && x1.equals(x2)) || (x2!=null && x2.equals(x1) || (x1==null && x2==null));
		return SqlBean.eq(x1, x2);
	}

	public static Date addYears(Date date, int years) {
		return Utils.addYears(date, years);
	}

	public static Date addDays(Date date, int days) {
		return Utils.addDays(date, days);
	}

//	public Double zaokraglenieKwotaWgWaluty(Double wartosc, Long walId){
//		return sqlBean.zaokraglenieKwotaWgWaluty(wartosc, walId);
//	}





//    public String gettersInfo() {
//	    return this.gettersInfo(false);
//    }

	public String gettersInfo(boolean shownulls) {
        StringBuilder sb = new StringBuilder();
		try {
			HashSet visitedReferences= new HashSet();
			this.buildGettersInfo(sb, 0, visitedReferences, shownulls);
		} catch (Exception ex){
			log.error(ex.getMessage(), ex);
		}
		return sb.toString();
	}

	private void buildGettersInfo(StringBuilder sb, int recuurencyLvl, HashSet visitedRefs, boolean shownulls) throws Exception {
		if (visitedRefs.contains(this))
			return;

		visitedRefs.add(this);

        sb.append("\r\n" + Utils.rpad("", recuurencyLvl, '.') + "---------------------------------------------------------------------------");
        sb.append("\r\n" + Utils.rpad("", recuurencyLvl, '.') + this);
        sb.append("\r\n" + Utils.rpad("", recuurencyLvl, '.') + "---------------------------------------------------------------------------");

//		Field[] fields = this.getClass().getFields();
//		List<Field> sortedFileds = Arrays.stream(fields).sorted(Comparator.comparing(Field::getName)).collect(Collectors.toList());
//		for (Field f : sortedFileds ) {
//			sb.append(" field " + f.getName() + ": " + this.getClass().getField(f.getName()));
//		}

		List<Method> getters = Arrays.stream(this.getClass()
				.getMethods())
				.filter(m -> m.getParameterCount() == 0 && (m.getName().startsWith("get") || m.getName().startsWith("is")))
				.sorted(Comparator.comparing(Method::getName))
				.collect(Collectors.toList());

        for (Method m : getters) {
            Object value = m.invoke(this);

            if (shownulls == false && value == null)
                continue;

            sb.append("\r\n" + Utils.rpad("", recuurencyLvl, '.') + Utils.rpad(m.getName(), 40, '.') + value);

            if (value instanceof ModelBase) {
                ((ModelBase) value).buildGettersInfo(sb, recuurencyLvl + 1, visitedRefs, shownulls); //rekurencja...
            }

            if (value instanceof Collection) {
                Collection elems = (Collection) value;
                for (Object el : elems) {
                    if (el instanceof ModelBase) {
                        ((ModelBase) el).buildGettersInfo(sb, recuurencyLvl + 1, visitedRefs, shownulls); //rekurencja...
                    }
                }
            }
        }

        sb.append("\r\n" + Utils.rpad("", recuurencyLvl, '.') + "---------------------------------------------------------------------------");
	}



}
