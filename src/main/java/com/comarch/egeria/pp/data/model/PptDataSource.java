package com.comarch.egeria.pp.data.model;

import com.comarch.egeria.pp.delegacje.model.DelBase;
import org.hibernate.annotations.DiscriminatorFormula;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.*;
import javax.sql.DataSource;
import java.io.Serializable;
import java.util.Date;

/**
 * The persistent class for the PPT_DATA_SOURCE database table.
 */
@Entity
@Table(name = "PPT_DATA_SOURCE", schema = "PPADM")
@NamedQuery(name = "PptDataSource.findAll", query = "SELECT d FROM PptDataSource d")
@DiscriminatorFormula("CASE WHEN DSN_TYP = 'ORACLE' THEN 'DataSourceOracle' ELSE 'DataSourcePostgres' END")
public abstract class PptDataSource extends ModelBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "DSN_NAZWA")
    private String dsnName;

    @Column(name = "DSN_TYP")
    private String dsnTyp;

    @Temporal(TemporalType.DATE)
    @Column(name = "DSN_AUDYT_DM")
    private Date dsnAudytDm;

    @Temporal(TemporalType.DATE)
    @Column(name = "DSN_AUDYT_DT", updatable = false)
    private Date dsnAudytDt;

    @Version
    @Column(name = "DSN_AUDYT_LM")
    private Long dsnAudytLm;

    @Column(name = "DSN_AUDYT_UM")
    private String dsnAudytUm;

    @Column(name = "DSN_AUDYT_UT", updatable = false)
    private String dsnAudytUt;

    public String getDsnName() {
        this.onRXGet("getDsnName");
        return dsnName;
    }

    public void setDsnName(String dsnName) {
        this.onRXSet("dsnName", this.dsnName, dsnName);
        this.dsnName = dsnName;
    }

    public String getDsnTyp() {
        this.onRXGet("getDsnName");
        return dsnTyp;
    }

    public void setDsnTyp(String dsnTyp) {
        this.onRXSet("dsnTyp", this.dsnTyp, dsnTyp);
        this.dsnTyp = dsnTyp;
    }

    public Date getDsnAudytDm() {
        return dsnAudytDm;
    }

    public void setDsnAudytDm(Date dsnAudytDm) {
        this.dsnAudytDm = dsnAudytDm;
    }

    public Date getDsnAudytDt() {
        return dsnAudytDt;
    }

    public void setDsnAudytDt(Date dsnAudytDt) {
        this.dsnAudytDt = dsnAudytDt;
    }

    public Long getDsnAudytLm() {
        return dsnAudytLm;
    }

    public void setDsnAudytLm(Long dsnAudytLm) {
        this.dsnAudytLm = dsnAudytLm;
    }

    public String getDsnAudytUm() {
        return dsnAudytUm;
    }

    public void setDsnAudytUm(String dsnAudytUm) {
        this.dsnAudytUm = dsnAudytUm;
    }

    public String getDsnAudytUt() {
        return dsnAudytUt;
    }

    public void setDsnAudytUt(String dsnAudytUt) {
        this.dsnAudytUt = dsnAudytUt;
    }

}
