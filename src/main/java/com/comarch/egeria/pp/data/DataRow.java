package com.comarch.egeria.pp.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.*;

import javax.faces.context.FacesContext;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.Administrator.model.PptAdmListyWartosci;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataRow implements Cloneable, Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	private SqlDataHandler sqlData = null;
	
	private ArrayList<Object> data = new ArrayList<>();
	
	public String format(String format){
		if (format==null || "".equals(format) ){
			return this.toString();		
		}
		
		try {
			
			if (format.contains("%")) { 
				int i=1;
				for (String cname : this.sqlData.getColumns()) {
					format = format.replace("%"+cname+"$","%"+i+"$");
					i+=1;
				}
			}

            Object[] args = this.getData().toArray();
            for(int i = 0; i < args.length; i++){
                if(args[i] == null){
                    args[i] = "";
                }
            }

            String ret = String.format(format, args);

            if (ret!=null && (ret.contains("\\n") || ret.contains("\\n")   )) { //ret.contains("\\r\\n")
                ret = ret.replace("\\r\\n", "\r\n"); //gdyby ktos chcial \r\n
				ret = ret.replace("\\r", "\r"); //gdyby ktos chcial tylko \r
				ret = ret.replace("\\n", "\n"); //gdyby ktos chcial tylko \n
            }

			if ("null".equals(ret)) ret = "";
			return ret;
			
		} catch (Exception e) { 
			log.debug(e.getMessage());
			return "err:DataRow.format/" + format;
		}
		
	}
	
	public DataRow(SqlDataHandler sqlData, ArrayList<Object> data) {
		this.setSqlData(sqlData);
		this.getData().addAll(data);
	}

	public DataRow(SqlDataHandler sqlData) {
		int size = sqlData.getColumns().size();
        Object[] arr = new Object[size];
		ArrayList<Object> al = new ArrayList<>(Arrays.asList( arr ));
		this.setSqlData(sqlData);
		this.getData().addAll(al);
	}

	public void reload(){
		if (this.getSqlData()!=null)
			this.getSqlData().reLoadDataRow(this);
	}
	
	@Override
	public String toString() {
		//return this.getData().toString();
		return this.asMap().toString();
	}

	public HashMap<String, Object> asMap(){
		HashMap<String, Object> ret = new HashMap<>();
		for (String c : this.sqlData.getColumns()) {
			ret.put(c,this.get(c));
		}
		return ret;
	}
	
	public int size(){
		return this.getData().size();
	}
	
	public boolean isEmpty(){
		return this.getData().isEmpty();
	}
	
	public DataRow clone(){
		try {
			DataRow ret = (DataRow) super.clone();
			ret.setData((ArrayList<Object>) this.getData().clone());
			return ret;
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void add(Object o){
		this.getData().add(o);
	}
	
	public Object[] toArray(){
		return this.getData().toArray();
	}
	
	public DataRow getDataRow(){
		return this;
	}
	
	
	public Object get(int index){
		return this.getData().get(index); 
	}
	
	public Object get(String columnName){
		String[] aljdef = null;
		
		if (columnName.contains("@")) {
			aljdef = columnName.split("@");
			columnName = aljdef[0];
		} else if(columnName.contains("%")) {
			return this.format(columnName);
		}
		
		int columnIndex = getSqlData().getColumnIndex(columnName);
		
		if (columnIndex>=0 && columnIndex < this.getData().size()){
			Object ret = this.getData().get(columnIndex);
			 
			if (aljdef!=null) {
				Object ljRet = null;
				
				String ljlwname = aljdef[1];
				String ljformat = aljdef[2];
				
				Map<String, Object> sessionMap = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
				User user = (User) sessionMap.get("user");
				SqlDataSelectionsHandler ljlw =  user.getLW(ljlwname);
				
				if (ljlw==null) return null;
				
				if (ljlw!=null) { 
					DataRow r = ljlw.find(ret);
					if (r!=null) {
						
						if (ljformat.contains("%"))
							ljRet = r.format(ljformat);
						else
							ljRet = r.get(ljformat);

					}
				}
				
				return ljRet;
			}
			
			return ret;
		}

		if (!"css_color".equals(columnName) && !"css_color_bg".equals(columnName))
			log.debug(this.getSqlData().getShortName() +  " --!!!brak kolumny!!!--> " + columnName);
		
		return null;
	}


	public Long getAsLong(String columnName){
		Object o = this.get(columnName);
		if (o!=null && o instanceof Number){
			return ((Number) o).longValue();
		}
		return null;
	}

	public Double getAsDouble(String columnName){
		Object o = this.get(columnName);
		if (o!=null && o instanceof Number){
			return ((Number) o).doubleValue();
		}
		return null;
	}

	public Date getAsDate(String columnName){
		Object o = this.get(columnName);
		if (o!=null && o instanceof java.sql.Date || o instanceof java.sql.Timestamp){
			return (Date) o;
		}
		return null;
	}

	public String getAsString(String columnName){
		Object o = this.get(columnName);
		if (o!=null && o instanceof String){
			return (String) o;
		}
		else {
			return this.getFormattedValue(columnName);
		}
	}


	public String getCssColorBg() {
		String ret = (String) this.get("css_color_bg");
		return ret;
	}
	public String getCssColorBgClass() {
		String ret = (String) this.get("css_color_bg");
		if (ret!=null && ret.startsWith("#")) ret = ret.substring(1);
		if (ret!=null) ret = "bg"+ret;
		return ret;
	}

	public String getCssColor() {

		String ret = (String) this.get("css_color");		
		return ret;

//		if (ret!=null && ret.startsWith("#{")) {  //ret==" #{row.get('lp') eq 3 ? 'green' : 'lightgray'}" TODO - wyliczanie koloru na podstawie EL zapisanego w definicji LW
//		ExpressionFactory factory = ExpressionFactory.newInstance();
//		ValueExpression ve = factory.createValueExpression( FacesContext.getCurrentInstance().getELContext(),  ""+ ret, String.class );
//		ret = (String) ve.getValue(FacesContext.getCurrentInstance().getELContext());
//	}
		
		
	}
	
	
	public Integer getInteger(String columnName){
		Integer ret = null;
		Object o = get(columnName);
		
		if (o==null)
			return null;
		
		if (o instanceof BigDecimal){
			ret = ((BigDecimal) o).intValue();
		} else {
			ret = Integer.parseInt(""+o);
		}
		return ret;
	}

	
	public Long getLong(String columnName){
		Long ret = null;
		Object o = get(columnName);
		
		if (o==null)
			return null;
		
		if (o instanceof BigDecimal){
			ret = ((BigDecimal) o).longValue();
		} else {
			ret = Long.parseLong(""+o);
		}
		return ret;
	}

	
	/**
	 * Replaces the element at the specified column in this DataRow with the specified element.
	 * @param columnName - (determine column position) column to replace element 
	 * @param value - element to be stored at the specified column position
	 * @return - the element previously at the specified column position
	 * 
	 * Throws: IndexOutOfBoundsException - if the columnName is wrong and column index is out of range (index < 0 || index >= size())
	 */
	public Object set(String columnName, Object value){
		int columnIndex = getSqlData().getColumnIndex(columnName);
		if (columnIndex>=0 && columnIndex < this.getData().size()){
			Object ret = this.get(columnIndex);
			this.getData().set(columnIndex, value);
			return ret;
		}else{
			throw new IndexOutOfBoundsException(this.getSqlData().getShortName() +  ":DataRow.set('" + columnName + "', " + value + ") -> Column: " + columnName + " not in " + this.getSqlData().getColumns());
		}
	}
	
	public int getColumnIndex(String sqlColumnName){
		return this.getSqlData().getColumnIndex(sqlColumnName);
	}
	
	
	public boolean isEmpty(String column){
		Object x = this.get(column);
		
		if (x==null){ 
			return true;
		}

        return x instanceof String && x.toString().trim().equals("");

    }
	
	
	public boolean isNotEmpty(String column){
		return !this.isEmpty(column);
	}
	
	
	public boolean containsAll(ArrayList<String> filterWords) {
		//String txtL = ("" + r.get("pp_abs_nazwa")).toLowerCase(); // to jesli chce filtrowac dane z okreslonej kolumny... 
		String txtL = toString().toLowerCase().replace(",", " "); // [aaa, bbb, ccc] -> [aaa bbb ccc]
		boolean containsFilter= false;
		
		for(String fw:filterWords ){
			if (txtL.contains(fw)){
				containsFilter= true;
			}
			else{
				containsFilter=false;
				break;
			}
		}
		return containsFilter;
	}



	HashMap<String, String> formatedValues = new HashMap<>();



	public String getFormattedValue(String colName){
		colName = colName.toLowerCase();

		String ret = this.formatedValues.get(colName);

		if (ret!=null)
			return ret;

		Object x = this.get(colName);

		if (x==null) {
			this.formatedValues.put(colName, "");
			return "";
		}

		if (x instanceof String) {
			this.formatedValues.put(colName, (String) x);
			return (String) x;
		}

		ret =  ""+x; //uwaga
		PptAdmListyWartosci col = this.sqlData.getColumnDef(colName);
		String format = col.getFormatPattern(); //String format = ds.getColFormat(col.getLstNazwaPola());
		if (format != null && !format.trim().isEmpty()) {
			if (x instanceof Timestamp)
				ret = Utils.format((Timestamp) x, format);
			if (x instanceof Number)
				ret = Utils.format((Number) x, format);
		} else {
			if (x instanceof Timestamp)
				ret = Utils.format((Timestamp) x);
			if (x instanceof Number) {
                //ret = Utils.format((Number) x);//tylko jesli to liczba dzesietna...
				if (x instanceof BigDecimal){
					BigDecimal bd = (BigDecimal) x;
					int scale = bd.scale();
					if (scale==0)
						ret = Utils.format((Number) x,"#0");
					else
						ret = Utils.format((Number) x, Utils.rpad("#0.",scale+3,'0'));
				}
            }
		}

		if (ret==null) ret = "";
		this.formatedValues.put(colName, ret);
		return ret;
	}


	public boolean containsFw(String fw, List<String> colNames){

		if(fw.contains("\"")) { //jest cudzyslów, traktujemy jak regex i całe pole
			for (String c : colNames) {
				String txtL = this.getFormattedValue(c).toLowerCase();
				if (SqlDataSelectionsHandler.checkStartsWithRegex(fw, txtL)){
					return true;
				}
			}
		} else if(fw.contains("*") || fw.contains("?") ) {//jest regex, ale nie cudzysłów, traktujemy jak fragment pola
			for (String c : colNames) {
				String txtL = this.getFormattedValue(c).toLowerCase();
				if (SqlDataSelectionsHandler.checkContainsWithRegex(fw, txtL)){
					return true;
				}
			}
		} else {
			for (String c : colNames) {
				String txtL = this.getFormattedValue(c).toLowerCase();
				if(txtL.contains(fw)) {
					return true;
				}
			}
		}
		return false;
	}

	
	
	public boolean containsAll(ArrayList<String> filterWords , List<String> colNames) {
		for(String fw : filterWords ){
			if (!this.containsFw(fw, colNames))
				return  false;
		}
		return true;

//		//String txtL = toString().toLowerCase().replace(",", " "); // [aaa, bbb, ccc] -> [aaa bbb ccc]
//		StringBuilder sb = new StringBuilder();
//		for (String c : colNames) {
//			sb.append(this.get(c)).append(" ");
//		}
//		String txtL = sb.toString().toLowerCase() ;
//
//		boolean containsFilter= false;
//
//		for(String fw:filterWords ){
////			System.out.println("???????????????????   "+ txtL  + ".contains?: " + fw + " -----------------> " + txtL.contains(fw));
//			if(fw.contains("*") || fw.contains("?") || fw.contains("\"")) {
//				containsFilter = SqlDataSelectionsHandler.checkContainsWithRegex(fw, txtL);
//			} else if (txtL.contains(fw)){
//				containsFilter= true;
//			}
//			else{
//				containsFilter=false;
//				break;
//			}
//		}
//		return containsFilter;
	}		
	
//	public boolean containsAll(String[] filterWords , String[] colNames) {
//		//String txtL = toString().toLowerCase().replace(",", " "); // [aaa, bbb, ccc] -> [aaa bbb ccc]
//		StringBuilder sb = new StringBuilder();
//		for (String c : colNames) {
//			sb.append(this.get(c)).append(" ");
//		}
//		String txtL = sb.toString().toLowerCase();
//		
//		boolean containsFilter= false;
//		
//		for(String fw:filterWords ){
//			//			System.out.println("???????????????????   "+ txtL  + ".contains?: " + fw + " -----------------> " + txtL.contains(fw));
//			if (txtL.contains(fw)){
//				containsFilter= true;
//			}
//			else{
//				containsFilter=false;
//				break;
//			}
//		}
//		return containsFilter;
//	}		

	public Long getIdAsLong(){
		Long ret = Long.valueOf(this.getSqlData().pkRows2IndexMap.get(this));
		return ret;
	}
	
	public String getIdAsString(){
		String ret = this.getSqlData().pkRows2IndexMap.get(this);
		return ret;
	}

	public SqlDataHandler getSqlData() {
		return sqlData;
	}

	public void setSqlData(SqlDataHandler sqlData) {
		this.sqlData = sqlData;
	}

	public ArrayList<Object> getData() {
		return data;
	}

	public void setData(ArrayList<Object> data) {
		this.data = data;
	}
	
	
	
	public void redirectTo(String redirectToPage) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", this.getIdAsString());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", this);
		Utils.redirectTo(redirectToPage);
	}
	
	
	public void redirectTo(String redirectToPage, Object id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", id);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", this);
		Utils.redirectTo(redirectToPage);
	}

}
