package com.comarch.egeria.pp.data;

import com.comarch.egeria.Utils;

import java.util.Date;

import static com.comarch.egeria.Utils.*;

public class KursWaluty extends RXObjectBase{

    public Long walId = 1L; //domyslnie PLN
    public String walSymbol = "PLN"; //domyslnie PLN
    public double przelicznik = 1.0; //przelicznik do PLN
    public double jednostka = 1.0; //przelicznik do PLN

    public String opis = null;
    public Date dataKursu; //data kursu NBP
    public Date naDzien; //dzien, na który kurs jest aktualny

    public Long dokId = null; //jesli kurs z dokumentu: id z kgt_dokumenty

    public KursWaluty() {
    }

    public KursWaluty(Number walId, Date naDzien){
        if (naDzien==null)
            naDzien = Utils.today();

        if (walId == null)
            walId = 1L;

        this.setNaDzien(naDzien);
        this.setWalId(walId.longValue());
    }
    
    public Date getDataKursu() {
        return dataKursu;
    }

    public void setDataKursu(Date dataKursu) {
        this.dataKursu = dataKursu;
    }


    public Date getNaDzien() {
        return naDzien;
    }

    public void setNaDzien(Date naDzien) {
        this.naDzien = naDzien;
    }


    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }


    public double getPrzelicznik() {
        return przelicznik;
    }

    public void setPrzelicznik(double przelicznik) {
        this.przelicznik = przelicznik;
    }

    public double getJednostka() {
        return jednostka;
    }

    public void setJednostka(double jednostka) {
        this.jednostka = jednostka;
    }

    public double getPrzelicznikJedn() {
        return Utils.divide(this.przelicznik,this.jednostka,6);
    }



    public Double wartoscPLN(Double wartoscWAL, int roundDecimalPlaces){
        if (wartoscWAL==null)
            return null;
        return Utils.multiply( wartoscWAL, this.getPrzelicznikJedn(), roundDecimalPlaces );
    }

    public Double wartoscPLN(Double wartoscWAL){
        return this.wartoscPLN(wartoscWAL,2);
    }


    public Long getWalId() {
        this.onRXGet("walId");
        return walId;
    }

    public void setWalId(Long walId) {
        this.onRXSet("walId", this.walId, walId);
        boolean eq = eq(this.walId, walId);
        this.walId = walId;
        if(!eq){
            this.setWalSymbol( getLW("WAL").findAndFormat(this.walId, "%wal_symbol$s"));
            this.setJednostka(getLW("WAL").find(this.walId).getAsDouble("WAL_JEDNOSTKA_KURSU"));
        }
    }


    public String getWalSymbol() {
        this.onRXGet("walSymbol");
        return walSymbol;
    }

    public void setWalSymbol(String walSymbol) {
        this.onRXSet("walSymbol", this.walSymbol, walSymbol);
        this.walSymbol = walSymbol;
    }

    public Long getDokId() {
        return dokId;
    }

    public void setDokId(Long dokId) {
        this.dokId = dokId;
    }


    public String getSortKey(){//klucz wyznaczający podsumowanie pozycji
        return this.walSymbol
                + lpad(this.przelicznik,10)
                + lpad(nz(this.getDokId()),10)
                + format(this.getDataKursu())
                + this.getOpis();
    }


    @Override
    public boolean equals(Object obj) {
        if (obj instanceof KursWaluty){
            KursWaluty kw = (KursWaluty) obj;
            return  eq(this.getWalId(), kw.getWalId())
                && eq(this.getPrzelicznik(), kw.getPrzelicznik())
                && eq(this.getDataKursu(), kw.getDataKursu())
                && eq(this.getOpis(), kw.getOpis());
        }
        return false;
    }


    @Override
    public int hashCode() {
        return this.getSortKey().hashCode();
    }
}
