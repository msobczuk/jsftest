package com.comarch.egeria.pp.data;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("timecnv")
public class TimeConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
		return null;
	}   
    
	@Override
	public String getAsString(FacesContext ctx, UIComponent component, Object value) {
//		System.out.println(component.getClass().getSimpleName() +" [" +component.getId() + "] <- "+  value.getClass().getSimpleName() + " // " + value);
		
		if (value instanceof Date){
			SimpleDateFormat format = new SimpleDateFormat("HH:mm");
			Date dt = (Date) value;
			return format.format(dt);
		}
		
		return value.toString();
		
	}

}
