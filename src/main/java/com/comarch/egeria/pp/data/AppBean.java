package com.comarch.egeria.pp.data;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.dataSources.BasicDataSourceTomcatAdapter;
import com.comarch.egeria.pp.data.dataSources.DataSourceEmptyInfo;
import com.comarch.egeria.pp.data.dataSources.IDataSourceInfo;
import com.comarch.egeria.pp.data.liquibase.LiquibaseUtils;
import com.comarch.egeria.pp.data.model.DataSourceBase;
import com.comarch.egeria.pp.data.model.DataSourceMSSQL;
import com.comarch.egeria.pp.data.model.DataSourceOracle;
import com.comarch.egeria.pp.data.model.DataSourcePostgres;
import com.comarch.egeria.pp.data.msg.Msg;
import com.comarch.egeria.pp.data.msg.MsgPipeListener;
import com.comarch.egeria.pp.data.msg.Push;
import com.comarch.egeria.pp.erd.model.DbObject;
import com.comarch.egeria.pp.kontrolaUprawnien.KontrolaUprawnien;
import com.comarch.egeria.pp.menu.model.PptAdmMenu;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.utils.constants.Const;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.*;

//import org.primefaces.push.EventBus;
//import org.primefaces.push.EventBusFactory;
//TODO javax.faces.ENABLE_WEBSOCKET_ENDPOINT
//http://hantsy.blogspot.com/2017/11/jsf-23websocket-support.html

@ApplicationScoped
@ManagedBean
@Named
@Scope("singleton")
public class AppBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@Resource(mappedName = Const.JNDI_CONNECTION_NAME)
	private DataSource ds;

	private DataSourceBase dataSource;

	private static final String PROTECTED_RESOURCES = "/WEB-INF/protectedElements";


	/* -------------------------------------------------------------------------
	    Kod akt. obiekty bazy korzysta z okreslonej wersji EAP_DDL (+zależności)

        Specfix: dla 6.0.4.005 (od 6.0.4.006 nie jest potrzebny)
        prospero://$/egeria4/6.0.4/fix_spec_web/006/006.PP_EG/

    	Parametr z pliku context.xml z Tomcata:
    	<Environment name="autoResetDb" type="java.lang.Boolean" value="false" />
    	Domyślnie false, jeśli nie zostanie zdefiniowany.
      ---------------------------------------------------------------------------*/
	@Inject
	private final Boolean autoResetDb = false;




	public static String version;
	
	public static AppBean app = null;

	public static ConcurrentHashMap<String, ConcurrentHashMap<String, String>> viewResourceFilesCHM = new ConcurrentHashMap<>();



	File applicationContextFolder = null; //(deploy app folder) - katalog, z którego serwer aplikacji uruchamia PP - np. W:\ApacheTomcat\webapps\PortalPracowniczy\
	File fileLogoFirmy = null; // domyslnie  /egeria/images/logoFirmy.svg"
	File fileLogoRpt = null; // domyślnie  /egeria/images/logoRpt.png"

	public static ConcurrentHashMap<String, String> urlMappings = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<String, String> xhtmlMappings = new ConcurrentHashMap<>();

	public static final Logger log = LogManager.getLogger();//public static final Logger log = LogManager.getLogger(AppBean.class);
	
	public static void addUrlMapping(String url, String xhtml) throws Exception{
		
		String x = urlMappings.get(url);
		if (x!=null)
			throw new Exception("Zdublowane mapowanie dla strony: " + url);
		
		
		urlMappings.put(url, xhtml);
		xhtmlMappings.put(xhtml, url);
	}
	
	
	public static SqlBean sqlBean =  new SqlBean();
	
	public static String language = "PL";
	
	public static ConcurrentHashMap<String, Timestamp > listyWartosciTSHM = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<String, SqlDataSelectionsHandler> listyWartosciHM = new ConcurrentHashMap<>();
	
	
	public static ConcurrentHashMap<String, User> sessionsUsers = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<String, Long> widokiXhtml = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<String, Long> raporty = new ConcurrentHashMap<>();
	
	public static MsgPipeListener msgListener; 
	public static Thread msgListenerThread;
	
//	private static String version = "";
	private Properties pomProperties;
	private Properties manifestProperties;


	private DataRow dbInstance = null;
	
	
	private Date startupTime = new Date();
	
	
	public Date getDate(){
		return new Date();
	}
	
	
	//flaga ustalajaca czy stosowac logi czasu wykonywania metod
	private static int logMethodsLevel = -1; // 0 kazdy przypadke; docelowo 100 - loguj metody z czasem dzialania powyzej 100ms 
	
	private static ConcurrentHashMap<String, Long> logMethodsSummaryHM = new ConcurrentHashMap<>();
	private static ConcurrentHashMap<String, Long> logMethodsCountHM = new ConcurrentHashMap<>();
	
	public static long logMethod(String methodname){
		Date date = new Date();
		if (logMethodsLevel<0) return date.getTime();
		
		//System.out.println(date + " start /-/ " + methodname + " /-/ ...");
		return date.getTime();
	}
	
	public static void logMethod(String methodname, long logMethodStart){
		if (logMethodsLevel<0) return;
		
		Date date = new Date();
		long ret = date.getTime() - logMethodStart;
		if (ret < logMethodsLevel) return;
		System.out.println(date + " stop /-/ " + methodname + " /-/-t-> " + ret + "[ms]");
		logMethodsSummaryHM.put(methodname, nz(logMethodsSummaryHM.get(methodname)) + ret);
		logMethodsCountHM.put(methodname, nz(logMethodsCountHM.get(methodname))+1);
	}

	public void logMethodClearStats(){
		logMethodsSummaryHM.clear();
		logMethodsCountHM.clear();
	}
	

	public List<String> logMethodsNames(){
		ArrayList<String> ret = new ArrayList<String>();
		ret.addAll(logMethodsSummaryHM.keySet());
		return ret;
	}





	
	
	@PostConstruct
	public void init() throws Exception {
		System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n" +
				"\n============================================================================================" +
				"\n============================================================================================" +
				"\n============================================================================================" +
				"\n"+ strTS() +" AppBean / init() --> START ... "
		);


		AppBean.app = this;

		JdbcUtils.ds = this.ds; //main datasource
		try (Connection conn = ds.getConnection()){
			this.setDataSource( getPPDataSourceInstance(conn) ); //test jdbc
		}catch(Exception e){
			log.error(e.getMessage(), e);
			throw e;//halt systemu (brak dostępu do bazy)
		}
//		try(HibernateContext h = new HibernateContext()){} // test konfiguracji hibernate'a


		String canonicalName = this.ds.getClass().getCanonicalName();
		if ("org.apache.tomcat.dbcp.dbcp2.BasicDataSource".equals(canonicalName) ) {
			bds = new BasicDataSourceTomcatAdapter(this.ds); //basic datasource
			System.out.println(strTS() +" AppBean / init() --> data source: '" + bds.getUsername() + "@" + bds.getUrl()  + "' ...");
		}


		applicationContextFolder = appContext.getResource("/").getFile(); //np. C:\Users\marcin.dziobek\IdeaProjects\pp\target ale w syst. prod jest to wlasciwy docelowy katalog, w którym jest deploy aplikacji
		Const.HOME_REDIRECT_URL = appContext.getApplicationName();// "/PortalPracowniczy" na PROD albo np. "/pp_war_exploded" na DEV

		System.out.println(strTS() +" AppBean / init() --> applicationContextFolder: '" + applicationContextFolder + "' ...");

		version =  this.getManifestProperties().getProperty("version"); //wczytuje manifest properties
		System.out.println(strTS() +" AppBean / init() --> version: '" + version + "' ...");


//		final DataRow dr = getPpKonfiguracjaDR("PP_KOMUNIKATY"); dziala

		try {
			if (!System.getProperty("os.name").startsWith("Windows")) { // w systemie widnows wielkosc znakow nie ma znaczenia, ael w Centos/UNIX czesc plikow powinna miec lowercase
				//zamienic na nazwy plikow na male literki w /SQL/....
				renameAllFilesLowerCase(Paths.get(getAbsolutePath(DbObject.SQL_RESOURCES_PATH).toString(), "packages")); //.pcs; .pcb
				renameAllFilesLowerCase(Paths.get(getAbsolutePath(DbObject.SQL_RESOURCES_PATH).toString(), "procedures")); //.prc
				renameAllFilesLowerCase(Paths.get(getAbsolutePath(DbObject.SQL_RESOURCES_PATH).toString(), "functions")); //.fun
				renameAllFilesLowerCase(Paths.get(getAbsolutePath(DbObject.SQL_RESOURCES_PATH).toString(), "views")); //.vw
				renameAllFilesLowerCase(Paths.get(getAbsolutePath(DbObject.SQL_RESOURCES_PATH).toString(), "triggers")); //.trg
			}
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
			throw ex;// -> halt systemu ?
		}



		if (autoResetDb) {//automatyczna przebudowa schematu PPADM przy starcie aplikacji

			try {

				if (this.getDataSource().isEgeria6DB()) {

					System.out.println(strTS() + " AppBean / init() --> /SQL/initial/ ... ");
					loadInitialDbObjects();

					System.out.println(strTS() + " AppBean / init() --> /SQL/tables/ ... ");
					rebuildTables();

					System.out.println(strTS() + " AppBean / init()  --> /SQL/ -> ppt_objects verification ... ");
					DbObject.loadAllResourceFiles2PptObjects(true);//wczytanie danych wg res. files do indeksu PPT_OBJECT i opcjonalnie aktualizacja/rekompilacja kodu PL/SQL w bazie

				} else {

					System.out.println(strTS() + " AppBean / init() --> LiquibaseUtils.update DB ... ");
					LiquibaseUtils.updateDataBasePP();

				}

				dodanieMenuSTD();

			} catch (Exception ex) {
				log.error(ex.getMessage(), ex);
				throw ex;// -> halt systemu ?
			}

		} else {
			System.out.println(strTS() + " AppBean / init() --> autoResetDb = false -----> POMINIĘTO AKTUALIZACJĘ BAZY DANYCH ... ");
//			try(HibernateContext h = new HibernateContext()){} // test konfiguracji hibernate'a
		}


		if(this.dataSource instanceof DataSourceOracle) {
			this.setDbInstance(sqlBean.getSql("v$instance", "select * from  v$instance").getData().get(0));
		}

		System.out.println(strTS() +" AppBean / init() --> DX ... " );
		try {
			RodoUtils.dsWczytajWidokiXhtmlCHM();
			RodoUtils.dsWczytajRaportyCHM();
		} catch (Exception ex){
			log.error(ex.getMessage(), ex);
		}

		System.out.println(strTS() +" AppBean / init() --> KU ... " );
		KontrolaUprawnien.reloadWuprCHMs(null);

		System.out.println(strTS() +" AppBean / init() --> import external /egeria/images/logoFirmy.png ... ");
		importExternalLogoFiles();

		System.out.println(strTS() + " AppBean / init() --> PPP_MSG/DBMS_ALERT -> msgListener... ");
		//wątek z nasłuchem komunikatów z bazy...
		msgListener = new MsgPipeListener(ds);
		msgListenerThread = new Thread(msgListener);
		msgListenerThread.start();
		
		System.out.println("" +
						"\n" + strTS() + " AppBean / init() --> END" +
						"\n============================================================================================" +
						"\n============================================================================================" +
						"\n============================================================================================" +
						"\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
	}


	private void loadInitialDbObjects() throws IOException, SQLException {
		Path path = Paths.get(AppBean.app.getApplicationContextFolder().getAbsolutePath(),
				PROTECTED_RESOURCES + "/SQL/initial/");

        try (Connection conn = JdbcUtils.getConnection(false)) {

            for (Path filePath : Files.walk(path).filter(Files::isRegularFile)
                    .sorted((p1, p2) -> p1.toFile().toString().compareTo(p2.toFile().toString()))
                    .collect(Collectors.toList())) {

                System.out.println(strTS() + " DB <------ " + filePath);

                if (filePath.toString().endsWith(".tab")) {
                    JdbcUtils.executeStatement(conn, filePath);
                } else {
                    JdbcUtils.executeStatement(conn, filePath);
                }
            }

        }
	}




	public List<DbObject> getDbObjectErrors(){
		return DbObject.DbObjectErrors;
	}



	private void rebuildTables() throws IOException, SQLException {
		try (Connection conn = JdbcUtils.getConnection(false)) {

			importFilesTab(conn);

			System.out.println(strTS() + " PPP_DDL.DDL_ALL_TABLES_AND_COLUMNS ... ");
			JdbcUtils.sqlSPCall(conn, "PPP_DDL.DDL_ALL_TABLES_AND_COLUMNS");
			System.out.println(strTS() + " PPP_DDL.DDL_ALL_TABLES_AND_COLUMNS DONE (przebudowano tabele schematu PPADM wg PPT_TABLES)");
		} catch (Exception e) {
			User.alert(e);
			log.error(e.getMessage(), e);
//			throw e; //TODO odkomentowac jesli w MF nie ma eksplozji
		}
	}
	
	public static void importFilesTab(Connection conn) throws IOException {
		Path path = Paths.get(AppBean.app.getApplicationContextFolder().getAbsolutePath(),
				PROTECTED_RESOURCES + "/SQL/tables/");

		for (   Path filePath : Files.walk(path)
				.filter(f -> Files.isRegularFile(f) && f.toString().endsWith(".tab"))
				.collect(Collectors.toList())
		) {
			try {
				System.out.println(strTS() + " PPT_TABLES <----  "+filePath);
				JdbcUtils.executeStatement(filePath);
			} catch (Exception ex) {
				log.error("loadTables / błąd w pliku: "+ filePath);
				log.error(ex.getMessage(), ex);
			}
		}
	}







	public DataSourceBase getPPDataSourceInstance(Connection conn) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		try {

			final DatabaseMetaData metaData = conn.getMetaData();
			final String databaseProductName = metaData.getDatabaseProductName();
			final String databaseProductVersion = metaData.getDatabaseProductVersion();
			final String userName = metaData.getUserName();
			final String url = metaData.getURL();
			final String driverName = metaData.getDriverName();
			final String driverVersion = metaData.getDriverVersion();


			System.out.println("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
					+ "\n------------------------------------------------------------------------BAZA DANYCH:-----------------------------------------------------------------------------------------------"
					+ "\n-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
					+ "\nrdbms: " + databaseProductName + " v.: " + databaseProductVersion
					+ "\njdbc url: " + url
					+ "\njdbc driver: " + driverName + " v.: " + driverVersion
					+ "\njdbc connection user: " + userName
					+ "\n-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");

			DataSourceBase ppds = null;

			if ("Oracle".equalsIgnoreCase (databaseProductName)) {
				ppds = new DataSourceOracle();
				try {
					//UWAGA: widok sys.all_tables pokazuje tylko te tabele, do ktorych USER ma jakies GRANT'y; W założeniu widać publiczne synonimy; Docelowo n/w test mozna rozbudować
					final Object x = JdbcUtils.sqlExecScalarQuery(conn, "select object_name from sys.all_objects where object_type = 'SYNONYM' and object_name = 'EAT_PARAMETRY_EGERII' and owner = 'PUBLIC'");
					ppds.setEgeria6DB( x!=null );
				} catch (Exception ex) {
					log.error(ex);
				}
//				Object sysdate = JdbcUtils.sqlExecScalarQuery(conn, "select sysdate from dual ");
				//this.setDbInstance(sqlBean.getSql("v$instance", "select * from  v$instance").getData().get(0));
			}
			else if ("PostgreSQL".equalsIgnoreCase (databaseProductName)){
				ppds = new DataSourcePostgres();
			}
//			else if ("MySQL".equalsIgnoreCase (databaseProductName)){
//				ppds = new DataSourceMySQL();
//			}
			else if (databaseProductName.contains("Microsoft SQL Server")){
				ppds = new DataSourceMSSQL();
			}
//			else if ("H2".equalsIgnoreCase(databaseProductName)) {
//				ppds = new DataSourceH2();
//			}

			if (ppds!=null) {
				ppds.setDsnName("pp_db");
				ppds.setDs(this.ds);
			}
			return ppds;

		}catch(Exception e) {
			System.out.println("\n\n\n" +
					"---------------------------------------------------------------------\n" +
					"Błąd połączenia JDBC. Sprawdź połączenie lub ustawienia (server.xml):\n" +
					"---------------------------------------------------------------------\n" +
					"...\n" +
					"<Resource auth=\"Container\" \n" +
					"          driverClassName=\"????????????????????????????????\" \n" +
					"          global=\"jdbc/pp_db\" \n" +
					"          name=\"jdbc/pp_db\" \n" +
					"          type=\"javax.sql.DataSource\" \n" +
					"          url=\"jdbc:oracle:thin:@???.???.???.???:????:?????\" \n" +
					"          username=\"PPADM\" \n" +
					"          password=\"??????????????\" />\n" +
					"...\n" +
					"---------------------------------------------------------------------\n" +
					"\n\n\n");

			throw  e;//halt!
		}
	}


	public void importExternalLogoFiles() {
		try { //nadpisanie logoFirmy (standardowe. pliki MF_logo3.png wewn. naszej apki sa przy starcie zstępowane logiem dedykowanym do raportów klienta)...
			String parentFolder = applicationContextFolder.getParent();

			fileLogoFirmy = new File(parentFolder + "/egeria/images/logoFirmy.svg");
			if (!fileLogoFirmy.exists())  fileLogoFirmy = new File(parentFolder + "/egeria/images/logoFirmy.png");
			else if (!fileLogoFirmy.exists())  fileLogoFirmy = new File(parentFolder + "/egeria/images/logoFirmy.gif");
			else if (!fileLogoFirmy.exists())  fileLogoFirmy = new File(parentFolder + "/egeria/images/logoFirmy.jpg");


			if (fileLogoFirmy.exists()) {
				Files.copy(fileLogoFirmy.toPath(), appContext.getResource("/resources/images/logoFirmy.png").getFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
				System.out.println("...skopiowano " + fileLogoFirmy.getName() + " jako " + appContext.getResource("/resources/images/logoFirmy.png").getFile().toPath());
			}


//			fileLogoRpt = new File(parentFolder + "/egeria/images/logoRpt.svg");
//			if (!fileLogoRpt.exists())  fileLogoRpt = new File(parentFolder + "/egeria/images/logoRpt.png");
//			else if (!fileLogoRpt.exists())  fileLogoRpt = new File(parentFolder + "/egeria/images/logoRpt.gif");
//			else if (!fileLogoRpt.exists())  fileLogoRpt = new File(parentFolder + "/egeria/images/logoRpt.jpg");

			fileLogoRpt = new File(parentFolder + "/egeria/images/logoRpt.png");
			if (fileLogoRpt.exists()) {
				Files.copy(fileLogoRpt.toPath(), appContext.getResource("/resources/images/MF_logo3.png").getFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
				Files.copy(fileLogoRpt.toPath(), appContext.getResource(PROTECTED_RESOURCES + "/Reports/compiledReports/resources/MF_logo3.png").getFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
				System.out.println("...skopiowano " + fileLogoRpt.toPath() + " jako " + appContext.getResource("/resources/images/MF_logo3.png").getFile().toPath());
				System.out.println("...skopiowano " + fileLogoRpt.toPath() + " jako " + appContext.getResource(PROTECTED_RESOURCES + "/Reports/compiledReports/resources/MF_logo3.png").getFile().toPath());
			}

		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
	}




	public SqlDataSelectionsHandler getLW(String sqlId) {
		return sqlBean.getLW(sqlId);
	}

	public SqlDataSelectionsHandler getLW(String sqlId, Object... params) {
		return sqlBean.getLW(sqlId, params);
	}

	public SqlDataSelectionsHandler getLW(String sqlId, ArrayList<Object> params) {
		return sqlBean.getLW(sqlId, params);
	}





	
	
	public static String getSessionId(){
		FacesContext ctx = FacesContext.getCurrentInstance();
		
		if (ctx==null) 
			return null;
		
		ExternalContext ectx = ctx.getExternalContext();
		if (ectx==null) 
			return null;
		
		HttpSession session = (HttpSession) ectx.getSession(false);
		if (session!=null)
			return session.getId();
		else 
			return null;
	}
	
	
	public static User getUser(){
		return getUser(getSessionId());
	}
	
	public static User getUser(String sessionId){
		if (sessionId != null)
			return sessionsUsers.get(sessionId);
		else 
			return null;
	}

	public List<String> connectedWebSocketsInfo(){
		return Push.getConnectedWebsocketsInfo();
	}


	public String getPushWebSocketUrl(String windowGuid, boolean wss){
		return Push.getPushWebSocketUrl(windowGuid, wss);
	}



	//testy websocketów
	private String webSocketUrl = "";

	public String getWebSocketUrl() {
		return webSocketUrl;
	}

	public void setWebSocketUrl(String webSocketUrl) {
		this.webSocketUrl = webSocketUrl;
	}


	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public String getRequestUrl(){
		return ""+((javax.servlet.http.HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRequestURL();
	} 
	
	public String shortVersionInfo(){
		String ret = ""+this.manifestProperties.get("version");
		int indexOf = ret.indexOf("_");
		ret = ret.substring(indexOf+1);
		return ret;
	}
	
	
	public String getPrimeFacesBuildVersion(){
		//return RequestContext.getCurrentInstance().getApplicationContext().getConfig().getBuildVersion();
		return "8.0"; //TODO ustalic dynamicznie wersje PF
	}


    public ApplicationContext getAppContext() {
        return appContext;
    }

    @Autowired
	private ApplicationContext appContext;
	 
	
	
	
	
	public Map<String, Collection<SqlDataSelectionsHandler>> requestBeansInfo(){
		TreeMap<String, Collection<SqlDataSelectionsHandler>> ret = new TreeMap<>();
		
		FacesContext ctx = FacesContext.getCurrentInstance();
		
		if (ctx!=null){
			
			ExternalContext ectx = ctx.getExternalContext();
			
			if (ectx!=null) {
				ret = getComarchSqlBeansInfo(ectx.getRequestMap());
			}
		}
		
		return ret;
	}
	
	
	public Map<String, Collection<SqlDataSelectionsHandler>> viewBeansInfo(){
		TreeMap<String, Collection<SqlDataSelectionsHandler>> ret = new TreeMap<>();
		
		FacesContext ctx = FacesContext.getCurrentInstance();
		if (ctx!=null){
			ret = getComarchSqlBeansInfo(ctx.getViewRoot().getViewMap());
		}
		
		return ret;
	}
	
	
	
	
	public Map<String, Collection<SqlDataSelectionsHandler>> sessionBeansInfo(){
		TreeMap<String, Collection<SqlDataSelectionsHandler>> ret = new TreeMap<>();
		
		FacesContext ctx = FacesContext.getCurrentInstance();
		
		if (ctx!=null){
			
			ExternalContext ectx = ctx.getExternalContext();
			
			if (ectx!=null) {
				ret = getComarchSqlBeansInfo(ectx.getSessionMap());
			}
		}
		
		return ret;
	}
	
	

	public Map<String, Collection<SqlDataSelectionsHandler>> applicationBeansInfo(){
		TreeMap<String, Collection<SqlDataSelectionsHandler>> ret = new TreeMap<>();
		
		FacesContext ctx = FacesContext.getCurrentInstance();
		
		if (ctx!=null){
			
			ExternalContext ectx = ctx.getExternalContext();
			
			if (ectx!=null) {
				ret = getComarchSqlBeansInfo(ectx.getApplicationMap());
			}
		}
		
		return ret;
	}

	
	public static Object getApplicationScopeBean(String key){
		return FacesContext.getCurrentInstance().getExternalContext().getApplicationMap().get(key);
	}	
	
	public static Object getSessionScopeBean(String key){
		FacesContext ctx = FacesContext.getCurrentInstance();
		if (ctx==null) return null;
		ExternalContext ectx = ctx.getExternalContext();
		if (ectx==null) return null;
		return ectx.getSessionMap().get(key);
	}

	public static Object getViewScopeBean(String key){
		return FacesContext.getCurrentInstance().getViewRoot().getViewMap().get(key);
	}	
	

	public TreeMap<String, Collection<SqlDataSelectionsHandler>> getComarchSqlBeansInfo(Map<String, Object> appMap) {
		TreeMap<String, Collection<SqlDataSelectionsHandler>> ret = new TreeMap<>();
		if (appMap==null || appMap.isEmpty())
		    return ret;
		
		for (String k : appMap.keySet()) {

			try {

                Object x = appMap.get(k);

				if (x==null){
					System.out.println("getComarchSqlBeansInfo /"+ k + "/ appMap -> x==null");
					continue;
				}

				if (x==null || x.getClass().getPackage()==null || x.getClass().getPackage().getName()==null || !x.getClass().getPackage().getName().startsWith("com.comarch"))
					continue;

				if (x instanceof SqlBean){
					ConcurrentHashMap<String, SqlDataSelectionsHandler> sqls = ((SqlBean) x).getSqls();
					ret.put(x.getClass().getSimpleName() +" #"+x.hashCode(), ((SqlBean) x).getSqls().values());
				}
				else{
					ret.put(x.getClass().getSimpleName() +" #"+x.hashCode(), new ArrayList<>());
				}

			} catch (Exception ex){
				log.error(ex.getMessage(), ex);
			}
		}
		
		return ret;
	}


	public static IDataSourceInfo bds = new DataSourceEmptyInfo();

	public static DataSource DATASOURCE = null;

	public static HashMap<String, SqlDataHandler> slowniki = new HashMap<>();
	// public static ArrayList<Object[]> data = new ArrayList<>();

	public void redirect(String destinationUrl) throws IOException{
		FacesContext.getCurrentInstance().getExternalContext().redirect(destinationUrl);
	}
	
	public String getForceConstruct() {
		return "";
	}

	
	
	public String getGrafikiURL(){
		String ret = "";
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		StringBuffer requestURL = req.getRequestURL();
		String contextPath = req.getContextPath();
		ret = requestURL.substring(0, requestURL.indexOf(contextPath));
		return ret + "/Grafiki/";
	} 



	  
    public Object decode(Object code, Object... args) {
    	return Decode(code, args); 
    }
    public static Object Decode(Object code, Object... args) {
        int index = 0;
        while (index < args.length-1) {
            if ((code==null && args[index]==null) || args[index].equals(code)) {
                return args[index + 1];
            }
            index+=2;
        }
        if (args.length>0 && args.length%2!=0)
        	return args[args.length-1];
        else
        	return null;
    }

    
    
    
    
    
	
	
	

	
	
	
	
	
	public static String concatValues(Object... args){
		StringBuilder sb = new StringBuilder();
		for(Object o:args){
			//TODO - poustawiac inne wrażliwe typy danych
			if (o instanceof Date){
				Date d = (Date) o;
				//sb.append( (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.S")).format(d) );
				sb.append( (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(d) );
			} else if (o instanceof String && "".equals(o)){ // ''->null 
				sb.append("null");			
			}else			
				sb.append(o);
			
			sb.append("; ");
		}
		
		return sb.toString();
	}
	
	
	
	public boolean renderDevMenu( UIComponent uic ){
		return true;
	}
	
	
	public boolean renderDevMenu(){
//		 UIComponent c = UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());
//		 if (c instanceof PanelMenu){
//			 PanelMenu m = (PanelMenu) c;
//			 
//		 }
		return true;	 
	}
	

	public static void sendPush(long employeeID, String message) {
	//TODO: PF7.0 http://hantsy.blogspot.com/2017/11/jsf-23websocket-support.html
//		EventBus eventBus = EventBusFactory.getDefault().eventBus();
//		eventBus.publish("/notify/" + employeeID, new FacesMessage(FacesMessage.SEVERITY_INFO,
//				"<i class='material-icons growl'>info</i>" + "INFORMACJA",
//				message + "<div class= 'copyLink info' style='color:yellow; position:relative!important'>Kopiuj treść</div>"));


	}

//	public static void sendPush(long employeeID, String message, Severity severity) {
//		TODO: PF7.0 http://hantsy.blogspot.com/2017/11/jsf-23websocket-support.html
//		try {
//
//			EventBus eventBus = EventBusFactory.getDefault().eventBus();
//			if (severity == FacesMessage.SEVERITY_INFO)
//				eventBus.publish("/notify/" + employeeID, new FacesMessage(FacesMessage.SEVERITY_INFO,
//						"<i class='material-icons growl'>info</i>" + "INFORMACJA",
//						message + "<div class= 'copyLink info' style='color:yellow; position:relative!important'>Kopiuj treść</div>"));
//
//			if (severity == FacesMessage.SEVERITY_WARN)
//				eventBus.publish("/notify/" + employeeID, new FacesMessage(FacesMessage.SEVERITY_WARN,
//						"<i class='material-icons growl'>warning</i>" + "OSTRZEŻENIE",
//						message + "<div class= 'copyLink warning' style='color:yellow; position:relative!important'>Kopiuj treść</div>"));
//
//			if (severity == FacesMessage.SEVERITY_ERROR)
//				eventBus.publish("/notify/" + employeeID, new FacesMessage(FacesMessage.SEVERITY_ERROR,
//						"<i class='material-icons growl'>error_outline</i>" + "BŁĄD",
//						message + "<div class= 'copyLink error' style='color:yellow; position:relative!important'>Kopiuj treść</div>"));
//
//			if (severity == FacesMessage.SEVERITY_FATAL) {
//				log.debug("AppBean.sendPush:  Nieobsługiwany SEVERITY_FATAL");
//
//			}
//		} catch (Exception e) {
//			log.error(e.getMessage(), e);
//		}
//	}

	
//	public static void sendPush(String user, String message, Severity severity) {
//		//TODO: PF7.0 http://hantsy.blogspot.com/2017/11/jsf-23websocket-support.html
//		try {
//			user = user.toLowerCase();
//			EventBus eventBus = EventBusFactory.getDefault().eventBus();
//			if (severity == FacesMessage.SEVERITY_INFO)
//				eventBus.publish("/notify/" + user, new FacesMessage(FacesMessage.SEVERITY_INFO,
//						"<i class='material-icons growl'>info</i>" + "INFORMACJA",
//						message + "<div class= 'copyLink info' style='color:yellow; position:relative!important'>Kopiuj treść</div>"));
//
//			if (severity == FacesMessage.SEVERITY_WARN)
//				eventBus.publish("/notify/" + user, new FacesMessage(FacesMessage.SEVERITY_WARN,
//						"<i class='material-icons growl'>warning</i>" + "OSTRZEŻENIE",
//						message + "<div class= 'copyLink warning' style='color:yellow; position:relative!important'>Kopiuj treść</div>"));
//
//
//			if (severity == FacesMessage.SEVERITY_ERROR)
//				eventBus.publish("/notify/" + user, new FacesMessage(FacesMessage.SEVERITY_ERROR,
//						"<i class='material-icons growl'>error_outline</i>" + "BŁĄD",
//						message + "<div class= 'copyLink error' style='color:yellow; position:relative!important'>Kopiuj treść</div>"));
//
//			if (severity == FacesMessage.SEVERITY_FATAL) {
//				log.debug("AppBean.sendPush:  Nieobsługiwany SEVERITY_FATAL");
//
//			}
//		} catch (Exception e) {
//			log.error(e.getMessage(), e);
//		}
//	}



//	public static void sendPush(Msg msg) {
//		//TODO: PF7.0 http://hantsy.blogspot.com/2017/11/jsf-23websocket-support.html
//		if (msg.getUsername()==null)
//			return;
//
//		try {
//
//			String user = msg.getUsername().toLowerCase();
//			EventBus eventBus = EventBusFactory.getDefault().eventBus();
//			eventBus.publish("/notify/" + user, new FacesMessage(msg.severity(), msg.icon()+msg.tytul(), msg.getTekst() ));
//
//		} catch (Exception e) {
//			log.error(e.getMessage(), e);
//		}
//	}
	
//
//	public static void info(String user, String msg, Object... args){
//		if (args.length>0)
//			msg = String.format(msg, args);
//
//		 sendPush(user, msg, FacesMessage.SEVERITY_INFO);
//	}
//
//
//	public static void warn(String user, String msg, Object... args){
//		if (args.length>0)
//			msg = String.format(msg, args);
//
//		 sendPush(user, msg, FacesMessage.SEVERITY_WARN);
//	}
//
//	public static void alert(String user, String msg, Object... args){
//		if (args.length>0)
//			msg = String.format(msg, args);
//
//		sendPush(user, msg, FacesMessage.SEVERITY_ERROR);
//	}





	
	public static void addMessage(String message) {
		addMessage(FacesMessage.SEVERITY_INFO, message);
	}
	
	
	public static void addMessage(Severity severity, String message) {
		try {
			String now = format(new Date(), "yyyy-MM-dd HH:mm:ss");

			if (severity == FacesMessage.SEVERITY_INFO) {
				addMessage(severity, now + " INFORMACJA", message);
			} else if (severity == FacesMessage.SEVERITY_WARN) {
				addMessage(severity, now + " OSTRZEŻENIE", message);
			} else if (severity == FacesMessage.SEVERITY_ERROR) {
				addMessage(severity, now + " BŁĄD", message);
			} else if (severity == FacesMessage.SEVERITY_FATAL) {
				log.debug("AppBean.addMessage:  Nieobsługiwany SEVERITY_FATAL");
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}



	public static void addMessage(Severity severity, String title, String message) {
		try {

			FacesContext context = FacesContext.getCurrentInstance();
			if (context==null) return;

			//semafor blokujący powtarzanie tych samych komunikatów
			final String key = "AppBean.addMessage:" + severity  + ":" + message;
			final Map<String, Object> requestMap = context.getExternalContext().getRequestMap();
			if ( requestMap.get(key) != null ){
				return;
			} else {
				requestMap.put(key, LocalDateTime.now());
			}


			String growlId = "template_growl";

			String btn = "<div class=\"copyLink msgseverity p0 right\" onclick=\"copyToClipboard( $(this).prev()[0] );\"><button class=\"ui-button p0\">Kopiuj treść</button></div>";

			if (severity == FacesMessage.SEVERITY_INFO) {

				context.addMessage(growlId, new FacesMessage(FacesMessage.SEVERITY_INFO,
						"<i class='material-icons growl'>info</i>" + title,
						"<div style=\"width:100%; margin:0!important; max-height:300px!important; overflow:auto;\">"+message+"</div>"
								+ btn.replace("msgserverity","info")));
			} else if (severity == FacesMessage.SEVERITY_WARN) {
				context.addMessage(growlId, new FacesMessage(FacesMessage.SEVERITY_WARN,
						"<i class='material-icons growl'>warning</i>" + title,
						"<div style=\"width:100%; margin:0!important; max-height:300px!important; overflow:auto;\">"+message+"</div>"
								+ btn.replace("msgserverity","warning")));
			} else if (severity == FacesMessage.SEVERITY_ERROR) {
				growlId = "alert_growl";
				context.addMessage(growlId, new FacesMessage(FacesMessage.SEVERITY_ERROR,
						"<i class='material-icons growl'>error_outline</i>" + title,
						"<div style=\"width:100%; margin:0!important; max-height:300px!important; overflow:auto;\">"+message+"</div>"
								+ btn.replace("msgserverity","error")));

			} else if (severity == FacesMessage.SEVERITY_FATAL) {
				log.debug("AppBean.addMessage:  Nieobsługiwany SEVERITY_FATAL");

			}

			if (context.getCurrentPhaseId()== PhaseId.RENDER_RESPONSE){
				// Utils.executeScript("console.log('"+title+": "+message+"'); update('"+growlId+"')"); //jesli poleciał wyjątek to i tak sie nie wykona
				User u = User.getCurrentUser();
				if (u != null) {
					Msg msg = new Msg();
					msg.setSeverity(""+severity);
					msg.setSessionId(msg.getUsername());
					msg.setUsername(u.getDbLogin());
					msg.setTytul("<i class='material-icons growl'>error_outline</i>" + title);
					msg.setTekst("<div style=\"width:100%; margin:0!important; max-height:300px!important; overflow:auto;\">"+message+"</div>"
							+ btn.replace("msgserverity","error"));
					// u.newMsgs.add(msg);//nie dodawaj do kolejki komunikatow systemowych
					Push.processEncB64(msg);
				}
			} else {
				com.comarch.egeria.Utils.update(growlId);
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}

	

//	public DataSource getDs() {
//		return this.ds;
//	}
//
//	public void setDs(DataSource ds) {
//		this.ds = ds;
//		DATASOURCE = ds;
//	}



	public DataRow getDbInstance() {
		return dbInstance;
	}



	public void setDbInstance(DataRow dbInstance) {
		this.dbInstance = dbInstance;
	}


	public IDataSourceInfo getBds() {
		return bds;
	}






	public Properties getPomProperties() {
		if (this.pomProperties == null){
			this.pomProperties = loadProperties( "/META-INF/maven/com.comarch.egeria/pp/pom.properties" );
		}
		return pomProperties;
	}

	public Properties getManifestProperties() {
		if (this.manifestProperties==null){
			this.manifestProperties = loadProperties( "/META-INF/MANIFEST.MF" );
		}
		return manifestProperties;
	}

	public Properties loadProperties(String path){
		Properties ret = new Properties();

		try {

			org.springframework.core.io.Resource res = appContext.getResource(path);
			ret.load(res.getInputStream());
			
//			ExternalContext ectx = (ExternalContext) FacesContext.getCurrentInstance().getExternalContext();
//			if (ectx !=null){
//				  ServletContext servletContext = (ServletContext) ectx.getContext();
//				  URL resource = servletContext.getResource( path ); //np.  "/META-INF/maven/com.comarch.egeria/pp/pom.properties";
//				  InputStream openStream = resource.openStream();
//				  ret.load(openStream);
//			}
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			ret = null;
		}
		return ret;
	}



	public String manifestTs = null;
	public String getManifestTs() {
		if (manifestTs==null) {
			try {
				org.springframework.core.io.Resource res = appContext.getResource("/META-INF/MANIFEST.MF");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				manifestTs = sdf.format(res.getFile().lastModified());
			} catch (Exception ex) {
				manifestTs = "-";
				log.error(ex.getMessage(), ex);
			}
		}
		return manifestTs;
	}



	public String getVersion(){
		return version;
	}

	public String getBuildTime(){
		return this.getManifestProperties().getProperty("buildTimestamp");
	}
	
	public Date getStartupTime() {
		return startupTime;
	}

	public void setStartupTime(Date startupTime) {
		this.startupTime = startupTime;
	}

	
	
	public int getLogMethodsLevel() {
		return logMethodsLevel;
	}

	public void setLogMethodsLevel(int logMethodsLevel) {
		AppBean.logMethodsLevel = logMethodsLevel;
	}

	public ConcurrentHashMap<String, Long> getLogMethodsSummaryHM() {
		return logMethodsSummaryHM;
	}

	public void setLogMethodsSummaryHM(ConcurrentHashMap<String, Long> logMethodsSummaryHM) {
		AppBean.logMethodsSummaryHM = logMethodsSummaryHM;
	}

	public ConcurrentHashMap<String, Long> getLogMethodsCountHM() {
		return logMethodsCountHM;
	}

	public void setLogMethodsCountHM(ConcurrentHashMap<String, Long> logMethodsCountHM) {
		AppBean.logMethodsCountHM = logMethodsCountHM;
	}

	public static String getXhtml(String url) {
		if (url==null) 
			return null;
		
		String xhtml = null;
		if (!url.startsWith("/faces/")){
			
			if (url.startsWith("/"))
				xhtml = urlMappings.get(url);
			else
				xhtml = urlMappings.get("/"+url);
		}
		else 
			xhtml = url;
		
		return xhtml;
	}

	public ConcurrentHashMap<String, User> getSessionsUsers(){
		return sessionsUsers;
	}


	public static void removeFromSessionsUsers(String sessionId){
		if (sessionId==null) return;

		if (eq(getSessionId(), sessionId)){
			try {
				//com.comarch.egeria.Utils.executeScript(" if (localStorage.getItem(usGUID) != null){ localStorage.removeItem(usGUID); }");
				User user = AppBean.sessionsUsers.remove(sessionId);
				if (user!=null) {
					com.comarch.egeria.Utils.executeScript(" if (localStorage.getItem('" + user.getUserSessionGUID() + "') != null){ localStorage.removeItem('" + user.getUserSessionGUID() + "'); }");
				}
			} catch (Exception ex){
				log.error( ex.getMessage());
			}
		}
		AppBean.sessionsUsers.remove(sessionId);
		if (AppBean.sessionsUsers.isEmpty()){
			synchronized (AppBean.app) {
				Push.wndguidToSessionId.clear();
				Push.usersSessions.clear();
			}
		}
	}


	public File getApplicationContextFolder() {
		return applicationContextFolder;
	}

	public File getFileLogoFirmy() {
		return fileLogoFirmy;
	}

	public File getFileLogoRpt() {
		return fileLogoRpt;
	}


	public String getRelativePath(File file){
		String ret = applicationContextFolder.toPath().relativize( file.toPath()  ).toString().replace("\\","/");
		if (!ret.startsWith("/")) ret = "/" + ret;
		return ret;
	}

	public String getWebappsRelativePath(File file){
		String ret = applicationContextFolder.getParentFile().toPath().relativize( file.toPath()  ).toString().replace("\\","/");
		if (!ret.startsWith("/")) ret = "/" + ret;
		return ret;
	}


	public ConcurrentHashMap<String, ConcurrentHashMap<String, String>> getViewResourceFilesCHM() {
		return viewResourceFilesCHM;
	}

	public ConcurrentHashMap<String, String> getViewResourceFilesCHM(String viewId){
		return viewResourceFilesCHM.get(viewId);
	}

	public List<Map.Entry<String, String>> getViewResourceFileNames(String viewId){
		ConcurrentHashMap<String, String> vret = viewResourceFilesCHM.get(viewId);
		if (vret!=null)
			return vret.entrySet().stream().collect(Collectors.toList());
		else
			return new ArrayList<>();
	}


	public static SqlDataSelectionsHandler sql(String sqlId, String sql, Object... params){
		SqlDataSelectionsHandler ret = sqlBean.getSql(sqlId, sql, params);
		return ret;
	}


	public DataRow getPpKonfiguracjaDR(String parametr){
		return Utils.konfiguracjaDR(parametr);
	}

	public String getPpKonfiguracjaWartoscZewn(String parametr){
		return Utils.konfiguracjaWartoscZewn(parametr);
	}

	public String getPpKonfiguracjaWartoscMax(String parametr){
		return Utils.konfiguracjaWartoscMax(parametr);
	}

	public String getPpKonfiguracjaOpis(String parametr){
		return Utils.konfiguracjaOpis(parametr);
	}
	public String getPpKonfiguracjaOpis2(String parametr){
		return Utils.konfiguracjaOpis2(parametr);
	}
	public String getPpKonfiguracjaOpis3(String parametr){
		return Utils.konfiguracjaOpis3(parametr);
	}

	public String getPpKonfiguracjaAlias(String parametr){
		return Utils.konfiguracjaAlias(parametr);
	}
	public String getPpKonfiguracjaAlias2(String parametr){
		return Utils.konfiguracjaAlias2(parametr);
	}
	public String getPpKonfiguracjaAlias3(String parametr){
		return Utils.konfiguracjaAlias3(parametr);
	}

	public String getPpKonfiguracjaAdminAdresMail(){
		String ret = getPpKonfiguracjaOpis("PP_ADMIN_ADRES_MAIL");
		ret = nvl(ret,"egeria.serwis@comarch.pl");
		return ret;
	}


	public DataSourceBase getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSourceBase dataSource) {
		this.dataSource = dataSource;
	}

	private static void dodanieMenuSTD(){
		try (HibernateContext h = new HibernateContext()) {
			List<PptAdmMenu> mnuTopLst = HibernateContext.query(h, "from PptAdmMenu where MNU_ID = 1");
			if (mnuTopLst.isEmpty()) {
				Connection conn = h.getConnection();
				JdbcUtils.sqlExecNoQuery(conn, "DELETE FROM PPT_JPA_GENKEYS WHERE PKEY_TABLE = 'PPT_ADM_MENU'");

				//rodzic calego menu
				PptAdmMenu mnuTop = new PptAdmMenu();
				mnuTop.setMnuRendered("N");
				mnuTop.setMnuAjax("N");
				mnuTop.setMnuKafelekBigSize("N");
//				HibernateContext.saveOrUpdate(h, mnuTop, true);

				//element menu "Panel administratora"
				PptAdmMenu mnuPanelAdministratora = new PptAdmMenu();
				mnuPanelAdministratora.setMnuLabel("Panel administratora");
				mnuPanelAdministratora.setMnuIcon("build");
				mnuPanelAdministratora.setMnuLp(1L);
				mnuPanelAdministratora.setMnuRendered("T");
				mnuPanelAdministratora.setMnuAjax("N");
				mnuPanelAdministratora.setMnuKafelekBigSize("N");
				mnuTop.addPptAdmMenus(mnuPanelAdministratora);

				//element podmenu "Konfiguracja menu"
				PptAdmMenu mnuKonfiguracjaMenu = new PptAdmMenu();
				mnuKonfiguracjaMenu.setMnuLabel("Konfiguracja menu");
				mnuKonfiguracjaMenu.setMnuUrl("KonfiguracjaMenu");
				mnuKonfiguracjaMenu.setMnuLp(1L);
				mnuKonfiguracjaMenu.setMnuRendered("T");
				mnuKonfiguracjaMenu.setMnuAjax("N");
				mnuKonfiguracjaMenu.setMnuKafelekBigSize("N");
				mnuPanelAdministratora.addPptAdmMenus(mnuKonfiguracjaMenu);

				HibernateContext.saveOrUpdate(h, mnuTop, true);
			}
			//PptAdmMenu mnuTop = (PptAdmMenu) HibernateContext.get(PptAdmMenu.class, 1L);
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
		}
	}


}