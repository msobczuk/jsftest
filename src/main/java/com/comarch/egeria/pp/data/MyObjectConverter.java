package com.comarch.egeria.pp.data;

import java.sql.Timestamp;
import java.util.TimeZone;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("myObjectConverter")
public class MyObjectConverter implements Converter  {
	
	MyNumConverter numCnv = new MyNumConverter();
	MyDateTimeConverter dtCnv = new MyDateTimeConverter();

	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {

		
		String pattern = (String) component.getAttributes().get("pattern");
//		if (value instanceof Timestamp && (pattern ==null || pattern.isEmpty())) pattern = "yyyy-MM-dd";
		
		//liczba
		if (value instanceof Number)
			return numCnv.getAsString(context, component, value);
		
		//data
		if (value instanceof Timestamp){
			dtCnv.setTimeZone(TimeZone.getDefault());
			String ret = dtCnv.getAsString(context, component, value);
			return ret;
		}
		
		return ""+value;
	}


}