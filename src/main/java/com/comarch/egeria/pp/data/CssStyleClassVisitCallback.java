package com.comarch.egeria.pp.data;

import javax.faces.component.UIComponent;
import javax.faces.component.visit.VisitCallback;
import javax.faces.component.visit.VisitContext;
import javax.faces.component.visit.VisitResult;
import java.util.ArrayList;
import java.util.List;

public class CssStyleClassVisitCallback implements VisitCallback {
//Efficient component tree traversal in JSF
//http://ovaraksin.blogspot.com/2011/12/efficient-component-tree-traversal-in.html

    String cssClass = null;
    public CssStyleClassVisitCallback(String cssClass){
        this.cssClass = cssClass;
    }

    private List<UIComponent> myComponents = new ArrayList<>();

    public List<UIComponent> getMyComponents() {
        return myComponents;
    }


    @Override
    public VisitResult visit(VisitContext context, UIComponent target) {

        if (!target.isRendered() || cssClass==null) {
            return VisitResult.REJECT;
        }

        String styleClass = (String) target.getAttributes().get("styleClass");
        if (styleClass != null && (" "+ styleClass+ " ").contains(" "+cssClass+" ")){
            myComponents.add(target);
        }

        return VisitResult.ACCEPT;
    }

}
