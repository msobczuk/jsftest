package com.comarch.egeria.pp.data;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.el.MethodExpression;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UINamingContainer;
import javax.faces.context.FacesContext;


@FacesComponent
public class CLovButtonJS extends UINamingContainer {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

//http://jugojava.blogspot.com/2011/09/jsf-composite-component-binding-to.html
	private UIInput val;

	public void actionListener(javax.faces.event.AjaxBehaviorEvent ae) {
		MethodExpression me = (MethodExpression)this.getAttributes().get("rowSelectedAction");
		if (me!=null){
			SqlDataSelectionsHandler datasource = (SqlDataSelectionsHandler) this.getAttributes().get("datasource");
			Object[] params = new Object[1];
			params[0] = datasource.find( val.getValue() );
			me.invoke( FacesContext.getCurrentInstance().getELContext(), params);
		}
	}


	public void btnlvActionListener(){
		MethodExpression me = (MethodExpression)this.getAttributes().get("rowSelectedAction");
		if (me!=null){
			SqlDataSelectionsHandler datasource = (SqlDataSelectionsHandler) this.getAttributes().get("datasource");
			Object[] params = new Object[1];
			params[0] = datasource.find( val.getValue() );
			me.invoke( FacesContext.getCurrentInstance().getELContext(), params);
		}
	}

	public UIInput getVal() {
		return val;
	}

	public void setVal(UIInput val) {
		this.val = val;
	}


	public void init(){
		//this.getFacetsAndChildren() --jest "val"
		this.setVal ( (UIInput) this.findComponent("val") ); //alternatywa dla wpisu binding="#{cc.val}"
	}



}
