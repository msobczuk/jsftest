package com.comarch.egeria.pp.data;

import org.apache.commons.io.input.ReversedLinesFileReader;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.comarch.egeria.Utils.round;

//import org.primefaces.context.RequestContext;

@ManagedBean 
@Named
@Scope("view")
public class LogsBean extends SqlBean {

	
	private java.nio.file.Path pthTomcat;
	private String ipAddress = "";
	private String serverName = "";
	private int serverPort;
	
	private List<File> logFiles = new ArrayList<>();
	private File currentFile = null;
	private ArrayList<String> lines = new ArrayList<>();
	
	

	

	
	
	@PostConstruct
	public void init(){
		String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
		setPthTomcat(Paths.get(realPath).getParent().getParent());
		reloadLogFiles();

		String initLogAttr = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("log");
		 if (initLogAttr==null)
			 initLogAttr = "catalina.out";
		
		 String log0  = initLogAttr.toLowerCase();
		
		if (log0!=null){
			File logFile = this.logFiles.stream().filter(f->f.getName().toLowerCase().equals(log0)).findFirst().orElse(null);
			if (logFile!=null)
				tail(logFile.toPath(), 1000);
				com.comarch.egeria.Utils.executeScript(""
						+ " var el = document.getElementById('frmCntr'); "
						+ " el.scrollTop = el.scrollHeight - el.clientHeight;"
						+ "");
		}
		
		try {
			setIpAddress(getIpAddress() + InetAddress.getLocalHost().getHostAddress());
		} catch (Exception e) {
			//System.out.println(e);
		}
		FacesContext ctx = FacesContext.getCurrentInstance();
		ExternalContext ectx = ctx.getExternalContext();
		this.serverName = ectx.getRequestServerName();
		this.setServerPort(ectx.getRequestServerPort());
	}


	public long getCurrentFileLength(){

		try {
			File f = this.getCurrentFile();
			if (f==null) return 0L;
			return f.length();
		} catch ( Exception ex){
			return -1L;
		}

	}

	
	public String linesToText(){
		if (currentFile==null)
			return "";
		
		return  getIpAddress() + " / "	+ getServerName() + ":" +this.serverPort + " / " 
				+ currentFile.getAbsolutePath() + "\r\n... ... ...[tail 1000]:\r\n" + String.join("\r\n", lines );
	}

	
	
	
	
	public void downloadLines() throws IOException {
		byte[] bytes = this.linesToText().getBytes(StandardCharsets.UTF_8);
		
	    FacesContext fc = FacesContext.getCurrentInstance();
	    HttpServletResponse response = (HttpServletResponse) fc.getExternalContext().getResponse();

	    response.reset(); // Some JSF component library or some Filter might have set some headers in the buffer beforehand. We want to get rid of them, else it may collide.
	    //response.setContentType(contentType); // Check http://www.iana.org/assignments/media-types for all types. Use if necessary ServletContext#getMimeType() for auto-detection based on filename.
	    response.setContentLength(bytes.length); // Set it with the file size. This header is optional. It will work if it's omitted, but the download progress will be unknown.
	    
	    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH.mm.ss");
	    response.setHeader("Content-Disposition", "attachment; filename=\"" + getIpAddress() + " "+  LocalDateTime.now().format(formatter) + " - catalina.out.txt\""); // The Save As popup magic is done here. You can give it any file name you want, this only won't work in MSIE, it will use current request URL as file name instead.
	    ServletOutputStream output = response.getOutputStream();
	    // Now you can write the InputStream of the file to the above OutputStream the usual way.
		output.write(bytes);
	    
	    fc.responseComplete(); // Important! Otherwise JSF will attempt to render the response which obviously will fail since it's already written with a file and closed.
	}
		
	

	public void reloadLogFiles() {
		try {
			
			logFiles.clear();
			currentFile = null;
			lines.clear();

			//Stream<java.nio.file.Path> filesStream = Files.walk(Paths.get(getPthTomcat().toString() + "/logs/")); //recursive
			Stream<java.nio.file.Path> filesStream = Files.list(Paths.get(getPthTomcat().toString() + "/logs/")); //not recursive
			
			this.logFiles = filesStream
				.map(p-> new File(p.toString()) )
				.filter(f->f.isFile() && f.length()>0)
				.sorted(Comparator.comparingLong(File::lastModified).reversed())
				.collect(Collectors.toList());
			

		} catch (Exception e) {
			this.lines.add(0, e.getMessage());
		}
	}
	
	
	public void tail(java.nio.file.Path pth, int linesCnt){
		File file = new File(pth.toString());
		tail(file, linesCnt);
	}
	
	public void tail(File file, int linesCnt){
		this.currentFile = file;
		lines.clear();
		try {
			int cnt = 0;
			ReversedLinesFileReader revLR = new ReversedLinesFileReader(file);
			while(cnt < linesCnt)
			{
				this.lines.add(0, revLR.readLine());
			    cnt++;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			this.lines.add(e.getMessage());
		}
	}
	
	
	
	
	
	
	public Date toDate(long ts){
		return new Date(ts);
	}

	
	public Double size(java.nio.file.Path pth){
		try {
			File f = new File(pth.toString());
			Double sizeKB = (0.0D + f.length()) / 1024;
			return round(sizeKB, 4);
		} catch (Exception e) {
		}
		
		return -1D;
	}
	
	
	public java.nio.file.Path getPthTomcat() {
		return pthTomcat;
	}


	public void setPthTomcat(java.nio.file.Path pthTomcat) {
		this.pthTomcat = pthTomcat;
	}




	public ArrayList<String> getLines() {
		return lines;
	}



	public void setLines(ArrayList<String> lines) {
		this.lines = lines;
	}


	public List<File> getLogFiles() {
		return logFiles;
	}


	public void setLogFiles(List<File> logFiles) {
		this.logFiles = logFiles;
	}




	public File getCurrentFile() {
		return currentFile;
	}




	public void setCurrentFile(File currentFile) {
		this.currentFile = currentFile;
	}


	public String getIpAddress() {
		return ipAddress;
	}


	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}


	public String getServerName() {
		return serverName;
	}


	public void setServerName(String serverName) {
		this.serverName = serverName;
	}





	public int getServerPort() {
		return serverPort;
	}





	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}
	
}
