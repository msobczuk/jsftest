package com.comarch.egeria.pp.data;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

@ManagedBean 
@Named
@Scope("view")
public class CLTMultiselectBean extends SqlBean{

	
	SqlDataSelectionsHandler sql;
	private LazySqlDataModel lazySqlDataModel;
	
	
	
	@PostConstruct
	public void init(){

//		sql = new SqlDataSelectionsHandler(); //
//		sql.setShortName("pracidx");
//		sql.setSql("select distinct pri_praid as prc_id, pri_imie1 as prc_imie, pri_nazwisko as prc_nazwisko from cdn.pracidx");
//		sql.setContextDependent(false);
		
		sql = this.getLW("PPL_KAD_PRACOWNICY");
		
		int size = sql.getData().size();
		this.lazySqlDataModel = new LazySqlDataModel(sql);
		int rowCount = this.getLazySqlDataModel().getRowCount();
		
	}



	public LazySqlDataModel getLazySqlDataModel() {
		return lazySqlDataModel;
	}



	public void setLazySqlDataModel(LazySqlDataModel lazySqlDataModel) {
		this.lazySqlDataModel = lazySqlDataModel;
	}



	
}
