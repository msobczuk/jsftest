

package com.comarch.egeria.pp.data.msg;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.eq;
import static com.comarch.egeria.Utils.nz;


/**
 * klasa do obsługi komunikacji przez websockety (o ile w sieci lokalnej ruch do nich nie jest wycinany)
 */
@ServerEndpoint("/push")
public class Push {
	public static final Logger log = LogManager.getLogger();
	
	// przeczytaj plik  /pp/sql/PLSQL.http.WEBSOCKET SMPL.sql
	
	//based on https://stackoverflow.com/questions/25947790/real-time-updates-from-database-using-jsf-java-ee
	//private static final Set<Session> SESSIONS = ConcurrentHashMap.newKeySet();
	
	public static  ConcurrentHashMap<String,  Set<Session>> usersSessions = new ConcurrentHashMap<>(); //UsersSessions

	public static  ConcurrentHashMap<String,  String> wndguidToSessionId = new ConcurrentHashMap<>(); //WND_GUID to SESSIONID


	public static String getPushWebSocketUrl(String windowGuid, boolean wss){

		FacesContext ctx = FacesContext.getCurrentInstance();
		if (ctx==null) return "";
		ExternalContext ectx = ctx.getExternalContext();
		if (ectx==null) return "";

		final String sessionId = ectx.getSessionId(false);

		wndguidToSessionId.put(windowGuid, sessionId);

		String ret = String.format("%1$s://%2$s:%3$s%4$s/push?wnd=%5$s",
				wss ? "wss" : "ws",//protokół
				ectx.getRequestServerName(),
				ectx.getRequestServerPort(),
				ectx.getRequestContextPath(),
				windowGuid
		);

		return ret;
	}


	private User getUser(Session session) {
		URI requestURI = session.getRequestURI();
		String qstr = requestURI.getQuery();
		String wndguid = qstr.substring(4);

		String sessionId = wndguidToSessionId.get(wndguid);
		if (sessionId!=null){
			return AppBean.getUser(sessionId);
		}


//		String sessionId = null;
//		if (qstr!=null && qstr.startsWith("id=")) {
//			sessionId = qstr.substring(3,35); //'id=6A351461AA1E76CCE9B092554F1CBDCA&wnd=030B4A82-1B7C-11CF-9D53-00AA003C9CB6'
//			return AppBean.getUser(sessionId);
//		} else
//			return null;

		return null;
	}





	@OnOpen
	public void onOpen(Session session) {
		User user = getUser(session);
		String userUC = "WEBSOCKET_GUEST";
		if (user!=null)
			userUC = user.getDbLogin().toUpperCase();

		Set<Session> sessions = usersSessions.get(userUC);
		if (sessions == null) {
			sessions = ConcurrentHashMap.newKeySet();
			usersSessions.put(userUC, sessions);
		}
		sessions.add(session);

    }


	@OnClose
	public void onClose(Session session) {
		URI requestURI = session.getRequestURI();
		String qstr = requestURI.getQuery();
 		String wndguid = qstr.substring(4);

		User user = getUser(session);
		String userUC = "WEBSOCKET_GUEST";
		if (user!=null)
			userUC = user.getDbLogin().toUpperCase();
		
		Set<Session> usessions = usersSessions.get(userUC);
		if (usessions==null || usessions.isEmpty()) return;
		usessions.remove(session);
		wndguidToSessionId.remove(wndguid);
    }
	
	
	
	
	
	@OnError
    public void onError(Throwable t) {
		
		String message = t.getMessage();
		if (message!=null && message.contains("java.io.IOException: Nawiązane połączenie zostało przerwane przez oprogramowanie zainstalowane w komputerze-hoście"))
			return;
		else
			log.error(t.getMessage(), t);
    }
 
	
	public static boolean hasSession(String user){
		user = ""+user.toUpperCase();
		Set<Session> usessions = usersSessions.get(user);
		return !(usessions==null || usessions.isEmpty());
	}
	
	
	 public static void push(String user, String temat, String tekst, String severity){
		 if (user==null || temat==null) return;

//			"<i class='material-icons growl'>info</i>" + "INFORMACJA", message + "<div class= 'copyLink info' style='color:yellow; position:relative!important'>Kopiuj treść</div>";
		 if (temat.contains("!!!MSGLOG!!!")){
				System.out.println((new Date()) + "...push... -> user: " + user + " temat: " + temat + " tekst: "+ tekst + " severity: "  + severity + " ...");
		 }
		 
		 String json = javax.json.Json.createObjectBuilder()
				 .add("summary",temat)
				 .add("detail", tekst)
				 .add("severity", severity)
				 .build().toString();
		 
		 synchronized (usersSessions) {
			 
			 if (temat.contains("!!!MSGLOG!!!")){
					System.out.println("...usersSessions.keySet(): ");
					for (String ukey : usersSessions.keySet()) {
						System.out.println(ukey);
					}
			 }
			 
			 
			 ArrayList<String> usrs = new ArrayList<String>();
			 
			 if (!"%".equals(user))
				 usrs.add(user.toUpperCase());
			 else
				 usrs.addAll(usersSessions.keySet());
			 
	 		for (String u : usrs) {
			
	 			Set<Session> usessions = usersSessions.get(u);
				if (usessions==null || usessions.isEmpty()) return;
				
				for (Session session : usessions) {
					if (session.isOpen()) {
						try {
							session.getAsyncRemote().sendText(json);	
						} catch (Exception e) {
							if (e instanceof java.lang.IllegalStateException)
								log.debug("!!!websocket / sesja zamknięta?" + e.getMessage());
							else
								log.error(e.getMessage(), e);
						}
					}
				}	
			}
			 
		 }
		 
		 if (temat.contains("!!!MSGLOG!!!"))
				System.out.println((new Date()) + "...push... END");
	 } 
	 
	 
	 
	 
		public static void process(String txt){
			if (txt==null || txt.isEmpty()) return;
			
			Msg msg = new Msg(txt);
			
			System.out.println(msg.getSeverity());
			
			
			String[] split = txt.split("\n");
			
			if ( split.length>=5 
				  && ("LOGOUT".equals(split[0]) || "ERROR".equals(split[0]) || "WARN".equals(split[0]) || "INFO".equals(split[0]))
				){
				
//				  v_data :=  P_SEVERITY   || chr(10)
//				          || p_wdm_id     || chr(10)
//				          || P_USER       || chr(10)
//				          || v_temat      || chr(10)
//				          || v_tekst;
				
				String severity = split[0];
				String wdm_id = split[1];
				String user = split[2];
				String temat = split[3];
				String tekst = split[4];
				
				for (int i=5; i<split.length; i++){
					tekst += "<br/>" + split[i];
				}
				
				push(user, temat, tekst, severity );
				
			}
			
		}
		
		
		public static void process(Msg msg){
			if (msg==null) return;
			//push(msg.getUsername(), msg.getTytul(), msg.getTekst(), msg.getSeverity() );
			String temacikWs = "LOGOUT".equals(msg.getSeverity()) ? msg.tytul() : "-";

			if (eq("%", msg.getUsername())) {//do wszystkich....
				for (String u :usersSessions.keySet()) {
					push(u, temacikWs, "-", msg.getSeverity());
				}
			} else {
				push(msg.getUsername(), temacikWs, "-", msg.getSeverity());//nie pchamy na ws niepewnych tekstów -np. polskich ogonków, bo możemy dostać w przgl. blad typu malformed exception
			}
		}

		public static void processEncB64(Msg msg)  {
			if (msg==null) return;
			String titleB64 = null;
			titleB64 = Base64.getEncoder().encodeToString( nz(msg.getTytul()).getBytes(StandardCharsets.UTF_8) );
			String txtB64 = Base64.getEncoder().encodeToString( nz(msg.getTekst()).getBytes(StandardCharsets.UTF_8) );
			push(msg.getUsername(), titleB64, txtB64, msg.getSeverity() );
		}

		public static List<String> getConnectedWebsocketsInfo(){
			ArrayList<String> ret = new ArrayList<>();
			 synchronized (usersSessions) {
				for (String ukey : usersSessions.keySet()) {
					Set<Session> set = usersSessions.get(ukey);
					List<String> collect = set.stream().map(s->s.toString()).collect(Collectors.toList());
					String txt = String.format("%1$s - %2$s", ukey, "" + String.join("; ", collect) );
					ret.add(txt);
				}
			 }
			return ret;
		}
		

		

}

