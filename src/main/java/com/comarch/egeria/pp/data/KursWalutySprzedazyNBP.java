package com.comarch.egeria.pp.data;

import com.comarch.egeria.Utils;

import java.util.Date;

import static com.comarch.egeria.Utils.eq;

public class KursWalutySprzedazyNBP extends KursWaluty{

    public KursWalutySprzedazyNBP(Number walId, Date naDzien){
        super(walId, naDzien);

        DataRow r = Utils.kursPrzelicznikSprzedazyNBPRow(walId, naDzien);
        if (r!=null) {
            String txt = String.format("Przelicznik sprzedaży NBP %3$s %1$s/PLN z dnia %2$s", this.getWalSymbol(), Utils.format(r.getAsDate("kw_data")), Utils.format(this.getJednostka(),"#0"));
            if (!eq(Utils.format(r.getAsDate("kw_data")), Utils.format(this.naDzien)))
                txt+= String.format(" (akt. na %1$s)", Utils.format(this.naDzien));

            this.setOpis(txt);
            this.setDataKursu(r.getAsDate("KW_DATA"));
            this.setPrzelicznik(r.getAsDouble("KW_PRZELICZNIK_SPRZEDAZY"));
        } else {
            this.setOpis(String.format("Brak przelicznika NBP %1$s/PLN sprzedaży na dzień %2$s ", this.getWalSymbol(), Utils.format(this.naDzien)));
        }
    }
}
