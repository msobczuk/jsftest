package com.comarch.egeria.pp.data;

import java.io.Serializable;

public class DataRowHolder implements Serializable {

	private boolean selected = false;
	private SqlDataSelectionsHandler lw = null;//ref. lw i tak jest utrzymywana na poziomie session scope
	private String pk = null;//klucz glowny wiersza

//	private DataRow row = null;//nie chcemy tu trzymac referencji do wiersza, bo po przeladowaniu danych lw mamy niaktualny wiersz (ktorego nie ma juz w lw)

	public DataRow getRow() {
		if (lw==null) return null;
		return lw.get(pk);
	}

	public void setRow(DataRow row) {
		//		this.row = row;
		if (row!=null &&  row.getSqlData() instanceof SqlDataSelectionsHandler){
			lw = (SqlDataSelectionsHandler) row.getSqlData();
			pk = row.getIdAsString();// ""+ row.get(0);
		} else {
			lw=null;
			pk=null;
		}
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
		lw.toggleSelected = null;

		DataRow row = this.getRow();

		if (row!=null) {
			lw.getSelectedRows().remove(row); //wywalamy zaznaczenie
			if (selected) {
				lw.getSelectedRows().add(0, row);//ustawiamy jako piwrwszy el. w zaznaczonych
			}
		}
	}
	
}
