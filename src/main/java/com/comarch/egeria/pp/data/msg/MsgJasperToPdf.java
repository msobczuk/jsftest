package com.comarch.egeria.pp.data.msg;

import com.comarch.egeria.Params;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import javax.sql.rowset.CachedRowSet;
import java.io.PipedInputStream;
import java.sql.Connection;

import static com.comarch.egeria.Utils.nz;


public class MsgJasperToPdf implements Runnable {
	public static final Logger log = LogManager.getLogger();
	public static DataSource ds;
	public static String txt = "";
	
	
	public MsgJasperToPdf(DataSource ds, String txt){
		MsgJasperToPdf.ds = ds;
		MsgJasperToPdf.txt = nz(txt);
	}
	
	
	@Override
	public void run() {

		if (txt.startsWith("JASPERTOPDF_ZALACZNIK")) 	doJasperToPdfZalacznik();



//				System.out.println("Działa");
//				Params params = new Params("wnd_id", 481L);
//				FileOutputStream fileOut = new FileOutputStream("c:\\tmp\\test_rpt.pdf");
//
//				PipedInputStream stream = ReportGenerator.externalTaskPdfAsStream(fileOut, "Delegacja_zagraniczna_wniosek", params);
////				fileOut.flush();
////				fileOut.close();
//		System.out.println("DONE");


	}

	private void doJasperToPdfZalacznik() {
		log.debug("doJasperToPdfZalacznik - zlecenie ... / " + txt);

		try ( Connection conn = ds.getConnection() ) {
			String sZalId = "";
			String sWndId = "";
			String sWndNumer = "";
			String sDokId = "";
			String sNazwaRaportu = "";

			final String[] arr = txt.split("\n");
			for (String s : arr) {
//				if (s.startsWith("P_ZAL_ID=")) sZalId = s.substring(s.indexOf("=")+1).trim();
//				if (s.startsWith("P_WND_ID=")) sWndId = s.substring(s.indexOf("=")+1).trim();
				if (s.startsWith("P_DOK_ID=")) sDokId = s.substring(s.indexOf("=")+1).trim();
//				if (s.startsWith("P_NAZWA_RAPORTU=")) sNazwaRaportu = s.substring(s.indexOf("=")+1).trim();
			}

			//if (nz(sZalId).isEmpty() ) return; //nie spelniono war, brzegowych
			if (nz(sDokId).isEmpty() ) return; //nie spelniono war, brzegowych
			//if (nz(sWndId).isEmpty() ) return; //nie spelniono war, brzegowych
			//if (nz(sNazwaRaportu).isEmpty() ) return; //nie spelniono war, brzegowych

			int rCnt = JdbcUtils.sqlExecUpdate(conn, 
					" UPDATE ZKT_ZALACZNIKI SET ZAL_NAZWA_PLIKU = '...'||ZAL_NAZWA_PLIKU WHERE ZAL_DOK_ID = ? and zal_blob is null and not ZAL_NAZWA_PLIKU like '...%' ", sDokId);//lock/semafor....
			if (rCnt<1) return;//inny serwer/watek podjał to zadanie


			CachedRowSet crs = JdbcUtils.sqlExecQuery("" +
							"-->ZAL--DKZ-KAL->WND\n" +
							"select " +
							"  zal_id, zal_nazwa_pliku, wnd_id, wnd_numer, dkz_id\n" +
							"from zkt_zalaczniki \n" +
							"    join ppt_del_zaliczki on zal_dok_id = dkz_dok_id \n" +
							"    join ppt_del_kalkulacje on dkz_kal_id = kal_id  \n" +
							"    join ppt_del_wnioski_delegacji on kal_wnd_id = wnd_id \n" +
							"where zal_blob is null " +
							" and ZAL_NAZWA_PLIKU like '...%' " +
							" and zal_dok_id = ? \n" +

							"UNION \n" +

							"-->ZAL--(DEK lub DEZ)->WND\n" +
							"select zal_id, zal_nazwa_pliku, wnd_id, wnd_numer, null\n" +
							"from zkt_zalaczniki \n" +
							"join  ppt_del_wnioski_delegacji on zal_dok_id = wnd_dok_id\n" +
							"where zal_blob is null " +
							" and ZAL_NAZWA_PLIKU like '...%' " +
							" and zal_dok_id = ? "
					, sDokId, sDokId );


			while (crs.next()) {
				sZalId = nz(crs.getString("ZAL_ID"));
				sWndId = nz(crs.getString("WND_ID"));
				sNazwaRaportu = nz(crs.getString("ZAL_NAZWA_PLIKU"))
						//.replace(".pdf", "")
						.replace("...", "");
				sNazwaRaportu = sNazwaRaportu.substring(0, sNazwaRaportu.indexOf("."));//odcinamy .pdf albo odcinamy .{WND_NUMER}.pdf

				sWndNumer = nz(crs.getString("wnd_numer"));
				if (sZalId.isEmpty() ) continue; //nie spelniono war, brzegowych
				if (sNazwaRaportu.isEmpty() ) continue; //nie spelniono war, brzegowych
				if (sWndId.isEmpty() ) continue; //nie spelniono war, brzegowych




				String zalNazwaPliku = String.format("%1$s.%2$s.pdf",sNazwaRaportu, sWndNumer);


				zalNazwaPliku = zalNazwaPliku
						.replace("\\", "_")
						.replace("/", "_")
						.replace(":", "_")
						.replace("?", "_")
						.replace("*", "_")
						.replace("\"", "_")
						.replace("<", "_")
						.replace(">", "_")
						.replace("|", "_");

				Params params = new Params("wnd_id", Long.parseLong(sWndId));
				PipedInputStream stream = ReportGenerator.pdfAsStream(sNazwaRaportu, params);
				rCnt = JdbcUtils.sqlExecUpdate(conn, "UPDATE ZKT_ZALACZNIKI SET ZAL_NAZWA_PLIKU = ?, ZAL_BLOB = ? WHERE ZAL_ID = ?", zalNazwaPliku, stream, sZalId);//...unlock/semafor
				log.debug("doJasperToPdfZalacznik, wygenerowano wydruk / zalNazwaPliku: " + zalNazwaPliku + " / zal_id: "+ sZalId);
			}
			log.debug("doJasperToPdfZalacznik, zrealizowano zlecenie / " + txt);
		}
			catch (Exception e) {
			log.error(e.getMessage(), e);
		}

	}
}
	

	
