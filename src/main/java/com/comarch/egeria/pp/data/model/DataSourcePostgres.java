package com.comarch.egeria.pp.data.model;
import com.comarch.egeria.pp.data.JdbcUtils;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.sql.Connection;
import java.sql.SQLException;

@Entity
public class DataSourcePostgres extends DataSourceBase {

    @Override
    public String getHibernateDialect() {
        return "org.hibernate.dialect.PostgreSQLDialect";
    }

    @Transient
    private final String formatStringSqlToLimitRowsLW = "select * from ( -- %1$s "
            + "\r\n---------------------------------------------------------------"
            + "\r\n%2$s"
            + "\r\n---------------------------------------------------------------"
            + "\r\n)x FETCH FIRST %3$s ROWS ONLY";

    @Override
    public String formatSqlToLimitRows(String sqlName, String sql, int limitToRows) {
        String sqlLimit1MRows = String.format(
                formatStringSqlToLimitRowsLW
                , sqlName, sql, limitToRows );
        return sqlLimit1MRows;
    }

    @Override
    public String getSysdate() {
        return "now()::timestamp";
    }

    @Override
    public Connection getConnection(Boolean setContext) throws SQLException {
        Connection conn = this.getDs().getConnection();
        JdbcUtils.addActiveConnInfo(conn);

/*        try {
            if (setContext) JdbcUtils.setContext(conn);
        } catch (Exception e) {
            conn.close();
            throw e;			//log.error(e.getMessage(), e);
        }*/

        //else clearContext();
        return conn;
    }


}
