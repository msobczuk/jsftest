package com.comarch.egeria.pp.data;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.DateTimeConverter;
import javax.faces.convert.FacesConverter;

@FacesConverter("myDateTimeConverter")
public class MyDateTimeConverter extends DateTimeConverter {

//	public MyDateTimeConverter() {
//		setPattern("yyyy-MM-dd hh:mm:ss");
//	}
	

//	@Override
//	public Object getAsObject(FacesContext context, UIComponent component, String value) {
//		if (value != null && value.length() != getPattern().length()) {
//			throw new ConverterException("Invalid format");
//		}
//
//		return super.getAsObject(context, component, value);
//	}
	
	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		String pattern = (String) component.getAttributes().get("pattern");
		if (pattern ==null || pattern.isEmpty())
			pattern = "yyyy-MM-dd";
		setPattern(pattern);
		return super.getAsObject(context, component, value);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		String pattern = (String) component.getAttributes().get("pattern");
		if (pattern ==null || pattern.isEmpty())
			pattern = "yyyy-MM-dd";
		setPattern(pattern);
		return super.getAsString(context, component, value);
	}


}