package com.comarch.egeria.pp.data;

import java.util.Date;
import java.util.Objects;

import javax.el.ValueExpression;
import javax.faces.component.FacesComponent;
import javax.faces.component.UINamingContainer;
import javax.faces.context.FacesContext;


@FacesComponent
public class CCalendarTime  extends UINamingContainer {
	
	
	
	private Date data;
	private Date godzina;
	private Object oValue;
	
	ValueExpression valueExpression;
	
    public void init() {
    	System.out.println("CCalendarTime.init...");
    	
    	oValue = getAttributes().get("value");
    	data = (Date) oValue;
    	godzina = (Date) oValue;
    	valueExpression = getValueExpression("value");
    	
    	
//    	setDatasource((SqlDataSelectionsHandler) getAttributes().get("datasource"));
//        table.setVar((String) getAttributes().get("var"));
        
        //table.getRowData()
        //table.getRowIndex()
        //table.getRowIndexVar()
        System.out.println("...CCalendarTime.init");
        
    }
    
    
    @Override
    public void processUpdates(FacesContext context)
    {
        Objects.requireNonNull(context);

        if(!isRendered())
        {
            return;
        }
       
        super.processUpdates(context);
        
        try
        {
        	Date value = new Date();
			valueExpression.setValue(context.getELContext(), data);
        }
        catch(RuntimeException e)
        {
            context.renderResponse();
            throw e;
        }
        
        
        
    }
    
    
    
    
    

	public Date getGodzina() {
		return godzina;
	}


	public void setGodzina(Date godzina) {
		System.out.println("setGodzina..." + godzina );
		this.godzina = godzina;
	}


	public Date getData() {
		return data;
	}


	public void setData(Date data) {
		this.data = data;
	}





}
