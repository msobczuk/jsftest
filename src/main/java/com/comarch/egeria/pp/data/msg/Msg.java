package com.comarch.egeria.pp.data.msg;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

import static com.comarch.egeria.Utils.nz;

//import org.primefaces.context.RequestContext;

public class Msg implements Serializable{
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	private Date otrzymano = new Date();
	
	private String tytul;
	private String tekst;
	private String severity;
	private String wdm_id;
	private String username;
	private String sessionId;
	
	public Severity severity(){
		if ("ERROR".equals(this.severity))
			return FacesMessage.SEVERITY_ERROR;
		else if ("WARN".equals(this.severity))
			return FacesMessage.SEVERITY_WARN;
		else 
			return FacesMessage.SEVERITY_INFO;
	}
	
	
	
	public String tytul(){
		if (tytul!=null && !"".equals(tytul))
			return tytul;
		else if ("ERROR".equals(this.severity))
			return "BŁĄD";
		else if ("WARN".equals(this.severity))
			return "OSTRZEŻENIE";
		else 
			return "INFOMRACJA";
	}

	
	public String icon(){
		if ("ERROR".equals(this.severity))
			return "<i class='material-icons growl'>error_outline</i>";
		else if ("WARN".equals(this.severity))
			return "<i class='material-icons growl'>warning</i>";
		else 
			return "<i class='material-icons growl'>info</i>";
	}
	
	
	public Msg(){}


	public Msg(String msgTxt){
		if (msgTxt==null || "".equals(msgTxt)) return; 
		msgTxt = msgTxt + "\n ";
		
		String[] split = msgTxt.split("\n"); //split -> ...  Trailing empty strings are therefore not included in the resulting array. 
//		  v_data :=  P_SEVERITY   || chr(10)
//        || p_wdm_id     || chr(10)
//        || P_USER       || chr(10)
//        || v_temat      || chr(10)
//        || v_tekst;		
		
		severity = split[0];
		setWdm_id(split[1]);
		setUsername(split[2]);
		
		if (split.length>3)
			tytul = split[3];

		if (split.length>4){
			tekst = split[4];
			for (int i=5; i<(split.length-1); i++){
				tekst += "<br/>" + split[i];
			}
		}
	}
	
	
	public void renderEgrGrowlMessage() {
//		System.out.println("renderEgrGrowlMessage ... ");
		try {
			
			FacesContext context = FacesContext.getCurrentInstance();
			if (context==null) return;
			
			
			
//			String growlId = "egr_growl";
//			
//			tekst += "<div class= 'copyLink info' style='color:yellow; position:relative!important'>Kopiuj treść</div>";	
//			
//			if ("ERROR".equals(severity)) {
//				context.addMessage(growlId, new FacesMessage(FacesMessage.SEVERITY_ERROR,
//						"<i class='material-icons growl'>error_outline</i>" + tytul, tekst));
//			}
//			else if ("WARN".equals(severity)) {
//				context.addMessage(growlId, new FacesMessage(FacesMessage.SEVERITY_WARN,
//						"<i class='material-icons growl'>warning</i>" + tytul, tekst));
//			}
//			else{
//				context.addMessage(growlId, new FacesMessage(FacesMessage.SEVERITY_INFO,
//						"<i class='material-icons growl'>info</i>" + tytul, tekst));
//			}
	
			//RequestContext.getCurrentInstance().update(growlId);
			
			
			if (wdm_id!=null && !"".equals(wdm_id)){
				wczytajEgrWiadomosc();
			}
				
			
			
			 String json = javax.json.Json.createObjectBuilder()
					 .add("summary",severityIcon()+tytul)
					 .add("detail", tekst)
					 .add("severity", severity)
					 .build().toString();
			
			 json = json.replace("'", "\\'");
			 json = json.replace("\"", "\\\"");
			 
			com.comarch.egeria.Utils.executeScript("PF('egr_growl').renderMessage(JSON.parse('" + json + "'));");
			
			
			
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
//		System.out.println("renderEgrGrowlMessage ... END");
	}	
	
	
	public String severityIcon(){
		if ("ERROR".equals(severity)) 
			return "<i class='material-icons growl'>error_outline</i>";
		else if ("WARN".equals(severity)) 
			return "<i class='material-icons growl'>warning</i>";
		else
			return "<i class='material-icons growl'>info</i>" ;		
	}
	
	
	public String severityIconName(){
		if ("ERROR".equals(severity)) 
			return "error_outline";
		else if ("WARN".equals(severity)) 
			return "warning";
		else
			return "info" ;		
	}
	
	public String severityIcon24px(){
		if ("ERROR".equals(severity)) 
			return "<i class='material-icons growl' style='font-size:24px!important;'>error_outline</i>";
		else if ("WARN".equals(severity)) 
			return "<i class='material-icons growl' style='font-size:24px!important;'>warning</i>";
		else
			return "<i class='material-icons growl' style='font-size:24px!important;'>info</i>" ;		
	}
	
	
	public String severityColor(){
		if ("ERROR".equals(severity)) 
			return "red";
		else if ("WARN".equals(severity)) 
			return "green";
		else
			return "steelblue" ;		
	}
	
	
	//TODO: zadanie pod m. translacji
	public String severityDescPL(){
		if ("ERROR".equals(severity)) 
			return "BŁĄD";
		else if ("WARN".equals(severity)) 
			return "OSTRZEŻENIE";
		else
			return "INFORMACJA" ;		
	}
	
	
	public void wczytajEgrWiadomosc() throws IOException, SQLException {
//		System.out.println("wczytajEgrWiadomosc ... ");
		
		SqlDataHandler sql = SqlBean.getSqlData(false, "select * from eat_wiadomosci where wdm_id = ? ", wdm_id);

		if (sql.getData().isEmpty())
			return;
		
		DataRow r = sql.getData().get(0);
		
		if (tytul==null || "".equals(tytul)){
			tytul = ""+ r.get("WDM_TEMAT");
		}
		
		if (tekst==null || "".equals(tekst)){
			tekst = nz( r.getAsString("WDM_TRESC") ); // Utils.asString( (Clob)
			tekst = tekst.replaceAll("\n", "<br/>");
			tekst = tekst.replaceAll("---", "--");

			if (tekst.length()>1000)
				tekst = tekst.substring(1, 1000) + "... <br/>...";
			
			tekst = String.format("<font size=\"2\">%1$s</font>", tekst);
		}
//		System.out.println("wczytajEgrWiadomosc ... END");
	}	
	
	
	public String getTytul() {
		return tytul;
	}
	public void setTytul(String tytul) {
		this.tytul = tytul;
	}
	public String getTekst() {
		return tekst;
	}
	public void setTekst(String tekst) {
		this.tekst = tekst;
	}
	public String getSeverity() {
		return severity;
	}
	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public String getWdm_id() {
		return wdm_id;
	}

	public void setWdm_id(String wdm_id) {
		this.wdm_id = wdm_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}



	public Date getOtrzymano() {
		return otrzymano;
	}



	public void setOtrzymano(Date otrzymano) {
		this.otrzymano = otrzymano;
	}


	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


}
