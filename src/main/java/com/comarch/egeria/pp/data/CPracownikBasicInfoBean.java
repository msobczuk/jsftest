package com.comarch.egeria.pp.data;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class CPracownikBasicInfoBean extends SqlBean {
	
//	@Inject
//	AppBean app;
	
	private DataRow pracownik;

	private DataRow ksTelInfo;
	
	
	
	public String setPrcId(Object id){
		setPracownik(AppBean.sqlBean.getLW("PPL_KAD_PRACOWNICY").find(id));
		setKsTelInfo(AppBean.sqlBean.getLW("PPL_DO_BASIC_INFO",id).find(id));
		return "";
	}


	public Object get(String colName){
		if (getPracownik()==null) return null;
		
		
		Object ret = null;
				
		if (getPracownik()!=null && getPracownik().getColumnIndex(colName)>=0)		
			ret = getPracownik().get(colName);
		
		if (ret == null && getKsTelInfo()!=null){
			ret = getKsTelInfo().get(colName);
		}
		return ret;
	}


	public DataRow getPracownik() {
		return pracownik;
	}


	public void setPracownik(DataRow pracownik) {
		this.pracownik = pracownik;
	}


	public DataRow getKsTelInfo() {
		return ksTelInfo;
	}


	public void setKsTelInfo(DataRow ksTelInfo) {
		this.ksTelInfo = ksTelInfo;
	}
	
	


}
