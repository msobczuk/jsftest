package com.comarch.egeria.pp.data.premie;

import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.premie.model.PptWnpkPremieKwoty;
import com.comarch.egeria.pp.data.premie.model.PptWnpwPremieWniosek;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.rowset.CachedRowSet;
import java.io.IOException;
import java.sql.SQLException;

@ManagedBean
@Named
@Scope("view")
public class PremieBean extends SqlBean  {
	private static final Logger log = LogManager.getLogger();//public bo wiekszosc beanow dziedziczy po tym
	
	@Inject
	SessionBean sessionBean;
	
	
	@Inject
	ObiegBean obieg;
	
	private PptWnpwPremieWniosek wniosek = null;
	
	@PostConstruct
	public void init(){
		Log.debug("PremieBean.init()...");
		
		obieg.setKategoriaObiegu("WN_PREMIE");
		
		String oId = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		if (oId == null){
			newPremieWniosek();
		}else {
			//wczytaj wniosek z bazy
			
			Long id = Long.parseLong(""+oId);
			this.setWniosek((PptWnpwPremieWniosek) HibernateContext.get(PptWnpwPremieWniosek.class, id));
			System.out.println("wczytano wniosek");
		}
		
		
	}

	
	private void newPremieWniosek() {
		setWniosek(new PptWnpwPremieWniosek());
		//TODO - załadowac kolekcje dzieci
		
		System.out.println("tworzymy nowy wniosek");
	}

	
	
	public void zapisz(){
		//TODO - validacja stanu encji
		
		try{
			this.setWniosek((PptWnpwPremieWniosek) HibernateContext.save(this.getWniosek()));
		}catch(Exception ex){
			log.error(ex.getMessage(), ex);
			return;
		}

		User.info("Zapisano wniosek (id: %1$s)", wniosek.getWnpwId());
		
	}

	
	public void kasuj() throws IOException{
		//TODO - kasowanie danych obiegu (jesli są)
		
		try{
			if (this.wniosek.getWnpwId() > 0) {
				obieg.usunDaneObiegu();
				HibernateContext.delete(this.wniosek);
				FacesContext.getCurrentInstance().getExternalContext().redirect("PremieListaWnioskow");
			}else{
				User.warn("Wniosek nie został jeszcze zapisany w bazie");
			}
			
		}catch(Exception ex){
			log.error(ex.getMessage(), ex);
		}

	}
	
	
	public void onWnpwObIdChange(ValueChangeEvent e) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		
		Object oldValue = e.getOldValue();
		Object newValue = e.getNewValue();
		System.out.println(String.format("onWnpwObIdChange.... %1$s -> %2$s", oldValue, newValue));
		//if (oldValue!=null && oldValue.equals(newValue)) return;
		
		for (Object object : this.wniosek.getPptWnpkPremieKwoties().toArray().clone()) {
			this.wniosek.removePptWnpkPremieKwoty((PptWnpkPremieKwoty) object);
		}
		
		if (newValue==null || newValue.equals(0))
			return;
			
		CachedRowSet crs = this.execSqlQuery(""
				+ "select ekz.zat_id "
				+ "from  ek_zatrudnienie ekz   "
				+ "		join egadm1.ek_pracownicy prc on ekz.ZAT_PRC_ID = prc.prc_id "
				+ "where (NVL (zat_data_do, SYSDATE) >= SYSDATE) AND (zat_data_zmiany <= LAST_DAY (SYSDATE))      "
				+ "		and ekz.ZAT_OB_ID = ? order by prc_nazwisko, prc_imie, prc_id ", newValue);
		while (crs.next()) {
			long zatId = crs.getLong(1);
			PptWnpkPremieKwoty kw = new PptWnpkPremieKwoty();
			kw.setWnpkZatId(zatId);
			this.wniosek.addPptWnpkPremieKwoty(kw);
		}
		
		System.out.println(String.format("onWnpwObIdChange %1$s -> %2$s; dzieciarnia: %3$s ", oldValue, newValue, wniosek.getPptWnpkPremieKwoties().size()));
	}
	
	
	
	

	public PptWnpwPremieWniosek getWniosek() {
		return wniosek;
	}


	public void setWniosek(PptWnpwPremieWniosek wniosek) {
		this.wniosek = wniosek;
	}
	
	
	
	
}
