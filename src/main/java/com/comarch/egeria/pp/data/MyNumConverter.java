package com.comarch.egeria.pp.data;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import javax.faces.convert.NumberConverter;

@FacesConverter("mynumcnv")
public class MyNumConverter extends NumberConverter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		setPattern((String) component.getAttributes().get("pattern"));
		return super.getAsObject(context, component, value);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		setPattern((String) component.getAttributes().get("pattern"));
		return super.getAsString(context, component, value);
	}

}
