package com.comarch.egeria.pp.data;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.HashMap;
import java.util.Map;

//import org.primefaces.context.RequestContext;


@ManagedBean
@ViewScoped
public class CSelectOneBean {

	HashMap<String, SqlDataSelectionsHandler> datasources = new HashMap<>(); // indeksowana
																				// przez
																				// clientId
	HashMap<String, Map> mapaSlownikow = new HashMap<>();
	Map<String, Object> slownik = new HashMap<>();
	private SqlDataSelectionsHandler datasource = null;

	String clientId = "";

	ELContext elContext;
	String expressionString;

	public Map<String, Object> getSlownik() {
		return slownik;
	}

	public void setSlownik(Map<String, Object> slownik) {
		this.slownik = slownik;
	}

	private Map<String, String> slownikCzyZapytanie = new HashMap<String, String>();

	@PostConstruct
	public void init() {

	}

	public String put(String clientid, SqlDataSelectionsHandler datasource) {
		this.datasources.put(clientid, datasource);
		return "";
	}

	public void czySlownik(String clientId, String czySlownik) {
		slownikCzyZapytanie.put(clientId, czySlownik);
	}

	public void wypelnijMape(String clientId, SqlDataSelectionsHandler datasource, String columns) {
		slownik = new HashMap<>();
		
		
		String[] columnsArray = null;
		if(columns != null && !columns.isEmpty()) {
			columns = columns.trim();
			columnsArray = columns.split(",");
		}
		
		String czySlownik = slownikCzyZapytanie.get(clientId);

		if (datasource != null)
			if (czySlownik.isEmpty() || "T".equals(czySlownik) || "Y".equals(czySlownik)) {

				for (DataRow dr : datasource.getData()) {
					slownik.put(""+dr.get(1), ""+dr.get(0));
				}
			} else {
				if(columnsArray != null) {
					for (DataRow dr : datasource.getData()) {
						slownik.put((String) dr.get(columnsArray[1].trim()), dr.get(columnsArray[0].trim()));
					}
				}
			}
		mapaSlownikow.put(clientId, slownik);
	}

	public Map<String, String> getSlownik(String clientId) {
		
		System.out.println("getSlownik .... clientId:" + clientId);
		Map ret = this.mapaSlownikow.get(clientId);
		System.out.println("getSlownik .... -> " + ret );
		return ret;
	}
}
