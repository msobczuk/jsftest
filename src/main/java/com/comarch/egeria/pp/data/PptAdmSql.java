package com.comarch.egeria.pp.data;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PPT_ADM_SQLS database table.
 * 
 */
@Entity
@Table(name="PPT_ADM_SQLS", schema="PPADM")
@NamedQuery(name="PptAdmSql.findAll", query="SELECT p FROM PptAdmSql p")
public class PptAdmSql implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="PPSL_ID")
	private String ppslId;

	@Column(name="PPSL_CACHEDURATION")
	private Integer ppslCacheduration;

	@Column(name="PPSL_DESCRIPTION")
	private String ppslDescription;

	@Lob
	@Column(name="PPSL_QUERY")
	private String ppslQuery;

	@Lob
	@Column(name="PPSL_QUERY_DELETED")
	private String ppslQueryDeleted;

	@Lob
	@Column(name="PPSL_QUERY_INSERTED")
	private String ppslQueryInserted;

	@Lob
	@Column(name="PPSL_QUERY_UPDATED")
	private String ppslQueryUpdated;

	@Column(name="PPSL_SCOPE")
	private Integer ppslScope;

	public PptAdmSql() {
	}

	public String getPpslId() {
		return this.ppslId;
	}

	public void setPpslId(String ppslId) {
		this.ppslId = ppslId;
	}

	public Integer getPpslCacheduration() {
		return this.ppslCacheduration;
	}

	public void setPpslCacheduration(Integer ppslCacheduration) {
		this.ppslCacheduration = ppslCacheduration;
	}

	public String getPpslDescription() {
		return this.ppslDescription;
	}

	public void setPpslDescription(String ppslDescription) {
		this.ppslDescription = ppslDescription;
	}

	public String getPpslQuery() {
		return this.ppslQuery;
	}

	public void setPpslQuery(String ppslQuery) {
		this.ppslQuery = ppslQuery;
	}

	public String getPpslQueryDeleted() {
		return this.ppslQueryDeleted;
	}

	public void setPpslQueryDeleted(String ppslQueryDeleted) {
		this.ppslQueryDeleted = ppslQueryDeleted;
	}

	public String getPpslQueryInserted() {
		return this.ppslQueryInserted;
	}

	public void setPpslQueryInserted(String ppslQueryInserted) {
		this.ppslQueryInserted = ppslQueryInserted;
	}

	public String getPpslQueryUpdated() {
		return this.ppslQueryUpdated;
	}

	public void setPpslQueryUpdated(String ppslQueryUpdated) {
		this.ppslQueryUpdated = ppslQueryUpdated;
	}

	public Integer getPpslScope() {
		return this.ppslScope;
	}

	public void setPpslScope(Integer ppslScope) {
		this.ppslScope = ppslScope;
	}

}