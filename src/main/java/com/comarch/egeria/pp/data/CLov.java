package com.comarch.egeria.pp.data;

import com.sun.faces.facelets.el.ContextualCompositeMethodExpression;
import org.primefaces.PrimeFaces;
import org.primefaces.component.commandbutton.CommandButton;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

//import org.primefaces.context.RequestContext;

@ManagedBean
@ViewScoped // @SessionScoped w Dialog Framework
public class CLov {

	HashMap<String, String> titles = new HashMap<>();  							//indeksowana przez clientId 
	HashMap<String, SqlDataSelectionsHandler> datasources = new HashMap<>(); 	//indeksowana przez clientId 
	HashMap<String, com.sun.faces.facelets.el.ContextualCompositeMethodExpression> rowSelectActions = new HashMap<>(); 	//indeksowana przez clientId com.sun.faces.facelets.el.ContextualCompositeMethodExpression
	HashMap<String, com.sun.faces.facelets.el.ContextualCompositeMethodExpression> rowsMultiSelectedActions = new HashMap<>();
	HashMap<String, Object> values = new HashMap<>(); 	//indeksowana przez clientId 	
	HashMap<String, String> valueFormats = new HashMap<>();  							//indeksowana przez clientId 
	HashMap<String, String> updates = new HashMap<>();  
	HashMap<String, String> dialogWidths = new HashMap<>();  
	HashMap<String, Boolean> multiselects = new HashMap<>();  
	
	
//	private boolean multiselect = false;
	
	private SqlDataSelectionsHandler datasource = null;
	private String title = "";
	private String dialogWidth;
	
	String clientId = "";

	CommandButton lovBtn;
	UIComponent lov;
	
	Object valueRefObject;

	ValueExpression valueExpression;
	ELContext elContext;
	String expressionString;
	
	@PostConstruct
	public void init(){
		
	}


	
	
	
	public void prepareDlg(Object o){
		
 		this.valueRefObject = o;
 		
		this.lovBtn = (CommandButton) UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());
		clientId = lovBtn.getClientId();
		clientId = clientId.replace(":lovBtn", "");
		this.lov = lovBtn.getParent().getParent();
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		elContext = facesContext.getELContext(); 
 		valueExpression = lov.getValueExpression("value");


		String newTitle = this.titles.get(clientId);
		SqlDataSelectionsHandler newDatasource = this.datasources.get(clientId);
		String newDlgWidth = this.dialogWidths.get(clientId);

		String chk1 = AppBean.concatValues(newTitle, newDatasource, newDlgWidth);
		String chk2 = AppBean.concatValues(title, datasource, dialogWidth);
		
		this.setTitle(newTitle);
		this.setDatasource(newDatasource);
		this.setDialogWidth(newDlgWidth);

		
		
		//pokaz i przeladuj dlg...   https://www.primefaces.org/showcase/ui/df/data.xhtml
		//RequestContext.getCurrentInstance().execute("PF('dlgLov').show();");
		if(!chk1.equals(chk2))//zmiana danych w LOV...
			com.comarch.egeria.Utils.update("dlgLov");
		
		
		
////Dialog Framework.... 		
//        Map<String,Object> options = new HashMap<String, Object>();
//        options.put("resizable", false);
//        options.put("draggable", false);
//        options.put("modal", true);
//        options.put("width", 1200);
//        options.put("height", 600);
//		RequestContext.getCurrentInstance().openDialog("cLovDF", options, null);

	}
	
	
	
	public void close(Object o){
//		System.out.println("close...................");
		//RequestContext.getCurrentInstance().closeDialog(o);
		PrimeFaces.current().dialog().closeDynamic(o);
	}
	
	
	
	
	
	
	
	public void setValueDoCcAttrsActionAndUpdate() throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
//		System.out.println("setval...." + clientId);
		DataRow r = this.datasource.getCurrentRow();
//		System.out.println(r);
		
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ELContext elContext = facesContext.getELContext(); 
		
		//własciwy lov (nie dialog) jako UIComponent zostal zapamietany prz prepareDlg()
		//UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
		//UIComponent lov = viewRoot.findComponent(this.clientId);
		
		//obliczenie nowej wartosci...
		String valueFormat = this.valueFormats.get(this.clientId);
		String val = null;
		if (valueFormat==null || valueFormat.trim().equals(""))
			val = r!=null?r.getIdAsString():null;
		else
			val = r.format(valueFormat);

		//this.value = val;
				
		if (this.valueRefObject!=null){
			setRefObjectValueByReflection(val);
		}else{
			//ustawienie wartosci do cc.attrs.value (wywolanie settera...)
			//ValueExpression valueExpression = lov.getValueExpression("value");
			if (valueExpression!=null && elContext!=null){
				valueExpression.setValue(elContext, val); //dziala ale tylko jesli lov nie byl zagniezdzony w kolekcji... np. reapeterku lub w datatable
			}
		}
		
		//strasznie wolne....
		//RequestContext.getCurrentInstance().execute("PFbyId('" + this.clientId + ":lovBtn').jq.trigger('click');");
		
		
		//po ustawieniu value wywloanie akcji (wg cc.attrs.roSelectedAction ) po ustawieniu wartosci ...
		ContextualCompositeMethodExpression action = this.rowSelectActions.get(this.clientId);
		if (action!=null){
			action.invoke(elContext, null);
		}
		
		
		
		//po ustawieniu value wywloanie akcji (wg cc.attrs.roSelectedAction ) po ustawieniu wartosci ...
		ContextualCompositeMethodExpression action1 = this.rowsMultiSelectedActions.get(this.clientId);
		if (action1!=null){
			action1.invoke(elContext, null);
		}
		
//		close(null);
		
		//po wybraniu wiersza, ustawieniu wlasciwego settera, wywolaniu ew. dodatkowej akcji odsw. lov...
		com.comarch.egeria.Utils.update(this.clientId+":lovInpText");
		
		
//		if (!this.isMultiselect()) {
//			//UWAGA w trybie multiselect dzialamy inaczej - ww dialogu jest przycisk [OK] z AJAXem, ktory puszcza update'y!!!! 
//			String update = this.updates.get(this.clientId);
//			if (update!=null && !"".equals(update.trim())){
//				List<String> l = Arrays.asList(update.split(" "));
//				while(l.remove("")){}
//				RequestContext.getCurrentInstance().update(l);
//			}
//		}
		
		String update = this.updates.get(this.clientId);
		if (update!=null && !"".equals(update.trim())){
			List<String> l = Arrays.asList(update.split(" "));
			while(l.remove("")){}
			com.comarch.egeria.Utils.update(l);
		}

//		RequestContext.getCurrentInstance().execute("PFbyId('" + this.clientId +":lovBtnUpdate').jq.trigger('click');");//update przez trigger na przycisku ukrytym we wlasciwym formularzu...
		
	}
	
	
	

	public boolean isMultiselect() {
		Boolean ret = this.multiselects.get(this.clientId);
		if (ret == null) return false;
		return ret;
	}



	public void test(){
		System.out.println("test........................................................");
	}
	
	




	private void setRefObjectValueByReflection(String val) throws IllegalAccessException, InvocationTargetException {
		expressionString = valueExpression.getExpressionString(); //#{delegacjaMFBean.wniosek.deltCeleDelegacjis.get(status.index).cdelKrId}
		
		String setterName = extractSetterName();

		for (Method m:this.valueRefObject.getClass().getMethods()){
			String name = m.getName(); 
			if (setterName.equals(name)){
				Class<?>[] pt = m.getParameterTypes();
				if(pt.length>1) continue;

				Class<?> t = pt[0];
				if (t.equals(Long.class)){
					m.invoke(this.valueRefObject, Long.parseLong(""+val));
				} else if (t.equals(String.class)){
					m.invoke(this.valueRefObject, val);
				} //TODO  - pozostale typy danych ???...

				break;
			}
		}
//			Method method = this.valueRefObject.getClass().getMethod("setCdelKrId", Long.class);
//			method.invoke(this.valueRefObject, Long.parseLong(""+val) );
	}





	private String extractSetterName() {
		//"#{delegacjaMFBean.wniosek.deltCeleDelegacjis.get(status.index).cdelKrId}" vs 	"setCdelKrId"
		String setterName = this.expressionString.substring(this.expressionString.lastIndexOf(".")+1);
		setterName = setterName.replace("}", "");
		setterName = "set" + setterName.substring(0,1).toUpperCase() + setterName.substring(1);
		return setterName;
	}
	
	
	
//	public UIComponent findComponent(final String id) {
//
//	    FacesContext context = FacesContext.getCurrentInstance(); 
//	    UIViewRoot root = context.getViewRoot();
//	    final UIComponent[] found = new UIComponent[1];
//
//	    root.visitTree(new FullVisitContext(context), new VisitCallback() {     
//	        @Override
//	        public VisitResult visit(VisitContext context, UIComponent component) {
//	            if(component.getId().equals(id)){
//	                found[0] = component;
//	                return VisitResult.COMPLETE;
//	            }
//	            return VisitResult.ACCEPT;              
//	        }
//	    });
//
//	    return found[0];
//
//	}

	
	
	
	
	
	
	
	public void put(String clientid,  String title){
		this.titles.put(clientid, title);
	}

	public void put(String clientid, SqlDataSelectionsHandler datasource){
		this.datasources.put(clientid, datasource);
	}

	
	public void put(String clientid, com.sun.faces.facelets.el.ContextualCompositeMethodExpression action){// com.sun.faces.facelets.el.ContextualCompositeMethodExpression
		this.rowSelectActions.put(clientid, action);
	}

	public void putMultiSelectAction(String clientid, com.sun.faces.facelets.el.ContextualCompositeMethodExpression action){// com.sun.faces.facelets.el.ContextualCompositeMethodExpression
		this.rowsMultiSelectedActions.put(clientid, action);
	}
	
	
	public void putValueFormat(String clientid, String format){// com.sun.faces.facelets.el.ContextualCompositeMethodExpression
		this.valueFormats.put(clientid, format);
	}
	
	public void putUpdate(String clientid, String update){// com.sun.faces.facelets.el.ContextualCompositeMethodExpression
		this.updates.put(clientid, update);
	}

	public void putDialogWidth(String clientid, String dialogWidth){
		this.dialogWidths.put(clientid, dialogWidth);
	}
	
	
	public void putMultiselect(String clientid, Boolean multiselect){
		this.multiselects.put(clientid, multiselect);
	}
	
	
	
	
	
	
	
	
	
	
	

	public SqlDataSelectionsHandler getDatasource() {
		return datasource;
	}


	public void setDatasource(SqlDataSelectionsHandler datasource) {
		this.datasource = datasource;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}	
	
	
	public boolean isNotEmpty(){
		return !this.datasources.isEmpty();
	}




//	public boolean isMultiselect() {
//		return multiselect;
//	}
//
//
//
//	public String setMultiselect(boolean multiselect) {
//		this.multiselect = multiselect;
//		return "";
//	}





	public String getDialogWidth() {
		return dialogWidth;
	}





	public void setDialogWidth(String dialogWidth) {
		this.dialogWidth = dialogWidth;
	}

	
	
	public String getToUpdate(){
		String ret = this.updates.get(this.clientId);
		return ret;
	}

}

