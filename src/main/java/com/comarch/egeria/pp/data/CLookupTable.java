package com.comarch.egeria.pp.data;

import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jfree.util.Log;
import org.primefaces.component.api.UIData;

import javax.el.MethodExpression;
import javax.el.ValueExpression;
import javax.faces.component.FacesComponent;
import javax.faces.component.UINamingContainer;
import javax.faces.context.FacesContext;
import java.io.IOException;

//import org.primefaces.context.RequestContext;


@FacesComponent
public class CLookupTable extends UINamingContainer {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	private UIData table;
	public UIData getTable() {
		return table;
	}
	public void setTable(UIData table) {
		this.table = table;

		String var = (String) getAttributes().get("var");
		if (var!=null)  table.setVar(var); //ustawiam wlasna nazwe dla var...

		String rowIndexVar = (String) getAttributes().get("rowIndexVar");
		if (rowIndexVar!=null)  table.setRowIndexVar(rowIndexVar); //ustawiam wlasna nazwe dla rowIndexVar...
	}


	private SqlDataSelectionsHandler datasource = null;


	public void test() {
		//table.setRowIndex();
	}



//http://jugojava.blogspot.com/2011/09/jsf-composite-component-binding-to.html
//	private UIInput inpSearch;


	public CLookupTable(){
//		System.out.println("ctor: CLookupTable / " + this);
//		Object ds = this.getAttributes().get("datasource");
//		System.out.println(ds);
	}


	public void init() {
		this.setTable( (UIData) this.findComponent("dane") ); // zamiast  binding="#{cc.table}" w pliku xhtml
//		System.out.println("CLookupTable.init / "  + this + " / ");
//		Object ds1 = this.getAttributes().get("datasource");
//		if (ds1==null)
//			System.out.println(ds1); czesto w dlgLovJS i dlgLOV jest null

	}

	@Override
	public void encodeAll(FacesContext context) throws IOException {
		datasource = (SqlDataSelectionsHandler) this.getAttributes().get("datasource");//nie ruszac i nie dodawac if ds ==null
		super.encodeAll(context);
	}



	public void rowSelectActionListener(javax.faces.event.AjaxBehaviorEvent ae) throws IOException {

		Boolean radioSelect = (Boolean) this.getAttributes().get("checkOneRow");

		SqlDataSelectionsHandler datasource = (SqlDataSelectionsHandler) this.getAttributes().get("datasource");
		DataRow selectedRow = !radioSelect ? datasource.getCurrentRow() : datasource.getRadioSelectedRow() ;

		String editRowPage = (String) this.getAttributes().get("editRowPage");

		if (editRowPage != null && !"".equals(editRowPage)) {

		    if (ae==null){
		        //przekierowanie przez urlQueryString
                User user = User.getCurrentUser();
                user.editRowPage = editRowPage;
                user.editRowPageEntityId = selectedRow.getIdAsString();
                com.comarch.egeria.Utils.executeScript("goEditUrlQueryEntity();");
                return;
            }

			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", selectedRow.getIdAsString());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", selectedRow);
			FacesContext.getCurrentInstance().getExternalContext().redirect(editRowPage); //??? a to zapętla się... doFilterAndSelectRowByUrlParams -> jeśli Phase = 6 (Rende_Response) to nie mozna  ustawiac flash'a...
			return;
		}

		String valueFormat = (String) this.getAttributes().get("valueFormat");
		this.setValue(selectedRow, valueFormat);

		MethodExpression me = null;

		me = (MethodExpression)this.getAttributes().get("rowSelectAction"); //java.lang.String action()
		if (me!=null){
			System.out.println(me.getExpressionString());
			me.invoke( FacesContext.getCurrentInstance().getELContext(), new Object[0]);
		}

		me =(MethodExpression)this.getAttributes().get("rowSelectedAction"); //void f(com.comarch.egeria.pp.data.DataRow)
		if (me!=null){
			System.out.println(me.getExpressionString());
			Object[] params = new Object[1];
			params[0] = selectedRow;
			me.invoke( FacesContext.getCurrentInstance().getELContext(), params );
		}

	}




	private void setValue(DataRow r, String valueFormat) throws IOException {
		Object val = null;

		if (valueFormat == null || valueFormat.trim().equals(""))
			val = r != null ? r.getIdAsString() : null;
		else
			val =  r!=null ? r.format(valueFormat) : "";

		//Object valueAttr = this.getAttributes().get("value"); nieee
		//Object ovexpr =  FacesContext.getCurrentInstance().getApplication().getExpressionFactory().createValueExpression(FacesContext.getCurrentInstance().getELContext(), "#{cc.attrs.value}", Object.class);
		Object ovexpr = this.getValueExpression("value");
		if (ovexpr!=null) {
			ValueExpression valueExpression = (ValueExpression) ovexpr;
			try {
				valueExpression.setValue(FacesContext.getCurrentInstance().getELContext(), val);
			} catch (Exception e) {
				if (!(e instanceof NullPointerException)) {
					throw e;// inne wyjatki chcemy zobaczyc
				}
			}
		}
	}





	public String doFilterAndSelectRowByUrlParams(){
		//System.out.println("doFilterAndSelectRowByUrlParams....");

		SqlDataSelectionsHandler lw = (SqlDataSelectionsHandler) this.getAttributes().get("datasource");

		if (lw==null)
			return "";

		String lwName = lw.getShortName();

		Object id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(lwName);
		if (id==null)
			return "";

		if (lw.getColumns().isEmpty())
			lw.reLoadData(); //lista nie jest jeszcze wczytana

		try {

			String idColName = lw.getColumns().get(0);
			lw.getData();
			lw.setColFilter(idColName, "="+ id, true);

			int size = lw.getData().size();
			if (size==1){

				DataRow r = lw.getData().get(0);
				lw.setCurrentRow(r);

//				UIComponent uic = UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());
				//DataTable tb =  (DataTable) uic.findComponent("dane");
				//if (tb==null)
			    //		return "";
				//String tbClientId = tb.getClientId();
				//RequestContext.getCurrentInstance().execute("PF(wid('" + tbClientId + "')).selectRow(0);");tak nie wolno bo się zapętli!!!!

				try {
                    rowSelectActionListener(null);
				} catch (Exception ex){
					log.debug(ex.getMessage());
				}

				return "... url ... ";

			} else if (size<=0)  {
				return "Brak wiersza " + id + " wskazanego w url. Możliwe, że nie masz do niego dostepu lub został usunięty z bazy.";
			} else if (size>1){
				return "Na liście jest więcej wierszy pasujących do parametrów w url.";
			}

		} catch (Exception e) {
			Log.error(e.getMessage(), e);
		}

		return "";
	}


	public SqlDataSelectionsHandler getDs() {
	    if (datasource==null)
            datasource = (SqlDataSelectionsHandler) this.getAttributes().get("datasource");

		return datasource;
	}

	public void setDs(SqlDataSelectionsHandler datasource) {
		this.datasource = datasource;
	}



}
