package com.comarch.egeria.pp.data;


import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@ManagedBean
@Named
@Scope("view")
public class LogoutMsgsBean {
    private static final long serialVersionUID = 1L;


    @PostConstruct
    public void init(){
        String p = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("timeout");
        if (p!=null) {
            FacesMessage fmsg = new FacesMessage(FacesMessage.SEVERITY_INFO, "SESJA WYGASŁA", "-");
            FacesContext.getCurrentInstance().addMessage(null, fmsg);
        }
    }
}
