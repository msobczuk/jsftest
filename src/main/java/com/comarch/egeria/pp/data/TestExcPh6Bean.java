package com.comarch.egeria.pp.data;

import com.comarch.egeria.web.User;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.time.LocalDateTime;


@Named
@Scope("view")
public class TestExcPh6Bean extends SqlBean {

	String info = null;


	@PostConstruct
	public void init() {
		this.info = "@PostConstruct init ... \n" + this + " ... \n inited at " + LocalDateTime.now();
		User.info(this.info);
		System.out.println(this.info);
	}


	public void setInfo(String excMsg) throws Exception {
		this.info = excMsg;
//		this.getInfo(); //wywolanie bledu w fazie 5
//		throw new EgrContextException(info);//wyjątek, który pojawi się podczas renderowania komponentu korzystającego z tego gettera
//		throw new Exception("ALAMAKOTA z setInfo");//wyjątek, który pojawi się podczas renderowania komponentu korzystającego z tego gettera
	}
	public String getInfo() throws Exception {
		if (this.info == null || this.info.isEmpty()) {
			System.out.println("...wymuszono błąd w fazie 6/RENDER_RESPONSE...");
			User.warn("...wymuszono błąd w fazie 6/RENDER_RESPONSE...");
			throw new Exception("ALAMAKOTAAAAAAAA z getInfo");//wyjątek, który pojawi się podczas renderowania komponentu korzystającego z tego gettera
//			throw  new ViewExpiredException();
		}
		return info;
	}






}


