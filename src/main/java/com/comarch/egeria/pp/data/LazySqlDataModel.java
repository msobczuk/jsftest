package com.comarch.egeria.pp.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.primefaces.model.FilterMeta;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

public class LazySqlDataModel extends LazyDataModel<DataRow> {
	
	
	SqlDataSelectionsHandler sql;

    String sortField = null;
    SortOrder sortOrder = null;

    public String getSortField() {
        return sortField;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }



    public LazySqlDataModel(SqlDataSelectionsHandler sql) {
    	this.sql = sql;
    }

	public List<DataRow> getDatasource() {
		ArrayList<DataRow> data = sql.getData();
		return data;
	}
	
	 @Override
    public DataRow getRowData(String rowKey) {
		 return sql.pkIndexMap.get(rowKey);
    }
	 
    @Override
    public Object getRowKey(DataRow r) {
        return r.getIdAsString();
    }

    
    
    @Override
    public int getRowCount() {
    	return this.getDatasource().size();
    }
    

    
    
    
    @Override
    public List<DataRow> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, FilterMeta> filters) {
        List<DataRow> data = new ArrayList<DataRow>();
 
        //filter
        for(DataRow r : getDatasource()) {
            boolean match = true;
 
            if (filters != null) {
                for (Iterator<String> it = filters.keySet().iterator(); it.hasNext();) {
                    try {
                        String filterProperty = it.next();
                        Object filterValue = filters.get(filterProperty);
                        String fieldValue = String.valueOf(r.getClass().getField(filterProperty).get(r));
 
                        if(filterValue == null || fieldValue.startsWith(filterValue.toString())) {
                            match = true;
                    }
                    else {
                            match = false;
                            break;
                        }
                    } catch(Exception e) {
                        match = false;
                    }
                }
            }
 
            if(match) {
                data.add(r);
            }
        }
 
        //sort
        if(sortField != null) {
            this.sortField = sortField;
            this.sortOrder = sortOrder;
            Collections.sort(data, new LazySorter(sortField, sortOrder));
        }
 
        //rowCount
        int dataSize = data.size();
        this.setRowCount(dataSize);
 
        //paginate
        if(dataSize > pageSize) {
            try {
                return data.subList(first, first + pageSize);
            }
            catch(IndexOutOfBoundsException e) {
                return data.subList(first, first + (dataSize % pageSize));
            }
        }
        else {
            return data;
        }
    }
	
}
