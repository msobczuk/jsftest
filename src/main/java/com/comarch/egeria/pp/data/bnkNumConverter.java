package com.comarch.egeria.pp.data;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("bnkNumCnv")
public class bnkNumConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext ctx, UIComponent component, String value) {
		return null;
	}

	@Override
	public String getAsString(FacesContext ctx, UIComponent component, Object value) {
		// System.out.println(component.getClass().getSimpleName() +" ["
		// +component.getId() + "] <- "+ value.getClass().getSimpleName() + " //
		// " + value);

		if (value instanceof String) {
			String txt = (String) value;

			if (!txt.contains("-")) {

				if (txt.startsWith("PL"))
					txt = txt.replace("PL", "").trim();
				int length = txt.length();
				if(length>=26){
				return txt.substring(0, length - 24) + " " + txt.substring(length - 24, length - 20) + " "
						+ txt.substring(length - 20, length - 16) + " " + txt.substring(length - 16, length - 12) + " "
						+ txt.substring(length - 12, length - 8) + " " + txt.substring(length - 8, length - 4) + " "
						+ txt.substring(length - 4, length);
				}
			}
		}

		return value.toString();

	}

}
