package com.comarch.egeria.pp.data;

import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

//import org.primefaces.context.RequestContext;

@ManagedBean
public class CSelectBooleanCheckboxTN {

	

	public Boolean getValue() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ELContext elContext = facesContext.getELContext(); 
		ValueExpression valueExpression = facesContext.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.value}", Object.class);
		String val = (""+valueExpression.getValue(elContext)).toUpperCase();
		return "T".equals(val);
	}

	public void setValue(Boolean value) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		ELContext elContext = facesContext.getELContext(); 
		ValueExpression valueExpression = facesContext.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.value}", Object.class);
		valueExpression.setValue(elContext, value ? "T":"N" );
		

		
	}
	
	public void doUpdate(){
		try {
			
			//UPDATE
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ELContext elContext = facesContext.getELContext(); 
			ValueExpression valueExpression1 = facesContext.getApplication().getExpressionFactory().createValueExpression(elContext, "#{cc.attrs.update}", Object.class);
			String update = (""+valueExpression1.getValue(elContext)).trim();
			if (!"".equals(update))
				com.comarch.egeria.Utils.update(update);

		} catch (Exception e) {
				e.printStackTrace();
		}

	}
	
}
