package com.comarch.egeria.pp.data;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.Administrator.model.PptAdmListyTabele;
import com.comarch.egeria.pp.Administrator.model.PptAdmListyWartosci;
import com.comarch.egeria.pp.data.model.DataSourceBase;
import com.comarch.egeria.pp.data.model.DataSourceOracle;
import com.comarch.egeria.pp.data.model.PptDataSource;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.sql.DataSource;
import javax.sql.rowset.CachedRowSet;
import java.io.IOException;
import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.comarch.egeria.Utils.nz;

public class SqlDataHandler implements Serializable {
	private static final long serialVersionUID = 1L;
	public static final Logger log = LogManager.getLogger(SqlDataHandler.class);

	//////////////////////////////////////////////////////////////////////////////////////
	// zmienne kontekstowe dla ustawienia połączenia DB z Egerią
	// ich ustawienie niekiedy jest konieczne aby poprawnie pobierac dane
	// zalezne od kontekstu w bazie
	//////////////////////////////////////////////////////////////////////////////////////
	private Boolean contextDependent = true;// czy przed wykonaniem zapytania sql ustawiac kontekst sesji uzytkownika na
											// polaczeniu

	//posortowana lista def. etykiet (JPA) + definicja LW w pierwszym wierszu
	private List<PptAdmListyWartosci> lst = new ArrayList<>();

	//czasy statystyki wczytywania LW.
	public static ConcurrentHashMap<String, Long> reLoadTSHM = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<String, Long> reLoadCountersHM = new ConcurrentHashMap<>();
	
	private ConcurrentHashMap<String, Object> dependencyTablesCHM = null; //lazy!!!
	
	//czy ustawiać konsolidację po połaczeniu do bazy
	private Boolean konsolidacja = false;
	
	//czy zezwalać na eksport danych z cLookupTable
	private Boolean czyPdf = false;
	private Boolean czyXls = false;
	private Boolean czyCsv = false;
	private Boolean czyXml = false;

	
	// private String DBCTXeapGlobalsUstawUzytkownika = null;
	//private String DBCTXcsspObiegiUtlUstawKategorie = null;
//	private Long DBCTXcsspObiegiUtlUstawIdEncji = null;
	//////////////////////////////////////////////////////////////////////////////////////

	private String detailsPanelView = null;
	private int gdElementWidth = 320; // szerokosc elementu w ukladzie grid
	private int gdElementHeight = 150; // wys. el. w ukl. grid
	private int gdColumnsCount = 4;
	private int gdElementsCount = 20;

	public String pkColumnName = null;
	public HashMap<String, DataRow> pkIndexMap = new HashMap<>();// klucz z kolumny z indeksem 0 jako String
	public HashMap<DataRow, String> pkRows2IndexMap = new HashMap<>();// do szybkiego wyłuskania klucza jesli mamy ref
																		// do DataRow

	private String shortName = ""; // np. "TOWARY" albo "LISTA TOWRÓW"
	private String name = null; // np. "TOWARY" albo "LISTA TOWRÓW"

	private long ts; // kiedy wczytano dane [ms]
	private long cacheDuration = 30 * 60000;
	private String sql = null; // np. "select Twr_TwrId, Twr_Kod, Twr_Nazwa, Twr_Kod, Twr_EAN from CDN.Towary"
	private ArrayList<Object> sqlParams = new ArrayList<>();// opcjonalnie tu podamy paramtry do zapytania sql...
	private String preQueryBlock = null;// opcjonalnie blok PLSQL zustawieniami np. dbms_session.set_context lub ppadm.ppp_global.ustaw_date_dla_struktury_po(sysdate);
	private Map<String,Object> sqlNamedParams = new HashMap<>();// opcjonalnie tu podamy nazwane paramtry do zapytania sql...
	private List<Object> preQueryBlockParams = new ArrayList<>();// opcjonalnie tu podamy paramtry do zapytania sql...
	// private ArrayList<Object[]> data = new ArrayList<>();
	protected ArrayList<DataRow> data = new ArrayList<>();
	private ArrayList<String> columns = new ArrayList<>();
	private ArrayList<String> columnsTypes = new ArrayList<>();

	//public DataSource ds;
	
	private long reloadTime = 0;
	private long reloadTimeTotal = 0; // statystyka - czas poswiecany na wczytywanie danych z bazy
	private long reloadCount = 0; // statystyka - ile razy wczytywano dane z bazy

	private DataSourceBase ds;

	public Map<String, String> genMap(String keyFormat, String valueFormat) {
		TreeMap<String, String> ret = new TreeMap<String, String>();
		this.getData();
		for (DataRow r : this.data)
			ret.put(r.format(keyFormat), r.format(valueFormat));

		return ret;
		// return new TreeMap<String, String>(ret);
	}



	public PptAdmListyWartosci getDefLst() {
		if (this.getLst()==null || this.getLst().isEmpty()) return null;
		return this.getLst().get(0);
	}


	public String getDefLstQuery() {
		PptAdmListyWartosci defLst = this.getDefLst();
		if (defLst==null) return null;
		return defLst.getLstQuery();
	}

	
	public void reLoadDataRow(String rowId){
		synchronized (this) {
			long t0 = new Date().getTime();
			this.ds = getDs();

			log.debug(this.shortName + " -> reLoadDataRow(String rowId) / " + rowId);


			if (getSql() == null || getSql().trim().equals(""))
				return;

			Boolean setContext = this.contextDependent;

			if ("sqlwupr".equals(this.shortName))
				setContext = false;


			try (Connection conn = getConnection(setContext)) {
				this.reloadCount++;

//				if (this.DBCTXcsspObiegiUtlUstawIdEncji != null && this.DBCTXcsspObiegiUtlUstawIdEncji != 0
//						&& this.DBCTXcsspObiegiUtlUstawKategorie != null) {
//					JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_kategorie", this.DBCTXcsspObiegiUtlUstawKategorie);
//					JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_id_encji", this.DBCTXcsspObiegiUtlUstawIdEncji);
//				}

				CachedRowSet crs = null;

				if (konsolidacja)
					JdbcUtils.sqlSPCall(conn, "EAP_GLOBALS.USTAW_KONSOLIDACJE", "T");

				try {

					executePreQueryBlock(conn);

					if (!sqlNamedParams.isEmpty()) {

						String lstQuery = this.getLst().get(0).getLstQuery();
						String sql = String.format("select * from ( \n %1$s \n ) where %2$s = :P_ROW_PK ", lstQuery, this.columns.get(0));
						HashMap<String, Object> params1 = new HashMap<>();
						params1.putAll(this.sqlNamedParams);
						params1.put("P_ROW_PK", rowId);
						crs = JdbcUtils.sqlExecQuery(conn, sql, params1);

					} else {

						String sql = String.format("select * from ( \n %1$s \n ) where %2$s = ? ", this.getSql(), this.columns.get(0));
						ArrayList<Object> params1 = (ArrayList<Object>) sqlParams.clone();
						params1.add(rowId);
						crs = JdbcUtils.sqlExecQuery(conn, sql, params1);

					}

				} finally {
					if (konsolidacja)
						JdbcUtils.sqlSPCall(conn, "EAP_GLOBALS.USTAW_KONSOLIDACJE", "N");
				}


				int cnt = crs.getMetaData().getColumnCount();
				DataRow r = this.pkIndexMap.get(rowId);

				if (crs.size() == 0) { //dane skasowano z bazy
					if (r != null) {
						this.pkIndexMap.remove(r.getIdAsString());
						this.pkRows2IndexMap.remove(r);
						this.data.remove(r);
					}

				} else { //wiersz nadal jest w bazie

					while (crs.next()) {
						if (r == null) {
							loadCrsData(crs, true);//doda nowy wiersz (zakladamy, ze crs ma tylko jeden taki)
						} else {
							for (int i = 1; i <= cnt; i++) {
								r.getData().set(i - 1, crs.getObject(i));
							}
						}
						break;//jesli wczytano wiecej wierszy to pewnie nie podano unikatowej kolumny na poczatku selecta//olewamy
					}
				}

			} catch (Exception e) {
				if (e.getMessage().contains("Sesja zakończona przez Administratora"))
					log.debug(e.getMessage());
				else
					log.error(e.getMessage(), e);
			}
			this.reloadTime = (new Date().getTime() - t0);
			this.reloadTimeTotal += this.reloadTime;
		}
	}
	
	
	
	public void reLoadDataRow(DataRow r){
		synchronized (this) {
			long t0 = new Date().getTime();
			this.ds = getDs();

			log.debug(this.shortName + " -> reLoadDataRow / " + r);
			String rowId = r.getIdAsString();

			if (getSql() == null || getSql().trim().equals(""))
				return;

			Boolean setContext = this.contextDependent;

			if ("sqlwupr".equals(this.shortName))
				setContext = false;


			try (Connection conn = getConnection(setContext)) {
				this.reloadCount++;

//				if (this.DBCTXcsspObiegiUtlUstawIdEncji != null && this.DBCTXcsspObiegiUtlUstawIdEncji != 0
//						&& this.DBCTXcsspObiegiUtlUstawKategorie != null) {
//					JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_kategorie", this.DBCTXcsspObiegiUtlUstawKategorie);
//					JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_id_encji", this.DBCTXcsspObiegiUtlUstawIdEncji);
//				}

				CachedRowSet crs = null;

				if (konsolidacja)
					JdbcUtils.sqlSPCall(conn, "EAP_GLOBALS.USTAW_KONSOLIDACJE", "T");

				try {

					executePreQueryBlock(conn);

					if (!sqlNamedParams.isEmpty()) {

						String lstQuery = this.getLst().get(0).getLstQuery();
						String sql = String.format("select * from ( \n %1$s \n ) where %2$s = :P_ROW_PK ", lstQuery, this.columns.get(0));
						HashMap<String, Object> params1 = new HashMap<>();
						params1.putAll(this.sqlNamedParams);
						params1.put("P_ROW_PK", rowId);
						crs = JdbcUtils.sqlExecQuery(conn, sql, params1);

					} else {

						String sql = String.format("select * from ( \n %1$s \n ) where %2$s = ? ", this.getSql(), this.columns.get(0));
						ArrayList<Object> params1 = (ArrayList<Object>) sqlParams.clone();
						params1.add(rowId);
						crs = JdbcUtils.sqlExecQuery(conn, sql, params1);

					}


				} finally {
					if (konsolidacja)
						JdbcUtils.sqlSPCall(conn, "EAP_GLOBALS.USTAW_KONSOLIDACJE", "N");
				}


				int cnt = crs.getMetaData().getColumnCount();


				if (crs.size() == 0) { //dane skasowano z bazy
					this.pkIndexMap.remove(r.getIdAsString());
					this.pkRows2IndexMap.remove(r);
					this.data.remove(r);
				} else { //wiersz nadal jest w bazie
					while (crs.next()) {
						for (int i = 1; i <= cnt; i++) {
							r.getData().set(i - 1, crs.getObject(i));
						}
						break;//jesli wczytano wiecej wierszy to pewnie nie podano unikatowej kolumny na poczatku selecta//olewamy
					}
				}


			} catch (Exception e) {
				if (e.getMessage().contains("Sesja zakończona przez Administratora"))
					log.debug(e.getMessage());
				else
					log.error(e.getMessage(), e);
			}
			this.reloadTime = (new Date().getTime() - t0);
			this.reloadTimeTotal += this.reloadTime;
		}
	} 
	
	
	
	
	public void reLoadData(){
		synchronized (this) {
			log.debug("SqlDataHandler.reLoadData/ " + this.shortName + " / połączenia aktywne: " + AppBean.bds.getNumActive());
			this.ds = getDs();

			String methodname = "SqlDataHandler.reLoadData/" + this.shortName + this.sqlParams;
			long t0 = AppBean.logMethod(methodname);

			data.clear();
			columns.clear();
			this.pkIndexMap.clear();
			this.pkRows2IndexMap.clear();

			if (getSql() == null || getSql().trim().equals(""))
				return;


			Boolean setContext = this.contextDependent;
			if ("sqlwupr".equals(this.shortName))
				setContext = false;


			try (Connection conn = getConnection(setContext)) {
				this.reloadCount++;

//				if (this.DBCTXcsspObiegiUtlUstawIdEncji != null && this.DBCTXcsspObiegiUtlUstawIdEncji != 0
//						&& this.DBCTXcsspObiegiUtlUstawKategorie != null) {
//					JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_kategorie", this.DBCTXcsspObiegiUtlUstawKategorie);
//					JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_id_encji", this.DBCTXcsspObiegiUtlUstawIdEncji);
//				}

				CachedRowSet crs = null;

				if (konsolidacja)
					JdbcUtils.sqlSPCall(conn, "EAP_GLOBALS.USTAW_KONSOLIDACJE", "T");

				try {

					executePreQueryBlock(conn);

					if (!sqlNamedParams.isEmpty()) {

						String lstQuery = this.getLst().get(0).getLstQuery();
						String sqlLimit1MRows = ds.formatSqlToLimitRows(this.shortName, lstQuery, 1000000);
						if ("ppl_lw_dependecies".toLowerCase().equals(this.shortName)) sqlLimit1MRows = lstQuery;
						crs = JdbcUtils.sqlExecQuery(conn, sqlLimit1MRows, sqlNamedParams);

					} else {

						String sqlLimit1MRows = ds.formatSqlToLimitRows(this.shortName, this.getSql(), 1000000);
						if ("ppl_lw_dependecies".toLowerCase().equals(this.shortName)) sqlLimit1MRows = this.getSql();
						crs = JdbcUtils.sqlExecQuery(conn, sqlLimit1MRows, sqlParams);

					}



					//aktualizacja statystyki LST_WCZYTWANO_KIEDY z dokładnoscią próbkowania do 15 minut
					Long lastTS = reLoadTSHM.get(this.shortName);
					if (lastTS == null) lastTS = 0L;
					if ((lastTS + 1000 * 60 * 15) < (new Date()).getTime()) //dokładność statystyk wczytywania LW (próbkowanie co 15 minut )
						JdbcUtils.sqlExecUpdate(conn, "UPDATE PPT_ADM_LISTY_WARTOSCI SET LST_WCZYTWANO_KIEDY = " +this.ds.getSysdate()+ " where LST_LP=1 and upper(LST_NAZWA) = ?", this.shortName.toUpperCase());

					reLoadTSHM.put(this.shortName, (new Date()).getTime());

					//zbieranie statystyki dot.ile razy wczytywano daną lw od startu aplikacji
					Long reLoadCntr = reLoadCountersHM.get(this.shortName);
					if (reLoadCntr == null) reLoadCntr = 0l;
					reLoadCntr++;
					reLoadCountersHM.put(this.shortName, reLoadCntr);


				} finally {

					if (konsolidacja)
						JdbcUtils.sqlSPCall(conn, "EAP_GLOBALS.USTAW_KONSOLIDACJE", "N");

				}

				loadCrsColumns(crs);
				loadCrsData(crs);
				setTs2Now();
			} catch (Exception e) {
				if (e.getMessage().contains("Sesja zakończona przez Administratora"))
					log.debug("!!! SqlDataHandler.reLoadData('" + this.shortName + "'," + (this.sqlNamedParams.isEmpty() ? this.sqlParams : this.sqlNamedParams) + ") ->" + e.getMessage());
				else
					log.error("!!! SqlDataHandler.reLoadData('" + this.shortName + "'," + (this.sqlNamedParams.isEmpty() ? this.sqlParams : this.sqlNamedParams) + ") ->\r\n"
							+ nz(this.sql)
							+ "\r\n->" + e.getMessage(), e);
			}


			AppBean.logMethod(methodname, t0);
			this.reloadTime = (new Date().getTime() - t0);
			this.reloadTimeTotal += this.reloadTime;
			log.debug("reLoadData END /" + this.shortName + " - połączenia aktywne: " + AppBean.bds.getNumActive());
		}
	}

	public void executePreQueryBlock(Connection conn)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {

		if (this.getPreQueryBlock() !=null ) {
			JdbcUtils.sqlExecNoQuery(conn, "" +
					"begin" +
					" ppp_global.ustaw_date_dla_struktury_po(sysdate); " +
					"end;");//przed prequery ustawiam typowe global'e istotne dla LW

			log.debug(this.shortName + " -> executePreQueryBlock: \n"+this.getPreQueryBlock() + "\npreQueryBlockParams: " + this.getPreQueryBlockParams() );
			CallableStatement stmt = conn.prepareCall(this.getPreQueryBlock());
			int i=0;
			for (Object p : this.preQueryBlockParams) {
				if (p instanceof java.util.Date){
					java.sql.Date date = new java.sql.Date(((java.util.Date) p).getTime());
					stmt.setObject(++i, date);
				}else
					stmt.setObject(++i, p);
			}
			stmt.execute();			//JdbcDAO.sqlExecNoQuery(conn, getPreQueryBlock());
		}
	}

	
	public void setTs2Now() {
		Date d = new Date();
		setTs(d.getTime());
	}

	public int getColumnIndex(String sqlColumnName) {
		int indexOf = this.getColumns().indexOf(sqlColumnName.toLowerCase());
		return indexOf;
	}

	public String getColumnType(String sqlColumnName) {
		int indexOf = getColumnIndex(sqlColumnName);
		if (indexOf >= 0 && this.columnsTypes != null && indexOf < this.columns.size())
			return this.columnsTypes.get(indexOf);
		return "";
	}

	private void loadCrsColumns(CachedRowSet crs) throws SQLException {
		int cnt = crs.getMetaData().getColumnCount();
		for (int i = 1; i <= cnt; i++) {
			getColumns().add(crs.getMetaData().getColumnName(i).toLowerCase());
			getColumnsTypes().add(crs.getMetaData().getColumnTypeName(i).toUpperCase());
		}
	}

	private void loadCrsData(CachedRowSet crs) throws SQLException {
		loadCrsData(crs, false);
	}
	
	
	private void loadCrsData(CachedRowSet crs, boolean asFirstRows) throws SQLException {
		crs.beforeFirst();
		String loginfo = null;
		int cnt = crs.getMetaData().getColumnCount();
		while (crs.next()) {
			ArrayList<Object> l = new ArrayList<Object>();
			for (int i = 1; i <= cnt; i++) {
				Object object = crs.getObject(i);
				if (object instanceof Clob){
					try {
						l.add (Utils.asString((Clob)object));
					} catch (IOException e) {
						log.error(e.getMessage());
					}
				} else
					l.add(object);
			}
			DataRow r = new DataRow(this, l);
			
			if (asFirstRows)
				data.add(0, r);
			else
				data.add(r);
			
			String pkString = "" + r.get(0); // załozenie - w k.0 jest pk; 

			if (pkColumnName!=null){ //jesli ktos w LW wpisal select * from tabela to id niekoniecznie jest pierwsza kolumna! w takim przypadku nalezy w konfiguracji LW wskazac, która to
//				if (!pkString.equals(r.get(pkColumnName)))
//					loginfo =  " pk: " + pkString + " --vs--> " + r.get(pkColumnName) +  " / pkColumnName: " + pkColumnName;
				pkString = "" + r.get(pkColumnName);  
			}
			
			// jest PK!!!
			this.pkIndexMap.put(pkString, r);
			this.pkRows2IndexMap.put(r, pkString);
		}
		
//		if (loginfo!=null)
//			System.out.println("\n\n\n\n\n\n" + this.shortName  + "\n"+ loginfo +  "\nsql: \n" + this.sql + "\n\n\n\n\n\n" );
	}

	public String findAndFormat(Object pk, String format) {
		if (pk == null) return "";
		this.getData();
		DataRow r = this.pkIndexMap.get(pk.toString());
		if (r == null) return "";
		return r.format(format);
	}


	public String firstRowFormat(String format) {
		DataRow r = firstRow();
		if (r == null) return "";
		return r.format(format);
	}

	private DataRow firstRow() {
		this.getData();
		if (this.data.size()>0)
			return this.data.get(0);
		else
			return null;
	}

	private DataRow lastRow() {
		this.getData();
		if (this.data.size()>0)
			return this.data.get(this.data.size()-1);
		else
			return null;
	}

	public String lastRowFormat(String format) {
		DataRow r = lastRow();
		if (r == null) return "";
		return r.format(format);
	}

	public DataRow find(Object pk) {
		this.getData();
		DataRow r = this.pkIndexMap.get("" + pk);
		return r;
	}
	
	
	public DataRow find(Object pk, boolean getData) {
		if ( getData ) this.getData();
		DataRow r = this.pkIndexMap.get("" + pk);
		return r;
	}

	public DataRow get(String pk) {
		if (pk == null || pk.equals("null")) return null;
		if (this.data.isEmpty()) this.getData();
		return this.pkIndexMap.get(pk);
	}

	public DataRow get(Object pk) {
		if (pk == null) return null;
		if (this.data.isEmpty()) this.getData();
		return this.pkIndexMap.get("" + pk);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		if (this.shortName == null)
			this.shortName = name;
	}

	public String getSql() {

		return sql;
	}

	public String setSql(String sql) {

		if (this.sql != null && !this.sql.equals(sql)) {
			// System.out.println("" + this.sql + " ?<-sql->? " + sql);
			this.setTs(0);
		}

		this.sql = sql;
		return "";
	}

	public String setSql(String sql, ArrayList<Object> sqlParams) {
		this.setSql(sql);
		this.setSqlParams(sqlParams);
		return "";
	}

	public static ConcurrentHashMap<String, Object> invalidatedSqlData = new ConcurrentHashMap<>();

	public static void invalidate(String sqlId) {
		invalidatedSqlData.put(("" + sqlId).toLowerCase().trim(), -1);
	}

	public static void invalidate(String sqlId, Object x) {
		invalidatedSqlData.put(("" + sqlId).toLowerCase().trim(), x);
	}

	public ArrayList<DataRow> getData() {
		synchronized (this) {
			Object ts1 = invalidatedSqlData.get("" + this.shortName);// TODO
			Date d = new Date();
			long time = d.getTime();
			if ((getTs() >= 0 && getTs() + this.cacheDuration < time) || ts1 != null) {// odsw. po cacheDuration //domyślnie
				// 1 minuta
				reLoadData();
				if (ts1 != null)
					invalidatedSqlData.remove("" + this.shortName);// TODO to jest błąd przy pracy rownoległej
			}

			return data;
		}
	}

	public void setData(ArrayList<DataRow> data) {
		this.data = data;
	}

	public ArrayList<String> getColumns() {
		return columns;
	}

	public void setColumns(ArrayList<String> columns) {
		this.columns = columns;
	}

	/**
	 * Czas w [ms] po jakim zbuforowane dane zostaną ponownie pobrane z bazy (czyli
	 * kiedy najwczesniej ponownie mozna przesłać do bazy zapytanie sql)
	 * 
	 * @return
	 */
	public long getCacheDuration() {
		return cacheDuration;
	}

	/**
	 * Czas w [ms] po jakim zbuforowane dane zostaną ponownie pobrane z bazy (czyli
	 * kiedy najwczesniej ponownie mozna przesłać do bazy zapytanie sql)
	 * 
	 * @param cacheDuration
	 */
	public void setCacheDuration(long cacheDuration) {
		this.cacheDuration = cacheDuration;
	}

	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = ("" + shortName).toLowerCase();
	}

	public int getGdElementWidth() {
		return gdElementWidth;
	}

	public void setGdElementWidth(int gdElementWidth) {
		this.gdElementWidth = gdElementWidth;
	}

	public int getGdElementHeight() {
		return gdElementHeight;
	}

	public void setGdElementHeight(int gdElementHeight) {
		this.gdElementHeight = gdElementHeight;
	}

	public int getGdColumnsCount() {
		return gdColumnsCount;
	}

	public void setGdColumnsCount(int gdColumnsCount) {
		this.gdColumnsCount = gdColumnsCount;
	}

	public int getGdElementsCount() {
		return gdElementsCount;
	}

	public void setGdElementsCount(int gdElementsCount) {
		this.gdElementsCount = gdElementsCount;
	}

	// public String getTableView() {
	// return tableView;
	// }
	//
	//
	// public void setTableView(String tableView) {
	// this.tableView = tableView;
	// }
	//
	//
	// public String getGridView() {
	// return gridView;
	// }
	//
	//
	// public void setGridView(String gridView) {
	// this.gridView = gridView;
	// }

	public String getDetailsPanelView() {
		return detailsPanelView;
	}

	public void setDetailsPanelView(String detailsPanelView) {
		this.detailsPanelView = detailsPanelView;
	}

	public ArrayList<Object> getSqlParams() {
		return sqlParams;
	}

	
	


		
	public String setSqlParams(ArrayList<Object> sqlParams) {
		if (sqlParams == null)
			sqlParams = new ArrayList<>();

		if (!("" + this.sqlParams).equals("" + sqlParams)) {
			this.setTs(0);// po zmienie parametrów unieważniam stan cache'a, przy kolejnym getData poleci
							// wczytanie danych...
		}

		this.sqlParams = sqlParams;
		return "";
	}

	
	public void setSqlParams(Object... params) {
		ArrayList<Object> pars = new ArrayList<Object>();
     	pars.addAll( Arrays.asList(params) );
     	setSqlParams(pars);
	}
	
	
	
	
	public Map<String, Object> getSqlNamedParams() {
		return sqlNamedParams;
	}

		
	public String setSqlNamedParams(Map<String, Object> sqlNamedParams) {
		if (sqlNamedParams == null)
			sqlNamedParams = new HashMap<>();

		if (!("" + this.sqlNamedParams).equals("" + sqlNamedParams)) {
			this.setTs(0);// po zmienie parametrów unieważniam stan cache'a, przy kolejnym getData poleci
							// wczytanie danych...
		}

		this.sqlNamedParams = sqlNamedParams;
		return "";
	}
	
		
	
	// public String getDefaultView() {
	// return defaultView;
	// }
	//
	// public void setDefaultView(String defaultView) {
	// this.defaultView = defaultView;
	// }

	public static String asString(List<DataRow> ret) {
		StringBuilder sb = new StringBuilder();
		for (DataRow r : ret) {
			sb.append(r + "\n");
		}
		return sb.toString();
	}

	public ArrayList<String> getColumnsTypes() {
		return columnsTypes;
	}

	public void setColumnsTypes(ArrayList<String> columnsTypes) {
		this.columnsTypes = columnsTypes;
	}

//	public String getDBCTXcsspObiegiUtlUstawKategorie() {
//		return DBCTXcsspObiegiUtlUstawKategorie;
//	}
//
//	public void setDBCTXcsspObiegiUtlUstawKategorie(String dBCTXcsspObiegiUtlUstawKategorie) {
//		DBCTXcsspObiegiUtlUstawKategorie = dBCTXcsspObiegiUtlUstawKategorie;
//	}
//
//	public Long getDBCTXcsspObiegiUtlUstawIdEncji() {
//		return DBCTXcsspObiegiUtlUstawIdEncji;
//	}
//
//	public void setDBCTXcsspObiegiUtlUstawIdEncji(Long dBCTXcsspObiegiUtlUstawIdEncji) {
//		DBCTXcsspObiegiUtlUstawIdEncji = dBCTXcsspObiegiUtlUstawIdEncji;
//	}

	public Boolean getContextDependent() {
		return contextDependent;
	}

	public void setContextDependent(Boolean contextDependent) {
		this.contextDependent = contextDependent;
	}

	public long getTs() {
		return ts;
	}

	public void setTs(long ts) {
		this.ts = ts;
	}

	public String resetTs() {
		this.ts = 0;
		return "";
	}

	public Boolean getKonsolidacja() {
		return konsolidacja;
	}

	public void setKonsolidacja(Boolean konsolidacja) {
		this.konsolidacja = konsolidacja;
	}

	public Boolean getCzyPdf() {
		return czyPdf;
	}

	public void setCzyPdf(Boolean czyPdf) {
		this.czyPdf = czyPdf;
	}

	public Boolean getCzyXls() {
		return czyXls;
	}

	public void setCzyXls(Boolean czyXls) {
		this.czyXls = czyXls;
	}

	public Boolean getCzyCsv() {
		return czyCsv;
	}

	public void setCzyCsv(Boolean czyCsv) {
		this.czyCsv = czyCsv;
	}

	public Boolean getCzyXml() {
		return czyXml;
	}

	public void setCzyXml(Boolean czyXml) {
		this.czyXml = czyXml;
	}

	public long getReloadTimeTotal() {
		return reloadTimeTotal;
	}

	public void setReloadTimeTotal(long reloadTimeTotal) {
		this.reloadTimeTotal = reloadTimeTotal;
	}

	public long getReloadCount() {
		return reloadCount;
	}

	public void setReloadCount(long reloadCount) {
		this.reloadCount = reloadCount;
	}

	public List<PptAdmListyWartosci> getLst() {
		return lst;
	}

	public void setLst(List<PptAdmListyWartosci> lst) {
		this.lst = lst;
	}

	public Long getLstId() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		if (this.lst!=null && !this.lst.isEmpty()){
			return this.lst.get(0).getLstId();
		}
		return null;
	}
	
	public ConcurrentHashMap<String, Object> getDependencyTablesCHM() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		if (this.dependencyTablesCHM==null){
			this.dependencyTablesCHM = new ConcurrentHashMap<>();
			Long lstId = this.getLstId();

			if (lstId!=null){
//				List<PptAdmListyTabele> lst = HibernateContext.query("from PptAdmListyTabele where LSTB_LST_ID = ?", lstId );
				List<PptAdmListyTabele> lst = this.lst.get(0).getPptAdmListyTabele();
				for (PptAdmListyTabele tb : lst) {
					this.dependencyTablesCHM.put(tb.getLstbTbtsTable(), tb);
				}

			}
		}
		return dependencyTablesCHM;
	}

	public void setDependencyTablesCHM(ConcurrentHashMap<String, Object> dependencyTablesCHM) {
		this.dependencyTablesCHM = dependencyTablesCHM;
	}

	public long getReloadTime() {
		return reloadTime;
	}

	public void setReloadTime(long reloadTime) {
		this.reloadTime = reloadTime;
	}

	public String getPreQueryBlock() {
		return preQueryBlock;
	}

	public void setPreQueryBlock(String preQueryBlock) {
		this.preQueryBlock = preQueryBlock;
	}

	public List<Object> getPreQueryBlockParams() {
		return preQueryBlockParams;
	}

	public void setPreQueryBlockParams(List<Object> preQueryBlockParams) {
		this.preQueryBlockParams = preQueryBlockParams;
	}


	HashMap<String, PptAdmListyWartosci> columnDefsHM = new HashMap<>();

	public PptAdmListyWartosci getColumnDef(String colName){
		if (colName==null) return null;
		colName = colName.toLowerCase();

		PptAdmListyWartosci ret = columnDefsHM.get(colName);
		if (ret == null){
			List<PptAdmListyWartosci> lst = this.lst;
			for (PptAdmListyWartosci coldef : lst) {
				if ( colName.equals( coldef.getLstNazwaPola().toLowerCase() )){
					columnDefsHM.put(colName, coldef);
					return coldef;
				}
			}
		}
		return ret;
	}


	public DataSourceBase getDs() {
		if (this.ds == null) {
			String dsname = "pp_db";
			if (this.getDefLst() != null && this.getDefLst().getLstDataSource() != null) {
				dsname = this.getDefLst().getLstDataSource();
			}
//			this.ds = (DataSourceBase) HibernateContext.get(DataSourceBase.class, dsname);
			this.ds = DataSourceBase.getDS(dsname);
		}
		return this.ds;
	}

	public  Connection getConnection(Boolean setContext) throws SQLException{
		Connection conn = getDs().getConnection(setContext);
		return conn;
	}

//		public DataSourceBase getDs(){
//		String dsname = null;
//		DataSourceBase ds = null;
//		if(this.getDefLst()==null || this.getDefLst().getLstDataSource()==null) {
//			ds = new DataSourceOracle();
//			ds.setDsnName("pp_db");
//			ds.setDsnTyp("ORACLE");
//			ds.setDs(JdbcUtils.ds);
//		}
//		else {
//			dsname = this.getDefLst().getLstDataSource();
//			ds = (DataSourceBase) HibernateContext.get(DataSourceBase.class, dsname);
//		}

//		return ds;
//	}

}
