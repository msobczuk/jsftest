package com.comarch.egeria.pp.data.model;

import com.comarch.egeria.pp.data.JdbcUtils;

import javax.persistence.Entity;
import java.sql.Connection;
import java.sql.SQLException;

@Entity
public class DataSourceOracle extends DataSourceBase {

    @Override
    public String getHibernateDialect() {
        return "org.hibernate.dialect.OracleDialect";
    }

    @Override
    public String formatSqlToLimitRows(String sqlName, String sql, int limitToRows) {
        String sqlLimit1MRows = String.format(
                JdbcUtils.formatStringSqlToLimitRowsLW
                , sqlName, sql, limitToRows );
        return sqlLimit1MRows;
    }

    @Override
    public String getSysdate() {
        return "SYSDATE";
    }

    @Override
    public Connection getConnection(Boolean setContext) throws SQLException {
        Connection conn = this.getDs().getConnection();
        JdbcUtils.addActiveConnInfo(conn);

        try {
            if (setContext) JdbcUtils.setContext(conn);
        } catch (Exception e) {
            conn.close();
            throw e;			//log.error(e.getMessage(), e);
        }

        //else clearContext();
        return conn;
    }
}
