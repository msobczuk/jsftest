package com.comarch.egeria.pp.data;

import com.comarch.egeria.Utils;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.utils.auth.exceptions.EgrContextException;
import com.comarch.egeria.web.common.utils.constants.Const;
import com.comarch.egeria.web.common.utils.lang.LanguageBean;
import oracle.jdbc.OracleConnection;
import oracle.jdbc.OraclePreparedStatement;
import oracle.jdbc.OracleTypes;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.EncodedResource;

import javax.faces.bean.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.sql.DataSource;
import javax.sql.rowset.CachedRowSet;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.*;
import java.util.Map.Entry;

import static com.comarch.egeria.Utils.nz;

//import com.comarch.egeria.web.User;


@ApplicationScoped
@Named
public class JdbcUtils {

	public static final Logger log = LogManager.getLogger(JdbcUtils.class);//public bo wiekszosc beanow dziedziczy po tym
	
	public static DataSource ds;//ds jest ustawiane z PostConstruct init w AppBean
	
	public static boolean useDbContext = true;
	
	public static int FETCH_SIZE = 4001;//def. w ORACLE to 10, ale to raczej za mało; zazwyczaj ustawiamy od 100 do 1000 ale na czas dev warto zwiększyć.
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////    SLEDZENIE OTAWRTYCH POLACZEN    /////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static List<JdbcConnInfo> activeConnections = Collections.synchronizedList(new ArrayList<>());
	public static void addActiveConnInfo(Connection conn) {
		if (!Const.JDBC_ACTIVE_CONN_INFO)
			return;
		
		StringBuilder sb = new StringBuilder();
		sb.append("" + new Date());
		FacesContext ctx = FacesContext.getCurrentInstance();
		if (ctx!=null){
		  sb.append( " -viewId-> " + ctx.getViewRoot().getViewId() );
		}
		
		StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
		for (int i=1; i<stElements.length; i++) {
			StackTraceElement ste = stElements[i];
			String className = ste.getClassName();
			sb.append("\n" + ste.getClassName() + "->" + ste.getMethodName());
			if (i>10) break;
		}
		
		removeClosedActiveConnInfos();
		JdbcUtils.activeConnections.add(new JdbcConnInfo(conn, sb.toString() ));
	}

	public static void removeClosedActiveConnInfos() {
		for (int i=activeConnections.size()-1; i>=0; i--){
			JdbcConnInfo ci = activeConnections.get(i);
			if (ci.isClosed())
				activeConnections.remove(i);
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	/**
	 * Pobiera nowe połączenie z puli (getDs()) i ustawia kontekst przez setContext czyli procedurę skł. cssp_obiegi_utl.ustaw_id_encji przekazując sessionId;
	 * na podstawie tego parametru kontekst jest ustawiany w bazie wg tego co zapamietano przy pierszym zalogowaniu z sesji apl. web. (uzytkownik, firma, etc... )
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnection() throws SQLException{
		Connection conn = ds.getConnection();
		addActiveConnInfo(conn);
		
		try {
			setContext(conn);	
		} catch (Exception e) {
			conn.close();
			throw e;			//log.error(e.getMessage(), e);
		}
		
		return conn;
	}


	/**
	 * Pobiera nowe polaczenie z puli (getDs()) i jeśli argumentem setContext jest true to ustawia kontekst przez setContext czyli procedurę skł. cssp_obiegi_utl.ustaw_id_encji przekazując sessionId;
	 * na podstawie tego parametru kontekst jest ustawiany w bazie wg tego co zapamietano przy pierszym zalogowaniu z sesji apl. web. (uzytkownik, firma, etc... )
	 * @param setContext
	 * @return
	 * @throws SQLException
	 */
	public static Connection getConnection(Boolean setContext) throws SQLException{
		Connection conn = ds.getConnection();
		addActiveConnInfo(conn);
		
		try {
			if (setContext) setContext(conn);	
		} catch (Exception e) {
			conn.close();
			throw e;			//log.error(e.getMessage(), e);
		}
		
		//else clearContext();
		return conn;
	}
	
	

	

//	public static void init(Connection conn, String dbUsername, String sessionId) throws SQLException, EgrAuthInitException {
//		String ret;
//		String poKomunikat;
//
//		try {
//			
//			CallableStatement cs = conn.prepareCall("{? = call eap_web.inicjuj( ?, ?, ? )}");
//			cs.registerOutParameter(1, OracleTypes.VARCHAR);
//			cs.setString(2, dbUsername);
//			cs.setString(3, sessionId);
//			cs.registerOutParameter(4, OracleTypes.VARCHAR);
//			cs.execute();
//			ret = cs.getString(1);
//	
//			if ("N".equals(ret)) {
//				poKomunikat = cs.getString(4);
//				throw new EgrAuthInitException(poKomunikat);
//			}
//			
//		} finally {
//		}
//
//	}
	
	
	//pierwotnie kontekst jest ustawiany z podaniem nazwy usera przy logowaniu, potem po stronie bazy wystarczy podac sessionId....
	public static void setContext(Connection conn) throws EgrContextException {
		if (!useDbContext) return;
		
		FacesContext ctx = FacesContext.getCurrentInstance(); 
		if (ctx==null) return;
		
		String sessionId = ctx.getExternalContext().getSessionId(false);
		
		if (sessionId == null || sessionId.equals("")) return;
		
		try {
			CallableStatement cs = conn.prepareCall("{? = call eap_web.ustaw_kontekst(?, ?)}");

			cs.registerOutParameter(1, OracleTypes.VARCHAR);
			cs.setString(2, sessionId);
			cs.registerOutParameter(3, OracleTypes.VARCHAR);
			cs.execute();
			User user = User.getCurrentUser();
			if (cs.getString(1).equals("N")){

				String msg = nz(cs.getString(3));
				if (msg.contains("Sesja zakończona przez Administratora")){
					if (user!=null) user.logout(true);
				}else {
					throw new EgrContextException(msg);
				}


//				String msg = ""+cs.getString(3);
//				if ( msg.contains("Sesja zakończona przez Administratora") ) 
//					return;
//				else 
//					throw new EgrContextException(msg);				
			}
			
			if(user != null) {
				sqlSPCall(conn, "ppp_global.ustaw_pracownika", user.getPrcId());
			}
			
		} catch (SQLException e) {
			//e.printStackTrace();
			throw new EgrContextException(LanguageBean.translate("INF_database_error"));
		}
	}
	
	
//	public static void init(Connection conn, String dbUsername) throws SQLException, EgrAuthInitException {
//		String ret;
//		String poKomunikat;
//		String sessionId = FacesContext.getCurrentInstance().getExternalContext().getSessionId(false);
//		
//		CallableStatement cs = conn.prepareCall("{? = call eap_web.inicjuj( ?, ?, ? )}");
//		cs.registerOutParameter(1, OracleTypes.VARCHAR);
//		cs.setString(2, dbUsername);
//		cs.setString(3, sessionId);
//		cs.registerOutParameter(4, OracleTypes.VARCHAR);
//		cs.execute();
//		ret = cs.getString(1);
//
//		if ("N".equals(ret)) {
//			poKomunikat = cs.getString(4);
//			throw new EgrAuthInitException(poKomunikat);
//		}
//		
//	}
	
	

//	@Override
//	public void clearContext(String sessionId) {
//		try (Connection conn = jdbc.getConnection()) {
//			CallableStatement cs = conn.prepareCall("{call eap_web.czysc_kontekst(?)}");
//
//			cs.setString(1, sessionId);
//			cs.execute();
//		} catch (SQLException e) {
//			log.error(e.getMessage(), e);
//		}
//		log.debug("Wyczyszczono kontekst");
//	}
	
	

	
	public static void sqlSPCall(Connection conn, String procedureName, Object... args) throws SQLException{
		sqlSPCall(conn, procedureName, Arrays.asList(args));
	}
	
	
	public static void sqlSPCall(Connection conn, String procedureName, List<Object> params) throws SQLException{
		CallableStatement cs = null;
		String txt = "\nsqlCallSP " + LocalDateTime.now()
		+ "\n---------------------------------------------------------------\n" 
		+    procedureName 
		+ "\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
		+    "params: " + params
		+ "\n---------------------------------------------------------------";
		//System.err.println(txt);
		log.debug(txt);
		
		try {
			String txtParams = "";
			
			if (params!=null && !params.isEmpty())
				txtParams = new String(new char[params.size()]).replace('\0', ' ').replaceAll(" ", " ? ").replaceAll("  ", ",").trim();
			
			cs = conn.prepareCall("{call " + procedureName + "(" + txtParams + ")}");
			int i=0;
			for (Object p : params) {
				if (p instanceof JdbcOutParameter){
					JdbcOutParameter pOut = (JdbcOutParameter) p;
					cs.registerOutParameter(++i, pOut.getType());
					pOut.setIndex(i);
				}else{
					//cs.setObject(++i, p);
					if (p instanceof java.util.Date){
						java.sql.Date date = new java.sql.Date(((java.util.Date) p).getTime());
						cs.setObject(++i, date);
					}else
						cs.setObject(++i, p);
				}
			}	

			cs.execute();
			
			for (Object p : params) {
				if (p instanceof JdbcOutParameter){
					JdbcOutParameter pOut = (JdbcOutParameter) p;
					pOut.setValue(cs.getObject(pOut.getIndex()));
				}
			}
			cs.clearParameters();
		} finally {
			if (cs != null) try {	cs.close();	} catch (SQLException ignore) {}
		}
	}
	
	
	
	
	
	
	//FUNKCJE SKALARNE... wybrane zwracane typy danych
	public static String sqlFnCLOB(Connection conn, String functionName, Object... args) throws SQLException, IOException {
		return Utils.asString((java.sql.Clob) sqlCallFN(conn, functionName,  java.sql.Types.CLOB, Arrays.asList(args)));
	}

	public static String sqlFnVarchar(Connection conn, String functionName, Object... args) throws SQLException{
		return (String) sqlCallFN(conn, functionName,  java.sql.Types.VARCHAR, Arrays.asList(args));
	}

	public static Date sqlFnDate(Connection conn, String functionName, Object... args) throws SQLException{
		return (Date) sqlCallFN(conn, functionName, java.sql.Types.DATE, Arrays.asList(args)); 
	}
	
	public static Date sqlFnDateTime(Connection conn, String functionName, Object... args) throws SQLException{
		return (Date) sqlCallFN(conn, functionName, java.sql.Types.TIMESTAMP, Arrays.asList(args)); 
	}
	
	
	public static Long sqlFnLong(Connection conn, String functionName, Object... args) throws SQLException{
		 Object ret = sqlCallFN(conn, functionName, java.sql.Types.NUMERIC, Arrays.asList(args)); 
		 if (ret instanceof BigDecimal)
			 return ((BigDecimal) ret).longValue();
		 else if (ret instanceof BigInteger)
			 return ((BigInteger) ret).longValue();
		 
		 return  (Long) ret;//TODO... dokoczyć rzutowania... 
	}
	
	public static Integer sqlFnInteger(Connection conn, String functionName, Object... args) throws SQLException{
		 Object ret = sqlCallFN(conn, functionName, java.sql.Types.INTEGER, Arrays.asList(args)); 
		 if (ret instanceof BigDecimal)
			 return ((BigDecimal) ret).intValue();
		 else if (ret instanceof BigInteger)
			 return ((BigInteger) ret).intValue();
		 
		 return  (Integer) ret;//TODO... dokoczyć rzutowania... 
	}
	
	
	public static Double sqlFnDouble(Connection conn, String functionName, Object... args) throws SQLException{
		 Object ret = sqlCallFN(conn, functionName, java.sql.Types.NUMERIC, Arrays.asList(args)); 
		 if (ret instanceof BigDecimal)
			 return ((BigDecimal) ret).doubleValue();
		 return  (Double) ret;//TODO... dokoczyć rzutowania... 
	}
	
	
	public static BigDecimal sqlFnBigDecimal(Connection conn, String functionName, Object... args) throws SQLException{
		 Object ret = sqlCallFN(conn, functionName, java.sql.Types.NUMERIC, Arrays.asList(args)); 
		 return ((BigDecimal) ret);
	}	
	
	
	public static Object sqlCallFN(Connection conn, String functionName, int returnedSqlType, List<Object> params) throws SQLException{
		Object ret = null;
		CallableStatement cs = null;
		String txt = "\nsqlCallSP / rettype: " + returnedSqlType + " / " + LocalDateTime.now()
		+ "\n---------------------------------------------------------------\n" 
		+    functionName 
		+ "\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
		+    "params: " + params
		+ "\n---------------------------------------------------------------";
		//System.err.println(txt);
		log.debug(txt);
		
		try {
			String txtParams = "";
			
			if (params!=null && !params.isEmpty())	txtParams = new String(new char[params.size()]).replace('\0', ' ').replaceAll(" ", " ? ").replaceAll("  ", ",").trim();
			
			cs = conn.prepareCall("{? = call " + functionName + "(" + txtParams + ")}");
//			cs = conn.prepareCall("{call ? := " + functionName + "(" + txtParams + ")}"); 
			cs.registerOutParameter(1, returnedSqlType);
			
			int i=1;//...InParameters
			for (Object p : params) {
				if (p instanceof JdbcOutParameter){
					JdbcOutParameter pOut = (JdbcOutParameter) p;
					cs.registerOutParameter(++i, pOut.getType());
					pOut.setIndex(i);
				} else if (p instanceof java.util.Date){
					cs.setObject(++i, Utils.asSqlDate((Date) p));
				} else if (p instanceof LocalDate){
					cs.setObject(++i, Utils.asSqlDate((LocalDate) p));
//				} else if (p instanceof LocalDateTime){
//					cs.setObject(++i, Utils.asSqlDate((LocalDateTime) p));
				}else
					cs.setObject(++i, p);
			}	

			cs.execute();
			
			for (Object p : params) {
				if (p instanceof JdbcOutParameter){
					JdbcOutParameter pOut = (JdbcOutParameter) p;
					pOut.setValue(cs.getObject(pOut.getIndex()));
				}
			}
			ret = cs.getObject(1); //cs.getString(1);
			cs.clearParameters();
			
		} finally {
			if (cs != null) try {	cs.close();	} catch (SQLException ignore) {}
		}
	
		return ret;
	}

	
	
	public static void sqlExecNoQuery(Connection con, String sql, Object... args) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		
		String txt = "\nsqlExecNoQuery " + LocalDateTime.now()
		+ "\n---------------------------------------------------------------\n" 
		+    sql 
		+ "\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
		+    "sqlParams: " + JdbcUtils.normalizeSqlParams(args)
		+ "\n---------------------------------------------------------------";
		//System.err.println(txt);
		log.debug(txt);
		
		PreparedStatement stmt = con.prepareStatement(sql);
		int i = 0;
		for (Object p : args) {
			stmt.setObject(++i, p);
		}
		stmt.executeQuery();
	}
	
	
	public static void sqlExecNoQuery(String sql, Object... args) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		try(Connection conn = JdbcUtils.getConnection()){
			 JdbcUtils.sqlExecNoQuery(conn, sql, args);
		}
	}	
	
	

	
	public static Object sqlExecScalarQuery(Connection con, String sql, Object... args) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		
		try {
			
			Object ret = null;
			
			CachedRowSet crs = sqlExecQuery(1, con, sql, Arrays.asList(args));
			crs.setFetchSize(1);
			crs.setMaxRows(1);
			
			while (crs.next()) {
				ret = crs.getObject(1);
				break;
			}
			//crs.close();
			return ret;

			
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	public static Object sqlExecScalarQuery(String sql, Object... args) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		Object ret = null;
		try(Connection conn = JdbcUtils.getConnection()){
			ret = JdbcUtils.sqlExecScalarQuery(conn, sql, args);
			return ret;
		}
	}	
	
	
	

	/**
	 * Metoda pobiera dane z bazy i zwraca jako CachedRowSet 
	 * w tej wersji nie zamykamy polaczenia, ktore zostalo przekazane z zewn, i musimy obsluzyc wyjatki 
	 * !!!! Koniecznie nalezy zadbać o zamkniecie przekazywanego polaczenia we wlasnej sekcji finlally !!!! 
	 * @param con
	 * @param sql
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static CachedRowSet sqlExecQuery(Connection con, String sql) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		String methodname = "sqlExecQuery";
		long t0 = AppBean.logMethod(methodname);
		
		ResultSet rs = null;
		Statement stmt = null;
		try {
			String txt = "\nsqlExecQuery " + LocalDateTime.now()
			+ "\n---------------------------------------------------------------\n" 
			+    sql 
			+ "\n---------------------------------------------------------------";
			//System.err.println(txt);
			log.debug(txt);
			CachedRowSet crs = null;
			
			stmt = con.createStatement( ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY );
			stmt.setFetchSize(FETCH_SIZE);
			rs = stmt.executeQuery(sql);
			rs.setFetchSize(FETCH_SIZE);
			Class<?> c = Class.forName("com.sun.rowset.CachedRowSetImpl");
			crs = (CachedRowSet) c.newInstance();
			crs.setFetchSize(FETCH_SIZE);
			crs.populate(rs);
	
			//System.out.println("zbuforowanych wierszy: " + crs.size());
			log.debug("zbuforowanych wierszy: " + crs.size());
			AppBean.logMethod(txt+"\nzbuforowanych wierszy: " + crs.size() + " // " + methodname, t0);
			return crs;
			
//		} catch (Exception e) {
//			
//			System.err.println("!!!>>>exc<<<!!!: " + e.toString());
//			
		} finally {
		    if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
		    if (stmt != null) try { stmt.close(); } catch (SQLException ignore) {}
		    
		}
	}
	
	
	
	
	
	
	/**
	 * Metoda pobiera dane z bazy i zwraca jako CachedRowSet (lub null w przypadku błędu)
	 * odwolujemy sie tylko do ds (polaczenie jest tworzone i zamykane), wyjatki wylapane; w razie bledu wynikiem jest null
	 * @param ds
	 * @param sql
	 * @return
	 */
	public static CachedRowSet sqlExecQuery(DataSource ds, String sql) {
//		System.err.println(".....sqlExecQuery.....");
		Connection con = null;
		
		try {
			con  = JdbcUtils.getConnection();
			setContext(con);
			return sqlExecQuery(con, sql);
			
		} catch (Exception e) {
			String txt = "!!!>>>sqlExecQuery>>>exc<<<!!!: " + e.toString();
			//System.err.println(txt);
			log.error(txt);
		} finally {
			if (con != null) try { con.close(); } catch (SQLException ignore) {}
		}

		return null;
	}



	public static CachedRowSet sqlExecQuery(Connection con, String sql, Object... args) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		return sqlExecQuery(con, sql, Arrays.asList(args));
	} 

	
	/**
	 * Metoda pobiera dane z bazy i zwraca jako CachedRowSet 
	 * w tej wersji nie zamykamy polaczenia, ktore zostalo przekazane z zewn, i musimy obsluzyc wyjatki 
	 * !!!! Koniecznie nalezy zadbać o zamkniecie przekazywanego polaczenia we wlasnej sekcji finlally !!!! 
	 * @param con
	 * @param sql
	 * @param params
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static CachedRowSet sqlExecQuery(Connection con, String sql, List<Object> params) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		String methodname = "sqlExecQuery";
		long t0 = AppBean.logMethod(methodname);
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
	
			if (params == null || params.isEmpty()) {
				return JdbcUtils.sqlExecQuery(con, sql);
			}
			
			String txt = "\nsqlExecQuery " + LocalDateTime.now()
			+ "\n---------------------------------------------------------------\n" 
			+    sql 
			+ "\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
			+    "sqlParams: " + JdbcUtils.normalizeSqlParams(params)
			+ "\n---------------------------------------------------------------";
			//System.err.println(txt);
			log.debug(txt);
	
			CachedRowSet crs = null;
			
			
			stmt = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			stmt.setFetchSize(FETCH_SIZE);
			
			int i = 0;
			for (Object p : params) {
				if (p instanceof java.util.Date){
					java.sql.Date date = new java.sql.Date(((java.util.Date) p).getTime());
					stmt.setObject(++i, date);
				}else
					stmt.setObject(++i, p);
			}
			
			rs = stmt.executeQuery();
			rs.setFetchSize(FETCH_SIZE);
			
			Class<?> c = Class.forName("com.sun.rowset.CachedRowSetImpl");
			crs = (CachedRowSet) c.newInstance();
			crs.setFetchSize(FETCH_SIZE);
			
			crs.populate(rs);
	
			//System.out.println("zbuforowanych wierszy: " + crs.size());
			log.debug("zbuforowanych wierszy: " + crs.size());
			AppBean.logMethod(txt+"\nzbuforowanych wierszy: " + crs.size() + " // " + methodname, t0);
			return crs;
			
		} finally {
		    if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
		    if (stmt != null) try { stmt.close(); } catch (SQLException ignore) {}

		}
		
	}	
	
	
	
	
	
	
	
	
	/**
	 * Metoda pobiera dane z bazy i zwraca jako CachedRowSet 
	 * w tej wersji nie zamykamy polaczenia, ktore zostalo przekazane z zewn, i musimy obsluzyc wyjatki 
	 * !!!! Koniecznie nalezy zadbać o zamkniecie przekazywanego polaczenia we wlasnej sekcji finlally !!!! 
	 * @param con
	 * @param sql
	 * @param params
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static CachedRowSet sqlExecQuery(Connection con, String sql, Map<String, Object> params) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		String methodname = "sqlExecQuery";
		long t0 = AppBean.logMethod(methodname);
		
		ResultSet rs = null;
		OraclePreparedStatement stmt = null;
		try {
	
			if (params == null || params.isEmpty()) {
				return JdbcUtils.sqlExecQuery(con, sql);
			}
			
			String txt = "\nsqlExecQuery " + LocalDateTime.now()
			+ "\n---------------------------------------------------------------\n" 
			+    sql 
			+ "\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
			+    "sqlParams: " + JdbcUtils.normalizeSqlParams(params)
			+ "\n---------------------------------------------------------------";
			//System.err.println(txt);
			log.debug(txt);
	
			CachedRowSet crs = null;
			
			Connection conn= con.unwrap(OracleConnection.class); //ApacheTomcat / con instanceof org.apache.tomcat.dbcp.dbcp2.PoolingDataSource$PoolGuardConnectionWrapper
			
			stmt = (OraclePreparedStatement) conn.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			stmt.setFetchSize(FETCH_SIZE);
			
			int i = 0;
			for (Entry p : params.entrySet() ) {
				if (p.getValue() instanceof java.util.Date){
					java.sql.Date date = new java.sql.Date(((java.util.Date) p.getValue()).getTime());
					stmt.setObjectAtName(""+p.getKey(), date);
				}else
					stmt.setObjectAtName(""+p.getKey(), p.getValue());
			}
			
			rs = stmt.executeQuery();
			rs.setFetchSize(FETCH_SIZE);
			
			Class<?> c = Class.forName("com.sun.rowset.CachedRowSetImpl");
			crs = (CachedRowSet) c.newInstance();
			crs.setFetchSize(FETCH_SIZE);
			
			crs.populate(rs);
	
			//System.out.println("zbuforowanych wierszy: " + crs.size());
			log.debug("zbuforowanych wierszy: " + crs.size());
			AppBean.logMethod(txt+"\nzbuforowanych wierszy: " + crs.size() + " // " + methodname, t0);
			return crs;
			
		} finally {
		    if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
		    if (stmt != null) try { stmt.close(); } catch (SQLException ignore) {}

		}
		
	}	
	
	
	/**
	 * Metoda pobiera dane z bazy i zwraca jako CachedRowSet 
	 * w tej wersji nie zamykamy polaczenia, ktore zostalo przekazane z zewn, i musimy obsluzyc wyjatki 
	 * !!!! Koniecznie nalezy zadbać o zamkniecie przekazywanego polaczenia we wlasnej sekcji finlally !!!! 
	 * @param con
	 * @param sql
	 * @param params
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static CachedRowSet sqlExecQuery(int fetchSize, Connection con, String sql, List<Object> params) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		String methodname = "sqlExecQuery";
		long t0 = AppBean.logMethod(methodname);
		
		ResultSet rs = null;
		PreparedStatement stmt = null;
		try {
	
			if (params == null || params.isEmpty()) {
				return JdbcUtils.sqlExecQuery(con, sql);
			}
			
			String txt = "\nsqlExecQuery " + LocalDateTime.now()
			+ "\n---------------------------------------------------------------\n" 
			+    sql 
			+ "\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
			+    "sqlParams: " + JdbcUtils.normalizeSqlParams(params)
			+ "\n---------------------------------------------------------------";
			//System.err.println(txt);
			log.debug(txt);
	
			CachedRowSet crs = null;
			
			stmt = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY );
			stmt.setFetchSize(fetchSize);
			
			int i = 0;
			for (Object p : params) {
				stmt.setObject(++i, p);
			}
			
			rs = stmt.executeQuery();
			rs.setFetchSize(fetchSize);
			
			Class<?> c = Class.forName("com.sun.rowset.CachedRowSetImpl");
			crs = (CachedRowSet) c.newInstance();
			crs.setFetchSize(fetchSize);
			
			crs.populate(rs);
	
			//System.out.println("zbuforowanych wierszy: " + crs.size());
			log.debug("zbuforowanych wierszy: " + crs.size());
			AppBean.logMethod(txt+"\nzbuforowanych wierszy: " + crs.size() + " // " + methodname, t0);
			return crs;
			
		} finally {
		    if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
		    if (stmt != null) try { stmt.close(); } catch (SQLException ignore) {}

		}
		
	}	
	
	
	
	/**
	 * Metoda pobiera dane z bazy i zwraca jako CachedRowSet (lub null w przypadku błędu)
	 * odwolujemy sie tylko do ds (polaczenie jest tworzone i zamykane), wyjatki wylapane; w razie bledu wynikiem jest null
	 * @param ds
	 * @param sql
	 * @param params
	 * @return
	 */
	public static CachedRowSet sqlExecQuery(DataSource ds, String sql, List<Object> params) {
		String methodname = "sqlExecQuery";
		long t0 = AppBean.logMethod(methodname);
		
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try {
	
			if (params == null || params.isEmpty()) {
				return JdbcUtils.sqlExecQuery(ds, sql);
			}
			
	
			String txt = "\nsqlExecQuery " + LocalDateTime.now()
			+ "\n---------------------------------------------------------------\n" 
			+    sql 
			+ "\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
			+    "sqlParams: " + JdbcUtils.normalizeSqlParams(params)
			+ "\n---------------------------------------------------------------";
			//System.err.println(txt);
			log.debug(txt);
	
			CachedRowSet crs = null;
			con = JdbcUtils.getConnection();
			setContext(con);
			stmt = con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY );
			int i = 0;
			for (Object p : params) {
				stmt.setObject(++i, p);
			}
			
			rs = stmt.executeQuery();
			Class<?> c = Class.forName("com.sun.rowset.CachedRowSetImpl");
			crs = (CachedRowSet) c.newInstance();
			crs.populate(rs);
	
			//System.out.println("zbuforowanych wierszy: " + crs.size());
			log.debug("zbuforowanych wierszy: " + crs.size());
			AppBean.logMethod(txt+"\nzbuforowanych wierszy: " + crs.size() + " // " + methodname, t0);
			return crs;
			
		} catch (Exception e) {
			
			String txt = "!!!>>>exc<<<!!!: " + e.toString();
			//System.err.println(txt);
			log.error(txt);
			
		} finally {
		    if (rs != null) try { rs.close(); } catch (SQLException ignore) {}
		    if (stmt != null) try { stmt.close(); } catch (SQLException ignore) {}
		    if (con != null) try { con.close(); } catch (SQLException ignore) {}
		}
		
		return null;
	}
	
	
	public static CachedRowSet sqlExecQuery(String sql, Object... args) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		try(Connection conn = JdbcUtils.getConnection()){
			  return JdbcUtils.sqlExecQuery(conn, sql, args);
		}
	}	
	
	


	public static int sqlExecUpdate(Connection con, String sql, Object... args)
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			String txt = "\nsqlExecUpdate " + LocalDateTime.now() + "\n---------------------------------------------------------------\n"
					+ sql + "\n- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n" + "sqlParams: " + JdbcUtils.normalizeSqlParams(args)
					+ "\n---------------------------------------------------------------";
			// System.err.println(txt);
			log.debug(txt);

			int i = 0;
//			for (Object p : args) {
//				stmt.setObject(++i, p);
//			}

			for (Object p : args) {
				 if (p instanceof java.util.Date){
					 stmt.setObject(++i, Utils.asSqlDate((Date) p));
				} else if (p instanceof LocalDate){
					 stmt.setObject(++i, Utils.asSqlDate((LocalDate) p));
				} else if (p instanceof InputStream){
					 stmt.setBlob(++i, (InputStream) p);
				}else
					 stmt.setObject(++i, p);
			}


			return stmt.executeUpdate();
		}
	}
	
	
	public static int sqlExecUpdate(String sql, Object... args) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		try (Connection conn = JdbcUtils.getConnection()) {
			return JdbcUtils.sqlExecUpdate(conn, sql, args);
		}
	}	


	public static String formatStringSqlToLimitRowsLW = "select * from ( -- %1$s " 
								+ "\r\n---------------------------------------------------------------"
								+ "\r\n%2$s"
								+ "\r\n---------------------------------------------------------------"
								+ "\r\n) where rownum <= %3$s";


	public static String formatStringSqlToLimitRows = "select * from ( "
								+ "\r\n---------------------------------------------------------------"
								+ "\r\n%1$s"
								+ "\r\n---------------------------------------------------------------"
								+ "\r\n) where rownum <= %2$s";
								
	/**
	 * Opakowuje zapytanie tak aby pobrac ograniczona liczbe wierszy (co powinno uchronic serwer aplikacji przed potencjalnymi bledamo typu OutOfMemoryError)
	 * TODO: W przypadku przejscia na inny RDBMS przepisac skladnie; Dla oracle'a mozna zotpytmalizowac dla 12c z uzyciem ... fetch first N rows only
	 * @param sqlName
	 * @param sql
	 * @param limitToRows
	 * @return
	 */
	public static String formatSqlToLimitRows(String sqlName, String sql, int limitToRows) {
		String sqlLimit1MRows = String.format(
							formatStringSqlToLimitRowsLW
							, sqlName, sql, limitToRows );
		return sqlLimit1MRows;
	}

	/**
	 * Opakowuje zapytanie tak aby pobrac ograniczona liczbe wierszy (co powinno uchronic serwer aplikacji przed potencjalnymi bledamo typu OutOfMemoryError)
	 * TODO: W przypadku przejscia na inny RDBMS przepisac skladnie; Dla oracle'a mozna zotpytmalizowac dla 12c z uzyciem ... fetch first N rows only
	 * @param sql
	 * @param limitToRows
	 * @return
	 */
	public static String formatSqlToLimitRows(String sql, int limitToRows) {

		String sqlLimit1MRows = String.format(
							formatStringSqlToLimitRows
							,  sql, limitToRows );
		return sqlLimit1MRows;
	}


	public static ArrayList<Object> normalizeSqlParams(Object... params){
		return JdbcUtils.normalizeSqlParams(Arrays.asList(params));
	}	

	public static ArrayList<Object> normalizeSqlParams(Collection<Object> params){
		ArrayList<Object> al = new ArrayList<>();
	
		if (params==null || params.isEmpty())
			return al;
		
		for (Object p : params) {
			
			if (p==null) 
				al.add("''"); //ORACLE !MSSQL 
			else if (p instanceof String)
				al.add("'"+p+"'");
			else
				al.add(p);
			
		}
		
		return al;
	}

	public static String formatAsCommaSeparatedNumbersString(List<Long> al) {
		String listString  = al.toString();
		listString = listString.substring(1, listString.length() - 1);
		return listString;
	}	


	public static void czyscKontekst(String sessionId){
		if (sessionId==null || sessionId.isEmpty())
			return;

		try (Connection conn = JdbcUtils.getConnection(false))  {
			JdbcUtils.sqlExecNoQuery(conn, "call eap_web.czysc_kontekst(?)", sessionId);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}



	public static void executeSqlScript(Resource resource, String encoding) throws SQLException, IOException {
		try(Connection conn = JdbcUtils.getConnection()){
			executeSqlScript(conn, resource, encoding);
		}
	}

    public static void executeSqlScript(Connection conn, Resource resource, String encoding) throws IOException, SQLException {
        EncodedResource encResource = new EncodedResource(resource, encoding); //narazie ważne - stosujemy CP1250 przy takich DML

		Path pthPre = Paths.get(resource.getFile().getAbsolutePath() + ".pre");
		if (pthPre.toFile().exists()){
			executeStatement(conn, pthPre);	//sqlPre = getSqlFromFile(pthPre);
		}

		log.debug("executeSqlScript / "  + resource.getDescription() + " / " + resource.getFilename());
		org.springframework.jdbc.datasource.init.ScriptUtils.executeSqlScript(conn, encResource,false,false, "--",";","/*","*/");
/*
UWAGA:
org.springframework.jdbc.datasource.init.ScriptUtils.executeSqlScript to badziewie - nieprawidlowo interpretuje literal jako komentarz i przez to źle dzieli skrypt na polecenia
jesli w trybie singleQuote trafi sie na koncu komentarz, a na jego koncu jest znak "'" to komentarz zostanie wycięty razem z tym znakiem "'" co powoduje ze nei jest rozpoznane zakonczenie trybu singleQuote

file1.sql:
-----------------------------------------
update tab1 set col1='Ala ma
--kota '; -- whole line is erased?!
update tab2 set col1='Ala ma --psa ';
-----------------------------------------

excuted scrip by org.springframework.jdbc.datasource.init.ScriptUtils.executeSqlScript:
-----------------------------------------
update tab1 set col1='Ala ma
update tab2 set col1='Ala ma --psa ';
-----------------------------------------
*/
		Path pthPost = Paths.get(resource.getFile().getAbsolutePath() + ".post");
		if (pthPost.toFile().exists()){
			executeStatement(conn, pthPost);	//sqlPre = getSqlFromFile(pthPre);
		}

	}



	public static void executeDmlScriptAsBeginEndStatement(Connection conn, Path filePath) throws IOException, SQLException {
		String sql = getSqlFromFile(filePath);

		Path pthPre = Paths.get(filePath.toString() + ".pre");
		if (pthPre.toFile().exists()){
			executeStatement(conn, pthPre);	//sqlPre = getSqlFromFile(pthPre);
		}

		log.debug("executeDmlScriptAsBeginEndStatement: " + filePath);

		Statement cs0 = conn.createStatement();
		cs0.execute("alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS'");
		sql = sql.replace("alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';", "");
		sql = "BEGIN \n"+ sql +"\nEND;";

		Statement cs = conn.createStatement();
		cs.execute(sql);

		Path pthPost = Paths.get(filePath.toString() + ".post");
		if (pthPost.toFile().exists()){
			executeStatement(conn, pthPost);
		}
	}

	private static String getSqlFromFile(Path filePath) throws IOException {
		List<String> lines = Files.readAllLines(filePath, Charset.forName("Cp1250"));
		String sql = "";

		for (String line : lines) {
			if (line.equals("/")) return sql;

			sql += line + "\n";
		}

		return sql.substring(0, sql.length() - 1);
	}




//	public static void executeCallableStatement(Path filePath) throws IOException, SQLException {
//		String sql = getSqlFromFile(filePath);
//		try (Connection conn = JdbcUtils.getConnection()) {
//			CallableStatement cs = conn.prepareCall(sql);
//			cs.execute();
//		}
//	}


	public static void executeStatement(Path filePath) throws IOException, SQLException {
		try (Connection conn = JdbcUtils.getConnection()) {
			executeStatement(conn, filePath);
		}
	}


	public static void executeStatement(Connection conn, Path filePath) throws IOException, SQLException {
		String sql = getSqlFromFile(filePath);

		Path pthPre = Paths.get(filePath.toString() + ".pre");
		if (pthPre.toFile().exists()){
		 	executeStatement(conn, pthPre);	//sqlPre = getSqlFromFile(pthPre);
		}

		log.debug("executeStatement: " + filePath);
		Statement cs = conn.createStatement();
		cs.execute(sql);

		Path pthPost = Paths.get(filePath.toString() + ".post");
		if (pthPost.toFile().exists()){
			executeStatement(conn, pthPost);
		}

	}






	public static String getViewSource(Connection conn, String owner, String objectName,  boolean asCreateOrReplace, boolean addOnwer, boolean appendSlash, boolean addRevisionHeader) throws IOException, SQLException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        //String ret = JdbcUtils.sqlFnCLOB(conn, "PPADM.DMP_VIEW_SOURCE", owner, objectName);
        //'\n CREATE OR REPLACE FORCE EDITIONABLE VIEW "PPADM"."EK_ZATRUDNIENIE" ("ZAT_ID", "ZAT_FNK_ID"..., "ZAT_OB_ID_WYDZIAL") AS \r\nSELECT \r\n ... \r\nFROM EGADM1.EK_ZATRUDNIENIE z\r\n'

        String ret = "view "
                + (addOnwer ?  owner + "." : "")
                + objectName + " as \r\n";


		String sql = nz((String) sqlExecScalarQuery(conn, "select TEXT from SYS.all_views where owner = ? and view_name = ?", owner, objectName));
		if (sql.isEmpty())
			return sql; //brak widoku w bazie

		if (addRevisionHeader && !sql.contains("$Revision::")) {
			int i = sql.indexOf("\n") + 1;//tuz za pierszym \n
			String sql0 = sql.substring(0, i);
			String sql1 = sql.substring(i);
			sql = sql0 + "-- $Revision::          $-- --ascii/cp1250:ąęćłńóśźż--\r\n" + sql1;
		}


		ret += sql;
        //"SELECT
        //-- $Revision::          $--
        //	z."ZAT_ID",z."ZAT_FNK_ID",z."ZAT_PRC_ID",z."ZAT_SK_ID",z."ZAT_DATA_PRZYJ",z."ZAT_DATA_ZMIANY",z."ZAT_STAWKA",z."ZAT_TYP_UMOWY",z."ZAT_GLOWNY_ZAKLAD_PRACY",z."ZAT_KTO_UTWORZYL",z."ZAT_KIEDY_UTWORZYL",z."ZAT_OKRES_DO",z."ZAT_DATA_ZWOLNIENIA",z."ZAT_WYMIAR",z."ZAT_TYP_STAWKI",z."ZAT_SPOSOB_PRZYJECIA",z."ZAT_SPOSOB_ZWOLNIENIA",z."ZAT_STATUS",z."ZAT_KTO_MODYFIKOWAL",z."ZAT_KIEDY_MODYFIKOWAL",z."ZAT_NR_UMOWY",z."ZAT_TEMAT",z."ZAT_ZUS",z."ZAT_KOSZTY",z."ZAT_F_PROBNY",z."ZAT_PP_ID",z."ZAT_TYP_ANGAZ",z."ZAT_KZ_ID",z."ZAT_OB_ID",z."ZAT_TERMIN_ZAPLATY",z."ZAT_DZIEN_ZAPLATY",z."ZAT_WAL_ID",z."ZAT_PLATNOSC",z."ZAT_WOJSKOWY_UP",z."ZAT_STAWKA_AKORD",z."ZAT_STATUS_DATA_OD",z."ZAT_STATUS_DATA_DO",z."ZAT_UBEZP_OB_DATA",z."ZAT_UBEZP_ZDROW_OB_DATA",z."ZAT_UBEZP_OB_EMERYT",z."ZAT_UBEZP_OB_RENTA",z."ZAT_UBEZP_OB_CHOROB",z."ZAT_UBEZP_OB_WYPADK",z."ZAT_UBEZP_ZDROW_DB_DATA",z."ZAT_UBEZP_DB_EMERYT",z."ZAT_UBEZP_DB_EMERYT_DATA",z."ZAT_UBEZP_DB_RENTA",z."ZAT_UBEZP_DB_RENTA_DATA",z."ZAT_UBEZP_DB_CHOROB",z."ZAT_UBEZP_DB_CHOROB_DATA",z."ZAT_UBEZP_DB_CHOROB_KWOTA",z."ZAT_ODPOWIEDZIALNOSC",z."ZAT_STAWKA_POSTOJOWA",z."ZAT_DATA_DO",z."ZAT_GPRC_KOD",z."ZAT_WYR_EMER_DATA",z."ZAT_WYR_RENT_DATA",z."ZAT_WYR_CHOR_DATA",z."ZAT_WYR_WYP_DATA",z."ZAT_WYR_ZDROW_DATA",z."ZAT_WYR_EMER_KOD",z."ZAT_WYR_RENT_KOD",z."ZAT_WYR_CHOR_KOD",z."ZAT_WYR_WYP_KOD",z."ZAT_WYR_ZDROW_KOD",z."ZAT_EKD_NUMER",z."ZAT_PRC_ID_ZAST",z."ZAT_WSKAZNIK",z."ZAT_WARTOSC_BAZOWA",z."ZAT_ZAKRES_OBOWIAZKOW",z."ZAT_PRC_ID_SZEF",z."ZAT_WAR_SZCZ",z."ZAT_ZWD_ID",z."ZAT_ZAT_ID",z."ZAT_PODATEK",z."ZAT_WYPOWIEDZENIE",z."ZAT_INNE_WARUNKI",z."ZAT_GODZINY_PONAD",z."ZAT_WAP_ID",z."ZAT_AUDYT_KT",z."ZAT_AUDYT_KM",z."ZAT_AUDYT_LM",z."ZAT_KOMENTARZ",z."ZAT_DATA_PODPISANIA",z."ZAT_ANEKS",z."ZAT_RODZAJ_AKORDU",z."ZAT_MIEJSCE_PRACY",z."ZAT_OPODATKOWANIE",z."ZAT_F_RODZAJ",z."ZAT_F_AKT_MIANOWANIA",z."ZAT_PRZYCHOD_AUT",z."ZAT_STN_ID",z."ZAT_UM_ID",z."ZAT_F_ZMIANA_PO_OBLICZENIU",z."ZAT_PRZYG_ZAW",z."ZAT_POZA_KRAJEM",z."ZAT_PRACE_INTERWENCYJNE",z."ZAT_LIMIT_NAD_ROK",z."ZAT_ODZ_ID",z."ZAT_POWOD_ZWOLNIENIA",z."ZAT_FORMA",z."ZAT_RODZAJ_PLACOWKI",z."ZAT_POZIOM_WYKSZT",z."ZAT_STOPIEN_AWANSU",z."ZAT_WYMIAR_LICZNIK",z."ZAT_WYMIAR_MIANOWNIK",z."ZAT_STAWKA_PE",z."ZAT_PENSUM",z."ZAT_DEF_0",z."ZAT_DEF_1",z."ZAT_DEF_2",z."ZAT_DEF_3",z."ZAT_DEF_4",z."ZAT_DEF_5",z."ZAT_DEF_6",z."ZAT_DEF_7",z."ZAT_DEF_8",z."ZAT_DEF_9",z."ZAT_WYDZIAL",z."ZAT_F_PORA_NOCNA",z."ZAT_ZAREJESTROWANIE",z."ZAT_WYREJESTROWANIE",z."ZAT_DATA_STANOWISKA",z."ZAT_SZCZ_WAR_CHAR",z."ZAT_FRM_ID",z."ZAT_DEF_50",z."ZAT_DEF_51",z."ZAT_DEF_52",z."ZAT_DEF_53",z."ZAT_DEF_54",z."ZAT_DEF_55",z."ZAT_DEF_56",z."ZAT_DEF_57",z."ZAT_DEF_58",z."ZAT_DEF_59",z."ZAT_DEF_80",z."ZAT_DEF_81",z."ZAT_DEF_82",z."ZAT_DEF_83",z."ZAT_DEF_84",z."ZAT_DEF_85",z."ZAT_DEF_86",z."ZAT_DEF_87",z."ZAT_DEF_88",z."ZAT_DEF_89",z."ZAT_STAWKA_INFO",z."ZAT_DEF_10",z."ZAT_DEF_11",z."ZAT_DEF_12",z."ZAT_DEF_13",z."ZAT_DEF_14",z."ZAT_DEF_15",z."ZAT_DEF_16",z."ZAT_DEF_17",z."ZAT_DEF_18",z."ZAT_DEF_19",z."ZAT_DEF_20",z."ZAT_DEF_21",z."ZAT_DEF_22",z."ZAT_DEF_23",z."ZAT_DEF_24",z."ZAT_DEF_25",z."ZAT_DEF_26",z."ZAT_DEF_27",z."ZAT_DEF_28",z."ZAT_DEF_29",z."ZAT_DEF_30",z."ZAT_DEF_31",z."ZAT_DEF_32",z."ZAT_DEF_33",z."ZAT_DEF_34",z."ZAT_DEF_35",z."ZAT_DEF_36",z."ZAT_DEF_37",z."ZAT_DEF_38",z."ZAT_DEF_39",z."ZAT_DEF_40",z."ZAT_DEF_41",z."ZAT_DEF_42",z."ZAT_DEF_43",z."ZAT_DEF_44",z."ZAT_DEF_45",z."ZAT_DEF_46",z."ZAT_DEF_47",z."ZAT_DEF_48",z."ZAT_DEF_49",z."ZAT_PROFIL",z."ZAT_GP_ID",z."ZAT_F_CZY_STN_DOSTOSOWANE",z."ZAT_OB_ID_WYDZIAL"
        //	--, z.zat_ob_id as ZAT_OB_ID_WYDZIAL
        //FROM EGADM1.EK_ZATRUDNIENIE z"

        //UWAGA - czasem nie da się z z bazy odzyskac identycznego skryptu jakim tworzono widok:
        //np. jeśli w oryginalnym zapytaniu użyto '*' ('select * from ... ') to w all_views jest jednak 'select <lista kolumn> from'

        if(asCreateOrReplace){
            ret = "create or replace " + ret;
        }

		ret = ret.replace("\r","").replace("\n","\r\n");//jesli czasem jest crlf (\r\n) a czasem lf (\n) to robimy wszedzie crlf


        if (appendSlash) {
            ret = nz(ret).trim() + "\r\n/\r\n";
        }

        return ret;
    }




	public static String getAllSource(Connection conn, String owner, String objectName, String objectType, boolean asCreateOrReplace, boolean addOnwer, boolean appendSlash, boolean addRevisionHeader) throws IOException, SQLException {
		String ret = nz(sqlFnCLOB(conn, "PPADM.DMP_ALL_SOURCE", owner, objectName, objectType));
		if (ret.isEmpty())
			return ret; // brak obiektu w bazie

		if (addOnwer) {
			int hdrlen = ret.length();
			if (hdrlen>500) hdrlen=500;

			String uc = ret.substring(1, hdrlen).toUpperCase();
			int afterObjName = uc.indexOf(objectName.toUpperCase())+objectName.length();
			if ("\"".equals(uc.substring(afterObjName,afterObjName+1) ) ) afterObjName++;
			ret = objectType.toLowerCase() + " " + owner + "." + objectName.toUpperCase() + ret.substring(afterObjName+1); //bez opakowania w cudzysłów
			// ret = objectType.toLowerCase() + " \"" + owner + "\".\"" + objectName.toUpperCase() + "\"" + ret.substring(afterObjName+1); //opakowanie w cudzysłów
		}

		if(asCreateOrReplace){
			ret = "create or replace " + ret;
		}

		if (addRevisionHeader && !ret.contains("$Revision::")) {
			String hdr =
"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\r\n" +
"-- $Header::                                                                                                                                                                                          $\r\n" +
"-- $Revision::                                                                                                                                                                                        $\r\n" +
"-- $Workfile::                                                                                                                                                                                        $\r\n" +
"-- $Modtime::                                                                                                                                                                                         $\r\n" +
"-- $Author::                                                                                                                                                                                          $\r\n" +
"-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ascii/cp1250:ąęćłńóśźż--\r\n";
			int i = ret.indexOf("\n") + 1;//tuz za pierszym \n
			String sql0 = ret.substring(0, i);
			String sql1 = ret.substring(i);
			ret = sql0 + hdr + sql1;
		}

		if (appendSlash) {
			ret = nz(ret).trim() + "\r\n/\r\n";
		}

		ret = ret.replace("\r","").replace("\n","\r\n");//jesli czasem jest crlf (\r\n) a czasem lf (\n) to robimy wszedzie crlf

		return ret;
	}

	public static String getDbSourceCode(Connection conn, String owner, String objectName, String objectType, boolean asCreateOrReplace, boolean addOnwer, boolean appendSlash, boolean addRevisionHeader) throws IOException, SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
		if ("VIEW".equals(objectType.toUpperCase())){
			return getViewSource(conn, owner, objectName, asCreateOrReplace, addOnwer, appendSlash, addRevisionHeader);
		} else {
			return getAllSource(conn, owner, objectName, objectType, asCreateOrReplace, addOnwer, appendSlash, addRevisionHeader);
		}
	}

	public static String getDbSourceCode(String owner, String objectName, String objectType, boolean asCreateOrReplace, boolean addOnwer, boolean appendSlash, boolean addRevisionHeader) throws IOException, SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
		try (Connection conn = getConnection()) {
			return getDbSourceCode(conn, owner, objectName, objectType, asCreateOrReplace, addOnwer, appendSlash, addRevisionHeader);
		}
	}



	public static String getDmpSL(String name) {
		try (Connection conn = getConnection()){
			return getDmpSL(conn, name);
		} catch (Exception e) {
			User.alert(e);
		}
		return null;
	}

	public static String getDmpSL(Connection conn, String name) throws SQLException, IOException {
		String dml = sqlFnCLOB(conn, "ppadm.dmp_sl", name);
		if (!dml.contains("ąęćłńóśźż")) {  dml += "\r\n--ascii/cp1250:ąęćłńóśźż\r\n"; }
		return dml;
	}
}
