package com.comarch.egeria.pp.data;

import com.comarch.egeria.pp.data.msg.Push;
import com.comarch.egeria.web.User;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@ManagedBean
@Named
@Scope("view")
public class V extends SqlBean {
	private static final long serialVersionUID = 1L;
	
	@Inject
	User user;

	private TestBean testBean;


	String windowNameGUID = null;

	public String getWindowNameGUID() {
		if (windowNameGUID==null) {
			windowNameGUID = UUID.randomUUID().toString();
		}
		return windowNameGUID;
	}

	String windowUserActivityTS = null;

	public void testWindowActivityTS(String windowUserActivityTS){

		try {

//			System.out.println("testWindowActivityTS / window: " + windowNameGUID);
			final boolean eq = eq(this.windowUserActivityTS, windowUserActivityTS);
			if (!eq) {
				user.activityTS = System.currentTimeMillis();//ts ozn. ostatnia aktyanowsc usera
				//System.out.println("window: " + windowNameGUID + "; user activity TS " + windowUserActivityTS);
				this.windowUserActivityTS = windowUserActivityTS;
				user.kontynuujEatSesjaZewn();
			}

			user.testActivityTS();
		} catch (Exception ex){
			System.out.println();
		}

	}



	/**
	 * Kopia parametrów przekazanych przez url na początku ładowania widoku
	 * (uwaga: w trakcie przetwarzania full request'a te parametry jeszcze są
	 * dostepne; ale potem każdy partial request modyfikuje
	 * ectx.getRequestParameterMap(), wic potrzebna moe być kopia (+scope=view)
	 * zawartości org. mapy
	 */
	private Map<String, String> urlParams;
	
	
	@PostConstruct
	public void init(){
		if (urlParams==null && FacesContext.getCurrentInstance()!=null){
			ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
			if (ectx!=null) {
				//urlParams = ectx.getRequestParameterMap();
				urlParams = new HashMap<String, String>();
				urlParams.putAll(ectx.getRequestParameterMap());
			}
		}
	}
	
	
	
	public void update(String id){
		com.comarch.egeria.Utils.update(id);
	}
	
	public void testPushWsGuest(){
		Push.push("WEBSOCKET_GUEST",""+LocalDateTime.now(),"OK","TEST/WEBSOCKET_GUEST");
	}
	
	
	public TestBean getTestBean() {
		if (testBean == null) {
			testBean = (TestBean) user.tmpUserObjectsCHM.get("TestBean");
			user.tmpUserObjectsCHM.remove("TestBean");
		}
		return testBean;
	}
	



	public Map getUrlParams() {
		return urlParams;
	}




	public void setUrlParams(Map urlParams) {
		this.urlParams = urlParams;
	}


	public void clearHeaderSatnObieguInfo(){
		com.comarch.egeria.Utils.executeScript("if (parent!=undefined && parent.setStanObiegu!=undefined) parent.setStanObiegu('');");
	}
	
	
	Object dataRowId = null;
	public Object getDataRowId(){
		if (dataRowId==null)
			dataRowId = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		return dataRowId;
	} 
	
	
//	public void setTestBean(TestBean testBean) {
//		this.testBean = testBean;
//	}


}



