package com.comarch.egeria.pp.data;

import com.comarch.egeria.pp.Administrator.model.PptAdmListyWartosci;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.*;
import com.lowagie.text.pdf.BaseFont;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.export.PDFOptions;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.ToggleSelectEvent;
import org.primefaces.event.UnselectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import java.awt.*;
import java.io.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.eq;
import static com.comarch.egeria.Utils.nz;

//https://www.primefaces.org/docs/api/5.1/org/primefaces/model/SelectableDataModel.html
@ManagedBean
public class SqlDataSelectionsHandler extends SqlDataHandler implements Serializable{
	private static final long serialVersionUID = 1L;
	public static final Logger log = LogManager.getLogger(SqlDataSelectionsHandler.class);
	
	private HashSet<Object> hiddenRowsPK = new HashSet<>();//identyfikatory wierszy do ukrycia,(wskazują wiersze które nie są pomijane przez getData())




	public HashSet<String> getRowsCssColorBgStyles(){
		HashSet<String> ret = new HashSet<>();
		for (DataRow r : this.getAllData()) {
			String color = r.getCssColorBg();
			if (color!=null){
				ret.add("."+r.getCssColorBgClass()+"{ background: " + color + "!important; } ");
			}
		}
		return ret;
	}



	public String getRowsCssBgStyle(){
		String ret ="<style type='text/css'>";
		for (String sCss: getRowsCssColorBgStyles()) {
			ret += "\r\n"+sCss;
		}
		ret +="\r\n</style>";
		return ret;
	}


	public void invalidateClientComponents(){
		com.comarch.egeria.Utils.executeScript("invalidateClassComponents('"+this.getShortName().replaceAll(" ","_")+"');");
	}


	public SqlDataSelectionsHandler hideRowsByColumnValue(String column_name, Object hideValue) {
		this.getData().stream().filter(r -> eq(hideValue, r.get(column_name))).forEach(r1 -> hide(r1.getIdAsString()));
		return this;
	}

	public SqlDataSelectionsHandler hideRowsByColumnNotValue(String column_name, Object hideValue) {
		this.getData().stream().filter(r -> !eq(hideValue, r.get(column_name))).forEach(r1 -> hide(r1.getIdAsString()));
		return this;
	}

	public SqlDataSelectionsHandler hideRowsPK(Collection pklist) {
		this.getHiddenRowsPK().clear();
		this.hideAll(pklist);
		return this;
	}
	
	
	public void clearHiddenRowsPK() {
		getHiddenRowsPK().clear();
	}
	
	public void hide(Object pk){
		if (pk==null) return;
		this.getHiddenRowsPK().add(pk);
	}
	
	public void hideAll(Collection pklist){
		if (pklist==null || pklist.isEmpty()) return;
		this.getHiddenRowsPK().addAll(pklist);
	}
	

	public Date getTsAsDate(){
		return new Date(this.getTs());
	}

//	implements org.primefaces.model.SelectableDataModel<Object>

//	@Override
//	public Object getRowData(String pkIndex) {
//		return this.pkIndexMap.get(pkIndex);
//	}
//
//	@Override
//	public String getRowKey(Object r) {
//		return this.pkRows2IndexMap.get(r);
//	}

	public SqlDataSelectionsHandler() {
		// this.setContextDependent(true);
	}
	
	
	
	public int size(){
		return this.data.size();
	}
	
	
	public SqlDataSelectionsHandler setPreQuery(String preQueryBlock){
		if (this.getPreQueryBlock()!=preQueryBlock) {
			this.setPreQueryBlock(preQueryBlock);
			//this.getPreQueryBlockParams().clear();
			this.setPreQueryBlockParams(new ArrayList<>());
			this.resetTs();
		}
		return this;
	}
	
	public SqlDataSelectionsHandler setPreQuery(String preQueryBlock, Object...  args){
		return setPreQuery(preQueryBlock, Arrays.asList(args));
	}
	
	public SqlDataSelectionsHandler setPreQuery(String preQueryBlock, List<Object> params){
		if (!eq(this.getPreQueryBlock(),preQueryBlock)) {
			this.setPreQueryBlock(preQueryBlock);
			this.resetTs();
		}
		
		if (!(""+this.getPreQueryBlockParams()).equals(""+params) ){
			this.setPreQueryBlockParams(params);
			this.resetTs();
		}
		
		return this;
	}
	
	
	
	
	
	
	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	private LazySqlDataModel lazySqlDataModel;
	
	public LazySqlDataModel getLazySqlDataModel() {
		if (this.lazySqlDataModel==null)
			this.setLazySqlDataModel(new LazySqlDataModel(this));
		return lazySqlDataModel;
	}

	public void setLazySqlDataModel(LazySqlDataModel lazySqlDataModel) {
		this.lazySqlDataModel = lazySqlDataModel;
	}	
	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	
	
	
	//agregacja/suma po wartosciach widocznych w getData:
	public BigDecimal sumAllData(String columnName){
		return suma(columnName, this.getAllData()); 
	}
	
	public BigDecimal sum(String columnName){
		return suma(columnName, this.getData()); 
	}
	
	public BigDecimal sumaAllData(String columnName){
		return suma(columnName, this.getAllData()); 
	}
	
	public BigDecimal suma(String columnName){
		return suma(columnName, this.getData()); 
	}
	
	public BigDecimal suma(String columnName, ArrayList<DataRow> data2){
		if (columnName==null || data2 == null)
			return null;
		
		BigDecimal ret = null;

		for (DataRow r : data2) {
			if (r.get(columnName) instanceof BigDecimal){
				BigDecimal x = nz((BigDecimal) r.get(columnName));
				ret = nz(ret).add(x);
			}
		}
		
		return ret;
	}
	
	
	
	
	
	public Object count(String columnName){
		return count(columnName, this.getData());
	}
	
	public Object countAllData(String columnName){
		return count(columnName, this.getAllData());
	}
	
	public int count(String columnName, ArrayList<DataRow> data){
		if (columnName==null)
			return 0;
		
		int ret = 0;
		for (DataRow r : data) {
			if (r.get(columnName) != null)
				ret++;
		}
		return ret;
	}	

	
	public Object countT(String columnName){
		return countValue(columnName, "T", this.getData());
	}
	
	public Object countTAllData(String columnName){
		return countValue(columnName, "T", this.getAllData());
	}
	
	
	public Object countN(String columnName){
		return countValue(columnName, "N", this.getData());
	}
	
	public Object countNAllData(String columnName){
		return countValue(columnName, "N", this.getAllData());
	}	
	
	public int countValue(String columnName, Object value, ArrayList<DataRow> data){
		if (columnName==null)
			return 0;
		
		int ret = 0;
		for (DataRow r : data) {
			if (r.get(columnName) != null && value.equals(r.get(columnName)))
				ret++;
		}
		return ret;
	}	
	
	
	
	
	
	public Object avg(String columnName){
		return avg(columnName, this.getData());
	}

	public Object avgAllData(String columnName){
		return avg(columnName, this.getAllData());
	}
	
	public Object avg(String columnName, ArrayList<DataRow> data){

		String columnType = this.getColumnType(columnName);
		if (columnType==null) return null;
		
		if (columnType.contains("NUMBER")){//BigDecimal
			List<BigDecimal> l1 = data.stream()
					.filter(r->r.get(columnName)!=null)
					.map(r-> ((BigDecimal) r.get(columnName)))
					.collect(Collectors.toList());
			
			if (l1.isEmpty()) return null;
			
			BigDecimal sum = BigDecimal.ZERO;
			for (BigDecimal bd : l1) {
				sum = sum.add(bd);
			}
			
			BigDecimal bigDecimal = new BigDecimal(l1.size());
			
			return sum.divide( bigDecimal , BigDecimal.ROUND_HALF_UP);
			
		}
		
		if (columnType.contains("DATE")){//Timestamp
			
			List<Long> l1 = data.stream()
					.filter(r->r.get(columnName)!=null)
					.map(r-> (((java.sql.Timestamp) r.get(columnName)).getTime()))
					.collect(Collectors.toList());
			
			if (l1.isEmpty()) return null;
			
			Long sum = 0L;
			for (Long l : l1) {
				sum += l;
			}
		
			return new java.sql.Timestamp(sum / l1.size());
		}
		
		return null;
	}	

	
	
	public Object min(String columnName){
		return min(columnName, this.getData());
	}
	
	public Object minAllData(String columnName){
		return min(columnName, this.getAllData());
	}

	public Object max(String columnName){
		return max(columnName, this.getData());
	}
	
	public Object maxAllData(String columnName){
		return max(columnName, this.getAllData());
	}	
	
	public Object min(String columnName, ArrayList<DataRow> data){
		Object ret = null;
		
		for (DataRow r : data) {
			Object x2 = r.get(columnName);
			
			if (x2 instanceof BigDecimal){
				ret = minBigDecimal(ret, x2);
			} else if (x2 instanceof String){
				ret = minString(ret, x2);				
			} else if (x2 instanceof java.sql.Date){
				ret = minSqlDate(ret, x2);
			} else if (x2 instanceof Timestamp){
				ret = minTimestamp(ret, x2);					
//			}else {
//				return null;
			}
		}
		
		return ret;
	}
	
	public Object max(String columnName, ArrayList<DataRow> data){
		Object ret = null;
		
		for (DataRow r : data) {
			Object x2 = r.get(columnName);
			
			if (x2 instanceof BigDecimal){
				ret = maxBigDecimal(ret, x2);
			} else if (x2 instanceof String){
				ret = maxString(ret, x2);				
			} else if (x2 instanceof java.sql.Date){
				ret = maxSqlDate(ret, x2);
			} else if (x2 instanceof Timestamp){
				ret = maxTimestamp(ret, x2);				
//			}else {
//				return null;
			}
		}
		
		return ret;
	}	
	
	
	public Object minBigDecimal(Object x1, Object x2) {
		if (x1==null) { 
			return x2;
		} else {
			if (x2 == null || ((BigDecimal) x1).compareTo((BigDecimal) x2) < 0 )
				return x1;
			else
				return x2;
		}
	}

	public Object maxBigDecimal(Object x1, Object x2) {
		if (x1==null) { 
			return x2;
		} else {
			if (x2 == null || ((BigDecimal) x1).compareTo((BigDecimal) x2) > 0 )
				return x1;
			else
				return x2;
		}
	}
	
	
	
	public Object minSqlDate(Object x1, Object x2) {
		if (x1==null) { 
			return x2;
		} else {
			if (x2 == null || ((java.sql.Date) x1).before((java.sql.Date) x2))
				return x1;
			else
				return x2;
		}
	}	
	
	public Object maxSqlDate(Object x1, Object x2) {
		if (x1==null) { 
			return x2;
		} else {
			if (x2 == null || ((java.sql.Date) x1).after((java.sql.Date) x2))
				return x1;
			else
				return x2;
		}
	}	
	

	public Object minTimestamp(Object x1, Object x2) {
		if (x1==null) { 
			return x2;
		} else {
			if (x2 == null || ((Timestamp) x1).before((Timestamp) x2))
				return x1;
			else
				return x2;
		}
	}	
	
	public Object maxTimestamp(Object x1, Object x2) {
		if (x1==null) { 
			return x2;
		} else {
			if (x2 == null || ((Timestamp) x1).after((Timestamp) x2))
				return x1;
			else
				return x2;
		}
	}	
	
	
	
	public Object minString(Object x1, Object x2) {
		if (x1==null) { 
			return x2;
		} else {
			if (x2 == null || ((String) x1).compareTo((String) x2) < 0 )
				return x1;
			else
				return x2;
		}
	}	
	
	public Object maxString(Object x1, Object x2) {
		if (x1==null) { 
			return x2;
		} else {
			if (x2 == null || ((String) x1).compareTo((String) x2) > 0 )
				return x1;
			else
				return x2;
		}
	}		
	
	
	
	public String agregationClass(String colName){
		if (colName==null)
			return null;
		
		PptAdmListyWartosci c = this.getColumn(colName.toLowerCase());
		
		if (c==null)
			return null;
		
		String ret = c.getLstEtykieta2();
		if ("countT".equals(ret) || "countN".equals(ret))
			ret = "count";
		
		return ret;
	}
	
	
	public Object agregation(String colName){
		if (colName==null)
			return null;
		
		PptAdmListyWartosci c = this.getColumn(colName.toLowerCase());
		
		if (c==null)
			return null;
		
		if ("sum".equals(c.getLstEtykieta2()) ){
			return sum(colName);
		}
		
		if ("min".equals(c.getLstEtykieta2()) ){
			return min(colName);
		}
		
		if ("max".equals(c.getLstEtykieta2()) ){
			return max(colName);
		}
		
		if ("count".equals(c.getLstEtykieta2()) ){
			return count(colName);
		}

		if ("countT".equals(c.getLstEtykieta2()) ){
			return countT(colName);
		}
		
		if ("countN".equals(c.getLstEtykieta2()) ){
			return countN(colName);
		}
		
		if ("avg".equals(c.getLstEtykieta2()) ){
			return avg(colName);
		}
				
		return null;
	}
	
	public boolean isAgregationNumeric(String colName){
		if (colName==null)
			return false;
		
		PptAdmListyWartosci c = this.getColumn(colName.toLowerCase());

		if (c==null)
			return false;
		
		if (     "sum".equals( c.getLstEtykieta2() ) 
			|| "count".equals(c.getLstEtykieta2())
			|| "countT".equals(c.getLstEtykieta2())
			|| "countN".equals(c.getLstEtykieta2()))
			return true;

        return this.getColumnType(colName).contains("NUMBER")
                && ("avg".equals(c.getLstEtykieta2()) || "min".equals(c.getLstEtykieta2()) || "max".equals(c.getLstEtykieta2()));

    }
	
	
	
	public boolean showAgregation(){
		for (PptAdmListyWartosci col : this.getLst()) {
			if (col.getLstEtykieta2()!=null)
				return true;
		}
		return false;
	}
	
	
	
	

	////////////////////////////////////// SelectableDataModel///////////////////////////////////////////////
	/// public class TableRows extends ArrayList, implements
	////////////////////////////////////// org.primefaces.model.SelectableDataModel<Object>
	// @Override
	// public Object getRowData(String pkIndex) {
	// return this.pkIndexMap.get(pkIndex);
	// }
	// @Override
	// public String getRowKey(Object r) {
	// return this.pkRows2IndexMap.get(r);
	// }
	////////////////////////////////////////////////////////////////////////////////////////////////////////

	private List<String> colNames = new ArrayList<String>(); // lista widocznych kolumn
	private List<String> colHeaders = new ArrayList<String>();
	private List<String> colWidths = new ArrayList<String>();
	private List<String> colWidths800600 = new ArrayList<String>();

	
	private HashMap<String, PptAdmListyWartosci> columnsHM = new HashMap<>();// mapa col_name -> col_header
	private HashMap<String, String> colHeadersHM = new HashMap<>();// mapa col_name -> col_header
	private HashMap<String, String> colWidthsHM = new HashMap<>();// mapa col_name -> col_header

	private HashMap<String, String> colFiltersHM = new HashMap<>();// mapa col_name -> col_header
	private HashMap<String, String> colConvertPatternHM = new HashMap<>();// mapa col_name -> col_header

	
	public String getColFormat(String colName){
		String ret = this.colConvertPatternHM.get(colName);
		if (ret==null) 
			return "";
		else 
			return ret; 
	}
	
	
	private ArrayList<DataRow> filteredData = null;
	private ArrayList<DataRow> selectedRows = new ArrayList<>();
	//private HashSet<DataRow> selectedRowsSet = new HashSet<>();
	private DataRow currentRow;
	private DataRow radioSelectedRow;

	private String textFilter = null;

	@Override
	public String resetTs() {

		return super.resetTs();
	}

	@Override
	public void reLoadData() {
		drhHM.clear();
		
		String currenRowId = null;
		if (this.currentRow!=null)
			currenRowId = this.currentRow.getIdAsString();
		
		Set<String> selectedRowsIdSet = this.selectedRows.stream().map(r->r.getIdAsString()).collect(Collectors.toSet());
		
		this.clearSelection();
		this.toggleSelected = null;

		super.reLoadData();
		
		this.rebuildFilteredData();
		
		for (String id : selectedRowsIdSet) {
			DataRow r = this.find(id);
			if (r!=null)
				this.setRowSelection(true, r);
		}
		
		if (currenRowId!=null)
			this.currentRow = this.find(currenRowId);
	}
	
	
	public void rebuildFilteredData(){
		String textFilter0 = this.getTextFilter();
		this.setTextFilter("");
		this.setTextFilter(textFilter0);
		this.setColFilter();
	}

	public DataRow first() {
		DataRow ret = null;
		if (this.getData() != null && !this.getData().isEmpty())
			ret = this.getData().get(0);
		return ret;
	}

	public Object first(String columnName) {
		Object ret = null;
		if (this.getData() != null && !this.getData().isEmpty())
			ret = this.getData().get(0).get(columnName);
		return ret;
	}

	public Object first(Integer columnIndex) {
		Object ret = null;
		if (this.getData() != null && !this.getData().isEmpty())
			ret = this.getData().get(0).get(columnIndex);
		return ret;
	}

	public String formatRowAsText(String format, DataRow r, boolean allColumns) {
		if (r == null)
			return "";

		String ret = "";
		if (allColumns || this.colNames == null || this.colNames.isEmpty())
			ret = r.format(format); //ret = String.format(format, r.toArray());
		else {
			ArrayList al = new ArrayList();
			for (String cn : this.colNames) {
				al.add(r.get(cn));
			}
			try {
				ret = String.format(format, al.toArray());	
			} catch (Exception e) {
				ret = r.format(format); //jesli ktos zamiast numerów %2$s przekazal formatowanie z nazwami kolumn np. %wsl_opis$s 
			}
		}
		return ret;
	}

	// ---------------------------------------implementacje dla
	// multiselect--------------------------------------->
	// ---------------------------------------implementacje dla
	// multiselect--------------------------------------->
	private DataTable pfDataTable;
	Boolean toggleIsChecked = false;
	ArrayList<DataRow> clntSelectedRows;
	// ArrayList<DataRow> rowSelectCheckboxOrder = new ArrayList<>();
	private int selectedRowsSizeLimit = -1;

	HashMap<DataRow, DataRowHolder> drhHM = new HashMap<>();

	public Boolean toggleSelected = false;

	public boolean isToggleSelected() {

		if (this.toggleSelected == null)
			calcSelectionToggler();

		return toggleSelected;
	}

	
	public void setToggleSelected(boolean toggleSelected) {
		this.toggleSelected = toggleSelected;
		this.setRowsSelection(this.toggleSelected, this.getData());
		
	}

	public void calcSelectionToggler() {
		this.toggleSelected = !this.getData().isEmpty();
		for (DataRow r : this.getData()) {
			if (!this.getRowHolder(r).isSelected()) {
				this.toggleSelected = false;
				break;
			}
		}
	}
	
	
//	public void togglerCheckBoxValueChangeListener(ValueChangeEvent event) {
//		Boolean newValue = (Boolean) event.getNewValue();
//		this.setRowsSelection(newValue, this.getData());
//	}
//
//	public void selectionBooleanCheckBoxValueChangeListener(ValueChangeEvent event) {
//		SelectBooleanCheckbox chk = (SelectBooleanCheckbox) event.getComponent();
//		Boolean newValue = (Boolean) event.getNewValue();
//		
//		if (newValue 
//			  && this.selectedRowsSizeLimit >= 0
//			  && this.selectedRowsSizeLimit <= this.selectedRows.size()) {
//			chk.setSelected(false);
//			User.warn("Liczbę zaznaczeń ograniczono do %s", ("" + this.getSelectedRowsSizeLimit()));
//			FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add(chk.getClientId());
//		} else{
//			DataRow r = null;
//			DataTable tb = (DataTable) chk.getParent().getParent();
//			try {
//				r = (DataRow) tb.getRowData();	
//			} catch (Exception e) {
//				log.debug(e.getMessage());
//			}
//			
//			if (r!=null)
//				setRowSelection(newValue , r);
//			
//			FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add(tb.getClientId()+":chkxtgglr");
//		}
//		
//		
//	}


	public void rowSelectCheckboxListener(org.primefaces.event.SelectEvent event){
		DataRow r = (DataRow) event.getObject();
		setRowSelection(true, r);
//		System.out.println("+"+r);
	}

	public void rowUnselectCheckboxListener(org.primefaces.event.UnselectEvent event){
		DataRow r = (DataRow) event.getObject();
		setRowSelection(false, r);
//		System.out.println("-"+r);
	}


	public void toggleSelectListener(ToggleSelectEvent event){
//		System.out.println(event.isSelected());
		this.setRowsSelection(event.isSelected(), this.getData());
	}

	public void setRowsSelection(Boolean isChecked, Collection<DataRow> rows) {
		for (DataRow r : rows) { 
			this.setRowSelection(isChecked, r);
		}
	}
	
	public void setRowSelection(Boolean isChecked, DataRow r) {
		if (r == null)
			return;

		if (isChecked && this.selectedRowsSizeLimit >= 0 && this.selectedRowsSizeLimit <= this.selectedRows.size()) {
			return;
		}

		getRowHolder(r).setSelected(isChecked);

//przeniesiono do setSelected		
//		if (isChecked) { 
//			this.selectedRows.remove(r);
//			this.selectedRows.add(0, r);
//		} else {
//			this.selectedRows.remove(r);
//		}
//		this.toggleSelected = null;

	}

	public void clearSelection() {
		for (Object or : this.getSelectedRows().toArray()) {
			DataRow r = (DataRow) or;
			this.setRowSelection(false, r);
		}
		this.radioSelectedRow = null;
	}




	public DataRowHolder getRowHolder(DataRow r) {
		DataRowHolder ret = drhHM.get(r);
		if (ret == null) {
			ret = new DataRowHolder();
			ret.setRow(r);
			drhHM.put(r, ret);
		}
		return ret;
	}

	public void onRowSelectCheckbox(SelectEvent e) {
		if (this.getSelectedRowsSizeLimit() > 0
				&& this.getSelectedRows().size() >= this.getSelectedRowsSizeLimit()) {
			User.warn("Liczbę zaznaczeń ograniczono do %s", ("" + this.getSelectedRowsSizeLimit())); 
			
			String clientId = e.getComponent().getClientId();
			com.comarch.egeria.Utils.update(clientId);
			return;
		}

		DataRow r = (DataRow) e.getObject();
		this.selectedRows.remove(r);
		this.selectedRows.add(0, r);
	}

	public void onRowUnselectCheckbox(UnselectEvent e) {
		System.out.println("onRowUnSelectCheckbox...");
		DataRow r = (DataRow) e.getObject();
		this.selectedRows.remove(r);
	}




	public List<DataRow> getSelectedRows() {
		return this.selectedRows;
		// ArrayList<DataRow> ret = new ArrayList<DataRow>();
		// ret.addAll(this.selectedRowsSet);
		// return ret;
	}

	public void setSelectedRows(ArrayList<DataRow> selectedRows) {
//		System.out.println("setSelectedRows...." + selectedRows);
		clntSelectedRows = selectedRows;

		// if (clntSelectedRows==null || clntSelectedRows.isEmpty())
		// this.currentRow=null;
		// else //this.currentRow=selectedRows.get(0);
		// this.currentRow=clntSelectedRows.get(clntSelectedRows.size()-1);
		//
		//
		// if (clntSelectedRows!=null && !clntSelectedRows.isEmpty())
		// this.getSelectedRowsSet().add(clntSelectedRows.get(clntSelectedRows.size()-1));
		// //ostatni w kolekcji, bo s wyniku scrollowania moze byc nie 1 ale 51
		// this.selectedRows.clear();
		// this.selectedRows.addAll(this.getSelectedRowsSet());
		//
		// System.out.println("clntSelectedRows -> " + clntSelectedRows);
		// System.out.println("currentRow -> " + currentRow);
		// System.out.println("selectedRows -> " + currentRow);
	}





	// <---------------------------------------implementacje dla
	// multiselect---------------------------------------
	// <---------------------------------------implementacje dla
	// multiselect---------------------------------------

	public List<String> getColNames() {
		return colNames;
	}

	public String setColNames(Object colNames) {
		// System.out.println("============================ setColNames: " + colNames);
		try {
			if ((colNames == null || "".equals(colNames)) && !this.colNames.isEmpty())
				return "";

			if (colNames == null || "".equals(colNames)) {// wszystkie kolumny
				this.colNames = new ArrayList<String>();
				this.colNames.addAll(this.getColumns());
			} else if (colNames instanceof List) { // przekazana lista kolumn
				this.colNames = (List<String>) colNames;
				return "";
			} else { // lista kolumn jako tekst
				this.colNames = parseAsList(colNames);
			}

			if (this.colNames.isEmpty())
				this.getColNames().addAll(this.getColumns());
			// System.out.println("colNames: " + this.colNames);
			return "";
		} finally {
			for (String cname : this.colNames) {
				cname = cname.toLowerCase();

				if (this.colHeadersHM.get(cname) == null)
					this.colHeadersHM.put(cname, cname);

			}
		}
	}

	
	public PptAdmListyWartosci getColumn(String columnName) {
		if (columnName == null)
			return null;
		else
			return this.columnsHM.get(columnName.toLowerCase());
	}

	
	public String getColHeader(String columnName) {
		if (columnName == null)
			return null;
		else
			return this.colHeadersHM.get(columnName.toLowerCase());
	}
	
	
	public String getColHeaderTooltip(String columnName) {
		if (columnName == null)
			return null;

		PptAdmListyWartosci c = this.getColumn(columnName);
		if (c==null) return null;
		
		return c.getLstOpis2();
	}	
	
	
	public String getColWidth(String columnName) {
		columnName = (""+columnName).toLowerCase();
		String ret = this.colWidthsHM.get(columnName.toLowerCase());
		
		if (ret==null){
			PptAdmListyWartosci c = this.getColumn(columnName);
			if (c!=null && c.getLstDlugosc()!=null)
				ret = ""+ c.getLstDlugosc();
		}
		
		return ret;
	}	

	public String getColWidth(String columnName, String format) {
		columnName = (""+columnName).toLowerCase();
		return this.colWidthsHM.get(columnName.toLowerCase());
	}

	public List<String> getColHeaders() {
		return colHeaders;
	}

	public String setColHeaders(Object colHeaders) {

		if (colHeaders == null || ("" + colHeaders).equals("") || ("" + colHeaders).equals("[]"))
			return "";

		if (!this.colHeaders.isEmpty() && this.colNames.toString().equals("[" + colHeaders + "]"))// potencjalne
																									// nadpisywanie
																									// listy wartosci
			return "";

		// System.out.println("============================ setColHeaders: " +
		// colHeaders);

		// if ((colHeaders==null || "".equals(colHeaders))
		// && !this.colHeaders.isEmpty()
		// && !this.colHeaders.toString().equals(this.colNames.toString()))
		// return "";

		if (colHeaders == null || "".equals(colHeaders)) {
			this.colHeaders = new ArrayList<String>();
			this.colHeaders.addAll(this.colNames);
		} else if (colHeaders instanceof List) {
			this.colHeaders = (List<String>) colHeaders;
		} else {
			this.colHeaders = parseAsList(colHeaders);
		}

		if (!this.colHeaders.isEmpty())
			for (int i = 0; i < this.colHeaders.size() && i < this.colNames.size(); i++) {
				this.colHeadersHM.put(this.colNames.get(i).toLowerCase(), this.colHeaders.get(i));
			}

		// if (this.colHeaders.isEmpty()) this.getColNames().addAll(this.colNames);

		return "";
	}

	public List<String> getColWidths() {
		return colWidths;
	}

	/**
	 * kolekcja ustalajaca css style min-width:???px dla kazdej z kolumn mozna
	 * wpisac liczby jako rozdzielany tekst; jesli wpisana zostanie mniejsza liczba
	 * elementow niz jest generowanych, podanych kolumn to ostatni element zostanie
	 * powielony (doc do 100 razy) w szczególnosci wpisanie np. tylko liczy 100
	 * oznaczac będzie, ze wszyskie kolumny dostana po 100px
	 * 
	 * @param colWidths
	 * @return
	 */
	public String setColWidths(Object colWidths) {


		if (!this.colWidths.isEmpty() && this.colWidths.size()!=100)
			return "";// raz wystarczy

		if (colWidths == null) {
			this.colWidths = new ArrayList<String>();
		} else if (colWidths instanceof List) {
			this.colWidths = new ArrayList<String>();
			for (Object o : (List) colWidths) {
				int w = 0;
				try {
					w = Integer.parseInt("" + o);
				} catch (Exception e) {
				}
				this.colWidths.add("" + w);
			}
		} else { // musi byc jako tekst. - liczby rozdziwlane spacjami, przecinkami lub ; .....
			this.colWidths = parseAsList(colWidths);
		}

		String repeat = this.colWidths.isEmpty() ? "0" : "" + this.colWidths.get(this.colWidths.size() - 1);

		for (int i = 0; i < 100; i++)
			this.colWidths.add(repeat);

		
		for (int i=0; i<this.colNames.size();i++){
			String colName = (""+this.colNames.get(i)).toLowerCase();
			this.colWidthsHM.put(colName, this.colWidths.get(i));
		}
		
		//System.out.println(this.getShortName() + this.colWidthsHM);
		
		return "";
	}

	private List<String> parseAsList(Object colHeaders) {
		ArrayList<String> ret = new ArrayList<>();
		String txt = ("" + colHeaders).trim().replace("'", "").replace("\"", "").replace("\t", " ")
				.replaceAll(" +", " ").replaceAll(",", ";");
		txt = txt.replace("; ", ";").replace(" ;", ";");
		if (!txt.contains(";") && !txt.contains(","))
			txt = txt.replace(" ", ";");

		String[] al = txt.split(";");
		ret.addAll(Arrays.asList(al));
		return ret;
	}

	public void setColFilters(HashMap<String, String> colFilters) {
		synchronized (this) {
			this.colFiltersHM.clear();
			this.colFiltersHM.putAll(colFilters);
			setColFilter();
		}
	}

	public void addColFilter(String colName, String value) {
		synchronized (this) {
			getColFiltersHM().put(colName, value);
			setColFilter();
		}
	}

	public void addColFilter(String colName, String value, boolean clearFilter) {
		if (clearFilter) clearFilter();
		addColFilter(colName, value);
	}

	public void setColFilter() {
		synchronized (this) {
			if (!this.colFiltersHM.keySet().isEmpty())
				this.setFilteredData(null);

			setTextFilter(this.textFilter);

			for (String colName : this.colFiltersHM.keySet()) {
				String columnType = getColumnType(colName);

				ArrayList<DataRow> dt = getFilteredOrAllData();
				ArrayList<DataRow> ret = new ArrayList<>();
				Set<DataRow> set = new HashSet<>();

				String colFilter = this.colFiltersHM.get(colName);
				if (colFilter == null || "".equals(colFilter) || " ".equals(colFilter))
					continue;
				colFilter = colFilter.toLowerCase().replace("&&", "&").replace("&", "");

				if (!colFilter.contains("\""))
					colFilter = colFilter.replace("  ", " ");

				colFilter = colFilter.replace("||", "|").replace("|", ";");
				//colFilter = colFilter.replace("*", ".*");

				String[] splitBySemicolon = colFilter.split(";");
				for (String expOr : splitBySemicolon) {
					ArrayList<DataRow> retOr = new ArrayList<>();

					//String[] splitByQout = expOr.split("\"");//podzial na wyrazenia objete w dzudzyslow
					String[] splitBySpace = expOr.split(" ");

					ArrayList<String> exprAnd = new ArrayList<>();

					String txt = "";
					for (int i = 0; i < splitBySpace.length; i++) {
						txt += splitBySpace[i];
						if (txt.contains("\"")) {// obsługa znaków " - np. ="główny specjalista"
							txt = txt.replace("\"", "");
							while (!txt.contains("\"") && i < (splitBySpace.length - 1)) {
								i++;
								txt += " " + splitBySpace[i];
							}
							if (splitBySpace[i].endsWith("\"") && !txt.contains("\""))
								txt += "\"";


							//txt = txt.replace("\"","");... test git
						}
						exprAnd.add(txt);
						txt = "";
					}


					for (DataRow dr : dt) {

						//Object o = dr.get(colName);
						String columnData = dr.getFormattedValue(colName);// o!=null ? "" + o : null;

						boolean add = true;
						String exp = "";

						for (String expAnd : exprAnd) {
							if ("".equals(expAnd))
								continue;
							exp += expAnd;

							add = add && checkAndFilterCondition(columnData, columnType, exp);

							exp = "";
						}
						if (add)
							retOr.add(dr);
					}
					set.addAll(retOr);
				}
				ret.addAll(set);
				this.setFilteredData(ret);
			}
		}
	}
	
	
	public void setColFilter(String colName, Object expr, boolean clearAllPreviousFilters) {
		synchronized (this) {
			if (colName == null)
				return;

			colName = colName.toLowerCase();

			if (clearAllPreviousFilters)
				this.clearFilter();

			this.colFiltersHM.put(colName, "" + expr);
			this.setColFilter();
		}
	}

	public void setColFilter(String colName, Object expr, boolean clearPreviousFilters, boolean quoteExpr) {
		String qtexpr = expr!=null ?  "\"" + expr + "\"" : "";
		setColFilter(colName, qtexpr, clearPreviousFilters);
	}
	

	private boolean checkAndFilterCondition(String txtToCompare, String columnType, String filterExpr) {
		if (filterExpr==null || filterExpr.isEmpty()) return false;

		if (filterExpr.substring(0, 1).matches("[<>=]")) {
			return checkFilterCondition(columnType, filterExpr, txtToCompare);
		} else {
		    return checkStartsWithRegex(filterExpr, txtToCompare);
			//return checkContainsWithRegex(filterExpr, txtToCompare);
		}
	}

	public static boolean checkStartsWithRegex(String filter, String txtToCompare) {
		if (txtToCompare==null)
			return false;

		filter = filter.replace("%", "*");//przyzwyczjenia z orcale'a górą
		filter = filter.replace("_", "?");//przyzwyczjenia z orcale'a górą

		filter = filter.replace("*", ".*");
		filter = filter.replace("?", ".");
		filter = filter.replace("{", "\\{");
		filter = filter.replace("}", "\\}");
		filter = filter.replace("[", "\\[");
		filter = filter.replace("]", "\\]");
		filter = filter.replace("(", "\\(");
		filter = filter.replace(")", "\\)");


		if (!filter.endsWith("\""))//jesli na koncu wyrazenia nie ma cudzyslowia
			filter = filter + ".*";
		else
			filter = filter.replace("\"","");

		Pattern pattern = Pattern.compile(filter);
		boolean ret = pattern.matcher(txtToCompare.toLowerCase()).matches();
//		if (ret)
//			System.out.println();

		return ret;
	}


	public static boolean checkContainsWithRegex(String filter, String txtToCompare) {
		if (txtToCompare==null)
			return false;

		filter = filter.replace("%", "*");//przyzwyczjenia z orcale'a górą
		filter = filter.replace("_", "?");//przyzwyczjenia z orcale'a górą

		filter = filter.replace("*", ".*");
		filter = filter.replace("?", ".");
		filter = filter.replace("{", "\\{");
		filter = filter.replace("}", "\\}");
		filter = filter.replace("[", "\\[");
		filter = filter.replace("]", "\\]");
		filter = filter.replace("(", "\\(");
		filter = filter.replace(")", "\\)");


		filter = filter.replace("\"", ""); //stosowano cudzysłów - pasowanie do całego pola
		filter = ".*" + filter + ".*";

		Pattern pattern = Pattern.compile(filter);

		boolean ret = pattern.matcher(txtToCompare.toLowerCase()).matches();
//		if (ret)
//			System.out.println();

		return ret;
	}

	private ArrayList<DataRow> getFilteredOrAllData() {
		ArrayList<DataRow> data0 = this.getFilteredData();
		if (data0 == null)
			data0 = this.getAllData();
		return data0;
	}

	private boolean checkFilterCondition(String columnType, String filterExpr, String columnData) {
		if (columnData == null || "null".equals(columnData) ){
            return "=null".equals(filterExpr);
		}

		if (columnData.isEmpty()){//null w danych kolumny lw
			return checkStringCondition(filterExpr, columnData);//'=' -> szukamy nulli
		}
		
		
		if (filterExpr.length() >= 1) {
			switch (columnType) {
			case "NUMBER":
				double number;
				try {
					number = Double.parseDouble(columnData.replace(",",".") );
				} catch (NumberFormatException exception) {
					return false;
				}
				return checkDoubleCondition(filterExpr, number);
			case "DATE":
				return checkStringCondition(filterExpr, columnData);
			case "VARCHAR2":
				return checkStringCondition(filterExpr, columnData);
			}
		}
		return false;
	}

	private boolean checkDoubleCondition(String expression, double number) {
		if (expression==null || expression.isEmpty()) return false;

		expression = expression.replace(",",".");

		double numberToCompare;
		try {

			if (expression.length()>=2 && expression.substring(1, 2).matches("[>=]")) {
				numberToCompare = Double.parseDouble(expression.substring(2));
				switch (expression.substring(0, 2)) {
				case ">=":
					return number >= numberToCompare;
				case "<=":
					return number <= numberToCompare;
				case "<>":
					return number != numberToCompare;
				}
			} else if (expression.length()>1) {
				numberToCompare = Double.parseDouble(expression.substring(1));

				switch (expression.substring(0, 1)) {
				case "=":
					return number == numberToCompare;
				case ">":
					return number > numberToCompare;
				case "<":
					return number < numberToCompare;
				}
//                numberToCompare = Double.parseDouble(expression);
//                return number == numberToCompare;
//			} else if (expression.length()==1 && "=".equals(expression)) {
//				return
			}

		} catch (Exception exception) {
			return false;
		}
		return false;
	}

	private boolean checkStringCondition(String expression, String dataToCompare) {
		if (expression==null || expression.isEmpty() || dataToCompare == null)
			return false;

		if (expression.length()>=2 && expression.substring(1, 2).matches("[>=]")) {
			switch (expression.substring(0, 2)) {
			case ">=":
				return expression.substring(2).compareTo(dataToCompare.toLowerCase()) <= 0;
			case "<=":
				return expression.substring(2).compareTo(dataToCompare.toLowerCase()) >= 0;
			case "<>":
				return expression.substring(2).compareTo(dataToCompare.toLowerCase()) != 0;
			}
		} else {
			switch (expression.substring(0, 1)) {
			case "=":
				return expression.substring(1).compareTo(dataToCompare.toLowerCase()) == 0;
			case ">":
				return expression.substring(1).compareTo(dataToCompare.toLowerCase()) < 0;
			case "<":
				return expression.substring(1).compareTo(dataToCompare.toLowerCase()) > 0;
			}
		}

		return false;
	}

	public void clearFilter() {
		synchronized (this) {
			this.setFilteredData(null);
			this.colFiltersHM.clear();
			this.setTextFilter("");
		}
	}

	
	
	
	public String getTextFilter() {
		return textFilter;
	}	
	
	public void setTextFilter(String filter) {
		setTextFilter(filter, this.getColNames());
	}

	public void setTextFilter(String filter, List<String> filterColNames2) { // ogolne filtrowanie slownikow za pomoca
																				// tekstu... mozna zmodyfikowac tak aby
																				// filtrowac po slowach albo wg regular
																				// expression...
		if (filterColNames2 == null || filterColNames2.isEmpty())
			return;

		if (filter != null && filter.equals(this.textFilter))
			return;

//		System.out.println("... ... ... SqlDataSelectionsHandler(shortname=='" + this.getShortName()
//				+ "').setTextFilter->" + filter + " /arr: " + filterColNames2);

		boolean appendingFilter = false;
		if (this.textFilter != null && !this.textFilter.equals("") && filter != null && !filter.equals("")
				&& filter.startsWith(this.textFilter))
			appendingFilter = true;
		// System.out.println("...appendingFilter: " + appendingFilter + " /
		// this.textFilter: " + this.textFilter+ " / filter: " + filter);

		this.textFilter = filter;

		if (filter == null || filter.trim().equals("")) {
			this.filteredData = null;

			if (!this.data.isEmpty())
				this.getData();
			
			this.toggleSelected = null;
			return;
		}

		String fltrL = filter.toLowerCase();
		String[] filterWords = fltrL.split(" ");

		ArrayList<String> words = new ArrayList<>();
		String txt ="";
		for (int i=0; i< filterWords.length; i++) {
			txt += filterWords[i];
			if (txt.contains("\"")){
				txt = txt.replace("\"","");
				while (!txt.contains("\"") && i<(filterWords.length-1)){
					i++;
					txt += " " + filterWords[i];
				}
				//txt = txt.replace("\"","");
			}

			txt = txt.replace("%","*");
			txt = txt.replace("_","?");
			words.add(txt);
			txt="";
		}


		ArrayList<DataRow> newFilteredData = new ArrayList<>();

		ArrayList<DataRow> data2filter = appendingFilter ? this.filteredData : getAllData();
		if (data2filter==null) data2filter = new ArrayList<>();

		if (filterColNames2 != null && !filterColNames2.isEmpty()) {
			for (DataRow r : data2filter) {
				if (r.containsAll(words, filterColNames2))
					newFilteredData.add(r);
			}
		} else {
			for (DataRow r : data2filter) {
				if (r.containsAll(words))
					newFilteredData.add(r);
			}
		}

		this.setFilteredData(newFilteredData);
		this.toggleSelected = null;
	}

	// private String globalSqlId = null;
	

	@Override
	public ArrayList<DataRow> getData() {
		long ts = this.getTs();
		
		
		ArrayList<DataRow> ret = getAllData();
		
		if (this.getTs()!=ts){
			if (this.getTextFilter()!=null && !"".equals(this.getTextFilter()))
				this.setTextFilter(this.getTextFilter());
			
			if (!this.getColFiltersHM().isEmpty())
				this.setColFilter();//po przeładowaniu danych z bazy trzeba na nowy przebudowac filteredData
		}
		
		if (this.filteredData != null) {
			ret = this.filteredData;
		} 
		
		if (getHiddenRowsPK()!=null && !getHiddenRowsPK().isEmpty()){
			
			ArrayList<DataRow> ret4hide = new ArrayList<>();
			ret4hide.addAll(ret);
			
			for (Object pk : getHiddenRowsPK()) {
				DataRow r = this.find(pk, false);
				if (r!=null)
					ret4hide.remove(r);
			}
			
			return ret4hide;
			
		} else {
			return ret;
		}
	}

	public ArrayList<DataRow> getAllData() {
		// if (globalSqlId!=null)
		// return (ArrayList<DataRow>) (AppBean.slowniki.get(globalSqlId)).getData();
		// else
		return super.getData();
	}

	/**
	 * wszystkie dane neizaleznie od filtrowania
	 * 
	 * @return
	 */
	public ArrayList<DataRow> getOriginalData() {
		return super.getData();
	}

	public ArrayList<DataRow> getFilteredData() {
		return filteredData;
	}

	public void setFilteredData(ArrayList<DataRow> filteredData) {
		this.filteredData = filteredData;
	}


	public Object getCurrentRowId(){
		if (this.currentRow==null) return null;
		return this.currentRow.get(0);
	}


	public String getCurrentRowIdAsString(){
		if (this.currentRow==null) return null;
		return this.currentRow.getIdAsString();
	}

	public Long getCurrentRowIdAsLong(){
		if (this.currentRow==null) return null;
		return this.currentRow.getIdAsLong();
	}


	public DataRow getCurrentRow() {
		// System.out.println("getCurrentRow ............. " + currentRow);
		return currentRow;
	}

	public void setCurrentRow(DataRow currentRow) {
		// System.out.println("setCurrentRow ............. " + currentRow);
		this.currentRow = currentRow;
	}

	public DataTable getPfDataTable() {
		return pfDataTable;
	}

	public void setPfDataTable(DataTable pfDataTable) {
		this.pfDataTable = pfDataTable;
	}

	public DataRow getRadioSelectedRow() {
		// System.out.println("getRadioSelectedRow...."+radioSelectedRow);
		return radioSelectedRow;
	}

	public void setRadioSelectedRow(DataRow radioSelectedRow) {
		// System.out.println("setRadioSelectedRow....");
		if (radioSelectedRow != null) {
			this.radioSelectedRow = radioSelectedRow;
			// System.out.println("radioSelectedRow = " + radioSelectedRow);
		}
	}

	public DataRow newEmptyRow() {
		int size = this.getColumns().size();
		DataRow r = new DataRow(this, new ArrayList<>(size));
		this.getColumns().forEach(c -> {
			r.add(null);// pusty wiersz / n-kolumn
		});
		// this.getData().add(r);//napewno - lepiej po skutecznym zapisaniu do bazy...
		return r;
	}

	public int getSelectedRowsSizeLimit() {
		return selectedRowsSizeLimit;
	}

	public String setSelectedRowsSizeLimit(int selectedRowsSizeLimit) {
		this.selectedRowsSizeLimit = selectedRowsSizeLimit;
		return "";
	}

	public HashMap<String, String> getColHeadersHM() {
		return colHeadersHM;
	}

	public void setColHeadersHM(HashMap<String, String> colHeadersHM) {
		this.colHeadersHM = colHeadersHM;
	}

	public HashMap<String, String> getColFiltersHM() {
		return colFiltersHM;
	}

	public void setColFiltersHM(HashMap<String, String> colFiltersHM) {
		this.colFiltersHM = colFiltersHM;
	}

	
	
	String filterInfo(){
		synchronized (this) {
			String ret = "";

			ArrayList<String> al = new ArrayList<>();

			if (this.filteredData != null && this.getAllData() != null && this.filteredData.size() < this.getAllData().size()) {

//			al.add (String.format("l. wierszy %1$s z %2$s", this.filteredData.size(), this.getAllData().size()));
//			if (this.textFilter!=null && !this.textFilter.isEmpty())
				al.add(String.format("filtr: '%1$s'", this.textFilter));

				for (String colName : this.colFiltersHM.keySet()) {
					if (this.colFiltersHM.get(colName) != null && !this.colFiltersHM.get(colName).isEmpty())
						al.add(String.format("[%1$s] %2$s", this.getColHeader(colName), this.colFiltersHM.get(colName)));
				}


			}

			if (!al.isEmpty())
				ret += String.join("; ", al);

			return ret;
		}
	}
	
	
	public String lwPageInfo() {
		String lwPageInfo = "";
		
		if (this.filteredData != null && this.getAllData() != null && this.filteredData.size() < this.getAllData().size()) 
			lwPageInfo = String.format("lw: %1$s (l. wierszy %2$s z %3$s)", this.getShortName(), this.filteredData.size(), this.getAllData().size());
		else
			lwPageInfo = String.format("lw: %1$s (l. wierszy: %2$s)", this.getShortName(), this.getAllData().size());
		return lwPageInfo;
	}





	public PDFOptions getPdfOptions() {
		PDFOptions pdfOpt = new PDFOptions();
		pdfOpt.setFacetFontSize("10");//rozmiar czc. w nag. kolumn
		pdfOpt.setCellFontSize("10"); //rozmiar czcionki w komórkach tabeli
		//pdfOpt.setCellFontStyle("BOLD");
		//pdfOpt.setCellFontColor("#000044");
		return pdfOpt;
	}


	public void preProcessPDF(Object document) throws IOException, DocumentException {
		Document pdf = (Document) document;
		pdf.setPageSize(PageSize.A4);//A4 PORTRAIT
		//pdf.setPageSize(PageSize.A4.rotate());//A4 LANDSCAPE

		// header/footer musi być przed pdf.open() żeby wyświetlił się też na pierwszej stronie
		
		Font font6 = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
		font6.setSize(6);
		
		Font font8 = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
		font8.setSize(8);

		Font font8gray = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
		font8gray.setSize(8);
		font8gray.setColor(Color.GRAY);

		Font font14 = FontFactory.getFont(BaseFont.HELVETICA, BaseFont.CP1250, BaseFont.EMBEDDED);
		font14.setSize(14);


		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		String logo = externalContext.getRealPath("") + File.separator + "resources" + File.separator + "images"
				+ File.separator + "MF_logo3.png";
		Image img = Image.getInstance(logo);
		img.scaleToFit(100, 60);




		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		Phrase phraseHeader = new Phrase("Wykonano: " + sdf.format(new Date()), font8);

		Paragraph paragraphHeader = new Paragraph();




		float[] widths = {70f, 30f};
		Table tb = new Table(2, 1);//l kolumn, l wierszy
		tb.setWidths(widths);

		Cell cell1 = new Cell();
		cell1.add(img);
		cell1.setBorder(Rectangle.NO_BORDER);

		User user = User.getCurrentUser();
		Paragraph paragraphHeaderRight1 = new Paragraph("Wykonano: " + sdf.format(new Date()), font8);
		Paragraph paragraphHeaderRight2 = new Paragraph("Przez: " + user.getEkPracownik().format("%prc_imie$s %prc_nazwisko$s"), font8);
		Paragraph paragraphHeaderRight3 = new Paragraph("Użytkownik: " + user.getDbLogin(), font8gray);

		Cell cell2 = new Cell();
		cell2.setBorder(Rectangle.NO_BORDER);
		cell2.add(paragraphHeaderRight1);
		cell2.add(paragraphHeaderRight2);
		cell2.add(paragraphHeaderRight3);
		//cell1.setBackgroundColor(Color.GREEN);
		tb.addCell(cell1);
		tb.addCell(cell2);
		tb.setWidth(100f);//100%
		tb.setBorder(Rectangle.NO_BORDER);

		paragraphHeader.add(tb);

		//HeaderFooter header = new HeaderFooter(phraseHeader, false);
		HeaderFooter header = new HeaderFooter(paragraphHeader, false);

		//header.setAlignment(Element.ALIGN_RIGHT);
		header.setBorder(Rectangle.NO_BORDER);
		//pdf.setHeader(header);krzywo!






		Phrase phraseFooter = new Phrase( String.format("%1$s %2$s / Strona ", lwPageInfo(), filterInfo() ), font8);
		
		HeaderFooter footer = new HeaderFooter(phraseFooter, true);		
		footer.setAlignment(Element.ALIGN_RIGHT);
		footer.setBorder(Rectangle.NO_BORDER);
		pdf.setFooter(footer);
		
		pdf.open();
		pdf.setPageSize(PageSize.A4);//A4 PORTRAIT
		//pdf.setPageSize(PageSize.A4.rotate());//A4 LANDSCAPE

		pdf.add(tb);
		//pdf.add(img);

		pdf.addTitle(this.getShortName());

		String name = this.getName();
		if (name==null ||  "null".equals(name)) 
			name = "";
		Paragraph paragraph = new Paragraph(name, font14);
		paragraph.setAlignment(Element.ALIGN_CENTER);
		pdf.add(paragraph);
		
		pdf.add(new Paragraph(" "));

		
	}



	public StreamedContent exportPDF() throws IOException, DocumentException {
		final PipedInputStream inputStream = ReportGenerator.getLwAsPdfStream(this);
		StreamedContent ret =  new DefaultStreamedContent(inputStream, "application/pdf", this.getShortName()+".pdf");
		return ret;
	}


	public StreamedContent exportXLSX() throws IOException {

		XSSFWorkbook wb = exportDataAsXlsxWorkbook();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		wb.write(outputStream);
		outputStream.flush();
		outputStream.close();

		byte[] bytes = outputStream.toByteArray();
		ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);

		StreamedContent ret =  new DefaultStreamedContent(inputStream, "application/xlsx", this.getShortName()+".xlsx");
		return ret;
	}


	public XSSFWorkbook exportDataAsXlsxWorkbook() {
		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sh = wb.createSheet(this.getShortName());

		HashMap<String, CellStyle> cellStyles = new HashMap<>();
		HashMap<String, String> agregacje = new HashMap<>();



		CellStyle hdrCellStyle = wb.createCellStyle();
		hdrCellStyle.setBorderTop(BorderStyle.THIN); //CellStyle.BORDER_THIN);
		hdrCellStyle.setBorderRight(BorderStyle.THIN);//CellStyle.BORDER_THIN);
		hdrCellStyle.setBorderBottom(BorderStyle.THIN); //CellStyle.BORDER_THIN);
		hdrCellStyle.setBorderLeft(BorderStyle.THIN); //CellStyle.BORDER_THIN);

		int rNr = 0;
		int cNr = 0;

		//Naglówki i buforowanie formatowania danych
		XSSFRow rowHdr = sh.createRow(rNr++);
		for (String c : this.getColNames()) {

			XSSFCell cell = rowHdr.createCell(cNr);
			cell.setCellValue(this.getColHeader(c));
			cell.setCellStyle(hdrCellStyle);

			PptAdmListyWartosci cdef = this.getColumnDef(c);
			sh.setColumnWidth(cNr, (int) (cdef.getLstDlugosc() * 50)); // 1 jednostka szerokosci to 1/256 statystycznego znaku // ... width (in units of 1/256th of a character width) ...

			String colFormat = this.getColFormat(c);
			String columnType = this.getColumnType(c);

			if ("DATE".equals(columnType)){

				CellStyle cstyle = wb.createCellStyle();
				if (colFormat==null || colFormat.isEmpty())
					colFormat = "yyyy-MM-dd";

				cstyle.setDataFormat(wb.getCreationHelper().createDataFormat().getFormat(colFormat)); //"yyyy-MM-dd HH:mm:ss"
				cellStyles.put(c,cstyle);

			}else if ("NUMBER".equals(columnType)&& !nz(colFormat).isEmpty()){

				CellStyle cstyle = wb.createCellStyle();
				cstyle.setDataFormat(wb.createDataFormat().getFormat(colFormat));//"#0.0000"
				cellStyles.put(c,cstyle);
			}

			String agreg = this.agregationClass(c);
			if (agreg!=null && !agreg.isEmpty())
				agregacje.put(c, agreg);

			cNr++;
		}


		List<DataRow> exportData = this.getData();
		if (this.getLazySqlDataModel()!=null){ 	//sortowanie jak w p:DataTable
			exportData = this.getLazySqlDataModel().load(0, exportData.size(), this.getLazySqlDataModel().sortField, this.getLazySqlDataModel().sortOrder, null);
		}

		//DANE - > Wiersze
		for (DataRow r : exportData) {
			XSSFRow rowObj = sh.createRow(rNr++);
			cNr = 0;
			for (String c : this.getColNames()) {
				XSSFCell cell = rowObj.createCell(cNr);
				Object o = r.get(c);

				if (o==null){
					cell.setCellValue("");
				} else if (o instanceof Number){
					cell.setCellValue(((Number) o).doubleValue());
				}else if (o instanceof Date){
					cell.setCellValue((Date) o);
					//cell.setCellStyle(cellDateTimeStyle);
				} else {
					cell.setCellValue(r.getFormattedValue(c));
				}

				if(cellStyles.get(c)!=null){
					cell.setCellStyle(cellStyles.get(c));
				}

				cNr++;
			}
		}


		//agregacje - wiersz z podsumowaniem:
		if (!agregacje.isEmpty()) {

			XSSFRow rowFtr0 = sh.createRow(rNr++);
			cNr = 0;
			for (String c : this.getColNames()) {
				String agr = agregacje.get(c);
				if (agr!=null) agr+=":";

				XSSFCell cell = rowFtr0.createCell(cNr);
				cell.setCellValue(agr);
				cNr++;
			}

			XSSFRow rowFtr = sh.createRow(rNr++);
			cNr = 0;
			for (String c : this.getColNames()) {
				XSSFCell cell = rowFtr.createCell(cNr);
				Object o = this.agregation(c);

				if (o==null){
					cell.setCellValue("");
				} else if (o instanceof Number){
					cell.setCellValue(((Number) o).doubleValue());
				}else if (o instanceof Date){
					cell.setCellValue((Date) o);
				}

				if(cellStyles.get(c)!=null){
					cell.setCellStyle(cellStyles.get(c));
				}

				cNr++;
			}
		}
		return wb;
	}

	public HashMap<String, String> getColConvertPatternHM() {
		return colConvertPatternHM;
	}

	public void setColConvertPatternHM(HashMap<String, String> colConvertPatternHM) {
		this.colConvertPatternHM = colConvertPatternHM;
	}

	public List<String> getColWidths800600() {
		return colWidths800600;
	}

	public void setColWidths800600(List<String> colWidths800600) {
		this.colWidths800600 = colWidths800600;
	}

	public HashMap<String, PptAdmListyWartosci> getColumnsHM() {
		return columnsHM;
	}

	public void setColumnsHM(HashMap<String, PptAdmListyWartosci> columnsHM) {
		this.columnsHM = columnsHM;
	}


	public HashSet<Object> getHiddenRowsPK() {
		return hiddenRowsPK;
	}


	public void setHiddenRowsPK(HashSet<Object> hiddenRowsPK) {
		this.hiddenRowsPK = hiddenRowsPK;
	}


}
