package com.comarch.egeria.pp.data.dataSources;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BasicDataSourceTomcatAdapter implements IDataSourceInfo {
	public static final Logger log = LogManager.getLogger();
	
	DataSource ds; // <-- org.apache.tomcat.dbcp.dbcp2.BasicDataSource;
	
	private Method mUsername;
	private Method mUrl;
	private Method mNumActive;
	private Method mMaxTotal;
	private Method mNumIdle;
	
	
	public BasicDataSourceTomcatAdapter(DataSource dataSource){
		this.ds = dataSource;
		
		Method[] declaredMethods = ds.getClass().getDeclaredMethods();
		for (Method m : declaredMethods) {

			if ("getUsername".equals(m.getName()))
				mUsername = m;
		
			if ("getUrl".equals(m.getName()))
				mUrl = m;

			if ("getNumActive".equals(m.getName()))
				mNumActive = m;

			if ("getMaxTotal".equals(m.getName()))
				mMaxTotal = m;

			if ("getNumIdle".equals(m.getName()))
				mNumIdle = m;
		}
	}
	
	
	@Override
	public String getUsername() {
		if (mUsername!=null)
			try {
				return ""+mUsername.invoke(ds, null);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			} 
		return null;
	}

	@Override
	public String getUrl() {
		if (mUrl!=null)
			try {
				return ""+mUrl.invoke(ds, null);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			} 
		return null;
	}

	@Override
	public Integer getNumActive() {
		if (mNumActive!=null)
			try {
				return (Integer) mNumActive.invoke(ds, null);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			} 
		return null;
	}

	@Override
	public Integer getNumIdle() {
		if (mNumIdle!=null)
			try {
				return (Integer) mNumIdle.invoke(ds, null);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			} 
		return null;
	}

	@Override
	public Integer getMaxTotal() {
		if (mMaxTotal!=null)
			try {
				return (Integer) mMaxTotal.invoke(ds, null);
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			} 
		return null;
	}

}
