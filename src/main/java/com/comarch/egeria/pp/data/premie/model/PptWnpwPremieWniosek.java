package com.comarch.egeria.pp.data.premie.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_WNPW_PREMIE_WNIOSEK database table.
 * 
 */
@Entity
@Table(name="PPT_WNPW_PREMIE_WNIOSEK")
@NamedQuery(name="PptWnpwPremieWniosek.findAll", query="SELECT p FROM PptWnpwPremieWniosek p")
public class PptWnpwPremieWniosek implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_WNPW_PREMIE_WNIOSEK",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_WNPW_PREMIE_WNIOSEK", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_WNPW_PREMIE_WNIOSEK")
	@Column(name="WNPW_ID")
	private long wnpwId;

	@Temporal(TemporalType.DATE)
	@Column(name="WNPW_DATA_WNIOSKU")
	private Date wnpwDataWniosku;

	@Column(name="WNPW_OPIS")
	private String wnpwOpis;
	
	@Column(name="WNPW_OB_ID")
	private Long wnpwObId;
	
	@Temporal(TemporalType.DATE)
	@Column(name="WNPW_AUDYT_DM")
	private Date wnpwAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="WNPW_AUDYT_DT")
	private Date wnpwAudytDt;

	@Column(name="WNPW_AUDYT_KM")
	private String wnpwAudytKm;

	@Column(name="WNPW_AUDYT_LM")
	private BigDecimal wnpwAudytLm;

	@Column(name="WNPW_AUDYT_UM")
	private String wnpwAudytUm;

	@Column(name="WNPW_AUDYT_UT")
	private String wnpwAudytUt;

	

	
	// TODO: @LazyCollection(LazyCollectionOption.FALSE)  vs @OneToMany( ... fetch=FetchType.EAGER ...) + @Fetch(FetchMode.SELECT)
	
	//bi-directional many-to-one association to PptWnpkPremieKwoty
	@OneToMany(mappedBy="pptWnpwPremieWniosek",  cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<PptWnpkPremieKwoty> pptWnpkPremieKwoties = new ArrayList<>();

	public PptWnpwPremieWniosek() {
	}

	public long getWnpwId() {
		return this.wnpwId;
	}

	public void setWnpwId(long wnpwId) {
		this.wnpwId = wnpwId;
	}

	public Date getWnpwDataWniosku() {
		return this.wnpwDataWniosku;
	}

	public void setWnpwDataWniosku(Date wnpwDataWniosku) {
		this.wnpwDataWniosku = wnpwDataWniosku;
	}

	public String getWnpwOpis() {
		return this.wnpwOpis;
	}

	public void setWnpwOpis(String wnpwOpis) {
		this.wnpwOpis = wnpwOpis;
	}

	public List<PptWnpkPremieKwoty> getPptWnpkPremieKwoties() {
		return this.pptWnpkPremieKwoties;
	}

	public void setPptWnpkPremieKwoties(List<PptWnpkPremieKwoty> pptWnpkPremieKwoties) {
		this.pptWnpkPremieKwoties = pptWnpkPremieKwoties;
	}

	public PptWnpkPremieKwoty addPptWnpkPremieKwoty(PptWnpkPremieKwoty pptWnpkPremieKwoty) {
		getPptWnpkPremieKwoties().add(pptWnpkPremieKwoty);
		pptWnpkPremieKwoty.setPptWnpwPremieWniosek(this);

		return pptWnpkPremieKwoty;
	}

	public PptWnpkPremieKwoty removePptWnpkPremieKwoty(PptWnpkPremieKwoty pptWnpkPremieKwoty) {
		getPptWnpkPremieKwoties().remove(pptWnpkPremieKwoty);
		pptWnpkPremieKwoty.setPptWnpwPremieWniosek(null);

		return pptWnpkPremieKwoty;
	}

	public Long getWnpwObId() {
		return wnpwObId;
	}

	public void setWnpwObId(Long wnpwObId) {
		this.wnpwObId = wnpwObId;
	}

	public Date getWnpwAudytDm() {
		return wnpwAudytDm;
	}

	public void setWnpwAudytDm(Date wnpwAudytDm) {
		this.wnpwAudytDm = wnpwAudytDm;
	}

	public Date getWnpwAudytDt() {
		return wnpwAudytDt;
	}

	public void setWnpwAudytDt(Date wnpwAudytDt) {
		this.wnpwAudytDt = wnpwAudytDt;
	}

	public String getWnpwAudytKm() {
		return wnpwAudytKm;
	}

	public void setWnpwAudytKm(String wnpwAudytKm) {
		this.wnpwAudytKm = wnpwAudytKm;
	}

	public BigDecimal getWnpwAudytLm() {
		return wnpwAudytLm;
	}

	public void setWnpwAudytLm(BigDecimal wnpwAudytLm) {
		this.wnpwAudytLm = wnpwAudytLm;
	}

	public String getWnpwAudytUm() {
		return wnpwAudytUm;
	}

	public void setWnpwAudytUm(String wnpwAudytUm) {
		this.wnpwAudytUm = wnpwAudytUm;
	}

	public String getWnpwAudytUt() {
		return wnpwAudytUt;
	}

	public void setWnpwAudytUt(String wnpwAudytUt) {
		this.wnpwAudytUt = wnpwAudytUt;
	}

}