package com.comarch.egeria.pp.data;

public class Lov {

	private SqlDataSelectionsHandler datasource;
	private String title;
	private String widgetVar;
	private String lovTextFormat;
	private String lovValueFormat;
	private Boolean lockInputText;
	
	private String dialogId;
	
	private long dlgTs = 0;
	
	
	int cntr = 0;
	
	public int getCntr(){
		int ret = cntr++;
		System.out.println("////////////////////////////////////////////////////////////////"+ret);
		return ret;
	}
	
	
	public Lov(SqlDataSelectionsHandler ds, String title, String lovTextFormat, String lovValueFormat, Boolean lockInputText) {
		this.title=title;
		if (title==null || title.isEmpty() && ds!=null)
			this.title = ds.getName();
		this.datasource=ds;
		this.lovTextFormat = lovTextFormat;
		this.lovValueFormat = lovValueFormat;
		this.lockInputText = lockInputText;
		if (ds!=null)
		this.widgetVar = (title+"_"+ds.getShortName())
				.replace(" ", "_")
				.replace("@", "_")
				.replace("(", "_")
				.replace(")", "_")
				.replace("/", "_")
				.replace("\\", "_")
				.replace(":", "_")
				.replace(",", "_")
				.replace(".", "_")
				.replace("[", "_")
				.replace("]", "_")
				.replace("{", "_")
				.replace("}", "_")
				;
	}
	
	
	public SqlDataSelectionsHandler getDatasource() {
		return datasource;
	}

	public void setDatasource(SqlDataSelectionsHandler datasource) {
		this.datasource = datasource;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWidgetVar() {
		return widgetVar;
	}

	public void setWidgetVar(String widgetVar) {
		this.widgetVar = widgetVar;
	}


	public String getLovTextFormat() {
		return lovTextFormat;
	}


	public void setLovTextFormat(String lovTextFormat) {
		this.lovTextFormat = lovTextFormat;
	}


	public String getLovValueFormat() {
		return lovValueFormat;
	}


	public void setLovValueFormat(String lovValueFormat) {
		this.lovValueFormat = lovValueFormat;
	}


	public Boolean getLockInputText() {
		return lockInputText;
	}


	public void setLockInputText(Boolean lockInputText) {
		this.lockInputText = lockInputText;
	}


	public String getDialogId() {
		return dialogId;
	}


	public void setDialogId(String dialogId) {
		this.dialogId = dialogId;
	}


	public long getDlgTs() {
		return dlgTs;
	}


	public void setDlgTs(long dlgTs) {
		this.dlgTs = dlgTs;
	} 
	
}
