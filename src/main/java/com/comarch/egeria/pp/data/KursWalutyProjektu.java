package com.comarch.egeria.pp.data;

import com.comarch.egeria.Utils;

import static com.comarch.egeria.pp.data.AppBean.sqlBean;

public class KursWalutyProjektu extends KursWaluty{

    public KursWalutyProjektu(String kodProjektu){
		DataRow r = sqlBean.getSL("DEL_PROJEKTY_PWSZ").find(kodProjektu);
		if(r != null) {
			
			String kursProjektu = ""+r.get("wsl_wartosc_zewn");
			double kurs = Double.valueOf(kursProjektu.replace(',', '.'));
			
			String symbolWaluty = ""+r.get("wsl_opis2");
			this.setWalSymbol(symbolWaluty);
//			long walId = Long.valueOf(""+r.get("wsl_wartosc_max"));
			this.setPrzelicznik(kurs);
			if(!symbolWaluty.isEmpty())
				this.setWalId(Utils.getWalId(symbolWaluty));
			else this.setWalId(1L);
			
			this.setOpis("Kurs z projektu: "+r.get("wsl_opis")); //TODO nazwa projektu do dodania
		}
    }
    
}
