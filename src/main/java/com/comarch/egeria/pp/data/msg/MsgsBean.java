package com.comarch.egeria.pp.data.msg;

import com.comarch.egeria.pp.danepracownika.pracownik.PracownikPowiadomieniaSqlBean;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;
import java.time.LocalDateTime;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;


@ManagedBean 
@Named
@Scope("view")
public class MsgsBean extends SqlBean {
	private static final long serialVersionUID = -3112;
	private static final Logger log = LogManager.getLogger();

	
	@Inject
	User user;
	
	@Inject
	PracownikPowiadomieniaSqlBean powiadomieniaBean;
	
	
	public static ConcurrentHashMap<String, ConcurrentHashMap<Thread, ConcurrentLinkedQueue<Msg>>> user2th2msg = new ConcurrentHashMap<>();
//	int cnt = 0;//licznik w ramach strony
	
	
	public void testPush(){
		Push.push(user.getDbLogin(),""+LocalDateTime.now(),"OK","TEST");
	}
	
	
	public int processNewMsgs(){
		int ret = 0;
		
		boolean updatedPowiadomienia = false;
		
		ConcurrentLinkedQueue<Msg> Q = user.newMsgs;
		
		while (!Q.isEmpty()) {
			Msg msg = Q.poll();
			if (msg==null) return ret;//conurrency
			ret++;

			
			if ("LOGOUT".equals(msg.getSeverity()) ){
				
				String tytul = ""+msg.getTytul();
				if  (  tytul.equals( "%" ) || tytul.equals( AppBean.getSessionId() ) ) {
					user.logout(true);
					return ret;
				} else {
					continue;
				}
			}
			
			
			if (msg.getWdm_id()!=null && ! "".equals(msg.getWdm_id().trim())){
				
//				powiadomieniaBean.reLoad(msg.getWdm_id());
//				if(!updatedPowiadomienia){
//					powiadomieniaBean.reLoad();
//					com.comarch.egeria.Utils.executeScript("parent.updatePowiadomienia();");??? TODO ustalic co powinno byc
//					updatedPowiadomienia = true;
//				}

				userShowMsgInfo(msg);
				continue;
				
			} else if (!user.getMsgs().contains(msg) ){
				user.getMsgs().add(0, msg);
				user.setCurrentMsg(msg);
			}
			
			//RequestContext.getCurrentInstance().execute("console.log('" + msg.tytul() + "'); parent.showEgrMsgs();");
			com.comarch.egeria.Utils.executeScript("parent.showEgrMsgs();");
			com.comarch.egeria.Utils.update("dlgKomunikat");
			
		}
		
		
		return ret;
	}
	
	
	
	
	
	
	
	



	public void userShowMsgInfo(Msg msg){
		String txt = msg.tytul().replace("'", "");
		
		DataRow r = powiadomieniaBean.getRow(msg.getWdm_id());
		if (r==null)
			return;
		
		String temat = (""+r.get("wdm_temat")).replace("'", "").replace("\"", ""); 
		String trescSkrot = (""+r.get("pp_tresc_skrot")).replace("'", "").replace("\"", "");
		
		String cmd = ""
				+ "try{ "
				+ "  parent.PF('template_growl').renderMessage({ 'summary':'" + temat + "', 'detail': '" + trescSkrot +" ...', 'severity':'info'}); "
				+ "} catch(err) { console.log(err) }";
		
		com.comarch.egeria.Utils.executeScript(cmd);
	}
	
	

	
	
	
	
//	public String thMsgListner(){
//		
//		if (processCollectedMsgs()>0)
//			return "...newCHMMsg...";
//		
//		ConcurrentHashMap<Thread, ConcurrentLinkedQueue<Msg>> userThreads = user2th2msg.get(user.getDbLogin());
//		if (userThreads == null){
//			userThreads = new ConcurrentHashMap<>();
//			user2th2msg.put(user.getDbLogin(), userThreads);
//		}
//		
//
//		try {
//			
//			userThreads.put(Thread.currentThread(), new ConcurrentLinkedQueue<>());
//			
//			if (cnt<=0)
//				Thread.sleep(500);
//			else
//				Thread.sleep(10000);
//			cnt++;
//
//		} catch (InterruptedException e) {
//		
//		} finally {
//			processCollectedMsgs();
//			userThreads.remove(Thread.currentThread());
//		}
//
//
//		String ret = ""+ (new Date()).getTime();
//		return ret;
//	}



	
	
	
	
	
	
	
	
	
//	public int processCollectedMsgs() {
//		
//		int ret = 0;
//		
//		boolean updatedPowiadomienia = false;
//		
//		try {
//			
//			ConcurrentHashMap<Thread, ConcurrentLinkedQueue<Msg>> userThreads = user2th2msg.get(user.getDbLogin());
//			
//			if (userThreads==null || userThreads.isEmpty())
//				return 0;
//			
//			ConcurrentLinkedQueue<Msg> Q = userThreads.get(Thread.currentThread()) ;
//			
//			if (Q==null)
//				return 0;
//			
//			while (!Q.isEmpty()) {
//				Msg msg = Q.poll();
//				ret++;
//
//				
//				if ("LOGOUT".equals(msg.getSeverity()) ){
//					
//					String tytul = ""+msg.getTytul();
//					if  (  tytul.equals( "%" ) || tytul.equals( AppBean.getSessionId() ) ) {
//						doLogout();
//						return ret;
//					} else {
//						continue;
//					}
//				}
//				
//				
//				if (msg.getWdm_id()!=null && ! "".equals(msg.getWdm_id().trim())){
//					
////					powiadomieniaBean.reLoad(msg.getWdm_id());
//					if(!updatedPowiadomienia){
//						powiadomieniaBean.reLoad();
//						RequestContext.getCurrentInstance().execute("parent.updatePowiadomienia();");
//						updatedPowiadomienia = true;
//					}
//
//					userShowMsgInfo(msg);
//					continue;
//					
//				} else if (!user.getMsgs().contains(msg) ){
//					user.getMsgs().add(0, msg);
//					user.setCurrentMsg(msg);
//				}
//				
//				//RequestContext.getCurrentInstance().execute("console.log('" + msg.tytul() + "'); parent.showEgrMsgs();");
//				RequestContext.getCurrentInstance().execute("parent.showEgrMsgs();");
//				RequestContext.getCurrentInstance().update("dlgKomunikat");
//				
//			}
//			
//		} catch (Exception ex){
//			System.out.println(ex);
//		}
//		
//		return ret;
//	}
	
		
	
//	//UWAGA: ta metoda dziala w innym wątku (np. w msgPipeListener ) 
//	public static void collectNewMsgTh(Msg msg){
//		if (msg==null || msg.getUsername()==null)
//			return;
//		
//		String username = msg.getUsername();
//		username = username.toUpperCase();
//		
//		ConcurrentHashMap<Thread, ConcurrentLinkedQueue<Msg>> userThreads = user2th2msg.get(username);
//
//		if (userThreads!=null){
//			for (Entry<Thread, ConcurrentLinkedQueue<Msg>> eth : userThreads.entrySet()) {
//				eth.getValue().add(msg);
//				eth.getKey().interrupt();
//			}
//		}
//		
//	}
	

}
