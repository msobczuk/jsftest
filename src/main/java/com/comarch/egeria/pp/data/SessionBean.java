package com.comarch.egeria.pp.data;

import com.comarch.egeria.Utils;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.utils.auth.EgrLoginBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.PartialViewContext;
import javax.faces.event.PhaseEvent;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import org.primefaces.context.RequestContext;

@ManagedBean
@Named
@Scope("session")
public class SessionBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;

	//	public static final Logger log = LogManager.getLogger(SessionBean.class);//public bo wiekszosc beanow dziedziczy po tym
	private static final Logger log = LogManager.getLogger();
	
	String sessionId = null;

	
	@Inject
	EgrLoginBean egrLoginBean;
	
	@Inject
	User user;
	
	
	@PreDestroy
	private void predestroy() {
		this.egrLoginBean = null;
		this.user = null;
	}
	
	
	private int pracownikTabViewActiveIndex = 0;
	private String pracownikTabViewFile = "PracownikTab1.xhtml";

	private DataRow selectedPracownik = null;

	private DataRow selectedKsiazkaPracownik = null;

	private DataRow selectedWyplata = null;
	private DataRow selectedSrodekTrwaly = null;
//	private DataRow selectedKontoBankowe = null;

	private DataRow selectedBadanie = null;
	private DataRow selectedRodzina = null;

	private DataRow selectedWniosekDoZatw = null;
	private DataRow selectedOdwolanieOdOceny = null;

	private DataRow selectedUmowa = null;

	private DataRow selectedSzkolenie = null;

	private DataRow selectedListaSzkolenie = null;

	private DataRow selectedSzkolenieZapis = null;

	private Long selectedOcenaId = null;

	private DataRow selectedArkusz = null;
	private DataRow selectedArkuszIPRZ = null;

	private DataRow selectedWniosekRekrutacja = null;
	
	private Long iprz_id;

	private DataRow selectedUmowaLoj = null;

	private String rodzajNaboru = "";
	
	// TODO - powiązac loginy z pracownikami...

	

	// private String channelID;

	private Object lista_szkolenie_id;
	private Object szkolenie_id;

	private List listaPozycjiAnkiet = new ArrayList<>();
	private List listaPozycjiWykladowcow = new ArrayList<>();
	
	private String menuLWValue;
	private String menuLWEditRowPage;

	// 106087;
	// private Object prc_id = 106094; //100220; // 3 Object bo nie mamy
	// pewności, że w
	// innych niż EGERIA BAZACH id pracownika
	// jest liczbą!
	
	//private Object prc_id;
	//private Object prc_imie;// = "Gumbert";
	//private Object prc_nazwisko;// = "Majewski";
	//private String user;
	//private String dbUser;
	
	// private List<String> funkcjeUzytkownika = new ArrayList<String>();

	// private HashMap<String, Object> idHM = new HashMap<String,Object>();

	HashMap<String, String> viewReferers = new HashMap<String, String>();
	HashMap<String, Object> viewEntityIds = new HashMap<String, Object>();

	
	
	
	@PostConstruct
	void init() {
		FacesContext ctx = FacesContext.getCurrentInstance();
		ExternalContext ectx = ctx.getExternalContext();
		sessionId = ectx.getSessionId(false);
		
		//za wczesnie - sessionbean powstaje jeszcze przed zalogowaniem!
//		this.setPrc_id(egrLoginBean.prc_id);
//		this.setPrc_imie(egrLoginBean.prc_imie);
//		this.setPrc_nazwisko(egrLoginBean.prc_nazwisko);
//		this.setUser(egrLoginBean.getUsername());
//		this.setDbUser(egrLoginBean.getDbUsername());
		
		// System.out.println("Session: " +sessionId + ";
		// SessionBean.instanceString: " + getInstanceInt());
		// System.out.println("Session: " +sessionId + ";
		// SessionBean.staticString: " + getInstanceInt());
		
		//		contentRedirectUrlFlash = egrLoginBean.getContentRedirectUrlFlash(); zły moment... nowa sesja jest tworzona nawet po kliknieciu w wyloguj...  a wtedylogin ja przejmuje... 
	}
	
	
	
	
	
	
	
	
	public void goPracownikView(DataRow pracownikRow) {

		try {

			this.setSelectedPracownik(pracownikRow);
//			this.setPrc_id(pracownikRow.get("prc_id"));
//			this.setPrc_imie(pracownikRow.get("prc_imie"));
//			this.setPrc_nazwisko(pracownikRow.get("prc_nazwisko"));

			String contextName = FacesContext.getCurrentInstance().getExternalContext().getContextName();
			// System.out.println(contextName );
			if (contextName != null && !contextName.trim().equals("")) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("/" + contextName + "/index.xhtml");
			} else {
				FacesContext.getCurrentInstance().getExternalContext().redirect("/index.xhtml");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

//	  private void processViewTree(UIComponent component) {
//	        // Go to every child
//		  
//		  
//		  HashSet<UIComponent> set = KontrolaUprawnien.getFacetsAndChildrenSet(component);
//		  
//	        for (UIComponent c: set) {
//	            // Display component ID and its type
////	            System.out.println("+ " + c.getId() + " ["+c.getClass()+"]");
//	            
//	            if (c instanceof SelectBooleanCheckbox){
//	            	SelectBooleanCheckbox p = (SelectBooleanCheckbox) c;
//					p.setDisabled(true);
//	            }
//	            
//
//	            processViewTree(c);
//	        }
//	    }

//    public int requestHashCode = -1;
//    public int requestPhase = -1;
	
	public void beforePhase(PhaseEvent event) {
//		HttpServletRequest req = Utils.getRequest();
//		StringBuffer url = req.getRequestURL();
//        String source = req.getParameter("javax.faces.source");
//        PhaseId phaseId = event.getPhaseId();
//        this.requestPhase = phaseId.getOrdinal();
//        this.requestHashCode = req.hashCode();
//        log.debug( url + "\t->\tjavax.faces.source:\t"+source+"\t->........................................................beforePhase " + phaseId);
//		 if (phaseId == PhaseId.RENDER_RESPONSE) {
//			 if (user.isRenderKU())
//				 com.comarch.egeria.Utils.update("hpgRenderKuJSTools");
//		 }
	}


	public void beforePhaseMainTemplate(PhaseEvent event) {
		HttpServletRequest req = Utils.getRequest();
		StringBuffer url = req.getRequestURL();
		log.debug( url + "\t........................................................beforePhaseMainTemplate " + event.getPhaseId() );
	}


	public void afterPhase(PhaseEvent event) {

	}

	private String previousPage = "";
	
	
	private String contentRedirectUrlFlash = null;

	public void preRenderView() throws IOException {
		
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		String referer = FacesContext.getCurrentInstance().getExternalContext().getRequestHeaderMap().get("referer"); // potencjalna
																														// wartosc
																														// np:
																														// http://localhost:8080/PortalPracowniczy/ListaZaplanowanychOcen



		String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();// klucz
																					// -
																					// aktualna
																					// strona...

//		System.out.println(".............................................................preRenderView...." + viewId);

		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		Object row = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("row");

		PartialViewContext pvc = FacesContext.getCurrentInstance().getPartialViewContext();
		boolean ajaxRequest = pvc.isAjaxRequest();
		boolean partialRequest = pvc.isPartialRequest();
		boolean executeAll = pvc.isExecuteAll();
		boolean renderAll = pvc.isRenderAll();
		boolean resetValues = pvc.isResetValues();
		
		
		if (!partialRequest 
				&& referer==null //wbito url bezposrednio w przeglądarce ?   
				&& !viewId.endsWith("index.xhtml")
				&& !viewId.endsWith("404.xhtml")
			)
		{
			//ustawic docelowy context i przekierowac na home...
			contentRedirectUrlFlash = AppBean.xhtmlMappings.get(""+viewId);
			if (contentRedirectUrlFlash==null) contentRedirectUrlFlash = viewId; //viewId to np.   "/test_*.xhtml"

			String queryString = request.getQueryString();
			String baseUrl =  request.getRequestURL().substring(0, request.getRequestURL().indexOf(request.getContextPath() ) ) + request.getContextPath();
			
			if (queryString!=null && !queryString.isEmpty())
				contentRedirectUrlFlash += "?" + queryString; //requestUrl;
			FacesContext.getCurrentInstance().getExternalContext().redirect(baseUrl);
		} else if (!partialRequest 
					&& referer!=null //wbito url bezposrednio w przeglądarce nie będąc zalogowanym ?   
					&& viewId.endsWith("index.xhtml")
					&& !viewId.endsWith("404.xhtml")
				)
			{
				contentRedirectUrlFlash = egrLoginBean.getContentRedirectUrlFlash();
			}
		
		

		if (!partialRequest && !previousPage.equals(viewId)) { 
			// przejscie do nowej strony... uwaga nie obsl. innych zakładek przegladarki...
			
			this.viewReferers.put(viewId, referer);
			this.viewEntityIds.put(viewId, id);
			previousPage = "" + viewId;
		} else 	if (!partialRequest && previousPage.equals(viewId)){
			//przeladowanie strony np. przez F5, ale nie tylko - moze byc, ze kliknieto w nowy obiekt w menu, a strona pozornie jest ta sama.... dla new dodajmy  url?new
			
	//			if (FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().containsKey("new")){
	//				this.viewEntityIds.put(viewId, null);
	//				//TODO: zapewnic przladowanie nowej encji po jej pierwszym zapisie... 
	//			}
			
		} 
		
		
		if (!partialRequest){
			log.debug(this.user + " ----------request/preRenderView----------> " + viewId);
			
			
			if ( this.user!=null 
					&& this.user.getDbLogin()!=null
					&& !viewId.contains("404.xhtml")
				){
			
				Long dxwId = AppBean.widokiXhtml.get(viewId);
				if (dxwId == null){
					dxwId = RodoUtils.dsWstawWidokXhtmlCHM(viewId);
				}
				
				if (dxwId !=null && dxwId > 0){
//					System.out.println("id: "+ id);
					
					Long drId = null;
//					if (isNumeric(""+id)){
					try {
						drId = Long.parseLong(""+id);	
					} catch (Exception e) {
						// to nie jest long i nie uda sie zabisac w PPT_DX_DZIENNIK.DXD_DR_ID
					}
//					} 
					RodoUtils.dsUruchomionoWidokXhtml(user, dxwId, drId);
				}
			
				log.debug("uzt_nazwa: " + this.user.getDbLogin()  + " / szwId: " + user.getSzwId() + " ----------uruchomienie formatki----------> " + viewId);
			}

		
		}
		else{
			log.trace(this.user + " -------partial-request/preRenderView-----> " + viewId);
		}
		
	}
	
	
	
	
	

	
	public void goNewEntityView(String view){
		this.setViewEntityId(null);
		try {
			//FacesContext.getCurrentInstance().getExternalContext().redirect(view);
			com.comarch.egeria.Utils.executeScript("setIframeContentSrc('" + view + "');");
		} catch (Exception e) {
			User.alert(e.getMessage());
		}
	}
	
	public void setViewEntityId(Object id) {
		String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
		this.viewEntityIds.put(viewId, id);
	}

	public Object getViewEntityId() {
		String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
		return this.viewEntityIds.get(viewId);
	}

	public Object getPrc_imie() {
		return user.getImie();
	}

//	public void setPrc_imie(Object prc_imie) {
//		this.prc_imie = prc_imie;
//	}

	public Object getPrc_nazwisko() {
		return user.getNazwisko();
	}

//	public void setPrc_nazwisko(Object prc_nazwisko) {
//		this.prc_nazwisko = prc_nazwisko;
//	}

	public void setPraownikTab(int i, String viewfile) {
		this.pracownikTabViewActiveIndex = i;
		this.pracownikTabViewFile = viewfile;
	}

	// public void showAllPrcData(Object prc_id){
	// try {
	// this.setPrc_id(prc_id);
	// String contextName =
	// FacesContext.getCurrentInstance().getExternalContext().getContextName();
	// //System.out.println(contextName );
	// if (contextName!=null && !contextName.trim().equals("")){
	// FacesContext.getCurrentInstance().getExternalContext().redirect("/" +
	// contextName +"/index.xhtml");
	// }else{
	// FacesContext.getCurrentInstance().getExternalContext().redirect("/index.xhtml");
	// }
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }



	public String currentViewRootId() {
		FacesContext ctx = FacesContext.getCurrentInstance();
		// ExternalContext ectx = ctx.getExternalContext();
		return ctx.getViewRoot().getViewId();
	}
	
	
	public String currentXhtmlFile() {
		FacesContext ctx = FacesContext.getCurrentInstance();
		String[] split = ctx.getViewRoot().getViewId().split("/");
		if (split.length>0)
			return split[split.length-1];
		else
			return "";
	}
	

	public Object getPrc_id() {
		return user.getPrcId();
	}

	public Long getPrc_id_Long() {
		return (long) user.getPrcId();
//		if (prc_id instanceof Number) {
//			return ((Number) prc_id).longValue();
//		}
//
//		return new Long("" + prc_id);
	}

//	public void setPrc_id(Object prc_id) {
//		// System.out.println("setPrc_id>>>" + prc_id);
//		this.prc_id = prc_id;
//	}

	public int getPracownikTabViewActiveIndex() {
		return pracownikTabViewActiveIndex;
	}

	public void setPracownikTabViewActiveIndex(int pracownikTabViewActiveIndex) {
		this.pracownikTabViewActiveIndex = pracownikTabViewActiveIndex;
	}

	public DataRow getSelectedWyplata() {
		// System.out.println("selectedWyplata <- " + selectedWyplata);
		return selectedWyplata;
	}

	public void setSelectedWyplata(DataRow selectedWyplata) {
		// System.out.println("selectedWyplata -> " + selectedWyplata);
		this.selectedWyplata = selectedWyplata;
	}

	public DataRow getSelectedSrodekTrwaly() {
		// System.out.println("selectedSrodekTrwaly <- " +
		// selectedSrodekTrwaly);
		return selectedSrodekTrwaly;
	}

	public void setSelectedSrodekTrwaly(DataRow selectedSrodekTrwaly) {
		// System.out.println("selectedSrodekTrwaly -> " +
		// selectedSrodekTrwaly);
		this.selectedSrodekTrwaly = selectedSrodekTrwaly;
	}


	public DataRow getSelectedBadanie() {
		return selectedBadanie;
	}

	public void setSelectedBadanie(DataRow selectedBadanie) {
		this.selectedBadanie = selectedBadanie;
	}

	public DataRow getSelectedRodzina() {
		return selectedRodzina;
	}

	public void setSelectedRodzina(DataRow selectedRodzina) {
		this.selectedRodzina = selectedRodzina;
	}

	public DataRow getSelectedKsiazkaPracownik() {
		return selectedKsiazkaPracownik;
	}

	public void setSelectedKsiazkaPracownik(DataRow selectedKsiazkaPracownik) {
		this.selectedKsiazkaPracownik = selectedKsiazkaPracownik;
	}

	public DataRow getSelectedPracownik() {
		return selectedPracownik;
	}

	public void setSelectedPracownik(DataRow selectedPracownik) {
		this.selectedPracownik = selectedPracownik;
	}

	public String getPracownikTabViewFile() {
		return pracownikTabViewFile;
	}

	public void setPracownikTabViewFile(String pracownikTabViewFile) {
		this.pracownikTabViewFile = pracownikTabViewFile;
	}

	public DataRow getSelectedWniosekDoZatw() {
		return selectedWniosekDoZatw;
	}

	public void setSelectedWniosekDoZatw(DataRow selectedWniosekDoZatw) {
		this.selectedWniosekDoZatw = selectedWniosekDoZatw;
	}

	public DataRow getSelectedOdwolanieOdOceny() {
		return selectedOdwolanieOdOceny;
	}

	public void setSelectedOdwolanieOdOceny(DataRow selectedOdwolanieOdOceny) {
		System.out.println("seting: >>>>>>> " + selectedOdwolanieOdOceny);
		this.selectedOdwolanieOdOceny = selectedOdwolanieOdOceny;
	}

	public DataRow getSelectedUmowa() {
		return selectedUmowa;
	}

	public void setSelectedUmowa(DataRow selectedUmowa) {
		this.selectedUmowa = selectedUmowa;
	}

	public DataRow getSelectedArkusz() {
		return selectedArkusz;
	}

	public void setSelectedArkusz(DataRow selectedArkusz) {
		this.selectedArkusz = selectedArkusz;
	}

	public DataRow getSelectedArkuszIPRZ() {
		return selectedArkuszIPRZ;
	}

	public void setSelectedArkuszIPRZ(DataRow selectedArkuszIPRZ) {
		this.selectedArkuszIPRZ = selectedArkuszIPRZ;
	}

	public DataRow getSelectedUmowaLoj() {
		return selectedUmowaLoj;
	}

	public void setSelectedUmowaLoj(DataRow selectedUmowaLoj) {
		this.selectedUmowaLoj = selectedUmowaLoj;
	}

	public Long getSelectedOcenaId() {
		return selectedOcenaId;
	}

	public void setSelectedOcenaId(Long selectedOcenaId) {
		this.selectedOcenaId = selectedOcenaId;
	}

	public DataRow getSelectedSzkolenie() {
		return selectedSzkolenie;
	}

	public void setSelectedSzkolenie(DataRow selectedSzkolenie) {
		this.selectedSzkolenie = selectedSzkolenie;
	}
	

	// public String getChannelID() {
	// return "/notify/" + getPrc_id_Long();
	// }

	// public void setChannelID(String channelID) {
	// this.channelID = channelID;
	// }

	public Object getLista_szkolenie_id() {
		return lista_szkolenie_id;
	}

	public void setLista_szkolenie_id(Object lista_szkolenie_id) {
		this.lista_szkolenie_id = lista_szkolenie_id;
	}

	public Object getSzkolenie_id() {
		return szkolenie_id;
	}

	public void setSzkolenie_id(Object szkolenie_id) {
		this.szkolenie_id = szkolenie_id;
	}
	
	public List getListaPozycjiAnkiet() {
		return listaPozycjiAnkiet;
	}

	public void setListaPozycjiAnkiet(List listaPozycjiAnkiet) {
		this.listaPozycjiAnkiet = listaPozycjiAnkiet;
	}

	public DataRow getSelectedSzkolenieZapis() {
		return selectedSzkolenieZapis;
	}

	public void setSelectedSzkolenieZapis(DataRow selectedSzkolenieZapis) {
		this.selectedSzkolenieZapis = selectedSzkolenieZapis;
	}

	public List getListaPozycjiWykladowcow() {
		return listaPozycjiWykladowcow;
	}

	public void setListaPozycjiWykladowcow(List listaPozycjiWykladowcow) {
		this.listaPozycjiWykladowcow = listaPozycjiWykladowcow;
	}

	public DataRow getSelectedListaSzkolenie() {
		return selectedListaSzkolenie;
	}

	public void setSelectedListaSzkolenie(DataRow selectedListaSzkolenie) {
		this.selectedListaSzkolenie = selectedListaSzkolenie;
	}

	public Long getIprz_id() {
		return iprz_id;
	}

	public void setIprz_id(Long iprz_id) {
		this.iprz_id = iprz_id;
	}

	public DataRow getSelectedWniosekRekrutacja() {
		return selectedWniosekRekrutacja;
	}

	public void setSelectedWniosekRekrutacja(DataRow selectedWniosekRekrutacja) {
		this.selectedWniosekRekrutacja = selectedWniosekRekrutacja;
	}
	
	public String getRodzajNaboru() {
		return rodzajNaboru;
	}

	public void setRodzajNaboru(String rodzajNaboru) {
		this.rodzajNaboru = rodzajNaboru;
	}



	public String getUser() {
		return user.getLogin() ;
	}



	public String getDbUser() {
		return user.getDbLogin();
	}



	public String getMenuLWValue() {
		return menuLWValue;
	}



	public void setMenuLWValue(String menuLWValue) {
		this.menuLWValue = menuLWValue;
	}



	public String getMenuLWEditRowPage() {
		return menuLWEditRowPage;
	}



	public void setMenuLWEditRowPage(String menuLWEditRowPage) {
		this.menuLWEditRowPage = menuLWEditRowPage;
	}




	public String getContentRedirectUrlFlash() {
		if ("/".equals(contentRedirectUrlFlash)) contentRedirectUrlFlash = null;
		
		String ret = contentRedirectUrlFlash;
		contentRedirectUrlFlash = "home.xhtml";
		if (ret==null || ret.isEmpty()) ret = "home.xhtml";
		if (ret.startsWith("/")) ret = ret.substring(1);

		if (ret.equals("index.xhtml")){
			ret = "home.xhtml";
		}

		return ret;
	}


//	public void setContentRedirectUrl(String contentRedirectUrl) {
//		this.contentRedirectUrl = contentRedirectUrl;
//	}



//	public void setDbUser(String dbUser) {
//		this.dbUser = dbUser;
//	}

}
