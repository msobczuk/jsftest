package com.comarch.egeria.pp.data;

import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.OcenaOkresowa.model.ZptOceny;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@Named
@Scope("view")
public class TestHibOcenaBean extends SqlBean {
	
	@Inject
	private SessionBean sessionBean;
	
	private ZptOceny ocena = null;
//	private Session s;

	
	@PostConstruct
	public void init(){
		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			ocena = s.get(ZptOceny.class, 12L);//TODO id pobrac (np. z session beana...)
			//s.evict(ocena); 
			s.close();//ocena->detached
		}
	}

	
	
	public void save(){
		System.out.println("TestHibOcenaBean.save ... ");
		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			ocena = (ZptOceny) s.merge(ocena); //MERGE jest ultrawazny, bo dopiero teraz w nowej/niezaleznej sesji zarządzamy obiektem ocena (to nowa zarzadzalna ocena; linijke wczesniej ocena byla skojarzona jako niezarzadzalna/detached) 
			s.saveOrUpdate(ocena);
			s.beginTransaction().commit();
		}
	}


	
	
	

	public ZptOceny getOcena() {
		return ocena;
	}


	public void setOcena(ZptOceny ocena) {
		this.ocena = ocena;
	}
	

}
