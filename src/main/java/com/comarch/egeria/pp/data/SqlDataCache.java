package com.comarch.egeria.pp.data;

import org.jfree.util.Log;
import org.springframework.context.annotation.Scope;

import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import javax.sql.rowset.CachedRowSet;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@ManagedBean 
@Named
@Scope("session")
public class SqlDataCache {
	
	public long ts = 0;// maksymalny TBTS 
	public long hts = 0; //maksymalny TS z Hibernate'a 

	public long lstTS = 0;

	public static ConcurrentHashMap<String, Long> htbtsCHM = new ConcurrentHashMap<>();
	public static ConcurrentHashMap<String, Long> tbtsCHM = new ConcurrentHashMap<>();
	//public ConcurrentHashMap<String, Long> tbtsCHM = new ConcurrentHashMap<>();
	public ConcurrentHashMap<String, SqlDataSelectionsHandler> sqls = new ConcurrentHashMap<>();

	
	@PreDestroy
	private void predestroy() {
		clear();
	}


	public void clear() {
//		if (this.tbtsCHM !=null){
//			this.tbtsCHM.clear();
//			this.tbtsCHM = new ConcurrentHashMap<>();
//		}
		if (this.sqls!=null){
			this.sqls.clear();
			this.sqls = new ConcurrentHashMap<>();
		}
	}
	
	
	
	
	public void loadTBTS() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		long tsmax = this.ts;

		//System.getProperties().setProperty("oracle.jdbc.J2EE13Compliant", "true");

		Long tsLst = null;

		try (Connection conn = JdbcUtils.getConnection(false)) {

			CachedRowSet crs = JdbcUtils.sqlExecQuery(conn," select TBTS_TABLE, cast(TBTS_TS as date) TBTS_TS  from PPADM.ppt_tab_timestamps where tbts_ts  > ? ", new Timestamp(this.ts));
			while ( crs.next() ){

				java.sql.Timestamp tbts0 = (Timestamp) crs.getObject("TBTS_TS");
				long tbts = tbts0.getTime();
				String tb = crs.getString("TBTS_TABLE");
				//this.tbtsCHM.put(tb, tbts);
				tbtsCHM.put(tb, tbts);
				if (this.ts < tbts){//tbts -> nowe dane w tb
					for (SqlDataSelectionsHandler lw : this.sqls.values()) {
						if (lw.getDependencyTablesCHM().get(tb)!=null) {
							lw.resetTs();
							Log.debug("SqlDataCache - TBTS / DBTS - unieważniono stan LW " + lw.getShortName() + " /tb: " + tb);
						}
					}

					if ("PPADM.PPT_ADM_LISTY_WARTOSCI".equals(tb) )
						tsLst = tbts; //System.out.println("Są zmiany w definicjach LW!");

					if (tsmax<tbts)
						tsmax = tbts;
				}
			}

			if (tsLst!=null) {
				//crs = JdbcUtils.sqlExecQuery("select distinct lst_nazwa from PPT_ADM_LISTY_WARTOSCI where LST_AUDYT_DM >  ? ", new Timestamp(tsLst-1000));
				//tak było fajnie aż komuś przyszło do głowy powyłączać dzialanie triggerów audytowych na bazie test... triggery mozna wyłączyć per użytkownik... trzeba przejsc na inną kolumne
				// ... np. na LST_SQL_TEST_KIEDY:
				crs = JdbcUtils.sqlExecQuery(conn,"select distinct lst_nazwa from PPT_ADM_LISTY_WARTOSCI where LST_SQL_TEST_KIEDY >  ? ", new Timestamp(tsLst-2000));
				while ( crs.next() ){
					String lwNzwLC = (""+ crs.getString("lst_nazwa")).toLowerCase();
					this.sqls.remove(lwNzwLC);
					AppBean.listyWartosciHM.remove(lwNzwLC);
				}
			}
		}

		this.ts = tsmax;
	}
	
	
	
	public void loadHTS() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		long tsmax = this.hts;
		
		List<Entry<String, Long>> collect = htbtsCHM.entrySet().stream().filter(e -> e.getValue()>this.hts).collect(Collectors.toList());
		for (Entry<String, Long> entry : collect) {
			long tbts = entry.getValue();
			String tb = entry.getKey();

			for (SqlDataSelectionsHandler lw : this.sqls.values()) {
				if (lw.getDependencyTablesCHM().get(tb)!=null) {
					lw.resetTs();
					Log.debug("SqlDataCache - Hibernate TS / Application TS - unieważniono stan LW " + lw.getShortName() + " /tb: " + tb);
				}
			}

			if (tsmax<tbts)
				tsmax = tbts;
		}

		this.hts = tsmax;
	}
	
	
	public long getTotalCachedRowsCount(){
		long ret = 0L;
		
		for ( Entry<String, SqlDataSelectionsHandler> x : sqls.entrySet()) {
			ret+=x.getValue().data.size();
		}
		
		return ret;
	}
	
	
	public long size(){
		return sqls.keySet().size();
	}
	
	public List<SqlDataSelectionsHandler> getListyWartosci(){
		if (this.sqls==null)
			return new ArrayList<>();
		
		return this.sqls.values().stream().sorted((a,b) -> a.getShortName().compareTo(b.getShortName()) ).collect(Collectors.toList());
	}
	
}
