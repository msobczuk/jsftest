package com.comarch.egeria.pp.data;

import org.primefaces.component.api.UIColumn;

import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.HashMap;

@ManagedBean
public class CLookupTableBean {
	
	public void show() {
		// System.out.println("cLookupTableBean.show.!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	}
	
	
//	public String doFilterAndSelectRowByUrlParams(SqlDataSelectionsHandler lw, V view){
//
//		if (lw==null)
//			return "";
//
//		String lwName = lw.getShortName();
//
////		ExternalContext ectx = FacesContext.getCurrentInstance().getExternalContext();
////		Map urlParams = ectx.getRequestParameterMap();
//
//		Object id = view.getUrlParams().get(lwName);
//		if (id==null)
//			return "";
//
//		if (lw.getColumns().isEmpty())
//			lw.reLoadData(); //lista nie jest jeszcze wczytana
//
//		try {
//
//			String idColName = lw.getColumns().get(0);
//			lw.getData();
//			lw.setColFilter(idColName, "="+ id, true);
//
////			if (!SqlBean.isNumeric(""+id)) return "... niedozwolny wpis w url ...";
////			lw.setSql("select * from (\n " + lw.getSql() + "\n) urlfiltr  where urlfiltr." + idColName + " = '" + id + "'" );//uwaga na sqlInjection - dlatego testujemy wczesniej SqlBean.isNumeric(""+id)
//
//			int size = lw.getData().size();
//			if (size==1){
//
//				DataRow r = lw.getData().get(0);
//				lw.setCurrentRow(r);
//
//				UIComponent uic = UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());
//				DataTable tb = (DataTable) uic.findComponent("dane");
//				if (tb==null)
//					return "";
//
//				String tbClientId = tb.getClientId();
//
////				RequestContext.getCurrentInstance().execute("update('" + uic.getClientId() + "');");
//				RequestContext.getCurrentInstance().execute("PF(wid('" + tbClientId + "')).selectRow(0);");
//				return "... url ... ";
//
//			} else if (size<=0)  {
//				return "Brak wiersza " + id + " wskazanego w url. Możliwe, że nie masz do niego dostepu lub został usunięty z bazy.";
//			} else if (size>1){
//				return "Na liście jest więcej wierszy pasujących do parametrów w url.";
//			}
//
//		} catch (Exception e) {
//			Log.error(e.getMessage(), e);
//		}
//
//		return "";
//	}
	
	

	private HashMap<String, Object> getIdHM() {
		HashMap<String, Object> idhm = (HashMap<String, Object>) FacesContext.getCurrentInstance().getExternalContext()
				.getFlash().get("idHM");
		if (idhm == null) {
			idhm = new HashMap<>();
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("idHM", idhm);
		}
		return idhm;
	}

	public void goNewEntityPage(String editPage) throws IOException {

		String vRootId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
		HashMap<String, Object> idhm = getIdHM();
		idhm.put(vRootId, null);

		// sessionBean.getIdHM().put(vRootId, null);

		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", null);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", null);
		FacesContext.getCurrentInstance().getExternalContext().redirect(editPage);
	}

//	public void setValue(DataRow r, String valueFormat, String editPage) throws IOException {
//
//		String strId = r.getIdAsString();
//		if (editPage != null && !"".equals(editPage)) {
//
//			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", strId);
//
//			String vRootId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
//			HashMap<String, Object> idhm = getIdHM();
//			idhm.put(vRootId, strId);
//
//			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", r);
//			FacesContext.getCurrentInstance().getExternalContext().redirect(editPage);
//			return;
//		}
//
//		Object val = null;
//
//		if (valueFormat == null || valueFormat.trim().equals(""))
//			val = r != null ? strId : null;
//		else
//			val = formatLovText(valueFormat, r);
//
//		FacesContext facesContext = FacesContext.getCurrentInstance();
//		ELContext elContext = facesContext.getELContext();
//		ValueExpression valueExpression = facesContext.getApplication().getExpressionFactory()
//				.createValueExpression(elContext, "#{cc.attrs.value}", Object.class);
//		try {
//			valueExpression.setValue(elContext, val);
//		} catch (Exception e) {
//			if (!(e instanceof NullPointerException)) {
//				throw e;// inne wyjatki chcemy zobaczyc
//			}
//		}
//	}

	public String setCurrentAttrValue(SqlDataSelectionsHandler ds, Object pk) {
		if (ds == null)
			return "";
		if (pk != null) 
			ds.setCurrentRow(ds.find(pk));
		else
			ds.setCurrentRow(null);

		return "";
	}

//	public String formatLovText(String format, DataRow r) {
//		// TODO... formatowanie wyniku LOV
//		if (r == null)
//			return "";
//
//		// String ret = String.format("%1s %2s", r.toArray() );
//		String ret = String.format(format, r.toArray());
//
//		return ret;
//	}
	// public void show(DataRow r){
	// System.out.println("cLookupTableBean.show: " + r);
	// }
	//
	// public void show(Object o){
	// System.out.println("cLookupTableBean.show: " + o);
	// }
	
	public String exportColumn(UIColumn column) {
        
//        for(UIComponent child: column.getChildren()) {
//
//            if(child instanceof HtmlOutputText && child.isRendered()) {
//
//            	String value = ComponentUtils.getValueToRender(FacesContext.getCurrentInstance(), child);
//
//            	if (value==null)
//            		return "";
//
//            	Object oValue = ((HtmlOutputText) child).getValue();
//
//            	if (oValue instanceof Timestamp)
//            		return (""+oValue)
//            				.replace(" 00:00:00.0", "")
//            				.replace(".0", "");
//
//            	else
//            		return ""+value;
//
//            }
//        }

        return "";
	}

	
	public static SqlDataSelectionsHandler findCurrentUIComponentDatasource(){
		UIComponent p = UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());
		while (p!=null){
			Object ds = p.getAttributes().get("datasource");
			if (ds!=null && ds instanceof SqlDataSelectionsHandler){
				return (SqlDataSelectionsHandler) ds;
			}
			p=p.getParent();
		}
		return null;
	}
	

}
