package com.comarch.egeria.pp.data.msg;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Obejście problemu brakujących websocketów w MF (pseudo long polling)
 * Bean inicjowany jest z EgrTemplate*.xhtml <p:remoteCommand name="pollmsg" ....
 */
@ManagedBean 
@Named
@Scope("view")
public class MsgPollBean extends SqlBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private static final Logger log = LogManager.getLogger();
	
	@Inject
	User user;

	public static  ConcurrentHashMap<String,  Thread> usersSessions = new ConcurrentHashMap<>(); //UsersSessions
	public static  ConcurrentHashMap<String,  Msg> usersMsgs = new ConcurrentHashMap<>(); //UsersSessions
	
	public void wczytajMsg() {
		try {
				
			//String ukey = (""+user.getLogin()).toUpperCase(); //XAAHE
			String ukey = (""+user.getDbLogin()).toUpperCase(); //MDZIOBEK
//			System.out.println("... MsgPollBean.wczytajMsg / " + ukey + " ...");
			
			usersSessions.put(ukey, Thread.currentThread());

			
//			for (int i=0; i<3; i++){
				
//				try {
//					Thread.currentThread().sleep(10000);	
//				} catch (Exception e) {
//					//przerwano oczekiwanie ...
//				}
				
				Msg msg = usersMsgs.get( ukey );
				
				if (msg!=null) {
					usersMsgs.remove(ukey);
					
					System.out.println(ukey + " / wczytajMsg->" + msg);
					
					if ("KILL".equals(msg.getSeverity())){
						//		System.out.println("....-> kill");
						return;
					}
					
					msg.renderEgrGrowlMessage();
					this.resetTs("ppl_adm_powiadomienia");
				}
				
//			}
			
			usersSessions.remove(ukey);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
	}
	
	
	//UWAGA metoda realizowana poza kontrkstem jsf (np. inicjowana przez MsgPipeListener.java po odebraniu komunikatu z bazy) !!!! 
	public static void process(String msgTxt){
//		System.out.println("MsgPollBean.process ... ");
		
		if (msgTxt==null) return;
		
		Msg msg = new Msg(msgTxt);
		if (msg.getUsername()==null)
			return;
		
		String ukey = (""+msg.getUsername()).toUpperCase();
		usersMsgs.put(ukey, msg);
			
//		Thread th = usersSessions.get(ukey);
//		if (th!=null){
//			System.out.println("MsgPollBean.process ... -> thread " + ukey + " ... ");
//			th.interrupt();
//		}
		
//		System.out.println("MsgPollBean.process ... END ");
	}
	
	
	//UWAGA metoda realizowana poza kontekstem jsf!!!! 
//	public static void doLogoutTh(String username){
//		Thread th = usersSessions.get((""+username).toUpperCase());
//		if (th!=null){
//			//usersMsgs.remove(username);
//			Msg msg = new Msg();
//			msg.setSeverity("KILL");
//			usersMsgs.put(username, msg);
////			System.out.println("doLogoutTh -> " + username);
//			th.interrupt();
//		}
//	}
	

	
//	//UWAGA metoda realizowana poza kontekstem jsf!!!! 
//	public static void putMsg(String username, String txt){
//		Thread th = usersSessions.get(username);
//		if (th!=null){
//			Msg msg = new Msg();
//			msg.setTekst(txt);
//			usersMsgs.put(username, msg);
//			th.interrupt();
//		}
//	}

	
}
