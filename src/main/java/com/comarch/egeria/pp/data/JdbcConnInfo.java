package com.comarch.egeria.pp.data;

import java.sql.Connection;
import java.sql.SQLException;

public class JdbcConnInfo {
	private Connection connection;
	private String description;
	
	public JdbcConnInfo(Connection conn, String description) {
		this.connection = conn;
		this.description = description;
	}
	
	public boolean isClosed(){
		if (this.connection == null)
			return true;
		
		try {
			return this.connection.isClosed();
		} catch (SQLException e) {
			e.printStackTrace();
			return true;
		}
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "" + this.description + "\n... isClosed(): " + isClosed();
	}
}
