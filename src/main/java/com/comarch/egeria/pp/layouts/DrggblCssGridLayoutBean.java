package com.comarch.egeria.pp.layouts;

import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.view.facelets.FaceletCache;
import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

import static com.comarch.egeria.Utils.nz;

@ManagedBean
@Named
@Scope("view")
public class DrggblCssGridLayoutBean {



    DrggblCssGridNode layout = new DrggblCssGridNode();

    DrggblCssGridNode availableNodes = new DrggblCssGridNode();

    HashMap<String, DrggblCssGridNode> nodesHM = new HashMap<>();


    @PostConstruct
    public void init(){

        this.layout.setElId("nodeTbx");
        nodesHM.put(this.layout.elId, this.layout);

        this.availableNodes.setElId("nodeRow1");
        nodesHM.put(this.availableNodes.elId, this.availableNodes);




        for (int i=0; i<2; i++){
            DrggblCssGridNode n = new DrggblCssGridNode();
            n.setElId("elkey"+i);
            n.parent = layout;
            layout.children.add(n);
            nodesHM.put(n.elId, n);
        }


        for (int i=0; i<5; i++){
            DrggblCssGridNode n = new DrggblCssGridNode();
            n.setElId("avkey"+i);
            n.parent = availableNodes;
            availableNodes.children.add(n);
            nodesHM.put(n.elId, n);
        }

    }



    public void dnd(){

        Map<String, String> p = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String elmnt = nz(p.get("elmnt"));
        String dropto = nz(p.get("dropto"));
        String beforeel = nz(p.get("beforeel"));


        DrggblCssGridNode node = nodesHM.get(elmnt);
        DrggblCssGridNode dropToNode = nodesHM.get(dropto);
        if (dropToNode==null) {
            availableNodes.addNode(node);
        }
        else {
            DrggblCssGridNode dropBeforeNode = nodesHM.get(beforeel);
            if (dropBeforeNode != null && dropBeforeNode.parent != null ) {
                dropToNode.addNode(node,  dropBeforeNode.parent.children.indexOf(dropBeforeNode) );
            }else {
                dropToNode.addNode(node, null);
            }
        }

        System.out.println("dodrop: " + elmnt + " -> " + dropto);
    }



//    public void tableToTree(){
//
//        Map<String, String> p = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
//        String property = p.get("property");
//
//        UIComponent currentComponent = UIComponent.getCurrentComponent(FacesContext.getCurrentInstance());
//
//        System.out.println("tableToTree " + property);
//    }

    public DrggblCssGridNode getAvailableNodes() {
        return availableNodes;
    }

    public void setAvailableNodes(DrggblCssGridNode availableNodes) {
        this.availableNodes = availableNodes;
    }




    public DrggblCssGridNode getLayout() {
        return layout;
    }

    public void setLayout(DrggblCssGridNode layout) {
        this.layout = layout;
    }

}
