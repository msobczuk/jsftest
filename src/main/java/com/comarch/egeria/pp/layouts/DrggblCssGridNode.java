package com.comarch.egeria.pp.layouts;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.JdbcUtils;
import org.openxmlformats.schemas.officeDocument.x2006.docPropsVTypes.Ui1Document;
import org.primefaces.component.dnd.Droppable;
import org.primefaces.event.DragDropEvent;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DrggblCssGridNode {


    String elId = "";
    String styleClass = "ui-g ui-g-v-center";
    DrggblCssGridNode parent = null;
    List<DrggblCssGridNode> children = new ArrayList<>();


    public void onDrop(DragDropEvent dragDropEvent) {
        FacesContext ctx = FacesContext.getCurrentInstance();

        Object source = dragDropEvent.getSource();
        DrggblCssGridNode dnode = (DrggblCssGridNode) dragDropEvent.getData();

        this.addNode(dnode);

        String dropid = dragDropEvent.getDropId();
        String dragid = dragDropEvent.getDragId();

        Map<String, String> params = ctx.getExternalContext().getRequestParameterMap();
        String left = params.get(dragid + "_left");
        String top = params.get(dragid + "_top");
        System.out.println("\r\n\r\n\r\n\r\n\r\n\r\nxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx dropid: " +dropid  +"; dragid: "+ dragid + "; left: " + left + "; top: " + top );

    }


    public DrggblCssGridNode addNode(DrggblCssGridNode node){
        return this.addNode(node, null);
    }

    public DrggblCssGridNode addNode(DrggblCssGridNode node, Integer position){
        if (node==null) {
            System.out.println();
            return null;
        }

        if (node.parent!=null){
            node.parent.removeNode(node);
        }

        node.parent = this;
        this.children.remove(node);

        if (position==null)
            this.children.add(node);
        else
            this.children.add(position, node);

        return node;
    }

    public DrggblCssGridNode removeNode(DrggblCssGridNode node){
        node.parent = null;
        this.children.remove(node);
        return node;
    }


    public DrggblCssGridNode getParent() {
        return parent;
    }

    public void setParent(DrggblCssGridNode parent) {
        this.parent = parent;
    }



    public List<DrggblCssGridNode> getChildren() {
        return children;
    }

    public void setChildren(List<DrggblCssGridNode> children) {
        this.children = children;
    }



    public String getElId() {
        return elId;
    }

    public void setElId(String elId) {
        this.elId = elId;
    }



    public String getStyleClass() {
        return styleClass;
    }

    public void setStyleClass(String styleClass) {
        this.styleClass = styleClass;
    }



}
