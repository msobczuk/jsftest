package com.comarch.egeria.pp.delegacjeStd.model;

import static com.comarch.egeria.Utils.nz;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.persistence.Entity;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@Entity
public class Delegacja extends PptDelWnioskiDelegacji {
	private static final long serialVersionUID = 1L;

	
	public Delegacja () {
		
	}
	
	
	public Delegacja ustawDomyslne() {
		
		this.setWndPrcId(User.getCurrentUser().getPrcIdLong());
		this.addNewCelDel();
		this.addNewSrodekLok();
		this.addNewZapraszajacy();
		this.setWndFBezkosztowa("N");
		this.setWndFZaliczka("N");
		this.setWndStdId(getStdIdMpips());
		
		this.dodajKalkulacje();
		
		return this;
	}
	
	public boolean validate(){
		ArrayList<String> errorList = new ArrayList<>();

		if(eq("T", nz(this.getWndFProjektowa())) && nz(this.getWndProjKod()).isEmpty())
			errorList.add("Po zaznaczeniu checkboxa 'Źródło finansownaia / Projekt' należy wskazać wybrany projekt.");

		if(this.getWndDataWyjazdu() == null)
			errorList.add("Należy podać datę wyjazdu.");
		
		if(this.getWndDataPowrotu() == null)
			errorList.add("Należy podać datę powrotu.");
		
		if(this.getWndDataWyjazdu() != null && this.getWndDataPowrotu() != null)
			if (this.getWndDataWyjazdu().after(this.getWndDataPowrotu()))
				errorList.add("Data wyjazdu nie może być większa niż data powrotu.");
		
		if(this.getWndOpis() == null || "".equals(this.getWndOpis()))
			errorList.add("Opis wniosku nie może być pusty");

		if(this.getDeltCeleDelegacjis().isEmpty())
			errorList.add("Należy podać cel delegacji");
		
		if (this.getDeltSrodkiLokomocjis().get(0).getSlokSrodekLokomocji() == null 
				|| this.getDeltSrodkiLokomocjis().get(0).getSlokSrodekLokomocji().isEmpty())
			errorList.add("Należy wskazać środek lokomocji");
		
		if(this.getDeltOsobyDodatkowes().size()>0) {
			List<PptDelOsobyDodatkowe> listaOsDod = this.getDeltOsobyDodatkowes().stream()
				.filter(os -> os.getOdoKlKod() == 0)
				.collect(Collectors.toList());
			if(listaOsDod.size()>0)
				errorList.add("Na liście osób dodatkowych znajdują się puste rekordy");
		}
		
		if(eq("T", this.getWndFZaliczka())) {
			if(!this.getKalkulacjaWniosek().walidujZaliczki() )
				errorList.add("Część I - zaliczki - należy poprawić wprowadzone dane");
		}
		
		if(!this.getKalkulacjaRozlicz().walidujZaliczki() )
			errorList.add("Część II - podsumowanie kosztów i zaliczek - należy poprawić wprowadzone dane");
		
		for (String er :errorList) {
			User.alert(er);
		}

		return errorList.isEmpty();
	}
	
	public void zapisz () {
		if(this.validate()) {
			if(this.getWndDataWniosku() ==  null)
				this.setWndDataWniosku(new Date());
			if(nz(this.getWndNumer()).isEmpty()) {
				Utils.updateComponent("tabViewId");
			}
			HibernateContext.saveOrUpdate(this);
			User.info("Pomyślnie zapisano wniosek %1$s", this.getWndNumer());
		}
	}
	
	public KalkulacjaWniosek getKalkulacjaWniosek() {
		if (this.getDeltKalkulacjes() == null || this.getDeltKalkulacjes().isEmpty()) 
			return null;
		 
		Optional<PptDelKalkulacje> findFirst = this.getDeltKalkulacjes().stream().filter(k->"WNK".equals(k.getKalRodzaj())).findFirst();
		if (findFirst.isPresent()) return (KalkulacjaWniosek) findFirst.get();
		
		return null;
	}
	
	public KalkulacjaWstepna getKalkulacjaWstepna() {
		if (this.getDeltKalkulacjes() == null || this.getDeltKalkulacjes().isEmpty()) 
			return null;
		 
		Optional<PptDelKalkulacje> findFirst = this.getDeltKalkulacjes().stream().filter(k->"WST".equals(k.getKalRodzaj())).findFirst();
		if (findFirst.isPresent()) return (KalkulacjaWstepna) findFirst.get();
		
		return null;
	}
	
	public KalkulacjaRozlicz getKalkulacjaRozlicz() {
		if (this.getDeltKalkulacjes() == null || this.getDeltKalkulacjes().isEmpty()) 
			return null;
		 
		Optional<PptDelKalkulacje> findFirst = this.getDeltKalkulacjes().stream().filter(k->"ROZ".equals(k.getKalRodzaj())).findFirst();
		if (findFirst.isPresent()) return (KalkulacjaRozlicz) findFirst.get();
		
		return null;
	}
	
	public void addNewZapraszajacy() {
		this.addDeltZapraszajacy (new PptDelZapraszajacy());
	}
	
	public void addNewCelDel() {
		this.addDeltCeleDelegacji (new PptDelCeleDelegacji());
	}
	
	public void addNewSrodekLok() {
		this.addDeltSrodkiLokomocji(new PptDelSrodkiLokomocji());
	}
	
	public void addNewOsDodatk() {
		this.addDeltOsobyDodatkowe(new PptDelOsobyDodatkowe());
	}
	
	private void dodajKalkulacje() {
		this.addDeltKalkulacje(new KalkulacjaWniosek().ustawDomyslne());
		this.addDeltKalkulacje(new KalkulacjaWstepna().ustawDomyslne());
		this.addDeltKalkulacje(new KalkulacjaRozlicz().ustawDomyslne());
	}
	
	public long getStdIdMpips() {
		long ret = 0;
		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_STAWKI");
		if(lw != null) {
			for(DataRow row : lw.getAllData()) {
				Object kodStawki = row.get("std_kod_stawki");
				if(kodStawki != null)
					if(kodStawki.equals("MPiPS")) {
						BigDecimal kod = (BigDecimal) row.get("std_id"); 
								return kod.longValue();
					}
					else continue;
					}
		}
		return ret;
	}
	
	@Override
	public void setWndFBezkosztowa(String wndFBezkosztowa) {
		super.setWndFBezkosztowa(wndFBezkosztowa);
		this.setWndFZaliczka("N");
	}
	

}
