package com.comarch.egeria.pp.delegacjeStd.model;

import static com.comarch.egeria.Utils.nz;
import static com.comarch.egeria.Utils.round;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Entity;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;

@Entity
public class Kalkulacja extends PptDelKalkulacje {
	private static final long serialVersionUID = 1L;
	
	public Kalkulacja() {
		
	}

	public List<PozycjaKalkulacji> getPozycjeKalkulacji(){ //a w HibernateContext zarówno .addAnnotatedClass(PozycjaKalkulacji.class) oraz .addAnnotatedClass(DeltPozycjeKalkulacji.class)
		return (List) super.getDeltPozycjeKalkulacjis() ; 
	}
	
	public List<PozycjaKalkulacji> getPozycjeKalkulacjiHotelowe(){
		return this.getPozycjeKalkulacji().stream()
				.filter(p -> "H".equals(p.getTypPozycjiKategoria()) || "RH".equals(p.getTypPozycjiKategoria()))
				.collect(Collectors.toList()) ;
	}
	
	public List<PozycjaKalkulacji> getPozycjeKalkulacjiPobytowe(){
		return this.getPozycjeKalkulacji().stream().filter(p -> "P".equals(p.getTypPozycjiKategoria())).collect(Collectors.toList()) ;
	}
	
	public List<PozycjaKalkulacji> getPozycjeKalkulacjiKomunikacyjne(){
		return this.getPozycjeKalkulacji().stream().filter(p -> "KM".equals(p.getTypPozycjiKategoria())).collect(Collectors.toList()) ;
	}
	
	public List<PozycjaKalkulacji> getPozycjeKalkulacjiRDojazdowe(){
		return this.getPozycjeKalkulacji().stream().filter(p -> "RD".equals(p.getTypPozycjiKategoria())).collect(Collectors.toList()) ;
	}
	
	public List<PozycjaKalkulacji> getPozycjeKalkulacjiInne(){
		return this.getPozycjeKalkulacji().stream().filter(p -> "I".equals(p.getTypPozycjiKategoria())).collect(Collectors.toList()) ;
	}	
	
	public List<PozycjaKalkulacji> getPozycjeKalkulacjiTransportowe(){
		return this.getPozycjeKalkulacji().stream().filter(p -> "T".equals(p.getTypPozycjiKategoria())).collect(Collectors.toList()) ;
	}	
	
	public List<PozycjaKalkulacji> getPklListByKtgDiet(String strKtgList, boolean exclude){

		List<String> kategorie = Arrays.asList(strKtgList.split(","));
		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_TPOZ_KTG", this.getDeltWnioskiDelegacji().getWndStdId());

		List<Long> tpozIds = lw.getData().stream()
				.filter(r ->(kategorie.contains(r.getIdAsString())))
				.mapToLong(r -> r.getAsLong("tpoz_id"))
				.boxed()
				.collect(Collectors.toList());
		
		List<PozycjaKalkulacji> ret;
		if (!exclude) 
			ret = this.getPozycjeKalkulacji().stream()
					.filter(p -> tpozIds.contains(p.getPklTpozId())).collect(Collectors.toList());
		else
			ret = this.getPozycjeKalkulacji().stream()
					.filter(p -> !tpozIds.contains(p.getPklTpozId())).collect(Collectors.toList());
		return ret;
	}
	
	
	public void addNewTrasa() {
		this.addDeltTrasy(new PptDelTrasy());
	}
	
	public void addNewDelCzynnosciSluzbowe () {
		this.addDelCzynnosciSluzbowe(new PptDelCzynnosciSluzbowe());
	}
	
	public void addNewPkl() {
		this.addDeltPozycjeKalkulacji(new PptDelPozycjeKalkulacji());
	}
	
	public void wyczyscTrasy() {
		List<PptDelTrasy> trasy = this.getDeltTrasies().stream().collect(Collectors.toList());
		for (PptDelTrasy t : trasy) {
			this.removeDeltTrasy(t);
		}
	}
	
	public String formatujDniCzynnosciSluzbowe(Double czas) { //TODO zastanowić się nad zmianą nazwy i dać to do jakiejs utlisowej klasy
		String ret = "";
		if(czas != null) {
			double dni = Math.floor((czas / 24));
			double x = czas - dni * 24;
			if(dni == 1.0)
				ret = ""+(int)dni+" dzień"+" "+round(x,2)+" h";
			else 
				ret = ""+(int)dni+" dni"+" "+round(x,2)+" h";
		}
		return ret;
	}
	
	public void generujCzynnosciSluzbowe() {
		if(eq(this.getDeltWnioskiDelegacji().getWndRodzaj(), 1L))
		generujCzynnosciSluzbowejDelKraj();
		else generujCzynnosciSluzboweDelZagr();
	}

	private void generujCzynnosciSluzbowejDelKraj() {
		if(this.getDeltTrasies().isEmpty() || this.getDeltTrasies() == null) {
			User.info("Brak danych w planie trasy, wygeneruj/wprowadź najpierw plan trasy lub wprowadź terminy czynności slużbowych ręcznie");
			return;
		}
		if(this.getDeltTrasies().get(0).getTrsDataOd() == null || this.getDeltTrasies().get(this.getDeltTrasies().size() - 1).getTrsDataDo() == null) {
			User.info("Data wyjazdu oraz przyjazdu musi być uzupełniona w planie trasy");
			return;
		}
		this.getDelCzynnosciSluzbowe().clear();
		PptDelTrasy pierwszyOdcinekTrasy = this.getDeltTrasies().get(0);
		PptDelTrasy ostatniOdcinekTrasy = this.getDeltTrasies().get(this.getDeltTrasies().size() - 1);
		
		PptDelCzynnosciSluzbowe czs = new PptDelCzynnosciSluzbowe(); 
		czs.setCzynKrajId(1L);
		czs.setCzynCzynnosciOd(pierwszyOdcinekTrasy.getTrsDataOd());
		czs.setCzynCzynnosciDo(ostatniOdcinekTrasy.getTrsDataDo());
		this.addDelCzynnosciSluzbowe(czs);
	}
	
	private void generujCzynnosciSluzboweDelZagr() {
		//TODO metoda sprawdzająca plan trasy przed generacją czynności slużbowych
		if(this.getDeltTrasies().isEmpty() || this.getDeltTrasies() == null) {
			User.info("Brak danych w planie trasy, wygeneruj/wprowadź najpierw plan trasy lub wprowadź terminy czynności slużbowych ręcznie");
			return;
		}
		if(this.getDeltTrasies().size()<2) {
			User.info("Plan trasy musi mieć co najmniej dwie pozycje");
			return;
		}
		if(this.getDeltTrasies().get(0).getTrsDataOd() == null || this.getDeltTrasies().get(this.getDeltTrasies().size() - 1).getTrsDataDo() == null) {
			User.info("Data wyjazdu oraz przyjazdu musi być uzupełniona w planie trasy");
			return;
		}
		Date dataPomocnicza = null; // data poprzedniego wiersza od
		this.getDelCzynnosciSluzbowe().clear();
		Date ostatniaData = (Date) this.getDeltTrasies().get(this.getDeltTrasies().size() - 1).getTrsDataDo().clone();
		
		for(PptDelTrasy trs : this.getDeltTrasies()) {
			PptDelCzynnosciSluzbowe czs = new PptDelCzynnosciSluzbowe(); 
			if(dataPomocnicza != null) {
				czs.setCzynKrajId(trs.getTrsKrIdOd());
				czs.setCzynCzynnosciOd(dataPomocnicza);
				czs.setCzynCzynnosciDo(trs.getTrsDataOd());
				this.addDelCzynnosciSluzbowe(czs);
			} 
			dataPomocnicza = trs.getTrsDataOd();
		}
		this.getDelCzynnosciSluzbowe().get(this.getDelCzynnosciSluzbowe().size() -1).setCzynCzynnosciDo(ostatniaData);
	}
	
	public void addPklByKtg(String ktgPozycji) {
		if(ktgPozycji == null || ktgPozycji.isEmpty())
			return;
		
		switch(ktgPozycji) {
		case "P": 
			addDietyPobytowe(1.0);
			break;
		case "H": 
			addDietaRyczaltHotel("DH", 1.0);
			break;
		case "RH":
			addDietaRyczaltHotel("RH", 1.0);
			break;
		case "RD":
			addRyczaltDojazdowy();
			break;
		case "KM":
			addRyczaltKom(1.0);
			break;
		default:
			addPklInne();
			break;
		}
	}
	
	
	//używane w del kraj i zagr, w zagr ustawi stawkę na 0.0 i zmieni się po wyborze kraju
	public void addDietyPobytowe(Double ilosc) {
		if(ilosc != 0.0) {
			PozycjaKalkulacji p = new PozycjaKalkulacji().ustawDomyslnePklPobytowa();
			this.addDeltPozycjeKalkulacji(p);
			p.setPklTpozId(p.getTpozIdByKodPozycji("DP"));
			p.setPklIlosc(ilosc);
			if(this.getDeltWnioskiDelegacji().getWndRodzaj().equals(1L))
			p.setPklStawka(30.0);
		}
	}
	
	//używane tylko w del zagr bo krid i tylko przy generowaniu diet
	public void addDietyPobytowe(Double ilosc, Long krId) {
		if(ilosc != 0.0) {
			PozycjaKalkulacji p = new PozycjaKalkulacji().ustawDomyslnePklPobytowa();
			this.addDeltPozycjeKalkulacji(p);
			p.setPklTpozId(p.getTpozIdByKodPozycji("DP"));
			p.setPklIlosc(ilosc);
			p.setPklKrId(krId);
		}
	}
	
	public void addDietaRyczaltHotel(String rodzaj, Double ilosc) {
		if(ilosc != 0.0) {
			PozycjaKalkulacji p = new PozycjaKalkulacji().ustawDomyslne();
			this.addDeltPozycjeKalkulacji(p);
			p.setPklTpozId(p.getTpozIdByKodPozycji(rodzaj));
			p.setPklIlosc(ilosc);
			if(this.getDeltWnioskiDelegacji().getWndRodzaj().equals(1L)) {
				if("DH".equals(rodzaj))
				p.setPklStawka(600.0);
				else p.setPklStawka(45.0);
			} 
		}
	}
	
	public void addDietaRyczaltHotel(String rodzaj, Double ilosc, Long krId) {
		if(ilosc != 0.0) {
			PozycjaKalkulacji p = new PozycjaKalkulacji().ustawDomyslne();
			this.addDeltPozycjeKalkulacji(p);
			p.setPklTpozId(p.getTpozIdByKodPozycji(rodzaj));
			p.setPklIlosc(ilosc);
			p.setPklKrId(krId);
		}
	}
	
	public void addRyczaltKom(Double ilosc) {
		if(ilosc != 0.0) {
			PozycjaKalkulacji p = new PozycjaKalkulacji().ustawDomyslne();
			this.addDeltPozycjeKalkulacji(p);
			p.setPklIlosc(ilosc);
			if(this.getDeltWnioskiDelegacji().getWndRodzaj().equals(1L)) 
				p.setPklStawka(6.0);
			p.setPklTpozId(p.getTpozIdByKodPozycji("RK")); 
		}
	}
	
	public void addRyczaltKom(Double ilosc, Long krId) {
		if(ilosc != 0.0) {
			PozycjaKalkulacji p = new PozycjaKalkulacji().ustawDomyslne();
			this.addDeltPozycjeKalkulacji(p);
			p.setPklIlosc(ilosc);
			p.setPklTpozId(p.getTpozIdByKodPozycji("RK")); 
			p.setPklKrId(krId);
		}
	}
	
	public void addRyczaltDojazdowy() {
		PozycjaKalkulacji p = new PozycjaKalkulacji().ustawDomyslne();
		this.addDeltPozycjeKalkulacji(p);
		p.setPklTpozId(p.getTpozIdByKodPozycji("RD"));
		if(this.getDeltWnioskiDelegacji().getWndRodzaj().equals(1L)) 
			p.setPklStawka(6.0);
	}
	
	public void addRyczaltDojazdowy(int ilosc, Long krId) {
		PozycjaKalkulacji p = new PozycjaKalkulacji().ustawDomyslne();
		this.addDeltPozycjeKalkulacji(p);
		p.setPklTpozId(p.getTpozIdByKodPozycji("RD"));
		p.setPklIlosc((double) ilosc);
		p.setPklKrId(krId);
	}
	
	public void addPklInne() {
		PozycjaKalkulacji p = new PozycjaKalkulacji().ustawDomyslne();
		this.addDeltPozycjeKalkulacji(p);
		p.setPklTpozId(p.getTpozIdByKodPozycji("I")); 
	}
	
	//JEDNA METODA NIEZALEŻNIE CZY CZAS SŁUŻBOWY CZY Z PLANU TRASY
	public void generujDiety(String kodPkl) {
		usunPozycjeKalkulacjiWgKodu(kodPkl);
		// jeżeli delegacja krajowa to liczymy według zasad dla krajowej
		if (this.getDeltWnioskiDelegacji().getWndRodzaj().equals(1L)) {
			generujDietyDelKraj(kodPkl);
		} else {
			generujDietyDelZagr(kodPkl);
		}
	}

	private void generujDietyDelKraj(String kodPkl) {
		Double ilegodzin = 0.0;
		int ileOsob = 1;
		// uwzględnienie w wyliczeniach możliwości wyjazdu więcej niż jednej osoby w ramach tej samej delegacji
		if (this.getDeltWnioskiDelegacji().getDeltOsobyDodatkowes().size() > 0)
			ileOsob = this.getDeltWnioskiDelegacji().getDeltOsobyDodatkowes().size() + 1;
		// OBLICZANIE CZASU TRWANIA DELEGACJI BĄDZ CZYNNOŚCI SŁUŻBOWYCH
		if (this.getDelCzynnosciSluzbowe().size() > 0)
			ilegodzin = this.getDelCzynnosciSluzbowe().stream()
					.filter(czs -> czs.getCzynCzas() != null)
					.collect(Collectors.summingDouble(czs -> czs.getCzynCzas()));
		else
			ilegodzin = this.zwrocCzasOdDoPlanTrasy();
		if (ilegodzin > 0.0) {
			switch (kodPkl) {
			case "DP":
				addDietyPobytowe(ileDietPobytowych(ilegodzin) * ileOsob);
				break;
			case "RK":
				addRyczaltKom(ileRyczaltowKom(ilegodzin) * ileOsob);
				break;
			case "DH":
				usunPozycjeKalkulacjiWgKodu("RH"); // tutaj konieczne
				addDietaRyczaltHotel("DH", ileNoclegow(ilegodzin) * ileOsob);
				break;
			case "RH":
				usunPozycjeKalkulacjiWgKodu("DH"); // tutaj konieczne
				addDietaRyczaltHotel("RH", ileNoclegow(ilegodzin) * ileOsob);
				break;
			}
		}
	}
	
	public double ileDietPobytowych(Double ilegodzin) {
		double ret = 0;
		double dni = Math.floor((ilegodzin / 24));
		double x = ilegodzin - dni * 24;
		ret = dni;

		//DLA DELEGACJI KRAJOWEJ
		if (this.getDeltWnioskiDelegacji().getWndRodzaj().equals(1L)) {
			boolean delegacjaPowyzejDnia = this.sprawdzCzyDelPowyzejDnia();
			if (delegacjaPowyzejDnia) {
				if (x > 0 && x <= 8.000)
					ret += 0.5;
				if (x > 8)
					ret += 1.0;
			} else {
				if (x >= 12.000)
					ret += 1.0;
				else if (x >= 8.000)
					ret += 0.5;
			}
		} else { //DLA DELEGACJI ZAGRANICZNEJ INNY SPOSÓB WYLICZANIA DIET
			if (x > 12.000)
				ret += 1.0000;
			else if (x >= 8.000)
				ret += 0.5000;
			else if (x > 0.000)
				ret += 0.3333;
		}
		return ret;
	}
	
	public double ileRyczaltowKom(Double ilegodzin) {
		double ret = 0;
		double dni = Math.floor((ilegodzin / 24));
		double x = ilegodzin - dni * 24;

		ret = dni;
		if (x > 0)
			ret += 1.0000;
		return ret;
	}
	
	public double ileNoclegow(Double ilegodzin) {
		double dni = 0.0;
		if(ilegodzin != null)
			dni = Math.floor((ilegodzin / 24));
		return dni;
	}
	
	public double zwrocCzasOdDoPlanTrasy() {
		if(this.getDeltTrasies().size()>0) {
			Date dataWyjazd = this.getDeltTrasies().get(0).getTrsDataOd();
			Date dataPowrot = this.getDeltTrasies().get(this.getDeltTrasies().size() - 1).getTrsDataDo();
			return (double) ((dataPowrot.getTime() - dataWyjazd.getTime())/(1000 * 60 * 60));
		} else return 0.0;
	}
	
	public boolean sprawdzCzyDelPowyzejDnia() {
		if(this.getDeltTrasies().size()>0) {
			Date dataWyjazd = this.getDeltTrasies().get(0).getTrsDataOd();
			Date dataPowrot = this.getDeltTrasies().get(this.getDeltTrasies().size() - 1).getTrsDataDo();
			return (double) ((dataPowrot.getTime() - dataWyjazd.getTime())/(1000 * 60 * 60)) > 24.0;
		} else if(this.getDelCzynnosciSluzbowe().size()>0)
			return this.getDelCzynnosciSluzbowe().stream().collect(Collectors.summingDouble(czs -> czs.getCzynCzas()))> 24.0;
		return false;
	}
	
	
	private void usunPozycjeKalkulacjiWgKodu(String kodPozycji) {
		HashSet<PptDelPozycjeKalkulacji> rem = getPozycjeKalkWgKodu(kodPozycji);
		for (PptDelPozycjeKalkulacji p : rem) {
			this.removeDeltPozycjeKalkulacji(p);
		}
	}

	private HashSet<PptDelPozycjeKalkulacji> getPozycjeKalkWgKodu(String kodPozycji) {
		SqlDataSelectionsHandler lw;
		if(this.getDeltWnioskiDelegacji().getWndRodzaj().equals(1L))
			lw = this.getLW("PPL_DEL_TYPY_POZYCJI_K");
		else 
			lw = this.getLW("PPL_DEL_TYPY_POZYCJI_Z");
		
		HashSet<PptDelPozycjeKalkulacji> rem = new HashSet<PptDelPozycjeKalkulacji>();
		for (PptDelPozycjeKalkulacji pkl : this.getDeltPozycjeKalkulacjis()) {
			DataRow r = lw.get(pkl.getPklTpozId());
			if(r != null) {
				if (kodPozycji.equals(r.get("TPOZ_KOD"))){
					rem.add(pkl);
				}
			}
		}
		return rem;
	}
	
	// OBLICZA KOSZT KILOMETRÓWKI NA PODSTAWIE PLANU TRASY
	public void generujKosztyPrzejazdu() {
		usunPozycjeKalkulacjiWgKodu("KT");
		SqlDataSelectionsHandler slownikSrodkiLok = this.getLW("PPL_SLOWNIK", "DEL_SRODEK_LOKOMOCJI");
		
		List<PptDelTrasy> trasy = this.getDeltTrasies().stream()
				.filter(trs -> (trs.getTrsSrodekLokomocji().equals("03") || trs.getTrsSrodekLokomocji().equals("07")) && trs.getTrsIloscKM() != null)
				.collect(Collectors.toList());
		
		if(trasy.size() == 0) {
			User.info("Brak odcinków trasy z środkiem lokomocji : samochód prywatny lub nie podano ilości przebytych kilometrów");
			return;
		}
			
		
		if(trasy.size() > 0) {
			for(PptDelTrasy trs : trasy) {
				PozycjaKalkulacji pkl = new PozycjaKalkulacji().ustawDomyslne();
				this.addDeltPozycjeKalkulacji(pkl);
				pkl.setPklTpozId(pkl.getTpozIdByKodPozycji("KT"));
				pkl.setPklOpis(trs.getTrsMiejscowoscOd()+" - "+trs.getTrsMiejscowoscDo());
				pkl.setPklIlosc(trs.getTrsIloscKM());
				String stawka =(String) slownikSrodkiLok.find(trs.getTrsSrodekLokomocji()).get("WSL_WARTOSC_ZEWN");
				String pojemnosc =(String) slownikSrodkiLok.find(trs.getTrsSrodekLokomocji()).get("WSL_OPIS2");
				if(pojemnosc.contains(">"))
					pkl.setPklPojemnosc(900.01);
				else pkl.setPklPojemnosc(900.00);
				
				pkl.setPklStawka(Double.parseDouble(stawka));
			}
		} else return;
	}
	
	//TYLKO W PRZYPADKU DELEGACJI ZAGRANICZNYCH, LICZY DIETY NA PODSTAWIE PLANU TRASY
	private void generujDietyDelZagr(String kodPkl) {
		if (this.getDeltTrasies().isEmpty() && this.getDelCzynnosciSluzbowe().isEmpty()) {
			User.alert("Nie można wygenerować diet, brak danych w planie trasy / czynnościach służbowych");
			return;
		} else if (!this.getDelCzynnosciSluzbowe().isEmpty())
			this.generujDietyZagrCzynnosciSluzbowe(kodPkl);
		else if(!this.getDeltTrasies().isEmpty())
			this.generujDietyDelZagrPlanTrasy(kodPkl);
	}


	// Tylko w przypadku delegacji zagranicznych, liczy diety na podstawie planu trasy
	public void generujDietyDelZagrPlanTrasy(String kodPkl) {
		int ileOsob = 1;
		// uwzględnienie w wyliczeniach możliwości wyjazdu więcej niż jednej osoby w ramach tej samej delegacji
		if (this.getDeltWnioskiDelegacji().getDeltOsobyDodatkowes().size() > 0)
			ileOsob = this.getDeltWnioskiDelegacji().getDeltOsobyDodatkowes().size() + 1;
		Long krId = null;
		Date dtKrOd = null;
		Date dtKrDo = null;
		int i = 0;

		for (PptDelTrasy t : this.getDeltTrasies()) {
			i++;
			krId = krId == null ?  t.getTrsKrIdDiety() : krId;
			dtKrOd = dtKrOd == null ? t.getTrsDataOd() : dtKrOd;

			if (krId.equals(t.getTrsKrIdDiety()) && i != this.getDeltTrasies().size())
				continue;
			else {
				dtKrDo = i == this.getDeltTrasies().size()? t.getTrsDataDo():t.getTrsDataOd();// jeśli ostatni element
				if ("DP".equals(kodPkl))
					this.addDietyPobytowe(this.ileDietPobytowych(this.zwrocCzasDataOdDo(dtKrOd, dtKrDo)) * ileOsob,krId);

				else if ("RH".equals(kodPkl) || "DH".equals(kodPkl))
					this.addDietaRyczaltHotel(kodPkl, this.ileNoclegow(this.zwrocCzasDataOdDo(dtKrOd, dtKrDo)) * ileOsob, krId);

				else if ("RK".equals(kodPkl))
					this.addRyczaltKom(this.ileRyczaltowKom(this.zwrocCzasDataOdDo(dtKrOd, dtKrDo)) * ileOsob, krId);

				else if ("RD".equals(kodPkl))
					this.addRyczaltDojazdowy(ileOsob, krId);
				// TERAZ PO PRZELICZENIU DIETY TRZEBA ustawić id kolejnego kraju
				dtKrOd = dtKrDo;
				krId = t.getTrsKrIdDiety();
			}
		}
	}
	
	//GENERUJE DIETY DELEGACJI ZAGRANICZNEJ Z CZASU CZYNNOSCI SLUZBOWYCH
	public void generujDietyZagrCzynnosciSluzbowe(String kodPkl) {
		int ileOsob = 1;
		// uwzględnienie w wyliczeniach możliwości wyjazdu więcej niż jednej osoby w ramach tej samej delegacji
		if (this.getDeltWnioskiDelegacji().getDeltOsobyDodatkowes().size() > 0)
			ileOsob = this.getDeltWnioskiDelegacji().getDeltOsobyDodatkowes().size() + 1;
		
		Map<Long, Double> listaKrajCzas = this.getDelCzynnosciSluzbowe().stream()
				.filter(czs -> czs.getCzynCzas() != null && czs.getCzynKrajId() != null)
				.collect(Collectors.groupingBy(cs -> cs.getCzynKrajId(), Collectors.summingDouble(cs -> cs.getCzynCzas())));
		for (Long krajId : listaKrajCzas.keySet()) {
			if ("DP".equals(kodPkl))
				this.addDietyPobytowe(this.ileDietPobytowych(listaKrajCzas.get(krajId)) *ileOsob, krajId);
			
			else if ("RH".equals(kodPkl) || "DH".equals(kodPkl))
				this.addDietaRyczaltHotel(kodPkl, this.ileNoclegow(listaKrajCzas.get(krajId)) *ileOsob, krajId);
			
			else if ("RK".equals(kodPkl))
				this.addRyczaltKom(this.ileRyczaltowKom(listaKrajCzas.get(krajId)) *ileOsob, krajId);
			
			else if ("RD".equals(kodPkl))
				this.addRyczaltDojazdowy(ileOsob, krajId);
		}
	}
	
	//TODO ZASTANOWIĆ SIĘ CZY NIE DO UTILSÓW JAKIŚ
	public Double zwrocCzasDataOdDo(Date dataOd, Date dataDo) {
		if (dataOd != null && dataDo != null)
			return (double) ((dataDo.getTime() - dataOd.getTime()) / (1000 * 60 * 60));
		else
			return 0.0;
	}
	
	//Póki co zrobiony pod del kraj
	public boolean walidujPlanTrasy() {
		
//		Date wndDataWyjazdu = this.getDeltWnioskiDelegacji().getWndDataWyjazdu();
//		Date wndDataPowrotu = this.getDeltWnioskiDelegacji().getWndDataPowrotu();
		
		if(this.getDeltTrasies() == null || this.getDeltTrasies().isEmpty()) {
			return true;
		}
		Date trsDataDoPoprzedniWiersz = null;
		
		if(this.getDeltTrasies().size()>0 && this.getDeltTrasies().size() < 2) {
			User.alert("Plan trasy musi zawierać co najmniej dwie pozycje");
			return false;
		}
		Date trsDataOdPierwsza = this.getDeltTrasies().get(0).getTrsDataOd();
		Date trsDataDoOstatnia = this.getDeltTrasies().get(this.getDeltTrasies().size()-1).getTrsDataDo();
		
		if(trsDataOdPierwsza != null && trsDataDoOstatnia != null){
				
//			if(!(
//				(trsDataOdPierwsza.compareTo(wndDataWyjazdu) >=0 && trsDataOdPierwsza.before(wndDataPowrotu)) 
//				&& 
//				(trsDataDoOstatnia.compareTo(wndDataWyjazdu) >=0 && trsDataDoOstatnia.before(wndDataPowrotu))
//				)) {
//						User.alert("Daty na planie trasy muszą się zawierać w wnioskowanym terminie trwania delegacji");
//						return false;
//					}
		} else {
			User.alert("Plan trasy - wprowadzone dane nie mogą być puste");
			return false;
		}
			
		if(!(trsDataOdPierwsza.before(trsDataDoOstatnia))) {
			User.alert("Data powrotu z delegacji nie może nastąpić przed datą wyjazdu, popraw dane");
			return false;
		}
			
		for( PptDelTrasy trs : this.getDeltTrasies()) {
//			może nie będą chcieli tej reguły 
			if(nz(trs.getTrsMiejscowoscOd()).isEmpty() || nz(trs.getTrsMiejscowoscDo()).isEmpty()) {
				User.alert("Należy uzupełnić plan trasy o brakujące dane w kolumnie miejscowość");
				return false;
			}
			
			if(trs.getTrsDataOd() != null && trs.getTrsDataDo() != null) {
					
				if(trsDataDoPoprzedniWiersz != null && trs.getTrsDataOd().before(trsDataDoPoprzedniWiersz)) {
					User.alert("Data wyjazdu z poprzedzającego wiersza w planie trasy nie może być późniejsza niż daty w kolejnych wierszach");
					return false;
				}
				
				trsDataDoPoprzedniWiersz = trs.getTrsDataDo();
				
				if(trs.getTrsDataDo().before(trs.getTrsDataOd())){
					User.alert("Niepoprawna data na planie trasy : data wyjazdu nie może nastąpić przed datą przyjazdu na danym odcinku planu trasy");
					return false;
				} 
			} 
		}
		return true;
	}
	
	//zwraca true jeśli nie ma zaliczek z pustymi numerami kont a wybraną formą płatności jako przelew
	public boolean walidujZaliczki() {
		List<PptDelZaliczki> zaliczkiDoPoprawy = this.getDelZaliczki().stream()
				.filter(z ->
						nz(z.getDkzFormaWyplaty()).isEmpty()
						|| eq("B",z.getDkzFormaWyplaty()) && z.getDkzNrKonta().isEmpty()
						|| nz(z.getDkzKwota()) == 0.0
						|| nz(z.getDkzWalId()) == 0L 
					)
				.collect(Collectors.toList());
		
		if(zaliczkiDoPoprawy != null && zaliczkiDoPoprawy.size()>0)
			return false;
		
		
		return true;
	}

		
		
}
