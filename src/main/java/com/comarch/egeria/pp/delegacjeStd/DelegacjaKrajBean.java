package com.comarch.egeria.pp.delegacjeStd;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.delegacjeStd.model.DelegacjaKraj;
import com.comarch.egeria.pp.delegacjeStd.model.PptDelWnioskiDelegacji;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@ManagedBean
@Named
@Scope("view")
public class DelegacjaKrajBean extends DelegacjaBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	
	private DelegacjaKraj delegacja = new DelegacjaKraj();
	private String rodzajNoclegu;
	
	@PostConstruct
	public void init() {
		
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		if (id==null) {
			this.setDelegacja( new DelegacjaKraj().ustawDomyslne() );
		}else {
			DelegacjaKraj d = (DelegacjaKraj) HibernateContext.get(DelegacjaKraj.class, (Serializable) id);
			this.setDelegacja(d);
		}
		
		this.getSlownik("DEL_SRODEK_LOKOMOCJI", false, false);
		this.getSlownik("DEL_FORMY_WYPLATY", false, false);
		this.getSlownik("DEL_OPLACAJACY_PKL", false, false);
		this.getSlownik("DEL_PROJEKTY_PWSZ", false, false);
		
		this.setRodzajNoclegu("DH");
	}


	public DelegacjaKraj getDelegacja() {
		return delegacja;
	}

	public void setDelegacja(DelegacjaKraj delegacja) {
		this.delegacja = delegacja;
	}

	public String getRodzajNoclegu() {
		return rodzajNoclegu;
	}

	public void setRodzajNoclegu(String rodzajNoclegu) {
		this.rodzajNoclegu = rodzajNoclegu;
	}
	
}
