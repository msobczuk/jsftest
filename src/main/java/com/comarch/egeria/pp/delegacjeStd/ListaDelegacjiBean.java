package com.comarch.egeria.pp.delegacjeStd;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

@ManagedBean // JBossTools
@Named
@Scope("view")

public class ListaDelegacjiBean extends SqlBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();


	public void nowyWniosek(DataRow r) {

		String typ = r.getIdAsString();
		String url = mapujTypWnioskuToUrl(typ);

		try {
			if (url!=null) {
				System.out.println(typ + " -> " + url);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", null);
				FacesContext.getCurrentInstance().getExternalContext().redirect(url);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}


	public void edytujWniosek(DataRow row) {

		if (row==null) return;

		String typ = (String) row.get("wnd_rodzaj_opis");
		String url = mapujTypWnioskuToUrl(typ);

		try {
			if (url!=null) {
				System.out.println(typ + " -> " + url);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", row);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", row.getIdAsLong());
				FacesContext.getCurrentInstance().getExternalContext().redirect(url);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}



	private String mapujTypWnioskuToUrl(String typ) {
		if (("Zagraniczna").equals(typ)) 
			return "DelZagr";
		else return "DelKraj";
	}



}
