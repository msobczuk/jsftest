package com.comarch.egeria.pp.delegacjeStd.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.comarch.egeria.pp.data.model.ModelBase;


/**
 * The persistent class for the PPT_DEL_CELE_DELEGACJI database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_CELE_DELEGACJI", schema="PPADM")
@NamedQuery(name="PptDelCeleDelegacji.findAll", query="SELECT d FROM PptDelCeleDelegacji d")
public class PptDelCeleDelegacji extends DelBase implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	
	
//	public PptDelCeleDelegacji clone(){
//		PptDelCeleDelegacji cel = new PptDelCeleDelegacji();
//		
//		cel.setCdelId(cdelId); //!!! Hibernate?
//		cel.setCdelAudytDm(cdelAudytDm);
//		cel.setCdelAudytDt(cdelAudytDt);
//		cel.setCdelAudytKm(cdelAudytKm);
//		cel.setCdelAudytLm(cdelAudytLm);
//		cel.setCdelAudytUm(cdelAudytUm);
//		cel.setCdelAudytUt(cdelAudytUt);
//		cel.setCdelKrId(cdelKrId);
//		cel.setCdelMiejscowosc(cdelMiejscowosc);
//		cel.setDeltWnioskiDelegacji(null);//to jest rodzic!!!
//		
//		return cel;
//	}
	

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_CELE_DELEGACJI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_CELE_DELEGACJI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_CELE_DELEGACJI")	
	@Column(name="CDEL_ID")
	private long cdelId;

	@Temporal(TemporalType.DATE)
	@Column(name="CDEL_AUDYT_DM")
	private Date cdelAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="CDEL_AUDYT_DT")
	private Date cdelAudytDt;

	@Column(name="CDEL_AUDYT_KM")
	private String cdelAudytKm;

//	@Column(name="CDEL_AUDYT_KT")
//	private String cdelAudytKt;

	@Column(name="CDEL_AUDYT_LM")
	private String cdelAudytLm;

	@Column(name="CDEL_AUDYT_UM")
	private String cdelAudytUm;

	@Column(name="CDEL_AUDYT_UT")
	private String cdelAudytUt;

/*	@Column(name="CDEL_CEL")
	private String cdelCel;*/

/*	@Column(name="CDEL_KATEGORIA")
	private String cdelKategoria;*/

/*	@Column(name="CDEL_KL_ID")
	private Long cdelKlId;*/

/*	@Temporal(TemporalType.DATE)
	@Column(name="CDEL_KONFERENCJA_DO")
	private Date cdelKonferencjaDo;

	@Temporal(TemporalType.DATE)
	@Column(name="CDEL_KONFERENCJA_OD")
	private Date cdelKonferencjaOd;*/

	@Column(name="CDEL_KR_ID")
	private Long cdelKrId = 1L;
	
//	@Column(name="CDEL_LP")
//	private Long cdelLp;

	@Column(name="CDEL_MIEJSCOWOSC")
	private String cdelMiejscowosc;
	
	@Column(name="CDEL_MIEJSCOWOSC_OD")
	private String cdelMiejscowoscOd;
	
	@Column(name="CDEL_MIEJSCOWOSC_ZAKONCZENIA")
	private String cdelMiejscowoscZakonczenia;

/*	@Column(name="CDEL_OPIS")
	private String cdelOpis;

	@Column(name="CDEL_OPIS_CELU")
	private String cdelOpisCelu;*/

	//bi-directional many-to-one association to DeltWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="CDEL_WND_ID")
	private PptDelWnioskiDelegacji deltWnioskiDelegacji;

	public PptDelCeleDelegacji() {
	}

	public long getCdelId() {
		this.onRXGet("cdelId");
		return this.cdelId;
	}

	public void setCdelId(long cdelId) {
		this.onRXSet("cdelId", this.cdelId, cdelId);
		this.cdelId = cdelId;
	}

	public Date getCdelAudytDm() {
		this.onRXGet("cdelAudytDm");
		return this.cdelAudytDm;
	}

	public void setCdelAudytDm(Date cdelAudytDm) {
		this.onRXSet("cdelAudytDm", this.cdelAudytDm, cdelAudytDm);
		this.cdelAudytDm = cdelAudytDm;
	}

	public Date getCdelAudytDt() {
		this.onRXGet("cdelAudytDt");
		return this.cdelAudytDt;
	}

	public void setCdelAudytDt(Date cdelAudytDt) {
		this.onRXSet("cdelAudytDt", this.cdelAudytDt, cdelAudytDt);
		this.cdelAudytDt = cdelAudytDt;
	}

	public String getCdelAudytKm() {
		this.onRXGet("cdelAudytKm");
		return this.cdelAudytKm;
	}

	public void setCdelAudytKm(String cdelAudytKm) {
		this.onRXSet("cdelAudytKm", this.cdelAudytKm, cdelAudytKm);
		this.cdelAudytKm = cdelAudytKm;
	}

//	public String getCdelAudytKt() {
//		return this.cdelAudytKt;
//	}
//
//	public void setCdelAudytKt(String cdelAudytKt) {
//		this.cdelAudytKt = cdelAudytKt;
//	}

	public String getCdelAudytLm() {
		this.onRXGet("cdelAudytLm");
		return this.cdelAudytLm;
	}

	public void setCdelAudytLm(String cdelAudytLm) {
		this.onRXSet("cdelAudytLm", this.cdelAudytLm, cdelAudytLm);
		this.cdelAudytLm = cdelAudytLm;
	}

	public String getCdelAudytUm() {
		this.onRXGet("cdelAudytUm");
		return this.cdelAudytUm;
	}

	public void setCdelAudytUm(String cdelAudytUm) {
		this.onRXSet("cdelAudytUm", this.cdelAudytUm, cdelAudytUm);
		this.cdelAudytUm = cdelAudytUm;
	}

	public String getCdelAudytUt() {
		this.onRXGet("cdelAudytUt");
		return this.cdelAudytUt;
	}

	public void setCdelAudytUt(String cdelAudytUt) {
		this.onRXSet("cdelAudytUt", this.cdelAudytUt, cdelAudytUt);
		this.cdelAudytUt = cdelAudytUt;
	}

//	public String getCdelCel() {
//		return this.cdelCel;
//	}
//
//	public void setCdelCel(String cdelCel) {
//		this.cdelCel = cdelCel;
//	}
//
//	public String getCdelKategoria() {
//		return this.cdelKategoria;
//	}
//
//	public void setCdelKategoria(String cdelKategoria) {
//		this.cdelKategoria = cdelKategoria;
//	}
//
//	public Long getCdelKlId() {
//		return this.cdelKlId;
//	}
//
//	public void setCdelKlId(Long cdelKlId) {
//		this.cdelKlId = cdelKlId;
//	}
//
//	public Date getCdelKonferencjaDo() {
//		return this.cdelKonferencjaDo;
//	}
//
//	public void setCdelKonferencjaDo(Date cdelKonferencjaDo) {
//		this.cdelKonferencjaDo = cdelKonferencjaDo;
//	}
//
//	public Date getCdelKonferencjaOd() {
//		return this.cdelKonferencjaOd;
//	}
//
//	public void setCdelKonferencjaOd(Date cdelKonferencjaOd) {
//		this.cdelKonferencjaOd = cdelKonferencjaOd;
//	}

	public Long getCdelKrId() {
		this.onRXGet("cdelKrId");
		return this.cdelKrId;
	}

	public void setCdelKrId(Long cdelKrId) {
//		System.out.println(this.cdelMiejscowosc + " / " + cdelKrId);
		this.onRXSet("cdelKrId", this.cdelKrId, cdelKrId);
		this.cdelKrId = cdelKrId;
	}

//	public Long getCdelLp() {
//		return this.cdelLp;
//	}

//	public void setCdelLp(Long cdelLp) {
//		this.cdelLp = cdelLp;
//	}

	public String getCdelMiejscowosc() {
		this.onRXGet("cdelMiejscowosc");
		return this.cdelMiejscowosc;
	}

	public void setCdelMiejscowosc(String cdelMiejscowosc) {
		this.onRXSet("cdelMiejscowosc", this.cdelMiejscowosc, cdelMiejscowosc);
		this.cdelMiejscowosc = cdelMiejscowosc;
	}

//	public String getCdelOpis() {
//		return this.cdelOpis;
//	}
//
//	public void setCdelOpis(String cdelOpis) {
//		this.cdelOpis = cdelOpis;
//	}
//
//	public String getCdelOpisCelu() {
//		return this.cdelOpisCelu;
//	}
//
//	public void setCdelOpisCelu(String cdelOpisCelu) {
//		this.cdelOpisCelu = cdelOpisCelu;
//	}

	public PptDelWnioskiDelegacji getDeltWnioskiDelegacji() {
		this.onRXGet("deltWnioskiDelegacji");
		return this.deltWnioskiDelegacji;
	}

	public void setDeltWnioskiDelegacji(PptDelWnioskiDelegacji deltWnioskiDelegacji) {
		this.onRXSet("deltWnioskiDelegacji", this.deltWnioskiDelegacji, deltWnioskiDelegacji);
		this.deltWnioskiDelegacji = deltWnioskiDelegacji;
	}

	public String getCdelMiejscowoscOd() {
		return cdelMiejscowoscOd;
	}

	public void setCdelMiejscowoscOd(String cdelMiejscowoscOd) {
		this.cdelMiejscowoscOd = cdelMiejscowoscOd;
	}

	public String getCdelMiejscowoscZakonczenia() {
		return cdelMiejscowoscZakonczenia;
	}

	public void setCdelMiejscowoscZakonczenia(String cdelMiejscowoscZakonczenia) {
		this.cdelMiejscowoscZakonczenia = cdelMiejscowoscZakonczenia;
	}

}
