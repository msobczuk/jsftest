package com.comarch.egeria.pp.delegacjeStd.model;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.model.ModelBase;

import java.io.Serializable;


//klasa z utilsami dla instancji delgacji

public class DelBase extends ModelBase implements Serializable {



    public String getWalSymbol(Object walId){
        if (walId==null) return null;
        return this.getLW("PPL_SYMBOLE_WALUT").findAndFormat(walId, "%wal_symbol2$s");
    }

    public Long getWalId(String walSymbol){
        if (walSymbol==null || walSymbol.isEmpty()) return null;

        DataRow dr = this.getLW("PPL_SYMBOLE_WALUT").getData()
                .stream()
                .filter(r -> eq(r.get("wal_symbol2"), walSymbol))
                .findFirst()
                .orElse(null);

        return dr != null ? dr.getIdAsLong() : null;
    }


    public String getKrajSymbol(Object krajId){
        return getLW("PPL_WSP_KRAJE").findAndFormat(krajId, "%kr_symbol$s");
    }

    public String getKrajNazwa(Object krajId){
        return getLW("PPL_WSP_KRAJE").findAndFormat(krajId, "%kr_nazwa$s");
    }

}
