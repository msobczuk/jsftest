package com.comarch.egeria.pp.delegacjeStd;

import com.comarch.egeria.pp.data.SqlBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import java.io.Serializable;

@ManagedBean // JBossTools
@Named
@Scope("view")

public class DelegacjaBean extends SqlBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	
	@PostConstruct
	public void init() {
	}
	


}
