package com.comarch.egeria.pp.delegacjeStd.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.comarch.egeria.pp.data.model.ModelBase;

@Entity
@Table(name="PPT_DEL_OSOBY_DODATKOWE")
@NamedQuery(name="PptDelOsobyDodatkowe.findAll", query="SELECT p FROM PptDelOsobyDodatkowe p")
public class PptDelOsobyDodatkowe extends DelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_OSOBY_DODATKOWE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_OSOBY_DODATKOWE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_OSOBY_DODATKOWE")	
	@Column(name="ODO_ID")
	private long odoId;

	@Temporal(TemporalType.DATE)
	@Column(name="ODO_AUDYT_DM")
	private Date odoAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="ODO_AUDYT_DT")
	private Date odoAudytDt;

	@Column(name="ODO_AUDYT_LM")
	private String odoAudytLm;

	@Column(name="ODO_AUDYT_UM")
	private String odoAudytUm;

	@Column(name="ODO_AUDYT_UT")
	private String odoAudytUt;

	@Column(name="ODO_KL_KOD")
	private long odoKlKod;

	//bi-directional many-to-one association to PptDelWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="ODO_WND_ID")
	private PptDelWnioskiDelegacji deltWnioskiDelegacji;

	public PptDelOsobyDodatkowe() {
	}

	public long getOdoId() {
		this.onRXGet("odoId");
		return this.odoId;
	}

	public void setOdoId(long odoId) {
		this.onRXSet("odoId", this.odoId, odoId);
		this.odoId = odoId;
	}

	public Object getOdoAudytDm() {
		this.onRXGet("odoAudytDm");
		return this.odoAudytDm;
	}

	public void setOdoAudytDm(Date odoAudytDm) {
		this.onRXSet("odoAudytDm", this.odoAudytDm, odoAudytDm);
		this.odoAudytDm = odoAudytDm;
	}

	public Object getOdoAudytDt() {
		this.onRXGet("odoAudytDt");
		return this.odoAudytDt;
	}

	public void setOdoAudytDt(Date odoAudytDt) {
		this.onRXSet("odoAudytDt", this.odoAudytDt, odoAudytDt);
		this.odoAudytDt = odoAudytDt;
	}

	public String getOdoAudytLm() {
		this.onRXGet("odoAudytLm");
		return this.odoAudytLm;
	}

	public void setOdoAudytLm(String odoAudytLm) {
		this.onRXSet("odoAudytLm", this.odoAudytLm, odoAudytLm);
		this.odoAudytLm = odoAudytLm;
	}

	public String getOdoAudytUm() {
		this.onRXGet("odoAudytUm");
		return this.odoAudytUm;
	}

	public void setOdoAudytUm(String odoAudytUm) {
		this.onRXSet("odoAudytUm", this.odoAudytUm, odoAudytUm);
		this.odoAudytUm = odoAudytUm;
	}

	public String getOdoAudytUt() {
		this.onRXGet("odoAudytUt");
		return this.odoAudytUt;
	}

	public void setOdoAudytUt(String odoAudytUt) {
		this.onRXSet("odoAudytUt", this.odoAudytUt, odoAudytUt);
		this.odoAudytUt = odoAudytUt;
	}

	public long getOdoKlKod() {
		this.onRXGet("odoKlKod");
		return this.odoKlKod;
	}

	public void setOdoKlKod(long odoKlKod) {
		this.onRXSet("odoKlKod", this.odoKlKod, odoKlKod);
		this.odoKlKod = odoKlKod;
	}

	public PptDelWnioskiDelegacji getDeltWnioskiDelegacji() {
		this.onRXGet("deltWnioskiDelegacji");
		return this.deltWnioskiDelegacji;
	}

	public void setDeltWnioskiDelegacji(PptDelWnioskiDelegacji pptDelWnioskiDelegacji) {
		this.onRXSet("deltWnioskiDelegacji", this.deltWnioskiDelegacji, pptDelWnioskiDelegacji);
		this.deltWnioskiDelegacji = pptDelWnioskiDelegacji;
	}

}
