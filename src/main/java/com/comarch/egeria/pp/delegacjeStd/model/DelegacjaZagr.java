package com.comarch.egeria.pp.delegacjeStd.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Entity;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.KursWalutyProjektu;
import com.comarch.egeria.pp.data.KursWalutySredniNBP;
import com.comarch.egeria.web.User;

@Entity
public class DelegacjaZagr extends Delegacja {
	private static final long serialVersionUID = 1L;

	

	public DelegacjaZagr ustawDomyslne() {
		 DelegacjaZagr ret = (DelegacjaZagr) super.ustawDomyslne();
		 ret.setWndRodzaj(2L);
		 //TODO ustawienia typowe dla nowej delegacji KRaj.Zagr
		 return  ret;
	}
	
	
	public void generujPlanTrasy(Kalkulacja kalk) {
		kalk.wyczyscTrasy();
		PptDelCeleDelegacji c0 = new PptDelCeleDelegacji();
		c0.setCdelKrId(1L);
		c0.setCdelMiejscowosc("Tarnów");
		int i = 1;
		for (PptDelCeleDelegacji c : this.getDeltCeleDelegacjis()) {
			//TODO metoda od walidacji?
			if(c.getCdelKrId() == null || c.getCdelMiejscowosc() == null 
					|| c.getDeltWnioskiDelegacji().getDeltSrodkiLokomocjis().get(0).getSlokSrodekLokomocji() == null) {
				User.alert("Część I - trasa i środki transportu - nie wszystkie wymagane pola zostały uzupełnione, popraw dane ");
				return;
			}
			PptDelTrasy t = new PptDelTrasy();
			t.setTrsKrIdOd(c0.getCdelKrId());
			t.setTrsMiejscowoscOd(c0.getCdelMiejscowosc());
			t.setTrsKrIdDo(c.getCdelKrId());
			t.setTrsKrIdDiety(c.getCdelKrId());
			t.setTrsMiejscowoscDo(c.getCdelMiejscowosc());
			t.setTrsSrodekLokomocji(c.getDeltWnioskiDelegacji().getDeltSrodkiLokomocjis().get(0).getSlokSrodekLokomocji());
			if (i == 1)
				t.setTrsDataOd(this.getWndDataWyjazdu());// domyslna I data to data wyjazdu wg wniosku
			kalk.addDeltTrasy(t);
			c0 = c;
			i++;
		}
		// ostatnia trasa - powrot do PL/Tarnów
		PptDelTrasy t = new PptDelTrasy();
		t.setTrsKrIdOd(c0.getCdelKrId());
		t.setTrsMiejscowoscOd(c0.getCdelMiejscowosc());
		t.setTrsKrIdDo(1L);
		t.setTrsKrIdDiety(c0.getCdelKrId());
		t.setTrsMiejscowoscDo("Tarnów");
		t.setTrsDataDo(this.getWndDataPowrotu());
		t.setTrsSrodekLokomocji(c0.getDeltWnioskiDelegacji().getDeltSrodkiLokomocjis().get(0).getSlokSrodekLokomocji());
		kalk.addDeltTrasy(t);
	}
	
	
	public void generujRozliczenie() {
		
		List<PptDelZaliczki> zaliczki = this.getKalkulacjaRozlicz().getDelZaliczki().stream().collect(Collectors.toList());
		
		for(PptDelZaliczki zal : zaliczki)
			this.getKalkulacjaRozlicz().removeDelZaliczki(zal);
		
		if(this.getKalkulacjaRozlicz().getKalDataKursu() == null) 
			this.getKalkulacjaRozlicz().setKalDataKursu(Utils.today());
		
		Map<Long, Double> sumyWalut = this.getKalkulacjaRozlicz().zwrocMapSumyWal();
		
		PptDelZaliczki zaliczkaWniosek = null;
		
		if(eq("T",this.getWndFZaliczka()))
			zaliczkaWniosek = this.getKalkulacjaWniosek().getDelZaliczki().get(0);
		
		if(zaliczkaWniosek != null) {	
			if(!sumyWalut.containsKey(zaliczkaWniosek.getDkzWalId())) {
				PptDelZaliczki zal = new PptDelZaliczki();
				zal.setDkzWalId(zaliczkaWniosek.getDkzWalId());
				zal.setDkzKwota(-zaliczkaWniosek.getDkzKwota());
				double kurs = 1.0;
				if(eq(this.getKursProjektu().getWalId(), zaliczkaWniosek.getDkzWalId()))
					kurs = this.getKursProjektu().getPrzelicznik();
				else
					kurs = this.getKursSredni(zaliczkaWniosek.getDkzWalId(), this.getKalkulacjaRozlicz().getKalDataKursu()).getPrzelicznik();
				
				zal.setDkzKurs(kurs);
				this.getKalkulacjaRozlicz().addDeltZaliczki(zal);
			}
		}
		
		for( Long walId : sumyWalut.keySet()) {
			PptDelZaliczki zal = new PptDelZaliczki();
			zal.setDkzWalId(walId);
			
			if(zaliczkaWniosek != null) {
				
				if(eq(zaliczkaWniosek.getDkzWalId(), walId)) {
					zal.setDkzKwota(sumyWalut.get(walId) - zaliczkaWniosek.getDkzKwota());
				} 
				else if(!eq(zaliczkaWniosek.getDkzWalId(), walId)) {
					zal.setDkzKwota(sumyWalut.get(walId));
				}
			} else {
				zal.setDkzKwota(sumyWalut.get(walId));
			}
			
			double kurs = 1.0;
			if(eq(this.getKursProjektu().getWalId(), walId))
				kurs = this.getKursProjektu().getPrzelicznik();
			else
				kurs = this.getKursSredni(walId, this.getKalkulacjaRozlicz().getKalDataKursu()).getPrzelicznik();
			
			zal.setDkzKurs(kurs);
			this.getKalkulacjaRozlicz().addDeltZaliczki(zal);
		}
		
	}
	
	public double zwrocKwotePlnPklRozliczenia() {
		
		double sumaPln = 0.0;
		double kurs = 1.0;
		
		Map<Long, Double> sumyWalutMap = this.getKalkulacjaRozlicz().zwrocMapSumyWal();
		
		for(long walId : sumyWalutMap.keySet()) {
			
			if(eq(this.getKursProjektu().getWalId(), walId))
				kurs = this.getKursProjektu().getPrzelicznik();
			else
				kurs = this.getKursSredni(walId, this.getKalkulacjaRozlicz().getKalDataKursu()).getPrzelicznik();
			
			double kwotaPlnWaluty = Utils.multiply(sumyWalutMap.get(walId),kurs,2);
			sumaPln+=kwotaPlnWaluty;
		}
		
		
		return sumaPln;
	}
	
	public KursWalutyProjektu getKursProjektu() {
		String kodProjektu = this.getWndProjKod();
		return new KursWalutyProjektu(kodProjektu);
	}
	
	public KursWalutySredniNBP getKursSredni(Long walId, Date naDzien) {
		return new KursWalutySredniNBP(walId, naDzien);
	}
	
	public boolean validate () {
		ArrayList<String> errorList = new ArrayList<>();
		
		List<PptDelCeleDelegacji> listaCeleMiejscowoscNull = this.getDeltCeleDelegacjis().stream()
			.filter(cel -> cel.getCdelMiejscowosc()== null || cel.getCdelMiejscowosc().isEmpty())
			.collect(Collectors.toList());
		
		List<PptDelCeleDelegacji> listaCeleKrajeNull = this.getDeltCeleDelegacjis().stream()
				.filter(cel -> cel.getCdelKrId()== null)
				.collect(Collectors.toList());
		
		if(listaCeleKrajeNull.size()>0)
			errorList.add("Cele delegacji : Należy wskazać kraj");
		
		if(listaCeleMiejscowoscNull.size()>0)
			errorList.add("Cele delegacji : Miejscowosć nie może być pusta");
		
		if(!this.getKalkulacjaRozlicz().walidujPlanTrasy()) {
			errorList.add("Błędne dane w planie trasy");
		}
		
		for (String er :errorList) {
			User.alert(er);
		}
		
		return super.validate() && errorList.isEmpty();
	}
	
	public void zapisz () {
		super.zapisz();
		if(this.getWndNumer() != null)
			Utils.setFormName("Delegacja zagraniczna nr: " + this.getWndNumer());
	}
	
	
}
