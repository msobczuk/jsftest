package com.comarch.egeria.pp.delegacjeStd.model;

import static com.comarch.egeria.Utils.nz;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Entity;

import com.comarch.egeria.Utils;

@Entity
public class KalkulacjaRozlicz extends Kalkulacja {
	private static final long serialVersionUID = 1L;
	
	
	public KalkulacjaRozlicz() {
		
	}
	
	public KalkulacjaRozlicz ustawDomyslne() {
		
		this.setKalRodzaj("ROZ");
		
		return this;
	}
	
	//DEL KRAJ
	public double zwrocKwoteDoRozliczen() {
		double ret = 0.0;
		
		Double sumaKwotPkl = this.getPozycjeKalkulacji().stream()
				.filter(pkl -> pkl.getPklKwota() != null && pkl.getPklKwota()>0.0 && eq("01",pkl.getPklOplacajacy()))
				.collect(Collectors.summingDouble(pkl -> pkl.getPklKwota()));
		if(sumaKwotPkl > 0.0)
			ret = sumaKwotPkl;
		
		return ret;
	}
	
	public Map<Long, Double> zwrocMapSumyWal() {
		Map<Long, Double> ret = this.getPozycjeKalkulacji().stream()
				.filter(p -> eq("01",p.getPklOplacajacy()))
				.collect(Collectors.groupingBy(p -> nz(p.getPklWalId()), Collectors.summingDouble(p -> p.getPklKwota())));
		return ret;
	}
	
//	zwraca kwotę w pln jedynie dla wypłaconych zaliczek na rozliczeniu z pominięciem kwot ujemnych, kwota taka sama jak w ZOB/ZOBE w EG
	public double zwrocSumePlnDkz() {
		if(!(this.getDelZaliczki().size()>0))
			return 0.0;
		else {
			return this.getDelZaliczki().stream()
				.filter(z -> z.getDkzKwota() > 0.0)
				.collect(Collectors.summingDouble(z -> Utils.multiply(z.getDkzKwota(),z.getDkzKurs(),2)));
		}
	}
	
	public boolean blokujSamPryw() {
		
		if(this.getDeltTrasies() != null && !this.getDeltTrasies().isEmpty()) {
			List<PptDelTrasy> trasy = this.getDeltTrasies().stream()
					.filter(t -> eq(t.getTrsSrodekLokomocji(), "03") || eq(t.getTrsSrodekLokomocji(), "07")).collect(Collectors.toList());
			
			if(trasy != null && trasy.size()>0)
				return false;
		}
		
		return true;
	}
	

	
}
