package com.comarch.egeria.pp.delegacjeStd.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;


/**
 * The persistent class for the PPT_DEL_TRASY database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_TRASY", schema="PPADM")
@NamedQuery(name="PptDelTrasy.findAll", query="SELECT d FROM PptDelTrasy d")
public class PptDelTrasy extends DelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_TRASY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_TRASY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_TRASY")	
	@Column(name="TRS_ID")
	private long trsId;

	@Temporal(TemporalType.DATE)
	@Column(name="TRS_AUDYT_DM")
	private Date trsAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="TRS_AUDYT_DT")
	private Date trsAudytDt;

	@Column(name="TRS_AUDYT_KM")
	private String trsAudytKm;

//	@Column(name="TRS_AUDYT_KT")
//	private String trsAudytKt;

	@Column(name="TRS_AUDYT_LM")
	private String trsAudytLm;

	@Column(name="TRS_AUDYT_UM")
	private String trsAudytUm;

	@Column(name="TRS_AUDYT_UT")
	private String trsAudytUt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRS_DATA_DO", columnDefinition="DATE")
	private Date trsDataDo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRS_DATA_OD", columnDefinition="DATE")
	private Date trsDataOd;

	@Column(name="TRS_KR_ID_DO")
	private Long trsKrIdDo = 1L;

	@Column(name="TRS_KR_ID_OD")
	private Long trsKrIdOd = 1L;

//	@Column(name="TRS_LP")
//	private Long trsLp;

	@Column(name="TRS_MIEJSCOWOSC_DO")
	private String trsMiejscowoscDo;

	@Column(name="TRS_MIEJSCOWOSC_OD")
	private String trsMiejscowoscOd;

//	@Column(name="TRS_ODLEGLOSC")
//	private Double trsOdleglosc;

	@Column(name="TRS_OPIS")
	private String trsOpis;

	@Column(name="TRS_SRODEK_LOKOMOCJI")
	private String trsSrodekLokomocji;
	
	@Column(name="TRS_KR_ID_DIETY")
	private Long trsKrIdDiety;
	
	//bi-directional many-to-one association to DeltRozliczeniaDelegacji
//	@ManyToOne(cascade={CascadeType.ALL}) !!!!!!!!!nie uzywac kaskady w kierunku rodzica
	@ManyToOne
	@JoinColumn(name="TRS_KAL_ID")
	private PptDelKalkulacje deltKalkulacje;
	
	@Column(name="TRS_KOSZT_BILETU")
	private Double trsKosztBiletu;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRS_TERMIN_WYKUPU_BILETU")
	private Date trsTerminWykupuBiletu;
	
	@Column(name="TRS_FAKTURA_VAT_NA_BILET")
	private String trsFakturaVatNaBilet;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRS_TERMIN_PLATNOSCI_ZA_BILET")
	private Date trsTerminPlatnosciZaBilet;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRS_CZYNNOSCI_SLUZBOWE_OD", columnDefinition="DATE")
	private Date trsCzynnosciSluzboweOd;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRS_CZYNNOSCI_SLUZBOWE_DO", columnDefinition="DATE")
	private Date trsCzynnosciSluzboweDo;
	
	@Column(name="TRS_ILOSC_KM")
	private Double trsIloscKM;
	
	//bi-directional many-to-one association to DeltTrasy
	@OneToMany(mappedBy="deltTrasy", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("PKL_ID ASC")
	private List<PptDelPozycjeKalkulacji> deltPozycjeKalkulacji = new ArrayList<>();

	public PptDelTrasy() {
	}
	
//	public PptDelTrasy clone(){//shallow copy
//		
//		PptDelTrasy tr = new PptDelTrasy();
//		
//		tr.setTrsId(0L);
//		tr.setTrsDataDo(trsDataDo);
//		tr.setTrsDataOd(trsDataOd);
//		tr.setTrsKrIdDo(trsKrIdDo);
//		tr.setTrsKrIdOd(trsKrIdOd);
//		tr.setTrsMiejscowoscDo(trsMiejscowoscDo);
//		tr.setTrsMiejscowoscOd(trsMiejscowoscOd);
//		tr.setTrsOpis(trsOpis);
//		tr.setTrsSrodekLokomocji(trsSrodekLokomocji);
//		tr.setTrsKrIdDiety(trsKrIdDiety);
//		
//		if(trsKosztBiletu != null) {
//			tr.setTrsKosztBiletu(trsKosztBiletu);
//		}
//
//		if(trsTerminWykupuBiletu != null) {
//			tr.setTrsTerminWykupuBiletu(trsTerminWykupuBiletu);
//		}
//		
//		if(trsFakturaVatNaBilet != null) {
//			tr.setTrsFakturaVatNaBilet(trsFakturaVatNaBilet);
//		}
//		
//		if(trsTerminPlatnosciZaBilet != null) {
//			tr.setTrsTerminPlatnosciZaBilet(trsTerminPlatnosciZaBilet);
//		}
//		
//		return tr;
//		
//	}
	

	public long getTrsId() {
		this.onRXGet("trsId");
		return this.trsId;
	}

	public void setTrsId(long trsId) {
		this.onRXSet("trsId", this.trsId, trsId);
		this.trsId = trsId;
	}

	public Date getTrsAudytDm() {
		this.onRXGet("trsAudytDm");
		return this.trsAudytDm;
	}

	public void setTrsAudytDm(Date trsAudytDm) {
		this.onRXSet("trsAudytDm", this.trsAudytDm, trsAudytDm);
		this.trsAudytDm = trsAudytDm;
	}

	public Date getTrsAudytDt() {
		this.onRXGet("trsAudytDt");
		return this.trsAudytDt;
	}

	public void setTrsAudytDt(Date trsAudytDt) {
		this.onRXSet("trsAudytDt", this.trsAudytDt, trsAudytDt);
		this.trsAudytDt = trsAudytDt;
	}

	public String getTrsAudytKm() {
		this.onRXGet("trsAudytKm");
		return this.trsAudytKm;
	}

	public void setTrsAudytKm(String trsAudytKm) {
		this.onRXSet("trsAudytKm", this.trsAudytKm, trsAudytKm);
		this.trsAudytKm = trsAudytKm;
	}

//	public String getTrsAudytKt() {
//		return this.trsAudytKt;
//	}
//
//	public void setTrsAudytKt(String trsAudytKt) {
//		this.trsAudytKt = trsAudytKt;
//	}

	public String getTrsAudytLm() {
		this.onRXGet("trsAudytLm");
		return this.trsAudytLm;
	}

	public void setTrsAudytLm(String trsAudytLm) {
		this.onRXSet("trsAudytLm", this.trsAudytLm, trsAudytLm);
		this.trsAudytLm = trsAudytLm;
	}

	public String getTrsAudytUm() {
		this.onRXGet("trsAudytUm");
		return this.trsAudytUm;
	}

	public void setTrsAudytUm(String trsAudytUm) {
		this.onRXSet("trsAudytUm", this.trsAudytUm, trsAudytUm);
		this.trsAudytUm = trsAudytUm;
	}

	public String getTrsAudytUt() {
		this.onRXGet("trsAudytUt");
		return this.trsAudytUt;
	}

	public void setTrsAudytUt(String trsAudytUt) {
		this.onRXSet("trsAudytUt", this.trsAudytUt, trsAudytUt);
		this.trsAudytUt = trsAudytUt;
	}

	public Date getTrsDataDo() {
		this.onRXGet("trsDataDo");
		return this.trsDataDo;
	}

	public void setTrsDataDo(Date trsDataDo) {
		this.onRXSet("trsDataDo", this.trsDataDo, trsDataDo);
		this.trsDataDo = trsDataDo;
	}

	public Date getTrsDataOd() {
		this.onRXGet("trsDataOd");
		return this.trsDataOd;
	}

	public void setTrsDataOd(Date trsDataOd) {
		this.onRXSet("trsDataOd", this.trsDataOd, trsDataOd);
		this.trsDataOd = trsDataOd;
	}

	public Long getTrsKrIdDo() {
		this.onRXGet("trsKrIdDo");
		return this.trsKrIdDo;
	}

	public void setTrsKrIdDo(Long trsKrIdDo) {
		this.onRXSet("trsKrIdDo", this.trsKrIdDo, trsKrIdDo);
		this.trsKrIdDo = trsKrIdDo;
	}

	public Long getTrsKrIdOd() {
		this.onRXGet("trsKrIdOd");
		return this.trsKrIdOd;
	}

	public void setTrsKrIdOd(Long trsKrIdOd) {
		this.onRXSet("trsKrIdOd", this.trsKrIdOd, trsKrIdOd);
		this.trsKrIdOd = trsKrIdOd;
	}

//	public Long getTrsLp() {
//		return this.trsLp;
//	}

//	public void setTrsLp(Long trsLp) {
//		this.trsLp = trsLp;
//	}

	public String getTrsMiejscowoscDo() {
		this.onRXGet("trsMiejscowoscDo");
		return this.trsMiejscowoscDo;
	}

	public void setTrsMiejscowoscDo(String trsMiejscowoscDo) {
		this.onRXSet("trsMiejscowoscDo", this.trsMiejscowoscDo, trsMiejscowoscDo);
		this.trsMiejscowoscDo = trsMiejscowoscDo;
	}

	public String getTrsMiejscowoscOd() {
		this.onRXGet("trsMiejscowoscOd");
		return this.trsMiejscowoscOd;
	}

	public void setTrsMiejscowoscOd(String trsMiejscowoscOd) {
		this.onRXSet("trsMiejscowoscOd", this.trsMiejscowoscOd, trsMiejscowoscOd);
		this.trsMiejscowoscOd = trsMiejscowoscOd;
	}

//	public Double getTrsOdleglosc() {
//		return this.trsOdleglosc;
//	}
//
//	public void setTrsOdleglosc(Double trsOdleglosc) {
//		this.trsOdleglosc = trsOdleglosc;
//	}

	public String getTrsOpis() {
		this.onRXGet("trsOpis");
		return this.trsOpis;
	}

	public void setTrsOpis(String trsOpis) {
		this.onRXSet("trsOpis", this.trsOpis, trsOpis);
		this.trsOpis = trsOpis;
	}

	public PptDelKalkulacje getDeltKalkulacje() {
		this.onRXGet("deltKalkulacje");
		return this.deltKalkulacje;
	}

	public void setDeltKalkulacje(PptDelKalkulacje deltKalkulacje) {
		this.onRXSet("deltKalkulacje", this.deltKalkulacje, deltKalkulacje);
		this.deltKalkulacje = deltKalkulacje;
	}

	public String getTrsSrodekLokomocji() {
		this.onRXGet("trsSrodekLokomocji");
		return this.trsSrodekLokomocji;
	}

	public void setTrsSrodekLokomocji(String trsSrodekLokomocji) {
		this.onRXSet("trsSrodekLokomocji", this.trsSrodekLokomocji, trsSrodekLokomocji);
		this.trsSrodekLokomocji = trsSrodekLokomocji;
	}

	public Long getTrsKrIdDiety() {
		this.onRXGet("trsKrIdDiety");
		return this.trsKrIdDiety;
	}

	public void setTrsKrIdDiety(Long trsKrIdDiety) {
		this.onRXSet("trsKrIdDiety", this.trsKrIdDiety, trsKrIdDiety);
		this.trsKrIdDiety = trsKrIdDiety;
	}

	public Double getTrsKosztBiletu() {
		this.onRXGet("trsKosztBiletu");
		return this.trsKosztBiletu;
	}

	public void setTrsKosztBiletu(Double trsKosztBiletu) {
		this.onRXSet("trsKosztBiletu", this.trsKosztBiletu, trsKosztBiletu);
		this.trsKosztBiletu = trsKosztBiletu;
	}

	public String getTrsFakturaVatNaBilet() {
		this.onRXGet("trsFakturaVatNaBilet");
		return this.trsFakturaVatNaBilet;
	}

	public void setTrsFakturaVatNaBilet(String trsFakturaVatNaBilet) {
		this.onRXSet("trsFakturaVatNaBilet", this.trsFakturaVatNaBilet, trsFakturaVatNaBilet);
		this.trsFakturaVatNaBilet = trsFakturaVatNaBilet;
	}

	public Date pgetTrsTerminPlatnosciZaBilet() {
		return getTrsTerminPlatnosciZaBilet();
	}

	public void setTrsTerminPlatnosciZaBilet(Date trsTerminPlatnosciZaBilet) {
		this.trsTerminPlatnosciZaBilet = trsTerminPlatnosciZaBilet;
	}

	public Date getTrsTerminWykupuBiletu() {
		this.onRXGet("trsTerminWykupuBiletu");
		return this.trsTerminWykupuBiletu;
	}

	public void setTrsTerminWykupuBiletu(Date trsTerminWykupuBiletu) {
		this.onRXSet("trsTerminWykupuBiletu", this.trsTerminWykupuBiletu, trsTerminWykupuBiletu);
		this.trsTerminWykupuBiletu = trsTerminWykupuBiletu;
	}

	public Date getTrsTerminPlatnosciZaBilet() {
		this.onRXGet("trsTerminPlatnosciZaBilet");
		return this.trsTerminPlatnosciZaBilet;
	}

	public Date getTrsCzynnosciSluzboweOd() {
		this.onRXGet("trsCzynnosciSluzboweOd");
		return this.trsCzynnosciSluzboweOd;
	}

	public void setTrsCzynnosciSluzboweOd(Date trsCzynnosciSluzboweOd) {
		this.onRXSet("trsCzynnosciSluzboweOd", this.trsCzynnosciSluzboweOd, trsCzynnosciSluzboweOd);
		this.trsCzynnosciSluzboweOd = trsCzynnosciSluzboweOd;
	}

	public Date getTrsCzynnosciSluzboweDo() {
		this.onRXGet("trsCzynnosciSluzboweDo");
		return this.trsCzynnosciSluzboweDo;
	}

	public void setTrsCzynnosciSluzboweDo(Date trsCzynnosciSluzboweDo) {
		this.onRXSet("trsCzynnosciSluzboweDo", this.trsCzynnosciSluzboweDo, trsCzynnosciSluzboweDo);
		this.trsCzynnosciSluzboweDo = trsCzynnosciSluzboweDo;
	}

	public List<PptDelPozycjeKalkulacji> getDeltPozycjeKalkulacji() {
		this.onRXGet("deltPozycjeKalkulacji");
		return this.deltPozycjeKalkulacji;
	}

	public void setDeltPozycjeKalkulacji(List<PptDelPozycjeKalkulacji> deltPozycjeKalkulacji) {
		this.onRXSet("deltPozycjeKalkulacji", this.deltPozycjeKalkulacji, deltPozycjeKalkulacji);
		this.deltPozycjeKalkulacji = deltPozycjeKalkulacji;
	}
	
	public PptDelPozycjeKalkulacji removeDeltPozycjeKalkulacji(PptDelPozycjeKalkulacji deltPozycjeKalkulacji) {
		this.onRXSet("deltPozycjeKalkulacji", null, this.deltPozycjeKalkulacji);
		getDeltPozycjeKalkulacji().remove(deltPozycjeKalkulacji);
		deltPozycjeKalkulacji.setDeltTrasy(null);

		return deltPozycjeKalkulacji;
	}
	
	public PptDelPozycjeKalkulacji addDeltPozycjeKalkulacji(PptDelPozycjeKalkulacji deltPozycjeKalkulacji) {
		this.onRXSet("deltPozycjeKalkulacji", null, this.deltPozycjeKalkulacji);
		getDeltPozycjeKalkulacji().add(deltPozycjeKalkulacji);
		deltPozycjeKalkulacji.setDeltTrasy(this);

		return deltPozycjeKalkulacji;
	}

	public Double getTrsIloscKM() {
		this.onRXGet("trsIloscKM");
		return this.trsIloscKM;
	}

	public void setTrsIloscKM(Double trsIloscKM) {
		this.onRXSet("trsIloscKM", this.trsIloscKM, trsIloscKM);
		this.trsIloscKM = trsIloscKM;
	}

}
