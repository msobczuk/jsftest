package com.comarch.egeria.pp.delegacjeStd.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.comarch.egeria.pp.data.model.ModelBase;


/**
 * The persistent class for the PPT_DEL_PROJEKTY database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_PROJEKTY", schema="PPADM")
@NamedQuery(name="PptDelProjekty.findAll", query="SELECT d FROM PptDelProjekty d")
public class PptDelProjekty extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

// jesli pojawi sie:	Duplicate generator named XXXXXXXXXXXXXX defined in this persistence unit
//	https://stackoverflow.com/questions/20941191/eclipse-duplicate-generator-named-id-generator-defined-in-this-persistence-uni
	
//	Select Window » Preferences
//	Expand Java Persistence » JPA » Errors/Warnings
//	Click Queries and generators
//	Set Duplicate generator defined to: Ignore
//	Click OK to apply changes and close the dialog
	
	
	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_PROJEKTY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_PROJEKTY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_PROJEKTY")
	@Column(name="PROJ_ID")
	private long projId;
	
	@Column(name="PROJ_OB_ID")
	private Long projObId;

	@Column(name="PROJ_OPIS")
	private String projOpis;
	
	

	//bi-directional many-to-one association to DeltWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="PROJ_WND_ID")
	private PptDelWnioskiDelegacji deltWnioskiDelegacji;


	public PptDelProjekty() {
	}

	public long getProjId() {
		this.onRXGet("projId");
		return this.projId;
	}

	public void setProjId(long projId) {
		this.onRXSet("projId", this.projId, projId);
		this.projId = projId;
	}

	public String getProjOpis() {
		this.onRXGet("projOpis");
		return this.projOpis;
	}

	public void setProjOpis(String projOpis) {
		this.onRXSet("projOpis", this.projOpis, projOpis);
		this.projOpis = projOpis;
	}

	/**
	 * @return the deltWnioskiDelegacji
	 */
	public PptDelWnioskiDelegacji getDeltWnioskiDelegacji() {
		return deltWnioskiDelegacji;
	}

	/**
	 * @param deltWnioskiDelegacji the deltWnioskiDelegacji to set
	 */
	public void setDeltWnioskiDelegacji(PptDelWnioskiDelegacji deltWnioskiDelegacji) {
		this.deltWnioskiDelegacji = deltWnioskiDelegacji;
	}

	public Long getProjObId() {
		return projObId;
	}

	public void setProjObId(Long projObId) {
		this.projObId = projObId;
	}


}
