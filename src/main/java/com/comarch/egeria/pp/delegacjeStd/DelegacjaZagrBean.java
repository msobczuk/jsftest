package com.comarch.egeria.pp.delegacjeStd;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.delegacjeStd.model.DelegacjaZagr;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

import java.io.Serializable;

@ManagedBean
@Named
@Scope("view")
public class DelegacjaZagrBean extends DelegacjaBean {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	
	private DelegacjaZagr delegacja = new DelegacjaZagr();
	private String rodzajNoclegu;
	
	@PostConstruct
	public void init() {
		
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		if (id==null) {
			this.setDelegacja( new DelegacjaZagr().ustawDomyslne() );
		}else {
			DelegacjaZagr d = (DelegacjaZagr) HibernateContext.get(DelegacjaZagr.class, (Serializable) id);
			this.setDelegacja(d);
		}
		
		this.getSlownik("DEL_SRODEK_LOKOMOCJI", false, false);
		this.getSlownik("DEL_FORMY_WYPLATY", false, false);
		this.getSlownik("DEL_OPLACAJACY_PKL", false, false);
		this.getSlownik("DEL_PROJEKTY_PWSZ", false, false);
		
		this.setRodzajNoclegu("DH");
	}

	public DelegacjaZagr getDelegacja() {
		return delegacja;
	}

	public void setDelegacja(DelegacjaZagr delegacja) {
		this.delegacja = delegacja;
	}

	public String getRodzajNoclegu() {
		return rodzajNoclegu;
	}

	public void setRodzajNoclegu(String rodzajNoclegu) {
		this.rodzajNoclegu = rodzajNoclegu;
	}
	
}
