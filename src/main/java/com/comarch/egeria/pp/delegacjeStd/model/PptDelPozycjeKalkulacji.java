package com.comarch.egeria.pp.delegacjeStd.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.comarch.egeria.pp.data.model.ModelBase;


/**
 * The persistent class for the DEL_POZYCJE_KALKULACJI database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_POZYCJE_KALKULACJI", schema="PPADM")
@DiscriminatorFormula("'PozycjaKalkulacji'")
@NamedQuery(name="PptDelPozycjeKalkulacji.findAll", query="SELECT d FROM PptDelPozycjeKalkulacji d")
public class PptDelPozycjeKalkulacji extends DelBase implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_POZYCJE_KALKULACJI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_POZYCJE_KALKULACJI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_POZYCJE_KALKULACJI")	
	@Column(name="PKL_ID")
	private long pklId;

	@Temporal(TemporalType.DATE)
	@Column(name="PKL_AUDYT_DM")
	private Date pklAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="PKL_AUDYT_DT")
	private Date pklAudytDt;

	@Column(name="PKL_AUDYT_KM")
	private String pklAudytKm;

//	@Column(name="PKL_AUDYT_KT")
//	private String pklAudytKt;

	@Column(name="PKL_AUDYT_LM")
	private String pklAudytLm;

	@Column(name="PKL_AUDYT_UM")
	private String pklAudytUm;

	@Column(name="PKL_AUDYT_UT")
	private String pklAudytUt;

//	@Column(name="PKL_F_OSOBA_TOWARZYSZACA")
//	private String pklFOsobaTowarzyszaca;

	@Column(name="PKL_ILOSC")
	private Double pklIlosc;

//	@Column(name="PKL_KAL_ID")
//	private Long pklKalId;
	
	//bi-directional many-to-one association to DeltKalkulacje
	@ManyToOne
	@JoinColumn(name="PKL_KAL_ID")
	private PptDelKalkulacje deltKalkulacje;

	@Column(name="PKL_KR_ID")
	private Long pklKrId;

	@Column(name="PKL_KWOTA")
	private Double pklKwota;

	@Column(name="PKL_KWOTA_GOTOWKI")
	private Double pklKwotaGotowki = 0.0;

	@Column(name="PKL_KWOTA_PRZELEWU")
	private Double pklKwotaPrzelewu = 0.0;

//	@Column(name="PKL_LP")
//	private Long pklLp;

	@Column(name="PKL_OPIS")
	private String pklOpis;

	@Column(name="PKL_STAWKA")
	private Double pklStawka;

	@Column(name="PKL_TPOZ_ID")
	private Long pklTpozId;

	@Column(name="PKL_WAL_ID")
	private Long pklWalId;
	
	@Column(name="PKL_KOSZT_MF_PROCENT")
	private Double pklKosztMFProcent;
	
	@Column(name="PKL_KOSZT_ZAPR_PROCENT")
	private Double pklKosztZaprProcent;
	
	@Column(name="PKL_KOSZT_ZAPR_KWOTA")
	private Double pklKosztZaprKwota;
	
	@Column(name="PKL_REFUNDACJA_PROCENT")
	private Double pklRefundacjaProcent;
	
	@Column(name="PKL_REFUNDACJA_KWOTA")
	private Double pklRefundacjaKwota;
	
	
	@Column(name="PKL_SNIADANIA")
	private Long pklSniadania;
	
	@Column(name="PKL_OBIADY")
	private Long pklObiady;	

	@Column(name="PKL_KOLACJE")
	private Long pklKolacje;
	
	@Column(name="PKL_KWOTA_PLN")
	private Double pklKwotaPln;
	
	@Column(name="PKL_KURS")
	private Double pklKurs;
	
	@Column(name="PKL_ROK")
	private Long pklRok;
	
	@Column(name="PKL_REFUNDAJCA_OPIS")
	private String pklRefundacjaOpis;
	
	@Column(name="PKL_KWOTA_MF")
	private Double pklKwotaMf;
	
	@Column(name="PKL_F_CZY_UMOWA_Z_WYK")
	private String pklFCzyUmowaZWyk;
	
	@Column(name="PKL_F_CZY_BEZPLATNA")
	private String pklFCzyBezplatna;
	
	@Column(name="PKL_FORMA_PLATNOSCI")
	private String pklFormaPlatnosci="01";
	
	@ManyToOne
	@JoinColumn(name="PKL_TRS_ID")
	private PptDelTrasy deltTrasy;
	
	@Temporal(TemporalType.DATE)
	@Column(name="PKL_TERMIN_WYKUPU")
	private Date pklTerminWykupu;
	
	@Column(name="PKL_NR_REJ")
	private String pklNrRej;
	
	@Column(name="PKL_SAMOCHOD")
	private String pklSamochod;
	
	@Column(name="PKL_POJEMNOSC")
	private Double pklPojemnosc;
	
	@Column(name="PKL_OPLACAJACY")
	private String pklOplacajacy;
	
	//bi-directional many-to-one association to DeltZrodlaFinansowania
	@OneToMany(mappedBy="deltPozycjeKalkulacji",  cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<PptDelZrodlaFinansowania> deltZrodlaFinansowanias = new ArrayList<>();
	
	
	public PptDelPozycjeKalkulacji() {
	}
	
//	public DeltPozycjeKalkulacji clone(){
//		
//		DeltPozycjeKalkulacji pkl = new DeltPozycjeKalkulacji();
//		
////		pkl.setDeltKalkulacje(deltKalkulacje);
////		pkl.setDeltZrodlaFinansowanias(deltZrodlaFinansowanias);
//		pkl.setPklId(0L);
//		pkl.setPklIlosc(pklIlosc);
//		pkl.setPklKolacje(pklKolacje);
//		pkl.setPklKosztMFProcent(pklKosztMFProcent);
//		pkl.setPklKosztZaprKwota(pklKosztZaprKwota);
//		pkl.setPklKosztZaprProcent(pklKosztZaprProcent);
//		pkl.setPklKrId(pklKrId);
//		pkl.setPklKwota(pklKwota);
//		pkl.setPklKwotaGotowki(pklKwotaGotowki);
//		pkl.setPklKwotaPrzelewu(pklKwotaPrzelewu);
//		pkl.setPklObiady(pklObiady);
//		pkl.setPklOpis(pklOpis);
//		pkl.setPklRefundacjaKwota(pklRefundacjaKwota);
//		pkl.setPklRefundacjaProcent(pklRefundacjaProcent);
//		pkl.setPklSniadania(pklSniadania);
//		pkl.setPklStawka(pklStawka);
//		pkl.setPklTpozId(pklTpozId);
//		pkl.setPklWalId(pklWalId);
//		pkl.setPklKwotaMf(pklKwotaMf);
//		pkl.setPklFormaPlatnosci(pklFormaPlatnosci);
//		pkl.setPklFCzyUmowaZWyk(pklFCzyUmowaZWyk);
//		pkl.setpklFCzyBezplatna(pklFCzyBezplatna);
//		pkl.setPklTerminWykupu(pklTerminWykupu);
//		
//		pkl.setSqlBean(sqlBean);
//		return pkl;
//		
//	}
	
	
	
//	public DeltPozycjeKalkulacji clone(boolean deepCopy){
//		DeltPozycjeKalkulacji pkl = clone();
//		if (!deepCopy) return pkl;
//
//		for (DeltZrodlaFinansowania zr: this.deltZrodlaFinansowanias){
//			DeltZrodlaFinansowania nzf = zr.clone(deepCopy);
//			nzf.setPkzfId(0L);
//			pkl.addDeltZrodlaFinansowania(nzf);
//		}
//		
//		return pkl;
//		
//	}
	
	
	
	public long getPklId() {
		this.onRXGet("pklId");
		return this.pklId;
	}

	public void setPklId(long pklId) {
		this.onRXSet("pklId", this.pklId, pklId);
		this.pklId = pklId;
	}

	public Date getPklAudytDm() {
		this.onRXGet("pklAudytDm");
		return this.pklAudytDm;
	}

	public void setPklAudytDm(Date pklAudytDm) {
		this.onRXSet("pklAudytDm", this.pklAudytDm, pklAudytDm);
		this.pklAudytDm = pklAudytDm;
	}

	public Date getPklAudytDt() {
		this.onRXGet("pklAudytDt");
		return this.pklAudytDt;
	}

	public void setPklAudytDt(Date pklAudytDt) {
		this.onRXSet("pklAudytDt", this.pklAudytDt, pklAudytDt);
		this.pklAudytDt = pklAudytDt;
	}

	public String getPklAudytKm() {
		this.onRXGet("pklAudytKm");
		return this.pklAudytKm;
	}

	public void setPklAudytKm(String pklAudytKm) {
		this.onRXSet("pklAudytKm", this.pklAudytKm, pklAudytKm);
		this.pklAudytKm = pklAudytKm;
	}

//	public String getPklAudytKt() {
//		return this.pklAudytKt;
//	}
//
//	public void setPklAudytKt(String pklAudytKt) {
//		this.pklAudytKt = pklAudytKt;
//	}

	public String getPklAudytLm() {
		this.onRXGet("pklAudytLm");
		return this.pklAudytLm;
	}

	public void setPklAudytLm(String pklAudytLm) {
		this.onRXSet("pklAudytLm", this.pklAudytLm, pklAudytLm);
		this.pklAudytLm = pklAudytLm;
	}

	public String getPklAudytUm() {
		this.onRXGet("pklAudytUm");
		return this.pklAudytUm;
	}

	public void setPklAudytUm(String pklAudytUm) {
		this.onRXSet("pklAudytUm", this.pklAudytUm, pklAudytUm);
		this.pklAudytUm = pklAudytUm;
	}

	public String getPklAudytUt() {
		this.onRXGet("pklAudytUt");
		return this.pklAudytUt;
	}

	public void setPklAudytUt(String pklAudytUt) {
		this.onRXSet("pklAudytUt", this.pklAudytUt, pklAudytUt);
		this.pklAudytUt = pklAudytUt;
	}

//	public String getPklFOsobaTowarzyszaca() {
//		return this.pklFOsobaTowarzyszaca;
//	}
//
//	public void setPklFOsobaTowarzyszaca(String pklFOsobaTowarzyszaca) {
//		this.pklFOsobaTowarzyszaca = pklFOsobaTowarzyszaca;
//	}

	public Double getPklIlosc() {
		this.onRXGet("pklIlosc");
		return this.pklIlosc;
	}

	public void setPklIlosc(Double pklIlosc) {
		this.onRXSet("pklIlosc", this.pklIlosc, pklIlosc);
		this.pklIlosc = pklIlosc;
	}

	public Long getPklKrId() {
		this.onRXGet("pklKrId");
		return this.pklKrId;
	}

	public void setPklKrId(Long pklKrId) {
		this.onRXSet("pklKrId", this.pklKrId, pklKrId);
		this.pklKrId = pklKrId;
	}
	
	

	public Double getPklKwota() {
		this.onRXGet("pklKwota");
		return this.pklKwota;
	}

	public void setPklKwota(Double pklKwota) {
		this.onRXSet("pklKwota", this.pklKwota, pklKwota);
		this.pklKwota = pklKwota;
	}

	public Double getPklKwotaGotowki() {
		this.onRXGet("pklKwotaGotowki");
		return this.pklKwotaGotowki;
	}

	public void setPklKwotaGotowki(Double pklKwotaGotowki) {
		this.onRXSet("pklKwotaGotowki", this.pklKwotaGotowki, pklKwotaGotowki);
		this.pklKwotaGotowki = pklKwotaGotowki;
	}

	public Double getPklKwotaPrzelewu() {
		this.onRXGet("pklKwotaPrzelewu");
		return this.pklKwotaPrzelewu;
	}

	public void setPklKwotaPrzelewu(Double pklKwotaPrzelewu) {
		this.onRXSet("pklKwotaPrzelewu", this.pklKwotaPrzelewu, pklKwotaPrzelewu);
		this.pklKwotaPrzelewu = pklKwotaPrzelewu;
	}

	public String getPklOpis() {
		this.onRXGet("pklOpis");
		return this.pklOpis;
	}

	public void setPklOpis(String pklOpis) {
		this.onRXSet("pklOpis", this.pklOpis, pklOpis);
		this.pklOpis = pklOpis;
	}

	public Double getPklStawka() {
		this.onRXGet("pklStawka");
		return this.pklStawka;
	}

	public void setPklStawka(Double pklStawka) {
		this.onRXSet("pklStawka", this.pklStawka, pklStawka);
		this.pklStawka = pklStawka;
	}

	public Long getPklTpozId() {
		this.onRXGet("pklTpozId");
		return this.pklTpozId;
	}

	public void setPklTpozId(Long pklTpozId) {
		this.onRXSet("pklTpozId", this.pklTpozId, pklTpozId);
		this.pklTpozId = pklTpozId;
	}

	public Long getPklWalId() {
		this.onRXGet("pklWalId");
		return this.pklWalId;
	}

	public void setPklWalId(Long pklWalId){
		this.onRXSet("pklWalId", this.pklWalId, pklWalId);
		this.pklWalId = pklWalId;
	}

	/**
	 * @return the deltKalkulacje
	 */
	public PptDelKalkulacje getDeltKalkulacje() {
		this.onRXGet("deltKalkulacje");
		return this.deltKalkulacje;
	}

	/**
	 * @param deltKalkulacje the deltKalkulacje to set
	 */
	public void setDeltKalkulacje(PptDelKalkulacje deltKalkulacje) {
		this.onRXSet("deltKalkulacje", this.deltKalkulacje, deltKalkulacje);
		this.deltKalkulacje = deltKalkulacje;
	}

	/**
	 * @return the pklKosztMFProcent
	 */
	public Double getPklKosztMFProcent() {
		this.onRXGet("pklKosztMFProcent");
		return this.pklKosztMFProcent;
	}

	/**
	 * @param pklKosztMFProcent the pklKosztMFProcent to set
	 */
	public void setPklKosztMFProcent(Double pklKosztMFProcent) {
		this.onRXSet("pklKosztMFProcent", this.pklKosztMFProcent, pklKosztMFProcent);
		this.pklKosztMFProcent = pklKosztMFProcent;
	}

	/**
	 * @return the pklKosztZaprProcent
	 */
	public Double getPklKosztZaprProcent() {
		this.onRXGet("pklKosztZaprProcent");
		return this.pklKosztZaprProcent;
	}

	/**
	 * @param pklKosztZaprProcent the pklKosztZaprProcent to set
	 */
	public void setPklKosztZaprProcent(Double pklKosztZaprProcent) {
		this.onRXSet("pklKosztZaprProcent", this.pklKosztZaprProcent, pklKosztZaprProcent);
		this.pklKosztZaprProcent = pklKosztZaprProcent;
	}

	/**
	 * @return the pklKosztZaprKwota
	 */
	public Double getPklKosztZaprKwota() {
		this.onRXGet("pklKosztZaprKwota");
		return this.pklKosztZaprKwota;
	}

	/**
	 * @param pklKosztZaprKwota the pklKosztZaprKwota to set
	 */
	public void setPklKosztZaprKwota(Double pklKosztZaprKwota) {
		this.onRXSet("pklKosztZaprKwota", this.pklKosztZaprKwota, pklKosztZaprKwota);
		this.pklKosztZaprKwota = pklKosztZaprKwota;
	}

	/**
	 * @return the pklRefundacjaProcent
	 */
	public Double getPklRefundacjaProcent() {
		this.onRXGet("pklRefundacjaProcent");
		return this.pklRefundacjaProcent;
	}

	/**
	 * @param pklRefundacjaProcent the pklRefundacjaProcent to set
	 */
	public void setPklRefundacjaProcent(Double pklRefundacjaProcent) {
		this.onRXSet("pklRefundacjaProcent", this.pklRefundacjaProcent, pklRefundacjaProcent);
		this.pklRefundacjaProcent = pklRefundacjaProcent;
	}

	/**
	 * @return the pklRefundacjaKwota
	 */
	public Double getPklRefundacjaKwota() {
		this.onRXGet("pklRefundacjaKwota");
		return this.pklRefundacjaKwota;
	}

	public void setPklRefundacjaKwota(Double pklRefundacjaKwota) {
		this.onRXSet("pklRefundacjaKwota", this.pklRefundacjaKwota, pklRefundacjaKwota);
		this.pklRefundacjaKwota = pklRefundacjaKwota;
	}

	public Long getPklSniadania() {
		this.onRXGet("pklSniadania");
		return this.pklSniadania;
	}

	public void setPklSniadania(Long pklSniadania) {
		this.onRXSet("pklSniadania", this.pklSniadania, pklSniadania);
		this.pklSniadania = pklSniadania;
	}

	public Long getPklObiady() {
		this.onRXGet("pklObiady");
		return this.pklObiady;
	}

	public void setPklObiady(Long pklObiady) {
		this.onRXSet("pklObiady", this.pklObiady, pklObiady);
		this.pklObiady = pklObiady;
	}

	public Long getPklKolacje() {
		this.onRXGet("pklKolacje");
		return this.pklKolacje;
	}

	public void setPklKolacje(Long pklKolacje) {
		this.onRXSet("pklKolacje", this.pklKolacje, pklKolacje);
		this.pklKolacje = pklKolacje;
	}

	
	public List<PptDelZrodlaFinansowania> getDeltZrodlaFinansowanias() {
		this.onRXGet("deltZrodlaFinansowanias");
		return this.deltZrodlaFinansowanias;
	}

	public void setDeltZrodlaFinansowanias(List<PptDelZrodlaFinansowania> deltZrodlaFinansowanias) {
		this.onRXSet("deltZrodlaFinansowanias", this.deltZrodlaFinansowanias, deltZrodlaFinansowanias);
		this.deltZrodlaFinansowanias = deltZrodlaFinansowanias;
	}

	public PptDelZrodlaFinansowania addDeltZrodlaFinansowania(PptDelZrodlaFinansowania deltZrodlaFinansowania) {
		this.onRXSet("deltZrodlaFinansowanias", null, this.deltZrodlaFinansowanias);
		getDeltZrodlaFinansowanias().add(deltZrodlaFinansowania);
		deltZrodlaFinansowania.setDeltPozycjeKalkulacji(this);

		return deltZrodlaFinansowania;
	}

	public PptDelZrodlaFinansowania removeDeltZrodlaFinansowania(PptDelZrodlaFinansowania deltZrodlaFinansowania) {
		this.onRXSet("deltZrodlaFinansowanias", null, this.deltZrodlaFinansowanias);
		getDeltZrodlaFinansowanias().remove(deltZrodlaFinansowania);
		deltZrodlaFinansowania.setDeltPozycjeKalkulacji(null);

		return deltZrodlaFinansowania;
	}
	
	public Double getPklKwotaPln() {
		this.onRXGet("pklKwotaPln");
		return this.pklKwotaPln;
	}


	public void setPklKwotaPln(Double pklKwotaPln) {
		this.onRXSet("pklKwotaPln", this.pklKwotaPln, pklKwotaPln);
		this.pklKwotaPln = pklKwotaPln;
	}

	public Double getPklKurs() {
		this.onRXGet("pklKurs");
		return this.pklKurs;
	}

	public void setPklKurs(Double pklKurs) {
		this.onRXSet("pklKurs", this.pklKurs, pklKurs);
		this.pklKurs = pklKurs;
	}

	public Long getPklRok() {
		this.onRXGet("pklRok");
		return this.pklRok;
	}

	public void setPklRok(Long pklRok) {
		this.onRXSet("pklRok", this.pklRok, pklRok);
		this.pklRok = pklRok;
	}
	
	public String getPklRefundacjaOpis() {
		this.onRXGet("pklRefundacjaOpis");
		return this.pklRefundacjaOpis;
	}

	public void setPklRefundacjaOpis(String pklRefundacjaOpis) {
		this.onRXSet("pklRefundacjaOpis", this.pklRefundacjaOpis, pklRefundacjaOpis);
		this.pklRefundacjaOpis = pklRefundacjaOpis;
	}
	
	
	@Override
	public String toString() {
		return String.format("id: %1$s; PklTpozId: %2$s; pklWalId: %3$s ",this.pklId, this.pklTpozId, this.pklWalId);
	}

	public Double getPklKwotaMf() {
		this.onRXGet("pklKwotaMf");
		return this.pklKwotaMf;
	}

	public void setPklKwotaMf(Double pklKwotaMf) {
		this.onRXSet("pklKwotaMf", this.pklKwotaMf, pklKwotaMf);
		this.pklKwotaMf = pklKwotaMf;
	}

	public String getPklFCzyUmowaZWyk() {
		this.onRXGet("pklFCzyUmowaZWyk");
		return this.pklFCzyUmowaZWyk;
	}

	public void setPklFCzyUmowaZWyk(String pklFCzyUmowaZWyk) {
		this.onRXSet("pklFCzyUmowaZWyk", this.pklFCzyUmowaZWyk, pklFCzyUmowaZWyk);
		this.pklFCzyUmowaZWyk = pklFCzyUmowaZWyk;
	}

	public String getpklFCzyBezplatna() {
		this.onRXGet("pklFCzyBezplatna");
		return this.pklFCzyBezplatna;
	}

	public void setpklFCzyBezplatna(String pklFCzyBezplatna) {
		this.onRXSet("pklFCzyBezplatna", this.pklFCzyBezplatna, pklFCzyBezplatna);
		this.pklFCzyBezplatna = pklFCzyBezplatna;
	}

	public String getPklFormaPlatnosci() {
		this.onRXGet("pklFormaPlatnosci");
		return this.pklFormaPlatnosci;
	}

	public void setPklFormaPlatnosci(String pklFormaPlatnosci) {
		this.onRXSet("pklFormaPlatnosci", this.pklFormaPlatnosci, pklFormaPlatnosci);
		this.pklFormaPlatnosci = pklFormaPlatnosci;
	}

	public PptDelTrasy getDeltTrasy() {
		this.onRXGet("deltTrasy");
		return this.deltTrasy;
	}

	public void setDeltTrasy(PptDelTrasy deltTrasy) {
		this.onRXSet("deltTrasy", this.deltTrasy, deltTrasy);
		this.deltTrasy = deltTrasy;
	}

	public Date getPklTerminWykupu() {
		this.onRXGet("pklTerminWykupu");
		return this.pklTerminWykupu;
	}

	public void setPklTerminWykupu(Date pklTerminWykupu) {
		this.onRXSet("pklTerminWykupu", this.pklTerminWykupu, pklTerminWykupu);
		this.pklTerminWykupu = pklTerminWykupu;
	}

	public String getPklNrRej() {
		this.onRXGet("pklNrRej");
		return this.pklNrRej;
	}

	public void setPklNrRej(String pklNrRej) {
		this.onRXSet("pklNrRej", this.pklNrRej, pklNrRej);
		this.pklNrRej = pklNrRej;
	}

	public String getPklSamochod() {
		this.onRXGet("pklSamochod");
		return this.pklSamochod;
	}

	public void setPklSamochod(String pklSamochod) {
		this.onRXSet("pklSamochod", this.pklSamochod, pklSamochod);
		this.pklSamochod = pklSamochod;
	}

	public Double getPklPojemnosc() {
		this.onRXGet("pklPojemnosc");
		return this.pklPojemnosc;
	}

	public void setPklPojemnosc(Double pklPojemnosc) {
		this.onRXSet("pklPojemnosc", this.pklPojemnosc, pklPojemnosc);
		this.pklPojemnosc = pklPojemnosc;
	}

	public String getPklOplacajacy() {
		return pklOplacajacy;
	}

	public void setPklOplacajacy(String pklOplacajacy) {
		this.pklOplacajacy = pklOplacajacy;
	}
	


	
}
