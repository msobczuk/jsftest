package com.comarch.egeria.pp.delegacjeStd.model;

import static com.comarch.egeria.Utils.round;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.comarch.egeria.pp.data.model.ModelBase;


/**
 * The persistent class for the PPT_DEL_KALKULACJE database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_KALKULACJE", schema="PPADM")
@NamedQuery(name="PptDelKalkulacje.findAll", query="SELECT d FROM PptDelKalkulacje d")
@DiscriminatorFormula("CASE "
		+ "WHEN KAL_RODZAJ='WNK' 		THEN 'KalkulacjaWniosek'"
		+ "WHEN KAL_RODZAJ='WST' 		THEN 'KalkulacjaWstepna'"
		+ "ELSE 'KalkulacjaRozlicz' END")

public class PptDelKalkulacje extends DelBase implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_KALKULACJE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_KALKULACJE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_KALKULACJE")	
	@Column(name="KAL_ID")
	private long kalId;

	@Temporal(TemporalType.DATE)
	@Column(name="KAL_AUDYT_DM")
	private Date kalAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="KAL_AUDYT_DT")
	private Date kalAudytDt;

	@Column(name="KAL_AUDYT_KM")
	private String kalAudytKm;

//	@Column(name="KAL_AUDYT_KT")
//	private String kalAudytKt;

	@Column(name="KAL_AUDYT_LM")
	private String kalAudytLm;

	@Column(name="KAL_AUDYT_UM")
	private String kalAudytUm;

	@Column(name="KAL_AUDYT_UT")
	private String kalAudytUt;

	@Temporal(TemporalType.DATE)
	@Column(name="KAL_DATA")
	private Date kalData = new Date();
	
	@Temporal(TemporalType.DATE)
	@Column(name="KAL_DATA_KURSU")
	private Date kalDataKursu;

//	@Column(name="KAL_F_AKTUALNA")
//	private String kalFAktualna;
//
//	@Column(name="KAL_F_ZATWIERDZONA")
//	private String kalFZatwierdzona;
//
//	@Temporal(TemporalType.DATE)
//	@Column(name="KAL_GODZINA_POWROTU")
//	private Date kalGodzinaPowrotu;

//	@Temporal(TemporalType.DATE)
//	@Column(name="KAL_GODZINA_WYJAZDU")
//	private Date kalGodzinaWyjazdu;
//
//	@Column(name="KAL_KNT_ID_PRC")
//	private Long kalKntIdPrc;
//
//	@Column(name="KAL_KNT_ID_WL")
//	private Long kalKntIdWl;
//
//	@Column(name="KAL_LICZBA_DNI")
//	private long kalLiczbaDni;
//
//	@Column(name="KAL_LICZBA_NOCLEGOW")
//	private long kalLiczbaNoclegow;
//
//	@Column(name="KAL_LICZBA_ROZPOCZETYCH_DNI")
//	private long kalLiczbaRozpoczetychDni;

//	@Column(name="KAL_LP")
//	private long kalLp;

	@Column(name="KAL_OPIS")
	private String kalOpis;

//	@Column(name="KAL_POLE01")
//	private String kalPole01;
//
//	@Column(name="KAL_POLE02")
//	private String kalPole02;
//
//	@Column(name="KAL_POLE03")
//	private String kalPole03;
//
//	@Column(name="KAL_POLE04")
//	private String kalPole04;
//
//	@Column(name="KAL_POLE05")
//	private String kalPole05;
//
//	@Column(name="KAL_POLE06")
//	private String kalPole06;
//
//	@Column(name="KAL_POLE07")
//	private String kalPole07;
//
//	@Column(name="KAL_POLE08")
//	private String kalPole08;
//
//	@Column(name="KAL_POLE09")
//	private String kalPole09;
//
//	@Column(name="KAL_POLE10")
//	private String kalPole10;

//	@Column(name="KAL_PRC_ID")
//	private long kalPrcId;
//	
	@Column(name="KAL_KW_ID")
	private Long kalKwId;
	
	@Column(name="KAL_KAL_ID")
	private Long kalKalId;
	
	@Column(name="KAL_RODZAJ")
	private String kalRodzaj;

	//bi-directional many-to-one association to DeltWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="KAL_WND_ID")
	private PptDelWnioskiDelegacji deltWnioskiDelegacji;
	
	@OneToMany(mappedBy="deltKalkulacje", cascade={CascadeType.ALL}, orphanRemoval=true)
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("PKL_ID ASC")
	private List<PptDelPozycjeKalkulacji> deltPozycjeKalkulacjis = new ArrayList<>();

	//bi-directional many-to-one association to DeltTrasy
	@OneToMany(mappedBy="deltKalkulacje", cascade={CascadeType.ALL}, orphanRemoval=true)
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("TRS_ID ASC")
	private List<PptDelTrasy> deltTrasies = new ArrayList<>();
	
	@OneToMany(mappedBy="deltKalkulacje", cascade={CascadeType.ALL}, orphanRemoval=true)
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("DKZ_ID ASC")
	private List<PptDelZaliczki> delZaliczki = new ArrayList<>();
	
	@OneToMany(mappedBy="deltKalkulacje", cascade={CascadeType.ALL}, orphanRemoval=true)
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("CZYN_ID ASC")
	private List<PptDelCzynnosciSluzbowe> delCzynnosciSluzbowe = new ArrayList<>();
	
	
	
	
	
	/**
	 * Nie mozna wprowadzic kazdej liczby jako liczba diet, dopuszczalne to liczby całkowite, l.całk.+połówki oraz l.całk.+1/3
	 * @param ileDiet
	 * @return
	 */
	public static boolean testLiczbaDiet(Double ileDiet, boolean fcs){
		if (ileDiet==null || ileDiet<0)
			return false;
		
		Double fract =  round(ileDiet % 1, 2);//z. ulamkowa z ileDiet
		if(!fcs) {
		if (fract==0.0 || fract == 0.33 || fract == 0.5)
			return true;
		} else {
			if (fract==0.0 || fract == 0.5)
				return true;
		}
			
		
		return false;
	}
	
	
//	public DeltKalkulacje clone(){//shallow copy
//		
//		DeltKalkulacje kal = new DeltKalkulacje();
//		kal.setKalData(kalData);
//		kal.setKalDataKursu(kalDataKursu);
//		kal.setKalId(0L);
//		kal.setKalRodzaj(this.getKalRodzaj());
//		kal.setKalOpis(this.getKalOpis());
//		kal.setDeltWnioskiDelegacji(null);
//		kal.setSqlBean(this.getSqlBean());
//
//		return kal;
//		
//	}
	
	
	
//	public DeltKalkulacje clone(boolean deepCopy){
//		DeltKalkulacje kal = clone(); //shallow copy
//		if (!deepCopy) return kal;
//		
//		for (DeltPozycjeKalkulacji pk: this.deltPozycjeKalkulacjis){
//			DeltPozycjeKalkulacji npk = pk.clone(deepCopy);
//			npk.setPklId(0L);
//			kal.addDeltPozycjeKalkulacji(npk);
//		}
//		
//		if(!this.deltTrasies.isEmpty()){
//			for(DeltTrasy tr : this.deltTrasies) {
//				DeltTrasy t = tr.clone();
//				
//				t.setTrsId(0L);
//				kal.addDeltTrasy(t);
//			}
//		}
//		
//		if(!this.delZaliczki.isEmpty()){
//			for( DelZaliczki zal : this.delZaliczki) {
//				DelZaliczki z = zal.clone();
//				
//				z.setDkzId(0L);
//				kal.addDeltZaliczki(z);
//			}
//		}
//		
//		if(!this.delCzynnosciSluzbowe.isEmpty()){
//			for( DelCzynnosciSluzbowe cz : this.delCzynnosciSluzbowe) {
//				DelCzynnosciSluzbowe c = cz.clone();
//				
//				c.setCzynId(0L);
//				kal.addDelCzynnosciSluzbowe(c);
//			}
//		}
//		
//		return kal;
//		
//	}
	
	
	
	
	public PptDelKalkulacje() {
	}

	public long getKalId() {
		this.onRXGet("kalId");
		return this.kalId;
	}

	public void setKalId(long kalId) {
		this.onRXSet("kalId", this.kalId, kalId);
		this.kalId = kalId;
	}

	public Date getKalAudytDm() {
		this.onRXGet("kalAudytDm");
		return this.kalAudytDm;
	}

	public void setKalAudytDm(Date kalAudytDm) {
		this.onRXSet("kalAudytDm", this.kalAudytDm, kalAudytDm);
		this.kalAudytDm = kalAudytDm;
	}

	public Date getKalAudytDt() {
		this.onRXGet("kalAudytDt");
		return this.kalAudytDt;
	}

	public void setKalAudytDt(Date kalAudytDt) {
		this.onRXSet("kalAudytDt", this.kalAudytDt, kalAudytDt);
		this.kalAudytDt = kalAudytDt;
	}

	public String getKalAudytKm() {
		this.onRXGet("kalAudytKm");
		return this.kalAudytKm;
	}

	public void setKalAudytKm(String kalAudytKm) {
		this.onRXSet("kalAudytKm", this.kalAudytKm, kalAudytKm);
		this.kalAudytKm = kalAudytKm;
	}

//	public String getKalAudytKt() {
//		return this.kalAudytKt;
//	}
//
//	public void setKalAudytKt(String kalAudytKt) {
//		this.kalAudytKt = kalAudytKt;
//	}

	public String getKalAudytLm() {
		this.onRXGet("kalAudytLm");
		return this.kalAudytLm;
	}

	public void setKalAudytLm(String kalAudytLm) {
		this.onRXSet("kalAudytLm", this.kalAudytLm, kalAudytLm);
		this.kalAudytLm = kalAudytLm;
	}

	public String getKalAudytUm() {
		this.onRXGet("kalAudytUm");
		return this.kalAudytUm;
	}

	public void setKalAudytUm(String kalAudytUm) {
		this.onRXSet("kalAudytUm", this.kalAudytUm, kalAudytUm);
		this.kalAudytUm = kalAudytUm;
	}

	public String getKalAudytUt() {
		this.onRXGet("kalAudytUt");
		return this.kalAudytUt;
	}

	public void setKalAudytUt(String kalAudytUt) {
		this.onRXSet("kalAudytUt", this.kalAudytUt, kalAudytUt);
		this.kalAudytUt = kalAudytUt;
	}

	public Date getKalData() {
		this.onRXGet("kalData");
		return this.kalData;
	}

	public void setKalData(Date kalData) {
		this.onRXSet("kalData", this.kalData, kalData);
		this.kalData = kalData;
	}


	public String getKalOpis() {
		this.onRXGet("kalOpis");
		return this.kalOpis;
	}

	public void setKalOpis(String kalOpis) {
		this.onRXSet("kalOpis", this.kalOpis, kalOpis);
		this.kalOpis = kalOpis;
	}


	/**
	 * @return the deltWnioskiDelegacji
	 */
	public PptDelWnioskiDelegacji getDeltWnioskiDelegacji() {
		this.onRXGet("deltWnioskiDelegacji");
		return this.deltWnioskiDelegacji;
	}

	/**
	 * @param deltWnioskiDelegacji the deltWnioskiDelegacji to set
	 */
	public void setDeltWnioskiDelegacji(PptDelWnioskiDelegacji deltWnioskiDelegacji) {
		this.onRXSet("deltWnioskiDelegacji", this.deltWnioskiDelegacji, deltWnioskiDelegacji);
		this.deltWnioskiDelegacji = deltWnioskiDelegacji;
	}

	/**
	 * @return the deltPozycjeKalkulacjis
	 */
	public List<PptDelPozycjeKalkulacji> getDeltPozycjeKalkulacjis() {
		this.onRXGet("deltPozycjeKalkulacjis");
		return this.deltPozycjeKalkulacjis;
	}

	
	/**
	 * @param deltPozycjeKalkulacjis the deltPozycjeKalkulacjis to set
	 */
	public void setDeltPozycjeKalkulacjis(List<PptDelPozycjeKalkulacji> deltPozycjeKalkulacjis) {
		this.onRXSet("deltPozycjeKalkulacjis", this.deltPozycjeKalkulacjis, deltPozycjeKalkulacjis);
		this.deltPozycjeKalkulacjis = deltPozycjeKalkulacjis;
	}
	
	
	public PptDelPozycjeKalkulacji addDeltPozycjeKalkulacji(PptDelPozycjeKalkulacji deltPozycjeKalkulacji) {
		this.onRXSet("deltPozycjeKalkulacjis", null, this.deltPozycjeKalkulacjis);
		getDeltPozycjeKalkulacjis().add(deltPozycjeKalkulacji);
		deltPozycjeKalkulacji.setDeltKalkulacje(this);
		
		return deltPozycjeKalkulacji;
	}

	public PptDelPozycjeKalkulacji removeDeltPozycjeKalkulacji(PptDelPozycjeKalkulacji deltPozycjeKalkulacji) {
		this.onRXSet("deltPozycjeKalkulacjis", null, this.deltPozycjeKalkulacjis);
		getDeltPozycjeKalkulacjis().remove(deltPozycjeKalkulacji);
		deltPozycjeKalkulacji.setDeltKalkulacje(null);

		return deltPozycjeKalkulacji;
	}

	public Long getKalKwId() {
		this.onRXGet("kalKwId");
		return this.kalKwId;
	}

	public void setKalKwId(Long kalKwId) {
		this.onRXSet("kalKwId", this.kalKwId, kalKwId);
		this.kalKwId = kalKwId;
	}

	public Date getKalDataKursu() {
		this.onRXGet("kalDataKursu");
		return this.kalDataKursu;
	}

	public void setKalDataKursu(Date kalDataKursu) {
		this.onRXSet("kalDataKursu", this.kalDataKursu, kalDataKursu);
		this.kalDataKursu = kalDataKursu;
	}
	

	public List<PptDelTrasy> getDeltTrasies() {
		this.onRXGet("deltTrasies");
		return this.deltTrasies;
	}

	public void setDeltTrasies(List<PptDelTrasy> deltTrasies) {
		this.onRXSet("deltTrasies", this.deltTrasies, deltTrasies);
		this.deltTrasies = deltTrasies;
	}

	public PptDelTrasy addDeltTrasy(PptDelTrasy deltTrasy) {
		this.onRXSet("deltTrasies", null, this.deltTrasies);
		getDeltTrasies().add(deltTrasy);
		deltTrasy.setDeltKalkulacje(this);

		return deltTrasy;
	}
	
	public PptDelTrasy removeDeltTrasy(PptDelTrasy deltTrasy) {
		this.onRXSet("deltTrasies", null, this.deltTrasies);
		getDeltTrasies().remove(deltTrasy);
		deltTrasy.setDeltKalkulacje(null);

		return deltTrasy;
	}

	public Long getKalKalId() {
		this.onRXGet("kalKalId");
		return this.kalKalId;
	}

	public void setKalKalId(Long kalKalId) {
		this.onRXSet("kalKalId", this.kalKalId, kalKalId);
		this.kalKalId = kalKalId;
	}

	public List<PptDelZaliczki> getDelZaliczki() {
		this.onRXGet("delZaliczki");
		return this.delZaliczki;
	}

	public void setDelZaliczki(List<PptDelZaliczki> delZaliczki) {
		this.onRXSet("delZaliczki", this.delZaliczki, delZaliczki);
		this.delZaliczki = delZaliczki;
	}


	public PptDelZaliczki addDeltZaliczki(PptDelZaliczki delZaliczki) {
		this.onRXSet("delZaliczki", null, this.delZaliczki);
		getDelZaliczki().add(delZaliczki);
		delZaliczki.setDeltKalkulacje(this);

		return delZaliczki;
	}

	public PptDelZaliczki removeDelZaliczki(PptDelZaliczki delZaliczki) {
		this.onRXSet("delZaliczki", null, this.delZaliczki);
		getDelZaliczki().remove(delZaliczki);
		delZaliczki.setDeltKalkulacje(null);

		return delZaliczki;
	}

	public String getKalRodzaj() {
		this.onRXGet("kalRodzaj");
		return this.kalRodzaj;
	}

	public void setKalRodzaj(String kalRodzaj) {
		this.onRXSet("kalRodzaj", this.kalRodzaj, kalRodzaj);
		this.kalRodzaj = kalRodzaj;
	}



	public List<PptDelCzynnosciSluzbowe> getDelCzynnosciSluzbowe() {
		this.onRXGet("delCzynnosciSluzbowe");
		return this.delCzynnosciSluzbowe;
	}



	public void setDelCzynnosciSluzbowe(List<PptDelCzynnosciSluzbowe> delCzynnosciSluzbowe) {
		this.onRXSet("delCzynnosciSluzbowe", this.delCzynnosciSluzbowe, delCzynnosciSluzbowe);
		this.delCzynnosciSluzbowe = delCzynnosciSluzbowe;
	}

	public PptDelCzynnosciSluzbowe addDelCzynnosciSluzbowe(PptDelCzynnosciSluzbowe delCzynnosciSluzbowe) {
		this.onRXSet("delCzynnosciSluzbowe", null, this.delCzynnosciSluzbowe);
		getDelCzynnosciSluzbowe().add(delCzynnosciSluzbowe);
		delCzynnosciSluzbowe.setDeltKalkulacje(this);

		return delCzynnosciSluzbowe;
	}

	public PptDelCzynnosciSluzbowe removeDelCzynnosciSluzbowe(PptDelCzynnosciSluzbowe delCzynnosciSluzbowe) {
		this.onRXSet("delCzynnosciSluzbowe", null, this.delCzynnosciSluzbowe);
		getDelCzynnosciSluzbowe().remove(delCzynnosciSluzbowe);
		delCzynnosciSluzbowe.setDeltKalkulacje(null);

		return delCzynnosciSluzbowe;
	}
	
	
	
}
