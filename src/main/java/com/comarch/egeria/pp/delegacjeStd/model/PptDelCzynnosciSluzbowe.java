package com.comarch.egeria.pp.delegacjeStd.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.comarch.egeria.pp.data.model.ModelBase;


@Entity
@Table(name="PPT_DEL_CZYNNOSCI_SLUZBOWE", schema="PPADM")
@NamedQuery(name="PptDelCzynnosciSluzbowe.findAll", query="SELECT d FROM PptDelCzynnosciSluzbowe d")
public class PptDelCzynnosciSluzbowe extends DelBase implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_CZYNNOSCI_SLUZBOWE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_CZYNNOSCI_SLUZBOWE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_CZYNNOSCI_SLUZBOWE")	
	@Column(name="CZYN_ID")
	private long czynId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CZYN_CZYNNOSCI_OD")
	private Date czynCzynnosciOd;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CZYN_CZYNNOSCI_DO")
	private Date czynCzynnosciDo;
	
	@Column(name="CZYN_OPIS")
	private String czynOpis;
	
	@Column(name="CZYN_CZAS")
	private Double czynCzas;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CZYN_AUDYT_DM")
	private Date czynAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CZYN_AUDYT_DT")
	private Date czynAudytDt;

	@Column(name="CZYN_AUDYT_KM")
	private String czynAudytKm;

	@Column(name="CZYN_AUDYT_LM")
	private String czynAudytLm;

	@Column(name="CZYN_AUDYT_UM")
	private String czynAudytUm;

	@Column(name="CZYN_AUDYT_UT")
	private String czynAudytUt;
	
	//bi-directional many-to-one association to PptDelWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="CZYN_KAL_ID")
	private PptDelKalkulacje deltKalkulacje;
	
	
	@Column(name="CZYN_KRAJ_ID")
	private Long czynKrajId;
	
	@Column(name="CZYN_CZAS_PRYWATNY")
	private Double czynCzasPrywatny;
	
	
//	public PptDelCzynnosciSluzbowe clone(){
//		
//		PptDelCzynnosciSluzbowe cz =  new PptDelCzynnosciSluzbowe();
//		cz.setCzynId(0L);
//		cz.setCzynCzynnosciOd(czynCzynnosciOd);
//		cz.setCzynCzynnosciDo(czynCzynnosciDo);
//		cz.setCzynCzas(czynCzas);
//		cz.setCzynOpis(czynOpis);
//		cz.setCzynKrajId(czynKrajId);
//		cz.setDeltKalkulacje(null);
//		
//		return cz;
//	}
//	

	public long getCzynId() {
		this.onRXGet("czynId");
		return this.czynId;
	}

	public void setCzynId(long czynId) {
		this.onRXSet("czynId", this.czynId, czynId);
		this.czynId = czynId;
	}

	public Date getCzynCzynnosciOd() {
		this.onRXGet("czynCzynnosciOd");
		return this.czynCzynnosciOd;
	}

	public void setCzynCzynnosciOd(Date czynCzynnosciOd) {
		this.onRXSet("czynCzynnosciOd", this.czynCzynnosciOd, czynCzynnosciOd);
		this.czynCzynnosciOd = czynCzynnosciOd;
		
		if(this.czynCzynnosciOd != null && this.czynCzynnosciDo != null)
			this.setCzynCzas((0.000 + this.czynCzynnosciDo.getTime() - czynCzynnosciOd.getTime()) / (1000 * 60 * 60));
	}

	public Date getCzynCzynnosciDo() {
		this.onRXGet("czynCzynnosciDo");
		return this.czynCzynnosciDo;
	}

	public void setCzynCzynnosciDo(Date czynCzynnosciDo) {
		this.onRXSet("czynCzynnosciDo", this.czynCzynnosciDo, czynCzynnosciDo);
		this.czynCzynnosciDo = czynCzynnosciDo;
		
		if(this.czynCzynnosciOd != null && czynCzynnosciDo != null)
			this.setCzynCzas((0.000 + czynCzynnosciDo.getTime() - this.czynCzynnosciOd.getTime()) / (1000 * 60 * 60));
	}

	public String getCzynOpis() {
		this.onRXGet("czynOpis");
		return this.czynOpis;
	}

	public void setCzynOpis(String czynOpis) {
		this.onRXSet("czynOpis", this.czynOpis, czynOpis);
		this.czynOpis = czynOpis;
	}

	public Double getCzynCzas() {
		this.onRXGet("czynCzas");
		return this.czynCzas;
	}

	public void setCzynCzas(Double czynCzas) {
		this.onRXSet("czynCzas", this.czynCzas, czynCzas);
		this.czynCzas = czynCzas;
	}

	public Date getCzynAudytDm() {
		this.onRXGet("czynAudytDm");
		return this.czynAudytDm;
	}

	public void setCzynAudytDm(Date czynAudytDm) {
		this.onRXSet("czynAudytDm", this.czynAudytDm, czynAudytDm);
		this.czynAudytDm = czynAudytDm;
	}

	public Date getCzynAudytDt() {
		this.onRXGet("czynAudytDt");
		return this.czynAudytDt;
	}

	public void setCzynAudytDt(Date czynAudytDt) {
		this.onRXSet("czynAudytDt", this.czynAudytDt, czynAudytDt);
		this.czynAudytDt = czynAudytDt;
	}

	public String getCzynAudytKm() {
		this.onRXGet("czynAudytKm");
		return this.czynAudytKm;
	}

	public void setCzynAudytKm(String czynAudytKm) {
		this.onRXSet("czynAudytKm", this.czynAudytKm, czynAudytKm);
		this.czynAudytKm = czynAudytKm;
	}

	public String getCzynAudytLm() {
		this.onRXGet("czynAudytLm");
		return this.czynAudytLm;
	}

	public void setCzynAudytLm(String czynAudytLm) {
		this.onRXSet("czynAudytLm", this.czynAudytLm, czynAudytLm);
		this.czynAudytLm = czynAudytLm;
	}

	public String getCzynAudytUm() {
		this.onRXGet("czynAudytUm");
		return this.czynAudytUm;
	}

	public void setCzynAudytUm(String czynAudytUm) {
		this.onRXSet("czynAudytUm", this.czynAudytUm, czynAudytUm);
		this.czynAudytUm = czynAudytUm;
	}

	public String getCzynAudytUt() {
		this.onRXGet("czynAudytUt");
		return this.czynAudytUt;
	}

	public void setCzynAudytUt(String czynAudytUt) {
		this.onRXSet("czynAudytUt", this.czynAudytUt, czynAudytUt);
		this.czynAudytUt = czynAudytUt;
	}

	public PptDelKalkulacje getDeltKalkulacje() {
		this.onRXGet("deltKalkulacje");
		return this.deltKalkulacje;
	}

	public void setDeltKalkulacje(PptDelKalkulacje deltKalkulacje) {
		this.onRXSet("deltKalkulacje", this.deltKalkulacje, deltKalkulacje);
		this.deltKalkulacje = deltKalkulacje;
	}

	public Long getCzynKrajId() {
		this.onRXGet("czynKrajId");
		return this.czynKrajId;
	}

	public void setCzynKrajId(Long czynKrajId) {
		this.onRXSet("czynKrajId", this.czynKrajId, czynKrajId);
		this.czynKrajId = czynKrajId;
	}

	public Double getCzynCzasPrywatny() {
		this.onRXGet("czynCzasPrywatny");
		return this.czynCzasPrywatny;
	}

	public void setCzynCzasPrywatny(Double czynCzasPrywatny) {
		this.onRXSet("czynCzasPrywatny", this.czynCzasPrywatny, czynCzasPrywatny);
		this.czynCzasPrywatny = czynCzasPrywatny;
	}

}
