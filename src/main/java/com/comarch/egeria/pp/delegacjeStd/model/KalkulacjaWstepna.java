package com.comarch.egeria.pp.delegacjeStd.model;

import javax.persistence.Entity;

@Entity
public class KalkulacjaWstepna extends Kalkulacja {
	private static final long serialVersionUID = 1L;
	
	
	public KalkulacjaWstepna() {
		
	}
	
	
	public KalkulacjaWstepna ustawDomyslne() {
		
		this.setKalRodzaj("WST");
		
		return this;
	}
	
	
}
