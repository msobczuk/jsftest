package com.comarch.egeria.pp.delegacjeStd.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.comarch.egeria.pp.data.model.ModelBase;


/**
 * The persistent class for the PPT_DEL_SRODKI_LOKOMOCJI database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_SRODKI_LOKOMOCJI", schema="PPADM")
@NamedQuery(name="PptDelSrodkiLokomocji.findAll", query="SELECT d FROM PptDelSrodkiLokomocji d")
public class PptDelSrodkiLokomocji extends DelBase implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_SRODKI_LOKOMOCJI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_SRODKI_LOKOMOCJI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_SRODKI_LOKOMOCJI")	
	@Column(name="SLOK_ID")
	private long slokId;

	@Temporal(TemporalType.DATE)
	@Column(name="SLOK_AUDYT_DM")
	private Date slokAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="SLOK_AUDYT_DT")
	private Date slokAudytDt;

	@Column(name="SLOK_AUDYT_KM")
	private String slokAudytKm;

//	@Column(name="SLOK_AUDYT_KT")
//	private String slokAudytKt;

	@Column(name="SLOK_AUDYT_LM")
	private String slokAudytLm;

	@Column(name="SLOK_AUDYT_UM")
	private String slokAudytUm;

	@Column(name="SLOK_AUDYT_UT")
	private String slokAudytUt;

//	@Column(name="SLOK_LP")
//	private Long slokLp;

/*	@Column(name="SLOK_MARKA")
	private String slokMarka;*/

	@Column(name="SLOK_OPIS_SRODKA_LOKOMOCJI")
	private String slokOpisSrodkaLokomocji;

/*	@Column(name="SLOK_POJEMNOSC")
	private String slokPojemnosc;*/

	@Column(name="SLOK_SRODEK_LOKOMOCJI")
	private String slokSrodekLokomocji;
	
	@Column(name="SLOK_F_CZY_UMOWA_Z_WYK")
	private String slokFCzyUmowaZWyk;
	
	@Column(name="SLOK_F_CZY_ZGODA_POJAZD_PRYW")
	private String slokFCzyZgodaPojazdPryw;
	
	@Column(name="SLOK_F_CZY_ZGODA_POJAZD_SLU")
	private String slokFCzyZgodaPojazdSlu;

	//bi-directional many-to-one association to DeltWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="SLOK_WND_ID")
	private PptDelWnioskiDelegacji deltWnioskiDelegacji;


//	public PptDelSrodkiLokomocji clone(){
//		
//		PptDelSrodkiLokomocji lok = new PptDelSrodkiLokomocji();
//		lok.setSlokId(slokId);
//		lok.setSlokAudytDm(slokAudytDm);
//		lok.setSlokAudytDt(slokAudytDt);
//		lok.setSlokAudytKm(slokAudytKm);
//		lok.setSlokAudytLm(slokAudytLm);
//		lok.setSlokAudytUm(slokAudytUm);
//		lok.setSlokAudytUt(slokAudytUt);
//		lok.setSlokOpisSrodkaLokomocji(slokOpisSrodkaLokomocji);
//		lok.setSlokSrodekLokomocji(slokSrodekLokomocji);
//		lok.setDeltWnioskiDelegacji(null); //rodzic! 
//		lok.setSlokFCzyUmowaZWyk(slokFCzyUmowaZWyk);
//		lok.setSlokFCzyZgodaPojazdPryw(slokFCzyZgodaPojazdPryw);
//		lok.setSlokFCzyZgodaPojazdSlu(slokFCzyZgodaPojazdSlu);
//		
//		return lok;
//	}
//	
	
	
	public PptDelSrodkiLokomocji() {
	}

	public long getSlokId() {
		this.onRXGet("slokId");
		return this.slokId;
	}

	public void setSlokId(long slokId) {
		this.onRXSet("slokId", this.slokId, slokId);
		this.slokId = slokId;
	}

	public Date getSlokAudytDm() {
		this.onRXGet("slokAudytDm");
		return this.slokAudytDm;
	}

	public void setSlokAudytDm(Date slokAudytDm) {
		this.onRXSet("slokAudytDm", this.slokAudytDm, slokAudytDm);
		this.slokAudytDm = slokAudytDm;
	}

	public Date getSlokAudytDt() {
		this.onRXGet("slokAudytDt");
		return this.slokAudytDt;
	}

	public void setSlokAudytDt(Date slokAudytDt) {
		this.onRXSet("slokAudytDt", this.slokAudytDt, slokAudytDt);
		this.slokAudytDt = slokAudytDt;
	}

	public String getSlokAudytKm() {
		this.onRXGet("slokAudytKm");
		return this.slokAudytKm;
	}

	public void setSlokAudytKm(String slokAudytKm) {
		this.onRXSet("slokAudytKm", this.slokAudytKm, slokAudytKm);
		this.slokAudytKm = slokAudytKm;
	}

//	public String getSlokAudytKt() {
//		return this.slokAudytKt;
//	}
//
//	public void setSlokAudytKt(String slokAudytKt) {
//		this.slokAudytKt = slokAudytKt;
//	}

	public String getSlokAudytLm() {
		this.onRXGet("slokAudytLm");
		return this.slokAudytLm;
	}

	public void setSlokAudytLm(String slokAudytLm) {
		this.onRXSet("slokAudytLm", this.slokAudytLm, slokAudytLm);
		this.slokAudytLm = slokAudytLm;
	}

	public String getSlokAudytUm() {
		this.onRXGet("slokAudytUm");
		return this.slokAudytUm;
	}

	public void setSlokAudytUm(String slokAudytUm) {
		this.onRXSet("slokAudytUm", this.slokAudytUm, slokAudytUm);
		this.slokAudytUm = slokAudytUm;
	}

	public String getSlokAudytUt() {
		this.onRXGet("slokAudytUt");
		return this.slokAudytUt;
	}

	public void setSlokAudytUt(String slokAudytUt) {
		this.onRXSet("slokAudytUt", this.slokAudytUt, slokAudytUt);
		this.slokAudytUt = slokAudytUt;
	}

//	public Long getSlokLp() {
//		return this.slokLp;
//	}

//	public void setSlokLp(Long slokLp) {
//		this.slokLp = slokLp;
//	}

/*	public String getSlokMarka() {
		this.onRXGet("slokMarka");
		return this.slokMarka;
	}*/

/*	public void setSlokMarka(String slokMarka) {
		this.onRXSet("slokMarka", this.slokMarka, slokMarka);
		this.slokMarka = slokMarka;
	}*/

	public String getSlokOpisSrodkaLokomocji() {
		this.onRXGet("slokOpisSrodkaLokomocji");
		return this.slokOpisSrodkaLokomocji;
	}

	public void setSlokOpisSrodkaLokomocji(String slokOpisSrodkaLokomocji) {
		this.onRXSet("slokOpisSrodkaLokomocji", this.slokOpisSrodkaLokomocji, slokOpisSrodkaLokomocji);
		this.slokOpisSrodkaLokomocji = slokOpisSrodkaLokomocji;
	}

/*	public String getSlokPojemnosc() {
		this.onRXGet("slokPojemnosc");
		return this.slokPojemnosc;
	}

	public void setSlokPojemnosc(String slokPojemnosc) {
		this.onRXSet("slokPojemnosc", this.slokPojemnosc, slokPojemnosc);
		this.slokPojemnosc = slokPojemnosc;
	}*/

	public String getSlokSrodekLokomocji() {
		this.onRXGet("slokSrodekLokomocji");
		return this.slokSrodekLokomocji;
	}

	public void setSlokSrodekLokomocji(String slokSrodekLokomocji) {
		this.onRXSet("slokSrodekLokomocji", this.slokSrodekLokomocji, slokSrodekLokomocji);
		this.slokSrodekLokomocji = slokSrodekLokomocji;
	}

	public PptDelWnioskiDelegacji getDeltWnioskiDelegacji() {
		this.onRXGet("deltWnioskiDelegacji");
		return this.deltWnioskiDelegacji;
	}

	public void setDeltWnioskiDelegacji(PptDelWnioskiDelegacji deltWnioskiDelegacji) {
		this.onRXSet("deltWnioskiDelegacji", this.deltWnioskiDelegacji, deltWnioskiDelegacji);
		this.deltWnioskiDelegacji = deltWnioskiDelegacji;
	}



	public String getSlokFCzyUmowaZWyk() {
		return slokFCzyUmowaZWyk;
	}



	public void setSlokFCzyUmowaZWyk(String slokFCzyUmowaZWyk) {
		this.slokFCzyUmowaZWyk = slokFCzyUmowaZWyk;
	}



	public String getSlokFCzyZgodaPojazdPryw() {
		return slokFCzyZgodaPojazdPryw;
	}



	public void setSlokFCzyZgodaPojazdPryw(String slokFCzyZgodaPojazdPryw) {
		this.slokFCzyZgodaPojazdPryw = slokFCzyZgodaPojazdPryw;
	}



	public String getSlokFCzyZgodaPojazdSlu() {
		return slokFCzyZgodaPojazdSlu;
	}



	public void setSlokFCzyZgodaPojazdSlu(String slokFCzyZgodaPojazdSlu) {
		this.slokFCzyZgodaPojazdSlu = slokFCzyZgodaPojazdSlu;
	}

}
