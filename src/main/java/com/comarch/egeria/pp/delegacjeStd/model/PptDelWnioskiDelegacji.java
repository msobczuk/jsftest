package com.comarch.egeria.pp.delegacjeStd.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.comarch.egeria.pp.data.model.ModelBase;


/**
 * The persistent class for the PPT_DEL_WNIOSKI_DELEGACJI database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_WNIOSKI_DELEGACJI", schema="PPADM")
@NamedQuery(name="PptDelWnioskiDelegacji.findAll", query="SELECT d FROM PptDelWnioskiDelegacji d")
@DiscriminatorFormula("CASE WHEN WND_RODZAJ = 1 THEN 'DelegacjaKraj' ELSE 'DelegacjaZagr' END")
public class PptDelWnioskiDelegacji extends DelBase implements Serializable {
	private static final long serialVersionUID = 1L;
	
//	@Transient
//	private SqlBean sql;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_WNIOSKI_DELEGACJI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_WNIOSKI_DELEGACJI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_WNIOSKI_DELEGACJI")	
	@Column(name="WND_ID")
	private long wndId;
	
	@Column(name="WND_ADR_ID")
	private Long wndAdrId;

	@Temporal(TemporalType.DATE)
	@Column(name="WND_AUDYT_DM")
	private Date wndAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="WND_AUDYT_DT")
	private Date wndAudytDt;

	@Column(name="WND_AUDYT_KM")
	private String wndAudytKm;

//	@Column(name="WND_AUDYT_KT")
//	private String wndAudytKt;

	@Column(name="WND_AUDYT_LM")
	private String wndAudytLm;

	@Column(name="WND_AUDYT_UM")
	private String wndAudytUm;

	@Column(name="WND_AUDYT_UT")
	private String wndAudytUt;

	@Temporal(TemporalType.DATE)
	@Column(name="WND_DATA_POWROTU")
	private Date wndDataPowrotu;

	@Temporal(TemporalType.DATE)
	@Column(name="WND_DATA_WNIOSKU")
	private Date wndDataWniosku;

	@Temporal(TemporalType.DATE)
	@Column(name="WND_DATA_WYJAZDU")
	private Date wndDataWyjazdu;

//	@Column(name="WND_F_PREFEROWANA_GOTOWKA")
//	private String wndFPreferowanaGotowka;

	@Column(name="WND_F_WYJAZD_Z_DOMU")
	private String wndFWyjazdZDomu = "N";

	@Column(name="WND_F_PRYW")
	private String wndFPryw;
	
	@Column(name="WND_ZAL_FORMA_WYPLATY")
	private String wndZalFormaWyplaty;
	
	@Column(name="WND_ZAL_KNT_ID")
	private Long wndZalKntId;

//	@Column(name="WND_KOLACJE")
//	private Long wndKolacje = 0l;

//	@Column(name="WND_NOCLEGI")
//	private Long wndNoclegi = 0l;

	@Column(name="WND_NUMER")
	private String wndNumer;

	@Column(name="WND_OB_ID")
	private Long wndObId;

//	@Column(name="WND_OBIADY")
//	private Long wndObiady = 0l;

	@Column(name="WND_OPIS")
	private String wndOpis;

	@Column(name="WND_POLE01")
	private String wndPole01;

	@Column(name="WND_POLE02")
	private String wndPole02;

	@Column(name="WND_POLE03")
	private String wndPole03;

	@Column(name="WND_POLE04")
	private String wndPole04;

	@Column(name="WND_POLE05")
	private String wndPole05;

	@Column(name="WND_POLE06")
	private String wndPole06;

	@Column(name="WND_POLE07")
	private String wndPole07;

	@Column(name="WND_POLE08")
	private String wndPole08;

	@Column(name="WND_POLE09")
	private String wndPole09;

	@Column(name="WND_POLE10")
	private String wndPole10;

	@Column(name="WND_PRC_ID")
	private Long wndPrcId;

	@Column(name="WND_OBS_PRC_ID")
	private Long wndObsPrcId;

	@Column(name="WND_RODZAJ")
	private Long wndRodzaj;

//	@Column(name="WND_SNIADANIA")
//	private Long wndSniadania = 0l;

	@Column(name="WND_STATUS")
	private Long wndStatus;

//	@Column(name="WND_TRANSPORT")
//	private String wndTransport = "N";
	
	@Column(name="WND_WND_ID")
	private Long 	wndWndId;
	
	@Column(name="WND_F_CZY_GRUPOWA")
	private String wndCzyGrupowa;
	
	@Column(name="WND_PRYW_DATA_WYJAZDU")
	private Date wndPrywDataWyjazdu;
	
	@Column(name="WND_PRYW_DATA_POWROTU")
	private Date wndPrywDataPowrotu;
	
	@Column(name="WND_TRASA_WYJAZDU")
	private String wndTrasaWyjazdu;
	
	@Column(name="WND_TRASA_POWROTU")
	private String wndTrasaPowrotu;
	
	@Column(name="wnd_f_czy_szkol")
	private String wndFCzySzkolenie;
	
	@Column(name="WND_UWAGI")
	private String wndUwagi;
	
	@Column(name="WND_GR_UZASADNIENIE")
	private String wndGrUzasadnienie;
	
	@Column(name="WND_F_BEZKOSZTOWA")
	private String wndFBezkosztowa;
	
	@Column(name="WND_F_PROJEKTOWA")
	private String wndFProjektowa;
	
	@Column(name="WND_F_KONFERENCYJNA")
	private String wndFKonferencyjna;
	
/*	@Column(name="WND_TYP_DIETY")
	private String wndTypDiety;*/
	
//	@ManyToOne
//	@JoinColumn(name="WND_STD_ID")
//	private DelStawkiDelegacji delStawkiDelegacji;
	
	@Column(name="WND_STD_ID")
	private Long wndStdId; //TODO = 11L;
	
	@Column(name="WND_F_ZALICZKA")
	private String wndFZaliczka;
	
	@Column(name="WND_F_JEDNA_WALUTA")
	private String wndFJednaWaluta;
	
	@Column(name="WND_NUMER_NR")
	private Long wndNumerNr;
	
	@Column(name="WND_F_MIEJSCOWOSC_POBYT")
	private String wndFMiejscowoscPobyt = "N";
	
	@Column(name="WND_MIEJSCE_DOCELOWE")
	private String wndMiejsceDocelowe;
	
	@Column(name="WND_F_CZY_SWIADCZ_NIESTD")
	private String wndFCzySwiadczNiestd;
	
	@Column(name="WND_F_KONF_KOSZTOWA")
	private String wndFKonfKosztowa;
	
	@Column(name="WND_SK_ID")
	private Long wndSkId;
	
	@Column(name="WND_WNR_ID")
	private Long wndWnrId;
	
	@Column(name="WND_F_DOMYSLNE_ZF")
	private String wndFDomyslneZf;
	
	@Column(name="WND_PROJ_KOD")
	private String wndProjKod;
	
	@Column(name="WND_DOK_ID")
	private Long wndDokId;

	//bi-directional many-to-one association to DeltCeleDelegacji
	@OneToMany(mappedBy="deltWnioskiDelegacji", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("CDEL_ID ASC")
	private List<PptDelCeleDelegacji> deltCeleDelegacjis = new ArrayList<>();

//	//bi-directional many-to-one association to DeltRozliczeniaDelegacji
//	@OneToMany(mappedBy="deltWnioskiDelegacji", fetch=FetchType.LAZY, cascade={CascadeType.ALL}, orphanRemoval=true)
//	private List<DeltRozliczeniaDelegacji> deltRozliczeniaDelegacjis = new ArrayList<>();
	
	
	//bi-directional many-to-one association to DeltSrodkiLokomocji
	@OneToMany(mappedBy="deltWnioskiDelegacji", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("SLOK_ID ASC")
	private List<PptDelSrodkiLokomocji> deltSrodkiLokomocjis = new ArrayList<>();
	
	//bi-directional many-to-one association to DeltZapraszajacy
	@OneToMany(mappedBy="deltWnioskiDelegacji", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("ZAPR_ID ASC")
	private List<PptDelZapraszajacy> deltZapraszajacys = new ArrayList<>();
	
	@OneToMany(mappedBy="deltWnioskiDelegacji", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<PptDelProjekty> deltProjektys = new ArrayList<>();
	
	@OneToMany(mappedBy="deltWnioskiDelegacji", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<PptDelKalkulacje> deltKalkulacjes = new ArrayList<>();

	
	
	@OneToMany(mappedBy="deltWnioskiDelegacji", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("ODO_ID ASC")
	private List<PptDelOsobyDodatkowe> deltOsobyDodatkowes = new ArrayList<>();
	
	

	public PptDelWnioskiDelegacji() {
	}

	public long getWndId() {
		this.onRXGet("wndId");
		return this.wndId;
	}

	public void setWndId(long wndId) {
		this.onRXSet("wndId", this.wndId, wndId);
		this.wndId = wndId;
	}

	public Long getWndAdrId() {
		this.onRXGet("wndAdrId");
		return this.wndAdrId;
	}

	public void setWndAdrId(Long wndAdrId) {
		this.onRXSet("wndAdrId", this.wndAdrId, wndAdrId);
		this.wndAdrId = wndAdrId;
	}

	public Date getWndAudytDm() {
		this.onRXGet("wndAudytDm");
		return this.wndAudytDm;
	}

	public void setWndAudytDm(Date wndAudytDm) {
		this.onRXSet("wndAudytDm", this.wndAudytDm, wndAudytDm);
		this.wndAudytDm = wndAudytDm;
	}

	public Date getWndAudytDt() {
		this.onRXGet("wndAudytDt");
		return this.wndAudytDt;
	}

	public void setWndAudytDt(Date wndAudytDt) {
		this.onRXSet("wndAudytDt", this.wndAudytDt, wndAudytDt);
		this.wndAudytDt = wndAudytDt;
	}

	public String getWndAudytKm() {
		this.onRXGet("wndAudytKm");
		return this.wndAudytKm;
	}

	public void setWndAudytKm(String wndAudytKm) {
		this.onRXSet("wndAudytKm", this.wndAudytKm, wndAudytKm);
		this.wndAudytKm = wndAudytKm;
	}

//	public String getWndAudytKt() {
//		return this.wndAudytKt;
//	}

//	public void setWndAudytKt(String wndAudytKt) {
//		this.wndAudytKt = wndAudytKt;
//	}

	public String getWndAudytLm() {
		this.onRXGet("wndAudytLm");
		return this.wndAudytLm;
	}

	public void setWndAudytLm(String wndAudytLm) {
		this.onRXSet("wndAudytLm", this.wndAudytLm, wndAudytLm);
		this.wndAudytLm = wndAudytLm;
	}

	public String getWndAudytUm() {
		this.onRXGet("wndAudytUm");
		return this.wndAudytUm;
	}

	public void setWndAudytUm(String wndAudytUm) {
		this.onRXSet("wndAudytUm", this.wndAudytUm, wndAudytUm);
		this.wndAudytUm = wndAudytUm;
	}

	public String getWndAudytUt() {
		this.onRXGet("wndAudytUt");
		return this.wndAudytUt;
	}

	public void setWndAudytUt(String wndAudytUt) {
		this.onRXSet("wndAudytUt", this.wndAudytUt, wndAudytUt);
		this.wndAudytUt = wndAudytUt;
	}

	public Date getWndDataPowrotu() {
		this.onRXGet("wndDataPowrotu");
		return this.wndDataPowrotu;
	}

	public void setWndDataPowrotu(Date wndDataPowrotu) {
		this.onRXSet("wndDataPowrotu", this.wndDataPowrotu, wndDataPowrotu);
		this.wndDataPowrotu = wndDataPowrotu;
	}

	public Date getWndDataWniosku() {
		this.onRXGet("wndDataWniosku");
		return this.wndDataWniosku;
	}

	public void setWndDataWniosku(Date wndDataWniosku) {
		this.onRXSet("wndDataWniosku", this.wndDataWniosku, wndDataWniosku);
		this.wndDataWniosku = wndDataWniosku;
	}

	public Date getWndDataWyjazdu() {
		this.onRXGet("wndDataWyjazdu");
		return this.wndDataWyjazdu;
	}

	public void setWndDataWyjazdu(Date wndDataWyjazdu) {
		this.onRXSet("wndDataWyjazdu", this.wndDataWyjazdu, wndDataWyjazdu);
		this.wndDataWyjazdu = wndDataWyjazdu;
	}

//	public String getWndFPreferowanaGotowka() {
//		return this.wndFPreferowanaGotowka;
//	}
//
//	public void setWndFPreferowanaGotowka(String wndFPreferowanaGotowka) {
//		this.wndFPreferowanaGotowka = wndFPreferowanaGotowka;
//	}

	public String getWndFWyjazdZDomu() {
		this.onRXGet("wndFWyjazdZDomu");
		return this.wndFWyjazdZDomu;
	}

	public void setWndFWyjazdZDomu(String wndFWyjazdZDomu) {
		this.onRXSet("wndFWyjazdZDomu", this.wndFWyjazdZDomu, wndFWyjazdZDomu);
		this.wndFWyjazdZDomu = wndFWyjazdZDomu;
	}

	public Long getWndZalKntId() {
		this.onRXGet("wndZalKntId");
		return this.wndZalKntId;
	}

	public void setWndKntId(Long wndZalKntId) {
		this.onRXSet("wndZalKntId", this.wndZalKntId, wndZalKntId);
		this.wndZalKntId = wndZalKntId;
	}

//	public Long getWndKolacje() {
//		return this.wndKolacje;
//	}
//
//	public void setWndKolacje(Long wndKolacje) {
//		this.wndKolacje = wndKolacje;
//	}
//
//	public Long getWndNoclegi() {
//		return this.wndNoclegi;
//	}
//
//	public void setWndNoclegi(Long wndNoclegi) {
//		this.wndNoclegi = wndNoclegi;
//	}

	public String getWndNumer() {
		this.onRXGet("wndNumer");
		return this.wndNumer;
	}

	public void setWndNumer(String wndNumer) {
		this.onRXSet("wndNumer", this.wndNumer, wndNumer);
		this.wndNumer = wndNumer;
	}

	public Long getWndObId() {
		this.onRXGet("wndObId");
		return this.wndObId;
	}

	public void setWndObId(Long wndObId) {
		this.onRXSet("wndObId", this.wndObId, wndObId);
		this.wndObId = wndObId;
	}

//	public Long getWndObiady() {
//		return this.wndObiady;
//	}
//
//	public void setWndObiady(Long wndObiady) {
//		this.wndObiady = wndObiady;
//	}

	public String getWndOpis() {
		this.onRXGet("wndOpis");
		return this.wndOpis;
	}

	public void setWndOpis(String wndOpis) {
		this.onRXSet("wndOpis", this.wndOpis, wndOpis);
		this.wndOpis = wndOpis;
	}

	public String getWndPole01() {
		this.onRXGet("wndPole01");
		return this.wndPole01;
	}

	public void setWndPole01(String wndPole01) {
		this.onRXSet("wndPole01", this.wndPole01, wndPole01);
		this.wndPole01 = wndPole01;
	}

	public String getWndPole02() {
		this.onRXGet("wndPole02");
		return this.wndPole02;
	}

	public void setWndPole02(String wndPole02) {
		this.onRXSet("wndPole02", this.wndPole02, wndPole02);
		this.wndPole02 = wndPole02;
	}

	public String getWndPole03() {
		this.onRXGet("wndPole03");
		return this.wndPole03;
	}

	public void setWndPole03(String wndPole03) {
		this.onRXSet("wndPole03", this.wndPole03, wndPole03);
		this.wndPole03 = wndPole03;
	}

	public String getWndPole04() {
		this.onRXGet("wndPole04");
		return this.wndPole04;
	}

	public void setWndPole04(String wndPole04) {
		this.onRXSet("wndPole04", this.wndPole04, wndPole04);
		this.wndPole04 = wndPole04;
	}

	public String getWndPole05() {
		this.onRXGet("wndPole05");
		return this.wndPole05;
	}

	public void setWndPole05(String wndPole05) {
		this.onRXSet("wndPole05", this.wndPole05, wndPole05);
		this.wndPole05 = wndPole05;
	}

	public String getWndPole06() {
		this.onRXGet("wndPole06");
		return this.wndPole06;
	}

	public void setWndPole06(String wndPole06) {
		this.onRXSet("wndPole06", this.wndPole06, wndPole06);
		this.wndPole06 = wndPole06;
	}

	public String getWndPole07() {
		this.onRXGet("wndPole07");
		return this.wndPole07;
	}

	public void setWndPole07(String wndPole07) {
		this.onRXSet("wndPole07", this.wndPole07, wndPole07);
		this.wndPole07 = wndPole07;
	}

	public String getWndPole08() {
		this.onRXGet("wndPole08");
		return this.wndPole08;
	}

	public void setWndPole08(String wndPole08) {
		this.onRXSet("wndPole08", this.wndPole08, wndPole08);
		this.wndPole08 = wndPole08;
	}

	public String getWndPole09() {
		this.onRXGet("wndPole09");
		return this.wndPole09;
	}

	public void setWndPole09(String wndPole09) {
		this.onRXSet("wndPole09", this.wndPole09, wndPole09);
		this.wndPole09 = wndPole09;
	}

	public String getWndPole10() {
		this.onRXGet("wndPole10");
		return this.wndPole10;
	}

	public void setWndPole10(String wndPole10) {
		this.onRXSet("wndPole10", this.wndPole10, wndPole10);
		this.wndPole10 = wndPole10;
	}

	public Long getWndPrcId() {
		this.onRXGet("wndPrcId");
		return this.wndPrcId;
	}

	public void setWndPrcId(Long wndPrcId) {
		this.onRXSet("wndPrcId", this.wndPrcId, wndPrcId);
		this.wndPrcId = wndPrcId;
	}

	public Long getWndObsPrcId() {
		this.onRXGet("wndObsPrcId");
		return this.wndObsPrcId;
	}

	public void setWndObsPrcId(Long wndObsPrcId) {
		this.onRXSet("wndObsPrcId", this.wndObsPrcId, wndObsPrcId);
		this.wndObsPrcId = wndObsPrcId;
	}

	public Long getWndRodzaj() {
		this.onRXGet("wndRodzaj");
		return this.wndRodzaj;
	}

	public void setWndRodzaj(Long wndRdelId) {
		this.wndRodzaj = wndRdelId;
	}

//	public Long getWndSniadania() {
//		return this.wndSniadania;
//	}
//
//	public void setWndSniadania(Long wndSniadania) {
//		this.wndSniadania = wndSniadania;
//	}

	public Long getWndStatus() {
		this.onRXGet("wndStatus");
		return this.wndStatus;
	}

	public void setWndStatus(Long wndStwdId) {
		this.wndStatus = wndStwdId;
	}

//	public String getWndTransport() {
//		return this.wndTransport;
//	}
//
//	public void setWndTransport(String wndTransport) {
//		this.wndTransport = wndTransport;
//	}

	public List<PptDelCeleDelegacji> getDeltCeleDelegacjis() {
		this.onRXGet("deltCeleDelegacjis");
		return this.deltCeleDelegacjis;
	}

	public void setDeltCeleDelegacjis(List<PptDelCeleDelegacji> deltCeleDelegacjis) {
		this.onRXSet("deltCeleDelegacjis", this.deltCeleDelegacjis, deltCeleDelegacjis);
		this.deltCeleDelegacjis = deltCeleDelegacjis;
	}

	public PptDelCeleDelegacji addDeltCeleDelegacji(PptDelCeleDelegacji deltCeleDelegacji) {
		this.onRXSet("deltCeleDelegacjis", null, this.deltCeleDelegacjis);
		getDeltCeleDelegacjis().add(deltCeleDelegacji);
		deltCeleDelegacji.setDeltWnioskiDelegacji(this);

		return deltCeleDelegacji;
	}

	public PptDelCeleDelegacji removeDeltCeleDelegacji(PptDelCeleDelegacji deltCeleDelegacji) {
		this.onRXSet("deltCeleDelegacjis", null, this.deltCeleDelegacjis);
		getDeltCeleDelegacjis().remove(deltCeleDelegacji);
		deltCeleDelegacji.setDeltWnioskiDelegacji(null);

		return deltCeleDelegacji;
	}

	public PptDelZapraszajacy addDeltZapraszajacy(PptDelZapraszajacy deltZapraszajacy) {
		this.onRXSet("deltZapraszajacys", null, this.deltZapraszajacys);
		getDeltZapraszajacys().add(deltZapraszajacy);
		deltZapraszajacy.setDeltWnioskiDelegacji(this);

		return deltZapraszajacy;
	}

	public PptDelZapraszajacy removeDeltZapraszajacy(PptDelZapraszajacy deltZapraszajacy) {
		this.onRXSet("deltZapraszajacys", null, this.deltZapraszajacys);
		getDeltZapraszajacys().remove(deltZapraszajacy);
		deltZapraszajacy.setDeltWnioskiDelegacji(null);

		return deltZapraszajacy;
	}
	
	
	public PptDelKalkulacje addDeltKalkulacje(PptDelKalkulacje deltKalkulacje) {
		this.onRXSet("deltKalkulacjes", null, this.deltKalkulacjes);
		getDeltKalkulacjes().add(deltKalkulacje);
		deltKalkulacje.setDeltWnioskiDelegacji(this);

		return deltKalkulacje;
	}

	public PptDelKalkulacje removeDeltKalkulacje(PptDelKalkulacje deltKalkulacje) {
		this.onRXSet("deltKalkulacjes", null, this.deltKalkulacjes);
		getDeltKalkulacjes().remove(deltKalkulacje);
		deltKalkulacje.setDeltWnioskiDelegacji(null);

		return deltKalkulacje;
	}
	
	
	
	public List<PptDelSrodkiLokomocji> getDeltSrodkiLokomocjis() {
		this.onRXGet("deltSrodkiLokomocjis");
		return this.deltSrodkiLokomocjis;
	}

	public void setDeltSrodkiLokomocjis(List<PptDelSrodkiLokomocji> deltSrodkiLokomocjis) {
		this.onRXSet("deltSrodkiLokomocjis", this.deltSrodkiLokomocjis, deltSrodkiLokomocjis);
		this.deltSrodkiLokomocjis = deltSrodkiLokomocjis;
	}

	public PptDelSrodkiLokomocji addDeltSrodkiLokomocji(PptDelSrodkiLokomocji deltSrodkiLokomocji) {
		this.onRXSet("deltSrodkiLokomocjis", null, this.deltSrodkiLokomocjis);
		getDeltSrodkiLokomocjis().add(deltSrodkiLokomocji);
		deltSrodkiLokomocji.setDeltWnioskiDelegacji(this);

		return deltSrodkiLokomocji;
	}

	public PptDelSrodkiLokomocji removeDeltSrodkiLokomocji(PptDelSrodkiLokomocji deltSrodkiLokomocji) {
		this.onRXSet("deltSrodkiLokomocjis", null, this.deltSrodkiLokomocjis);
		getDeltSrodkiLokomocjis().remove(deltSrodkiLokomocji);
		deltSrodkiLokomocji.setDeltWnioskiDelegacji(null);

		return deltSrodkiLokomocji;
	}
	
	public PptDelProjekty addDeltProjekty(PptDelProjekty deltProjekty) {
		this.onRXSet("deltProjektys", null, this.deltProjektys);
		getDeltProjektys().add(deltProjekty);
		deltProjekty.setDeltWnioskiDelegacji(this);

		return deltProjekty;
	}

	public PptDelProjekty removeDeltProjekty(PptDelProjekty deltProjekty) {
		this.onRXSet("deltProjektys", null, this.deltProjektys);
		getDeltProjektys().remove(deltProjekty);
		deltProjekty.setDeltWnioskiDelegacji(null);

		return deltProjekty;
	}
	
	public List<PptDelZapraszajacy> getDeltZapraszajacys() {
		this.onRXGet("deltZapraszajacys");
		return this.deltZapraszajacys;
	}

	/**
	 * @param deltZapraszajacys the deltZapraszajacys to set
	 */
	public void setDeltZapraszajacys(List<PptDelZapraszajacy> deltZapraszajacys) {
		this.onRXSet("deltZapraszajacys", this.deltZapraszajacys, deltZapraszajacys);
		this.deltZapraszajacys = deltZapraszajacys;
	}

	/**
	 * @return the wndWndId
	 */
	public Long getWndWndId() {
		this.onRXGet("wndWndId");
		return this.wndWndId;
	}

	/**
	 * @param wndWndId the wndWndId to set
	 */
	public void setWndWndId(Long wndWndId) {
		
		
		this.onRXSet("wndWndId", this.wndWndId, wndWndId);
		this.wndWndId = wndWndId;
	}

	/**
	 * @return the wndCzyGrupowa
	 */
	public String getWndCzyGrupowa() {
		this.onRXGet("wndCzyGrupowa");
		return this.wndCzyGrupowa;
	}

	/**
	 * @param wndCzyGrupowa the wndCzyGrupowa to set
	 */
	public void setWndCzyGrupowa(String wndCzyGrupowa) {
		this.onRXSet("wndCzyGrupowa", this.wndCzyGrupowa, wndCzyGrupowa);
		this.wndCzyGrupowa = wndCzyGrupowa;
	}

	/**
	 * @return the wndPrywDataWyjazdu
	 */
	public Date getWndPrywDataWyjazdu() {
		this.onRXGet("wndPrywDataWyjazdu");
		return this.wndPrywDataWyjazdu;
	}

	/**
	 * @param wndPrywDataWyjazdu the wndPrywDataWyjazdu to set
	 */
	public void setWndPrywDataWyjazdu(Date wndPrywDataWyjazdu) {
		this.onRXSet("wndPrywDataWyjazdu", this.wndPrywDataWyjazdu, wndPrywDataWyjazdu);
		this.wndPrywDataWyjazdu = wndPrywDataWyjazdu;
	}

	/**
	 * @return the wndPrywDataPowrotu
	 */
	public Date getWndPrywDataPowrotu() {
		this.onRXGet("wndPrywDataPowrotu");
		return this.wndPrywDataPowrotu;
	}

	/**
	 * @param wndPrywDataPowrotu the wndPrywDataPowrotu to set
	 */
	public void setWndPrywDataPowrotu(Date wndPrywDataPowrotu) {
		this.onRXSet("wndPrywDataPowrotu", this.wndPrywDataPowrotu, wndPrywDataPowrotu);
		this.wndPrywDataPowrotu = wndPrywDataPowrotu;
	}

	/**
	 * @return the wndGrUzasadnienie
	 */
	public String getWndGrUzasadnienie() {
		this.onRXGet("wndGrUzasadnienie");
		return this.wndGrUzasadnienie;
	}

	/**
	 * @param wndGrUzasadnienie the wndGrUzasadnienie to set
	 */
	public void setWndGrUzasadnienie(String wndGrUzasadnienie) {
		this.onRXSet("wndGrUzasadnienie", this.wndGrUzasadnienie, wndGrUzasadnienie);
		this.wndGrUzasadnienie = wndGrUzasadnienie;
	}

	/**
	 * @return the wndTrasaWyjazdu
	 */
	public String getWndTrasaWyjazdu() {
		this.onRXGet("wndTrasaWyjazdu");
		return this.wndTrasaWyjazdu;
	}

	/**
	 * @param wndTrasaWyjazdu the wndTrasaWyjazdu to set
	 */
	public void setWndTrasaWyjazdu(String wndTrasaWyjazdu) {
		this.onRXSet("wndTrasaWyjazdu", this.wndTrasaWyjazdu, wndTrasaWyjazdu);
		this.wndTrasaWyjazdu = wndTrasaWyjazdu;
	}

	/**
	 * @return the wndTrasaPowrotu
	 */
	public String getWndTrasaPowrotu() {
		this.onRXGet("wndTrasaPowrotu");
		return this.wndTrasaPowrotu;
	}

	/**
	 * @param wndTrasaPowrotu the wndTrasaPowrotu to set
	 */
	public void setWndTrasaPowrotu(String wndTrasaPowrotu) {
		this.onRXSet("wndTrasaPowrotu", this.wndTrasaPowrotu, wndTrasaPowrotu);
		this.wndTrasaPowrotu = wndTrasaPowrotu;
	}

	/**
	 * @return the wndFCzySzkolenie
	 */
	public String getWndFCzySzkolenie() {
		this.onRXGet("wndFCzySzkolenie");
		return this.wndFCzySzkolenie;
	}

	/**
	 * @param wndFCzySzkolenie the wndFCzySzkolenie to set
	 */
	public void setWndFCzySzkolenie(String wndFCzySzkolenie) {
//		if ("T".equals(wndFCzySzkolenie)){
//			this.wndFKonferencyjna = "N";
//		}
		this.onRXSet("wndFCzySzkolenie", this.wndFCzySzkolenie, wndFCzySzkolenie);
		this.wndFCzySzkolenie = wndFCzySzkolenie;
	}

	/**
	 * @return the wndUwagi
	 */
	public String getWndUwagi() {
		this.onRXGet("wndUwagi");
		return this.wndUwagi;
	}

	/**
	 * @param wndUwagi the wndUwagi to set
	 */
	public void setWndUwagi(String wndUwagi) {
		this.onRXSet("wndUwagi", this.wndUwagi, wndUwagi);
		this.wndUwagi = wndUwagi;
	}

	/**
	 * @return the deltProjektys
	 */
	public List<PptDelProjekty> getDeltProjektys() {
		this.onRXGet("deltProjektys");
		return this.deltProjektys;
	}

	/**
	 * @param deltProjektys the deltProjektys to set
	 */
	public void setDeltProjektys(List<PptDelProjekty> deltProjektys) {
		this.onRXSet("deltProjektys", this.deltProjektys, deltProjektys);
		this.deltProjektys = deltProjektys;
	}

	/**
	 * @return the deltKalkulacjes
	 */
	public List<PptDelKalkulacje> getDeltKalkulacjes() {
		return deltKalkulacjes;
	}

	/**
	 * @param deltKalkulacjes the deltKalkulacjes to set
	 */
	public void setDeltKalkulacjes(List<PptDelKalkulacje> deltKalkulacjes) {
		this.deltKalkulacjes = deltKalkulacjes;
	}

	public String getWndFPryw() {
		this.onRXGet("wndFPryw");
		return this.wndFPryw;
	}

	public void setWndFPryw(String wndFPryw) {
		this.onRXSet("wndFPryw", this.wndFPryw, wndFPryw);
		this.wndFPryw = wndFPryw;
	}

	/**
	 * @return the wndFBezkosztowa
	 */
	public String getWndFBezkosztowa() {
		this.onRXGet("wndFBezkosztowa");
		return this.wndFBezkosztowa;
	}

	/**
	 * @param wndFBezkosztowa the wndFBezkosztowa to set
	 */
	public void setWndFBezkosztowa(String wndFBezkosztowa) {
		this.onRXSet("wndFBezkosztowa", this.wndFBezkosztowa, wndFBezkosztowa);
		this.wndFBezkosztowa = wndFBezkosztowa;
	}

	/**
	 * @return the wndFProjektowa
	 */
	public String getWndFProjektowa() {
		this.onRXGet("wndFProjektowa");
		return this.wndFProjektowa;
	}

	/**
	 * @param wndFProjektowa the wndFProjektowa to set
	 */
	public void setWndFProjektowa(String wndFProjektowa) {
		this.onRXSet("wndFProjektowa", this.wndFProjektowa, wndFProjektowa);
		this.wndFProjektowa = wndFProjektowa;
	}

	/**
	 * @return the wndFKonferencyjna
	 */
	public String getWndFKonferencyjna() {
		this.onRXGet("wndFKonferencyjna");
		return this.wndFKonferencyjna;
	}

	/**
	 * @param wndFKonferencyjna the wndFKonferencyjna to set
	 */
	public void setWndFKonferencyjna(String wndFKonferencyjna) {
//		if ("T".equals(wndFKonferencyjna)){
//			this.wndFCzySzkolenie = "N";
//		}
		this.onRXSet("wndFKonferencyjna", this.wndFKonferencyjna, wndFKonferencyjna);
		this.wndFKonferencyjna = wndFKonferencyjna;
	}

	public String getWndZalFormaWyplaty() {
		this.onRXGet("wndZalFormaWyplaty");
		return this.wndZalFormaWyplaty;
	}

	public void setWndZalFormaWyplaty(String wndZalFormaWyplaty) {
		this.onRXSet("wndZalFormaWyplaty", this.wndZalFormaWyplaty, wndZalFormaWyplaty);
		this.wndZalFormaWyplaty = wndZalFormaWyplaty;
	}

	public Long getWndStdId() {
		this.onRXGet("wndStdId");
		return this.wndStdId;
	}

	public void setWndStdId(Long wndStdId) {
		this.onRXSet("wndStdId", this.wndStdId, wndStdId);
		this.wndStdId = wndStdId;
	}

	public String getWndFZaliczka() {
		this.onRXGet("wndFZaliczka");
		return this.wndFZaliczka;
	}

	public void setWndFZaliczka(String wndFZaliczka) {
		this.onRXSet("wndFZaliczka", this.wndFZaliczka, wndFZaliczka);
		this.wndFZaliczka = wndFZaliczka;
	}

	public String getWndFJednaWaluta() {
		this.onRXGet("wndFJednaWaluta");
		return this.wndFJednaWaluta;
	}

	public void setWndFJednaWaluta(String wndFJednaWaluta) {
		this.onRXSet("wndFJednaWaluta", this.wndFJednaWaluta, wndFJednaWaluta);
		this.wndFJednaWaluta = wndFJednaWaluta;
	}

	public Long getWndNumerNr() {
		this.onRXGet("wndNumerNr");
		return this.wndNumerNr;
	}

	public void setWndNumerNr(Long wndNumerNr) {
		this.onRXSet("wndNumerNr", this.wndNumerNr, wndNumerNr);
		this.wndNumerNr = wndNumerNr;
	}


	public String getWndFMiejscowoscPobyt() {
		this.onRXGet("wndFMiejscowoscPobyt");
		return this.wndFMiejscowoscPobyt;
	}

	public void setWndFMiejscowoscPobyt(String wndFMiejscowoscPobyt) {
		this.onRXSet("wndFMiejscowoscPobyt", this.wndFMiejscowoscPobyt, wndFMiejscowoscPobyt);
		this.wndFMiejscowoscPobyt = wndFMiejscowoscPobyt;
	}

	public String getWndMiejsceDocelowe() {
		this.onRXGet("wndMiejsceDocelowe");
		return this.wndMiejsceDocelowe;
	}

	public void setWndMiejsceDocelowe(String wndMiejsceDocelowe) {
		this.onRXSet("wndMiejsceDocelowe", this.wndMiejsceDocelowe, wndMiejsceDocelowe);
		this.wndMiejsceDocelowe = wndMiejsceDocelowe;
	}

	public String getWndFCzySwiadczNiestd() {
		this.onRXGet("wndFCzySwiadczNiestd");
		return this.wndFCzySwiadczNiestd;
	}

	public void setWndFCzySwiadczNiestd(String wndFCzySwiadczNiestd) {
		this.onRXSet("wndFCzySwiadczNiestd", this.wndFCzySwiadczNiestd, wndFCzySwiadczNiestd);
		this.wndFCzySwiadczNiestd = wndFCzySwiadczNiestd;
	}

	public String getWndFKonfKosztowa() {
		this.onRXGet("wndFKonfKosztowa");
		return this.wndFKonfKosztowa;
	}

	public void setWndFKonfKosztowa(String wndFKonfKosztowa) {
		this.onRXSet("wndFKonfKosztowa", this.wndFKonfKosztowa, wndFKonfKosztowa);
		this.wndFKonfKosztowa = wndFKonfKosztowa;
	}

	public Long getWndSkId() {
		this.onRXGet("wndSkId");
		return this.wndSkId;
	}

	public void setWndSkId(Long wndSkId) {
		this.onRXSet("wndSkId", this.wndSkId, wndSkId);
		this.wndSkId = wndSkId;
	}
	

	public void setWndWnrId(Long wndWnrId) {
		this.wndWnrId = wndWnrId;
	}

	public String getWndFDomyslneZf() {
		this.onRXGet("wndFDomyslneZf");
		return this.wndFDomyslneZf;
	}

	public void setWndFDomyslneZf(String wndFDomyslneZf) {
		this.onRXSet("wndFDomyslneZf", this.wndFDomyslneZf, wndFDomyslneZf);
		this.wndFDomyslneZf = wndFDomyslneZf;
	}

	public String getWndProjKod() {
		this.onRXGet("wndProjKod");
		return this.wndProjKod;
	}

	public void setWndProjKod(String wndProjKod) {
		this.onRXSet("wndProjKod", this.wndProjKod, wndProjKod);
		this.wndProjKod = wndProjKod;
	}

	public Long getWndDokId() {
		this.onRXGet("wndDokId");
		return this.wndDokId;
	}

	public void setWndDokId(Long wndDokId) {
		this.onRXSet("wndDokId", this.wndDokId, wndDokId);
		this.wndDokId = wndDokId;
	}

	public List<PptDelOsobyDodatkowe> getDeltOsobyDodatkowes() {
		this.onRXGet("deltOsobyDodatkowes");
		return this.deltOsobyDodatkowes;
	}

	public void setDeltOsobyDodatkowes(List<PptDelOsobyDodatkowe> delOsobyDodatkowe) {
		this.onRXSet("deltOsobyDodatkowes", this.deltOsobyDodatkowes, delOsobyDodatkowe);
		this.deltOsobyDodatkowes = delOsobyDodatkowe;
	}
	
	public PptDelOsobyDodatkowe addDeltOsobyDodatkowe(PptDelOsobyDodatkowe delOsobyDodatkowe) {
		this.onRXSet("deltOsobyDodatkowes", null, this.deltOsobyDodatkowes);
		getDeltOsobyDodatkowes().add(delOsobyDodatkowe);
		delOsobyDodatkowe.setDeltWnioskiDelegacji(this);

		return delOsobyDodatkowe;
	}

	public PptDelOsobyDodatkowe removeDeltOsobyDodatkowe(PptDelOsobyDodatkowe delOsobyDodatkowe) {
		this.onRXSet("deltOsobyDodatkowes", null, this.deltOsobyDodatkowes);
		getDeltOsobyDodatkowes().remove(delOsobyDodatkowe);
		delOsobyDodatkowe.setDeltWnioskiDelegacji(null);

		return delOsobyDodatkowe;
	}


}
