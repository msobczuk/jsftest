package com.comarch.egeria.pp.delegacjeStd.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.comarch.egeria.pp.data.model.ModelBase;


/**
 * The persistent class for the PPT_DEL_ZRODLA_FINANSOWANIA database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_ZRODLA_FINANSOWANIA", schema="PPADM")
@NamedQuery(name="PptDelZrodlaFinansowania.findAll", query="SELECT d FROM PptDelZrodlaFinansowania d")
public class PptDelZrodlaFinansowania extends DelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="PKZF_AUDYT_DM")
	private Date pkzfAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="PKZF_AUDYT_DT")
	private Date pkzfAudytDt;

	@Column(name="PKZF_AUDYT_KM")
	private String pkzfAudytKm;

	@Column(name="PKZF_AUDYT_LM")
	private String pkzfAudytLm;

	@Column(name="PKZF_AUDYT_UM")
	private String pkzfAudytUm;

	@Column(name="PKZF_AUDYT_UT")
	private String pkzfAudytUt;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_ZRODLA_FINANSOWANIA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_ZRODLA_FINANSOWANIA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_ZRODLA_FINANSOWANIA")	
	@Column(name="PKZF_ID")
	private long pkzfId;

	@Column(name="PKZF_KWOTA")
	private Double pkzfKwota;

	@Column(name="PKZF_OPIS")
	private String pkzfOpis;

	@Column(name="PKZF_PROCENT")
	private Double pkzfProcent;

	@Column(name="PKZF_SK_ID")
	private Long pkzfSkId;

	//bi-directional many-to-one association to DeltPozycjeKalkulacji
	@ManyToOne
	@JoinColumn(name="PKZF_PKL_ID")
	private PptDelPozycjeKalkulacji deltPozycjeKalkulacji;
	

	public PptDelZrodlaFinansowania() {
	}

	public Date getPkzfAudytDm() {
		this.onRXGet("pkzfAudytDm");
		return this.pkzfAudytDm;
	}

	public void setPkzfAudytDm(Date pkzfAudytDm) {
		this.onRXSet("pkzfAudytDm", this.pkzfAudytDm, pkzfAudytDm);
		this.pkzfAudytDm = pkzfAudytDm;
	}

	public Date getPkzfAudytDt() {
		this.onRXGet("pkzfAudytDt");
		return this.pkzfAudytDt;
	}

	public void setPkzfAudytDt(Date pkzfAudytDt) {
		this.onRXSet("pkzfAudytDt", this.pkzfAudytDt, pkzfAudytDt);
		this.pkzfAudytDt = pkzfAudytDt;
	}

	public String getPkzfAudytKm() {
		this.onRXGet("pkzfAudytKm");
		return this.pkzfAudytKm;
	}

	public void setPkzfAudytKm(String pkzfAudytKm) {
		this.onRXSet("pkzfAudytKm", this.pkzfAudytKm, pkzfAudytKm);
		this.pkzfAudytKm = pkzfAudytKm;
	}

	public String getPkzfAudytLm() {
		this.onRXGet("pkzfAudytLm");
		return this.pkzfAudytLm;
	}

	public void setPkzfAudytLm(String pkzfAudytLm) {
		this.onRXSet("pkzfAudytLm", this.pkzfAudytLm, pkzfAudytLm);
		this.pkzfAudytLm = pkzfAudytLm;
	}

	public String getPkzfAudytUm() {
		this.onRXGet("pkzfAudytUm");
		return this.pkzfAudytUm;
	}

	public void setPkzfAudytUm(String pkzfAudytUm) {
		this.onRXSet("pkzfAudytUm", this.pkzfAudytUm, pkzfAudytUm);
		this.pkzfAudytUm = pkzfAudytUm;
	}

	public String getPkzfAudytUt() {
		this.onRXGet("pkzfAudytUt");
		return this.pkzfAudytUt;
	}

	public void setPkzfAudytUt(String pkzfAudytUt) {
		this.onRXSet("pkzfAudytUt", this.pkzfAudytUt, pkzfAudytUt);
		this.pkzfAudytUt = pkzfAudytUt;
	}

	public long getPkzfId() {
		this.onRXGet("pkzfId");
		return this.pkzfId;
	}

	public void setPkzfId(long pkzfId) {
		this.onRXSet("pkzfId", this.pkzfId, pkzfId);
		this.pkzfId = pkzfId;
	}

	public Double getPkzfKwota() {
		this.onRXGet("pkzfKwota");
		return this.pkzfKwota;
	}

	public void setPkzfKwota(Double pkzfKwota) {
		this.onRXSet("pkzfKwota", this.pkzfKwota, pkzfKwota);				
		this.pkzfKwota = pkzfKwota;				
	}

	public String getPkzfOpis() {
		this.onRXGet("pkzfOpis");
		return this.pkzfOpis;
	}

	public void setPkzfOpis(String pkzfOpis) {
		this.onRXSet("pkzfOpis", this.pkzfOpis, pkzfOpis);
		this.pkzfOpis = pkzfOpis;
	}

	public Double getPkzfProcent() {
		this.onRXGet("pkzfProcent");
		return this.pkzfProcent;
	}

	public void setPkzfProcent(Double pkzfProcent) {
		this.onRXSet("pkzfProcent", this.pkzfProcent, pkzfProcent);
		this.pkzfProcent = pkzfProcent;
	}

	public Long getPkzfSkId() {
		this.onRXGet("pkzfSkId");
		return this.pkzfSkId;
	}

	public void setPkzfSkId(Long pkzfSkId) {
		this.onRXSet("pkzfSkId", this.pkzfSkId, pkzfSkId);
		this.pkzfSkId = pkzfSkId;
	}

	public PptDelPozycjeKalkulacji getDeltPozycjeKalkulacji() {
		this.onRXGet("deltPozycjeKalkulacji");
		return this.deltPozycjeKalkulacji;
	}

	public void setDeltPozycjeKalkulacji(PptDelPozycjeKalkulacji deltPozycjeKalkulacji) {
		this.onRXSet("deltPozycjeKalkulacji", this.deltPozycjeKalkulacji, deltPozycjeKalkulacji);
		this.deltPozycjeKalkulacji = deltPozycjeKalkulacji;
	}
	
}
