package com.comarch.egeria.pp.delegacjeStd.model;

import java.util.ArrayList;

import javax.persistence.Entity;

import com.comarch.egeria.Utils;
import com.comarch.egeria.web.User;

@Entity
public class DelegacjaKraj extends Delegacja {
	private static final long serialVersionUID = 1L;
	
	public DelegacjaKraj ustawDomyslne() {
		DelegacjaKraj ret = (DelegacjaKraj) super.ustawDomyslne();
		ret.setWndRodzaj(1L);
		PptDelCeleDelegacji celDelegacji = this.getDeltCeleDelegacjis().get(0);
		celDelegacji.setCdelMiejscowoscOd("Tarnów");
		celDelegacji.setCdelMiejscowoscZakonczenia("Tarnów");
		 //TODO ustawienia typowe dla nowej delegacji KRaj.Zagr
		 return ret;
	}
	
	
	public void generujPlanTrasy(Kalkulacja kalk) {
		
		kalk.wyczyscTrasy();
	
		String miejscowoscPoczatkowa = this.getDeltCeleDelegacjis().get(0).getCdelMiejscowoscOd();
		String miejscowoscDocelowa = this.getDeltCeleDelegacjis().get(this.getDeltCeleDelegacjis().size()-1).getCdelMiejscowosc();
		String miejscowoscKoncowa = this.getDeltCeleDelegacjis().get(this.getDeltCeleDelegacjis().size()-1).getCdelMiejscowoscZakonczenia();
		String slokSrodekLokomocji = this.getDeltSrodkiLokomocjis().get(0).getSlokSrodekLokomocji();
		
		
		//TODO rozbudować walidację i dać do osobnej metody
		if(miejscowoscPoczatkowa == null || miejscowoscPoczatkowa.isEmpty()) {
			User.alert("Miejscowość rozpoczęcia delegacji nie może być pusta");
			return;
		} 	else if(miejscowoscDocelowa == null || miejscowoscDocelowa.isEmpty()) {
			User.alert("Miejscowosć docelowa nie może być pusta");
			return;
		} 	else if(miejscowoscKoncowa == null || miejscowoscKoncowa.isEmpty()) {
			User.alert("Miejscowość zakończenia delegacji nie może być pusta");
			return;
		} 	else if(slokSrodekLokomocji == null || slokSrodekLokomocji.isEmpty()) {
			User.alert("Należy wybrać środek lokomocji");
			return;
		} 
			
			PptDelTrasy trsStart = new PptDelTrasy();
			PptDelTrasy trsKoniec = new PptDelTrasy();
			trsStart.setTrsKrIdOd(1L);
			trsStart.setTrsKrIdDo(1L);
			trsStart.setTrsSrodekLokomocji(slokSrodekLokomocji);
			trsStart.setTrsDataOd(this.getWndDataWyjazdu());
			trsStart.setTrsMiejscowoscOd(miejscowoscPoczatkowa);
			trsStart.setTrsMiejscowoscDo(miejscowoscDocelowa);
			
			trsKoniec.setTrsKrIdOd(1L);
			trsKoniec.setTrsKrIdDo(1L);
			trsKoniec.setTrsSrodekLokomocji(slokSrodekLokomocji);
			trsKoniec.setTrsDataDo(this.getWndDataPowrotu());
			trsKoniec.setTrsMiejscowoscOd(miejscowoscDocelowa);
			trsKoniec.setTrsMiejscowoscDo(miejscowoscKoncowa);
			kalk.addDeltTrasy(trsStart);
			kalk.addDeltTrasy(trsKoniec);
	}
	
	public void generujRozliczenie() {
		double kwotaPobranejZaliczki = 0.0;
		
		if( eq("T", this.getWndFZaliczka()))
				kwotaPobranejZaliczki = this.getKalkulacjaWniosek().zwrocKwoteZaliczek();
		
		double kwotaWydanychPkl = this.getKalkulacjaRozlicz().zwrocKwoteDoRozliczen();
		
		this.getKalkulacjaRozlicz().getDelZaliczki().clear();
		
		PptDelZaliczki zal = new PptDelZaliczki();
		zal.setDkzWalId(1L);
		if(kwotaPobranejZaliczki > 0.0)
			zal.setDkzKwota(kwotaWydanychPkl - kwotaPobranejZaliczki);
		else
			zal.setDkzKwota(kwotaWydanychPkl);
		
		this.getKalkulacjaRozlicz().addDeltZaliczki(zal);
	}
	
	
		
	public boolean validate () {
		ArrayList<String> errorList = new ArrayList<>();
		
		if(this.getWndMiejsceDocelowe() == null || this.getWndMiejsceDocelowe().isEmpty())
			errorList.add("Należy podać miejsce docelowe podróży");
		
		if(this.getDeltCeleDelegacjis().get(0).getCdelMiejscowosc() == null || this.getDeltCeleDelegacjis().get(0).getCdelMiejscowosc().isEmpty())
			errorList.add("Miejscowosć docelowa nie może być pusta");
		if(this.getDeltCeleDelegacjis().get(0).getCdelMiejscowoscOd()== null || this.getDeltCeleDelegacjis().get(0).getCdelMiejscowoscOd().isEmpty())
			errorList.add("Miejscowość rozpoczęcia delegacji nie może być pusta");
		if(this.getDeltCeleDelegacjis().get(0).getCdelMiejscowoscZakonczenia()== null || this.getDeltCeleDelegacjis().get(0).getCdelMiejscowoscZakonczenia().isEmpty())
			errorList.add("Miejscowość zakończenia delegacji nie może być pusta");
		
		if(!this.getKalkulacjaRozlicz().walidujPlanTrasy()) {
			errorList.add("Błędne dane w planie trasy");
		}
		
		for (String er :errorList) {
			User.alert(er);
		}
		
		return super.validate() && errorList.isEmpty();
	}
	
	public void zapisz () {
		super.zapisz();
		if(this.getWndNumer() != null)
			Utils.setFormName("Delegacja krajowa nr: " + this.getWndNumer());
	}
	
}
