package com.comarch.egeria.pp.delegacjeStd.model;

import static com.comarch.egeria.Utils.nz;
import static com.comarch.egeria.Utils.round;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

@Entity
public class PozycjaKalkulacji extends PptDelPozycjeKalkulacji {
	private static final long serialVersionUID = 1L;
	
	
	public PozycjaKalkulacji ustawDomyslne() {
		this.setPklIlosc(1.0);
		this.setPklStawka(0.0);
		this.setPklRefundacjaKwota(0.0);
		this.setPklWalId(1L);
		this.setPklOplacajacy("01");
		
		return this;
	}
	
	public PozycjaKalkulacji ustawDomyslnePklPobytowa() {
		this.setPklIlosc(1.0);
		this.setPklStawka(0.0);
		this.setPklRefundacjaKwota(0.0);
		this.setPklWalId(1L);
		this.setPklOplacajacy("01");
		this.setPklSniadania(null);
		this.setPklObiady(null);
		this.setPklKolacje(null);
		
		return this;
	}
	
	
	public String getTypPozycjiKod() {
		DataRow r = getTypPozycjiDataRow();
		if (r!=null) return (String) r.get("TPOZ_KOD");
		return null;
	}

	public String getTypPozycjiKategoria() {
		DataRow r = getTypPozycjiDataRow();
		if (r!=null) return (String) r.get("TPOZ_KATEGORIA");
		return null;
	}

	public String getTypPozycjiNazwa() {
		DataRow r = getTypPozycjiDataRow();
		if (r!=null) return (String) r.get("TPOZ_NAZWA");
		return null;
	}
	
	public String getTypPozycjiOpis() {
		DataRow r = getTypPozycjiDataRow();
		if (r!=null) return (String) r.get("TPOZ_OPIS");
		return null;
	}
		
	public DataRow getTypPozycjiDataRow() {
		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_TYPY_POZYCJI");
		DataRow r = lw.find(this.getPklTpozId());
		return r;
	}
	
	
	public void przeliczKwotePkl() {
		Long pklIlosc =  ((Double) (nz(this.getPklIlosc())  * 10000)).longValue() ;
		Long pklStawka = ((Double) (nz(this.getPklStawka()) * 10000)).longValue() ; 
		
		if (this.getPklIlosc() == 0.0 || this.getPklStawka() == 0.0) {
			this.setPklKwota(0.0);
			return;
		}
		
		Long ret = 0l;
		ret = (pklIlosc * pklStawka);

		if (ret < 0l) ret = 0l;
		this.setPklKwota(round((1.0d * ret) / (10000*10000), 2));
		
	}
	
	public void przeliczKwotePklPobytowa() {
		Long pklIlosc =  ((Double) (nz(this.getPklIlosc())  * 10000)).longValue() ;
		Long pklStawka = ((Double) (nz(this.getPklStawka()) * 10000)).longValue() ; 
		
		Long ret = 0l;
	
		ret = (pklIlosc * pklStawka);
		if(this.getDeltKalkulacje().getDeltWnioskiDelegacji().getWndRodzaj()==2) {
			ret -= nz(this.getPklSniadania()) * pklStawka * 1500; //0.25;
			ret -= nz(this.getPklObiady()) * pklStawka * 3000;// 0.50;
			ret -= nz(this.getPklKolacje()) * pklStawka * 1500; // 0.25;
			
		} else {
			ret -= nz(this.getPklSniadania()) * pklStawka * 2500; //0.15;
			ret -= nz(this.getPklObiady()) * pklStawka * 5000;// 0.30;
			ret -= nz(this.getPklKolacje()) * pklStawka * 2500; // 0.30;
		}
		
		if (ret < 0l) ret = 0l;
		this.setPklKwota(round((1.0d * ret) / (10000*10000), 2));
	}
	
	
	public Long getTpozIdByKodPozycji(String kodPozycji) {
		Long wndStdId = this.getDeltKalkulacje().getDeltWnioskiDelegacji().getWndStdId();
		Long wndRodzaj = this.getDeltKalkulacje().getDeltWnioskiDelegacji().getWndRodzaj();
		SqlDataSelectionsHandler lw;
		Long ret = null;
		if(wndRodzaj.equals(1L))
			lw = this.getLW("PPL_DEL_TYPY_POZYCJI_K");
		else 
			lw = this.getLW("PPL_DEL_TYPY_POZYCJI_Z");
		for(DataRow r : lw.getAllData()) {
			if(r.get("tpoz_kod").equals(kodPozycji)) {
				Object oTpozId = r.get("tpoz_id");
				ret = Long.parseLong(""+oTpozId);
				break;
			}
		}
		return ret;
	}
	
	public void ustawStawkeiWaluteDietyZagr() {
		if(eq(this.getTypPozycjiKategoria(), "I"))
			return;
		Date pobytOd = this.getDeltKalkulacje().getDeltWnioskiDelegacji().getWndDataWyjazdu();
		pobytOd = pobytOd == null ? Utils.today() : pobytOd;
		String idInna="";
		SqlDataSelectionsHandler lw;
		if(this.getTypPozycjiKategoria().equals("P") || this.getTypPozycjiKategoria().equals("KM") || this.getTypPozycjiKategoria().equals("RD"))
			lw = this.getLW("PPL_DEL_STAWKI_DIET_ZAGR", this.getDeltKalkulacje().getDeltWnioskiDelegacji().getWndStdId());
		else lw = this.getLW("PPL_DEL_STAWKI_DIET_HOT", this.getDeltKalkulacje().getDeltWnioskiDelegacji().getWndStdId());
		
		for(DataRow r : lw.getData()) {
			if (r != null) {
				if ((""+this.getPklKrId()).equals(""+r.get("WST_KR_ID")) && pobytOd.after((Date) r.get("WST_OBOWIAZUJE_OD"))) {
					this.setPklWalId(Long.parseLong(""+r.get("WST_WAL_ID")));
					this.setPklStawka(Double.parseDouble(""+r.get("WST_KWOTA")));
					return;
				}
				if(eq(""+r.get("WST_KR_ID"), "null"))
					idInna=r.getIdAsString();
			}
		}
		DataRow inna = lw.find(idInna);
		this.setPklStawka(Double.parseDouble(""+inna.get("WST_KWOTA")));//
		this.setPklWalId(Long.parseLong( ""+inna.get("WST_WAL_ID") ));// EUR
	}
	
	public Long zwrocWalIdWgKraju(Long krajId){
		if(krajId==null)
			return 0L; 
		SqlDataSelectionsHandler waluty = this.getLW("PPL_KRAJE_WALUTY_ALL");
		DataRow r = waluty.find(krajId);
		if(r == null)
			return 1L;
		BigDecimal walId = (BigDecimal) r.get("WAL_ID");
		return walId.longValue();
	}
	
	@Override 
	public void setPklIlosc(Double pklIlosc) {
		super.setPklIlosc(pklIlosc);
		if("P".equals(this.getTypPozycjiKategoria())) {
			this.przeliczKwotePklPobytowa();
		} else 
		super.setPklKwota(pklIlosc*nz(this.getPklStawka()));
	}
	
	@Override 
	public void setPklStawka(Double pklStawka) {
		super.setPklStawka(pklStawka);
		if("P".equals(this.getTypPozycjiKategoria())) {
			this.przeliczKwotePklPobytowa();
		} else 
		super.setPklKwota(this.getPklIlosc()*pklStawka);
	}
	
	@Override 
	public void setPklSniadania(Long pklSniadania) {
		super.setPklSniadania(pklSniadania);
		if("P".equals(this.getTypPozycjiKategoria()))
			this.przeliczKwotePklPobytowa();
	}
	
	@Override 
	public void setPklObiady(Long pklObiady) {
		super.setPklObiady(pklObiady);
		if("P".equals(this.getTypPozycjiKategoria()))
			this.przeliczKwotePklPobytowa();
	}
	
	@Override 
	public void setPklKolacje(Long pklKolacje) {
		super.setPklKolacje(pklKolacje);
		if("P".equals(this.getTypPozycjiKategoria()))
			this.przeliczKwotePklPobytowa();
	}
	
	@Override 
	public void setPklKrId(Long pklKrId) {
		if (eq(pklKrId, this.getPklKrId())) return;
		super.setPklKrId(pklKrId);
		this.ustawStawkeiWaluteDietyZagr();
		if ("RH".equals(this.getTypPozycjiKategoria()) ){
			this.setPklStawka(this.getPklStawka()*0.25);
		}
		if ("KM".equals(this.getTypPozycjiKategoria()) ){
			this.setPklStawka(this.getPklStawka()*0.1);
		}
		if ("I".equals(this.getTypPozycjiKategoria())) {
			this.setPklStawka(0.0);
			this.setPklWalId(zwrocWalIdWgKraju(pklKrId));
		}
	}
}
