package com.comarch.egeria.pp.delegacjeStd.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.comarch.egeria.pp.data.model.ModelBase;


/**
 * The persistent class for the PPT_DEL_ZALICZKI database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_ZALICZKI")
@NamedQuery(name="PptDelZaliczki.findAll", query="SELECT d FROM PptDelZaliczki d")
public class PptDelZaliczki extends DelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_ZALICZKI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_ZALICZKI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_ZALICZKI")
	@Column(name="DKZ_ID")
	private long dkzId;

	@Column(name="DKZ_FORMA_WYPLATY")
	private String dkzFormaWyplaty;
	
	@ManyToOne
	@JoinColumn(name="DKZ_KAL_ID")
	private PptDelKalkulacje deltKalkulacje;

	@Column(name="DKZ_KWOTA")
	private Double dkzKwota;

	@Column(name="DKZ_OPIS")
	private String dkzOpis;

	@Column(name="DKZ_WAL_ID")
	private Long dkzWalId;
	
	
	@Column(name="DKZ_AUDYT_UT")
	private String dkzAudytUt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DKZ_AUDYT_DT")
	private Date dkzAudytDt;
	
	@Column(name="DKZ_AUDYT_UM")
	private String dkzAudytUm;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DKZ_AUDYT_DM")
	private Date dkzAudytDm;
	
	@Column(name="DKZ_AUDYT_LM")
	private BigDecimal dkzAudytLm;

	@Column(name="DKZ_AUDYT_KM")
	private String dkzAudytKm;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DKZ_DATA_WYP")
	private Date dkzDataWyp;
	
	@Column(name="DKZ_DOK_ID")
	private Long dkzDokId;
	
	@Column(name="DKZ_NR_KONTA")
	private String dkzNrKonta;
	
	@Column(name="DKZ_KARTA_KWOTA")
	private Double dkzKartaKwota;
	
	@Column(name="DKZ_ZAL_WYDANO")
	private Double dkzZalWydano;
	
	@Column(name="DKZ_SRODKI_WLASNE")
	private Double dkzSrodkiWlasne;
	
	@Column(name="DKZ_KURS")
	private Double dkzKurs;
	
	@Column(name="DKZ_CZY_ROWNOWARTOSC")
	private String dkzCzyRownowartosc;


	public PptDelZaliczki() {
	}

	public long getDkzId() {
		this.onRXGet("dkzId");
		return this.dkzId;
	}

	public void setDkzId(long dkzId) {
		this.onRXSet("dkzId", this.dkzId, dkzId);
		this.dkzId = dkzId;
	}

	public String getDkzFormaWyplaty() {
		this.onRXGet("dkzFormaWyplaty");
		return this.dkzFormaWyplaty;
	}

	public void setDkzFormaWyplaty(String dkzFormaWyplaty) {
		this.onRXSet("dkzFormaWyplaty", this.dkzFormaWyplaty, dkzFormaWyplaty);
		this.dkzFormaWyplaty = dkzFormaWyplaty;
	}

	public Double getDkzKwota() {
		this.onRXGet("dkzKwota");
		return this.dkzKwota;
	}

	public void setDkzKwota(Double dkzKwota) {
		this.onRXSet("dkzKwota", this.dkzKwota, dkzKwota);
		this.dkzKwota = dkzKwota;
	}

	public String getDkzOpis() {
		this.onRXGet("dkzOpis");
		return this.dkzOpis;
	}

	public void setDkzOpis(String dkzOpis) {
		this.onRXSet("dkzOpis", this.dkzOpis, dkzOpis);
		this.dkzOpis = dkzOpis;
	}

	public Long getDkzWalId() {
		this.onRXGet("dkzWalId");
		return this.dkzWalId;
	}

	public void setDkzWalId(Long dkzWalId) {
		this.onRXSet("dkzWalId", this.dkzWalId, dkzWalId);
		this.dkzWalId = dkzWalId;
	}

	public PptDelKalkulacje getDeltKalkulacje() {
		this.onRXGet("deltKalkulacje");
		return this.deltKalkulacje;
	}

	public void setDeltKalkulacje(PptDelKalkulacje deltKalkulacje) {
		this.onRXSet("deltKalkulacje", this.deltKalkulacje, deltKalkulacje);
		this.deltKalkulacje = deltKalkulacje;
	}

	public String getDkzAudytUt() {
		this.onRXGet("dkzAudytUt");
		return this.dkzAudytUt;
	}

	public void setDkzAudytUt(String dkzAudytUt) {
		this.onRXSet("dkzAudytUt", this.dkzAudytUt, dkzAudytUt);
		this.dkzAudytUt = dkzAudytUt;
	}

	public Date getDkzAudytDt() {
		this.onRXGet("dkzAudytDt");
		return this.dkzAudytDt;
	}

	public void setDkzAudytDt(Date dkzAudytDt) {
		this.onRXSet("dkzAudytDt", this.dkzAudytDt, dkzAudytDt);
		this.dkzAudytDt = dkzAudytDt;
	}

	public String getDkzAudytUm() {
		this.onRXGet("dkzAudytUm");
		return this.dkzAudytUm;
	}

	public void setDkzAudytUm(String dkzAudytUm) {
		this.onRXSet("dkzAudytUm", this.dkzAudytUm, dkzAudytUm);
		this.dkzAudytUm = dkzAudytUm;
	}

	public Date getDkzAudytDm() {
		this.onRXGet("dkzAudytDm");
		return this.dkzAudytDm;
	}

	public void setDkzAudytDm(Date dkzAudytDm) {
		this.onRXSet("dkzAudytDm", this.dkzAudytDm, dkzAudytDm);
		this.dkzAudytDm = dkzAudytDm;
	}

	public BigDecimal getDkzAudytLm() {
		this.onRXGet("dkzAudytLm");
		return this.dkzAudytLm;
	}

	public void setDkzAudytLm(BigDecimal dkzAudytLm) {
		this.onRXSet("dkzAudytLm", this.dkzAudytLm, dkzAudytLm);
		this.dkzAudytLm = dkzAudytLm;
	}

	public String getDkzAudytKm() {
		this.onRXGet("dkzAudytKm");
		return this.dkzAudytKm;
	}

	public void setDkzAudytKm(String dkzAudytKm) {
		this.onRXSet("dkzAudytKm", this.dkzAudytKm, dkzAudytKm);
		this.dkzAudytKm = dkzAudytKm;
	}

	public Date getDkzDataWyp() {
		this.onRXGet("dkzDataWyp");
		return this.dkzDataWyp;
	}

	public void setDkzDataWyp(Date dkzDataWyp) {
		this.onRXSet("dkzDataWyp", this.dkzDataWyp, dkzDataWyp);
		this.dkzDataWyp = dkzDataWyp;
	}

	public Long getDkzDokId() {
		this.onRXGet("dkzDokId");
		return this.dkzDokId;
	}

	public void setDkzDokId(Long dkzDokId) {
		this.onRXSet("dkzDokId", this.dkzDokId, dkzDokId);
		this.dkzDokId = dkzDokId;
	}

	public String getDkzNrKonta() {
		this.onRXGet("dkzNrKonta");
		return this.dkzNrKonta;
	}

	public void setDkzNrKonta(String dkzNrKonta) {
		this.onRXSet("dkzNrKonta", this.dkzNrKonta, dkzNrKonta);
		this.dkzNrKonta = dkzNrKonta;
	}

	public Double getDkzKartaKwota() {
		this.onRXGet("dkzKartaKwota");
		return this.dkzKartaKwota;
	}

	public void setDkzKartaKwota(Double dkzKartaKwota) {
		this.onRXSet("dkzKartaKwota", this.dkzKartaKwota, dkzKartaKwota);
		this.dkzKartaKwota = dkzKartaKwota;
	}

	public Double getDkzZalWydano() {
		this.onRXGet("dkzZalWydano");
		return this.dkzZalWydano;
	}

	public void setDkzZalWydano(Double dkzZalWydano) {
		this.onRXSet("dkzZalWydano", this.dkzZalWydano, dkzZalWydano);
		this.dkzZalWydano = dkzZalWydano;
	}

	public Double getDkzSrodkiWlasne() {
		this.onRXGet("dkzSrodkiWlasne");
		return this.dkzSrodkiWlasne;
	}

	public void setDkzSrodkiWlasne(Double dkzSrodkiWlasne) {
		this.onRXSet("dkzSrodkiWlasne", this.dkzSrodkiWlasne, dkzSrodkiWlasne);
		this.dkzSrodkiWlasne = dkzSrodkiWlasne;
	}

	public Double getDkzKurs() {
		this.onRXGet("dkzKurs");
		return this.dkzKurs;
	}

	public void setDkzKurs(Double dkzKurs) {
		this.onRXSet("dkzKurs", this.dkzKurs, dkzKurs);
		this.dkzKurs = dkzKurs;
	}

	public String getDkzCzyRownowartosc() {
		this.onRXGet("dkzCzyRownowartosc");
		return this.dkzCzyRownowartosc;
	}

	public void setDkzCzyRownowartosc(String dkzCzyRownowartosc) {
		this.onRXSet("dkzCzyRownowartosc", this.dkzCzyRownowartosc, dkzCzyRownowartosc);
		this.dkzCzyRownowartosc = dkzCzyRownowartosc;
	}

}
