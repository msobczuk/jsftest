package com.comarch.egeria.pp.delegacjeStd.model;

import java.util.stream.Collectors;

import javax.persistence.Entity;

@Entity
public class KalkulacjaWniosek extends Kalkulacja {
	private static final long serialVersionUID = 1L;
	
	
	public KalkulacjaWniosek() {
		
	}
	
	public KalkulacjaWniosek ustawDomyslne() {
		
		this.setKalRodzaj("WNK");
		
		PptDelZaliczki zaliczka = new PptDelZaliczki();
		
		zaliczka.setDkzWalId(1L);
		this.addDeltZaliczki(zaliczka);
		
		return this;
	}
	
	public double zwrocKwoteZaliczek() {
		double ret = 0.0;
		
		Double sumaKwotDkz = this.getDelZaliczki().stream()
				.filter(dkz -> dkz.getDkzKwota() != null && dkz.getDkzKwota()>0.0)
				.collect(Collectors.summingDouble(dkz -> dkz.getDkzKwota()));
		if(sumaKwotDkz > 0.0)
			ret = sumaKwotDkz;
		
		return ret;
	}
	
	
	
	
}
