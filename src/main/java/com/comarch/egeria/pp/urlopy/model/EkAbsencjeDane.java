package com.comarch.egeria.pp.urlopy.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_ABS_ABSENCJE_DANE database table.
 * 
 */
@Entity
@Table(name="PPT_ABS_ABSENCJE_DANE", schema="PPADM")
@NamedQuery(name="EkAbsencjeDane.findAll", query="SELECT e FROM EkAbsencjeDane e")
public class EkAbsencjeDane implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ABS_ABSENCJE_DANE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ABS_ABSENCJE_DANE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ABS_ABSENCJE_DANE")
	@Column(name="AB_ID")
	private long abId;

	@Column(name="AB_AUDYT_KM")
	private String abAudytKm;

	@Column(name="AB_AUDYT_KT")
	private String abAudytKt;

	@Column(name="AB_AUDYT_LM")
	private BigDecimal abAudytLm;

	@Temporal(TemporalType.DATE)
	@Column(name="AB_DATA_DO")
	private Date abDataDo;

	@Temporal(TemporalType.DATE)
	@Column(name="AB_DATA_LICZENIA")
	private Date abDataLiczenia;

	@Temporal(TemporalType.DATE)
	@Column(name="AB_DATA_OD")
	private Date abDataOd;

	@Column(name="AB_DNI_WYKORZYSTANE")
	private Long abDniWykorzystane;

	@Column(name="AB_F_ANULOWANA")
	private String abFAnulowana;

	@Column(name="AB_F_CZESC")
	private String abFCzesc;

	@Column(name="AB_F_MODYFIKACJA")
	private String abFModyfikacja;

	@Column(name="AB_F_WALORYZACJA")
	private String abFWaloryzacja;

	@Column(name="AB_FRM_ID")
	private Long abFrmId;

	@Column(name="AB_GODZINY_WYKORZYSTANE")
	private Long abGodzinyWykorzystane;

	@Temporal(TemporalType.DATE)
	@Column(name="AB_AUDYT_DM")
	private Date abAudytDM;

	@Temporal(TemporalType.DATE)
	@Column(name="AB_AUDYT_DT")
	private Date abAudytDT;

	@Column(name="AB_KOD_DODATKOWY")
	private String abKodDodatkowy;

	@Column(name="AB_KOD_FUNDUSZU")
	private String abKodFunduszu;

	@Column(name="AB_KOD_UB")
	private Long abKodUb;

	@Column(name="AB_KOD_ZUS")
	private Long abKodZus;

	@Column(name="AB_AUDYT_UM")
	private String abAudytUM;

	@Column(name="AB_AUDYT_UT")
	private String abAudytUT;

	@Column(name="AB_OGONEK")
	private Long abOgonek;

	@Column(name="AB_PROCENT_FUNDUSZU")
	private Long abProcentFunduszu;

//	@Column(name="AB_STATUS")
//	private String abStatus;

	//bi-directional many-to-one association to EkZwolnienia
	@ManyToOne
	@JoinColumn(name="AB_ZWOL_ID")
	private EkZwolnienia ekZwolnienia;

	public EkAbsencjeDane() {
	}

	public long getAbId() {
		return this.abId;
	}

	public void setAbId(long abId) {
		this.abId = abId;
	}

	public String getAbAudytKm() {
		return this.abAudytKm;
	}

	public void setAbAudytKm(String abAudytKm) {
		this.abAudytKm = abAudytKm;
	}

	public String getAbAudytKt() {
		return this.abAudytKt;
	}

	public void setAbAudytKt(String abAudytKt) {
		this.abAudytKt = abAudytKt;
	}

	public BigDecimal getAbAudytLm() {
		return this.abAudytLm;
	}

	public void setAbAudytLm(BigDecimal abAudytLm) {
		this.abAudytLm = abAudytLm;
	}

	public Date getAbDataDo() {
		return this.abDataDo;
	}

	public void setAbDataDo(Date abDataDo) {
		this.abDataDo = abDataDo;
	}

	public Date getAbDataLiczenia() {
		return this.abDataLiczenia;
	}

	public void setAbDataLiczenia(Date abDataLiczenia) {
		this.abDataLiczenia = abDataLiczenia;
	}

	public Date getAbDataOd() {
		return this.abDataOd;
	}

	public void setAbDataOd(Date abDataOd) {
		this.abDataOd = abDataOd;
	}

	public Long getAbDniWykorzystane() {
		return this.abDniWykorzystane;
	}

	public void setAbDniWykorzystane(Long abDniWykorzystane) {
		this.abDniWykorzystane = abDniWykorzystane;
	}

	public String getAbFAnulowana() {
		return this.abFAnulowana;
	}

	public void setAbFAnulowana(String abFAnulowana) {
		this.abFAnulowana = abFAnulowana;
	}

	public String getAbFCzesc() {
		return this.abFCzesc;
	}

	public void setAbFCzesc(String abFCzesc) {
		this.abFCzesc = abFCzesc;
	}

	public String getAbFModyfikacja() {
		return this.abFModyfikacja;
	}

	public void setAbFModyfikacja(String abFModyfikacja) {
		this.abFModyfikacja = abFModyfikacja;
	}

	public String getAbFWaloryzacja() {
		return this.abFWaloryzacja;
	}

	public void setAbFWaloryzacja(String abFWaloryzacja) {
		this.abFWaloryzacja = abFWaloryzacja;
	}

	public Long getAbFrmId() {
		return this.abFrmId;
	}

	public void setAbFrmId(Long abFrmId) {
		this.abFrmId = abFrmId;
	}

	public Long getAbGodzinyWykorzystane() {
		return this.abGodzinyWykorzystane;
	}

	public void setAbGodzinyWykorzystane(Long abGodzinyWykorzystane) {
		this.abGodzinyWykorzystane = abGodzinyWykorzystane;
	}

	public Date getAbAudytDM() {
		return this.abAudytDM;
	}

	public void setAbAudytDM(Date abAudytDM) {
		this.abAudytDM = abAudytDM;
	}

	public Date getAbAudytDT() {
		return this.abAudytDT;
	}

	public void setAbAudytDT(Date abAudytDT) {
		this.abAudytDT = abAudytDT;
	}

	public String getAbKodDodatkowy() {
		return this.abKodDodatkowy;
	}

	public void setAbKodDodatkowy(String abKodDodatkowy) {
		this.abKodDodatkowy = abKodDodatkowy;
	}

	public String getAbKodFunduszu() {
		return this.abKodFunduszu;
	}

	public void setAbKodFunduszu(String abKodFunduszu) {
		this.abKodFunduszu = abKodFunduszu;
	}

	public Long getAbKodUb() {
		return this.abKodUb;
	}

	public void setAbKodUb(Long abKodUb) {
		this.abKodUb = abKodUb;
	}

	public Long getAbKodZus() {
		return this.abKodZus;
	}

	public void setAbKodZus(Long abKodZus) {
		this.abKodZus = abKodZus;
	}

	public String getAbAudytUM() {
		return this.abAudytUM;
	}

	public void setAbAudytUM(String abAudytUM) {
		this.abAudytUM = abAudytUM;
	}

	public String getAbAudytUT() {
		return this.abAudytUT;
	}

	public void setAbAudytUT(String abAudytUT) {
		this.abAudytUT = abAudytUT;
	}

	public Long getAbOgonek() {
		return this.abOgonek;
	}

	public void setAbOgonek(Long abOgonek) {
		this.abOgonek = abOgonek;
	}

	public Long getAbProcentFunduszu() {
		return this.abProcentFunduszu;
	}

	public void setAbProcentFunduszu(Long abProcentFunduszu) {
		this.abProcentFunduszu = abProcentFunduszu;
	}

//	public String getAbStatus() {
//		return this.abStatus;
//	}
//
//	public void setAbStatus(String abStatus) {
//		this.abStatus = abStatus;
//	}

	public EkZwolnienia getEkZwolnienia() {
		return this.ekZwolnienia;
	}

	public void setEkZwolnienia(EkZwolnienia ekZwolnienia) {
		this.ekZwolnienia = ekZwolnienia;
	}

}