package com.comarch.egeria.pp.urlopy;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.urlopy.model.PptUrlopyWnioskowane;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import net.sf.jasperreports.engine.JRException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

import static com.comarch.egeria.Utils.asSqlDate;
import static com.comarch.egeria.Utils.trunc;

//import org.primefaces.context.RequestContext;

@ManagedBean 
@Named
@Scope("view")
public class WnUrlopowyBean2 extends SqlBean {
	private static final Logger log = LogManager.getLogger();

	@Inject
	User user;
	
	private Integer rok = 0;//pole rok ustawiane na widoku
	private ArrayList<Integer> lata = new ArrayList<>();
	private Date dzien = new Date();
	
	private PptUrlopyWnioskowane currentWniosek = new PptUrlopyWnioskowane();
		
	//pole, które ze słownika odczytuje czy pole z pracownikiem rozaptrującym urlop ma byc wyswietlane czy nie
	//uwaga przy podgrywaniu słownikow do Tarnowa, trzeba bedzie o tym pamietac
	private String czyRozpatrujacy = null;

	@PostConstruct
	public void init(){
		this.setRok((LocalDate.now().getYear()));
		
		nowyWniosek();
		
		this.lata.add(rok);
		this.lata.add(rok - 1);
		this.lata.add(rok - 2);
		this.lata.add(rok - 3);
		this.lata.add(rok - 4);
		
		this.setCzyRozpatrujacy();		
		
		//"PPS_ABS_KOLORY_TYPY"
		this.getSL("PPS_ABS_KOLORY_TYPY").get("wniosek").get("wsl_opis");
	}
	
	
	/**
	 * Wczytujemy dla kliknietego/wskazanego dnia wnioski (takze godzinowe w l. mnogiej) ktore sa na ten dzien (lub jego czesc) aktualne
	 * Jako current domyslnie ustawiamy pierwszy z wniosków godzinowych z danego dnia.
	 * Jesli to nie są godzinowe to jest tam tylko jeden wionsek 
	 * @param naDzien
	 */
	public void wczytajWniosek(Date naDzien){
		
		ArrayList<PptUrlopyWnioskowane> wnioskiNaDzien = new ArrayList<>();
		String hql = " FROM PptUrlopyWnioskowane x WHERE x.uprwPrcId = ?0 and ?1 BETWEEN trunc(x.uprwDataOd) and trunc(x.uprwDataDo) ORDER BY x.uprwDataOd";
		wnioskiNaDzien.addAll(HibernateContext.query(hql, user.getPrcIdLong(), naDzien) );
		if(!wnioskiNaDzien.isEmpty()) {//wybranie daty dla ktorej jest juz jakis wniosek
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(naDzien);
			
			PptUrlopyWnioskowane naWybranyDzien = wnioskiNaDzien.get(0);
			naWybranyDzien.setRok(calendar.get(Calendar.YEAR));
			setCurrentWniosek(wnioskiNaDzien.get(0));
		}else {//tworzenie nowego wniosku
			Date dataOd = this.currentWniosek.getUprwDataOd();
            Date dataDo = this.currentWniosek.getUprwDataDo();
            
            if(dataOd == null && dataDo == null) {//nie wybrano jeszcze zadnej daty
            	getCurrentWniosek().setUprwDataOd(naDzien);
            }
            else if (dataOd != null && dataDo == null) {//wybrano juz date od ale jeszcze nie date do
            	getCurrentWniosek().setUprwDataDo(naDzien);
            }
            else if (dataOd != null && dataDo != null){//mamy zaznaczony wniosek ktory ma juz obie daty, w takim przypadku trzeba chyba utworzyc nowy wniosek
            	                                       //wybraną date wprowadzic do daty rozpoczecia urlopu, trzeba pewnie tez przpeisac wszystkie pola 
            	                                       //takie jak grupa absencji, rda_kod, pracownik zastepujacy oraz zatwierdzajacy z poprzedniego urlopu
            	String dgKod = getCurrentWniosek().getUprwDgKod();
            	String rdaKod = getCurrentWniosek().getUprwRdaKod();
            	Integer zastPrcId = getCurrentWniosek().getUprwZastPrcId();
            	Long zatwPrcId = getCurrentWniosek().getUprwZatwPrcId();
            	Integer rdaId = getCurrentWniosek().getUprwRdaId();
            	
            	nowyWniosek();
            	getCurrentWniosek().setUprwDgKod(dgKod);
            	getCurrentWniosek().setUprwRdaKod(rdaKod);
            	getCurrentWniosek().setUprwZastPrcId(zastPrcId);
            	getCurrentWniosek().setUprwZatwPrcId(zatwPrcId);
            	getCurrentWniosek().setUprwRdaId(rdaId);
            	
            	getCurrentWniosek().setUprwDataOd(naDzien);
            	getCurrentWniosek().setUprwDataDo(null);
            }
            
		}
				
/*		if(this.currentWniosek.getUprwDataOd()!=null && this.currentWniosek.getUprwDataDo()!=null){
			RequestContext.getCurrentInstance().update("formData");
			RequestContext.getCurrentInstance().update("formPlanuj");
			RequestContext.getCurrentInstance().update("frmTMnu");
		} else {
			RequestContext.getCurrentInstance().update("formPlanuj:datyGodziny");
			RequestContext.getCurrentInstance().update("formPlanuj:dtDo");
			RequestContext.getCurrentInstance().update("formPlanuj:liczbaDni");
			RequestContext.getCurrentInstance().update("formPlanuj:liczbaGodzin");
		}*/
		
		com.comarch.egeria.Utils.update("formData");
		com.comarch.egeria.Utils.update("formPlanuj");
		com.comarch.egeria.Utils.update("frmTMnu");
		
	}
	
	public void nowyWniosek(){
			PptUrlopyWnioskowane nowy = new PptUrlopyWnioskowane();
			nowy.setUprwPrcId(user.getPrcIdLong());
			nowy.setRok(rok);
			setCurrentWniosek(nowy);
			
			
			String dg_kod = null;
			try {
				Optional<DataRow> findFirst = currentWniosek.getDgKodLW().getData().stream().filter(r -> ("A_UR1".equals(""+r.get("DG_KOD")))).findFirst();
				dg_kod = (String) findFirst.get().get("DG_KOD");
			} catch (Exception e) {
				if (!currentWniosek.getDgKodLW().getData().isEmpty())
					dg_kod = (String) currentWniosek.getDgKodLW().getData().get(0).get("DG_KOD");
			}
			if(dg_kod!=null) currentWniosek.setUprwDgKod(dg_kod);
		}
	

	
	public Integer getRok() {
		return rok;
	}

	public void setRok(Integer naRok) {	
		if(eq(this.rok, naRok))
			return;
		
		this.rok = naRok;
		this.getLW("PPL_ABS_URLOPY_WNIOSKOW").setSqlParams( rok, user.getPrcIdLong() );
		this.getLW("PPL_ABS_ABSENCJE").setSqlParams( rok, user.getPrcIdLong() );
		this.getLW("PPL_SWIETA").setSqlParams(rok);
		this.getLW("PPL_ABS_PLANOWANE").setSqlParams( rok, user.getPrcIdLong() );
		nowyWniosek();
		
	}
	
	public ArrayList<Integer> getLata() {
		return lata;
	}

	public void setLata(ArrayList<Integer> lata) {
		this.lata = lata;
	}
	
	public Date getDzien() {
		return dzien;
	}

	public void setDzien(Date dzien) {
		this.wczytajWniosek(dzien);
		this.dzien = dzien;
	}
	
	public PptUrlopyWnioskowane getCurrentWniosek() {
		return currentWniosek;
	}

	public void setCurrentWniosek(PptUrlopyWnioskowane currentWniosek) {
		this.currentWniosek = currentWniosek;
		this.currentWniosek.setSqlBean(this);
	}
	
	public SqlDataSelectionsHandler getPplAbsCzasPracy() {
		
		return getLW(
				"PPL_ABS_CZASY_PRACY", 
					user.getPrcId(), currentWniosek.getUprwDataOd(),  currentWniosek.getUprwDataDo(),
					user.getPrcId(), currentWniosek.getUprwDataDo(),  currentWniosek.getUprwDataDo() 
				);
	}
	

	
	public String getCzyRozpatrujacy() {
		return czyRozpatrujacy;
	}

	public void setCzyRozpatrujacy() {
		if(this.getSlownik("URL_KONFIGURACJA").find("URL_ROZPATRUJACY") != null)
			this.czyRozpatrujacy = "" + this.getSlownik("URL_KONFIGURACJA").find("URL_ROZPATRUJACY").get("RV_HIGH_VALUE");
		else this.czyRozpatrujacy = "N";
	}
	
	
	public void zapiszNowyUrlOdDo() throws InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		if (!validateBeforeSave())
			return;

		if(currentWniosek.getUprwStatus()!=null && currentWniosek.getUprwStatus()<0)
			currentWniosek.setUprwStatus(0);

		try {	
			HibernateContext.saveOrUpdate(currentWniosek);
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		this.getLW("PPL_ABS_URLOPY_WNIOSKOW").resetTs();
		this.getLW("PPL_ABS_RDA_KOD").resetTs();
		com.comarch.egeria.Utils.update("formData");
		com.comarch.egeria.Utils.update("formPlanuj");

	}	
	
	public void odplanujUrlopOdDo() {

		if (currentWniosek.getUprwDataOd() == null || currentWniosek.getUprwDataDo() == null) {
			
			if(czyWybranoAbsencje()){
				User.warn("Nie możesz anulować wybranej absencji");
			}else
			{
				User.warn("Wskaż urlop, który zamierzasz anulować");
			}
			nowyWniosek();
			return;
		}

		//tymczasowa mozilowsc usuwania urlopów za poprzednie lata	
/*		if (this.rok < new Date().getYear() + 1900) {

			AppBean.addMessage(FacesMessage.SEVERITY_WARN, "Nie można usunąć urlopu za poprzednie lata!");

			System.out.println("Nie można usunąć urlopu!!!!!");
			return;
		}*/

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();

			CriteriaBuilder builder = s.getCriteriaBuilder();
			CriteriaQuery<PptUrlopyWnioskowane> criteria = builder.createQuery(PptUrlopyWnioskowane.class);

			Root<PptUrlopyWnioskowane> from = criteria.from(PptUrlopyWnioskowane.class);

			Predicate p1 = builder.or( builder.between(from.get("uprwDataOd"), currentWniosek.getUprwDataOd(), currentWniosek.getUprwDataDo()),
									   builder.and(  builder.equal(from.get("uprwDataOd"), currentWniosek.getUprwDataOd()),
											         builder.equal(from.get("uprwDataDo"), currentWniosek.getUprwDataDo())) );
			

			Predicate p2 = builder.equal(from.get("uprwPrcId"), user.getPrcId());


			criteria.where(p1, p2);
		

			List<PptUrlopyWnioskowane> kasujUrlPlanowane = s.createQuery(criteria).getResultList();

			s.close();

			if (!kasujUrlPlanowane.isEmpty()) {
				for (PptUrlopyWnioskowane urpl : kasujUrlPlanowane) {
					if (urpl.getUprwStatus() == null || urpl.getUprwStatus() < 1) {
						HibernateContext.delete(urpl);
					} else {
						User.alert("Nie można usunąć wprowadzonego urlopu w dniach " + this.format(urpl.getUprwDataOd()) + " - "
								+ this.format(urpl.getUprwDataDo()) + "! Skontaktuj się z przełożonym");
					}
				}
			}


		}
		
		this.getLW("PPL_ABS_URLOPY_WNIOSKOW").resetTs();
		com.comarch.egeria.Utils.update("formData");
		com.comarch.egeria.Utils.update("formPlanuj");
		nowyWniosek();
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////Metody do walidacji////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//mozna pokombinowac moze udalo by sie walidacje zrealizowac w ten sposób, że dodawał bym daną absencje do EGR i jezeli nie walnie żadnym błędem to znaczy, że wszystko OK i usuwam tak dodaną do EGR absencje
//jezeli natomiast coś jebnie to dany komunikat daje na ekran	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public boolean validateBeforeSave() {
		ArrayList<String> inv = new ArrayList<String>();

		Date dataOd = this.currentWniosek.getUprwDataOd();
		Date dataDo = this.currentWniosek.getUprwDataDo();
		
		if(this.currentWniosek.getRdaKodRow()==null)
		{
			User.alert("Należy wybrać typ urlopu!");
			return false;
		}
		
		if (currentWniosek.getUprwRdaKod() == null) {
			inv.add("Rodzaj urlopu to pole wymagane");
		}
		
		if (currentWniosek.getUprwPrcId() == null) {
			inv.add("Musi być wybrany pracownik dla, którego jest wnioskowany urlop!");
		}
		
		if (currentWniosek.getUprwZatwPrcId() == null && "T".equals(this.czyRozpatrujacy) ) {
			inv.add("Osoba zatwierdzająca nie została wskazana!");
		}
		
		if (currentWniosek.getUprwZastPrcId() == null) {
			User.info("Nie została wybrana osoba zastępująca!");
		}
		
		if (currentWniosek.getUprwStatus() != null && currentWniosek.getUprwStatus() > 0) {
			inv.add("Nie można modyfikować tego wniosku o urlop, jest on już procedowny!");
		}
		
		
		if (dataOd == null || dataDo == null) {
			inv.add("Data początkowa i końcowa urlopu to pola wymagane");
		} else {
			//tymczasowa mozilowsc dodawania urlopów za poprzednie lata			
			/*if (dataOd.getYear() < new Date().getYear() || dataDo.getYear() < new Date().getYear()) {
							inv.add("Nie można planować urlopu za poprzednie lata!");
			}*/
			if (dataOd.after(dataDo)) {
				inv.add("Data końcowa nie może być wcześniejsza od daty początkowej");
			}
			
			if (currentWniosek.getCzyGodzinowy()) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(dataOd);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				Date data1 = cal.getTime();

				cal.setTime(dataDo);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				Date data2 = cal.getTime();

				if (data1.compareTo(data2) != 0) {
					inv.add("Urlop godzinowy może być wnioskowany w obrębie jednego dnia!");
				}
				
				if(dataOd.getHours()>=dataDo.getHours()){
					inv.add("Godzina od i do nie mogą przyjmować tej samej wartości!");
				}
				
				if(dataOd.getMinutes()!=dataDo.getMinutes()){
					inv.add("Urlop może być wprowadzony jedynie na pełne godziny!");
				}
			}
			
			DataRow absencjaKolidujacaDR = findAbsencjaKolidujaca(dataOd, dataDo);
			
			if (absencjaKolidujacaDR!=null){
				inv.add(String.format("Wprowadzono co najmniej jedną nieobecność kolidującą (od %1$s do %2$s)", this.format(absencjaKolidujacaDR.get("AB_DATA_OD")), this.format(absencjaKolidujacaDR.get("AB_DATA_DO"))  ));
			}
			
			DataRow wniosekKolidujacyDR = findWniosekKolidujacy(dataOd, dataDo, currentWniosek.getUprwId());
			
			if (wniosekKolidujacyDR!=null){
				inv.add(String.format("Wprowadzono co najmniej jeden wniosek na dane termin (od %1$s do %2$s)", this.format(wniosekKolidujacyDR.get("UPRW_DATA_OD")), this.format(wniosekKolidujacyDR.get("UPRW_DATA_DO"))  ));
			}
			
			if(currentWniosek.getUprwId() == 0L){			
				if (czyPrzekroczonoLimitNowyWn(dataOd, dataDo, currentWniosek.getUprwPrcId(), currentWniosek.getCzyGodzinowy())) {
					inv.add(String.format("Przekroczono limit dla %1$s", this.currentWniosek.getRdaKodRow().get("RDA_NAZWA")));
				}
			}
			else
			{
				if (czyPrzekroczonoLimitModWn(dataOd, dataDo, currentWniosek.getUprwPrcId(), currentWniosek.getCzyGodzinowy(), currentWniosek.getUprwId() )) {
					inv.add(String.format("Przekroczono limit dla %1$s", this.currentWniosek.getRdaKodRow().get("RDA_NAZWA")));
				}	
			}
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			if(currentWniosek.czyWybranoUrlopNaZadanie()) { 
				
				if(currentWniosek.getUprwId() == 0L){			
					if (czyPrzekroczonoLimitNowyWnNaZadanie(dataOd, dataDo)) {
						inv.add("Przekroczono limit dla urlopu na żądanie!");
					}
				}
				else
				{
					if (czyPrzekroczonoLimitModWnNaZadanie(dataOd, dataDo, this.getCurrentWniosek().getUprwId())) {
						inv.add("Przekroczono limit dla urlopu na żądanie!");
					}	
				}
				
			}
			
			
			
		}
		
		
		SqlDataSelectionsHandler ret = this.getLW("PPL_ABS_WALIDACJA_URLOPU");
		Map<String, Object> sqlNamedParams = new HashMap<>();
		sqlNamedParams.put("P_DATA_OD", dataOd);
		sqlNamedParams.put("P_DATA_DO", dataDo);
		sqlNamedParams.put("P_RDA_ID", currentWniosek.getUprwRdaId());
		sqlNamedParams.put("P_PRC_ID", currentWniosek.getUprwPrcId());
		ret.setSqlNamedParams(sqlNamedParams);
		
		if(   !(     ret!=null  
				&& ret.getAllData()!=null 
				&& ret.getAllData().get(0)!=null 
				&& ret.getAllData().get(0).get("dni_na_ktorych_nie_ma_limitu")!=null
				&& "0".equals(""+ret.getAllData().get(0).get("dni_na_ktorych_nie_ma_limitu"))
			  )
		) inv.add("Błąd walidacji wniosku względem okresów limitów na dany urlop!");
		
		
		for (String txt : inv) {
			User.alert(txt);
		}

		return inv.isEmpty();
	}
	
	private boolean czyWybranoAbsencje() {
		boolean ret = false;
		
		Date kliknietaData = currentWniosek.getUprwDataOd(); 
		
		
		for (DataRow dr : this.getLW("PPL_ABS_ABSENCJE").getData()){
			Date absOd = (Date) dr.get("AB_DATA_OD");
			Date absDo = (Date) dr.get("AB_DATA_DO");
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(absOd);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			absOd = cal.getTime();
			
			cal.setTime(absDo);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			absDo = cal.getTime();
			
			
			if (    (kliknietaData.after(absOd) && kliknietaData.before(absDo)) || 
					(absOd.equals(kliknietaData) || absDo.equals(kliknietaData) )   ){//czyli tak naprawde została wybrana absencja
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	private DataRow findAbsencjaKolidujaca(Date dataOd, Date dataDo) {
		DataRow ret = null;
		
		for (DataRow dr : this.getLW("PPL_ABS_ABSENCJE").getData()){
			Date absOd = (Date) dr.get("AB_DATA_OD");
			Date absDo = (Date) dr.get("AB_DATA_DO");
			if ( !(dataOd.after(absDo) || dataDo.before(absOd)) ){//jest nieobencosc kolidujaca
				ret = dr;
				break;
			}
		}
		return ret;
	}
	
	private DataRow findWniosekKolidujacy(Date dataOd, Date dataDo,long uprwId ) {
		DataRow ret = null;
		
		for (DataRow dr : this.getLW("PPL_ABS_URLOPY_WNIOSKOW").getData()) {
			Date wOd = (Date) dr.get("UPRW_DATA_OD");
			Date wDo = (Date) dr.get("UPRW_DATA_DO");
			if ( !(dataOd.after(wDo) || dataDo.before(wOd)) && uprwId!=dr.getLong("UPRW_ID") ){//jest wniosek kolidujacy
				ret = dr;
				break;
			}
		}
		return ret;
	}
	
	private boolean czyPrzekroczonoLimitNowyWn(Date dataOd, Date dataDo, long prcId, boolean czyGodzinowy ){
		Double limit=null;
		Double urlop=0.0;

/*		if ("UŹ".equals( this.currentWniosek.getRdaKodRow().get("RDA_KOD") )){
			return czyPrzekroczonoLimitNowyWnUZ (dataOd, dataDo, prcId);
		}*/

		
		if(this.currentWniosek.getRdaKodRow().get("dni_do_wykorzystania")==null)
			return true;
		else {
			limit= ((BigDecimal)    this.currentWniosek.getRdaKodRow().get("dni_do_wykorzystania")).doubleValue();
		}
		
		try (Connection conn = JdbcUtils.getConnection()) {
						
			if(czyGodzinowy){
				urlop = JdbcUtils.sqlFnDouble(conn, "PPADM.URLOP_GODZ_LICZBA_GODZIN", prcId, asSqlDate(dataOd), asSqlDate(dataDo))/getNormaUrlopowa(dataDo);
			}else{
				urlop = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_godz_harm_ind", prcId, asSqlDate(trunc(dataOd)), asSqlDate(trunc(dataDo)))/getNormaUrlopowa(dataDo);
			}
						
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

        return !(limit - urlop >= 0);
	}
	
	private boolean czyPrzekroczonoLimitModWn(Date dataOd, Date dataDo, long prcId, boolean czyGodzinowy, long wnId ){
		Double limit=null;
		//Double limitR=null;
		String kodNowy=null;
		String kodStary="";
		Double urlopPoMod=0.0;
		Double urlopPrzedMod=0.0;
		Date dataOdPrzedMod = null;
		Date dataDoPrzedMod = null;	
		
/*		if ("UŹ".equals( getCurrentAbsRodzajDR().get("RDA_KOD") )){
			return czyPrzekroczonoLimitModWnUZ(dataOd, dataDo, prcId, wnId);
		}*/
		
		if(this.currentWniosek.getRdaKodRow().get("dni_do_wykorzystania")==null || this.currentWniosek.getRdaKodRow().get("RDA_KOD")==null)
			return true;
		else{
			limit=  ((BigDecimal) this.currentWniosek.getRdaKodRow().get("dni_do_wykorzystania")).doubleValue();
			kodNowy=this.currentWniosek.getRdaKodRow().get("RDA_KOD").toString();
		}
				
		try (Connection conn = JdbcUtils.getConnection()) {
			
			Optional<DataRow> findFirst = this.getLW("PPL_ABS_URLOPY_WNIOSKOW").getData().stream().filter(r -> Long.valueOf( r.get("UPRW_ID").toString() ) == wnId).findFirst();
			DataRow wn = findFirst.get();
			if(wn==null || wn.get("UPRW_RDA_KOD")==null || wn.get("UPRW_DATA_OD")==null || wn.get("UPRW_DATA_DO")==null ){
				return true;
			}else {
				kodStary=wn.get("UPRW_RDA_KOD").toString();
				dataOdPrzedMod=(Date) wn.get("UPRW_DATA_OD");
				dataDoPrzedMod=(Date) wn.get("UPRW_DATA_DO");
			}
			
			if(czyGodzinowy){
				
				urlopPoMod = JdbcUtils.sqlFnDouble(conn, "PPADM.URLOP_GODZ_LICZBA_GODZIN", prcId, asSqlDate(dataOd), asSqlDate(dataDo))/getNormaUrlopowa(dataDo);
				urlopPrzedMod = JdbcUtils.sqlFnDouble(conn, "PPADM.URLOP_GODZ_LICZBA_GODZIN", prcId, asSqlDate(dataOdPrzedMod), asSqlDate(dataDoPrzedMod))/getNormaUrlopowa(dataDoPrzedMod);
				
			}else{
				urlopPoMod = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_godz_harm_ind", 
						prcId, asSqlDate(trunc(dataOd)), asSqlDate(trunc(dataDo)))/getNormaUrlopowa(dataDo);
				
				urlopPrzedMod = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_godz_harm_ind",
	                    prcId, asSqlDate(trunc(dataOdPrzedMod)), asSqlDate(trunc(dataDoPrzedMod)))/getNormaUrlopowa(dataDoPrzedMod);	
			}
			
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		
		if(kodStary.equals(kodNowy)){
            return !(limit - urlopPoMod + urlopPrzedMod >= 0);
		}
		else
		{
            return !(limit - urlopPoMod >= 0);
		}
	}
	
	private boolean czyPrzekroczonoLimitNowyWnNaZadanie(Date dataOd, Date dataDo) {
		Double limit=null;
		Double urlop=0.0;
		
		
		
		if(this.currentWniosek.getUrlopNaZadanieDataRow().get("dni_do_wykorzystania")==null) {
			return true;
		} else {
			limit= ((BigDecimal)    this.currentWniosek.getUrlopNaZadanieDataRow().get("dni_do_wykorzystania")).doubleValue();
		}
		
		try (Connection conn = JdbcUtils.getConnection()) {
				urlop = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_dni_harm_ind",  currentWniosek.getUprwPrcId(), asSqlDate(trunc(dataOd)), asSqlDate(trunc(dataDo)));			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

        return !(limit - urlop >= 0);

	}
	
	private boolean czyPrzekroczonoLimitModWnNaZadanie(Date dataOd, Date dataDo, long wnId) {
		Double limit=null;
		String kodNowy=null;
		String kodStary="";
		Double urlopPoMod=0.0;
		Double urlopPrzedMod=0.0;
		Date dataOdPrzedMod = null;
		Date dataDoPrzedMod = null;	

		
		if(this.currentWniosek.getUrlopNaZadanieDataRow().get("dni_do_wykorzystania")==null)
			return true;
		else{
			limit=  ((BigDecimal) this.currentWniosek.getUrlopNaZadanieDataRow().get("dni_do_wykorzystania")).doubleValue();
			kodNowy=this.currentWniosek.getRdaKodRow().get("RDA_KOD").toString();
		}
				
		try (Connection conn = JdbcUtils.getConnection()) {
			
			Optional<DataRow> findFirst = this.getLW("PPL_ABS_URLOPY_WNIOSKOW").getData().stream().filter(r -> Long.valueOf( r.get("UPRW_ID").toString() ) == wnId).findFirst();
			DataRow wn = findFirst.get();
			if(wn==null || wn.get("UPRW_RDA_KOD")==null || wn.get("UPRW_DATA_OD")==null || wn.get("UPRW_DATA_DO")==null ){
				return true;
			}else {
				kodStary=wn.get("UPRW_RDA_KOD").toString();
				dataOdPrzedMod=(Date) wn.get("UPRW_DATA_OD");
				dataDoPrzedMod=(Date) wn.get("UPRW_DATA_DO");
			}
			

				urlopPoMod = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_dni_harm_ind", 
						currentWniosek.getUprwPrcId(), asSqlDate(trunc(dataOd)), asSqlDate(trunc(dataDo)));
				
				urlopPrzedMod = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_dni_harm_ind",
						currentWniosek.getUprwPrcId(), asSqlDate(trunc(dataOdPrzedMod)), asSqlDate(trunc(dataDoPrzedMod)));	
			
			
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		
		if(kodStary.equals(kodNowy)){
            return !(limit - urlopPoMod + urlopPrzedMod >= 0);
		}
		else
		{
            return !(limit - urlopPoMod >= 0);
		}
	}
	
	
	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////koniec Metod do walidacji//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////Metody odpowiedzialne za wyswietlanie norm dobowych i godzinowych//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public String getNormaUrlopowaString(Date dataDo) {
		
		if(dataDo==null) dataDo=new Date();
		
		String normaUrlopowaString = "";
		
		try (Connection conn = JdbcUtils.getConnection()) {
			normaUrlopowaString = JdbcUtils.sqlFnVarchar(conn, "ek_pck_utl02.czas_z_godzin", getNormaUrlopowa(dataDo));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		if(normaUrlopowaString==null || normaUrlopowaString=="") normaUrlopowaString="8:0";
		return normaUrlopowaString;
	}
	
	public Double getNormaUrlopowa(Date dataDo) {
		
		if(dataDo==null) dataDo=new Date();
		
		Double normaUrlopowa = 0.0;
		try (Connection conn = JdbcUtils.getConnection()) {
			normaUrlopowa = JdbcUtils.sqlFnDouble(conn, "ek_pck_absencje.il_godz_na_dzien_urlopu", user.getPrcIdLong(), asSqlDate(dataDo));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		if(normaUrlopowa==null || normaUrlopowa==0.0) normaUrlopowa=8.0;
		return normaUrlopowa;
	}
	
	
	public String getNormaDobowaString(Date dataDo) {
		
		if(dataDo==null) dataDo=new Date();
		
		String normaDobowaString = "";
		
		try (Connection conn = JdbcUtils.getConnection()) {
			normaDobowaString = JdbcUtils.sqlFnVarchar(conn, "ek_pck_utl02.czas_z_godzin", getNormaDobowa(dataDo));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		if(normaDobowaString==null || normaDobowaString=="") normaDobowaString="8:0";
		return normaDobowaString;
	}
	
	public Double getNormaDobowa(Date dataDo) {
		
		if(dataDo==null) dataDo=new Date();
		
		Double normaDobowa = 0.0;
		try (Connection conn = JdbcUtils.getConnection()) {
			normaDobowa = JdbcUtils.sqlFnDouble(conn, "ek_pck_limity.norma_dobowa_na_dzien", user.getPrcIdLong(), asSqlDate(dataDo));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		if(normaDobowa==null || normaDobowa==0.0) normaDobowa=8.0;
		return normaDobowa;
		
	}
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////koniec Metody odpowiedzialne za wyswietlanie norm dobowych i godzinowych///////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////Metody odpowiedzialne za kolorowanie do dalszego sprawdzenia///////////////////////////////////////////////////////////////////////////////////	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Zwraca ciąg tekstowy z klasami css i datami dla dni objetych wnioskami w PP
	 * @return np. "nUŹ 2018/2/15, nUŹ 2018/2/15, nUW 2018/3/5, nUW 2018/3/6, nUŹ 2018/2/15, nUW 2018/3/5, nUW 2018/3/6, nUW 2018/6/12, nUW 2018/6/13, nUŹ 2018/2/15, nUW 2018/3/5"
	 */
	public String blueDays() {

		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");

		Collection<Date> dni = new LinkedList<>();

		HashMap<Date, String> dniHM = new HashMap<>();

		for (DataRow dr : this.getLW("PPL_ABS_URLOPY_WNIOSKOW").getData()) {

			String cssClass = calcBlueDayCssClass(dr);

			List<Date> days = getDaysBetweenDates((Date) dr.get("UPRW_DATA_OD"), (Date) dr.get("UPRW_DATA_DO"));
			dni.addAll(days);
			for (Date d : days) {
				dniHM.put(d, cssClass);
			}

			Calendar c = Calendar.getInstance();
			for (Date d : days) {
				c.setTime(d);
				int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
				if (dayOfWeek != 1 && dayOfWeek != 7) {
					String dts = dtf.format(d);
					sb.append((dniHM.get(d) + "         ").substring(0, 9) + dts + ", ");
					//sb.append(dniHM.get(d) +" " + dts + ", ");
				}
			}

		}

		return sb.toString();  
	}
	
	private String calcBlueDayCssClass(DataRow dr) {
		Integer status = dr.getInteger("UPRW_STATUS");
		String cssClass = ("" + dr.get("UPRW_RDA_KOD")).toUpperCase();
		
		if (status == null)
			return cssClass;
		
		if (status == 10) {// zatwierdzono wniosek
			cssClass = "t-" + cssClass;
		} else if (status < 0) {// odrzucono wniosek
			cssClass = "n-" + cssClass;
		} else if (status > 0) {// oczekujacy wniosek
			cssClass = "o-" + cssClass;
		}
		return cssClass;
	}
	
	public static List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (calendar.getTime().before(enddate)) {
			Date result = calendar.getTime();
			dates.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		dates.add(enddate);
		return dates;
	}
	
	/**
	 * Zwraca ciąg tekstowy z klasami css i datami dla dni objetych nieobecnosciami w EGR
	 * @return np. "UW  2018/1/16, UW  2018/1/3, UW  2018/2/6, UW  2018/2/13, UW  2018/2/14, UW  2018/1/19, UW  2018/2/26, UW  2018/1/10, UW  2018/1/11, UW  2018/1/22, "
	 */
	public String absencjeDays() {

		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");

		Collection<Date> dni = new LinkedList<>();

		HashMap<Date, String> dniHM = new HashMap<>();

		for (DataRow dr : this.getLW("PPL_ABS_ABSENCJE").getData()){
			List<Date> days = getDaysBetweenDates((Date) dr.get("AB_DATA_OD"), (Date) dr.get("AB_DATA_DO"));
			dni.addAll(days);
			for (Date d : days) {
				dniHM.put(d, "" + dr.get("RDA_KOD"));
			}
		}

		Calendar c = Calendar.getInstance();
		for (Date d : dni) {
			c.setTime(d);
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			if (dayOfWeek != 1 && dayOfWeek != 7) {
				String dts = dtf.format(d);
				sb.append((dniHM.get(d) + "         ").substring(0, 9) + dts + ", ");
			}
		}

		return sb.toString();
	}
	
	
	//TODO na koncu to trzeba poprawic
	/**
	 * Zwraca ciąg tekstowy z klasami css i datami dla dni objetych nieobecnosciami w EGR
	 * @return np. "2018/4/9, 2018/4/10, 2018/4/11, 2018/4/12, 2018/4/12," 
	 */
	public String lightBlueDays() {
		
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");
		if (currentWniosek.getUprwDataOd() != null && currentWniosek.getUprwDataDo() != null) {
			if (currentWniosek.getUprwDataOd().getTime() <= currentWniosek.getUprwDataDo().getTime()) {
				List<Date> dni = getDaysBetweenDates(currentWniosek.getUprwDataOd(), currentWniosek.getUprwDataDo());
				dni.add(currentWniosek.getUprwDataDo());
				Calendar c = Calendar.getInstance();
				for (Date d : dni) {
					c.setTime(d);
					int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
					if (dayOfWeek != 1 && dayOfWeek != 7) {
						String dts = dtf.format(d);
						sb.append(dts + ", ");
					}
				}

				return sb.toString();
			} else {
				return null;
			}

		}
		return null;
	}
	
	/**
	 * Zwraca ciąg tekstowy z klasami css i datami swiątecznymi w EGR
	 * @return np. "2018/4/9, 2018/4/10, 2018/4/11, 2018/4/12, 2018/4/12," 
	 */
	public String swietaDays() {

		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");

		Collection<Date> dni = new LinkedList<>();

		for (DataRow dr : this.getLW("PPL_SWIETA", currentWniosek.getRok()).getData()) {
			dni.add((Date) dr.get("DA_DATA"));
		}

		Calendar c = Calendar.getInstance();
		for (Date d : dni) {
			c.setTime(d);
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			if (dayOfWeek != 1 && dayOfWeek != 7) {
				String dts = dtf.format(d);
				sb.append(dts + ", ");
			}
		}

		return sb.toString();
	}
	
	/**
	 * Zwraca ciąg tekstowy z klasami css i datami dla dni objetych wnioskami w PP
	 * @return np. "zRP 2018/2/15, zRP 2018/2/15"
	 */
	public String rocznyPlanDays() {
		
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");

		Collection<Date> dni = new LinkedList<>();

		for (DataRow dr : this.getLW("PPL_ABS_PLANOWANE").getData()) {

			List<Date> days = getDaysBetweenDates((Date) dr.get("URPL_DATA_OD"), (Date) dr.get("URPL_DATA_DO"));
			dni.addAll(days);

			Calendar c = Calendar.getInstance();
			for (Date d : dni) {
				c.setTime(d);
				int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
				if (dayOfWeek != 1 && dayOfWeek != 7) {
					String dts = dtf.format(d);
					sb.append(dts + ", ");
				}
			}

		}

		return sb.toString();
	}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////koniec metod odpowiedzialne za kolorowanie do dalszego sprawdzenia/////////////////////////////////////////////////////////////////////////////	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	



	//TODO do poprawki na koncu
	public void raport() {

		if (this.getCurrentWniosek().getUprwId() == 0) {
			User.info("Nie wybrano żadnego wniosku.", FacesMessage.SEVERITY_WARN);
			return;
		}
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("id_wniosku", this.currentWniosek.getUprwId());
			params.put("pozostalo_dni", new Double( this.currentWniosek.getRdaKodRow().get("dni_do_wykorzystania").toString()));
			params.put("zalegle_dni", new Double( this.currentWniosek.getRdaKodRow().get("zalegly").toString()));
			ReportGenerator.displayReportAsPDF(params, "Wniosek_o_urlop");
		} catch (JRException | SQLException | IOException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}

	}
	
	

	
	public void test_raport() throws IOException {
		//String sqlId = "PP_WYDZIALY";
		//Map<String, Object> sqlNamedParams = new HashMap<>();
		//sqlNamedParams.put("POLE_1", 101236);
		//ReportGenerator.lWtoJRXML(sqlId, sqlNamedParams);
		
		
		//String sqlId = "PP_STAN_WYZ_W_DEPART";
		//Map<String, Object> sqlNamedParams = new HashMap<>();
		//sqlNamedParams.put("POLE_1", 100875);
		//ReportGenerator.lWtoRaport(sqlId, sqlNamedParams);
		
		//lWStoJRXML(LinkedHashMap<String, Map<String, Object>> listyWartosci)
		

		//Map<String, Object> sqlNamedParams = new HashMap<>();
		//sqlNamedParams.put("POLE_1", 101236);
		
		//ReportGenerator.dellll();
		
		
		LinkedHashMap<String, Map<String, Object>> listyWartosci = new LinkedHashMap<String, Map<String, Object>>();
		Map<String, Object> sqlNamedParams;
		
		sqlNamedParams = new HashMap<>();
		sqlNamedParams.put("POLE_1", 101236);
		listyWartosci.put("PP_WYDZIALY", sqlNamedParams);
		
		sqlNamedParams = new HashMap<>();
		sqlNamedParams.put("POLE_1", 100875);
		listyWartosci.put("PP_STAN_WYZ_W_DEPART", sqlNamedParams);
		
		sqlNamedParams = new HashMap<>();
		sqlNamedParams.put("POLE_1", 105744);
		listyWartosci.put("PPL_PRC_HIST_ZATR", sqlNamedParams);
		
		sqlNamedParams = new HashMap<>();
		sqlNamedParams.put("POLE_1", 105744);
		listyWartosci.put("PP_KAD_RODZINA", sqlNamedParams);
		
		sqlNamedParams = new HashMap<>();
		sqlNamedParams.put("POLE_1", 105744);
		listyWartosci.put("PPL_KAD_PRC_JEZYKI", sqlNamedParams);
		
		ReportGenerator.lWStoJRXML(listyWartosci);
		
	}
	
}
