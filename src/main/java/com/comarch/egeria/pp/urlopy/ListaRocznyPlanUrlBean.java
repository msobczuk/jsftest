package com.comarch.egeria.pp.urlopy;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean 
@Named
@Scope("view")
public class ListaRocznyPlanUrlBean extends SqlBean {
	private static final Logger log = LogManager.getLogger();
	
	
	@Inject
	User user;

	private Integer rok = (LocalDate.now().getYear());
	private ArrayList<Integer> lata = new ArrayList<>();
	private Long selectedPracownikId;
	private DataRow selectedPracownik;
	private String uwaga;
	


	
	@PostConstruct
	public void init(){
		
		this.lata.add(rok - 2);
		this.lata.add(rok - 1);
		this.lata.add(rok);
		this.lata.add(rok + 1);
		this.lata.add(rok + 2);

	}


	public void zatwierdzPlan(Long prcId, Integer rok) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		if(prcId==null || prcId.longValue()==0 || rok==null || rok.intValue()==0){
			User.info("Nie możesz zatwierdzić tego planu!");
			return;
		}
		
		try (Connection conn = JdbcUtils.getConnection()) {
			
			JdbcUtils.sqlExecNoQuery(conn, "UPDATE PPADM.PPT_ABS_URLOPY_PLANOWANE set URPL_STATUS='Z'"
					+ " WHERE URPL_PRC_ID="+prcId.toString()
					+       " AND EXTRACT(YEAR FROM URPL_DATA_OD)<="+rok.toString()+" and EXTRACT(YEAR FROM URPL_DATA_DO)>="+rok.toString());
			
			User.info("Plan urlopowy został zatwierdzony!");
		}
		catch (Exception e) {
			User.alert("Błąd przy zatwierdzaniu tego planu!");
		}
	}
	
    public void odrzucPlan(Long prcId, Integer rok) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException{
		if(prcId==null || prcId.longValue()==0 || rok==null || rok.intValue()==0){
			User.info("Nie możesz odrzucić tego planu!");
			return;
		}
		
		if(this.uwaga == null || "".equals(this.uwaga)){
			User.info("Należy wypełnić uzasadnienie odrzucenia planu urlopowego!");
			return;
		}
		
		try (Connection conn = JdbcUtils.getConnection()) {
			
			JdbcUtils.sqlExecNoQuery(conn, "UPDATE PPADM.PPT_ABS_URLOPY_PLANOWANE set URPL_STATUS='O',URPL_OPIS='"+this.uwaga+"'"
					+ " WHERE URPL_PRC_ID="+prcId.toString()
					+       " AND EXTRACT(YEAR FROM URPL_DATA_OD)<="+rok.toString()+" and EXTRACT(YEAR FROM URPL_DATA_DO)>="+rok.toString());
			
			User.info("Plan urlopowy został odrzucony!");
		}
		catch (Exception e) {
			User.alert("Błąd przy odrzucaniu tego planu!");
		}
	}
    
    public void raportPracownik(){
    	if(selectedPracownikId!=null && selectedPracownikId.longValue()!=0){
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("prc_id", selectedPracownikId);
				params.put("rok", rok);
				ReportGenerator.displayReportAsPDF(params, "roczny_plan_konkretny_pracownik");
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
    	}
    }
    
    public void raportWszyscy(){
    	if(user.getPrcIdLong()!=null && user.getDbLogin()!=null){
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("prc_id", user.getPrcIdLong());
				params.put("rok", rok);
				params.put("login", user.getDbLogin());
				ReportGenerator.displayReportAsPDF(params, "roczny_plan_wszyscy");
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
    	}
    }


	public Integer getRok() {
		return rok;
	}


	public void setRok(Integer rok) {
		this.rok = rok;
	}


	public ArrayList<Integer> getLata() {
		return lata;
	}


	public void setLata(ArrayList<Integer> lata) {
		this.lata = lata;
	}


	public Long getSelectedPracownikId() {
		return selectedPracownikId;
	}


	public void setSelectedPracownikId(Long selectedPracownikId) {
		this.selectedPracownikId = selectedPracownikId;
		
		SqlDataSelectionsHandler lw = this.getLW("PPL_URLOPY_PLANOWANE");
		if (lw!=null)
			this.selectedPracownik = lw.find(selectedPracownikId);
	}


	public DataRow getSelectedPracownik() {
		return selectedPracownik;
	}


	public void setSelectedPracownik(Long selectedPracownikId) {
		if(selectedPracownikId==null)
			return;
		
		SqlDataSelectionsHandler pracownicy = this.getLW("PPL_URLOPY_PLANOWANE");

		this.selectedPracownik = pracownicy.get(selectedPracownikId.intValue());

	}


	public String getUwaga() {
		return uwaga;
	}


	public void setUwaga(String uwaga) {
		this.uwaga = uwaga;
	}

}
