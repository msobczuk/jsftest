package com.comarch.egeria.pp.urlopy.model;

import java.io.Serializable;
import javax.persistence.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_ABS_URLOPY_PLANOWANE database table.
 * 
 */
@Entity
@Table(name="PPT_ABS_URLOPY_PLANOWANE", schema="PPADM")
@NamedQuery(name="PptUrlopyPlanowane.findAll", query="SELECT p FROM PptUrlopyPlanowane p")
public class PptUrlopyPlanowane implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ABS_URLOPY_PLANOWANE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ABS_URLOPY_PLANOWANE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ABS_URLOPY_PLANOWANE")
	@Column(name="URPL_ID")
	private long urplId;

	@Temporal(TemporalType.DATE)
	@Column(name="URPL_DATA_DO")
	private Date urplDataDo;

	@Temporal(TemporalType.DATE)
	@Column(name="URPL_DATA_OD")
	private Date urplDataOd;

	@Column(name="URPL_KAL_ID")
	private Long urplKalId;

	@Column(name="URPL_OPIS")
	private String urplOpis;

	@Column(name="URPL_ORG_ID")
	private Long urplOrgId;

	@Column(name="URPL_PRC_ID")
	private Long urplPrcId;

	@Column(name="URPL_TYP")
	private String urplTyp;
	
	@Column(name="URPL_STATUS")
	private String urplStatus="N";

	//bi-directional many-to-one association to PptUrlopyPlanowaneDni
	@OneToMany(mappedBy="pptUrlopyPlanowane", cascade=CascadeType.ALL)
	private List<PptUrlopyPlanowaneDni> pptUrlopyPlanowaneDnis = new ArrayList<>();

	public PptUrlopyPlanowane() {
	}

	public long getUrplId() {
		return this.urplId;
	}

	public void setUrplId(long urplId) {
		this.urplId = urplId;
	}

	public Date getUrplDataDo() {
		return this.urplDataDo;
	}

	public void setUrplDataDo(Date urplDataDo) {
		this.urplDataDo = urplDataDo;
	}

	public Date getUrplDataOd() {
		return this.urplDataOd;
	}

	public void setUrplDataOd(Date urplDataOd) {
		this.urplDataOd = urplDataOd;
	}

	public Long getUrplKalId() {
		return this.urplKalId;
	}

	public void setUrplKalId(Long urplKalId) {
		this.urplKalId = urplKalId;
	}

	public String getUrplOpis() {
		return this.urplOpis;
	}

	public void setUrplOpis(String urplOpis) {
		this.urplOpis = urplOpis;
	}

	public Long getUrplOrgId() {
		return this.urplOrgId;
	}

	public void setUrplOrgId(Long urplOrgId) {
		this.urplOrgId = urplOrgId;
	}

	public Long getUrplPrcId() {
		return this.urplPrcId;
	}

	public void setUrplPrcId(Long urplPrcId) {
		this.urplPrcId = urplPrcId;
	}

	public String getUrplTyp() {
		return this.urplTyp;
	}

	public void setUrplTyp(String urplTyp) {
		this.urplTyp = urplTyp;
	}

	public List<PptUrlopyPlanowaneDni> getPptUrlopyPlanowaneDnis() {
		return this.pptUrlopyPlanowaneDnis;
	}

	public void setPptUrlopyPlanowaneDnis(List<PptUrlopyPlanowaneDni> pptUrlopyPlanowaneDnis) {
		this.pptUrlopyPlanowaneDnis = pptUrlopyPlanowaneDnis;
	}

	public PptUrlopyPlanowaneDni addPptUrlopyPlanowaneDni(PptUrlopyPlanowaneDni pptUrlopyPlanowaneDni) {
		getPptUrlopyPlanowaneDnis().add(pptUrlopyPlanowaneDni);
		pptUrlopyPlanowaneDni.setPptUrlopyPlanowane(this);

		return pptUrlopyPlanowaneDni;
	}

	public PptUrlopyPlanowaneDni removePptUrlopyPlanowaneDni(PptUrlopyPlanowaneDni pptUrlopyPlanowaneDni) {
		getPptUrlopyPlanowaneDnis().remove(pptUrlopyPlanowaneDni);
		pptUrlopyPlanowaneDni.setPptUrlopyPlanowane(null);

		return pptUrlopyPlanowaneDni;
	}

	public String getUrplStatus() {
		return urplStatus;
	}

	public void setUrplStatus(String urplStatus) {
		this.urplStatus = urplStatus;
	}

}