package com.comarch.egeria.pp.urlopy;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.urlopy.model.PptUrlopyWnioskowane;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import net.sf.jasperreports.engine.JRException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;

import static com.comarch.egeria.Utils.*;

@ManagedBean 
@Named
@Scope("view")
public class WnUrlopowyBean extends SqlBean {
	private static final Logger log = LogManager.getLogger();

	@Inject
	User user;
	
	@Inject
	protected ReportGenerator ReportGenerator;
	

	DataRow rUrlopWypoczynkowy;
	
	private Integer rok = (LocalDate.now().getYear());
	private ArrayList<Integer> lata = new ArrayList<>();
	private Date dzien = new Date();
	
	ArrayList<PptUrlopyWnioskowane> wnioskiNaRok = new ArrayList<>();
	private ArrayList<PptUrlopyWnioskowane> wnioskiNaDzien = new ArrayList<>();
	private PptUrlopyWnioskowane currentWniosek = new PptUrlopyWnioskowane();
	
	//pole, które ze słownika odczytuje czy pole z pracownikiem rozaptrującym urlop ma byc wyswietlane czy nie
	//uwaga przy podgrywaniu słownikow do Tarnowa, trzeba bedzie o tym pamietac
	private String czyRozpatrujacy = null;

	
	@PostConstruct
	public void init(){
		this.currentWniosek.setSqlBean(this);
		SqlDataSelectionsHandler sqlDH = getRodzajeAbsencjiLW();
		ArrayList<DataRow> data = null;
		
		if(sqlDH!=null && sqlDH.getData()!=null)
			data = sqlDH.getData();

		


		
		try {
			Optional<DataRow> findFirst = data.stream().filter(r -> ("urlop wypoczynkowy".equals( (""+r.get("RDA_NAZWA")).toLowerCase() ))).findFirst();
			rUrlopWypoczynkowy = findFirst.get();
		} catch (Exception e) {
			if (!data.isEmpty())
			rUrlopWypoczynkowy = data.get(0);
		}
		
//		this.setCurrentAbsRodzajDR(rUrlopWypoczynkowy);
		
			
		wczytajListeWnioskow(this.rok);
		this.lata.add(rok);
		this.lata.add(rok - 1);
		this.lata.add(rok - 2);
		this.lata.add(rok - 3);
		this.lata.add(rok - 4);
	

		this.loadSqlData("sqlUrlopyWnioskowane", ""
				+ " SELECT * "
				+ " FROM (select ? as rok from dual) p, PPADM.PPT_ABS_URLOPY_WNIOSKOWANE "
				+ " where UPRW_PRC_ID = ? "
				+ "     and extract(year from uprw_data_od) <= p.rok  and  p.rok  <=  extract(year from uprw_data_do) "
				+ " order by uprw_data_od "
				, rok, user.getPrcIdLong());

		
		this.getLW("PPL_ABS_ABSENCJE", rok, user.getPrcIdLong()).getData();
		
		
/*		this.loadSqlData("sqlSwieta", ""
				+ " select  distinct "
				+ "    SW_ID  "
				+ "   ,SW_KAL_ID "
				+ "   ,SW_DATA "
				+ " from  EGADM1.CSS_SWIETA "
				+ " where extract (year from sw_data) = ? "
				+ " order by SW_KAL_ID, sw_data "
				, rok);*/
		
		this.getLW("PPL_SWIETA", rok).getData();

		
		this.getLW("PPL_ABS_PLANOWANE", rok, user.getPrcIdLong()).getData();
		
		this.setCzyRozpatrujacy();

		this.newWniosek();
	}
	
	
	
	
	public SqlDataSelectionsHandler getPplAbsCzasPracy() {
		
		return getLW(
				"PPL_ABS_CZASY_PRACY", 
					user.getPrcId(), currentWniosek.getUprwDataOd(),  currentWniosek.getUprwDataDo(),
					user.getPrcId(), currentWniosek.getUprwDataDo(),  currentWniosek.getUprwDataDo() 
				);
		
	}


	//boolean liczLimitZaCalyRok = false;
	public SqlDataSelectionsHandler getRodzajeAbsencjiLW() {
		//SqlDataHandler sqlDH=this.getLW("PPL_ABS_RODZAJE_ABSENCJI", user.getPrcId(), rok, rok, rok, rok, rok, user.getPrcId(), user.getPrcId(), rok, rok, user.getPrcId(), rok, rok);
		
		SqlDataSelectionsHandler lwLRok = this.getLW("PPL_ABS_LIMITY_ROK");
		Map<String, Object> sqlNamedParams = new HashMap<>();
		sqlNamedParams.put("P_PRC_ID", user.getPrcId());
		sqlNamedParams.put("P_ROK", rok);
		lwLRok.setSqlNamedParams(sqlNamedParams);
		
		SqlDataSelectionsHandler ret = this.getLW("PPL_ABS_RODZAJE_ABSENCJI");
		sqlNamedParams = new HashMap<>();
		sqlNamedParams.put("P_PRC_ID", user.getPrcId());
		sqlNamedParams.put("P_ROK", rok);
		//??????????????????????????????????????????????
		//sqlNamedParams.put("P_DATA_OD", this.currentWniosek.getUprwDataOd()==null ? new Date() : this.currentWniosek.getUprwDataOd() );
		sqlNamedParams.put("P_DATA_DO", this.currentWniosek.getUprwDataDo()==null ? new Date() : this.currentWniosek.getUprwDataDo() );

		
		ret.setSqlNamedParams(sqlNamedParams);
		return ret;
	}

	

	public void newWniosek(){
		PptUrlopyWnioskowane newWniosek = new PptUrlopyWnioskowane();
		this.currentWniosek.setSqlBean(this);
		
		newWniosek.setUprwPrcId(user.getPrcIdLong());
		newWniosek.setRok(rok);
		newWniosek.czyStaryWidokWniosekUrlopowyXhtml = true;
		
		if(getCurrentAbsRodzajDR()!=null){
			if(getCurrentAbsRodzajDR().get("RDA_ID")!=null && getCurrentAbsRodzajDR().get("RDA_KOD")!=null)
				//newWniosek.setUprwRdaId( ((BigDecimal) getCurrentAbsRodzajDR().get("RDA_ID")).intValue() );
				newWniosek.setUprwRdaKod( ""+ getCurrentAbsRodzajDR().get("RDA_KOD") );
		}
		else
		{
/*			if(rUrlopWypoczynkowy!=null && rUrlopWypoczynkowy.get("RDA_ID")!=null)
				newWniosek.setUprwRdaId( ((BigDecimal) rUrlopWypoczynkowy.get("RDA_ID")).intValue() );*/
			if(rUrlopWypoczynkowy!=null)
				newWniosek.setUprwRdaKod( ""+ rUrlopWypoczynkowy.get("RDA_KOD") );
		}
		
/*		if( this.currentWniosek.getUprwZastPrcId()!=null ) newWniosek.setUprwZastPrcId( this.currentWniosek.getUprwZastPrcId() );
		if( this.currentWniosek.getUprwZatwPrcId()!=null ) newWniosek.setUprwZatwPrcId( this.currentWniosek.getUprwZatwPrcId() );*/
		this.setCurrentWniosek(newWniosek);
	}

		
	public void wczytajListeWnioskow(Integer rok){

		this.getWnioskiNaDzien().clear();
		this.wnioskiNaRok.clear();
		
		String hql = String.format( " FROM PptUrlopyWnioskowane x WHERE x.uprwPrcId = %1$s and ( year(x.uprwDataOd) <= %2$s and %2$s <= year(x.uprwDataDo)  ) ORDER BY x.uprwDataOd" , user.getPrcIdLong(), rok);
		this.wnioskiNaRok.addAll(HibernateContext.query(hql));
	}
	
	
	
	/**
	 * Wczytujemy dla kliknietego/wskazanego dnia wnioski (takze godzinowe w l. mnogiej) ktore sa na ten dzien (lub jego czesc) aktualne
	 * Jako current domyslnie ustawiamy pierwszy z wniosków godzinowych z danego dnia.
	 * Jesli to nie są godzinowe to jest tam tylko jeden wionsek 
	 * @param naDzien
	 */
	public void wczytajWniosek(Date naDzien){
		
		if(this.getCurrentAbsRodzajDR()==null)
		{
			User.alert("Należy wybrać typ urlopu!");
			return;
		}
		
		LocalDate ldtNaDzien = asLocalDate(naDzien);
		
		this.getWnioskiNaDzien().clear();
		
		Boolean czyUstawionaDataOd = false;
		Date od = new Date();
		
		//jezeli najpier wybrano zastepujacego i zatwierdzajacego a potem daty z kalndarza po lewej
		boolean czyNowy = this.currentWniosek.getUprwId() == 0L;
		Integer zastepujacy = this.currentWniosek.getUprwZastPrcId();
		Long zatwierdzajacy = this.currentWniosek.getUprwZatwPrcId();
		

		
		
		if(this.currentWniosek.getUprwDataOd()!=null && this.currentWniosek.getUprwDataDo()==null){
			czyUstawionaDataOd=true;
			od=this.currentWniosek.getUprwDataOd();
		}
		
		//this.setCurrentWniosek(null);
		//newWniosek();
		
		for (PptUrlopyWnioskowane wn : this.wnioskiNaRok) {
			LocalDate ldtOd = asLocalDate( wn.getUprwDataOd() );
			LocalDate ldtDo = asLocalDate( wn.getUprwDataDo() );
			
			
			if ( ldtNaDzien.equals(ldtOd) 
					|| ldtOd.isBefore(ldtNaDzien) && ldtDo.isAfter(ldtNaDzien) 
					|| ldtDo.isEqual(ldtNaDzien)
					){
				this.getWnioskiNaDzien().add(wn);
				//System.out.println(wn.getUprwDataOd()  + " - " +  wn.getUprwDataDo());
			}

		}
		
		
		if (!this.getWnioskiNaDzien().isEmpty()){
			PptUrlopyWnioskowane naWybranyDzien = this.getWnioskiNaDzien().get(0);
			//if (naWybranyDzien.getUprwId()!=0)
			HibernateContext.refresh(naWybranyDzien);
			this.setCurrentWniosek(naWybranyDzien);
		}
		else
		{
			newWniosek();
			if(czyUstawionaDataOd){
				if(od.before(naDzien)){
					this.currentWniosek.setUprwDataOd(od);
					this.currentWniosek.setUprwDataDo(naDzien);
				}
				else
				{
					this.currentWniosek.setUprwDataOd(naDzien);
					this.currentWniosek.setUprwDataDo(od);	
				}	
			}
			else
			{
				this.currentWniosek.setUprwDataOd(naDzien);
				this.currentWniosek.setUprwDataDo(null);
			}
			
			//ten if jest potrzebny przez to jak jest skonstruowany model pod nowy widok
			if(this.currentWniosek.getUprwRdaId()==null || this.currentWniosek.getUprwRdaKod()==null) {
				this.currentWniosek.setUprwRdaKod( ""+ getCurrentAbsRodzajDR().get("RDA_KOD") );
			}
			
			//jezeli najpier wybrano zastepujacego i zatwierdzajacego a potem daty z kalndarza po lewej
			if(czyNowy){
				this.currentWniosek.setUprwZastPrcId(zastepujacy);
				this.currentWniosek.setUprwZatwPrcId(zatwierdzajacy);
			}
		}	
		
		this.currentWniosek.czyStaryWidokWniosekUrlopowyXhtml = true;
		
		if(this.currentWniosek.getUprwDataOd()!=null && this.currentWniosek.getUprwDataDo()!=null){
			com.comarch.egeria.Utils.update("formData");
			com.comarch.egeria.Utils.update("formPlanuj");
		} else {
			com.comarch.egeria.Utils.update("formPlanuj:datyGodziny");
			/*RequestContext.getCurrentInstance().update("formPlanuj:dtDo");
			RequestContext.getCurrentInstance().update("formPlanuj:liczbaDni");
			RequestContext.getCurrentInstance().update("formPlanuj:liczbaGodzin");*/
		}	

	}

	
	
	public static List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (calendar.getTime().before(enddate)) {
			Date result = calendar.getTime();
			dates.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		dates.add(enddate);
		return dates;
	}
	
	
	
	

	/**
	 * Zwraca ciąg tekstowy z klasami css i datami dla dni objetych wnioskami w PP
	 * @return np. "nUŹ 2018/2/15, nUŹ 2018/2/15, nUW 2018/3/5, nUW 2018/3/6, nUŹ 2018/2/15, nUW 2018/3/5, nUW 2018/3/6, nUW 2018/6/12, nUW 2018/6/13, nUŹ 2018/2/15, nUW 2018/3/5"
	 */
	public String blueDays() {
		
//		System.out.println("blueDays......................................................................................");

		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");

		Collection<Date> dni = new LinkedList<>();

		HashMap<Date, String> dniHM = new HashMap<>();

		for (DataRow dr : this.getSqlData("sqlUrlopyWnioskowane")) {

			String cssClass = calcBlueDayCssClass(dr);

			List<Date> days = getDaysBetweenDates((Date) dr.get("UPRW_DATA_OD"), (Date) dr.get("UPRW_DATA_DO"));
			dni.addAll(days);
			for (Date d : days) {
				dniHM.put(d, cssClass);
			}

			Calendar c = Calendar.getInstance();
			for (Date d : dni) {
				c.setTime(d);
				int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
				if (dayOfWeek != 1 && dayOfWeek != 7) {
					String dts = dtf.format(d);
					sb.append((dniHM.get(d) + "     ").substring(0, 5) + dts + ", ");
				}
			}

		}
//		System.out.println("blueDays -> " + sb.toString()); 
		return sb.toString(); //blueDays -> 
	}



	private String calcBlueDayCssClass(DataRow dr) {
		Integer status = dr.getInteger("UPRW_STATUS");
		String cssClass = ("" + dr.get("UPRW_RDA_KOD")).toUpperCase();
		
		if (status == null)
			return cssClass;
		
		if (status == 10) {// zatwierdzono wniosek
			cssClass = "t-" + cssClass;
		} else if (status < 0) {// odrzucono wniosek
			cssClass = "n-" + cssClass;
		} else if (status > 0) {// oczekujacy wniosek
			cssClass = "o-" + cssClass;
		}
		return cssClass;
	}
	
	

	/**
	 * Zwraca ciąg tekstowy z klasami css i datami dla dni objetych nieobecnosciami w EGR
	 * @return np. "UW  2018/1/16, UW  2018/1/3, UW  2018/2/6, UW  2018/2/13, UW  2018/2/14, UW  2018/1/19, UW  2018/2/26, UW  2018/1/10, UW  2018/1/11, UW  2018/1/22, "
	 */
	public String absencjeDays() {
	
//		System.out.println("absencjeDays............. .................................................................................................");

		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");

		Collection<Date> dni = new LinkedList<>();

		HashMap<Date, String> dniHM = new HashMap<>();

		for (DataRow dr : this.getLW("PPL_ABS_ABSENCJE").getData()){
			List<Date> days = getDaysBetweenDates((Date) dr.get("AB_DATA_OD"), (Date) dr.get("AB_DATA_DO"));
			dni.addAll(days);
			for (Date d : days) {
				dniHM.put(d, "" + dr.get("RDA_KOD"));
			}
		}

		Calendar c = Calendar.getInstance();
		for (Date d : dni) {
			c.setTime(d);
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			if (dayOfWeek != 1 && dayOfWeek != 7) {
				String dts = dtf.format(d);
				sb.append((dniHM.get(d) + "     ").substring(0, 5) + dts + ", ");
			}
		}

//		System.out.println("absencjeDays -> " + sb.toString());
		return sb.toString();
	}
	
	
	
	
	/**
	 * Zwraca ciąg tekstowy z klasami css i datami dla dni objetych nieobecnosciami w EGR
	 * @return np. "2018/4/9, 2018/4/10, 2018/4/11, 2018/4/12, 2018/4/12," 
	 */
	public String lightBlueDays() {
//		System.out.println("lightBlueDays ...................................................................................................");
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");
		if (currentWniosek.getUprwDataOd() != null && currentWniosek.getUprwDataDo() != null) {
			if (currentWniosek.getUprwDataOd().getTime() <= currentWniosek.getUprwDataDo().getTime()) {
				List<Date> dni = getDaysBetweenDates(currentWniosek.getUprwDataOd(), currentWniosek.getUprwDataDo());
				dni.add(currentWniosek.getUprwDataDo());
				Calendar c = Calendar.getInstance();
				for (Date d : dni) {
					c.setTime(d);
					int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
					if (dayOfWeek != 1 && dayOfWeek != 7) {
						String dts = dtf.format(d);
						sb.append(dts + ", ");
					}
				}
//				System.out.println("lightBlueDays -> " + sb.toString());
				return sb.toString();
			} else {
				return null;
			}

		}
		return null;
	}
	
	/**
	 * Zwraca ciąg tekstowy z klasami css i datami swiątecznymi w EGR
	 * @return np. "2018/4/9, 2018/4/10, 2018/4/11, 2018/4/12, 2018/4/12," 
	 */
	public String swietaDays() {

		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");

		Collection<Date> dni = new LinkedList<>();

		for (DataRow dr : this.getLW("PPL_SWIETA", rok).getData()) {
			dni.add((Date) dr.get("DA_DATA"));
		}

		Calendar c = Calendar.getInstance();
		for (Date d : dni) {
			c.setTime(d);
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			if (dayOfWeek != 1 && dayOfWeek != 7) {
				String dts = dtf.format(d);
				sb.append(dts + ", ");
			}
		}

		return sb.toString();
	}
	
	

	/**
	 * Zwraca ciąg tekstowy z klasami css i datami dla dni objetych wnioskami w PP
	 * @return np. "zRP 2018/2/15, zRP 2018/2/15"
	 */
	public String rocznyPlanDays() {
		
//			System.out.println("blueDays......................................................................................");

		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");

		Collection<Date> dni = new LinkedList<>();

		for (DataRow dr : this.getLW("PPL_ABS_PLANOWANE").getData()) {

			List<Date> days = getDaysBetweenDates((Date) dr.get("URPL_DATA_OD"), (Date) dr.get("URPL_DATA_DO"));
			dni.addAll(days);

			Calendar c = Calendar.getInstance();
			for (Date d : dni) {
				c.setTime(d);
				int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
				if (dayOfWeek != 1 && dayOfWeek != 7) {
					String dts = dtf.format(d);
					sb.append(dts + ", ");
				}
			}

		}
//			System.out.println("blueDays -> " + sb.toString()); 
		return sb.toString(); //blueDays -> 
	}
	

	

	public boolean validateBeforeSave() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		ArrayList<String> inv = new ArrayList<String>();

		Date dataOd = this.currentWniosek.getUprwDataOd();
		Date dataDo = this.currentWniosek.getUprwDataDo();
		
		if(this.getCurrentAbsRodzajDR()==null)
		{
			User.alert("Należy wybrać typ urlopu!");
			return false;
		}
		
		
		if (dataOd == null || dataDo == null) {
			inv.add("Data początkowa i końcowa urlopu to pola wymagane");
		} else {
			
//tymczasowa mozilowsc dodawania urlopów za poprzednie lata			
/*			if (dataOd.getYear() < new Date().getYear() || dataDo.getYear() < new Date().getYear()) {
				inv.add("Nie można planować urlopu za poprzednie lata!");
			}*/

			if (dataOd.after(dataDo)) {
				inv.add("Data końcowa nie może być wcześniejsza od daty początkowej");
			}

			if (currentWniosek.getCzyGodzinowy()) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(dataOd);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				Date data1 = cal.getTime();

				cal.setTime(dataDo);
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				Date data2 = cal.getTime();

				if (data1.compareTo(data2) != 0) {
					inv.add("Urlop godzinowy może być wnioskowany w obrębie jednego dnia!");
				}
				
				if(dataOd.getHours()>=dataDo.getHours()){
					inv.add("Godzina od i do nie mogą przyjmować tej samej wartości!");
				}
				
				if(dataOd.getMinutes()!=dataDo.getMinutes()){
					inv.add("Urlop może być wprowadzony jedynie na pełne godziny!");
				}
			}

			
			DataRow absencjaKolidujacaDR = findAbsencjaKolidujaca(dataOd, dataDo);
			
			if (absencjaKolidujacaDR!=null){
				inv.add(String.format("Wprowadzono co najmniej jedną nieobecność kolidującą (od %1$s do %2$s)", absencjaKolidujacaDR.get("AB_DATA_OD"), absencjaKolidujacaDR.get("AB_DATA_DO")  ));
			}
			
			DataRow wniosekKolidujacyDR = findWniosekKolidujacy(dataOd, dataDo, currentWniosek.getUprwId());
			
			if (wniosekKolidujacyDR!=null){
				inv.add(String.format("Wprowadzono co najmniej jeden wniosek na dane termin (od %1$s do %2$s)", wniosekKolidujacyDR.get("UPRW_DATA_OD"), wniosekKolidujacyDR.get("UPRW_DATA_DO")  ));
			}
			

			if(currentWniosek.getUprwId() == 0L){			
				if (czyPrzekroczonoLimitNowyWn(dataOd, dataDo,currentWniosek.getUprwPrcId(),currentWniosek.getCzyGodzinowy())) {
					inv.add(String.format("Przekroczono limit dla %1$s", getCurrentAbsRodzajDR().get("RDA_NAZWA")));
				}
			}
			else
			{
				if (czyPrzekroczonoLimitModWn(dataOd, dataDo,currentWniosek.getUprwPrcId(),currentWniosek.getCzyGodzinowy(), currentWniosek.getUprwId() )) {
					inv.add(String.format("Przekroczono limit dla %1$s", getCurrentAbsRodzajDR().get("RDA_NAZWA")));
				}	
			}
			



		}

		if (currentWniosek.getUprwRdaKod() == null) {
			inv.add("Rodzaj urlopu to pole wymagane");
		}
		
		if (currentWniosek.getUprwPrcId() == null) {
			inv.add("Musi być wybrany pracownik dla, którego jest wnioskowany urlop!");
		}
		
		if (currentWniosek.getUprwZatwPrcId() == null && "T".equals(this.czyRozpatrujacy) ) {
			inv.add("Osoba zatwierdzająca nie została wskazana!");
		}
		
		if (currentWniosek.getUprwZastPrcId() == null) {
			User.info("Nie została wybrana osoba zastępująca!");
		}
		
		if (currentWniosek.getUprwStatus() != null && currentWniosek.getUprwStatus() > 0) {
			inv.add("Nie można modyfikować tego wniosku o urlop, jest on już procedowny!");
		}

		
		
		SqlDataSelectionsHandler ret = this.getLW("PPL_ABS_WALIDACJA_URLOPU");
		Map<String, Object> sqlNamedParams = new HashMap<>();
		sqlNamedParams = new HashMap<>();
		sqlNamedParams.put("P_DATA_OD", dataOd);
		sqlNamedParams.put("P_DATA_DO", dataDo);
		sqlNamedParams.put("P_RDA_ID", currentWniosek.getUprwRdaId());
		sqlNamedParams.put("P_PRC_ID", currentWniosek.getUprwPrcId());
		ret.setSqlNamedParams(sqlNamedParams);
		
		if(   !(     ret!=null  
				&& ret.getAllData()!=null 
				&& ret.getAllData().get(0)!=null 
				&& ret.getAllData().get(0).get("dni_na_ktorych_nie_ma_limitu")!=null
				&& "0".equals(""+ret.getAllData().get(0).get("dni_na_ktorych_nie_ma_limitu"))
			  )
		) inv.add("Błąd walidacji wniosku względem okresów limitów na dany urlop!");
	
		
		
		for (String txt : inv) {
			User.alert(txt);
		}

		return inv.isEmpty();
	}



	private DataRow findAbsencjaKolidujaca(Date dataOd, Date dataDo) {
		DataRow ret = null;
		
		for (DataRow dr : this.getLW("PPL_ABS_ABSENCJE").getData()){
			Date absOd = (Date) dr.get("AB_DATA_OD");
			Date absDo = (Date) dr.get("AB_DATA_DO");
			if ( !(dataOd.after(absDo) || dataDo.before(absOd)) ){//jest nieobencosc kolidujaca
				ret = dr;
				break;
			}
		}
		return ret;
	}
	
	
	private DataRow findWniosekKolidujacy(Date dataOd, Date dataDo,long uprwId ) {
		DataRow ret = null;
		
		for (DataRow dr : this.getSqlData("sqlUrlopyWnioskowane")) {
			Date wOd = (Date) dr.get("UPRW_DATA_OD");
			Date wDo = (Date) dr.get("UPRW_DATA_DO");
			if ( !(dataOd.after(wDo) || dataDo.before(wOd)) && uprwId!=dr.getLong("UPRW_ID") ){//jest wniosek kolidujacy
				ret = dr;
				break;
			}
		}
		return ret;
	}
	
	
	private boolean czyPrzekroczonoLimitNowyWnUZ(Date dataOd, Date dataDo, long prcId ){
		Double limit=null;
		Double urlop=0.0;
		Double limit2=null;
		Double urlop2=0.0;
		
		if(getCurrentAbsRodzajDR().get("dni_do_wykorzystania")==null)
			return true;
		else
			limit=Double.valueOf(getCurrentAbsRodzajDR().get("dni_do_wykorzystania").toString());
		
		    if(this.getRodzajeAbsencjiLW().find("UW")!=null && this.getRodzajeAbsencjiLW().find("UW").get("dni_do_wykorzystania")!=null) {
		    	limit2=Double.valueOf(this.getRodzajeAbsencjiLW().find("UW").get("dni_do_wykorzystania").toString());
		    }
		    else {	
		    	if(this.getRodzajeAbsencjiLW().find("UR1")!=null && this.getRodzajeAbsencjiLW().find("UR1").get("dni_do_wykorzystania")!=null) {
		    		limit2=Double.valueOf(this.getRodzajeAbsencjiLW().find("UR1").get("dni_do_wykorzystania").toString());
		    	} else {
		    		return true;
		    	}
		    }
		
		try (Connection conn = JdbcUtils.getConnection()) {
				urlop = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_dni_harm_ind", 
						prcId, asSqlDate(trunc(dataOd)), asSqlDate(trunc(dataDo)));
				
				urlop2 = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_godz_harm_ind", 
						prcId, asSqlDate(trunc(dataOd)), asSqlDate(trunc(dataDo)))/getNormaUrlopowa(dataDo);
							
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		return !(limit - urlop >= 0) || !(limit2 - urlop2 >= 0);
	}
	
	private boolean czyPrzekroczonoLimitNowyWn(Date dataOd, Date dataDo, long prcId, boolean czyGodzinowy ){
		Double limit=null;
		Double limitR=null;
		Double urlop=0.0;

		if ("UŻ".equals( getCurrentAbsRodzajDR().get("RDA_KOD_SERWIS") )){
			return czyPrzekroczonoLimitNowyWnUZ (dataOd, dataDo, prcId);
		}

		
		if(getCurrentAbsRodzajDR().get("dni_do_wykorzystania")==null || getLimitRokDR().get("dni_do_wykorzystania")==null)
			return true;
		else {
			//limit =Double.valueOf(getCurrentAbsRodzajDR().get("dni_do_wykorzystania").toString());
			limit= ((BigDecimal) getCurrentAbsRodzajDR().get("dni_do_wykorzystania")).doubleValue();
			limitR= ((BigDecimal)         getLimitRokDR().get("dni_do_wykorzystania")).doubleValue();
		}
		
		try (Connection conn = JdbcUtils.getConnection()) {
						
			if(czyGodzinowy){
				urlop = JdbcUtils.sqlFnDouble(conn, "PPADM.URLOP_GODZ_LICZBA_GODZIN", prcId, asSqlDate(dataOd), asSqlDate(dataDo))/getNormaUrlopowa(dataDo);
			}else{
				urlop = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_godz_harm_ind", prcId, asSqlDate(trunc(dataOd)), asSqlDate(trunc(dataDo)))/getNormaUrlopowa(dataDo);
			}
						
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}


		return !(limit - urlop >= 0) || !(limitR - urlop >= 0);
	}
	
	private boolean czyPrzekroczonoLimitModWnUZ(Date dataOd, Date dataDo, long prcId, long wnId ){
		String kodStary="";
		Double urlopPoMod=0.0;
		Double urlopPoModDni=0.0;
		Double urlopPrzedMod=0.0;
		Double urlopPrzedModDni=0.0;
		Date dataOdPrzedMod = null;
		Date dataDoPrzedMod = null;
		Double limit=null;
		Double limit2=null;

		
		try (Connection conn = JdbcUtils.getConnection()) {
			Optional<DataRow> findFirst = this.getSql("sqlUrlopyWnioskowane").getData().stream().filter(r -> Long.valueOf( r.get("UPRW_ID").toString() ) == wnId).findFirst();
			DataRow wn = findFirst.get();
			if(wn==null || wn.get("UPRW_RDA_KOD")==null || wn.get("UPRW_DATA_OD")==null || wn.get("UPRW_DATA_DO")==null ){
				return true;
			}else {
				kodStary=wn.get("UPRW_RDA_KOD").toString();
				dataOdPrzedMod=(Date) wn.get("UPRW_DATA_OD");
				dataDoPrzedMod=(Date) wn.get("UPRW_DATA_DO");
				
				limit=Double.valueOf(getCurrentAbsRodzajDR().get("dni_do_wykorzystania").toString());
				
			    if(this.getRodzajeAbsencjiLW().find("UW")!=null && this.getRodzajeAbsencjiLW().find("UW").get("dni_do_wykorzystania")!=null)
			    	limit2=Double.valueOf(this.getRodzajeAbsencjiLW().find("UW").get("dni_do_wykorzystania").toString());
			    else {	
			    	if(this.getRodzajeAbsencjiLW().find("UR1")!=null && this.getRodzajeAbsencjiLW().find("UR1").get("dni_do_wykorzystania")!=null) {
			    		limit2=Double.valueOf(this.getRodzajeAbsencjiLW().find("UR1").get("dni_do_wykorzystania").toString());
			    	} else {
			    		return true;
			    	}
			    }
				
				urlopPoMod = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_godz_harm_ind", 
						prcId, asSqlDate(trunc(dataOd)), asSqlDate(trunc(dataDo)))/getNormaUrlopowa(dataDo);
				
				urlopPrzedMod = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_godz_harm_ind",
	                    prcId, asSqlDate(trunc(dataOdPrzedMod)), asSqlDate(trunc(dataDoPrzedMod)))/getNormaUrlopowa(dataDoPrzedMod);
				
				urlopPoModDni = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_dni_harm_ind", 
						prcId, asSqlDate(trunc(dataOd)), asSqlDate(trunc(dataDo)));
				
				urlopPrzedModDni = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_dni_harm_ind", 
						prcId, asSqlDate(trunc(dataOdPrzedMod)), asSqlDate(trunc(dataDoPrzedMod)));
			}
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		if("UŹ".equals(kodStary) || "UR11".equals(kodStary) ){
			return !(limit - urlopPoModDni + urlopPrzedModDni >= 0) || !(limit2 - urlopPoMod + urlopPrzedMod >= 0);
		}
		else{ 
			if("UW".equals(kodStary) || "UR1".equals(kodStary)){
				return !(limit - urlopPoModDni >= 0) || !(limit2 - urlopPoMod + urlopPrzedMod >= 0);
			}else
				return !(limit - urlopPoModDni >= 0) || !(limit2 - urlopPoMod >= 0);
		}
	}
	
	private boolean czyPrzekroczonoLimitModWn(Date dataOd, Date dataDo, long prcId, boolean czyGodzinowy, long wnId ){
		Double limit=null;
		Double limitR=null;
		String kodNowy=null;
		String kodStary="";
		Double urlopPoMod=0.0;
		Double urlopPrzedMod=0.0;
		Date dataOdPrzedMod = null;
		Date dataDoPrzedMod = null;	
		
		if ("UŻ".equals( getCurrentAbsRodzajDR().get("RDA_KOD") )){
			return czyPrzekroczonoLimitModWnUZ(dataOd, dataDo, prcId, wnId);
		}
		
		if(getCurrentAbsRodzajDR().get("dni_do_wykorzystania")==null || getCurrentAbsRodzajDR().get("RDA_KOD")==null)
			return true;
		else{
			limit=  ((BigDecimal) getCurrentAbsRodzajDR().get("dni_do_wykorzystania")).doubleValue();
			limitR= ((BigDecimal)         getLimitRokDR().get("dni_do_wykorzystania")).doubleValue();
			kodNowy=getCurrentAbsRodzajDR().get("RDA_KOD").toString();
		}
				
		try (Connection conn = JdbcUtils.getConnection()) {
			
			Optional<DataRow> findFirst = this.getSql("sqlUrlopyWnioskowane").getData().stream().filter(r -> Long.valueOf( r.get("UPRW_ID").toString() ) == wnId).findFirst();
			DataRow wn = findFirst.get();
			if(wn==null || wn.get("UPRW_RDA_KOD")==null || wn.get("UPRW_DATA_OD")==null || wn.get("UPRW_DATA_DO")==null ){
				return true;
			}else {
				kodStary=wn.get("UPRW_RDA_KOD").toString();
				dataOdPrzedMod=(Date) wn.get("UPRW_DATA_OD");
				dataDoPrzedMod=(Date) wn.get("UPRW_DATA_DO");
			}
			
			if(czyGodzinowy){
				
				urlopPoMod = JdbcUtils.sqlFnDouble(conn, "PPADM.URLOP_GODZ_LICZBA_GODZIN", prcId, asSqlDate(dataOd), asSqlDate(dataDo))/getNormaUrlopowa(dataDo);
				urlopPrzedMod = JdbcUtils.sqlFnDouble(conn, "PPADM.URLOP_GODZ_LICZBA_GODZIN", prcId, asSqlDate(dataOdPrzedMod), asSqlDate(dataDoPrzedMod))/getNormaUrlopowa(dataDoPrzedMod);
				
			}else{
				urlopPoMod = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_godz_harm_ind", 
						prcId, asSqlDate(trunc(dataOd)), asSqlDate(trunc(dataDo)))/getNormaUrlopowa(dataDo);
				
				urlopPrzedMod = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_godz_harm_ind",
	                    prcId, asSqlDate(trunc(dataOdPrzedMod)), asSqlDate(trunc(dataDoPrzedMod)))/getNormaUrlopowa(dataDoPrzedMod);	
			}
			
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		
		if(kodStary.equals(kodNowy) || (("UŹ".equals(kodStary) || "UR11".equals(kodStary)) && ("UW".equals(kodNowy)|| "UR1".equals(kodNowy)) ) ){
			return !(limit - urlopPoMod + urlopPrzedMod >= 0) || !(limitR - urlopPoMod + urlopPrzedMod >= 0);
		}
		else
		{
			return !(limit - urlopPoMod >= 0) || !(limitR - urlopPoMod >= 0);
		}
	}
	
	
	public void zapiszNowyUrlOdDo() throws InterruptedException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

		if (!validateBeforeSave())
			return;
		

		
		if(currentWniosek.getUprwStatus()!=null && currentWniosek.getUprwStatus()<0)
			currentWniosek.setUprwStatus(0);

		try {	
			HibernateContext.saveOrUpdate(currentWniosek);
			//this.currentWniosek.setSqlBean(this);
			this.getSql("sqlUrlopyWnioskowane").resetTs();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		this.wczytajListeWnioskow(this.rok);
		this.getSql("sqlUrlopyWnioskowane").resetTs();
		this.getRodzajeAbsencjiLW().resetTs();
		this.getLW("PPL_ABS_LIMITY_ROK").resetTs();
		//this.getLW("PPL_URLOPY_WNIOSKOWANIE").resetTs();

	}
	
	

	
	public void odplanujUrlopOdDo() {

		if (currentWniosek.getUprwDataOd() == null || currentWniosek.getUprwDataDo() == null) {
			
			if(czyWybranoAbsencje()){
				User.warn("Nie możesz anulować wybranej absencji");
			}else
			{
				User.warn("Wskaż urlop, który zamierzasz anulować");
			}
			newWniosek();
			return;
		}

		//tymczasowa mozilowsc usuwania urlopów za poprzednie lata	
/*		if (this.rok < new Date().getYear() + 1900) {

			AppBean.addMessage(FacesMessage.SEVERITY_WARN, "Nie można usunąć urlopu za poprzednie lata!");

			System.out.println("Nie można usunąć urlopu!!!!!");
			return;
		}*/

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();

			CriteriaBuilder builder = s.getCriteriaBuilder();
			CriteriaQuery<PptUrlopyWnioskowane> criteria = builder.createQuery(PptUrlopyWnioskowane.class);

			Root<PptUrlopyWnioskowane> from = criteria.from(PptUrlopyWnioskowane.class);

			Predicate p1 = builder.or( builder.between(from.get("uprwDataOd"), currentWniosek.getUprwDataOd(), currentWniosek.getUprwDataDo()),
									   builder.and(  builder.equal(from.get("uprwDataOd"), currentWniosek.getUprwDataOd()),
											         builder.equal(from.get("uprwDataDo"), currentWniosek.getUprwDataDo())) );
			

			Predicate p2 = builder.equal(from.get("uprwPrcId"), user.getPrcId());


			criteria.where(p1, p2);
		

			
			List<PptUrlopyWnioskowane> kasujUrlPlanowane = s.createQuery(criteria).getResultList();

			s.close();

			if (!kasujUrlPlanowane.isEmpty()) {
				for (PptUrlopyWnioskowane urpl : kasujUrlPlanowane) {
					if (urpl.getUprwStatus() == null || urpl.getUprwStatus() < 1) {
						HibernateContext.delete(urpl);
					} else {
						User.alert("Nie można usunąć wprowadzonego urlopu w dniach " + urpl.getUprwDataOd() + " - "
								+ urpl.getUprwDataDo() + "! Skontaktuj się z przełożonym");
					}
				}
			}

			this.getSql("sqlUrlopyWnioskowane").resetTs();
			this.getRodzajeAbsencjiLW().resetTs();
			this.getLW("PPL_ABS_LIMITY_ROK").resetTs();
			//this.getLW("PPL_URLOPY_WNIOSKOWANIE").resetTs();
		}

		newWniosek();
		this.wczytajListeWnioskow(this.rok);

	}
	

	private boolean czyWybranoAbsencje() {
		boolean ret = false;
		
		Date kliknietaData = currentWniosek.getUprwDataOd(); 
		
		
		for (DataRow dr : this.getLW("PPL_ABS_ABSENCJE").getData()){
			Date absOd = (Date) dr.get("AB_DATA_OD");
			Date absDo = (Date) dr.get("AB_DATA_DO");
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(absOd);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			absOd = cal.getTime();
			
			cal.setTime(absDo);
			cal.set(Calendar.HOUR_OF_DAY, 0);
			cal.set(Calendar.MINUTE, 0);
			cal.set(Calendar.SECOND, 0);
			cal.set(Calendar.MILLISECOND, 0);
			absDo = cal.getTime();
			
			
			if (    (kliknietaData.after(absOd) && kliknietaData.before(absDo)) || 
					(absOd.equals(kliknietaData) || absDo.equals(kliknietaData) )   ){//czyli tak naprawde została wybrana absencja
				ret = true;
				break;
			}
		}
		return ret;
	}
	
	
	public void raport() {

		if (this.getCurrentWniosek().getUprwId() == 0) {
			User.info("Nie wybrano żadnego wniosku.", FacesMessage.SEVERITY_WARN);
			return;
		}
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("id_wniosku", this.currentWniosek.getUprwId());
			params.put("pozostalo_dni", new Double( this.getCurrentAbsRodzajDR().get("dni_do_wykorzystania").toString()));
			params.put("zalegle_dni", new Double( this.getCurrentAbsRodzajDR().get("zalegly").toString()));
			com.comarch.egeria.web.common.reports.ReportGenerator.displayReportAsPDF(params, "Wniosek_o_urlop");
		} catch (JRException | SQLException | IOException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}

	}
		
			
	public Integer getRok() {
		return rok;
	}

	public void setRok(Integer naRok) {
		if (eq(naRok, this.rok))
			return;
		
//		if (!eq(naRok, this.rok))
		currentWniosek = new PptUrlopyWnioskowane();
		
		this.wczytajListeWnioskow(naRok);
		this.rok = naRok;
		ArrayList<DataRow> data = this.getRodzajeAbsencjiLW().getData();
		this.getSql("sqlUrlopyWnioskowane").setSqlParams( rok, user.getPrcIdLong() );
		this.getLW("PPL_ABS_ABSENCJE").setSqlParams( rok, user.getPrcIdLong() );
		//this.getSql("sqlSwieta").setSqlParams( rok );
		this.getLW("PPL_SWIETA").setSqlParams(rok);
		this.getLW("PPL_ABS_PLANOWANE").setSqlParams( rok, user.getPrcIdLong() );
		newWniosek();
		//ArrayList<DataRow> norma = this.getLW("PPL_ABS_NORMA_PRACOWNIK", user.getPrcId(), rok, rok).getData();
		
	}
	
	public Date getDzien() {
		return dzien;
	}

	public void setDzien(Date dzien) {
		this.wczytajWniosek(dzien);
		this.dzien = dzien;
	}
	
	public PptUrlopyWnioskowane getCurrentWniosek() {
		return currentWniosek;
	}

	public void setCurrentWniosek(PptUrlopyWnioskowane currentWniosek) {
		this.currentWniosek = currentWniosek;
		this.currentWniosek.setSqlBean(this);
		String uprwRdaKod = currentWniosek.getUprwRdaKod();
		SqlDataSelectionsHandler lw = this.getRodzajeAbsencjiLW();
//		this.setCurrentAbsRodzajDR(lw.find(uprwRdaKod));
	}

	public ArrayList<PptUrlopyWnioskowane> getWnioskiNaDzien() {
		return wnioskiNaDzien;
	}

	public void setWnioskiNaDzien(ArrayList<PptUrlopyWnioskowane> wnioskiNaDzien) {
		this.wnioskiNaDzien = wnioskiNaDzien;
	}

	public ArrayList<Integer> getLata() {
		return lata;
	}

	public void setLata(ArrayList<Integer> lata) {
		this.lata = lata;
	}

	
	public DataRow getLimitRokDR() {
		return this.getLW("PPL_ABS_LIMITY_ROK").find(this.currentWniosek.getUprwRdaKod());
	}

	public DataRow getCurrentAbsRodzajDR() {
		return this.getRodzajeAbsencjiLW().find(this.currentWniosek.getUprwRdaKod());
	}


	public String getNormaUrlopowaString(Date dataDo) {
		
		if(dataDo==null) dataDo=new Date();
		
		String normaUrlopowaString = "";
		
		try (Connection conn = JdbcUtils.getConnection()) {
			normaUrlopowaString = JdbcUtils.sqlFnVarchar(conn, "ek_pck_utl02.czas_z_godzin", getNormaUrlopowa(dataDo));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		if(normaUrlopowaString==null || normaUrlopowaString=="") normaUrlopowaString="8:0";
		return normaUrlopowaString;
	}


	
	public Double getNormaUrlopowa(Date dataDo) {
		
		if(dataDo==null) dataDo=new Date();
		
		Double normaUrlopowa = 0.0;
		try (Connection conn = JdbcUtils.getConnection()) {
			normaUrlopowa = JdbcUtils.sqlFnDouble(conn, "ek_pck_absencje.il_godz_na_dzien_urlopu", user.getPrcIdLong(), asSqlDate(dataDo));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		if(normaUrlopowa==null || normaUrlopowa==0.0) normaUrlopowa=8.0;
		return normaUrlopowa;
	}
	
	
	public String getNormaDobowaString(Date dataDo) {
		
		if(dataDo==null) dataDo=new Date();
		
		String normaDobowaString = "";
		
		try (Connection conn = JdbcUtils.getConnection()) {
			normaDobowaString = JdbcUtils.sqlFnVarchar(conn, "ek_pck_utl02.czas_z_godzin", getNormaDobowa(dataDo));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		if(normaDobowaString==null || normaDobowaString=="") normaDobowaString="8:0";
		return normaDobowaString;
	}
	
	public Double getNormaDobowa(Date dataDo) {
		
		if(dataDo==null) dataDo=new Date();
		
		Double normaDobowa = 0.0;
		try (Connection conn = JdbcUtils.getConnection()) {
			normaDobowa = JdbcUtils.sqlFnDouble(conn, "ek_pck_limity.norma_dobowa_na_dzien", user.getPrcIdLong(), asSqlDate(dataDo));
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		if(normaDobowa==null || normaDobowa==0.0) normaDobowa=8.0;
		return normaDobowa;
	}




	public String getCzyRozpatrujacy() {
		return czyRozpatrujacy;
	}




	public void setCzyRozpatrujacy() {
		if(this.getSlownik("URL_KONFIGURACJA").find("URL_ROZPATRUJACY") != null)
			this.czyRozpatrujacy = "" + this.getSlownik("URL_KONFIGURACJA").find("URL_ROZPATRUJACY").get("RV_HIGH_VALUE");
		else this.czyRozpatrujacy = "N";
	}


}
