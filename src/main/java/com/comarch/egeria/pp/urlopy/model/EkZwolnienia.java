package com.comarch.egeria.pp.urlopy.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_ABS_ZWOLNIENIA database table.
 * 
 */
@Entity
@Table(name="PPT_ABS_ZWOLNIENIA", schema="PPADM")
@NamedQuery(name="EkZwolnienia.findAll", query="SELECT e FROM EkZwolnienia e")
public class EkZwolnienia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ABS_ZWOLNIENIA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ABS_ZWOLNIENIA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ABS_ZWOLNIENIA")
	@Column(name="ZWOL_ID")
	private long zwolId;

	@Column(name="ZWOL_AUDYT_KM")
	private String zwolAudytKm;

	@Column(name="ZWOL_AUDYT_KT")
	private String zwolAudytKt;

	@Column(name="ZWOL_AUDYT_LM")
	private BigDecimal zwolAudytLm;

	@Temporal(TemporalType.DATE)
	@Column(name="ZWOL_DATA_DOSTARCZENIA")
	private Date zwolDataDostarczenia;

	@Temporal(TemporalType.DATE)
	@Column(name="ZWOL_DATA_WYSTAWIENIA")
	private Date zwolDataWystawienia;

	@Column(name="ZWOL_F_NOWY_OKRES_ZAS")
	private String zwolFNowyOkresZas;

	@Column(name="ZWOL_FRM_ID")
	private Long zwolFrmId;

	@Temporal(TemporalType.DATE)
	@Column(name="ZWOL_AUDYT_DM")
	private Date zwolAudytDM;

	@Temporal(TemporalType.DATE)
	@Column(name="ZWOL_AUDYT_DT")
	private Date zwolAudytDT;

	@Column(name="ZWOL_AUDYT_UM")
	private String zwolAudytUM;

	@Column(name="ZWOL_AUDYT_UT")
	private String zwolAudytUT;

	@Column(name="ZWOL_NUMER_ZWOLNIENIA")
	private String zwolNumerZwolnienia;

	@Column(name="ZWOL_OPIS")
	private String zwolOpis;

	@Column(name="ZWOL_PRC_ID")
	private Long zwolPrcId;

	@Column(name="ZWOL_PRZED_ZATR")
	private String zwolPrzedZatr;

	@Column(name="ZWOL_RDA_ID")
	private Long zwolRdaId;

	@Column(name="ZWOL_SERIA_ZWOLNIENIA")
	private String zwolSeriaZwolnienia;

	//bi-directional many-to-one association to EkAbsencjeDane
	@OneToMany(mappedBy="ekZwolnienia")
	private List<EkAbsencjeDane> ekAbsencjeDanes;

	public EkZwolnienia() {
	}

	public long getZwolId() {
		return this.zwolId;
	}

	public void setZwolId(long zwolId) {
		this.zwolId = zwolId;
	}

	public String getZwolAudytKm() {
		return this.zwolAudytKm;
	}

	public void setZwolAudytKm(String zwolAudytKm) {
		this.zwolAudytKm = zwolAudytKm;
	}

	public String getZwolAudytKt() {
		return this.zwolAudytKt;
	}

	public void setZwolAudytKt(String zwolAudytKt) {
		this.zwolAudytKt = zwolAudytKt;
	}

	public BigDecimal getZwolAudytLm() {
		return this.zwolAudytLm;
	}

	public void setZwolAudytLm(BigDecimal zwolAudytLm) {
		this.zwolAudytLm = zwolAudytLm;
	}

	public Date getZwolDataDostarczenia() {
		return this.zwolDataDostarczenia;
	}

	public void setZwolDataDostarczenia(Date zwolDataDostarczenia) {
		this.zwolDataDostarczenia = zwolDataDostarczenia;
	}

	public Date getZwolDataWystawienia() {
		return this.zwolDataWystawienia;
	}

	public void setZwolDataWystawienia(Date zwolDataWystawienia) {
		this.zwolDataWystawienia = zwolDataWystawienia;
	}

	public String getZwolFNowyOkresZas() {
		return this.zwolFNowyOkresZas;
	}

	public void setZwolFNowyOkresZas(String zwolFNowyOkresZas) {
		this.zwolFNowyOkresZas = zwolFNowyOkresZas;
	}

	public Long getZwolFrmId() {
		return this.zwolFrmId;
	}

	public void setZwolFrmId(Long zwolFrmId) {
		this.zwolFrmId = zwolFrmId;
	}

	public Date getZwolAudytDM() {
		return this.zwolAudytDM;
	}

	public void setZwolAudytDM(Date zwolAudytDM) {
		this.zwolAudytDM = zwolAudytDM;
	}

	public Date getZwolAudytDT() {
		return this.zwolAudytDT;
	}

	public void setZwolAudytDT(Date zwolAudytDT) {
		this.zwolAudytDT = zwolAudytDT;
	}

	public String getZwolAudytUM() {
		return this.zwolAudytUM;
	}

	public void setZwolAudytUM(String zwolAudytUM) {
		this.zwolAudytUM = zwolAudytUM;
	}

	public String getZwolAudytUT() {
		return this.zwolAudytUT;
	}

	public void setZwolAudytUT(String zwolAudytUT) {
		this.zwolAudytUT = zwolAudytUT;
	}

	public String getZwolNumerZwolnienia() {
		return this.zwolNumerZwolnienia;
	}

	public void setZwolNumerZwolnienia(String zwolNumerZwolnienia) {
		this.zwolNumerZwolnienia = zwolNumerZwolnienia;
	}

	public String getZwolOpis() {
		return this.zwolOpis;
	}

	public void setZwolOpis(String zwolOpis) {
		this.zwolOpis = zwolOpis;
	}

	public Long getZwolPrcId() {
		return this.zwolPrcId;
	}

	public void setZwolPrcId(Long zwolPrcId) {
		this.zwolPrcId = zwolPrcId;
	}

	public String getZwolPrzedZatr() {
		return this.zwolPrzedZatr;
	}

	public void setZwolPrzedZatr(String zwolPrzedZatr) {
		this.zwolPrzedZatr = zwolPrzedZatr;
	}

	public Long getZwolRdaId() {
		return this.zwolRdaId;
	}

	public void setZwolRdaId(Long zwolRdaId) {
		this.zwolRdaId = zwolRdaId;
	}

	public String getZwolSeriaZwolnienia() {
		return this.zwolSeriaZwolnienia;
	}

	public void setZwolSeriaZwolnienia(String zwolSeriaZwolnienia) {
		this.zwolSeriaZwolnienia = zwolSeriaZwolnienia;
	}

	public List<EkAbsencjeDane> getEkAbsencjeDanes() {
		return this.ekAbsencjeDanes;
	}

	public void setEkAbsencjeDanes(List<EkAbsencjeDane> ekAbsencjeDanes) {
		this.ekAbsencjeDanes = ekAbsencjeDanes;
	}

	public EkAbsencjeDane addEkAbsencjeDane(EkAbsencjeDane ekAbsencjeDane) {
		getEkAbsencjeDanes().add(ekAbsencjeDane);
		ekAbsencjeDane.setEkZwolnienia(this);

		return ekAbsencjeDane;
	}

	public EkAbsencjeDane removeEkAbsencjeDane(EkAbsencjeDane ekAbsencjeDane) {
		getEkAbsencjeDanes().remove(ekAbsencjeDane);
		ekAbsencjeDane.setEkZwolnienia(null);

		return ekAbsencjeDane;
	}

}