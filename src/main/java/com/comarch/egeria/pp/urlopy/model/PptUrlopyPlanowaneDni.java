package com.comarch.egeria.pp.urlopy.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PPT_ABS_URLOPY_PLANOWANE_DNI database table.
 * 
 */
@Entity
@Table(name="PPT_ABS_URLOPY_PLANOWANE_DNI", schema="PPADM")
@NamedQuery(name="PptUrlopyPlanowaneDni.findAll", query="SELECT p FROM PptUrlopyPlanowaneDni p")
public class PptUrlopyPlanowaneDni implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ABS_URLOPY_PLANOWANE_DNI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ABS_URLOPY_PLANOWANE_DNI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ABS_URLOPY_PLANOWANE_DNI")
	@Column(name="URPD_ID")
	private long urpdId;

	@Temporal(TemporalType.DATE)
	@Column(name="URPD_DATA")
	private Date urpdData;

	@Column(name="URPD_KAL_ID")
	private Long urpdKalId;

	@Column(name="URPD_OPIS")
	private String urpdOpis;

	@Column(name="URPD_ORG_ID")
	private Long urpdOrgId;

	@Column(name="URPD_PRC_ID")
	private Long urpdPrcId;

	@Column(name="URPD_ROK")
	private Integer urpdRok;

	@Column(name="URPD_TYP")
	private String urpdTyp;

	//bi-directional many-to-one association to PptUrlopyPlanowane
	@ManyToOne
	@JoinColumn(name="URPD_URPL_ID")
	private PptUrlopyPlanowane pptUrlopyPlanowane;

	public PptUrlopyPlanowaneDni() {
	}

	public long getUrpdId() {
		return this.urpdId;
	}

	public void setUrpdId(long urpdId) {
		this.urpdId = urpdId;
	}

	public Date getUrpdData() {
		return this.urpdData;
	}

	public void setUrpdData(Date urpdData) {
		this.urpdData = urpdData;
	}

	public Long getUrpdKalId() {
		return this.urpdKalId;
	}

	public void setUrpdKalId(Long urpdKalId) {
		this.urpdKalId = urpdKalId;
	}

	public String getUrpdOpis() {
		return this.urpdOpis;
	}

	public void setUrpdOpis(String urpdOpis) {
		this.urpdOpis = urpdOpis;
	}

	public Long getUrpdOrgId() {
		return this.urpdOrgId;
	}

	public void setUrpdOrgId(Long urpdOrgId) {
		this.urpdOrgId = urpdOrgId;
	}

	public Long getUrpdPrcId() {
		return this.urpdPrcId;
	}

	public void setUrpdPrcId(Long urpdPrcId) {
		this.urpdPrcId = urpdPrcId;
	}

	public Integer getUrpdRok() {
		return this.urpdRok;
	}

	public void setUrpdRok(Integer urpdRok) {
		this.urpdRok = urpdRok;
	}

	public String getUrpdTyp() {
		return this.urpdTyp;
	}

	public void setUrpdTyp(String urpdTyp) {
		this.urpdTyp = urpdTyp;
	}

	public PptUrlopyPlanowane getPptUrlopyPlanowane() {
		return this.pptUrlopyPlanowane;
	}

	public void setPptUrlopyPlanowane(PptUrlopyPlanowane pptUrlopyPlanowane) {
		this.pptUrlopyPlanowane = pptUrlopyPlanowane;
	}

}