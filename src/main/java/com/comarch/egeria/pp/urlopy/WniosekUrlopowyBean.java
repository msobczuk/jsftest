package com.comarch.egeria.pp.urlopy;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Named;

import org.hibernate.Transaction;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.urlopy.model.EkAbsencjeDane;
import com.comarch.egeria.pp.urlopy.model.EkZwolnienia;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@Scope("view")
@Named
public class WniosekUrlopowyBean extends SqlBean implements Serializable {

	public String godzina;

	private boolean czyCaly = false;

	private Date dataOd;
	private Date dataDo;
	private String rodzajWniosku;
	private String prcId;
	private String jOrg;
	private String uzasadnienie;
	
	public String getRodzajWniosku() {
		return rodzajWniosku;
	}

	public void setRodzajWniosku(String rodzajWniosku) {
		this.rodzajWniosku = rodzajWniosku;
	}

	public String getPrcId() {
		return prcId;
	}

	public void setPrcId(String prcId) {
		this.prcId = prcId;
	}

	public String getjOrg() {
		return jOrg;
	}

	public void setjOrg(String jOrg) {
		this.jOrg = jOrg;
	}

	public Date getDataOd() {
		return dataOd;
	}

	public void setDataOd(Date dataOd) {
		this.dataOd = dataOd;
	}

	public Date getDataDo() {
		return dataDo;
	}

	public void setDataDo(Date dataDo) {
		this.dataDo = dataDo;
	}

	public boolean isCzyCaly() {
		return czyCaly;
	}

	public void setCzyCaly(boolean czyCaly) {
		this.czyCaly = czyCaly;
	}

	public Map<String, String> pracownicy = new HashMap<>();

	public Map<String, String> getPracownicy() {

		for (DataRow dr : this.getSqlData("sqlPracownicy")) {

			pracownicy.put(dr.get("pp_prc_nazwisko") + " " + dr.get("pp_prc_imie"), dr.get("pp_prc_id") + "");
		}

		return pracownicy;
	}

	public Map<String, String> rodzajeUrlopu = new HashMap<>();

	public Map<String, String> getRodzajeUrlopu() {

		for (DataRow dr : this.getSqlData("sqlRodzajeUrlopu")) {

			rodzajeUrlopu.put(dr.get("pp_rda_nazwa") + "", dr.get("pp_rda_id") + "");
		}

		return rodzajeUrlopu;
	}

	public Map<String, String> jednostkiOrg = new HashMap<>();

	public Map<String, String> getJednostkiOrg() {

		for (DataRow dr : this.getSqlData("sqlJednostka")) {

			jednostkiOrg.put(dr.get("pp_ob_nazwa") + "", dr.get("pp_ob_id") + "");
		}

		return jednostkiOrg;
	}

	public void zatwierdz() {
		if (listaZmienianychWnioskow.size() == 0) {
			return;
		}

		try(HibernateContext h = new HibernateContext()) {
			Transaction t = h.getSession().beginTransaction();

			for (int i = 0; i < listaZmienianychWnioskow.size(); i++) {
				Long wniosekId = Long.parseLong(listaZmienianychWnioskow.get(i));

				EkAbsencjeDane ead = new EkAbsencjeDane();

				ead = (EkAbsencjeDane) h.getSession().get(EkAbsencjeDane.class, wniosekId);

				//ead.setAbStatus("Z"); TODO: DO WYJANSNIENIA  - AKTUALNIE BRAK POLA W BAZIE
				h.getSession().saveOrUpdate(ead);
			}
			t.commit();
		}
	}

	public void setPracownicy(Map<String, String> pracownicy) {
		this.pracownicy = pracownicy;
	}

	private List<String> listaZmienianychWnioskow = new ArrayList<String>();
	//private List<String> listaZmienianychZwolnien = new ArrayList<String>();
	
	public void zmienListeWnioskow(DataRow wybranyWniosek) {
		DataRow temp = wybranyWniosek;
		String id = temp.get("pp_ab_id").toString();
		String idZwol = temp.get("pp_zwol_id").toString();
		for (int i = 0; i < listaZmienianychWnioskow.size(); i++) {
			if (listaZmienianychWnioskow.get(i) == id) {
				listaZmienianychWnioskow.remove(i);
				//listaZmienianychZwolnien.remove(i);
				return;
			}
		}

		listaZmienianychWnioskow.add(id);
		//listaZmienianychZwolnien.add(idZwol);
	}

	public void usun() {
		

		
		try(HibernateContext h = new HibernateContext()){
			
			Transaction t = h.getSession().beginTransaction();
			
			for(int i = 0; i<listaZmienianychWnioskow.size();i++) {
				EkAbsencjeDane ead = new EkAbsencjeDane();
				
				System.out.println("Absencje dane: "+listaZmienianychWnioskow.get(i));
				//System.out.println("Zwolnienie: " +listaZmienianychZwolnien.get(i));
				
				//ezw.setZwolId(Long.parseLong(listaZmienianychZwolnien.get(i)));
				
				ead.setAbId(Long.parseLong(listaZmienianychWnioskow.get(i)));
				
				
				if(ead != null) {
					System.out.println("Usuwam ead");
					h.getSession().delete(ead);
					t.commit();
				}
				
				t.commit();
			}
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void zapisz() {
		EkAbsencjeDane ead = new EkAbsencjeDane();
		EkZwolnienia ezw = new EkZwolnienia();
		
		
		try (HibernateContext h = new HibernateContext()) {
			
			Transaction t = h.getSession().beginTransaction();
			System.out.println("Ustawiam zwolnienie");
			
			System.out.println("Rodzaj wniosku: " +Long.parseLong(rodzajWniosku));
			ezw.setZwolRdaId(Long.parseLong(rodzajWniosku));
			
			System.out.println("Prc id: "+Long.parseLong(prcId));
			ezw.setZwolPrcId(Long.parseLong(prcId));

			
			System.out.println("Ustawiam na sztywno");
			//TODO na sztywno
			ezw.setZwolFNowyOkresZas("N");
			ezw.setZwolFrmId(1L);
			ezw.setZwolPrzedZatr("N");
			
			System.out.println("Uzasadnienie: " +uzasadnienie);
			
			if(!uzasadnienie.isEmpty()) {
				System.out.println("Uzasadnienie nie jest puste");
				ezw.setZwolOpis(uzasadnienie);
			}
			
			System.out.println("Ustawiam absencje");
			
			DateFormat formatter = new SimpleDateFormat("yy/MM/dd");
			String dataOdStr = formatter.format(dataOd);
			String dataDoStr = formatter.format(dataDo);
			System.out.println("Data od: " +formatter.parse(dataOdStr));
			
			ead.setAbDataOd(formatter.parse(dataOdStr));
			System.out.println("Data do: " +formatter.parse(dataDoStr));
			ead.setAbDataDo(formatter.parse(dataDoStr));
			
			int liczbaDni;
			
			System.out.println("Czy caly: " +czyCaly);
			if(czyCaly) {
				liczbaDni = (int) (dataDo.getTime() - dataOd.getTime())/(1000 * 60 * 60 * 24);
				int godzin = 8 + 8 * liczbaDni;
				godzina = String.valueOf(godzin);
				
				System.out.println("Liczba dni" +liczbaDni);
				
				ead.setAbDniWykorzystane(Long.parseLong(String.valueOf(liczbaDni)));
				ead.setAbFCzesc("N");
			} else {
				ead.setAbFCzesc("T");
			}
			
			System.out.println("Wykorzystane godziny: " +godzina);
			//TODO na sztywno
			ead.setAbFWaloryzacja("N");
			ead.setAbFModyfikacja("N");
			ead.setAbFrmId(1L);
			ead.setAbFAnulowana("N");
			
			ead.setAbGodzinyWykorzystane(Long.parseLong(godzina));
			//ead.setAbStatus("P"); //DO WYJASNIENIA - AKTUALNIE BRAK POLA W BAZIE!!
			
			System.out.println("Ustawiam listę");
			List<EkAbsencjeDane> ekAbsencjeDane = new ArrayList<EkAbsencjeDane>();
			ezw.setEkAbsencjeDanes(ekAbsencjeDane);
			ezw.addEkAbsencjeDane(ead);
			
			
//			System.out.println("Zapisuje ead");
//			h.getSession().saveOrUpdate(ead);
			System.out.println("zapisuje ezw");
			h.getSession().saveOrUpdate(ezw);
			t.commit();
			System.out.println("Po commit, odpalam zapis absencji dane");
			zapiszAbsencjeDane(ead);
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void zapiszAbsencjeDane(EkAbsencjeDane ead) {
		ead = (EkAbsencjeDane) HibernateContext.save(ead);
		System.out.println("Zapisano ead");
	}
	
	public String getUzasadnienie() {
		return uzasadnienie;
	}

	public void setUzasadnienie(String uzasadnienie) {
		this.uzasadnienie = uzasadnienie;
	}

	public String getGodzina() {

		return godzina;
	}

	public void setGodzina(String godzina) {
		this.godzina = godzina;
	}
}
