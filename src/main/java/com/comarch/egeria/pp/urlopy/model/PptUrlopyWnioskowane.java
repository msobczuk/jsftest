package com.comarch.egeria.pp.urlopy.model;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.data.model.ModelBase;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

/**
 * The persistent class for the PPT_URLOPY_WNIOSKOWANE database table.
 * 
 */
@Entity
@Table(name = "PPT_ABS_URLOPY_WNIOSKOWANE", schema="PPADM")
@NamedQuery(name = "PptUrlopyWnioskowane.findAll", query = "SELECT p FROM PptUrlopyWnioskowane p")
public class PptUrlopyWnioskowane extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ABS_URLOPY_WNIOSKOWANE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ABS_URLOPY_WNIOSKOWANE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ABS_URLOPY_WNIOSKOWANE")
	@Column(name = "UPRW_ID")
	private long uprwId;

	@Transient
	private Date uprwGodzinaOd;
	
	@Transient
	private Date uprwGodzinaDo;

	
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPRW_DATA_DO")
	private Date uprwDataDo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPRW_DATA_OD")
	private Date uprwDataOd;

/*	@Column(name = "UPRW_KAL_ID")
	private BigDecimal uprwKalId;*/
	
	@Column(name = "UPRW_OPIS")
	private String uprwOpis;

/*	@Column(name = "UPRW_ORG_ID")
	private BigDecimal uprwOrgId;*/

	@Column(name = "UPRW_PRC_ID")
	private Long uprwPrcId;

	@Column(name = "UPRW_RDA_KOD")
	private String uprwRdaKod;
	
	@Column(name = "UPRW_RDA_ID")
	private Integer uprwRdaId;

	@Column(name = "UPRW_STATUS")
	private Integer uprwStatus;
	
	@Column(name = "UPRW_TYP")
	private String uprwTyp;

	@Column(name = "UPRW_ZAST_PRC_ID")
	private Integer uprwZastPrcId;
	
	@Column(name = "UPRW_ZATW_PRC_ID")
	private Long uprwZatwPrcId;
	
	@Column(name = "UPRW_ZATWIERDZIL_PRC_ID")
	private Long uprwZatwierdzilPrcId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPRW_DATA_ZATWIERDZENIA")
	private Date uprwDataZatwierdzenia;
	
	@Column(name = "UPRW_DG_KOD")
	private String uprwDgKod;
	
	@Transient
	private Integer rok = LocalDate.now().getYear();
	
	@Transient
	public boolean czyStaryWidokWniosekUrlopowyXhtml = false; 
	
	
	public PptUrlopyWnioskowane() {
	}

	public long getUprwId() {
		this.onRXGet("uprwId");
		return this.uprwId;
	}

	public void setUprwId(long uprwId) {
		this.onRXSet("uprwId", this.uprwId, uprwId);
		this.uprwId = uprwId;
	}
	
	

	public void aktualizujDomyslneGodzinyOdDo(Date naDzien){
		if(naDzien == null) return; 
		
		this.onRXSet("uprwDataDo", this.uprwDataDo, naDzien);
		this.onRXSet("uprwDataOd", this.uprwDataOd, naDzien);
		this.uprwDataDo=naDzien;
		this.uprwDataOd=naDzien;
		
		SqlDataSelectionsHandler lw = this.getSqlBean().getLW("PPL_URL_GODZINY_DOM", uprwPrcId, naDzien);
		
		if (lw !=null && !lw.getData().isEmpty()) {
			DataRow r = lw.getData().get(0);
				if(r!=null && r.get("od")!=null && r.get("do")!=null) {
					this.onRXSet("uprwDataDo", this.uprwDataDo, r.get("od"));
					this.onRXSet("uprwDataOd", this.uprwDataOd, r.get("do"));
					this.uprwDataOd=(Date) r.get("od");
					this.uprwDataDo=(Date) r.get("do");
				}
		}	
	}

	public Date getUprwDataDo() {
		//this.setUprwGodzinaDo(this.uprwDataDo);
		this.onRXGet("uprwDataDo");
		return this.uprwDataDo;
	}

	public void setUprwDataDo(Date uprwDataDo) {
		
		if(this.getCzyGodzinowy()){
			if(uprwDataDo!=this.uprwDataDo) aktualizujDomyslneGodzinyOdDo(uprwDataDo);
		}
		else {
			this.onRXSet("uprwDataDo", this.uprwDataDo, uprwDataDo);
			this.uprwDataDo = uprwDataDo;
		}
		
		
		//sprawdzenie czy data DO nie jest przed datą OD, jezeli tak to zamiana dat
		if(this.getUprwDataOd()!=null && this.getUprwDataDo()!=null && this.getUprwDataDo().before(this.getUprwDataOd())) {
			Date dataPom = this.getUprwDataOd();
			this.uprwDataOd = this.uprwDataDo;
			this.uprwDataDo = dataPom;
		}
		
	}
	
	
	public Date getUprwGodzinaDo() {
		this.onRXGet("uprwDataDo");
		return this.uprwDataDo;
	}

	public void setUprwGodzinaDo(Date uprwGodzinaDo) {
		this.onRXSet("uprwGodzinaDo", this.uprwGodzinaDo, uprwGodzinaDo);
		this.uprwGodzinaDo = uprwGodzinaDo;
		
		//User.warn("... setUprwGodzinaDo: " + uprwGodzinaDo);
				
		if(uprwGodzinaDo!=null){
			
			if (this.uprwDataDo==null)
				this.uprwDataDo = new Date();
			
			Calendar calGDo = Calendar.getInstance();
			calGDo.setTime(this.uprwGodzinaDo); 
			
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.uprwDataDo);  
			cal.set(Calendar.HOUR_OF_DAY, calGDo.get(Calendar.HOUR_OF_DAY));  
	        cal.set(Calendar.MINUTE, calGDo.get(Calendar.MINUTE)); 
	        cal.set(Calendar.SECOND, 0); 
	        this.uprwDataDo=cal.getTime();
		}
		
		
		
	}
	
	
	
	
	
	
	
	
	public Date getUprwDataOd() {
		this.onRXGet("uprwDataOd");
		//this.setUprwGodzinaOd(this.uprwDataOd);
		return this.uprwDataOd;
	}

	public void setUprwDataOd(Date uprwDataOd) {
		
		if(this.getCzyGodzinowy()){
			if(uprwDataOd!=this.uprwDataOd) aktualizujDomyslneGodzinyOdDo(uprwDataOd);
		}
		else {
			this.onRXSet("uprwDataOd", this.uprwDataOd, uprwDataOd);
			this.uprwDataOd = uprwDataOd;
		//this.setUprwGodzinaOd(this.uprwDataOd);
		}

	}

	
	
	
	public Date getUprwGodzinaOd() {
		this.onRXGet("uprwDataOd");
		//return uprwGodzinaOd;
		return uprwDataOd;
		
	}

	public void setUprwGodzinaOd(Date uprwGodzinaOd) {
		this.onRXSet("uprwGodzinaOd", this.uprwGodzinaOd, uprwGodzinaOd);
		this.uprwGodzinaOd = uprwGodzinaOd;
		
				
		if(uprwGodzinaOd!=null){
			
			if (this.uprwDataOd==null)
				this.uprwDataOd = new Date();
			
			Calendar calGOd = Calendar.getInstance();
			calGOd.setTime(this.uprwGodzinaOd); 
			
			
			Calendar cal = Calendar.getInstance();
			cal.setTime(this.uprwDataOd);  
	        cal.set(Calendar.HOUR_OF_DAY, calGOd.get(Calendar.HOUR_OF_DAY));  
	        cal.set(Calendar.MINUTE, calGOd.get(Calendar.MINUTE)); 
	        cal.set(Calendar.SECOND, 0); 
	        this.uprwDataOd=cal.getTime();
		}
	}

	
	
	
	
/*	public BigDecimal getUprwKalId() {
		return this.uprwKalId;
	}

	public void setUprwKalId(BigDecimal uprwKalId) {
		this.uprwKalId = uprwKalId;
	}*/

	public String getUprwOpis() {
		this.onRXGet("uprwOpis");
		return this.uprwOpis;
	}

	public void setUprwOpis(String uprwOpis) {
		this.onRXSet("uprwOpis", this.uprwOpis, uprwOpis);
		this.uprwOpis = uprwOpis;
	}

/*	public BigDecimal getUprwOrgId() {
		return this.uprwOrgId;
	}

	public void setUprwOrgId(BigDecimal uprwOrgId) {
		this.uprwOrgId = uprwOrgId;
	}*/

	public Long getUprwPrcId() {
		this.onRXGet("uprwPrcId");
		return this.uprwPrcId;
	}

	public void setUprwPrcId(Long uprwPrcId) {
		this.onRXSet("uprwPrcId", this.uprwPrcId, uprwPrcId);
		this.uprwPrcId = uprwPrcId;
	}

	public String getUprwRdaKod() {
		this.onRXGet("uprwRdaKod");
		return this.uprwRdaKod;
	}

	public void setUprwRdaKod(String uprwRdaKod) {
		this.onRXSet("uprwRdaKod", this.uprwRdaKod, uprwRdaKod);
		this.uprwRdaKod = uprwRdaKod;
		
		
		//jezeli to jest stary widok(WniosekUrlopowy.xhtml) to ten kod musi sie wykonac	
		if(czyStaryWidokWniosekUrlopowyXhtml && this.getLW("PPL_ABS_RODZAJE_ABSENCJI")!=null && this.getLW("PPL_ABS_RODZAJE_ABSENCJI").find(uprwRdaKod)!=null) {
			this.uprwDgKod = "" + this.getLW("PPL_ABS_RODZAJE_ABSENCJI").find(uprwRdaKod).get("dg_kod");
		}
		
		
		if(this.getCzyGodzinowy() && this.uprwDataOd==null && this.uprwDataDo==null){
			aktualizujDomyslneGodzinyOdDo(new Date());
		}
		
		else if(this.getCzyGodzinowy()){
			aktualizujDomyslneGodzinyOdDo( this.uprwDataDo==null ? this.uprwDataOd : this.uprwDataDo );
		}
		
		DataRow r = getRdaKodLW().find(uprwRdaKod);// getSql("PPL_ABS_RDA_KOD").find(uprwRdaKod);
		if(r!=null) {
			BigDecimal rdaId = (BigDecimal) r.get("RDA_ID");
			setUprwRdaId(rdaId.intValue());
		}
		
	}

	public Integer getUprwStatus() {
		this.onRXGet("uprwStatus");
		return uprwStatus;
	}

	public void setUprwStatus(Integer uprwStatus) {
		this.onRXSet("uprwStatus", this.uprwStatus, uprwStatus);
		this.uprwStatus = uprwStatus;
	}
	
	public String getUprwTyp() {
		this.onRXGet("uprwTyp");
		return this.uprwTyp;
	}

	public void setUprwTyp(String uprwTyp) {
		this.onRXSet("uprwTyp", this.uprwTyp, uprwTyp);
		this.uprwTyp = uprwTyp;
	}
	
	public Integer getUprwZastPrcId() {
		this.onRXGet("uprwZastPrcId");
		return uprwZastPrcId;
	}

	public void setUprwZastPrcId(Integer uprwZastPrcId) {
		this.onRXSet("uprwZastPrcId", this.uprwZastPrcId, uprwZastPrcId);
		this.uprwZastPrcId = uprwZastPrcId;
	}

	public Integer getUprwRdaId() {
		this.onRXGet("uprwRdaId");
		return uprwRdaId;
	}

	public void setUprwRdaId(Integer uprwRdaId) {
		this.onRXSet("uprwRdaId", this.uprwRdaId, uprwRdaId);
		this.uprwRdaId = uprwRdaId;
	}
	
	public Long getUprwZatwPrcId() {
		this.onRXGet("uprwZatwPrcId");
		return uprwZatwPrcId;
	}

	public void setUprwZatwPrcId(Long uprwZatwPrcId) {
		this.onRXSet("uprwZatwPrcId", this.uprwZatwPrcId, uprwZatwPrcId);
		this.uprwZatwPrcId = uprwZatwPrcId;
	}

	public Long getUprwZatwierdzilPrcId() {
		this.onRXGet("uprwZatwierdzilPrcId");
		return uprwZatwierdzilPrcId;
	}

	public void setUprwZatwierdzilPrcId(Long uprwZatwierdzilPrcId) {
		this.onRXSet("uprwZatwierdzilPrcId", this.uprwZatwierdzilPrcId, uprwZatwierdzilPrcId);
		this.uprwZatwierdzilPrcId = uprwZatwierdzilPrcId;
	}

	public Date getUprwDataZatwierdzenia() {
		this.onRXGet("uprwDataZatwierdzenia");
		return uprwDataZatwierdzenia;
	}

	public void setUprwDataZatwierdzenia(Date uprwDataZatwierdzenia) {
		this.onRXSet("uprwDataZatwierdzenia", this.uprwDataZatwierdzenia, uprwDataZatwierdzenia);
		this.uprwDataZatwierdzenia = uprwDataZatwierdzenia;
	}
	
	public String getUprwDgKod() {
		this.onRXGet("uprwDgKod");
		return uprwDgKod;
	}

	public void setUprwDgKod(String uprwDgKod) {
		this.onRXSet("uprwDgKod", this.uprwDgKod, uprwDgKod);
		this.uprwDgKod = uprwDgKod;

		String rdaKodDomyslny = (String) getRdaKodLW().first("RDA_KOD");
		this.setUprwRdaKod(rdaKodDomyslny);
	}
	
	public boolean getCzyGodzinowy(){
		
		if (this.getUprwRdaKod()==null) return false;
		
		String rdaKod = this.getUprwRdaKod();
		SqlDataSelectionsHandler sDH = this.getLW("PPL_ABS_RDA_KOD_NAZWA");
		if(sDH==null) 
			return false;
		else
		{
			DataRow dr = sDH.find(rdaKod);
		
			if (dr==null) return false;
			
			String rdaNazwa = (String) dr.get("RDA_NAZWA");
            return rdaNazwa.toLowerCase().contains("godz");
		}
	
	}
	
	public SqlDataSelectionsHandler getDgKodLW() {
		Map<String, Object> sqlNamedParams = new HashMap<>();		
		SqlDataSelectionsHandler ret = this.getLW("PPL_ABS_DG_KOD");
		sqlNamedParams = new HashMap<>();
		sqlNamedParams.put("P_PRC_ID", uprwPrcId);
		sqlNamedParams.put("P_ROK", rok);
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, rok);
		cal.set(Calendar.MONTH, 11); // 11 = december
		cal.set(Calendar.DAY_OF_MONTH, 31); // new years eve
		Date koniecRoku = cal.getTime();
		
		sqlNamedParams.put("P_DATA_DO", this.getUprwDataDo()==null ? koniecRoku : this.getUprwDataDo() );
		ret.setSqlNamedParams(sqlNamedParams);
		return ret;
	}
	
	public SqlDataSelectionsHandler getRdaKodLW() {

		Map<String, Object> sqlNamedParams = new HashMap<>();		
		SqlDataSelectionsHandler ret = this.getLW("PPL_ABS_RDA_KOD");
		sqlNamedParams = new HashMap<>();
		sqlNamedParams.put("P_PRC_ID", uprwPrcId);
		sqlNamedParams.put("P_ROK", rok);
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, rok);
		cal.set(Calendar.MONTH, 11); // 11 = december
		cal.set(Calendar.DAY_OF_MONTH, 31); // new years eve
		Date koniecRoku = cal.getTime();
		
		sqlNamedParams.put("P_DG_KOD", this.getUprwDgKod());
		sqlNamedParams.put("P_DATA_DO", this.getUprwDataDo()==null ? koniecRoku : this.getUprwDataDo() );
		ret.setSqlNamedParams(sqlNamedParams);
		return ret;
	}
	
	public DataRow getRdaKodRow() {
		return this.getRdaKodLW().find(this.getUprwRdaKod());
	}
	
	public DataRow getUrlopNaZadanieDataRow() {

		Map<String, Object> sqlNamedParams = new HashMap<>();		
		SqlDataSelectionsHandler ret = this.getLW("PPL_ABS_URLOP_NA_ZADANIE");
		sqlNamedParams = new HashMap<>();
		sqlNamedParams.put("P_PRC_ID", uprwPrcId);
		sqlNamedParams.put("P_ROK", rok);
		
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, rok);
		cal.set(Calendar.MONTH, 11); // 11 = december
		cal.set(Calendar.DAY_OF_MONTH, 31); // new years eve
		Date koniecRoku = cal.getTime();
		

		sqlNamedParams.put("P_DATA_DO", this.getUprwDataDo()==null ? koniecRoku : this.getUprwDataDo() );
		ret.setSqlNamedParams(sqlNamedParams);
		return ret.first();
	}
	
	public boolean czyWybranoUrlopNaZadanie() {
		String kodyUrlopNaZadanie = "";
		if(this.getUrlopNaZadanieDataRow()!=null) {
			kodyUrlopNaZadanie = "" +this.getUrlopNaZadanieDataRow().get("rda_kody");
		} else {
			return false;
		}
		
		
		if(this.getUprwRdaKod() != null && kodyUrlopNaZadanie.contains(this.getUprwRdaKod())) {
			return true;
		} else {
			return false;
		}
	}

	
	
	public Integer getRok() {
		return rok;
	}

	public void setRok(Integer naRok) {
		this.rok = naRok;
	}





}