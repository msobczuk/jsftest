package com.comarch.egeria.pp.urlopy;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.rowset.CachedRowSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.JdbcOutParameter;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.urlopy.model.PptUrlopyWnioskowane;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;
import oracle.jdbc.OracleTypes;

@ManagedBean
@Scope("view")
@Named
public class ListaWniskowUrlopowychBean extends SqlBean{
	private static final Logger log = LogManager.getLogger();

	private static final long serialVersionUID = 1L;
	
	private Long uprwIdDoOdrzucenia;
	private String uwaga;

	@Inject
	User user;
	


	public void zatwierdzWniosek(DataRow r, String uprawnienia) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		if( r!=null ){
			if( ("calosc").equals(uprawnienia) ){
				//zatwierdzenie wniosku
				
				try (Connection conn = JdbcUtils.getConnection()) {
				
					Object prcid = r.get("pp_prc_id");
					Object rdaid = r.get("uprw_rda_id");
					Object dataod = r.get("od_uprw");
					Object datado = r.get("do_uprw");
					
					
					
					java.util.Date utilDate = (Date) dataod;
					java.sql.Timestamp timestampOd = new java.sql.Timestamp(utilDate.getTime());
	
					utilDate = (Date) datado;
					java.sql.Timestamp timestampDo = new java.sql.Timestamp(utilDate.getTime());
					
					JdbcOutParameter pBlad = new JdbcOutParameter(OracleTypes.VARCHAR);
					String opis = "";
					
					Integer ret = JdbcUtils.sqlFnInteger(conn, "EKP_API_INTRANET.WSTAW_ABSENCJE", prcid, rdaid, timestampOd, timestampDo, pBlad, opis);
					
					if (ret == 1) {
						JdbcUtils.sqlExecNoQuery(conn,
								"UPDATE PPADM.PPT_ABS_URLOPY_WNIOSKOWANE set UPRW_STATUS=10,"
																	  + "  UPRW_DATA_ZATWIERDZENIA=sysdate,"
																	  + "  UPRW_ZATWIERDZIL_PRC_ID=?"
								+ " WHERE UPRW_ID=?", user.getPrcIdLong(),r.getIdAsLong());
					} else {

						if (pBlad != null && !pBlad.toString().equals("null")) {
							User.alert("" + pBlad.getValue());
						} else {
							User.alert(
									"Nieudane zatwierdzenie wniosku urlopowego - brak kalendarza indywidualnego na wybrany okres od "
											+ timestampOd + " do " + timestampDo);
						}
						log.error("" + pBlad.getValue());
					}
				}
				
			}
			else
			{
				//narazie tego nie ma, procesowej dalej wniosku, zwiększenie statusu o jeden
			}
		}
		
	}

	public void odrzucWniosek()
			throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		if(uprwIdDoOdrzucenia==null || uprwIdDoOdrzucenia.longValue()==0) {
			return;
		}
		
		if(this.uwaga == null || "".equals(this.uwaga)){
			User.info("Wniosek urlopowy został odrzucony bez podania uzasadnienia!");
			//return;
		}
		
		

		// 2. ustawic status i ew. komentarz
		try (Connection conn = JdbcUtils.getConnection()) {
			JdbcUtils.sqlExecNoQuery(conn, "UPDATE PPADM.PPT_ABS_URLOPY_WNIOSKOWANE set UPRW_STATUS=-1,"
																			   + "  UPRW_DATA_ZATWIERDZENIA=sysdate,"
																			   + "  UPRW_ZATWIERDZIL_PRC_ID=?,"
																			   + "  UPRW_OPIS=?"
					  + " WHERE UPRW_ID=?",
					user.getPrcIdLong(),
					this.uwaga,
					this.uprwIdDoOdrzucenia);
		}
		
		this.uwaga = "";
		this.uprwIdDoOdrzucenia = null;
	}

	public Long getUprwIdDoOdrzucenia() {
		return uprwIdDoOdrzucenia;
	}

	public void setUprwIdDoOdrzucenia(Long uprwIdDoOdrzucenia) {
		this.uprwIdDoOdrzucenia = uprwIdDoOdrzucenia;
	}

	public String getUwaga() {
		return uwaga;
	}

	public void setUwaga(String uwaga) {
		this.uwaga = uwaga;
	}

}