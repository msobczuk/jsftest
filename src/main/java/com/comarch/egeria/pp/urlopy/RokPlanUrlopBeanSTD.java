package com.comarch.egeria.pp.urlopy;

import static com.comarch.egeria.Utils.*;
import java.io.Serializable;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.urlopy.model.PptUrlopyPlanowane;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@Scope("view")
@Named
public class RokPlanUrlopBeanSTD extends SqlBean implements Serializable {
	
	@Inject
	User user;

	private Integer rok = (LocalDate.now().getYear());
	private ArrayList<Integer> lata = new ArrayList<>();
	private PptUrlopyPlanowane urlop =new PptUrlopyPlanowane();
	private Date dzien = new Date();
	ArrayList<PptUrlopyPlanowane> wnioskiNaRok = new ArrayList<>();
	private String uwaga;
	private String status;
	//private Double godzin;

	
	
	@PostConstruct
	public void init(){
		

		this.lata.add(rok + 1);
		this.lata.add(rok);
		this.lata.add(rok - 1);

		
		ArrayList<DataRow> data= this.getLW("PPL_PLANY_LIMIT_STD", user.getPrcId(), rok).getData();
		
/*		this.loadSqlData("sqlSwieta", ""
				+ " select "
				+ "    da_id  "
				+ "   ,da_rd_kod "
				+ "   ,da_data "
				+ " from  EGADM1.EK_DATY "
				+ " where extract (year from da_data) = ? "
				+ " order by DA_DATA"
				, rok);*/
		
		this.getLW("PPL_SWIETA", rok).getData();
				
		this.getLW("PPL_URL_URLOPY_PLANOWANE", rok, user.getPrcId()).getData();
					
		wczytajListeWnioskow(this.rok);

		ustawUwage();
		newWniosek();	
	}
	
	public void ustawUwage(){
		this.status="";
		this.uwaga="";
		if(!this.getLW("PPL_URL_URLOPY_PLANOWANE").getData().isEmpty())
			this.status=this.getLW("PPL_URL_URLOPY_PLANOWANE").getData().get(0).get("URPL_STATUS").toString();
		if(!this.getLW("PPL_URL_URLOPY_PLANOWANE").getData().isEmpty() && "O".equals( this.getLW("PPL_URL_URLOPY_PLANOWANE").getData().get(0).get("URPL_STATUS").toString()) )
			this.uwaga=""+this.getLW("PPL_URL_URLOPY_PLANOWANE").getData().get(0).get("URPL_OPIS");	
	}
	
	public void newWniosek(){
		PptUrlopyPlanowane newWniosek = new PptUrlopyPlanowane();
		newWniosek.setUrplPrcId(user.getPrcIdLong());
		this.setUrlop(newWniosek);
	}
	
	public void wczytajListeWnioskow(Integer rok){

		this.wnioskiNaRok.clear();		
		String hql = String.format( " FROM PptUrlopyPlanowane x WHERE x.urplPrcId = %1$s and ( year(x.urplDataOd) <= %2$s and %2$s <= year(x.urplDataDo)  ) ORDER BY x.urplDataOd" , user.getPrcIdLong(), rok);
		this.wnioskiNaRok.addAll(HibernateContext.query(hql));
	}
	
	public void wczytajWniosek(Date naDzien){
		
		LocalDate ldtNaDzien = asLocalDate(naDzien);
				
		Boolean czyUstawionaDataOd = false;
		Boolean czyJestUrlopNaDzien = false;
		Date od = new Date();
		if(this.urlop.getUrplDataOd()!=null && this.urlop.getUrplDataDo()==null){
			czyUstawionaDataOd=true;
			od=this.urlop.getUrplDataOd();
		}
		
		//this.setCurrentWniosek(null);
		newWniosek();
		
		for (PptUrlopyPlanowane wn : this.wnioskiNaRok) {
			LocalDate ldtOd = asLocalDate( wn.getUrplDataOd() );
			LocalDate ldtDo = asLocalDate( wn.getUrplDataDo() );
			
			
			if ( ldtNaDzien.equals(ldtOd) 
					|| ldtOd.isBefore(ldtNaDzien) && ldtDo.isAfter(ldtNaDzien) 
					|| ldtDo.isEqual(ldtNaDzien)
					){
				this.setUrlop(wn);
				czyJestUrlopNaDzien=true;
			}

		}
		
		
		if (!czyJestUrlopNaDzien)
		{
			newWniosek();
			if(czyUstawionaDataOd){
				if(od.before(naDzien)){
					this.urlop.setUrplDataOd(od);
					this.urlop.setUrplDataDo(naDzien);
				}
				else
				{
					this.urlop.setUrplDataOd(naDzien);
					this.urlop.setUrplDataDo(od);	
				}	
			}
			else
			{
				this.urlop.setUrplDataOd(naDzien);
			}
		}	
		
		
	}
	
	
	
	public static List<Date> getDaysBetweenDates(Date startdate, Date enddate) {
		List<Date> dates = new ArrayList<Date>();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(startdate);

		while (calendar.getTime().before(enddate)) {
			Date result = calendar.getTime();
			dates.add(result);
			calendar.add(Calendar.DATE, 1);
		}
		dates.add(enddate);
		return dates;
	}
	
	
	public String blueDays() {
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");

		Collection<Date> dni = new LinkedList<>();

		HashMap<Date, String> dniHM = new HashMap<>();

		for (DataRow dr : this.getSqlData("PPL_URL_URLOPY_PLANOWANE")) {

			String cssClass = calcBlueDayCssClass(dr);

			List<Date> days = getDaysBetweenDates((Date) dr.get("URPL_DATA_OD"), (Date) dr.get("URPL_DATA_DO"));
			dni.addAll(days);
			for (Date d : days) {
				dniHM.put(d, cssClass);
			}

			Calendar c = Calendar.getInstance();
			for (Date d : dni) {
				c.setTime(d);
				int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
				if (dayOfWeek != 1 && dayOfWeek != 7) {
					String dts = dtf.format(d);
					sb.append((dniHM.get(d) + "        ").substring(0, 7) + dts + ", ");
				}
			}

		}
//		System.out.println("blueDays -> " + sb.toString()); 
		return sb.toString(); //blueDays -> 	
	}
	
	private String calcBlueDayCssClass(DataRow dr) {
		String status = ("" + dr.get("URPL_STATUS")).toUpperCase();
		String cssClass = ("" + dr.get("URPL_TYP")).toUpperCase();
		
		if (status == null)
			return cssClass;
		
		if ("Z".equals(status)) {// zatwierdzono wniosek
			cssClass = "t-" + cssClass;
		} else if ("O".equals(status)) {// odrzucono wniosek
			cssClass = "n-" + cssClass;
		} else if ("N".equals(status)) {// oczekujacy wniosek
			cssClass = "o-" + cssClass;
		}
		return cssClass;
	}
	
	
	/**
	 * Zwraca ciąg tekstowy z klasami css i datami swiątecznymi w EGR
	 * @return np. "2018/4/9, 2018/4/10, 2018/4/11, 2018/4/12, 2018/4/12," 
	 */
	public String swietaDays() {

		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");

		Collection<Date> dni = new LinkedList<>();

		for (DataRow dr : this.getLW("PPL_SWIETA", rok).getData()) {
			dni.add((Date) dr.get("DA_DATA"));
		}

		Calendar c = Calendar.getInstance();
		for (Date d : dni) {
			c.setTime(d);
			int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
			if (dayOfWeek != 1 && dayOfWeek != 7) {
				String dts = dtf.format(d);
				sb.append(dts + ", ");
			}
		}

		return sb.toString();
	}
	
	
	/**
	 * Zwraca ciąg tekstowy z klasami css i datami dla dni objetych nieobecnosciami w EGR
	 * @return np. "2018/4/9, 2018/4/10, 2018/4/11, 2018/4/12, 2018/4/12," 
	 */
	public String lightBlueDays() {
//		System.out.println("lightBlueDays ...................................................................................................");
		StringBuilder sb = new StringBuilder();
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy/M/d");
		
		if(urlop == null || urlop.getUrplDataOd()==null || urlop.getUrplDataDo()==null)
		{
			return null;
		}
		else
		{
			if(urlop.getUrplDataOd().getTime()<=urlop.getUrplDataDo().getTime())
			{
				List<Date> dni = getDaysBetweenDates(urlop.getUrplDataOd(), urlop.getUrplDataDo());
				//dni.add(pptUrlopyPlanowane.getUrplDataDo());
				Calendar c = Calendar.getInstance();
				for (Date d : dni) {
					c.setTime(d);
					int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
					if (dayOfWeek != 1 && dayOfWeek != 7) {
						String dts = dtf.format(d);
						sb.append(dts + ", ");
					}
				}
				return sb.toString();
			}
		}		
		return null;
		
	}
	
	public String mapujBySL(String sqlId, String key, String defaultvalue){
		SqlDataSelectionsHandler sl = this.getSlownik(sqlId);
		DataRow r = sl.find(key);
		if (r==null)
		 return defaultvalue;
		
		
		String ret = ""+r.get(1);
		return ret;
	} 
	
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public Double getLiczbaDniWniosekLacznie() {

		Double liczbaDni = 0.0;
		Double godzin = 0.0;
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		
		if(urlop.getUrplDataOd()!=null && urlop.getUrplDataDo()!=null)
		{
			try (Connection conn = JdbcUtils.getConnection()) {
				liczbaDni = JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_godz_harm_ind", user.getPrcIdLong(),
						asSqlDate(urlop.getUrplDataOd()), asSqlDate(urlop.getUrplDataDo()));
				
				godzin = JdbcUtils.sqlFnDouble(conn, "ek_pck_absencje.il_godz_na_dzien_urlopu", user.getPrcIdLong(), asSqlDate(urlop.getUrplDataDo()));
	
			} catch (Exception e) {
				e.printStackTrace();
			}
			if(godzin==null || godzin==0.0) return null;	
			return round(liczbaDni.doubleValue()/godzin,2);
		}
		else
		{
			return null;
		}	

	}
	
	
	public Double getPozostaloDni() {
		Double ret = 20D; // TODO.. ustalic dla kogo ile...!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		Double godzin = 0.0;
		SqlDataSelectionsHandler lwLimitDni=this.getLW("PPL_PLANY_LIMIT_STD");
		
		
		if( this.urlop.getUrplTyp()!=null && 
			!lwLimitDni.getData().isEmpty() && 
			lwLimitDni.find(this.urlop.getUrplTyp())!=null && 
			lwLimitDni.find(this.urlop.getUrplTyp()).get("ld_dni")!=null
		)
		{
			ret=Double.valueOf(lwLimitDni.find(this.urlop.getUrplTyp()).get("ld_dni").toString());
		}
		else
		{
			return 0.0D;
		}
		
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		//zF.stream().filter(z -> z.getPkzfKwota()>0).collect(Collectors.toList());
		try (Connection conn = JdbcUtils.getConnection()) {
				for (PptUrlopyPlanowane wn : this.wnioskiNaRok.stream().filter(u -> this.urlop.getUrplTyp().equals(u.getUrplTyp())).collect(Collectors.toList())) {
					
						godzin = JdbcUtils.sqlFnDouble(conn, "ek_pck_absencje.il_godz_na_dzien_urlopu", user.getPrcIdLong(), asSqlDate(wn.getUrplDataDo()));
						
						if(!(godzin==null || godzin==0.0))
						ret =ret -  JdbcUtils.sqlFnDouble(conn, "ek_pck_procedury_hi.z_godz_harm_ind", user.getPrcIdLong(),
								asSqlDate(wn.getUrplDataOd()), asSqlDate(wn.getUrplDataDo())).doubleValue()/godzin;
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return round(ret,2);
	}
	
	
	public Double getLiczbaDniNaRok() {
		SqlDataSelectionsHandler lwLimitDni=this.getLW("PPL_PLANY_LIMIT_STD");
		Double ret = 0.0D;
		
		if(lwLimitDni!=null 
				&& lwLimitDni.find(this.urlop.getUrplTyp())!=null 
				&& lwLimitDni.find(this.urlop.getUrplTyp()).get("ld_dni")!=null
		)
			ret=Double.valueOf(lwLimitDni.find(this.urlop.getUrplTyp()).get("ld_dni").toString());
		
		return round(ret,2);
	}
	
	
	public boolean validateBeforeSave() {
		ArrayList<String> inv = new ArrayList<String>();
		
		if(this.urlop.getUrplTyp()==null || "".equals(this.urlop.getUrplTyp()))
		{
			User.alert("Należy wybrać typ planowanego urlopu!");
			return false;
		}
		
		if(this.urlop.getUrplDataOd()==null || this.urlop.getUrplDataDo()==null )
		{
			User.alert("Data początkowa i końcowa urlopu to pola wymagane!");
			return false;
		}
		
		if(this.urlop.getUrplDataOd().getTime()>this.urlop.getUrplDataDo().getTime())
		{
			inv.add("Data początkowa nie może być po terminie końca urlopu!");
		}
		
		Double pozostaloDni=getPozostaloDni();
		if(pozostaloDni==0.0D || pozostaloDni<0.0D){
			inv.add("Nie masz urlopu do wykorzystania!");
		}
		
		if(getLiczbaDniWniosekLacznie()>pozostaloDni){
			inv.add("Nie masz tyle dni urlopu do wykorzystania!");
		}
		
		Boolean czyJestKolidujacy = false;
		for (PptUrlopyPlanowane wn : this.wnioskiNaRok) {
			if( !( this.urlop.getUrplDataDo().before(wn.getUrplDataOd()) ||
				   this.urlop.getUrplDataOd().after(wn.getUrplDataDo())   )  ){
				czyJestKolidujacy=true;
				break;
			}
		}
		
		if(czyJestKolidujacy){
			inv.add("Znaleziono urlop kolidujący!");
		}
		
		//komentarz chwilowy ponieważ strona Roczny plan urlopowy bedzie testowany na rok 2017
/*		if (this.urlop.getUrplDataOd().getYear()  < new Date().getYear() || this.urlop.getUrplDataDo().getYear()  < new Date().getYear()) {
			inv.add("Nie można planować urlopu za poprzednie lata!");
		}*/
		
		if(!this.getLW("PPL_URL_URLOPY_PLANOWANE").getData().isEmpty() && "Z".equals(""+ this.getLW("PPL_URL_URLOPY_PLANOWANE").getData().get(0).get("URPL_STATUS")) )
			inv.add("Nie można modyfikować zatwierdzonego planu urlopowego!");

		for (String txt : inv) {
			User.alert(txt);
		}

		return inv.isEmpty();
	}
	
	public void zapiszNowyUrlOdDo(){
		
		if(this.urlop.getUrplId()!=0)
			return;
		
		if (!validateBeforeSave())
			return;
			
		HibernateContext.save(this.urlop);
		this.wczytajListeWnioskow(this.rok);
		
		try (Connection conn = JdbcUtils.getConnection()) {
			
			JdbcUtils.sqlExecNoQuery(conn, "UPDATE PPADM.PPT_ABS_URLOPY_PLANOWANE set URPL_STATUS='N'"
					+ " WHERE URPL_PRC_ID="+user.getPrcId()
					+       " AND EXTRACT(YEAR FROM URPL_DATA_OD)<="+rok.toString()+" and EXTRACT(YEAR FROM URPL_DATA_DO)>="+rok.toString());
		}
		catch (Exception e) {
			User.alert("Błąd przy dodawaniu elementu planu urlopowego!");
		}
		
		this.getLW("PPL_URL_URLOPY_PLANOWANE").resetTs();
		//newWniosek();
		ustawUwage();
	}
	
	public void odplanujUrlopOdDo() {
		
		if(this.urlop.getUrplId()==0L)
			return;
		
		if(!this.getLW("PPL_URL_URLOPY_PLANOWANE").getData().isEmpty() && "Z".equals( this.getLW("PPL_URL_URLOPY_PLANOWANE").getData().get(0).get("URPL_STATUS").toString()) ){
			User.alert("Nie można modyfikować zatwierdzonego planu urlopowego!");
			return;
		}
		
		//tymczasowa mozliwosc usuwania urlopu za poprzednie lata
/*		if (this.urlop.getUrplDataOd().getYear() < new Date().getYear()) {
			User.alert("Nie można usunąć urlopu za poprzednie lata!");
			System.out.println("Nie można usunąć urlopu!!!!!");
			return;
		}*/
				
		HibernateContext.delete(this.urlop);
		this.wczytajListeWnioskow(this.rok);
		
		try (Connection conn = JdbcUtils.getConnection()) {
			
			JdbcUtils.sqlExecNoQuery(conn, "UPDATE PPADM.PPT_ABS_URLOPY_PLANOWANE set URPL_STATUS='N'"
					+ " WHERE URPL_PRC_ID="+user.getPrcId()
					+       " AND EXTRACT(YEAR FROM URPL_DATA_OD)<="+rok.toString()+" and EXTRACT(YEAR FROM URPL_DATA_DO)>="+rok.toString());
		}
		catch (Exception e) {
			User.alert("Błąd przy usuwaniu elementu planu urlopowego!");
		}
		
		this.getLW("PPL_URL_URLOPY_PLANOWANE").resetTs();
		newWniosek();
		ustawUwage();
	}
	
	
	
	
	
	public Integer getRok() {
		return rok;
	}
	
	public void setRok(Integer rok) {
		this.rok = rok;
		//this.getSql("sqlSwieta").setSqlParams( rok );
		this.getLW("PPL_SWIETA").setSqlParams(rok);
		this.getLW("PPL_URL_URLOPY_PLANOWANE").setSqlParams( rok, user.getPrcIdLong() );
		wczytajListeWnioskow(rok);
		ArrayList<DataRow> data= this.getLW("PPL_PLANY_LIMIT_STD", user.getPrcId(), rok).getData();
		ustawUwage();
	}
	
	public ArrayList<Integer> getLata() {
		return lata;
	}
	
	public void setLata(ArrayList<Integer> lata) {
		this.lata = lata;
	}


	public PptUrlopyPlanowane getUrlop() {
		return urlop;
	}


	public void setUrlop(PptUrlopyPlanowane urlop) {
		this.urlop = urlop;
	}

	public Date getDzien() {
		return dzien;
	}

	public void setDzien(Date dzien) {
		this.dzien = dzien;
		wczytajWniosek(dzien);
	}

	public String getUwaga() {
		return uwaga;
	}

	public void setUwaga(String uwaga) {
		this.uwaga = uwaga;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
