package com.comarch.egeria.pp.Administrator.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

@Entity
@Table(name="PPT_ADM_LISTY_TABELE")
@NamedQuery(name="PptAdmListyTabele.findAll", query="SELECT p FROM PptAdmListyTabele p")
public class PptAdmListyTabele implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ADM_LISTY_TABELE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ADM_LISTY_TABELE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ADM_LISTY_TABELE")
	@Column(name="LSTB_ID")
	private Long lstbId;

	//@Column(name="LSTB_LST_ID")
	//private Long lstbLstId;
	
	//bi-directional many-to-one association to PptAdmListyWartosci
	@ManyToOne
	@JoinColumn(name="LSTB_LST_ID")
	private PptAdmListyWartosci pptAdmListyWartosci;

	@Column(name="LSTB_TBTS_TABLE")
	private String lstbTbtsTable;

	@Column(name="LSTB_TABLE_OWNER")
	private String lstbTableOwner;
	
	@Column(name="LSTB_TABLE_NAME")
	private String lstbTableName;
	
	@Column(name="LSTB_REF_BY_OWNER")
	private String lstbRefByOwner;
	
	@Column(name="LSTB_REF_BY_NAME")
	private String lstbRefByName;
	
	@Column(name="LSTB_REF_BY_TYPE")
	private String lstbRefByType;

	
	
	public PptAdmListyTabele(){
//		System.out.println();
	}
	
	public Long getLstbId() {
		return lstbId;
	}

	public void setLstbId(Long lstbId) {
		this.lstbId = lstbId;
	}

//	public Long getLstbLstId() {
//		return lstbLstId;
//	}
//
//	public void setLstbLstId(Long lstbLstId) {
//		this.lstbLstId = lstbLstId;
//	}

	public PptAdmListyWartosci getPptAdmListyWartosci() {
		return pptAdmListyWartosci;
	}

	public void setPptAdmListyWartosci(PptAdmListyWartosci pptAdmListyWartosci) {
		this.pptAdmListyWartosci = pptAdmListyWartosci;
	}
	

	
	public String getLstbTbtsTable() {
		return lstbTbtsTable;
	}

	public void setLstbTbtsTable(String lstbTbtsTable) {
		this.lstbTbtsTable = lstbTbtsTable;
	}

	public String getLstbTableOwner() {
		return lstbTableOwner;
	}

	public void setLstbTableOwner(String lstbTableOwner) {
		this.lstbTableOwner = lstbTableOwner;
	}

	public String getLstbTableName() {
		return lstbTableName;
	}

	public void setLstbTableName(String lstbTableName) {
		this.lstbTableName = lstbTableName;
	}

	public String getLstbRefByOwner() {
		return lstbRefByOwner;
	}

	public void setLstbRefByOwner(String lstbRefByOwner) {
		this.lstbRefByOwner = lstbRefByOwner;
	}

	public String getLstbRefByName() {
		return lstbRefByName;
	}

	public void setLstbRefByName(String lstbRefByName) {
		this.lstbRefByName = lstbRefByName;
	}

	public String getLstbRefByType() {
		return lstbRefByType;
	}

	public void setLstbRefByType(String lstbRefByType) {
		this.lstbRefByType = lstbRefByType;
	}

	
}
