package com.comarch.egeria.pp.Administrator;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.erd.model.DbObjListaWartosci;
import com.comarch.egeria.pp.erd.model.DbObject;
import com.comarch.egeria.web.User;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.ZipOutputStream;

import static com.comarch.egeria.Utils.nz;

@ManagedBean
@Named
@Scope("view")
public class ListaListWartosciBean extends SqlBean {


	public void wczytajDMLStdDLaZazn(){
		SqlDataSelectionsHandler lw = this.getSql("PPL_ADM_LISTY_WARTOSCI");
		if (lw==null || lw.getSelectedRows().isEmpty()) return;

		try (Connection conn = JdbcUtils.getConnection()){

			try {

				JdbcUtils.sqlSPCall(conn, "EAP_GLOBALS.USTAW_MODYFIKUJ_AUDYT", "N");
//				ArrayList<String> lResultOK = new ArrayList<>();
				ArrayList<String> lResultErr = new ArrayList<>();

				for (DataRow r : lw.getSelectedRows()) {
					String lwNazwa = r.getAsString("pp_lst_nazwa");
					try {
						DbObjListaWartosci.tryResetToStdVersion(conn, lwNazwa);
//						lResultOK.add(lwNazwa.toUpperCase() + ".sql");
					} catch (Exception ex) {
						lResultErr.add(lwNazwa.toUpperCase() + ".sql");
						User.alert(ex);
					}
				}

				if (!lResultErr.isEmpty()) User.warn("Listy wartości - nie wykonano skryptu(-ów): \n" + String.join("\n", lResultErr));

//				if (!lResultOK.isEmpty()) User.info("Listy wartości - wykonano skrypt(y): \n" + String.join("\n", lResultOK));

			} finally {
				JdbcUtils.sqlSPCall(conn, "EAP_GLOBALS.USTAW_MODYFIKUJ_AUDYT", "T");
			}

			lw.resetTs();
		} catch (Exception e) {
			User.alert(e);
		}
	}


	public void dodajListe() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("SzczegolyListyWartosci");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public void edit(DataRow r) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", r.getIdAsLong());
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", r);
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("lista", r.get("pp_lst_nazwa"));
		Utils.redirectTo("SzczegolyListyWartosci");
	}


	public StreamedContent exportDml() throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();
		String lwname = "";

		SqlDataSelectionsHandler lw = this.getSql("PPL_ADM_LISTY_WARTOSCI");
		if (lw==null || lw.getSelectedRows().isEmpty()) return null;

		try (Connection conn = JdbcUtils.getConnection()){
			for (DataRow r : lw.getSelectedRows()) {
				lwname += r.get("pp_lst_nazwa") + ".";
				String dml = JdbcUtils.sqlFnCLOB(conn, "ppadm.dmp_lw", r.get("pp_lst_nazwa"));
				if (!dml.contains("ąęćłńóśźż")) {
					dml += "\r\n--ascii/cp1250:ąęćłńóśźż\r\n";
				}
				sb.append(dml);
//				System.out.println(dml);
			}
		} catch (Exception e) {
			e.printStackTrace();
			User.alert(e);
		}

		String fname = null;

		String now = Utils.format(new Date(), "yyyy-MM-dd.HH.mm.ss");

		if (lw.getSelectedRows().size()==1)
			fname = "pp.dmp.lw." + lwname + now + ".sql";
		else
			fname = "pp.dmp.lw.("+lw.getSelectedRows().size()+")." + now + ".sql";

		ByteArrayInputStream is = new ByteArrayInputStream( sb.toString().getBytes("Cp1250") );
		StreamedContent ret =  new DefaultStreamedContent(is, "text/plain", fname);
		return ret;
	}







    public StreamedContent exportDmlZip(){
        String lwname = "";

        SqlDataSelectionsHandler lw = this.getSql("PPL_ADM_LISTY_WARTOSCI");
        if (lw==null || lw.getSelectedRows().isEmpty()) return null;

        try (Connection conn = JdbcUtils.getConnection()){

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(baos);
            for (DataRow r : lw.getSelectedRows()) {
                lwname = (String) r.get("pp_lst_nazwa");
                String dmlTxt = JdbcUtils.sqlFnCLOB(conn, "ppadm.dmp_lw", lwname);
				if (!dmlTxt.contains("ąęćłńóśźż")) {
					dmlTxt += "\r\n--ascii/cp1250:ąęćłńóśźż\r\n";
				}
                Utils.addToZipFile(dmlTxt.getBytes("Cp1250"), "lw."+lwname + ".sql", zos);
            }
            zos.close();
            ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray() );


        String fname = null;
        String now = Utils.format(new Date(), "yyyy-MM-dd.HH.mm.ss");

        if (lw.getSelectedRows().size()==1)
            fname = "pp.dmp.lw." + lwname + now + ".zip";
        else
            fname = "pp.dmp.lw.("+lw.getSelectedRows().size()+")." + now + ".zip";

            return new DefaultStreamedContent(bis , "text/plain", fname);

        } catch (Exception e) {
            User.alert(e);
        }

        return null;
    }


	public void updateAutoAkt(boolean aa) {
		SqlDataSelectionsHandler lw = this.getSql("PPL_ADM_LISTY_WARTOSCI");
		if (lw == null || lw.getSelectedRows().isEmpty()) return;
		try (Connection conn = JdbcUtils.getConnection()) {
			for (DataRow r : lw.getSelectedRows()) {
				final String oName = nz(r.getAsString("pp_lst_nazwa")).toUpperCase();
				boolean b = DbObject.updateAktualizacjeAutomatyczne(conn, "PPADM", "LW", oName, aa);
				if (!b){
					User.warn("Nie ustawiono flagi AA dla %1$s. Sprawdź czy taki wpis jest w PPT_OBJECTS.", oName);
				}
			}
		} catch (Exception e) {
			User.alert(e);
		}
		lw.resetTs();
	}






//    public StreamedContent pobierzSelected(){
//        SqlDataSelectionsHandler zLista = this.getSql("sqlZalaczniki");
//        List<DataRow> sel = zLista.getSelectedRows();
//
//
//        if (sel.isEmpty()) return null;
//
//        if (sel.size()==1){
//            return pobierz(sel.get(0));
//        }
//
//
//        try {
//            HashSet<String> fileNames = new HashSet<String>();
//
//            ByteArrayOutputStream baos = new ByteArrayOutputStream();
//            ZipOutputStream zos = new ZipOutputStream(baos);
//            for (DataRow dr : sel) {
//                addToZipFile(dr, zos, fileNames);
//            }
//            zos.close();
//            ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray() );
//            return new DefaultStreamedContent(bis , contentType("zalaczniki.zip"), "zalaczniki.zip");
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//        return null;
//    }



//	public void editSelectedRow() throws SQLException {
//		SqlDataSelectionsHandler p = this.getSql("PPL_ADM_LISTY_WARTOSCI");
//		if (p != null) {
//			System.out.println(p.getName() + " / currentRow: " + p.getCurrentRow());
//
//			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", p.getCurrentRow().getIdAsLong());
//
//			//FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", p.getCurrentRow());
//
//			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("lista", p.getCurrentRow().get("pp_lst_nazwa"));
//
//			try {
//				FacesContext.getCurrentInstance().getExternalContext().redirect("SzczegolyListyWartosci");
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//
//		}
//	}


}
