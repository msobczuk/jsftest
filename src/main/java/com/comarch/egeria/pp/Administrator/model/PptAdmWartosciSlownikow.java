package com.comarch.egeria.pp.Administrator.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_ADM_WARTOSCI_SLOWNIKOW database table.
 * 
 */
@Entity
@Table(name="PPT_ADM_WARTOSCI_SLOWNIKOW")
@NamedQuery(name="PptAdmWartosciSlownikow.findAll", query="SELECT p FROM PptAdmWartosciSlownikow p")
public class PptAdmWartosciSlownikow implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ADM_WARTOSCI_SLOWNIKOW",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ADM_WARTOSCI_SLOWNIKOW", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ADM_WARTOSCI_SLOWNIKOW")	
	@Column(name="WSL_ID")
	private Long wslId;

	@Column(name="WSL_ALIAS")
	private String wslAlias;

	@Column(name="WSL_ALIAS2")
	private String wslAlias2;

	@Column(name="WSL_ALIAS3")
	private String wslAlias3;

	@Column(name="WSL_ALIAS4")
	private String wslAlias4;

	@Column(name="WSL_ALIAS5")
	private String wslAlias5;

	@Column(name="WSL_ALIAS6")
	private String wslAlias6;

	@Column(name="WSL_ALIAS7")
	private String wslAlias7;

	@Column(name="WSL_ALIAS8")
	private String wslAlias8;

	@Column(name="WSL_ALIAS9")
	private String wslAlias9;

//	@Temporal(TemporalType.DATE)
//	@Column(name="WSL_AUDYT_DM")
//	private Date wslAudytDm;
//
//	@Temporal(TemporalType.DATE)
//	@Column(name="WSL_AUDYT_DT")
//	private Date wslAudytDt;
//
//	@Column(name="WSL_AUDYT_KM")
//	private String wslAudytKm;
//
//	@Column(name="WSL_AUDYT_KT")
//	private String wslAudytKt;
//
//	@Column(name="WSL_AUDYT_LM")
//	private BigDecimal wslAudytLm;
//
//	@Column(name="WSL_AUDYT_UM")
//	private String wslAudytUm;
//
//	@Column(name="WSL_AUDYT_UT")
//	private String wslAudytUt;

	@Column(name="WSL_OPIS")
	private String wslOpis;

	@Column(name="WSL_OPIS2")
	private String wslOpis2;

	@Column(name="WSL_OPIS3")
	private String wslOpis3;

	@Column(name="WSL_OPIS4")
	private String wslOpis4;

	@Column(name="WSL_OPIS5")
	private String wslOpis5;

	@Column(name="WSL_OPIS6")
	private String wslOpis6;

	@Column(name="WSL_OPIS7")
	private String wslOpis7;

	@Column(name="WSL_OPIS8")
	private String wslOpis8;

	@Column(name="WSL_OPIS9")
	private String wslOpis9;

	@Column(name="WSL_SL_NAZWA")
	private String wslSlNazwa;

	@Column(name="WSL_STATUS")
	private String wslStatus;

	@Column(name="WSL_TYP")
	private String wslTyp;

	@Column(name="WSL_WARTOSC")
	private String wslWartosc = "";

	@Column(name="WSL_WARTOSC_MAX")
	private String wslWartoscMax;

	@Column(name="WSL_WARTOSC_ZEWN")
	private String wslWartoscZewn;

	
	@Column(name="WSL_LP")
	private Long wslLp;
	
	//bi-directional many-to-one association to PptAdmSlowniki
	@ManyToOne
	@JoinColumn(name="WSL_SL_ID")
	private PptAdmSlowniki pptAdmSlowniki;

	public PptAdmWartosciSlownikow() {
	}

	public Long getWslId() {
		return this.wslId;
	}

	public void setWslId(Long wslId) {
		this.wslId = wslId;
	}

	public String getWslAlias() {
		return this.wslAlias;
	}

	public void setWslAlias(String wslAlias) {
		this.wslAlias = wslAlias;
	}

	public String getWslAlias2() {
		return this.wslAlias2;
	}

	public void setWslAlias2(String wslAlias2) {
		this.wslAlias2 = wslAlias2;
	}

	public String getWslAlias3() {
		return this.wslAlias3;
	}

	public void setWslAlias3(String wslAlias3) {
		this.wslAlias3 = wslAlias3;
	}

	public String getWslAlias4() {
		return this.wslAlias4;
	}

	public void setWslAlias4(String wslAlias4) {
		this.wslAlias4 = wslAlias4;
	}

	public String getWslAlias5() {
		return this.wslAlias5;
	}

	public void setWslAlias5(String wslAlias5) {
		this.wslAlias5 = wslAlias5;
	}

	public String getWslAlias6() {
		return this.wslAlias6;
	}

	public void setWslAlias6(String wslAlias6) {
		this.wslAlias6 = wslAlias6;
	}

	public String getWslAlias7() {
		return this.wslAlias7;
	}

	public void setWslAlias7(String wslAlias7) {
		this.wslAlias7 = wslAlias7;
	}

	public String getWslAlias8() {
		return this.wslAlias8;
	}

	public void setWslAlias8(String wslAlias8) {
		this.wslAlias8 = wslAlias8;
	}

	public String getWslAlias9() {
		return this.wslAlias9;
	}

	public void setWslAlias9(String wslAlias9) {
		this.wslAlias9 = wslAlias9;
	}

//	public Date getWslAudytDm() {
//		return this.wslAudytDm;
//	}
//
//	public void setWslAudytDm(Date wslAudytDm) {
//		this.wslAudytDm = wslAudytDm;
//	}
//
//	public Date getWslAudytDt() {
//		return this.wslAudytDt;
//	}
//
//	public void setWslAudytDt(Date wslAudytDt) {
//		this.wslAudytDt = wslAudytDt;
//	}
//
//	public String getWslAudytKm() {
//		return this.wslAudytKm;
//	}
//
//	public void setWslAudytKm(String wslAudytKm) {
//		this.wslAudytKm = wslAudytKm;
//	}
//
//	public String getWslAudytKt() {
//		return this.wslAudytKt;
//	}
//
//	public void setWslAudytKt(String wslAudytKt) {
//		this.wslAudytKt = wslAudytKt;
//	}
//
//	public BigDecimal getWslAudytLm() {
//		return this.wslAudytLm;
//	}
//
//	public void setWslAudytLm(BigDecimal wslAudytLm) {
//		this.wslAudytLm = wslAudytLm;
//	}
//
//	public String getWslAudytUm() {
//		return this.wslAudytUm;
//	}
//
//	public void setWslAudytUm(String wslAudytUm) {
//		this.wslAudytUm = wslAudytUm;
//	}
//
//	public String getWslAudytUt() {
//		return this.wslAudytUt;
//	}
//
//	public void setWslAudytUt(String wslAudytUt) {
//		this.wslAudytUt = wslAudytUt;
//	}

	public String getWslOpis() {
		return this.wslOpis;
	}

	public void setWslOpis(String wslOpis) {
		this.wslOpis = wslOpis;
	}

	public String getWslOpis2() {
		return this.wslOpis2;
	}

	public void setWslOpis2(String wslOpis2) {
		this.wslOpis2 = wslOpis2;
	}

	public String getWslOpis3() {
		return this.wslOpis3;
	}

	public void setWslOpis3(String wslOpis3) {
		this.wslOpis3 = wslOpis3;
	}

	public String getWslOpis4() {
		return this.wslOpis4;
	}

	public void setWslOpis4(String wslOpis4) {
		this.wslOpis4 = wslOpis4;
	}

	public String getWslOpis5() {
		return this.wslOpis5;
	}

	public void setWslOpis5(String wslOpis5) {
		this.wslOpis5 = wslOpis5;
	}

	public String getWslOpis6() {
		return this.wslOpis6;
	}

	public void setWslOpis6(String wslOpis6) {
		this.wslOpis6 = wslOpis6;
	}

	public String getWslOpis7() {
		return this.wslOpis7;
	}

	public void setWslOpis7(String wslOpis7) {
		this.wslOpis7 = wslOpis7;
	}

	public String getWslOpis8() {
		return this.wslOpis8;
	}

	public void setWslOpis8(String wslOpis8) {
		this.wslOpis8 = wslOpis8;
	}

	public String getWslOpis9() {
		return this.wslOpis9;
	}

	public void setWslOpis9(String wslOpis9) {
		this.wslOpis9 = wslOpis9;
	}

	public String getWslSlNazwa() {
		return this.wslSlNazwa;
	}

	public void setWslSlNazwa(String wslSlNazwa) {
		this.wslSlNazwa = wslSlNazwa;
	}

	public String getWslStatus() {
		return this.wslStatus;
	}

	public void setWslStatus(String wslStatus) {
		this.wslStatus = wslStatus;
	}

	public String getWslTyp() {
		return this.wslTyp;
	}

	public void setWslTyp(String wslTyp) {
		this.wslTyp = wslTyp;
	}

	public String getWslWartosc() {
		return this.wslWartosc;
	}

	public void setWslWartosc(String wslWartosc) {
		this.wslWartosc = wslWartosc;
	}

	public String getWslWartoscMax() {
		return this.wslWartoscMax;
	}

	public void setWslWartoscMax(String wslWartoscMax) {
		this.wslWartoscMax = wslWartoscMax;
	}

	public String getWslWartoscZewn() {
		return this.wslWartoscZewn;
	}

	public void setWslWartoscZewn(String wslWartoscZewn) {
		this.wslWartoscZewn = wslWartoscZewn;
	}

	public PptAdmSlowniki getPptAdmSlowniki() {
		return this.pptAdmSlowniki;
	}

	public void setPptAdmSlowniki(PptAdmSlowniki pptAdmSlowniki) {
		this.pptAdmSlowniki = pptAdmSlowniki;
	}

	public Long getWslLp() {
		return wslLp;
	}

	public void setWslLp(Long wslLp) {
		this.wslLp = wslLp;
	}

}