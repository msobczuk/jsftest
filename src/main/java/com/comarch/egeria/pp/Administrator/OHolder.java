package com.comarch.egeria.pp.Administrator;

public class OHolder {
	
	private Object value = null;
	
	public OHolder(Object value) {
		this.value = value;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}

	
	@Override
	public String toString() {
		return String.valueOf(value);
	}
}
