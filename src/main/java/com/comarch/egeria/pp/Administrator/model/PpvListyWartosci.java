package com.comarch.egeria.pp.Administrator.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPV_LISTY_WARTOSCI database table.
 * 
 */
@Entity
@Table(name="PPV_LISTY_WARTOSCI", schema="PPADM")
@NamedQuery(name="PpvListyWartosci.findAll", query="SELECT p FROM PpvListyWartosci p")
public class PpvListyWartosci implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="LST_AUDYT_DM")
	private Date lstAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="LST_AUDYT_DT")
	private Date lstAudytDt;

	@Column(name="LST_AUDYT_KM")
	private String lstAudytKm;

	@Column(name="LST_AUDYT_KT")
	private String lstAudytKt;

	@Column(name="LST_AUDYT_LM")
	private BigDecimal lstAudytLm;

	@Column(name="LST_AUDYT_UM")
	private String lstAudytUm;

	@Column(name="LST_AUDYT_UT")
	private String lstAudytUt;

	@Column(name="LST_DLUGOSC")
	private Long lstDlugosc;

	@Column(name="LST_DLUGOSC_800_600")
	private Long lstDlugosc800600;

	@Column(name="LST_ETYKIETA")
	private String lstEtykieta;

	@Column(name="LST_ETYKIETA2")
	private String lstEtykieta2;

	@Column(name="LST_ETYKIETA3")
	private String lstEtykieta3;

	@Column(name="LST_ETYKIETA4")
	private String lstEtykieta4;

	@Column(name="LST_ETYKIETA5")
	private String lstEtykieta5;

	@Column(name="LST_ETYKIETA6")
	private String lstEtykieta6;

	@Column(name="LST_ETYKIETA7")
	private String lstEtykieta7;

	@Column(name="LST_ETYKIETA8")
	private String lstEtykieta8;

	@Column(name="LST_ETYKIETA9")
	private String lstEtykieta9;

	@Column(name="LST_F_INTERFEJS")
	private String lstFInterfejs;

	@Column(name="LST_F_UKRYTE")
	private String lstFUkryte;

	@Column(name="LST_F_ZWROT")
	private String lstFZwrot;

	@Id
	@Column(name="LST_ID")
	private Long lstId;

	@Column(name="LST_LB_CODE_ETYKIETA")
	private String lstLbCodeEtykieta;

	@Column(name="LST_LB_CODE_OPIS")
	private String lstLbCodeOpis;

	@Column(name="LST_LP")
	private Long lstLp;

	@Column(name="LST_NAZWA")
	private String lstNazwa;

	@Column(name="LST_NAZWA_POLA")
	private String lstNazwaPola;

	@Column(name="LST_OPIS")
	private String lstOpis;

	@Column(name="LST_OPIS2")
	private String lstOpis2;

	@Column(name="LST_OPIS3")
	private String lstOpis3;

	@Column(name="LST_OPIS4")
	private String lstOpis4;

	@Column(name="LST_OPIS5")
	private String lstOpis5;

	@Column(name="LST_OPIS6")
	private String lstOpis6;

	@Column(name="LST_OPIS7")
	private String lstOpis7;

	@Column(name="LST_OPIS8")
	private String lstOpis8;

	@Column(name="LST_OPIS9")
	private String lstOpis9;

	@Column(name="LST_QUERY")
	private String lstQuery;

	public PpvListyWartosci() {
	}

	public Date getLstAudytDm() {
		return this.lstAudytDm;
	}

	public void setLstAudytDm(Date lstAudytDm) {
		this.lstAudytDm = lstAudytDm;
	}

	public Date getLstAudytDt() {
		return this.lstAudytDt;
	}

	public void setLstAudytDt(Date lstAudytDt) {
		this.lstAudytDt = lstAudytDt;
	}

	public String getLstAudytKm() {
		return this.lstAudytKm;
	}

	public void setLstAudytKm(String lstAudytKm) {
		this.lstAudytKm = lstAudytKm;
	}

	public String getLstAudytKt() {
		return this.lstAudytKt;
	}

	public void setLstAudytKt(String lstAudytKt) {
		this.lstAudytKt = lstAudytKt;
	}

	public BigDecimal getLstAudytLm() {
		return this.lstAudytLm;
	}

	public void setLstAudytLm(BigDecimal lstAudytLm) {
		this.lstAudytLm = lstAudytLm;
	}

	public String getLstAudytUm() {
		return this.lstAudytUm;
	}

	public void setLstAudytUm(String lstAudytUm) {
		this.lstAudytUm = lstAudytUm;
	}

	public String getLstAudytUt() {
		return this.lstAudytUt;
	}

	public void setLstAudytUt(String lstAudytUt) {
		this.lstAudytUt = lstAudytUt;
	}

	public Long getLstDlugosc() {
		return this.lstDlugosc;
	}

	public void setLstDlugosc(Long lstDlugosc) {
		this.lstDlugosc = lstDlugosc;
	}

	public Long getLstDlugosc800600() {
		return this.lstDlugosc800600;
	}

	public void setLstDlugosc800600(Long lstDlugosc800600) {
		this.lstDlugosc800600 = lstDlugosc800600;
	}

	public String getLstEtykieta() {
		return this.lstEtykieta;
	}

	public void setLstEtykieta(String lstEtykieta) {
		this.lstEtykieta = lstEtykieta;
	}

	public String getLstEtykieta2() {
		return this.lstEtykieta2;
	}

	public void setLstEtykieta2(String lstEtykieta2) {
		this.lstEtykieta2 = lstEtykieta2;
	}

	public String getLstEtykieta3() {
		return this.lstEtykieta3;
	}

	public void setLstEtykieta3(String lstEtykieta3) {
		this.lstEtykieta3 = lstEtykieta3;
	}

	public String getLstEtykieta4() {
		return this.lstEtykieta4;
	}

	public void setLstEtykieta4(String lstEtykieta4) {
		this.lstEtykieta4 = lstEtykieta4;
	}

	public String getLstEtykieta5() {
		return this.lstEtykieta5;
	}

	public void setLstEtykieta5(String lstEtykieta5) {
		this.lstEtykieta5 = lstEtykieta5;
	}

	public String getLstEtykieta6() {
		return this.lstEtykieta6;
	}

	public void setLstEtykieta6(String lstEtykieta6) {
		this.lstEtykieta6 = lstEtykieta6;
	}

	public String getLstEtykieta7() {
		return this.lstEtykieta7;
	}

	public void setLstEtykieta7(String lstEtykieta7) {
		this.lstEtykieta7 = lstEtykieta7;
	}

	public String getLstEtykieta8() {
		return this.lstEtykieta8;
	}

	public void setLstEtykieta8(String lstEtykieta8) {
		this.lstEtykieta8 = lstEtykieta8;
	}

	public String getLstEtykieta9() {
		return this.lstEtykieta9;
	}

	public void setLstEtykieta9(String lstEtykieta9) {
		this.lstEtykieta9 = lstEtykieta9;
	}

	public String getLstFInterfejs() {
		return this.lstFInterfejs;
	}

	public void setLstFInterfejs(String lstFInterfejs) {
		this.lstFInterfejs = lstFInterfejs;
	}

	public String getLstFUkryte() {
		return this.lstFUkryte;
	}

	public void setLstFUkryte(String lstFUkryte) {
		this.lstFUkryte = lstFUkryte;
	}

	public String getLstFZwrot() {
		return this.lstFZwrot;
	}

	public void setLstFZwrot(String lstFZwrot) {
		this.lstFZwrot = lstFZwrot;
	}

	public Long getLstId() {
		return this.lstId;
	}

	public void setLstId(Long lstId) {
		this.lstId = lstId;
	}

	public String getLstLbCodeEtykieta() {
		return this.lstLbCodeEtykieta;
	}

	public void setLstLbCodeEtykieta(String lstLbCodeEtykieta) {
		this.lstLbCodeEtykieta = lstLbCodeEtykieta;
	}

	public String getLstLbCodeOpis() {
		return this.lstLbCodeOpis;
	}

	public void setLstLbCodeOpis(String lstLbCodeOpis) {
		this.lstLbCodeOpis = lstLbCodeOpis;
	}

	public Long getLstLp() {
		return this.lstLp;
	}

	public void setLstLp(Long lstLp) {
		this.lstLp = lstLp;
	}

	public String getLstNazwa() {
		return this.lstNazwa;
	}

	public void setLstNazwa(String lstNazwa) {
		if (lstNazwa!=null) lstNazwa = lstNazwa.toUpperCase();
		this.lstNazwa = lstNazwa;
	}

	public String getLstNazwaPola() {
		return this.lstNazwaPola;
	}

	public void setLstNazwaPola(String lstNazwaPola) {
		this.lstNazwaPola = lstNazwaPola;
	}

	public String getLstOpis() {
		return this.lstOpis;
	}

	public void setLstOpis(String lstOpis) {
		this.lstOpis = lstOpis;
	}

	public String getLstOpis2() {
		return this.lstOpis2;
	}

	public void setLstOpis2(String lstOpis2) {
		this.lstOpis2 = lstOpis2;
	}

	public String getLstOpis3() {
		return this.lstOpis3;
	}

	public void setLstOpis3(String lstOpis3) {
		this.lstOpis3 = lstOpis3;
	}

	public String getLstOpis4() {
		return this.lstOpis4;
	}

	public void setLstOpis4(String lstOpis4) {
		this.lstOpis4 = lstOpis4;
	}

	public String getLstOpis5() {
		return this.lstOpis5;
	}

	public void setLstOpis5(String lstOpis5) {
		this.lstOpis5 = lstOpis5;
	}

	public String getLstOpis6() {
		return this.lstOpis6;
	}

	public void setLstOpis6(String lstOpis6) {
		this.lstOpis6 = lstOpis6;
	}

	public String getLstOpis7() {
		return this.lstOpis7;
	}

	public void setLstOpis7(String lstOpis7) {
		this.lstOpis7 = lstOpis7;
	}

	public String getLstOpis8() {
		return this.lstOpis8;
	}

	public void setLstOpis8(String lstOpis8) {
		this.lstOpis8 = lstOpis8;
	}

	public String getLstOpis9() {
		return this.lstOpis9;
	}

	public void setLstOpis9(String lstOpis9) {
		this.lstOpis9 = lstOpis9;
	}

	public String getLstQuery() {
		return this.lstQuery;
	}

	public void setLstQuery(String lstQuery) {
		this.lstQuery = lstQuery;
	}

}