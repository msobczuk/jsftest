package com.comarch.egeria.pp.Administrator;

import com.comarch.egeria.pp.Administrator.model.PptAdmListyTabele;
import com.comarch.egeria.pp.Administrator.model.PptAdmListyWartosci;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.erd.model.DbObject;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.jfree.util.Log;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.rowset.CachedRowSet;
import java.io.*;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.nz;


@ManagedBean
@Scope("view")
@Named
public class SzczegolyListyWartosciBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	@Inject
	SessionBean sessionBean;

	private String nazwaListy = "";

	private ArrayList<Object> listaRow = new ArrayList<>();

	private ArrayList<PptAdmListyWartosci> listaElementow = new ArrayList<PptAdmListyWartosci>();

	private PptAdmListyWartosci glownyElement = new PptAdmListyWartosci();

	private TreeMap<String, OHolder> mapaArgumentow = new TreeMap<>();

	@PostConstruct
	public void init() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Long id = null;

		nazwaListy = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("lista");

		if (nazwaListy==null)//mozliwe przekazanie nazwy listy przez parameter w url
			nazwaListy = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("lista");

		if (nazwaListy != null && !"".equals(nazwaListy)){
			BigDecimal ret = (BigDecimal) this.execScalarQuery("select LST_ID from PPT_ADM_LISTY_WARTOSCI where LST_LP=1 and upper(LST_NAZWA) = ?", nazwaListy.toUpperCase());
			if (ret != null)
				id = ret.longValue();
		}

		if (id==null || id==0) {
			glownyElement = new PptAdmListyWartosci();
			glownyElement.setLstLp(1L);
			glownyElement.setLstDlugosc(50L);
			glownyElement.setLstFInterfejs("N");
			glownyElement.setLstFUkryte("N");
			glownyElement.setLstFZwrot("T");
			glownyElement.setLstKonsoliduj("N");
			glownyElement.setLstNazwa(nz(nazwaListy).toUpperCase());
			User.info("Tworzenie nowej listy wartości '%1$s'", glownyElement.getLstNazwa());
			return;
		}

//		this.glownyElement = (PptAdmListyWartosci) HibernateContext.get(PptAdmListyWartosci.class, id);
		try (HibernateContext h = new HibernateContext()){
			this.glownyElement = h.getSession().get(PptAdmListyWartosci.class, id);
			this.glownyElement.getPptAdmListyWartoscis().size(); //LAZY!
			this.glownyElement.getPptAdmListyTabele().size(); //LAZY!
		}

		if (glownyElement.getPptAdmListyWartoscis() != null)
			Collections.sort(glownyElement.getPptAdmListyWartoscis(), comparator_columns);



	}


	Comparator<PptAdmListyWartosci> comparator_columns = new Comparator<PptAdmListyWartosci>() {

		@Override
		public int compare(PptAdmListyWartosci o1, PptAdmListyWartosci o2) {
			Long i = o2.getLstLp();
			Long j = o1.getLstLp();
			if (i < j) {
				return 1;
			} else if (i > j) {
				return -1;
			} else {
				return 0;
			}
		}

	};

	public void wykonajZapytanieZParametrami() {
		ArrayList<Object> params = new ArrayList<>();

		Iterator<String> it = mapaArgumentow.keySet().iterator();
		while (it.hasNext()) {
			String arg = it.next();
			Boolean czyCalyString = false;
			String tempZapytanie = glownyElement.getLstQuery();

			while (!czyCalyString) {

				if (tempZapytanie.contains(arg)) {

					int index = tempZapytanie.indexOf(arg);
					tempZapytanie = tempZapytanie.substring(index + 7);

					if ((mapaArgumentow.get(arg).getValue()).equals(arg)) {
						params.add("%");
					} else {
						params.add(mapaArgumentow.get(arg).getValue());
					}

				} else {
					czyCalyString = true;
				}

			}
		}

		String sql = glownyElement.getLstQuery().replaceAll("(:POLE).([^)])", "?");
		this.loadSqlData("sqlTest", sql, params);
		this.getSql("sqlTest").reLoadData();
	}



	public Boolean testOracleDependeciesTables(boolean showDependencies){
		//this.glownyElement.setLstSqlTestKiedy(new Date());

		String sql = this.glownyElement.getLstQuery();
		if (sql==null || sql.trim().isEmpty()){
			User.alert("Pole zapytanie SQL nie moze być puste.");
			this.glownyElement.setLstSqlIleParametrow(0L);
			this.glownyElement.setLstSqlTestPoprawny("N");
			return false;
		}

		sql = SqlBean.replaceEgParamsPOLE_N(sql);
		sql = SqlBean.replaceEgParamsPOLEN(sql);
		Long ile = 0L+ sql.split("\\?").length - 1;
		this.glownyElement.setLstSqlIleParametrow(ile);

		sql = sql.replace("?","''"); //wywalamy parametry / ustawiamy na null
		sql = JdbcUtils.formatSqlToLimitRows(sql,0);//na wszelki wypadek nie chcemy zadnych wierszy...
		sql = "CREATE OR REPLACE VIEW PPADM.PPV_LW_COMPILE_DEPENDENCIES as \n" + sql;

		try(Connection conn = JdbcUtils.getConnection()) {
			JdbcUtils.sqlExecNoQuery(conn, sql);
			this.glownyElement.setLstSqlTestPoprawny("T");

			syncPptAdmListyTabele();

			Set<String> tables = this.glownyElement.getPptAdmListyTabele().stream().map(t->t.getLstbTbtsTable()).collect(Collectors.toSet());

			User.info("DEPENDENCIES/TABLES (%1$s)\r\n" + join("\r\n" ), this.glownyElement.getPptAdmListyTabele().size() );

			if (showDependencies)
				com.comarch.egeria.Utils.executeScript("$( jq( 'formId:accSql:lvORADT:lovBtn' )).click();");

			testujKolumny(conn);

			return true;
		} catch (Exception e) {
			Log.error(e);
			User.alert(e.getMessage(), e);
			this.glownyElement.setLstSqlTestPoprawny("N");
			return false;
		}

	}

	private void testujKolumny(Connection conn) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		ArrayList<String> sqlColumns = new ArrayList<>();
		CachedRowSet crs = JdbcUtils.sqlExecQuery(conn, "select * from PPV_LW_COMPILE_DEPENDENCIES");
		for (int i = 1; i < crs.getMetaData().getColumnCount()+1; i++) {
			sqlColumns.add (crs.getMetaData().getColumnLabel(i).toLowerCase());
		}

		String kol2wer = "";
		for (PptAdmListyWartosci c : this.glownyElement.getPptAdmListyWartoscisEtykietyAll()) {
			if (sqlColumns.contains(c.getLstNazwaPola())
			 || c.getLstNazwaPola().contains("@")
			 || c.getLstNazwaPola().contains("%")
			) continue;

			System.out.println("xxxxxxxxxx " + c.getLstNazwaPola());
			kol2wer += "\r\n"+c.getLstNazwaPola() + " (lp: " + c.getLstLp() + ")";
		}
		if (!kol2wer.isEmpty()){
			User.warn("Zweryfikuj kolumny: \r\n" + kol2wer);
		}
	}


	public void syncPptAdmListyTabele() {

		SqlDataSelectionsHandler lwDepTab = this.getLW("PPL_LW_DEPENDECIES");
		lwDepTab.reLoadData();
		lwDepTab.setName("Hierarchia zależności dla LW '" + this.glownyElement.getLstNazwa() + "'");//dynamiczny tyuł dla pdf

		Set<String> tbNamesSet = this.glownyElement.getPptAdmListyTabele().stream().map(t->t.getLstbTbtsTable()).collect(Collectors.toSet());
		for (DataRow r : lwDepTab.getData()) {
			if ("TABLE".equals(r.get("referenced_type")) && !tbNamesSet.contains(""+r.get("referenced_object"))){
				PptAdmListyTabele tb = new PptAdmListyTabele();
				tb.setLstbTbtsTable(""+r.get("referenced_object"));
				tb.setLstbTableOwner(""+r.get("referenced_owner"));
				tb.setLstbTableName(""+r.get("referenced_name"));
				tb.setLstbRefByType(""+r.get("ref_by_type"));
				tb.setLstbRefByOwner(""+r.get("ref_by_owner"));
				tb.setLstbRefByName(""+r.get("ref_by_name"));
				this.glownyElement.addPptAdmListyTabele(tb);
			}
		}

		Set<String> lwDepTabNames = lwDepTab.getData().stream()
				.filter(r -> "TABLE".equals(  r.get("referenced_type") ) && !"SYS".equals(r.get("referenced_owner")))
				.map(r ->""+r.get("referenced_object"))
				.sorted()
				.collect(Collectors.toSet());


		Set<PptAdmListyTabele> tmpIfToRemoveSet = this.glownyElement.getPptAdmListyTabele().stream().collect(Collectors.toSet());
		for (PptAdmListyTabele tb : tmpIfToRemoveSet) {
			if (!lwDepTabNames.contains( tb.getLstbTbtsTable()))
				this.glownyElement.removePptAdmListyTabele(tb);
		}
	}

	public void testZapytania(Boolean czyWyswietlac) {

		Boolean mozeZleInterpretowac = false;

		if (glownyElement.getLstQuery() != null && !glownyElement.getLstQuery().isEmpty()) {

			String zapytanie = glownyElement.getLstQuery();
			ArrayList<Object> params = new ArrayList<Object>();

			if (czyWyswietlac) {
				if (this.getSql("sqlTest") != null) {
					this.getSql("sqlTest").resetTs();
				}

				String tempZapytanie = zapytanie;
				Boolean czyCalyString = false;

				while (!czyCalyString) {

					if (tempZapytanie.contains(":POLE_")) {
						int index = tempZapytanie.indexOf(":POLE_");
						String tempString = tempZapytanie.substring(index, index + 7);
						tempZapytanie = tempZapytanie.substring(index + 7);

						OHolder oHolder = new OHolder(tempString);
						mapaArgumentow.put(tempString, oHolder);

					} else {
						czyCalyString = true;
					}
				}

				for (Map.Entry<String, OHolder> entry : mapaArgumentow.entrySet()) {
					params.add("%");
				}

				String sql = zapytanie.replaceAll("(:POLE).([^)])", "?");
				this.removeSql("sqlTest");
				this.loadSqlData("sqlTest", sql, params);
			} else {

				if (zapytanie.contains("POLE_")) {
					mozeZleInterpretowac = true;
				}

				zapytanie = usunParametry(zapytanie);
				zapytanie = JdbcUtils.formatSqlToLimitRows(zapytanie, czyWyswietlac ? 1000000 : 1);

				CachedRowSet crs;
				try(Connection conn = JdbcUtils.getConnection()) {

					crs = JdbcUtils.sqlExecQuery(conn, zapytanie, params);

					if (crs != null) {
						User.info("Zapytanie jest poprawne");
					}
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
					if (mozeZleInterpretowac) {
						User.warn(
								"Zapytanie posiada błędy, bądź nieudane automatyczne związanie parametrów - spróbuj wyświetlić testowe wyniki",
								e.getMessage());
					} else {
						User.warn("Zapytanie posiada błędy!", e.getMessage());
					}
					Log.error(e);
					e.printStackTrace();
				}
			}
		}


		List<String> colNames = this.glownyElement.getPptAdmListyWartoscisEtykietyAll().stream().map(e -> e.getLstNazwaPola()).collect(Collectors.toList());
		this.getSql("sqlTest").setColNames(colNames);
		List<Long> colWidths = this.glownyElement.getPptAdmListyWartoscisEtykietyAll().stream().map(e -> e.getLstDlugosc()).collect(Collectors.toList());
		this.getSql("sqlTest").setColWidths(colWidths);
	}

	private String usunParametry(String zapytanie) {
		String sql = zapytanie;
		//sql = sql.replaceAll("(:POLE).([^)])", "'' ");
		//sql = sql.replaceAll("(:[a-zA-Z0-9_]+)", "''"); //\S+ -> Several non-whitespace characters
		sql = sql.replaceAll("(:[a-zA-Z0-9_]+)", "''"); //\S+ -> Several non-whitespace characters
		sql = sql.replaceAll("[?]", "''");
		//System.out.println("SQL po replace all -------------------------------  " + sql);
		return sql;
	}

	public boolean containsPole(final List<PptAdmListyWartosci> list, final String pole) {
		return list.stream().filter(o -> o.getLstNazwaPola() != null && o.getLstNazwaPola().equals(pole)).findFirst()
				.isPresent();
	}

	public void wypelnijPola() {
		if (glownyElement.getLstQuery() != null && !glownyElement.getLstQuery().isEmpty()) {

			String zapytanie = glownyElement.getLstQuery();
			ArrayList<Object> params = new ArrayList<Object>();
			zapytanie = usunParametry(zapytanie);
			zapytanie = JdbcUtils.formatSqlToLimitRows(zapytanie, 1);

			CachedRowSet crs;
			try (Connection conn = JdbcUtils.getConnection()){
				crs = JdbcUtils.sqlExecQuery(conn, zapytanie, params);

				if (crs != null) {

					if ( glownyElement.getLstNazwaPola() == null ||  "".equals(glownyElement.getLstNazwaPola()) )
					 	 glownyElement.setLstNazwaPola(crs.getMetaData().getColumnLabel(1).toLowerCase());

					if (glownyElement.getLstEtykieta() == null || "".equals(glownyElement.getLstEtykieta()))
						glownyElement.setLstEtykieta(crs.getMetaData().getColumnLabel(1).toLowerCase());

					for (int i = 2; i < crs.getMetaData().getColumnCount()+1; i++) {

						String pole = crs.getMetaData().getColumnLabel(i).toLowerCase();

						if (!containsPole(glownyElement.getPptAdmListyWartoscis(), pole)) {

							PptAdmListyWartosci pal = new PptAdmListyWartosci();

							if (glownyElement.getPptAdmListyWartoscis().isEmpty()) {
								pal.setLstLp(2L);//2 bo pierwszy element to zawsze sam glowny el.
							}else{
								PptAdmListyWartosci ostatni = glownyElement
													.getPptAdmListyWartoscis()
													.get(glownyElement.getPptAdmListyWartoscis().size() - 1);

								pal.setLstLp(ostatni.getLstLp() + 1L);
							}

							pal.setLstDlugosc(100L);
							pal.setLstFInterfejs("N");
							pal.setLstFUkryte("N");
							pal.setLstFZwrot("T");
							pal.setLstNazwa(nazwaListy);
							pal.setLstNazwaPola(pole);

							pal.setLstEtykieta(pole);

							glownyElement.addPptAdmListyWartosci(pal);
						}
					}
				}
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
				User.warn("Zapytanie posiada błędy, bądź nieudane automatyczne związanie parametrów", e.getMessage());
				User.warn("Nieudane automatyczne wypełnianie pól", e.getMessage());
				Log.error(e);
				e.printStackTrace();
			}

		}
	}




	public void zapisz() {

		if (glownyElement.getLstNazwa() == null || glownyElement.getLstNazwa().isEmpty()) {
			User.alert("Nazwa listy nie może być pusta");
			return;
		}

		if (glownyElement.getPptAdmListyWartoscis() != null && !glownyElement.getPptAdmListyWartoscis().isEmpty()) {
			for (PptAdmListyWartosci element : glownyElement.getPptAdmListyWartoscis()) {
				if (element.getLstNazwa() == null || element.getLstNazwa().isEmpty()) {
					element.setLstNazwa(glownyElement.getLstNazwa());
				}
			}
		}

		if (!validateBeforeSave())
			return;

//		if(!testOracleDependeciesTables(false)) return;
		testOracleDependeciesTables(false);//tylko atualizujemy satn sql, liczbe parametrow i czas wykonania testu (pozwalamy czasowo na zapis nieprawidłowych)

		try (HibernateContext h = new HibernateContext()) {
			Object sysdate = JdbcUtils.sqlExecScalarQuery(h.getConnection(), "select sysdate from dual ");
			this.glownyElement.setLstSqlTestKiedy((Date) sysdate);

			HibernateContext.saveOrUpdate(h, glownyElement, true);
			glownyElement.getPptAdmListyWartoscis().size();//LAZY!
			glownyElement.getPptAdmListyTabele().size();//LAZY!
			User.info("Udany zapis / lst_id: %2$s <br/>nazwa: '%1$s'", glownyElement.getLstNazwa(),  glownyElement.getLstId());
			com.comarch.egeria.Utils.update("formId:accSql:lstSqlStats");
			nazwaListy = glownyElement.getLstNazwa();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert("Błąd w zapisie listy wartości!", e);
			return;
		}

		String jrxmlFileRealPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/WEB-INF/protectedElements/Reports/sourceReports/"+ glownyElement.getLstNazwa() +".jrxml");
		File f= new File(jrxmlFileRealPath);
		if(f.exists()) {
			f.delete();
		}
	}



	public void switchLp(PptAdmListyWartosci element, int step) {
		Long lp = element.getLstLp();
		if (step<0 && lp>2 || step>0){
			Optional<PptAdmListyWartosci> elToSwitch = glownyElement.getPptAdmListyWartoscis().stream().filter(x->x.getLstLp()==(lp+step)).findFirst();
			if (elToSwitch.isPresent())
				elToSwitch.get().setLstLp(lp);
			element.setLstLp(lp+step);
		}
	}





	public void dodajNowaEtykiete() {
		PptAdmListyWartosci nowa = new PptAdmListyWartosci();

		if (!glownyElement.getPptAdmListyWartoscis().isEmpty()) {
			PptAdmListyWartosci ostatni = glownyElement.getPptAdmListyWartoscis()
											.get(glownyElement.getPptAdmListyWartoscis().size() - 1);
			nowa.setLstLp(ostatni.getLstLp() + 1L);
		} else {
			nowa.setLstLp(2L);
		}
		nowa.setLstDlugosc(50L);
		nowa.setLstFInterfejs("N");
		nowa.setLstFUkryte("N");
		nowa.setLstFZwrot("T");
		nowa.setLstNazwa(nazwaListy);
		// listaElementow.add(nowa);
		glownyElement.addPptAdmListyWartosci(nowa);
	}


	public void usun() throws IOException {

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			glownyElement = (PptAdmListyWartosci) s.merge(glownyElement);


			s.delete(glownyElement);

			s.beginTransaction().commit();
			User.info("Usunięto listę wartości z wszystkimi jej elementami");
		} catch (Exception e) {
			User.alert("Błąd w usuwaniu listy wartości!");
			log.error(e.getMessage());
			return;
		}

		FacesContext.getCurrentInstance().getExternalContext().redirect("ListyWartosci");

	}

	private boolean validateBeforeSave() {
		ArrayList<String> inval = new ArrayList<String>();

		if (glownyElement.getLstQuery() == null || glownyElement.getLstQuery().isEmpty()) {
			inval.add("Lista musi posiadać zapytanie!");
		}

		if (glownyElement.getLstEtykieta() == null || glownyElement.getLstEtykieta().isEmpty()) {
			inval.add("Każdy element musi posiadać etykietę");
		}

		Long i = 0L;
		List<Long> powtarzalneLP = new ArrayList<>();
		for (PptAdmListyWartosci element : glownyElement.getPptAdmListyWartoscis()) {
			if (element.getLstNazwa() == null || element.getLstNazwa().isEmpty()) {
				inval.add("Każdy element musi posiadać nazwę listy");
			}

			if (element.getLstEtykieta() == null || element.getLstEtykieta().isEmpty()) {
				inval.add("Każdy element musi posiadać etykietę");
			}

			if (element.getLstLp() == 1L) {
				i++;
			}

			if (element.getLstLp() >= 100L) {
				inval.add("LP nie może być większe od 99");
			}

			if (element.getLstNazwaPola() == null || element.getLstNazwaPola().isEmpty()) {
				inval.add("Każdy element musi posiadać nazwę pola");
			}

			if (powtarzalneLP.contains(element.getLstLp())) {
				inval.add("LP: " + element.getLstLp() + " nie może się powtarzać!");
			} else {
				powtarzalneLP.add(element.getLstLp());
			}
		}

		if (i > 1L) {
			inval.add("Tylko jedna kolumna może mieć Lp = 1!");
		}

		for (String msg : inval) {
			User.alert(msg);
		}

		return inval.isEmpty();
	}

	public ArrayList<PptAdmListyWartosci> getListaElementow() {
		return listaElementow;
	}

	public void setListaElementow(ArrayList<PptAdmListyWartosci> listaElementow) {
		this.listaElementow = listaElementow;
	}

	public PptAdmListyWartosci getGlownyElement() {
		return glownyElement;
	}

	public void setGlownyElement(PptAdmListyWartosci pierwszyElement) {
		this.glownyElement = pierwszyElement;
	}

	public ArrayList<Object> getListaRow() {
		return listaRow;
	}

	public void setListaRow(ArrayList<Object> listaRow) {
		this.listaRow = listaRow;
	}

	public String getNazwaListy() {
		return nazwaListy;
	}

	public void setNazwaListy(String nazwaListy) {
		this.nazwaListy = nazwaListy;
	}


	public Boolean getCzyNowy() {
		return nz(this.glownyElement.getLstId())==0L;
	}


	public TreeMap<String, OHolder> getMapaArgumentow() {
		return mapaArgumentow;
	}

	public void setMapaArgumentow(TreeMap<String, OHolder> mapaArgumentow) {
		this.mapaArgumentow = mapaArgumentow;
	}


	public void updateAutoAkt(boolean aa) {
		try (final HibernateContext h = new HibernateContext()) {
			DbObject.updateOrInsertAktualizacjeAutomatyczne(h,  "PPADM", "LW", this.glownyElement.getLstNazwa().toUpperCase(), aa);
			this.getLW("ppl_adm_listy_wartosci").resetTs();
		} catch (Exception e) {
			User.alert(e);
		}
	}

	public boolean isAutoAkt() {
		final DataRow lwRow = this.getLW("ppl_adm_listy_wartosci")
				.getAllData().stream()
				.filter(r -> eq(this.glownyElement.getLstNazwa(), r.getAsString("pp_lst_nazwa")))
				.findFirst().orElse(null);

		if (lwRow == null)
			return false;
		else
			return eq("T", lwRow.getAsString("objp_f_auto_akt"));
	}


	public StreamedContent exportDml() throws UnsupportedEncodingException {
		StringBuilder sb = new StringBuilder();
		String lwname = this.glownyElement.getLstNazwa().toUpperCase();

		try (Connection conn = JdbcUtils.getConnection()){
			String dml = JdbcUtils.sqlFnCLOB(conn, "ppadm.dmp_lw", lwname);
			if (!dml.contains("ąęćłńóśźż")) {  dml += "\r\n--ascii/cp1250:ąęćłńóśźż\r\n"; }
			sb.append(dml);
		} catch (Exception e) {
			User.alert(e);
		}

		String fname = null;
		fname = "lw." + lwname + ".sql";
		ByteArrayInputStream is = new ByteArrayInputStream( sb.toString().getBytes("Cp1250") );
		StreamedContent ret =  new DefaultStreamedContent(is, "text/plain", fname);
		return ret;
	}
}
