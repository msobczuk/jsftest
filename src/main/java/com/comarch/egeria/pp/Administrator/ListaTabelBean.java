package com.comarch.egeria.pp.Administrator;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.data.liquibase.LiquibaseUtils;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;

import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.zip.ZipOutputStream;

@Named
@Scope("view")
public class ListaTabelBean extends SqlBean {

    private static final Logger log = LogManager.getLogger();
    private static final String LW_TABLES_SQL_ID = "PPL_ADM_TABLES";
    private static final String DDL_TABLE_SP_NAME = "PPP_DDL.DDL_TABLE_AND_COLUMNS";
    private static final String DDL_TABLES_SP_NAME = "PPP_DDL.DDL_ALL_TABLES_AND_COLUMNS";
    private static final String DMP_TABLE_FN_NAME = "PPADM.DMP_TAB";
    private static final String TABLE_NAME_COLUMN = "tb_table_name";
    private static final String TAB_FILE_EXTENSION = ".tab";
    private static final String ZIP_FILE_EXTENSION = ".zip";
    private static final String DATE_TIME_FORMAT = "yyyy-MM-dd.HH.mm.ss";
    private static final String CONTENT_TYPE = "text/plain";

    public void resetujZaznaczoneTabele() {
        SqlDataSelectionsHandler lw = getPplAdmTables();
        if (lw == null || lw.getSelectedRows().isEmpty()) return;

        try (Connection conn = JdbcUtils.getConnection()) {
            for (DataRow dr : lw.getSelectedRows()) {
                JdbcUtils.sqlSPCall(conn, DDL_TABLE_SP_NAME, dr.getIdAsLong());
                lw.resetTs();
            }
        } catch (Exception e) {
            User.alert("Nieudana próba resetowania zaznaczonych tabel w bazie danych do wersji standard");
            log.error(e.getMessage(), e);
        }
    }

    public void resetujWszystkieNiezgodneTabele() {
        try (Connection conn = JdbcUtils.getConnection()) {
            JdbcUtils.sqlSPCall(conn, DDL_TABLES_SP_NAME);
            this.getSql(LW_TABLES_SQL_ID).resetTs();
        } catch (Exception e) {
            User.alert("Nieudana próba resetowania tabel w bazie danych do wersji standard");
            log.error(e.getMessage(), e);
        }
    }

    public StreamedContent exportSelectionDml() {
        SqlDataSelectionsHandler lw = getPplAdmTables();
        if (lw == null || lw.getSelectedRows().isEmpty()) return null;

        if (lw.getSelectedRows().size()==1){
            return exportDmlFile();
        } else {
            return exportDmlZip();
        }
    }



    public StreamedContent exportSelectionXML() {
        SqlDataSelectionsHandler lw = getPplAdmTables();
        if (lw == null || lw.getSelectedRows().isEmpty()) return null;

        if (lw.getSelectedRows().size()==1){
            return exportXmlFile();
        } else {
            return exportXmlZip();
        }
    }


    public StreamedContent exportXmlFile() {
        StringBuilder sql = new StringBuilder();
        String tableName = null;

        SqlDataSelectionsHandler lw = getPplAdmTables();
        if (lw == null || lw.getSelectedRows().isEmpty()) return null;

        try (Connection conn = JdbcUtils.getConnection()) {
            for (DataRow r : lw.getSelectedRows()) {
                tableName = r.getAsString("tb_table_name");
                final String xml = LiquibaseUtils.dmpOracleTableChangelog(r.getAsString("tb_owner"), tableName);
                sql.append(xml);
            }

            String fname = tableName + ".xml";//

//            if (lw.getSelectedRows().size() == 1) {
//                fname = getDmlTableFileName(tableName, null, ".xml");
//            } else {
//                String now = Utils.format(new Date(), DATE_TIME_FORMAT);
//                fname = getDmlTablesFileName(lw.getSelectedRows().size(), now, ".xml");
//            }

//            ByteArrayInputStream bis = new ByteArrayInputStream(sql.toString().getBytes("Cp1250"));
            ByteArrayInputStream bis = new ByteArrayInputStream(sql.toString().getBytes(StandardCharsets.UTF_8));
            return new DefaultStreamedContent(bis, CONTENT_TYPE, fname);

        } catch (Exception e) {
            User.alert(e);
        }

        return null;
    }


    public StreamedContent exportXmlZip() {
        String tableName = null;

        SqlDataSelectionsHandler lw = getPplAdmTables();
        if (lw == null || lw.getSelectedRows().isEmpty()) return null;

        try (Connection conn = JdbcUtils.getConnection()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(baos);

            for (DataRow r : lw.getSelectedRows()) {
                tableName = (String) r.get(TABLE_NAME_COLUMN);
                final String xml = LiquibaseUtils.dmpOracleTableChangelog(r.getAsString("tb_owner"), tableName);
                String filename = tableName + ".xml";
                Utils.addToZipFile(xml.getBytes(StandardCharsets.UTF_8), filename, zos);
            }

            zos.close();
            ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());

            String now = Utils.format(new Date(), DATE_TIME_FORMAT);
            String fname = "pp.dmp.liquibase.tables.xml.(" + lw.getSelectedRows().size() + ")." + now + ".zip";

//            if (lw.getSelectedRows().size() == 1) {
//                fname = getDmlTableFileName(tableName, now, ZIP_FILE_EXTENSION);
//            } else {
//                fname = getDmlTablesFileName(lw.getSelectedRows().size(), now, ZIP_FILE_EXTENSION);
//            }

            return new DefaultStreamedContent(bis, CONTENT_TYPE, fname);
        } catch (Exception e) {
            User.alert(e);
        }

        return null;
    }







    

    public StreamedContent exportDmlFile() {
        StringBuilder sql = new StringBuilder();
        String tableName = null;

        SqlDataSelectionsHandler lw = getPplAdmTables();
        if (lw == null || lw.getSelectedRows().isEmpty()) return null;

        try (Connection conn = JdbcUtils.getConnection()) {
            for (DataRow r : lw.getSelectedRows()) {
                tableName = (String) r.get(TABLE_NAME_COLUMN);
                String dml = JdbcUtils.sqlFnCLOB(conn, DMP_TABLE_FN_NAME, tableName);
                if (!dml.contains("ąęćłńóśźż")) {
                    dml += "\r\n--ascii/cp1250:ąęćłńóśźż\r\n";
                }
                sql.append(dml);
            }

            String fname;

            if (lw.getSelectedRows().size() == 1) {
                fname = getDmlTableFileName(tableName, null, TAB_FILE_EXTENSION);
            } else {
                String now = Utils.format(new Date(), DATE_TIME_FORMAT);
                fname = getDmlTablesFileName(lw.getSelectedRows().size(), now, TAB_FILE_EXTENSION);
            }

            ByteArrayInputStream bis = new ByteArrayInputStream(sql.toString().getBytes("Cp1250"));
            return new DefaultStreamedContent(bis, CONTENT_TYPE, fname);
        } catch (Exception e) {
            User.alert(e);
        }

        return null;
    }


    public StreamedContent exportDmlZip() {
        String tableName = null;

        SqlDataSelectionsHandler lw = getPplAdmTables();
        if (lw == null || lw.getSelectedRows().isEmpty()) return null;

        try (Connection conn = JdbcUtils.getConnection()) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(baos);

            for (DataRow r : lw.getSelectedRows()) {
                tableName = (String) r.get(TABLE_NAME_COLUMN);
                String dmlTxt = JdbcUtils.sqlFnCLOB(conn, DMP_TABLE_FN_NAME, tableName);
                if (!dmlTxt.contains("ąęćłńóśźż")) {
                    dmlTxt += "\r\n--ascii/cp1250:ąęćłńóśźż\r\n";
                }
                String filename = getDmlTableFileName(tableName, null, TAB_FILE_EXTENSION);
                Utils.addToZipFile(dmlTxt.getBytes("Cp1250"), filename, zos);
            }

            zos.close();
            ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray());

            String fname;
            String now = Utils.format(new Date(), DATE_TIME_FORMAT);

            if (lw.getSelectedRows().size() == 1) {
                fname = getDmlTableFileName(tableName, now, ZIP_FILE_EXTENSION);
            } else {
                fname = getDmlTablesFileName(lw.getSelectedRows().size(), now, ZIP_FILE_EXTENSION);
            }

            return new DefaultStreamedContent(bis, CONTENT_TYPE, fname);
        } catch (Exception e) {
            User.alert(e);
        }

        return null;
    }

    public SqlDataSelectionsHandler getPplAdmTables() {
        return this.getLW(LW_TABLES_SQL_ID);
    }


    private String getDmlTablesFileName(int selectedRowsSize, String dateTime, String fileExtension) {
        return "pp.dmp.tab.(" + selectedRowsSize + ")." + dateTime + fileExtension;
    }

    private String getDmlTableFileName(String tableName, String dateTime, String fileExtension) {
        StringBuilder filename = new StringBuilder(tableName);

        if (dateTime != null) {
            filename.append(".").append(dateTime);
        }

        return filename.append(fileExtension).toString();
    }
    
    
    
    public void aktualizujPptTablesZPlikowTab() throws IOException, SQLException{
        try (Connection conn = JdbcUtils.getConnection()) {
            AppBean.importFilesTab(conn);
        }
    }
    
    
}
