package com.comarch.egeria.pp.Administrator.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_ADM_SLOWNIKI database table.
 * 
 */
@Entity
@Table(name="PPT_ADM_SLOWNIKI")
@NamedQuery(name="PptAdmSlowniki.findAll", query="SELECT p FROM PptAdmSlowniki p")
public class PptAdmSlowniki implements Serializable {
	private static final long serialVersionUID = 1L;
	
	
	public void show(){
		System.out.println("==========================================================================================================");
		System.out.println(String.format("SL_ID: %1$s; SL_NAZWA: %2$s" , this.slId, this.slNazwa));
		for (PptAdmWartosciSlownikow ws : pptAdmWartosciSlownikows) {
			System.out.println(String.format("#%3$s; WSL_SL_NAZWA: %1$s; WSL_WARTOSC: %2$s" , ws.getWslSlNazwa(), ws.getWslWartosc() , ws.getWslId() ));
		}
		System.out.println("==========================================================================================================");
	}
	

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ADM_SLOWNIKI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ADM_SLOWNIKI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ADM_SLOWNIKI")	
	@Column(name="SL_ID")
	private Long slId;

//	@Column(name="SL_AUDYT_KM")
//	private String slAudytKm;
//
//	@Column(name="SL_AUDYT_KT")
//	private String slAudytKt;
//
//	@Column(name="SL_AUDYT_LM")
//	private BigDecimal slAudytLm;
//
//	@Temporal(TemporalType.DATE)
//	@Column(name="SL_AUDYT_DT")
//	private Date slDateCreated;
//
//	@Temporal(TemporalType.DATE)
//	@Column(name="SL_AUDYT_DM")
//	private Date slDateModified;

	@Column(name="SL_F_INSTALL_DODAJ")
	private String slFInstallDodaj;

	@Column(name="SL_F_INSTALL_MODYFIKUJ")
	private String slFInstallModyfikuj;

	@Column(name="SL_F_INSTALL_USUN")
	private String slFInstallUsun;

	@Column(name="SL_MAX_DLUGOSC")
	private Long slMaxDlugosc;

	@Column(name="SL_MAX_ILOSC")
	private Long slMaxIlosc;

	@Column(name="SL_NAZWA")
	private String slNazwa;

	@Column(name="SL_NAZWA_ZEWN")
	private String slNazwaZewn;

	@Column(name="SL_OPIS")
	private String slOpis;

	@Column(name="SL_POZIOM_DOSTEPU")
	private String slPoziomDostepu;

//	@Column(name="SL_AUDYT_UT")
//	private String slUserCreated;
//
//	@Column(name="SL_AUDYT_UM")
//	private String slUserModified;

	@Column(name="SL_WZORZEC")
	private String slWzorzec;
	
	@Column(name="SL_F_AKTUALIZUJ")
	private String slFAktualizuj;

	//bi-directional many-to-one association to PptAdmWartosciSlownikow
	@OneToMany(mappedBy="pptAdmSlowniki", cascade={CascadeType.ALL},  orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("WSL_LP, WSL_WARTOSC ASC")
	private List<PptAdmWartosciSlownikow> pptAdmWartosciSlownikows  = new ArrayList<>();

	public PptAdmSlowniki() {
	}

	public Long getSlId() {
		return this.slId;
	}

	public void setSlId(Long slId) {
		this.slId = slId;
	}

//	public String getSlAudytKm() {
//		return this.slAudytKm;
//	}
//
//	public void setSlAudytKm(String slAudytKm) {
//		this.slAudytKm = slAudytKm;
//	}
//
//	public String getSlAudytKt() {
//		return this.slAudytKt;
//	}
//
//	public void setSlAudytKt(String slAudytKt) {
//		this.slAudytKt = slAudytKt;
//	}
//
//	public BigDecimal getSlAudytLm() {
//		return this.slAudytLm;
//	}
//
//	public void setSlAudytLm(BigDecimal slAudytLm) {
//		this.slAudytLm = slAudytLm;
//	}
//
//	public Date getSlDateCreated() {
//		return this.slDateCreated;
//	}
//
//	public void setSlDateCreated(Date slDateCreated) {
//		this.slDateCreated = slDateCreated;
//	}
//
//	public Date getSlDateModified() {
//		return this.slDateModified;
//	}
//
//	public void setSlDateModified(Date slDateModified) {
//		this.slDateModified = slDateModified;
//	}

	public String getSlFInstallDodaj() {
		return this.slFInstallDodaj;
	}

	public void setSlFInstallDodaj(String slFInstallDodaj) {
		this.slFInstallDodaj = slFInstallDodaj;
	}

	public String getSlFInstallModyfikuj() {
		return this.slFInstallModyfikuj;
	}

	public void setSlFInstallModyfikuj(String slFInstallModyfikuj) {
		this.slFInstallModyfikuj = slFInstallModyfikuj;
	}

	public String getSlFInstallUsun() {
		return this.slFInstallUsun;
	}

	public void setSlFInstallUsun(String slFInstallUsun) {
		this.slFInstallUsun = slFInstallUsun;
	}

	public Long getSlMaxDlugosc() {
		return this.slMaxDlugosc;
	}

	public void setSlMaxDlugosc(Long slMaxDlugosc) {
		this.slMaxDlugosc = slMaxDlugosc;
	}

	public Long getSlMaxIlosc() {
		return this.slMaxIlosc;
	}

	public void setSlMaxIlosc(Long slMaxIlosc) {
		this.slMaxIlosc = slMaxIlosc;
	}

	public String getSlNazwa() {
		return this.slNazwa;
	}

	public void setSlNazwa(String slNazwa) {
		this.slNazwa = slNazwa;
	}

	public String getSlNazwaZewn() {
		return this.slNazwaZewn;
	}

	public void setSlNazwaZewn(String slNazwaZewn) {
		this.slNazwaZewn = slNazwaZewn;
	}

	public String getSlOpis() {
		return this.slOpis;
	}

	public void setSlOpis(String slOpis) {
		this.slOpis = slOpis;
	}

	public String getSlPoziomDostepu() {
		return this.slPoziomDostepu;
	}

	public void setSlPoziomDostepu(String slPoziomDostepu) {
		this.slPoziomDostepu = slPoziomDostepu;
	}

//	public String getSlUserCreated() {
//		return this.slUserCreated;
//	}
//
//	public void setSlUserCreated(String slUserCreated) {
//		this.slUserCreated = slUserCreated;
//	}
//
//	public String getSlUserModified() {
//		return this.slUserModified;
//	}
//
//	public void setSlUserModified(String slUserModified) {
//		this.slUserModified = slUserModified;
//	}

	public String getSlWzorzec() {
		return this.slWzorzec;
	}

	public void setSlWzorzec(String slWzorzec) {
		this.slWzorzec = slWzorzec;
	}
	
	public String getSlFAktualizuj() {
		return slFAktualizuj;
	}

	public void setSlFAktualizuj(String slFAktualizuj) {
		this.slFAktualizuj = slFAktualizuj;
	}

	public List<PptAdmWartosciSlownikow> getPptAdmWartosciSlownikows() {
		return this.pptAdmWartosciSlownikows;
	}

	public void setPptAdmWartosciSlownikows(List<PptAdmWartosciSlownikow> pptAdmWartosciSlownikows) {
		this.pptAdmWartosciSlownikows = pptAdmWartosciSlownikows;
	}

	public PptAdmWartosciSlownikow addPptAdmWartosciSlownikow(PptAdmWartosciSlownikow pptAdmWartosciSlownikow) {
		getPptAdmWartosciSlownikows().add(pptAdmWartosciSlownikow);
		pptAdmWartosciSlownikow.setPptAdmSlowniki(this);

		return pptAdmWartosciSlownikow;
	}

	public PptAdmWartosciSlownikow removePptAdmWartosciSlownikow(PptAdmWartosciSlownikow pptAdmWartosciSlownikow) {
		getPptAdmWartosciSlownikows().remove(pptAdmWartosciSlownikow);
		pptAdmWartosciSlownikow.setPptAdmSlowniki(null);

		return pptAdmWartosciSlownikow;
	}

}