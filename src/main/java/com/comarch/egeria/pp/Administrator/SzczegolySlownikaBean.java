package com.comarch.egeria.pp.Administrator;

import com.comarch.egeria.pp.Administrator.model.PptAdmSlowniki;
import com.comarch.egeria.pp.Administrator.model.PptAdmWartosciSlownikow;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.erd.model.DbObject;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

@ManagedBean
@Scope("view")
@Named
public class SzczegolySlownikaBean extends SqlBean implements Serializable {
	private static final Logger log = LogManager.getLogger();

	@Inject
	SessionBean sessionBean;


	// private DataRow listaWartosciDR = null;

	PptAdmSlowniki slownikWartosci = new PptAdmSlowniki();

	PptAdmWartosciSlownikow wartoscSlownikowa = new PptAdmWartosciSlownikow();

	// List<PptAdmWartosciSlownikow> listaWartosciSlownika = new
	// ArrayList<PptAdmWartosciSlownikow>();

	Boolean czyNowy = false;

	String checkString = "";

	String nazwaSlownika = "";

	String nazwaSlownikaEdit = "";

	@PostConstruct
	public void init() {
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		String sl_nazwa = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("sl_nazwa");
		if (sl_nazwa!=null) {
			Optional<DataRow> any = getLW("ppl_adm_slowniki").getAllData().stream().filter(r -> sl_nazwa.equals(r.get("sl_nazwa"))).findAny();
			if (any.isPresent()) {
				DataRow rs = any.get();
				id = rs.getIdAsLong();
			}
		}

		if (id == null) {
			slownikWartosci.setPptAdmWartosciSlownikows(new ArrayList<PptAdmWartosciSlownikow>());
			slownikWartosci.setSlPoziomDostepu("0");
			slownikWartosci.setSlMaxDlugosc(100L);
			slownikWartosci.setSlFInstallDodaj("T");
			slownikWartosci.setSlFInstallModyfikuj("T");
			slownikWartosci.setSlFInstallUsun("T");

			wartoscSlownikowa.setWslTyp("UC");
			wartoscSlownikowa.setWslStatus("N");

			slownikWartosci.addPptAdmWartosciSlownikow(wartoscSlownikowa);

			czyNowy = true;

		} else {
			this.slownikWartosci = (PptAdmSlowniki) HibernateContext.get(PptAdmSlowniki.class, (Serializable) id);
			nazwaSlownika = slownikWartosci.getSlNazwa();
		}

		checkString = getCheckString();

		nazwaSlownikaEdit = nazwaSlownika;
	}

	Comparator<PptAdmWartosciSlownikow> comparator_columns = new Comparator<PptAdmWartosciSlownikow>() {

		@Override
		public int compare(PptAdmWartosciSlownikow o1, PptAdmWartosciSlownikow o2) {
			Long i = o2.getWslId();
			Long j = o1.getWslId();
			if (i < j) {
				return 1;
			} else if (i > j) {
				return -1;
			} else {
				return 0;
			}
		}

	};

	public void dodajWartosc() {

		
		PptAdmWartosciSlownikow paw = new PptAdmWartosciSlownikow();
		paw.setWslTyp("UC");
		paw.setWslStatus("N");
		paw.setWslSlNazwa(slownikWartosci.getSlNazwa());
		slownikWartosci.addPptAdmWartosciSlownikow(paw);
	}



	public void zmienNazwe() {
		slownikWartosci.setSlNazwa(nazwaSlownikaEdit);

		for(PptAdmWartosciSlownikow paw : slownikWartosci.getPptAdmWartosciSlownikows()) {
			paw.setWslSlNazwa(nazwaSlownikaEdit);
		}
		
		nazwaSlownika = nazwaSlownikaEdit;
		zapisz();
	}

	
	
	
	
	public void zapisz() {

		prepareData();

		if (!validateBeforeSave()) {
			return;
		}

		try {

			this.slownikWartosci.show();
			slownikWartosci = (PptAdmSlowniki) HibernateContext.save(slownikWartosci);
			this.slownikWartosci.show();
			
			User.info("Udany zapis słownika");

		} catch (Exception e) {
			log.error(e.getStackTrace());

			if (getCause(e.getCause()) != null) {

				if (getCause(e.getCause()).getMessage().contains("PPT_ADM_WARTOSCI_SLOWNIKO_UK1")) {
					User.alert("Nie można utworzyć dwóch wartości o takiej samej nazwie w jednym słowniku!");
					return;
				}

				if (getCause(e.getCause()).getMessage().contains("PPT_ADM_SLOWNIKI_UK1")) {
					User.alert("Nie można utworzyć dwóch słowników o takiej samej nazwie!");
					return;
				}
			}

			User.alert("Błąd w zapisie słownika!", e.getMessage());
			return;
		}

		checkString = getCheckString();

	}

	Throwable getCause(Throwable e) {
		Throwable cause = null;
		Throwable result = e;

		while (null != (cause = result.getCause()) && (result != cause)) {
			result = cause;
		}
		return result;
	}


	private void prepareData() {

		if (slownikWartosci.getSlNazwa() == null || slownikWartosci.getSlNazwa().isEmpty()) {
			slownikWartosci.setSlNazwa(nazwaSlownika);
		}

		for (PptAdmWartosciSlownikow paw : slownikWartosci.getPptAdmWartosciSlownikows()) {
			if (paw.getWslWartosc() == null) {
				slownikWartosci.removePptAdmWartosciSlownikow(paw);
			} else if (paw.getWslSlNazwa() == null) {
				paw.setWslSlNazwa(slownikWartosci.getSlNazwa());
			}
		}

	}

	public String getNazwaSlownika() {
		return nazwaSlownika;
	}

	public void setNazwaSlownika(String nazwaSlownika) {
		this.nazwaSlownika = nazwaSlownika;
	}

	public void usunWartosc(PptAdmWartosciSlownikow sw) {
		slownikWartosci.removePptAdmWartosciSlownikow(sw);
	}

	public void usun() throws IOException {
		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			s.delete(slownikWartosci);
			s.beginTransaction().commit();
		}
		FacesContext.getCurrentInstance().getExternalContext().redirect("ListaSlownikow");

	}

	private boolean validateBeforeSave() {
		ArrayList<String> inval = new ArrayList<String>();

//		String currentCheckString = this.getCheckString();

//		if (this.checkString.equals(currentCheckString))
//			inval.add("Brak zmian do zapisania.");

		if (slownikWartosci.getSlNazwa() == null || slownikWartosci.getSlNazwa().isEmpty()) {
			inval.add("Nazwa słownika nie może być pusta!");
		}

		if (slownikWartosci.getSlOpis() == null || slownikWartosci.getSlOpis().isEmpty()) {
			inval.add("Opis słownika nie może być pusty!");
		}

		if (slownikWartosci.getSlMaxDlugosc() == null || slownikWartosci.getSlMaxDlugosc() == 0L) {
			inval.add("Długość wartości nie może być pusta!");
		}

		if (slownikWartosci.getSlPoziomDostepu() == null || slownikWartosci.getSlPoziomDostepu().isEmpty()) {
			inval.add("Poziom dostępu nie może być pusty!");
		}

		if (slownikWartosci.getSlFInstallDodaj() == null || slownikWartosci.getSlFInstallDodaj().isEmpty()
				|| slownikWartosci.getSlFInstallModyfikuj() == null
				|| slownikWartosci.getSlFInstallModyfikuj().isEmpty() || slownikWartosci.getSlFInstallUsun() == null
				|| slownikWartosci.getSlFInstallUsun().isEmpty()) {
			inval.add(
					"Należy określić, czy skrypty instalacyjne mogą wstawiać, modyfikować i usuwać wartości słownika");
		}

		if (slownikWartosci.getPptAdmWartosciSlownikows() != null
				|| !slownikWartosci.getPptAdmWartosciSlownikows().isEmpty()) {
			for (PptAdmWartosciSlownikow cws : slownikWartosci.getPptAdmWartosciSlownikows()) {
				if (cws.getWslWartosc() == null || cws.getWslWartosc().isEmpty()) {
					inval.add("Wartości słownika nie może być pusta!");
				}

				if (cws.getWslSlNazwa() == null || cws.getWslSlNazwa().isEmpty()) {
					inval.add("Przed dodaniem wartości należy zapisać słownik");
				}
			}
		}

		for (String msg : inval) {
			User.alert(msg);
		}

		return inval.isEmpty();
	}

	public String getCheckString() {
		String ret = "";

		if (slownikWartosci != null) {
			ret = ret + AppBean.concatValues(slownikWartosci.getSlNazwa(), slownikWartosci.getSlOpis(),
					slownikWartosci.getPptAdmWartosciSlownikows().size(), slownikWartosci.getSlPoziomDostepu());
		}

		if (slownikWartosci.getPptAdmWartosciSlownikows() != null
				&& !slownikWartosci.getPptAdmWartosciSlownikows().isEmpty()) {

			for (PptAdmWartosciSlownikow wartosc : slownikWartosci.getPptAdmWartosciSlownikows()) {
				ret = ret + AppBean.concatValues(wartosc.getWslWartosc(), wartosc.getWslAlias(), wartosc.getWslStatus(),
						wartosc.getWslTyp(), wartosc.getWslOpis(), wartosc.getWslTyp(), wartosc.getWslWartoscZewn());
			}

		}

		return ret;
	}

	public Boolean getCzyNowy() {
		return czyNowy;
	}

	public void setCzyNowy(Boolean czyNowy) {
		this.czyNowy = czyNowy;
	}

	public String getNazwaSlownikaEdit() {
		return nazwaSlownikaEdit;
	}

	public void setNazwaSlownikaEdit(String nazwaSlownikaEdit) {
		this.nazwaSlownikaEdit = nazwaSlownikaEdit;
	}

	public PptAdmSlowniki getSlownikWartosci() {
		return slownikWartosci;
	}

	public void setSlownikWartosci(PptAdmSlowniki slownikWartosci) {
		this.slownikWartosci = slownikWartosci;
	}

	public PptAdmWartosciSlownikow getWartoscSlownikowa() {
		return wartoscSlownikowa;
	}

	public void setWartoscSlownikowa(PptAdmWartosciSlownikow wartoscSlownikowa) {
		this.wartoscSlownikowa = wartoscSlownikowa;
	}

	public void updateAutoAkt(boolean aa) {
		try (final HibernateContext h = new HibernateContext()) {
			DbObject.updateOrInsertAktualizacjeAutomatyczne(h,  "PPADM", "SL",  this.slownikWartosci.getSlNazwa().toUpperCase(), aa);
			this.getLW("ppl_adm_slowniki").resetTs();
		} catch (Exception e) {
			User.alert(e);
		}
	}

	public boolean isAutoAkt() {
		final DataRow lwRow = this.getLW("ppl_adm_slowniki")
				.getAllData().stream()
				.filter(r -> eq(this.slownikWartosci.getSlNazwa(), r.getAsString("sl_nazwa")))
				.findFirst().orElse(null);

		if (lwRow == null)
			return false;
		else
			return eq("T", lwRow.getAsString("objp_f_auto_akt"));
	}


	public StreamedContent exportDml() throws UnsupportedEncodingException {
		String name = this.slownikWartosci.getSlNazwa().toUpperCase();
		String fname = "sl." + name + ".sql";
		String dml = JdbcUtils.getDmpSL(name);
		ByteArrayInputStream is = new ByteArrayInputStream( dml.getBytes("Cp1250") );
		StreamedContent ret =  new DefaultStreamedContent(is, "text/plain", fname);
		return ret;
	}

}
