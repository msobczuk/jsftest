package com.comarch.egeria.pp.Administrator;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.erd.model.DbObjSlownik;
import com.comarch.egeria.pp.erd.model.DbObject;
import com.comarch.egeria.web.User;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;

import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.util.Date;
import java.util.zip.ZipOutputStream;

import static com.comarch.egeria.Utils.nz;

@Named
@Scope("view")
public class ListaSlownikowBean extends SqlBean {


    public void edit(DataRow r) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", r.getIdAsLong());
        FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", r);
        Utils.redirectTo("SzczegolySlownika");
    }




    public void wczytajDMLStdDLaZazn(){
        SqlDataSelectionsHandler lw = this.getSql("PPL_ADM_SLOWNIKI");
        if (lw==null || lw.getSelectedRows().isEmpty()) return;

        for (DataRow r : lw.getSelectedRows()) {
            String slNazwa = r.getAsString("sl_nazwa");
            if(DbObjSlownik.resetToStdVersion(slNazwa)) {
                User.warn("Słowniki: wykonano skrypt sl." + slNazwa.toUpperCase() + ".sql");
            }
        }
        lw.resetTs();
    }


    public StreamedContent exportDml() throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        String lwname = "";

        SqlDataSelectionsHandler lw = this.getSql("PPL_ADM_SLOWNIKI");
        if (lw==null || lw.getSelectedRows().isEmpty()) return null;

        try (Connection conn = JdbcUtils.getConnection()){
            for (DataRow r : lw.getSelectedRows()) {
                lwname += r.get("sl_nazwa") + ".";
                String dml = JdbcUtils.getDmpSL(conn, r.getAsString("sl_nazwa"));// JdbcUtils.sqlFnCLOB(conn, "ppadm.dmp_sl", r.get("sl_nazwa"));
                sb.append(dml);
            }
        } catch (Exception e) {
            User.alert(e);
        }

        String fname = null;

        String now = Utils.format(new Date(), "yyyy-MM-dd.HH.mm.ss");

        if (lw.getSelectedRows().size()==1)
            fname = "pp.dmp.sl." + lwname + now + ".sql";
        else
            fname = "pp.dmp.sl.("+lw.getSelectedRows().size()+")." + now + ".sql";

        ByteArrayInputStream is = new ByteArrayInputStream( sb.toString().getBytes("Cp1250") );
        StreamedContent ret =  new DefaultStreamedContent(is, "text/plain", fname);
        return ret;
    }





    public StreamedContent exportDmlZip(){
        String name = "";

        SqlDataSelectionsHandler lw = this.getSql("PPL_ADM_SLOWNIKI");
        if (lw==null || lw.getSelectedRows().isEmpty()) return null;

        try (Connection conn = JdbcUtils.getConnection()){

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(baos);
            for (DataRow r : lw.getSelectedRows()) {
                name = (String) r.get("sl_nazwa");
                String dmlTxt =  JdbcUtils.getDmpSL(conn, name);
                Utils.addToZipFile(dmlTxt.getBytes("Cp1250"), "sl."+name + ".sql", zos);
            }
            zos.close();
            ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray() );


            String fname = null;
            String now = Utils.format(new Date(), "yyyy-MM-dd.HH.mm.ss");

            if (lw.getSelectedRows().size()==1)
                fname = "pp.dmp.sl." + name + now + ".zip";
            else
                fname = "pp.dmp.sl.("+lw.getSelectedRows().size()+")." + now + ".zip";

            return new DefaultStreamedContent(bis , "text/plain", fname);

        } catch (Exception e) {
            User.alert(e);
        }

        return null;
    }



    public void updateAutoAkt(boolean aa) {
        SqlDataSelectionsHandler lw = this.getSql("PPL_ADM_SLOWNIKI");
        if (lw == null || lw.getSelectedRows().isEmpty()) return;
        try (Connection conn = JdbcUtils.getConnection()) {
            for (DataRow r : lw.getSelectedRows()) {
                final String oName = nz(r.getAsString("sl_nazwa")).toUpperCase();
                boolean b = DbObject.updateAktualizacjeAutomatyczne(conn, "PPADM", "SL", oName, aa);
                if (!b){
                    User.warn("Nie ustawiono flagi AA dla %1$s. Sprawdź czy taki wpis jest w PPT_OBJECTS.", oName);
                }
            }
        } catch (Exception e) {
            User.alert(e);
        }
        lw.resetTs();
    }




}
