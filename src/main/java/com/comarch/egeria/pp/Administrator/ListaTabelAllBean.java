package com.comarch.egeria.pp.Administrator;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.data.liquibase.LiquibaseUtils;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;

import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.zip.ZipOutputStream;

@Named
@Scope("view")
public class ListaTabelAllBean extends SqlBean {

    public StreamedContent exportSelectionXML() {
        SqlDataSelectionsHandler lw = getAllTables();
        if (lw == null || lw.getSelectedRows().isEmpty()) return null;

        if (lw.getSelectedRows().size()==1){
            String TableOwner = lw.getSelectedRows().get(0).getAsString("tb_owner");
            String tableName = lw.getSelectedRows().get(0).getAsString("tb_table_name");
            return LiquibaseUtils.exportXmlFile(TableOwner, tableName);
        } else {
            return LiquibaseUtils.exportXmlZip(lw);
        }
    }

    public SqlDataSelectionsHandler getAllTables() {
        return this.getLW("PPL_ALL_TABLES");
    }
}
