package com.comarch.egeria.pp.Administrator;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.translacja.model.PptAdmTranslacje;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@ManagedBean
@Scope("view")
@Named
public class DaneTranslacjiBean extends SqlBean implements Serializable {
	private static final Logger log = LogManager.getLogger();

	@Inject
	SessionBean sessionBean;


	PptAdmTranslacje translacja = new PptAdmTranslacje();
	
	List<PptAdmTranslacje> listaTranslacji = new ArrayList<>();
	
	@PostConstruct
	public void init() {
		Long id = (Long) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		if (id == null) {
			translacja = new PptAdmTranslacje();
			listaTranslacji.add(translacja);
			return;
		}

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			translacja = s.get(PptAdmTranslacje.class, id);
			
			if(translacja != null) {
				
				ArrayList<Object> params =  new ArrayList<>();
				params.add(translacja.getTraPl());
				
				this.loadSqlData("sqlTlumaczenia","select tra_id from ppt_adm_translacje where tra_pl = ?",params);
				
				SqlDataSelectionsHandler ssh = this.getSql("sqlTlumaczenia");
				if(ssh.getData() != null) {
					for(DataRow dr : ssh.getData()) {
						listaTranslacji.add(s.get(PptAdmTranslacje.class, dr.getIdAsLong()));
					}
				}
			}
			s.close();
		}
	}

	public PptAdmTranslacje getTranslacja() {
		return translacja;
	}

	public void setTranslacja(PptAdmTranslacje translacja) {
		this.translacja = translacja;
	}

	public List<PptAdmTranslacje> getListaTranslacji() {
		return listaTranslacji;
	}

	public void setListaTranslacji(List<PptAdmTranslacje> listaTranslacji) {
		this.listaTranslacji = listaTranslacji;
	}

	public void zapisz() {

		if (!validateBeforeSave()) {
			return;
		}
		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			s.close();
		} catch (Exception e) {
			log.error(e.getStackTrace());
		}
	}

	public void dodajNowaWartosc() {
	}

	public void zapiszWartosc() {

	}


	public void usunWartosc(PptAdmTranslacje tr) {
	}

	public void usun() throws IOException {

		FacesContext.getCurrentInstance().getExternalContext().redirect("ListaTranslacji");

	}

	private boolean validateBeforeSave() {
		ArrayList<String> inval = new ArrayList<String>();


		for (String msg : inval) {
			User.alert(msg);
		}

		return inval.isEmpty();
	}

	public String getCheckString() {
		String ret = "";

		return ret;
	}


}
