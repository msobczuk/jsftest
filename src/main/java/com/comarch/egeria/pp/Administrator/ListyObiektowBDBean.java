package com.comarch.egeria.pp.Administrator;

import com.comarch.egeria.Params;
import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.erd.model.*;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;

import javax.inject.Named;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.zip.ZipOutputStream;

import static com.comarch.egeria.Utils.nz;

@Named
@Scope("session")
public class ListyObiektowBDBean extends SqlBean {

    private static final Logger log = LogManager.getLogger();

    private String activeTab = "PROCEDURE"; // 'PROCEDURE', 'FUNCTION', 'VIEW', 'PACKAGE', 'PACKAGE BODY', 'TRIGGER'  // "prc";

    public SqlDataSelectionsHandler getPplAdmDbObjects(){
        SqlDataSelectionsHandler lw = this.getLW("PPL_ADM_DB_OBJECTS", activeTab);

        if (eq(activeTab,"VIEW") && lw.getTs()<=0){
            try {
                ConcurrentHashMap<String, DbObjView> resfilesviews = DbObjView.getAllResFilesDbObjViewsCHM();
                SqlDataSelectionsHandler lwAllViewText = getLW("PPL_ALL_VIEW_TEXT");

                for (DataRow r : lw.getData()) {
                    String key = r.getAsString("objp_owner") + "." + r.getAsString("objp_name");

                    DbObjView obj = resfilesviews.get(key);
                    DataRow rvtxt = lwAllViewText.find(key);

                    //przekolorowac i ustawic wynik w zaleznosci od tego czy sys.all_views.text jest np. w pliku src
                    if (obj==null) {//brak pliku .vw
                        r.set("diff_rows", 9999L);
                        r.set("css_color", "#FF0000");
                        continue;
                    }

                    if (rvtxt==null) {//brak widoku w sys.all_source
                        r.set("diff_rows", 9998L);
                        r.set("css_color", "#CC0000");
                        continue;
                    }

                    String vwFileCode = nz(obj.getCode()).replace("\r","");
                    String sysAllViewTextCode = rvtxt.getAsString("TEXT").replace("\r","");

                    if (!vwFileCode.contains(sysAllViewTextCode)) {//kod .vw zawier kod z sys.all_view.text
                        r.set("diff_rows", 1L);
                        r.set("css_color", "#AA0000");
                    } else {
                        r.set("diff_rows", 0L);
                        r.set("css_color", "#000000");
                    }

                }

                resfilesviews.clear();
            } catch (Exception ex){
                User.alert(ex);
            }
        }
        return lw;
    }

    public Params getFileExtensions() {
        return DbObject.fileExtensions;
    }

    public void resetujZaznaczoneDoWersjiStd() {
        SqlDataSelectionsHandler lw = getPplAdmDbObjects();
        if (lw == null || lw.getSelectedRows().isEmpty()) return;

        resetujDoWersjiStd(lw.getSelectedRows());
        lw.resetTs();
    }


    public void kasujZaznaczone() {
        SqlDataSelectionsHandler lw = getPplAdmDbObjects();
        if (lw == null || lw.getSelectedRows().isEmpty()) return;

        try (Connection conn = JdbcUtils.getConnection()){
            for (DataRow r : lw.getSelectedRows()) {
                DbObject.deletePptObjectAndPptAllSource(conn, r.getIdAsLong());
            }
        } catch (Exception e) {
            User.alert(e);
        }

        lw.resetTs();
    }


    private void resetujDoWersjiStd(List<DataRow> dataRows) {
        int resetObjects = 0;

        List<DataRow> dataRowsToUnselect = new ArrayList<>();

        try {
            for (DataRow dr : dataRows) {
                if (resetujObiektDoWersjiStd(dr)) {
                    resetObjects++;
                    dataRowsToUnselect.add(dr);
                } else {
                    User.warn("Niepowodzenie rekompilacji kodu %1$s (%2$s) ", dr.getAsString("objp_name"), dr.getAsString("objp_object_type"));
                }
            }
            User.info("Sukces: " + resetObjects + ", niepowodzenie: " + (dataRows.size() - resetObjects));
        } catch (Exception e) {
            User.alert("Resetowanie obiektu(-ów) nie powiodło się");
            log.error(e.getMessage(), e);
        }

        for (DataRow r : dataRowsToUnselect) {
            this.getPplAdmDbObjects().setRowSelection(false, r);
        }
    }

    private boolean resetujObiektDoWersjiStd(DataRow dr) {
        String objectName = dr.getAsString("objp_name");
        String type = dr.getAsString("objp_object_type");

        switch (type) {
            case "PROCEDURE":
                return DbObjProcedure.resetToStdVersion(objectName);
            case "FUNCTION":
                return DbObjFunction.resetToStdVersion(objectName);
            case "VIEW":
                return DbObjView.resetToStdVersion(objectName);
            case "PACKAGE":
                return DbObjPackage.resetToStdVersion(objectName);
            case "PACKAGE BODY":
                return DbObjPackageBody.resetToStdVersion(objectName);
            case "TRIGGER":
                return DbObjTrigger.resetToStdVersion(objectName);
        }

        return false;
    }

    public void resetujWszystkieDoWersjiStd() {
        SqlDataSelectionsHandler lw = getPplAdmDbObjects(); // this.getLW(LW_DB_OBJECTS_SQL_ID, activeTab); // getLwParamByActiveTabId());
        if (lw == null) return;

        List<DataRow> dataRows = lw.getAllData().stream()
                .filter(dr -> ((BigDecimal) dr.get("diff_rows")).intValue() != 0).collect(Collectors.toList());

        resetujDoWersjiStd(dataRows);
        lw.resetTs();
    }

    public void onTabChange(TabChangeEvent event) {
        activeTab = event.getTab().getTitle();
    }


    public int getTabViewActiveIndex() {
        return tabViewActiveIndex;
    }

    public void setTabViewActiveIndex(int tabViewActiveIndex) {
        this.tabViewActiveIndex = tabViewActiveIndex;
    }

    int tabViewActiveIndex = 0;






    public StreamedContent dmpSelectedAllSource(){
        SqlDataSelectionsHandler lw = getPplAdmDbObjects(); // getLW(LW_DB_OBJECTS_SQL_ID);
        if (lw==null || lw.getSelectedRows().isEmpty()){
            return null;
        }

        if (lw.getSelectedRows().size()==1) {
            return dmpAllSource2SqlFile(lw.getSelectedRows().get(0));
        }

        if (lw.getSelectedRows().size()>1) {
            return dmpAllSource2ZipFile(lw.getSelectedRows());
        }

        return null;
    }


    private StreamedContent dmpAllSource2SqlFile(DataRow r){
        if (r==null) return null;

        String owner = r.getAsString("objp_owner");
        String objectName = nz(r.getAsString("objp_name"));
        String objectType = r.getAsString("objp_object_type");

        try (Connection conn = JdbcUtils.getConnection()){
            String code = getDmpCode(owner, objectName, objectType, conn);
            ByteArrayInputStream is = new ByteArrayInputStream( code.getBytes("Cp1250") );
            return  new DefaultStreamedContent(is, "text/plain", DbObject.getFileName(owner, objectName, objectType));
        } catch (Exception e) {
            User.alert(e);
        }

        return null;
    }

    private String getDmpCode(String owner, String objectName, String objectType, Connection conn) throws IOException, SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        String code = JdbcUtils.getDbSourceCode(conn, owner, objectName, objectType, true, true, true, true);
        if (!code.contains("ąęćłńóśźż")) {
            code += "\r\n--ascii/cp1250:ąęćłńóśźż\r\n";
        }
        return code;
    }


    private StreamedContent dmpAllSource2ZipFile(List<DataRow> rows)  {
        try (Connection conn = JdbcUtils.getConnection()){
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream zos = new ZipOutputStream(baos);
            for (DataRow r : rows) {
                String owner = r.getAsString("objp_owner");
                String objectName = nz(r.getAsString("objp_name"));
                String objectType = r.getAsString("objp_object_type");
                String code = getDmpCode(owner, objectName, objectType, conn);
                Utils.addToZipFile(code.getBytes("Cp1250"), DbObject.getFileName(owner, objectName, objectType), zos); // +".sql"
            }
            zos.close();
            ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray() );

            String now = Utils.format(new Date(), "yyyy-MM-dd.HH.mm.ss");
            String fname = "dmp("+rows.size()+")." + now + ".zip";
            return new DefaultStreamedContent(bis , "text/plain", fname);

        } catch (Exception e) {
            User.alert(e);
        }
        return null;
    }


    public void testLoadResourceFiles2pptObjects(){
//        DbObject.loadResourceFiles2PptObjects(false);

        try (HibernateContext h = new HibernateContext()) {
            DbObjPackage.loadStdVersionFromResources(h, false);
            DbObjView.loadStdVersionFromResources(h, false);
            DbObjProcedure.loadStdVersionFromResources(h, false);
            DbObjFunction.loadStdVersionFromResources(h, false);
            DbObjPackageBody.loadStdVersionFromResources(h, false);
            DbObjTrigger.loadStdVersionFromResources(h, false);
            DbObjSlownik.loadStdVersionFromResources(h, false);
            DbObjListaWartosci.loadStdVersionFromResources(h, false);
        } catch (Exception ex) {
            log.error(ex);
        }

    }
    
    
    public void loadResourceFiles2pptObjectsActiveTab(){
    	
      try (HibernateContext h = new HibernateContext()) {
    	  switch (activeTab) {
    	  	case "PROCEDURE":
    	  		DbObjProcedure.loadStdVersionFromResources(h, false);
    	  		break;
    	  	case "FUNCTION":
    	  		DbObjFunction.loadStdVersionFromResources(h, false);
    	  		break;
    	  	case "VIEW":
    	  		DbObjView.loadStdVersionFromResources(h, false);
    	  		break;
    	  	case "PACKAGE":
    	  		DbObjPackage.loadStdVersionFromResources(h, false);
    	  		break;
    	  	case "PACKAGE BODY":
    	  		DbObjPackageBody.loadStdVersionFromResources(h, false);
    	  		break;
    	  	case "TRIGGER":
    	  		DbObjTrigger.loadStdVersionFromResources(h, false);
    	  		break;
    	  }
    	  
      } catch (Exception ex) {
          log.error(ex);
      }

  }


    public void updateAutoAkt(boolean aa) {
        SqlDataSelectionsHandler lw = getPplAdmDbObjects();
        if (lw == null || lw.getSelectedRows().isEmpty()) return;
        try (Connection conn = JdbcUtils.getConnection()) {
            for (DataRow r : lw.getSelectedRows()) {

                String owner = r.getAsString("objp_owner");
                String objectName = nz(r.getAsString("objp_name"));
                String objectType = r.getAsString("objp_object_type");

                boolean b = DbObject.updateAktualizacjeAutomatyczne(conn, owner, objectType, objectName, aa);
                if (!b){
                    User.warn("Nie ustawiono flagi AA dla %1$s. Sprawdź czy taki wpis jest w PPT_OBJECTS.", objectName);
                }
            }
        } catch (Exception e) {
            User.alert(e);
        }
        lw.resetTs();
    }


}