package com.comarch.egeria.pp.Administrator;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import org.springframework.context.annotation.Scope;

import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

@Named
@Scope("view")
public class ListaTranslacjiBean extends SqlBean {

	@Inject
	User user;

	HashMap listaList = new HashMap<>();

	public void editSelectedRow() throws SQLException {
		SqlDataSelectionsHandler p = this.getSql("ppl_adm_translacje");
		if (p != null) {
			System.out.println(p.getName() + " / currentRow: " + p.getCurrentRow());

			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", p.getCurrentRow());

			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id",
					p.getCurrentRow().getIdAsLong());

			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("DaneTranslacji");
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	public void dodajTranslacje() {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", null);
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("DaneTranslacji");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
