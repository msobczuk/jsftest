package com.comarch.egeria.pp.CzasPracy;

import java.io.Serializable;

import java.util.Date;

import javax.faces.bean.ManagedBean;

import javax.inject.Named;

import org.springframework.context.annotation.Scope;


import com.comarch.egeria.pp.data.SqlBean;

@ManagedBean
@Scope("view")
@Named
public class CzasPracyBean extends SqlBean implements Serializable {


	
	
	Date minDate;
	
	private Date selectedDate = new Date();
	

    public Date getSelectedDate() {
        return selectedDate;
    }
 
    public void setSelectedDate(Date date) {
        this.selectedDate = date;
    }
	

}
