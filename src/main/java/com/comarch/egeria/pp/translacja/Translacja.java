package com.comarch.egeria.pp.translacja;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.primefaces.component.outputlabel.OutputLabel;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.translacja.model.PptAdmTranslacje;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@ManagedBean
@Named
@Scope("view")
public class Translacja extends SqlBean {
	private static final Logger log = LogManager.getLogger();

	@Inject
	User user;
	String viewId = "-";

	String polski = "";
	String angielski = "";
	String identyfikator = "";

	@PostConstruct
	public void init() {

		FacesContext ctx = FacesContext.getCurrentInstance();
		this.viewId = ctx.getViewRoot().getViewId(); // /faces/DelegacjeMF/DelegacjaMFZagr.xhtml

		try (HibernateContext h = new HibernateContext()) {

			this.loadSqlData("sqlTranslacje", "select tra_id from PPT_ADM_TRANSLACJE where tra_lang = \'PL\'", new ArrayList<>());
			SqlDataSelectionsHandler ssh = this.getSql("sqlTranslacje");
			ArrayList<DataRow> data = ssh.getData();

			Session s = h.getSession();

			if (!Translacja.etykietyJezyki.containsKey("PL")) {
				Translacja.etykietyJezyki.put("PL", new HashMap<>());
			}

			for (DataRow dr : data) {
				PptAdmTranslacje pt = (PptAdmTranslacje) s.get(PptAdmTranslacje.class, dr.getIdAsLong());
				Translacja.etykietyJezyki.get("PL").put(pt.getTraPl(), pt.getTraTlum());
				// AppBean.etykiety.put(pt.getTraPl(), pt.getTraEn());
			}

			s.close();
		}

	}

	ArrayList<UIComponent> translateComponents = new ArrayList<>();
	ArrayList<UIComponent> translateLabels = new ArrayList<>();

	public static HashMap etykiety = new HashMap<>();

	public static HashMap<String, HashMap> etykietyJezyki = new HashMap<>();

	public void zmienJezyk() {
		try (HibernateContext h = new HibernateContext()) {
			
			Session s = h.getSession();

			// TODO na sztywno angielski
			if (AppBean.language.equals("PL")) {

				if (Translacja.etykietyJezyki.get("EN") == null) {
					Translacja.etykietyJezyki.put("EN", new HashMap<>());
				}

				AppBean.language = "EN";
				
				ArrayList param = new ArrayList<>();
				param.add(AppBean.language);
				this.loadSqlData("sqlTranslacje", "select tra_id from PPT_ADM_TRANSLACJE where tra_lang = ?", param);
				SqlDataSelectionsHandler ssh = this.getSql("sqlTranslacje");
				ArrayList<DataRow> data = ssh.getData();
				
				for (DataRow dr : data) {
					PptAdmTranslacje pt = (PptAdmTranslacje) s.get(PptAdmTranslacje.class, dr.getIdAsLong());
					Translacja.etykietyJezyki.get("EN").put(pt.getTraPl(), pt.getTraTlum());
					// AppBean.etykiety.put(pt.getTraPl(), pt.getTraEn());
				}

			} else {

				if (AppBean.language.equals("EN")) {

					AppBean.language = "PL";
					
					if (Translacja.etykietyJezyki.get("PL") == null) {
						Translacja.etykietyJezyki.put("PL", new HashMap<>());
					}

					ArrayList param = new ArrayList<>();
					param.add(AppBean.language);
					this.loadSqlData("sqlTranslacje", "select tra_id from PPT_ADM_TRANSLACJE where tra_lang = ?", param);
					SqlDataSelectionsHandler ssh = this.getSql("sqlTranslacje");
					ArrayList<DataRow> data = ssh.getData();
					
					for (DataRow dr : data) {
						PptAdmTranslacje pt = (PptAdmTranslacje) s.get(PptAdmTranslacje.class, dr.getIdAsLong());
						Translacja.etykietyJezyki.get("PL").put(pt.getTraPl(), pt.getTraPl());
						// AppBean.etykiety.put(pt.getTraPl(), pt.getTraEn());
					}
				}
			}
			s.close();
		}

	}

	public void iterateLabels(UIComponent uic) throws IOException {
		FacesContext ctx = FacesContext.getCurrentInstance();
		if (uic == null)
			uic = ctx.getViewRoot();

		if (uic instanceof OutputLabel) {

//			System.out.println(uic.getClientId() + " id: " + uic.getId() + "   outputLabel.value:"
//					+ ((OutputLabel) uic).getValue());

			if (!translateLabels.contains(uic)) {
				translateLabels.add(uic);
			}
		}

		List<UIComponent> uicSet = getFacetsAndChildrenSet(uic);
		for (UIComponent c : uicSet) {
			iterateLabels(c);
		}
	}

	public void iterateChildren(UIComponent uic) throws IOException {
		FacesContext ctx = FacesContext.getCurrentInstance();

		if (uic == null)
			uic = ctx.getViewRoot();

		if (uic instanceof OutputLabel) {

			System.out.println(uic.getClientId() + " id: " + uic.getId() + "   outputLabel.value:"
					+ ((OutputLabel) uic).getValue());

		}

		if (uic instanceof OutputLabel) {
			if (!translateLabels.contains(uic)) {
				translateLabels.add(uic);
			}
		}

		List<UIComponent> uicSet = getFacetsAndChildrenSet(uic);

		for (UIComponent c : uicSet) {
			iterateChildren(c);
		}
	}

	public void iterateChildrenByID(String id) throws IOException {
		UIComponent uid = FacesContext.getCurrentInstance().getViewRoot().findComponent(id);
		translateLabels = new ArrayList<>();
		if (uid != null) {
			iterateChildren(uid);
		} else
			System.out.println("iterateChildren: nie znaleziobno elementu wg id: " + id);

	}

	public void showTraceKuComponents() {

		for (UIComponent uic : translateComponents) {
			log.trace(uic.getClientId() + " : " + uic.getId() + " ///////////////////////// "
					+ uic.getAttributes().get("kuid") + "  //////////////////////// " + uic.toString());
		}

	}

//	// TODO - rekurencja przegladania i blokowania kontrolek w glab...
//	// ie:
//	// https://victorjabur.com/2011/01/10/how-to-set-recursively-component-properties-using-java-server-faces-jsf/
//	/**
//	 * @param uic
//	 * @throws IOException
//	 */
//	public void iterateChildren(String kuid, UIComponent uic, Boolean readOnly) throws IOException {
//
//		FacesContext ctx = FacesContext.getCurrentInstance();
//
//		if (uic == null)
//			uic = UIComponent.getCurrentComponent(ctx);
//
//		if (uic instanceof OutputLabel) {
//
//			System.out.println("   outputLabel.value:" + ((OutputLabel) uic).getValue());
//		}
//		HashSet<UIComponent> uicSet = getFacetsAndChildrenSet(uic);
//
//		for (UIComponent c : uicSet) {
//
//			iterateChildren(kuid, c, readOnly);
//		}
//	}

	// public void testTranslacji() {
	// for(UIComponent ui : translateLabels) {
	//
	// if(ui instanceof OutputLabel) {
	//
	// OutputLabel lbl = (OutputLabel) ui;
	// System.out.println( " outputLabel.value:" + ((OutputLabel)ui).getValue()
	// );
	//
	// if(AppBean.etykiety.containsKey(lbl.getValue())) {
	// lbl.setValue(AppBean.etykiety.get(lbl.getValue()));
	// }
	//
	//
	//// //idDwdOsob:j_idt132
	//// if (lbl.getId().contains("j_idt132") ||
	// lbl.getClientId(ctx).contains("j_idt132") ){
	//// System.out.println();
	//// }
	//
	// }
	// }
	// }

}