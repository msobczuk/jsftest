package com.comarch.egeria.pp.translacja.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PPT_TRANSLACJE database table.
 * 
 */
@Entity
@Table(name="PPT_ADM_TRANSLACJE")
@NamedQuery(name="PptAdmTranslacje.findAll", query="SELECT p FROM PptAdmTranslacje p")
public class PptAdmTranslacje implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_PPT_ADM_TRANSLACJE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ADM_TRANSLACJE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_PPT_ADM_TRANSLACJE")
	@Column(name="TRA_ID")
	private long traId;

	@Temporal(TemporalType.DATE)
	@Column(name="TRA_AUDYT_DM")
	private Date traAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="TRA_AUDYT_DT")
	private Date traAudytDt;

	@Column(name="TRA_AUDYT_KM")
	private String traAudytKm;

	@Column(name="TRA_AUDYT_LM")
	private String traAudytLm;

	@Column(name="TRA_AUDYT_UM")
	private String traAudytUm;

	@Column(name="TRA_AUDYT_UT")
	private String traAudytUt;

	@Column(name="TRA_LANG")
	private String traLang;

	@Column(name="TRA_PL")
	private String traPl;

	@Column(name="TRA_TLUM")
	private String traTlum;

	public PptAdmTranslacje() {
	}

	public long getTraId() {
		return this.traId;
	}

	public void setTraId(long traId) {
		this.traId = traId;
	}

	public Date getTraAudytDm() {
		return this.traAudytDm;
	}

	public void setTraAudytDm(Date traAudytDm) {
		this.traAudytDm = traAudytDm;
	}

	public Date getTraAudytDt() {
		return this.traAudytDt;
	}

	public void setTraAudytDt(Date traAudytDt) {
		this.traAudytDt = traAudytDt;
	}

	public String getTraAudytKm() {
		return this.traAudytKm;
	}

	public void setTraAudytKm(String traAudytKm) {
		this.traAudytKm = traAudytKm;
	}

	public String getTraAudytLm() {
		return this.traAudytLm;
	}

	public void setTraAudytLm(String traAudytLm) {
		this.traAudytLm = traAudytLm;
	}

	public String getTraAudytUm() {
		return this.traAudytUm;
	}

	public void setTraAudytUm(String traAudytUm) {
		this.traAudytUm = traAudytUm;
	}

	public String getTraAudytUt() {
		return this.traAudytUt;
	}

	public void setTraAudytUt(String traAudytUt) {
		this.traAudytUt = traAudytUt;
	}

	public String getTraLang() {
		return this.traLang;
	}

	public void setTraLang(String traLang) {
		this.traLang = traLang;
	}

	public String getTraPl() {
		return this.traPl;
	}

	public void setTraPl(String traPl) {
		this.traPl = traPl;
	}

	public String getTraTlum() {
		return this.traTlum;
	}

	public void setTraTlum(String traTlum) {
		this.traTlum = traTlum;
	}

}