package com.comarch.egeria.pp.iprz.model;

import java.io.Serializable;
import javax.persistence.*;



/**
 * The persistent class for the PPT_IPR_LUKI_KOMPETENCYJNE database table.
 * 
 */
@Entity
@Table(name="PPT_IPR_LUKI_KOMPETENCYJNE")
@NamedQuery(name="PptIprLukiKompetencyjne.findAll", query="SELECT p FROM PptIprLukiKompetencyjne p")
public class PptIprLukiKompetencyjne implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_IPR_LUKI_KOMPETENCYJNE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_IPR_LUKI_KOMPETENCYJNE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_IPR_LUKI_KOMPETENCYJNE")		
	@Column(name="IPLK_ID")
	private long iplkId;

	@Column(name="IPLK_OPIS")
	private String iplkOpis;

	@Column(name="IPLK_WARTOSC")
	private String iplkWartosc;

	//bi-directional many-to-one association to PptIprIprz
	@ManyToOne
	@JoinColumn(name="IPLK_IPRZ_ID")
	private ZptIprz pptIprIprz;

	public PptIprLukiKompetencyjne() {
	}

	public long getIplkId() {
		return this.iplkId;
	}

	public void setIplkId(Long iplkId) {
		this.iplkId = iplkId;
	}

	public String getIplkOpis() {
		return this.iplkOpis;
	}

	public void setIplkOpis(String iplkOpis) {
		this.iplkOpis = iplkOpis;
	}

	public String getIplkWartosc() {
		return this.iplkWartosc;
	}

	public void setIplkWartosc(String iplkWartosc) {
		this.iplkWartosc = iplkWartosc;
	}

	public ZptIprz getPptIprIprz() {
		return this.pptIprIprz;
	}

	public void setPptIprIprz(ZptIprz pptIprIprz) {
		this.pptIprIprz = pptIprIprz;
	}

}