package com.comarch.egeria.pp.iprz;

import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.iprz.model.PptIprLukiKompetencyjne;
import com.comarch.egeria.pp.iprz.model.ZptIprz;
import com.comarch.egeria.pp.raporty.GeneratorRaportow;
import com.comarch.egeria.pp.raporty.RaportyBean;
import com.comarch.egeria.pp.szkolenia.model.OstPlanIndyw;
import com.comarch.egeria.pp.szkolenia.model.PptSzkSzkolenia;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import net.sf.jasperreports.engine.JRException;
import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.rowset.CachedRowSet;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.nz;
import static com.comarch.egeria.Utils.trunc;

//import org.primefaces.context.RequestContext;

@ManagedBean
@Named
@Scope("view")
public class ArkuszIPRZBean extends SqlBean {
	private static final long serialVersionUID = 1L;

	@Inject
	User user;

	@Inject
	SessionBean sessionBean;

	@Inject
	GeneratorRaportow genrap;

	@Inject
	RaportyBean raportyBean;

	@Inject
	ObiegBean obieg;

//	@Inject
//	protected ReportGenerator ReportGenerator;

	HashSet<String> updateAfterSave = new HashSet<String>();

	private Long selectedArkuszId = null;

	private DataRow arkuszDR = null;

	private ZptIprz arkusz = new ZptIprz();

	private long prcId;

	private String obszarRozwojowy = "";
	private List<Object> dzialaniaRozwojowe = new ArrayList<>();

	private String szkolenieObszarWiedzy = "";

	private List<Object> formyOrganizacji = new ArrayList<>();

	private String formaProwadzenia = "";
	private String tematSzkolenia = "";

	private boolean czyWyswietlacDlg = false;
	
	OstPlanIndyw currentPlan = null;

	private OstPlanIndyw currentPlanTmp = null;

	//String checkString = "";

	@PostConstruct
	public void init() {

		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		obieg.setKategoriaObiegu("WN_IPRZ_ARKUSZ");

		if (id != null) {
			arkuszDR = (DataRow) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("row");
		}

		if (arkuszDR == null) {
			this.arkusz = new ZptIprz();
			this.arkusz.setOstPlanIndyws(new ArrayList<>());
			return;
		}

		wczytajArkusz(id);

	}

	public String addUpdateAfterSave(String uiId) {
		this.updateAfterSave.add(uiId);
		return null;
	}

	public String zaznaczWczytaneSzkolenia() {
		HashSet<String> szkoleniaKeySet = this.getSzkoleniaKeySet();
		SqlDataSelectionsHandler dodatkoweRows = getSql("OS_SZKOLENIA_POWSZECHNE");

		dodatkoweRows.clearSelection();
		for (DataRow dr : dodatkoweRows.getData()) {
			if (szkoleniaKeySet.contains(dr.get(0))) {
				dodatkoweRows.setRowSelection(true, dr);
			}
		}
		return "";
	}

	private HashSet<String> getSzkoleniaKeySet() {
		HashSet<String> szkoleniaKeySet = new HashSet<String>();
		if (arkusz != null && arkusz.getOstPlanIndyws() != null)
			for (OstPlanIndyw k : arkusz.getOstPlanIndyws()) {
				szkoleniaKeySet.add(k.getPisSzkPowszech());
			}
		return szkoleniaKeySet;
	}

	public void setCurrentSzkolSpec(OstPlanIndyw currentSzkolSpec) throws CloneNotSupportedException {
		this.currentPlan = currentSzkolSpec;
		this.currentPlanTmp = currentSzkolSpec.clone();
	}
	
//	


	
	public void addNewSzkol() {
		this.currentPlan = null;
		this.currentPlanTmp = new OstPlanIndyw();
		this.currentPlanTmp.setPisPrcId(arkusz.getIprzPrcId());
		
	}

	public void addNewSzkolSpec() {
		OstPlanIndyw szkolSpec = new OstPlanIndyw();
		szkolSpec.setPisPrcId(arkusz.getIprzPrcId());
		arkusz.addOstPlanIndyw(szkolSpec);
	}

	public void addSzkolenieNiecywilne() {
		OstPlanIndyw szkol = new OstPlanIndyw();
		szkol.setPisPrcId(arkusz.getIprzPrcId());
		szkol.setPisFCzyNieSc("T");
		arkusz.addOstPlanIndyw(szkol);
	}

	public boolean containsTemat(final List<OstPlanIndyw> list, final String temat) {
		return list.stream().filter(o -> o.getPisTemat() != null && o.getPisTemat().equals(temat)).findFirst()
				.isPresent();
	}

	public boolean containsForma(final List<OstPlanIndyw> list, final String forma) {
		return list.stream().filter(o -> o.getPisSspeForma() != null && o.getPisTemat().equals(forma)).findFirst()
				.isPresent();
	}

	public boolean containsObszar(final List<OstPlanIndyw> list, final String obszar) {
		return list.stream()
				.filter(o -> o.getPisSspeObszarWiedzy() != null && o.getPisSspeObszarWiedzy().equals(obszar))
				.findFirst().isPresent();
	}
	public void onCurrentTmpPisOfIdChanged() {
		if (this.currentPlanTmp == null)
			return;

		DataRow dr = getLW("ppl_szk_oferty_szkolen").get(currentPlanTmp.getPisOfId());
		currentPlanTmp.setPisTemat((String) dr.get("of_nazwa"));
		currentPlanTmp.setPisTerminDo((Date) dr.get("szk_data_do"));
		currentPlanTmp.setPisTerminOd((Date) dr.get("szk_data_od"));
		currentPlanTmp.setPisMiejsceSzkol((String) dr.get("szk_lokalizacja"));
		currentPlanTmp.setPisSspeObszarWiedzy((String) dr.get("OF_RODZAJ"));
		currentPlanTmp.setPisSspeForma((String) dr.get("of_forma_org"));
		currentPlanTmp.setPisSzkPowszech((String) dr.get("of_czy_powszechne"));

		BigDecimal bdSzkId = (BigDecimal) dr.get("szk_id");
		if (bdSzkId != null) {
			this.currentPlanTmp.setPptSzkSzkolenia(
					(PptSzkSzkolenia) HibernateContext.get(PptSzkSzkolenia.class, bdSzkId.longValue()));
		} else {
			this.currentPlanTmp.setPptSzkSzkolenia(null);
		}
	}

	public void zapisz() {
		System.out.println("ArkuszIPRZBean.zapisz() ... ");

		if (!validateBeforeSaveArkusz())
			return;

		SqlDataSelectionsHandler szkoleniaAllRows = this.getSql("OS_SZKOLENIA_POWSZECHNE");
		if (szkoleniaAllRows != null) {
		}

		try {
			this.arkusz = (ZptIprz) HibernateContext.save(this.arkusz);
			User.info("Zapisano arkusz IPRZ");
		} catch (Exception e) {
			User.alert("Błąd w zapisie arkusza!\n" + e);
			System.err.println(e);
		}
		sessionBean.setViewEntityId(arkusz.getIprzId());// obsluga odsw.. F5

		wczytajArkusz(arkusz.getIprzId());

		for (String uiId : this.updateAfterSave)
			com.comarch.egeria.Utils.update(uiId);
	}

	private void wczytajArkusz(Object id) {

		setSelectedArkuszId(Long.parseLong("" + id));

		obieg.setIdEncji(this.getSelectedArkuszId());

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			this.setArkusz(s.get(ZptIprz.class, getSelectedArkuszId()));
		}

//		checkString = getCheckString();
//		System.out.println("CheckString: " + checkString);
	}

	public void raport() {

		if (arkusz != null && arkusz.getIprzId() > 0) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("IPRZ_ID", arkusz.getIprzId());
			try {
				ReportGenerator.displayReportAsPDF(params, "IPRZ");

			} catch (JRException | SQLException | IOException e) {
				e.printStackTrace();
				User.alert(e.getMessage());
			}
		}
	}

	public void usun() throws IOException {

		if (!validateAuthBeforeDelete())
			return;

		if (!validateBeforeDelete())
			return;

		try (Connection conn = JdbcUtils.getConnection(false)) {
			obieg.usunDaneObiegu();
		} catch (SQLException e) {
			e.printStackTrace();
			User.alert("Nieudane usuniecie danych obiegu");
		}

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			s.delete(this.arkusz);
			s.beginTransaction().commit();
		}

		FacesContext.getCurrentInstance().getExternalContext().redirect("ListaArkuszyIPRZ");

	}

	private Boolean validateAuthBeforeDelete() {
		ArrayList<String> inval = new ArrayList<String>();

		if (!user.hasPermission("PP_OCE_ADMINISTRATOR"))
			inval.add("Brak uprawnień do kasowania tych danych.");

		for (String msg : inval) {
			User.alert(msg);
		}
		return inval.isEmpty();
	}

	private Boolean validateBeforeDelete() {
		ArrayList<String> inval = new ArrayList<String>();

		if (this.arkusz.getIprzId() == 0)
			inval.add("Dane nie są jeszcze zapisane w bazie.");

		for (String msg : inval) {
			User.alert(msg);
		}
		return inval.isEmpty();
	}

	private Boolean validateBeforeSaveArkusz() {
		ArrayList<String> inval = new ArrayList<String>();
		
		

		try (Connection conn = JdbcUtils.getConnection()) {
			List<Object> params = new ArrayList<>();

			params.add(arkusz.getIprzPrcId());
			CachedRowSet crs = JdbcUtils.sqlExecQuery(conn,
					"select usz_id, prc_imie, prc_nazwisko from PPT_SZK_UCZESTNICY_SZKOLEN "
							+ "JOIN ek_pracownicy on prc_id = usz_prc_id "
							+ "where usz_prc_id = ? and USZ_DATA_WYPELNIENIA is null",
					params);

			if (crs != null) {
				if (crs.next()) {
					User.warn("Pracownik " + crs.getString("prc_imie") + " " + crs.getString("prc_nazwisko")
							+ " posiada niewypełnione ankiety szkoleniowe.");
				}
			}
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			e.printStackTrace();
			inval.add(e.getMessage());
		}

//		String currentCheckString = this.getCheckString();
//		System.out.println("CheckString przy walidacji: " + checkString);
		
//		List<Long> listaSzkolen = arkusz.getSzkoleniaNiebedaceSzkoleniamiWSluzbieCywilnej().stream().map(n -> n.getPisOfId()).collect(Collectors.toList());
//		HashSet<Long> listaSzkolenHS = new HashSet<Long>(listaSzkolen);	
//		if(listaSzkolenHS.size() < listaSzkolen.size()) {
//			inval.add("Powtarzająca się nazwa szkolenia");
//		}
		inval.addAll(arkusz.validationOstPlanIndywsErrors());
		

//		if (this.checkString.equals(currentCheckString))
//			inval.add("Brak zmian do zapisania.");

		if (arkusz.getIprzPrcId() == null || arkusz.getIprzPrcId() == 0L) {
			inval.add("Pracownik jest polem wymaganym.");
		}

		if (arkusz.getIprzSciezka() == null || arkusz.getIprzSciezka().trim().equals("")) {
			inval.add("Ścieżka kariery jest polem wymaganym.");
		}

		if (arkusz.getIprzProgram() == null || arkusz.getIprzProgram().trim().equals("")) {
			inval.add("Program rozwojowy jest polem wymaganym.");
		}

		if (arkusz.getIprzTalenty() == null || arkusz.getIprzTalenty().trim().equals("")) {
			inval.add("Program zarządzania talentami jest polem wymaganym.");
		}

		Date truncIprzAudytDt = trunc(arkusz.getIprzAudytDt());
		Date truncIprzDataObowiazywaniaOd = trunc(arkusz.getIprzDataObowiazywaniaOd());
		Date truncIprzDataObowiazywaniaDo = trunc(arkusz.getIprzDataObowiazywaniaDo());

		if (truncIprzDataObowiazywaniaOd != null && truncIprzDataObowiazywaniaDo != null) {

			if (truncIprzDataObowiazywaniaDo.before(truncIprzDataObowiazywaniaOd)) {
				inval.add("Data końca obowiązywania nie może być wcześniejsza od daty początku");
			}

			if (!truncIprzDataObowiazywaniaOd.equals(truncIprzDataObowiazywaniaDo)
					&& truncIprzDataObowiazywaniaOd.after(truncIprzDataObowiazywaniaDo)) {
				inval.add("Data początku obowiązywania nie może być wcześniejsza od daty końca");
			}

			if (truncIprzAudytDt != null) {

				if (truncIprzAudytDt.after(truncIprzDataObowiazywaniaOd)) {
					inval.add("Data początku obowiązywania nie może być wcześniejsza od daty utworzenia arkusza");
				}

				if (truncIprzAudytDt.after(truncIprzDataObowiazywaniaDo)) {
					inval.add("Data końca obowiązywania nie może być wcześniejsza od daty utworzenia arkusza");
				}

			} else {
				if (!truncIprzDataObowiazywaniaOd.equals(today()) && truncIprzDataObowiazywaniaOd.before(today())) {
					inval.add("Data początku obowiązywania nie może być wcześniejsza od daty dzisiejszej");
				}

			}
		}
		if (arkusz.getSzkoleniaSpecjalistyczne() != null || !arkusz.getSzkoleniaSpecjalistyczne().isEmpty()) {
			for (Object oopi : arkusz.getSzkoleniaSpecjalistyczne()) {
				OstPlanIndyw opi = (OstPlanIndyw) oopi;
				if (	   nz(opi.getPisTemat()).equals("")
						|| nz(opi.getPisSspeForma()).equals("") 
						|| nz(opi.getPisSspeObszarWiedzy()).equals("")) {
					inval.add("W szkoleniach należy podac wymagane informacje (temat, forma, obszar wiedzy).");
				}
			}
		}

		User.alert(inval);
//		for (String msg : inval) {
//			User.alert(msg);
//		}
		return inval.isEmpty();
	}

	public void usunArkusz() {

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			s.delete(this.arkusz); // << usuniecie rekordu
			s.beginTransaction().commit();
			User.info("Usunięto arkusz IPRZ");
		} catch (Exception e) {
			User.alert("Błąd w usuwaniu arkusza!\n" + e);
			System.err.println(e);
		}

	}
	public void zatwierdzCurrentTmp() {
		if (currentPlanTmp == null)
			return;
		
		if (validatePlatneSzkolenia()) {
			if (this.currentPlan == null) {
				this.arkusz.addOstPlanIndyw(this.currentPlanTmp);
			} else {
				int indexOf = this.arkusz.getOstPlanIndyws().indexOf(this.currentPlan);
				this.arkusz.getOstPlanIndyws().set(indexOf, this.currentPlanTmp);
			}
			this.currentPlan = this.currentPlanTmp;
		}
	}
	
	private Boolean validatePlatneSzkolenia() {
		ArrayList<String> inval = new ArrayList<String>();

		SqlDataSelectionsHandler szkolenia = this.getLW("PPL_SZK_OFERTY_SZKOLEN");
//		DataRow wybraneSzkolenie = szkolenia.getCurrentRow();
//		if (!("T".equals(wybraneSzkolenie.get("of_f_platne")))) 
//			return true; nie wolno zakładać że użytkownik wybrał szkolenie z rejetru... jesli od razu wybral spoza rejestru bedzie nullpointer, albo zwracamy info dot. poprzedniego lub przypadkowego wyboru na LW przed kliknięciem w Szkolenie spoza rejestru...

//		List<OstPlanIndyw> listaPlatnych = this.arkusz.getOstPlanIndyws().stream()
//				.filter(o -> o.getPisOfId()!=null 
//							&& szkolenia.find(o.getPisOfId())!=null 
//							&& "T".equals(szkolenia.find(o.getPisOfId()).get("of_f_platne")))
//				.collect(Collectors.toList());
		
		if (this.currentPlanTmp.getPisOfId() != null
			&& szkolenia.find(this.currentPlanTmp.getPisOfId()) != null
			&& !("T".equals(szkolenia.find(this.currentPlanTmp.getPisOfId()).get("of_f_platne"))))
			return true;
		
		List<OstPlanIndyw> listaPlatnych = this.arkusz.getOstPlanIndyws().stream()
				.filter(o -> (o.getPisOfId() != null 
							&& szkolenia.find(o.getPisOfId()) != null 
							&& "T".equals(szkolenia.find(o.getPisOfId()).get("of_f_platne")))
							|| (o.getPisOfId() == null && o.getPisFCzyRejestr() == "T") )
				.collect(Collectors.toList());
		
		if (listaPlatnych.isEmpty())
			return true;
		
		if (listaPlatnych.size() >= 3)
			inval.add("Można wprowadzić tylko 3 szkolenia płatne. "
					+ "Za płatne szkolenia uznawane są te oznaczone jako płatne oraz wszystkie szkolenia spoza rejestru.");

		for (String msg : inval) {
			User.alert(msg);
		}
		return inval.isEmpty();
	}

	public String getCheckString() {
		String ret = "";

		if (arkusz != null) {
			ret = ret + AppBean.concatValues(arkusz.getIprzId(), arkusz.getIprzPrcId(), arkusz.getIprzProgram(),
					arkusz.getIprzSciezka(), arkusz.getIprzDataObowiazywaniaOd(), arkusz.getIprzDataObowiazywaniaDo(),
					arkusz.getIprzOpis1(), arkusz.getIprzOpis2(), arkusz.getIprzPodsumowanie(),
					arkusz.getIprzTalenty());

			if (arkusz.getOstPlanIndyws() != null) {
				for (OstPlanIndyw opi : arkusz.getOstPlanIndyws()) {
					ret = ret + AppBean.concatValues(opi.getPisId(), opi.getOstZapisySzkolen(), opi.getPisDataZapisu(),
							opi.getPisMiejsceSzkol(), opi.getPisOrganizator(), opi.getPisPrcId(), opi.getPisRok(),
							opi.getPisSspeForma(), opi.getPisSspeObszarWiedzy(), opi.getPisSzkPowszech(),
							opi.getPisTemat(), opi.getPisTerminOd(), opi.getPisTerminDo(), opi.getPisUzasadnienie());
				}
			}
			if (arkusz.getPptIprLukiKompetencyjnes() != null) {
				for (PptIprLukiKompetencyjne luk : arkusz.getPptIprLukiKompetencyjnes()) {
					ret += AppBean.concatValues(luk.getIplkId(), luk.getIplkWartosc());
				}
			}
		}

		return ret;
	}

	public void addLuka() {
		PptIprLukiKompetencyjne luka = new PptIprLukiKompetencyjne();
		arkusz.addPptIprLukiKompetencyjne(luka);
	}

	public DataRow getArkuszDR() {
		return arkuszDR;
	}

	public void setArkuszDR(DataRow arkuszDR) {
		this.arkuszDR = arkuszDR;
	}

	public ZptIprz getArkusz() {
		return arkusz;
	}

	public void setArkusz(ZptIprz arkusz) {
		this.arkusz = arkusz;
	}

	public long getPrcId() {
		return prcId;
	}

	public void setPrcId(long prcId) {
		this.prcId = prcId;
	}

	public String getObszarRozwojowy() {
		return obszarRozwojowy;
	}

	public void setObszarRozwojowy(String obszarRozwojowy) {
		this.obszarRozwojowy = obszarRozwojowy;
	}

	public List<Object> getDzialaniaRozwojowe() {
		return dzialaniaRozwojowe;
	}

	public void setDzialaniaRozwojowe(List<Object> dzialaniaRozwojowe) {
		this.dzialaniaRozwojowe = dzialaniaRozwojowe;
	}

	public List<Object> getFormyOrganizacji() {
		if (formyOrganizacji.isEmpty()) {
			SqlDataSelectionsHandler ssh = this.getSql("OS_FORMA_ORGANIZACJI");
			for (DataRow dr : ssh.getData())
				formyOrganizacji.add(dr.get(1));
		}
		return formyOrganizacji;
	}

	public void setFormyOrganizacji(List<Object> formyOrganizacji) {
		this.formyOrganizacji = formyOrganizacji;
	}

	public boolean isCzyWyswietlacDlg() {
		return czyWyswietlacDlg;
	}

	public void setCzyWyswietlacDlg(boolean czyWyswietlacDlg) {
		this.czyWyswietlacDlg = czyWyswietlacDlg;
	}

	public String getSzkolenieObszarWiedzy() {
		return szkolenieObszarWiedzy;
	}

	public void setSzkolenieObszarWiedzy(String szkolenieObszarWiedzy) {
		this.szkolenieObszarWiedzy = szkolenieObszarWiedzy;
	}

	public String getFormaProwadzenia() {
		return formaProwadzenia;
	}

	public void setFormaProwadzenia(String formaProwadzenia) {
		this.formaProwadzenia = formaProwadzenia;
	}

	public String getTematSzkolenia() {
		return tematSzkolenia;
	}

	public void setTematSzkolenia(String tematSzkolenia) {
		this.tematSzkolenia = tematSzkolenia;
	}

	public boolean isCzyNowy() {
		return this.arkusz.getIprzId() == 0;
	}

	public Long getSelectedArkuszId() {
		return selectedArkuszId;
	}

	public void setSelectedArkuszId(Long selectedArkuszId) {
		this.selectedArkuszId = selectedArkuszId;
	}
	public OstPlanIndyw getCurrentPlan() {
		return currentPlan;
	}

	public OstPlanIndyw getCurrentPlanTmp() {
		return currentPlanTmp;
	}

	public void setCurrentPlanTmp(OstPlanIndyw currentPlanTmp) {
		this.currentPlanTmp = currentPlanTmp;
	}

	public void setCurrentPlan(OstPlanIndyw currentPlan) {
		this.currentPlan = currentPlan;
	}
	public int getAccIndex() {
		try {

			if (this.currentPlan==null
				|| "T".equals( this.currentPlan.getPisFCzyRejestr() ) 
				|| this.currentPlan.getPisId()==0 )
				return 0;
			else
				return 1;
			
		} catch (Exception e) {
			throw e;
		}
	}

	public void setAccIndex(int accIndex) {
		//nic
	}


}



//private void dodajUsunOcene(List<DataRow> szkoleniaRows) {
//	if (szkoleniaRows != null) {
//		HashSet<String> szkoleniaKeySet = getSzkoleniaKeySet();
//		HashSet<String> szkoleniaRowsKeySet = new HashSet<String>();
//
//		for (DataRow dr : szkoleniaRows) {
//			String szkolenie = "" + dr.get(0);
//			szkoleniaRowsKeySet.add(szkolenie);
//			if (!szkoleniaKeySet.contains(szkolenie)) {
//				addSzkolenieNowe(szkolenie);
//			}
//		}
//
//		for (OstPlanIndyw opi : arkusz.getOstPlanIndyws()) {
//			if (opi.getPisTemat() != null) {
//				szkoleniaRowsKeySet.add(opi.getPisTemat());
//			}
//		}
//
//		ArrayList<OstPlanIndyw> toRemove = new ArrayList<OstPlanIndyw>();
//
//		if (this.arkusz.getOstPlanIndyws() != null)
//			for (OstPlanIndyw k : this.arkusz.getOstPlanIndyws()) {
//				if (k.getPisSzkPowszech() != null) {
//					if (!szkoleniaRowsKeySet.contains(k.getPisSzkPowszech())) {
//						toRemove.add(k);
//					}
//				} else if (k.getPisTemat() != null) {
//					if (!szkoleniaRowsKeySet.contains(k.getPisTemat())) {
//						toRemove.add(k);
//					}
//				}
//			}
//
//		toRemove.forEach(k -> {
//			this.arkusz.removeOstPlanIndyw(k);
//		});
//
//		try (HibernateContext h = new HibernateContext()) {
//			Session s = h.getSession();
//			for (OstPlanIndyw temp : toRemove) {
//				s.delete(temp);
//				s.beginTransaction().commit();
//			}
//		} catch (Exception e) {
//			User.alert("Błąd w usuwaniu szkoleń!\n" + e);
//			System.err.println(e);
//		}
//
//		System.out.println(this.arkusz.getOstPlanIndyws());
//	}
//}

//private void addSzkolenieNowe(String szkolenie) {
//OstPlanIndyw k = new OstPlanIndyw();
//k.setPisPrcId(arkusz.getIprzPrcId());
//
//k.setPisSzkPowszech(szkolenie);
//
//arkusz.addOstPlanIndyw(k);
//}