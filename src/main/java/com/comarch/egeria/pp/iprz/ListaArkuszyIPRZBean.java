package com.comarch.egeria.pp.iprz;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

@Named
@Scope("view")
public class ListaArkuszyIPRZBean extends SqlBean {
	@Inject
	private SessionBean sessionBean;
	private Date dataObOd;
	private Date dataObDo;
	private String strDataOd = "";
	private String strDataDo = "";
	private String obId = "%";

	// @PostConstruct
	// public void init() {
	// Calendar cal = Calendar.getInstance();
	// cal.set(2025, 11, 31);
	// dataObDo = cal.getTime();
	// strDataDo = new SimpleDateFormat("yy/MM/dd").format(dataObDo);
	// cal.set(1990, 0, 1);
	// dataObOd = cal.getTime();
	// strDataOd = new SimpleDateFormat("yy/MM/dd").format(dataObOd);
	// }

//	public void editSelectedRow() {
//		System.out.println(
//				"=================================ListaOcenBean.editSelectedRow===================================");
//		SqlDataSelectionsHandler p = this.getSql("PP_IPRZ_LISTA"); // sql do
//																	// zmiany
//		if (p != null) {
//			System.out.println(p.getName() + " / currentRow: " + p.getCurrentRow());
//
//			sessionBean.setIprz_id(p.getCurrentRow().getIdAsLong());
//
//			// Przekierowanie do edycji rekordu
//		}
//		System.out.println(
//				"============================================================================================");
//	}

	public void dodajNowyArkusz() {
		sessionBean.setIprz_id(0L);
		com.comarch.egeria.Utils.executeScript("setIframeContentSrc('ArkuszIPRZ');");
		
//		try {
//			FacesContext.getCurrentInstance().getExternalContext().redirect("ArkuszIPRZ");
//		} catch (IOException e) {
//			e.printStackTrace();
//		}

	}

	public void wyszukaj() {
		if (dataObOd == null) {
			strDataOd = "";
		}

		if (dataObDo == null) {
			strDataDo = "";
		}
	}

	public Date getDataObOd() {
		return dataObOd;
	}

	public void setDataObOd(Date dataObOd) {
		if (dataObOd != null) {
			this.dataObOd = dataObOd;
			strDataOd = new SimpleDateFormat("yy/MM/dd").format(dataObOd);
		}
	}

	public Date getDataObDo() {
		return dataObDo;
	}

	public void setDataObDo(Date dataObDo) {
		if (dataObDo != null) {
			this.dataObDo = dataObDo;
			strDataDo = new SimpleDateFormat("yy/MM/dd").format(dataObDo);
		}
	}

	public String getStrDataOd() {
		return strDataOd;
	}

	public void setStrDataOd(String strDataOd) {
		this.strDataOd = strDataOd;
	}

	public void wyczyscDataOd() {
		this.strDataOd = "";
		dataObOd = null;
	}

	public void wyczyscDataDo() {
		this.strDataDo = "";
		dataObDo = null;
	}
	
	public String getStrDataDo() {
		return strDataDo;
	}

	public void setStrDataDo(String strDataDo) {
		this.strDataDo = strDataDo;
	}

	public String getObId() {
		return obId;
	}

	public void setObId(String obId) {
		this.obId = obId;
	}
}
