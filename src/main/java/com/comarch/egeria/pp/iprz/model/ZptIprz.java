package com.comarch.egeria.pp.iprz.model;

import static com.comarch.egeria.Utils.*;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.szkolenia.model.OstPlanIndyw;
import com.comarch.egeria.web.User;

import oracle.net.aso.o;
import oracle.net.aso.x;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;


/**
 * The persistent class for the PPT_IPR_IPRZ database table.
 * 
 */
@Entity
@Table(name="PPT_IPR_IPRZ", schema="PPADM")
@NamedQuery(name="ZptIprz.findAll", query="SELECT z FROM ZptIprz z")
public class ZptIprz implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_IPR_IPRZ",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_IPR_IPRZ", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_IPR_IPRZ")	
	@Column(name="IPRZ_ID")
	private long iprzId;

	@Temporal(TemporalType.DATE)
	@Column(name="IPRZ_AUDYT_DM")
	private Date iprzAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="IPRZ_AUDYT_DT")
	private Date iprzAudytDt;

	@Column(name="IPRZ_AUDYT_KM")
	private String iprzAudytKm;

	@Column(name="IPRZ_AUDYT_LM")
	private String iprzAudytLm;

	@Column(name="IPRZ_AUDYT_UM")
	private String iprzAudytUm;

	@Column(name="IPRZ_AUDYT_UT")
	private String iprzAudytUt;
	
	@Temporal(TemporalType.DATE)
	@Column(name="IPRZ_DATA_OBOWIAZYWANIA_OD")
	private Date iprzDataObowiazywaniaOd;

	@Temporal(TemporalType.DATE)
	@Column(name="IPRZ_DATA_OBOWIAZYWANIA_DO")
	private Date iprzDataObowiazywaniaDo;
	
	@Column(name="IPRZ_OPIS1")
	private String iprzOpis1;
	
	@Column(name="IPRZ_OPIS2")
	private String iprzOpis2;
	
	@Column(name="IPRZ_PODSUMOWANIE")
	private String iprzPodsumowanie;

	@Column(name="IPRZ_PRC_ID")
	private Long iprzPrcId;

	@Column(name="IPRZ_PROGRAM")
	private String iprzProgram;

	@Column(name="IPRZ_SCIEZKA")
	private String iprzSciezka;

	@Column(name="IPRZ_TALENTY")
	private String iprzTalenty;

	//bi-directional many-to-one association to OstPlanIndyw
	@OneToMany(mappedBy="zptIprz",cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 20)
	private List<OstPlanIndyw> ostPlanIndyws = new ArrayList<>();

	
	//bi-directional many-to-one association to PptIprLukiKompetencyjne
	@OneToMany(mappedBy="pptIprIprz",cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 20)
	private List<PptIprLukiKompetencyjne> pptIprLukiKompetencyjnes = new ArrayList<>();
	
	public ZptIprz() {
	}

	
	
	
	
	public List<String> validationOstPlanIndywsErrors(){
		
		ArrayList<String> ret = new ArrayList<>();
		
		Map<Long, Long> hm = this.getSzkoleniaNiebedaceSzkoleniamiWSluzbieCywilnej().stream()
				.collect(Collectors.groupingBy(x->nz(x.getPisOfId()), Collectors.counting()));
		
		int size = hm.entrySet().stream().filter(e->e.getValue()>1).collect(Collectors.toList()).size(); 
		
		if (size > 0)
			ret.add("Szkolenia niebędące szkoleniami w służbie cywilnej nie mogą się powatarzać (wybierz różne nazwy).");
		
		return ret;
	}
	
	
	
	
	
	
	
	
	
	public long getIprzId() {
		return this.iprzId;
	}

	public void setIprzId(long iprzId) {
		this.iprzId = iprzId;
	}

	public Date getIprzAudytDm() {
		return this.iprzAudytDm;
	}

	public void setIprzAudytDm(Date iprzAudytDm) {
		this.iprzAudytDm = iprzAudytDm;
	}

	public Date getIprzAudytDt() {
		return this.iprzAudytDt;
	}

	public void setIprzAudytDt(Date iprzAudytDt) {
		this.iprzAudytDt = iprzAudytDt;
	}

	public String getIprzAudytKm() {
		return this.iprzAudytKm;
	}

	public void setIprzAudytKm(String iprzAudytKm) {
		this.iprzAudytKm = iprzAudytKm;
	}

	public String getIprzAudytLm() {
		return this.iprzAudytLm;
	}

	public void setIprzAudytLm(String iprzAudytLm) {
		this.iprzAudytLm = iprzAudytLm;
	}

	public String getIprzAudytUm() {
		return this.iprzAudytUm;
	}

	public void setIprzAudytUm(String iprzAudytUm) {
		this.iprzAudytUm = iprzAudytUm;
	}

	public String getIprzAudytUt() {
		return this.iprzAudytUt;
	}

	public void setIprzAudytUt(String iprzAudytUt) {
		this.iprzAudytUt = iprzAudytUt;
	}
	
	public Date getIprzDataObowiazywaniaOd() {
		return iprzDataObowiazywaniaOd;
	}

	public void setIprzDataObowiazywaniaOd(Date iprzDataObowiazywaniaOd) {
		this.iprzDataObowiazywaniaOd = iprzDataObowiazywaniaOd;
	}

	public Date getIprzDataObowiazywaniaDo() {
		return iprzDataObowiazywaniaDo;
	}

	public void setIprzDataObowiazywaniaDo(Date iprzDataObowiazywaniaDo) {
		this.iprzDataObowiazywaniaDo = iprzDataObowiazywaniaDo;
	}

	public String getIprzOpis1() {
		return iprzOpis1;
	}

	public void setIprzOpis1(String iprzOpis1) {
		this.iprzOpis1 = iprzOpis1;
	}

	public String getIprzOpis2() {
		return iprzOpis2;
	}

	public void setIprzOpis2(String iprzOpis2) {
		this.iprzOpis2 = iprzOpis2;
	}
	
	public String getIprzPodsumowanie() {
		return this.iprzPodsumowanie;
	}

	public void setIprzPodsumowanie(String iprzPodsumowanie) {
		this.iprzPodsumowanie = iprzPodsumowanie;
	}

	public Long getIprzPrcId() {
		return this.iprzPrcId;
	}

	public void setIprzPrcId(Long iprzPrcId) {
		this.iprzPrcId = iprzPrcId;
		for(OstPlanIndyw o : ostPlanIndyws)
			o.setPisPrcId(iprzPrcId);
	}

	public String getIprzProgram() {
		return this.iprzProgram;
	}

	public void setIprzProgram(String iprzProgram) {
		this.iprzProgram = iprzProgram;
	}

	public String getIprzSciezka() {
		return this.iprzSciezka;
	}

	public void setIprzSciezka(String iprzSciezka) {
		this.iprzSciezka = iprzSciezka;
	}

	public String getIprzTalenty() {
		return this.iprzTalenty;
	}

	public void setIprzTalenty(String iprzTalenty) {
		this.iprzTalenty = iprzTalenty;
	}

	
	public List<OstPlanIndyw> getSzkoleniaSpecjalistyczne() {
		List<OstPlanIndyw> ret = this.ostPlanIndyws.stream().filter(x -> x.getPisSzkPowszech()==null && !"T".equals( x.getPisFCzyNieSc() ) ).collect(Collectors.toList());
		return ret ;
	}
	
	public List<OstPlanIndyw> getSzkoleniaNiebedaceSzkoleniamiWSluzbieCywilnej() {
		List<OstPlanIndyw> ret = this.ostPlanIndyws.stream().filter(x->"T".equals( x.getPisFCzyNieSc() ) ).collect(Collectors.toList());
		return ret ;
	}
	
	public List<OstPlanIndyw> getSzkoleniaBedaceSzkoleniamiWSluzbieCywilnej(){
		List<OstPlanIndyw> ret = this.ostPlanIndyws.stream().filter(x->"N".equals( x.getPisFCzyNieSc() ) ).collect(Collectors.toList());
		return ret ;
	}
	
	
	public List<OstPlanIndyw> getOstPlanIndyws() {
		return this.ostPlanIndyws;
	}

	public void setOstPlanIndyws(List<OstPlanIndyw> ostPlanIndyws) {
		this.ostPlanIndyws = ostPlanIndyws;
	}

	public OstPlanIndyw addOstPlanIndyw(OstPlanIndyw ostPlanIndyw) {
		getOstPlanIndyws().add(ostPlanIndyw);
		ostPlanIndyw.setZptIprz(this);

		return ostPlanIndyw;
	}

	public OstPlanIndyw removeOstPlanIndyw(OstPlanIndyw ostPlanIndyw) {
		getOstPlanIndyws().remove(ostPlanIndyw);
		ostPlanIndyw.setZptIprz(null);

		return ostPlanIndyw;
	}

	public PptIprLukiKompetencyjne removeLuka(PptIprLukiKompetencyjne pptIprLukiKompetencyjne) {

		getPptIprLukiKompetencyjnes().remove(pptIprLukiKompetencyjne);
		
		return pptIprLukiKompetencyjne;
	}
	
	
	public List<PptIprLukiKompetencyjne> getPptIprLukiKompetencyjnes() {
		return this.pptIprLukiKompetencyjnes;
	}

	public void setPptIprLukiKompetencyjnes(List<PptIprLukiKompetencyjne> pptIprLukiKompetencyjnes) {
		this.pptIprLukiKompetencyjnes = pptIprLukiKompetencyjnes;
	}

	public PptIprLukiKompetencyjne addPptIprLukiKompetencyjne(PptIprLukiKompetencyjne pptIprLukiKompetencyjne) {
		getPptIprLukiKompetencyjnes().add(pptIprLukiKompetencyjne);
		pptIprLukiKompetencyjne.setPptIprIprz(this);

		return pptIprLukiKompetencyjne;
	}

	public PptIprLukiKompetencyjne removePptIprLukiKompetencyjne(PptIprLukiKompetencyjne pptIprLukiKompetencyjne) {
		getPptIprLukiKompetencyjnes().remove(pptIprLukiKompetencyjne);
		pptIprLukiKompetencyjne.setPptIprIprz(null);

		return pptIprLukiKompetencyjne;
	}	
	
}