package com.comarch.egeria.pp.zfss.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import com.comarch.egeria.pp.data.model.ModelBase;


/**
 * The persistent class for the PPT_ZFSS_PORECZYCIELE database table.
 * 
 */
@Entity
@Table(name="PPT_ZFSS_PORECZYCIELE", schema="PPADM")
@NamedQuery(name="PptZfssPoreczyciele.findAll", query="SELECT p FROM PptZfssPoreczyciele p")
public class PptZfssPoreczyciele extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ZFSS_PORECZYCIELE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ZFSS_PORECZYCIELE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ZFSS_PORECZYCIELE")	
	@Column(name="ZFSP_ID")
	private long zfspId;

	@Column(name="ZFSP_ADRES")
	private String zfspAdres;

	@Column(name="ZFSP_AUDYT_DM")
	private Date zfspAudytDm;

	@Column(name="ZFSP_AUDYT_DT")
	private Date zfspAudytDt;

	@Column(name="ZFSP_AUDYT_LM")
	private String zfspAudytLm;

	@Column(name="ZFSP_AUDYT_UM")
	private String zfspAudytUm;

	@Column(name="ZFSP_AUDYT_UT")
	private String zfspAudytUt;

	@Column(name="ZFSP_OPIS")
	private String zfspOpis;
	
	@Column(name="ZFSP_PRC_ID")
	private Long zfspPrcId;

	@Column(name="ZFSP_PORECZYCIEL")
	private String zfspPoreczyciel;
	
	@Column(name="ZFSP_DATA_POTWIERDZENIA")
	private Date zfspDataPotwierdzenia;

	//bi-directional many-to-one association to PptZfssWniosek
	@ManyToOne
	@JoinColumn(name="ZFSP_ZFS_ID")
	private PptZfssWniosek pptZfssWniosek;

	public PptZfssPoreczyciele() {
	}

	public long getZfspId() {
		this.onRXGet("zfspId");
		return this.zfspId;
	}

	public void setZfspId(long zfspId) {
		this.onRXSet("zfspId", this.zfspId, zfspId);
		this.zfspId = zfspId;
	}

	public String getZfspAdres() {
		this.onRXGet("zfspAdres");
		return this.zfspAdres;
	}

	public void setZfspAdres(String zfspAdres) {
		this.onRXSet("zfspAdres", this.zfspAdres, zfspAdres);
		this.zfspAdres = zfspAdres;
	}

	public Date getZfspAudytDm() {
		this.onRXGet("zfspAudytDm");
		return this.zfspAudytDm;
	}

	public void setZfspAudytDm(Date zfspAudytDm) {
		this.onRXSet("zfspAudytDm", this.zfspAudytDm, zfspAudytDm);
		this.zfspAudytDm = zfspAudytDm;
	}

	public Date getZfspAudytDt() {
		this.onRXGet("zfspAudytDt");
		return this.zfspAudytDt;
	}

	public void setZfspAudytDt(Date zfspAudytDt) {
		this.onRXSet("zfspAudytDt", this.zfspAudytDt, zfspAudytDt);
		this.zfspAudytDt = zfspAudytDt;
	}

	public String getZfspAudytLm() {
		this.onRXGet("zfspAudytLm");
		return this.zfspAudytLm;
	}

	public void setZfspAudytLm(String zfspAudytLm) {
		this.onRXSet("zfspAudytLm", this.zfspAudytLm, zfspAudytLm);
		this.zfspAudytLm = zfspAudytLm;
	}

	public String getZfspAudytUm() {
		this.onRXGet("zfspAudytUm");
		return this.zfspAudytUm;
	}

	public void setZfspAudytUm(String zfspAudytUm) {
		this.onRXSet("zfspAudytUm", this.zfspAudytUm, zfspAudytUm);
		this.zfspAudytUm = zfspAudytUm;
	}

	public String getZfspAudytUt() {
		this.onRXGet("zfspAudytUt");
		return this.zfspAudytUt;
	}

	public void setZfspAudytUt(String zfspAudytUt) {
		this.onRXSet("zfspAudytUt", this.zfspAudytUt, zfspAudytUt);
		this.zfspAudytUt = zfspAudytUt;
	}

	public String getZfspOpis() {
		this.onRXGet("zfspOpis");
		return this.zfspOpis;
	}

	public void setZfspOpis(String zfspOpis) {
		this.onRXSet("zfspOpis", this.zfspOpis, zfspOpis);
		this.zfspOpis = zfspOpis;
	}

	public String getZfspPoreczyciel() {
		this.onRXGet("zfspPoreczyciel");
		return this.zfspPoreczyciel;
	}

	public void setZfspPoreczyciel(String zfspPoreczyciel) {
		this.onRXSet("zfspPoreczyciel", this.zfspPoreczyciel, zfspPoreczyciel);
		this.zfspPoreczyciel = zfspPoreczyciel;
	}

	public PptZfssWniosek getPptZfssWniosek() {
		this.onRXGet("pptZfssWniosek");
		return this.pptZfssWniosek;
	}

	public void setPptZfssWniosek(PptZfssWniosek pptZfssWniosek) {
		this.onRXSet("pptZfssWniosek", this.pptZfssWniosek, pptZfssWniosek);
		this.pptZfssWniosek = pptZfssWniosek;
	}

	public Long getZfspPrcId() {
		return zfspPrcId;
	}

	public void setZfspPrcId(Long zfspPrcId) {
		this.zfspPrcId = zfspPrcId;
	}

	public Date getZfspDataPotwierdzenia() {
		return zfspDataPotwierdzenia;
	}

	public void setZfspDataPotwierdzenia(Date zfspDataPotwierdzenia) {
		this.zfspDataPotwierdzenia = zfspDataPotwierdzenia;
	}

}
