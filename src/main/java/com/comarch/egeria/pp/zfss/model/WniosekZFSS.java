package com.comarch.egeria.pp.zfss.model;

import javax.faces.context.FacesContext;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.comarch.egeria.Utils.*;

@Entity
public class WniosekZFSS extends PptZfssWniosek {
	
	public WniosekZFSS() {
		
	}
	
	
	public WniosekZFSS ustawieniaDomyslne() {

		User user = User.getCurrentUser();
		if (user!=null)
			zfsPrcId = user.getPrcIdLong();
		
		
		if (!(this instanceof OswiadczenieZFSS)) {
			wczytajAktualneOswiadczenieDoWniosku(this.zfsPrcId);
		}
		return this;
	}

	@Transient
	String req ="";
	
	private OswiadczenieZFSS wczytajAktualneOswiadczenieDoWniosku(long prcId) {
		if (this instanceof OswiadczenieZFSS) 
			return null;//nie ma oswiadczen do oswiadczen
		
		SqlDataSelectionsHandler lw = getLW("PPL_ZFSS_WNIOSEK_OSWIADCZ", prcId);
		DataRow r = lw.first();

		if (r!=null){
			OswiadczenieZFSS o = (OswiadczenieZFSS) HibernateContext.get(OswiadczenieZFSS.class, r.getIdAsLong());
			this.setOswiadczenie(o);
			return o;
		} else {
			this.setOswiadczenie(null);
			if (!req.equals(FacesContext.getCurrentInstance().getExternalContext().getRequest().toString())) {
				req = FacesContext.getCurrentInstance().getExternalContext().getRequest().toString();
				User.warn("Nie znaleziono zaakceptowanego przez komisję ZFŚS oświadczenia o sytuacji życiowej, rodzinnej i materialnej za aktualny rok." +
					"\n\nPrzed złożeniem wniosku należy wypełnić w/w oświadczenie oraz uzyskać akceptację komisji ZFŚS w obiegu dokumentów: " +
					"\n\nZFŚS->Oświadczenia->Lista oświadczeń->[+Dodaj oświadczenie]");
			}
		}
		
		return null;
	}
	
	public void setZfsPrcId(Long zfsPrcId) {
		//potestować tutaj
		wczytajAktualneOswiadczenieDoWniosku(zfsPrcId);
		super.setZfsPrcId(zfsPrcId);
	}
	
	
	
	public void zapisz() {
		if(validate()) {
			super.saveOrUpdate();
			//this.setZfsId(this.getZfsId());//rxset?
		}
	}
	
	public void usun() {

		try {
			HibernateContext.delete(this);
			
			if (!(this instanceof OswiadczenieZFSS)) {
				FacesContext.getCurrentInstance().getExternalContext().redirect("ListaWnioskowZFSS");
			}else {
				FacesContext.getCurrentInstance().getExternalContext().redirect("ListaOswiadczenZFSS");
			}
				
		} catch (Exception e) {
			User.alert("Błąd w usuwaniu wniosku!" + e);
			//log.error(e.getMessage(), e);
		}

	}

	public void addNewPptZfssRodzina() {
		this.addPptZfssRodzina(new PptZfssRodzina());
	}

	public OswiadczenieZFSS getOswiadczenie() {
		OswiadczenieZFSS oswiadczenie = (OswiadczenieZFSS) this.getPptZfssWniosek();
		
		if (!(this instanceof OswiadczenieZFSS) && oswiadczenie==null) {
			oswiadczenie = wczytajAktualneOswiadczenieDoWniosku(this.zfsPrcId);
			if (oswiadczenie != null)
				User.warn("Ustawiono/wczytano oświadczenie %1$s (id: %2%s) z dnia '%3$s' dla wniosku.", 
						oswiadczenie.zfsNumer,  oswiadczenie.zfsId,  format(oswiadczenie.zfsDataZlozeniaWniosku));
		}
		
		return oswiadczenie;
	}

	public void setOswiadczenie(OswiadczenieZFSS o) {
		this.setPptZfssWniosek(o);
	}

	
	private ArrayList<String> czyPowtarzSieWRodzinie(){
		ArrayList<String> al = new ArrayList<>();
		
		for (PptZfssRodzina x : this.pptZfssRodzinas) {
			PptZfssRodzina juzTakiJest = this.pptZfssRodzinas
			.stream().filter(
					xx -> !x.equals(xx)
					&& eq( x.getZfsrImie() , xx.getZfsrImie() )
					&& eq( x.getZfsrNazwisko(), xx.getZfsrNazwisko() )
					&& eq( x.getZfsrDataUrodzenia() , xx.getZfsrDataUrodzenia() )
					).findFirst().orElse(null);

			if (juzTakiJest!=null) {
				String txt = String.format( "Członkowie rodziny / wielokrotny wpis dla %1$s %2$s (ur. %3$s)."
						, juzTakiJest.getZfsrImie(), juzTakiJest.getZfsrNazwisko(), Utils.format( juzTakiJest.getZfsrDataUrodzenia()) );
				
				if (!al.contains(txt))
					al.add(txt);
			}
			
		}
		return al;
	} 
	
	public boolean validate(){
		ArrayList<String> al = new ArrayList<>();
//		al.add("super.validate...");
		
		for(PptZfssRodzina r : this.getPptZfssRodzinas()) {
			if(
					//(r.getZfsrDataUrodzenia() == null) // data urodzenia istotna dla dzieci (kod 11 lub 21)
					 r.getZfsrImie() == null || "".equals(r.getZfsrImie())
					|| r.getZfsrNazwisko() == null || "".equals(r.getZfsrNazwisko())
					|| r.getZfsrPokrewienstwo() == null || "".equals(r.getZfsrPokrewienstwo())
			)
				al.add("Należy uzupełnić wszystkie dane członków rodziny.");


			if(	r.getZfsrDataUrodzenia() == null
					&& (eq("11", r.getZfsrPokrewienstwo()) || eq("21", r.getZfsrPokrewienstwo()) ) )
				al.add("Dla członków rodziny z kodem pokrewieństwa 11 lub 21 (dzieci) należy uzupełnić datę urodzenia.");
		}


		ArrayList<String> czyPowtarzSieWRodzinie = this.czyPowtarzSieWRodzinie();	
		if(czyPowtarzSieWRodzinie!=null && !czyPowtarzSieWRodzinie.isEmpty())
			al.addAll(czyPowtarzSieWRodzinie);
		

		if (!(this instanceof OswiadczenieZFSS) && this.getOswiadczenie()==null){
			//al.add("Brak oświadczenia za aktualny rok.");//nie blokujemy zapisu
			User.warn("Brak oświadczenia za aktualny rok.");
		}

		if (nz(this.zfsPrcId)==0){
			al.add("Pracownika składający wniosek jest wymagany.");
		}

		for (String s :al) {
			User.alert(s);
		}

		return al.isEmpty();
	}
	
	
	public void ustawZfsPrcId(DataRow r) {
		this.setZfsPrcId(r.getIdAsLong());
	}






	public void wczytajCzlonkowRodzinyWgEK(){
		SqlDataSelectionsHandler lw = this.getLW("pp_kad_rodzina", this.zfsPrcId);
		for (DataRow dr : lw.getAllData()) {
			PptZfssRodzina czr = getPptZfssRodzinaByPplKadRodzinaDataRow(dr);
			this.addPptZfssRodzina(czr);
		}
	}

	public void wczytajDzieciDo15latWgEK(){
		SqlDataSelectionsHandler lw = this.getLW("pp_kad_rodzina", this.zfsPrcId);
		for (DataRow dr : lw.getAllData()) {
			PptZfssRodzina czr = getPptZfssRodzinaByPplKadRodzinaDataRow(dr);
			if (!czyDzieckoDo15Lat(czr)) continue;
			this.addPptZfssRodzina(czr);
		}
	}

	private PptZfssRodzina getPptZfssRodzinaByPplKadRodzinaDataRow(DataRow dr) {
		PptZfssRodzina czr = new PptZfssRodzina();
		czr.setZfsrImie((String) dr.get("pp_czr_imie"));
		czr.setZfsrNazwisko((String) dr.get("pp_czr_nazwisko"));
		czr.setZfsrDataUrodzenia((Date) dr.get("pp_czr_data_urodzenia"));
		czr.setZfsrPokrewienstwo((String) dr.get("stopien_pokrewienstwa"));
		return czr;
	}


	public void wczytajCzlonkowRodzinyzOswiadczenia(){
		if (this.getOswiadczenie()==null) return;
		for (PptZfssRodzina x : this.getOswiadczenie().getPptZfssRodzinas()) {
			PptZfssRodzina czr = getPptZfssRodzinaCopy(x);
			this.addPptZfssRodzina(czr);
		}
	}


	public void wczytajDzieciDo15latzOswiadczenia(){
		if (this.getOswiadczenie()==null) return;
		for (PptZfssRodzina x : this.getOswiadczenie().getPptZfssRodzinas()) {
			if (!czyDzieckoDo15Lat(x)) continue;
			PptZfssRodzina czr = getPptZfssRodzinaCopy(x);
			this.addPptZfssRodzina(czr);
		}
	}


	private PptZfssRodzina getPptZfssRodzinaCopy(PptZfssRodzina x) {
		PptZfssRodzina czr = new PptZfssRodzina();
		czr.setZfsrImie(x.getZfsrImie());
		czr.setZfsrNazwisko(x.getZfsrNazwisko());
		czr.setZfsrDataUrodzenia(x.getZfsrDataUrodzenia());
		czr.setZfsrPokrewienstwo(x.getZfsrPokrewienstwo());
		return czr;
	}

	public PptZfssRodzina addPptZfssRodzina(PptZfssRodzina y) {
		if (this.pptZfssRodzinas.stream().filter(x -> eq(x, y)).findAny().isPresent()) {
			User.warn("Nie dodano wpisu (duplikat): \n%1$s %2$s (data ur.: %3$s).", y.getZfsrImie(), y.getZfsrNazwisko(), format(y.getZfsrDataUrodzenia()));
			return null;
		}
		return super.addPptZfssRodzina(y);
	}

	public static boolean eq(PptZfssRodzina x, PptZfssRodzina y){
		return (
					eq(	x.getZfsrImie(), 				x.getZfsrImie()				)
				&& 	eq(	x.getZfsrNazwisko(), 			y.getZfsrNazwisko()			)
				&& 	eq(	x.getZfsrDataUrodzenia(),		y.getZfsrDataUrodzenia()	)
				&& 	eq(	x.getZfsrPokrewienstwo(), 		y.getZfsrPokrewienstwo()	)
				//&& eq(	x.getZfsrNazwaSzkoly(), 		y.getZfsrNazwaSzkoly()		) szkola, lp, czy audyt mogą sie w czasie zmieniac
		);
	}


	private boolean czyDzieckoDo15Lat(PptZfssRodzina x) {

		if (!"11".equals(x.getZfsrPokrewienstwo()) && !"21".equals(x.getZfsrPokrewienstwo()))
			return false;

		return !asLocalDate(x.getZfsrDataUrodzenia()).plusYears(15).isBefore(LocalDate.now());//osoba ukonczyla 15 lat

	}


	public Double getSumPptZfssBileties(){
		if (this.getPptZfssBileties() == null || this.getPptZfssBileties().isEmpty())
			return null;

		double ret = this.getPptZfssBileties().stream().mapToDouble((b -> nz(b.getZfsbKwotaBrutto()))).sum();
		return ret;
	}
	
}


