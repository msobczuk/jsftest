package com.comarch.egeria.pp.zfss;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.zfss.model.OswiadczenieZFSS;
import com.comarch.egeria.pp.zfss.model.WniosekZFSSPozyczka;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean // JBossTools
@Named
@Scope("view")

public class WniosekPozyczkaZFSSBean extends SqlBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	
	private WniosekZFSSPozyczka wniosek = null;
	
	@PostConstruct
	public void init() {

		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		if (id!=null) {
			WniosekZFSSPozyczka w = (WniosekZFSSPozyczka) HibernateContext.get(WniosekZFSSPozyczka.class, (Serializable) id);
			setWniosek(w);
		} else {
			WniosekZFSSPozyczka w = new WniosekZFSSPozyczka();
			w.ustawieniaDomyslne();
			setWniosek(w);
		}
		wniosek.ustawPoswiadczenie();
	}
	
	public void raport() {

		if (wniosek != null && wniosek.getZfsId() > 0) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("zfs_id", wniosek.getZfsId());
			try {
				ReportGenerator.displayReportAsPDF(params, "ZFSS_Pozyczka");

			} catch (JRException | SQLException | IOException e) {
				e.printStackTrace();
				User.alert(e.getMessage());
			}
		}
	}
	

	public WniosekZFSSPozyczka getWniosek() {
		return wniosek;
	}

	public void setWniosek(WniosekZFSSPozyczka wniosek) {
		this.wniosek = wniosek;
	}
	
	
}
