package com.comarch.egeria.pp.zfss.model;

import javax.persistence.Entity;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;

import java.util.ArrayList;

import static com.comarch.egeria.Utils.*;

@Entity
public class WniosekZFSSPozyczka extends WniosekZFSS {
	
	public WniosekZFSSPozyczka() {
		this.setZfsTyp("WPozyczka");
	}

	
	private ArrayList<String> czyPowtarzSiePoreczyciel(){
		ArrayList<String> al = new ArrayList<>();
		
		for (PptZfssPoreczyciele x : this.pptZfssPoreczycieles) {
			PptZfssPoreczyciele juzTakiJest = this.pptZfssPoreczycieles
			.stream().filter(
					xx -> !x.equals(xx)
					&& eq( x.getZfspPrcId(), xx.getZfspPrcId() )
					).findFirst().orElse(null);

			if (juzTakiJest!=null) {
				String txt = "Żaden poręczyciel nie może występować dwa razy w ramach jednego wniosku.";
				
				if (!al.contains(txt))
					al.add(txt);
			}
			
		}
		return al;
	} 
	
	public boolean validate(){

		ArrayList<String> al = new ArrayList<>();
		if (this.zfsCelPozyczki==null){
			al.add("W polu 'Przeznaczenie pożyczki' nalezy wybrać wartość.");
		}

		if (nz(this.zfsKwotaPozyczki)<=0.0){
			al.add("W polu 'Wysokość pożyczki' należy podać liczbę większą od zera.");
		}

		if (nz(this.zfsLiczbaRat)<=0.0){
			al.add("W polu 'Liczba miesięcznych rat' należy podać liczbę większą od zera.");
		}

		if ( this.pptZfssPoreczycieles.size()<2 ){
			al.add("Należy wybrać co najmniej dwóch poręczycieli.");
		}
		
		ArrayList<String> czyPowtarzSiePoreczyciel = this.czyPowtarzSiePoreczyciel();	
		if(czyPowtarzSiePoreczyciel!=null && !czyPowtarzSiePoreczyciel.isEmpty())
			al.addAll(czyPowtarzSiePoreczyciel);

		for (String s :al) {
			User.alert(s);
		}

		return super.validate() && al.isEmpty();
	}





	public void dodajPoreczyciela(DataRow r){
		//System.out.println("dodajPoreczyciela /" + r);
		Long prcId = r.getIdAsLong();

		if( this.getZfsPrcId().equals(prcId)){
			User.warn("Nie można poręczyć własnej pożyczki!");
			return;
		}



		PptZfssPoreczyciele p = new PptZfssPoreczyciele();
		p.setZfspPrcId(prcId);

		this.addPptZfssPoreczyciele(p);
	}
	
	public void ustawPoswiadczenie() {
		if(this.getZfsOpis()==null || "".equals(this.getZfsOpis())) {
			SqlDataSelectionsHandler lw = getLW("ZFSS_POSWIADCZENIE", this.getZfsPrcId());
			DataRow r = lw.first();
	
			if (r!=null && r.get("zaswiadczenie")!=null){
				this.setZfsOpis("" + r.get("zaswiadczenie"));
			}
		}
			
	}

}
