package com.comarch.egeria.pp.zfss;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.zfss.model.WniosekZFSSWypoczynekZimowoWiosenny;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean // JBossTools
@Named
@Scope("view")

public class WypoczynekZimowoWiosennyZFSSBean extends SqlBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	
	private WniosekZFSSWypoczynekZimowoWiosenny wniosek = new WniosekZFSSWypoczynekZimowoWiosenny();
	
	@PostConstruct
	public void init() {

		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		
		if (id!=null) {
			WniosekZFSSWypoczynekZimowoWiosenny w = (WniosekZFSSWypoczynekZimowoWiosenny) HibernateContext.get(WniosekZFSSWypoczynekZimowoWiosenny.class, (Serializable) id);
			setWniosek(w);
		} else {
			WniosekZFSSWypoczynekZimowoWiosenny w = new WniosekZFSSWypoczynekZimowoWiosenny();
			w.ustawieniaDomyslne();
			setWniosek(w);
		}
		
		//wniosek.przeliczKwoteDoFin();
	}

	public WniosekZFSSWypoczynekZimowoWiosenny getWniosek() {
		return wniosek;
	}

	public void setWniosek(WniosekZFSSWypoczynekZimowoWiosenny wniosek) {
		this.wniosek = wniosek;
	}
	
	public void raport() {

		if (wniosek != null && wniosek.getZfsId() > 0) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("zfs_id", wniosek.getZfsId());
			try {
				ReportGenerator.displayReportAsPDF(params, "ZFSS_WypoczynekZimaWiosna");

			} catch (JRException | SQLException | IOException e) {
				e.printStackTrace();
				User.alert(e.getMessage());
			}
		}
	}
	
//	private void ustawKwoteDoFin() {
//		if(!(wniosek.getZfsKwotaDofin() == null || wniosek.getZfsKwotaDofin() == 0 )) return;
//
//		if(wniosek.getOswiadczenie() != null && (wniosek.getOswiadczenie().getZfsKwotaPonizej6k() != null || "T".equals(wniosek.getOswiadczenie().getZfsCzyPrzekroczono6k() ))) {
//			Double wysokoscZarobkow = wniosek.getOswiadczenie().getZfsKwotaPonizej6k() != null ? wniosek.getOswiadczenie().getZfsKwotaPonizej6k() : 6001.0;
//
//			SqlDataSelectionsHandler limity = this.getLW("PPL_ZFSS_WYP_WI_ZIM_KWOTY", wysokoscZarobkow);
//			if(limity != null && limity.getAllData().get(0).get("wysokosc_dofi_dla_pracownika") != null) {
//				Double wysokoscDoFin = Double.parseDouble("" + limity.getAllData().get(0).get("wysokosc_dofi_dla_pracownika"));
//				wniosek.setZfsKwotaDofin(wysokoscDoFin);
//			}
//		}
//	}




	
}
