package com.comarch.egeria.pp.zfss.model;

import javax.persistence.Entity;
import javax.persistence.Transient;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@Entity
public class WniosekZFSSWypoczynek extends WniosekZFSS {
	
	
	@Transient
	private String  opisRegulaminowaKwoteDoFin = null;
	
	public WniosekZFSSWypoczynek() {
		zfsTyp = "WWypoczynek";
		//zfsPrcId = AppBean.getUser().getPrcIdLong();
		//TODO przeniesc getUserId z AppBean do User
		
		
	}
	
	public void addNewPptZfssRodzina() {
		super.addNewPptZfssRodzina();
		this.setZfsKwotaDofin(this.obliczWartoscRegulaminowaKwoteDoFin());
	}
	
	
	
	public PptZfssRodzina removePptZfssRodzina(PptZfssRodzina pptZfssRodzina) {
		PptZfssRodzina removePptZfssRodzina = super.removePptZfssRodzina(pptZfssRodzina);
		this.setZfsKwotaDofin(this.obliczWartoscRegulaminowaKwoteDoFin());
		return removePptZfssRodzina;
		
	}
	
	
	
	
	public void setZfsKwotaDofin(Double zfsKwotaDofin) {
		super.setZfsKwotaDofin(zfsKwotaDofin);
		
		getOpisUstawKwoteDoFin();
		
//		if (getZfsKwotaDofin()==null || !zfsKwotaDofin.equals(obliczWartoscRegulaminowaKwoteDoFin())) {
//			this.opisRegulaminowaKwoteDoFin = null;
//		} 
		
	}
	

	public Double obliczWartoscRegulaminowaKwoteDoFin() {
		//if(!(this.getZfsKwotaDofin() == null || this.getZfsKwotaDofin() == 0 )) return;
		
		this.opisRegulaminowaKwoteDoFin = null;
		
		if(this.getOswiadczenie() != null && (this.getOswiadczenie().getZfsKwotaPonizej6k() != null || "T".equals(this.getOswiadczenie().getZfsCzyPrzekroczono6k() ))) {
			Double wysokoscZarobkow = this.getOswiadczenie().getZfsKwotaPonizej6k() != null ? this.getOswiadczenie().getZfsKwotaPonizej6k() : 6001.0;
			
			SqlDataSelectionsHandler limity = this.getLW("PPL_ZFSS_WYP_KWOTY", wysokoscZarobkow);
			if(limity != null && limity.getAllData().get(0).get("wysokosc_dofi_dla_pracownika") != null && limity.getAllData().get(0).get("wysokosc_dofi_dla_dziecka") != null) {
				Double wysokoscDoFin = Double.parseDouble("" + limity.getAllData().get(0).get("wysokosc_dofi_dla_pracownika"));
				Double wysokoscDoFinDziecko = Double.parseDouble("" + limity.getAllData().get(0).get("wysokosc_dofi_dla_dziecka"));

				if(this.getPptZfssRodzinas() != null) {
					
					double zfsKwotaDofinNew = wysokoscDoFin + this.getPptZfssRodzinas().size() * wysokoscDoFinDziecko;
					this.opisRegulaminowaKwoteDoFin = String.format("wg regulaminu: %1$,.2f = %2$,.2f + %3$s * %4$,.2f", zfsKwotaDofinNew, wysokoscDoFin , this.getPptZfssRodzinas().size() , wysokoscDoFinDziecko );
					return zfsKwotaDofinNew;
					
				}
				else {
					this.opisRegulaminowaKwoteDoFin = String.format("regulaminowa wysokość dofinansowania ( %1$,.2f )", wysokoscDoFin);
					return wysokoscDoFin;
				}
			}
		}
		
		return null;
	}

	
	
	public String getOpisUstawKwoteDoFin() {
		if (eq(this.getZfsKwotaDofin(), obliczWartoscRegulaminowaKwoteDoFin()))
			return opisRegulaminowaKwoteDoFin;	
		else 
			return null;
		
	}

	
	
	
	
	
	
	

}
