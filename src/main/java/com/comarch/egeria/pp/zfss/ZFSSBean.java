package com.comarch.egeria.pp.zfss;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.zfss.model.WniosekZFSS;
import com.comarch.egeria.pp.zfss.model.WniosekZFSSPozyczka;

@ManagedBean // JBossTools
@Named
@Scope("view")

public class ZFSSBean extends SqlBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();


	//TODO wspolne metody adekwatne dla kpontrolerów wniosków (np. raporty)

}
