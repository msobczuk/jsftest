package com.comarch.egeria.pp.zfss;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.zfss.model.WniosekZFSSWypoczynek;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean // JBossTools
@Named
@Scope("view")

public class WypoczynekZFSSBean extends SqlBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	
	private WniosekZFSSWypoczynek wniosek = new WniosekZFSSWypoczynek();
	
	@PostConstruct
	public void init() {
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		
		if (id!=null) {
			WniosekZFSSWypoczynek w = (WniosekZFSSWypoczynek) HibernateContext.get(WniosekZFSSWypoczynek.class, (Serializable) id);
			setWniosek(w);
		} else {
			WniosekZFSSWypoczynek w = new WniosekZFSSWypoczynek();
			w.ustawieniaDomyslne();
			setWniosek(w);
			wniosek.setZfsKwotaDofin(wniosek.obliczWartoscRegulaminowaKwoteDoFin());
		}		
		
	}

	public WniosekZFSSWypoczynek getWniosek() {
		return wniosek;
	}

	public void setWniosek(WniosekZFSSWypoczynek wniosek) {
		this.wniosek = wniosek;
	}
	
	public void raport() {

		if (wniosek != null && wniosek.getZfsId() > 0) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("zfs_id", wniosek.getZfsId());
			try {
				ReportGenerator.displayReportAsPDF(params, "ZFSS_Wypoczynek");

			} catch (JRException | SQLException | IOException e) {
				e.printStackTrace();
				User.alert(e.getMessage());
			}
		}
	}

/*	private void ustawKwoteDoFin() {
		if(!(wniosek.getZfsKwotaDofin() == null || wniosek.getZfsKwotaDofin() == 0 )) return;
		
		if(wniosek.getOswiadczenie() != null && (wniosek.getOswiadczenie().getZfsKwotaPonizej6k() != null || "T".equals(wniosek.getOswiadczenie().getZfsCzyPrzekroczono6k() ))) {
			Double wysokoscZarobkow = wniosek.getOswiadczenie().getZfsKwotaPonizej6k() != null ? wniosek.getOswiadczenie().getZfsKwotaPonizej6k() : 6001.0;
			
			SqlDataSelectionsHandler limity = this.getLW("PPL_ZFSS_WYP_KWOTY", wysokoscZarobkow);
			if(limity != null && limity.getAllData().get(0).get("wysokosc_dofi_dla_pracownika") != null && limity.getAllData().get(0).get("wysokosc_dofi_dla_dziecka") != null) {
				Double wysokoscDoFin = Double.parseDouble("" + limity.getAllData().get(0).get("wysokosc_dofi_dla_pracownika"));
				Double wysokoscDoFinDziecko = Double.parseDouble("" + limity.getAllData().get(0).get("wysokosc_dofi_dla_dziecka"));

				if(wniosek.getPptZfssRodzinas() != null)
					wniosek.setZfsKwotaDofin(wysokoscDoFin + wniosek.getPptZfssRodzinas().size() * wysokoscDoFinDziecko);
				else
					wniosek.setZfsKwotaDofin(wysokoscDoFin);
			}
		}
	}*/





	
}
