package com.comarch.egeria.pp.zfss.model;

import javax.persistence.Entity;

import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

@Entity
public class WniosekZFSSWypoczynekZimowoWiosenny extends WniosekZFSS {
	
	public WniosekZFSSWypoczynekZimowoWiosenny() {
		zfsTyp = "WZimowoWiosenny";
	}


//	@Override
//	public void setZfsPrcId(Long zfsPrcId) {
//		boolean eq = eq(this.zfsPrcId, zfsPrcId);
//		super.setZfsPrcId(zfsPrcId);
//
//		if (!eq){
//			przeliczKwoteDoFin();
//		}
//	}


	@Override
	public void setOswiadczenie(OswiadczenieZFSS o) {
//		boolean eq1 = eq(this.pptZfssWniosek, o);
//		boolean eq2 = eq(this.getOswiadczenie(), o);

		Long oldOswId = o!=null?o.zfsId:null;
		Long newOswId = this.pptZfssWniosek!=null?this.pptZfssWniosek.zfsId:null;
		boolean eq = eq(oldOswId, newOswId);

		super.setOswiadczenie(o);
		if (!eq){
			przeliczKwoteDoFin();
		}
	}

	public void przeliczKwoteDoFin() {
		//if(!(this.getZfsKwotaDofin() == null || this.getZfsKwotaDofin() == 0 )) return;

		if(this.getOswiadczenie() != null && (this.getOswiadczenie().getZfsKwotaPonizej6k() != null || "T".equals(this.getOswiadczenie().getZfsCzyPrzekroczono6k() ))) {
			Double wysokoscZarobkow = this.getOswiadczenie().getZfsKwotaPonizej6k() != null ? this.getOswiadczenie().getZfsKwotaPonizej6k() : 6001.0;

			SqlDataSelectionsHandler limity = this.getLW("PPL_ZFSS_WYP_WI_ZIM_KWOTY", wysokoscZarobkow);
			if(limity != null && limity.getAllData().get(0).get("wysokosc_dofi_dla_pracownika") != null) {
				Double wysokoscDoFin = Double.parseDouble("" + limity.getAllData().get(0).get("wysokosc_dofi_dla_pracownika"));
				this.setZfsKwotaDofin(wysokoscDoFin);
			}
		}
	}

}
