package com.comarch.egeria.pp.zfss;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.comarch.egeria.pp.zfss.model.WniosekZFSSMultiSport;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.zfss.model.WniosekZFSSUniwersalny;

@ManagedBean // JBossTools
@Named
@Scope("view")

public class UniwersalnyWniosekZFSSBean extends SqlBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	
	private WniosekZFSSUniwersalny wniosek = new WniosekZFSSUniwersalny();
	

	@PostConstruct
	public void init() {
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		if (id!=null) {
			WniosekZFSSUniwersalny w = (WniosekZFSSUniwersalny) HibernateContext.get(WniosekZFSSUniwersalny.class, (Serializable) id);
			setWniosek(w);
		} else {
			WniosekZFSSUniwersalny w = (WniosekZFSSUniwersalny) new WniosekZFSSUniwersalny().ustawieniaDomyslne();
			setWniosek(w);
		}
	}


	public WniosekZFSSUniwersalny getWniosek() {
		return wniosek;
	}

	public void setWniosek(WniosekZFSSUniwersalny wniosek) {
		this.wniosek = wniosek;
	}






	
}
