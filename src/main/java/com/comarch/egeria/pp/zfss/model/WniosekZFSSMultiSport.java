package com.comarch.egeria.pp.zfss.model;

import javax.persistence.Entity;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.comarch.egeria.Utils.*;

import java.util.ArrayList;

@Entity
public class WniosekZFSSMultiSport extends WniosekZFSS {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	public WniosekZFSSMultiSport() {
		zfsTyp = "WMultiSport";
		//zfsPrcId = AppBean.getUser().getPrcIdLong();
		//TODO przeniesc getUserId z AppBean do User
		
		
	}
	

	public void setZfsMultisport(String zfsMultisport) {
		super.setZfsMultisport(zfsMultisport);
		DataRow r = getMultiSportProgramDataRow(this.zfsMultisport);
		if (r!=null) {
			this.setZfsMultisWartMiesie(asDouble((String) r.get("wsl_alias")));
			if(this.getZfsMultisWartMiesieDziecko() == null || this.getZfsMultisWartMiesieDziecko().doubleValue() == 0)
				this.setZfsMultisWartMiesieDziecko(asDouble((String) r.get("wsl_alias3")));
		}

		if (this.getZfsMultisOsbTowProgram()==null)
			this.setZfsMultisOsbTowProgram(zfsMultisport);
	}

    public void setZfsMultisOsbTowProgram(String zfsMultisOsbTowProgram) {
	    super.setZfsMultisOsbTowProgram(zfsMultisOsbTowProgram);
        DataRow r = getMultiSportProgramDataRow(this.zfsMultisOsbTowProgram);
        if (r!=null)
            this.setZfsMultisFinansDodatkKarty(asDouble((String) r.get("wsl_alias2")));
    }
    
    public void setZfsMultisDzieckoProgram(String zfsMultisDzieckoProgram) {
	    super.setZfsMultisDzieckoProgram(zfsMultisDzieckoProgram);
        DataRow r = getMultiSportProgramDataRow(this.zfsMultisDzieckoProgram);
        if (r!=null)
        	this.setZfsMultisWartMiesieDziecko(asDouble((String) r.get("wsl_alias3")));
    }


	public DataRow getMultiSportProgramDataRow(String wslWartosc){
		SqlDataSelectionsHandler lw = getSL( "ZFSS_PROGRAMY_MULTISPORT");
		return lw.find(wslWartosc);
	}



	public Double getSumPptZfssBileties(){
		if (this.getPptZfssBileties() == null || this.getPptZfssBileties().isEmpty())
			return null;

		double ret = this.getPptZfssBileties().stream().mapToDouble((b -> nz(b.getZfsbKwotaBrutto()))).sum();
		return ret;
	}


	public Double getSumaPotracenZaDzieci(){
		Double ret = nz(this.getZfsMultisWartMiesieDziecko()) * this.getPptZfssRodzinas().size();
		return ret;
	}

	public Double getSumaMiesiecznychPotracen(){
		Double ret =  nz( this.getZfsMultisWartMiesie() );
		if ("T".equals(this.zfsMultisCzyTowarzyszaca))
			ret += nz(this.getZfsMultisFinansDodatkKarty());

		ret += getSumaPotracenZaDzieci();
		return ret;
	}
	
	public boolean validate() {
		ArrayList<String> al = new ArrayList<>();
		
		if(this.zfsMultisCzyTowarzyszaca != null && "T".equals(this.zfsMultisCzyTowarzyszaca) && (zfsMultisOsbTowProgram == null || "".equals(zfsMultisOsbTowProgram) || zfsMultisOsbTowImieNaz == null || "".equals(zfsMultisOsbTowImieNaz) || zfsMultisFinansDodatkKarty == null) )
			al.add("Należy uzupełnić wszystkie dane dotyczące programu Multisport osoby towarzyszącej.");
				
		
		for (String s :al) {
			User.alert(s);
		}


		return super.validate() && al.isEmpty();
	}


}
