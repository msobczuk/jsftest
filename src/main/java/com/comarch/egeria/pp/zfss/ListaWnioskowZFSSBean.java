package com.comarch.egeria.pp.zfss;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.zfss.model.WniosekZFSS;
import com.comarch.egeria.pp.zfss.model.WniosekZFSSPozyczka;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;

@ManagedBean // JBossTools
@Named
@Scope("view")

public class ListaWnioskowZFSSBean extends SqlBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();


	public void nowyWniosek(DataRow r) {

		String typ = r.getIdAsString();
		String url = mapujTypWnioskuToUrl(typ);

		try {
			if (url!=null) {
				System.out.println(typ + " -> " + url);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", null);
				FacesContext.getCurrentInstance().getExternalContext().redirect(url);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}


	public void edytujWniosek(DataRow row) {

//		System.out.println(row);
		if (row==null) return;

		String typ = (String) row.get("zfs_typ");
		String url = mapujTypWnioskuToUrl(typ);

		try {
			if (url!=null) {
				System.out.println(typ + " -> " + url);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", row);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", row.getIdAsLong());
				FacesContext.getCurrentInstance().getExternalContext().redirect(url);
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}



	private String mapujTypWnioskuToUrl(String typ) {
		String url = null;

		switch (typ) {
			case "WPozyczka":
				url = "WniosekZFSSPozyczka";
				break;
				
			case "WBezzwrotnaPomoc":
				url = "WniosekZFSSBezzwrotnaPomoc";
				break;	
				
			case "WZimowoWiosenny":
				url = "WniosekZFSSZimowoWiosenny";
				break;	
				
			case "WWypoczynek":
				url = "WniosekZFSSWypoczynek";
				break;

			case "WMultiSport":
				url = "WniosekZFSSMultiSport";
				break;


			case "WBiletyiKarnety":
				url = "WniosekZFSSBiletyiKarnety";
				break;


			default:
				url = "WniosekZFSSUniwersalnyPozostale";
				break;
		}
		return url;
	}


}
