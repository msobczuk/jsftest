package com.comarch.egeria.pp.zfss.model;

import javax.persistence.Entity;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

import static com.comarch.egeria.Utils.nz;

import java.util.ArrayList;
import java.util.Date;

@Entity
public class OswiadczenieZFSS extends WniosekZFSS {
	
	public OswiadczenieZFSS() {

		zfsTyp = "O";
		User user = User.getCurrentUser();
		if (user!=null)
			zfsPrcId = user.getPrcIdLong();

	}
	
	
	public void ustawWnioskodawce(DataRow r){
		this.setZfsPrcId(r.getIdAsLong()); //TODO przy zmianie prcId wcyzyscic rodzinke (wypada zmodyfikowac setZfsPrcId)
	}
	

	public boolean validate() {
		ArrayList<String> al = new ArrayList<>();
		
		if( this.getZfsCzyPrzekroczono6k() == null || ("N".equals(this.getZfsCzyPrzekroczono6k()) &&  (this.getZfsKwotaPonizej6k() == null || this.getZfsKwotaPonizej6k().doubleValue() == 0)) )
			al.add("Należy uzupełnić dane o przeciętnym miesięcznym dochodzie.");
		
		for (String s :al) {
			User.alert(s);
		}


		return super.validate() && al.isEmpty();
	}

}
