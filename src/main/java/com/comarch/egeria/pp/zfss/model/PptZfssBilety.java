package com.comarch.egeria.pp.zfss.model;

import java.io.Serializable;
import javax.persistence.*;

import com.comarch.egeria.pp.data.model.ModelBase;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_ZFSS_BILETY database table.
 * 
 */
@Entity
@Table(name="PPT_ZFSS_BILETY", schema="PPADM")
@NamedQuery(name="PptZfssBilety.findAll", query="SELECT p FROM PptZfssBilety p")
public class PptZfssBilety extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ZFSS_BILETY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ZFSS_BILETY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ZFSS_BILETY")	
	@Column(name="ZFSB_ID")
	private long zfsbId;

	@Column(name="ZFSB_AUDYT_DM")
	private Date zfsbAudytDm;

	@Column(name="ZFSB_AUDYT_DT")
	private Date zfsbAudytDt;

	@Column(name="ZFSB_AUDYT_LM")
	private String zfsbAudytLm;

	@Column(name="ZFSB_AUDYT_UM")
	private String zfsbAudytUm;

	@Column(name="ZFSB_AUDYT_UT")
	private String zfsbAudytUt;

	@Column(name="ZFSB_DATA_WYSTAWIENIA")
	private Date zfsbDataWystawienia;

	@Column(name="ZFSB_KWOTA_BRUTTO")
	private Double zfsbKwotaBrutto;

	@Column(name="ZFSB_NUMER_FAKTURY")
	private String zfsbNumerFaktury;

	//bi-directional many-to-one association to PptZfssWniosek
	@ManyToOne
	@JoinColumn(name="ZFSB_ZFS_ID")
	private PptZfssWniosek pptZfssWniosek;

	public PptZfssBilety() {
	}

	public long getZfsbId() {
		this.onRXGet("zfsbId");
		return this.zfsbId;
	}

	public void setZfsbId(long zfsbId) {
		this.onRXSet("zfsbId", this.zfsbId, zfsbId);
		this.zfsbId = zfsbId;
	}

	public Date getZfsbAudytDm() {
		this.onRXGet("zfsbAudytDm");
		return this.zfsbAudytDm;
	}

	public void setZfsbAudytDm(Date zfsbAudytDm) {
		this.onRXSet("zfsbAudytDm", this.zfsbAudytDm, zfsbAudytDm);
		this.zfsbAudytDm = zfsbAudytDm;
	}

	public Date getZfsbAudytDt() {
		this.onRXGet("zfsbAudytDt");
		return this.zfsbAudytDt;
	}

	public void setZfsbAudytDt(Date zfsbAudytDt) {
		this.onRXSet("zfsbAudytDt", this.zfsbAudytDt, zfsbAudytDt);
		this.zfsbAudytDt = zfsbAudytDt;
	}

	public String getZfsbAudytLm() {
		this.onRXGet("zfsbAudytLm");
		return this.zfsbAudytLm;
	}

	public void setZfsbAudytLm(String zfsbAudytLm) {
		this.onRXSet("zfsbAudytLm", this.zfsbAudytLm, zfsbAudytLm);
		this.zfsbAudytLm = zfsbAudytLm;
	}

	public String getZfsbAudytUm() {
		this.onRXGet("zfsbAudytUm");
		return this.zfsbAudytUm;
	}

	public void setZfsbAudytUm(String zfsbAudytUm) {
		this.onRXSet("zfsbAudytUm", this.zfsbAudytUm, zfsbAudytUm);
		this.zfsbAudytUm = zfsbAudytUm;
	}

	public String getZfsbAudytUt() {
		this.onRXGet("zfsbAudytUt");
		return this.zfsbAudytUt;
	}

	public void setZfsbAudytUt(String zfsbAudytUt) {
		this.onRXSet("zfsbAudytUt", this.zfsbAudytUt, zfsbAudytUt);
		this.zfsbAudytUt = zfsbAudytUt;
	}

	public Date getZfsbDataWystawienia() {
		this.onRXGet("zfsbDataWystawienia");
		return this.zfsbDataWystawienia;
	}

	public void setZfsbDataWystawienia(Date zfsbDataWystawienia) {
		this.onRXSet("zfsbDataWystawienia", this.zfsbDataWystawienia, zfsbDataWystawienia);
		this.zfsbDataWystawienia = zfsbDataWystawienia;
	}

	public Double getZfsbKwotaBrutto() {
		this.onRXGet("zfsbKwotaBrutto");
		return this.zfsbKwotaBrutto;
	}

	public void setZfsbKwotaBrutto(Double zfsbKwotaBrutto) {
		this.onRXSet("zfsbKwotaBrutto", this.zfsbKwotaBrutto, zfsbKwotaBrutto);
		this.zfsbKwotaBrutto = zfsbKwotaBrutto;
	}

	public String getZfsbNumerFaktury() {
		this.onRXGet("zfsbNumerFaktury");
		return this.zfsbNumerFaktury;
	}

	public void setZfsbNumerFaktury(String zfsbNumerFaktury) {
		this.onRXSet("zfsbNumerFaktury", this.zfsbNumerFaktury, zfsbNumerFaktury);
		this.zfsbNumerFaktury = zfsbNumerFaktury;
	}

	public PptZfssWniosek getPptZfssWniosek() {
		this.onRXGet("pptZfssWniosek");
		return this.pptZfssWniosek;
	}

	public void setPptZfssWniosek(PptZfssWniosek pptZfssWniosek) {
		this.onRXSet("pptZfssWniosek", this.pptZfssWniosek, pptZfssWniosek);
		this.pptZfssWniosek = pptZfssWniosek;
	}

}
