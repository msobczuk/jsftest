package com.comarch.egeria.pp.zfss;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;

@ManagedBean // JBossTools
@Named
@Scope("view")

public class ListaOswiadczenZFSSBean extends SqlBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();


	public void dodajOswiadczenie() {
		try {
			if ("OswiadczenieZFSS" !=null) {
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", null);
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", null);
				FacesContext.getCurrentInstance().getExternalContext().redirect("OswiadczenieZFSS");
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}


}
