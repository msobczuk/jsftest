package com.comarch.egeria.pp.zfss;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.xml.crypto.Data;

import com.comarch.egeria.pp.data.DataRow;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.wnioskiZaswiadczenia.model.PptWnioskiZaswiadczenia;
import com.comarch.egeria.pp.zfss.model.OswiadczenieZFSS;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean // JBossTools
@Named
@Scope("view")

public class OswiadczenieZFSSBean extends SqlBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	private OswiadczenieZFSS oswiadczenie = null;
	
	@PostConstruct
	public void init() {

		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		DataRow row = (DataRow) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("row");

		if (id!=null){
			this.setOswiadczenie((OswiadczenieZFSS) HibernateContext.get(OswiadczenieZFSS.class, (Serializable) id));
		}else {
			this.setOswiadczenie(new OswiadczenieZFSS());
		}

	}
	
	public void raport() {

		if (oswiadczenie != null && oswiadczenie.getZfsId() > 0) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("zfs_id", oswiadczenie.getZfsId());
			try {
				ReportGenerator.displayReportAsPDF(params, "ZFSS_Oswiadczenie");

			} catch (JRException | SQLException | IOException e) {
				e.printStackTrace();
				User.alert(e.getMessage());
			}
		}
	}


	public OswiadczenieZFSS getOswiadczenie() {
		return oswiadczenie;
	}

	public void setOswiadczenie(OswiadczenieZFSS oswiadczenie) {
		this.oswiadczenie = oswiadczenie;
	}


}
