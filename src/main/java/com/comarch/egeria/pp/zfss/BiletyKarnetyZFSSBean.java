package com.comarch.egeria.pp.zfss;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import com.comarch.egeria.pp.zfss.model.WniosekZFSSWypoczynekZimowoWiosenny;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.zfss.model.WniosekZFSSBiletyKarnety;

@ManagedBean // JBossTools
@Named
@Scope("view")

public class BiletyKarnetyZFSSBean extends SqlBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	
	private WniosekZFSSBiletyKarnety wniosek = new WniosekZFSSBiletyKarnety();
	
	@PostConstruct
	public void init() {
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		if (id!=null) {
			WniosekZFSSBiletyKarnety w = (WniosekZFSSBiletyKarnety) HibernateContext.get(WniosekZFSSBiletyKarnety.class, (Serializable) id);
			setWniosek(w);
		} else {
			WniosekZFSSBiletyKarnety w = (WniosekZFSSBiletyKarnety) new WniosekZFSSBiletyKarnety().ustawieniaDomyslne();
			w.setZfsMiejsceZlozeniaWniosku("Tarnów");
			w.setZfsDataZlozeniaWniosku(new Date());
			setWniosek(w);
		}
	}
	
	public void raport() {

		if (wniosek != null && wniosek.getZfsId() > 0) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("zfs_id", wniosek.getZfsId());
			try {
				ReportGenerator.displayReportAsPDF(params, "ZFSS_Bilety");

			} catch (JRException | SQLException | IOException e) {
				e.printStackTrace();
				User.alert(e.getMessage());
			}
		}
	}

	public WniosekZFSSBiletyKarnety getWniosek() {
		return wniosek;
	}

	public void setWniosek(WniosekZFSSBiletyKarnety wniosek) {
		this.wniosek = wniosek;
	}






	
}
