package com.comarch.egeria.pp.zfss.model;

import java.io.Serializable;
import javax.persistence.*;

import com.comarch.egeria.pp.data.model.ModelBase;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_ZFSS_RODZINA database table.
 * 
 */
@Entity
@Table(name="PPT_ZFSS_RODZINA", schema="PPADM")
@NamedQuery(name="PptZfssRodzina.findAll", query="SELECT p FROM PptZfssRodzina p")
public class PptZfssRodzina extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ZFSS_RODZINA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ZFSS_RODZINA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ZFSS_RODZINA")	
	@Column(name="ZFSR_ID")
	private long zfsrId;

	@Column(name="ZFSR_AUDYT_DM")
	private Date zfsrAudytDm;

	@Column(name="ZFSR_AUDYT_DT")
	private Date zfsrAudytDt;

	@Column(name="ZFSR_AUDYT_LM")
	private String zfsrAudytLm;

	@Column(name="ZFSR_AUDYT_UM")
	private String zfsrAudytUm;

	@Column(name="ZFSR_AUDYT_UT")
	private String zfsrAudytUt;

	@Column(name="ZFSR_DATA_URODZENIA")
	private Date zfsrDataUrodzenia;

	@Column(name="ZFSR_IMIE")
	private String zfsrImie;

	@Column(name="ZFSR_LP")
	private Long zfsrLp;

	@Column(name="ZFSR_NAZWA_SZKOLY")
	private String zfsrNazwaSzkoly;

	@Column(name="ZFSR_NAZWISKO")
	private String zfsrNazwisko;

	@Column(name="ZFSR_POKREWIENSTWO")
	private String zfsrPokrewienstwo;

	//bi-directional many-to-one association to PptZfssWniosek
	@ManyToOne
	@JoinColumn(name="ZFSR_ZFS_ID")
	private PptZfssWniosek pptZfssWniosek;

	public PptZfssRodzina() {
	}

	public long getZfsrId() {
		this.onRXGet("zfsrId");
		return this.zfsrId;
	}

	public void setZfsrId(long zfsrId) {
		this.onRXSet("zfsrId", this.zfsrId, zfsrId);
		this.zfsrId = zfsrId;
	}

	public Date getZfsrAudytDm() {
		this.onRXGet("zfsrAudytDm");
		return this.zfsrAudytDm;
	}

	public void setZfsrAudytDm(Date zfsrAudytDm) {
		this.onRXSet("zfsrAudytDm", this.zfsrAudytDm, zfsrAudytDm);
		this.zfsrAudytDm = zfsrAudytDm;
	}

	public Date getZfsrAudytDt() {
		this.onRXGet("zfsrAudytDt");
		return this.zfsrAudytDt;
	}

	public void setZfsrAudytDt(Date zfsrAudytDt) {
		this.onRXSet("zfsrAudytDt", this.zfsrAudytDt, zfsrAudytDt);
		this.zfsrAudytDt = zfsrAudytDt;
	}

	public String getZfsrAudytLm() {
		this.onRXGet("zfsrAudytLm");
		return this.zfsrAudytLm;
	}

	public void setZfsrAudytLm(String zfsrAudytLm) {
		this.onRXSet("zfsrAudytLm", this.zfsrAudytLm, zfsrAudytLm);
		this.zfsrAudytLm = zfsrAudytLm;
	}

	public String getZfsrAudytUm() {
		this.onRXGet("zfsrAudytUm");
		return this.zfsrAudytUm;
	}

	public void setZfsrAudytUm(String zfsrAudytUm) {
		this.onRXSet("zfsrAudytUm", this.zfsrAudytUm, zfsrAudytUm);
		this.zfsrAudytUm = zfsrAudytUm;
	}

	public String getZfsrAudytUt() {
		this.onRXGet("zfsrAudytUt");
		return this.zfsrAudytUt;
	}

	public void setZfsrAudytUt(String zfsrAudytUt) {
		this.onRXSet("zfsrAudytUt", this.zfsrAudytUt, zfsrAudytUt);
		this.zfsrAudytUt = zfsrAudytUt;
	}

	public Date getZfsrDataUrodzenia() {
		this.onRXGet("zfsrDataUrodzenia");
		return this.zfsrDataUrodzenia;
	}

	public void setZfsrDataUrodzenia(Date zfsrDataUrodzenia) {
		this.onRXSet("zfsrDataUrodzenia", this.zfsrDataUrodzenia, zfsrDataUrodzenia);
		this.zfsrDataUrodzenia = zfsrDataUrodzenia;
	}

	public String getZfsrImie() {
		this.onRXGet("zfsrImie");
		return this.zfsrImie;
	}

	public void setZfsrImie(String zfsrImie) {
		this.onRXSet("zfsrImie", this.zfsrImie, zfsrImie);
		this.zfsrImie = zfsrImie;
	}

	public Long getZfsrLp() {
		this.onRXGet("zfsrLp");
		return this.zfsrLp;
	}

	public void setZfsrLp(Long zfsrLp) {
		this.onRXSet("zfsrLp", this.zfsrLp, zfsrLp);
		this.zfsrLp = zfsrLp;
	}

	public String getZfsrNazwaSzkoly() {
		this.onRXGet("zfsrNazwaSzkoly");
		return this.zfsrNazwaSzkoly;
	}

	public void setZfsrNazwaSzkoly(String zfsrNazwaSzkoly) {
		this.onRXSet("zfsrNazwaSzkoly", this.zfsrNazwaSzkoly, zfsrNazwaSzkoly);
		this.zfsrNazwaSzkoly = zfsrNazwaSzkoly;
	}

	public String getZfsrNazwisko() {
		this.onRXGet("zfsrNazwisko");
		return this.zfsrNazwisko;
	}

	public void setZfsrNazwisko(String zfsrNazwisko) {
		this.onRXSet("zfsrNazwisko", this.zfsrNazwisko, zfsrNazwisko);
		this.zfsrNazwisko = zfsrNazwisko;
	}

	public String getZfsrPokrewienstwo() {
		this.onRXGet("zfsrPokrewienstwo");
		return this.zfsrPokrewienstwo;
	}

	public void setZfsrPokrewienstwo(String zfsrPokrewienstwo) {
		this.onRXSet("zfsrPokrewienstwo", this.zfsrPokrewienstwo, zfsrPokrewienstwo);
		this.zfsrPokrewienstwo = zfsrPokrewienstwo;
	}

	public PptZfssWniosek getPptZfssWniosek() {
		this.onRXGet("pptZfssWniosek");
		return this.pptZfssWniosek;
	}

	public void setPptZfssWniosek(PptZfssWniosek pptZfssWniosek) {
		this.onRXSet("pptZfssWniosek", this.pptZfssWniosek, pptZfssWniosek);
		this.pptZfssWniosek = pptZfssWniosek;
	}

}
