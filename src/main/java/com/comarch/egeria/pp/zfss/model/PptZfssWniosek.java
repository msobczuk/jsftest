package com.comarch.egeria.pp.zfss.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.comarch.egeria.pp.data.model.ModelBase;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_ZFSS_WNIOSEK database table.
 * 
 */
@Entity
@Table(name="PPT_ZFSS_WNIOSEK", schema="PPADM")
@NamedQuery(name="PptZfssWniosek.findAll", query="SELECT p FROM PptZfssWniosek p")
@DiscriminatorFormula("CASE "
					+ "WHEN ZFS_TYP='O' 					THEN 'OswiadczenieZFSS'"
					+ "WHEN ZFS_TYP='WPozyczka' 			THEN 'WniosekZFSSPozyczka'"
					+ "WHEN ZFS_TYP='WZimowoWiosenny'	 	THEN 'WniosekZFSSWypoczynekZimowoWiosenny'"
					+ "WHEN ZFS_TYP='WWypoczynek' 			THEN 'WniosekZFSSWypoczynek'"
					+ "WHEN ZFS_TYP='WBezzwrotnaPomoc'	 	THEN 'WniosekZFSSBezzwrotnaPomoc'"
					+ "WHEN ZFS_TYP='WMultiSport' 			THEN 'WniosekZFSSMultiSport'"
					+ "WHEN ZFS_TYP='WBiletyiKarnety' 		THEN 'WniosekZFSSBiletyKarnety'" 
					+ "WHEN ZFS_TYP='WUniwersalnyPozostale' THEN 'WniosekZFSSUniwersalny'"
					+ "ELSE 'PptZfssWniosek' END")
public class PptZfssWniosek extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ZFSS_WNIOSEK",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ZFSS_WNIOSEK", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ZFSS_WNIOSEK")	
	@Column(name="ZFS_ID")
	long zfsId;

	@Column(name="ZFS_AUDYT_DM")
	Date zfsAudytDm;

	@Column(name="ZFS_AUDYT_DT")
	Date zfsAudytDt;

	@Column(name="ZFS_AUDYT_LM")
	String zfsAudytLm;

	@Column(name="ZFS_AUDYT_UM")
	String zfsAudytUm;

	@Column(name="ZFS_AUDYT_UT")
	String zfsAudytUt;

	@Column(name="ZFS_CEL_POZYCZKI")
	String zfsCelPozyczki;

	@Column(name="ZFS_CZY_PRZEKROCZONO_6K")
	String zfsCzyPrzekroczono6k;

	@Column(name="ZFS_DATA_UMOWY")
	Date zfsDataUmowy;

	@Column(name="ZFS_DATA_WNIOSKU_O_ZAPOMOGE")
	Date zfsDataWnioskuOZapomoge;

	@Column(name="ZFS_DATA_ZLOZENIA_WNIOSKU")
	Date zfsDataZlozeniaWniosku;

	@Column(name="ZFS_DOCHOD_NETTO")
	Double zfsDochodNetto;

	@Column(name="ZFS_DOCHOD_W_ROK")
	Double zfsDochodWRok;

	@Column(name="ZFS_INNE_DOCHODY")
	Double zfsInneDochody;

	@Column(name="ZFS_KWOTA_DOFIN")
	Double zfsKwotaDofin;

	@Column(name="ZFS_KWOTA_INNE_WPLYWY")
	Double zfsKwotaInneWplywy;

	@Column(name="ZFS_KWOTA_PONIZEJ_6K")
	Double zfsKwotaPonizej6k;

	@Column(name="ZFS_KWOTA_POZYCZKI")
	Double zfsKwotaPozyczki;

	@Column(name="ZFS_LICZBA_RAT")
	Double zfsLiczbaRat;

	@Column(name="ZFS_MIEJSCE_ZLOZENIA_WNIOSKU")
	String zfsMiejsceZlozeniaWniosku;

	@Column(name="ZFS_MULTIS_CZY_TOWARZYSZACA")
	String zfsMultisCzyTowarzyszaca;

	@Column(name="ZFS_MULTIS_FINANS_DODATK_KARTY")
	Double zfsMultisFinansDodatkKarty;

	@Column(name="ZFS_MULTIS_OSB_TOW_IMIE_NAZ")
	String zfsMultisOsbTowImieNaz;

	@Column(name="ZFS_MULTIS_OSB_TOW_PROGRAM")
	String zfsMultisOsbTowProgram;

	@Column(name="ZFS_MULTIS_POTRACENIE_OD_MIES")
	String zfsMultisPotracenieOdMies;

	@Column(name="ZFS_MULTIS_POTRACENIE_OD_ROK")
	Double zfsMultisPotracenieOdRok;

	@Column(name="ZFS_MULTIS_WART_MIESIE")
	Double zfsMultisWartMiesie;

	@Column(name="ZFS_MULTISPORT")
	String zfsMultisport;

	@Column(name="ZFS_NUMER")
	String zfsNumer;

	@Column(name="ZFS_NUMER_UMOWY")
	String zfsNumerUmowy;

	@Column(name="ZFS_OPIS")
	String zfsOpis;

	@Column(name="ZFS_POW_NIE_POZYCZKI")
	String zfsPowNiePozyczki;

	@Column(name="ZFS_POWOD_ODMOWY")
	String zfsPowodOdmowy;

	@Column(name="ZFS_PRC_ID")
	Long zfsPrcId;

	@Column(name="ZFS_PROP_KWOTA_ZAPOMOGI")
	Double zfsPropKwotaZapomogi;

	@Column(name="ZFS_SUMA_FAKTUR")
	Double zfsSumaFaktur;

	@Column(name="ZFS_TYP")
	String zfsTyp;

	@Column(name="ZFS_ZAROBKI_WNIOSKODAWCY")
	Double zfsZarobkiWnioskodawcy;

	@Column(name="ZFS_ZAROBKI_WSPOLMALZONKA")
	Double zfsZarobkiWspolmalzonka;
	
	@Column(name="ZFS_MULTIS_WART_MIESIE_DZIECKO")
	Double zfsMultisWartMiesieDziecko;
	
	@Column(name="ZFS_MULTIS_DZIECKO_PROGRAM")
	String zfsMultisDzieckoProgram;

	//bi-directional many-to-one association to PptZfssBilety
	@OneToMany(mappedBy="pptZfssWniosek", cascade={CascadeType.ALL}, orphanRemoval=true)
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("ZFSB_ID ASC")
	List<PptZfssBilety> pptZfssBileties = new ArrayList<>();

    //bi-directional many-to-one association to PptZfssPoreczyciele
	@OneToMany(mappedBy="pptZfssWniosek", cascade={CascadeType.ALL}, orphanRemoval=true)
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("ZFSP_ID ASC")
	List<PptZfssPoreczyciele> pptZfssPoreczycieles = new ArrayList<>();

    //bi-directional many-to-one association to PptZfssRodzina
	@OneToMany(mappedBy="pptZfssWniosek", cascade={CascadeType.ALL}, orphanRemoval=true)
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("ZFSR_ID ASC")
	List<PptZfssRodzina> pptZfssRodzinas = new ArrayList<>();

    //bi-directional many-to-one association to PptZfssWniosek
	@ManyToOne
	@JoinColumn(name="ZFS_ZFS_ID")
	PptZfssWniosek pptZfssWniosek;

	//bi-directional many-to-one association to PptZfssWniosek
	@OneToMany(mappedBy="pptZfssWniosek",  orphanRemoval=true)  // cascade={CascadeType.ALL},niw stosować kaskady przy referencji oswiadczenie -1---*-> wnioski
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("ZFS_ID ASC")
	List<PptZfssWniosek> pptZfssWnioseks = new ArrayList<>();

    public PptZfssWniosek() {
	}

	public long getZfsId() {
		this.onRXGet("zfsId");
		return this.zfsId;
	}

	public void setZfsId(long zfsId) {
		this.onRXSet("zfsId", this.zfsId, zfsId);
		this.zfsId = zfsId;
	}

	public Date getZfsAudytDm() {
		this.onRXGet("zfsAudytDm");
		return this.zfsAudytDm;
	}

	public void setZfsAudytDm(Date zfsAudytDm) {
		this.onRXSet("zfsAudytDm", this.zfsAudytDm, zfsAudytDm);
		this.zfsAudytDm = zfsAudytDm;
	}

	public Date getZfsAudytDt() {
		this.onRXGet("zfsAudytDt");
		return this.zfsAudytDt;
	}

	public void setZfsAudytDt(Date zfsAudytDt) {
		this.onRXSet("zfsAudytDt", this.zfsAudytDt, zfsAudytDt);
		this.zfsAudytDt = zfsAudytDt;
	}

	public String getZfsAudytLm() {
		this.onRXGet("zfsAudytLm");
		return this.zfsAudytLm;
	}

	public void setZfsAudytLm(String zfsAudytLm) {
		this.onRXSet("zfsAudytLm", this.zfsAudytLm, zfsAudytLm);
		this.zfsAudytLm = zfsAudytLm;
	}

	public String getZfsAudytUm() {
		this.onRXGet("zfsAudytUm");
		return this.zfsAudytUm;
	}

	public void setZfsAudytUm(String zfsAudytUm) {
		this.onRXSet("zfsAudytUm", this.zfsAudytUm, zfsAudytUm);
		this.zfsAudytUm = zfsAudytUm;
	}

	public String getZfsAudytUt() {
		this.onRXGet("zfsAudytUt");
		return this.zfsAudytUt;
	}

	public void setZfsAudytUt(String zfsAudytUt) {
		this.onRXSet("zfsAudytUt", this.zfsAudytUt, zfsAudytUt);
		this.zfsAudytUt = zfsAudytUt;
	}

	public String getZfsCelPozyczki() {
		this.onRXGet("zfsCelPozyczki");
		return this.zfsCelPozyczki;
	}

	public void setZfsCelPozyczki(String zfsCelPozyczki) {
		this.onRXSet("zfsCelPozyczki", this.zfsCelPozyczki, zfsCelPozyczki);
		this.zfsCelPozyczki = zfsCelPozyczki;
	}

	public String getZfsCzyPrzekroczono6k() {
		this.onRXGet("zfsCzyPrzekroczono6k");
		return this.zfsCzyPrzekroczono6k;
	}

	public void setZfsCzyPrzekroczono6k(String zfsCzyPrzekroczono6k) {
		this.onRXSet("zfsCzyPrzekroczono6k", this.zfsCzyPrzekroczono6k, zfsCzyPrzekroczono6k);
		this.zfsCzyPrzekroczono6k = zfsCzyPrzekroczono6k;
	}

	public Date getZfsDataUmowy() {
		this.onRXGet("zfsDataUmowy");
		return this.zfsDataUmowy;
	}

	public void setZfsDataUmowy(Date zfsDataUmowy) {
		this.onRXSet("zfsDataUmowy", this.zfsDataUmowy, zfsDataUmowy);
		this.zfsDataUmowy = zfsDataUmowy;
	}

	public Date getZfsDataWnioskuOZapomoge() {
		this.onRXGet("zfsDataWnioskuOZapomoge");
		return this.zfsDataWnioskuOZapomoge;
	}

	public void setZfsDataWnioskuOZapomoge(Date zfsDataWnioskuOZapomoge) {
		this.onRXSet("zfsDataWnioskuOZapomoge", this.zfsDataWnioskuOZapomoge, zfsDataWnioskuOZapomoge);
		this.zfsDataWnioskuOZapomoge = zfsDataWnioskuOZapomoge;
	}

	public Date getZfsDataZlozeniaWniosku() {
		this.onRXGet("zfsDataZlozeniaWniosku");
		return this.zfsDataZlozeniaWniosku;
	}

	public void setZfsDataZlozeniaWniosku(Date zfsDataZlozeniaWniosku) {
		this.onRXSet("zfsDataZlozeniaWniosku", this.zfsDataZlozeniaWniosku, zfsDataZlozeniaWniosku);
		this.zfsDataZlozeniaWniosku = zfsDataZlozeniaWniosku;
	}

	public Double getZfsDochodNetto() {
		this.onRXGet("zfsDochodNetto");
		return this.zfsDochodNetto;
	}

	public void setZfsDochodNetto(Double zfsDochodNetto) {
		this.onRXSet("zfsDochodNetto", this.zfsDochodNetto, zfsDochodNetto);
		this.zfsDochodNetto = zfsDochodNetto;
	}

	public Double getZfsDochodWRok() {
		this.onRXGet("zfsDochodWRok");
		return this.zfsDochodWRok;
	}

	public void setZfsDochodWRok(Double zfsDochodWRok) {
		this.onRXSet("zfsDochodWRok", this.zfsDochodWRok, zfsDochodWRok);
		this.zfsDochodWRok = zfsDochodWRok;
	}

	public Double getZfsInneDochody() {
		this.onRXGet("zfsInneDochody");
		return this.zfsInneDochody;
	}

	public void setZfsInneDochody(Double zfsInneDochody) {
		this.onRXSet("zfsInneDochody", this.zfsInneDochody, zfsInneDochody);
		this.zfsInneDochody = zfsInneDochody;
	}

	public Double getZfsKwotaDofin() {
		this.onRXGet("zfsKwotaDofin");
		return this.zfsKwotaDofin;
	}

	public void setZfsKwotaDofin(Double zfsKwotaDofin) {
		this.onRXSet("zfsKwotaDofin", this.zfsKwotaDofin, zfsKwotaDofin);
		this.zfsKwotaDofin = zfsKwotaDofin;
	}

	public Double getZfsKwotaInneWplywy() {
		this.onRXGet("zfsKwotaInneWplywy");
		return this.zfsKwotaInneWplywy;
	}

	public void setZfsKwotaInneWplywy(Double zfsKwotaInneWplywy) {
		this.onRXSet("zfsKwotaInneWplywy", this.zfsKwotaInneWplywy, zfsKwotaInneWplywy);
		this.zfsKwotaInneWplywy = zfsKwotaInneWplywy;
	}

	public Double getZfsKwotaPonizej6k() {
		this.onRXGet("zfsKwotaPonizej6k");
		return this.zfsKwotaPonizej6k;
	}

	public void setZfsKwotaPonizej6k(Double zfsKwotaPonizej6k) {
		this.onRXSet("zfsKwotaPonizej6k", this.zfsKwotaPonizej6k, zfsKwotaPonizej6k);
		this.zfsKwotaPonizej6k = zfsKwotaPonizej6k;
	}

	public Double getZfsKwotaPozyczki() {
		this.onRXGet("zfsKwotaPozyczki");
		return this.zfsKwotaPozyczki;
	}

	public void setZfsKwotaPozyczki(Double zfsKwotaPozyczki) {
		this.onRXSet("zfsKwotaPozyczki", this.zfsKwotaPozyczki, zfsKwotaPozyczki);
		this.zfsKwotaPozyczki = zfsKwotaPozyczki;
	}

	public Double getZfsLiczbaRat() {
		this.onRXGet("zfsLiczbaRat");
		return this.zfsLiczbaRat;
	}

	public void setZfsLiczbaRat(Double zfsLiczbaRat) {
		this.onRXSet("zfsLiczbaRat", this.zfsLiczbaRat, zfsLiczbaRat);
		this.zfsLiczbaRat = zfsLiczbaRat;
	}

	public String getZfsMiejsceZlozeniaWniosku() {
		this.onRXGet("zfsMiejsceZlozeniaWniosku");
		return this.zfsMiejsceZlozeniaWniosku;
	}

	public void setZfsMiejsceZlozeniaWniosku(String zfsMiejsceZlozeniaWniosku) {
		this.onRXSet("zfsMiejsceZlozeniaWniosku", this.zfsMiejsceZlozeniaWniosku, zfsMiejsceZlozeniaWniosku);
		this.zfsMiejsceZlozeniaWniosku = zfsMiejsceZlozeniaWniosku;
	}

	public String getZfsMultisCzyTowarzyszaca() {
		this.onRXGet("zfsMultisCzyTowarzyszaca");
		return this.zfsMultisCzyTowarzyszaca;
	}

	public void setZfsMultisCzyTowarzyszaca(String zfsMultisCzyTowarzyszaca) {
		this.onRXSet("zfsMultisCzyTowarzyszaca", this.zfsMultisCzyTowarzyszaca, zfsMultisCzyTowarzyszaca);
		this.zfsMultisCzyTowarzyszaca = zfsMultisCzyTowarzyszaca;
	}

	public Double getZfsMultisFinansDodatkKarty() {
		this.onRXGet("zfsMultisFinansDodatkKarty");
		return this.zfsMultisFinansDodatkKarty;
	}

	public void setZfsMultisFinansDodatkKarty(Double zfsMultisFinansDodatkKarty) {
		this.onRXSet("zfsMultisFinansDodatkKarty", this.zfsMultisFinansDodatkKarty, zfsMultisFinansDodatkKarty);
		this.zfsMultisFinansDodatkKarty = zfsMultisFinansDodatkKarty;
	}

	public String getZfsMultisOsbTowImieNaz() {
		this.onRXGet("zfsMultisOsbTowImieNaz");
		return this.zfsMultisOsbTowImieNaz;
	}

	public void setZfsMultisOsbTowImieNaz(String zfsMultisOsbTowImieNaz) {
		this.onRXSet("zfsMultisOsbTowImieNaz", this.zfsMultisOsbTowImieNaz, zfsMultisOsbTowImieNaz);
		this.zfsMultisOsbTowImieNaz = zfsMultisOsbTowImieNaz;
	}

	public String getZfsMultisOsbTowProgram() {
		this.onRXGet("zfsMultisOsbTowProgram");
		return this.zfsMultisOsbTowProgram;
	}

	public void setZfsMultisOsbTowProgram(String zfsMultisOsbTowProgram) {
		this.onRXSet("zfsMultisOsbTowProgram", this.zfsMultisOsbTowProgram, zfsMultisOsbTowProgram);
		this.zfsMultisOsbTowProgram = zfsMultisOsbTowProgram;
	}

	public String getZfsMultisPotracenieOdMies() {
		this.onRXGet("zfsMultisPotracenieOdMies");
		return this.zfsMultisPotracenieOdMies;
	}

	public void setZfsMultisPotracenieOdMies(String zfsMultisPotracenieOdMies) {
		this.onRXSet("zfsMultisPotracenieOdMies", this.zfsMultisPotracenieOdMies, zfsMultisPotracenieOdMies);
		this.zfsMultisPotracenieOdMies = zfsMultisPotracenieOdMies;
	}

	public Double getZfsMultisPotracenieOdRok() {
		this.onRXGet("zfsMultisPotracenieOdRok");
		return this.zfsMultisPotracenieOdRok;
	}

	public void setZfsMultisPotracenieOdRok(Double zfsMultisPotracenieOdRok) {
		this.onRXSet("zfsMultisPotracenieOdRok", this.zfsMultisPotracenieOdRok, zfsMultisPotracenieOdRok);
		this.zfsMultisPotracenieOdRok = zfsMultisPotracenieOdRok;
	}

	public Double getZfsMultisWartMiesie() {
		this.onRXGet("zfsMultisWartMiesie");
		return this.zfsMultisWartMiesie;
	}

	public void setZfsMultisWartMiesie(Double zfsMultisWartMiesie) {
		this.onRXSet("zfsMultisWartMiesie", this.zfsMultisWartMiesie, zfsMultisWartMiesie);
		this.zfsMultisWartMiesie = zfsMultisWartMiesie;
	}

	public String getZfsMultisport() {
		this.onRXGet("zfsMultisport");
		return this.zfsMultisport;
	}

	public void setZfsMultisport(String zfsMultisport) {
		this.onRXSet("zfsMultisport", this.zfsMultisport, zfsMultisport);
		this.zfsMultisport = zfsMultisport;
	}

	public String getZfsNumer() {
		this.onRXGet("zfsNumer");
		return this.zfsNumer;
	}

	public void setZfsNumer(String zfsNumer) {
		this.onRXSet("zfsNumer", this.zfsNumer, zfsNumer);
		this.zfsNumer = zfsNumer;
	}

	public String getZfsNumerUmowy() {
		this.onRXGet("zfsNumerUmowy");
		return this.zfsNumerUmowy;
	}

	public void setZfsNumerUmowy(String zfsNumerUmowy) {
		this.onRXSet("zfsNumerUmowy", this.zfsNumerUmowy, zfsNumerUmowy);
		this.zfsNumerUmowy = zfsNumerUmowy;
	}

	public String getZfsOpis() {
		this.onRXGet("zfsOpis");
		return this.zfsOpis;
	}

	public void setZfsOpis(String zfsOpis) {
		this.onRXSet("zfsOpis", this.zfsOpis, zfsOpis);
		this.zfsOpis = zfsOpis;
	}

	public String getZfsPowNiePozyczki() {
		this.onRXGet("zfsPowNiePozyczki");
		return this.zfsPowNiePozyczki;
	}

	public void setZfsPowNiePozyczki(String zfsPowNiePozyczki) {
		this.onRXSet("zfsPowNiePozyczki", this.zfsPowNiePozyczki, zfsPowNiePozyczki);
		this.zfsPowNiePozyczki = zfsPowNiePozyczki;
	}

	public String getZfsPowodOdmowy() {
		this.onRXGet("zfsPowodOdmowy");
		return this.zfsPowodOdmowy;
	}

	public void setZfsPowodOdmowy(String zfsPowodOdmowy) {
		this.onRXSet("zfsPowodOdmowy", this.zfsPowodOdmowy, zfsPowodOdmowy);
		this.zfsPowodOdmowy = zfsPowodOdmowy;
	}

	public Long getZfsPrcId() {
		this.onRXGet("zfsPrcId");
		return this.zfsPrcId;
	}

	public void setZfsPrcId(Long zfsPrcId) {
		this.onRXSet("zfsPrcId", this.zfsPrcId, zfsPrcId);
		this.zfsPrcId = zfsPrcId;
	}

	public Double getZfsPropKwotaZapomogi() {
		this.onRXGet("zfsPropKwotaZapomogi");
		return this.zfsPropKwotaZapomogi;
	}

	public void setZfsPropKwotaZapomogi(Double zfsPropKwotaZapomogi) {
		this.onRXSet("zfsPropKwotaZapomogi", this.zfsPropKwotaZapomogi, zfsPropKwotaZapomogi);
		this.zfsPropKwotaZapomogi = zfsPropKwotaZapomogi;
	}

	public Double getZfsSumaFaktur() {
		this.onRXGet("zfsSumaFaktur");
		return this.zfsSumaFaktur;
	}

	public void setZfsSumaFaktur(Double zfsSumaFaktur) {
		this.onRXSet("zfsSumaFaktur", this.zfsSumaFaktur, zfsSumaFaktur);
		this.zfsSumaFaktur = zfsSumaFaktur;
	}

	public String getZfsTyp() {
		this.onRXGet("zfsTyp");
		return this.zfsTyp;
	}

	public void setZfsTyp(String zfsTyp) {
		this.onRXSet("zfsTyp", this.zfsTyp, zfsTyp);
		this.zfsTyp = zfsTyp;
	}

	public Double getZfsZarobkiWnioskodawcy() {
		this.onRXGet("zfsZarobkiWnioskodawcy");
		return this.zfsZarobkiWnioskodawcy;
	}

	public void setZfsZarobkiWnioskodawcy(Double zfsZarobkiWnioskodawcy) {
		this.onRXSet("zfsZarobkiWnioskodawcy", this.zfsZarobkiWnioskodawcy, zfsZarobkiWnioskodawcy);
		this.zfsZarobkiWnioskodawcy = zfsZarobkiWnioskodawcy;
	}

	public Double getZfsZarobkiWspolmalzonka() {
		this.onRXGet("zfsZarobkiWspolmalzonka");
		return this.zfsZarobkiWspolmalzonka;
	}

	public void setZfsZarobkiWspolmalzonka(Double zfsZarobkiWspolmalzonka) {
		this.onRXSet("zfsZarobkiWspolmalzonka", this.zfsZarobkiWspolmalzonka, zfsZarobkiWspolmalzonka);
		this.zfsZarobkiWspolmalzonka = zfsZarobkiWspolmalzonka;
	}

	public List<PptZfssBilety> getPptZfssBileties() {
		this.onRXGet("pptZfssBileties");
		return this.pptZfssBileties;
	}

	public void setPptZfssBileties(List<PptZfssBilety> pptZfssBileties) {
		this.onRXSet("pptZfssBileties", this.pptZfssBileties, pptZfssBileties);
		this.pptZfssBileties = pptZfssBileties;
	}

	public PptZfssBilety addPptZfssBilety(PptZfssBilety pptZfssBilety) {
		this.onRXSet("pptZfssBileties", null, this.pptZfssBileties);
		getPptZfssBileties().add(pptZfssBilety);
		pptZfssBilety.setPptZfssWniosek(this);

		return pptZfssBilety;
	}

	public PptZfssBilety removePptZfssBilety(PptZfssBilety pptZfssBilety) {
		this.onRXSet("pptZfssBileties", null, this.pptZfssBileties);
		getPptZfssBileties().remove(pptZfssBilety);
		pptZfssBilety.setPptZfssWniosek(null);

		return pptZfssBilety;
	}

	public List<PptZfssPoreczyciele> getPptZfssPoreczycieles() {
		this.onRXGet("pptZfssPoreczycieles");
		return this.pptZfssPoreczycieles;
	}

	public void setPptZfssPoreczycieles(List<PptZfssPoreczyciele> pptZfssPoreczycieles) {
		this.onRXSet("pptZfssPoreczycieles", this.pptZfssPoreczycieles, pptZfssPoreczycieles);
		this.pptZfssPoreczycieles = pptZfssPoreczycieles;
	}

	public PptZfssPoreczyciele addPptZfssPoreczyciele(PptZfssPoreczyciele pptZfssPoreczyciele) {
		this.onRXSet("pptZfssPoreczycieles", null, this.pptZfssPoreczycieles);
		getPptZfssPoreczycieles().add(pptZfssPoreczyciele);
		pptZfssPoreczyciele.setPptZfssWniosek(this);

		return pptZfssPoreczyciele;
	}

	public PptZfssPoreczyciele removePptZfssPoreczyciele(PptZfssPoreczyciele pptZfssPoreczyciele) {
		this.onRXSet("pptZfssPoreczycieles", null, this.pptZfssPoreczycieles);
		getPptZfssPoreczycieles().remove(pptZfssPoreczyciele);
		pptZfssPoreczyciele.setPptZfssWniosek(null);

		return pptZfssPoreczyciele;
	}

	public List<PptZfssRodzina> getPptZfssRodzinas() {
		this.onRXGet("pptZfssRodzinas");
		return this.pptZfssRodzinas;
	}

	public void setPptZfssRodzinas(List<PptZfssRodzina> pptZfssRodzinas) {
		this.onRXSet("pptZfssRodzinas", this.pptZfssRodzinas, pptZfssRodzinas);
		this.pptZfssRodzinas = pptZfssRodzinas;
	}

	public PptZfssRodzina addPptZfssRodzina(PptZfssRodzina pptZfssRodzina) {
		this.onRXSet("pptZfssRodzinas", null, this.pptZfssRodzinas);
		getPptZfssRodzinas().add(pptZfssRodzina);
		pptZfssRodzina.setPptZfssWniosek(this);

		return pptZfssRodzina;
	}

	public PptZfssRodzina removePptZfssRodzina(PptZfssRodzina pptZfssRodzina) {
		this.onRXSet("pptZfssRodzinas", null, this.pptZfssRodzinas);
		getPptZfssRodzinas().remove(pptZfssRodzina);
		pptZfssRodzina.setPptZfssWniosek(null);

		return pptZfssRodzina;
	}

	public PptZfssWniosek getPptZfssWniosek() {
		this.onRXGet("pptZfssWniosek");
		return this.pptZfssWniosek;
	}

	public void setPptZfssWniosek(PptZfssWniosek pptZfssWniosek) {
		this.onRXSet("pptZfssWniosek", this.pptZfssWniosek, pptZfssWniosek);
		this.pptZfssWniosek = pptZfssWniosek;
	}

	public List<PptZfssWniosek> getPptZfssWnioseks() {
		this.onRXGet("pptZfssWnioseks");
		return this.pptZfssWnioseks;
	}

	public void setPptZfssWnioseks(List<PptZfssWniosek> pptZfssWnioseks) {
		this.onRXSet("pptZfssWnioseks", this.pptZfssWnioseks, pptZfssWnioseks);
		this.pptZfssWnioseks = pptZfssWnioseks;
	}

	public PptZfssWniosek addPptZfssWniosek(PptZfssWniosek pptZfssWniosek) {
		getPptZfssWnioseks().add(pptZfssWniosek);
		pptZfssWniosek.setPptZfssWniosek(this);

		return pptZfssWniosek;
	}

	public PptZfssWniosek removePptZfssWniosek(PptZfssWniosek pptZfssWniosek) {
		getPptZfssWnioseks().remove(pptZfssWniosek);
		pptZfssWniosek.setPptZfssWniosek(null);

		return pptZfssWniosek;
	}

	public Double getZfsMultisWartMiesieDziecko() {
		this.onRXGet("zfsMultisWartMiesieDziecko");
		return zfsMultisWartMiesieDziecko;
	}

	public void setZfsMultisWartMiesieDziecko(Double zfsMultisWartMiesieDziecko) {
		this.onRXSet("zfsMultisWartMiesieDziecko", this.zfsMultisWartMiesieDziecko, zfsMultisWartMiesieDziecko);
		this.zfsMultisWartMiesieDziecko = zfsMultisWartMiesieDziecko;
	}

	public String getZfsMultisDzieckoProgram() {
		this.onRXGet("zfsMultisDzieckoProgram");
		return zfsMultisDzieckoProgram;
	}

	public void setZfsMultisDzieckoProgram(String zfsMultisDzieckoProgram) {
		this.onRXSet("zfsMultisDzieckoProgram", this.zfsMultisDzieckoProgram, zfsMultisDzieckoProgram);
		this.zfsMultisDzieckoProgram = zfsMultisDzieckoProgram;
	}

}
