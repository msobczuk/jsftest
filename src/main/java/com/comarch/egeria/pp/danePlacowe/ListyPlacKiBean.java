package com.comarch.egeria.pp.danePlacowe;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import com.lowagie.text.DocumentException;
import net.sf.jasperreports.engine.JRException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.addDays;
import static com.comarch.egeria.Utils.nz;

@Named
@Scope("view")
public class ListyPlacKiBean extends SqlBean implements Serializable {
	private static final Logger log = LogManager.getLogger();
	private static final long serialVersionUID = 1L;

	@Inject
	User user;

	public String okres = null; //"2018-02"

	@PostConstruct
	public void init(){
		ArrayList<DataRow> okrData = this.getLwOkresy().getData();
		if (!okrData.isEmpty())
			this.okres = okrData.get(0).getAsString("OKRES");
	}

	public StreamedContent drukuj() throws IOException, DocumentException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
		this.getSqlDataCache().loadTBTS();
		//byte[] bytes = ReportGenerator.generujSlElToPdfData("LISTA_PLAC_KI_EL2PDF");

		byte[] bytes = ReportGenerator.generujSlElToPdfData(this.getSL("LISTA_PLAC_KI_EL2PDF"));

		return Utils.asStreamedContent(bytes, "application/pdf", "Wynagrodzenie " + this.okres + ".pdf");
	}

	public StreamedContent getPdfRMUA() throws IOException, DocumentException, ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException, JRException {
//		HashMap<String, Object> parameters = new HashMap<>();
//
//		LocalDate dataOd = LocalDate.of(Integer.parseInt(this.getOkres().substring(0,4)), Integer.parseInt(this.getOkres().substring(5, 7)), 1);
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
//		String formattedString = dataOd.format(formatter);
//
//		parameters.put("p_frm_id", (long) user.getFrmId());
//		parameters.put("p_prc_id", user.getPrcIdLong());
//		parameters.put("p_data_od", Date.from(dataOd.atStartOfDay(ZoneId.systemDefault()).toInstant()));
//		parameters.put("p_miesiac", java.sql.Date.valueOf(dataOd));
//		parameters.put("p_data", formattedString);
//
//		return  new DefaultStreamedContent(ReportGenerator.pdfAsStream("RMUA", parameters), "application/pdf", "rmua_" + this.okres + ".pdf");

		return drukuj();

	}

	public SqlDataSelectionsHandler getLwGrupySkl(){
		return this.getLW("PPL_LISTA_PLAC_KI_GRUPY");
	}

	public SqlDataSelectionsHandler getLwSkladniki(int grIndex){
		if (this.getLwGrupySkl()==null || this.getLwGrupySkl().getData().size()<grIndex)
			return null;
		DataRow rgr = this.getLwGrupySkl().getData().get(grIndex - 1);

		SqlDataSelectionsHandler ret = getLW("PPL_LISTA_PLAC_KI", this.getOkres(), rgr.getAsString("dg_kod"));

		if (ret!=null)
			ret.setName(rgr.getAsString( "dg_nazwa" ));

		return ret;
	}

	public SqlDataSelectionsHandler getLwSkladniki(){
		return getLW("PPL_LISTA_PLAC_KI", this.getOkres(), "%" ) ;
	}

	public List<DataRow> getSkladniki(String dgKod){
		return this.getLwSkladniki().getData().stream().filter(r->eq(dgKod,r.getAsString("dg_kod"))).collect(Collectors.toList());
	}

	public double getSkladnikiSuma(String dgKod){
		return this.getSkladniki(dgKod).stream().mapToDouble(r->r.getAsDouble("sk_wartosc")).sum();
	}

	public SqlDataSelectionsHandler getLwOkresy(){
		return this.getLW("PPL_LISTA_PLAC_KI_OKRESY");
	}

	public List<String> getOkresy(){
		return this.getLwOkresy().getData().stream().map(r->r.getAsString("OKRES")).collect(Collectors.toList());
	}

	public String getOkres() {
		return okres;
	}

	public void setOkres(String okres) {
		boolean eq = eq(okres, this.okres);
		this.okres = okres;
		if (!eq){
			this.getLwSkladniki().resetTs();
			Utils.updateComponent("frmCntr");
		}
	}

	public void raportRMUA(DataRow r) {
		if (r==null) return;

		HashMap<String, Object> parameters = new HashMap<>();

		String reportName;

		String rokres = "" + r.get("okres");
		String czyRoczne = "" + r.get("roczne");

		parameters.put("p_frm_id", (long) user.getFrmId());
		parameters.put("p_prc_id", user.getPrcIdLong());
		parameters.put("p_identyfikator_rap", "" + r.get("identyfikator_rmua"));

		if ("T".equals(czyRoczne)) {
			reportName = "RMUA_roczne";

			parameters.put("p_rok", "" + r.get("rok"));

		} else {
			reportName = "RMUA";

			SqlDataSelectionsHandler ret = getLW("PPL_RMUA_ABSCENCJE_PRC");
			Map<String, Object> sqlNamedParams = new HashMap<>();

			LocalDate dataOd = LocalDate.of(Integer.parseInt(rokres.substring(0,4)), Integer.parseInt(rokres.substring(5, 7)), 1);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
			String formattedString = dataOd.format(formatter);

			parameters.put("p_data", formattedString);
			parameters.put("p_miesiac", java.sql.Date.valueOf(dataOd));

			sqlNamedParams.put("P_DATA_OD", formattedString);
			sqlNamedParams.put("P_DATA_DO", formattedString);
			sqlNamedParams.put("P_PRC_ID", user.getPrcIdLong());

			ret.setSqlNamedParams(sqlNamedParams);

			ArrayList<HashMap<String, String>> absences = new ArrayList<>();
			prepareAbsences(ret.getData(), absences);

			parameters.put("p_absencje", absences);
		}

		printReport(parameters, reportName);
	}

	public static void prepareAbsences(ArrayList<DataRow> absencesList, ArrayList<HashMap<String, String>> absences) {
		String opis;
		String opisPop = "";
		Date dataOd;
		Date dataDo;
		int dskId = 0;
		String kodZus;
		int dni;
		int i = 0;
		double kwota;
		Date dataOdPop = null;
		Date dataDoPop = null;
		int dskIdPop = 0;
		String kodZusPop = "";
		int dniPop = 0;
		double kwotaPop = 0;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setLenient( false );
		SimpleDateFormat sdfMiesiac = new SimpleDateFormat("MM-yyyy");
		sdf.setLenient( false );
		DecimalFormat df = new DecimalFormat("#.##");
		df.setMinimumFractionDigits(2);
		String miesiac;
		String miesiacPop = "";

		for (DataRow dataRow : absencesList) {
			i++;
			dataOd = dataRow.getAsDate("ab_data_od");
			dataDo = dataRow.getAsDate("ab_data_do");
			dni = dataRow.getAsLong("ab_dni_wykorzystane").intValue();
			kwota = dataRow.getAsDouble("kwota");
			dskId = dataRow.getAsLong("dsk_id") != null ? dataRow.getAsLong("dsk_id").intValue() : dskId;
			kodZus = dataRow.getAsString("ab_kod_zus");
			miesiac = dataRow.getAsString("miesiac");
			opis = dataRow.getAsString("opis");

			if (i == 1) {//pierwszy rekord
				dskIdPop = dskId;
				dataOdPop = dataOd;
				dataDoPop = dataDo;
				kodZusPop = kodZus;
				dniPop = dni;
				kwotaPop = kwota;
				opisPop = opis;
				miesiacPop = miesiac;

			} else if ((dskId == dskIdPop) && (kodZus.equals(kodZusPop)) && (miesiac.equals(miesiacPop)) && (sdf.format(dataOd).equals(sdf.format(addDays(dataDoPop, 1))))) {
				dataDoPop = dataDo;
				dniPop = dniPop + dni;
				kwotaPop = kwotaPop + kwota;

			} else {
				addAbsenceToList(absences, opisPop, dataOdPop, dataDoPop, kodZusPop, dniPop, kwotaPop, sdf, sdfMiesiac, df);

				dskIdPop = dskId;
				dataOdPop = dataOd;
				dataDoPop = dataDo;
				kodZusPop = kodZus;
				dniPop = dni;
				kwotaPop = kwota;
				opisPop = opis;
				miesiacPop = miesiac;
			}
		}

		// dodaje ostatni element do ArrayList
		if (i > 0) {
			addAbsenceToList(absences, opisPop, dataOdPop, dataDoPop, kodZusPop, dniPop, kwotaPop, sdf, sdfMiesiac, df);
		}

	}

	private static void addAbsenceToList(ArrayList<HashMap<String, String>> absences, String opisPop,
										 Date dataOdPop, Date dataDoPop, String kodZusPop, int dniPop,
										 double kwotaPop, SimpleDateFormat sdf, SimpleDateFormat sdfMiesiac, DecimalFormat df) {
		HashMap<String, String> absenceRow = new HashMap<>();
		absenceRow.put("kod", kodZusPop);
		absenceRow.put("data_od", sdf.format(dataOdPop));
		absenceRow.put("data_do", sdf.format(dataDoPop));
		absenceRow.put("il_dni", String.valueOf(dniPop));
		absenceRow.put("kwota", df.format(kwotaPop));
		absenceRow.put("opis", opisPop);
		absenceRow.put("miesiac", sdfMiesiac.format(dataOdPop));
		absences.add(absenceRow);
	}

	private void printReport(HashMap<String, Object> parameters, String reportName) {
		try {
			ReportGenerator.displayReportAsPDF(parameters, reportName);
		} catch (JRException | IOException | SQLException e) {
			User.alert(e);
			log.error(e.getMessage(), e);
		}
	}

}