package com.comarch.egeria.pp.danepracownika.pracownik;

import static com.comarch.egeria.Utils.*;
import static java.lang.Math.toIntExact;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIViewRoot;
import javax.inject.Inject;
import javax.inject.Named;

import com.comarch.egeria.web.common.utils.constants.Const;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.utils.auth.EgrLoginBean;

@ManagedBean
@Named
@Scope("session")
public class PracownikPowiadomieniaSqlBean extends SqlBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
//	private static final String APP_CONTEXT = "PortalPracowniczy5";
	
	private int numberOfUnreadNotifications;
	private int previousNumberOfUnreadNotifications = 0;
	private DataRow selectedPowiadomienie = null;
	private String trescPowiadomienia = null;
	
	public UIViewRoot viewState;

	
	@Inject
	EgrLoginBean egrLoginBean;
	
	@PreDestroy
	private void predestroy() {
		this.egrLoginBean = null;
	}
	
	public DataRow getRow(Object wdmId){
		SqlDataSelectionsHandler lw = this.getLW( "PPL_ADM_POWIADOMIENIA", egrLoginBean.getUsername().toUpperCase() );
		
		if (lw==null || lw.size()==0)
			return null;

		return lw.get(wdmId);
	}
	
	
	public void reLoad(String wdmId) {
		this.getLW( "PPL_ADM_POWIADOMIENIA", egrLoginBean.getUsername().toUpperCase() ).reLoadDataRow(wdmId);
		userInfoIleNowychPowiadomien();
	}

	public void reLoad() {
		this.getLW( "PPL_ADM_POWIADOMIENIA", egrLoginBean.getUsername().toUpperCase() ).reLoadData();
		userInfoIleNowychPowiadomien();
	}

	
	public void userInfoIleNowychPowiadomien() {
		int diff = getNumberOfUnreadNotifications() - previousNumberOfUnreadNotifications;
		if (diff > 0)
			if (diff == 1)
				User.info("Masz " + diff + " nowe  powiadomienie.");
			else if (diff == 2 || diff == 3 || diff == 4)
				User.info("Masz " + diff + " nowe  powiadomienia.");
			else
				User.info("Masz " + diff + " nowych  powiadomień.");
	}

	
	
	public String formatujCHR10BR(String txt) {
		if(txt == null) return null;
		txt = txt.replace("\r\n", "<br/>");
		txt = txt.replace("\r", "<br/>");
		txt = txt.replace("\n", "<br/>");
		return txt; 
	}


	public DataRow getSelectedPowiadomienie() {

		return selectedPowiadomienie;

	}

	public void setSelectedPowiadomienie(DataRow selectedPowiadomienie) {
		this.selectedPowiadomienie = selectedPowiadomienie;
	}

	
	public void wczytajWyswietlPowiadomienie(DataRow selectedPowiadomienie)  {
		 
		if (selectedPowiadomienie == null || selectedPowiadomienie.equals(this.selectedPowiadomienie)) return;
		
		this.selectedPowiadomienie = selectedPowiadomienie;
		
		try {
			
			this.trescPowiadomienia = "";
			Object clb = this.execScalarQuery( "SELECT WDM_TRESC FROM EAADM.EAT_WIADOMOSCI  WHERE WDM_ID = ?", selectedPowiadomienie.get("wdm_id") );
			this.trescPowiadomienia = formatujCHR10BR(  asString((Clob) clb) );
			wstawLink();
		
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		setNotificationRead(this.selectedPowiadomienie);
		
		com.comarch.egeria.Utils.update("dlgTrescPowiadomieniaId");
	}
	
	private void wstawLink() {
		String link = null;
		
		int beginIndex = this.trescPowiadomienia.indexOf("http://");
		if (beginIndex == -1)
			beginIndex = this.trescPowiadomienia.indexOf("https://");
		
		if (beginIndex == -1)
			return;
		
		String fragmentZLinkiem = this.trescPowiadomienia.substring(beginIndex);
		String calyLink = "";
		
		if (fragmentZLinkiem == null)
			return;
		
		int endIndex = fragmentZLinkiem.indexOf("<br/>");
		if (endIndex == -1)
			endIndex = fragmentZLinkiem.indexOf(" ");
		
		if (endIndex == -1)
			calyLink = fragmentZLinkiem;
		else
			calyLink = fragmentZLinkiem.substring(0, endIndex);
		
		int index = calyLink.indexOf(Const.HOME_REDIRECT_URL + "/") + (Const.HOME_REDIRECT_URL + "/").length();
		
		link = calyLink.substring(index);
		
		String html = "<button class=\"ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-left\" "
					+ "onclick=\"PrimeFaces.bcn(this,event,[function(event){PF('dlgPowiadomienia').hide(); "
					+ "parent.setIframeContentSrc('" + link + "')}]);\" "
					+ "type=\"submit\" role=\"button\" aria-disabled=\"false\"> "
					+ "<i class=\"material-icons pp-commandbutton-icon\" style=\"font-size:null\">launch</i> "
					+ "<span class=\"ui-button-text ui-c\">" + calyLink + "</span> </button>";
		
		this.trescPowiadomienia = this.trescPowiadomienia.replace(calyLink, html);		
	}

	public int getNumberOfUnreadNotifications() {
		try {

			SqlDataSelectionsHandler lw = this.getLW("PPL_ADM_POWIADOMIENIA", egrLoginBean.getUsername().toUpperCase() );
			if ( lw == null || lw.getData().size()==0)
				return 0;
			else {
				previousNumberOfUnreadNotifications = numberOfUnreadNotifications;
				
				numberOfUnreadNotifications = 0;
				numberOfUnreadNotifications = toIntExact( lw.getOriginalData().stream()
						.filter(dr -> dr.get("WDM_F_PRZECZYTANA").equals("N")).count());

				return numberOfUnreadNotifications;
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		return 0;
	}
	
	
	public void setNotificationRead(DataRow dr){
		
		PreparedStatement preparedStatement = null;

		if(dr.get("WDM_F_PRZECZYTANA").equals("T")) 
			return;
		
		try (Connection conn = JdbcUtils.getConnection()) {
			
			String updateString = "update EAADM.EAT_WIADOMOSCI set WDM_F_PRZECZYTANA = 'T' WHERE WDM_ID = ?";
			preparedStatement = conn.prepareStatement(updateString);
			preparedStatement.setBigDecimal (1,(BigDecimal)dr.get("WDM_ID"));
			preparedStatement.executeUpdate();

			this.reloadAndUpdate(selectedPowiadomienie);
			
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
		}
	}
	

	public void setNumberOfUnreadNotifications(int numberOfUnreadNotifications) {
		this.numberOfUnreadNotifications = numberOfUnreadNotifications;
	}

	public String getTrescPowiadomienia() {
		return trescPowiadomienia;
	}

	public void setTrescPowiadomienia(String trescPowiadomienia) {
		this.trescPowiadomienia = trescPowiadomienia;
	}

}
