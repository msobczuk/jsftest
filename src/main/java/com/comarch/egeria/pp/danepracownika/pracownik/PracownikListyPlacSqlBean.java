package com.comarch.egeria.pp.danepracownika.pracownik;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.inject.Named;

import com.comarch.egeria.pp.danePlacowe.ListyPlacKiBean;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@Named
@Scope("view")
public class PracownikListyPlacSqlBean extends SqlBean implements Serializable {
	
	private static final Logger log = LogManager.getLogger();

	@Inject
	User user;
	
	private static final long serialVersionUID = 1L;
	
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
	private String okres = LocalDate.now().format(formatter);
	ArrayList<String> okresy = null;
	ArrayList<String> rodzajeListy = new ArrayList<>();
	ArrayList<String> nazwyListy = new ArrayList<>();
	ArrayList<String> selectedRodzajeListy = new ArrayList<>();
	ArrayList<String> selectedNazwyListy = new ArrayList<>();
	ArrayList<ListaPlac> listyPlac = new ArrayList<>();
	TreeMap<Integer, ArrayList<DataRow>> filteredSlownik = new TreeMap<Integer, ArrayList<DataRow>>();
	

	
	public void reportPasekMF() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		HashMap<String, Object> parameters = new HashMap<>();
		
		LocalDate date = LocalDate.of(Integer.parseInt(this.getOkres().substring(0,4)), Integer.parseInt(this.getOkres().substring(5, 7)), 1);

		String sql =
				"  INSERT INTO EGADM1.EK_TMP_RAPORTY 	( trp_id, 			   				 trp_uruch_id,	trp_rap_nazwa,   	trp_prc_id 	) "
																			+ "VALUES ( ek_seq_trp.NEXTVAL, 	?,							'PASEK' ,    		? 					) " ;
		
		Object uruchId = this.execScalarQuery("select ek_seq_uruch.NEXTVAL from dual");
		Long prcid = user.getPrcIdLong();
		//long frmId = 1;
		this.execNoQuery(sql, uruchId, prcid);
		this.execNoQuery("DELETE FROM EAADM.EATT_LST");
		
		ArrayList<ListaPlac> zaznaczoneListyPlac = getZaznaczoneListyPlac();
		for (ListaPlac lista : zaznaczoneListyPlac) {
			String sqlEattLst = "INSERT INTO EAADM.EATT_LST VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";
			this.execNoQuery(sqlEattLst, lista.getTmpLista(), lista.getTmpId(), lista.getLstNumer(),
					lista.getLstNazwa(), lista.getLstDataWyplaty(), lista.getLstDataObliczen(), lista.getLstId(),
					"0", "0", "0");
		}
		
		Long sesjaId = null;
		try(Connection conn = JdbcUtils.getConnection()) {
			sesjaId = JdbcUtils.sqlFnLong(conn, "eap_lst_tmp.utrwal");
		}
		
		parameters.put("p_trp_id", uruchId.toString());   
		parameters.put("p_rodzaj_listy", "%");
		parameters.put("p_odz_id", 1);
		parameters.put("p_data_od", java.sql.Date.valueOf(date.with(TemporalAdjusters.firstDayOfMonth())));
		parameters.put("p_data_do", java.sql.Date.valueOf(date.with(TemporalAdjusters.lastDayOfMonth())));
		parameters.put("p_lst_id_lista", "EK_LISTY2");
		parameters.put("p_warunek", "AND lst_id IN (select to_number(tmp_pole_5) from eatt_lst where tmp_lista = $P{p_lst_id_lista})");
		
		try {
			ReportGenerator.displayReportAsPDF(parameters, "pasekmf");
		} catch (JRException|IOException e) {
			log.error("{}", e);
		}
	
	}

	public void reportRmuaZaMiesiac() throws SQLException {		
		HashMap<String, Object> parameters = new HashMap<>();

		LocalDate dataOd = LocalDate.of(Integer.parseInt(this.getOkres().substring(0,4)), Integer.parseInt(this.getOkres().substring(5, 7)), 1);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		String formattedString = dataOd.format(formatter);

		parameters.put("p_frm_id", (long) user.getFrmId());
		parameters.put("p_prc_id", user.getPrcIdLong());
		parameters.put("p_identyfikator_rap", dataOd.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		parameters.put("p_miesiac", java.sql.Date.valueOf(dataOd));
		parameters.put("p_data", formattedString);

		SqlDataSelectionsHandler ret = getLW("PPL_RMUA_ABSCENCJE_PRC");
		Map<String, Object> sqlNamedParams = new HashMap<>();

		sqlNamedParams.put("P_DATA_OD", formattedString);
		sqlNamedParams.put("P_DATA_DO", formattedString);
		sqlNamedParams.put("P_PRC_ID", user.getPrcIdLong());

		ret.setSqlNamedParams(sqlNamedParams);

		ArrayList<HashMap<String, String>> absences = new ArrayList<>();
		ListyPlacKiBean.prepareAbsences(ret.getData(), absences);

		parameters.put("p_absencje", absences);
		
		try {
			ReportGenerator.displayReportAsPDF(parameters, "RMUA");
		} catch (JRException|IOException e) {
			log.error(e.getMessage(), e);
		}
	}
	

	private SqlDataSelectionsHandler slownikListyPlac = new SqlDataSelectionsHandler();

	public void filterSlownik() {
		this.filteredSlownik.clear();
		for (DataRow dr : this.getListyPlacData()) {
			if (this.okres.equals(dr.get("PP_LST_OKRES").toString())
					&& selectedRodzajeListy.contains((dr.get("PP_DRL_NAZWA")).toString())
					&& selectedNazwyListy.contains((dr.get("PP_LST_NAZWA")).toString())) {

				Integer tmpint = Integer.valueOf(dr.get("PP_DG_NUMER").toString());
				ArrayList<DataRow> arr = this.filteredSlownik.get(tmpint);
				if (arr==null) {
					arr = new ArrayList<>();
					this.filteredSlownik.put(tmpint, arr);
				}
				//arr.add(dr); //po staremu - bez sumowania po PP_DSK_KOD
//				dopiszLubDodajWartoscWg_PP_DSK_KOD(arr, dr);
				arr.add(dr.clone());
			}
		}
		// wypiszMape();
	}


	public void okresChanged() {
		updateRodzajeListy();
	}

	public void rodzajeChanged() {
		updateNazwyListy();
	}
	
	public Object liczSume(String numerGr){
		BigDecimal sum = BigDecimal.ZERO;
		for (DataRow dr : filteredSlownik.get(Integer.valueOf(numerGr))){
			sum = sum.add(new BigDecimal(dr.get("PP_SK_WARTOSC").toString()));
		}
		return sum;
	}

	public void updateRodzajeListy() {
//		System.out.println("Updejtuje liste rodzajelisty <<<<<<<<<<<<<<<<<");
		this.rodzajeListy.clear();
		String tmp = null;
		for (DataRow dr : this.getListyPlacData()) {
			if (this.okres.equals(dr.get("PP_LST_OKRES").toString())) {
				tmp = (dr.get("PP_DRL_NAZWA")).toString();
				if (!this.rodzajeListy.contains(tmp))
					this.rodzajeListy.add(tmp);
			}
		}
		this.selectedRodzajeListy = this.rodzajeListy;
		updateNazwyListy();
	}

	public void wypiszMape() {
		System.out.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		for (Integer it : filteredSlownik.keySet()) {
			System.out.println("+++++ grupa:" + it );
			for (DataRow dr : filteredSlownik.get(it)) {
				System.out.println("++ " + dr);
			}
		}
	}

	public void updateNazwyListy() {
//		System.out.println("Updejtuje liste nazwylisty <<<<<<<<<<<<<<<<<");
		this.nazwyListy.clear();
		String tmp = null;
		for (DataRow dr : this.getListyPlacData()) {
			if (this.okres.equals(dr.get("PP_LST_OKRES").toString())
					&& selectedRodzajeListy.contains((dr.get("PP_DRL_NAZWA")).toString())) {
				tmp = (dr.get("PP_LST_NAZWA")).toString();
				if (!this.nazwyListy.contains(tmp))
					this.nazwyListy.add(tmp);
			}
		}
		this.selectedNazwyListy = this.nazwyListy;

	}

	public ArrayList<DataRow> getListyPlacData() {
		slownikListyPlac = this.getLW("PPL_PRC_LISTA_PLAC", user.getPrcIdLong(), this.okres);
		return this.slownikListyPlac.getData();
	}

	public void setListyPlacData(ArrayList<DataRow> ListyPlacData) {
	}

	public String getTextFilterValue() {
		return this.slownikListyPlac.getTextFilter();
	}

	public void setTextFilterValue(String textFilterValue) {
		this.slownikListyPlac.setTextFilter(textFilterValue);
	}

	public String getOkres() {
		return okres;
	}

	public void setOkres(String okres) {
		this.okres = okres;
	}

	public ArrayList<String> getOkresy() {

		this.getLW("PPL_PRC_LISTA_PLAC_OKRESY", user.getPrcIdLong());
		
		if (this.okresy == null) {
			okresy = new ArrayList<>();
			this.okresy.add(okres);
			String tmp = null;

			for (DataRow dr : this.getLW("PPL_PRC_LISTA_PLAC_OKRESY", user.getPrcIdLong()).getData() ) {
				tmp = (dr.get("PP_LST_OKRES")).toString();
				if (!this.okresy.contains(tmp))
					this.okresy.add(tmp);
			}

		}
		/*if (okresy.size()==1){
			Date d = new Date();
			int r1 = d.getYear()+1900;
			int m1 = d.getMonth();
			
			while (m1>9){
				okresy.add(""+r1+"-"+m1);
				m1--;
			}
			while (m1>0){
				okresy.add(""+r1+"-0"+m1);
				m1--;
			}
			
			
		}*/
		
		return okresy;
	}

	public void setOkresy(ArrayList<String> okresy) {
		this.okresy = okresy;
	}

	public ArrayList<String> getRodzajeListy() {
		if (this.rodzajeListy.isEmpty()) {
			updateRodzajeListy();
		}

		return rodzajeListy;
	}

	public void setRodzajeListy(ArrayList<String> rodzajeListy) {
		this.rodzajeListy = rodzajeListy;
	}

	public ArrayList<String> getSelectedRodzajeListy() {
		return selectedRodzajeListy;
	}

	public void setSelectedRodzajeListy(ArrayList<String> selectedRodzajeListy) {
		this.selectedRodzajeListy = selectedRodzajeListy;
	}

	// testowa metoda do sprawdzenia dobrego oznaczania
	public void wypiszWybrane() {
		System.out.println("=================================================================");
		System.out.println("Wybrane rodzaje:");
		for (String str : selectedRodzajeListy) {
			System.out.println("Wybrany rodzaj: " + str);
		}
		System.out.println("=================================================================");
		System.out.println("Wybrane nazwy:");
		for (String str : selectedNazwyListy) {
			System.out.println("Wybrana nazwa: " + str);
		}
	}

	public ArrayList<String> getNazwyListy() {

		if (this.nazwyListy.isEmpty()) {
			updateNazwyListy();
		}

		return nazwyListy;
	}

	public void setNazwyListy(ArrayList<String> nazwyListy) {
		this.nazwyListy = nazwyListy;
	}

	public ArrayList<String> getSelectedNazwyListy() {
		return selectedNazwyListy;
	}

	public void setSelectedNazwyListy(ArrayList<String> selectedNazwyListy) {
		this.selectedNazwyListy = selectedNazwyListy;
	}

	public SqlDataSelectionsHandler getSlownikListyPlac() {
		return slownikListyPlac;
	}

	public void setSlownikListyPlac(SqlDataSelectionsHandler slownikListyPlac) {
		this.slownikListyPlac = slownikListyPlac;
	}

	/*public ArrayList<DataRow> getFilteredSlownik() {
		filterSlownik();
		return filteredSlownik;
	}

	public void setFilteredSlownik(ArrayList<DataRow> filteredSlownik) {
		this.filteredSlownik = filteredSlownik;
	}*/

	public Map<Integer, ArrayList<DataRow>> getFilteredSlownik() {
		filterSlownik();
//		BYŁ PROBLEM Z NULLEM JEZELI KOLEKCJA BYLA ZWRACANA PUSTA
//		DO SPRAWDZENIA POPRAWNOSC GDY BEDA DANE W TABELI
		if(filteredSlownik.isEmpty())
			return null;
		return filteredSlownik;
	}

	public void setFilteredSlownik(TreeMap<Integer, ArrayList<DataRow>> filteredSlownik) {
		this.filteredSlownik = filteredSlownik;
	}
	
	public ArrayList<ListaPlac> getListyPlac() {
		this.listyPlac.clear();
		for (DataRow dr : this.getListyPlacData()) {
			ListaPlac lista = new ListaPlac();
			
			lista.setTmpLista("EK_LISTY2");
			lista.setTmpId(null);
			lista.setLstNumer((dr.get("PP_LST_NUMER")).toString());
			lista.setLstNazwa((dr.get("PP_LST_TEMAT")).toString());
			lista.setLstDataWyplaty((dr.get("PP_LST_DATA_WYPLATY")).toString());
			lista.setLstDataObliczen((dr.get("PP_LST_DATA_OBLICZEN")).toString());
			lista.setLstId((dr.get("PP_LST_ID")).toString());
			
			if (!this.listyPlac.contains(lista))
				this.listyPlac.add(lista);
		}
				
		return listyPlac;
	}
	
	public ArrayList<ListaPlac> getZaznaczoneListyPlac() {
		//this.listyPlac.clear();
		
		ArrayList<ListaPlac> ret = new ArrayList<ListaPlac>();
		ArrayList<String> addedLstIds = new ArrayList<String>();
		
		for (DataRow dr : getZaznaczoneSkladnikiListyPlacDR() ) {
			String lstId = (dr.get("PP_LST_ID")).toString();

			if (addedLstIds.contains(lstId))
				continue;
			
			addedLstIds.add(lstId);
			
			ListaPlac lista = new ListaPlac();
			lista.setTmpLista("EK_LISTY2");
			lista.setTmpId(null);
			lista.setLstNumer((dr.get("PP_LST_NUMER")).toString());
			lista.setLstNazwa((dr.get("PP_LST_TEMAT")).toString());
			lista.setLstDataWyplaty((dr.get("PP_LST_DATA_WYPLATY")).toString());
			lista.setLstDataObliczen((dr.get("PP_LST_DATA_OBLICZEN")).toString());
			lista.setLstId(lstId);
			
			if (!ret.contains(lista))
				ret.add(lista);
		}
				
		return ret;
	}
	
	
	public ArrayList<DataRow> getZaznaczoneSkladnikiListyPlacDR(){
		ArrayList<DataRow> ret = new ArrayList<DataRow>();
		for (DataRow dr : this.getListyPlacData()) {
			if (this.okres.equals(dr.get("PP_LST_OKRES").toString())
					&& selectedRodzajeListy.contains((dr.get("PP_DRL_NAZWA")).toString())
					&& selectedNazwyListy.contains((dr.get("PP_LST_NAZWA")).toString())){
				ret.add(dr);
				
			}
		}
		return ret;
	}
	
	 // tabela tymczasowa eatt_lst 
	class ListaPlac {
		private String tmpLista;
		private String tmpId;
		private String lstNumer;
		private String lstNazwa;
		private String lstDataWyplaty;
		private String lstDataObliczen;
		private String lstId;
//		private String tmpPole6;
//		private String tmpPole7;
//		private String tmpPole8;
		
		public String getTmpLista() {
			return tmpLista;
		}
		
		public void setTmpLista(String tmpLista) {
			this.tmpLista = tmpLista;
		}
		
		public String getTmpId() {
			return tmpId;
		}
		
		public void setTmpId(String tmpId) {
			this.tmpId = tmpId;
		}
		
		public String getLstNumer() {
			return lstNumer;
		}
		
		public void setLstNumer(String lstNumer) {
			this.lstNumer = lstNumer;
		}
		
		public String getLstNazwa() {
			return lstNazwa;
		}
		
		public void setLstNazwa(String lstNazwa) {
			this.lstNazwa = lstNazwa;
		}
		
		public String getLstDataWyplaty() {
			return lstDataWyplaty;
		}
		
		public void setLstDataWyplaty(String lstDataWyplaty) {
			this.lstDataWyplaty = lstDataWyplaty;
		}
		
		public String getLstDataObliczen() {
			return lstDataObliczen;
		}
		
		public void setLstDataObliczen(String lstDataObliczen) {
			this.lstDataObliczen = lstDataObliczen;
		}
		
		public String getLstId() {
			return lstId;
		}
		
		public void setLstId(String lstId) {
			this.lstId = lstId;
		}
		
	}

}
//public void raport(String rapNazwa) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
////if(umowaDR != null){
//HashMap<String, Object> params = new HashMap<String, Object>();
////params.put("id_umowy", umowaDR.getIdAsLong());
//
//
//String mstr = this.getOkres().substring(5, 7);
//int m = Integer.parseInt( mstr );
//int r = Integer.parseInt( this.getOkres().substring(0,4) );
//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//Date date = new Date();
//
//params.put("p_rok", r);
//params.put("p_miesiac",  m ); //"2017-07"
//
//params.put("p_rodzaj_listy", "LPLAC");
//params.put("p_typ_daty_listy", "ZA_MIESIAC");
//params.put("p_grupowanie", "BRAK");
//params.put("p_podsumowanie", "T");
//params.put("p_dk_kod", "PASEK");
//params.put("p_oswiadczenie", "T");
//params.put("p_data_wypelnienia", sdf.format(date));
//params.put("p_tresc_pod"," .. ");
////params.put("P_TRP_ID", 106653);   //OBEJSCIE API z EK_PCK_TMP . wstawianie pracownikow do EK_TMP_RAPORTY
//
//Object uruchId = this.execScalarQuery("select ek_seq_uruch.NEXTVAL from dual");
//String sql =
//		"  INSERT INTO EGADM1.EK_TMP_RAPORTY 	( trp_id, 			   				 trp_uruch_id,	trp_rap_nazwa,   	trp_prc_id 	) "
//																	+ "VALUES ( ek_seq_trp.NEXTVAL, 	?,							'PASEKMF' ,    		? 					) " ;
//Long prcid = user.getPrcIdLong();
//
////long frmId = 1;
//this.execNoQuery(sql, uruchId, prcid);
//
//params.put("P_TRP_ID", uruchId);   //OBEJSCIE API z EK_PCK_TMP . wstawianie pracownikow do EK_TMP_RAPORTY
//
//genrap.uruchomRaportOceanRTF_UOKIK(rapNazwa, params);
////}
//}


//public void raportPasekMF() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
//HashMap<String, Object> params = new HashMap<String, Object>();
//
//String mstr = this.getOkres().substring(5, 7);
//int m = Integer.parseInt( mstr );
//int r = Integer.parseInt( this.getOkres().substring(0,4) );
//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//Date date = new Date();
//
//Object uruchId = this.execScalarQuery("select ek_seq_uruch.NEXTVAL from dual");
//String sql =
//"  INSERT INTO EGADM1.EK_TMP_RAPORTY 	( trp_id, 			   				 trp_uruch_id,	trp_rap_nazwa,   	trp_prc_id 	) "
//															+ "VALUES ( ek_seq_trp.NEXTVAL, 	?,							'PASEKMF' ,    		? 					) " ;
//
//Long prcid = user.getPrcIdLong();
//long frmId = 1;
//
//this.execNoQuery(sql, uruchId, prcid, frmId);
//
//this.execNoQuery("DELETE FROM EAADM.EATT_LST");
//
//String sqlEattLst = "";
//
//ArrayList<ListaPlac> zaznaczoneListyPlac = getZaznaczoneListyPlac();
//for (ListaPlac lista : zaznaczoneListyPlac) {
//	sqlEattLst = "INSERT INTO EAADM.EATT_LST VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ? )";
//	
//
//	this.execNoQuery(sqlEattLst, lista.getTmpLista(), lista.getTmpId(), lista.getLstNumer(),
//			lista.getLstNazwa(), lista.getLstDataWyplaty(), lista.getLstDataObliczen(), lista.getLstId(),
//			"0", "0", "0");
//}
//
//Long sesjaId = null;
//try(Connection conn = JdbcUtils.getConnection()) {
//	sesjaId = JdbcUtils.sqlFnLong(conn, "eap_lst_tmp.utrwal");
//}
//
//
//params.put("P_TRP_ID", uruchId);   
//
//params.put("p_rok", r);
//params.put("p_miesiac",  m ); 
//params.put("p_typ_daty_listy", "ZA_MIESIAC");
//params.put("p_podsumowanie", "N");
//params.put("p_oswiadczenie", "N");
//params.put("p_data_wypelnienia", sdf.format(date));
//
//
//params.put("P_LST_ID", "MULTI");
//params.put("P_LST_ID_LISTA", "EK_LISTY2");
//
//params.put("p_rodzaj_listy", "%"); //params.put("p_rodzaj_listy", "%%");
//
//params.put("P_SESSION_ID", sesjaId);
//
////params.put("P_LST_ID", "%%");
////params.put("p_rodzaj_listy", "%%"); //params.put("p_rodzaj_listy", "%%");
//
////params.put("p_grupowanie", "BRAK");
////params.put("p_podsumowanie", "T");
////params.put("p_dk_kod", "PASEK");	
////params.put("p_tresc_pod"," .. ");
////params.put("P_FRM_ID", 1);
////params.put("p_angaz", r);
////params.put("P_TRP_ID", 106653);   //OBEJSCIE API z EK_PCK_TMP . wstawianie pracownikow do EK_TMP_RAPORTY
//			
//genrap.uruchomRaportOceanRTF_PDF("pasekmf", params);
//}

