package com.comarch.egeria.pp.danepracownika.pracownik.bo.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.comarch.egeria.pp.danepracownika.pracownik.bo.PracownikBo;
import com.comarch.egeria.pp.danepracownika.pracownik.dao.PracownikDao;
import com.comarch.egeria.pp.danepracownika.pracownik.domain.Pracownik;


@Named
public class PracownikBoImpl implements PracownikBo {

	@Inject
	private PracownikDao pracownikDao;
	
	
	@Override
	public List<Pracownik> getPracownicy() {
		return pracownikDao.getPracownicy();
	}

	
}
