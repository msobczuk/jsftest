package com.comarch.egeria.pp.danepracownika.pracownik;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.danepracownika.pracownik.bo.PracownikBo;
import com.comarch.egeria.pp.danepracownika.pracownik.domain.Pracownik;

@Named
@Scope("view")
public class PracownicyBean implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(PracownicyBean.class);

	@Inject
	private PracownikBo pracownikBo;

	private List<Pracownik> pracownicy;
	private Pracownik selectedPracownik;
	
	public void onPrcSelect(){
		System.out.println("selectedPracownik: " + selectedPracownik);
	}

	@PostConstruct
	private void init() {
		//pracownicy = new ArrayList<Pracownik>();
		
		pracownicy = pracownikBo.getPracownicy();
	}

	public List<Pracownik> getPracownicy() {
		return pracownicy;
	}

	public void setPracownicy(List<Pracownik> pracownicy) {
		this.pracownicy = pracownicy;
	}

	public Pracownik getSelectedPracownik() {
		return selectedPracownik;
	}

	public void setSelectedPracownik(Pracownik selectedPpracownik) {
		this.selectedPracownik = selectedPpracownik;
	}

}
