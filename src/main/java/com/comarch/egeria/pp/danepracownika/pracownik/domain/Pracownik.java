package com.comarch.egeria.pp.danepracownika.pracownik.domain;

public class Pracownik {
	private Long prcId;
	private Long prcNumer;
	private String prcNazwisko;
	private String prcImie;
	private String prcImieOjca;
	private String prcImieMatki;

	public Long getPrcId() {
		return prcId;
	}

	public void setPrcId(Long prcId) {
		this.prcId = prcId;
	}

	public Long getPrcNumer() {
		return prcNumer;
	}

	public void setPrcNumer(Long prcNumer) {
		this.prcNumer = prcNumer;
	}

	public String getPrcNazwisko() {
		return prcNazwisko;
	}

	public void setPrcNazwisko(String prcNazwisko) {
		this.prcNazwisko = prcNazwisko;
	}

	public String getPrcImie() {
		return prcImie;
	}

	public void setPrcImie(String prcImie) {
		this.prcImie = prcImie;
	}

	public String getPrcImieOjca() {
		return prcImieOjca;
	}

	public void setPrcImieOjca(String prcImieOjca) {
		this.prcImieOjca = prcImieOjca;
	}

	public String getPrcImieMatki() {
		return prcImieMatki;
	}

	public void setPrcImieMatki(String prcImieMatki) {
		this.prcImieMatki = prcImieMatki;
	}

	@Override
	public String toString() {
		return "Pracownik [prcId=" + prcId + ", prcNumer=" + prcNumer + ", prcNazwisko=" + prcNazwisko + ", prcImie="
				+ prcImie + ", prcImieOjca=" + prcImieOjca + ", prcImieMatki=" + prcImieMatki + "]";
	}
	
}
