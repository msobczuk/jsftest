package com.comarch.egeria.pp.danepracownika.pracownik.bo;

import java.util.List;

import com.comarch.egeria.pp.danepracownika.pracownik.domain.Pracownik;

public interface PracownikBo {
	
	List<Pracownik> getPracownicy();
	
}
