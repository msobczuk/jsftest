package com.comarch.egeria.pp.danepracownika.pracownik.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.inject.Named;
import javax.sql.DataSource;

import com.comarch.egeria.pp.danepracownika.pracownik.dao.PracownikDao;
import com.comarch.egeria.pp.danepracownika.pracownik.domain.Pracownik;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.web.common.utils.constants.Const;

@Named
public class PracownikDaoImpl implements PracownikDao {

	@Override
	public List<Pracownik> getPracownicy() {

		List<Pracownik> ret = new ArrayList<Pracownik>();

		try (Connection conn = JdbcUtils.getConnection()) {

			Pracownik prc = new Pracownik();
			prc.setPrcId(1L);
			prc.setPrcNumer(500L);
			prc.setPrcNazwisko("Szczech");
			prc.setPrcImie("Jakub");

			PreparedStatement pstmt = conn.prepareStatement(
					"select prc_id, prc_numer, prc_nazwisko, prc_imie, prc_imie_ojca, prc_imie_matki from ek_pracownicy where rownum <= 100");

			ResultSet rs = pstmt.executeQuery();

			Pracownik ob;
			while (rs.next()) {
				ob = new Pracownik();

				ob.setPrcId(rs.getLong("prc_id"));
				ob.setPrcNumer(rs.getLong("prc_numer"));
				ob.setPrcNazwisko(rs.getString("prc_nazwisko"));
				ob.setPrcImie(rs.getString("prc_imie"));
				ob.setPrcImieOjca(rs.getString("prc_imie_ojca"));
				ob.setPrcImieMatki(rs.getString("prc_imie_matki"));

				ret.add(ob);
			}
			rs.close();//!!!
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ret;

	}

}
