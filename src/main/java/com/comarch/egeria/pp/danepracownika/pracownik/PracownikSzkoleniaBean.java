package com.comarch.egeria.pp.danepracownika.pracownik;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;

@ManagedBean
@Named
@Scope("view")
public class PracownikSzkoleniaBean extends SqlBean {
	@Inject
	SessionBean sessionBean;
	
	private long prc_id;
	
	@PostConstruct
	public void init() {
		long id = sessionBean.getPrc_id_Long();
		setPrc_id(id);
		
	}

	public void testButton() {
		System.out.println("PracownikSzkoleniaBean.testButton() ... ");
		User.info("Udało się");
	}
	
	public long getPrc_id() {
		return prc_id;
	}
	
	public void setPrc_id(long prc_id) {
		this.prc_id = prc_id;
	}
}
