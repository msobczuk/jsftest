package com.comarch.egeria.pp.danepracownika.pracownik.dao;

import java.util.List;

import com.comarch.egeria.pp.danepracownika.pracownik.domain.Pracownik;

public interface PracownikDao {

	List<Pracownik> getPracownicy();
}
