package com.comarch.egeria.pp.danepracownika.pracownik;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

@Named
@Scope("view")
public class PracownikAbsencjeSqlBean extends SqlBean implements Serializable {
	
	String obecnaData = null;

	private Integer year = LocalDate.now().getYear();
	
//	private Map<String, String> years = null;
	
	ArrayList<Integer> years = null;
	
	@Inject
	SessionBean sessionBean;
	
	private SqlDataSelectionsHandler slownikAbsencje =  new SqlDataSelectionsHandler();
	
	public String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		this.obecnaData = dateFormat.format(date);
		return this.obecnaData;
	}
	
	public ArrayList<DataRow> getAbsencjeData() {
		slownikAbsencje = this.getSql("PP_PRC_NIEOBECNOSCI");
		if(this.slownikAbsencje != null)
			return this.slownikAbsencje.getData();
		
		return null;
	}
	
	public void setAbsencjeData(ArrayList<DataRow> absencjeData) {
	}	
	
	public String getTextFilterValue() {
		return this.slownikAbsencje.getTextFilter();
	}
	
	public void setTextFilterValue(String textFilterValue) {
		this.slownikAbsencje.setTextFilter(textFilterValue);
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		//slownikAbsencje.setTextFilter("");//przy zmianie roku czyszcze filtr tekstowy
		this.year = year;//po odswiezeniu danych nastapi filtrowanie przez slownik i sql z parametrem (zmiana parametru sql wymusi ponowne wyslanie zapytania do bazy)...
	}


	public ArrayList<Integer> getYears() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		if (this.years == null) {
			years = new ArrayList<>();
			
			Date dataPrzyjecia = (Date) this.execScalarQuery("select ZAT_DATA_PRZYJ from EK_ZATRUDNIENIE where zat_prc_id = ?", sessionBean.getPrc_id());
			if (dataPrzyjecia==null) dataPrzyjecia = new Date();
			int y = dataPrzyjecia.getYear() + 1900;
			int currentYear = LocalDate.now().getYear();
			
			while(currentYear >= y){
				years.add(currentYear);
				currentYear--;
			}
		}
		return years;
	}


	public void setYears(ArrayList<Integer> years) {
		this.years = years;
	}


}









