package com.comarch.egeria.pp.delegacje;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.delegacje.model.*;
import com.comarch.egeria.pp.zalaczniki.ZalacznikiBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import com.lowagie.text.DocumentException;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.nz;
import static com.comarch.egeria.Utils.round;

//import org.primefaces.context.RequestContext;
//import com.comarch.egeria.pp.delegacje.model.DeltWnioskiDelegacji;

@ManagedBean
@Named
@Scope("view")
public class DelegacjaKRBean extends SqlBean {
	private static final Logger log = LogManager.getLogger();

	@Inject
	ObiegBean obieg;

	@Inject
	protected ReportGenerator ReportGenerator;
	
//	@Inject
//	ZalacznikiBean zalaczniki;

	private Date dataWyplaty;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	private WniosekDelegacjiKraj wniosek;
	
	private DeltKalkulacje kalkulacjaWniosek;
	private DeltKalkulacje kalkulacjaWstepna;
	private DeltKalkulacje kalkulacjaRozliczenia;

	private Long wndWndId;

	private WniosekDelegacjiKraj wniosekDelegacjaPowiazana;

	private DeltTrasy selectedTrasa;
	
	private double podsumowanieWst;
	private double podsumowanieRozl;
	
	private List<DeltZrodlaFinansowania> listaZfKalkWst = new ArrayList<>();
	private List<DeltZrodlaFinansowania> zfDoRozliczenia = new ArrayList<>();
	
	private DeltPozycjeKalkulacji currentPkl = new DeltPozycjeKalkulacji();
	
	private int tabDelegacji = 0;
	
	private Boolean delWObiegu = false;
	
	private Boolean czyMogeZaliczke = false;

//	private Boolean czyDietaHotelowa;
	
	@PostConstruct
	public void init() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		if (id != null) {
			this.setWniosek(wczytajWniosek(Long.parseLong("" + id)));
//			naprawEncje();
		} else {
			this.setWniosek(newWniosek());
		}

		if (kalkulacjaWniosek==null)
			kalkulacjaWniosek = addNewKalkulacje("WNK");
		
		if (kalkulacjaWstepna==null)
			kalkulacjaWstepna = addNewKalkulacje("WST");
		
		if (kalkulacjaRozliczenia==null)
			kalkulacjaRozliczenia = addNewKalkulacje("ROZ");
		
		this.wndWndId = wniosek.getWndWndId();
		this.wczytajListyWartDel();
		this.wczytajSlownikiDelegacje();
		
//		this.przeliczFiltryDomyslnegoZF();
//		this.setCzyDietaHotelowa(true);
//		this.wniosek.setWndFPryw("N");
		
	}



	public String wyznaczCzynnosciObiegu() {
		//TODO - wywalic tego typu metody i wstawic te wywolania do akcji w obiegach (decyzja WB).
		
		if(this.obieg == null || this.obieg.stanObiegu() == null || this.obieg.stanObiegu().isEmpty()) 
			return "";
		
		//Jeśli obieg na etapie przekazania do FK to...
//		if("ROZLICZONO".equals(this.obieg.stanObiegu()) && (this.wniosek.getWndDokId() == null || this.wniosek.getWndDokId() == 0)) {
//				
//			try (Connection conn = JdbcUtils.getConnection())  {
//
//				//generacja noty
//				Long dekId = JdbcUtils.sqlFnLong(conn, "PPADM.PPP_INTEGRACJA_FIN.generuj_dek", this.wniosek.getWndId());
//				if(dekId == null) {
//					User.alert("Wystąpił błąd w trakcie generowania noty");
//					return "";
//				}
//				
//				//zapisanie id noty w wniosku
//				this.wniosek.setWndDokId(dekId);
//
//				generujRaportyJakoZalacznikiPdfDlaDokID(dekId, true, true,true);
//
//				//skierowanie noty do FK i rozpoczęcie na niej obiegu
//				JdbcUtils.sqlSPCall(conn, "PPADM.PPP_INTEGRACJA_FIN.skieruj_rozl_del_do_fk", dekId, "test DEK");
//				
//				//zapisanie danych
//				HibernateContext.saveOrUpdate(this.wniosek);
//			
//			}	catch (Exception e) {
//				log.error(e.getMessage(), e);
//				User.alert("Wystąpił błąd w trakcie tworzenia dokumentu noty");
//				User.alert(e.getMessage());
//			}
//			
//		} 
		
//		if("POZIOM30".equals(this.obieg.stanObiegu()) && this.wniosek.getWndPole10() == null ) {
//			this.wniosek.setWndPole10("POZIOM30");
//			HibernateContext.saveOrUpdate(this.wniosek);
//		}
//		
//		if("POZIOM10".equals(this.obieg.stanObiegu())) {
//			if("POZIOM30".equals(this.wniosek.getWndPole10()))
//				return "NVL(COB_NAZWA2,'#POZIOM30') like '%#POZIOM30%'";
//			else return "NVL(COB_NAZWA2,'#POZIOM20') like '%#POZIOM20%'";
//		}
		
		return "";
		
		
	}	
	
	public void dodajNoweCzynnosciSluzbowe(DeltKalkulacje kalk) {
		DelCzynnosciSluzbowe czyn = new DelCzynnosciSluzbowe();
		czyn.setCzynKrajId(1L);
		kalk.addDelCzynnosciSluzbowe(czyn);
	}
	
	public String formatujDniCzynnosciSluzbowe(Double czas) {
		String ret = "";
		if(czas != null){
			double dni = Math.floor((czas / 24));
			double x = czas - dni * 24;
			
			if(dni == 1.0)
				ret = ""+(int)dni+" dzień"+" "+round(x,2)+" h";
			else 
				ret = ""+(int)dni+" dni"+" "+round(x,2)+" h";
		}
		return ret;
	}
	

	public void wyznaczCzynnosciSluzbowe(DeltKalkulacje kalk) {
		
		if(kalk.getDeltTrasies().isEmpty() || kalk.getDeltTrasies() == null) {
			User.info("Brak danych w planie trasy, wygeneruj/wprowadź najpierw plan trasy lub wprowadź terminy czynności slużbowych ręcznie");
			return;
		}
		
		if(kalk.getDeltTrasies().get(0).getTrsDataOd() == null || kalk.getDeltTrasies().get(kalk.getDeltTrasies().size() - 1).getTrsDataDo() == null) {
			User.info("Data wyjazdu oraz przyjazdu musi być uzupełniona w planie trasy");
			return;
		}
		
		kalk.getDelCzynnosciSluzbowe().clear();
		DeltTrasy pierwszyOdcinekTrasy = kalk.getDeltTrasies().get(0);
		DeltTrasy ostatniOdcinekTrasy = kalk.getDeltTrasies().get(kalk.getDeltTrasies().size() - 1);
		
		DelCzynnosciSluzbowe czs = new DelCzynnosciSluzbowe(); 
		czs.setCzynKrajId(1L);
		
		czs.setCzynCzynnosciOd(pierwszyOdcinekTrasy.getTrsDataOd());
		czs.setCzynCzynnosciDo(ostatniOdcinekTrasy.getTrsDataDo());
		
		kalk.addDelCzynnosciSluzbowe(czs);
		
	}
	
	public void pobierzDokMF(String nazwaDokumentu) {
		try {
			String realPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/resources/docs/delegacje/"+nazwaDokumentu);
			File file = new File(realPath);
			openFile(file );
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
	}
	
	
	public void raportyDodatkowe(String nazwaRaportu){		
		if (wniosek!= null && wniosek.getWndId() != 0) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("wnd_id", wniosek.getWndId());
			try {
				com.comarch.egeria.web.common.reports.ReportGenerator.displayReportAsDOCX(params, nazwaRaportu);
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
	}
	
		
	public void openFile(File file) throws IOException {
		
		FacesContext fc = FacesContext.getCurrentInstance();
	    ExternalContext ec = fc.getExternalContext();

	    OutputStream output = ec.getResponseOutputStream();
		String fileName = file.getName();
		String contentType = ec.getMimeType(fileName); 
		int contentLength = (int) file.length();
	    
	    ec.responseReset(); 
	    ec.setResponseContentType(contentType); 
	    ec.setResponseContentLength(contentLength); 
	    //ec.setResponseHeader("Content-type", "application/pdf");
	    ec.setResponseHeader("Content-disposition", "inline; filename="+file.getName());
	    ec.setResponseHeader("pragma", "public");
		
		Files.copy(file.toPath() , output);
		
		fc.responseComplete(); 
	}
	
	
	
	private void wczytajListyWartDel() {
		this.getLW("PPL_KRAJE_WALUTY_ALL");
		this.getLW("PPL_DEL_KONTA_PRC_PLN", this.wniosek.getWndPrcId());
	}
	
	private void wczytajSlownikiDelegacje() {
		this.getSlownik("DEL_FORMY_WYPLATY");
		this.getSlownik("DEL_ZAPRASZAJACY");
		this.wniosek.getSLDelSrodekLokomocji();
		this.getSlownik("DEL_FORMY_PLATNOSCI");
	}
	
	private WniosekDelegacjiKraj newWniosek() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		WniosekDelegacjiKraj ret = new WniosekDelegacjiKraj();
		this.setWniosek(ret);
		
		ret.setWndRodzaj(1L); // 2 dla zagranicznej, 1 dla krajowej
		
		ret.setWndPrcId(AppBean.getUser().getPrcIdLong());
		ret.setWndFWyjazdZDomu("N");
		ret.setWndCzyGrupowa("N");
		ret.setWndFZaliczka("N");
		ret.setWndFBezkosztowa("N");
		ret.setWndFJednaWaluta("N");
		ret.setWndDataWniosku(new Date()); // Opcjonalnie
		
		addNewCDEL();
		addNewSLOK();
		addNewZapraszajacy();
		addNewProjekt();
		
		this.kalkulacjaWniosek =  addNewKalkulacje("WNK");
//		addPozycjeKalkulacjiByTypyPozycji(kalkulacjaWniosek);
//		addNewZaliczka();

		this.kalkulacjaWstepna = addNewKalkulacje("WST");
		return ret;
	}
	
	
	public void delKonfKosztKomunikat(javax.faces.event.ValueChangeEvent e) {
		if("T".equals(this.wniosek.getWndFKonferencyjna()) && e.getNewValue().equals(true))
		User.info("Zaznaczyłeś delegację konferencyjną kosztową. Załącz wniosek potwierdzający zaangażowanie środków");
	}
	
	public DelZaliczki addNewZaliczka() {
		
		DelZaliczki zal = new DelZaliczki();
		zal.setDkzFormaWyplaty("B");
		zal.setDkzWalId(1L);
		if(wniosek.getWndDataWyjazdu() != null) {
			zal.setDkzDataWyp(checkDate(wniosek.getWndDataWyjazdu()));
		}
		if(kalkulacjaWniosek.getDelZaliczki() == null) {
			kalkulacjaWniosek.setDelZaliczki(new ArrayList<>());
		}
		kalkulacjaWniosek.addDeltZaliczki(zal);
		
		return zal;
	}
	
	public void removeZaliczka(DelZaliczki zal){
		kalkulacjaWniosek.removeDelZaliczki(zal);
	}
	
	public Date zwrocMaxDateZWniosku(Date dzien) {
		
		if(dzien != null) {
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(dzien);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		
		return calendar.getTime();
		} else return today();
	}
	
	
	public boolean walidujPlanTrasy(DeltKalkulacje kalk, boolean czyKalkWstepna) {
		if (kalk==null) return true;
		return kalk.validateDeltTrasies();

//		Date wndDataWyjazdu = this.wniosek.getWndDataWyjazdu();
//		Date wndDataPowrotu = zwrocMaxDateZWniosku(this.wniosek.getWndDataPowrotu());
//
//		Date doSprawdzenia = null;
//		boolean ret = false;
//
//		if(kalk.getDeltTrasies().size()>0) {
//			if(kalk.getDeltTrasies().size() < 2){
//				User.alert("Plan trasy musi zawierać co najmniej dwie pozycje");
//				return false;
//			}
//
//			if(czyKalkWstepna)
//				if(kalk.getDeltTrasies().get(0).getTrsDataOd() != null && kalk.getDeltTrasies().get(kalk.getDeltTrasies().size()-1).getTrsDataDo() != null){
//			if(!( ((kalk.getDeltTrasies().get(0).getTrsDataOd().compareTo(wndDataWyjazdu) >=0 && kalk.getDeltTrasies().get(0).getTrsDataOd().before(wndDataPowrotu))
//					&&
//					(kalk.getDeltTrasies().get(kalk.getDeltTrasies().size()-1).getTrsDataDo().compareTo(wndDataWyjazdu) >=0 && kalk.getDeltTrasies().get(kalk.getDeltTrasies().size()-1).getTrsDataDo().before(wndDataPowrotu)))
//					)) {
//							User.alert("Daty na planie trasy muszą się zawierać w wnioskowanym terminie trwania delegacji");
//							return false;
//						}
//			} else {
//					User.alert("Plan trasy - wprowadzone dane nie mogą być puste");
//					return false;
//			}
//
//			if(!(kalk.getDeltTrasies().get(0).getTrsDataOd().before(kalk.getDeltTrasies().get(kalk.getDeltTrasies().size()-1).getTrsDataDo()))) {
//				User.alert("Data powrotu z delegacji nie może nastąpić przed datą wyjazdu, popraw dane");
//				return false;
//			}
//
//			for( DeltTrasy trs : kalk.getDeltTrasies()) {
//
//				if(trs.getTrsMiejscowoscOd() == null || trs.getTrsMiejscowoscDo() == null || trs.getTrsMiejscowoscOd().isEmpty() || trs.getTrsMiejscowoscDo().isEmpty()) {
//
//					User.alert("Należy uzupełnić plan trasy o brakujące dane w kolumnie miejscowość");
//					return false;
//				}
//
//				if(trs.getTrsDataOd() != null && trs.getTrsDataDo() != null) {
//
//						if(doSprawdzenia != null) {
//							if(trs.getTrsDataOd().after(doSprawdzenia)) {
//
//							}
//							else {
//								User.alert("Data wyjazdu z poprzedzającego wiersza w planie trasy nie może być późniejsza niż daty w kolejnych wierszach");
//								return false;
//							}
//						}
//
//						doSprawdzenia = trs.getTrsDataDo();
//
//						if(trs.getTrsDataDo().after(trs.getTrsDataOd())){
//							continue;
//						} else {
//							User.alert("Niepoprawna data na planie trasy : data wyjazdu nie może nastąpić przed datą przyjazdu na danym odcinku planu trasy");
//							return false;
//						}
//
//				} else {
////					User.alert("Nie wszystkie daty na planie trasy zostały uzupełnione, uzupełnij dane przed zapisem");
////					return false;
//				}
//			}
//			ret = true;
//		} else return true;
//		return ret;
	}
	
	public Date checkDate(Date dzien) {
		
		if(dzien == null)
			return today();

		Calendar startCal = Calendar.getInstance();
		startCal.setTime(dzien);
		startCal.add(Calendar.DATE, -5);
		int dayOfWeek = startCal.get(Calendar.DAY_OF_WEEK);

		switch (dayOfWeek) {
		case Calendar.SUNDAY:
			startCal.add(Calendar.DATE, -3);
			break;
		case Calendar.MONDAY:
			startCal.add(Calendar.DATE, -4);
			break;
//		default:
//			startCal.add(Calendar.DATE, -2);
//			if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
//				startCal.add(Calendar.DATE, -1);
//			} else if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
//				startCal.add(Calendar.DATE, -2);
//			}
//			break;
		}

		if (startCal.get(Calendar.DAY_OF_MONTH) == 28) {
			switch (dayOfWeek) {
			case Calendar.SUNDAY:
				startCal.add(Calendar.DATE, -2);
				break;
			case Calendar.MONDAY:
				startCal.add(Calendar.DATE, -3);
				break;
			default:
				startCal.add(Calendar.DATE, -1);
				if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
					startCal.add(Calendar.DATE, -1);
				} else if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					startCal.add(Calendar.DATE, -2);
				}
				break;
			}
		}
		dzien = startCal.getTime();
		
		if(dzien.before(today()))
			dzien = today();
		
		return dzien;
	}
	
	public void sprawdzStanObiegu() {
		
		ArrayList<DataRow> data = this.getLW("PPL_DEL_CZY_MA_NIEROZ", wniosek.getWndPrcId()).getData();
		if (!data.isEmpty())
			setCzyMogeZaliczke("N".equals(data.get(0).get("czy_ma_nieroz_delegacje")));
		
		String stanObiegu = this.obieg.stanObiegu();
		if(!stanObiegu.isEmpty()){ 
			this.setDelWObiegu(true);
			if (";POZIOM50;POZIOM60;POZIOM70;POZIOM75;POZIOM80;POZIOM90;STOP;".contains(";"+stanObiegu+";"))
				setCzyMogeZaliczke(true);
		}
		else this.setDelWObiegu(false);
	}
	
//	public void usunZaliczkiDelBezkosztowa(Boolean bezkosztowa) {
//		if(bezkosztowa.equals(true)) {
//			if("N".equals(this.wniosek.getWndFBezkosztowa())) {
//				this.wniosek.setWndFZaliczka("N");
//			}
//		}
//	}

	private void usunZaliczkiWniosek() {
		Iterator<DelZaliczki> zaliczki = this.kalkulacjaWniosek.getDelZaliczki().iterator();
		while(zaliczki.hasNext()) {
			DelZaliczki zal = zaliczki.next();
			zaliczki.remove();
		}
	}
	
//	public boolean czyZaldoWyplacenia (DeltKalkulacje kalk) {
//		boolean ret = false;
//
//		if(!kalk.getDeltPozycjeKalkulacjis().isEmpty()) {
//			for(DeltPozycjeKalkulacji pkl : kalk.getDeltPozycjeKalkulacjis()) {
//				if(pkl.getPklFormaPlatnosci() != null)
//				if("01".equals(pkl.getPklFormaPlatnosci()) ||"04".equals(pkl.getPklFormaPlatnosci())) {
//					ret = true;
//					return ret;
//				}
//			}
//		}
//
//		return ret;
//	}
	
	
//	public Boolean wyslaneKPKW (DeltKalkulacje kalk) {
//		Boolean ret = false;
//
//		for( DelZaliczki zal : kalk.getDelZaliczki()) {
//			if(zal.getDkzDokId()!=null) {
//				ret = true;
//				return ret;
//			}
//		}
//		return ret;
//	}
	
//	public void dodajKosztBiletu(DeltKalkulacje kalk, DeltTrasy trasa) {
//
///*		if(trasa.getTrsId() == 0L) {
//			User.info("Przed dodaniem biletu zapisz trasę");
//			return;
//		}*/
//
//		DeltPozycjeKalkulacji poprzedniBilet = null;
//
//		List<DeltPozycjeKalkulacji> listaBiletowPkl = kalk.getDeltPozycjeKalkulacjis().stream().filter(pkl -> pkl.getDeltTrasy() != null).collect(Collectors.toList());
//		if(listaBiletowPkl.size() > 0) {
//			for(DeltPozycjeKalkulacji pkl : listaBiletowPkl) {
//				if(pkl.getDeltTrasy().getTrsId() == trasa.getTrsId())
//					poprzedniBilet = pkl;
//			}
//		}
//
//
//		if(poprzedniBilet != null) {
//			if(poprzedniBilet.getDeltTrasy().getTrsId()==(trasa.getTrsId())) {
//				poprzedniBilet.setPklStawka(trasa.getTrsKosztBiletu());
//				przeliczKwote(poprzedniBilet);
//
//				if(trasa.getTrsKosztBiletu() == null) {
//					trasa.removeDeltPozycjeKalkulacji(poprzedniBilet);
//					kalk.removeDeltPozycjeKalkulacji(poprzedniBilet);
//				}
//
//			}
//		} else  if(trasa.getTrsKosztBiletu() != null){
//				DeltPozycjeKalkulacji p = newPozycjaKalkulacji();
//				trasa.addDeltPozycjeKalkulacji(p);
//				kalk.addDeltPozycjeKalkulacji(p);
//				p.setPklKwota(trasa.getTrsKosztBiletu());
//				p.setPklStawka(trasa.getTrsKosztBiletu());
//				p.setPklTpozId(getTpozIdByKtgPozycji("T"));
//				ustawZFAngaze(p);
//
//				//tukej trzeba sprawdzić czy to działa
////				p.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(p.getPklTpozId()));
//				if(eq("03",trasa.getTrsSrodekLokomocji()) || eq("07",trasa.getTrsSrodekLokomocji())) {
//					p.setPklFormaPlatnosci("04");
//					p.setPklOpis(kalk.getOpisTrasyKrajeMiasta());
//					User.info("Utworzona zostala pozycja kalkulacji odpowiadająca za koszt całej trasy");
//				} else {
//					p.setPklFormaPlatnosci("03");//.... samolot i pociag
//					p.setPklOpis("" + trasa.getTrsMiejscowoscOd() + " / "+trasa.getTrsMiejscowoscDo());
//				}
////				p.setPklRefundacjaOpis(""+trasa.getTrsId());
////				p.setDeltTrasy(trasa.clone());
////				p.setPklKrId(1L);
////				p.setPklWalId(1L);
//				p.setPklRefundacjaProcent(this.zwrocProcentRefundacji(p.getPklTpozId()));
//				p.setPklRefundacjaKwota(this.ustalKwoteRefundacji(p));
////				p.setPklKosztMFProcent(this.zwrocProcentKosztMF(p.getPklTpozId()));
////				p.setPklKwotaMf(this.ustalKosztMf(p));
//				p.setPklIlosc(1.0);
//
////				przeliczKwote(p);
//			}
//		}

	
//	public void zmienKosztNaPlanie(DeltKalkulacje kalk, DeltPozycjeKalkulacji pkl) {
//		if(pkl.getDeltTrasy() != null) {
//			Long idTrasy = pkl.getDeltTrasy().getTrsId();
//			for(DeltTrasy trs : kalk.getDeltTrasies()) {
//				if(idTrasy.equals(trs.getTrsId())) {
//					trs.setTrsKosztBiletu(pkl.getPklStawka());
//				}
//			}
//		}
//	}
	
//	public void usunKoszt(DeltKalkulacje kalk, DeltPozycjeKalkulacji pkl) {
//		if("T".equals(pkl.getKategoria()) && pkl.getDeltTrasy() != null) {
//			List<DeltTrasy> cel = kalk.getDeltTrasies().stream().filter(t ->pkl.getDeltTrasy().getTrsId()==(t.getTrsId())).collect(Collectors.toList());
//			if(cel.size()>0) {
//				cel.get(0).setTrsKosztBiletu(null);
//				kalk.removeDeltPozycjeKalkulacji(pkl);
//				cel.get(0).removeDeltPozycjeKalkulacji(pkl);
//			}
//		} else {
//			kalk.removeDeltPozycjeKalkulacji(pkl);
//		}
//	}
	
	public Double zwrocProcentKosztMF (Long tpozID) {
 		Double ret = 100.0;
		
		for( DeltPozycjeKalkulacji pkl : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
			if(tpozID.equals(pkl.getPklTpozId()))
				ret = pkl.getPklKosztMFProcent();
		}
		return ret;
	}
	
	public Double zwrocProcentRefundacji (Long tpozID) {
		Double ret = 0.0;
		
		for( DeltPozycjeKalkulacji pkl : kalkulacjaWstepna.getDeltPozycjeKalkulacjis()) {
			if(tpozID.equals(pkl.getPklTpozId()))
				ret = pkl.getPklRefundacjaProcent();
		}
		return ret;
	}
	
	public void obliczPodsumowanieRozliczenie(Boolean obliczZaliczki){
		obliczPodsumowanie(this.kalkulacjaRozliczenia, false);
		
		if(obliczZaliczki == true) {
			obliczZalRozliczenie(kalkulacjaWstepna.clone(true));
		}
	}
	
	public void obliczPodsumowanie(DeltKalkulacje kalk, Boolean zaliczki) {
		
		List<DeltZrodlaFinansowania> listaZF = new ArrayList<>();
		double kwotaPodsumowania = 0.0;
		
		if(sprawdzZf(kalk))
			return;
		
		// tukej jeśli bezkosztowa
		if(this.wniosek.getWndFBezkosztowa().equals("T")) {
			this.wniosek.setWndFZaliczka("N");
			this.kalkulacjaRozliczenia.getDelZaliczki().clear();
			if(kalk.getKalRodzaj().equals("WST")) {
				this.podsumowanieWst = kwotaPodsumowania;
			}
			
			if(kalk.getKalRodzaj().equals("ROZ")) {
				this.podsumowanieRozl = kwotaPodsumowania;
			}
			
			User.info("Zaznaczono opcje delegacji bezkosztowej, koszt całkowity wynosi 0");
			
			return;
		}
		
		for(DeltPozycjeKalkulacji pkl : kalk.getDeltPozycjeKalkulacjis()) {
			kwotaPodsumowania += pkl.getPklKwotaMf();
			if(!pkl.getDeltZrodlaFinansowanias().isEmpty()) {
//			if(!pkl.getDeltZrodlaFinansowanias().isEmpty() && "01".equals(pkl.getPklFormaPlatnosci()) ||"04".equals(pkl.getPklFormaPlatnosci())) {
				listaZF.addAll(pkl.getDeltZrodlaFinansowanias());
			}
		}
		
		grupujZF(listaZF);
		
		if(kalk.getKalRodzaj().equals("WST")) {
			this.podsumowanieWst = kwotaPodsumowania;
			this.listaZfKalkWst.clear();
			this.listaZfKalkWst.addAll(listaZF);
		}
			
		if(kalk.getKalRodzaj().equals("ROZ")) {
			this.podsumowanieRozl = kwotaPodsumowania;
			this.zfDoRozliczenia.clear();
			this.zfDoRozliczenia.addAll(listaZF);
		}
		
		if("T".equals(this.wniosek.getWndFZaliczka()) && zaliczki.equals(true))
			obliczZaliczkiKalkWst();
	}


	public void grupujZF(List<DeltZrodlaFinansowania> listaZF) {
		HashMap<Long, Double> zfWstZgrupowaneMap = listaZF.stream().filter(zf -> zf.getPkzfKwota()!=null).collect(Collectors.groupingBy(
				DeltZrodlaFinansowania::getPkzfSkId,
				HashMap::new, 
				Collectors.summingDouble(DeltZrodlaFinansowania::getPkzfKwota)));
		
		listaZF.clear();
		for(Long zfSkId : zfWstZgrupowaneMap.keySet()) {
			DeltZrodlaFinansowania zf = new DeltZrodlaFinansowania();
			zf.setPkzfSkId(zfSkId);
			zf.setPkzfKwota(zfWstZgrupowaneMap.get(zfSkId));
			listaZF.add(zf);
		}
	}
	
	/**
	 * @param kalk - przekzana kalkulacja, 
	 * @return sprawdza czy w przekazanej kalkulacji nie występują źródła finansowania bez wskazanego kodu źródła, jeśli tak to zwraca true
	 */
	private Boolean sprawdzZf(DeltKalkulacje kalk) {
		for(DeltPozycjeKalkulacji pkl : kalk.getDeltPozycjeKalkulacjis()) {
			List<DeltZrodlaFinansowania> zfSkidNull = pkl.getDeltZrodlaFinansowanias().stream().filter(zf -> zf.getPkzfSkId() == null).collect(Collectors.toList());
			if(zfSkidNull.size()>0) {
				String ktoraCzesc = "";
				if("WST".equals(kalk.getKalRodzaj())) {
					ktoraCzesc = "części II - kalkulacja wstępna";
				} else ktoraCzesc = "części III - kalkulacja rozliczająca";
				User.alert("Pozycje kalkulacji na " + ktoraCzesc  +" zawierają źródła finansowania bez wybranego kodu, popraw dane");
				return true;
			}
		}
		return false;
	}
	

	public Date wyznaczDateRozl() {
		Date dataRozl = null;
		if(!kalkulacjaRozliczenia.getDeltTrasies().isEmpty()) {
//			DeltTrasy ostatniOdcTrasy = this.kalkulacjaRozliczenia.getDeltTrasies()
//					.get(kalkulacjaRozliczenia.getDeltTrasies().size() - 1);
			
		Date maxDate = kalkulacjaRozliczenia.getDeltTrasies().stream().filter(trs -> trs.getTrsDataDo() != null).map(DeltTrasy::getTrsDataDo).max(Date::compareTo).orElse(null);
			
			if(maxDate != null)
				return DateUtils.addDays(maxDate, 14);
		} else if(wniosek.getWndDataPowrotu() != null)
				dataRozl = DateUtils.addDays(wniosek.getWndDataPowrotu(), 14);
		else {
			User.info("Nie udało się ustalić daty obowiązującego rozliczenia");
		}
		
		return dataRozl;
	}
	

	public void aktualizujKwotyZF(DeltPozycjeKalkulacji pkl) {
		if(pkl.getDeltZrodlaFinansowanias().size() > 0) {
			for(DeltZrodlaFinansowania z : pkl.getDeltZrodlaFinansowanias()) {
				if(z.getPkzfSkId() != null) {
					z.setPkzfKwota(procentNaKwote(nz(z.getPkzfProcent()), pkl.getPklKwotaMf()));
					if(z.getDelAngazes().size() > 0) {
						for(DelAngaze a : z.getDelAngazes()) {
							a.setAngKwota(procentNaKwote(a.getAngProcent(), z.getPkzfKwota()));
						}
					}
				}
			}
		}
	}
	
	private void obliczZaliczkiKalkWst() {
			
		List<DelZaliczki> zaliczkiCzescIIBezDokId = this.kalkulacjaWstepna.getDelZaliczki().stream().filter(dkz -> dkz.getDkzDokId()==null).collect(Collectors.toList());
		
		Double juzWziete = kalkulacjaWstepna.getDelZaliczki().stream().filter(dkz -> dkz.getDkzDokId()!=null).collect(Collectors.summingDouble(dkz -> dkz.getDkzKwota()));
				
		Double szacowanyKoszt = kalkulacjaWstepna.getDeltPozycjeKalkulacjis().stream()
				.filter(pkl -> "01".equals(pkl.getPklFormaPlatnosci()) ||"04".equals(pkl.getPklFormaPlatnosci()) && pkl.getPklKwotaMf() != 0.0)
				.collect(Collectors.summingDouble(pkl -> pkl.getPklKwotaMf()));  //odpowiednik listaWalutGotowka z DelegacjaMFBean
		
		//if(szacowanyKoszt - juzWziete>0){
		
			if(!zaliczkiCzescIIBezDokId.isEmpty()){
				zaliczkiCzescIIBezDokId.get(0).setDkzKwota(szacowanyKoszt - juzWziete);
			}
			else{
				List<DelZaliczki> zaliczkiCzescI = this.kalkulacjaWniosek.getDelZaliczki().stream().collect(Collectors.toList());
				if(!zaliczkiCzescI.isEmpty()){
					DelZaliczki zaliczka = new DelZaliczki();
					zaliczka.setDkzWalId(zaliczkiCzescI.get(0).getDkzWalId());
					if(zaliczkiCzescI.get(0).getDkzDataWyp() == null){
						zaliczka.setDkzDataWyp(checkDate(wniosek.getWndDataWyjazdu()));
					} else {
						zaliczka.setDkzDataWyp(zaliczkiCzescI.get(0).getDkzDataWyp());
					}
					zaliczka.setDkzFormaWyplaty(zaliczkiCzescI.get(0).getDkzFormaWyplaty());
					zaliczka.setDkzNrKonta(zaliczkiCzescI.get(0).getDkzNrKonta());
					zaliczka.setDkzOpis(zaliczkiCzescI.get(0).getDkzOpis());
					zaliczka.setDkzKwota(szacowanyKoszt - juzWziete);
					
						
					kalkulacjaWstepna.addDeltZaliczki(zaliczka);
				} else {
					DelZaliczki zaliczka = new DelZaliczki();
					zaliczka.setDkzWalId(1L);
					zaliczka.setDkzDataWyp(checkDate(wniosek.getWndDataWyjazdu()));
					zaliczka.setDkzKwota(szacowanyKoszt - juzWziete);
					kalkulacjaWstepna.addDeltZaliczki(zaliczka);
				}
			//}
		
		}
		
		//usuwanie zerowych lub ujemnych zaliczek bez dok_id,  <0.01d a nie <0.0d bo trzeba usunac zaliczki na kwote mniejsza niz 1/100
		List<DelZaliczki> zaliczkiDoUsuniecia = kalkulacjaWstepna.getDelZaliczki().stream().filter(zal -> zal.getDkzDokId()==null && (zal.getDkzKwota()==null || zal.getDkzKwota()<=0.01d)).collect(Collectors.toList());
		for(DelZaliczki delZal : zaliczkiDoUsuniecia)
				kalkulacjaWstepna.removeDelZaliczki(delZal);
		
	}
	
	
	/**
	 * @param pkzfSkId - ID źródła finansowania
	 * @return - Dla podanego ID źródła finansowania (stanowiska kosztowego) zwraca odpowiadający mu kod
	 */
	public String kodZF(Object pkzfSkId){
		if (pkzfSkId==null)
			return "";
		
		SqlDataSelectionsHandler zrodlaFin = this.getLW("PPL_DEL_ZRODLA_FIN");
		DataRow r = zrodlaFin.get(pkzfSkId);
		if (r==null)
			return "";
		
		String zfKod = (String) r.get("SK_KOD");
		
		return zfKod;
	} 
	
	
	public void wyplacZaliczke(
			DelZaliczki zal,
			DeltKalkulacje kalk
			) {

		
		if(zal.getDkzDataWyp() == null && zal.getDkzFormaWyplaty() == null){
			 User.alert("Uzupełnij brakujące dane przed wygenerowaniem polecenia wypłaty zaliczki");
			 return;
		}
		
/*		if(("K").equals(zal.getDkzFormaWyplaty()) && zal.getDkzOpis().length() == 0) {
			User.alert("Uzupełnij opis dla wybranej formy wypłaty zaliczki w gotówce");
			return;
		}*/

		if(zal.getDkzFormaWyplaty()==null || zal.getDkzFormaWyplaty().isEmpty()){
			User.alert("Podaj formę wypłaty.");
			return;
		}
		
		if(("B").equals(zal.getDkzFormaWyplaty()) && (zal.getDkzNrKonta() == null || zal.getDkzNrKonta().isEmpty())) {
			User.alert("Podaj numer konta");
			return;
		}
		
		if(zal.getDkzKwota() == 0) {
			User.alert("Nie można wnioskować o zerową zaliczkę");
			return;
		}



		if(wniosek.getWndId() == 0)
		zapisz();

		List<DeltZrodlaFinansowania> zF = getZFdlaDokumentuNZ(kalk);
		if (zF == null) return;



		try (Connection conn = JdbcUtils.getConnection())  {
			String frmWyplaty = (""+zal.getDkzFormaWyplaty()).toUpperCase(); // ma być K jak kasa lub B jak bank
			String rodzajDokFin = JdbcUtils.sqlFnVarchar(conn, "PPADM.PPP_INTEGRACJA_FIN.f_zwroc_rodzaj_dokumentu", frmWyplaty , "WY", zal.getDkzWalId());
			Timestamp dataWyplaty = new Timestamp(zal.getDkzDataWyp().getTime());

			BigDecimal dokId = JdbcUtils.sqlFnBigDecimal(conn, "PPADM.PPP_INTEGRACJA_FIN.WSTAW_KPKW_POPW", 
					zal.getDkzKwota(), 
					1.0, 
					"wypłata zaliczki", 
					rodzajDokFin,
					zal.getDkzWalId(), 
					zal.getDkzNrKonta(), 
					wniosek.getWndPrcId(),
					wniosek.getWndId(),
					dataWyplaty,
					kalk.getKalRodzaj(),
					zal.getDkzId()
			);

			getLW("PPL_DEL_KURS_DKZ_DOK").resetTs();

			Double doPodzialuZaoPLN = round(zal.getDkzKwota(), 2);
			Double rozdzielono = 0d;
			Double sumaZF = zF.stream().mapToDouble(DeltZrodlaFinansowania::getPkzfKwota).sum();
			
			for(int i=0;i<zF.size();i++){
				DeltZrodlaFinansowania zf = zF.get(i);

				Double kwotaPLN = 0d;
				
				if(i==zF.size()-1){
					kwotaPLN = doPodzialuZaoPLN - rozdzielono;
				}else{
					kwotaPLN = zf.getPkzfKwota();
					kwotaPLN = round(kwotaPLN *(zal.getDkzKwota()/sumaZF), 2);
				}

				for (DelAngaze ang : zf.getDelAngazes()) {
					BigDecimal podzialID = JdbcUtils.sqlFnBigDecimal(conn,
							"PPADM.PPP_INTEGRACJA_FIN.f_wstaw_lub_akt_psk", dokId, zf.getPkzfSkId(), kwotaPLN, (wniosek.getWndDataWyjazdu().getYear() + 1900), ang.getAngWnrId());
				}

				
				rozdzielono = rozdzielono + kwotaPLN;
			}

			//			przeliczenie procentów w podziałach na stanowiska kosztow
			JdbcUtils.sqlSPCall(conn, "PPADM.PPP_INTEGRACJA_FIN.oblicz_procenty_psk", dokId);
			
			if (dokId!=null) {
				zal.setDkzDokId(dokId.longValueExact());
				
				zapisz();

				generujRaportyJakoZalacznikiPdfDlaDokID(dokId, true, true,  true);

				JdbcUtils.sqlSPCall(conn, "PPADM.WYPLAC_ZALICZKE", dokId);
				this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", kalk.getKalId()).resetTs();
			}
			else
				zal.setDkzDokId(null);
			
			
			
		} catch (Exception e) {
//			log.error(e.getMessage(), e);
//			User.alert("Wystąpił błąd w trakcie tworzenia dokumentu");
			User.alert(e);
		} /*finally {
			lw.resetTs();
		}*/
		
//		przygotujDaneKalkRozl(); TODO odkomentuj jak zdebugujesz
	}



	private List<DeltZrodlaFinansowania> getZFdlaDokumentuNZ(DeltKalkulacje kalk) {
		List<DeltZrodlaFinansowania> zF = new ArrayList<>();

		for(DeltPozycjeKalkulacji p : kalk.getDeltPozycjeKalkulacjis()) {
			if(!(p.getPklWalId().equals(1L) && ("01".equals(p.getPklFormaPlatnosci()) || "04".equals(p.getPklFormaPlatnosci()))))
				continue;

			if (p.getDeltZrodlaFinansowanias().isEmpty()){
				User.alert("Nie wszystkie pozycje posiadają źródło finansowania.");
				return null;
			}

			for( DeltZrodlaFinansowania z: p.getDeltZrodlaFinansowanias()){
				if (z.getDelAngazes().isEmpty()){
					User.alert("Nie wszystkie pozycje posiadają wniosek o zaangażowanie środków.");
					return null;
				}
				zF.add(z);
			}
		}

//		//this.grupujZF(zF); angaze sa konieczne/ grupowaniem zajmuje sie metoda f_wstaw_lub_akt_psk
////		zF.sort(Comparator.comparingDouble(DeltZrodlaFinansowania::getPkzfKwota));
//
//		String kalRodzaj = kalk.getKalRodzaj(); //		"WST" lub "ROZ"
//		if ("ROZ".equals(kalRodzaj)) kalRodzaj="%";
//		SqlDataSelectionsHandler pobraneZF = this.getLW("PPL_ZALICZKI_KWOTY", this.wniosek.getWndId(), kalRodzaj, 1L);
//		if(pobraneZF!=null){
//			for(DeltZrodlaFinansowania zrodlo: zF){
//				DataRow r = pobraneZF.find(zrodlo.getPkzfSkId());
//				if(r!=null){
//					Double kwotaPLN  = ((BigDecimal) r.get("kwota_pln")).doubleValue();
//					if(kwotaPLN!=null && kwotaPLN>0){
//						zrodlo.setPkzfKwota(zrodlo.getPkzfKwota()-kwotaPLN>0?zrodlo.getPkzfKwota()-kwotaPLN:0);
//					}
//				}
//			}
//		}



		zF = zF.stream().filter(z -> nz(z.getPkzfKwota())>0).collect(Collectors.toList());
		return zF;
	}


//	public void modyfikujZalDok(DelZaliczki zal) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
//
//		if(zal.getDkzDokId() == null || zal.getDkzDataWyp() == null)
//			return;
//
//		 if(!sprawdzCzyZalZatwierdzona(zal)){
//			try (Connection conn = JdbcUtils.getConnection()) {
//					Timestamp dataWyplaty = new Timestamp(zal.getDkzDataWyp().getTime());
//					JdbcUtils.sqlSPCall(conn, "PPADM.ppp_del_zaliczki.Modyfikuj_dok_zal",zal.getDkzDokId(), dataWyplaty);
//					HibernateContext.saveOrUpdate(this.wniosek);
//					User.info("Zmodyfikowano datę wypłaty na zaliczce w FK");
//			} catch (Exception e) {
//				User.alert(e);
//			}
//		 } else {
//			 User.alert("Zaliczka została już zatwierdzona i nie można jej modyfikować");
//			 return;
//		 }
//	}
	
//	public void usunZalKW(DelZaliczki zal, DeltKalkulacje kalk) throws SQLException{
//		if(zal.getDkzDokId() != null) {
//			try (Connection conn = JdbcUtils.getConnection()) {
//				Long dokIdDoUsuniecia = zal.getDkzDokId();
//				JdbcUtils.sqlSPCall(conn, "PPADM.ppp_del_zaliczki.usun_zal",dokIdDoUsuniecia);
//				ustawReferencjeKalkulacji();
//				kalk.removeDelZaliczki(zal);
//				HibernateContext.saveOrUpdate(this.wniosek);
//			} catch (Exception e) {
//				User.alert(e);
//			}
//		} else {
//			this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", kalk.getKalId()).resetTs();
//			kalk.removeDelZaliczki(zal);
//		}
//	}
	
	
	public boolean sprawdzCzyZalZatwierdzona(DelZaliczki zal) {
		if (zal==null || zal.getDeltKalkulacje()==null)
			return false;
			
		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", zal.getDeltKalkulacje().getKalId());
		if (lw==null)
			return false;
		
		DataRow r = lw.find(zal.getDkzDokId());
		if (r==null)
			return false;
		
		Object object = r.get("dok_f_zatwierdzony");
		return ("T".equals( object) );
	}


//	public boolean sprawdzCzyZalWyplacona(DelZaliczki zal) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
//	if(zal.getDkzDokId() != null) {	
//		 Object czyWyplacona = this.execScalarQuery("select dok_dok_id_zap from kgt_dokumenty where dok_id = ?", 
//					zal.getDkzDokId());
//		 if(czyWyplacona != null){
//			 return true;
//		 } else return false;
//		} else return false;
//	}
	
//	public Double zwrocKwoteDoRozliczen() {
//		return kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis().stream()
//				.filter(pkl -> pkl != null && pkl.getPklKwotaMf() != null
//				&& "01".equals(pkl.getPklFormaPlatnosci()) ||"04".equals(pkl.getPklFormaPlatnosci()) && pkl.getPklKwotaMf() != 0.0)
//				.mapToDouble(DeltPozycjeKalkulacji::getPklKwotaMf)
//				.sum();
//	}
	
	
//	public void generujDokumentNZ(
//			DelZaliczki zal,
//			DeltKalkulacje kalk
//			) {
//
//		if(wniosek.getWndId() == 0)
//		zapisz();
//
//		List<DeltZrodlaFinansowania> zF = getZFdlaDokumentuNZ(kalk);
//		if (zF == null) return;
//
//
//
//		zal.setDkzDataWyp(today());
//
//		if(zal.getDkzFormaWyplaty() == null || zal.getDkzFormaWyplaty().isEmpty()){
//			 User.alert("Podaj formę rozliczenia");
//			 return;
//		}
//
//		//jak ktos to odkomentuje to trzeba przy okazji polatac nullPointer przy zal.getDkzOpis().length()
//		/*if( "K".equals(zal.getDkzFormaWyplaty()) && zal.getDkzOpis().length() == 0) {
//			User.alert("Uzupełniij opis dla wybranej formy wypłaty zaliczki w gotówce.");
//			return;
//		}*/
//
//		if( "B".equals(zal.getDkzFormaWyplaty()) && (zal.getDkzNrKonta() == null || zal.getDkzNrKonta().isEmpty())) {
//			User.alert("Podaj nr konta");
//			return;
//		}
//
//
//		try (Connection conn = JdbcUtils.getConnection())  {
//
//			String frmWyplaty = (""+zal.getDkzFormaWyplaty()).toUpperCase(); // ma być K jak kasa lub B jak bank
//
//			String wyplataWplata = this.rodzajDokumentu(zal);
//
//			String rodzajDokFin = JdbcUtils.sqlFnVarchar(conn, "PPADM.PPP_INTEGRACJA_FIN.f_zwroc_rodzaj_dokumentu", frmWyplaty , wyplataWplata, zal.getDkzWalId());
//			Timestamp dataWyplaty = new Timestamp(zal.getDkzDataWyp().getTime());
//
//			Double kwota = 0.0;
//
//			kwota = "WY".equals(wyplataWplata) ? zal.getDkzSrodkiWlasne() : zal.getDkzKwota();
//
//			BigDecimal dokId = JdbcUtils.sqlFnBigDecimal(conn, "PPADM.PPP_INTEGRACJA_FIN.WSTAW_KPKW_POPW",
//					Math.abs(kwota),
//					1.0,
//					"rozliczenie",
//					rodzajDokFin,
//					zal.getDkzWalId(),
//					zal.getDkzNrKonta(),
//					wniosek.getWndPrcId(),
//					wniosek.getWndId(),
//					dataWyplaty,
//					kalk.getKalRodzaj(),
//					zal.getDkzId()
//			);
//			getLW("PPL_DEL_KURS_DKZ_DOK").resetTs();
//
//			Double doPodzialuZaoPLN = kwota;
//			Double rozdzielono = 0d;
//			Double sumaZF = zF.stream().mapToDouble(DeltZrodlaFinansowania::getPkzfKwota).sum();
//
//			for(int i=0;i<zF.size();i++){
//				DeltZrodlaFinansowania zf = zF.get(i);
//
//				Double kwotaPLN = 0d;
//
//				if(i==zF.size()-1){
//					kwotaPLN = doPodzialuZaoPLN - rozdzielono;
//				}else{
//					kwotaPLN = zf.getPkzfKwota();
//					kwotaPLN = round(kwotaPLN *(kwota/sumaZF), 2);
//				}
//
//
//				for (DelAngaze ang : zf.getDelAngazes()) {
//					BigDecimal podzialID = JdbcUtils.sqlFnBigDecimal(conn,
//							"PPADM.PPP_INTEGRACJA_FIN.f_wstaw_lub_akt_psk", dokId, zf.getPkzfSkId(), kwotaPLN, (wniosek.getWndDataWyjazdu().getYear() + 1900), ang.getAngWnrId());
//				}
//
//				rozdzielono = rozdzielono + kwotaPLN;
//			}
//
//			JdbcUtils.sqlSPCall(conn, "PPADM.PPP_INTEGRACJA_FIN.oblicz_procenty_psk", dokId);
//
//			if (dokId!=null) {
//				zal.setDkzDokId(dokId.longValueExact());
//				zapisz();
//
//				generujRaportyJakoZalacznikiPdfDlaDokID(dokId, true, true, true);
//
//
//				JdbcUtils.sqlSPCall(conn, "PPADM.WYPLAC_ZALICZKE", dokId);
//			}
//			else
//				zal.setDkzDokId(null);
//
//
//
//		} catch (Exception e) {
////			log.error(e.getMessage(), e);
////			User.alert("Wystąpił błąd w trakcie tworzenia dokumentu");
//			User.alert(e);
//		}
////		przygotujDaneKalkRozl(); TODO odkomentuj jak zdebugujesz
//	}



	private void generujRaportyJakoZalacznikiPdfDlaDokID(Object dokId, boolean czyRaportPolecenia,  boolean czyRaportRozliczenia,  boolean czyZalacznikDlaNZ_DELZ_FK5) throws SQLException, IOException, JRException, DocumentException {
		this.wniosek.generujRaportyJakoZalacznikiPdfDlaDokID(dokId, czyRaportPolecenia, czyRaportRozliczenia, czyZalacznikDlaNZ_DELZ_FK5);
		

	}


	public String rodzajDokumentu (DelZaliczki zal) {
		
		String ret = "";
		if(zal.getDkzSrodkiWlasne() > 0)
				ret = "WY";
			else ret = "WP";

			
		return ret;
	}
	
	public void przygotujDaneKalkRozl() {
		
//		if("T".equals(wniosek.getWndFZaliczka())) {
//			for(DelZaliczki z : kalkulacjaWstepna.getDelZaliczki()) {
//			if( z.getDkzDokId() == null){
//				 User.alert("W części II występuje zaliczka bez wygenerowanego dokumentu FIN."
//					 		+ "\n"+ "Wygeneruj polecenie przelewu/wypłaty <b>dla każdej</b> wnioskowanej zaliczki przed przystąpieniem do rozliczenia.");
//				 this.setTabDelegacji(1);
//				 return;
//				}
//			}
//		}
		
		DeltKalkulacje kopiaKalkWst = this.kalkulacjaWstepna.clone(true);
		kopiaKalkWst.setKalId(0L);
		kopiaKalkWst.setDeltWnioskiDelegacji(wniosek);
		
		this.kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis().clear();
		for(DeltPozycjeKalkulacji pkl:kopiaKalkWst.getDeltPozycjeKalkulacjis()) {
//			if("01".equals(pkl.getPklFormaPlatnosci()) ||"04".equals(pkl.getPklFormaPlatnosci()))
			if(pkl.czyKopiowacNaRozliczenie())
			this.kalkulacjaRozliczenia.addDeltPozycjeKalkulacji(pkl);
		}
		
		this.kalkulacjaRozliczenia.getDeltTrasies().clear();
		for(DeltTrasy t:kopiaKalkWst.getDeltTrasies()) {
			this.kalkulacjaRozliczenia.addDeltTrasy(t);		
		}
		
		for(DeltPozycjeKalkulacji pk: this.kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis()) {
			if(pk.cloneSrc.getDeltTrasy() != null)
			{
				pk.cloneSrc.getDeltTrasy().cloneDest.addDeltPozycjeKalkulacji(pk);
			}
		}
		
		
		this.kalkulacjaRozliczenia.getDelCzynnosciSluzbowe().clear();
		if(!kopiaKalkWst.getDelCzynnosciSluzbowe().isEmpty()) {
			for(DelCzynnosciSluzbowe cz : kopiaKalkWst.getDelCzynnosciSluzbowe()) {
				this.kalkulacjaRozliczenia.addDelCzynnosciSluzbowe(cz);
			}
		}
		

		
		
		if("T".equals(wniosek.getWndFZaliczka()))
		obliczPodsumowanieRozliczenie(false);
		

		this.kalkulacjaRozliczenia.przeliczPodzialZFvsDKZ();
		if (this.zapisz()) {
			dodajZapiszRelacjaPklTrs();			
		}
		

		
		
//		List<DeltPozycjeKalkulacji> kosztTransportuRoz = kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis().stream().filter(pkl ->"T".equals(pkl.getKategoria())).collect(Collectors.toList());
//		for(DeltPozycjeKalkulacji pkl : kosztTransportuRoz) {
//			 List<DeltTrasy> deltTrasy = kalkulacjaRozliczenia.getDeltTrasies().stream()
//					.filter(trs -> trs.getTrsKosztBiletu() != null)
//					.filter(trs -> trs.getTrsKosztBiletu().equals(pkl.getPklKwotaMf()))
//					.collect(Collectors.toList());
//			if(deltTrasy!= null && !deltTrasy.isEmpty()) 
//				pkl.setDeltTrasy(deltTrasy.get(0));
//		}
		
//		this.tabDelegacji = 2;
	}

	private void dodajZapiszRelacjaPklTrs() {
		int cnt = 0;
		for(DeltPozycjeKalkulacji pkl : this.kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis()) {
			if(pkl.cloneSrc.getDeltTrasy() != null) {
				
				pkl.setDeltTrasy(pkl.cloneSrc.getDeltTrasy().cloneDest);
				cnt+=1;

			}
		}
		if (cnt>0) HibernateContext.saveOrUpdate(this.wniosek);//this.zapisz();
	}

	private void obliczZalRozliczenie(DeltKalkulacje kopiaKalkWst) {
		
		Double kwotaGotowkiRozliczenie = kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis().stream()
				.filter(pkl -> "01".equals(pkl.getPklFormaPlatnosci()) ||"04".equals(pkl.getPklFormaPlatnosci()) && pkl.getPklKwotaMf() != 0.0)
				.collect(Collectors.summingDouble(pkl -> pkl.getPklKwotaMf()));
		
		Double sumaDoWyplatyROZ = this.kalkulacjaRozliczenia.getDelZaliczki().stream()
				.filter(dkz -> dkz.getDkzWalId().equals(1L) && dkz.getDkzDokId()!=null)
				.collect(Collectors.summingDouble(dkz -> dkz.getDkzSrodkiWlasne()));
		
		
		Double sumaDoOddaniaROZ = this.kalkulacjaRozliczenia.getDelZaliczki().stream()
				.filter(dkz -> dkz.getDkzWalId().equals(1L) && dkz.getDkzDokId()!=null)
				.collect(Collectors.summingDouble(dkz -> dkz.getDkzKwota()));
		 
		
		Double sumaZaliczekWST = kopiaKalkWst.getDelZaliczki().stream()
				.filter(dkz -> dkz.getDkzWalId().equals(1L) && dkz.getDkzDokId()!=null)
				.collect(Collectors.summingDouble(dkz -> dkz.getDkzKwota()));
		
		
		Double ileFaktycznieWydanoROZ = nz(kwotaGotowkiRozliczenie);
		
		List<DelZaliczki> doUsuniecia = this.kalkulacjaRozliczenia.getDelZaliczki().stream().filter(dkz -> dkz.getDkzWalId().equals(1L) && dkz.getDkzDokId()==null).collect(Collectors.toList());
		for(DelZaliczki zal : doUsuniecia) 
			kalkulacjaRozliczenia.removeDelZaliczki(zal);
		
		if(Utils.round(sumaDoWyplatyROZ, 2) - Utils.round(sumaDoOddaniaROZ, 2) == Utils.round(ileFaktycznieWydanoROZ, 2) - Utils.round(sumaZaliczekWST, 2)){
            //wszystko jest ju git
		}
		else
		{
			//teraz trzeba nową zaliczke, zaraz obliczymy jaką			
			DelZaliczki zal = new DelZaliczki();
			zal.setDkzWalId(1L);
			if("T".equals(wniosek.getWndFZaliczka())){
			
				Double wziete = this.kalkulacjaRozliczenia.getDelZaliczki().stream()
						.filter(dkz -> dkz.getDkzWalId().equals(1L))
						.collect(Collectors.summingDouble(dkz -> dkz.getDkzZalWydano()));
				
				zal.setDkzZalWydano(sumaZaliczekWST - wziete);
				
				if(ileFaktycznieWydanoROZ - sumaZaliczekWST > sumaDoWyplatyROZ - sumaDoOddaniaROZ){
					zal.setDkzKwota(0d);
					zal.setDkzSrodkiWlasne(ileFaktycznieWydanoROZ - sumaZaliczekWST-(sumaDoWyplatyROZ - sumaDoOddaniaROZ));
				}else{
					zal.setDkzKwota(-(ileFaktycznieWydanoROZ - sumaZaliczekWST-(sumaDoWyplatyROZ - sumaDoOddaniaROZ)) );
					zal.setDkzSrodkiWlasne(0d);
					
				}
				
			}else{
				zal.setDkzKwota((ileFaktycznieWydanoROZ - sumaDoWyplatyROZ)<0?(sumaDoWyplatyROZ - ileFaktycznieWydanoROZ):0);
				zal.setDkzZalWydano(0.0);
				zal.setDkzSrodkiWlasne((ileFaktycznieWydanoROZ - sumaDoWyplatyROZ)>0?(ileFaktycznieWydanoROZ - sumaDoWyplatyROZ):0);
			}
			
			kalkulacjaRozliczenia.addDeltZaliczki(zal);	
		}
		
	}
	
	
	public Double obliczRozniceRoz(Long walID, Double wartoscPLN) {
		
		Double ret = 0.0;
		for(DelZaliczki z : kalkulacjaWstepna.getDelZaliczki()) {
			if(z.getDkzWalId().equals(walID)) {
				ret = wartoscPLN -	z.getDkzKwota();
			}
		}
		return ret;
	}

	public void ustawReferencjeKalkulacji(){
		
		Optional<DeltKalkulacje> findFirst = this.wniosek.getDeltKalkulacjes().stream().filter(k->"WNK".equals( (""+ k.getKalRodzaj()).toUpperCase() ) ).findFirst();
		if (findFirst.isPresent())
			this.kalkulacjaWniosek = findFirst.get();
		
		Optional<DeltKalkulacje> findFirst2 = this.wniosek.getDeltKalkulacjes().stream().filter(k->"WST".equals( (""+ k.getKalRodzaj()).toUpperCase() ) ).findFirst();
		if (findFirst2.isPresent())
			this.kalkulacjaWstepna = findFirst2.get();
		
		Optional<DeltKalkulacje> findFirst3 = this.wniosek.getDeltKalkulacjes().stream().filter(k->"ROZ".equals( (""+ k.getKalRodzaj()).toUpperCase() ) ).findFirst();
		if (findFirst3.isPresent())
			this.kalkulacjaRozliczenia = findFirst3.get();
		
	}
	
	private void naprawEncje() {
		
		if ("T".equals(wniosek.getWndFBezkosztowa())) {
			if (kalkulacjaWniosek != null && kalkulacjaWniosek.getDeltPozycjeKalkulacjis() != null) {
				for (Object p : kalkulacjaWniosek.getDeltPozycjeKalkulacjis().toArray())
					kalkulacjaWniosek.removeDeltPozycjeKalkulacji((DeltPozycjeKalkulacji) p);
			}
		}

		if (wniosek.getDeltSrodkiLokomocjis().isEmpty()) {
			wniosek.addDeltSrodkiLokomocji(new DeltSrodkiLokomocji());
			User.warn("Odtworzono brakujący środek lokomocji (uzupełnij dane).");
		}
	}

//	public static void addZF2Pkl(DeltPozycjeKalkulacji pkl) {
//		
//		DeltZrodlaFinansowania zf = new DeltZrodlaFinansowania();
//		zf.setPkzfKwota(0.0);
//		zf.setPkzfProcent(100.0);
//		zf.setPkzfOpis("zf nr " + (pkl.getDeltZrodlaFinansowanias().size() + 1));
//		pkl.addDeltZrodlaFinansowania(zf);
//	}
	
	public void addZF2PklDlg(DeltPozycjeKalkulacji pkl) {
		
		DeltZrodlaFinansowania zf = new DeltZrodlaFinansowania();
		
		Double procentZf = 0.0;
		for(DeltZrodlaFinansowania zfin : pkl.getDeltZrodlaFinansowanias()) {
			procentZf += zfin.getPkzfProcent();
		}
		zf.setPkzfProcent(100.0 - procentZf);
		if(pkl.getPklKwotaMf() != null)
			zf.setPkzfKwota(procentNaKwote(zf.getPkzfProcent(), pkl.getPklKwotaMf()));
		else zf.setPkzfKwota(0.0);
		
		zf.setPkzfOpis("zf nr " + (pkl.getDeltZrodlaFinansowanias().size() + 1));
		dodajAngazDoZf(zf);
		
		pkl.addDeltZrodlaFinansowania(zf);
	}
	
	public void dodajAngazDoZf(DeltZrodlaFinansowania zf) {
		Double procentAngaz = 0.0;
		for(DelAngaze an : zf.getDelAngazes()) {
			procentAngaz += an.getAngProcent();
		}
		DelAngaze nowyAngaz = new DelAngaze();
		nowyAngaz.setAngProcent(100.0 - procentAngaz);
		nowyAngaz.setAngKwota(procentNaKwote(nowyAngaz.getAngProcent(), nz(zf.getPkzfKwota())));
		zf.addDelAngaze(nowyAngaz);
	}
	
	public void showDlgZF4CurrentPklDlg(DeltPozycjeKalkulacji pkl) {
		currentPkl = pkl;
		com.comarch.egeria.Utils.update("dlgZrKrFinId");
		Utils.executeScript("PFbyId('dlgZrKrFinId').show();");
	}

//	public void copyZF2AllPkl(DeltPozycjeKalkulacji pkl) {
//
//		String txt = "";
//		for (DeltPozycjeKalkulacji p : pkl.getDeltKalkulacje().getDeltPozycjeKalkulacjis()) {
//			if (p.equals(pkl))
//				continue;
//
//			for (Object z : p.getDeltZrodlaFinansowanias().toArray())
//				p.removeDeltZrodlaFinansowania((DeltZrodlaFinansowania) z);
//
//			for (DeltZrodlaFinansowania z : pkl.getDeltZrodlaFinansowanias()) {
//				if(z.getPkzfSkId() == null) {
//					User.alert("Próba skopiowania źródła finansownaia bez podanego kodu do pozostąłyc pozycji, popraw dane");
//					return;
//				}
//				p.addDeltZrodlaFinansowania(z.clone(false));
//			}
//
//			txt += String.format("p. '%1$s'; <br/>",
//					this.getSql("PPL_DEL_TYPY_POZYCJI_K").findAndFormat(p.getPklTpozId(), "%2$s"));
//		}
//
//		if (txt.length() > 0)
//
//		User.info("Skopiowano źródła finasowania");
//	}



//	 metoda zwracająca kategorię pozycji po podaniu id pozycji i wtedy porównanie kategorii i kopiowanie źródeł finansowania tylko dla pozycji o tych samych kategoriach
//	public void copyZFByTypPkl(DeltPozycjeKalkulacji p, List<DeltPozycjeKalkulacji> pkl) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
//
//		String txt = "";
//			List<DeltZrodlaFinansowania> kopiowaneZf = new ArrayList<>();
//
//			for(DeltZrodlaFinansowania z : p.getDeltZrodlaFinansowanias()) {
//				if(!z.getDelAngazes().isEmpty())
//					kopiowaneZf.add(z.clone(true));
//				  else
//					kopiowaneZf.add(z.clone(false));
//			}
//
//			for (DeltPozycjeKalkulacji poz : pkl) {
//				if(p.equals(poz))
//					continue;
//
//				for (Object z : poz.getDeltZrodlaFinansowanias().toArray())
//					poz.removeDeltZrodlaFinansowania((DeltZrodlaFinansowania) z);
//
//				for(DeltZrodlaFinansowania kZf : kopiowaneZf) {
//					if(kZf.getPkzfSkId() == null) {
//						User.alert("Próba skopiowania źródła Finansowania bez podanego kodu do pozostałych pozycji, popraw dane");
//						return;
//					}
//					kZf.setPkzfKwota(procentNaKwote(kZf.getPkzfProcent(), poz.getPklKwotaMf()));
//					if(!kZf.getDelAngazes().isEmpty()) {
//						for(DelAngaze a : kZf.getDelAngazes()) {
//							a.setAngKwota(procentNaKwote(a.getAngProcent(),kZf.getPkzfKwota()));
//						}
//					}
//					poz.addDeltZrodlaFinansowania(kZf.clone(true));
//				}
//				txt += String.format("p. '%1$s'; <br/>",
//						this.getSql("PPL_DEL_TYPY_POZYCJI_K").findAndFormat(p.getPklTpozId(), "%2$s"));
//			}
//			if (txt.length() > 0)
//			User.info("Skopiowano źródła finasowania");
//	}

	
	public void generujDiety(String ktgPozycji, DeltKalkulacje kalk) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Long tpozId = 0L;

		if(kalk.getDeltTrasies().isEmpty())
			return;
		
		if("KM".equals(ktgPozycji))
			tpozId = getTpozIdByKtgPozycji(ktgPozycji); 
		else 
			tpozId = getTpozIdByKtgDiety(ktgPozycji);
		
		if(eq("H",ktgPozycji) || eq("RH", ktgPozycji)) {
			usunPozycjeKalkulacjiWgKtg("H", kalk);
			usunPozycjeKalkulacjiWgKtg("RH", kalk);
		}else 
			usunPozycjeKalkulacjiWgKtg(ktgPozycji, kalk);
		
		DeltPozycjeKalkulacji o = newPozycjaKalkulacji();
		o.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(tpozId));
		Long krId = 1L;
		Date dtKrOd = null;
		Date dtKrDo = null;
		
		Double ilegodzin = sprawdzIleGodzinDel(kalk); 
		if(ilegodzin == 0.0)
			return;
		
		Boolean delegacjaPowyzejDnia = ilegodzin > 24.0;
		
//		Double ilegodzin = 0.0;
//		List<DeltTrasy> trasyBezCzynnSluz = kalk.getDeltTrasies().stream().filter(t -> t.getTrsCzynnosciSluzboweOd() == null || t.getTrsCzynnosciSluzboweDo() == null).collect(Collectors.toList());
		
		Date dataWyjazdu = kalk.getDeltTrasies().get(0).getTrsDataOd();
		
//		if(trasyBezCzynnSluz.size() > 1) {
			
			dtKrOd = dataWyjazdu;
			dtKrDo = kalk.getDeltTrasies().get(kalk.getDeltTrasies().size()-1).getTrsDataDo();
//			
//		} else {
//				for (DeltTrasy t : kalk.getDeltTrasies()) {
//					dtKrOd = t.getTrsCzynnosciSluzboweOd();
//					dtKrDo = t.getTrsCzynnosciSluzboweDo() != null ? t.getTrsCzynnosciSluzboweDo() : dtKrOd;
//					
//					Double godziny = (double) ((dtKrDo.getTime() - dtKrOd.getTime())/(1000 * 60 * 60));
//					ilegodzin += godziny;
//			}
//		}
//		
			if ("P".equals(ktgPozycji)) 
				przeliczDodajPKL_Pobytowa(ilegodzin, tpozId, o, krId, dataWyjazdu, kalk, delegacjaPowyzejDnia);
			
			else if("KM".equals(ktgPozycji))
				przeliczDodajRyczalt_Komunikacyjny(tpozId, o, krId, ilegodzin, kalk);
			
			else 
				przeliczDodajPKL_Hotelowa(sprawdzIleGodzinDel(kalk), tpozId, o, krId, dataWyjazdu, kalk);
			
			if("RH".equals(ktgPozycji)) {
				o.setPklStawka(45.0);
				przeliczKwote(o);
			}

			else if("H".equals(ktgPozycji)) {
				o.setPklStawka(600.0);
				przeliczKwote(o);
			}
		
			
		kalk.bilansujDiety(ktgPozycji, false);
		if("KM".equals(ktgPozycji))
			kalk.bilansujRyczaltKM();
		
	}
	
	public void generujDiety_CS(String ktgPozycji, DeltKalkulacje kalk) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		Long tpozId = 0L;
		if("KM".equals(ktgPozycji) || "RD".equals(ktgPozycji) || "RH".equals(ktgPozycji)) {
			tpozId = getTpozIdByKtgPozycji(ktgPozycji); 
		} else {
			tpozId = getTpozIdByKtgDiety(ktgPozycji);
		}
		
		if(eq("H",ktgPozycji) || eq("RH", ktgPozycji)) {
			usunPozycjeKalkulacjiWgKtg("H", kalk);
			usunPozycjeKalkulacjiWgKtg("RH", kalk);
		}else 
			usunPozycjeKalkulacjiWgKtg(ktgPozycji, kalk);
		
		Map<Long, Double> listaKrajCzas = kalk.getDelCzynnosciSluzbowe().stream().collect(Collectors.groupingBy(cs -> cs.getCzynKrajId(), Collectors.summingDouble(cs -> cs.getCzynCzas())));
		
		
		for(Long krajId : listaKrajCzas.keySet()) {
			
			DeltPozycjeKalkulacji o = newPozycjaKalkulacji();
			o.setPklTpozId(tpozId);
			o.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(o.getPklTpozId()));
			
			if("P".equals(ktgPozycji))
			przeliczDodajPKL_Pobytowa( listaKrajCzas.get(krajId), tpozId, o, krajId, wniosek.getWndDataWyjazdu(),kalk, listaKrajCzas.get(krajId)>24.0);
			if("RH".equals(ktgPozycji)||"H".equals(ktgPozycji))
			przeliczDodajPKL_Hotelowa(listaKrajCzas.get(krajId), tpozId, o, krajId, wniosek.getWndDataWyjazdu(), kalk);
			if("RH".equals(ktgPozycji)) {
				o.setPklStawka(45.0);
				przeliczKwote(o);
			}

			else if("H".equals(ktgPozycji)) {
				o.setPklStawka(600.0);
				przeliczKwote(o);
			}
			
			if("KM".equals(ktgPozycji))
				przeliczDodajRyczalt_Komunikacyjny(tpozId, o, krajId, listaKrajCzas.get(krajId), kalk);
		}
		
		kalk.bilansujDiety(ktgPozycji, false);
		
	}
	
	
	private String getFormaPlatnosciZWniosku(Long tPozId) {
		 List<String> czyPozycjaZWniosku = kalkulacjaWniosek.getDeltPozycjeKalkulacjis().stream().filter(
				pkl -> tPozId.equals(pkl.getPklTpozId())).map(pkl -> pkl.getPklFormaPlatnosci()).collect(Collectors.toList());
			String formaPlatnosci = "";
		 if(!czyPozycjaZWniosku.isEmpty()) {
			 formaPlatnosci = czyPozycjaZWniosku.get(0);
		 }
		if(!nz(formaPlatnosci).isEmpty())
			return formaPlatnosci;
		else
			return "01";
	}
	

	public static Double sprawdzIleGodzinDel(DeltKalkulacje kalk) {
		Date dataWyjazd = kalk.getDeltTrasies().get(0).getTrsDataOd();
		Date dataPowrot = kalk.getDeltTrasies().get(kalk.getDeltTrasies().size() - 1).getTrsDataDo();
		
		if(dataWyjazd == null || dataPowrot == null) {
			User.alert("Nie uzupełniono wymaganych dat na planie trasy, popraw dane");
			return 0.0;
		}
		
		return (double) ((dataPowrot.getTime() - dataWyjazd.getTime())/(1000 * 60 * 60));

	}
	
	public void dodajTrase(DeltKalkulacje kalk) {
		
		DeltTrasy trasa = new DeltTrasy();
		trasa.setTrsKrIdDiety(1L);
		trasa.setTrsKrIdDo(1L);
		trasa.setTrsKrIdOd(1L);
		kalk.addDeltTrasy(trasa);
	}
	
	public List<DeltZrodlaFinansowania> skopiujZF(Long tpozId) {
		
		List<DeltZrodlaFinansowania> listaZFDoSkopiowania = new ArrayList<>();
		
		if(!kalkulacjaWniosek.getDeltPozycjeKalkulacjis().stream().filter(pkl -> tpozId.equals(pkl.getPklTpozId())).collect(Collectors.toList()).isEmpty()) {
			List<DeltZrodlaFinansowania> listaZF = kalkulacjaWniosek.getDeltPozycjeKalkulacjis().stream().filter(pkl -> tpozId.equals(pkl.getPklTpozId())).collect(Collectors.toList()).get(0).getDeltZrodlaFinansowanias();

			if(!listaZF.isEmpty()) {
				for(DeltZrodlaFinansowania zf : listaZF) {
					if(zf.getPkzfSkId()!=null && zf.getPkzfProcent()!=null){
					DeltZrodlaFinansowania z = new DeltZrodlaFinansowania();
					
					if(zf.getDelAngazes().size()>0)
					z = zf.clone(true);
					else z = zf.clone();
					
					listaZFDoSkopiowania.add(z);
					}
				}
			}
		}
		return listaZFDoSkopiowania;
	}
	
	HashMap<String, Long> pozKtgHM = new HashMap<>();
	public Long getTpozIdByKtgDiety(String ktgPozycji)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {

		Long ret = pozKtgHM.get(ktgPozycji) ; //= 5L;
		
		if (ret!=null){
			return ret;
		}
		
		Long wndStdId = this.wniosek.getWndStdId();
		Object oTpozId = this.execScalarQuery("select TPOZ_ID from PPT_DEL_TYPY_POZYCJI where TPOZ_STD_ID = ? and TPOZ_KATEGORIA = ? ",  
							wndStdId, ktgPozycji);
		if(wndStdId == null){
			User.alert("Wymagany jest typ stawki");
			ret = 0L;
		} else if (oTpozId==null){
			User.alert("Nie zdefiniowano typu pozycji z kat. %1$s i dla podanego typu stawki",  ktgPozycji);
			ret = 0L;
		} else {
			ret = Long.parseLong(""+oTpozId);
		}
		
		pozKtgHM.put(ktgPozycji, ret);
		
		return ret;
	}
	
	public Long getTpozIdByKtgPozycji( String ktgPozycji) {
		
		if(ktgPozycji == null || ktgPozycji.isEmpty())
			return 0L;
		
		BigDecimal ret = null;
		
		SqlDataSelectionsHandler typyPozycji = this.wniosek.getLwTypyPozycji();// this.getLW("PPL_DEL_TYPY_POZYCJI_K");
		if(typyPozycji != null) {
			for( DataRow r : typyPozycji.getData()) {
				if(r.get("tpoz_kategoria").equals(ktgPozycji)) {
					ret = (BigDecimal) r.get("tpoz_id");
					if(ret == null) {
						return 0L;
					}
				}

			}
		}

		return ret.longValue();
	}
	
//	public Long WalutaWgKraju(Long krajId){
//		if(krajId==null)
//			return 0L;
//
//		SqlDataSelectionsHandler waluty = this.getLW("PPL_KRAJE_WALUTY_ALL");
//		DataRow r = waluty.get(krajId);
//		if(r == null)
//			return 1L;
//
//		BigDecimal walId = (BigDecimal) r.get("WAL_ID");
//
//		return walId.longValue();
//	}


//    public void przeliczFiltryDomyslnegoZF() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
//		SqlDataSelectionsHandler zrodlaFinLW = this.getLW("PPL_DEL_ZRODLA_FIN");
//		zrodlaFinLW.clearFilter();
////		if("T".equals(wniosek.getWndFProjektowa())) {
////			zrodlaFinLW.setColFilter("par", "441?", false);//zagraniczne
////			zrodlaFinLW.setColFilter("prj", this.wniosek.getWndProjKod(), false);
////		} else {
//		String wndFCzySzkolenie = wniosek.getWndFCzySzkolenie();
//		if (wndFCzySzkolenie==null) wndFCzySzkolenie = "N";
//		SqlDataSelectionsHandler lwPrefiltrZF = this.getLW("PPL_DEL_PREFILTR_ZF", "K", wndFCzySzkolenie, this.wniosek.getWndProjKod());
//		for (DataRow r : lwPrefiltrZF.getData()) {
//			zrodlaFinLW.setColFilter((String)r.get("segment"), nz((String) r.get("colfilter")), false);
//		}
////		}
//
//
////		if("T".equals(wniosek.getWndFCzySzkolenie())) {
////			zrodlaFinLW.setColFilter("par", "4550||4700", false); // test fest
////			zrodlaFinLW.setColFilter("roz", "75001", false);//Nie wiem czy przy szkoleniach też sie to przyda...
////		}
////
////		else if("T".equals(wniosek.getWndFProjektowa())) {
//////			Object zfKod = this.execScalarQuery("select sk_kod from EGADM1.wdrv_css_stanowiska_kosztow where sk_kod like 'WY%' and prj = ?",
//////					this.wniosek.getWndProjKod());
//////			zrodlaFinLW.setColFilter("sk_kod", (String)zfKod, false);
////
////			zrodlaFinLW.setColFilter("par", "441?", false);//441?=krajowe projekty (ostatnia cyfra par jest dowolna w projektach)
////			zrodlaFinLW.setColFilter("prj", this.wniosek.getWndProjKod(), false);
////		} else {
////			zrodlaFinLW.setColFilter("par", "4410", false);
////			String departament = zwrocDepartamentPrc();
////			zrodlaFinLW.setColFilter("dep", departament, false);//WB: ustawiac w krajowych, a w zagraniczych chyba nie
////			//zrodlaFinLW.setColFilter("roz", "75001", false); //WB: nie ustawiac (na razie ... wrócimy do tego)
////		}
//
//		ustawDomyslneZaangazowanieSrodkow(zrodlaFinLW);
//
//	}
	
//	private String zwrocDepartamentPrc(){
//		DataRow prc = getLW("PRC").find(wniosek.getWndPrcId());
//		if (prc==null) return "";
//
//		DataRow rDepartamet = getLW("OB").find(prc.get("zat_ob_id"));
//		if (rDepartamet==null) return "";
//
//		return nz( (String) rDepartamet.get("ob_kod") );
//	}

	//TODO wywalic
	public void ustawDomyslneZaangazowanieSrodkow(SqlDataSelectionsHandler zrodlaFinLW) {	
		if(wniosek.getWndSkId() != null) {
			DataRow r = zrodlaFinLW.get(wniosek.getWndSkId());
			if(r != null) {
				String kodBudzetu = (String) r.get("kod_skrocony");
				this.getLW("PPL_DEL_ZAANGAZOWANIE_ZF").setColFilter("sk_kod_skrocony",kodBudzetu, true);
			}
		} 
//		else {
//			this.getLW("PPL_DEL_ZAANGAZOWANIE_ZF").setColFilter("dep",zwrocDepartamentPrc(), false);
//		}
	}

	private void przeliczDodajPKL_Pobytowa(Double ilegodzin, long tpozId, DeltPozycjeKalkulacji o, Long krId, Date dtKrOd, DeltKalkulacje kalk, Boolean delegacjaPowyzejDnia) {
		
		//		long tStart = new Date().getTime();
		kalk.addDeltPozycjeKalkulacji(o);
		o.setPklTpozId(tpozId);
		ustawZFAngaze(o);
		
//		o.setPklKrId(krId);
//		o.setPklSniadania(0L);
//		o.setPklObiady(0L);
//		o.setPklKolacje(0L);
//		o.setPklKosztMFProcent(this.zwrocProcentKosztMF(o.getPklTpozId()));
		o.setPklRefundacjaProcent(this.zwrocProcentRefundacji(o.getPklTpozId()));


	 	o.przeliczStawkaPobytowa(); //ustawStawkeDietyKraj(o, dtKrOd);
//		o.setPklKwotaMf(this.ustalKosztMf(o));
		o.setPklRefundacjaKwota(this.ustalKwoteRefundacji(o));
		o.setPklIlosc(ileDietKrajowe(ilegodzin, delegacjaPowyzejDnia));
		
//		aktualizujKwotyZF(o);
		
		
		//		System.out.println("Czas trwania metody przeliczDodajPKL_Pobytowa : " + (new Date().getTime() - tStart));
	}
	
	private void przeliczDodajPKL_Hotelowa(Double ilegodzin, long tpozId, DeltPozycjeKalkulacji o, Long krId, Date dtKrOd, DeltKalkulacje kalk) {
		
		//		long tStart = new Date().getTime();
		kalk.addDeltPozycjeKalkulacji(o);
		o.setPklTpozId(tpozId);
		ustawZFAngaze(o);
//		o.setPklKrId(krId);
//		o.setPklKosztMFProcent(this.zwrocProcentKosztMF(o.getPklTpozId()));
		o.setPklRefundacjaProcent(this.zwrocProcentRefundacji(o.getPklTpozId()));
		
//		ustawStawkeiWaluteDietyHotel(o, dtKrOd);
//		o.setPklKwotaMf(this.ustalKosztMf(o));
		o.setPklRefundacjaKwota(this.ustalKwoteRefundacji(o));
		
//		aktualizujKwotyZF(o);
		if(kalk.getDelCzynnosciSluzbowe().size()>0)
			o.setPklIlosc(ileDietHotelowych_CS(ilegodzin));
		else o.setPklIlosc(ileDietHotelowych(kalk));
		
		if(o.getPklIlosc() == 0)
			kalk.removeDeltPozycjeKalkulacji(o);
		
		
		//		System.out.println("Czas trwania metody przeliczDodajPKL_Hotelowa : " + (new Date().getTime() - tStart));
	}
	
	private void przeliczDodajRyczalt_Komunikacyjny(Long tpozId, DeltPozycjeKalkulacji o, Long krId, Double czas, DeltKalkulacje kalk) {
		kalk.addDeltPozycjeKalkulacji(o);
		
		o.setPklIlosc(ileDniRyczaltKom(czas));
		o.setPklTpozId(tpozId);
		o.setPklKrId(krId);
		o.setPklKosztMFProcent(this.zwrocProcentKosztMF(o.getPklTpozId()));
		o.setPklRefundacjaProcent(this.zwrocProcentRefundacji(o.getPklTpozId()));
		
		ustawZFAngaze(o);
		
		o.setPklStawka(6.0);
		o.setPklKwota(o.getPklStawka() * o.getPklIlosc());
		o.setPklKwotaMf(this.ustalKosztMf(o));
		o.setPklRefundacjaKwota(this.ustalKwoteRefundacji(o));
		
		aktualizujKwotyZF(o);
		
		if(o.getPklIlosc() == 0)
			kalk.removeDeltPozycjeKalkulacji(o);
	}
	
	public static double ileDniRyczaltKom(Double czas) {
		double ret = 0;
		double hours = czas;
		double dni = Math.floor((hours / 24));
		
		double x = hours - dni * 24;

		ret = dni;
		if (x > 0)
			ret += 1.0000;

		return ret;
	}
	
	public Double ustalKosztMf(DeltPozycjeKalkulacji pkl) {
		if(pkl.getPklKwota() != null)
//		
//		if(pkl.getPklKosztMFProcent() != null) {
//		Double procentMF = pkl.getPklKosztMFProcent()/100.0;
//		
		return pkl.getPklKwota();

//		return procentMF * pkl.getPklKwota();
//		}
		else return 0.0;
	}
	
	private Double ustalKwoteRefundacji(DeltPozycjeKalkulacji pkl) {
		
		//		long tStart = new Date().getTime();
		
		Double procentRefundacji = nz(pkl.getPklRefundacjaProcent())/100.0;
		
		//		System.out.println("Czas trwania metody ustalKwoteRefundacji : " + (new Date().getTime() - tStart));
		return procentRefundacji * nz(pkl.getPklKwota());
	}

	private void usunPozycjeKalkulacjiWgKtg(String ktgPozycji, DeltKalkulacje kalk) {
		
		//		long tStart = new Date().getTime();
		
		// kasuje ew. istniejace diety...
		HashSet<DeltPozycjeKalkulacji> rem = getPozycjeKalkWgKtg(ktgPozycji, kalk);
		for (DeltPozycjeKalkulacji p : rem) {
			kalk.removeDeltPozycjeKalkulacji(p);
			}
		//		System.out.println("Czas trwania metody usunPozycjeRozliczeniaWgKtg : " + (new Date().getTime() - tStart));
	}

	private HashSet<DeltPozycjeKalkulacji> getPozycjeKalkWgKtg(String ktgPozycji, DeltKalkulacje kalk) {
		
		//		long tStart = new Date().getTime();
		
		SqlDataSelectionsHandler lw = this.wniosek.getLwTypyPozycji();// this.getLW("PPL_DEL_TYPY_POZYCJI_K");
		HashSet<DeltPozycjeKalkulacji> rem = new HashSet<DeltPozycjeKalkulacji>();
		for (DeltPozycjeKalkulacji pkl : kalk.getDeltPozycjeKalkulacjis()) {
			Long pklTpozId = pkl.getPklTpozId();
			DataRow r = lw.get(pklTpozId);
			if(r != null) {
				if (ktgPozycji.equals(r.get("TPOZ_KATEGORIA"))){
					rem.add(pkl);
				}
			}
		}
		
		//		System.out.println("Czas trwania metody getPozycjeRozliczeniaWgKtg : " + (new Date().getTime() - tStart));
		return rem;
	}

	
	public static double ileDietKrajowe(Double ilegodzin, Boolean delegacjaPowyzejDnia) {
		
		//		long tStart = new Date().getTime();
		
		double ret = 0;
		double hours = ilegodzin;
		double dni = Math.floor((hours / 24));

		double x = hours - dni * 24;
		
		ret = dni;
		
		if(delegacjaPowyzejDnia) {
			if(x > 0 && x <= 8.000) {
				ret += 0.5;
			}
			
			if(x > 8) {
				ret += 1.0;
			}
		} else {
			if (x >= 12.000)
				ret += 1.0;
			else if (x >= 8.000)
				ret += 0.5;
		}
		
		//		System.out.println("Czas trwania metody ileDietKrajowe : " + (new Date().getTime() - tStart));

		return ret;
	}
	
//	public void ustawStawkeDietyKraj(DeltPozycjeKalkulacji p, Date pobytOd) {
//
//		//		long tStart = new Date().getTime();
//
//		if(pobytOd == null)
//			pobytOd = today();
//
//		String krPkl = "" + p.getPklKrId();
//		for (DataRow r : this.getLW("PPL_DEL_STAWKI_DIET_ZAGR", this.wniosek.getWndStdId()).getData()) {
//			String krIdWst = "" + r.get("WST_KR_ID");
//			String walIdWst = "" + r.get("WST_WAL_ID");
//			String kwota = "" + r.get("WST_KWOTA");
//			Date obowiazujeOd = (Date) r.get("WST_OBOWIAZUJE_OD");
//			if (krPkl.equals(krIdWst) && pobytOd.after(obowiazujeOd)) {
//				p.setPklWalId(Long.parseLong(walIdWst));
//				p.setPklStawka(Double.parseDouble(kwota));
//				przeliczKwote(p);
//				return;
//			}
//		}
//		przeliczKwote(p);
//
//		//		System.out.println("Czas trwania metody ustawStawkeDietyKraj : " + (new Date().getTime() - tStart));
//	}

//	public void ustawStawkeiWaluteDietyHotel(DeltPozycjeKalkulacji p, Date pobytOd) {
//		//		long tStart = new Date().getTime();
//
//		String krPkl = "" + p.getPklKrId();
//		for (DataRow r : this.getLW("PPL_DEL_STAWKI_DIET_HOT", this.wniosek.getWndStdId()).getData()) {
//			String krIdWst = "" + r.get("WST_KR_ID");
//			String walIdWst = "" + r.get("WST_WAL_ID");
//			String kwota = "" + r.get("WST_KWOTA");
//			Date obowiazujeOd = (Date) r.get("WST_OBOWIAZUJE_OD");
//			if (krPkl.equals(krIdWst) && pobytOd.after(obowiazujeOd)) {
//				p.setPklWalId(Long.parseLong(walIdWst));
//				p.setPklStawka(Double.parseDouble(kwota));
//				przeliczKwote(p);
//				return;
//			}
//		}
//		przeliczKwote(p);
//		//		System.out.println("Czas trwania metody ustawStawkeiWaluteDietyHotel : " + (new Date().getTime() - tStart));
//	}

	private double ileDietHotelowych(DeltKalkulacje kalk) {
		
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date dataOdFormatowana = null;
		Date dataDoFormatowana = null;
		try {
			dataOdFormatowana = formatter.parse(formatter.format(kalk.getDeltTrasies().get(0).getTrsDataOd()));
			dataDoFormatowana = formatter.parse(formatter.format(kalk.getDeltTrasies().get(kalk.getDeltTrasies().size() -1).getTrsDataDo()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return Days.daysBetween(new DateTime(dataOdFormatowana), new DateTime(dataDoFormatowana)).getDays();
		
		
		//		long tStart = new Date().getTime();
		
//		double dni = kalk.getDeltTrasies().get(kalk.getDeltTrasies().size() -1).getTrsDataDo().getDate() - kalk.getDeltTrasies().get(0).getTrsDataOd().getDate();
//		double dni = Math.floor((ilegodzin / 24));
		//		System.out.println("Czas trwania metody ileDietHotelowych : " + (new Date().getTime() - tStart));
		
//		return dni;
	}
	
	public static double ileDietHotelowych_CS(Double czas) {
		double hours = czas;
		double dni = Math.floor((hours / 24));

		return dni;
	}

	public void przeliczKwote(DeltPozycjeKalkulacji p) {
//
//		Long pklIlosc =  ((Double) (nz(p.getPklIlosc())  * 10000)).longValue() ;
//		Long pklStawka = ((Double) (nz(p.getPklStawka()) * 10000)).longValue() ;
//
//		if (p.getPklIlosc() == null || p.getPklIlosc() == 0.0 || p.getPklStawka() == null || p.getPklStawka() == 0.0) {
//			p.setPklKwota(0.0);
//			p.setPklKwotaMf(0.0);
//			p.setPklKwotaPln(0.0);
//			return;
//		}
//
///*		Long ret = 0l;
//		ret = round(p.getPklIlosc() * p.getPklStawka(), 2);
//		ret -= round(nz(p.getPklSniadania()) * p.getPklStawka() * 0.25, 2);
//		ret -= round(nz(p.getPklObiady()) * p.getPklStawka() * 0.50, 2);
//		ret -= round(nz(p.getPklKolacje()) * p.getPklStawka() * 0.25, 2);
//
//		if (ret < 0.0)
//			ret = 0.0;*/
//
//
//		Long ret = 0l;
//		ret = (pklIlosc * pklStawka);
//		ret -= nz(p.getPklSniadania()) * pklStawka * 2500; //0.25;
//		ret -= nz(p.getPklObiady()) * pklStawka * 5000;// 0.50;
//		ret -= nz(p.getPklKolacje()) * pklStawka * 2500; // 0.25;
//
//		if (ret < 0l) ret = 0l;
//
//		//p.setPklKwota(round(ret, 2));
//		p.setPklKwota(round((1.0d * ret) / (10000*10000), 2));
//		if(this.ustalKosztMf(p) != 0.0)
//		p.setPklKwotaMf(this.ustalKosztMf(p));
//		aktualizujKwotyZF(p); //TODO sprwadź w działaniu jak to się sprawuje
//
//		//		System.out.println("Czas trwania metody przeliczKwote : " + (new Date().getTime() - tStart));
	}

/*	public static double round(double value, int places) {
		
		//		long tStart = new Date().getTime();
		
		if (places < 0)
			throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		
		//		System.out.println("Czas trwania metody round : " + (new Date().getTime() - tStart));
		
		return (double) tmp / factor;
	}*/
	
	public double ilePosilkow(DeltPozycjeKalkulacji pkl){
		double ret = 0.0;
		double roznica = round(pkl.getPklIlosc(), 0) - pkl.getPklIlosc();
		if(roznica == 0 || roznica == 0.5)
			ret = round(pkl.getPklIlosc(), 0);
		else 
			ret = round(pkl.getPklIlosc(), 0) + 1;
		
		return ret;
	}
	
	public void aktualizujKwoteRozl(DelZaliczki pozycjaRozliczenia) {
		
		//		long tStart = new Date().getTime();
		
		Double wydanaZaliczka = nz(pozycjaRozliczenia.getDkzZalWydano());
		Double pobranaZaliczka = 0.0;
		
		if("T".equals(this.wniosek.getWndFZaliczka())) {
			pobranaZaliczka = kalkulacjaWstepna.getDelZaliczki().stream().filter(zal -> zal.getDkzDokId() != null && zal.getDkzWalId().equals(pozycjaRozliczenia.getDkzWalId()))
				.collect(Collectors.toList()).get(0).getDkzKwota();
		}
		
		if(wydanaZaliczka > pobranaZaliczka) {
			User.alert("Podano nieprawidłową kwotę pobranej zaliczki");
			return;
		}
		
		Double kwotaKarta = nz(pozycjaRozliczenia.getDkzKartaKwota());
		
		Double srodkiWlasne = nz(pozycjaRozliczenia.getDkzSrodkiWlasne());
		
		if((Math.abs(kwotaKarta) + Math.abs(wydanaZaliczka) + Math.abs(srodkiWlasne)) != this.getPodsumowanieRozl()) {
			User.alert("Wskazany łączny koszt jest inny niż wynikający z podsumowania kosztów"); // tukej
			return;
		}
		
		if(wydanaZaliczka > this.getPodsumowanieRozl()){
			User.alert("Kwota wydanej zaliczki przewyższa kwotę wynikającą z podsumowania rozliczenia delegacji, popraw kwoty");
		}
			
		pozycjaRozliczenia.setDkzKwota(
				 wydanaZaliczka - pobranaZaliczka + srodkiWlasne);
		
		//		System.out.println("Czas trwania metody aktualizujKwoteRozl : " + (new Date().getTime() - tStart));

	}

	
	
	/**
	 * metoda wykorzystywana w prtzycisku na Tab II do kopiwoania pozycji kosztów nie będących dietami 
	 */
//	public void kopiujKoszty() {  //tukej błąd1 kolejny raz
//
//		//		long tStart = new Date().getTime();
//
//		if (kalkulacjaWniosek != null) {
//
//			HashSet<DeltPozycjeKalkulacji> pklP = getPozycjeKalkWgKtg("P",kalkulacjaWniosek);
//			HashSet<DeltPozycjeKalkulacji> pklH = getPozycjeKalkWgKtg("H",kalkulacjaWniosek);
//
//			// czyszczenie...
//			ArrayList<DeltPozycjeKalkulacji> rem = new ArrayList<DeltPozycjeKalkulacji>();
//			for (DeltPozycjeKalkulacji p : this.getKalkulacjaWstepna().getDeltPozycjeKalkulacjis()) {
//				if (!pklP.contains(p) && !pklH.contains(p) && !(p.getPklKwota()>0))
//					rem.add(p);
//			}
//			for (DeltPozycjeKalkulacji p : rem)
//				this.getKalkulacjaWstepna().removeDeltPozycjeKalkulacji(p);
//
//			List<Long> tpozIdList = new ArrayList<Long>();
//			for(DeltPozycjeKalkulacji pkl :  kalkulacjaWstepna.getDeltPozycjeKalkulacjis()){
//				tpozIdList.add(pkl.getPklTpozId());
//			}
//
//			for (DeltPozycjeKalkulacji pWst : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
//				if(tpozIdList.contains(pWst.getPklTpozId()) || !pWst.getKategoria().equals("I")) //if (pWst.getPklTpozId() <= 6L) -> 21.12.2017 || pWst.getPklKwota() == 0.0
//					continue;
//
//				DeltPozycjeKalkulacji pRozl = newPozycjaKalkulacji();
//				pRozl.setPklTpozId(pWst.getPklTpozId());
//				pRozl.setPklKrId(pWst.getPklKrId());
//				//if(pWst!=null && pWst.getDeltZrodlaFinansowanias()!=null && !pWst.getDeltZrodlaFinansowanias().isEmpty()) pRozl.setDeltZrodlaFinansowanias(pWst.getDeltZrodlaFinansowanias());
//				pRozl.setPklKosztMFProcent(pWst.getPklKosztMFProcent());
//				pRozl.setPklKosztZaprProcent(pWst.getPklKosztZaprProcent());
//				pRozl.setPklRefundacjaProcent(pWst.getPklRefundacjaProcent());
//				pRozl.setPklOpis(pWst.getPklOpis());
//                pRozl.setPklIlosc(1.0);
//				pRozl.setPklKwota(0.0);
//				pRozl.setPklKwotaMf(0.0);
//				pRozl.setPklFormaPlatnosci(this.getFormaPlatnosciZWniosku(pRozl.getPklTpozId()));
//
//				this.getKalkulacjaWstepna().addDeltPozycjeKalkulacji(pRozl);
//
//				dodajDomyslneZfAn(pRozl);
//
//			}
//
//		}
//
//		//		System.out.println("Czas trwania metody kopiujKoszty : " + (new Date().getTime() - tStart));
//	}
	
	//Modyfikacja po konsultacji z Michałem
	public void kopiujDaneZDelegacjiGrupowej( ) {
		
		//		long tStart = new Date().getTime();

		wniosek.setWndWndId(this.getWndWndId());
		
		DeltWnioskiDelegacji wniosekPowiazany = (WniosekDelegacjiKraj) HibernateContext.get(WniosekDelegacjiKraj.class, wniosek.getWndWndId());// wczytajWniosekDelegacjiDB(wniosek.getWndWndId());
		wniosek.setWndWndId(wniosekPowiazany.getWndId());
		

		
		kopiujDeltCeleDelegacjis(wniosekPowiazany);		
		
		kopiujDeltZapraszajacys(wniosekPowiazany);		
		
		kopiujDeltSrodkiLokomocjis(wniosekPowiazany);		
		
		kopiujDeltKalkulacjes(wniosekPowiazany);
		
		
		
		wniosek.setWndDataWyjazdu(wniosekPowiazany.getWndDataWyjazdu());
		wniosek.setWndDataPowrotu(wniosekPowiazany.getWndDataPowrotu());
		wniosek.setWndTrasaWyjazdu(wniosekPowiazany.getWndTrasaWyjazdu());
		wniosek.setWndTrasaPowrotu(wniosekPowiazany.getWndTrasaPowrotu());
        ustawReferencjeKalkulacji();
        
		//		System.out.println("Czas trwania metody kopiujDaneZDelegacjiGrupowej : " + (new Date().getTime() - tStart));
	}

	private void kopiujDeltKalkulacjes(DeltWnioskiDelegacji wniosekPowiazany) {
		if (wniosekPowiazany==null || wniosekPowiazany.getKalkulacjaWniosek()==null) return;

		final DeltKalkulacje kalWnkOld = wniosek.getKalkulacjaWniosek();
		List<DelZaliczki> zaliczkiWniosek = kalWnkOld.getDelZaliczki();

		DeltKalkulacje kalkCopy = wniosekPowiazany.getKalkulacjaWniosek().clone(true);
		kalkCopy.setKalId(0L);

		kalkCopy.clearAllZaliczki();//nie klonujemy ustawien dla zaliczek - i tak nie podlegają klonowaniu
		for(DelZaliczki z : zaliczkiWniosek){
			z.setDkzId(0L);
			kalkCopy.addDeltZaliczki(z);
		}

		wniosek.clearDeltKalkulacjes(); // czyscWniosekDeltKalkulacjes();
		wniosek.addDeltKalkulacje(kalkCopy);


//stary kod... prowokuje błędy
//		if(zaliczkiWniosek.size()>1 || zaliczkiWniosek.get(0).getDkzNrKonta() != null || zaliczkiWniosek.get(0).getDkzOpis() != null) {
//			for(DelZaliczki z : zaliczkiWniosek){
//				z.setDkzId(0L);
//				kalkCopy.addDeltZaliczki(z);
//			}
//		} else {
//			kalkCopy.addNewDeltZaliczki();// .addDeltZaliczki(addNewZaliczka());
//		}
//		wniosek.addDeltKalkulacje(kalkCopy);
	}

	private void kopiujDeltSrodkiLokomocjis(DeltWnioskiDelegacji wniosekPowiazany) {
		
		//		long tStart = new Date().getTime();
		
		czyscWniosekDeltSrodkiLokomocjis();
		for( DeltSrodkiLokomocji lok : wniosekPowiazany.getDeltSrodkiLokomocjis()) {
			DeltSrodkiLokomocji lokCopy = lok.clone();
			lokCopy.setSlokId(0L);
			wniosek.addDeltSrodkiLokomocji(lokCopy);//lokCopy.setDeltWnioskiDelegacji(wniosek);
		}
		
		//		System.out.println("Czas trwania metody kopiujDeltSrodkiLokomocjis : " + (new Date().getTime() - tStart));
	}

	private void kopiujDeltZapraszajacys(DeltWnioskiDelegacji wniosekPowiazany) {
		
		//		long tStart = new Date().getTime();
		
		czyscWniosekDeltZapraszajacys();
		for( DeltZapraszajacy zapr : wniosekPowiazany.getDeltZapraszajacys()) {
			DeltZapraszajacy zaprCopy = zapr.clone();
			zaprCopy.setZaprId(0L);
			wniosek.addDeltZapraszajacy(zaprCopy);			//zaprCopy.setDeltWnioskiDelegacji(wniosek);
		}
		
		//		System.out.println("Czas trwania metody kopiujDeltZapraszajacys : " + (new Date().getTime() - tStart));
	}

private void kopiujDeltCeleDelegacjis(DeltWnioskiDelegacji wniosekPowiazany) {
	
	//		long tStart = new Date().getTime();
	
	czyscWniosekDeltCeleDelegacjis();
	for (DeltCeleDelegacji cel : wniosekPowiazany.getDeltCeleDelegacjis()) {
		DeltCeleDelegacji c1 = cel.clone();
		c1.setCdelId(0L);// ID!!!!
		wniosek.addDeltCeleDelegacji(c1); //c1.setDeltWnioskiDelegacji(wniosek);
	}
	
	//		System.out.println("Czas trwania metody kopiujDeltCeleDelegacjis : " + (new Date().getTime() - tStart));
}

private void czyscWniosekDeltCeleDelegacjis() {
	//		long tStart = new Date().getTime();
	
	for (Object p : wniosek.getDeltCeleDelegacjis().toArray()) {
		wniosek.removeDeltCeleDelegacji((DeltCeleDelegacji) p);
	}
	//		System.out.println("Czas trwania metody czyscWniosekDeltCeleDelegacjis : " + (new Date().getTime() - tStart));
}

//private void czyscWniosekDeltKalkulacjes() {
//
//	//		long tStart = new Date().getTime();
//
//	for(Object k : wniosek.getDeltKalkulacjes().toArray()) {
//		wniosek.removeDeltKalkulacje((DeltKalkulacje)k);
//	}
//	//		System.out.println("Czas trwania metody czyscWniosekDeltKalkulacjes : " + (new Date().getTime() - tStart));
//}

private void czyscWniosekDeltSrodkiLokomocjis() {
	
	//		long tStart = new Date().getTime();
	
	for( Object l : wniosek.getDeltSrodkiLokomocjis().toArray()) {
		wniosek.removeDeltSrodkiLokomocji((DeltSrodkiLokomocji)l);
	}
	
	//		System.out.println("Czas trwania metody czyscWniosekDeltSrodkiLokomocjis : " + (new Date().getTime() - tStart));
}

	private void czyscWniosekDeltZapraszajacys() {
		//		long tStart = new Date().getTime();
		
		for( Object z: wniosek.getDeltZapraszajacys().toArray()) {
			wniosek.removeDeltZapraszajacy((DeltZapraszajacy)z);
		}
		
		//		System.out.println("Czas trwania metody czyscWniosekDeltZapraszajacys : " + (new Date().getTime() - tStart));
	}

	public void generujTrasy() {

		//		long tStart = new Date().getTime();
		
		wyczyscTrasy();

		DeltCeleDelegacji c0 = new DeltCeleDelegacji();
		c0.setCdelKrId(1L);
		
		String miejscowoscPoczatkowa = wniosek.getDeltCeleDelegacjis().get(0).getCdelMiejscowoscOd();

		List<DeltCeleDelegacji> cele = wniosek.getDeltCeleDelegacjis().stream()
		.filter(c -> c.getCdelMiejscowosc().isEmpty() || c.getCdelMiejscowoscOd().isEmpty() )
		.collect(Collectors.toList());

		if(cele.size() > 0) {
			User.alert("Część I - trasa i środki transportu - nie wszystkie miejscowości zostały wprowadzone, popraw dane ");
			return;
		} else if(wniosek.getDeltSrodkiLokomocjis().get(0).getSlokSrodekLokomocji() == null) {
			User.alert("Nie wybrano środka transportu, popraw dane");
			return;
		} else if(wniosek.getDeltCeleDelegacjis().get(0).getCdelMiejscowoscZakonczenia().isEmpty()){
			User.alert("Miejscowość zakończenia nie może być pusta, popraw dane");
			return;
		}
		
		if(wniosek.getWndDataWyjazdu() == null || wniosek.getWndDataPowrotu() == null) {
			User.alert("Termin trwania delegacji musi zostać wprowadzony");
			return;
		}
		
		int i = 1;
		for (DeltCeleDelegacji c : wniosek.getDeltCeleDelegacjis()) {
			
			DeltTrasy t = new DeltTrasy();
			t.setTrsKrIdOd(c0.getCdelKrId());
			t.setTrsMiejscowoscOd(c.getCdelMiejscowoscOd());
			t.setTrsKrIdDo(c.getCdelKrId());
			t.setTrsKrIdDiety(c.getCdelKrId());
			t.setTrsMiejscowoscDo(c.getCdelMiejscowosc());
			t.setTrsSrodekLokomocji(c.getDeltWnioskiDelegacji().getDeltSrodkiLokomocjis().get(0).getSlokSrodekLokomocji());

			if (i == 1)
				t.setTrsDataOd(wniosek.getWndDataWyjazdu());// domyslna I data
															// to data wyjazdu
															// wg wniosku
			t.setTrsCzynnosciSluzboweOd(t.getTrsDataOd());
			getKalkulacjaWstepna().addDeltTrasy(t);

			c0 = c;
			i++;
		}

		// ostatnia trasa - powrot do PL/Wawa
		DeltTrasy t = new DeltTrasy();
		// t.setTrsLp(rozliczenie.getDeltTrasies().size() + 1L);
		t.setTrsKrIdOd(c0.getCdelKrId());
		t.setTrsKrIdDiety(c0.getCdelKrId());
		t.setTrsMiejscowoscOd(c0.getCdelMiejscowosc());
		t.setTrsKrIdDo(1L);
		
		List<DeltCeleDelegacji> mjscZak = 
				wniosek.getDeltCeleDelegacjis().stream().filter(cel ->! nz(cel.getCdelMiejscowoscZakonczenia()).isEmpty()).collect(Collectors.toList());
		if(!mjscZak.isEmpty())
			t.setTrsMiejscowoscDo(mjscZak.get(0).getCdelMiejscowoscZakonczenia());
		else
			t.setTrsMiejscowoscDo(miejscowoscPoczatkowa);
		
		t.setTrsSrodekLokomocji(c0.getDeltWnioskiDelegacji().getDeltSrodkiLokomocjis().get(0).getSlokSrodekLokomocji());

		//t.setTrsDataDo(wniosek.getWndDataPowrotu());// domyslna data powrotu wg
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(wniosek.getWndDataPowrotu());
		cal.set(Calendar.HOUR_OF_DAY, 1);
		t.setTrsDataDo(cal.getTime());
													// wniosku
		t.setTrsCzynnosciSluzboweDo(t.getTrsDataDo());

		getKalkulacjaWstepna().addDeltTrasy(t);
		this.zapisz();
		
		//		System.out.println("Czas trwania metody generujTrasy : " + (new Date().getTime() - tStart));

	}

	public void addKoszt(DeltKalkulacje kalkulacja) {
		
		//		long tStart = new Date().getTime();
		DeltPozycjeKalkulacji p = newPozycjaKalkulacji();
		kalkulacja.addDeltPozycjeKalkulacji(p);
		p.setPklTpozId(this.getTpozIdByKtgPozycji("I"));
		ustawZFAngaze(p);
		p.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(p.getPklTpozId()));
//		p.setPklKosztMFProcent(zwrocProcentKosztMF(p.getPklTpozId())); // tukej
		
		p.setPklIlosc(1.0);
		//		System.out.println("Czas trwania metody addKoszt : " + (new Date().getTime() - tStart));
	}
	
	public void ustawProcentKwoteMf(DeltPozycjeKalkulacji pkl) {
		
		//		long tStart = new Date().getTime();
		
		if(pkl.getPklTpozId() != null && pkl.getPklKosztMFProcent() == null) {
			pkl.setPklKosztMFProcent(this.zwrocProcentKosztMF(pkl.getPklTpozId()));
			pkl.setPklKwotaMf(this.ustalKosztMf(pkl));
		}
		
		//		System.out.println("Czas trwania metody ustawProcentKwoteMf : " + (new Date().getTime() - tStart));
	}

	public void addDietaPobytowa(DeltKalkulacje kalkulacja) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

		//		long tStart = new Date().getTime();
		DeltPozycjeKalkulacji p = newPozycjaKalkulacji();
		kalkulacja.addDeltPozycjeKalkulacji(p);
		p.setPklTpozId(getTpozIdByKtgDiety("P"));
		ustawZFAngaze(p);
		p.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(p.getPklTpozId()));
		p.setPklStawka(30.0);
//		p.setPklSniadania(0L);
//		p.setPklObiady(0L);
//		p.setPklKolacje(0L);
		p.setPklRefundacjaProcent(this.zwrocProcentRefundacji(p.getPklTpozId()));
		
		p.setPklIlosc(1.0);
		
		//		System.out.println("Czas trwania metody addDietaPobytowa : " + (new Date().getTime() - tStart));
	}

	private void ustawZFAngaze(DeltPozycjeKalkulacji p) {
		p.getDeltZrodlaFinansowanias().clear();
		
		if(skopiujZF(p.getPklTpozId()).size() != 0) {
			for(DeltZrodlaFinansowania zf : skopiujZF(p.getPklTpozId())) {
				p.addDeltZrodlaFinansowania(zf);
			}
		}
		
		if(p.getDeltZrodlaFinansowanias().isEmpty()) {
			dodajDomyslneZfAn(p);
		}
	}

	public void addDietaHotelowa(DeltKalkulacje kalkulacja, String rodzaj) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		//		long tStart = new Date().getTime();		
		DeltPozycjeKalkulacji p = newPozycjaKalkulacji();
		kalkulacja.addDeltPozycjeKalkulacji(p);
		p.setDeltKalkulacje(kalkulacja);
		if("H".equals(rodzaj)) {
			p.setPklTpozId(getTpozIdByKtgDiety("H"));
			ustawZFAngaze(p);
			p.setPklStawka(600.0);
		}
		else {
				p.setPklTpozId(getTpozIdByKtgPozycji("RH"));
				ustawZFAngaze(p);
				p.setPklStawka(45.0);
		}
		p.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(p.getPklTpozId()));
		p.setPklIlosc(1.0);
		
		p.setPklRefundacjaProcent(this.zwrocProcentRefundacji(p.getPklTpozId()));
		p.setPklKosztMFProcent(this.zwrocProcentKosztMF(p.getPklTpozId()));
		
//		przeliczKwote(p);
		
		
		//		System.out.println("Czas trwania metody addDietaHotelowa : " + (new Date().getTime() - tStart));
	}
	
	public void addRyczaltKM(DeltKalkulacje kalkulacja) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		//		long tStart = new Date().getTime();		
		
		DeltPozycjeKalkulacji p = newPozycjaKalkulacji();
		kalkulacja.addDeltPozycjeKalkulacji(p);
		p.setPklTpozId(getTpozIdByKtgPozycji("KM"));
		ustawZFAngaze(p);
		p.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(p.getPklTpozId()));
		p.setPklStawka(6.0);
		p.setPklRefundacjaProcent(this.zwrocProcentRefundacji(p.getPklTpozId()));
		p.setPklKosztMFProcent(this.zwrocProcentKosztMF(p.getPklTpozId()));
		
		p.setPklIlosc(1.0);
//		przeliczKwote(p);
		
		
		//		System.out.println("Czas trwania metody addRyczaltKM : " + (new Date().getTime() - tStart));
	}


	private void dodajDomyslneZfAn(DeltPozycjeKalkulacji p) {
		if("T".equals(wniosek.getWndFDomyslneZf()) && wniosek.getWndSkId() != null && wniosek.getWndWnrId() != null){
			DeltZrodlaFinansowania zf = new DeltZrodlaFinansowania();
			DelAngaze an = new DelAngaze();
			zf.setPkzfProcent(100.0);
			zf.setPkzfKwota(0.0);
			zf.setPkzfSkId(wniosek.getWndSkId());
			an.setAngWnrId(wniosek.getWndWnrId());
			an.setAngProcent(100.0);
			an.setAngKwota(0.0);
			zf.addDelAngaze(an);
			p.addDeltZrodlaFinansowania(zf);
		}
	}


	private void wyczyscTrasy() {
		HashSet<DeltTrasy> trasy = new HashSet<DeltTrasy>();
		trasy.addAll(getKalkulacjaWstepna().getDeltTrasies());
		for (DeltTrasy t : trasy) {
			getKalkulacjaWstepna().removeDeltTrasy(t);
		}
	}

	private DeltPozycjeKalkulacji newPozycjaKalkulacji() {
		DeltPozycjeKalkulacji pk = new DeltPozycjeKalkulacji();
		pk.setPklIlosc(0.000);
		pk.setPklStawka(0.0);
		pk.setPklKwotaPrzelewu(0L);
		pk.setPklKwotaMf(0.0);
		pk.setPklRefundacjaKwota(0.0);
		pk.setPklKwotaGotowki(" ");
		pk.setPklWalId(1L);
		pk.setPklKwota(0.0);
		pk.setPklKrId(1L);
		
		return pk;
	}

	private WniosekDelegacjiKraj wczytajWniosek(long id) {
		final WniosekDelegacjiKraj wniosek = (WniosekDelegacjiKraj) HibernateContext.get(WniosekDelegacjiKraj.class, id);
		this.setWniosek(wniosek);
		ustawReferencjeKalkulacji();
		naprawEncje();
		return this.wniosek;
	}

//	private WniosekDelegacjiKraj wczytajWniosekDelegacjiDB(long id) {
//		WniosekDelegacjiKraj wniosek = new WniosekDelegacjiKraj();
//		wniosek = (WniosekDelegacjiKraj) HibernateContext.get(WniosekDelegacjiKraj.class, id);
//		return wniosek;
//	}



	public boolean zapisz() {
		
		if (!wniosek.validate())
			return false;

//		showEntityDetails();
		
		try {
			
			if (eq(nz(this.wniosek.getWndId()),0L))//jesli to nowa delegacja to potrzebna aktualizacja tabView aby odblokowac czesc II i czesc III
				Utils.updateComponent("tabViewId");
			
			HibernateContext.saveOrUpdate(this.wniosek);
			ustawReferencjeKalkulacji();
			User.info("Pomyślnie zapisano wniosek %1$s", wniosek.getWndNumer());
			Utils.setFormName("Delegacja krajowa nr: " + this.wniosek.getWndNumer());//RequestContext.getCurrentInstance().execute("parent.setFormName('Delegacja krajowa nr: " + this.wniosek.getWndNumer() + "')");
			return true;
		} catch (Exception e) {
			User.alert(e); //User.alert(e.getMessage(), e);
		}
		
		return false;
	}

	public void usun() {
		
		try (Connection conn = JdbcUtils.getConnection(false)) {
			obieg.usunDaneObiegu();
		} catch (SQLException e) {
			log.error(e.getMessage(), e);
			User.alert("Nieudane usuniecie danych obiegu: " + e);
			return;
		}

		try {
			ZalacznikiBean.deleteAll("ZAL_DEL_ID", wniosek.getWndId());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert("Błąd przy próbie usunięcia załączników: " + e);
			return;
		}

		try {
			HibernateContext.delete(wniosek);
			User.info("Usunięto delegację");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert("Błąd w usuwaniu delegacji!", e);
			return;
		}

		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("Delegacje");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void anulujWniosek() {
		//		long tStart = new Date().getTime();	
		
		this.wniosek.setWndStatus(0L);
		zapisz();
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("Delegacje");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//		System.out.println("Czas trwania metody anulujWniosek : " + (new Date().getTime() - tStart));
	}
	
	public void raportRozliczenie() { 
		
		//		long tStart = new Date().getTime();	
		
		if (wniosek.getWndId() != 0) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("wnd_id", wniosek.getWndId());
				com.comarch.egeria.web.common.reports.ReportGenerator.displayReportAsPDF(params, "Delegacja_krajowa_rozliczenie");

			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
		//		System.out.println("Czas trwania metody raportRozliczenie : " + (new Date().getTime() - tStart));
	}
	
	public void raportPolecenie() { 
		
		//		long tStart = new Date().getTime();	
		
		if (wniosek.getWndId() != 0) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("wnd_id", wniosek.getWndId());
				com.comarch.egeria.web.common.reports.ReportGenerator.displayReportAsPDF(params, "Delegacja_krajowa_polecenie");

			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
		
		//		System.out.println("Czas trwania metody raportPolecenie : " + (new Date().getTime() - tStart));
	}
	

	public void updateView() {
		//		long tStart = new Date().getTime();	
		
		com.comarch.egeria.Utils.update("tabViewId");
		
		//		System.out.println("Czas trwania metody updateView : " + (new Date().getTime() - tStart));
	}


	private void showEntityDetails() {
		
		//		long tStart = new Date().getTime();	
		
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("-----------------------------------------------------------------------");
		System.out.println(wniosek.getWndId() + " " + wniosek.getWndUwagi() + " " + wniosek.getWndPole08());
		System.out.println("-----------------------------------------------------------------------");
		for (DeltCeleDelegacji c : this.wniosek.getDeltCeleDelegacjis()) {
			System.out.println(c.getCdelId() + " " + c.getCdelKrId() + " " + c.getCdelMiejscowosc());
		}
		System.out.println("-----------------------------------------------------------------------");

		this.loadEgSlownik("DEL_SRODEK_LOKOMOCJI");
		for (DeltSrodkiLokomocji s : wniosek.getDeltSrodkiLokomocjis()) {

			System.out.println(s.getSlokId() + " " + s.getSlokSrodekLokomocji());
		}

		System.out.println("-----------------------------------------------------------------------");
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("-----------------------------------------------------------------------");
		System.out.println("-----------------------------------------------------------------------");
		
		//		System.out.println("Czas trwania metody showEntityDetails : " + (new Date().getTime() - tStart));
	}
	
	
	private Boolean validateBeforeSaveDelegacja() {
		//		long tStart = new Date().getTime();	
		// walidacja - do dalszego refaktoringu
		ArrayList<String> inval = new ArrayList<String>();

		// if (ocena.getOceMaxDataOceny() == null)
		// inval.add("Termin sporządzenia oceny jest polem wymaganym.");

		String wymagalne = "";
		
		sprawdzZapraszajacych(inval);
		
		Date dzisiaj = new Date();
		
		if(wniosek.getWndDataPowrotu() != null)
		if(wniosek.getWndId() == 0 && wniosek.getWndDataPowrotu().before(dzisiaj)) {
			User.warn("Termin delegacji jest datą przeszłą");
		}
		if(sprawdzZf(kalkulacjaWstepna) || sprawdzZf(kalkulacjaRozliczenia))
			return false;
		
		for (DeltPozycjeKalkulacji p : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
			if(nz(p.getPklKosztMFProcent()) + nz(p.getPklKosztZaprProcent()) + nz(p.getPklRefundacjaProcent()) != 100.0) {
				inval.add("RODZAJE KOSZTÓW : Procentowa suma dla pojedynczego rodzaju kosztu powinna wynosić 100%");
				break;
			}
		}
		
		if (wniosek.getWndDataWniosku() == null) {
			inval.add("Data złożenia wniosku nie może być pusta");
		}
		
		if (wniosek.getWndDataWyjazdu() != null && wniosek.getWndDataPowrotu()!=null &&
				wniosek.getWndDataWyjazdu().after(wniosek.getWndDataPowrotu())){
			inval.add("Data wyjazdu jest nie może być większa niż data powrotu.");
		}
		
		if (wniosek.getWndPrywDataWyjazdu() != null && wniosek.getWndPrywDataPowrotu()!=null &&
				wniosek.getWndPrywDataWyjazdu().after(wniosek.getWndPrywDataPowrotu())){
			inval.add("Data prywatnego wyjazdu jest nie może być większa niż data powrotu.");
		}

		if (wniosek.getWndOpis() == null || "".equals(wniosek.getWndOpis()))
			wymagalne = wymagalne + "Opis celu delegacji; ";

		if ("T".equals(wniosek.getWndCzyGrupowa())
				&& (wniosek.getWndGrUzasadnienie() == null || wniosek.getWndGrUzasadnienie().trim().equals("")))
			wymagalne = wymagalne + "Uzasadnienie składu delegacji dla delegacji grupowych; ";

		if (wniosek.getWndDataPowrotu() == null || wniosek.getWndDataWyjazdu() == null)
			wymagalne = wymagalne + "Termin delegacji (daty od /do); ";

		if (wniosek.getDeltCeleDelegacjis().isEmpty())
			wymagalne = wymagalne + "Cel delgacji (miejscowość); ";

		for (DeltCeleDelegacji c : wniosek.getDeltCeleDelegacjis()) {
			if (c.getCdelMiejscowosc() == null || "".equals(c.getCdelMiejscowosc().trim()))
				wymagalne = wymagalne + "Miejscowość docelowa; ";
		}

		if ("T".equals(wniosek.getWndFWyjazdZDomu())
				&& (wniosek.getWndPrywDataPowrotu() == null || wniosek.getWndPrywDataWyjazdu() == null))
			wymagalne = wymagalne + "Termin podróży prywatnej (daty od /do); ";

		List<DeltSrodkiLokomocji> listaSrodkiLokomocji = wniosek.getDeltSrodkiLokomocjis().stream().filter(
				sl -> (sl.getSlokSrodekLokomocji().isEmpty())).collect(Collectors.toList());
		
		if (wniosek.getDeltSrodkiLokomocjis() == null || wniosek.getDeltSrodkiLokomocjis().isEmpty()
				|| listaSrodkiLokomocji.size() > 0)
			wymagalne = wymagalne + "Środek lokomocji; ";
		
		List<DeltSrodkiLokomocji> srodkiBezZgody = wniosek.getDeltSrodkiLokomocjis().stream().filter(
				sl -> ("02".equals(sl.getSlokSrodekLokomocji()) && "N".equals(sl.getSlokFCzyZgodaPojazdSlu()))).collect(Collectors.toList());
		if(srodkiBezZgody.size() > 0 ) {
			User.warn("Nie zaznaczono potwierdzenia uzyskania akceptacji zapotrzebowania na samochód służbowy");
		}
		
		List<DeltSrodkiLokomocji> specjalneSrodki = wniosek.getDeltSrodkiLokomocjis().stream()
				.filter(slok -> (eq(true,slok.czyWymagaDodatkowejZgody()))).collect(Collectors.toList());
		
		List<DeltSrodkiLokomocji> samochodPrywatnyBezZgody = specjalneSrodki.stream().filter(sl -> eq("03", sl.getSlokSrodekLokomocji()) || eq("07", sl.getSlokSrodekLokomocji()))
		.filter(sl -> eq("N",sl.getSlokFCzyZgodaPojazdPryw())).collect(Collectors.toList());
		
		if(samochodPrywatnyBezZgody.size() > 0 ) {
			User.warn("Nie zaznaczono potwierdzenia uzyskania akceptacji zapotrzebowania na samochód prywatny");
		}
		
		for (DeltPozycjeKalkulacji p : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
			if (p.getPklTpozId() == null)
				wymagalne = wymagalne + "Rodzaj kosztu; ";
		}
		
		if(specjalneSrodki.size() > 0) {
			SqlDataSelectionsHandler lwZalaczniki = getLW("SQLZALACZNIKI",wniosek.getWndId(), "ZAL_WND_ID");
			if(lwZalaczniki.getData().size() < 1) {
				User.warn("Wybrano środek transportu, który wymaga dołączenia dodatkowej zgody jako załącznik");
			} 
		}

		if (!wymagalne.isEmpty()) {
			inval.add("Następujące pola są wymagalne: " + wymagalne);
		}
		
//		if(!czyMogeZaliczke && "T".eq0uals(wniosek.getWndFZaliczka())){
//			inval.add("Nie możesz brać zaliczek, masz nierozliczone delegacje!");
//		}
		
		if(eq("T",this.wniosek.getWndFZaliczka())) {
			DelZaliczki zaliczka = this.kalkulacjaWniosek.getDelZaliczki().get(0);
			if( "B".equals(zaliczka.getDkzFormaWyplaty()) && nz(zaliczka.getDkzNrKonta()).isEmpty()) {
				inval.add("Zaliczki : podaj numer konta");
			}
		}
		
		if(!this.walidujPlanTrasy(kalkulacjaWstepna, true)) {
			inval.add("Błędne dane w planie trasy cz II");
		}
		
		if(!this.walidujPlanTrasy(kalkulacjaRozliczenia, false)) {
			inval.add("Błędne dane w planie trasy cz III");
		}
		
		if(!kalkulacjaWniosek.walidujRyczaltKM()) {
			inval.add("Część I - rodzaje kosztów. W przypadku przejazdu samochodem prywatnym nie przysługuje ryczałt komunikacyjny, popraw dane");
		}
		
		if(!kalkulacjaWstepna.walidujRyczaltKM()) {
			inval.add("Część II - ryczałt komunikacyjny. W przypadku przejazdu samochodem prywatnym nie przysługuje ryczałt komunikacyjny, popraw dane");
		}
		
		if(!kalkulacjaRozliczenia.walidujRyczaltKM()) {
			inval.add("Część III - ryczałt komunikacyjny. W przypadku przejazdu samochodem prywatnym nie przysługuje ryczałt komunikacyjny, popraw dane");
		}
		
		if("T".equals(this.wniosek.getWndFMiejscowoscPobyt())){
			if(this.kalkulacjaWniosek.getDeltPozycjeKalkulacjis().stream()
					.filter(p -> p.getPklTpozId().equals(getTpozIdByKtgPozycji("P"))
							 ||  p.getPklTpozId().equals(getTpozIdByKtgPozycji("H"))
							 ||  p.getPklTpozId().equals(getTpozIdByKtgPozycji("RH"))).
					findFirst().isPresent()) {
				inval.add("W przypadku zaznaczenia checkbox'a 'Pobyt stały lub czasowy delegowanego miejscowością delegowania' należy usunąć z rodzajów kosztów "
						+ "pozycje dieta pobytowa i należności za nocleg");
			} 
		}

		
		if (!inval.isEmpty()) {
			for (String msg : inval) {
				User.alert(msg);
			}
		}
		//		System.out.println("Czas trwania metody validateBeforeSaveDelegacja : " + (new Date().getTime() - tStart));
		return inval.isEmpty();
	}

	private void sprawdzZapraszajacych(ArrayList<String> inval) {
		
		//		long tStart = new Date().getTime();	
		
		if(wniosek.getDeltZapraszajacys().size() > 1)
		for(DeltZapraszajacy zapr : wniosek.getDeltZapraszajacys()) {
			try {
				if(zapr.getZaprOpis() == null) {
					inval.add("Lista zapraszających zawiera pustą pozycję");
				}
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		//		System.out.println("Czas trwania metody sprawdzZapraszajacych : " + (new Date().getTime() - tStart));
	}

	public DeltWnioskiDelegacji getWniosek() {
		return wniosek;
	}

	public void setWniosek(WniosekDelegacjiKraj wniosek) {
		this.wniosek = wniosek;
		if (wniosek!=null) this.wniosek.setSqlBean(this);
	}

	public void deleteCity(int index) {
		
		//		long tStart = new Date().getTime();	
		
		DeltCeleDelegacji c = this.wniosek.getDeltCeleDelegacjis().get(index);
		this.wniosek.removeDeltCeleDelegacji(c);
		if (this.wniosek.getDeltCeleDelegacjis().isEmpty()) {
            DeltCeleDelegacji cel = new DeltCeleDelegacji();
            cel.setCdelKrId(1L);
            this.wniosek.addDeltCeleDelegacji(cel);
		}
		
		//		System.out.println("Czas trwania metody deleteCity : " + (new Date().getTime() - tStart));
	}

	public DeltCeleDelegacji addNewCDEL() {
		
		//		long tStart = new Date().getTime();	
		
		DeltCeleDelegacji c = new DeltCeleDelegacji();
		c.setCdelKrId(1L);// ustawiam PL
		c.setCdelMiejscowoscOd("Warszawa");
		c.setCdelMiejscowoscZakonczenia("Warszawa");
		if (wniosek.getDeltCeleDelegacjis() == null)
			wniosek.setDeltCeleDelegacjis(new ArrayList<>());
		this.wniosek.addDeltCeleDelegacji(c);
		
		//		System.out.println("Czas trwania metody addNewCDEL : " + (new Date().getTime() - tStart));
		return c;
	}

	public DeltKalkulacje addNewKalkulacje(String rodzaj) {
		
		//		long tStart = new Date().getTime();	
		
		if (wniosek.getDeltKalkulacjes() == null)
			wniosek.setDeltKalkulacjes(new ArrayList<DeltKalkulacje>());
		DeltKalkulacje k = new DeltKalkulacje();
		k.setKalData(new Date());
		k.setKalRodzaj(rodzaj);

		wniosek.addDeltKalkulacje(k);
		//		System.out.println("Czas trwania metody addNewKalkulacje : " + (new Date().getTime() - tStart));
		return k;
	}

	public void addNewZapraszajacy() {
		
		//		long tStart = new Date().getTime();	
		wniosek.addDeltZapraszajacy(new DeltZapraszajacy());
		
		//		System.out.println("Czas trwania metody addNewZapraszajacy : " + (new Date().getTime() - tStart));
	}

	public void deleteZapraszajacy(int index) {
		
		//		long tStart = new Date().getTime();	
		
		DeltZapraszajacy s = this.wniosek.getDeltZapraszajacys().get(index);
		wniosek.removeDeltZapraszajacy(s);
		
		
		//		System.out.println("Czas trwania metody deleteZapraszajacy : " + (new Date().getTime() - tStart));
	}

	public void addNewProjekt() {
		//		long tStart = new Date().getTime();	
		wniosek.addDeltProjekty(new DeltProjekty());
		//		System.out.println("Czas trwania metody addNewProjekt : " + (new Date().getTime() - tStart));
	}

	public void deleteProjekt(int index) {
		//		long tStart = new Date().getTime();
		DeltProjekty s = this.wniosek.getDeltProjektys().get(index);
		wniosek.removeDeltProjekty(s);
		
		//		System.out.println("Czas trwania metody deleteProjekt : " + (new Date().getTime() - tStart));
	}

	public void deleteSrodekLokomocji(int index) {
		//		long tStart = new Date().getTime();
		DeltSrodkiLokomocji sl = wniosek.getDeltSrodkiLokomocjis().get(index);
		wniosek.removeDeltSrodkiLokomocji(sl);

		if (wniosek.getDeltSrodkiLokomocjis().isEmpty()) {
			wniosek.addDeltSrodkiLokomocji(new DeltSrodkiLokomocji());
		}
		
		//		System.out.println("Czas trwania metody deleteSrodekLokomocji : " + (new Date().getTime() - tStart));
	}

	public DeltSrodkiLokomocji addNewSLOK() {
		//		long tStart = new Date().getTime();
		DeltSrodkiLokomocji slok = new DeltSrodkiLokomocji();
		if (wniosek.getDeltSrodkiLokomocjis() == null)
			wniosek.setDeltSrodkiLokomocjis(new ArrayList<>());
		wniosek.addDeltSrodkiLokomocji(slok);
		
		//		System.out.println("Czas trwania metody addNewSLOK : " + (new Date().getTime() - tStart));
		return slok;
	}

	public void deleteWndWndId() {
		//		long tStart = new Date().getTime();
		wniosek.setWndWndId(null);

		setWndWndId(null);
		
		//		System.out.println("Czas trwania metody deleteWndWndId : " + (new Date().getTime() - tStart));
	}


	public List<DeltCeleDelegacji> getCities() {
		return this.wniosek.getDeltCeleDelegacjis();
	}

	/**
	 * @return the wniosekDelegacjaPowiazana
	 */
	public DeltWnioskiDelegacji getWniosekDelegacjaPowiazana() {
		return wniosekDelegacjaPowiazana;
	}

	/**
	 * @param wniosekDelegacjaPowiazana
	 *            the wniosekDelegacjaPowiazana to set
	 */
	public void setWniosekDelegacjaPowiazana(WniosekDelegacjiKraj wniosekDelegacjaPowiazana) {
		this.wniosekDelegacjaPowiazana = wniosekDelegacjaPowiazana;
	}

//	public void wczytajWniosekDelegacjaPowiazana() {
//		wniosekDelegacjaPowiazana = wczytajWniosekDelegacjiDB(wniosek.getWndWndId());
//	}

	public DeltTrasy getSelectedTrasa() {
		return selectedTrasa;
	}

	public void setSelectedTrasa(DeltTrasy selectedTrasa) {
		this.selectedTrasa = selectedTrasa;
	}

	/**
	 * @return the kalkulacjaWstepna
	 */
	public DeltKalkulacje getKalkulacjaWniosek() {
		return kalkulacjaWniosek;
	}

	public void setKalkulacjaWniosek(DeltKalkulacje kalkulacjaWniosek) {
		this.kalkulacjaWniosek = kalkulacjaWniosek;
	}


	public Long getWndWndId() {
		return wndWndId;
	}


	public void setWndWndId(Long wndWndId) {

		if (wniosek.getDeltCeleDelegacjis() == null || wniosek.getDeltCeleDelegacjis().size() == 0
				|| (wniosek.getDeltCeleDelegacjis().get(0).getCdelKrId() == null)) {
			wniosek.setWndWndId(wndWndId);
			this.wndWndId = wndWndId;
			kopiujDaneZDelegacjiGrupowej();
			return;
		}

		if (wndWndId != null) {
//			RequestContext context = RequestContext.getCurrentInstance();
//			context.execute("PF('confKopiujCele').show();");
			com.comarch.egeria.Utils.update("PF('confKopiujCele').show();");
		}
		
		this.wndWndId = wndWndId;
	}

//	public void zaznaczPozycje() {
//
//		//		long tStart = new Date().getTime();
//		SqlDataSelectionsHandler dsh = this.getLW("PPL_DEL_TYPY_POZYCJI_K");
//
//		for (DataRow dr : dsh.getData()) {
//
//			for (DeltPozycjeKalkulacji poz : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
//
//				if (dr.getIdAsLong().equals(poz.getPklTpozId())) {
//					dsh.setRowSelection(true, dr);
//					break;
//				}
//			}
//		}
//		//		System.out.println("Czas trwania metody zaznaczPozycje : " + (new Date().getTime() - tStart));
//	}
	
	public void obliczMaxProcentPkl(DeltPozycjeKalkulacji pkl, double nowaWartość, String rodzaj) {
		Double mfProcent = nz(pkl.getPklKosztMFProcent());
		Double zaprProcent = nz(pkl.getPklKosztZaprProcent());
		Double refundacjaProcent = nz(pkl.getPklRefundacjaProcent());
		
		double suma = mfProcent + zaprProcent + refundacjaProcent;
		
		if(suma > 100.0) {
			if("z".equals(rodzaj)) {
				if((100.0 - nowaWartość - refundacjaProcent) >= 0.0)
					pkl.setPklKosztMFProcent(100.0 - nowaWartość - refundacjaProcent);
				else {
					User.alert("Popraw procentowy podział kosztu");
					return;
				}
			} else if("r".equals(rodzaj)) {
				if((100.0 - nowaWartość - zaprProcent) >= 0.0)
					pkl.setPklKosztMFProcent(100.0 - nowaWartość - zaprProcent);
				else {
					User.alert("Popraw procentowy podział kosztu");
					return;
				}
			} else {
				pkl.setPklKosztMFProcent(100.0 - zaprProcent - refundacjaProcent);
				User.alert("Popraw procentowy podział kosztu dla zapraszającego bądź refundacji");
				return;
			}
		}
	}

	// dodaje pozycje kalkulacji o ustalonych typach TODO sprawdzic czy nie trzeba czegos usunac
//	public void dodajPozycje() {
//
//		//		long tStart = new Date().getTime();
//
//		for (DataRow dr : this.getLW("PPL_DEL_TYPY_POZYCJI_K").getSelectedRows()) {
//
////			Boolean czyPowtorzenie = false;
////
////			for (DeltPozycjeKalkulacji pk : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
////
////				if (pk.getPklTpozId().equals(Long.parseLong(dr.get("TPOZ_ID").toString()))) {
////					czyPowtorzenie = true;
////					break;
////				}
////			}
////
////			if (czyPowtorzenie) {
////				break;
////			}
//
//			DeltPozycjeKalkulacji pkl = new DeltPozycjeKalkulacji();
//			pkl.setDeltKalkulacje(kalkulacjaWniosek);
//
//			pkl.setPklTpozId(Long.parseLong(dr.get("TPOZ_ID").toString()));
//			pkl.setPklStawka(0D);
//			pkl.setPklId(0L);
//			pkl.setPklKwota(0D);
//			pkl.setPklWalId(1L);
//			pkl.setPklIlosc(0D);
////			pkl.setPklFOsobaTowarzyszaca("N");
//			pkl.setPklKwotaPrzelewu(0L);
//			pkl.setPklKwotaGotowki("0");
//
//			pkl.setPklKosztMFProcent(100.0);
//			pkl.setPklKosztZaprProcent(0.0);
//			pkl.setPklRefundacjaProcent(0.0);
//
//			if(eq("T",pkl.getKategoria())) {
//				DeltSrodkiLokomocji slok = pkl.getDeltKalkulacje().getWniosek().getDeltSrodkiLokomocjis().get(0);
//				if(slok != null && (eq("03",slok.getSlokSrodekLokomocji()) || eq("07",slok.getSlokSrodekLokomocji()))) {
//					pkl.setPklFormaPlatnosci("04");
//				} else
//					pkl.setPklFormaPlatnosci("03");
//			}
//
//			else if(eq("H",pkl.getKategoria())) {
//					pkl.setPklFormaPlatnosci("03");
//			}
//			else pkl.setPklFormaPlatnosci("04");
//
//			if("T".equals(wniosek.getWndFDomyslneZf())) {
//				DeltZrodlaFinansowania zf = new DeltZrodlaFinansowania();
//				zf.setPkzfProcent(100.0);
//				zf.setPkzfKwota(0.0);
//				if(wniosek.getWndSkId() != null)
//					zf.setPkzfSkId(new Long(wniosek.getWndSkId()));
//
//				DelAngaze angaz = new DelAngaze();
//
//				if(wniosek.getWndWnrId() != null) {
//					angaz.setAngWnrId(new Long(wniosek.getWndWnrId()));
//					angaz.setAngProcent(100.0);
//					zf.addDelAngaze(angaz);
//				}
//
//				pkl.addDeltZrodlaFinansowania(zf);
//
//			}
//				kalkulacjaWniosek.addDeltPozycjeKalkulacji(pkl);
//
//		}
//		kalkulacjaWniosek.getDeltPozycjeKalkulacjis().sort(Comparator.comparing(DeltPozycjeKalkulacji::getLp));
//		this.getLW("PPL_DEL_TYPY_POZYCJI_K").clearSelection();
//		//		System.out.println("Czas trwania metody dodajPozycje : " + (new Date().getTime() - tStart));
//
//		this.getWniosek().setCzyDomyslnieDietaHotelowa(null);
//	}
	
	public boolean blokujBtnJesliDpnz(List<DeltPozycjeKalkulacji> listaPkl) {
		
		boolean ret = false;
		List<DeltPozycjeKalkulacji> list = listaPkl.stream()
			.filter(pkl -> pkl.getDeltPklWyplNzs() != null)
			.filter(p -> p.getDeltPklWyplNzs().size() > 0)
			.collect(Collectors.toList());
		
		if (list.size() > 0) {
			ret = true;
		}
		
		return ret;
	}
	
	public double ustawKwote (Double kwotaWej, DeltZrodlaFinansowania zf){
		
		//		long tStart = new Date().getTime();
		if(kwotaWej == null)
			kwotaWej = 0.0;
		Double ret = procentNaKwote(zf.getPkzfProcent(), kwotaWej);
		for ( DelAngaze a : zf.getDelAngazes()){
			a.setAngKwota(procentNaKwote(a.getAngProcent(), ret));
		}
		//		System.out.println("Czas trwania metody ustawKwote : " + (new Date().getTime() - tStart));
		return ret;
	}
	
	public double ustawProcent(Double kwotaWej, DeltZrodlaFinansowania zf) {
		//		long tStart = new Date().getTime();
		for ( DelAngaze a : zf.getDelAngazes()){
			a.setAngKwota(procentNaKwote(a.getAngProcent(), zf.getPkzfKwota()));
		}
		//		System.out.println("Czas trwania metody ustawProcent : " + (new Date().getTime() - tStart));
		return kwotaNaProcent(zf.getPkzfKwota(), nz(kwotaWej));
		
	}
	
	public Double procentNaKwote (Double procent, Double kwotaWej) {
		if(kwotaWej != 0.0)
		return round(((nz(procent)*nz(kwotaWej))/100),2);
		else return 0.0;
	}
	
	public Double kwotaNaProcent (Double kwotaAkt, Double kwotaWej) {
		if(kwotaWej != 0.0 && kwotaAkt != 0)
		return round(((100*nz(kwotaAkt))/nz(kwotaWej)),2);
		else return 0.0;
	}
	public Double sumaKwotZf( DeltPozycjeKalkulacji pkl) {
		//		long tStart = new Date().getTime();
		Double suma = 0.0;
		for( DeltZrodlaFinansowania zf : pkl.getDeltZrodlaFinansowanias()) {
			if(zf.getPkzfKwota()!=null)
			suma += zf.getPkzfKwota();
		}
		//		System.out.println("Czas trwania metody sumaKwotZf : " + (new Date().getTime() - tStart));
		return round(suma, 2);
	}
	
	public Double sumaKwotAngazy( DeltZrodlaFinansowania zf) {
		//		long tStart = new Date().getTime();
		Double suma = 0.0;
		for(  DelAngaze a : zf.getDelAngazes()) {
			if(a.getAngKwota()!=null)
			suma += a.getAngKwota();
		}
		//		System.out.println("Czas trwania metody sumaKwotAngazy : " + (new Date().getTime() - tStart));
		return round(suma,2);
	}

	public SimpleDateFormat getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(SimpleDateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}

	public DeltKalkulacje getKalkulacjaWstepna() {
		return kalkulacjaWstepna;
	}

	public void setKalkulacjaWstepna(DeltKalkulacje kalkulacjaWstepna) {
		this.kalkulacjaWstepna = kalkulacjaWstepna;
	}

	public Date getDataWyplaty() {
		return dataWyplaty;
	}

	public void setDataWyplaty(Date dataWyplaty) {
		this.dataWyplaty = dataWyplaty;
	}

	public DeltKalkulacje getKalkulacjaRozliczenia() {
		return kalkulacjaRozliczenia;
	}

	public void setKalkulacjaRozliczenia(DeltKalkulacje kalkulacjaRozliczenia) {
		this.kalkulacjaRozliczenia = kalkulacjaRozliczenia;
	}

	public DeltPozycjeKalkulacji getCurrentPkl() {
		return currentPkl;
	}

	public void setCurrentPkl(DeltPozycjeKalkulacji currentPkl) {
		this.currentPkl = currentPkl;
	}

	public int getTabDelegacji() {
		return tabDelegacji;
	}

	public void setTabDelegacji(int tabDelegacji) {
		this.tabDelegacji = tabDelegacji;
	}


	public double getPodsumowanieRozl() {
		return podsumowanieRozl;
	}

	public void setPodsumowanieRozl(double podsumowanieRozl) {
		this.podsumowanieRozl = podsumowanieRozl;
	}

	public List<DeltZrodlaFinansowania> getZfDoRozliczenia() {
		return zfDoRozliczenia; 
	}

	public void setZfDoRozliczenia(List<DeltZrodlaFinansowania> zfDoRozliczenia) {
		this.zfDoRozliczenia = zfDoRozliczenia;
	}

	public List<DeltZrodlaFinansowania> getListaZfKalkWst() {
		return listaZfKalkWst;
	}

	public void setListaZfKalkWst(List<DeltZrodlaFinansowania> listaZfKalkWst) {
		this.listaZfKalkWst = listaZfKalkWst;
	}

	public double getPodsumowanieWst() {
		return podsumowanieWst;
	}

	public void setPodsumowanieWst(double podsumowanieWst) {
		this.podsumowanieWst = podsumowanieWst;
	}

	public Boolean getDelWObiegu() {
		return delWObiegu;
	}

	public void setDelWObiegu(Boolean delWObiegu) {
		this.delWObiegu = delWObiegu;
	}

	public Boolean getCzyMogeZaliczke() {
		return czyMogeZaliczke;
	}

	public void setCzyMogeZaliczke(Boolean czyMogeZaliczke) {
		this.czyMogeZaliczke = czyMogeZaliczke;
	}

//	//leniuch jesli ma na cz. I  jest pkl/RH to fasle, WPP true
//	public Boolean getCzyDietaHotelowa() {
//		this.wniosek.onRXGet("czyDietaHotelowa");
//		if (czyDietaHotelowa == null && this.kalkulacjaWniosek!=null && this.kalkulacjaWniosek.getDeltPozycjeKalkulacjis()!=null) {
//			long cntRH = this.kalkulacjaWniosek.getDeltPozycjeKalkulacjis().stream().filter(p -> "RH".equals(p.getKategoria())).count();
//			this.setCzyDietaHotelowa(cntRH <= 0);
//		}
//		return czyDietaHotelowa;
//	}
//
//
//	public void setCzyDietaHotelowa(Boolean czyDietaHotelowa) {
//		this.wniosek.onRXSet("czyDietaHotelowa", this.czyDietaHotelowa, czyDietaHotelowa);
//		this.czyDietaHotelowa = czyDietaHotelowa;
//	}

}
