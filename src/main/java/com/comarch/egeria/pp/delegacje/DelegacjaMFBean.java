package com.comarch.egeria.pp.delegacje;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.delegacje.model.*;
import com.comarch.egeria.pp.zalaczniki.ZalacznikiBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import net.sf.jasperreports.engine.JRException;
import org.apache.commons.lang.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.getWalId;
import static com.comarch.egeria.Utils.nz;

//import org.bouncycastle.ocsp.Req;
//import org.primefaces.context.RequestContext;
//import com.comarch.egeria.pp.delegacje.model.DeltWnioskiDelegacji;

@ManagedBean
@Named
@Scope("view")
public class DelegacjaMFBean extends SqlBean {
//	private int czI_ActiveIndex = 0;
//	private int czII_ActiveIndex = 0;
//	private int czIII_ActiveIndex = 0;
	
	private static final Logger log = LogManager.getLogger();

	@Inject
	ObiegBean obieg;

//	@Inject
//	protected ReportGenerator ReportGenerator;


//	private Date dataKursu;
	
//	private Date dataWyplaty;
	
	private Object waluta;

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	private WniosekDelegacjiZagr wniosek;
	
	private DeltKalkulacje kalkulacjaWniosek;
	private DeltKalkulacje kalkulacjaWstepna;
	private DeltKalkulacje kalkulacjaRozliczenia;

	private String rodzajDelegacji;
	private Long wndWndId;

	private WniosekDelegacjiZagr wniosekDelegacjaPowiazana;

	private DeltTrasy selectedTrasa;
	
//	private double podsumowaniePln;
	
//	private double podsumowaniePlnRozl;
	
//	private HashMap<Long, Double> sumyWgWalut = new HashMap<>();
	
//	private HashMap<Long, List<DeltZrodlaFinansowania>> zfWgWalut = new HashMap<>();
	
//	private HashMap<Long, Double> sumyWgWalutKalkRoz = new HashMap<>();
	
//	private Map<Long, Double> sumyWalutDoRozliczenia = new HashMap<>();
	
	private final HashMap<Long, List<DeltZrodlaFinansowania>> zfWgWalutKalkRoz = new HashMap<>();
	
	private final HashMap<Long, Double> listaKrajCzasPrywaty = new HashMap<>();
	
//	private List<DeltZrodlaFinansowania> zfDoRozliczenia = new ArrayList<>();
	
	private DeltPozycjeKalkulacji currentPkl = new DeltPozycjeKalkulacji();
	
	//private SqlDataSelectionsHandler walutyZaliczki =  this.getLW("PPL_KRAJE_WALUTY", ",1,");
	
	private int tabDelegacji = 0;
	
	private Boolean delWObiegu = false;
	
	private Boolean czyMogeZaliczke = false;
	
	private Boolean blokujRDKM = false;
	
//	private Boolean czyDietaHotelowa;
	

	
	
	public SqlDataSelectionsHandler getLwRozliczeniePklNzWgFifo() {
		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_ROZLICZ_FIFO")
				.setPreQuery(""
						+ "begin "
						+ " ppadm.ppp_global.rozlicz_fifo_wnd_id := ? ; "
						+ "end; ", wniosek.getWndId());
		
		return lw;
	}
	

//	public DelZaliczki addNewZaliczka() {
//		DelZaliczki zal = new DelZaliczki();
//		zal.setDkzFormaWyplaty("B");
////		zal.setDkzWalId(1L);
//		if(wniosek.getWndDataWyjazdu() != null) {
//			zal.setDkzDataWyp(checkDate(wniosek.getWndDataWyjazdu()));
//		}
//		if(kalkulacjaWniosek.getDelZaliczki() == null) {
//			kalkulacjaWniosek.setDelZaliczki(new ArrayList<>());
//		}
//		kalkulacjaWniosek.addDeltZaliczki(zal);
//		return zal;
//	}
	
	public void removeZaliczka(DelZaliczki zal){
		kalkulacjaWniosek.removeDelZaliczki(zal);
	}
	
//	public void porownajDateTrasyZWnioskiem(DeltTrasy trs) {
//		
//		Date wndDataWyjazdu = this.wniosek.getWndDataWyjazdu();
//		Date wndDataPowrotu = zwrocMaxDateZWniosku(this.wniosek.getWndDataPowrotu());
//		
//		Date trsDataOd = trs.getTrsDataOd();
//		if(trsDataOd != null)
//		if(trsDataOd.compareTo(wndDataWyjazdu)  >= 0 && trsDataOd.compareTo(wndDataPowrotu) <=0)
//			return;
//		else {
//			User.alert("Niepoprawna data popraw datę na planie trasy");
//			trs.setTrsDataOd(wndDataWyjazdu);
//		}
//		
//		
//	}
//	
//	public boolean porownajDateTrasyZWnioskiem(Date dzien) {
//		if(dzien != null) {
//			if(dzien.compareTo(wniosek.getWndDataWyjazdu())  >= 0 && dzien.compareTo(zwrocMaxDateZWniosku(wniosek.getWndDataPowrotu())) <=0) {
//				return true;
//			} else {
//				User.alert("Data : "+dzien +" ,jest spoza zakresu trwania wnioskowanej delegacji, popraw dane");
//				return false;
//			}
//		}
//		else return false;
//	}
	
	
	public Date zwrocMaxDateZWniosku(Date dzien) {
		
		if(dzien != null) {
		Calendar calendar = Calendar.getInstance();
		
		calendar.setTime(dzien);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		
		return calendar.getTime();
		} else return today();
	}
	
	
	public boolean walidujPlanTrasy(DeltKalkulacje kalk, boolean czyKalkWstepna) {
		if (kalk==null) return true;
		return kalk.validateDeltTrasies();

//
//		Date wndDataWyjazdu = this.wniosek.getWndDataWyjazdu();
//		Date wndDataPowrotu = zwrocMaxDateZWniosku(this.wniosek.getWndDataPowrotu());
//
//		Date doSprawdzenia = null;
//		boolean ret = false;
//
//		if(kalk.getDeltTrasies().size() > 0) {
//			if(kalk.getDeltTrasies().size() < 2){
//				User.alert("Plan trasy musi zawierać co najmniej dwie pozycje");
//				return false;
//			}
//
//			if(czyKalkWstepna)
//				if(kalk.getDeltTrasies().get(0).getTrsDataOd() != null && kalk.getDeltTrasies().get(kalk.getDeltTrasies().size()-1).getTrsDataDo() != null) {
//			if(!( ((kalk.getDeltTrasies().get(0).getTrsDataOd().compareTo(wndDataWyjazdu) >=0 && kalk.getDeltTrasies().get(0).getTrsDataOd().before(wndDataPowrotu))
//					&&
//					(kalk.getDeltTrasies().get(kalk.getDeltTrasies().size()-1).getTrsDataDo().compareTo(wndDataWyjazdu) >=0 && kalk.getDeltTrasies().get(kalk.getDeltTrasies().size()-1).getTrsDataDo().before(wndDataPowrotu)))
//					)) {
//							User.alert("Daty na planie trasy muszą się zawierać w wnioskowanym terminie trwania delegacji");
//							return false;
//						}
//			} else {
//				User.alert("Plan trasy - wprowadzone dane nie mogą być puste");
//				return false;
//			}
//
//			if(!(kalk.getDeltTrasies().get(0).getTrsDataOd().before(kalk.getDeltTrasies().get(kalk.getDeltTrasies().size()-1).getTrsDataDo()))) {
//				User.alert("Data powrotu z delegacji nie może nastąpić przed datą wyjazdu, popraw dane");
//				return false;
//			}
//
//			for( DeltTrasy trs : kalk.getDeltTrasies()) {
//
//				if(trs.getTrsMiejscowoscOd() == null || trs.getTrsMiejscowoscDo() == null || trs.getTrsMiejscowoscOd().isEmpty() || trs.getTrsMiejscowoscDo().isEmpty()) {
//
//					User.alert("Należy uzupełnić plan trasy o brakujące dane w kolumnie miejscowość");
//					return false;
//				}
//
//				if(trs.getTrsDataOd() != null && trs.getTrsDataDo() != null) {
//
//
//						if(doSprawdzenia != null) {
//							if(trs.getTrsDataOd().after(doSprawdzenia)) {
//
//							}
//							else {
//								User.alert("Data wyjazdu z poprzedzającego wiersza w planie trasy nie może być późniejsza niż daty w kolejnych wierszach");
//								return false;
//							}
//						}
//
//						doSprawdzenia = trs.getTrsDataDo();
//
//						if(trs.getTrsDataDo().after(trs.getTrsDataOd())){
//							continue;
//						} else {
//							User.alert("Niepoprawna data na planie trasy : data wyjazdu nie może nastąpić przed datą przyjazdu na danym odcinku planu trasy");
//							return false;
//						}
//
//				} else {
////					User.alert("Nie wszystkie daty na planie trasy zostały uzupełnione, uzupełnij dane przed zapisem");
////					return false;
//				}
//			}
//			ret = true;
//		} else return true;
//		return ret;
	}
	
public Date checkDate(Date dzien) {
	
		if(dzien == null)
			return today();

		Calendar startCal = Calendar.getInstance();
		startCal.setTime(dzien);
		startCal.add(Calendar.DATE, -5);
		int dayOfWeek = startCal.get(Calendar.DAY_OF_WEEK);

		switch (dayOfWeek) {
		case Calendar.SUNDAY:
			startCal.add(Calendar.DATE, -3);
			break;
		case Calendar.MONDAY:
			startCal.add(Calendar.DATE, -4);
			break;
//		default:
//			startCal.add(Calendar.DATE, -2);
//			if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
//				startCal.add(Calendar.DATE, -1);
//			} else if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
//				startCal.add(Calendar.DATE, -2);
//			}
//			break;
		}

		if (startCal.get(Calendar.DAY_OF_MONTH) == 28) {
			switch (dayOfWeek) {
			case Calendar.SUNDAY:
				startCal.add(Calendar.DATE, -2);
				break;
			case Calendar.MONDAY:
				startCal.add(Calendar.DATE, -3);
				break;
			default:
				startCal.add(Calendar.DATE, -1);
				if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
					startCal.add(Calendar.DATE, -1);
				} else if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
					startCal.add(Calendar.DATE, -2);
				}
				break;
			}
		}
		dzien = startCal.getTime();
		
		if(dzien.before(today()))
			dzien = today();
		
		return dzien;

	}
	

	
	public void sprawdzStanObiegu() {
		if(wniosek.getWndPrcId()!=null){
			ArrayList<DataRow> data = this.getLW("PPL_DEL_CZY_MA_NIEROZ", wniosek.getWndPrcId()).getData();
			if (!data.isEmpty())
				setCzyMogeZaliczke("N".equals(data.get(0).get("czy_ma_nieroz_delegacje")));
		}
		
		
		String stanObiegu = this.obieg.stanObiegu();
		if(!stanObiegu.isEmpty()){ 
			this.setDelWObiegu(true);
			if (";POZIOM70;POZIOM80;POZIOM90;POZIOM100;POZIOM110;POZIOM115;POZIOM120;STOP;".contains(";"+stanObiegu+";"))
				setCzyMogeZaliczke(true);
		}
		else this.setDelWObiegu(false);
			
	}
	
	public String wyznaczCzynnosciObieguGenerujDEZ(WniosekDelegacjiZagr wniosek) {
		
		if(this.obieg == null || this.obieg.stanObiegu() == null || this.obieg.stanObiegu().isEmpty()) 
			return "";
		
		
		//Jeśli obieg na etapie przekazania do FK to...
//		if("ROZLICZONO".equals(this.obieg.stanObiegu()) && (this.wniosek.getWndDokId() == null)) {
//				
////			try (Connection conn = JdbcUtils.getConnection())  {
////
////				//generacja noty
////				Long dezId = JdbcUtils.sqlFnLong(conn, "PPADM.PPP_INTEGRACJA_FIN.generuj_dez", this.wniosek.getWndId());
////				if(dezId == null) {
////					User.alert("Wystąpił błąd w trakcie generowania noty");
////					return "";
////				}
////				
////				//zapisanie id noty w wniosku
////				this.wniosek.setWndDokId(dezId);
////				
////				//wygenerowanie raportu wniosku i rozliczenia i podpięcia ich jako zal do wniosku
////				generujRaportyJakoZalacznikiPdfDlaDokID(dezId, true, true, true);
////
////				//skierowanie noty do FK i rozpoczęcie na niej obiegu
////				JdbcUtils.sqlSPCall(conn, "PPADM.PPP_INTEGRACJA_FIN.skieruj_rozl_del_do_fk", dezId, "test DEZ");
////				
////				//zapisanie danych
////				HibernateContext.saveOrUpdate(this.wniosek);
////			
////			}	catch (Exception e) {
////				log.error(e.getMessage(), e);
////				User.alert("Wystąpił błąd w trakcie tworzenia dokumentu noty");
////				User.alert(e.getMessage());
////			}
////			
//		} 
		
		if("POZIOM27".equals(this.obieg.stanObiegu())) {
			
//			Double koszt = null;
//			try (Connection conn = JdbcUtils.getConnection())  {
//		PT 262498		koszt = JdbcUtils.sqlFnDouble(conn, "PPADM.KOSZTY_DELEGACJI", wniosek.getWndId() , "WST");
//			}catch (Exception e) {
//				log.error(e.getMessage(), e);
//			}

			double koszt = this.wniosek.getKalkulacjaWstepna().sumaAngazeKwotaPLN();//albo tak
//			double koszt = this.wniosek.getKalkulacjaWstepna().getKwotaPLNWstepna(); // albo tak (ale nie przez Jdbc, skoro wymagamy zapisania zmian przed wykoanniem czynnosci)


//			if(koszt != null) {

				if(koszt > 6000.0) {
					return  "NVL(COB_NAZWA2,'#6TYS+') like '%#6TYS+%'";
				} else
					return  "NVL(COB_NAZWA2,'#6TYS-') like '%#6TYS-%'";

//			} else return  "NVL(COB_NAZWA2,'#6TYS-') like '%#6TYS-%'";
		}
		if("POZIOM10".equals(this.obieg.stanObiegu())) {
			if("T".equals(wniosek.getWndFProjektowa()))
				return "NVL(COB_NAZWA2,'#zat_wnioskodawca_prj') like '%#zat_wnioskodawca_prj%'";
			
			else if(porownajDepPrcSk()) // TODO Tutaj funkcja która sprawdzi departament delegowanego z departamentem źródła fin czy są takie same
				return  "NVL(COB_NAZWA2,'#zat_wnioskodawca_dep') like '%#zat_wnioskodawca_dep%'";
			else 
				return  "NVL(COB_NAZWA2,'#zat_wnioskodawca_inne') like '%#zat_wnioskodawca_inne%'";
		}
		
		if("POZIOM70".equals(this.obieg.stanObiegu())) {
			if("T".equals(wniosek.getWndFProjektowa()))
				return "NVL(COB_NAZWA2,'#roz_PRJ') like '%#roz_PRJ%'";
			
			else if(porownajDepPrcSk()) // TODO Tutaj funkcja która sprawdzi departament delegowanego z departamentem źródła fin czy są takie same
				return  "NVL(COB_NAZWA2,'#roz_DEP') like '%#roz_DEP%'";
			else 
				return  "NVL(COB_NAZWA2,'#roz_INNE') like '%#roz_INNE%'";
		} else {
				return "";
		}
		
	}

//	private void generujRaportyJakoZalacznikiPdfDlaDokID(Object dokId,  boolean czyRaportPolecenia,  boolean czyRaportRozliczenia,  boolean czyZalacznikDlaNZ_DEL_FK5) throws SQLException, IOException, JRException, DocumentException {
//		this.wniosek.generujRaportyJakoZalacznikiPdfDlaDokID(dokId, czyRaportPolecenia, czyRaportRozliczenia, czyZalacznikDlaNZ_DEL_FK5);
//	}

//	public void dodajRaportyDoZal() {
//		if (wniosek.getWndId() != 0) {
//			try {
//				HashMap<String, Object> params = new HashMap<String, Object>();
//				params.put("wnd_id", wniosek.getWndId());
//				
//				PipedInputStream stream = com.comarch.egeria.web.common.reports.ReportGenerator.pdfAsStream(params, "Delegacja_zagraniczna_wniosek");
//				ZalacznikiBean.zapiszRaportStream(stream, "Delegacja_zagraniczna_polecenie.pdf", "ZAL_WND_ID", this.wniosek.getWndId(), "raport polecenia");
//				//zalaczniki.zapiszRaportStream(stream, "Delegacja_zagraniczna_polecenie.pdf", "");
//				
//				PipedInputStream stream1 = com.comarch.egeria.web.common.reports.ReportGenerator.pdfAsStream(params, "Delegacja_zagraniczna_rozliczenie");
//				ZalacznikiBean.zapiszRaportStream(stream1, "Delegacja_zagraniczna_rozliczenie.pdf", "ZAL_WND_ID", this.wniosek.getWndId(), "raport rozliczenia");
//				//zalaczniki.zapiszRaportStream(stream1, "Delegacja_zagraniczna_rozliczenie.pdf", "");
//				
//			} catch (JRException | SQLException | IOException e) {
//				log.error(e.getMessage(), e);
//				User.alert(e.getMessage());
//			}
//			
//			System.out.println();
//		}
//	}
	
	
	private boolean porownajDepPrcSk() {

		DataRow prc = getLW("PRC").find( wniosek.getWndPrcId() );
		if (prc==null) return false;

		
		Object idDepPrc = null;
		String departament = "";
		
		if(this.wniosek.getWndSkId() == null)
			return false;

		idDepPrc = prc.get("zat_ob_id");  //JdbcUtils.sqlFnLong(conn, "PPADM.GET_PRC_DEPARTAMENT_ID", wniosek.getWndPrcId());
	 	if(idDepPrc == null)
			 return false;
		 
		 DataRow zf = this.getLW("PPL_DEL_ZRODLA_FIN").find(this.wniosek.getWndSkId());
		 if(zf == null) return false; 
		 
		 DataRow ob = this.getLW("OB").find(idDepPrc);
		 if (ob!=null) 
			 departament = (String) ob.get("ob_kod");
//					 (String) this.execScalarQuery("select ob_kod from EGADM1.CSS_OBIEKTY_W_PRZEDSIEB where OB_ID = ?", idDepPrc);
		 
		 if(departament != null ) //&& depSk != null 
			 return departament.equals(zf.get("dep"));
		  else 
			 return false;
			 

		
	}
	
//	public void usunZaliczkiDelBezkosztowa(Boolean bezkosztowa) {
//		if("N".equals(this.wniosek.getWndFBezkosztowa())) {
//			this.wniosek.setWndFZaliczka("N");
//			usunZaliczkiKalk(this.kalkulacjaWniosek);
//		}
//	}

	private void usunZaliczkiKalk(DeltKalkulacje kalk) {
		Iterator<DelZaliczki> zaliczki = kalk.getDelZaliczki().iterator();
		while(zaliczki.hasNext()) {
			DelZaliczki zal = zaliczki.next();
			zaliczki.remove();
		}
	}
	
//	public void dodajKosztBiletu(DeltKalkulacje kalk, DeltTrasy trasa) {
//
////		if(trasa.getTrsId() == 0L) {
////			User.info("Przed dodaniem biletu zapisz delegację");
////			return;
////		}
//
//		DeltPozycjeKalkulacji poprzedniBilet = null;
//
//		List<DeltPozycjeKalkulacji> listaBiletowPkl = kalk.getDeltPozycjeKalkulacjis().stream().filter(pkl -> pkl.getDeltTrasy() != null).collect(Collectors.toList());
//		if(listaBiletowPkl.size() > 0) {
//			for(DeltPozycjeKalkulacji pkl : listaBiletowPkl) {
//				if(pkl.getDeltTrasy().getTrsId() == trasa.getTrsId())
//					poprzedniBilet = pkl;
//			}
//		}
//
//		
//		if(poprzedniBilet != null) {
//			if(poprzedniBilet.getDeltTrasy().getTrsId()==(trasa.getTrsId())) {
//				poprzedniBilet.setPklStawka(trasa.getTrsKosztBiletu());
//				przeliczKwote(poprzedniBilet);
//
//				if(trasa.getTrsKosztBiletu() == null) {
//					trasa.removeDeltPozycjeKalkulacji(poprzedniBilet);
//					kalk.removeDeltPozycjeKalkulacji(poprzedniBilet);
//				}
//
//			}
//		} else  if(trasa.getTrsKosztBiletu() != null){
//				DeltPozycjeKalkulacji p = newPozycjaKalkulacji(kalk.getKalRodzaj());
//				p.setPklTpozId(getTpozIdByKtgPozycji("T"));
//				if(eq("03",trasa.getTrsSrodekLokomocji()) || eq("07",trasa.getTrsSrodekLokomocji())) {
//					p.setPklFormaPlatnosci("04");
//					p.setPklOpis(kalk.getOpisTrasyKrajeMiasta());
//					User.info("Utworzona zostala pozycja kalkulacji odpowiadająca za koszt całej trasy");
//				} else {
//					p.setPklFormaPlatnosci("03");//wg ostatnich ustalen tylko samolot i pociąg mają umowa z wykonawca
//					p.setPklOpis("" + symbolKraju(trasa.getTrsKrIdOd()) + "/"+trasa.getTrsMiejscowoscOd()
//					+ " do " + symbolKraju(trasa.getTrsKrIdDo()) + "/ "+trasa.getTrsMiejscowoscDo());
//				}
//				p.setPklKrId(1L);
//				p.setPklStawka(trasa.getTrsKosztBiletu());
//				ustawZFAngaze(p);
//				p.setPklIlosc(1.0);
//
//				p.setPklRefundacjaProcent(this.zwrocProcentRefundacji(p.getPklTpozId()));
//				p.setPklRefundacjaKwota(this.ustalKwoteRefundacji(p));
//
//
////				przeliczKwote(p);
//				trasa.addDeltPozycjeKalkulacji(p);
//				//kalk.addDeltPozycjeKalkulacji(p);
//			}
//		}

	
//	public void zmienKosztNaPlanie(DeltKalkulacje kalk, DeltPozycjeKalkulacji pkl) {
//		if(pkl.getDeltTrasy() != null) {
//			Long idTrasy = pkl.getDeltTrasy().getTrsId();
//			for(DeltTrasy trs : kalk.getDeltTrasies()) {
//				if(idTrasy.equals(trs.getTrsId())) {
//					trs.setTrsKosztBiletu(pkl.getPklStawka());
//				}
//			}
//		}
//	}
	
//	public void usunKoszt(DeltKalkulacje kalk, DeltPozycjeKalkulacji pkl) {
//		if("T".equals(pkl.getKategoria()) && pkl.getDeltTrasy() != null) {
//			List<DeltTrasy> cel = kalk.getDeltTrasies().stream().filter(t ->pkl.getDeltTrasy().getTrsId()==(t.getTrsId())).collect(Collectors.toList());
//			if(cel.size()>0) {
//				cel.get(0).setTrsKosztBiletu(null);
//				kalk.removeDeltPozycjeKalkulacji(pkl);
//				cel.get(0).removeDeltPozycjeKalkulacji(pkl);
//			}
//		} else {
//			kalk.removeDeltPozycjeKalkulacji(pkl);
//		}
//	}
	
	
	public Double zwrocProcentKosztMF (Long tpozID) {
 		Double ret = 100.0;
		
		for( DeltPozycjeKalkulacji pkl : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
			if(tpozID.equals(pkl.getPklTpozId()))
				ret = pkl.getPklKosztMFProcent();
		}
		return ret;
	}
	
	public Double zwrocProcentRefundacji (Long tpozID) {
		Double ret = 0.0;
		
		for( DeltPozycjeKalkulacji pkl : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
			if(tpozID.equals(pkl.getPklTpozId()))
				ret = pkl.getPklRefundacjaProcent();
		}
		return ret;
	}
	
//	/**
//	 * Oblicza sumy kwot po walucie wraz z przypisaniem źródeł finansowania dla każdej z walut oraz ustawia datę kursów
//	 */
//	public void obliczPodsumowanieWgWalut(){
//
//		getSumyWgWalut().clear();
//		getZfWgWalut().clear();
//
//		ustawDateKursow(kalkulacjaWstepna);
//
//		if("T".equals(this.wniosek.getWndFBezkosztowa())) {
//			this.wniosek.setWndFZaliczka("N");
//			this.kalkulacjaWstepna.getDelZaliczki().clear();
//			this.setPodsumowaniePln(0.0);
//
//			getSumyWgWalut().put(1L, 0.0);
//			User.info("Zaznaczono opcje delegacji bezkosztowej, koszt całkowity wynosi 0");
//
//			return;
//		}
//
//		if(sprawdzZf(kalkulacjaWstepna))
//			return;
//
//		HashMap<Long, List<DeltZrodlaFinansowania>> zfMapa = new HashMap<>();
//		for (DeltPozycjeKalkulacji pkl : kalkulacjaWstepna.getDeltPozycjeKalkulacjis()) {
//
//			if(pkl.getPklWalId() == null || pkl.getPklKwotaMf() == null)
//				return;
//
//			if(pkl.getPklWalId().equals(1L)) {
//				pkl.setPklKurs(1.0);
//			}
//
//
//			pkl.setPklKurs(kursPrzelicznikSprzedazyNBP(pkl.getPklWalId(), kalkulacjaWstepna.getKalDataKursu()));
//
//			if(pkl.getPklKwotaMf() != null && pkl.getPklKwotaMf() > 0.00) {
//				Long walId = nz(pkl.getPklWalId());
//				getSumyWgWalut().put(walId, nz(getSumyWgWalut().get(walId)) + pkl.getPklKwotaMf());
//
//				List<DeltZrodlaFinansowania> list = zfMapa.get(walId);
//				if(list==null)
//					list = new ArrayList<>();
//
//				list.addAll(pkl.getDeltZrodlaFinansowanias());
//				// TODO zamienić tą hashmapę na inny odpowiednik, który posiada pod value parę skID i kwota a raczej suma kwot jeśli skID się powtórzy,
//				if(list != null)
//				zfMapa.put(walId, list);
//			}
//		}
//
//		/*for(long walId : getSumyWgWalut().keySet())
//			getSumyWgWalut().put(walId, zaokraglenieKwotaWgWaluty(nz(getSumyWgWalut().get(walId)), walId));*/
//
//		if(zfMapa.size() > 0) {
//			for(Long walID : zfMapa.keySet()) {
//				List<DeltZrodlaFinansowania> listaZf = zfMapa.get(walID);
//
//				grupujZf(listaZf);
//
//				getZfWgWalut().put(walID, listaZf);
//			}
//		}
//	}

//	private void grupujZf(List<DeltZrodlaFinansowania> listaZf) {
//		HashMap<Long, Double> listaZfRozZgrupowana = listaZf.stream().filter(zf -> zf.getPkzfKwota() != null).collect(Collectors.groupingBy(
//				DeltZrodlaFinansowania::getPkzfSkId,
//				HashMap::new,
//				Collectors.summingDouble(DeltZrodlaFinansowania::getPkzfKwota)));
//
//		listaZf.clear();
//		for(Long t : listaZfRozZgrupowana.keySet()) {
//			DeltZrodlaFinansowania z = new DeltZrodlaFinansowania();
//			z.setPkzfSkId(t);
//			z.setPkzfKwota(listaZfRozZgrupowana.get(t));
//			listaZf.add(z);
//		}
//	}
	
	
//	public void obliczPodsumowanieRozliczenie(Boolean obliczZaliczki){
//
//		//jeśli delegacje bezkosztowa
//		if("T".equals(this.wniosek.getWndFBezkosztowa())) {
//			this.wniosek.setWndFZaliczka("N");
//			this.kalkulacjaRozliczenia.getDelZaliczki().clear();
//			this.setPodsumowaniePlnRozl(0.0);
//
//			getSumyWgWalutKalkRoz().put(1L, 0.0);
//			User.info("Zaznaczono opcje delegacji bezkosztowej, koszt całkowity wynosi 0");
//
//			return;
//		}
//
//		//jeśli są nieuzupełnione zródła fin to nie liczymy dalej
//		if(sprawdzZf(kalkulacjaRozliczenia))
//			return;
//
//		//ustawiamy datę kursów dla nowych zaliczek rozliczenia
//		ustawDateKursow(kalkulacjaRozliczenia);
//
//		//jeśli są koszty to na początku czyścimy listy pomocnicze
//		getSumyWgWalutKalkRoz().clear();
//		getZfWgWalutKalkRoz().clear();
//
//		//czy tak kolekcja to kwoty w pln?
//		zfDoRozliczenia.clear();
//
//		//kolekcja pomocnicza, gdzie mamy zsumowane kwoty z danej waluty jeśli gotówka bądz przelew
//		setSumyWalutDoRozliczenia(this.zwrocKwoteDoRozliczen());
//
//
//
//		//dla każdej pkl ustaw pomocniczo kurs i zbierz kwoty z zf dla danej waluty
//		for (DeltPozycjeKalkulacji pkl : kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis()) {
//			Long walId = nz(pkl.getPklWalId());
//
//			if(walId.equals(1L)){
//				pkl.setPklKurs(1.0);
//			}
//			pkl.setPklKurs(this.zwrocKursRozliczeniaWaluty(walId));
//
//			if(pkl.getPklKwotaMf() != null && pkl.getPklKwotaMf() > 0.00) {
//				getSumyWgWalutKalkRoz().put(walId, nz(getSumyWgWalutKalkRoz().get(walId)) + nz(pkl.getPklKwotaMf()));
//
//				List<DeltZrodlaFinansowania> list = getZfWgWalutKalkRoz().get(walId);
//				if(list==null)
//					list = new ArrayList<>();
//
//				list.addAll(pkl.getDeltZrodlaFinansowanias());
//
//				grupujZf(list);
//				getZfWgWalutKalkRoz().put(walId, list);
//			}
//		}
//
//		/*for(long walId : getSumyWgWalutKalkRoz().keySet())
//			getSumyWgWalutKalkRoz().put(walId, zaokraglenieKwotaWgWaluty(nz(getSumyWgWalutKalkRoz().get(walId)), walId));*/
//
//		// jeśli w obliczeniu rozliczenia/podsumowania liczymy też zaliczki
//		if(obliczZaliczki) {
//			//dla każdej waluty występującej w rozliczeniu wez zrodla fin
//			for(Long walID : getZfWgWalutKalkRoz().keySet()) {
//				List<DeltZrodlaFinansowania> listaZfRoz = getZfWgWalutKalkRoz().get(walID);
//
//				//ponowne grupowanie na wszelki wypadek?
//				grupujZf(listaZfRoz);
//
//				//TODO sprawdz czy ten if nie jest martwy
//				if(getZfWgWalut().containsKey(walID) && "T".equals(this.wniosek.getWndFBezkosztowa())) {
//					List<DeltZrodlaFinansowania> listaZfWst = getZfWgWalut().get(walID);
//
//					grupujZf(listaZfWst);
//
//					for(DeltZrodlaFinansowania zfR : listaZfRoz) {
//
//						DeltZrodlaFinansowania zf = new DeltZrodlaFinansowania();
//						zf.setPkzfSkId(zfR.getPkzfSkId());
//						zf.setPkzfKwota(zfR.getPkzfKwota());
//
//						for(DeltZrodlaFinansowania zfW : listaZfWst) {
//							if(zfR.getPkzfSkId().equals(zfW.getPkzfSkId())) {
//								zf.setPkzfKwota( (zf.getPkzfKwota() * kursPrzelicznikSprzedazyNBP(walID, kalkulacjaRozliczenia.getKalDataKursu()) - (zfW.getPkzfKwota()) * kursPrzelicznikSprzedazyNBP(walID, kalkulacjaRozliczenia.getKalDataKursu())));
//							}
//						}
//						zf.setPkzfKwota(zf.getPkzfKwota() * kursPrzelicznikSprzedazyNBP(walID, kalkulacjaRozliczenia.getKalDataKursu()));
//						zf.setPkzfKwota(Math.round( (zf.getPkzfKwota() / getJednostkaKursu(walID)) *100)/100.0d );
//						zfDoRozliczenia.add(zf);
//					}
//
//				}
//				//to się wykona, czyli przygotuje liste zf do rozliczenia z uwzględnieniem jednostki kursu i to w pln?
//				else {
//					for(DeltZrodlaFinansowania zfR : listaZfRoz) {
//						DeltZrodlaFinansowania zf = new DeltZrodlaFinansowania();
//						zf.setPkzfSkId(zfR.getPkzfSkId());
//						zf.setPkzfKwota( kursPrzelicznikSprzedazyNBP(walID, kalkulacjaRozliczenia.getKalDataKursu()) * zfR.getPkzfKwota() );
//						zf.setPkzfKwota(Math.round( (zf.getPkzfKwota() / getJednostkaKursu(walID)) *100)/100.0d );
//						zfDoRozliczenia.add(zf);
//					}
//				}
//			}
//
//			grupujZf(zfDoRozliczenia);
//			//przygotuj kopię całej kalkulacji? TODO nie trzeba całej kalkulacji kopiować tylko zaliczki?
//			DeltKalkulacje kopiaKalkWst = this.kalkulacjaWstepna.clone(true);
//			obliczZalRozliczenie(kopiaKalkWst);
//		}
//
//	}

	/**
	 * @param kalk - przekzana kalkulacja, 
	 * @return sprawdza czy w przekazanej kalkulacji nie występują źródła finansowania bez wskazanego kodu źródła, jeśli tak to zwraca true
	 */
	private Boolean sprawdzZf(DeltKalkulacje kalk) {
		if(!kalk.getDeltPozycjeKalkulacjis().isEmpty())
		for(DeltPozycjeKalkulacji pkl : kalk.getDeltPozycjeKalkulacjis()) {
			List<DeltZrodlaFinansowania> zfSkidNull = pkl.getDeltZrodlaFinansowanias().stream().filter(zf -> zf.getPkzfSkId() == null).collect(Collectors.toList());
			if(zfSkidNull.size()>0) {
				String ktoraCzesc = "";
				if("WST".equals(kalk.getKalRodzaj())) {
					ktoraCzesc = "części II - kalkulacja wstępna";
				} else ktoraCzesc = "części III - kalkulacja rozliczająca";
				User.alert("Pozycje kalkulacji na " + ktoraCzesc  +" zawierają źródła finansowania bez wybranego kodu, popraw dane");
				return true;
			}
		}
		return false;
	}

	public Date wyznaczDateRozl() {
		Date dataRozl = null;
		if(!kalkulacjaRozliczenia.getDeltTrasies().isEmpty()) {
//			DeltTrasy ostatniOdcTrasy = this.kalkulacjaRozliczenia.getDeltTrasies()
//					.get(kalkulacjaRozliczenia.getDeltTrasies().size() - 1);
			
		Date maxDate = kalkulacjaRozliczenia.getDeltTrasies().stream().filter(trs -> trs.getTrsDataDo() != null).map(DeltTrasy::getTrsDataDo).max(Date::compareTo).orElse(null);
			
			if(maxDate != null)
				return DateUtils.addDays(maxDate, 14);
		} else if(wniosek.getWndDataPowrotu() != null)
				dataRozl = DateUtils.addDays(wniosek.getWndDataPowrotu(), 14);
		else {
			User.info("Nie udało się ustalić daty obowiązującego rozliczenia");
		}
		
		return dataRozl;
	}
	
//	public Boolean wyslaneKPKW (DeltKalkulacje kalk) {
//
//		Boolean ret = false;
//
//		for( DelZaliczki zal : kalk.getDelZaliczki()) {
//			if(zal.getDkzDokId()!=null) {
//				ret = true;
//				return ret;
//			}
//		}
//
//		return ret;
//	}
	

//	/**
//	 * Przy wyliczeniu podsumowania ustawia datę kursów na taką jak data wniosku jeśli podana albo na 27.01.2017 gdyż tam na pewno są dane o kursach
//	 */
//	private void ustawDateKursow(DeltKalkulacje kalkulacja) {
//		if(kalkulacja.getKalDataKursu() != null) return;
//
//		if(kalkulacja.getKalDataKursu() == null && wniosek.getWndDataWniosku() != null) {
//			kalkulacja.setKalDataKursu(wniosek.getWndDataWniosku());
//		} else {
//			dataKursu = new Date((2017-1900), 0, 27);
//			kalkulacja.setKalDataKursu(dataKursu);
//		}
//	}
	
	
//	public void aktualizujKwotyZF(DeltPozycjeKalkulacji pkl) {
//		if(pkl.getDeltZrodlaFinansowanias().size() > 0) {
//			for(DeltZrodlaFinansowania z : pkl.getDeltZrodlaFinansowanias()) {
//				if(z.getPkzfSkId() != null) {
//					z.setPkzfKwota(procentNaKwote(nz(z.getPkzfProcent()), pkl.getPklKwotaMf()));
//					if(z.getDelAngazes().size() > 0) {
//						for(DelAngaze a : z.getDelAngazes()) {
//							a.setAngKwota(procentNaKwote(a.getAngProcent(), z.getPkzfKwota()));
//						}
//					}
//				}
//			}
//		}
//	}
	
	// zaliczki po staremu
//	public void obliczZaliczki1() {
//		
//		usunZaliczkiKalk(this.kalkulacjaWstepna);
//		
//		List<DelZaliczki> zaliczkiWniosek = new ArrayList<>();
//		for(DelZaliczki zal : kalkulacjaWniosek.getDelZaliczki()) {
//			zaliczkiWniosek.add(zal.clone());
//		}
//		
//		List<Long> lZalWalId = zaliczkiWniosek.stream().map(zal -> zal.getDkzWalId()).collect(Collectors.toList());
//		
//		if(getSumyWgWalut().size() == 0)
//			obliczPodsumowanieWgWalut();
//	
//		for(Long w : getSumyWgWalut().keySet()) {
//			if(!lZalWalId.contains(w)) {
//				DelZaliczki zaliczka = new DelZaliczki();
//				zaliczka.setDkzWalId(w);
//				zaliczka.setDkzKwota(zaokraglenieWgWaluty(getSumyWgWalut().get(w),this.symbolWaluty(w)));
//				zaliczka.setDkzDataWyp(checkDate(wniosek.getWndDataWyjazdu()));
//				kalkulacjaWstepna.addDeltZaliczki(zaliczka);
//			}
//		}
//		
//		for( DelZaliczki zal : zaliczkiWniosek) {
//			if(getSumyWgWalut().containsKey(zal.getDkzWalId())) {
//				
//				DelZaliczki zaliczka = new DelZaliczki();
//				zaliczka.setDkzWalId(zal.getDkzWalId());
//				zaliczka.setDkzKwota(zaokraglenieWgWaluty(getSumyWgWalut().get(zal.getDkzWalId()), this.symbolWaluty(zal.getDkzWalId())));
//
//				
//				if(zal.getDkzDataWyp() == null){
//					zaliczka.setDkzDataWyp(checkDate(wniosek.getWndDataWyjazdu()));
//				} else {
//					zaliczka.setDkzDataWyp(zal.getDkzDataWyp());
//				}
//				
//				zaliczka.setDkzFormaWyplaty(zal.getDkzFormaWyplaty());
//				zaliczka.setDkzNrKonta(zal.getDkzNrKonta());
//				zaliczka.setDkzOpis(zal.getDkzOpis());
//				
//				if(zaliczka.getDkzKwota()>0)
//				kalkulacjaWstepna.addDeltZaliczki(zaliczka);
//			} 
//		}
//	}
	
//	public void obliczZaliczki() {
//
//		HashMap<Long, DelZaliczki> ret = new HashMap<Long, DelZaliczki>();
//
//		List<DeltPozycjeKalkulacji> PKLDoPozostaloDoPwyplaty = kalkulacjaWstepna.getDeltPozycjeKalkulacjis().stream()
//				.filter(p ->("01".equals(p.getPklFormaPlatnosci()) ||"04".equals(p.getPklFormaPlatnosci())))
//				.filter(p -> p.getKwotaWalPozaZaliczkami()>0)
//				.collect(Collectors.toList());
//
//		for (DeltPozycjeKalkulacji pkl : PKLDoPozostaloDoPwyplaty) {
//
//			DelZaliczki z = ret.get(pkl.getPklWalId());
//			if (z==null) {
//				z = new DelZaliczki();
//				z.setDkzWalId(pkl.getPklWalId());
//				z.setDkzKwota(0.0);
//				z.setDkzDataWyp(checkDate(wniosek.getWndDataWyjazdu()));
//				ret.put(z.getDkzWalId(), z);
//			}
//
//			z.setDkzKwota(nz(z.getDkzKwota()) + pkl.getKwotaWalPozaZaliczkami());
//
//			if (z.getDeltKalkulacje()==null) {// && nz(z.getDkzKwota()) > 0.0
//				DelZaliczki zalWniosek = kalkulacjaWniosek.getDelZaliczki().stream().filter(zz -> eq(zz.getDkzWalId(), pkl.getPklWalId()) || zz.getDkzWalId() == null ).findFirst().orElse(null);
//                if (zalWniosek!=null) {
//                    z.setDkzFormaWyplaty(zalWniosek.getDkzFormaWyplaty());
//                    z.setDkzNrKonta(zalWniosek.getDkzNrKonta());
//                    z.setDkzDataWyp(checkDate(wniosek.getWndDataWyjazdu()));
//                    z.setDkzCzyRownowartosc( zalWniosek.getDkzCzyRownowartosc() );
//                }
//                if(z.getDkzKurs() == null)
//                	z.setDkzKurs(new KursWalutySprzedazyNBP(z.getDkzWalId(), kalkulacjaWstepna.getKalDataKursu()).getPrzelicznik());
//				kalkulacjaWstepna.addDeltZaliczki(z);
//			}
//
//			List<DeltZrodlaFinansowania> zfl = pkl.getDeltZrodlaFinansowanias().stream().filter(zf -> zf.getDelZaliczki() == null).collect(Collectors.toList());
//			for (DeltZrodlaFinansowania zf : zfl) {
//				z.addDeltZrodlaFinansowania(zf);
//			}
//
//
////			DeltPklWyplNz dpnz = new DeltPklWyplNz();
////			dpnz.setDpnzWalId(pkl.getPklWalId());
////			dpnz.setDpnzKwotaWal(pkl.getKwotaWalPozaZaliczkami());
////			dpnz.setDpnzKalId(kalkulacjaWstepna.getKalId());
////			dpnz.setDpnzWndId(this.wniosek.getWndId());
////			//dpnz.setDpnzKurs(DPNZ_KURS WYLICZA NZ);
////			//dpnz.setDpnzKwotaPln(DPNZ_KWOTA_PLN wylicza NZ);
////			pkl.addDeltPklWyplNz(dpnz);
////			z.addDeltPklWyplNz(dpnz);
//
//		}
//
//		for (DelZaliczki z : ret.values()) { //polerowanie zaliczek
//			z.setDkzKwota( zaokraglenieKwotaWgWaluty(z.getDkzKwota(), z.getDkzWalId()  ) ); //zaokraglanie wg walut i dostepnych w NBP min. nominałów
//		}
//
//
//		if (1==1)
//			return;
//
////		drop table PPADM.PPT_DEL_PKL_WYPL_NZ cascade constraints; -- select * from sys.all_tab_columns where column_name like 'DPNZ%';
////		create table PPADM.PPT_DEL_PKL_WYPL_NZ(
////				DPNZ_ID NUMBER(10),
////				DPNZ_PKL_ID NUMBER(10),
////				DPNZ_DKZ_ID NUMBER(10),
////				DPNZ_DOK_ID NUMBER(10),
////				DPNZ_WAL_ID NUMBER(10),
////				DPNZ_KWOTA_WAL NUMBER(16,4),
////				DPNZ_KURS NUMBER(16,2),
////				DPNZ_KWOTA_PLN NUMBER(16,2)
////		);
//
//
//
//
//
//
//
//
//
////		if(kalkulacjaWstepna.getKalDataKursu() == null)
////			ustawDateKursow(kalkulacjaWstepna);
////
////		Map<Long, Double> listaWalutGotowka = kalkulacjaWstepna.getDeltPozycjeKalkulacjis().stream()
////				.filter(pkl ->("01".equals(pkl.getPklFormaPlatnosci()) ||"04".equals(pkl.getPklFormaPlatnosci())) && pkl.getPklKwotaMf() != 0.0)
////				.collect(Collectors.groupingBy(pkl -> pkl.getPklWalId(), Collectors.summingDouble(pkl -> pkl.getPklKwotaMf())));
//////
//////		List<Long> walutyRownowartosc = kalkulacjaWniosek.getDelZaliczki().stream()
//////				.filter(zal -> zal.getDkzWalId()!=null && "T".equals(zal.getDkzCzyRownowartosc()) && listaWalutGotowka.containsKey(zal.getDkzWalId()))
//////				.map(zal -> zal.getDkzWalId())
//////				.collect(Collectors.toList());
////
////		Double kwota = null;
//////		for(Long walId : walutyRownowartosc){
//////			if( listaWalutGotowka.containsKey(1L) ){
//////				kwota = listaWalutGotowka.get(walId) * kursPrzelicznikSprzedazyNBP(walId, kalkulacjaWstepna.getKalDataKursu());
//////				listaWalutGotowka.replace(1L, listaWalutGotowka.get(1L)+kwota );
//////				listaWalutGotowka.remove(walId);
//////			}else{
//////				kwota = listaWalutGotowka.get(walId) * kursPrzelicznikSredniNBP(walId, kalkulacjaWstepna.getKalDataKursu());
//////				listaWalutGotowka.put(1L, kwota );
//////				listaWalutGotowka.remove(walId);
//////			}
//////		}
////
////		//TODO zapis kurs równowartosci
////
////		for(Long walId : listaWalutGotowka.keySet()) {
////			List<DelZaliczki> zaliczkaWdanejWalucie = kalkulacjaWstepna.getDelZaliczki().stream().filter(zal -> zal.getDkzDokId()==null && zal.getDkzWalId().equals(walId)).collect(Collectors.toList());
////
////			Double juzWziete = kalkulacjaWstepna.getDelZaliczki().stream().filter(zal -> zal.getDkzDokId()!=null && zal.getDkzWalId().equals(walId)).collect(Collectors.summingDouble(zal -> zal.getDkzKwota()));
////
////            if(!zaliczkaWdanejWalucie.isEmpty()) {
////                zaliczkaWdanejWalucie.get(0).setDkzKwota(zaokraglenieKwotaWgWaluty(listaWalutGotowka.get(walId) - juzWziete>0.0 ? listaWalutGotowka.get(walId) - juzWziete : 0, walId));
////            }
////            else{
////                DelZaliczki zaliczkaWstepna = new DelZaliczki();
////
////                List<DelZaliczki> zaliczkaWniosek = kalkulacjaWniosek.getDelZaliczki().stream().filter(zal -> zal.getDkzWalId()!=null && zal.getDkzWalId().equals(walId)).collect(Collectors.toList());
////                if(!zaliczkaWniosek.isEmpty())
////                    zaliczkaWstepna = zaliczkaWniosek.get(0).clone();
////
////                zaliczkaWstepna.setDkzWalId(walId);
////                zaliczkaWstepna.setDkzKwota(zaokraglenieKwotaWgWaluty(listaWalutGotowka.get(walId) - juzWziete, walId));
////                zaliczkaWstepna.setDkzDataWyp(checkDate(wniosek.getWndDataWyjazdu()));
////                if(zaliczkaWstepna.getDkzKurs() == null)
////                zaliczkaWstepna.setDkzKurs(kursPrzelicznikSprzedazyNBP(walId, kalkulacjaWstepna.getKalDataKursu()));
////
////                DelZaliczki zalWniosek = kalkulacjaWniosek.getDelZaliczki().stream().filter(z -> eq(z.getDkzWalId(), walId) || z.getDkzWalId() == null ).findFirst().orElse(null);
////                if (zalWniosek!=null) {
////                    zaliczkaWstepna.setDkzFormaWyplaty(zalWniosek.getDkzFormaWyplaty());
////                    zaliczkaWstepna.setDkzNrKonta(zalWniosek.getDkzNrKonta());
////                    zaliczkaWstepna.setDkzCzyRownowartosc( zalWniosek.getDkzCzyRownowartosc() );
////
////                }
////
////                kalkulacjaWstepna.addDeltZaliczki(zaliczkaWstepna);
////
////            }
////		}
////
////		//usuwanie zerowych lub ujemnych zaliczek bez dok_id, <0.01d a nie <0.0d bo trzeba usunac zaliczki na kwote mniejsza niz 1/100
////		List<DelZaliczki> zaliczkiDoUsuniecia = kalkulacjaWstepna.getDelZaliczki().stream().filter(zal -> zal.getDkzDokId()==null && (zal.getDkzKwota()==null || zal.getDkzKwota()<0.01d)).collect(Collectors.toList());
////		for(DelZaliczki delZal : zaliczkiDoUsuniecia)
////			kalkulacjaWstepna.removeDelZaliczki(delZal);
////
////		//usuwanie zaliczek w walutach których nie ma w kosztach oraz bez dok_id (z wyjatkiem zaliczek z równowartoscia w PLN)
////		List<DelZaliczki> zaliczkiDoUsuniecia2 = kalkulacjaWstepna.getDelZaliczki().stream().filter(zal -> zal.getDkzDokId()==null && !listaWalutGotowka.containsKey(zal.getDkzWalId()) && !"T".equals(zal.getDkzCzyRownowartosc()) ).collect(Collectors.toList());
////		for(DelZaliczki delZal : zaliczkiDoUsuniecia2)
////			kalkulacjaWstepna.removeDelZaliczki(delZal);
//
//	}

//	public String getSymbolWalutyKonta(DelZaliczki zaliczkaWstepna) {
//		SqlDataSelectionsHandler ppl_del_konta_prc = getLW("PPL_DEL_KONTA_PRC", wniosek.getWndPrcId());
//		return ppl_del_konta_prc.findAndFormat(zaliczkaWstepna.getDkzNrKonta(), "%wal_symbol$s");
//	}

//	public boolean czyZaldoWyplacenia (DeltKalkulacje kalk) {
//		boolean ret = false;
//
//		if(!kalk.getDeltPozycjeKalkulacjis().isEmpty()) {
//			for(DeltPozycjeKalkulacji pkl : kalk.getDeltPozycjeKalkulacjis()) {
//				if(pkl.getPklFormaPlatnosci() != null)
//				if("01".equals(pkl.getPklFormaPlatnosci()) ||"04".equals(pkl.getPklFormaPlatnosci())) {
//					ret = true;
//					return ret;
//				}
//			}
//		}
//
//		return ret;
//	}
	
	public String symbolWaluty(Object walId){
		if (walId==null)
			return "";
		
		SqlDataSelectionsHandler waluty = this.getLW("PPL_SYMBOLE_WALUT");
		DataRow r = waluty.get(walId);
		if (r==null)
			return "";
		
		String symbol = (String) r.get("WAL_SYMBOL2");
		return symbol;
	}
	
	public String symbolKraju(Object pklKrId){
		if (pklKrId==null)
			return "";
		
		SqlDataSelectionsHandler kraje = this.getLW("PPL_WSP_KRAJE");
		DataRow r = kraje.get(pklKrId);
		if (r==null)
			return "";
		
		String symbol = (String) r.get("KR_SYMBOL");
		return symbol;
	}
	
	/**
	 * @param pkzfSkId - ID źródła finansowania
	 * @return - Dla podanego ID źródła finansowania (stanowiska kosztowego) zwraca odpowiadający mu kod
	 */
	public String kodZF(Object pkzfSkId){
		if (pkzfSkId==null)
			return "";
		
		SqlDataSelectionsHandler zrodlaFin = this.getLW("PPL_DEL_ZRODLA_FIN");
		DataRow r = zrodlaFin.get(pkzfSkId);
		if (r==null)
			return "";
		
		String zfKod = (String) r.get("SK_KOD");
		return zfKod;
	} 
	
	
//	public Double zaokraglenieWgWaluty(Double wartosc, String symbolWaluty){
//		Double ret;
//		wartosc = nz(wartosc);
//
//		switch (symbolWaluty) {
//			case "NOK":
//			case "DKK":
//			case "SEK":
//				ret = new BigDecimal(wartosc).setScale(0, RoundingMode.HALF_EVEN).doubleValue();
//				break;
//
//			case "JPY":
//				ret = Double.parseDouble(new BigDecimal(wartosc).setScale(-2, RoundingMode.HALF_EVEN).toPlainString());
//				break;
//
//			case "CHF":
//				ret = new BigDecimal(Math.ceil(wartosc * 20) / 20).setScale(2, RoundingMode.HALF_UP).doubleValue();
//				break;
//
//			default:
//				ret = new BigDecimal(wartosc).setScale(4, RoundingMode.HALF_EVEN).doubleValue();
//
//		}
//
//		return ret;
//	}
	
//	public void wyplacZaliczke(
//			DelZaliczki zal,
//			DeltKalkulacje kalk
//			) {
//
//		if(zal.getDkzDataWyp() == null || zal.getDkzFormaWyplaty() == null){
//			 User.alert("Nie podano daty wypłaty zaliczki. Uzupełnij brakujące dane przed wygenerowaniem polecenia wypłaty zaliczki");
//			 return;
//		}
//
//		if(zal.getDkzFormaWyplaty()==null || zal.getDkzFormaWyplaty().isEmpty()){
//			User.alert("Podaj formę wypłaty.");
//			return;
//		}
//
//		if("B".equals(zal.getDkzFormaWyplaty()) && (zal.getDkzNrKonta() == null ||  zal.getDkzNrKonta().isEmpty())) {
//			User.alert("Podaj numer konta");
//			return;
//		}
//
//		if(zal.getDkzKurs() == null) {
//			if(zal.getDkzWalId() == 1L)
//				zal.setDkzKurs(1.0);
//			else {
//				User.alert("Brak kursu w bazie dla wybranej waluty zaliczki, dokument nie zostanie wysłany");
//				return;
//			}
//		}
//
//		if(zal.getDkzKwota() == 0) {
//			User.alert("Nie można wnioskować o zerową zaliczkę");
//			return;
//		}
//
//		if(wniosek.getWndId() == 0)
//			zapisz();
//
//		List<DeltZrodlaFinansowania> zF = getZFdlaDokumentuNZ(zal, kalk, zal.getDkzKurs());
//		if (zF == null) return;
//
//
//
//		try (Connection conn = JdbcUtils.getConnection())  {
////			System.out.println((wniosek.getWndDataWyjazdu().getYear()+1900));
//			String frmWyplaty = (""+zal.getDkzFormaWyplaty()).toUpperCase(); // ma być K jak kasa lub B jak bank
//
//			String rodzajDokFin = JdbcUtils.sqlFnVarchar(conn, "PPADM.PPP_INTEGRACJA_FIN.f_zwroc_rodzaj_dokumentu", frmWyplaty , "WY", zal.getDkzWalId());
//			Timestamp dataWyplaty = new Timestamp(zal.getDkzDataWyp().getTime());
//
////			zal.setDkzKurs(p_dok_kurs);
//
//			BigDecimal dokId = JdbcUtils.sqlFnBigDecimal(conn,
//					"PPADM.PPP_INTEGRACJA_FIN.WSTAW_KPKW_POPW",
//					zal.getDkzKwota(),
//					zal.getDkzKurs(),
//					"wypłata zaliczki",
//					rodzajDokFin,
//					zal.getDkzWalId(), //waluta inna niz PLN nawet jesli Rownowartosc w PLN i gotówka
//					zal.getDkzNrKonta(),
//					wniosek.getWndPrcId(),
//					wniosek.getWndId(),
//					dataWyplaty,
//					kalk.getKalRodzaj(),
//					zal.getDkzId()
//			);
//
//			getLW("PPL_DEL_KURS_DKZ_DOK").resetTs();
//
//			for(DeltPklWyplNz dpnz : zal.getDeltPklWyplNzs()) {
//				dpnz.setDpnzDokId(dokId.longValue());
//			}
//
//
//			Double doPodzialuZaoPLN = round(zal.getDkzKwota()*zal.getDkzKurs()/this.getJednostkaKursu(zal.getDkzWalId()), 2);
//			Double rozdzielono = 0d;
//			Double doPodzialu = zF.stream().collect(Collectors.summingDouble(z -> z.getPkzfKwota()));
//			Double doPodzialuZaokWAL = this.zaokraglenieKwotaWgWaluty(doPodzialu, zal.getDkzWalId());
//			Double sumaZF = zF.stream().mapToDouble(DeltZrodlaFinansowania::getPkzfKwota).sum();
//
//
//
//			for(int i=0;i<zF.size();i++){
//				DeltZrodlaFinansowania zf = zF.get(i);
//
//				Double kwotaPLN = 0d;
//
//				if(i==zF.size()-1){
//					kwotaPLN = doPodzialuZaoPLN - rozdzielono;
//					//kwotaPLN = kwotaPLN *(zal.getDkzKwota()/sumaZF);
//				}else{
//					kwotaPLN = (zf.getPkzfKwota()/doPodzialu)*doPodzialuZaokWAL*zal.getDkzKurs()/this.getJednostkaKursu(zal.getDkzWalId());
//					kwotaPLN = round(kwotaPLN *(zal.getDkzKwota()/sumaZF), 2);
//				}
//
//				for (DelAngaze ang : zf.getDelAngazes()) {
//					BigDecimal podzialID = JdbcUtils.sqlFnBigDecimal(conn,
//							"PPADM.PPP_INTEGRACJA_FIN.f_wstaw_lub_akt_psk", dokId, zf.getPkzfSkId(), kwotaPLN, (wniosek.getWndDataWyjazdu().getYear() + 1900), ang.getAngWnrId());
//				}
//
//				rozdzielono = rozdzielono + kwotaPLN;
//			}
//
//			JdbcUtils.sqlSPCall(conn, "PPADM.PPP_INTEGRACJA_FIN.oblicz_procenty_psk", dokId);
//
//
//			if (dokId!=null) {
//				zal.setDkzDokId(dokId.longValueExact());
//
//				zapisz();
//
//				generujRaportyJakoZalacznikiPdfDlaDokID(dokId, true, true, true);
//
////				JdbcUtils.sqlSPCall(conn, "PPADM.WYPLAC_ZALICZKE", dokId);
//				this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", kalk.getKalId()).resetTs();
//			}
//			else
//				zal.setDkzDokId(null);
//
//
//			// TODO zapisać z PPL_ZALOGOWANY oddział i firmę zapamiętać a raczej przynajmniej oddział i ustawić niżej dopiero,
//
//		} catch (Exception e) {
//			//log.error(e.getMessage(), e);
//			//User.alert("Wystąpił błąd w trakcie tworzenia dokumentu");
//			User.alert(e);
//		}
////		przygotujDaneKalkRozl(); TODO odkomentuj jak zdebugujesz
//	}



//	private List<DeltZrodlaFinansowania> getZFdlaDokumentuNZ(DelZaliczki zal, DeltKalkulacje kalk, Double p_dok_kurs) {
//		List<DeltZrodlaFinansowania> zF = new ArrayList<>();
//		for(DeltPozycjeKalkulacji p : kalk.getDeltPozycjeKalkulacjis()) {
//			if(!(p.getPklWalId().equals(zal.getDkzWalId()) && ("01".equals(p.getPklFormaPlatnosci()) ||"04".equals(p.getPklFormaPlatnosci()))))
//				continue;
//
//			if (p.getDeltZrodlaFinansowanias().isEmpty()){
//				User.alert("Nie wszystkie pozycje posiadają źródło finansowania.");
//				return null;
//			}
//
//			for( DeltZrodlaFinansowania z: p.getDeltZrodlaFinansowanias()){
//				if (z.getDelAngazes().isEmpty()){
//					User.alert("Nie wszystkie pozycje posiadają wniosek o zaangażowanie środków.");
//					return null;
//				}
//				zF.add(z);
//			}
//		}
//
////		String kalRodzaj = kalk.getKalRodzaj(); //		"WST" lub "ROZ"
////		if ("ROZ".equals(kalRodzaj)) kalRodzaj="%";
////		SqlDataSelectionsHandler pobraneZF = this.getLW("PPL_ZALICZKI_KWOTY", this.wniosek.getWndId(), kalRodzaj, zal.getDkzWalId());
////		if(pobraneZF!=null){
////			for(DeltZrodlaFinansowania zrodlo: zF){
////				DataRow r = pobraneZF.find(zrodlo.getPkzfSkId());
////				if(r!=null){
////					Double kwotaPLN  = ((BigDecimal) r.get("kwota_pln")).doubleValue();
////					if(kwotaPLN!=null && kwotaPLN>0){
////						kwotaPLN = round((kwotaPLN/p_dok_kurs)*this.getJednostkaKursu(zal.getDkzWalId()), 2); //tutaj tak naprawde nie jest to juz kwota w PLN a w danej walucie
////						zrodlo.setPkzfKwota(zrodlo.getPkzfKwota()-kwotaPLN>0?zrodlo.getPkzfKwota()-kwotaPLN:0);
////					}
////				}
////			}
////		}
//
//		zF = zF.stream().filter(z -> nz(z.getPkzfKwota())>0).collect(Collectors.toList());
//		return zF;
//	}


//	public void modyfikujZalDok(DelZaliczki zal) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
//
//		if(zal.getDkzDokId() == null || zal.getDkzDataWyp() == null)
//			return;
//		Timestamp dataWyplaty = new Timestamp(zal.getDkzDataWyp().getTime());
//
//		 if(!sprawdzCzyZalZatwierdzona(zal)){
//			try (Connection conn = JdbcUtils.getConnection()) {
//					JdbcUtils.sqlSPCall(conn, "PPADM.ppp_del_zaliczki.Modyfikuj_dok_zal",zal.getDkzDokId(), dataWyplaty);
//					HibernateContext.saveOrUpdate(this.wniosek);
//					User.info("Zmodyfikowano datę wypłaty na zaliczce w FK");
//			} catch (Exception e) {
//				User.alert(e);
//			}
//		 } else {
//			 User.alert("Zaliczka została już zatwierdzona i nie można jej modyfikować");
//			 return;
//		 }
//	}
	
//	public void usunZalKW(DelZaliczki zal, DeltKalkulacje kalk) throws SQLException{
//		if(zal.getDkzDokId() != null) {
//			try (Connection conn = JdbcUtils.getConnection()) {
//				Long dokIdDoUsuniecia = zal.getDkzDokId();
//				JdbcUtils.sqlSPCall(conn, "PPADM.ppp_del_zaliczki.usun_zal",dokIdDoUsuniecia);
//				kalk.removeDelZaliczki(zal);
//				HibernateContext.saveOrUpdate(this.wniosek);
//			} catch (Exception e) {
//				User.alert(e);
//			}
//		} else {
//			this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", kalk.getKalId()).resetTs();
//			kalk.removeDelZaliczki(zal);
//		}
//	}
	
	
//	public boolean sprawdzCzyZalZatwierdzona(DelZaliczki zal) {
//		if (zal==null || zal.getDeltKalkulacje()==null)
//			return false;
//
//		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", zal.getDeltKalkulacje().getKalId());
//		if (lw==null)
//			return false;
//
//		DataRow r = lw.find(zal.getDkzDokId());
//		if (r==null)
//			return false;
//
//		Object object = r.get("dok_f_zatwierdzony");
//		return ("T".equals( object) );
//
////		if(zal.getDkzDokId() != null) {
////		 Object czyZatwierdzony = this.execScalarQuery("select dok_f_zatwierdzony, dok_dok_id_zap from kgt_dokumenty where dok_id = ?",
////					zal.getDkzDokId());
////		 if("T".equals(czyZatwierdzony)){
//////			 User.alert("Zaliczka została już zatwierdzona i nie można jej modyfikować");
////			 return true;
////		 } else return false;
////		} else return false;
//	}
	
//	public boolean sprawdzCzyZalWyplacona(DelZaliczki zal) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
//		if (zal==null || zal.getDeltKalkulacje()==null)
//			return false;
//			
//		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", zal.getDeltKalkulacje().getKalId());
//		if (lw==null)
//			return false;
//		
//		DataRow r = lw.find(zal.getDkzDokId());
//		if (r==null)
//			return false;
//		
//		Object object = r.get("dok_dok_id_zap");
//		return object != null;
//	}
	
//	public void generujDokumentNZ(DelZaliczki zal) {
//
//		if(zal.getDkzDataWyp() == null)
//			zal.setDkzDataWyp(today());
//
//		zal.setDkzKurs(zal.getKurs().getPrzelicznik());
//
//		if(zal.getDkzFormaWyplaty()==null || zal.getDkzFormaWyplaty().isEmpty()){
//			User.alert("Podaj formę rozliczenia.");
//			return;
//		}
//
//		if( "B".equals(zal.getDkzFormaWyplaty()) && (zal.getDkzNrKonta() == null || zal.getDkzNrKonta().isEmpty())) {
//			User.alert("Podaj nr konta");
//			return;
//		}
//
//		if(wniosek.getWndId() == 0) {
//			zapisz();
//		}
//
//		List<DeltZrodlaFinansowania> zF = getZFdlaDokumentuNZ(zal, zal.getDeltKalkulacje(), zal.getDkzKurs());
//		if (zF == null) return;
//
//		String frmWyplaty = (""+zal.getDkzFormaWyplaty()).toUpperCase(); // ma być K jak kasa lub B jak bank
//		String wyplataWplata = this.rodzajDokumentu(zal);
//		Timestamp dataWyplaty = new Timestamp(zal.getDkzDataWyp().getTime());
//		Double doPodzialuZaoPLN = round(zal.getDkzKwota()*zal.getDkzKurs()/this.getJednostkaKursu(zal.getDkzWalId()), 2);
//		Double rozdzielono = 0d;
//		Double doPodzialu = zF.stream().collect(Collectors.summingDouble(z -> z.getPkzfKwota()));
//		Double doPodzialuZaokWAL = doPodzialu;
//		Double sumaZF = zF.stream().mapToDouble(DeltZrodlaFinansowania::getPkzfKwota).sum();
//
//		try (Connection conn = JdbcUtils.getConnection())  {
//
//			String rodzajDokFin = JdbcUtils.sqlFnVarchar(conn, "PPADM.PPP_INTEGRACJA_FIN.f_zwroc_rodzaj_dokumentu", frmWyplaty , wyplataWplata, zal.getDkzWalId());
//            BigDecimal dokId = JdbcUtils.sqlFnBigDecimal(conn, "PPADM.PPP_INTEGRACJA_FIN.WSTAW_KPKW_POPW",
//								    Math.abs(zal.getDkzKwota()),
//								    	zal.getDkzKurs(),
//								    	zal.getDeltKalkulacje().getKalRodzaj().equals("ROZ") ? "rozliczenie":"wypłata zaliczki",
//										rodzajDokFin,
//										zal.getDkzWalId(),
//										zal.getDkzNrKonta(),
//										wniosek.getWndPrcId(),
//										wniosek.getWndId(),
//										dataWyplaty,
//										zal.getDeltKalkulacje().getKalRodzaj(),
//										zal.getDkzId()
//								);
//
//			getLW("PPL_DEL_KURS_DKZ_DOK").resetTs();
//
//			for(int i=0;i<zF.size();i++){
//				DeltZrodlaFinansowania zf = zF.get(i);
//				Double kwotaPLN = 0d;
//
//				if(i==zF.size()-1){
//					kwotaPLN = doPodzialuZaoPLN - rozdzielono;
//				}else{
//					kwotaPLN = (zf.getPkzfKwota()/doPodzialu)*doPodzialuZaokWAL*zal.getDkzKurs()/this.getJednostkaKursu(zal.getDkzWalId());
//					kwotaPLN = round(kwotaPLN *(zal.getDkzKwota()/sumaZF), 2);
//				}
//
//				for (DelAngaze ang : zf.getDelAngazes()) {
//					System.out.println();
//					BigDecimal podzialID = JdbcUtils.sqlFnBigDecimal(conn,
//							"PPADM.PPP_INTEGRACJA_FIN.f_wstaw_lub_akt_psk", dokId, zf.getPkzfSkId(), kwotaPLN,(wniosek.getWndDataWyjazdu().getYear()+1900), ang.getAngWnrId());
//				}
//
//				rozdzielono = rozdzielono + kwotaPLN;
//			}
//			JdbcUtils.sqlSPCall(conn, "PPADM.PPP_INTEGRACJA_FIN.oblicz_procenty_psk", dokId);
//
//			if (dokId!=null) {
//				zal.setDkzDokId(dokId.longValueExact());
//				zapisz();
////				JdbcUtils.sqlSPCall(conn, "PPADM.WYPLAC_ZALICZKE", dokId);
//				generujRaportyJakoZalacznikiPdfDlaDokID(dokId, true, true, true);
//
//			}
//			else
//				zal.setDkzDokId(null);
//
//		} catch (Exception e) {
//			User.alert(e);
//		}
//
//		zal.getDeltKalkulacje().clearDelZaliczkiReszta();
//	}

//	private Long zwrocDokIdKalkWst( Long walId) {
//		Long ret = null;
//		if(this.wniosek.getWndFZaliczka().equals("N")) {
//			List<DelZaliczki> listaZal = kalkulacjaWstepna.getDelZaliczki().stream().filter(zal -> zal.getDkzWalId().equals(walId)).collect(Collectors.toList());
//			if(listaZal.size() > 0) {
//				List<DelZaliczki> listaZDokId = listaZal.stream().filter(z -> z.getDkzDokId() != null).collect(Collectors.toList());
//				if(listaZDokId.size() > 0)
//					ret = listaZDokId.get(0).getDkzDokId();
//			}
//		}
//		return ret;
//	}
	
//	private Double zwrocKursZKG(Long dkzDokId, DeltKalkulacje kalk) {
//
//		Double kurs = null;
//
//		SqlDataSelectionsHandler daneKsgKalLW = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL",kalk.getKalId() );
//
//		DataRow r = daneKsgKalLW.find(dkzDokId);
//
//		if(r != null);
//		kurs = (Double)r.get("dok_kurs");
//
//	return kurs;
//}

//	public String rodzajDokumentu (DelZaliczki zal) {
//		String ret = "";
//			if(zal.getDkzKwota() > 0)
//				ret = "WY";
//			else ret = "WP";
//
//		return ret;
//	}
	
	
	
		
	public void przygotujDaneKalkRozl() {	
//		if("T".equals(wniosek.getWndFZaliczka())) {
//			for(DelZaliczki z : kalkulacjaWstepna.getDelZaliczki()) {
//			if( z.getDkzDokId() == null){
//				 User.alert("W części II występuje zaliczka bez wygenerowanego dokumentu FIN."
//				 		+ "\n"+ "Wygeneruj polecenie przelewu/wypłaty <b>dla każdej</b> wnioskowanej zaliczki przed przystąpieniem do rozliczenia.");
//				 this.setTabDelegacji(1);
//				 return;
//				}
//			}
//		}
		
//		Date dzisiaj = new Date(); //TODO taki warunek można postawic w przyszłości
//		if(dzisiaj.before(wniosek.getWndDataPowrotu())) {
//			 User.alert("Nie można rozliczyć delegacji w trakcie jej trwania");
//			 return;
//		}
		
		DeltKalkulacje kopiaKalkWst = this.kalkulacjaWstepna.clone(true);
		kopiaKalkWst.setKalId(0L);
		kopiaKalkWst.setDeltWnioskiDelegacji(wniosek);
		
		this.kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis().clear();
		for(DeltPozycjeKalkulacji pkl:kopiaKalkWst.getDeltPozycjeKalkulacjis()) {
//			if("01".equals(pkl.getPklFormaPlatnosci()) ||"04".equals(pkl.getPklFormaPlatnosci()))
			if(pkl.czyKopiowacNaRozliczenie())
			this.kalkulacjaRozliczenia.addDeltPozycjeKalkulacji(pkl);
		}

		this.kalkulacjaRozliczenia.usunZFZeWszystkichDKZwNZ();
		this.kalkulacjaRozliczenia.przypiszZFDoWszystkichNieprzypisanychDKZzNZ();

		this.kalkulacjaRozliczenia.getDeltTrasies().clear();
		for(DeltTrasy t:kopiaKalkWst.getDeltTrasies()) {
			this.kalkulacjaRozliczenia.addDeltTrasy(t);
		}
		
		
		for(DeltPozycjeKalkulacji pk: this.kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis()) {
			if(pk.cloneSrc.getDeltTrasy() != null)
			{
				pk.cloneSrc.getDeltTrasy().cloneDest.addDeltPozycjeKalkulacji(pk);
			}
		}
		
		
		
		
		this.kalkulacjaRozliczenia.getDelCzynnosciSluzbowe().clear();
		if(!kopiaKalkWst.getDelCzynnosciSluzbowe().isEmpty()) {
			for(DelCzynnosciSluzbowe cz : kopiaKalkWst.getDelCzynnosciSluzbowe()) {
				this.kalkulacjaRozliczenia.addDelCzynnosciSluzbowe(cz);
			}
		}
		
//		if("T".equals(wniosek.getWndFZaliczka()))
//		obliczPodsumowanieRozliczenie(false);
//		
//		else
//			obliczPodsumowanieRozliczenie(false);
//		
		//this.kalkulacjaRozliczenia.clearDelZaliczkiReszta();

		this.kalkulacjaRozliczenia.przeliczPodzialZFvsDKZ();
		this.zapisz();
		//
//		List<DeltPozycjeKalkulacji> kosztTransportuRoz = kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis().stream().filter(pkl ->"T".equals(pkl.getKategoria())).collect(Collectors.toList());
//		for(DeltPozycjeKalkulacji pkl : kosztTransportuRoz) {
//			 List<DeltTrasy> deltTrasy = kalkulacjaRozliczenia.getDeltTrasies().stream()
//					.filter(trs -> trs.getTrsKosztBiletu() != null)
//					.filter(trs -> trs.getTrsKosztBiletu().equals(pkl.getPklKwotaMf()))
//					.collect(Collectors.toList());
//			 if(deltTrasy!= null && !deltTrasy.isEmpty()) 
//				pkl.setDeltTrasy(deltTrasy.get(0));
//		}
		
//		this.tabDelegacji = 2;
	}
	
	
//	private void obliczZalRozliczenie(DeltKalkulacje kopiaKalkWst) { /// tukej ostro
//		//this.kalkulacjaRozliczenia.getDelZaliczki().clear();
//
//			// patrzymy i sumujemy kolekcję id waluty, kwota - oczywiście tylko przelew i gotówka
//			Map<Long, Double> listaWalutGotowka = kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis().stream()
//					.filter(pkl -> "01".equals(pkl.getPklFormaPlatnosci()) ||"04".equals(pkl.getPklFormaPlatnosci()) && pkl.getPklKwotaMf() != 0.0)
//					.collect(Collectors.groupingBy(pkl -> pkl.getPklWalId(), Collectors.summingDouble(pkl -> pkl.getPklKwotaMf())));
//
//
//			//sprawdzamy, które z walut mają zaznaczoną równowartość w pln
////			List<Long> walutyRownowartosc = kalkulacjaWniosek.getDelZaliczki().stream()
////					.filter(zal -> zal.getDkzWalId()!=null && "T".equals(zal.getDkzCzyRownowartosc()) && listaWalutGotowka.containsKey(zal.getDkzWalId()))
////					.map(zal -> zal.getDkzWalId())
////					.collect(Collectors.toList());
//
//
//			//TODO - to do poprawy bo równowartość to kurs sredni a poza tym trzeba go wziąć z dkz_dok_id a nie liczyć teraz
//			Double kwota = null;
////			for(Long walId : walutyRownowartosc){
////				if( listaWalutGotowka.containsKey(1L) ){
////					kwota = listaWalutGotowka.get(walId) * kursPrzelicznikSprzedazyNBP(walId, kalkulacjaRozliczenia.getKalDataKursu());
////					listaWalutGotowka.replace(1L, listaWalutGotowka.get(1L)+kwota );
////					listaWalutGotowka.remove(walId);
////				}else{
////					kwota = listaWalutGotowka.get(walId) * kursPrzelicznikSprzedazyNBP(walId, kalkulacjaRozliczenia.getKalDataKursu());
////					listaWalutGotowka.put(1L, kwota );
////					listaWalutGotowka.remove(walId);
////				}
////			}
//
//
//			//TUKEJ POSZŁA ZMIANA dodano sprawdzCzyDokAnulowany żeby sprawdzić czy dokument nie został anulowany, jak anulowany to nie bierzemy pod uwagę tak jakby nie miał tej zalliczki
//			List<Long> walutyZNieWydanaZaliczka = kopiaKalkWst.getDelZaliczki().stream()
//					.filter(dkz -> listaWalutGotowka.get(dkz.getDkzWalId())==null && dkz.getDkzDokId()!=null
//						&& !sprawdzCzyDokAnulowany(dkz.getDkzDokId()))
//					.map(dkz -> dkz.getDkzWalId())
//					.collect(Collectors.toList());
//
//
//			for(Long wal : walutyZNieWydanaZaliczka){
//				listaWalutGotowka.put(wal, null);
//			}
//
//
//			for(Long walId : listaWalutGotowka.keySet()) {
//
//				Double sumaDoWyplatyROZ = this.kalkulacjaRozliczenia.getDelZaliczki().stream()
//						.filter(dkz -> dkz.getDkzWalId().equals(walId) && dkz.getDkzDokId()!=null)
//						.collect(Collectors.summingDouble(dkz -> dkz.getDkzSrodkiWlasne()));
//
//
//				Double sumaDoOddaniaROZ = this.kalkulacjaRozliczenia.getDelZaliczki().stream()
//						.filter(dkz -> dkz.getDkzWalId().equals(walId) && dkz.getDkzDokId()!=null)
//						.collect(Collectors.summingDouble(dkz -> dkz.getDkzKwota()));
//
//				// tutaj też zmieniono zamiast dkz.getKwota jest zwrorcKwoteDokKgt czyli bezpośrednio z dokumentu a nie naszej tabeli
//				Double sumaZaliczekWST = kopiaKalkWst.getDelZaliczki().stream()
//						.filter(dkz -> dkz.getDkzWalId().equals(walId) && dkz.getDkzDokId()!=null && !sprawdzCzyDokAnulowany(dkz.getDkzDokId()))
//						.collect(Collectors.summingDouble(dkz -> this.zwrocKwoteDokKgt(dkz.getDkzDokId())));
//
//
//				Double ileFaktycznieWydanoROZ = nz(listaWalutGotowka.get(walId));
//
//				List<DelZaliczki> doUsuniecia = this.kalkulacjaRozliczenia.getDelZaliczki().stream().filter(dkz -> dkz.getDkzWalId().equals(walId) && dkz.getDkzDokId()==null).collect(Collectors.toList());
//				for(DelZaliczki zal : doUsuniecia)
//					kalkulacjaRozliczenia.removeDelZaliczki(zal);
//
//
//				if(Utils.round(sumaDoWyplatyROZ, 2) - Utils.round(sumaDoOddaniaROZ, 2) == Utils.round(ileFaktycznieWydanoROZ, 2) - Utils.round(sumaZaliczekWST, 2)){
////					if(Utils.round(sumaDoWyplatyROZ, 2) - Utils.round(sumaDoOddaniaROZ, 2) == Utils.round(zaokraglenieKwotaWgWaluty(ileFaktycznieWydanoROZ, walId), 2) - Utils.round(sumaZaliczekWST, 2)){
//                    //wszystko jest ju git
//				}
//				else
//				{
//					//teraz trzeba nową zaliczke, zaraz obliczymy jaką
//
//					DelZaliczki zal = new DelZaliczki();
//					zal.setDkzWalId(walId);
//
//					if("T".equals(wniosek.getWndFZaliczka())){
//						Double wziete = this.kalkulacjaRozliczenia.getDelZaliczki().stream()
//								.filter(dkz -> dkz.getDkzWalId().equals(walId))
//								.collect(Collectors.summingDouble(dkz -> dkz.getDkzZalWydano()));
//
//						zal.setDkzZalWydano(sumaZaliczekWST - wziete);
////						zal.setDkzZalWydano(zaokraglenieKwotaWgWaluty(sumaZaliczekWST - wziete, walId));
//						if(ileFaktycznieWydanoROZ - sumaZaliczekWST > sumaDoWyplatyROZ - sumaDoOddaniaROZ){
//							zal.setDkzKwota(0d);
//							zal.setDkzSrodkiWlasne(ileFaktycznieWydanoROZ - sumaZaliczekWST-(sumaDoWyplatyROZ - sumaDoOddaniaROZ));
////							zal.setDkzSrodkiWlasne(zaokraglenieKwotaWgWaluty(ileFaktycznieWydanoROZ - sumaZaliczekWST-(sumaDoWyplatyROZ - sumaDoOddaniaROZ), walId));
//						}else{
//							zal.setDkzKwota(-(ileFaktycznieWydanoROZ - sumaZaliczekWST-(sumaDoWyplatyROZ - sumaDoOddaniaROZ)));
////							zal.setDkzKwota(zaokraglenieKwotaWgWaluty(-(ileFaktycznieWydanoROZ - sumaZaliczekWST-(sumaDoWyplatyROZ - sumaDoOddaniaROZ)), walId) );
//							zal.setDkzSrodkiWlasne(0d);
//
//						}
//					}else{
//						zal.setDkzKwota((ileFaktycznieWydanoROZ - sumaDoWyplatyROZ)<0?(sumaDoWyplatyROZ - ileFaktycznieWydanoROZ):0);
//						zal.setDkzZalWydano(0.0);
//						zal.setDkzSrodkiWlasne((ileFaktycznieWydanoROZ - sumaDoWyplatyROZ)>0?(ileFaktycznieWydanoROZ - sumaDoWyplatyROZ):0);
//					}
//
//					zal.setDkzKurs(zwrocKursRozliczeniaWaluty(walId));
//					kalkulacjaRozliczenia.addDeltZaliczki(zal);
//
//				}
//
//			}
//
//
//			//usuwanie zaliczek w walutach których nie ma w kosztach oraz bez dok_id (z wyjatkiem zaliczek z równowartoscia w PLN)
//			List<DelZaliczki> zaliczkiDoUsuniecia = kalkulacjaRozliczenia.getDelZaliczki().stream().filter(zal -> zal.getDkzDokId()==null && !listaWalutGotowka.containsKey(zal.getDkzWalId()) && !"T".equals(zal.getDkzCzyRownowartosc()) ).collect(Collectors.toList());
//			for(DelZaliczki delZal : zaliczkiDoUsuniecia)
//				kalkulacjaRozliczenia.removeDelZaliczki(delZal);
//
//
//			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//			//tutaj trzeba jeszcze dodac zaliczki dla tych walut dla ktorych byla wzieta zaliczka na czesc II a w czesc III usunieto wydatki w tej walucie
//			/*List<DelZaliczki> doOddaniaBoNicNieWydane = kopiaKalkWst.getDelZaliczki().stream().filter(dkz -> dkz.getDkzDokId()!=null && listaWalutGotowka.get(dkz.getDkzWalId())==null ).collect(Collectors.toList());
//			for(DelZaliczki zalDoOddania : doOddaniaBoNicNieWydane){
//				List<DelZaliczki> zaliczkiWTejWalucie = this.kalkulacjaRozliczenia.getDelZaliczki().stream().filter(dkz -> dkz.getDkzWalId().equals(zalDoOddania.getDkzWalId())).collect(Collectors.toList());
//				if(zaliczkiWTejWalucie.isEmpty()){
//					DelZaliczki zal = new DelZaliczki();
//					zal.setDkzWalId(zalDoOddania.getDkzWalId());
//					zal.setDkzZalWydano(zaokraglenieKwotaWgWaluty(zalDoOddania.getDkzKwota(), zalDoOddania.getDkzWalId()));
//					zal.setDkzSrodkiWlasne(0d);
//					zal.setDkzKwota(zaokraglenieKwotaWgWaluty(zalDoOddania.getDkzKwota(), zalDoOddania.getDkzWalId()));
//					kalkulacjaRozliczenia.addDeltZaliczki(zal);
//				}
//			}*/
//			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	/*		if("T".equals(wniosek.getWndFZaliczka())) {
//
//				for(Long walId : listaWalutGotowka.keySet()) {
//
//					List<DelZaliczki> listaPobranaZal = kopiaKalkWst.getDelZaliczki().stream().filter(dkz -> dkz.getDkzWalId().equals(walId)).collect(Collectors.toList());
//					if(!listaPobranaZal.isEmpty()) {
//						DelZaliczki pobranaZal = listaPobranaZal.get(0);
//
//						DelZaliczki zal = new DelZaliczki();
//						zal.setDkzWalId(pobranaZal.getDkzWalId());
//
//						if(listaWalutGotowka.get(walId) > pobranaZal.getDkzKwota()) {
//							zal.setDkzSrodkiWlasne(listaWalutGotowka.get(walId) - pobranaZal.getDkzKwota());
//							zal.setDkzZalWydano(pobranaZal.getDkzKwota());
//							zal.setDkzKwota(0.0);
//
//						} else if (listaWalutGotowka.get(walId) < pobranaZal.getDkzKwota()) {
//							zal.setDkzZalWydano(listaWalutGotowka.get(walId));
//							zal.setDkzKwota(pobranaZal.getDkzKwota() - listaWalutGotowka.get(walId));
//							zal.setDkzSrodkiWlasne(0.0);
//
//						} else if(listaWalutGotowka.get(walId).equals(pobranaZal.getDkzKwota())) {
//							zal.setDkzZalWydano(listaWalutGotowka.get(walId));
//							zal.setDkzKwota(0.0);
//							zal.setDkzSrodkiWlasne(0.0);
//						}
//
//						kalkulacjaRozliczenia.addDeltZaliczki(zal);
//					} else {
//						DelZaliczki zal = new DelZaliczki();
//						zal.setDkzWalId(walId);
//						zal.setDkzZalWydano(0.0);
//						zal.setDkzSrodkiWlasne(listaWalutGotowka.get(walId));
//						zal.setDkzKwota(0.0);
//
//						kalkulacjaRozliczenia.addDeltZaliczki(zal);
//					}
//				}
//			} else {
//				for(Long walId : listaWalutGotowka.keySet()) {
//
//					DelZaliczki zal = new DelZaliczki();
//					zal.setDkzWalId(walId);
//					zal.setDkzZalWydano(0.0);
//					zal.setDkzSrodkiWlasne(listaWalutGotowka.get(walId));
//					zal.setDkzKwota(0.0);
//
//					kalkulacjaRozliczenia.addDeltZaliczki(zal);
//				}
//			}*/
//
//	}
	
//	public boolean sprawdzCzyDokAnulowany (Long dokId) {
//		if(dokId == null)
//			return true;
//
//		boolean ret = false;
//
//		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.kalkulacjaWstepna.getKalId());
//		DataRow kgtRow = lw.find(dokId);
//
//		if(kgtRow != null) {
//			Object object = kgtRow.get("dok_f_anulowany");
//			if(object != null) {
//				ret = !object.equals("N");
//			}
//		}
//
//		return ret;
//	}
	
//	public double zwrocKwoteDokKgt (Long dokId) {
//		double ret = 0.00;
//
//		if(dokId == null)
//			return ret;
//
//
//		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.kalkulacjaWstepna.getKalId());
//		DataRow kgtRow = lw.find(dokId);
//
//		if(kgtRow != null) {
//			Object object = kgtRow.get("dok_kwota_waluty");
//			if(object != null) {
//				ret = ((BigDecimal)object).doubleValue();
//			}
//		}
//
//		return ret;
//	}
	
//	public double zwrocKwotePlnDokKgt (Long dokId) {
//		double ret = 0.00;
//
//		if(dokId == null)
//			return ret;
//
//
//		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.kalkulacjaWstepna.getKalId());
//		DataRow kgtRow = lw.find(dokId);
//
//		if(kgtRow != null) {
//			Object object = kgtRow.get("dok_kwota");
//			if(object != null) {
//				ret = ((BigDecimal)object).doubleValue();
//			}
//		}
//
//		return ret;
//	}
	
//	public double zwrocKursDokKgt (Long dokId) {
//		double ret = 0.00;
//
//		if(dokId == null)
//			return ret;
//
//
//		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.kalkulacjaWstepna.getKalId());
//		DataRow kgtRow = lw.find(dokId);
//
//		if(kgtRow != null) {
//			Object object = kgtRow.get("dok_kurs");
//			if(object != null) {
//				ret = ((BigDecimal)object).doubleValue();
//			}
//		}
//
//		return ret;
//	}
	
	
//	public double zwrocKursDokKgtRoz (Long dokId) {
//		double ret = 0.00;
//
//		if(dokId == null)
//			return ret;
//
//
//		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.kalkulacjaRozliczenia.getKalId());
//		DataRow kgtRow = lw.find(dokId);
//
//		if(kgtRow != null) {
//			Object object = kgtRow.get("dok_kurs");
//			if(object != null) {
//				ret = ((BigDecimal)object).doubleValue();
//			}
//		}
//
//		return ret;
//	}
	
	
	
//	HashMap<Long, String> opisZwrocKursRozliczeniaWalutyHM = new HashMap<>();
//	public String opisZwrocKursRozliczeniaWaluty(Long walId){
//		return opisZwrocKursRozliczeniaWalutyHM.get(walId);
//	}
//	public double zwrocKursRozliczeniaWaluty(Long walId) {
//		if(walId == 1) {
//			opisZwrocKursRozliczeniaWalutyHM.put(walId, "kurs  techniczny (dla PLN)");
//			return 1.0;
//		}
//		else {
//
//			this.dataKursu = this.kalkulacjaRozliczenia.getKalDataKursu() != null ?
//					this.kalkulacjaRozliczenia.getKalDataKursu() : this.kalkulacjaWstepna.getKalDataKursu();
//
//			List<DelZaliczki> dokIdZalRoz = this.kalkulacjaRozliczenia.getDelZaliczki()
//					.stream()
//					.filter(zal -> zal.getDkzDokId() != null && zal.getDkzWalId().equals( walId )  && !this.sprawdzCzyDokAnulowany(zal.getDkzDokId()))
//					.collect(Collectors.toList())
//					;
//
//			List<DelZaliczki> dokIdZalPobranej = this.kalkulacjaWstepna.getDelZaliczki()
//					.stream()
//					.filter(zal -> zal.getDkzDokId() != null && zal.getDkzWalId().equals( walId ) && !this.sprawdzCzyDokAnulowany(zal.getDkzDokId()))
//					.collect(Collectors.toList())
//					;
//
//			if("N".equals(this.wniosek.getWndFZaliczka())) {
//				if(dokIdZalRoz.size()>0) {
//					opisZwrocKursRozliczeniaWalutyHM.put(walId, "kurs z dokumentu zaliczki/rozliczenia w module FIN/NZ");
//					return this.zwrocKursDokKgtRoz(dokIdZalRoz.get(0).getDkzDokId());
//				}else {
//					opisZwrocKursRozliczeniaWalutyHM.put(walId, "kurs sprzedaży NBP z dnia " + format(this.dataKursu));
//					return this.kursPrzelicznikSprzedazyNBP(walId, this.dataKursu);
//				}
//			} else {
//				if(dokIdZalPobranej.size()>0) {
//					opisZwrocKursRozliczeniaWalutyHM.put(walId, "kurs z wypłaconej zaliczki w module FIN/NZ");
//					return this.zwrocKursDokKgt(dokIdZalPobranej.get(0).getDkzDokId());
//				}
//				else if(dokIdZalRoz.size()>0) {
//					opisZwrocKursRozliczeniaWalutyHM.put(walId, "kurs z dokumentu zaliczki/rozliczenia w module FIN/NZ");
//					return this.zwrocKursDokKgtRoz(dokIdZalRoz.get(0).getDkzDokId());
//				}
//				else if(dokIdZalPobranej.size()<1 && dokIdZalRoz.size()<1) {
//					opisZwrocKursRozliczeniaWalutyHM.put(walId, "kurs sprzedaży NBP z dnia " + format(this.dataKursu));
//					return this.kursPrzelicznikSprzedazyNBP(walId, this.dataKursu);
//				}
//			}
//		}
//
//		opisZwrocKursRozliczeniaWalutyHM.put(walId, "kurs sprzedaży NBP z dnia " + format(this.dataKursu));
//		return this.kursPrzelicznikSprzedazyNBP(walId, this.dataKursu);
//	}
	
	
//	private void obliczZalRozliczenie1(DeltKalkulacje kopiaKalkWst) {
//		this.kalkulacjaRozliczenia.getDelZaliczki().clear();
//
//		if("T".equals(wniosek.getWndFZaliczka())) {
//			for(DelZaliczki z : kopiaKalkWst.getDelZaliczki()) {
//				if(z.getDkzDokId() != null && sumyWgWalutKalkRoz.get(z.getDkzWalId()) != null) {
//
//					DelZaliczki zal = new DelZaliczki();
//					zal.setDkzWalId(z.getDkzWalId());
//					zal.setDkzKwota(sumyWgWalutKalkRoz.get(z.getDkzWalId()) - z.getDkzKwota());
//
//					zal.setDkzZalWydano(Math.abs(z.getDkzKwota()));
//					if(zal.getDkzKwota() > 0) {
//						zal.setDkzSrodkiWlasne(zal.getDkzKwota());
//					}
//					if(zal.getDkzKwota().equals(0.0)) {
//						zal.setDkzSrodkiWlasne(0.0);
//					}
//
//					if(zal.getDkzKwota() < 0) {
//						zal.setDkzZalWydano(sumyWgWalutKalkRoz.get(z.getDkzWalId()));
//						zal.setDkzSrodkiWlasne(0.0);
//                    }
//					// tukej2
//					zal.setDkzKartaKwota(0.0);
//					kalkulacjaRozliczenia.addDeltZaliczki(zal);
//				}
//			}
//
//			List<Long> listaWalutPobranychZaliczek = kopiaKalkWst.getDelZaliczki().stream().map(zal -> zal.getDkzWalId()).collect(Collectors.toList());
//			List<Long> noweWaluty=new ArrayList<Long>();
//			noweWaluty.addAll(sumyWgWalutKalkRoz.keySet());
//			noweWaluty.removeAll(listaWalutPobranychZaliczek);
//
//			if(noweWaluty.size() > 0) {
//				for(Long wal : noweWaluty) {
//					DelZaliczki zal = new DelZaliczki();
//					zal.setDkzWalId(wal);
//					zal.setDkzKwota(sumyWgWalutKalkRoz.get(wal));
//
//					zal.setDkzZalWydano(0.0);
//					zal.setDkzKartaKwota(0.0);
//					zal.setDkzSrodkiWlasne(zal.getDkzKwota());
//					kalkulacjaRozliczenia.addDeltZaliczki(zal);
//				}
//
//
//			}
//
//		} else {
//			for(Long wal : sumyWgWalutKalkRoz.keySet()) {
//
//				DelZaliczki zal = new DelZaliczki();
//				zal.setDkzWalId(wal);
//				zal.setDkzKwota(sumyWgWalutKalkRoz.get(wal));
//
//				zal.setDkzZalWydano(0.0);
//				zal.setDkzKartaKwota(0.0);
//				zal.setDkzSrodkiWlasne(zal.getDkzKwota());
//				kalkulacjaRozliczenia.addDeltZaliczki(zal);
//			}
//		}
//	}
	
	
	
//	public Double obliczRozniceRoz(Long walID, Double wartoscPLN) {
//		Double ret = 0.0;
//		for(DelZaliczki z : kalkulacjaWstepna.getDelZaliczki()) {
//			if(z.getDkzWalId().equals(walID)) {
//				Double kwotaZalPLN = kursPrzelicznikSprzedazyNBP(walID, z.getDkzDataWyp()) * z.getDkzKwota();
//				ret = wartoscPLN - kwotaZalPLN;
//			}
//		}
//		return ret;
//	}
	
	
	
	@PostConstruct
	public void init() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		if (id != null) {
			this.setWniosek(wczytajWniosek(Long.parseLong("" + id)));
//			naprawEncje();
		} else {
			this.setWniosek( newWniosek() );
		}
		
		if (kalkulacjaWniosek==null){
			kalkulacjaWniosek = addNewKalkulacje("WNK");
			kalkulacjaWniosek.setSqlBean(this);
		}
		
		if (kalkulacjaWstepna==null){
			kalkulacjaWstepna = addNewKalkulacje("WST");
			kalkulacjaWstepna.setSqlBean(this);
		}
		
		if (kalkulacjaRozliczenia==null){
			kalkulacjaRozliczenia = addNewKalkulacje("ROZ");
			kalkulacjaRozliczenia.setSqlBean(this);
		}
		
		this.wndWndId = wniosek.getWndWndId();
				
		this.wczytajListyWartDel();
		this.wczytajSlownikiDelegacje();
		
		this.sprawdzStawki();
		
//		this.przeliczFiltryDomyslnegoZF();
		
//		this.setCzyDietaHotelowa(true);
		
	}
	
	private void sprawdzStawki(){
		
		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_STAWKI");
		DataRow r = lw.get(this.wniosek.getWndStdId());
		if( r != null) {
			String nazwaStawki = (String)r.get("STD_NAZWA_STAWKI");
			setBlokujRDKM(!"mpips".equals(nazwaStawki.toLowerCase()));
		}
	}

	private void wczytajListyWartDel() {
		this.getLW("PPL_KRAJE_WALUTY_ALL");
		this.getLW("PPL_DEL_KONTA_PRC", this.wniosek.getWndPrcId());
	}

	private void wczytajSlownikiDelegacje() {
		this.getSlownik("DEL_FORMY_WYPLATY");
		this.getSlownik("DEL_ZAPRASZAJACY");
		this.wniosek.getSLDelSrodekLokomocji();
		this.getSlownik("DEL_FORMY_PLATNOSCI");
	}
	
	public void ustawReferencjeKalkulacji(){
		Optional<DeltKalkulacje> findFirst = this.wniosek.getDeltKalkulacjes().stream().filter(k->"WNK".equals( (""+ k.getKalRodzaj()).toUpperCase() ) ).findFirst();
		if (findFirst.isPresent()){
			this.kalkulacjaWniosek = findFirst.get();
			this.kalkulacjaWniosek.setSqlBean(this);
		}
		
		Optional<DeltKalkulacje> findFirst2 = this.wniosek.getDeltKalkulacjes().stream().filter(k->"WST".equals( (""+ k.getKalRodzaj()).toUpperCase() ) ).findFirst();
		if (findFirst2.isPresent()){
			this.kalkulacjaWstepna = findFirst2.get();
			this.kalkulacjaWstepna.setSqlBean(this);
		}
		
		Optional<DeltKalkulacje> findFirst3 = this.wniosek.getDeltKalkulacjes().stream().filter(k->"ROZ".equals( (""+ k.getKalRodzaj()).toUpperCase() ) ).findFirst();
		if (findFirst3.isPresent()){
			this.kalkulacjaRozliczenia = findFirst3.get();
			this.kalkulacjaRozliczenia.setSqlBean(this);
		}
	}
	
	
//	public Double kursPrzelicznikSprzedazyNBP( Object waluta, Date naDzien ){
//		return Utils.kursPrzelicznikSprzedazyNBP(waluta, naDzien);
////		Double ret = null;
////
////		if(naDzien == null)
////			naDzien = today();
////
////		ArrayList<DataRow> kursyWalut = this.getLW("PPL_DEL_KURS_SPRZEDAZY").getData();
////
////
////
////		if(kursyWalut != null) {
////			for (DataRow r : kursyWalut) {
////				if (! waluta.toString().equals( r.get("KW_WAL_Z_ID").toString()))
////					continue;
////
////				if (! naDzien.after( (Date) r.get("KW_DATA") ) && !naDzien.equals(r.get("KW_DATA")))
////					continue;
////				ret = ((BigDecimal) r.get("KW_PRZELICZNIK_SPRZEDAZY")).doubleValue();
////				break;
////			}
////		}
////		if (ret==null)
////			ret = 1.0;
////
////		return ret;
//	}
	
	
//	public DataRow kursPrzelicznikSprzedazyNBPRow( Object walId, Date naDzien ){
//		return Utils.kursPrzelicznikSprzedazyNBPRow(walId, naDzien);
//
////		DataRow ret = null;
////
////		if(naDzien == null)
////			naDzien = today();
////
////		ArrayList<DataRow> kursyWalut = this.getLW("PPL_DEL_KURS_SPRZEDAZY").getData();
////
////		if(kursyWalut != null) {
////			for (DataRow r : kursyWalut) {
////
////				if (! (""+walId).equals( r.get("KW_WAL_Z_ID").toString()))
////					continue;
////
////				if (! naDzien.after( (Date) r.get("KW_DATA") ) && !naDzien.equals(r.get("KW_DATA")))
////					continue;
////
////                ret = r;
////				break;
////			}
////		}
////
////		if (ret==null)
////			System.out.println();
////
////		return ret;
//	}
	
	
//	public Double kursPrzelicznikSredniNBP( Object waluta, Date naDzien ){
//		return Utils.kursPrzelicznikSredniNBP(waluta, naDzien);
//	}

//	public DataRow kursPrzelicznikSredniNBPRow( Object walId, Date naDzien ){
//
//		DataRow ret = null;
//
//		if(naDzien == null)
//			naDzien = today();
//
//		ArrayList<DataRow> kursyWalut = this.getLW("PPL_DEL_KURS_SREDNI").getData();
//
//		if(kursyWalut != null) {
//			for (DataRow r : kursyWalut) {
//
//				if (! (""+walId).equals( r.get("KW_WAL_Z_ID").toString()))
//					continue;
//
//				if (! naDzien.after( (Date) r.get("KW_DATA") ) && !naDzien.equals(r.get("KW_DATA")))
//					continue;
//
//				ret = r;
//				break;
//			}
//		}
//
//		if (ret==null)
//			System.out.println();
//
//		return ret;
//	}
	
//	private void naprawEncje() {
//		if ("T".equals(wniosek.getWndFBezkosztowa())) {
//			if (kalkulacjaWniosek != null && kalkulacjaWniosek.getDeltPozycjeKalkulacjis() != null) {
//				for (Object p : kalkulacjaWniosek.getDeltPozycjeKalkulacjis().toArray())
//					kalkulacjaWniosek.removeDeltPozycjeKalkulacji((DeltPozycjeKalkulacji) p);
//			}
//		}
//
//		if (wniosek.getDeltSrodkiLokomocjis().isEmpty()) {
//			wniosek.addDeltSrodkiLokomocji(new DeltSrodkiLokomocji());
//			User.warn("Odtworzono brakujący środek lokomocji (uzupełnij dane).");
//		}
//
//	}

//	public static void addZF2Pkl(DeltPozycjeKalkulacji pkl) {
//		
//		DeltZrodlaFinansowania zf = new DeltZrodlaFinansowania();
//		pkl.addDeltZrodlaFinansowania(zf);
//		zf.setPkzfKwota(0.0);
//		zf.setPkzfProcent(100.0);
//		zf.setPkzfOpis("zf nr " + (pkl.getDeltZrodlaFinansowanias().size() + 1));
//		
//	}

//	public void addZF2PklDlg(DeltPozycjeKalkulacji pkl) {
//		DeltZrodlaFinansowania zf = new DeltZrodlaFinansowania();
//
//		Double procentZf = 0.0;
//		for(DeltZrodlaFinansowania zfin : pkl.getDeltZrodlaFinansowanias()) {
//			procentZf += zfin.getPkzfProcent();
//		}
//		zf.setPkzfProcent(100.0 - procentZf);
//		if(pkl.getPklKwotaMf() != null)
//			zf.setPkzfKwota(procentNaKwote(zf.getPkzfProcent(), pkl.getPklKwotaMf()));
//		else zf.setPkzfKwota(0.0);
//
//		zf.setPkzfOpis("zf nr " + (pkl.getDeltZrodlaFinansowanias().size() + 1));
//		dodajAngazDoZf(zf);
//
//		pkl.addDeltZrodlaFinansowania(zf);
//
//
//	}
	
//	public void dodajAngazDoZf(DeltZrodlaFinansowania zf) {
//		Double procentAngaz = 0.0;
//		for(DelAngaze an : zf.getDelAngazes()) {
//			procentAngaz += an.getAngProcent();
//		}
//		DelAngaze nowyAngaz = new DelAngaze();
//		zf.addDelAngaze(nowyAngaz);
//		nowyAngaz.setAngProcent(100.0 - procentAngaz);
//		nowyAngaz.setAngKwota(procentNaKwote(nowyAngaz.getAngProcent(), nz(zf.getPkzfKwota())));
//
//	}
	
	public void showDlgZF4CurrentPklDlg(DeltPozycjeKalkulacji pkl) {
		currentPkl = pkl;
		com.comarch.egeria.Utils.update("dlgZrFinId");
		Utils.executeScript("PFbyId('dlgZrFinId').show();");
	}

//	public void copyZF2AllPkl(DeltPozycjeKalkulacji pkl) {
//		String txt = "";
//		for (DeltPozycjeKalkulacji p : pkl.getDeltKalkulacje().getDeltPozycjeKalkulacjis()) {
//			if (p.equals(pkl))
//				continue;
//
//			for (Object z : p.getDeltZrodlaFinansowanias().toArray())
//				p.removeDeltZrodlaFinansowania((DeltZrodlaFinansowania) z);
//
//			for (DeltZrodlaFinansowania z : pkl.getDeltZrodlaFinansowanias()) {
//				if(z.getPkzfSkId() == null) {
//					User.alert("Próba skopiowania źródła finansownaia bez podanego kodu do pozostąłyc pozycji, popraw dane");
//					return;
//				}
//				p.addDeltZrodlaFinansowania(z.clone(false));
//			}
//
//			txt += String.format("p. '%1$s'; <br/>",
//					this.getSql("PPL_DEL_TYPY_POZYCJI_Z").findAndFormat(p.getPklTpozId(), "%2$s"));
//		}
//
//		if (txt.length() > 0)
//
//		User.info("Skopiowano źródła finasowania");
//	}



//	metoda zwracająca kategorię pozycji po podaniu id pozycji i wtedy porównanie kategorii i kopiowanie źródeł finansowania tylko dla pozycji o tych samych kategoriach
//	public void copyZFByTypPkl(DeltPozycjeKalkulacji p, List<DeltPozycjeKalkulacji> pkl) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
//
//			String txt = "";
//			List<DeltZrodlaFinansowania> kopiowaneZf = new ArrayList<>();
//
//			for(DeltZrodlaFinansowania z : p.getDeltZrodlaFinansowanias()) {
//				if(!z.getDelAngazes().isEmpty())
//					kopiowaneZf.add(z.clone(true));
//				  else
//					kopiowaneZf.add(z.clone(false));
//			}
//
//			for (DeltPozycjeKalkulacji poz : pkl) {
//				if(p.equals(poz))
//					continue;
//
//				for (Object z : poz.getDeltZrodlaFinansowanias().toArray())
//					poz.removeDeltZrodlaFinansowania((DeltZrodlaFinansowania) z);
//
//				for(DeltZrodlaFinansowania kZf : kopiowaneZf) {
//					if(kZf.getPkzfSkId() == null) {
//						User.alert("Próba skopiowania źródła finansownania bez podanego kodu do pozostałych pozycji, popraw dane");
//						return;
//					}
//					kZf.setPkzfKwota(procentNaKwote(kZf.getPkzfProcent(), poz.getPklKwotaMf()));
//					if(!kZf.getDelAngazes().isEmpty()) {
//						for(DelAngaze a : kZf.getDelAngazes()) {
//							a.setAngKwota(procentNaKwote(a.getAngProcent(),kZf.getPkzfKwota()));
//						}
//					}
//
//					poz.addDeltZrodlaFinansowania(kZf.clone(true));
//				}
//
//
//				if(!poz.getDeltZrodlaFinansowanias().isEmpty()){
//					for(DeltZrodlaFinansowania z : poz.getDeltZrodlaFinansowanias()){
//						z.setPkzfKwota(z.getPkzfKwota());
//						if(!z.getDelAngazes().isEmpty()){
//							for(DelAngaze a : z.getDelAngazes()){
//								a.setAngKwota(a.getAngKwota());
//							}
//						}
//					}
//				}
//
//				txt += String.format("p. '%1$s'; <br/>",
//						this.getSql("PPL_DEL_TYPY_POZYCJI_Z").findAndFormat(p.getPklTpozId(), "%2$s"));
//			}
//			if (txt.length() > 0)
//			User.info("Skopiowano źródła finasowania");
//	}

	
public void generujDiety(String ktgPozycji, DeltKalkulacje kalk) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		if(kalk.getDeltTrasies().isEmpty())
			return;
		
		List<Long> listaCelow = this.wniosek.getDeltCeleDelegacjis().stream()
				.map(cel -> cel.getCdelKrId()).collect(Collectors.toList()); // TODO dokończ
		
		Long tpozId = 0L;
		if("KM".equals(ktgPozycji) || "RD".equals(ktgPozycji)) {
			tpozId = getTpozIdByKtgPozycji(ktgPozycji); 
		} else {
			tpozId = getTpozIdByKtgDiety(ktgPozycji);
		}
		
		if(eq("H",ktgPozycji) || eq("RH", ktgPozycji)) {
			usunPozycjeKalkulacjiWgKtg("H", kalk);
			usunPozycjeKalkulacjiWgKtg("RH", kalk);
		}else 
			usunPozycjeKalkulacjiWgKtg(ktgPozycji, kalk);
		
		Long krId = null;
		Date dtKrOd = null;
		Date dtKrDo = null;
		String miejscowosc = null;
		
		Long lp = 1L;
		// dodaje diety wg stanu trasy
		for (DeltTrasy t : kalk.getDeltTrasies()) {
			if (krId == null) {
				miejscowosc = t.getTrsMiejscowoscDo();
				krId = t.getTrsKrIdDiety();
				dtKrOd = t.getTrsDataOd();
				dtKrDo = t.getTrsDataDo() != null ? t.getTrsDataDo() : dtKrOd;
			}
			
			if(!listaCelow.contains(t.getTrsKrIdOd())) {
				continue;
			}
			krId = t.getTrsKrIdOd();
			//TODO test czy teraz dobrze działa !!!
//			listaCelow.remove(t.getTrsKrIdOd());
			 if(krId == null) { 
	                User.alert("Brak informacji o wszystkich Krajach delegacji na planie trasy, popraw dane"); 
	                return; 
	         }

			if (t.getTrsDataDo() != null && t.getTrsDataDo().after(dtKrDo))
				dtKrDo = t.getTrsDataDo();

			if (!krId.equals(t.getTrsKrIdDiety())) {// zmiana kraju...
				miejscowosc = t.getTrsMiejscowoscOd();
				
				if(t.getTrsDataOd() != null)
				dtKrDo = t.getTrsDataOd();
				
				if(dtKrOd == null || dtKrDo == null) {
					User.alert("Nie uzupełniono wymaganych dat na planie trasy, popraw dane");
					return;
				}
				
				DeltPozycjeKalkulacji o = newPozycjaKalkulacji(kalk.getKalRodzaj());//kalk.getKalRodzaj()
				
				if ("P".equals(ktgPozycji)) 
					przeliczDodajPKL_Pobytowa(tpozId, o, krId, dtKrOd, dtKrDo, kalk);
				
				else if("RH".equals(ktgPozycji)||"H".equals(ktgPozycji)) {
					o.setPklOpis(miejscowosc);
					przeliczDodajPKL_Hotelowa(tpozId, o, krId, dtKrOd, dtKrDo, kalk);
				}
				
				else if("KM".equals(ktgPozycji))
					przeliczDodajRyczalt_Komunikacyjny(tpozId, o, krId, dtKrOd, dtKrDo, kalk);
				
				else if("RD".equals(ktgPozycji))
					przeliczDodajRyczalt_Dojazdowy(tpozId, o, krId, kalk);
				
				dtKrOd = dtKrDo;
			}
			
		}

	kalk.bilansujDiety(ktgPozycji, zwrocTypStawki().equals("FCS"));
			
		if("KM".equals(ktgPozycji))
			kalk.bilansujRyczaltKM();

	}


	private String zwrocTypStawki() {
		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_STAWKI");
			DataRow r = lw.get(wniosek.getWndStdId());
			String nazwaStawki = "mpips";
			
			if( r != null) {
				nazwaStawki = (String)r.get("STD_NAZWA_STAWKI");
			}
			return nazwaStawki;
	}
	
	public void generujDiety_CS(String ktgPozycji, DeltKalkulacje kalk) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		Long tpozId = 0L;
		String miejscowosc = null;
		
		if("KM".equals(ktgPozycji) || "RD".equals(ktgPozycji)) {
			tpozId = getTpozIdByKtgPozycji(ktgPozycji); 
		} else {
			tpozId = getTpozIdByKtgDiety(ktgPozycji);
		}
		
		if(eq("H",ktgPozycji) || eq("RH", ktgPozycji)) {
			usunPozycjeKalkulacjiWgKtg("H", kalk);
			usunPozycjeKalkulacjiWgKtg("RH", kalk);
		}else 
			usunPozycjeKalkulacjiWgKtg(ktgPozycji, kalk);
		
		Map<Long, Double> listaKrajCzas = kalk.getDelCzynnosciSluzbowe().stream().collect(Collectors.groupingBy(cs -> cs.getCzynKrajId(), Collectors.summingDouble(cs -> cs.getCzynCzas())));
		
		
		if("RH".equals(ktgPozycji)||"H".equals(ktgPozycji)) {
			for(DelCzynnosciSluzbowe cs : kalk.getDelCzynnosciSluzbowe()) {
				DeltPozycjeKalkulacji o = newPozycjaKalkulacji(kalk.getKalRodzaj());
				o.setPklTpozId(tpozId);
				o.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(o.getPklTpozId()));
				
				List<DeltTrasy> trasy = kalk.getDeltTrasies().stream().filter(trs -> trs.getTrsKrIdOd().equals(cs.getCzynKrajId())).collect(Collectors.toList());
				if(!trasy.isEmpty()) {
					miejscowosc = trasy.get(0).getTrsMiejscowoscOd();
					if(miejscowosc != null)
					o.setPklOpis(miejscowosc);
				}
				
				przeliczDodajPKL_Hotelowa(tpozId, o, cs.getCzynKrajId(), cs.getCzynCzynnosciOd(), cs.getCzynCzynnosciDo(), kalk);
			}
		} else {
		
			for(Long krajId : listaKrajCzas.keySet()) {
				
				DeltPozycjeKalkulacji o = newPozycjaKalkulacji(kalk.getKalRodzaj());
				o.setPklTpozId(tpozId);
				o.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(o.getPklTpozId()));
				
				if("P".equals(ktgPozycji))
				przeliczDodajPKL_Pobytowa_CS(tpozId, o, krajId, listaKrajCzas.get(krajId), kalk);
				
	//			if("RH".equals(ktgPozycji)||"H".equals(ktgPozycji)) {
	//				List<DeltTrasy> trasy = kalk.getDeltTrasies().stream().filter(trs -> trs.getTrsKrIdOd().equals(krajId)).collect(Collectors.toList());
	//				if(!trasy.isEmpty()) {
	//					miejscowosc = trasy.get(0).getTrsMiejscowoscOd();
	//					if(miejscowosc != null)
	//					o.setPklOpis(miejscowosc);
	//				}
	//				przeliczDodajPKL_Hotelowa_CS(tpozId, o, krajId, listaKrajCzas.get(krajId), kalk);
	//			}
				if("RD".equals(ktgPozycji))
				przeliczDodajRyczalt_Dojazdowy(tpozId, o, krajId, kalk); //uwaga
				
				if("KM".equals(ktgPozycji))
				przeliczDodajRyczalt_Komunikacyjny_CS(tpozId, o, krajId, listaKrajCzas.get(krajId), kalk);
			}
		}

		kalk.bilansujDiety(ktgPozycji, zwrocTypStawki().equals("FCS"));
		
		if("KM".equals(ktgPozycji))
			kalk.bilansujRyczaltKM();
		
	}



	
	private void przeliczDodajPKL_Pobytowa_CS(long tpozId, DeltPozycjeKalkulacji o, Long krId, Double czas, DeltKalkulacje kalk) {
		
		//o.setDeltKalkulacje(kalkulacjaWstepna);
		o.setPklKrId(krId);
		ustawZFAngaze(o);
		if(!zwrocTypStawki().equals("FCS"))
			o.setPklIlosc(ileDietZagranicznych_CS(czas));
		else {
			o.setPklIlosc(ileDietZagranicznych_FCS(czas));
		}
//		o.setPklKosztMFProcent(this.zwrocProcentKosztMF(o.getPklTpozId()));
		o.setPklRefundacjaProcent(this.zwrocProcentRefundacji(o.getPklTpozId()));
		
//		o.setPklKwotaMf(this.ustalKosztMf(o));
		o.setPklRefundacjaKwota(this.ustalKwoteRefundacji(o));
		
//		aktualizujKwotyZF(o);
		
		//kalk.addDeltPozycjeKalkulacji(o);
		if (o.getPklIlosc() <= 0) 
			kalk.removeDeltPozycjeKalkulacji(o);
	}
	
//	private void przeliczDodajPKL_Hotelowa_CS(long tpozId, DeltPozycjeKalkulacji o, Long krId, Double czas, DeltKalkulacje kalk) {
//
//		//o.setDeltKalkulacje(kalk);
//		o.setPklTpozId(tpozId);
//		o.setPklKrId(krId);
//		ustawZFAngaze(o);
//		o.setPklIlosc(ileDietHotelowych_CS(czas));
////		o.setPklKosztMFProcent(this.zwrocProcentKosztMF(o.getPklTpozId()));
//		o.setPklRefundacjaProcent(this.zwrocProcentRefundacji(o.getPklTpozId()));
//
////		o.setPklKwotaMf(this.ustalKosztMf(o));
//		o.setPklRefundacjaKwota(this.ustalKwoteRefundacji(o));
//
////		aktualizujKwotyZF(o);
//
//		//kalk.addDeltPozycjeKalkulacji(o);
//		if (o.getPklIlosc() <= 0)
//			kalk.removeDeltPozycjeKalkulacji(o);
//	}
	
	private void przeliczDodajRyczalt_Komunikacyjny_CS(Long tpozId, DeltPozycjeKalkulacji o, Long krId, Double czas, DeltKalkulacje kalk) {
		
		o.setPklTpozId(tpozId);
		o.setPklKrId(krId);
		ustawZFAngaze(o);
		o.setPklIlosc(ileDniRyczaltKom_CS(czas));
//		o.setPklKosztMFProcent(this.zwrocProcentKosztMF(o.getPklTpozId()));
		o.setPklRefundacjaProcent(this.zwrocProcentRefundacji(o.getPklTpozId()));
		
//		ustawStawkeiWaluteDietyZagr(o, wniosek.getWndDataPowrotu());
//		o.setPklStawka(o.getPklStawka()*0.1);
//		o.setPklKwotaMf(this.ustalKosztMf(o));
		o.setPklRefundacjaKwota(this.ustalKwoteRefundacji(o));
		
//		aktualizujKwotyZF(o);
		
		//if(o.getPklIlosc() != 0)
		//kalk.addDeltPozycjeKalkulacji(o);
	}
	
	
	public void wyznaczKrajeCzynnosciSluzbowych(DeltKalkulacje kalk) {
		
		if(kalk.getDeltTrasies().isEmpty() || kalk.getDeltTrasies() == null) {
			User.info("Brak danych w planie trasy, wygeneruj/wprowadź najpierw plan trasy lub wprowadź terminy czynności slużbowych ręcznie");
			return;
		}
		
		if(kalk.getDeltTrasies().size()<2) {
			User.info("Plan trasy musi mieć co najmniej dwie pozycje");
			return;
		}
		
		if(kalk.getDeltTrasies().get(0).getTrsDataOd() == null || kalk.getDeltTrasies().get(kalk.getDeltTrasies().size() - 1).getTrsDataDo() == null) {
			User.info("Data wyjazdu oraz przyjazdu musi być uzupełniona w planie trasy");
			return;
		}
		

		
		Date dataPomocnicza = null; // data poprzedniego wiersza od
		kalk.getDelCzynnosciSluzbowe().clear();
		listaKrajCzasPrywaty.clear();
		double hours = 0.0;
		Double godzinyPomocniczo = 0.0;
		
		Date ostatniaData = (Date) kalk.getDeltTrasies().get(kalk.getDeltTrasies().size() - 1).getTrsDataDo().clone();
		
		for(DeltTrasy trs : kalk.getDeltTrasies()) {
			DelCzynnosciSluzbowe czs = new DelCzynnosciSluzbowe(); 
			
			if(dataPomocnicza != null) {
				czs.setCzynKrajId(trs.getTrsKrIdOd());
				czs.setCzynCzynnosciOd(dataPomocnicza);
				
				czs.setCzynCzynnosciDo(trs.getTrsDataOd());
				
				if(godzinyPomocniczo != null) {
					czs.setCzynCzasPrywatny(godzinyPomocniczo);
					godzinyPomocniczo = null;	
				} else
					czs.setCzynCzasPrywatny(hours);
				
				if(trs.getTrsCzynnosciSluzboweOd() != null && trs.getTrsCzynnosciSluzboweDo() !=null) { // dodaj czas prywaty
					hours = (0.000 + trs.getTrsCzynnosciSluzboweDo().getTime() - trs.getTrsCzynnosciSluzboweOd().getTime()) / (1000 * 60 * 60);
					getListaKrajCzasPrywaty().put(trs.getTrsKrIdDiety(), hours);
				} else 
					hours = 0.0;
				
				dataPomocnicza = null;
				
				kalk.addDelCzynnosciSluzbowe(czs);
				
				dataPomocnicza = trs.getTrsDataOd();
			} else {
				dataPomocnicza = trs.getTrsDataOd();
				if(trs.getTrsCzynnosciSluzboweOd() != null && trs.getTrsCzynnosciSluzboweDo() !=null) { // dodaj czas prywaty
					godzinyPomocniczo = (0.000 + trs.getTrsCzynnosciSluzboweDo().getTime() - trs.getTrsCzynnosciSluzboweOd().getTime()) / (1000 * 60 * 60);
					getListaKrajCzasPrywaty().put(trs.getTrsKrIdDiety(), godzinyPomocniczo);
				}
				
			}
		}
		
		kalk.getDelCzynnosciSluzbowe().get(kalk.getDelCzynnosciSluzbowe().size() -1).setCzynCzynnosciDo(ostatniaData);
	}
	
	public List<DeltZrodlaFinansowania> skopiujZF(Long tpozId) {
		
		
		List<DeltZrodlaFinansowania> listaZFDoSkopiowania = new ArrayList<>();
		if(tpozId != null)
		if(!kalkulacjaWniosek.getDeltPozycjeKalkulacjis().stream().filter(pkl -> tpozId.equals(pkl.getPklTpozId())).collect(Collectors.toList()).isEmpty()) {
			List<DeltZrodlaFinansowania> listaZF = kalkulacjaWniosek.getDeltPozycjeKalkulacjis().stream().filter(pkl -> tpozId.equals(pkl.getPklTpozId())).collect(Collectors.toList()).get(0).getDeltZrodlaFinansowanias();
	
			if(!listaZF.isEmpty()) {
				for(DeltZrodlaFinansowania zf : listaZF) {
					if(zf.getPkzfSkId()!=null && zf.getPkzfProcent()!=null){
					DeltZrodlaFinansowania z = new DeltZrodlaFinansowania();
					
					if(zf.getDelAngazes().size()>0)
					z = zf.clone(true);
					else z = zf.clone();
					
					listaZFDoSkopiowania.add(z);
					}
				}
			}
		}
		return listaZFDoSkopiowania;
		
	}
	

	HashMap<String, Long> pozKtgHM = new HashMap<>();
	public Long getTpozIdByKtgDiety(String ktgPozycji)
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		
		//HashMap<String, Long> pozKtgHM = new HashMap<>();
		
		Long ret = pozKtgHM.get(ktgPozycji) ; //= 5L;
		
		if (ret!=null)
			return ret;
		
		
		Long wndStdId = this.wniosek.getWndStdId();
		Object oTpozId = this.execScalarQuery("select TPOZ_ID from PPT_DEL_TYPY_POZYCJI where TPOZ_STD_ID = ? and TPOZ_KATEGORIA = ? ",  
							wndStdId, ktgPozycji);
		if(wndStdId == null){
			User.alert("Wymagany jest typ stawki");
			ret = 0L;
		} else if (oTpozId==null){
			User.alert("Nie zdefiniowano typu pozycj z kat. %1$s i dla podanego typu stawki",  ktgPozycji);
			ret = 0L;
		} else {
			ret = Long.parseLong(""+oTpozId);
		}
		
		
		pozKtgHM.put(ktgPozycji, ret);
		return ret;
	}
	
	public Long getTpozIdByKtgPozycji( String ktgPozycji) {
		
		if(ktgPozycji == null || ktgPozycji.isEmpty())
			return 0L;
		BigDecimal ret = null;
		
		SqlDataSelectionsHandler typyPozycji = this.wniosek.getLwTypyPozycji();// this.getLW("PPL_DEL_TYPY_POZYCJI_Z");
		if(typyPozycji != null) {
			for( DataRow r : typyPozycji.getData()) {
				if(r.get("tpoz_kategoria").equals(ktgPozycji)) {
					ret = (BigDecimal) r.get("tpoz_id");
					if(ret == null) {
						return 0L;
					}
				}

			}
		}
		if(ret != null)
		return ret.longValue();
		else 
			return null;
	}

//	public void przeliczFiltryDomyslnegoZF() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
//		SqlDataSelectionsHandler zrodlaFinLW = this.getLW("PPL_DEL_ZRODLA_FIN");//WB: nie ustawiac (na razie)
//
//		zrodlaFinLW.clearFilter();
////		if("T".equals(wniosek.getWndFProjektowa())) {
////			zrodlaFinLW.setColFilter("par", "442?", false);//zagraniczne
////			zrodlaFinLW.setColFilter("prj", this.wniosek.getWndProjKod(), false);
////		} else {
//		String wndFCzySzkolenie = wniosek.getWndFCzySzkolenie();
//		if (wndFCzySzkolenie==null) wndFCzySzkolenie = "N";
//		SqlDataSelectionsHandler lwPrefiltrZF = this.getLW("PPL_DEL_PREFILTR_ZF", "Z", wndFCzySzkolenie, wniosek.getWndProjKod());
//		for (DataRow r : lwPrefiltrZF.getData()) {
//			zrodlaFinLW.setColFilter((String)r.get("segment"), nz((String) r.get("colfilter")), false);
//		}
////		}
//
//
////		if("T".equals(wniosek.getWndFCzySzkolenie())) {
////			zrodlaFinLW.setColFilter("par", "4550||4700", false); // test fest
////			zrodlaFinLW.setColFilter("roz", "75001", false);//???
////		}
////
////		else if("T".equals(wniosek.getWndFProjektowa())) {
////
////			//ŹLE?!!!! - execScalarQuery nie sprawdza ile wiereszy zwr. sql ... tu jest chyba więcej problemów
////			//Object zfKod = this.execScalarQuery("select sk_kod from EGADM1.wdrv_css_stanowiska_kosztow where sk_kod like 'WY%' and prj = ?", this.wniosek.getWndProjKod());
////			//zrodlaFinLW.setColFilter("sk_kod", (String)zfKod, false);
////
////			//zrodlaFinLW.setColFilter("par", "441?", false);//krajowe
////			zrodlaFinLW.setColFilter("par", "442?", false);//zagraniczne
////			zrodlaFinLW.setColFilter("prj", this.wniosek.getWndProjKod(), false);
////			//TODO ustawić paragraf i komórkę merytoryczną według maila Witka
////		} else {
////			zrodlaFinLW.setColFilter("par", "4420", false);//WB: ok - ustawaić
////			//TODO funkcja Witka z maila z dziś
////
////			//String departament = zwrocDepartamentPrc();
////			//zrodlaFinLW.setColFilter("dep", departament, false);//WB: ustawiac w krajowych, a w zagraniczych chyba nie
////			//zrodlaFinLW.setColFilter("roz", "75001", false);//WB: nie ustawiac (na razie)
////		}
//
//		ustawDomyslneZaangazowanieSrodkow(zrodlaFinLW);
//
//	}

	
//	private String zwrocDepartamentPrc(){
//		DataRow prc = getLW("PRC").find(wniosek.getWndPrcId());
//		if (prc==null) return "";
//
//		DataRow rDepartamet = getLW("OB").find(prc.get("zat_ob_id"));
//		if (rDepartamet==null) return "";
//
//		return nz( (String) rDepartamet.get("ob_kod") );
//	}

	//TODO wywalic
	public void ustawDomyslneZaangazowanieSrodkow(SqlDataSelectionsHandler zrodlaFinLW) {	
		if(wniosek.getWndSkId() != null) {
			DataRow r = zrodlaFinLW.get(wniosek.getWndSkId());
			if(r != null) {
				String kodBudzetu = (String) r.get("kod_skrocony");
				this.getLW("PPL_DEL_ZAANGAZOWANIE_ZF").setColFilter("sk_kod_skrocony", kodBudzetu, true);
			}
		} 
//		else {
//				this.getLW("PPL_DEL_ZAANGAZOWANIE_ZF").setColFilter("dep",zwrocDepartamentPrc(), false);
//		}
	}
	
	
	public String formatujDniCzynnosciSluzbowe(Double czas) {
		String ret = "";
		if(czas != null) {
			double dni = Math.floor((czas / 24));
			double x = czas - dni * 24;
			
			if(dni == 1.0)
				ret = ""+(int)dni+" dzień"+" "+round(x,2)+" h";
			else 
				ret = ""+(int)dni+" dni"+" "+round(x,2)+" h";
		}
		return ret;
	}

	
	private void przeliczDodajPKL_Pobytowa(long tpozId, DeltPozycjeKalkulacji o, Long krId, Date dtKrOd, Date dtKrDo, DeltKalkulacje kalk) {
		
//		kalk.addDeltPozycjeKalkulacji(o);
		//o.setDeltKalkulacje(kalkulacjaWstepna);
//		 o.getPklRefundacjaProcent();
		
		o.setPklTpozId(tpozId);
		o.setPklKrId(krId);
		ustawZFAngaze(o);
		
		o.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(o.getPklTpozId()));
//		o.setPklSniadania(0L);
//		o.setPklObiady(0L);
//		o.setPklKolacje(0L);
		if(!zwrocTypStawki().equals("FCS"))
			o.setPklIlosc(ileDietZagranicznych(dtKrOd, dtKrDo));
		else {
			double czas = (0.000 + dtKrDo.getTime() - dtKrOd.getTime()) / (1000 * 60 * 60);
			if(kalk.getDeltTrasies().get(0).getTrsDataOd().equals(dtKrOd) || kalk.getDeltTrasies().get(kalk.getDeltTrasies().size() - 1).getTrsDataDo().equals(dtKrDo)) {
				czas+=2.0;
			}
			
			o.setPklIlosc(ileDietZagranicznych_FCS(czas));
		}
//		o.setPklKosztMFProcent(this.zwrocProcentKosztMF(o.getPklTpozId()));
		o.setPklRefundacjaProcent(this.zwrocProcentRefundacji(o.getPklTpozId()));
		
		//ustawStawkeiWaluteDietyZagr(o, dtKrOd);
//		o.setPklKwotaMf(this.ustalKosztMf(o));
		o.setPklRefundacjaKwota(this.ustalKwoteRefundacji(o));
		

//		aktualizujKwotyZF(o);
		
		//kalk.addDeltPozycjeKalkulacji(o);
		if (o.getPklIlosc() <= 0) 
			kalk.removeDeltPozycjeKalkulacji(o);
		
//		if(o.getPklIlosc() != 0)

	}
	
	private void przeliczDodajPKL_Hotelowa(long tpozId, DeltPozycjeKalkulacji o, Long krId, Date dtKrOd, Date dtKrDo, DeltKalkulacje kalk) {
		
		//o.setDeltKalkulacje(kalk);
		o.setPklTpozId(tpozId);
		o.setPklKrId(krId);
		ustawZFAngaze(o);
		o.setPklIlosc(ileDietHotelowych(dtKrOd, dtKrDo));
		
		o.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(o.getPklTpozId()));
//		o.setPklKosztMFProcent(this.zwrocProcentKosztMF(o.getPklTpozId()));
		o.setPklRefundacjaProcent(this.zwrocProcentRefundacji(o.getPklTpozId()));
		
//		ustawStawkeiWaluteDietyHotel(o, dtKrOd);
//		o.setPklKwotaMf(this.ustalKosztMf(o));
		o.setPklRefundacjaKwota(this.ustalKwoteRefundacji(o));
		
//		aktualizujKwotyZF(o);
		
		//kalk.addDeltPozycjeKalkulacji(o);
		if (o.getPklIlosc() <= 0) 
			kalk.removeDeltPozycjeKalkulacji(o);
//		if(o.getPklIlosc() != 0)

	}
	
	private void przeliczDodajRyczalt_Komunikacyjny(Long tpozId, DeltPozycjeKalkulacji o, Long krId, Date dtKrOd,
			Date dtKrDo, DeltKalkulacje kalk) {
		
		o.setPklTpozId(tpozId);
		o.setPklKrId(krId);
		ustawZFAngaze(o);
		o.setPklIlosc(ileDniRyczaltKom(dtKrOd, dtKrDo));
		o.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(o.getPklTpozId()));
//		o.setPklKosztMFProcent(this.zwrocProcentKosztMF(o.getPklTpozId()));
		o.setPklRefundacjaProcent(this.zwrocProcentRefundacji(o.getPklTpozId()));
		
		
//		ustawStawkeiWaluteDietyZagr(o, dtKrOd);
//		o.setPklStawka(o.getPklStawka()*0.1);
//		o.setPklKwotaMf(this.ustalKosztMf(o));
		o.setPklRefundacjaKwota(this.ustalKwoteRefundacji(o));
		
//		aktualizujKwotyZF(o);
		
		if (o.getPklIlosc() <= 0) 
			kalk.removeDeltPozycjeKalkulacji(o);
		
		//if(o.getPklIlosc() != 0)
		//kalk.addDeltPozycjeKalkulacji(o);
	}
	
	private void przeliczDodajRyczalt_Dojazdowy(Long tpozId, DeltPozycjeKalkulacji o, Long krId, DeltKalkulacje kalk) {
		
		o.setPklTpozId(tpozId);
		o.setPklKrId(krId);
		ustawZFAngaze(o);
		if(krId == 1L) 
			o.setPklIlosc(0.0);
		else
			o.setPklIlosc(1.0);
		
		o.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(o.getPklTpozId()));
//		o.setPklKosztMFProcent(this.zwrocProcentKosztMF(o.getPklTpozId()));
		o.setPklRefundacjaProcent(this.zwrocProcentRefundacji(o.getPklTpozId()));
//		ustawStawkeiWaluteDietyZagr(o, dtKrOd);
//		o.setPklKwotaMf(this.ustalKosztMf(o));
		o.setPklRefundacjaKwota(this.ustalKwoteRefundacji(o));
		
//		aktualizujKwotyZF(o);
		
		if (o.getPklIlosc() <= 0) 
			kalk.removeDeltPozycjeKalkulacji(o);
	}
	

	
//	public Double ustalKosztMf(DeltPozycjeKalkulacji pkl) {
//		if(pkl.getPklKwota() != null)
////		if(pkl.getPklKosztMFProcent() != null) {
////		Double procentMF = pkl.getPklKosztMFProcent()/100.0;
//		return pkl.getPklKwota();
////		}
//		else return 0.0;
//	}
	
	private Double ustalKwoteRefundacji(DeltPozycjeKalkulacji pkl) {
		Double procentRefundacji = pkl.getPklRefundacjaProcent()/100.0;
		return procentRefundacji * pkl.getPklKwota();
	}
	
	private void usunPozycjeKalkulacjiWgKtg(String ktgPozycji, DeltKalkulacje kalk) {
		// kasuje ew. istniejace diety...
		HashSet<DeltPozycjeKalkulacji> rem = getPozycjeWgKtg(ktgPozycji, kalk);
		for (DeltPozycjeKalkulacji p : rem) 
//			if(p.getPklPklId() != null) {
//				p.setPklStawka(0.0);
//				p.setPklIlosc(0.0);
//				p.przeliczKwote();
//			} 
//	else 
		if(p.getDeltPklWyplNzs().isEmpty()) {
				kalk.removeDeltPozycjeKalkulacji(p);
			}
	}

	private HashSet<DeltPozycjeKalkulacji> getPozycjeWgKtg(String ktgPozycji, DeltKalkulacje kalk) {
		SqlDataSelectionsHandler lw = this.wniosek.getLwTypyPozycji();// this.getLW("PPL_DEL_TYPY_POZYCJI_Z");
		HashSet<DeltPozycjeKalkulacji> rem = new HashSet<DeltPozycjeKalkulacji>();
		for (DeltPozycjeKalkulacji pkl : kalk.getDeltPozycjeKalkulacjis()) {
			Long pklTpozId = pkl.getPklTpozId();
			DataRow r = lw.get(pklTpozId);
			if(r != null) {
				if (ktgPozycji.equals(r.get("TPOZ_KATEGORIA"))){
					rem.add(pkl);
				}
			}
		}
		return rem;
	}

	
	public static double ileDietZagranicznych(Date dtPobytKrajOd, Date dtPobytKrajDo) {
		
		
		if( dtPobytKrajOd == null || dtPobytKrajDo == null) {
			User.alert("Nie uzupełniono wymaganych dat na planie trasy, popraw dane");
			return 0.0;
		}
		
		double ret = 0;
		double hours = (0.000 + dtPobytKrajDo.getTime() - dtPobytKrajOd.getTime()) / (1000 * 60 * 60);
		double dni = Math.floor((hours / 24));

		double x = hours - dni * 24;

		ret = dni;
		if (x > 12.000)
			ret += 1.0000;
		else if (x >= 8.000)
			ret += 0.5000;
		else if (x > 0.000)
			ret += 0.3333;

		return ret;
	}
	
	public static double ileDietZagranicznych_CS(Double czas) {
		double ret = 0;
		double hours = czas;
		double dni = Math.floor((hours / 24));

		double x = hours - dni * 24;

		ret = dni;
		if (x > 12.000)
			ret += 1.0000;
		else if (x >= 8.000)
			ret += 0.5000;
		else if (x > 0.000)
			ret += 0.3333;

		return ret;
	}
	
	public static double ileDietZagranicznych_FCS(Double czas) {
		double ret = 0;
		double hours = czas;
		double dni = Math.floor((hours / 24));

		double x = hours - dni * 24;

		ret = dni;
		if (x > 12.000)
			ret += 1.0000;
		else if (x > 6.000)
			ret += 0.5000;

		return ret;
	}
	
//	public static double ileDietHotelowych_CS(Double czas) {
//		double hours = czas;
//		double dni = Math.floor((hours / 24));
//
//		return dni;
//	}
	
	public static double ileDniRyczaltKom_CS(Double czas) {
		double ret = 0;
		double hours = czas;
		double dni = Math.floor((hours / 24));
		
		double x = hours - dni * 24;

		ret = dni;
		if (x > 0)
			ret += 1.0000;

		return ret;
	}
		

//	public void ustawStawkeiWaluteDietyZagr(DeltPozycjeKalkulacji p, Date pobytOd) {
//
//		if(pobytOd == null)
//			pobytOd = today();
//
//		String krPkl = "" + p.getPklKrId();
//		String idInnaP="";
//		for (DataRow r : this.getLW("PPL_DEL_STAWKI_DIET_ZAGR", this.wniosek.getWndStdId()).getData()) {
//			String krIdWst = "" + r.get("WST_KR_ID");
//			String walIdWst = "" + r.get("WST_WAL_ID");
//			String kwota = "" + r.get("WST_KWOTA");
//
//			double stawka = Double.parseDouble(kwota);
//			if (eq(p.getKategoria(), "KM")){
//				stawka = stawka * 0.1;
//			}
//
//			Date obowiazujeOd = (Date) r.get("WST_OBOWIAZUJE_OD");
//			if (krPkl.equals(krIdWst) && pobytOd.after(obowiazujeOd)) {
////				System.out.println("-stawka-> " + kwota);
////				System.out.println("-waluta-> " + walIdWst);
//				p.setPklWalId(Long.parseLong(walIdWst));
//				p.setPklStawka(stawka);
//				przeliczKwote(p);
//				return;
//			}
//
//			if("null".equals(krIdWst))
//				idInnaP=r.getIdAsString();
//		}
//
//		DataRow innaP = this.getLW("PPL_DEL_STAWKI_DIET_ZAGR").get(idInnaP);
//		p.setPklStawka(Double.parseDouble(""+innaP.get("WST_KWOTA")));//
//		p.setPklWalId(Long.parseLong( ""+innaP.get("WST_WAL_ID") ));// EUR
//		przeliczKwote(p);
//	}
	
//
//	public void ustawStawkeiWaluteDietyHotel(DeltPozycjeKalkulacji p, Date pobytOd) {
//		String krPkl = "" + p.getPklKrId();
//		String idInnaH="";
//		//for (DataRow r : this.getLW("PPL_DEL_STAWKI_DIET_HOT", this.wniosek.getWndStdId()).getData()) {
//		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_KRAJE_STAWKI", this.wniosek.getWndStdId(), "H", pobytOd);
//		for (DataRow r : lw.getAllData()) {
//
//			String krIdWst = "" + r.get("KR_ID");
//			String walIdWst = "" + r.get("WST_WAL_ID");
////			Date obowiazujeOd = (Date) r.get("WST_OBOWIAZUJE_OD");
//			if (krPkl.equals(krIdWst)) {
//				String kwota = "" + r.get("WST_KWOTA");
//
//				double stawka = Double.parseDouble(kwota);
//				if (eq(p.getKategoria(), "RH")) {
//					stawka = stawka * 0.25;
//				}
////				 && pobytOd.after(obowiazujeOd)
//				p.setPklWalId(Long.parseLong(walIdWst));
//				p.setPklStawka(stawka);
//				przeliczKwote(p);
//				return;
//			}
//
//			if("null".equals(krIdWst))
//				idInnaH=r.getIdAsString();
//		}
//		this.getLW("PPL_DEL_STAWKI_DIET_HOT", this.wniosek.getWndStdId()).getData();
//		DataRow innaH = this.getLW("PPL_DEL_STAWKI_DIET_HOT").get(idInnaH);
//		p.setPklStawka(Double.parseDouble(""+innaH.get("WST_KWOTA")));//
//		p.setPklWalId(Long.parseLong( ""+innaH.get("WST_WAL_ID") ));// EUR
//		przeliczKwote(p);
//	}
	
//	public Double podajStawkeDiety(Long krId, Long stdId, String ktg) { tukej dalej
//		Double ret = 0.0;
//		
//		return ret;
//	}

	private double ileDietHotelowych(Date dtPobytKrajOd, Date dtPobytKrajDo) { // tukej potem sprawdź
		
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date dataOdFormatowana = null;
		Date dataDoFormatowana = null;
		try {
			dataOdFormatowana = formatter.parse(formatter.format(dtPobytKrajOd));
			dataDoFormatowana = formatter.parse(formatter.format(dtPobytKrajDo));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return Days.daysBetween(new DateTime(dataOdFormatowana), new DateTime(dataDoFormatowana)).getDays();
		
//		double hours = (0.000 + dtPobytKrajDo.getTime() - dtPobytKrajOd.getTime()) / (1000 * 60 * 60);
//		double dni = Math.floor((hours / 24));
//		double ileDniOgolnie = dtPobytKrajDo.getDate() - dtPobytKrajOd.getDate();
//		if(dni != ileDniOgolnie) {
//			return ileDniOgolnie;
//		}
//		return dni;

	}
	
	public static double ileDniRyczaltKom(Date dtPobytKrajOd, Date dtPobytKrajDo) {
		
		if(dtPobytKrajOd == null || dtPobytKrajDo == null) {
			User.alert("Nie uzupełniono wymaganych dat na planie trasy, popraw dane");
			return 0.0;
		}
		
		double ret = 0;
		double hours = (0.000 + dtPobytKrajDo.getTime() - dtPobytKrajOd.getTime()) / (1000 * 60 * 60);
		double dni = Math.floor((hours / 24));
		
		double x = hours - dni * 24;

		ret = dni;
		if (x > 0)
			ret += 1.0000;

		return ret;
	}

	public void przeliczKwote(DeltPozycjeKalkulacji p) {
//		Long pklIlosc =  ((Double) (nz(p.getPklIlosc())  * 10000)).longValue() ;
//		Long pklStawka = ((Double) (nz(p.getPklStawka()) * 10000)).longValue() ;
//
//		if (pklIlosc == null || pklIlosc == 0.0 || pklStawka == null || pklStawka == 0.0) {
//			p.setPklKwota(0.0);
//			p.setPklKwotaMf(0.0);
////			p.setPklKwotaPln(0.0);
//			return;
//		}
//
//		Long ret = 0l;
//		ret = (pklIlosc * pklStawka);
//		ret -= nz(p.getPklSniadania()) * pklStawka * 1500; //0.15;
//		ret -= nz(p.getPklObiady()) * pklStawka * 3000;// 0.30;
//		ret -= nz(p.getPklKolacje()) * pklStawka * 3000; // 0.30;
//
//		if (ret < 0l) ret = 0l;
//
//		p.setPklKwota(round((1.0d * ret) / (10000*10000), 2));
//
////		if(!(p.getDeltKalkulacje() == null)) {
////			if("WST".equals(p.getDeltKalkulacje().getKalRodzaj()))
////			p.setPklKwotaPln(round(p.getPklKwota() * kursPrzelicznikSprzedazyNBP(p.getPklWalId(), wniosek.getWndDataWyjazdu()), 2));
////
////			else
////			if(p.getDeltKalkulacje().getKalDataKursu() != null)
////				p.setPklKwotaPln(round(p.getPklKwota() * kursPrzelicznikSprzedazyNBP(p.getPklWalId(), p.getDeltKalkulacje().getKalDataKursu()), 2));
////			else p.setPklKwotaPln(round(p.getPklKwota() * kursPrzelicznikSprzedazyNBP(p.getPklWalId(), wniosek.getWndDataWyjazdu()), 2));
////		}
//		if(this.ustalKosztMf(p) != 0.0)
//		p.setPklKwotaMf(this.ustalKosztMf(p));
//
//		aktualizujKwotyZF(p); //TODO sprwadź w działaniu jak to się sprawuje
	}

//	public static double round(double value, int places) {
//		if (places < 0)
//			throw new IllegalArgumentException();
//
//		long factor = (long) Math.pow(10, places);
//		value = value * factor;
//		long tmp = Math.round(value);
//		return (double) tmp / factor;
//	}
	
//	public double ilePosilkow(DeltPozycjeKalkulacji pkl){
//		double ret = 0.0;
//		double roznica = round(pkl.getPklIlosc(), 0) - pkl.getPklIlosc();
//		if(roznica == 0 || roznica == 0.5)
//			ret = round(pkl.getPklIlosc(), 0);
//		else
//			ret = round(pkl.getPklIlosc(), 0) + 1;
//
//		return ret;
//	}
	
	
//	public void aktualizujKwoteRozl(DelZaliczki pozycjaRozliczenia) {
//		Double wydanaZaliczka = nz(pozycjaRozliczenia.getDkzZalWydano());
//		Double pobranaZaliczka = 0.0;
//
//		if("T".equals(this.wniosek.getWndFZaliczka())) {
//			pobranaZaliczka = kalkulacjaWstepna.getDelZaliczki().stream().filter(zal -> zal.getDkzDokId() != null && zal.getDkzWalId().equals(pozycjaRozliczenia.getDkzWalId()))
//				.collect(Collectors.toList()).get(0).getDkzKwota();
//		}
//
//		if(wydanaZaliczka > pobranaZaliczka) {
//			User.alert("Podano nieprawidłową kwotę pobranej zaliczki");
//			return;
//		}
//
//		Double kwotaKarta = nz(pozycjaRozliczenia.getDkzKartaKwota());
//
//		Double srodkiWlasne = nz(pozycjaRozliczenia.getDkzSrodkiWlasne());
//
//		if((Math.abs(kwotaKarta) + Math.abs(wydanaZaliczka) + Math.abs(srodkiWlasne)) != sumyWgWalutKalkRoz.get(pozycjaRozliczenia.getDkzWalId())) {
//			User.alert("Wskazany łączny koszt jest inny niż wynikający z podsumowania kosztów"); // tukej
//			return;
//		}
//
//		if(wydanaZaliczka > sumyWgWalutKalkRoz.get(pozycjaRozliczenia.getDkzWalId())){
//			User.alert("Kwota wydanej zaliczki przewyższa kwotę wynikającą z podsumowania rozliczenia delegacji, popraw kwoty");
//		}
//
//		pozycjaRozliczenia.setDkzKwota(
//				 wydanaZaliczka - pobranaZaliczka + srodkiWlasne);
//
//	}
	
//	/**
//	 * metoda wykorzystywana w prtzycisku na Tab II do kopiwoania pozycji kosztów nie będących dietami
//	 */
//	public void kopiujKoszty() {
//
//		if (kalkulacjaWniosek != null) {
//
////			HashSet<DeltPozycjeKalkulacji> pklP = getPozycjeWgKtg("P", kalkulacjaWniosek);
////			HashSet<DeltPozycjeKalkulacji> pklH = getPozycjeWgKtg("H", kalkulacjaWniosek);
//
//			// czyszczenie...
////			ArrayList<DeltPozycjeKalkulacji> rem = new ArrayList<DeltPozycjeKalkulacji>();
////			for (DeltPozycjeKalkulacji p : this.getKalkulacjaWstepna().getDeltPozycjeKalkulacjis()) {
////				if (!pklP.contains(p) && !pklH.contains(p) && !(p.getPklKwota()>0))
////					rem.add(p);
////			}
////			for (DeltPozycjeKalkulacji p : rem)
////				this.getKalkulacjaWstepna().removeDeltPozycjeKalkulacji(p);
//
//			List<Long> tpozIdList = new ArrayList<Long>();
//			for(DeltPozycjeKalkulacji pkl :  kalkulacjaWstepna.getDeltPozycjeKalkulacjis()){
//				tpozIdList.add(pkl.getPklTpozId());
//			}
//
//			List<DeltPozycjeKalkulacji> nowePklLista = new ArrayList<DeltPozycjeKalkulacji>();
//
//			for (DeltPozycjeKalkulacji pWst : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
//				if(tpozIdList.contains(pWst.getPklTpozId()) || !pWst.getKategoria().equals("I")) //if (pWst.getPklTpozId() <= 6L) -> 21.12.2017 || pWst.getPklKwota() == 0.0
//					continue;
//
//				DeltPozycjeKalkulacji pkl = newPozycjaKalkulacji(this.getKalkulacjaWstepna().getKalRodzaj());
//				pkl.setPklTpozId(pWst.getPklTpozId());
//				pkl.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(pkl.getPklTpozId()));
//				pkl.setPklKrId(pWst.getPklKrId());
//				//if(pWst!=null && pWst.getDeltZrodlaFinansowanias()!=null && !pWst.getDeltZrodlaFinansowanias().isEmpty()) pkl.setDeltZrodlaFinansowanias(pWst.getDeltZrodlaFinansowanias());
//				pkl.setPklKosztMFProcent(pWst.getPklKosztMFProcent());
//				pkl.setPklKosztZaprProcent(pWst.getPklKosztZaprProcent());
//				pkl.setPklRefundacjaProcent(pWst.getPklRefundacjaProcent());
//				pkl.setPklOpis(pWst.getPklOpis());
//                pkl.setPklIlosc(1.0);
//				pkl.setPklKwota(0.0);
//				pkl.setPklKwotaMf(0.0);
//
//				pkl.setPklFormaPlatnosci(this.getFormaPlatnosciZWniosku(pkl.getPklTpozId()));
//
//				nowePklLista.add(pkl);
//				//this.getKalkulacjaWstepna().addDeltPozycjeKalkulacji(pkl);
//
//				dodajDomyslneZfAn(pkl);
//
//			}
//
//			if(nowePklLista.size() == 0) {
//				User.info("Brak nowych pozycji do skopiowania");
//				return;
//			}
//
//		}
//
//	}

	
	//Modyfikacja po konsultacji z Michałem
	public void kopiujDaneZDelegacjiGrupowej( ) {
		if(this.getWndWndId()==null)
			return;
		
		wniosek.setWndWndId(this.getWndWndId());
		
		WniosekDelegacjiZagr wniosekPowiazany =  (WniosekDelegacjiZagr) HibernateContext.get(WniosekDelegacjiZagr.class, wniosek.getWndWndId()); //wczytajWniosekDelegacjiDB(wniosek.getWndWndId());
		wniosek.setWndWndId(wniosekPowiazany.getWndId());
		

		
		kopiujDeltCeleDelegacjis(wniosekPowiazany);		
		
		try {
			if(wniosekPowiazany.getDeltZapraszajacys().get(0).getZaprOpis() != null)
			kopiujDeltZapraszajacys(wniosekPowiazany);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		kopiujDeltSrodkiLokomocjis(wniosekPowiazany);		
		
		kopiujDeltKalkulacjes(wniosekPowiazany);
		
		
		
		wniosek.setWndDataWyjazdu(wniosekPowiazany.getWndDataWyjazdu());
		wniosek.setWndDataPowrotu(wniosekPowiazany.getWndDataPowrotu());
		wniosek.setWndTrasaWyjazdu(wniosekPowiazany.getWndTrasaWyjazdu());
		wniosek.setWndTrasaPowrotu(wniosekPowiazany.getWndTrasaPowrotu());
		wniosek.setWndOpis(wniosekPowiazany.getWndOpis());
		if(wniosekPowiazany.getWndSkId() != null && wniosekPowiazany.getWndWnrId() != null) {
			wniosek.setWndFDomyslneZf(wniosekPowiazany.getWndFDomyslneZf());
			wniosek.setWndSkId(wniosekPowiazany.getWndSkId());
			wniosek.setWndWnrId(wniosekPowiazany.getWndWnrId());
        }

        ustawReferencjeKalkulacji();
	}

	private void kopiujDeltKalkulacjes(WniosekDelegacjiZagr wniosekPowiazany) {
		if (wniosekPowiazany==null || wniosekPowiazany.getKalkulacjaWniosek()==null) return;

//		List<DelZaliczki> zaliczkiWniosek = wniosek.getDeltKalkulacjes().stream().filter(k->k.getKalRodzaj().equals("WNK")).collect(Collectors.toList()).get(0).getDelZaliczki();
		wniosek.clearDeltKalkulacjes(); //		czyscWniosekDeltKalkulacjes();
//		for( DeltKalkulacje kalk : wniosekPowiazany.getDeltKalkulacjes()) {
//			if("WNK".equals(kalk.getKalRodzaj())){
				
				DeltKalkulacje kalkCopy = wniosekPowiazany.getKalkulacjaWniosek().clone(true);
				kalkCopy.setKalId(0L);
				kalkCopy.setDeltWnioskiDelegacji(wniosek);
				wniosek.addDeltKalkulacje(kalkCopy);
//			}
//		}
	}

	private void kopiujDeltSrodkiLokomocjis(WniosekDelegacjiZagr wniosekPowiazany) {
		czyscWniosekDeltSrodkiLokomocjis();
		for( DeltSrodkiLokomocji lok : wniosekPowiazany.getDeltSrodkiLokomocjis()) {
			DeltSrodkiLokomocji lokCopy = lok.clone();
			lokCopy.setSlokId(0L);
			wniosek.addDeltSrodkiLokomocji(lokCopy);//lokCopy.setDeltWnioskiDelegacji(wniosek);
		}
	}

	private void kopiujDeltZapraszajacys(WniosekDelegacjiZagr wniosekPowiazany) {
		czyscWniosekDeltZapraszajacys();
		for( DeltZapraszajacy zapr : wniosekPowiazany.getDeltZapraszajacys()) {
			DeltZapraszajacy zaprCopy = zapr.clone();
			zaprCopy.setZaprId(0L);
			wniosek.addDeltZapraszajacy(zaprCopy);			//zaprCopy.setDeltWnioskiDelegacji(wniosek);
		}
	}

private void kopiujDeltCeleDelegacjis(WniosekDelegacjiZagr wniosekPowiazany) {
	czyscWniosekDeltCeleDelegacjis();
	for (DeltCeleDelegacji cel : wniosekPowiazany.getDeltCeleDelegacjis()) {
		DeltCeleDelegacji c1 = cel.clone();
		c1.setCdelId(0L);// ID!!!!
		wniosek.addDeltCeleDelegacji(c1); //c1.setDeltWnioskiDelegacji(wniosek);
	}
}

private void czyscWniosekDeltCeleDelegacjis() {
	for (Object p : wniosek.getDeltCeleDelegacjis().toArray()) {
		wniosek.removeDeltCeleDelegacji((DeltCeleDelegacji) p);
	}
}

//private void czyscWniosekDeltKalkulacjes() {
//	wniosek.removeDeltKalkulacje(wniosek.getDeltKalkulacjes().stream().filter(k->k.getKalRodzaj().equals("WNK")).collect(Collectors.toList()).get(0));
//}

private void czyscWniosekDeltSrodkiLokomocjis() {
	for( Object l : wniosek.getDeltSrodkiLokomocjis().toArray()) {
		wniosek.removeDeltSrodkiLokomocji((DeltSrodkiLokomocji)l);
	}
}

	private void czyscWniosekDeltZapraszajacys() {
		for( Object z: wniosek.getDeltZapraszajacys().toArray()) {
			wniosek.removeDeltZapraszajacy((DeltZapraszajacy)z);
		}
	}

	public void generujTrasy() {

		wyczyscTrasy();

		DeltCeleDelegacji c0 = new DeltCeleDelegacji();
		c0.setCdelKrId(1L);
		c0.setCdelMiejscowosc("Warszawa");

		int i = 1;
		for (DeltCeleDelegacji c : wniosek.getDeltCeleDelegacjis()) {
			
			if(c.getCdelKrId() == null || c.getCdelMiejscowosc() == null || c.getDeltWnioskiDelegacji().getDeltSrodkiLokomocjis().get(0).getSlokSrodekLokomocji() == null) {
				User.alert("Część I - trasa i środki transportu - nie wszystkie wymagane pola zostały uzupełnione, popraw dane ");
				return;
			}
			
			DeltTrasy t = new DeltTrasy();
			// t.setTrsLp(rozliczenie.getDeltTrasies().size() + 1L);
			t.setTrsKrIdOd(c0.getCdelKrId());
			t.setTrsMiejscowoscOd(c0.getCdelMiejscowosc());
			t.setTrsKrIdDo(c.getCdelKrId());
			t.setTrsKrIdDiety(c.getCdelKrId());
			t.setTrsMiejscowoscDo(c.getCdelMiejscowosc());
			t.setTrsSrodekLokomocji(c.getDeltWnioskiDelegacji().getDeltSrodkiLokomocjis().get(0).getSlokSrodekLokomocji());

			if (i == 1)
				t.setTrsDataOd(wniosek.getWndDataWyjazdu());// domyslna I data
															// to data wyjazdu
															// wg wniosku
			getKalkulacjaWstepna().addDeltTrasy(t);

			c0 = c;
			i++;
		}

		// ostatnia trasa - powrot do PL/Wawa
		DeltTrasy t = new DeltTrasy();
		// t.setTrsLp(rozliczenie.getDeltTrasies().size() + 1L);
		t.setTrsKrIdOd(c0.getCdelKrId());
		t.setTrsKrIdDiety(c0.getCdelKrId());
		t.setTrsMiejscowoscOd(c0.getCdelMiejscowosc());
		t.setTrsKrIdDo(1L);
		t.setTrsMiejscowoscDo("Warszawa");
		t.setTrsSrodekLokomocji(c0.getDeltWnioskiDelegacji().getDeltSrodkiLokomocjis().get(0).getSlokSrodekLokomocji());

		
		Calendar cal = Calendar.getInstance();
		cal.setTime(wniosek.getWndDataPowrotu());
		cal.set(Calendar.HOUR_OF_DAY, 1);
		t.setTrsDataDo(cal.getTime());

		getKalkulacjaWstepna().addDeltTrasy(t);
		this.zapisz();

	}

	public void addKoszt(DeltKalkulacje kalkulacja) {
		DeltPozycjeKalkulacji p = newPozycjaKalkulacji(kalkulacja.getKalRodzaj());
		p.setPklIlosc(1.0); 
		p.setPklTpozId(this.getTpozIdByKtgPozycji("I"));
		dodajDomyslneZfAn(p);
	}
	
//	public void ustawProcentKwoteMf(DeltPozycjeKalkulacji pkl) {
//		if(pkl.getPklTpozId() != null && pkl.getPklKosztMFProcent() == null) {
//			pkl.setPklKosztMFProcent(this.zwrocProcentKosztMF(pkl.getPklTpozId()));
//			pkl.setPklKwotaMf(this.ustalKosztMf(pkl));
//		}
//	}
	
	public void ustawZFAngaze(DeltPozycjeKalkulacji p) {
		//?
		p.getDeltZrodlaFinansowanias().clear();//zle... tak nie nalezy robic
		
		if(skopiujZF(p.getPklTpozId()).size() != 0) {
			for(DeltZrodlaFinansowania zf : skopiujZF(p.getPklTpozId())) {
				p.addDeltZrodlaFinansowania(zf);
			}
		}
		
		if(p.getDeltZrodlaFinansowanias().isEmpty()) {
			dodajDomyslneZfAn(p);
		}
	}

	public void addDietaPobytowa(DeltKalkulacje kalkulacja) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

		DeltPozycjeKalkulacji p = newPozycjaKalkulacji(kalkulacja.getKalRodzaj());
		p.setPklTpozId(getTpozIdByKtgDiety("P"));
		ustawZFAngaze(p);
		p.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(p.getPklTpozId()));
		p.setPklIlosc(1.0);
		p.setPklRefundacjaProcent(this.zwrocProcentRefundacji(p.getPklTpozId()));
//		p.setPklKosztMFProcent(this.zwrocProcentKosztMF(p.getPklTpozId()));
		
		
		//kalkulacja.addDeltPozycjeKalkulacji(p);
	}

//	public void addDietaHotelowa(DeltKalkulacje kalkulacja) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
//		
//		DeltPozycjeKalkulacji p = newPozycjaKalkulacji();
//		p.setPklTpozId(getTpozIdByKtgDiety("H"));
//		p.setPklIlosc(1.0);
//		p.setPklRefundacjaProcent(this.zwrocProcentRefundacji(p.getPklTpozId()));
//		p.setPklKosztMFProcent(this.zwrocProcentKosztMF(p.getPklTpozId()));
//		
//		if(skopiujZF(p.getPklTpozId()).size() != 0) {
//			for(DeltZrodlaFinansowania zf : skopiujZF(p.getPklTpozId())) {
//				p.addDeltZrodlaFinansowania(zf);
//			}
//		}
//		
//		kalkulacja.addDeltPozycjeKalkulacji(p);
//	}
//	
//	public void addRyczaltDojazd(DeltKalkulacje kalkulacja) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
//		
//		DeltPozycjeKalkulacji p = newPozycjaKalkulacji();
//		p.setPklTpozId(getTpozIdByKtgDiety("RD"));
//		p.setPklIlosc(1.0);
//		p.setPklRefundacjaProcent(this.zwrocProcentRefundacji(p.getPklTpozId()));
//		p.setPklKosztMFProcent(this.zwrocProcentKosztMF(p.getPklTpozId()));
//		
//		if(skopiujZF(p.getPklTpozId()).size() != 0) {
//			for(DeltZrodlaFinansowania zf : skopiujZF(p.getPklTpozId())) {
//				p.addDeltZrodlaFinansowania(zf);
//			}
//		}
//		
//		kalkulacja.addDeltPozycjeKalkulacji(p);
//	}
	
//	public void kopiujZfZWniosku(DeltPozycjeKalkulacji p){
//		if(p.getPklTpozId() != null) {
//			if(p.getDeltZrodlaFinansowanias().size() == 0)
//				if(skopiujZF(p.getPklTpozId()).size() != 0) {
//					for(DeltZrodlaFinansowania zf : skopiujZF(p.getPklTpozId())) {
//						p.addDeltZrodlaFinansowania(zf.clone());
//					}
//				}
//			aktualizujKwotyZF(p);
//		}
//
//	}
	
	public void addPklByKtgPozycji(DeltKalkulacje kalkulacja, String ktgPozycji) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		
		DeltPozycjeKalkulacji p = newPozycjaKalkulacji(kalkulacja.getKalRodzaj());
		if("H".equals(ktgPozycji) || "RH".equals(ktgPozycji) ) {
			p.setPklTpozId(getTpozIdByKtgDiety(ktgPozycji));
		} else {
			p.setPklTpozId(getTpozIdByKtgPozycji(ktgPozycji));
		}
		ustawZFAngaze(p);
		p.setPklFormaPlatnosci(getFormaPlatnosciZWniosku(p.getPklTpozId()));
		p.setPklIlosc(1.0);
		p.setPklRefundacjaProcent(this.zwrocProcentRefundacji(p.getPklTpozId()));
//		p.setPklKosztMFProcent(this.zwrocProcentKosztMF(p.getPklTpozId())); //TODO ZASTANOWIĆ SIĘ CZY DALEJ POTZREBNE
//		this.przeliczKwote(p);
		//kalkulacja.addDeltPozycjeKalkulacji(p);
	}
	
	private String getFormaPlatnosciZWniosku(Long tPozId) {
		 List<String> czyPozycjaZWniosku = kalkulacjaWniosek.getDeltPozycjeKalkulacjis().stream().filter(
				pkl -> tPozId.equals(pkl.getPklTpozId())).map(pkl -> pkl.getPklFormaPlatnosci()).collect(Collectors.toList());
			String formaPlatnosci = "";
		 if(!czyPozycjaZWniosku.isEmpty()) {
			 formaPlatnosci = czyPozycjaZWniosku.get(0);
		 }
		if(!nz(formaPlatnosci).isEmpty())
			return formaPlatnosci;
		else
			return "01";
	}
	
//	private String getFormaPlatnosciZWniosku(String ktg) {
//		 List<String> czyPozycjaZWniosku = kalkulacjaWniosek.getDeltPozycjeKalkulacjis().stream().filter(
//				pkl -> ktg.equals(pkl.getKategoria())).map(pkl -> pkl.getPklFormaPlatnosci()).collect(Collectors.toList());
//			String formaPlatnosci = "";
//		 if(!czyPozycjaZWniosku.isEmpty()) {
//			 formaPlatnosci = czyPozycjaZWniosku.get(0);
//		 }
//		if(formaPlatnosci != null)
//		return formaPlatnosci;
//			else
//				return "01";
//	}

	private void dodajDomyslneZfAn(DeltPozycjeKalkulacji p) {
		//?
		if("T".equals(wniosek.getWndFDomyslneZf()) && wniosek.getWndSkId() != null && wniosek.getWndWnrId() != null){
			DeltZrodlaFinansowania zf = new DeltZrodlaFinansowania();
			DelAngaze an = new DelAngaze();
			zf.setPkzfProcent(100.0);
			zf.setPkzfKwota(0.0);
			zf.setPkzfSkId(wniosek.getWndSkId());
			an.setAngWnrId(wniosek.getWndWnrId());
			an.setAngProcent(100.0);
			an.setAngKwota(0.0);
			zf.addDelAngaze(an);
			p.addDeltZrodlaFinansowania(zf);
		}
	}

	
	private void wyczyscTrasy() {
		HashSet<DeltTrasy> trasy = new HashSet<DeltTrasy>();
		trasy.addAll(getKalkulacjaWstepna().getDeltTrasies());
		for (DeltTrasy t : trasy) {
			getKalkulacjaWstepna().removeDeltTrasy(t);
		}

	}

	private WniosekDelegacjiZagr newWniosek() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {

		WniosekDelegacjiZagr ret = new WniosekDelegacjiZagr();
		this.setWniosek(ret);
		//this.wniosek.setSql(this);
		ret.setWndRodzaj(2L); // 2 dla zagranicznej, 1 dla krajowej
		ret.setWndPrcId(AppBean.getUser().getPrcIdLong());
		ret.setWndFWyjazdZDomu("N");
		ret.setWndCzyGrupowa("N");
		ret.setWndFZaliczka("N");
		ret.setWndFBezkosztowa("N");
		ret.setWndFJednaWaluta("N");
		ret.setWndDataWniosku(new Date()); // Opcjonalnie
		
		
		//wniosek.setWndStdId();
		
		addNewCDEL();
		addNewSLOK();
		addNewZapraszajacy();
		addNewProjekt();
		
		this.kalkulacjaWniosek =  addNewKalkulacje("WNK");

//		addPozycjeKalkulacjiByTypyPozycji(kalkulacjaWniosek);
//		addNewZaliczka();

		this.kalkulacjaWstepna = addNewKalkulacje("WST");
		
		
		return ret;
	}


//	public void onTypDietChanged(javax.faces.event.ValueChangeEvent e) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
//
//		pozKtgHM.clear(); // kategoria -> tpozId ...
//
//		this.wniosek.setWndStdId((Long) e.getNewValue());
//
//		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_STAWKI");
//		DataRow r = lw.get(e.getNewValue());
//		if( r != null) {
//			String nazwaStawki = (String)r.get("STD_NAZWA_STAWKI");
//			if(!"mpips".equals(nazwaStawki.toLowerCase())) {
//
//				usunRDKM(this.kalkulacjaWstepna);
//
//				setBlokujRDKM(true);
//			} else
//				setBlokujRDKM(false);
//		}
//
//		if(this.kalkulacjaWstepna.getDelCzynnosciSluzbowe().size() != 0) {
//			generujDiety_CS("P", this.kalkulacjaWstepna);
////			if(getCzyDietaHotelowa().equals(true))  //grr tak nie wolno - możliwy NPE
//			if (eq(true, this.getWniosek().getCzyDomyslnieDietaHotelowa()))
//				generujDiety_CS("H", this.kalkulacjaWstepna);
//			else
//				generujDiety_CS("RH", this.kalkulacjaWstepna);
//		} else {
//			generujDiety("P", this.kalkulacjaWstepna);
////			if(getCzyDietaHotelowa().equals(true)) //grr tak nie wolno - możliwy NPE
//			if (eq(true, this.getWniosek().getCzyDomyslnieDietaHotelowa()))
//				generujDiety("H", this.kalkulacjaWstepna);
//			else
//				generujDiety("RH", this.kalkulacjaWstepna);
//		}
//	}
	
	
//	private void usunRDKM(DeltKalkulacje kalk) {
//		List<DeltPozycjeKalkulacji> listaKMiRD = kalk.getDeltPozycjeKalkulacjis().stream().filter(pkl -> SqlBean.eq(pkl.getPklTpozId(), this.getTpozIdByKtgPozycji("KM")) || SqlBean.eq(pkl.getPklTpozId(), this.getTpozIdByKtgPozycji("RD"))).collect(Collectors.toList());
//		if(listaKMiRD.size() > 0) {
//			for(DeltPozycjeKalkulacji pkl : listaKMiRD) {
//				kalk.getDeltPozycjeKalkulacjis().remove(pkl);
//			}
//		}
//
//	}
	
	
//	private void addPozycjeKalkulacjiByTypyPozycji(DeltKalkulacje kalkulacja) {
//
//		SqlDataSelectionsHandler sl = getSql("PP_ID_TYPOW_POZYCJI"
//					, "select tpoz_id from PPADM.PPT_DEL_TYPY_POZYCJI  where (TPOZ_STD_ID is null OR TPOZ_STD_ID = ?)"
//					, wniosek.getWndStdId() );
//
//		for (DataRow dr :  sl.getData() ) {
//			long tpozId = Long.parseLong(dr.get(0).toString());
//
//			DeltPozycjeKalkulacji pk = newPozycjaKalkulacji(kalkulacja.getKalRodzaj());
//
//			pk.setPklTpozId(tpozId);
//			pk.addDeltZrodlaFinansowania(new DeltZrodlaFinansowania());
//			pk.setPklKosztMFProcent(100.0); // ustawiam domyślnie 100% kosztu dla MF
//			pk.setPklKosztZaprProcent(0.0);
//			pk.setPklRefundacjaProcent(0.0);
//
//			//kalkulacja.addDeltPozycjeKalkulacji(pk);
//		}
//
//	}

	private DeltPozycjeKalkulacji newPozycjaKalkulacji(String typKal) {
		DeltPozycjeKalkulacji pk = new DeltPozycjeKalkulacji();
		// pk.setPklLp(0L);
//		pk.setPklFOsobaTowarzyszaca("N");
		if(typKal==null) 
			typKal="WST";
			
		switch (typKal) {
		case "WST":
			kalkulacjaWstepna.addDeltPozycjeKalkulacji(pk);
			break;
		case "WNK":
			kalkulacjaWniosek.addDeltPozycjeKalkulacji(pk);
			break;
		case "ROZ":
			kalkulacjaRozliczenia.addDeltPozycjeKalkulacji(pk);
			break;	

		}
		
		pk.setPklWalId(1L);
		pk.setPklIlosc(0.000);
		pk.setPklStawka(0.0);
		pk.setPklKwotaPrzelewu(0L);
		pk.setPklKwotaGotowki(" ");
		pk.setPklKwota(0.0);
		pk.setPklKwotaMf(0.0);
		pk.setPklRefundacjaKwota(0.0);
		
		return pk;
	}

	private WniosekDelegacjiZagr wczytajWniosek(long id) {
		final WniosekDelegacjiZagr wniosek = (WniosekDelegacjiZagr) HibernateContext.get(WniosekDelegacjiZagr.class, id);
		this.setWniosek(wniosek);
		ustawReferencjeKalkulacji();
		//naprawEncje();
		return this.wniosek;
	}

//	private WniosekDelegacjiZagr wczytajWniosekDelegacjiDB(long id) {
////		System.out.println("wczytajWniosek / " + id + ".... ");
//
//		WniosekDelegacjiZagr ret = new WniosekDelegacjiZagr();
//
//		try (HibernateContext h = new HibernateContext()) {
//			ret = h.getSession().get(WniosekDelegacjiZagr.class, id);
//
////			ret.getDeltCeleDelegacjis().size();// doczytaj kolekcję (LAZY!)
////			ret.getDeltSrodkiLokomocjis().size(); // doczytaj kolejną
//														// kolekcję (LAZY!)
////			ret.getDeltZapraszajacys().size(); // doczytaj kolejną kolekcję
//													// (LAZY!)
////			ret.getDeltProjektys().size(); // doczytaj kolejną kolekcję
//												// (LAZY!)
//
////			ret.getDeltKalkulacjes().size(); // doczytaj kolejną kolekcję
//													// (LAZY!)
////			for (DeltKalkulacje k : ret.getDeltKalkulacjes()) {
////				k.getDeltPozycjeKalkulacjis().size();
////				for (DeltPozycjeKalkulacji pkl : k.getDeltPozycjeKalkulacjis()) {
////					pkl.getDeltZrodlaFinansowanias().size();
////				}
////			}
//
//		}
//		//wniosek.setSql(this);
//		return ret;
//	}



	public void zapisz() {
		
		if (!wniosek.validate())
			return;
		
		this.getLwRozliczeniePklNzWgFifo().resetTs();
		
		try {

			if (eq(nz(this.wniosek.getWndId()),0L))//jesli to nowa delegacja to potrzebna aktualizacja tabView aby odblokowac czesc II i czesc III
				Utils.updateComponent("tabViewId");

			HibernateContext.saveOrUpdate(this.wniosek);
			ustawReferencjeKalkulacji();
			User.info("Pomyślnie zapisano wniosek %1$s", wniosek.getWndNumer());
			Utils.setFormName("Delegacja zagraniczna nr: " + wniosek.getWndNumer()); //RequestContext.getCurrentInstance().execute("parent.setFormName('Delegacja zagraniczna nr: " + wniosek.getWndNumer() + "')");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e); //User.alert(e.getMessage(), e);
			return;
		}
		
	}

	public void usun() {
		
		if(this.getLW("PPL_DEL_WND_DZIECI" ,wniosek.getWndId()).getData().size()>0) {
			User.alert("Nie można usunąć wniosku powiązanego z inną delegacją");
			return;
		}
		
		try (Connection conn = JdbcUtils.getConnection(false)) {
			obieg.usunDaneObiegu();
		} catch (SQLException e) {
			User.alert("Nieudane usuniecie danych obiegu: " + e);
			return;
		}

		try {
			ZalacznikiBean.deleteAll("ZAL_DEL_ID", wniosek.getWndId());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert("Błąd przy próbie usunięcia załączników: " + e);
			return;
		}

		try {
			HibernateContext.delete(wniosek);
			User.info("Usunięto delegację");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert("Błąd w usuwaniu delegacji!", e);
			return;
		}

		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("Delegacje");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void anulujWniosek() {
		
		if(getLW("PPL_DEL_WND_DZIECI" ,this.wniosek.getWndId()).getData().size()>0) {
			User.alert("Nie można anulować wniosku powiązanego z inną delegacją");
			return;
		}
		
		this.wniosek.setWndStatus(0L);
		zapisz();
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("Delegacje");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void raportWniosek() {
		if (wniosek.getWndId() != 0) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("wnd_id", wniosek.getWndId());
				com.comarch.egeria.web.common.reports.ReportGenerator.displayReportAsPDF(params, "Delegacja_zagraniczna_wniosek");

			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
	}
	
	public void raportRozliczenie() {
		if (wniosek.getWndId() != 0) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("wnd_id", wniosek.getWndId());
				com.comarch.egeria.web.common.reports.ReportGenerator.displayReportAsPDF(params, "Delegacja_zagraniczna_rozliczenie");

			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
	}
	


//	public void updateView() {
//		com.comarch.egeria.Utils.update("tabViewId");
//	}


//	private void showEntityDetails() {
//		System.out.println("-----------------------------------------------------------------------");
//		System.out.println("-----------------------------------------------------------------------");
//		System.out.println("-----------------------------------------------------------------------");
//		System.out.println("-----------------------------------------------------------------------");
//		System.out.println(wniosek.getWndId() + " " + wniosek.getWndUwagi() + " " + wniosek.getWndPole08());
//		System.out.println("-----------------------------------------------------------------------");
//		for (DeltCeleDelegacji c : this.wniosek.getDeltCeleDelegacjis()) {
//			System.out.println(c.getCdelId() + " " + c.getCdelKrId() + " " + c.getCdelMiejscowosc());
//		}
//		System.out.println("-----------------------------------------------------------------------");
//
//		this.loadEgSlownik("DEL_SRODEK_LOKOMOCJI");
//		for (DeltSrodkiLokomocji s : wniosek.getDeltSrodkiLokomocjis()) {
//			System.out.println(s.getSlokId() + " " + s.getSlokSrodekLokomocji());
//		}
//
//		System.out.println("-----------------------------------------------------------------------");
//		System.out.println("-----------------------------------------------------------------------");
//		System.out.println("-----------------------------------------------------------------------");
//		System.out.println("-----------------------------------------------------------------------");
//	}
	
	
//	public void  wyznaczWalutyDoZaliczek() {
//		String idKraje = "";
//		
//		for( DeltCeleDelegacji cel : wniosek.getDeltCeleDelegacjis()){
//			Long krID = cel.getCdelKrId();
//			if(krID != null) {
//				idKraje += ","+ cel.getCdelKrId();
//			}
//		}
//		idKraje += ",";
//		
//		walutyZaliczki = new SqlDataSelectionsHandler();
//		walutyZaliczki = this.getLW("PPL_KRAJE_WALUTY", idKraje);
//	}
	
//	public Long WalutaWgKraju(Long krajId){
//		if(krajId==null)
//			return 0L;
//
//		SqlDataSelectionsHandler waluty = this.getLW("PPL_KRAJE_WALUTY_ALL");
//		DataRow r = waluty.get(krajId);
//		if(r == null)
//			return 1L;
//
//		BigDecimal walId = (BigDecimal) r.get("WAL_ID");
//
//		return walId.longValue();
//	}


    private Boolean validateBeforeSaveDelegacja() {
		// walidacja - do dalszego refaktoringu
		ArrayList<String> inval = new ArrayList<String>();


		//czy w przypadku zaznaczenia Równowartośc w PLN jest konto w PLN (albo gotówka)
		if(eq("T", this.wniosek.getWndFZaliczka()) && !kalkulacjaWniosek.validateDelZaliczki()) {
			inval.add("Część I : błędne dane w zaliczkach");//WTF?! a po kiego jesli nie zaznaczmy czy Zaliczka???.... grrrrr
		}
		if (!kalkulacjaWstepna.validateDelZaliczki()) {
			inval.add("Część II : błędne dane w podsumowaniu kosztów");
		}
		if(!kalkulacjaRozliczenia.validateDelZaliczki()) {
			inval.add("Część III : błędne dane w podsumowaniu kosztów");
		}

//		validujZaliczkiKalkulacji(this.getKalkulacjaWniosek(), inval);
//		validujZaliczkiKalkulacji(this.getKalkulacjaWstepna(), inval);
//		validujZaliczkiKalkulacji(this.getKalkulacjaRozliczenia(), inval);

		String wymagalne = "";
		
		sprawdzZapraszajacych(inval);
		
		Date dzisiaj = new Date();
		
		if(wniosek.getWndDataPowrotu() != null)
		if(wniosek.getWndId() == 0 && wniosek.getWndDataPowrotu().before(dzisiaj)) {
			User.warn("Termin delegacji jest datą przeszłą");
		}
		
		if(sprawdzZf(kalkulacjaWstepna) || sprawdzZf(kalkulacjaRozliczenia))
			return false;
		
		for (DeltPozycjeKalkulacji p : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
			if(nz(p.getPklKosztMFProcent()) + nz(p.getPklKosztZaprProcent()) + nz(p.getPklRefundacjaProcent()) != 100.0) {
				inval.add("RODZAJE KOSZTÓW : Procentowa suma dla pojedynczego rodzaju kosztu powinna wynosić 100%");
				break;
			}
			
		}
		
		if (wniosek.getWndDataWniosku() == null) {
			inval.add("Data złożenia wniosku nie może być pusta");
		}
		
		if (wniosek.getWndDataWyjazdu() != null && wniosek.getWndDataPowrotu()!=null &&
				wniosek.getWndDataWyjazdu().after(wniosek.getWndDataPowrotu())){
				
			inval.add("Data wyjazdu nie może być większa niż data powrotu.");
		}
		
		if (wniosek.getWndPrywDataWyjazdu() != null && wniosek.getWndPrywDataPowrotu()!=null &&
				wniosek.getWndPrywDataWyjazdu().after(wniosek.getWndPrywDataPowrotu())){
				
			inval.add("Data prywatnego wyjazdu nie może być większa niż data powrotu.");
		}

		if (wniosek.getWndOpis() == null || "".equals(wniosek.getWndOpis()))
			wymagalne = wymagalne + "Opis celu delegacji; ";
		// inval.add("Opis celu delegacji jest polem wymaganym.");

		if ("T".equals(wniosek.getWndCzyGrupowa())
				&& (wniosek.getWndGrUzasadnienie() == null || wniosek.getWndGrUzasadnienie().trim().equals("")))
			wymagalne = wymagalne + "Uzasadnienie składu delegacji dla delegacji grupowych; ";
		// inval.add("Dla delegacji grupowych uzasadnienie składu delegacji jest
		// polem wymaganym.");

		if (wniosek.getWndDataPowrotu() == null || wniosek.getWndDataWyjazdu() == null)
			wymagalne = wymagalne + "Termin delegacji (daty od /do); ";
		// inval.add("Termin delegacji (daty od /do) jest polem wymaganym.");

		if (wniosek.getDeltCeleDelegacjis().isEmpty())
			wymagalne = wymagalne + "Cel delgacji (kraj i miejscowość); ";
		// inval.add("Cel delgacji (kraj i miejscowość) jest wymagany.");

		for (DeltCeleDelegacji c : wniosek.getDeltCeleDelegacjis()) {
			if (c.getCdelKrId() == null)
				wymagalne = wymagalne + "Kraj; ";
			// inval.add("Kraj jest polem wymaganym.");
			if (c.getCdelMiejscowosc() == null || c.getCdelMiejscowosc().trim().equals(""))
				wymagalne = wymagalne + "Miejscowość docelowa; ";
			// inval.add("Miejscowość docelowa jest polem wymaganym.");
		}

		if ("T".equals(wniosek.getWndFWyjazdZDomu())
				&& (wniosek.getWndPrywDataPowrotu() == null || wniosek.getWndPrywDataWyjazdu() == null))
			wymagalne = wymagalne + "Termin podróży prywatnej (daty od /do); ";
		// inval.add("Termin podróży prywatnej (daty od /do) jest polem
		// wymaganym.");

		List<DeltSrodkiLokomocji> listaSrodkiLokomocji = wniosek.getDeltSrodkiLokomocjis().stream().filter(
				sl -> (sl.getSlokSrodekLokomocji().isEmpty())).collect(Collectors.toList());
		
		if (wniosek.getDeltSrodkiLokomocjis() == null || wniosek.getDeltSrodkiLokomocjis().isEmpty()
				|| listaSrodkiLokomocji.size() > 0)
			wymagalne = wymagalne + "Środek lokomocji; ";
		// inval.add("Środek lokomocji jest polem wymaganym.");

		for (DeltPozycjeKalkulacji p : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
			if (p.getPklTpozId() == null)
				wymagalne = wymagalne + "Rodzaj kosztu - wniosek; ";
			// inval.add("Rodzaj kosztu jest polem wymaganym.");
		}
		
		for (DeltPozycjeKalkulacji p : kalkulacjaWstepna.getDeltPozycjeKalkulacjis()) {
			if (p.getPklTpozId() == null)
				wymagalne = wymagalne + "Rodzaj kosztu - kalkulacja wstępna;";
			// inval.add("Rodzaj kosztu jest polem wymaganym.");
		}
		
		for (DeltPozycjeKalkulacji p : kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis()) {
			if (p.getPklTpozId() == null)
				wymagalne = wymagalne + "Rodzaj kosztu; - kalkulacja rozliczenia ";
			// inval.add("Rodzaj kosztu jest polem wymaganym.");
		}
		
//		if(!czyMogeZaliczke && "T".equals(wniosek.getWndFZaliczka())){
//			inval.add("Nie możesz brać zaliczek, masz nierozliczone delegacje!");
//		}
		
		if(!this.kalkulacjaWstepna.sprawdzDostepnoscKursowWalut()) {
			inval.add("Brak kursów sprzedaży dla daty kalkulacji wstępnej na dzień: " + format(this.kalkulacjaWstepna.getKalDataKursu())+ ", operacja przerwana");
		}
		
		if(!this.walidujPlanTrasy(kalkulacjaWstepna, true)) {
			inval.add("Błędne dane w planie trasy cz II");
		}
		
		if(!this.walidujPlanTrasy(kalkulacjaRozliczenia, false)) {
			inval.add("Błędne dane w planie trasy cz III");
		}

		if (!wymagalne.isEmpty()) {
			inval.add("Następujące pola są wymagalne: " + wymagalne);
		}

		if (!inval.isEmpty()) {
			for (String msg : inval) {
				User.alert(msg);
			}
		}

		return inval.isEmpty();
	}

	private void sprawdzZapraszajacych(ArrayList<String> inval) {
		if(wniosek.getDeltZapraszajacys().size() > 1)
		for(DeltZapraszajacy zapr : wniosek.getDeltZapraszajacys()) {
			try {
				if(zapr.getZaprOpis() == null || zapr.getZaprOpis().length() == 0) {
					inval.add("Lista zapraszających zawiera pustą pozycję");
				}
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public WniosekDelegacjiZagr getWniosek() {
		return wniosek;
	}

	public void setWniosek(WniosekDelegacjiZagr wniosek) {
		this.wniosek = wniosek;
		if (wniosek!=null) this.wniosek.setSqlBean(this);
	}

	public void deleteCity(int index) {
		DeltCeleDelegacji c = this.wniosek.getDeltCeleDelegacjis().get(index);
		this.wniosek.removeDeltCeleDelegacji(c);
		if (this.wniosek.getDeltCeleDelegacjis().isEmpty()) {
			this.wniosek.addDeltCeleDelegacji(new DeltCeleDelegacji());
		}
	}

	public DeltCeleDelegacji addNewCDEL() {
		DeltCeleDelegacji c = new DeltCeleDelegacji();
		for (DeltCeleDelegacji c1 : wniosek.getDeltCeleDelegacjis()) {
			c.setCdelKrId(c1.getCdelKrId());// zostanie ostatni kraj z listy
		}
		if (wniosek.getDeltCeleDelegacjis() == null)
			wniosek.setDeltCeleDelegacjis(new ArrayList<>());
		this.wniosek.addDeltCeleDelegacji(c);
		return c;
	}

	public DeltKalkulacje addNewKalkulacje(String rodzaj) {
		if (wniosek.getDeltKalkulacjes() == null)
			wniosek.setDeltKalkulacjes(new ArrayList<DeltKalkulacje>());
		
		DeltKalkulacje k = new DeltKalkulacje();
		k.setSqlBean(this);
		k.setKalData(new Date());

		//Date datakursowszacunkowych = new KursWalutySredniNBP(getWalId("EUR"), Utils.addDays(Utils.today(), -1)).getDataKursu();
		Date datakursowszacunkowych = new KursWalutySprzedazyNBP(getWalId("EUR"), Utils.today()).getDataKursu();
		if (datakursowszacunkowych!=null)
		k.setKalDataKursu(datakursowszacunkowych);
		k.setKalRodzaj(rodzaj);
		
		wniosek.addDeltKalkulacje(k);

		return k;
	}

	public void addNewZapraszajacy() {
		wniosek.addDeltZapraszajacy(new DeltZapraszajacy());
	}

	public void deleteZapraszajacy(int index) {
		DeltZapraszajacy s = this.wniosek.getDeltZapraszajacys().get(index);
		wniosek.removeDeltZapraszajacy(s);
	}

	public void addNewProjekt() {
		wniosek.addDeltProjekty(new DeltProjekty());
	}

	public void deleteProjekt(int index) {
		DeltProjekty s = this.wniosek.getDeltProjektys().get(index);
		wniosek.removeDeltProjekty(s);
	}

	public void deleteSrodekLokomocji(int index) {
		DeltSrodkiLokomocji sl = wniosek.getDeltSrodkiLokomocjis().get(index);
		wniosek.removeDeltSrodkiLokomocji(sl);

		if (wniosek.getDeltSrodkiLokomocjis().isEmpty()) {
			wniosek.addDeltSrodkiLokomocji(new DeltSrodkiLokomocji());
		}
	}

	public DeltSrodkiLokomocji addNewSLOK() {
		DeltSrodkiLokomocji slok = new DeltSrodkiLokomocji();
		if (wniosek.getDeltSrodkiLokomocjis() == null)
			wniosek.setDeltSrodkiLokomocjis(new ArrayList<>());
		wniosek.addDeltSrodkiLokomocji(slok);
		return slok;
	}
	
	
	public void dodajTrase (DeltKalkulacje kalk) {
		if(!kalk.getDeltTrasies().isEmpty()) {
			DeltTrasy ostatniaTrasa = kalk.getDeltTrasies().get(kalk.getDeltTrasies().size() - 1);
			if(ostatniaTrasa.getTrsKrIdDo() != null) {
				DeltTrasy trs = new DeltTrasy();
				trs.setTrsKrIdOd(ostatniaTrasa.getTrsKrIdDo());
				trs.setTrsKrIdDiety(ostatniaTrasa.getTrsKrIdDiety());
				
				kalk.addDeltTrasy(trs);
			} else {
				kalk.addNewTrasa();
			}
		} else {
			kalk.addNewTrasa();
		}
	} 

	public void deleteWndWndId() {
		wniosek.setWndWndId(null);

		setWndWndId(null);
	}

//	public List<DeltCeleDelegacji> getCities() {
//		return this.wniosek.getDeltCeleDelegacjis();
//	}

	/**
	 * @return the wniosekDelegacjaPowiazana
	 */
	public WniosekDelegacjiZagr getWniosekDelegacjaPowiazana() {
		return wniosekDelegacjaPowiazana;
	}

	/**
	 * @param wniosekDelegacjaPowiazana
	 *            the wniosekDelegacjaPowiazana to set
	 */
	public void setWniosekDelegacjaPowiazana(WniosekDelegacjiZagr wniosekDelegacjaPowiazana) {
		this.wniosekDelegacjaPowiazana = wniosekDelegacjaPowiazana;
	}

	public void wczytajWniosekDelegacjaPowiazana() {
		wniosekDelegacjaPowiazana = (WniosekDelegacjiZagr) HibernateContext.get(WniosekDelegacjiZagr.class, wniosek.getWndWndId()); // wczytajWniosekDelegacjiDB(wniosek.getWndWndId());
	}

	public DeltTrasy getSelectedTrasa() {
		return selectedTrasa;
	}

	public void setSelectedTrasa(DeltTrasy selectedTrasa) {
		this.selectedTrasa = selectedTrasa;
	}

	/**
	 * @return the kalkulacjaWstepna
	 */
	public DeltKalkulacje getKalkulacjaWniosek() {
		return kalkulacjaWniosek;
	}

	/**
	 * @param kalkulacjaWniosek
	 *            the kalkulacjaWstepna to set
	 */
	public void setKalkulacjaWniosek(DeltKalkulacje kalkulacjaWniosek) {
		this.kalkulacjaWniosek = kalkulacjaWniosek;
	}

	public void setRodzajDelegacjiDefaults(String rodzajDelegacji) {

		wniosek.setWndFCzySzkolenie("N");
		wniosek.setWndCzyGrupowa("N");
		wniosek.setWndFBezkosztowa("N");
		wniosek.setWndFProjektowa("N");
		wniosek.setWndFKonferencyjna("N");


		switch (rodzajDelegacji) {
		case "ZAGR_SZK":
			wniosek.setWndFCzySzkolenie("T");
			break;
		case "ZAGR_GRUP":
			wniosek.setWndCzyGrupowa("T");
			break;
		case "ZAGR_BKSZ":
			wniosek.setWndFBezkosztowa("T");

			if (!"ZAGR_BKSZ".equals(this.rodzajDelegacji) && !this.kalkulacjaWniosek.getDeltPozycjeKalkulacjis().isEmpty())
				User.warn("Wybrałeś typ delegacji: Bezkosztowa. \n Wprowadzone koszty zostaną usunięte przy zapisie.");

			break;
		case "ZAGR_PROJ":
			wniosek.setWndFProjektowa("T");
			break;
		case "ZAGR_KONF":
			wniosek.setWndFKonferencyjna("T");
			break;

		}

		this.rodzajDelegacji = rodzajDelegacji;
	}

//	/**
//	 * @return the rodzajDelegacji
//	 */
//	public String getRodzajDelegacji() {
//
//		if (wniosek.getWndFCzySzkolenie() != null && wniosek.getWndFCzySzkolenie().equals("T"))
//			return "ZAGR_SZK";
//		if (wniosek.getWndFBezkosztowa() != null && wniosek.getWndFBezkosztowa().equals("T"))
//			return "ZAGR_BKSZ";
//		if (wniosek.getWndFProjektowa() != null && wniosek.getWndFProjektowa().equals("T"))
//			return "ZAGR_PROJ";
//		if (wniosek.getWndCzyGrupowa() != null && wniosek.getWndCzyGrupowa().equals("T"))
//			return "ZAGR_GRUP";
//		if (wniosek.getWndFKonferencyjna() != null && wniosek.getWndFKonferencyjna().equals("T"))
//			return "ZAGR_KONF";
//
//		return rodzajDelegacji;
//	}

	public Long getWndWndId() {
		return wndWndId;
	}


	public void setWndWndId(Long wndWndId) {

		if (wniosek.getDeltCeleDelegacjis() == null || wniosek.getDeltCeleDelegacjis().size() == 0
				|| (wniosek.getDeltCeleDelegacjis().get(0).getCdelKrId() == null)) {
//			wniosek.setWndWndId(wndWndId);
			this.wndWndId = wndWndId;
			kopiujDaneZDelegacjiGrupowej();
			return;
		}

		if (wndWndId != null && !wndWndId.equals(wniosek.getWndWndId())) {
//			RequestContext context = RequestContext.getCurrentInstance();
//			context.execute("PF('confKopiujCele').show();");
			com.comarch.egeria.Utils.update("PF('confKopiujCele').show();");
		}
		this.wndWndId = wndWndId;
	}

//	public void zaznaczPozycje() {
//		SqlDataSelectionsHandler dsh = this.getLW("PPL_DEL_TPOZ_Z_DLG", this.wniosek.getWndStdId());
//
//		for (DataRow dr : dsh.getData()) {
//
//			for (DeltPozycjeKalkulacji poz : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
//
//				if (dr.getIdAsLong().equals(poz.getPklTpozId())) {
//					dsh.setRowSelection(true, dr);
//					break;
//				}
//			}
//		}
//
//	}
	
	
	public void obliczMaxProcentPkl(DeltPozycjeKalkulacji pkl, double nowaWartość, String rodzaj) {
		Double mfProcent = nz(pkl.getPklKosztMFProcent());
		Double zaprProcent = nz(pkl.getPklKosztZaprProcent());
		Double refundacjaProcent = nz(pkl.getPklRefundacjaProcent());
		
		double suma = mfProcent + zaprProcent + refundacjaProcent;
		
		if(suma > 100.0) {
			if("z".equals(rodzaj)) {
				if((100.0 - nowaWartość - refundacjaProcent) >= 0.0)
					pkl.setPklKosztMFProcent(100.0 - nowaWartość - refundacjaProcent);
				else {
					User.alert("Popraw procentowy podział kosztu");
					return;
				}
			} else if("r".equals(rodzaj)) {
				if((100.0 - nowaWartość - zaprProcent) >= 0.0)
					pkl.setPklKosztMFProcent(100.0 - nowaWartość - zaprProcent);
				else {
					User.alert("Popraw procentowy podział kosztu");
					return;
				}
			} else {
				pkl.setPklKosztMFProcent(100.0 - zaprProcent - refundacjaProcent);
				User.alert("Popraw procentowy podział kosztu dla zapraszającego bądź refundacji");
				return;
			}
		}
	}

	// dodaje pozycje kalkulacji o ustalonych typach
	public void dodajPozycjeZZZ() {

		for (DataRow dr : this.getLW("PPL_DEL_TPOZ_Z_DLG", this.wniosek.getWndStdId()).getSelectedRows()) {

//			Boolean czyPowtorzenie = false;
//
//			for (DeltPozycjeKalkulacji pk : kalkulacjaWniosek.getDeltPozycjeKalkulacjis()) {
//
//				if (pk.getPklTpozId().equals(Long.parseLong(dr.get("TPOZ_ID").toString()))) {
//					czyPowtorzenie = true;
//					break;
//
//				}
//
//			}
//
//			if (czyPowtorzenie) {
//				break;
//			}

			DeltPozycjeKalkulacji pkl = new DeltPozycjeKalkulacji();
			pkl.setDeltKalkulacje(kalkulacjaWniosek);

			pkl.setPklTpozId(Long.parseLong(dr.get("TPOZ_ID").toString()));
			this.ustawZFAngaze(pkl);
			pkl.setPklStawka(0D);
//			pkl.setPklId(0L);
			pkl.setPklKwota(0D);
			pkl.setPklWalId(1L);
			pkl.setPklIlosc(0D);
			pkl.setPklKwotaPrzelewu(0L);
			pkl.setPklKwotaGotowki("0");
			
			pkl.setPklKosztMFProcent(100.0);
			pkl.setPklKosztZaprProcent(0.0);
			pkl.setPklRefundacjaProcent(0.0);
			
			if(eq("T",pkl.getKategoria())) {
				DeltSrodkiLokomocji slok = pkl.getDeltKalkulacje().getWniosek().getDeltSrodkiLokomocjis().get(0);
				if(slok != null && (eq("03",slok.getSlokSrodekLokomocji()) || eq("07",slok.getSlokSrodekLokomocji()))) {
					pkl.setPklFormaPlatnosci("04");
				} else 
					pkl.setPklFormaPlatnosci("03");
			}
					
			else if(eq("H",pkl.getKategoria())) {
					pkl.setPklFormaPlatnosci("03");
			}
			else pkl.setPklFormaPlatnosci("04");

			kalkulacjaWniosek.addDeltPozycjeKalkulacji(pkl);
			
		}
		
		kalkulacjaWniosek.getDeltPozycjeKalkulacjis().sort(Comparator.comparing(DeltPozycjeKalkulacji::getLp));
		this.getLW("PPL_DEL_TPOZ_Z_DLG", this.wniosek.getWndStdId()).clearSelection();

	}
	
//	public double ustawKwote (Double kwotaWej, DeltZrodlaFinansowania zf){
//		Double ret = procentNaKwote(zf.getPkzfProcent(), nz(kwotaWej));
//		for ( DelAngaze a : zf.getDelAngazes()){
//			a.setAngKwota(procentNaKwote(a.getAngProcent(), ret));
//		}
//		return ret;
//	}
	
//	public double ustawProcent(Double kwotaWej, DeltZrodlaFinansowania zf) {
//		for ( DelAngaze a : zf.getDelAngazes()){
//			a.setAngKwota(procentNaKwote(a.getAngProcent(), zf.getPkzfKwota()));
//		}
//		return kwotaNaProcent(zf.getPkzfKwota(), kwotaWej);
//	}
	
//	public Double procentNaKwote (Double procent, Double kwotaWej) {
//		kwotaWej = nz(kwotaWej);
//		procent = nz(procent);
//		if(kwotaWej != 0.0)
//		return round(((procent*kwotaWej)/100),2);
//		else return 0.0;
//	}
	
//	public Double kwotaNaProcent (Double kwotaAkt, Double kwotaWej) {
//		if(kwotaWej != 0.0 && kwotaAkt != 0)
//		return round(((100*nz(kwotaAkt))/nz(kwotaWej)),2);
//		else return 0.0;
//	}
	
	public Double sumaKwotZf( DeltPozycjeKalkulacji pkl) {
		Double suma = 0.0;
		for( DeltZrodlaFinansowania zf : pkl.getDeltZrodlaFinansowanias()) {
			if(zf.getPkzfKwota()!=null)
			suma += zf.getPkzfKwota();
		}
		return round(suma, 2);
	}
	
	public Double sumaKwotAngazy( DeltZrodlaFinansowania zf) {
		Double suma = 0.0;
		for(  DelAngaze a : zf.getDelAngazes()) {
			if(a.getAngKwota()!=null)
			suma += a.getAngKwota();
		}
		return round(suma,2);
	}
	
//	public Double getJednostkaKursu(long walId){
//
//		if(walId == 0)
//			return 1.0;
//
//		DataRow walut = this.getLW("PPL_SYMBOLE_WALUT").get(walId);
//
//		if(walut != null && walut.get("WAL_JEDNOSTKA_KURSU")!=null)
//				return ((BigDecimal) walut.get("WAL_JEDNOSTKA_KURSU")).doubleValue();
//			else
//				return null;
//	}
	
//	public  Map<Long, Double> zwrocKwoteDoRozliczen() {
//		Map<Long, Double> listaWalutGotowka =null;
//		listaWalutGotowka  = kalkulacjaRozliczenia.getDeltPozycjeKalkulacjis().stream()
//				.filter(pkl -> "01".equals(pkl.getPklFormaPlatnosci()) ||"04".equals(pkl.getPklFormaPlatnosci()) && pkl.getPklKwotaMf() != 0.0)
//				.collect(Collectors.groupingBy(pkl -> pkl.getPklWalId(), Collectors.summingDouble(pkl -> pkl.getPklKwotaMf())));
//
//		return listaWalutGotowka;
//	}
	
	
//	public double obliczKwotePlnWaluty(DeltKalkulacje kalk, Long walId) {
//
//		if(kalk != null && walId != null) {
//			Double kwotaPln = kalk.getDeltPozycjeKalkulacjis().stream()
//			.filter(pkl -> pkl.getPklWalId() != null && pkl.getPklWalId().equals(walId))
//			.collect(Collectors.summingDouble(pkl -> pkl.obliczKwotePln()));
//			if(kwotaPln != null) return kwotaPln;
//
////			Double kwotaWaluty = kalk.getDeltPozycjeKalkulacjis().stream()
////			.filter(pkl -> pkl.getPklWalId() != null && pkl.getPklWalId().equals(walId))
////			.collect(Collectors.summingDouble(pkl -> pkl.getPklKwotaMf()));
////			if(kwotaWaluty != null)
////				return round( (kwotaWaluty * this.zwrocKursRozliczeniaWaluty(walId)) /this.getJednostkaKursu(walId) , 2) ;
//		}
//
//		return 0.0;
//	}
	
//	public double obliczKwotePlnRozliczenie(DeltKalkulacje kalk, Long walId) {
//
//		if(kalk != null && walId != null) {
//			Double kwotaPln = kalk.getDeltPozycjeKalkulacjis().stream()
//			.filter(pkl -> pkl.getPklWalId() != null && pkl.getPklWalId().equals(walId))
//			.filter(pkl -> "01".equals(pkl.getPklFormaPlatnosci()) || "04".equals(pkl.getPklFormaPlatnosci()))
//			.collect(Collectors.summingDouble(pkl -> pkl.obliczKwotePln()));
//			if(kwotaPln != null) return kwotaPln;
//		}
//
//		return 0.0;
//	}
	
	
	//TODO dokończyć  i wstawić wszędzie na xhtml
	public boolean blokujBtnJesliDpnz(List<DeltPozycjeKalkulacji> listaPkl) {
		
		boolean ret = false;
		List<DeltPozycjeKalkulacji> list = listaPkl.stream()
			.filter(pkl -> pkl.getDeltPklWyplNzs() != null)
			.filter(p -> p.getDeltPklWyplNzs().size() > 0)
			.collect(Collectors.toList());
		
		if (list.size() > 0) {
			ret = true;
		}
		
		return ret;
	}
	
	public double round(double value, int places) {
	    return Utils.round(value, places);
	}

//	public Date getDataKursu() {
//		return dataKursu;
//	}
//
//	public void setDataKursu(Date dataKursu) {
//		this.dataKursu = dataKursu;
//	}

	public Object getWaluta() {
		return waluta;
	}

	public void setWaluta(Object waluta) {
		this.waluta = waluta;
	}

	public SimpleDateFormat getDateFormat() {
		return dateFormat;
	}

	public void setDateFormat(SimpleDateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}

	public DeltKalkulacje getKalkulacjaWstepna() {
		return kalkulacjaWstepna;
	}

	public void setKalkulacjaWstepna(DeltKalkulacje kalkulacjaWstepna) {
		this.kalkulacjaWstepna = kalkulacjaWstepna;
	}



//	public HashMap<Long, Double> getSumyWgWalut() {
//		return sumyWgWalut;
//	}
//
//
//	public void setSumyWgWalut(HashMap<Long, Double> sumyWgWalut) {
//		this.sumyWgWalut = sumyWgWalut;
//	}

//	public Date getDataWyplaty() {
//		return dataWyplaty;
//	}
//
//	public void setDataWyplaty(Date dataWyplaty) {
//		this.dataWyplaty = dataWyplaty;
//	}

	public DeltKalkulacje getKalkulacjaRozliczenia() {
		return kalkulacjaRozliczenia;
	}

	public void setKalkulacjaRozliczenia(DeltKalkulacje kalkulacjaRozliczenia) {
		this.kalkulacjaRozliczenia = kalkulacjaRozliczenia;
	}

	public DeltPozycjeKalkulacji getCurrentPkl() {
		return currentPkl;
	}

	public void setCurrentPkl(DeltPozycjeKalkulacji currentPkl) {
		this.currentPkl = currentPkl;
	}

//	public double getPodsumowaniePln() {
//		return podsumowaniePln;
//	}
//
//	public void setPodsumowaniePln(double podsumowaniePln) {
//		this.podsumowaniePln = podsumowaniePln;
//	}

//	public HashMap<Long, List<DeltZrodlaFinansowania>>  getZfWgWalut() {
//		return zfWgWalut;
//	}
//
//	public void setZfWgWalut(HashMap<Long, List<DeltZrodlaFinansowania>>  zfWgWalut) {
//		this.zfWgWalut = zfWgWalut;
//	}

//	public SqlDataSelectionsHandler getWalutyZaliczki() {
//		Set<Long> collect = wniosek.getDeltCeleDelegacjis().stream().map(c->c.getCdelKrId()).collect(Collectors.toSet());
//		return this.getLW("PPL_KRAJE_WALUTY", ','+this.join(collect, ",") + ',');
//	}

//	public void setWalutyZaliczki(SqlDataSelectionsHandler walutyZaliczki) {
//		this.walutyZaliczki = walutyZaliczki;
//	}

	public int getTabDelegacji() {
		return tabDelegacji;
	}

	public void setTabDelegacji(int tabDelegacji) {
		this.tabDelegacji = tabDelegacji;
	}

//	public HashMap<Long, Double> getSumyWgWalutKalkRoz() {
//		return sumyWgWalutKalkRoz;
//	}
//
//	public void setSumyWgWalutKalkRoz(HashMap<Long, Double> sumyWgWalutKalkRoz) {
//		this.sumyWgWalutKalkRoz = sumyWgWalutKalkRoz;
//	}

//	public HashMap<Long, List<DeltZrodlaFinansowania>> getZfWgWalutKalkRoz() {
//		return zfWgWalutKalkRoz;
//	}
//
//	public void setZfWgWalutKalkRoz(HashMap<Long, List<DeltZrodlaFinansowania>> zfWgWalutKalkRoz) {
//		this.zfWgWalutKalkRoz = zfWgWalutKalkRoz;
//	}

//	public double getPodsumowaniePlnRozl() {
//		return podsumowaniePlnRozl;
//	}
//
//	public void setPodsumowaniePlnRozl(double podsumowaniePlnRozl) {
//		this.podsumowaniePlnRozl = podsumowaniePlnRozl;
//	}

//	public List<DeltZrodlaFinansowania> getZfDoRozliczenia() {
//		return zfDoRozliczenia;
//	}
//
//	public void setZfDoRozliczenia(List<DeltZrodlaFinansowania> zfDoRozliczenia) {
//		this.zfDoRozliczenia = zfDoRozliczenia;
//	}

//	public Boolean getDelWObiegu() {
//		return delWObiegu;
//	}

	public void setDelWObiegu(Boolean delWObiegu) {
		this.delWObiegu = delWObiegu;
	}

	public Boolean getCzyMogeZaliczke() {
		return czyMogeZaliczke;
	}

	public void setCzyMogeZaliczke(Boolean czyMogeZaliczke) {
		this.czyMogeZaliczke = czyMogeZaliczke;
	}

	public Boolean getBlokujRDKM() {
		return blokujRDKM;
	}

	public void setBlokujRDKM(Boolean blokujRDKM) {
		this.blokujRDKM = blokujRDKM;
	}

//	public Boolean getCzyDietaHotelowa() {
//		return czyDietaHotelowa;
//	}
//
//	public void setCzyDietaHotelowa(Boolean czyDietaHotelowa) {
//		this.czyDietaHotelowa = czyDietaHotelowa;
//	}

	public HashMap<Long, Double> getListaKrajCzasPrywaty() {
		return listaKrajCzasPrywaty;
	}

//	public void setListaKrajCzasPrywaty(HashMap<Long, Double> listaKrajCzasPrywaty) {
//		this.listaKrajCzasPrywaty = listaKrajCzasPrywaty;
//	}

//	public int getCzI_ActiveIndex() {
//		return czI_ActiveIndex;
//	}
//
//	public void setCzI_ActiveIndex(int czI_ActiveIndex) {
//		this.czI_ActiveIndex = czI_ActiveIndex;
//	}

//	public int getCzII_ActiveIndex() {
//		return czII_ActiveIndex;
//	}
//
//	public void setCzII_ActiveIndex(int czII_ActiveIndex) {
//		this.czII_ActiveIndex = czII_ActiveIndex;
//	}

//	public int getCzIII_ActiveIndex() {
//		return czIII_ActiveIndex;
//	}
//
//	public void setCzIII_ActiveIndex(int czIII_ActiveIndex) {
//		this.czIII_ActiveIndex = czIII_ActiveIndex;
//	}

//	public Map<Long, Double> getSumyWalutDoRozliczenia() {
//		return sumyWalutDoRozliczenia;
//	}
//
//	public void setSumyWalutDoRozliczenia(Map<Long, Double> sumyWalutDoRozliczenia) {
//		this.sumyWalutDoRozliczenia = sumyWalutDoRozliczenia;
//	}


}
