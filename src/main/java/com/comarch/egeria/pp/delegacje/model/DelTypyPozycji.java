package com.comarch.egeria.pp.delegacje.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_DEL_TYPY_POZYCJI database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_TYPY_POZYCJI")
@NamedQuery(name="DelTypyPozycji.findAll", query="SELECT p FROM DelTypyPozycji p")
public class DelTypyPozycji implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_TYPY_POZYCJI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_TYPY_POZYCJI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_TYPY_POZYCJI")
	@Column(name="TPOZ_ID")
	private long tpozId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TPOZ_AUDYT_DM")
	private Date tpozAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TPOZ_AUDYT_DT", updatable=false)
	private Date tpozAudytDt;

	@Column(name="TPOZ_AUDYT_KM")
	private String tpozAudytKm;

	@Column(name="TPOZ_AUDYT_KT")
	private String tpozAudytKt;

	@Column(name="TPOZ_AUDYT_LM")
	private BigDecimal tpozAudytLm;

	@Column(name="TPOZ_AUDYT_UM")
	private String tpozAudytUm;

	@Column(name="TPOZ_AUDYT_UT", updatable=false)
	private String tpozAudytUt;

	@Column(name="TPOZ_F_ZALICZKA")
	private String tpozFZaliczka="N";

	@Column(name="TPOZ_KATEGORIA")
	private String tpozKategoria;

	@Column(name="TPOZ_KOD")
	private String tpozKod;

	@Column(name="TPOZ_NAZWA")
	private String tpozNazwa;

	@Column(name="TPOZ_OPIS")
	private String tpozOpis;
	
	@Column(name="TPOZ_F_CZY_ZAGR")
	private String tpozFCzyZagr="T";
	
	@Column(name="TPOZ_F_CZY_KRAJ")
	private String tpozFCzyKraj="T";
	
	@Column(name="TPOZ_LP")
	private Long tpozLp;

	@Column(name = "TPOZ_DOMYSLNA_PLATNOSC")
	String tpozDomyslnaPlatnosc = "01";

	
	
	
//	@Column(name="TPOZ_STD_ID")
//	private Long tpozStdId;	
	
	//bi-directional many-to-one association to DelStawkiDelegacji
	@ManyToOne
	@JoinColumn(name="TPOZ_STD_ID")
	private DelStawkiDelegacji delStawkiDelegacji;
	
	//private Long tpozStdId;
	
/*	//bi-directional many-to-one association to DeltPozycjeKalkulacji
	@OneToMany(mappedBy="delTypyPozycji", fetch=FetchType.LAZY, cascade={CascadeType.ALL}, orphanRemoval=true)
	private List<DeltPozycjeKalkulacji> deltPozycjeKalkulacjis = new ArrayList<>();*/

	public DelTypyPozycji() {
	}

	public long getTpozId() {
		return this.tpozId;
	}

	public void setTpozId(long tpozId) {
		this.tpozId = tpozId;
	}

	public Date getTpozAudytDm() {
		return this.tpozAudytDm;
	}

	public void setTpozAudytDm(Date tpozAudytDm) {
		this.tpozAudytDm = tpozAudytDm;
	}

	public Date getTpozAudytDt() {
		return this.tpozAudytDt;
	}

	public void setTpozAudytDt(Date tpozAudytDt) {
		this.tpozAudytDt = tpozAudytDt;
	}

	public String getTpozAudytKm() {
		return this.tpozAudytKm;
	}

	public void setTpozAudytKm(String tpozAudytKm) {
		this.tpozAudytKm = tpozAudytKm;
	}

	public String getTpozAudytKt() {
		return this.tpozAudytKt;
	}

	public void setTpozAudytKt(String tpozAudytKt) {
		this.tpozAudytKt = tpozAudytKt;
	}

	public BigDecimal getTpozAudytLm() {
		return this.tpozAudytLm;
	}

	public void setTpozAudytLm(BigDecimal tpozAudytLm) {
		this.tpozAudytLm = tpozAudytLm;
	}

	public String getTpozAudytUm() {
		return this.tpozAudytUm;
	}

	public void setTpozAudytUm(String tpozAudytUm) {
		this.tpozAudytUm = tpozAudytUm;
	}

	public String getTpozAudytUt() {
		return this.tpozAudytUt;
	}

	public void setTpozAudytUt(String tpozAudytUt) {
		this.tpozAudytUt = tpozAudytUt;
	}

	public String getTpozFZaliczka() {
		return this.tpozFZaliczka;
	}

	public void setTpozFZaliczka(String tpozFZaliczka) {
		this.tpozFZaliczka = tpozFZaliczka;
	}


	public String getTpozKategoria() {
		return this.tpozKategoria;
	}

	public void setTpozKategoria(String tpozKategoria) {
		this.tpozKategoria = tpozKategoria;
	}

	public String getTpozKod() {
		return this.tpozKod;
	}

	public void setTpozKod(String tpozKod) {
		this.tpozKod = tpozKod;
	}

	public String getTpozNazwa() {
		return this.tpozNazwa;
	}

	public void setTpozNazwa(String tpozNazwa) {
		this.tpozNazwa = tpozNazwa;
	}

	public String getTpozOpis() {
		return this.tpozOpis;
	}

	public void setTpozOpis(String tpozOpis) {
		this.tpozOpis = tpozOpis;
	}

//	public Long getTpozStdId() {
//		return tpozStdId;
//	}
//
//	public void setTpozStdId(Long tpozStdId) {
//		this.tpozStdId = tpozStdId;
//	}


	public DelStawkiDelegacji getDelStawkiDelegacji() {
		return this.delStawkiDelegacji;
	}

	public void setDelStawkiDelegacji(DelStawkiDelegacji delStawkiDelegacji) {
		this.delStawkiDelegacji = delStawkiDelegacji;
	}

	public String getTpozFCzyZagr() {
		return tpozFCzyZagr;
	}

	public void setTpozFCzyZagr(String tpozFCzyZagr) {
		this.tpozFCzyZagr = tpozFCzyZagr;
	}

	public String getTpozFCzyKraj() {
		return tpozFCzyKraj;
	}

	public void setTpozFCzyKraj(String tpozFCzyKraj) {
		this.tpozFCzyKraj = tpozFCzyKraj;
	}

	public Long getTpozLp() {
		return tpozLp;
	}

	public void setTpozLp(Long tpozLp) {
		this.tpozLp = tpozLp;
	}

	public String getTpozDomyslnaPlatnosc() {
		return tpozDomyslnaPlatnosc;
	}

	public void setTpozDomyslnaPlatnosc(String tpozDomyslnaPlatnosc) {
		this.tpozDomyslnaPlatnosc = tpozDomyslnaPlatnosc;
	}


	
//	public Long getTpozStdId() {
//		return this.getDelStawkiDelegacji().getStdId() ;
//	}
//
//	public void setTpozStdId(Long tpozStdId) {
//		DelStawkiDelegacji std = (DelStawkiDelegacji) HibernateContext.get(DelStawkiDelegacji.class, tpozStdId);
//		std.addDelTypyPozycji(this);
//	}

}