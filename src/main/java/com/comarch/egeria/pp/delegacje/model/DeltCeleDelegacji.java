package com.comarch.egeria.pp.delegacje.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * The persistent class for the PPT_DEL_CELE_DELEGACJI database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_CELE_DELEGACJI", schema="PPADM")
@NamedQuery(name="DeltCeleDelegacji.findAll", query="SELECT d FROM DeltCeleDelegacji d")
public class DeltCeleDelegacji implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	
	
	public DeltCeleDelegacji clone(){
		DeltCeleDelegacji cel = new DeltCeleDelegacji();
		
		cel.cdelId = cdelId; //!!! Hibernate?
		cel.cdelAudytDm = cdelAudytDm;
		cel.cdelAudytDt = cdelAudytDt;
		cel.cdelAudytKm = cdelAudytKm;
		cel.cdelAudytLm = cdelAudytLm;
		cel.cdelAudytUm = cdelAudytUm;
		cel.cdelAudytUt = cdelAudytUt;
		cel.cdelKrId = cdelKrId;
		cel.cdelMiejscowosc = cdelMiejscowosc;
		cel.deltWnioskiDelegacji = null;//to jest rodzic!!!
		
		
		return cel;
	}
	

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_CELE_DELEGACJI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_CELE_DELEGACJI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_CELE_DELEGACJI")	
	@Column(name="CDEL_ID")
	private long cdelId;

	@Temporal(TemporalType.DATE)
	@Column(name="CDEL_AUDYT_DM")
	private Date cdelAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="CDEL_AUDYT_DT", updatable=false)
	private Date cdelAudytDt;

	@Column(name="CDEL_AUDYT_KM")
	private String cdelAudytKm;

//	@Column(name="CDEL_AUDYT_KT")
//	private String cdelAudytKt;

	@Column(name="CDEL_AUDYT_LM")
	private String cdelAudytLm;

	@Column(name="CDEL_AUDYT_UM")
	private String cdelAudytUm;

	@Column(name="CDEL_AUDYT_UT", updatable=false)
	private String cdelAudytUt;

/*	@Column(name="CDEL_CEL")
	private String cdelCel;*/

/*	@Column(name="CDEL_KATEGORIA")
	private String cdelKategoria;*/

/*	@Column(name="CDEL_KL_ID")
	private Long cdelKlId;*/

/*	@Temporal(TemporalType.DATE)
	@Column(name="CDEL_KONFERENCJA_DO")
	private Date cdelKonferencjaDo;

	@Temporal(TemporalType.DATE)
	@Column(name="CDEL_KONFERENCJA_OD")
	private Date cdelKonferencjaOd;*/

	@Column(name="CDEL_KR_ID")
	private Long cdelKrId;
	
//	@Column(name="CDEL_LP")
//	private Long cdelLp;

	@Column(name="CDEL_MIEJSCOWOSC")
	private String cdelMiejscowosc;
	
	@Column(name="CDEL_MIEJSCOWOSC_OD")
	private String cdelMiejscowoscOd;
	
	@Column(name="CDEL_MIEJSCOWOSC_ZAKONCZENIA")
	private String cdelMiejscowoscZakonczenia;

/*	@Column(name="CDEL_OPIS")
	private String cdelOpis;

	@Column(name="CDEL_OPIS_CELU")
	private String cdelOpisCelu;*/

	//bi-directional many-to-one association to DeltWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="CDEL_WND_ID")
	private DeltWnioskiDelegacji deltWnioskiDelegacji;

	public DeltCeleDelegacji() {
	}

	public long getCdelId() {
		return this.cdelId;
	}

	public void setCdelId(long cdelId) {
		this.cdelId = cdelId;
	}

	public Date getCdelAudytDm() {
		return this.cdelAudytDm;
	}

	public void setCdelAudytDm(Date cdelAudytDm) {
		this.cdelAudytDm = cdelAudytDm;
	}

	public Date getCdelAudytDt() {
		return this.cdelAudytDt;
	}

	public void setCdelAudytDt(Date cdelAudytDt) {
		this.cdelAudytDt = cdelAudytDt;
	}

	public String getCdelAudytKm() {
		return this.cdelAudytKm;
	}

	public void setCdelAudytKm(String cdelAudytKm) {
		this.cdelAudytKm = cdelAudytKm;
	}

//	public String getCdelAudytKt() {
//		return this.cdelAudytKt;
//	}
//
//	public void setCdelAudytKt(String cdelAudytKt) {
//		this.cdelAudytKt = cdelAudytKt;
//	}

	public String getCdelAudytLm() {
		return this.cdelAudytLm;
	}

	public void setCdelAudytLm(String cdelAudytLm) {
		this.cdelAudytLm = cdelAudytLm;
	}

	public String getCdelAudytUm() {
		return this.cdelAudytUm;
	}

	public void setCdelAudytUm(String cdelAudytUm) {
		this.cdelAudytUm = cdelAudytUm;
	}

	public String getCdelAudytUt() {
		return this.cdelAudytUt;
	}

	public void setCdelAudytUt(String cdelAudytUt) {
		this.cdelAudytUt = cdelAudytUt;
	}

//	public String getCdelCel() {
//		return this.cdelCel;
//	}
//
//	public void setCdelCel(String cdelCel) {
//		this.cdelCel = cdelCel;
//	}
//
//	public String getCdelKategoria() {
//		return this.cdelKategoria;
//	}
//
//	public void setCdelKategoria(String cdelKategoria) {
//		this.cdelKategoria = cdelKategoria;
//	}
//
//	public Long getCdelKlId() {
//		return this.cdelKlId;
//	}
//
//	public void setCdelKlId(Long cdelKlId) {
//		this.cdelKlId = cdelKlId;
//	}
//
//	public Date getCdelKonferencjaDo() {
//		return this.cdelKonferencjaDo;
//	}
//
//	public void setCdelKonferencjaDo(Date cdelKonferencjaDo) {
//		this.cdelKonferencjaDo = cdelKonferencjaDo;
//	}
//
//	public Date getCdelKonferencjaOd() {
//		return this.cdelKonferencjaOd;
//	}
//
//	public void setCdelKonferencjaOd(Date cdelKonferencjaOd) {
//		this.cdelKonferencjaOd = cdelKonferencjaOd;
//	}

	public Long getCdelKrId() {
		return this.cdelKrId;
	}

	public void setCdelKrId(Long cdelKrId) {
//		System.out.println(this.cdelMiejscowosc + " / " + cdelKrId);
		this.cdelKrId = cdelKrId;
	}

//	public Long getCdelLp() {
//		return this.cdelLp;
//	}

//	public void setCdelLp(Long cdelLp) {
//		this.cdelLp = cdelLp;
//	}

	public String getCdelMiejscowosc() {
		return this.cdelMiejscowosc;
	}

	public void setCdelMiejscowosc(String cdelMiejscowosc) {
		this.cdelMiejscowosc = cdelMiejscowosc;
	}

//	public String getCdelOpis() {
//		return this.cdelOpis;
//	}
//
//	public void setCdelOpis(String cdelOpis) {
//		this.cdelOpis = cdelOpis;
//	}
//
//	public String getCdelOpisCelu() {
//		return this.cdelOpisCelu;
//	}
//
//	public void setCdelOpisCelu(String cdelOpisCelu) {
//		this.cdelOpisCelu = cdelOpisCelu;
//	}

	public DeltWnioskiDelegacji getDeltWnioskiDelegacji() {
		return this.deltWnioskiDelegacji;
	}

	public void setDeltWnioskiDelegacji(DeltWnioskiDelegacji deltWnioskiDelegacji) {
		this.deltWnioskiDelegacji = deltWnioskiDelegacji;
	}

	public String getCdelMiejscowoscOd() {
		return cdelMiejscowoscOd;
	}

	public void setCdelMiejscowoscOd(String cdelMiejscowoscOd) {
		this.cdelMiejscowoscOd = cdelMiejscowoscOd;
	}

	public String getCdelMiejscowoscZakonczenia() {
		return cdelMiejscowoscZakonczenia;
	}

	public void setCdelMiejscowoscZakonczenia(String cdelMiejscowoscZakonczenia) {
		this.cdelMiejscowoscZakonczenia = cdelMiejscowoscZakonczenia;
	}

}