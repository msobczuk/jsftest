package com.comarch.egeria.pp.delegacje.model;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.KursWaluty;
import com.comarch.egeria.pp.data.model.ModelBase;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static com.comarch.egeria.Utils.*;


/**
 * The persistent class for the PPT_DEL_ZALICZKI database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_ANGAZE")
@NamedQuery(name="DelAngaze.findAll", query="SELECT p FROM DelAngaze p")
public class DelAngaze extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_ANGAZE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_ANGAZE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_ANGAZE")	
	@Column(name="ANG_ID")
	private long angId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ANG_AUDYT_DM")
	private Date angAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="ANG_AUDYT_DT", updatable=false)
	private Date angAudytDt;

	@Column(name="ANG_AUDYT_KM")
	private String angAudytKm;

	@Column(name="ANG_AUDYT_LM")
	private String angAudytLm;

	@Column(name="ANG_AUDYT_UM")
	private String angAudytUm;

	@Column(name="ANG_AUDYT_UT", updatable=false)
	private String angAudytUt;

	@Column(name="ANG_KWOTA")
	private Double angKwota;

	@Column(name="ANG_OPIS")
	private String angOpis;

	@Column(name="ANG_PROCENT")
	private Double angProcent;

	@Column(name="ANG_WNR_ID")
	private Long angWnrId;
	
	@Column(name="ANG_ROZN_ZAOKR_NZ_PLN")
	private double angRoznZaokrNzPln = 0.0;
	
	@Column(name="ANG_ROZN_ZAOKR_DKZ_PLN")
	private double angRoznZaokrDkzPln = 0.0;

	//bi-directional many-to-one association to PptDelWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="ANG_WND_ID")
	private DeltWnioskiDelegacji deltWnioskiDelegacji;

	//bi-directional many-to-one association to PptDelZrodlaFinansowania
	@ManyToOne
	@JoinColumn(name="ANG_PKZF_ID")
	private DeltZrodlaFinansowania deltZrodlaFinansowania;

	public DelAngaze() {
	}
	
	public DelAngaze clone(){
		
		DelAngaze an = new DelAngaze();
		
		an.sqlBean = sqlBean;
		an.angKwota = angKwota;
		an.angProcent = angProcent;
		an.angWnrId = angWnrId;
		
		
		return an;
		
	}

	public long getAngId() {
		return this.angId;
	}

	public void setAngId(long angId) {
		this.angId = angId;
	}

	public Date getAngAudytDm() {
		return this.angAudytDm;
	}

	public void setAngAudytDm(Date angAudytDm) {
		this.angAudytDm = angAudytDm;
	}

	public Date getAngAudytDt() {
		return this.angAudytDt;
	}

	public void setAngAudytDt(Date angAudytDt) {
		this.angAudytDt = angAudytDt;
	}

	public String getAngAudytKm() {
		return this.angAudytKm;
	}

	public void setAngAudytKm(String angAudytKm) {
		this.angAudytKm = angAudytKm;
	}

	public String getAngAudytLm() {
		return this.angAudytLm;
	}

	public void setAngAudytLm(String angAudytLm) {
		this.angAudytLm = angAudytLm;
	}

	public String getAngAudytUm() {
		return this.angAudytUm;
	}

	public void setAngAudytUm(String angAudytUm) {
		this.angAudytUm = angAudytUm;
	}

	public String getAngAudytUt() {
		return this.angAudytUt;
	}

	public void setAngAudytUt(String angAudytUt) {
		this.angAudytUt = angAudytUt;
	}

	public Double getAngKwota() {
	    this.onRXGet("angKwota");
		return this.angKwota;
	}

	public void setAngKwota(Double angKwota) {
		if (angKwota!=null){
			angKwota = Utils.round(angKwota,2);
		}

	    this.onRXSet("angKwota", this.angKwota, angKwota);
		boolean eq = eq(angKwota, this.angKwota);
		this.angKwota = angKwota;
		if(!eq && this.deltZrodlaFinansowania!=null){
			double kwotaZF = nz(this.deltZrodlaFinansowania.getPkzfKwota());

			if (kwotaZF!=0.0)
				this.angProcent = multiply( divide(angKwota, kwotaZF), 100.0, 2);

		}
	}
	

	public String getAngOpis() {
		return this.angOpis;
	}

	public void setAngOpis(String angOpis) {
		this.angOpis = angOpis;
	}

	public Double getAngProcent() {
        this.onRXGet("angProcent");
		return this.angProcent;
	}

	public void setAngProcent(Double angProcent) {
	    this.onRXSet("angProcent", this.angProcent, angProcent);
		boolean eq = eq(angProcent, this.angProcent);
		this.angProcent = angProcent;
		if(!eq && this.deltZrodlaFinansowania!=null){
			//double kwotaZF = nz(this.deltZrodlaFinansowania.getPkzfKwota());
			double angkwota = divide( multiply(this.deltZrodlaFinansowania.getPkzfKwota(), angProcent), 100.0, 2) ;//round(kwotaZF * nz(angProcent) / 100, 2);

			if (Double.isFinite(angkwota))
				this.angKwota = angkwota;
			else
				this.angKwota = 0.0;
		}
	}

	public Long getAngWnrId() {
	    this.onRXGet("angWnrId");
		return this.angWnrId;
	}

	public void setAngWnrId(Long angWnrId) {
	    this.onRXSet("angWnrId", this.angWnrId, angWnrId);
		this.angWnrId = angWnrId;
	}
	
	public double getAngRoznZaokrNzPln() {
	    this.onRXGet("angRoznZaokrNzPln");
		return this.angRoznZaokrNzPln;
	}

	public void setAngRoznZaokrNzPln(double angRoznZaokrNzPln) {
	    this.onRXSet("angRoznZaokrNzPln", this.angRoznZaokrNzPln, angRoznZaokrNzPln);
		this.angRoznZaokrNzPln = angRoznZaokrNzPln;
	}
	
	public double getAngRoznZaokrDkzPln() {
	    this.onRXGet("angRoznZaokrDkzPln");
		return this.angRoznZaokrDkzPln;
	}

	public void setAngRoznZaokrDkzPln(double angRoznZaokrDkzPln) {
	    this.onRXSet("angRoznZaokrDkzPln", this.angRoznZaokrDkzPln, angRoznZaokrDkzPln);
		this.angRoznZaokrDkzPln = angRoznZaokrDkzPln;
	}

	public DeltWnioskiDelegacji getDeltWnioskiDelegacji() {
		return this.deltWnioskiDelegacji;
	}

	public void setDeltWnioskiDelegacji(DeltWnioskiDelegacji deltWnioskiDelegacji) {
		this.deltWnioskiDelegacji = deltWnioskiDelegacji;
	}

	public DeltZrodlaFinansowania getDeltZrodlaFinansowania() {
		return this.deltZrodlaFinansowania;
	}

	public void setDeltZrodlaFinansowania(DeltZrodlaFinansowania deltZrodlaFinansowania) {
		this.deltZrodlaFinansowania = deltZrodlaFinansowania;
	}


	public String zfSortKey(){
		return lpad(nz(this.getAngWnrId()),10) + ":" + lpad(nz(this.getAngProcent()),5);
	}



	public KursWaluty getKurs(){
		if (this.getDeltZrodlaFinansowania()==null)
			return null;

//		if (!isRozrachunekDelegowanego()) {
//			KursWaluty ret = new KursWaluty();
//			ret.setOpis("Nie można określić kursu waluty dla pozycji, które nie stanowią części rozrachunków z pracownikiem (jeśli forma wypłaty to 'umowa z wykonwacą' albo 'karta służobowa')");
//			ret.setPrzelicznik(0.0);
//			return ret;
//		}

		return this.getDeltZrodlaFinansowania().getKurs();
	}


	public Double getKwotaPLN(){

//		if (!isRozrachunekDelegowanego()) {
//			return 0.0;
//		}

		return  this.getKurs().wartoscPLN(this.getAngKwota(), 2);
    }



	public boolean isRozrachunekDelegowanego(){
		if (this.getDeltZrodlaFinansowania()==null) return true;
		return this.getDeltZrodlaFinansowania().isRozrachunekDelegowanego();
	}


    public String getNumerWniosku(){
		return getLW("PPL_DEL_ZAANGAZOWANIE_ZF").findAndFormat(this.getAngWnrId(),"%6$s");
	}


	public DeltKalkulacje getKalkulacja(){
		if (this.getDeltZrodlaFinansowania()==null || this.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji()==null) return null;
		return this.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji().getDeltKalkulacje();
	}

}