package com.comarch.egeria.pp.delegacje.model;

import com.comarch.egeria.Params;
import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.zalaczniki.ZalacznikiBean;
import com.lowagie.text.DocumentException;
import net.sf.jasperreports.engine.JRException;

import javax.persistence.Entity;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import static com.comarch.egeria.Utils.*;

/**
 * logika biznesowa delegacji zagranicznych
 * @author mateusz.bogusz
 */
@Entity
public class WniosekDelegacjiZagr extends DeltWnioskiDelegacji {

//	void test() {
//		
//	}

	@Override
	public SqlDataSelectionsHandler getLwTypyPozycji(){
		return getLW("PPL_DEL_TYPY_POZYCJI_Z");
	}

	@Override
	public SqlDataSelectionsHandler getRodzajeKosztow() {//typy pozycji z uwzglednieniem wybranych stawek MPiPS / FCS
		return getLW("ppl_del_tpoz_z_dlg", this.getWndStdId());
	}


	public  void generujRaportyJakoZalacznikiPdfDlaDokID(Object dokId,  boolean czyRaportPolecenia,  boolean czyRaportRozliczenia,  boolean czyZalacznikDlaNZ_DEL_FK5) throws SQLException, IOException, JRException, DocumentException {
		Params params = new Params("wnd_id", this.getWndId());
		
		if(this.getWndId() == 0)
			return;

		if (czyRaportPolecenia) {
			//ZalacznikiBean.generujRaportPdfZapiszJakoZalacznik("Delegacja_zagraniczna_wniosek", params, "ZAL_DOK_ID", dokId, "raport polecenia");
			ZalacznikiBean.generujRaportPdfZapiszJakoZalacznik("Delegacja_zagraniczna_wniosek", params, "Delegacja_zagraniczna_polecenie.pdf", "ZAL_DOK_ID", dokId ,"raport polecenia");
		}

		if (czyRaportRozliczenia) {
			//ZalacznikiBean.generujRaportPdfZapiszJakoZalacznik("Delegacja_zagraniczna_rozliczenie", params, "ZAL_DOK_ID", dokId, "raport rozliczenia");
			ZalacznikiBean.generujRaportPdfZapiszJakoZalacznik("Delegacja_zagraniczna_rozliczenie", params, "Delegacja_zagraniczna_rozliczenie.pdf", "ZAL_DOK_ID", dokId ,"raport rozliczenie");
		}

		if (czyZalacznikDlaNZ_DEL_FK5) {
			ZalacznikiBean.zapiszZalacznik(com.comarch.egeria.web.common.reports.ReportGenerator.generujSlElToPdfData("DELZ_FK5_EL2PDF"), "DELZ_FK5.pdf", "ZAL_DOK_ID", dokId, "raport polecenia - informacje dodatkowe");
		}

	}

	@Override
	public boolean validate() {
		return super.validate();
	}



	public void ustawStawkeWaluteDietyPKL(DeltPozycjeKalkulacji p, Date pobytOd) {
		Date pobytOdOrToday = nvl(pobytOd, today());
		final SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_STAWKI_DIET_ZAGR", this.getWndStdId());
		if (lw==null) return;

		DataRow dr = lw.getAllData().stream()//szukamy stawki dedykowanej dla kraju
				.filter(r -> eq(p.getPklKrId(), r.getAsLong("WST_KR_ID"))
						&& r.getAsDate("WST_OBOWIAZUJE_OD") != null //niby pole jest not null...
						&& r.getAsDate("WST_OBOWIAZUJE_OD").before(pobytOdOrToday)
				).findFirst().orElse(null);

		if (dr==null)//jesli nie ma stawki dedykowanej dla kraju, szukamy st dla pozostałych (gdzie WST_KR_ID=null)
			dr = lw.getAllData().stream()
					.filter(r -> eq(null, r.get("WST_KR_ID")))
					.findFirst().orElse(null);

		if (dr==null) return;

		Double stawka = nz(dr.getAsDouble("WST_KWOTA"));
		if (eq(p.getKategoria(), "KM")){//jesli to ryczałt komunikacyjny to tylko 10 % stawki diety pobytowej
			stawka = Utils.round(stawka * 0.1,2);
		}

		p.setPklStawka(stawka);
		p.setPklWalId(dr.getAsLong("WST_WAL_ID"));
	}


	public void ustawStawkeiWaluteDietyHotel(DeltPozycjeKalkulacji p, Date pobytOd){
		Date pobytOdOrToday = (pobytOd != null)  ? pobytOd : today();
		final SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_KRAJE_STAWKI", this.getWndStdId(), "H", pobytOdOrToday );
		if (lw==null) return;

		DataRow dr = lw.getAllData().stream()
				.filter(r -> eq(p.getPklKrId(), r.getAsLong("KR_ID"))
				).findFirst().orElse(null);

		if (dr == null)
			dr = lw.getAllData().stream()
					.filter(r -> eq(null, r.getAsLong("KR_ID"))
					).findFirst().orElse(null);

		if (dr==null) return;

		Double stawka = nz(dr.getAsDouble("WST_KWOTA"));

		if(eq("RH",p.getKategoria())) {//jesli to ryczałt hotelowy to tylko 25% stawki hotelowej
			stawka = Utils.round(stawka * 0.25 ,2);
		}

		p.setPklWalId(nz(dr.getAsLong("WST_WAL_ID")));
		p.setPklStawka(stawka);
	}

}
