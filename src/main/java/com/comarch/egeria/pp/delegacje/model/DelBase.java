package com.comarch.egeria.pp.delegacje.model;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.data.model.ModelBase;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import static com.comarch.egeria.Utils.nz;


//klasa z utilsami dla instancji delgacji

public abstract class DelBase extends ModelBase implements Serializable {

	public abstract DeltWnioskiDelegacji getWniosek();


    public String getWalSymbol(Object walId){
        return Utils.getWalSymbol(walId);
    }

    public Long getWalId(String walSymbol){
        return Utils.getWalId(walSymbol);
    }


    public String getKrajSymbol(Object krajId){
        return Utils.getKrajSymbol(krajId);
    }

    public String getKrajNazwa(Object krajId){
        return Utils.getKrajNazwa(krajId);
    }


    public static ConcurrentHashMap<Long, DelTypyPozycji> typyPozycjiCHM = new ConcurrentHashMap<>();

    public DelTypyPozycji getDelTypyPozycjiByTpozId(Long tpozId){
        DelTypyPozycji ret = typyPozycjiCHM.get(nz(tpozId));
        if (ret==null){
            for (DelTypyPozycji tp : (List<DelTypyPozycji>) HibernateContext.query("SELECT p FROM DelTypyPozycji p")) {
                typyPozycjiCHM.put(tp.getTpozId(),tp);
            }
            ret = typyPozycjiCHM.get(nz(tpozId));
        }

        return ret;
    }
    
	public SqlDataSelectionsHandler getSLDelSrodekLokomocji() {
		return getSlownik("DEL_SRODEK_LOKOMOCJI", false, false);
	}


	public SqlDataSelectionsHandler getLwTypyPozycji(){
		return null; //metoda polimorficzna
	}




	public Long getTpozIdByKtgPozycji( String ktgPozycji) {
    	if (this.getWniosek()==null) return null;
    	return this.getWniosek().getTpozIdByKtgPozycji(ktgPozycji);
	}

	public Boolean isKrajowa(){
		if (this.getWniosek()==null) return null;
		return this.getWniosek().isKrajowa();
	}

	public Boolean isZagraniczna(){
		return this.getWniosek().isZagraniczna();
	}



//	//metoda z delegacji mfbean, dodany jeden parametr do wyszczególnienia czy del zagr czy kraj
//	public Long getTpozIdByKtgPozycji( String ktgPozycji, String delRodzaj) {
//
//		if(ktgPozycji == null || ktgPozycji.isEmpty())
//			return 0L;
//		BigDecimal ret = null;
//
//		SqlDataSelectionsHandler typyPozycji = null;
//
//		if(eq("z",delRodzaj.toLowerCase()))
//		typyPozycji = this.getLW("PPL_DEL_TYPY_POZYCJI_Z");
//
//
//		//TODO sprawdzić jak jest dla delegacji krajowych i dodać warunek
//
//		if(typyPozycji != null) {
//			for( DataRow r : typyPozycji.getData()) {
//				if(r.get("tpoz_kategoria").equals(ktgPozycji)) {
//					ret = (BigDecimal) r.get("tpoz_id");
//					if(ret == null) {
//						return 0L;
//					}
//				}
//
//			}
//		}
//		if(ret != null)
//		return ret.longValue();
//		else
//			return null;
//	}
	
	public Double getJednostkaKursu(long walId){
		
		if(walId == 0)
			return 1.0;
		
		DataRow walut = this.getLW("PPL_SYMBOLE_WALUT").get(walId);
		
		if(walut != null && walut.get("WAL_JEDNOSTKA_KURSU")!=null) 
				return ((BigDecimal) walut.get("WAL_JEDNOSTKA_KURSU")).doubleValue();
			else
				return null;		
	}

//	public String getSymbolWalutyKonta(String nrKonta, Long prcId) {
//		SqlDataSelectionsHandler ppl_del_konta_prc = getLW("PPL_DEL_KONTA_PRC", prcId);
//		return ppl_del_konta_prc.findAndFormat(nrKonta, "%wal_symbol$s");
//	}





	public Date obliczDomyslnaDateDlaZaliczki(Date delOd) {

		if(delOd == null)
			return Utils.today();

		Calendar startCal = Calendar.getInstance();
		startCal.setTime(delOd);
		startCal.add(Calendar.DATE, -5);
		int dayOfWeek = startCal.get(Calendar.DAY_OF_WEEK);

		switch (dayOfWeek) {
			case Calendar.SUNDAY:
				startCal.add(Calendar.DATE, -3);
				break;
			case Calendar.MONDAY:
				startCal.add(Calendar.DATE, -4);
				break;
//		default:
//			startCal.add(Calendar.DATE, -2);
//			if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
//				startCal.add(Calendar.DATE, -1);
//			} else if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
//				startCal.add(Calendar.DATE, -2);
//			}
//			break;
		}

		if (startCal.get(Calendar.DAY_OF_MONTH) == 28) {
			switch (dayOfWeek) {
				case Calendar.SUNDAY:
					startCal.add(Calendar.DATE, -2);
					break;
				case Calendar.MONDAY:
					startCal.add(Calendar.DATE, -3);
					break;
				default:
					startCal.add(Calendar.DATE, -1);
					if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
						startCal.add(Calendar.DATE, -1);
					} else if(startCal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
						startCal.add(Calendar.DATE, -2);
					}
					break;
			}
		}

		delOd = startCal.getTime();

		if(delOd.before(Utils.today()))
			delOd = Utils.today();

		return delOd;

	}



	public Long getWalutaWgKraju(Long krajId){
		if(krajId==null)
			return 1L;

		DataRow r = this.getLW("PPL_KRAJE_WALUTY_ALL").find(krajId);

		if(r == null) {
			this.onRXGet("isKrajowa() ? 1L : 50000L");
			return this.isKrajowa() ? 1L : 50000L;
		}

		return r.getAsLong("WAL_ID");
	}
	
	
}
