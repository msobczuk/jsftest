package com.comarch.egeria.pp.delegacje.model;

import java.io.Serializable;
import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_DEL_WARTOSCI_STAWEK database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_WARTOSCI_STAWEK")
@NamedQuery(name="DelWartosciStawek.findAll", query="SELECT p FROM DelWartosciStawek p")
public class DelWartosciStawek implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_WARTOSCI_STAWEK",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_WARTOSCI_STAWEK", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_WARTOSCI_STAWEK")
	@Column(name="WST_ID")
	private long wstId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="WST_AUDYT_DM")
	private Date wstAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="WST_AUDYT_DT", updatable=false)
	private Date wstAudytDt;

	@Column(name="WST_AUDYT_KM")
	private String wstAudytKm;

	@Column(name="WST_AUDYT_KT")
	private String wstAudytKt;

	@Column(name="WST_AUDYT_LM")
	private BigDecimal wstAudytLm;

	@Column(name="WST_AUDYT_UM")
	private String wstAudytUm;

	@Column(name="WST_AUDYT_UT", updatable=false)
	private String wstAudytUt;

	@Column(name="WST_F_KWOTA")
	private String wstFKwota;

	@Column(name="WST_KR_ID")
	private Long wstKrId;

	@Column(name="WST_KWOTA")
	private BigDecimal wstKwota;

	@Temporal(TemporalType.DATE)
	@Column(name="WST_OBOWIAZUJE_OD")
	private Date wstObowiazujeOd;

	@Column(name="WST_PROCENT")
	private BigDecimal wstProcent;

	@ManyToOne
	@JoinColumn(name="WST_STD_ID")
	private DelStawkiDelegacji delStawkiDelegacji;

	@Column(name="WST_STD_ID_OBL")
	private BigDecimal wstStdIdObl;

	@Column(name="WST_WAL_ID")
	private Long wstWalId;
	
	@Column(name="WST_TYP_DIETY")
	private String wstTypDiety;
	

	public DelWartosciStawek() {
	}

	public long getWstId() {
		return this.wstId;
	}

	public void setWstId(long wstId) {
		this.wstId = wstId;
	}

	public Date getWstAudytDm() {
		return this.wstAudytDm;
	}

	public void setWstAudytDm(Date wstAudytDm) {
		this.wstAudytDm = wstAudytDm;
	}

	public Date getWstAudytDt() {
		return this.wstAudytDt;
	}

	public void setWstAudytDt(Date wstAudytDt) {
		this.wstAudytDt = wstAudytDt;
	}

	public String getWstAudytKm() {
		return this.wstAudytKm;
	}

	public void setWstAudytKm(String wstAudytKm) {
		this.wstAudytKm = wstAudytKm;
	}

	public String getWstAudytKt() {
		return this.wstAudytKt;
	}

	public void setWstAudytKt(String wstAudytKt) {
		this.wstAudytKt = wstAudytKt;
	}

	public BigDecimal getWstAudytLm() {
		return this.wstAudytLm;
	}

	public void setWstAudytLm(BigDecimal wstAudytLm) {
		this.wstAudytLm = wstAudytLm;
	}

	public String getWstAudytUm() {
		return this.wstAudytUm;
	}

	public void setWstAudytUm(String wstAudytUm) {
		this.wstAudytUm = wstAudytUm;
	}

	public String getWstAudytUt() {
		return this.wstAudytUt;
	}

	public void setWstAudytUt(String wstAudytUt) {
		this.wstAudytUt = wstAudytUt;
	}

	public String getWstFKwota() {
		return this.wstFKwota;
	}

	public void setWstFKwota(String wstFKwota) {
		this.wstFKwota = wstFKwota;
	}

	public Long getWstKrId() {
		return this.wstKrId;
	}

	public void setWstKrId(Long wstKrId) {
		this.wstKrId = wstKrId;
	}

	public BigDecimal getWstKwota() {
		return this.wstKwota;
	}

	public void setWstKwota(BigDecimal wstKwota) {
		this.wstKwota = wstKwota;
	}

	public Date getWstObowiazujeOd() {
		return this.wstObowiazujeOd;
	}

	public void setWstObowiazujeOd(Date wstObowiazujeOd) {
		this.wstObowiazujeOd = wstObowiazujeOd;
	}

	public BigDecimal getWstProcent() {
		return this.wstProcent;
	}

	public void setWstProcent(BigDecimal wstProcent) {
		this.wstProcent = wstProcent;
	}

/*	public BigDecimal getWstStdId() {
		return this.wstStdId;
	}

	public void setWstStdId(BigDecimal wstStdId) {
		this.wstStdId = wstStdId;
	}*/

	public BigDecimal getWstStdIdObl() {
		return this.wstStdIdObl;
	}

	public void setWstStdIdObl(BigDecimal wstStdIdObl) {
		this.wstStdIdObl = wstStdIdObl;
	}

	public Long getWstWalId() {
		return this.wstWalId;
	}

	public void setWstWalId(Long wstWalId) {
		this.wstWalId = wstWalId;
	}

	public DelStawkiDelegacji getDelStawkiDelegacji() {
		return delStawkiDelegacji;
	}

	public void setDelStawkiDelegacji(DelStawkiDelegacji delStawkiDelegacji) {
		this.delStawkiDelegacji = delStawkiDelegacji;
	}

	public String getWstTypDiety() {
		return wstTypDiety;
	}

	public void setWstTypDiety(String wstTypDiety) {
		this.wstTypDiety = wstTypDiety;
	}

}