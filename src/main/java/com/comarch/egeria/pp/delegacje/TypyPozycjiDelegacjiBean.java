package com.comarch.egeria.pp.delegacje;

import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.delegacje.model.DelBase;
import com.comarch.egeria.pp.delegacje.model.DelStawkiDelegacji;
import com.comarch.egeria.pp.delegacje.model.DelTypyPozycji;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@ManagedBean
@Scope("view")
@Named
public class TypyPozycjiDelegacjiBean extends SqlBean implements Serializable {
	private static final Logger log = LogManager.getLogger();

	@Inject
	SessionBean sessionBean;


	private DelTypyPozycji delTypyPozycji;
	
	
	@PostConstruct
	private void init() {	
		String id = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
		
		if (id !=null )
				this.setDelTypyPozycji((DelTypyPozycji) HibernateContext.get(DelTypyPozycji.class,Long.parseLong(""+id)));
		else {
			this.setDelTypyPozycji(new DelTypyPozycji());
			this.setTpozStdDomyslna();
		}
	}
	
	private boolean validateBeforeSave() {
		ArrayList<String> inval = new ArrayList<String>();
		
		if (delTypyPozycji.getTpozKod() == null || delTypyPozycji.getTpozKod().isEmpty()) {
			inval.add("Kod typu pozycji nie może być pusty!");
		}
		
		if (delTypyPozycji.getTpozNazwa() == null || delTypyPozycji.getTpozNazwa().isEmpty()) {
			inval.add("Nazwa typu pozycji nie może być pusta!");
		}
		
		if (delTypyPozycji.getTpozKategoria() == null || delTypyPozycji.getTpozKategoria().isEmpty()) {
			inval.add("Kategoria typu pozycji nie może być pusta!");
		}
		
		if(delTypyPozycji.getTpozKategoria().equals("P") || delTypyPozycji.getTpozKategoria().equals("H"))
			if(delTypyPozycji.getDelStawkiDelegacji()==null )
				inval.add("Dla diet hotelowych lub pobytowych wybór stawki jest wymagany!");

		
		for (String msg : inval) {
			User.alert(msg);
		}

		return inval.isEmpty();
	}
	
	public void zapisz(){
		
		if (!validateBeforeSave()) {
			return;
		}
		
		try {
			
			this.delTypyPozycji = (DelTypyPozycji) HibernateContext.save(this.delTypyPozycji);
			DelBase.typyPozycjiCHM.clear();
			User.info("Poprawnie zapisono dane"); 
			
		} catch (Exception e) {
			User.info("Nie zapisano danych"); 
			log.error(e.getMessage(), e);
			
		}		
	}
	
	public void usun() throws IOException {
		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			s.delete(this.delTypyPozycji);
			s.beginTransaction().commit();
			DelBase.typyPozycjiCHM.clear();
		}
		FacesContext.getCurrentInstance().getExternalContext().redirect("TypyPozycjiDelegacjiLista");
	}

	public DelTypyPozycji getDelTypyPozycji() {
		return delTypyPozycji;
	}

	public void setDelTypyPozycji(DelTypyPozycji delTypyPozycji) {
		this.delTypyPozycji = delTypyPozycji;
	}

	public Long getTpozStdId() {
		
		if (this.delTypyPozycji.getDelStawkiDelegacji()==null)
			return null;
		
		return this.delTypyPozycji.getDelStawkiDelegacji().getStdId() ;
	}

	public void setTpozStdId(Long tpozStdId) {
		if (this.delTypyPozycji.getDelStawkiDelegacji()!=null) {
			this.delTypyPozycji.getDelStawkiDelegacji().removeDelTypyPozycji(this.delTypyPozycji);
		}

		if (tpozStdId==null) {
			setTpozStdDomyslna();
		} else {
			DelStawkiDelegacji std = (DelStawkiDelegacji) HibernateContext.get(DelStawkiDelegacji.class, tpozStdId);
			std.addDelTypyPozycji(this.delTypyPozycji);
		}
	}

	public void setTpozStdDomyslna() {
		List<DelStawkiDelegacji> lst = HibernateContext.query("from DelStawkiDelegacji where STD_KOD_STAWKI = 'Domyslna'");
		if (!lst.isEmpty()){
			lst.get(0).addDelTypyPozycji(this.delTypyPozycji);
		}
	}

}
