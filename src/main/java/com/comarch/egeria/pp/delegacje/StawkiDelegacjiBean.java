package com.comarch.egeria.pp.delegacje;

import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.delegacje.model.DelStawkiDelegacji;
import com.comarch.egeria.pp.delegacje.model.DelWartosciStawek;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@ManagedBean
@Scope("view")
@Named
public class StawkiDelegacjiBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LogManager.getLogger();

	@Inject
	SessionBean sessionBean;
	
	private DelStawkiDelegacji delStawkiDelegacji;
	DataRow dataRow;

	private DelWartosciStawek currentDelWartosciStawek;
	
	
	
	private String filterTxtH = "";
	private String filterTxtP = "";
	private int filteredSizeP;
	private int filteredSizeH;
	private int sizeP;
	private int sizeH;
	
//	Date obowiazujeOdDataDomyslna = null;//jesli wprowadzamy kolejne stawki to chcemy podpowiadac poprzednio wprowadzona date
	
	
	@PostConstruct
	private void init() {	
		
		String id = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");
			
		if (id !=null )
				this.setDelStawkiDelegacji((DelStawkiDelegacji) HibernateContext.get(DelStawkiDelegacji.class,Long.parseLong(""+id)));
		else 
				this.setDelStawkiDelegacji(new DelStawkiDelegacji());
		
		
		this.setFilterTxtP(""); 
		this.setFilterTxtH("");	
		
		
		if(this.delStawkiDelegacji.getDelWartosciStaweksInnePobytowe().isEmpty())
		{
			DelWartosciStawek sP=new DelWartosciStawek();
			sP.setWstTypDiety("P");
			sP.setWstWalId(50000L);
			sP.setWstKwota(new BigDecimal(41));
			sP.setWstFKwota("T");
			sP.setWstObowiazujeOd(new Date());
			delStawkiDelegacji.addDelWartosciStawek(sP);
		}
			
		
		
		
		if(this.delStawkiDelegacji.getDelWartosciStaweksInneHotelowe().isEmpty())
		{
			DelWartosciStawek sH=new DelWartosciStawek();
			sH.setWstTypDiety("H");
			sH.setWstWalId(50000L);
			sH.setWstKwota(new BigDecimal(41));
			sH.setWstFKwota("T");
			sH.setWstObowiazujeOd(new Date());
			delStawkiDelegacji.addDelWartosciStawek(sH);
		}

	}
	
	
	
	public void addDelWartosciStawek(){
		currentDelWartosciStawek.setWstFKwota("T");
		delStawkiDelegacji.addDelWartosciStawek(currentDelWartosciStawek);
	
		if ("H".equals(currentDelWartosciStawek.getWstTypDiety() ))
			this.sizeH++;
		
		if ("P".equals(currentDelWartosciStawek.getWstTypDiety() ))
			this.sizeP++;
		
		this.setFilterTxtH( this.getFilterTxtH() );
		this.setFilterTxtP( this.getFilterTxtP() );
	}
	
	
	public void newDelWartosciStawekH(){
		DelWartosciStawek ret = new DelWartosciStawek();
		ret.setWstTypDiety("H");
		if (currentDelWartosciStawek!=null){
			ret.setWstObowiazujeOd(currentDelWartosciStawek.getWstObowiazujeOd());
			ret.setWstWalId(currentDelWartosciStawek.getWstWalId());
		}
		currentDelWartosciStawek = ret;
		com.comarch.egeria.Utils.update("dlgWartoscStawkiEdycjaId");
	}
	
	public void newDelWartosciStawekP(){
		DelWartosciStawek ret = new DelWartosciStawek();
		ret.setWstTypDiety("P");
		if (currentDelWartosciStawek!=null){
			ret.setWstObowiazujeOd(currentDelWartosciStawek.getWstObowiazujeOd());
			ret.setWstWalId(currentDelWartosciStawek.getWstWalId());
		}
		currentDelWartosciStawek = ret;
		com.comarch.egeria.Utils.update("dlgWartoscStawkiEdycjaId");
	}
	
	public void removeDelWartosciStawek(DelWartosciStawek delWartosciStawek){
		
		if ("H".equals(delWartosciStawek.getWstTypDiety() ))
			this.sizeH--;
		
		if ("P".equals(delWartosciStawek.getWstTypDiety() ))
			this.sizeP--;
		
		delStawkiDelegacji.removeDelWartosciStawek(delWartosciStawek);
	}
	
	
	
	public void zapisz() {

		if (!validateBeforeSave()) {
			return;
		}

		try {
			
			this.delStawkiDelegacji = (DelStawkiDelegacji) HibernateContext.save(this.delStawkiDelegacji);
			User.info("Udany zapis stawki delegacji");

		} catch (Exception e) {
			log.error(e.getStackTrace());

			if (getCause(e.getCause()) != null) {

				if (getCause(e.getCause()).getMessage().contains("PPC_STD_UK_KOD")) {
					User.alert("Nie można utworzyć dwóch stawek delegacji o tym samym kodzie!");
					return;
				}

				if (getCause(e.getCause()).getMessage().contains("PPC_STD_UK_NAZWA")) {
					User.alert("Nie można utworzyć dwóch stawek delegacji o takiej samej nazwie!");
					return;
				}
			}

			User.alert("Błąd w zapisie stawki!"+e.getMessage(), e.getMessage());
			return;
		}
	}
	
	
	public String getKraj(Long krajId){
		DataRow r = this.getLW("PPL_WSP_KRAJE").get(krajId);
		if (r==null)
			return null;
		
		String ret = r.format("%2$s");
		return ret;
	}
	
	
	public String getWaluta(Long walId){
		DataRow r = this.getLW("PPL_SYMBOLE_WALUT").get(walId);
		if (r==null)
			return null;

		String ret = r.format("%2$s");
		return ret;
	}
	
	
	
	
	
	//////////////////////////////////////// filtrowanie: ////////////////////////////////////////////////////////////////
	
	private List<DelWartosciStawek> wstListP;// = filter(this.delStawkiDelegacji.getDelWartosciStaweks(), "P", "") ;
	private List<DelWartosciStawek> wstListH;// = filter(this.delStawkiDelegacji.getDelWartosciStaweks(), "H", "") ;
	
//	public void doFilterP(){
//		setWstListP(filter(this.getWstListP(), "P", this.getFilterTxtP()));
//		this.filteredSizeP = wstListP.size();
//	}

	
//	public void doFilterH(){
//		setWstListH(filter(this.getWstListH(), "H", this.getFilterTxtH()));
//		this.filteredSizeH = wstListH.size();
//	}
//	
	
	public List<DelWartosciStawek> filter(List<DelWartosciStawek> wstList, String typ, String txt){
		
		if (wstList==null) 
			return null;
		try {
			
			final String txtL = (""+txt).toLowerCase();
			
			List<DelWartosciStawek> ret = wstList.stream().filter( w-> w.getWstKrId()!=null).collect(Collectors.toList());
			
			ret = ret.stream()
					
					.filter(w -> 
					w.getWstTypDiety().equals(typ)
					 &&	 (
						     ("" + getKraj( w.getWstKrId() )).toLowerCase().contains (txtL)
						  || ("" + getWaluta( w.getWstWalId() )).toLowerCase().contains (txtL)
						  || ("" + w.getWstKwota() ).contains (txtL)
						  || ("" + w.getWstObowiazujeOd() ).contains (txtL)
					     )
					  )
					.sorted((w1, w2)->Collator.getInstance(new Locale("pl","PL")).compare((""+getKraj( w1.getWstKrId() )), (""+getKraj( w2.getWstKrId() ))))
					.collect(Collectors.toList());
			
			return ret;
			
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}

		return null;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	Throwable getCause(Throwable e) {
		Throwable cause = null;
		Throwable result = e;

		while (null != (cause = result.getCause()) && (result != cause)) {
			result = cause;
		}
		return result;
	}
	
	private boolean validateBeforeSave() {
		ArrayList<String> inval = new ArrayList<String>();

		if (delStawkiDelegacji.getStdKodStawki() == null ||delStawkiDelegacji.getStdKodStawki().isEmpty()) {
			inval.add("Kod stawki nie może być pusty!");
		}

		if (delStawkiDelegacji.getStdNazwaStawki() == null ||delStawkiDelegacji.getStdNazwaStawki().isEmpty()) {
			inval.add("Nazwa stawki nie może być pusty!");
		}
		
		if (delStawkiDelegacji.getStdStanDefinicji() == null ||delStawkiDelegacji.getStdStanDefinicji().isEmpty()) {
			inval.add("Stan definicji stawki nie może być pusty!");
		}
		else
			if(delStawkiDelegacji.getStdStanDefinicji().length()>1)
				inval.add("Stan definicji stawki musi być jednym znakiem!");
		
		if (delStawkiDelegacji.getDelWartosciStaweks()!=null || !delStawkiDelegacji.getDelWartosciStaweks().isEmpty())
			for(DelWartosciStawek ws: delStawkiDelegacji.getDelWartosciStaweks()){
				
				/*if(ws.getWstKrId()==null)
					inval.add("Każda wartość stawki musi mieć zdefiniowany kraj!");*/
				
				if(ws.getWstObowiazujeOd()==null)
					inval.add("Każda wartość stawki musi mieć zdefiniowaną date od której obowiązuje!");
				
				if(ws.getWstKwota()==null)
					inval.add("Każda wartość stawki musi mieć zdefiniowaną kwote!");
				
				if(ws.getWstWalId()==null)
					inval.add("Każda wartość stawki musi mieć zdefiniowaną walute!");
					
			}
			
		
		

		
		if(dataRow != null){
			ArrayList<DataRow> data = dataRow.getSqlData().getData();
			for (DataRow d : data) {
				
				if(d.get("STD_KOD_STAWKI")==delStawkiDelegacji.getStdKodStawki()) 
					inval.add("Nie można utworzyć dwóch stawek delegacji o tym samym kodzie!");
				
				if(d.get("STD_NAZWA_STAWKI")==delStawkiDelegacji.getStdNazwaStawki()) 
					inval.add("Nie można utworzyć dwóch stawek delegacji o takiej samej nazwie!");
				
			}	
		}
		
		for (String msg : inval) {
			User.alert(msg);
		}

		return inval.isEmpty();
	}
	
	
	public void usun() throws IOException {
		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			s.delete(delStawkiDelegacji);
			s.beginTransaction().commit();
		}
		FacesContext.getCurrentInstance().getExternalContext().redirect("StawkiDelegacji");
	}
	


	public DelStawkiDelegacji getDelStawkiDelegacji() {
		return delStawkiDelegacji;
	}

	public void setDelStawkiDelegacji(DelStawkiDelegacji delStawkiDelegacji) {
		this.delStawkiDelegacji = delStawkiDelegacji;
	}


	public DelWartosciStawek getCurrentDelWartosciStawek() {
		return currentDelWartosciStawek;
	}


	public void setCurrentDelWartosciStawek(DelWartosciStawek currentDelWartosciStawek) {
		this.currentDelWartosciStawek = currentDelWartosciStawek;
	}



	public String getFilterTxtH() {
		return filterTxtH;
	}



	public void deleteFilterTxtH() {
		this.setFilterTxtH(null);
	}
	
	public void deleteFilterTxtP() {
		this.setFilterTxtP(null);
	}


	public int getFilteredSizeP() {
		return filteredSizeP;
	}


	public void setFilteredSize(int filteredSizeP) {
		this.filteredSizeP = filteredSizeP;
	}



	public String getFilterTxtP() {
		return filterTxtP;
	}



	public void setFilterTxtP(String filterTxtP) {
		if (filterTxtP==null)
			filterTxtP="";

		setWstListP(filter(this.delStawkiDelegacji.getDelWartosciStaweks(), "P", filterTxtP)); 

		
		if("".equals(filterTxtP))
			this.setSizeP(wstListP.size());
		
		this.filteredSizeP = wstListP.size();


		this.filterTxtP = filterTxtP;
	}
	
	public void setFilterTxtH(String filterTxtH) {
		if (filterTxtH==null)
			filterTxtH="";
		
		setWstListH(filter(this.delStawkiDelegacji.getDelWartosciStaweks(), "H", filterTxtH));
		
		if("".equals(filterTxtH))
			this.setSizeH(wstListH.size());
		
		this.filteredSizeH = wstListH.size();
		
		
		this.filterTxtH = filterTxtH;
		
	}
		



	public int getFilteredSizeH() {
		return filteredSizeH;
	}



	public void setFilteredSizeH(int filteredSizeH) {
		this.filteredSizeH = filteredSizeH;
	}



	public List<DelWartosciStawek> getWstListP() {
		return wstListP;
	}



	public void setWstListP(List<DelWartosciStawek> wstListP) {
		this.wstListP = wstListP;
	}



	public List<DelWartosciStawek> getWstListH() {
		return wstListH;
	}



	public void setWstListH(List<DelWartosciStawek> wstListH) {
		this.wstListH = wstListH;
	}



	public int getSizeP() {
		return sizeP;
	}



	public void setSizeP(int sizeP) {
		this.sizeP = sizeP;
	}



	public int getSizeH() {
		return sizeH;
	}



	public void setSizeH(int sizeH) {
		this.sizeH = sizeH;
	}
	

}
