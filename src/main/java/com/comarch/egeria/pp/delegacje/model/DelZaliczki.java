package com.comarch.egeria.pp.delegacje.model;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.*;


/**
 * The persistent class for the PPT_DEL_ZALICZKI database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_ZALICZKI")
@NamedQuery(name="DelZaliczki.findAll", query="SELECT d FROM DelZaliczki d")
public class DelZaliczki extends DelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_ZALICZKI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_ZALICZKI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_ZALICZKI")
	@Column(name="DKZ_ID")
	private long dkzId;

	@Column(name="DKZ_FORMA_WYPLATY")
	private String dkzFormaWyplaty;
	
	@ManyToOne
	@JoinColumn(name="DKZ_KAL_ID")
	private DeltKalkulacje deltKalkulacje;

	@Column(name="DKZ_KWOTA")
	private Double dkzKwota;

	@Column(name="DKZ_OPIS")
	private String dkzOpis;

	@Column(name="DKZ_WAL_ID")
	private Long dkzWalId;
	
	
	@Column(name="DKZ_AUDYT_UT", updatable=false)
	private String dkzAudytUt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DKZ_AUDYT_DT", updatable=false)
	private Date dkzAudytDt;
	
	@Column(name="DKZ_AUDYT_UM")
	private String dkzAudytUm;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="DKZ_AUDYT_DM")
	private Date dkzAudytDm;
	
	@Column(name="DKZ_AUDYT_LM")
	private BigDecimal dkzAudytLm;

	@Column(name="DKZ_AUDYT_KM")
	private String dkzAudytKm;
	
	@Temporal(TemporalType.DATE)
	@Column(name="DKZ_DATA_WYP")
	private Date dkzDataWyp;
	
	@Column(name="DKZ_DOK_ID")
	private Long dkzDokId;
	
	@Column(name="DKZ_NR_KONTA")
	private String dkzNrKonta;
	
	@Column(name="DKZ_KARTA_KWOTA")
	private Double dkzKartaKwota;
	
	@Column(name="DKZ_ZAL_WYDANO")
	private Double dkzZalWydano;
	
	@Column(name="DKZ_SRODKI_WLASNE")
	private Double dkzSrodkiWlasne;
	
	@Column(name="DKZ_KURS")
	private Double dkzKurs = 1.0;
	
	@Column(name="DKZ_CZY_ROWNOWARTOSC")
	private String dkzCzyRownowartosc;
	
	@ManyToOne
	@JoinColumn(name="DKZ_ZWR_DKZ_ID")
	private DelZaliczki delZaliczkaZWR;
	
	@Column(name="DKZ_F_DOK_NZ")
	private String dkzFDokNz;


	//bi-directional many-to-one association to DeltPklWyplNz
	@OneToMany(mappedBy="delZaliczki")//, kaskada tylko z PKL cascade={CascadeType.ALL}, orphanRemoval=true
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<DeltZrodlaFinansowania> deltZrodlaFinansowanias = new ArrayList<>();
	

	
	public List<DeltZrodlaFinansowania> getDeltZrodlaFinansowaniasDlaKalkulacji(DeltKalkulacje kal) {
		return this.getDeltZrodlaFinansowanias().stream()
				.filter(zf-> zf.getDelZaliczki()!=null && eq(kal, zf.getDelZaliczki().getDeltKalkulacje()))//tylko zwiazane ze wskazana kalkulacja
				.collect(Collectors.toList());
	}

	
	
	public List<DeltZrodlaFinansowania> getDeltZrodlaFinansowanias() {
		this.onRXGet("deltZrodlaFinansowanias");
		return this.deltZrodlaFinansowanias;
	}

	public void setDeltZrodlaFinansowanias(List<DeltZrodlaFinansowania> deltZrodlaFinansowanias) {
		this.onRXSet("deltZrodlaFinansowanias", this.deltZrodlaFinansowanias, deltZrodlaFinansowanias);
		this.deltZrodlaFinansowanias = deltZrodlaFinansowanias;
	}

	public DeltZrodlaFinansowania addDeltZrodlaFinansowania(DeltZrodlaFinansowania deltZrodlaFinansowania) {
		this.onRXSet("deltZrodlaFinansowanias", null, deltZrodlaFinansowanias);
		getDeltZrodlaFinansowanias().add(deltZrodlaFinansowania);
		deltZrodlaFinansowania.setDelZaliczki(this);

		return deltZrodlaFinansowania;
	}

	public DeltZrodlaFinansowania removeDeltZrodlaFinansowania(DeltZrodlaFinansowania deltZrodlaFinansowania) {
		this.onRXSet("deltZrodlaFinansowanias", null, deltZrodlaFinansowanias);
		getDeltZrodlaFinansowanias().remove(deltZrodlaFinansowania);
		deltZrodlaFinansowania.setDelZaliczki(null);

		return deltZrodlaFinansowania;
	}






	//bi-directional many-to-one association to DeltPklWyplNz
	@OneToMany(mappedBy="delZaliczki",  cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<DeltPklWyplNz> deltPklWyplNzs = new ArrayList<>();

	public List<DeltPklWyplNz> getDeltPklWyplNzs() {
		this.onRXGet("deltPklWyplNzs");
		return this.deltPklWyplNzs;
	}

	public void setDeltPklWyplNzs(List<DeltPklWyplNz> deltPklWyplNzs) {
		this.onRXSet("deltPklWyplNzs", this.deltPklWyplNzs, deltPklWyplNzs);
		this.deltPklWyplNzs = deltPklWyplNzs;
	}

	public DeltPklWyplNz addDeltPklWyplNz(DeltPklWyplNz deltPklWyplNz) {
		this.onRXSet("deltPklWyplNzs", null, deltPklWyplNzs);
		getDeltPklWyplNzs().add(deltPklWyplNz);
		deltPklWyplNz.setDelZaliczki(this);

		return deltPklWyplNz;
	}

	public DeltPklWyplNz removeDeltPklWyplNz(DeltPklWyplNz deltPklWyplNz) {
		this.onRXSet("deltPklWyplNzs", null, deltPklWyplNzs);
		getDeltPklWyplNzs().remove(deltPklWyplNz);
		deltPklWyplNz.setDeltPozycjeKalkulacji(null);

		return deltPklWyplNz;
	}
	
	@OneToMany(mappedBy="delZaliczkaZWR") //, cascade={CascadeType.ALL}, orphanRemoval=true)
//	@OneToMany(mappedBy="delZaliczkaZWR", cascade={CascadeType.ALL}, orphanRemoval=true)
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("DKZ_ID ASC")
	private List<DelZaliczki> delZaliczkiZwr = new ArrayList<>();
	
	
	public List<DelZaliczki> getDelZaliczkiZwr() {
		this.onRXGet("delZaliczkiZwr");
		return this.delZaliczkiZwr;
	}

	public void setDelZaliczkiZwr(List<DelZaliczki> delZaliczkiZwr) {
		this.onRXSet("delZaliczkiZwr", this.delZaliczkiZwr, delZaliczkiZwr);
		this.delZaliczkiZwr = delZaliczkiZwr;
	}
	
	public DelZaliczki addDelZaliczkiZWR(DelZaliczki delZaliczki) {
		this.onRXSet("delZaliczki", null, delZaliczki);
		getDelZaliczkiZwr().add(delZaliczki);
		delZaliczki.setDelZaliczkaZWR(this);

		return delZaliczki;
	}
	
	public DelZaliczki removeDelZaliczkiZWR(DelZaliczki delZaliczki) {
		this.onRXSet("delZaliczki", null, delZaliczki);
		getDelZaliczkiZwr().remove(delZaliczki);
		delZaliczki.setDelZaliczkaZWR(null);

		return delZaliczki;
	}
	
	
	





	
	
	
	


	public DelZaliczki() {
	}
	
	public DelZaliczki clone(){
		
		DelZaliczki zal = new DelZaliczki();

		zal.sqlBean = sqlBean;
		zal.dkzId = 0L;
		zal.dkzFormaWyplaty = dkzFormaWyplaty;
		zal.dkzKwota = dkzKwota;
		zal.dkzOpis = dkzOpis;
		zal.dkzWalId = dkzWalId;
		zal.dkzDokId = dkzDokId;
		zal.dkzNrKonta = dkzNrKonta;
		zal.dkzDataWyp = dkzDataWyp;
		zal.dkzKurs = dkzKurs;
		zal.dkzCzyRownowartosc = dkzCzyRownowartosc;
		
		return zal;
		
	}

	public long getDkzId() {
		this.onRXGet("dkzId");
		return this.dkzId;
	}

	public void setDkzId(long dkzId) {
		this.onRXSet("dkzId", this.dkzId, dkzId);
		this.dkzId = dkzId;
	}

	public String getDkzFormaWyplaty() {
		this.onRXGet("dkzFormaWyplaty");
		return this.dkzFormaWyplaty;
	}

	public void setDkzFormaWyplaty(String dkzFormaWyplaty) {
		this.onRXSet("dkzFormaWyplaty", this.dkzFormaWyplaty, dkzFormaWyplaty);
		this.dkzFormaWyplaty = dkzFormaWyplaty;
	}


	public boolean isFormaWyplatyGotowka(){
		return eq("K",this.getDkzFormaWyplaty());
	}
	public boolean isFormaWyplatyPrzelew(){
		return eq("B",this.getDkzFormaWyplaty());
	}


	public Double getDkzKwota() {
		this.onRXGet("dkzKwota");
		return this.dkzKwota;
	}

	public void setDkzKwota(Double dkzKwota) {
		this.onRXSet("dkzKwota", this.dkzKwota, dkzKwota);
		boolean eq = eq(this.dkzKwota, dkzKwota);
		boolean zmniejszono = false;
		if(nz(this.dkzKwota) < 0.0) {
			zmniejszono = nz(this.dkzKwota * -1) > nz(dkzKwota * -1);
		} else {
			zmniejszono = nz(this.dkzKwota) > nz(dkzKwota);
		}
		this.dkzKwota = dkzKwota;
		if (zmniejszono && this.getDeltKalkulacje()!=null && !this.getDeltZrodlaFinansowanias().isEmpty()){//wywalic zf (za duże) i na nowo poprawnie przypisac
			List<DeltZrodlaFinansowania> zfl = this.getDeltZrodlaFinansowanias().stream().collect(Collectors.toList());
			for (DeltZrodlaFinansowania zf : zfl) {
				this.removeDeltZrodlaFinansowania(zf);
			}
			this.getDeltKalkulacje().przypiszZFDoWyplat(this);

			if (this.dkzDokId==null) {
				//zmniejszono - chcemy od razu wydzielic resztę do nowej DKZ...

				//chwilowe blokowanie zaliczek poza NZ
				this.getDeltKalkulacje()
						.getDelZaliczki()
						.stream()
						.filter(z-> z.getDkzDokId() == null)
						.forEach(z->z.dkzDokId = -999999999L);

//				this.dkzDokId = -1L;
				this.getDeltKalkulacje().przeliczPodzialZFvsDKZ();
//				this.dkzDokId = null;

				//odblokowanie zablowkowanych wczesniej DKZ
				this.getDeltKalkulacje()
						.getDelZaliczki()
						.stream()
						.filter(z-> eq(z.getDkzDokId(),-999999999L))
						.forEach(z->z.dkzDokId = null);
			}
		}
	}

	public String getDkzOpis() {
		this.onRXGet("dkzOpis");
		return this.dkzOpis;
	}

	public void setDkzOpis(String dkzOpis) {
		this.onRXSet("dkzOpis", this.dkzOpis, dkzOpis);
		this.dkzOpis = dkzOpis;
	}

	public Long getDkzWalId() {
		this.onRXGet("dkzWalId");
		return this.dkzWalId;
	}

	public void setDkzWalId(Long dkzWalId) {
		this.onRXSet("dkzWalId", this.dkzWalId, dkzWalId);
		this.dkzWalId = dkzWalId;
		
		/*this.setDkzKwota(this.getDkzKwota());
		this.setDkzKartaKwota(this.getDkzKartaKwota());
		this.setDkzSrodkiWlasne(this.getDkzSrodkiWlasne());
		this.setDkzZalWydano(this.getDkzZalWydano());*/
	}

	public DeltKalkulacje getDeltKalkulacje() {
		this.onRXGet("deltKalkulacje");
		return deltKalkulacje;
	}

	public void setDeltKalkulacje(DeltKalkulacje deltKalkulacje) {
		this.onRXSet("deltKalkulacje", this.deltKalkulacje, deltKalkulacje);
		this.deltKalkulacje = deltKalkulacje;
	}

	public String getDkzAudytUt() {
		return dkzAudytUt;
	}

	public void setDkzAudytUt(String dkzAudytUt) {
		this.dkzAudytUt = dkzAudytUt;
	}

	public Date getDkzAudytDt() {
		return dkzAudytDt;
	}

	public void setDkzAudytDt(Date dkzAudytDt) {
		this.dkzAudytDt = dkzAudytDt;
	}

	public String getDkzAudytUm() {
		return dkzAudytUm;
	}

	public void setDkzAudytUm(String dkzAudytUm) {
		this.dkzAudytUm = dkzAudytUm;
	}

	public Date getDkzAudytDm() {
		return dkzAudytDm;
	}

	public void setDkzAudytDm(Date dkzAudytDm) {
		this.dkzAudytDm = dkzAudytDm;
	}

	public BigDecimal getDkzAudytLm() {
		return dkzAudytLm;
	}

	public void setDkzAudytLm(BigDecimal dkzAudytLm) {
		this.dkzAudytLm = dkzAudytLm;
	}

	public String getDkzAudytKm() {
		return dkzAudytKm;
	}

	public void setDkzAudytKm(String dkzAudytKm) {
		this.dkzAudytKm = dkzAudytKm;
	}

	public Date getDkzDataWyp() {
		this.onRXGet("dkzDataWyp");
		return dkzDataWyp;
	}

	public void setDkzDataWyp(Date dkzDataWyp) {
		this.onRXSet("dkzDataWyp", this.dkzDataWyp, dkzDataWyp);
		this.dkzDataWyp = dkzDataWyp;
	}

	public Long getDkzDokId() {
		this.onRXGet("dkzDokId");
		return dkzDokId;
	}

	public void setDkzDokId(Long dkzDokId) {
		this.onRXSet("dkzDokId", this.dkzDokId, dkzDokId);
		this.dkzDokId = dkzDokId;
	}

	public String getDkzNrKonta() {
		this.onRXGet("dkzNrKonta");
		return dkzNrKonta;
	}

	public void setDkzNrKonta(String dkzNrKonta) {
		this.onRXSet("dkzNrKonta", this.dkzNrKonta, dkzNrKonta);
		this.dkzNrKonta = dkzNrKonta;
		
		sprawdzNrKontaRownowartosc();
	}

	public Double getDkzKartaKwota() {
		this.onRXGet("dkzKartaKwota");
		return dkzKartaKwota;
	}

	public void setDkzKartaKwota(Double dkzKartaKwota) {
		this.onRXSet("dkzKartaKwota", this.dkzKartaKwota, dkzKartaKwota);
		this.dkzKartaKwota = dkzKartaKwota;
	}

	public Double getDkzZalWydano() {
		this.onRXGet("dkzZalWydano");
		return dkzZalWydano;
	}

	public void setDkzZalWydano(Double dkzZalWydano) {
		this.onRXSet("dkzZalWydano", this.dkzZalWydano, dkzZalWydano);
		this.dkzZalWydano = dkzZalWydano;
	}

	public Double getDkzSrodkiWlasne() {
		this.onRXGet("dkzSrodkiWlasne");
		return dkzSrodkiWlasne;
	}

	public void setDkzSrodkiWlasne(Double dkzSrodkiWlasne) {
		this.onRXSet("dkzSrodkiWlasne", this.dkzSrodkiWlasne, dkzSrodkiWlasne);
		this.dkzSrodkiWlasne = dkzSrodkiWlasne;
	}

	

	public Double getDkzKurs() {
		
		this.onRXGet("dkzKurs");
		if(this.dkzKurs != null) {
			return dkzKurs;
		}
		else {
			return this.getKursWstepny().przelicznik;
		}
	}

	public void setDkzKurs(Double dkzKurs) {
		this.onRXSet("dkzKurs", this.dkzKurs, dkzKurs);
		this.dkzKurs = dkzKurs;
	}

	public String getDkzCzyRownowartosc() {
		this.onRXGet("dkzCzyRownowartosc");
		return dkzCzyRownowartosc;
	}

	public void setDkzCzyRownowartosc(String dkzCzyRownowartosc) {
		this.onRXSet("dkzCzyRownowartosc", this.dkzCzyRownowartosc, dkzCzyRownowartosc);
		

		String old = this.dkzCzyRownowartosc;
		this.dkzCzyRownowartosc = dkzCzyRownowartosc;

		
		if (this.getDeltKalkulacje()!=null && !this.getDeltKalkulacje().validateDataKursu()) {
			this.dkzCzyRownowartosc = old;
			return;
		}
		sprawdzNrKontaRownowartosc();
		
		if(this.getDeltKalkulacje() == null || this.getDkzWalId() == null)
			return;
		Date kalDataKursu = this.getDeltKalkulacje().getKalDataKursu();
//		if(eq("T", dkzCzyRownowartosc)) {
//			if(kalDataKursu != null) {
//				this.setDkzKurs(new KursWalutySredniNBP(this.getDkzWalId(), kalDataKursu).przelicznik);
//			}
//		} else
			this.setDkzKurs(new KursWalutySprzedazyNBP(this.getDkzWalId(), kalDataKursu).przelicznik);
		
//		ustawKursSredni();
	}
	
	public DelZaliczki getDelZaliczkaZWR() {
		this.onRXGet("delZaliczkaZWR");
		return delZaliczkaZWR;
	}

	public void setDelZaliczkaZWR(DelZaliczki delZaliczkaZWR) {
		this.onRXSet("delZaliczkaZWR", this.delZaliczkaZWR, delZaliczkaZWR);
		final boolean eq = eq(this.delZaliczkaZWR, delZaliczkaZWR);
		this.delZaliczkaZWR = delZaliczkaZWR;

		if (!eq && delZaliczkaZWR!=null){
			final KursWaluty kursDokNZ = delZaliczkaZWR.getKursDokNZ();
			if (kursDokNZ!=null) this.setDkzKurs(kursDokNZ.przelicznik); //PT 263793
		}
	}
	
	public String getDkzFDokNz() {
		this.onRXGet("dkzFDokNz");
		return dkzFDokNz;
	}

	public void setDkzFDokNz(String dkzFDokNz) {
		this.onRXSet("dkzFDokNz", this.dkzFDokNz, dkzFDokNz);
		this.dkzFDokNz = dkzFDokNz;
	}

	
//	private void ustawKursSredni() {
//
//		if(this.getDeltKalkulacje() == null || this.getDkzDokId() == null)
//			return;
//
//		if(eq("T", this.getDkzCzyRownowartosc()) && this.getDkzWalId() != null && this.getDkzWalId() != 1L) {
//			DelegacjaMFBean delBean = (DelegacjaMFBean) this.getDeltKalkulacje().getDeltWnioskiDelegacji().getSqlBean();
//			Double kursPrzelicznikSredniNBP = delBean.kursPrzelicznikSredniNBP(this.getDkzWalId(), this.deltKalkulacje.getKalDataKursu());
//			if(kursPrzelicznikSredniNBP != null)
//			this.setDkzKurs(kursPrzelicznikSredniNBP);
//			else delBean.zwrocKursRozliczeniaWaluty(this.getDkzWalId());
//		} else if(this.getDkzWalId() != null && this.getDkzWalId() != 1L) {
//			DelegacjaMFBean delBean = (DelegacjaMFBean) this.getDeltKalkulacje().getDeltWnioskiDelegacji().getSqlBean();
//			this.setDkzKurs(delBean.zwrocKursRozliczeniaWaluty(this.getDkzWalId()));
//		}
//	}

	private void sprawdzNrKontaRownowartosc() {
		if (this.getDeltKalkulacje() == null
			|| isKrajowa() //równowartosc jest tylko na zagranicznych
			|| !eq("B", this.getDkzFormaWyplaty())//nie bank - czyli gotówka
			|| nz(this.getDkzNrKonta()).trim().isEmpty() //nie ma jescze konta
		)
			return;

		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_KONTA_PRC");
		if(eq(this.getDeltKalkulacje().getKalRodzaj(), "ROZ") && this.getDkzKwota() < 0.0) {
			lw = this.getLW("PPL_DEL_KONTA_MF");
		}

		String kontoWalSymbol = nz(lw.findAndFormat(this.getDkzNrKonta(), "%wal_symbol$s"));
		final boolean walutyZgodne = eq(this.getWalSymbol(), kontoWalSymbol);
		final boolean kontoPLN = eq("PLN", kontoWalSymbol);
		final boolean rownowartosc = eq("T", this.getDkzCzyRownowartosc());

		if(walutyZgodne && rownowartosc) {//konto PLN waluta PLN ...  albo np. konto EUR i waluta EUR ... nie ma po co stosowac Równowartość
			this.setDkzCzyRownowartosc("N");
			User.warn("Wybrano numer konta prowadzony w tej samej walucie.\n\nOdznaczono \"Równowartość\"");
		} else if (kontoPLN && !walutyZgodne && !rownowartosc) { //konto PLN, waluta EUR... trzeba zaznaczyc Równowartość
			this.setDkzCzyRownowartosc("T");
			User.warn("Wybrano numer konta prowadzony w innej walucie.\n\nZaznaczono \"Równowartość\"");
		} else if (!kontoPLN && !walutyZgodne) {//nie mozna wybrac konta EUR i np. waluty USD na DKZ bo nie mamy kursów i przeliczników USD<->EUR
			this.setDkzNrKonta("");
			User.alert("Wybrano numer konta prowadzony w walucie niezgodnej z walutą dyspozycji przelewu. " +
					"Rachunek powinien byc prowadzony w walucie PLN albo jego waluta powinna być zgodna z walutą dyspozycji.");
		}


//					if(eq("T", this.getDkzCzyRownowartosc())) {
//						if(!eq("PLN", kontoWalSymbol)) {
//							this.setDkzNrKonta("");
//							User.alert("W przypadku zaznaczenia równowartości, należy wybrać nr konta prowadzony w walucie PLN");
//						}
//					} else {
//						if(!eq(this.getWalSymbol(), kontoWalSymbol)) {
//							this.setDkzNrKonta("");
//							User.alert("Wybrano numer konta prowadzony w walucie niezgodnej z walutą zaliczki");
//						}
//					}
	}







//	public List<DeltPozycjeKalkulacji> getDpnzPozycjeKalkulacji(){ //lista PKL powiazanych przez DPNZ
//		List<DeltPozycjeKalkulacji> ret = this.getDeltPklWyplNzs()
//				.stream()
//				.filter(dpnz -> dpnz.getDeltPozycjeKalkulacji()!=null)
//				.map(dpnz -> dpnz.getDeltPozycjeKalkulacji())
//				.distinct()
//				.collect(Collectors.toList());
//		return ret;
//	}
//
//	public List<DeltPozycjeKalkulacji> getPotencjalneDpnzPozycjeKalkulacji(){//lista PKL mozliwych do powiazania przez DPNZ
//		List<DeltPozycjeKalkulacji> ret = this.getDeltKalkulacje().getDeltPozycjeKalkulacjis()
//				.stream()
//				.filter(pkl -> eq(pkl.getPklWalId(), this.dkzWalId) && pkl.getPozaKwotaWalDpnzDkz() != 0.0)
//				.collect(Collectors.toList());
//		return ret;
//	}

//	public double getSumaKwotaWalDpnzPkl(){//jaka czesc kowty zaliczki jest powiazana z pozycjami PKL przez DPNZ
//		return this.getDeltPklWyplNzs().stream().mapToDouble( dpnz-> nz(dpnz.getDpnzKwotaWal()) ).sum();
//	}
//	public double getPozaKwotaWalDpnzPkl(){//jaka czesc kowty zaliczki NIE jest powiazana z pozycjami PKL przez DPNZ (docelowo tu ma byc 0.0, albo czerwonym po oczach...)
//		return nz(this.getDkzKwota()) - this.getSumaKwotaWalDpnzPkl();
//	}





	public KursWaluty getKurs(){
		KursWaluty kurs = null;

		Date kalDataKursu = this.getDkzDataWyp();
		if (this.getDeltKalkulacje()!=null) {
			kalDataKursu = this.getDeltKalkulacje().getKalDataKursu();
		}

		if (kalDataKursu==null) kalDataKursu = Utils.today();

		if (nz(this.getDkzWalId())==1) {//PLN
			kurs = new KursWaluty(1L, kalDataKursu);
			return kurs;
		}

		if (this.getDkzDokId()==null || this.getCzyDokAnulowany()) { //brak dok. w NZ -> NBP - kurs sprzedazy (rownowartosc) na dzien z kalkulacji ...

			if (nz(this.getDkzKwota()) < 0.0 && this.getDelZaliczkaZWR()!=null){
				return this.getDelZaliczkaZWR().getKursDokNZ();
			}

			kurs = new KursWalutySprzedazyNBP(this.getDkzWalId(), kalDataKursu);

		}else { //jest dok. w NZ -> kurs wg dokumentu NZ (kgt_dokumenty.dok_kurs)
			kurs = getKursDokNZ();
		}

		return kurs;
	}


	public KursWaluty getKursDokNZ(){
		KursWaluty kurs = new KursWaluty(this.getDkzWalId(), new Date());
		SqlDataSelectionsHandler lwDkzKursy = getLW("PPL_DEL_KURS_DKZ_DOK", this.getDeltKalkulacje().getDeltWnioskiDelegacji().getWndId());
		DataRow r = lwDkzKursy.find(this.getDkzId());
		if(r != null) {
			kurs.setPrzelicznik(r.getAsDouble("dok_kurs"));
			Date dataKursuWgNZ = r.getAsDate("dok_data_kursu");
			if (dataKursuWgNZ==null) dataKursuWgNZ = r.getAsDate("dok_data_operacji");
			if (dataKursuWgNZ==null) dataKursuWgNZ = r.getAsDate("dok_data_wystawienia");
			kurs.setDataKursu(dataKursuWgNZ);
		} else {
			User.alert("LW - PPL_DEL_KURS_DKZ_DOK - Brak kursy dla wskazanej waluty");
			kurs.setPrzelicznik(1.0);
		}
		kurs.setOpis("Kurs dokumentu NZ " + this.getDkzDokId());
		kurs.setDokId(this.getDkzDokId());
		return kurs;
	}

	public KursWaluty getKursWstepny(){
		KursWaluty kurs = null;
		
		Date kalDataKursu = this.getDkzDataWyp();
		if (this.getDeltKalkulacje()!=null) {
			kalDataKursu = this.getDeltKalkulacje().getKalDataKursu();
		}
		
//		if(this.getDkzKurs() != null) {
//			kurs.setPrzelicznik(this.getDkzKurs());
//			kurs.setOpis("Kurs wstępny zaliczki z dnia " + kalDataKursu);
//		} else {
		if (kalDataKursu==null) kalDataKursu = Utils.today();


		if (eqAny(this.getDkzWalId(), null, 0L, 1L)) { //if (eq(this.getDkzWalId(),1L) || eq(nz(this.getDkzWalId()), 0L)) {//PLN
			kurs = new KursWaluty(1L, kalDataKursu);
			return kurs;
		}
		
		
//		if (eq("T",this.getDkzCzyRownowartosc())) {
//			kurs = new KursWalutySredniNBP(this.getDkzWalId(), kalDataKursu);
//		} else {
			kurs = new KursWalutySprzedazyNBP(this.getDkzWalId(), kalDataKursu);
//		}
//		}
			
		return kurs;
			
	}


//	public double getKwota(){
//		return nz(this.getDkzKwota()) - nz(this.do)
//	}

	public double getKwotaPLN(){
		return nz(this.getKurs().wartoscPLN(this.getDkzKwota()));
	}
	
	public double getKwotaPlnWstepna() {
		
		KursWaluty kurs = new KursWaluty();
		
		if(this.getDkzKurs() != null) {
			kurs.setPrzelicznik(this.getDkzKurs());
			kurs.setWalId(this.getDkzWalId());
		} 
//		else {
//			if(eq("T", this.getDkzCzyRownowartosc())){
//				kurs = new KursWalutySredniNBP(this.getDkzWalId(), this.getDeltKalkulacje().getKalDataKursu());
//			} else 
//				kurs = new KursWalutySprzedazyNBP(this.getDkzWalId(), this.getDeltKalkulacje().getKalDataKursu());
//		}
//		
		return nz(kurs.wartoscPLN(this.getDkzKwota()));
	}


	public String getWalSymbol(){
		return this.getWalSymbol(this.getDkzWalId());
	}

    public List<DelAngaze> getAngaze(DeltKalkulacje kal){
		final String kalRodzaj = kal.getKalRodzaj();
		final List<DelAngaze> ret = this.getAngaze().stream()
				.filter(a -> a.getKalkulacja() != null
						&& eq(kalRodzaj, a.getKalkulacja().getKalRodzaj()))
				.collect(Collectors.toList());
		return ret;
    }


	public List<DelAngaze> getAngaze(){
		ArrayList<DelAngaze> ret = new ArrayList<>();
		this.getDeltZrodlaFinansowanias().stream().forEach(zf->ret.addAll(zf.getDelAngazes()));
		return ret;
	}

	public List<DelAngaze> getAngazeDlaZwrotu(){//angaze dla dokumentu DKZ zwracajacego czesc lub calosc wyplaconej wczesiej zaliczki/zaliczek
		if (nz(this.getDkzKwota())>=0)
			return null; //jesli to nie jest zwrot (czyli dkz_kwota to liczbą dodatnia)

		if (this.getDeltKalkulacje()==null || this.getDeltKalkulacje().getDeltWnioskiDelegacji()==null)
			return null;
		
		List<DelAngaze> ret = new ArrayList<>();

		this.getDeltKalkulacje().getDeltWnioskiDelegacji().getDokumentyDKZwNZ().stream()
				.filter(z->eq(z.getDkzWalId(), this.getDkzWalId()) && nz(z.getDkzKwota()) > 0.0)
				.forEach(z->ret.addAll(z.getAngaze()));

		return ret;
	}


	public String podsSortKey(){
		return 	  lpad(this.getWalSymbol(),5)
				+ lpad(nz(this.getDkzDokId()),10,'Z')
				+ lpad(nz(this.getDkzId()),10)
        ;
	}


	public boolean getCzyZatwierdzona(){
		if (this.getDeltKalkulacje()==null)
			return false;

		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.getDeltKalkulacje().getKalId());
		if (lw==null)
			return false;

		DataRow r = lw.find(this.getDkzDokId());
		if (r==null)
			return false;

		return ("T".equals(r.get("dok_f_zatwierdzony")) );
	}
	
	public boolean getCzyDokAnulowany(){
		if (this.getDeltKalkulacje()==null)
			return false;

		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.getDeltKalkulacje().getKalId());
		if (lw==null)
			return false;

		DataRow r = lw.find(this.getDkzDokId());
		if (r==null)
			return false;

		return ("T".equals(r.get("dok_f_anulowany")) );
	}




	public Date getNzDokDataOperacji(){
		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.getDeltKalkulacje().getKalId());
		DataRow dataRow = lw.find(this.getDkzDokId());
		
		if(dataRow != null) {
			return (Date) dataRow.get("dok_data_operacji");
		} 
		
		return null;
	}

	public Double getNzDokKwotaWaluty(){
		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.getDeltKalkulacje().getKalId());
		DataRow dataRow = lw.find(this.getDkzDokId());

		if(dataRow != null) {
			if(this.getDkzKwota() > 0.0)
				return dataRow.getAsDouble("dok_kwota_waluty");
			else return -1 * dataRow.getAsDouble("dok_kwota_waluty");
		}

		return null;
	}
	
	public String getNzDokStatus(){
		if (this.getDeltKalkulacje()==null || this.getDeltKalkulacje().getKalId()==0l) return "";
		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.getDeltKalkulacje().getKalId());
		return nz(lw.findAndFormat(this.getDkzDokId(), "%status$s"));
	}
	
	public String getNzDokKolor(){
		if (this.getDeltKalkulacje()==null || this.getDeltKalkulacje().getKalId()==0l) return "";
		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.getDeltKalkulacje().getKalId());
		return nz(lw.findAndFormat(this.getDkzDokId(), "%kolor$s"));
	}
	
	public Double getNzDokKwotaPln(){
		if (this.getDeltKalkulacje()==null || this.getDeltKalkulacje().getKalId()==0l) return null;

		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.getDeltKalkulacje().getKalId());
		DataRow dataRow = lw.find(this.getDkzDokId());
		
		if(dataRow != null) {
			return ((BigDecimal) dataRow.get("dok_kwota")).doubleValue();
		} 
		
		return null;
	}
	
	//jeśli znana jest kwota pln z dok id to ją zwraca w innym przypadku kwota pln jest obliczana
	public Double getKwotaPlnDokDkz () {
		Double ret = 0.0;
		
		if(this.getNzDokKwotaPln() != null) {
			ret = nz(this.getNzDokKwotaPln());
			if(this.getDkzKwota() < 0)
				ret*=-1;
		} else ret = this.getKwotaPLN();

		return ret;
	}




	public void usunDokNZ() {

		if(this.getDkzDokId() != null) {
			try (HibernateContext h = new HibernateContext()) {

				//JdbcUtils.sqlSPCall(h.getConnection(), "PPADM.ppp_del_zaliczki.usun_zal", this.getDkzDokId());
				JdbcUtils.sqlSPCall(h.getConnection(), "PPADM.PPP_INTEGRACJA_FIN.USUN_DOKUMENT_NZ", this.getDkzDokId());
				this.setDkzDokId(null);//wymuszenie RX'a
				HibernateContext.refresh(h,this);
				this.onRXSet("dkzDokId", null, this.dkzDokId);//wymuszenie updateów RX
			} catch (Exception e) {
				User.alert(e);
			}
		}


	}


	public void przeliczPodzialZFvsDKZ(){
	    if (this.getDeltKalkulacje()!=null) {
            this.getDeltKalkulacje().przeliczPodzialZFvsDKZ();
        }
    }
	
	public double getKwotaWyplacana() {
		return nz(nz(this.getDkzKwota()) + this.getKwotaZwracana());//uwaga - kwota zwracana jest ujemna, dlatego +
	}
	
	public double getKwotaZwracana() {
		return this.getDelZaliczkiZwr().stream().mapToDouble(z -> nz(z.getDkzKwota())).sum();
	}
	
	
	
//public void generujDokumentNZ() {
//
//	if (this.getDeltKalkulacje()==null || this.getDeltKalkulacje().getDeltWnioskiDelegacji()==null) return;
//
//	if(this.getDkzId() == 0) {
//		User.alert("Zapisz wniosek przed generacją dokumentu");
//		return;
//	}
//
//
//	DeltWnioskiDelegacji wniosek = this.getDeltKalkulacje().getDeltWnioskiDelegacji();
//
//	if(eq(null,this.getDkzDataWyp())){
//		 User.alert("Nie podano daty wypłaty zaliczki");
//		 return;
//	}
//	if( eq(null,this.getDkzKwota()) || this.getDkzKwota() == 0) {
//		User.alert("Nie można wnioskować o zerową zaliczkę");
//		return;
//	}
//
//	if(nz(this.getDkzFormaWyplaty()).isEmpty()){
//		User.alert("Podaj formę rozliczenia");
//		return;
//	}
//
//	if( "B".equals(this.getDkzFormaWyplaty()) && nz(this.getDkzNrKonta()).isEmpty()) {
//		User.alert("Podaj nr konta");
//		return;
//	}
//	List<DeltZrodlaFinansowania> zflist = this.getDeltZrodlaFinansowanias();
//
//	//Sztucznie jeszcze raz wymuszam przypisanie zf dla tego dkz jeśli ma pusto
//	if(zflist == null || zflist.isEmpty()) {
//		this.getDeltKalkulacje().przypiszZFDoWyplat(this);
//		zflist = this.getDeltZrodlaFinansowanias();
//	}
//	//jeśli mimo powyższego dalej jest pusto to trzeba przerwać akcję
//	if(zflist == null || zflist.isEmpty()) {
//		User.alert("Nie przypisano źródeł finansowania do dokumentu NZ, sprawdź dane");
//		return;
//	} else {
//		double sumaZF = zflist.stream().mapToDouble(zf -> zf.getPkzfKwota()).sum();
//		if(sumaZF < this.getDkzKwota()) {
//			User.alert("Kwota nie została w pełni rozpisana na źródła finansowania, sprawdź dane");
//			return;
//		}
//	}
//
//	if( this.getDkzKurs() == null || this.getDkzKurs() == 0.0) {
//		if(eq(1L,this.dkzWalId)) {
//			this.setDkzKurs(1.0);
//		} else
//		this.setDkzKurs(this.getKurs().przelicznik);
//	}
//
////	Zastanowic się czy ma sens?
//	if(wniosek.getWndId() == 0) {
//		wniosek.zapisz();
//	}
//		try (HibernateContext h = new HibernateContext())  {
//
//			Connection conn = h.getConnection();
//
//			String pWplataWyplata = this.getDkzKwota() >0.0 ? "WY" : "WP";
//
//			String rodzajDokFin = JdbcUtils.sqlFnVarchar(conn, "PPADM.PPP_INTEGRACJA_FIN.f_zwroc_rodzaj_dokumentu",
//					nz(this.getDkzFormaWyplaty()).toUpperCase() , pWplataWyplata, this.getDkzWalId());
//
//
//			BigDecimal dokId = JdbcUtils.sqlFnBigDecimal(conn, "PPADM.PPP_INTEGRACJA_FIN.WSTAW_KPKW_POPW",
//								    Math.abs(this.getDkzKwota()),
//								    	this.getDkzKurs(),
//								    	this.getDeltKalkulacje().getKalRodzaj().equals("ROZ") ? "rozliczenie":"wypłata zaliczki",
//										rodzajDokFin,
//										this.getDkzWalId(),
//										this.getDkzNrKonta(),
//										wniosek.getWndPrcId(),
//										wniosek.getWndId(),
//										this.getDkzDataWyp(), //dataWyplaty,
//										this.getDeltKalkulacje().getKalRodzaj(),
//										this.getDkzId()
//								);
//
//			getLW("PPL_DEL_KURS_DKZ_DOK").resetTs();
//
//			for (DeltZrodlaFinansowania zf : zflist) {
//				for (DelAngaze ang : zf.getDelAngazes()) {
//					BigDecimal podzialID = JdbcUtils.sqlFnBigDecimal(conn,
//							"PPADM.PPP_INTEGRACJA_FIN.f_wstaw_lub_akt_psk"
//							, dokId, zf.getPkzfSkId(), Math.abs(ang.getKwotaPLN()), Utils.asLocalDate(wniosek.getWndDataWyjazdu()).getYear(), ang.getAngWnrId());
//				}
//			}
//
//			JdbcUtils.sqlSPCall(conn, "PPADM.PPP_INTEGRACJA_FIN.oblicz_procenty_psk", dokId);
//
//			if (dokId!=null) {
//
//				this.setDkzDokId(dokId.longValue());//wymuszanie RX
//
//				wniosek.zapisz(h);
//
//				if(eq("WST", this.getDeltKalkulacje().getKalRodzaj()))
//					wniosek.generujRaportyJakoZalacznikiPdfDlaDokID(dokId, true, false, true);
//				else
//					wniosek.generujRaportyJakoZalacznikiPdfDlaDokID(dokId, true, true, true);
//
//				this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.getDeltKalkulacje().getKalId()).resetTs();
//			}
//			else
//				this.setDkzDokId(null);
//
//		} catch (Exception e) {
//			User.alert(e);
//		}
//
//		przeliczPodzialZFvsDKZ(); //this.getDeltKalkulacje().clearDelZaliczkiReszta();
//	}


	public void generujDokumentNZ(HibernateContext h) throws SQLException {

		Connection conn = h.getConnection();
		Long dokId = JdbcUtils.sqlFnLong(conn, "PPADM.PPP_INTEGRACJA_FIN.GENERUJ_DOKUMENT_NZ", this.getDkzId());
		HibernateContext.refresh(h, this);//wczytanie m.in. nowego DKZ_DOK_ID / ale tez audytu itp...
		this.dkzDokId = dokId;//gdyby refresh nie wczytal zmian
		this.onRXSet("dkzDokId", null, this.dkzDokId);//wymuszenie updateów RX
		User.info("EGR/NZ: wygenerowano dok. " + dokId);
		this.getLW("PPL_DEL_KURS_DKZ_DOK").resetTs();
		this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.getDeltKalkulacje().getKalId()).resetTs();

		//przeliczPodzialZFvsDKZ();!!!!nie //this.getDeltKalkulacje().clearDelZaliczkiReszta();
	}


	public String getRodzajDyspozycji() {
		String ret = "";
		
		if(nz(this.getDkzKwota())>0) {
			ret = "B".equals(nz(this.getDkzFormaWyplaty())) ? "PP":"KW";
		} else 
			ret = "B".equals(nz(this.getDkzFormaWyplaty())) ? "PO":"KP";
		
		return ret;
	}
	
	public String getDyspozycjaTitle(String rodzaj) {
		String ret = "";
		
		switch(rodzaj) {
		
			case "PP" :
				ret = "wygeneruj w NZ dyspozycję PP (Polecenie Przelewu)";
				break;
			
			case "PO" :
				ret = "wygeneruj w NZ dyspozycję PO (Przelew Obcy)";
				break;
				
			case "KP" :
				ret = "wygeneruj w NZ dyspozycję KP (Kasa Przyjmie)";
				break;
				
			case "KW" :
				ret = "wygeneruj w NZ dyspozycję KW (Kasa Wypłaci)";
				break;
		}
		
		
		return ret;
	}


	public void ustawDomyslnaFormeWyplatyWgZaliczekWnioskuCzI(){
		if (this.getDeltKalkulacje()==null || this.getDeltKalkulacje().getDeltWnioskiDelegacji()==null)
			return;

		DeltWnioskiDelegacji wniosek = this.getDeltKalkulacje().getDeltWnioskiDelegacji();
		
		if(wniosek.getWndFZaliczka() == null || eq("N",wniosek.getWndFZaliczka()))
			return;

		if (nz(this.getDkzKwota())>0.0){
			//jesli to wypłata!
			DelZaliczki zalWnk = wniosek.getKalkulacjaWniosek().getDelZaliczki().stream()
					.filter(z -> eq(z.getDkzWalId(), this.getDkzWalId()) || z.getDkzWalId() == null)
					.findFirst().orElse(null);

			if (zalWnk != null) {
				this.setDkzFormaWyplaty(zalWnk.getDkzFormaWyplaty());
				this.setDkzCzyRownowartosc(zalWnk.getDkzCzyRownowartosc());
				this.setDkzNrKonta(zalWnk.getDkzNrKonta());
//			this.setDkzDataWyp(checkDate(wniosek.getWndDataWyjazdu()));
			}
		} else {//mniej niż 0 (lub 0) - generowany jest zwrot kasy...
			if (this.getDkzFormaWyplaty()==null) {
				this.setDkzFormaWyplaty("01"); //na razie domyslnie dla zwrotów jest gotówka
			}
		}
	}


	public void removeAllDelZaliczkiZWR(boolean deleteKalkulacjaZwrDkz){
		List<DelZaliczki> zwrl = this.getDelZaliczkiZwr().stream().collect(Collectors.toList());
		for (DelZaliczki zwr : zwrl) {
			this.removeDelZaliczkiZWR(zwr);
			if (deleteKalkulacjaZwrDkz && zwr.getDeltKalkulacje()!=null) {
				zwr.getDeltKalkulacje().removeDelZaliczki(zwr);
			}
		}
	}


	public void removeAllDeltZrodlaFinansowania(){
		List<DeltZrodlaFinansowania> zfs = this.getDeltZrodlaFinansowanias().stream().collect(Collectors.toList());
		for (DeltZrodlaFinansowania zf : zfs) {
			this.removeDeltZrodlaFinansowania(zf);
		}
	}
	
	public boolean isZaliczka() {
		boolean ret = false;
		
		DeltKalkulacje kalkulacja = this.getDeltKalkulacje();
		if(kalkulacja != null && eq("WST", kalkulacja.getKalRodzaj())) {
			ret = true;
		}
		
		return ret;
	}


	public boolean walidujDkz() {
		ArrayList<String> inval = new ArrayList<String>();

//		brzegówka brzegów
		if (this.getDeltKalkulacje()==null || this.getDeltKalkulacje().getDeltWnioskiDelegacji()==null) {
			User.alert("Zaliczka nie podpięta do kalkulacji, otwórz ponownie formularz");
			return false;
		}

		if(!eq("WNK", this.getDeltKalkulacje().getKalRodzaj())) {
			if( eq(null,this.getDkzKwota()) || this.getDkzKwota() == 0) {
				inval.add("Nie można wnioskować o zerową zaliczkę");
			}

			if(eq(null,this.getDkzDataWyp())){
				inval.add("Nie podano daty wypłaty zaliczki");

			}

			//TODO zastanowić się nad poniższym przypisaniem źródeł do zaliczek
			List<DeltZrodlaFinansowania> zflist = this.getDeltZrodlaFinansowanias();

			//Sztucznie jeszcze raz wymuszam przypisanie zf dla tego dkz jeśli ma pusto
			if(zflist == null || zflist.isEmpty()) {
				this.getDeltKalkulacje().przypiszZFDoWyplat(this);
				zflist = this.getDeltZrodlaFinansowanias();
			}


			if (nz(this.getDkzKwota())>0.0) {//ZF są istotne dla wyplat; dla zwrotów trzeba korzystac z PSK
				//jeśli mimo powyższego dalej jest pusto to trzeba przerwać akcję
				if (zflist == null || zflist.isEmpty()) {
					inval.add("Nie przypisano źródeł finansowania do dokumentu NZ, sprawdź dane");
				} else {
					double sumaZF = zflist.stream().mapToDouble(zf -> zf.getPkzfKwota()).sum();
					if (sumaZF < this.getDkzKwota()) {
						inval.add("Kwota nie została w pełni rozpisana na źródła finansowania, sprawdź dane");
					}
				}
			}


			if( this.getDkzKurs() == null || this.getDkzKurs() == 0.0) {
				if(eq(1L,this.dkzWalId)) {
					this.setDkzKurs(1.0);
				} else
					this.setDkzKurs(this.getKurs().przelicznik);
			}
		}

		if(nz(this.getDkzFormaWyplaty()).isEmpty()){
			inval.add("Podaj formę płatności");
		}

		if(	eq("T",this.getDkzFDokNz())//Nr konta sprawdzamy po zaznaczeniu Czy generować dyspozycję...
				&& "B".equals(this.getDkzFormaWyplaty())//tylko dla przelew = (B)ank
				&& nz(this.getDkzNrKonta()).isEmpty() //tylko jesli nie podano konta
		) {
//			inval.add("Podaj nr konta");
			String txt = null;
			if (eq("WST", this.getDeltKalkulacje().getKalRodzaj())) txt = "Część II (kalkulacja wstępna):";
			if (eq("ROZ", this.getDeltKalkulacje().getKalRodzaj())) txt = "Część III (kalkulacja rozliczająca):";
			if (txt!=null) //tylko jesli to kalkulacja (WST)ępna lub (ROZ)liczająca
				inval.add(
					String.format("%3$s\nDyspozycja na kwotę %1$s %2$s - 'Nr konta' jest polem wymaganym."
					, Utils.format(this.getDkzKwota()), this.getWalSymbol(), txt));
		}

		if("T".equals(this.getDkzCzyRownowartosc()) && "B".equals(this.getDkzFormaWyplaty())) { //czy Równowartość w PLN oraz Przelew (B=Bank)
			Object kndId = this.getDkzNrKonta();
			//TODO: getDkzNrKonta to nie powinien być String a np. Long, bo faktycznie my tam zapisujemy knd_id z ckk_konta_dane a nie numer konta
			//tylko po kiego pole DKZ_NR_KONTA w bazie danych jest typu VARCHAR2(100 BYTE) a robi za FK do CKK_KONTA_DANE.KND_ID

			String walSymbol = null;
			if (nz(this.getDkzKwota()) >= 0.0) { //wyplata dla prc - uwaga  na kalkulacji WNK (cz. I). nie musi byc podawana dkzkwota (dlatego nz)
				walSymbol = getLW("PPL_DEL_KONTA_PRC", this.getWniosek().getWndPrcId()).findAndFormat(kndId, "%wal_symbol$s");
			} else { //zwrot na konto MF
				walSymbol = getLW("PPL_DEL_KONTA_MF").findAndFormat(kndId, "%wal_symbol$s");
			}

			if (!eq("PLN", walSymbol)) {
				String txt = "Dla formy płatności 'przelew' po zaznaczeniu pola 'Równowartośc w PLN' należy podać konto w PLN ";
				if (eq("WNK", this.getDeltKalkulacje().getKalRodzaj())) txt += "(Część I - wniosek)";
				if (eq("WST", this.getDeltKalkulacje().getKalRodzaj())) txt += "(Część II - kalkulacja wstępna)";
				if (eq("ROZ", this.getDeltKalkulacje().getKalRodzaj())) txt += "(Część III - kalkulacja rozliczająca)";
				inval.add(txt);
			}
		}

		for (String msg : inval) {
			User.alert(msg);
		}

		return (inval.isEmpty());
	}

	@Override
	public DeltWnioskiDelegacji getWniosek() {
		if (this.getDeltKalkulacje()==null) return null;
		return this.getDeltKalkulacje().getDeltWnioskiDelegacji();
	}



	public void tryUstawDomyslnyNrKonta(){
		final DeltWnioskiDelegacji wnd = this.getWniosek();
		if (wnd == null
				|| wnd.getKalkulacjaWniosek() == null
		) return;
		final DeltKalkulacje kalWNK = wnd.getKalkulacjaWniosek();

		DelZaliczki dkz0 = kalWNK.getDelZaliczki().stream()
				.filter(z -> eq(z.getDkzWalId(), this.getDkzWalId()) && eq(z.getDkzFormaWyplaty(), this.getDkzFormaWyplaty()))
				.findFirst().orElse(null);

		if (dkz0==null) dkz0 = kalWNK.getDelZaliczki().stream()
				.filter(z -> z.getDkzWalId()==null && eq(z.getDkzFormaWyplaty(), this.getDkzFormaWyplaty()))
				.findFirst().orElse(null);

		if (dkz0==null) return;

		this.setDkzCzyRownowartosc(dkz0.getDkzCzyRownowartosc());
		this.setDkzNrKonta(dkz0.getDkzNrKonta());
	}

}
