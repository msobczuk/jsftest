package com.comarch.egeria.pp.delegacje.model;

import javax.persistence.*;

import com.comarch.egeria.pp.data.model.ModelBase;

import java.io.Serializable;
import java.sql.SQLException;

/**
 * The persistent class for the PPT_DEL_PKL_WYPL_NZ database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_PKL_WYPL_NZ", schema="PPADM")
@NamedQuery(name="DeltPklWyplNz.findAll", query="SELECT d FROM DeltPklWyplNz d")
public class DeltPklWyplNz extends ModelBase implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;


	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_PKL_WYPL_NZ",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_PKL_WYPL_NZ", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_PKL_WYPL_NZ")
	@Column(name="DPNZ_ID")
	private long dpnzId;

//    @Column(name="DPNZ_ZAL_ID")
//	private Long dpnzZalId;

	
	//bi-directional many-to-one association to DeltPozycjeKalkulacji
	@ManyToOne
    @JoinColumn(name="DPNZ_DKZ_ID")
    private DelZaliczki delZaliczki;

    public DelZaliczki getDelZaliczki() {
        return this.delZaliczki;
    }

    public void setDelZaliczki(DelZaliczki delZaliczki) {
        this.delZaliczki = delZaliczki;
    }

   
    
    
    
    //bi-directional many-to-one association to DeltPozycjeKalkulacji
    @ManyToOne
    @JoinColumn(name="DPNZ_PKL_ID")
    private DeltPozycjeKalkulacji deltPozycjeKalkulacji;

    public DeltPozycjeKalkulacji getDeltPozycjeKalkulacji() {
        return this.deltPozycjeKalkulacji;
    }

    public void setDeltPozycjeKalkulacji(DeltPozycjeKalkulacji deltPozycjeKalkulacji) {
        this.deltPozycjeKalkulacji = deltPozycjeKalkulacji;
    }


    @Column(name="DPNZ_WND_ID")
    private Long dpnzWndId;
    
    @Column(name="DPNZ_KAL_ID")
    private Long dpnzKalId;
    
    @Column(name="DPNZ_DOK_ID")
    private Long dpnzDokId;

    @Column(name="DPNZ_WAL_ID")
	private Long dpnzWalId;

    @Column(name="DPNZ_KWOTA_WAL")
    private Double dpnzKwotaWal;

    @Column(name="DPNZ_KURS")
    private Double dpnzKurs;

    @Column(name="DPNZ_KWOTA_PLN")
    private Double dpnzKwotaPln;




    public Long getDpnzDokId() {
    	this.onRXGet("dpnzDokId");
    	return dpnzDokId;
    }

    public void setDpnzDokId(Long dpnzDokId) {
    	this.onRXSet("dpnzDokId", this.dpnzDokId, dpnzDokId);
        this.dpnzDokId = dpnzDokId;
    }

    public Long getDpnzWalId() {
    	this.onRXGet("dpnzWalId");
        return dpnzWalId;
    }

    public void setDpnzWalId(Long dpnzWalId) {
    	this.onRXSet("dpnzWalId", this.dpnzWalId, dpnzWalId);
        this.dpnzWalId = dpnzWalId;
    }

    public Double getDpnzKwotaWal() {
    	this.onRXGet("dpnzKwotaWal");
        return dpnzKwotaWal;
    }

    public void setDpnzKwotaWal(Double dpnzKwotaWal) {
    	this.onRXSet("dpnzKwotaWal", this.dpnzKwotaWal, dpnzKwotaWal);
        this.dpnzKwotaWal = dpnzKwotaWal;
    }

    public Double getDpnzKwotaPln() {
    	this.onRXGet("dpnzKwotaPln");
        return dpnzKwotaPln;
    }

    public void setDpnzKwotaPln(Double dpnzKwotaPln) {
    	this.onRXSet("dpnzKwotaPln", this.dpnzKwotaPln, dpnzKwotaPln);
        this.dpnzKwotaPln = dpnzKwotaPln;
    }

    public Double getDpnzKurs() {
    	this.onRXGet("dpnzKurs");
        return dpnzKurs;
    }

    public void setDpnzKurs(Double dpnzKurs) {
    	this.onRXSet("dpnzKurs", this.dpnzKurs, dpnzKurs);
        this.dpnzKurs = dpnzKurs;
    }

	public Long getDpnzWndId() {
		this.onRXGet("dpnzWndId");
		return dpnzWndId;
	}

	public void setDpnzWndId(Long dpnzWndId) {
		this.onRXSet("dpnzWndId", this.dpnzWndId, dpnzWndId);
		this.dpnzWndId = dpnzWndId;
	}

	public Long getDpnzKalId() {
		this.onRXGet("dpnzKalId");
		return this.dpnzKalId;
	}

	public void setDpnzKalId(Long dpnzKalId) {
		this.onRXSet("dpnzKalId", this.dpnzKalId, dpnzKalId);
		this.dpnzKalId = dpnzKalId;
	}



}