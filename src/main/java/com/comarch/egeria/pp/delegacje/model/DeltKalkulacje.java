package com.comarch.egeria.pp.delegacje.model;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.KursWaluty;
import com.comarch.egeria.pp.data.KursWalutySprzedazyNBP;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.delegacje.DelegacjaKRBean;
import com.comarch.egeria.pp.delegacje.DelegacjaMFBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.time.DayOfWeek;
import java.util.*;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.*;


/**
 * The persistent class for the PPT_DEL_KALKULACJE database table.
 *
 */
@Entity
@Table(name="PPT_DEL_KALKULACJE", schema="PPADM")
@NamedQuery(name="DeltKalkulacje.findAll", query="SELECT d FROM DeltKalkulacje d")
public class DeltKalkulacje extends DelBase implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_KALKULACJE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_KALKULACJE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_KALKULACJE")
	@Column(name="KAL_ID")
	private long kalId;

	@Temporal(TemporalType.DATE)
	@Column(name="KAL_AUDYT_DM")
	private Date kalAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="KAL_AUDYT_DT", updatable=false)
	private Date kalAudytDt;

	@Column(name="KAL_AUDYT_KM")
	private String kalAudytKm;

//	@Column(name="KAL_AUDYT_KT")
//	private String kalAudytKt;

	@Column(name="KAL_AUDYT_LM")
	private String kalAudytLm;

	@Column(name="KAL_AUDYT_UM")
	private String kalAudytUm;

	@Column(name="KAL_AUDYT_UT", updatable=false)
	private String kalAudytUt;

	@Temporal(TemporalType.DATE)
	@Column(name="KAL_DATA")
	private Date kalData;

	@Temporal(TemporalType.DATE)
	@Column(name="KAL_DATA_KURSU")
	private Date kalDataKursu;

//	@Column(name="KAL_F_AKTUALNA")
//	private String kalFAktualna;
//
//	@Column(name="KAL_F_ZATWIERDZONA")
//	private String kalFZatwierdzona;
//
//	@Temporal(TemporalType.DATE)
//	@Column(name="KAL_GODZINA_POWROTU")
//	private Date kalGodzinaPowrotu;

//	@Temporal(TemporalType.DATE)
//	@Column(name="KAL_GODZINA_WYJAZDU")
//	private Date kalGodzinaWyjazdu;
//
//	@Column(name="KAL_KNT_ID_PRC")
//	private Long kalKntIdPrc;
//
//	@Column(name="KAL_KNT_ID_WL")
//	private Long kalKntIdWl;
//
//	@Column(name="KAL_LICZBA_DNI")
//	private long kalLiczbaDni;
//
//	@Column(name="KAL_LICZBA_NOCLEGOW")
//	private long kalLiczbaNoclegow;
//
//	@Column(name="KAL_LICZBA_ROZPOCZETYCH_DNI")
//	private long kalLiczbaRozpoczetychDni;

//	@Column(name="KAL_LP")
//	private long kalLp;

	@Column(name="KAL_OPIS")
	private String kalOpis;

//	@Column(name="KAL_POLE01")
//	private String kalPole01;
//
//	@Column(name="KAL_POLE02")
//	private String kalPole02;
//
//	@Column(name="KAL_POLE03")
//	private String kalPole03;
//
//	@Column(name="KAL_POLE04")
//	private String kalPole04;
//
//	@Column(name="KAL_POLE05")
//	private String kalPole05;
//
//	@Column(name="KAL_POLE06")
//	private String kalPole06;
//
//	@Column(name="KAL_POLE07")
//	private String kalPole07;
//
//	@Column(name="KAL_POLE08")
//	private String kalPole08;
//
//	@Column(name="KAL_POLE09")
//	private String kalPole09;
//
//	@Column(name="KAL_POLE10")
//	private String kalPole10;

//	@Column(name="KAL_PRC_ID")
//	private long kalPrcId;
//	
	@Column(name="KAL_KW_ID")
	private Long kalKwId;

	@Column(name="KAL_KAL_ID")
	private Long kalKalId;

	@Column(name="KAL_RODZAJ")
	private String kalRodzaj;

	//bi-directional many-to-one association to DeltWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="KAL_WND_ID")
	private DeltWnioskiDelegacji deltWnioskiDelegacji;

	@OneToMany(mappedBy="deltKalkulacje", cascade={CascadeType.ALL}, orphanRemoval=true)
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("PKL_ID ASC")
	private List<DeltPozycjeKalkulacji> deltPozycjeKalkulacjis = new ArrayList<>();

	//bi-directional many-to-one association to DeltTrasy
	@OneToMany(mappedBy="deltKalkulacje", cascade={CascadeType.ALL}, orphanRemoval=true)
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("TRS_DATA_OD, TRS_DATA_OD, TRS_ID ASC")
	private List<DeltTrasy> deltTrasies = new ArrayList<>();

	@OneToMany(mappedBy="deltKalkulacje", cascade={CascadeType.ALL}, orphanRemoval=true)
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("DKZ_ID ASC")
	private List<DelZaliczki> delZaliczki = new ArrayList<>();

	@OneToMany(mappedBy="deltKalkulacje", cascade={CascadeType.ALL}, orphanRemoval=true)
	@BatchSize(size = 200)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("CZYN_ID ASC")
	private List<DelCzynnosciSluzbowe> delCzynnosciSluzbowe = new ArrayList<>();




	public double podsumujDiety(String ktgPozycji) {
		double ret = this.getDeltPozycjeKalkulacjis()
				.stream()
				.filter(pkl ->(ktgPozycji).equals(pkl.getKategoria()))
				.mapToDouble(pkl -> round(nz(pkl.getPklIlosc()), 6))
				.sum();
		return ret;
	}

	public double maxLiczbaDietWgPlanuTrasy(Boolean fcs){

		if (this.deltTrasies==null || this.deltTrasies.isEmpty())
			return 0;

		double ret = 0;

		Date trsDataOd = this.getDeltTrasies().get(0).getTrsDataOd();
		Date trsDataDo = this.getDeltTrasies().get( this.deltTrasies.size()-1 ) .getTrsDataDo();


		if(this.deltWnioskiDelegacji.getWndRodzaj().equals(2L)) // 2 - zagraniczna, 1 - krajowa
			if(!fcs) {
				ret = DelegacjaMFBean.ileDietZagranicznych(trsDataOd, trsDataDo);
			} else {
				double czas = (0.000 + trsDataDo.getTime() - trsDataOd.getTime()) / (1000 * 60 * 60) + 4.0;
				ret = DelegacjaMFBean.ileDietZagranicznych_FCS(czas);
			}
		else
			ret = DelegacjaKRBean.ileDietKrajowe(DelegacjaKRBean.sprawdzIleGodzinDel(this), DelegacjaKRBean.sprawdzIleGodzinDel(this)>24.0);

		return ret;
	}

	public double maxLiczbaRyczaltKMWgPlanuTrasy(){

		if (this.deltTrasies==null || this.deltTrasies.isEmpty())
			return 0;

		double ret = 0;

		Date trsDataOd = this.getDeltTrasies().get(0).getTrsDataOd();
		Date trsDataDo = this.getDeltTrasies().get( this.deltTrasies.size()-1 ) .getTrsDataDo();


		if(this.deltWnioskiDelegacji.getWndRodzaj().equals(2L)) // 2 - zagraniczna, 1 - krajowa
			ret = DelegacjaMFBean.ileDniRyczaltKom(trsDataOd, trsDataDo);
		else
			ret = DelegacjaKRBean.ileDniRyczaltKom(DelegacjaKRBean.sprawdzIleGodzinDel(this));

		return ret;
	}


	public double maxLiczbaDietWgCzynnosciSluzbowych(Boolean fcs){

		double ret = 0;

		double czasSluzbowyTotal = this.delCzynnosciSluzbowe.stream().mapToDouble( p->nz( p.getCzynCzas())).sum();

		if(this.deltWnioskiDelegacji.getWndRodzaj().equals(2L)) {
			if(!fcs) {
				ret = DelegacjaMFBean.ileDietZagranicznych_CS(czasSluzbowyTotal);
			} else {
				czasSluzbowyTotal+=4.0;
				ret = DelegacjaMFBean.ileDietZagranicznych_FCS(czasSluzbowyTotal);
			}
		}else
			ret = DelegacjaKRBean.ileDietKrajowe(czasSluzbowyTotal, czasSluzbowyTotal>24.0);

		return ret;
	}

	public double maxLiczbaRyczaltKMWgCzynnosciSluzbowych(){

		double ret = 0;

		double czasSluzbowyTotal = this.delCzynnosciSluzbowe.stream().mapToDouble( p->nz( p.getCzynCzas())).sum();

		if(this.deltWnioskiDelegacji.getWndRodzaj().equals(2L)) {
			ret = DelegacjaMFBean.ileDniRyczaltKom_CS(czasSluzbowyTotal);
		}else
			ret = DelegacjaKRBean.ileDniRyczaltKom(czasSluzbowyTotal);

		return ret;
	}

	public void bilansujRyczaltKM() {
		if (this.deltPozycjeKalkulacjis==null || this.deltPozycjeKalkulacjis.isEmpty())
			return;

		double sumaRyczalt = this.podsumujDiety("KM");
		double maxLiczbaRyczalt = 0.0;

		if (this.delCzynnosciSluzbowe==null || this.delCzynnosciSluzbowe.isEmpty())
			maxLiczbaRyczalt = this.maxLiczbaRyczaltKMWgPlanuTrasy();
		else
			maxLiczbaRyczalt = maxLiczbaRyczaltKMWgCzynnosciSluzbowych();

		if (sumaRyczalt == maxLiczbaRyczalt)
			return;

		double roznica = round(sumaRyczalt - maxLiczbaRyczalt,4);

		List<DeltPozycjeKalkulacji> pozycje = this.deltPozycjeKalkulacjis
				.stream().filter(p->"KM".equals( p.getKategoria() ))
				.collect(Collectors.toList());

		if (tryPomniejsz( pozycje, roznica, false)) {
			User.info("Liczba ryczałtow komunikacyjnych została pomniejszona w wyniku bilansowania. Suma wyliczonych ryczałtów: " + sumaRyczalt + " , limit ryczałtów według planu trasy: " + maxLiczbaRyczalt);
			return;
		}

	}

	public void bilansujDiety(String ktgPozycji, boolean fcs) {
		if (ktgPozycji == null)
			return;

		if (this.deltPozycjeKalkulacjis==null || this.deltPozycjeKalkulacjis.isEmpty())
			return;

		if(!"P".equals(ktgPozycji))
			return;


		double sumaDiet = this.podsumujDiety(ktgPozycji);
		double maxLiczbaDiet = 0.0;


		if (this.delCzynnosciSluzbowe==null || this.delCzynnosciSluzbowe.isEmpty())
			maxLiczbaDiet = this.maxLiczbaDietWgPlanuTrasy(fcs);
		else
			maxLiczbaDiet = maxLiczbaDietWgCzynnosciSluzbowych(fcs);


		if (sumaDiet == maxLiczbaDiet)
			return;

		double roznica = round(sumaDiet - maxLiczbaDiet,4);


		List<DeltPozycjeKalkulacji> pozycje = this.deltPozycjeKalkulacjis
				.stream().filter(p->ktgPozycji.equals( p.getKategoria() ))
				.collect(Collectors.toList());


		if (tryPomniejsz( pozycje, roznica, fcs))
			return;

		if(!fcs) {
			//nie udało się pomniejszyc PKL na jednej tylko pozycji: przyklad: PKL: 0,3333 + 0,3333 vs MAX=0.5 -> roznica 0.1667 - nie da sie odjac tak aby zostala prawidlowa wartosc
			if (tryPomniejsz(pozycje, roznica+0.1667, false)){
				if (tryPomniejsz(pozycje, -0.1667, false)){
					return;
				}
			}

            //....bardzo po... przypadek... chyba nie zaistnieje
			if (tryPomniejsz(pozycje, roznica+0.3333, false)){
				if (tryPomniejsz(pozycje, -0.3333, false)){
					return;
				}
			}

        }

		User.alert("Nie udało się automatycznie zbilansować liczby diet. Różnica między sumą diet a maksymalną liczbą diet wynosi: " + roznica);
	}



	private boolean tryPomniejsz(List<DeltPozycjeKalkulacji> pozycje, double roznica, boolean fcs) {
		for (int i = pozycje.size()-1; i>=0; i--){
			DeltPozycjeKalkulacji pkl = pozycje.get(i);
			Double pklIlosc = nz( pkl.getPklIlosc() );
			if (roznica <= pklIlosc){
				double pklIloscPomniejszona = pklIlosc-roznica;
				if (testLiczbaDiet(pklIloscPomniejszona,fcs)){
					pkl.setPklIlosc(pklIloscPomniejszona);
					pkl.przeliczKwote();

					if (!(""+pkl.getPklOpis()).contains("[*]"))
						pkl.setPklOpis(nz(pkl.getPklOpis()) + " [*]");

					return true;
				}
			}
		}

		return false;
	}


	/**
	 * Nie mozna wprowadzic kazdej liczby jako liczba diet, dopuszczalne to liczby całkowite, l.całk.+połówki oraz l.całk.+1/3
	 * @param ileDiet
	 * @return
	 */
	public static boolean testLiczbaDiet(Double ileDiet, boolean fcs){
		if (ileDiet==null || ileDiet<0)
			return false;

		Double fract =  round(ileDiet % 1, 2);//z. ulamkowa z ileDiet
		if(!fcs) {
            return fract == 0.0 || fract == 0.33 || fract == 0.5;
		} else {
            return fract == 0.0 || fract == 0.5;
		}


    }


	public DeltKalkulacje clone(){//shallow copy

		DeltKalkulacje kal = new DeltKalkulacje();

		kal.kalId = 0L;
		kal.kalData = kalData;
		kal.kalDataKursu = kalDataKursu;
		kal.kalRodzaj = kalRodzaj;
		kal.kalOpis = kalOpis;
		kal.deltWnioskiDelegacji = null;
		kal.sqlBean = sqlBean;

		return kal;

	}



	public DeltKalkulacje clone(boolean deepCopy){
		DeltKalkulacje kal = clone(); //shallow copy
		if (!deepCopy) return kal;

		for (DeltPozycjeKalkulacji pk: this.deltPozycjeKalkulacjis){
			DeltPozycjeKalkulacji npk = pk.clone(deepCopy);
			npk.setPklId(0L);
			npk.cloneSrc = pk;
			pk.cloneDest = npk;
			kal.addDeltPozycjeKalkulacji(npk);
		}

		if(!this.deltTrasies.isEmpty()){
			for(DeltTrasy tr : this.deltTrasies) {
				DeltTrasy t = tr.clone();

				t.setTrsId(0L);
				t.cloneSrc = tr;
				tr.cloneDest = t;
				kal.addDeltTrasy(t);
			}
		}

//		if(!this.delZaliczki.isEmpty()){
//			for( DelZaliczki zal : this.delZaliczki) {
//				DelZaliczki z = zal.clone();
//				
//				z.setDkzId(0L);
//				kal.addDeltZaliczki(z);
//			}
//		}

		if(!this.delCzynnosciSluzbowe.isEmpty()){
			for( DelCzynnosciSluzbowe cz : this.delCzynnosciSluzbowe) {
				DelCzynnosciSluzbowe c = cz.clone();

				c.setCzynId(0L);
				kal.addDelCzynnosciSluzbowe(c);
			}
		}

		return kal;

	}




	public DeltKalkulacje() {
	}

	public long getKalId() {
		this.onRXGet("kalId");
		return this.kalId;
	}

	public void setKalId(long kalId) {
		this.onRXSet("kalId", this.kalId, kalId);
		this.kalId = kalId;
	}

	public Date getKalAudytDm() {
		return this.kalAudytDm;
	}

	public void setKalAudytDm(Date kalAudytDm) {
		this.kalAudytDm = kalAudytDm;
	}

	public Date getKalAudytDt() {
		return this.kalAudytDt;
	}

	public void setKalAudytDt(Date kalAudytDt) {
		this.kalAudytDt = kalAudytDt;
	}

	public String getKalAudytKm() {
		return this.kalAudytKm;
	}

	public void setKalAudytKm(String kalAudytKm) {
		this.kalAudytKm = kalAudytKm;
	}

//	public String getKalAudytKt() {
//		return this.kalAudytKt;
//	}
//
//	public void setKalAudytKt(String kalAudytKt) {
//		this.kalAudytKt = kalAudytKt;
//	}

	public String getKalAudytLm() {
		return this.kalAudytLm;
	}

	public void setKalAudytLm(String kalAudytLm) {
		this.kalAudytLm = kalAudytLm;
	}

	public String getKalAudytUm() {
		return this.kalAudytUm;
	}

	public void setKalAudytUm(String kalAudytUm) {
		this.kalAudytUm = kalAudytUm;
	}

	public String getKalAudytUt() {
		return this.kalAudytUt;
	}

	public void setKalAudytUt(String kalAudytUt) {
		this.kalAudytUt = kalAudytUt;
	}

	public Date getKalData() {
		this.onRXGet("kalData");
		return this.kalData;
	}

	public void setKalData(Date kalData) {
		this.onRXSet("kalData", this.kalData, kalData);
		this.kalData = kalData;
	}

	public String getKalOpis() {
		this.onRXGet("kalOpis");
		return this.kalOpis;
	}

	public void setKalOpis(String kalOpis) {
		this.onRXSet("kalOpis", this.kalOpis, kalOpis);
		this.kalOpis = kalOpis;
	}

	/**
	 * @return the deltWnioskiDelegacji
	 */
	public DeltWnioskiDelegacji getDeltWnioskiDelegacji() {
		this.onRXGet("deltWnioskiDelegacji");
		return deltWnioskiDelegacji;
	}

	/**
	 * @param deltWnioskiDelegacji the deltWnioskiDelegacji to set
	 */
	public void setDeltWnioskiDelegacji(DeltWnioskiDelegacji deltWnioskiDelegacji) {
		this.onRXSet("deltWnioskiDelegacji", this.deltWnioskiDelegacji, deltWnioskiDelegacji);
		this.deltWnioskiDelegacji = deltWnioskiDelegacji;
	}

	public void addWniosekPkl(long tpozId) {
		DeltPozycjeKalkulacji pkl = new DeltPozycjeKalkulacji();
		pkl.setDeltKalkulacje(this);

		pkl.setPklTpozId(tpozId);
		//this.ustawZFAngaze(pkl); //celowo tu nie ustawiamy ZF - jak trzeba to się ręcznie ustawi, ale puste ZF na WNK/PKL znaczy domyslne ZF na WNK
		pkl.setPklStawka(0D);
		pkl.setPklKwota(0D);
		pkl.setPklWalId(1L);
		pkl.setPklIlosc(0D);
		pkl.setPklKwotaPrzelewu(0L);
		pkl.setPklKwotaGotowki("0");
		pkl.setPklKosztMFProcent(100.0);
		pkl.setPklKosztZaprProcent(0.0);
		pkl.setPklRefundacjaProcent(0.0);
		final DelTypyPozycji tp = this.getDelTypyPozycjiByTpozId(tpozId);
		if (tp!=null && tp.getTpozDomyslnaPlatnosc()!=null){
			pkl.setPklFormaPlatnosci(tp.getTpozDomyslnaPlatnosc());
		} else {
			if (eq("T", pkl.getKategoria())) {
				DeltSrodkiLokomocji slok = pkl.getDeltKalkulacje().getWniosek().getDeltSrodkiLokomocjis().get(0);
				if (slok != null && (eq("03", slok.getSlokSrodekLokomocji()) || eq("07", slok.getSlokSrodekLokomocji()))) {
					pkl.setPklFormaPlatnosci("04");
				} else
					pkl.setPklFormaPlatnosci("03");
			} else if (eq("H", pkl.getKategoria())) {
				pkl.setPklFormaPlatnosci("03");
			} else pkl.setPklFormaPlatnosci("04");
		}

		this.addDeltPozycjeKalkulacji(pkl);
	}




	public List<DeltPozycjeKalkulacji> getDeltPozycjeKalkulacjis() {
		this.onRXGet("deltPozycjeKalkulacjis");
		return deltPozycjeKalkulacjis;
	}

	public List<DeltPozycjeKalkulacji> getDeltPozycjeKalkulacjisGotowkaPrzelew() {
		return this.getDeltPozycjeKalkulacjis().stream().filter(p->p.isGotowka() || p.isPrzelew()).collect(Collectors.toList());
	}

	public List<DeltPozycjeKalkulacji> getDeltPozycjeKalkulacjisNieGotowkaNiePrzelew() {
		return this.getDeltPozycjeKalkulacjis().stream().filter(p->!p.isGotowka() && !p.isPrzelew()).collect(Collectors.toList());
	}

	public void setDeltPozycjeKalkulacjis(List<DeltPozycjeKalkulacji> deltPozycjeKalkulacjis) {
		this.onRXSet("deltPozycjeKalkulacjis", this.deltPozycjeKalkulacjis, deltPozycjeKalkulacjis);
		this.deltPozycjeKalkulacjis = deltPozycjeKalkulacjis;
	}



	public List<DeltPozycjeKalkulacji> getDeltPozycjeKalkulacjisPozostaleKoszty(Long tpozIdP) {
		//#{delegacjaMFBean.kalkulacjaWstepna.deltPozycjeKalkulacjis.stream().filter(pkl -> pkl.pklTpozId != tpozIdP and pkl.pklTpozId != tpozIdH).toList() }"
		List<DeltPozycjeKalkulacji> ret = this.getDeltPozycjeKalkulacjis().stream().filter(p-> nz(p.getPklTpozId()).equals(tpozIdP) ).collect(Collectors.toList());
		return ret;
	}


	public void clearAllDeltPozycjeKalkulacji(){
		final List<DeltPozycjeKalkulacji> lst = this.getDeltPozycjeKalkulacjis().stream().collect(Collectors.toList());
		lst.stream().forEach(pkl->this.removeDeltPozycjeKalkulacji(pkl));
	}


	public DeltPozycjeKalkulacji addDeltPozycjeKalkulacji(DeltPozycjeKalkulacji deltPozycjeKalkulacji) {
		this.onRXSet("deltPozycjeKalkulacjis", null, this.deltPozycjeKalkulacjis);
		getDeltPozycjeKalkulacjis().add(deltPozycjeKalkulacji);
		deltPozycjeKalkulacji.setDeltKalkulacje(this);

		//TODO ustawic domyslne ZF dla dodanej wlasnie PKL

		this.clearDelZaliczkiReszta();


		return deltPozycjeKalkulacji;
	}

	public void forCzyDomyslnieDietaHotelowa() {

		if (this.getDeltWnioskiDelegacji()==null || !"WNK".equals(this.getKalRodzaj()))
			return;

		this.getDeltWnioskiDelegacji().setCzyDomyslnieDietaHotelowa(null);
	}

	public DeltPozycjeKalkulacji addDeltPozycjeKalkulacji(DeltPozycjeKalkulacji deltPozycjeKalkulacji, boolean nieCzyscZaliczek) {
		this.onRXSet("deltPozycjeKalkulacjis", null, this.deltPozycjeKalkulacjis);
		getDeltPozycjeKalkulacjis().add(deltPozycjeKalkulacji);
		deltPozycjeKalkulacji.setDeltKalkulacje(this);

		//nieCzyscZaliczek -- dodane na potrzeby doliczania dopełnienia w formie pkl
		if(!nieCzyscZaliczek)
		this.clearDelZaliczkiReszta();
		forCzyDomyslnieDietaHotelowa();
		return deltPozycjeKalkulacji;
	}

	public DeltPozycjeKalkulacji removeDeltPozycjeKalkulacji(DeltPozycjeKalkulacji deltPozycjeKalkulacji) {
		this.onRXSet("deltPozycjeKalkulacjis", null, this.deltPozycjeKalkulacjis);
		if(eq("T", deltPozycjeKalkulacji.getKategoria()) && deltPozycjeKalkulacji.getDeltTrasy() != null) {
			List<DeltTrasy> cel = this.getDeltTrasies().stream()
					.filter(t ->deltPozycjeKalkulacji.getDeltTrasy().getTrsId()==(t.getTrsId()))
					.collect(Collectors.toList());
			if(cel.size()>0) {
				cel.get(0).setTrsKosztBiletu(null);
				cel.get(0).removeDeltPozycjeKalkulacji(deltPozycjeKalkulacji);
			}
		}
		getDeltPozycjeKalkulacjis().remove(deltPozycjeKalkulacji);
		deltPozycjeKalkulacji.setDeltKalkulacje(null);

        this.clearDelZaliczkiReszta();//?!
		forCzyDomyslnieDietaHotelowa();
		return deltPozycjeKalkulacji;
	}

    public DeltPozycjeKalkulacji superRemoveDeltPozycjeKalkulacji(DeltPozycjeKalkulacji deltPozycjeKalkulacji) {
		// Noromalnie wystarczyłoby napisać super.removeDeltPozycjeKalkulacji(...),
		// ale nie utworzono klasy logicznej Kalkulacje.java z kl. extends DeltKalkulacje
		// i nie można łatwo stosowac super.removeDeltPozycjeKalkulacji(...),
		// a bywa że to jest potrzebne aby kod sie nie zapętał
        getDeltPozycjeKalkulacjis().remove(deltPozycjeKalkulacji);
        deltPozycjeKalkulacji.setDeltKalkulacje(null);

        return deltPozycjeKalkulacji;
    }

	public Long getKalKwId() {
		this.onRXGet("kalKwId");
		return kalKwId;
	}

	public void setKalKwId(Long kalKwId) {
		this.onRXSet("kalKwId", this.kalKwId, kalKwId);
		this.kalKwId = kalKwId;
	}

	public Date getKalDataKursu() {
		this.onRXGet("kalDataKursu");
		return kalDataKursu;
	}

	public void setKalDataKursu(Date kalDataKursu) {

		boolean eq = eq(this.kalDataKursu, kalDataKursu);
		this.onRXSet("kalDataKursu", this.kalDataKursu, kalDataKursu);

//		if (!validateDataKursu(kalDataKursu))
//			return;

		//this.kalDataKursu = kalDataKursu;
		if(!eq) {
			this.kalDataKursu = getDataKursu(kalDataKursu);
			this.przeliczZaliczkiPoNowymKursie();
		}
	}

	public void przeliczZaliczkiPoNowymKursie () {

//		System.out.println("Tukej");

		List<DelZaliczki> zaliczkiReszta = this.getDelZaliczki().stream().filter(dkz -> dkz.getDkzDokId() == null).collect(Collectors.toList());
//		List<DelZaliczki> zaliczkiReszta = this.getDelZaliczkiReszta();
		for( DelZaliczki zal : zaliczkiReszta) {
			zal.setDkzKurs(zal.getKursWstepny().przelicznik);
		}
	}



	public boolean validateDataKursu() {
		return validateDataKursu(this.kalDataKursu);
	}

	public boolean validateDataKursu(Date kalDataKursu) {
		return eq( getDataKursu(kalDataKursu), kalDataKursu );
	}


	public Date getDataKursu(Date naDzien) { // z jakiego dnia jest/ma być kurs

		Date ret = naDzien;

		if(this.getDeltWnioskiDelegacji()==null)
			return ret; //nowa/niedodana kalkulacja

		if(this.getDeltWnioskiDelegacji().isKrajowa() || eq("WNK", this.getKalRodzaj()))
			return ret;//nieistotne dla delegacji krajowych i kalkulacji WNK (Wniosek)

		Long walIdEur = this.getWalId("EUR");

		KursWaluty kw = new KursWalutySprzedazyNBP(walIdEur, Utils.today());//mozliwe, ze trzeba rozwinac test kursu EUR na pozostale waluty kalkulacji (iter this.walutySymbole()) ....
		if (kw.getDataKursu()!=null && kw.getDataKursu().before(naDzien)){
			User.warn("Kursy sprzedaży NBP z dnia %1$s są niedostępne. " +
					"\nAktualne kursy walut są z dnia %2$s.", format(naDzien), format(kw.getDataKursu()));
			//TODO poniższe dwie linijki możliwe, że trzeba bęzie przywrócić po testach i konsutlacjach !!!
//			this.onRXSet("kalDataKursuNaDzien", naDzien, this.kalDataKursu);
//			ret = kw.getDataKursu();
		}

//		kw = new KursWalutySredniNBP(walIdEur, Utils.today());
//		if (kw.getDataKursu().before(naDzien)){
//			long cntDkzCzyRownowartosc = this.getDokumentyDKZPozaNZ().stream().filter(dkz -> eq("T", dkz.getDkzCzyRownowartosc())).count();
//			if (cntDkzCzyRownowartosc>0){
//				User.warn("Kursy średnie NBP z dnia %1$s są niedostępne (przeliczanie do PLN z zaznaczeniem pola 'Równowartość PLN' nie jest możliwe). " +
//						"\nAktualne kursy walut są z dnia %2$s.", format(naDzien), format(kw.getDataKursu()));
//				this.onRXSet("kalDataKursuNaDzien", naDzien, this.kalDataKursu);
//				ret = kw.getDataKursu();
//			}
//		}

		return ret;
	}


//	public List<DeltTrasy> getDeltTrasiesSorted() {
//
//		return this.getDeltTrasies().stream()
//				.sorted() //ustalam przez comparable / compareTo w DeltTrasy
//				.collect(Collectors.toList());
//
////		return this.getDeltTrasies().stream()
////				.sorted(
////						(t1,t2)-> (Utils.format( t1.getTrsDataOd(), "yyyy-MM-dd HH.mm.ss" ) + Utils.format( t1.getTrsDataDo(), "yyyy-MM-dd HH.mm.ss" ))
////						.compareTo(Utils.format( t2.getTrsDataOd(), "yyyy-MM-dd HH.mm.ss" ) + Utils.format( t2.getTrsDataDo(), "yyyy-MM-dd HH.mm.ss" ))
////				)
////				.collect(Collectors.toList());
//	}

	public List<DeltTrasy> getDeltTrasies() {
		this.onRXGet("deltTrasies");
		return this.deltTrasies;
//		if (this.deltTrasies==null) { return null; }
//		return this.deltTrasies.stream().sorted().collect(Collectors.toList());
	}

	public void trySortDeltTrasies(){
		if (this.deltTrasies==null) return;
		onRXSet("deltTrasies",null, this.deltTrasies);

		final long ileBezDat = this.deltTrasies.stream().filter(t -> t.getTrsDataOd() == null || t.getTrsDataDo() == null).count();

		if (ileBezDat<=0) { //jesli nie ma takich gdzie brakuje daty od lub daty do
			Collections.sort(this.deltTrasies);
		}

	}

	public boolean validateDeltTrasies() {
		if (this.getDeltWnioskiDelegacji()==null) return true;

		Date wndDataWyjazdu = this.getDeltWnioskiDelegacji().getWndDataWyjazdu();
		Date wndDataPowrotu = Utils.lastMinuteOfDay(this.getDeltWnioskiDelegacji().getWndDataPowrotu());

		Date doSprawdzenia = null;
		boolean ret = false;

		if(this.getDeltTrasies().size()>0) {
			if(this.getDeltTrasies().size() < 2){
				User.alert("Plan trasy musi zawierać co najmniej dwie pozycje");
				return false;
			}

			if("WST".equals( this.getKalRodzaj() ))
				if(this.getDeltTrasies().get(0).getTrsDataOd() != null && this.getDeltTrasies().get(this.getDeltTrasies().size()-1).getTrsDataDo() != null){
					if(!( ((this.getDeltTrasies().get(0).getTrsDataOd().compareTo(wndDataWyjazdu) >=0 && this.getDeltTrasies().get(0).getTrsDataOd().before(wndDataPowrotu))
							&&
							(this.getDeltTrasies().get(this.getDeltTrasies().size()-1).getTrsDataDo().compareTo(wndDataWyjazdu) >=0 && this.getDeltTrasies().get(this.getDeltTrasies().size()-1).getTrsDataDo().before(wndDataPowrotu)))
					)) {
						User.alert("Daty na planie trasy muszą się zawierać w wnioskowanym terminie trwania delegacji");
						return false;
					}
				} else {
					User.alert("Plan trasy - wprowadzone dane nie mogą być puste");
					return false;
				}

			if(!(this.getDeltTrasies().get(0).getTrsDataOd().before(this.getDeltTrasies().get(this.getDeltTrasies().size()-1).getTrsDataDo()))) {
				User.alert("Data powrotu z delegacji nie może nastąpić przed datą wyjazdu, popraw dane");
				return false;
			}

			for( DeltTrasy trs : this.getDeltTrasies()) {

				if(trs.getTrsKrIdOd() == null || trs.getTrsKrIdDo() == null) {
					User.alert("Należy uzupełnić plan trasy o brakujące dane w kolumnie kraj");
					return false;
				}

				if(trs.getTrsMiejscowoscOd() == null || trs.getTrsMiejscowoscDo() == null || trs.getTrsMiejscowoscOd().isEmpty() || trs.getTrsMiejscowoscDo().isEmpty()) {

					User.alert("Należy uzupełnić plan trasy o brakujące dane w kolumnie miejscowość");
					return false;
				}

				if(trs.getTrsDataOd() != null && trs.getTrsDataDo() != null) {

					if(doSprawdzenia != null) {
						if(trs.getTrsDataOd().after(doSprawdzenia)) {

						}
						else {
							User.alert("Data wyjazdu z poprzedzającego wiersza w planie trasy nie może być późniejsza niż daty w kolejnych wierszach");
							return false;
						}
					}

					doSprawdzenia = trs.getTrsDataDo();

					if(trs.getTrsDataDo().after(trs.getTrsDataOd())){
						continue;
					} else {
						User.alert("Niepoprawna data na planie trasy : data wyjazdu nie może nastąpić przed datą przyjazdu na danym odcinku planu trasy");
						return false;
					}

				} else {
//					User.alert("Nie wszystkie daty na planie trasy zostały uzupełnione, uzupełnij dane przed zapisem");
//					return false;
				}
			}
			ret = true;
		} else return true;
		return ret;
	}






	public void setDeltTrasies(List<DeltTrasy> deltTrasies) {
		this.onRXSet("deltTrasies", this.deltTrasies, deltTrasies);
		this.deltTrasies = deltTrasies;
	}

	public DeltTrasy addDeltTrasy(DeltTrasy deltTrasy) {
		this.onRXSet("deltTrasies", null, deltTrasies);
		this.deltTrasies.add(deltTrasy); //getDeltTrasies ma dodane sortowanie
		deltTrasy.setDeltKalkulacje(this);

		return deltTrasy;
	}

	public void addNewTrasa() {
		DeltTrasy t = new DeltTrasy();
		this.addDeltTrasy(t);
	}

	public DeltTrasy removeDeltTrasy(DeltTrasy deltTrasy) {
		this.onRXSet("deltTrasies", null, deltTrasies);
		if(deltTrasy.getDeltPozycjeKalkulacji().size() > 0)
			removeDeltPozycjeKalkulacji(deltTrasy.getDeltPozycjeKalkulacji().get(0));
		this.deltTrasies.remove(deltTrasy);//getDeltTrasies ma dodane sortowanie
		deltTrasy.setDeltKalkulacje(null);

		return deltTrasy;
	}

	public Long getKalKalId() {
		this.onRXGet("kalKalId");
		return kalKalId;
	}

	public void setKalKalId(Long kalKalId) {
		this.onRXSet("kalKalId", this.kalKalId, kalKalId);
		this.kalKalId = kalKalId;
	}

	public List<DelZaliczki> getDelZaliczki() {
		this.onRXGet("delZaliczki");
		return delZaliczki;
	}

	public void setDelZaliczki(List<DelZaliczki> delZaliczki) {
		this.onRXSet("delZaliczki", this.delZaliczki, delZaliczki);
		this.delZaliczki = delZaliczki;
	}


	public DelZaliczki addDeltZaliczki(DelZaliczki delZaliczki) {
		this.onRXSet("delZaliczki", null, delZaliczki);
		getDelZaliczki().add(delZaliczki);
		delZaliczki.setDeltKalkulacje(this);

		if (delZaliczki.getDkzFormaWyplaty()==null) {
			delZaliczki.ustawDomyslnaFormeWyplatyWgZaliczekWnioskuCzI();
		}

		przypiszZFDoWyplat(delZaliczki);
		if (delZaliczki.getDkzNrKonta()==null) delZaliczki.tryUstawDomyslnyNrKonta();

		//kurs waluty - jeśli dodawana zaliczka nie ma żadnego kursu (albo jest domylsna wartosc 1.0)
		if (delZaliczki.getDkzKurs()==null || eq(1.0,delZaliczki.getDkzKurs())) {
			if (eq("PLN",delZaliczki.getWalSymbol())){
				delZaliczki.setDkzKurs(1.0);
			} else { //potrzebny przelcznik
				final KursWaluty kurs = delZaliczki.getKurs();
				if (kurs!=null)
					delZaliczki.setDkzKurs(kurs.getPrzelicznik());
			}
		}

		return delZaliczki;
	}


	public DelZaliczki addNewDeltZaliczki(){
		//ustawienia domyslne i wywalic delZaliczki kontrolerów metody addNewZaliczka
		DelZaliczki delZaliczki = new DelZaliczki();
		if (isKrajowa()) delZaliczki.setDkzWalId(1L);
		delZaliczki.setDkzFormaWyplaty("B");
		final DeltWnioskiDelegacji wnk = this.getWniosek();
		if(wnk !=null && wnk.getWndDataWyjazdu() != null) {
			delZaliczki.setDkzDataWyp(obliczDomyslnaDateDlaZaliczki(wnk.getWndDataWyjazdu())); //w kontrolerach to obliczDomyslnaDateDlaZaliczki bolo jako checkDate
		}
		this.addDeltZaliczki(delZaliczki);
		return  delZaliczki;
	}


	public DelZaliczki removeDelZaliczki(DelZaliczki delZaliczki) {
		this.onRXSet("delZaliczki", null, delZaliczki);


		delZaliczki.removeAllDelZaliczkiZWR(true);
		delZaliczki.removeAllDeltZrodlaFinansowania();

		getDelZaliczki().remove(delZaliczki);
		delZaliczki.setDeltKalkulacje(null);


//		List<DeltZrodlaFinansowania> zfs = delZaliczki.getDeltZrodlaFinansowanias().stream().collect(Collectors.toList());
//		for (DeltZrodlaFinansowania zf : zfs) {
//			delZaliczki.removeDeltZrodlaFinansowania(zf);
//		}

		return delZaliczki;
	}




	public String getKalRodzaj() {
		this.onRXGet("kalRodzaj");
		return kalRodzaj;
	}

	public void setKalRodzaj(String kalRodzaj) {
		this.onRXSet("kalRodzaj", this.kalRodzaj, kalRodzaj);
		this.kalRodzaj = kalRodzaj;
	}



	public List<DelCzynnosciSluzbowe> getDelCzynnosciSluzbowe() {
		this.onRXGet("delCzynnosciSluzbowe");
		return delCzynnosciSluzbowe;
	}



	public void setDelCzynnosciSluzbowe(List<DelCzynnosciSluzbowe> delCzynnosciSluzbowe) {
		this.onRXSet("delCzynnosciSluzbowe", this.delCzynnosciSluzbowe, delCzynnosciSluzbowe);
		this.delCzynnosciSluzbowe = delCzynnosciSluzbowe;
	}

	public DelCzynnosciSluzbowe addDelCzynnosciSluzbowe(DelCzynnosciSluzbowe delCzynnosciSluzbowe) {
		this.onRXSet("delCzynnosciSluzbowe", null, delCzynnosciSluzbowe);
		getDelCzynnosciSluzbowe().add(delCzynnosciSluzbowe);
		delCzynnosciSluzbowe.setDeltKalkulacje(this);

		return delCzynnosciSluzbowe;
	}

	public DelCzynnosciSluzbowe removeDelCzynnosciSluzbowe(DelCzynnosciSluzbowe delCzynnosciSluzbowe) {
		this.onRXSet("delCzynnosciSluzbowe", null, delCzynnosciSluzbowe);
		getDelCzynnosciSluzbowe().remove(delCzynnosciSluzbowe);
		delCzynnosciSluzbowe.setDeltKalkulacje(null);

		return delCzynnosciSluzbowe;
	}

	public void addNewDelCzynnosciSluzbowe () {
		this.addDelCzynnosciSluzbowe(new DelCzynnosciSluzbowe());
	}


	public List<DeltPozycjeKalkulacji> getPklListByKtgDiet(String strKtgList, boolean exclude){

		List<String> kategorie = Arrays.asList(strKtgList.split(","));

		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_TPOZ_KTG", this.getDeltWnioskiDelegacji().getWndStdId());

		List<Long> tpozIds = lw.getData().stream()
				.filter(r ->(kategorie.contains(r.getIdAsString()))

				)
				.mapToLong(r -> r.getAsLong("tpoz_id"))
				.boxed()
				.collect(Collectors.toList());

		List<DeltPozycjeKalkulacji> ret;

		if (!exclude)
			ret = this.getDeltPozycjeKalkulacjis().stream()
					.filter(p -> tpozIds.contains(p.getPklTpozId())).collect(Collectors.toList());
		else
			ret = this.getDeltPozycjeKalkulacjis().stream()
					.filter(p -> !tpozIds.contains(p.getPklTpozId())).collect(Collectors.toList());

		return ret;
	}

	public boolean isValidZFPklList(List<DeltPozycjeKalkulacji> pklLst){
		long count = pklLst.stream().filter(p -> !p.isValidZFPkl()).count();
        //Utils.updateCssClassComponents(null,"validZFPklList");
		return (count==0);
	}


	public Map<String, Double> sumyPklKwotaMf(List<DeltPozycjeKalkulacji> lstPkl){
        Map<String, Double> ret = lstPkl.stream()
        		.collect(Collectors.groupingBy(p -> p.getWalSymbol(), Collectors.summingDouble(p -> nz(p.getPklKwotaMf()))));
        return ret;
//        .filter(p -> p.isRozrachunekDelegowanego())
    }

	public Map<String, Double> sumyPklKwotaMf(){
		return this.sumyPklKwotaMf(this.getDeltPozycjeKalkulacjis());
	}


	public List<String> walutySymbole(){

		List<String> ret = new ArrayList<>();

		ret.addAll(
                this.getDeltPozycjeKalkulacjis()
				.stream()
				.map(p -> p.getWalSymbol())
				.distinct()
//				.sorted()
				.collect(Collectors.toList())
        );

        ret.addAll(
		this.getDelZaliczki().stream()
                .map(z->z.getWalSymbol())
                .distinct()
//                .sorted()
                .collect(Collectors.toList())
        );

		return ret.stream().filter(w->w!=null).distinct().sorted().collect(Collectors.toList());
	}



//	public List<DeltPozycjeKalkulacji> getDeltPozycjeKalkulacjisBySymbolWaluty(String walSymbol) {
//		return this.getDeltPozycjeKalkulacjis().stream()
//				.filter(pkl->eq(walSymbol, pkl.getWalSymbol()))
//				.sorted( (p1,p2) -> p1.getSortKey().compareTo(p2.getSortKey()) )
//				.collect(Collectors.toList());
//	}


//	public List<DelAngaze> getDelAngazeBySymbolWaluty(String walSymbol){
//		List<DelAngaze> ret = new ArrayList<>();
//
//		this.getDeltPozycjeKalkulacjisBySymbolWaluty(walSymbol).stream()
//				.forEach(pkl-> pkl.getDeltZrodlaFinansowanias().stream()
//						.forEach(zf->ret.addAll(zf.getDelAngazes())));
//
//		return ret;
//	}



	public List<DelAngaze> getDelAngaze(){
		List<DelAngaze> ret = new ArrayList<>();
		this.getDeltPozycjeKalkulacjis().stream()
				.forEach(pkl-> pkl.getDeltZrodlaFinansowanias().stream()
						.forEach(zf->ret.addAll(zf.getDelAngazes())));

		return ret;
	}

	public HashMap<KursWaluty, List<DelAngaze>> getAngazeByKursyWalut(){
		HashMap<KursWaluty, List<DelAngaze>> ret = new HashMap<>();

		for (DelAngaze a : this.getDelAngaze()) {
			List<DelAngaze> al = ret.get(a.getKurs());
			if (al==null) al = new ArrayList<>();
			al.add(a);
			ret.put(a.getKurs(),al);
		}

		return ret;
	}

	public List<KursWaluty> getSortedKusryWalut(HashMap<KursWaluty, List<DelAngaze>> podsKalkulacji){
		Set<KursWaluty> kws = podsKalkulacji.keySet();
		return kws.stream().sorted((k1,k2)->k1.getSortKey().compareTo(k2.getSortKey())).collect(Collectors.toList());
	}


	public double sumaAngazeKwota(List<DelAngaze> la){
		return la.stream().mapToDouble(a->nz(a.getAngKwota())).sum();
	}

	public double sumaAngazeKwotaPLN(List<DelAngaze> la){
		return la.stream().mapToDouble(a->nz(a.getKwotaPLN())).sum();
	}

	public double sumaAngazeKwotaPLN(){
		return this.getDelAngaze().stream().mapToDouble(a->nz(a.getKwotaPLN())).sum();
	}


	public double sumaAngazeKwotaPLNGotowkaPrzelew(){
		return this.getDelAngaze().stream()
				.filter(
						a->a.getDeltZrodlaFinansowania()!=null && a.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji()!=null
						&& (a.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji().isGotowka() || a.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji().isPrzelew())
		) .mapToDouble(a->nz(a.getKwotaPLN())).sum();
	}

	public double sumaAngazeKwotaPLNNieGotowkaNiePrzelew(){
		return this.getDelAngaze().stream()
				.filter(
						a->a.getDeltZrodlaFinansowania()!=null && a.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji()!=null
								&& (!a.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji().isGotowka() && !a.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji().isPrzelew())
				) .mapToDouble(a->nz(a.getKwotaPLN())).sum();
	}



	public List<DelZaliczki> getDokumentyDKZwNZ(){
		if("WST".equals(this.kalRodzaj))
			return this.getDelZaliczki().stream().filter(z -> z.getDkzDokId() != null).collect(Collectors.toList());

		else if (this.getDeltWnioskiDelegacji()!=null)
			return this.getDeltWnioskiDelegacji().getDokumentyDKZwNZ();
		else
			return new ArrayList<>();
	}

	public List<DelZaliczki> getDokumentyDKZwNZ(Long walId){
		return this.getDokumentyDKZwNZ().stream().filter(z-> eq(z.getDkzWalId(), walId) ).collect(Collectors.toList());
	}

	public List<DelZaliczki> getDokumentyDKZwNZ(String walSymbol){
		return this.getDokumentyDKZwNZ().stream().filter(z->eq(z.getWalSymbol(),walSymbol)).collect(Collectors.toList());
	}

	public double  sumaDokumentyDKZwNZ(Long walId){
		return this.getDokumentyDKZwNZ(walId).stream().mapToDouble(z->nz(z.getDkzKwota())).sum();
	}

	public double  sumaDokumentyDKZwNZ(String walSymbol){
		return this.getDokumentyDKZwNZ(walSymbol).stream()
				.filter(z -> !z.getCzyDokAnulowany())
				.mapToDouble(z->nz(z.getDkzKwota())).sum();
	}

	public double  sumaPlnDokumentyDKZwNZ(String walSymbol){
		return this.getDokumentyDKZwNZ(walSymbol).stream().mapToDouble(z->nz(z.getKwotaPLN())).sum();
	}

	public double sumaPKL(String walSymbol){
		return this.getDeltPozycjeKalkulacjis().stream().filter(p->eq(p.getWalSymbol(), walSymbol)
																	&& ("01".equals(p.getPklFormaPlatnosci()) ||"04".equals(p.getPklFormaPlatnosci())))
														.mapToDouble(p->nz(p.getPklKwotaMf())).sum();
	}

	public double sumaPKL(Long walId){
		return this.getDeltPozycjeKalkulacjis().stream().filter(p->eq(p.getPklWalId(), walId)
																	&& ("01".equals(p.getPklFormaPlatnosci()) ||"04".equals(p.getPklFormaPlatnosci())))
														.mapToDouble(p->nz(p.getPklKwotaMf())).sum();
	}

	public double sumaPKLWszystkie(String walSymbol){
		return this.getDeltPozycjeKalkulacjis().stream().filter(p->eq(p.getWalSymbol(), walSymbol))
														.mapToDouble(p->nz(p.getPklKwotaMf())).sum();
	}


	public List<DelZaliczki> getDokumentyDKZPozaNZ(){
		List<DelZaliczki> collect = this.getDelZaliczki().stream().filter(z->z.getDkzDokId()==null).collect(Collectors.toList());
		return collect;
	}


	public List<DelZaliczki> getDokumentyDKZPozaNZ(String walSymbol){
		return this.getDokumentyDKZPozaNZ().stream().filter(z->eq(z.getWalSymbol(), walSymbol)).collect(Collectors.toList());
	}

	public double sumaDokumentyDKZPozaNZ(String walSymbol){
		return this.getDokumentyDKZPozaNZ(walSymbol).stream().mapToDouble(z->nz(z.getDkzKwota())).sum();
	}

	public double getKwotaPklPozaDKZ(String walSymbol){
		return round(this.sumaPKL(walSymbol) - this.sumaDokumentyDKZwNZ(walSymbol) - this.sumaDokumentyDKZPozaNZ(walSymbol),2);
	}


//	public KursWaluty kursSredniNBP(String walSymbol){
//		return new KursWalutySredniNBP( this.getWalId(walSymbol) , this.getKalDataKursu());
//	}

	public KursWaluty kursSprzedazyNBP(String walSymbol){
		return new KursWalutySprzedazyNBP( this.getWalId(walSymbol) , this.getKalDataKursu());
	}

	public double getKwotaPklPozaDKZBySprzedazyPLN(String walSymbol){
		return  multiply(getKwotaPklPozaDKZ(walSymbol), kursSprzedazyNBP(walSymbol).getPrzelicznik(),2);
	}




//	public double getKwotaPLN(String walSymbol){
//		double sumaPlnDkzNZ = sumaPlnDokumentyDKZwNZ(walSymbol);
//		double sumaPlnDkzPozaNZ = getDokumentyDKZPozaNZ(walSymbol).stream().mapToDouble(z -> z.getKwotaPLN()).sum();
//		double resztaPklPozaDkz = getKwotaPklPozaDKZBySprzedazyPLN(walSymbol);
//		return round(sumaPlnDkzNZ + sumaPlnDkzPozaNZ + resztaPklPozaDkz,2);
//	}


	public double getKwotaPLN(){
		return getKwotaPLNwNZ() + getKwotaPLNPozaNZ();
	}

	public double getKwotaPLNwNZ(){
		double ret = this.getDokumentyDKZwNZ().stream()
				.filter(z-> !z.getCzyDokAnulowany())
				.mapToDouble(z->z.getKwotaPLN()).sum(); //DKZ/NZ wypłacone także jako zaliczki z cz. II
		return ret;
	}

	public double getKwotaPLNPozaNZ(){
		double ret = this.getDelZaliczkiReszta().stream().mapToDouble(z -> z.getKwotaPLN()).sum();
		return ret;
	}

	public double getKwotaPLNWstepna(){
		return getKwotaWstepnaPLNwNZ() + getKwotaWstepnaPLNPozaNZ();
	}

	public double getKwotaWstepnaPLNwNZ(){
		double ret = this.getDokumentyDKZwNZ().stream()
				.filter(z-> !z.getCzyDokAnulowany())
				.mapToDouble(z->z.getKwotaPlnWstepna()).sum(); //DKZ/NZ wypłacone także jako zaliczki z cz. II
		return ret;
	}

	public double getKwotaWstepnaPLNPozaNZ(){
		double ret = this.getDelZaliczkiReszta().stream().mapToDouble(z -> z.getKwotaPlnWstepna()).sum();
		return ret;
	}










	public List<DelZaliczki> getDkzListPodsumowanie(){
		List<DelZaliczki> ret = new ArrayList<>();

		ret.addAll(this.getDokumentyDKZwNZ());

		ret.addAll(this.getDelZaliczkiReszta());

		return ret.stream()
				.sorted((z1,z2)-> (z1.podsSortKey().compareTo(z2.podsSortKey())))
				.collect(Collectors.toList());
	}




	public List<DelZaliczki> getDelZaliczkiReszta(){//dokumenty DKZ (w tym zaliczki) ktore nie zostaly wygenerowane/wyplacone w NZ
		this.onRXGet("delZaliczkiReszta");

		List<DelZaliczki> ret = this.getDokumentyDKZPozaNZ();
		if (ret.isEmpty()) {
			obliczDelZaliczkiReszta();
			ret = this.getDokumentyDKZPozaNZ();
		}

		return ret;
	}



	public void clearDelZaliczkiReszta(){
		if("WNK".equals(this.getKalRodzaj()))
			return;

		List<DelZaliczki> ret = this.getDokumentyDKZPozaNZ();// this.getDelZaliczki().stream().filter(z->z.getDkzDokId()==null).collect(Collectors.toList());
		for (DelZaliczki z : ret) {

//			if(eq(nz(this.getDeltWnioskiDelegacji().getWndRodzaj()), 2L)) {

//				List<DeltPozycjeKalkulacji> pkldoUsunieciaDopelnienie = new ArrayList();


//				List<DeltPozycjeKalkulacji> dopelnienieWalutyLista = this.getDeltPozycjeKalkulacjisPozostaleKoszty(this.getTpozIdByKtgPozycji("D"));
//				if(dopelnienieWalutyLista != null) {
//					for(DeltPozycjeKalkulacji p : dopelnienieWalutyLista) {
//						if(eq("T", p.getPklFDopelnienie())) {
//
//							boolean czyUsuwać = true;
//
////							tukej warunek z zf, w tym miejscu mamy jedynie pkl z dopelninia , teraz będziemy patrzeć czy taki pkl ma zrodla fin które są związane z zaliczką i jeśłi ta zaliczka została już wypłacona
////							to go nie usuniemy
//
//							List<DeltZrodlaFinansowania> zrodlaFinDanegoPkl = p.getDeltZrodlaFinansowanias().stream().filter(zf -> zf.getDelZaliczki() != null).collect(Collectors.toList());
//							if(zrodlaFinDanegoPkl != null) {
//
//								for (DeltZrodlaFinansowania zf : zrodlaFinDanegoPkl) {
//
//									if(this.getDelZaliczki().contains(zf.getDelZaliczki())) {
//										if(zf.getDelZaliczki().getDkzDokId() != null && !zf.getDelZaliczki().getCzyDokAnulowany())
//										czyUsuwać = false;
//									} else continue;
//								}
//
//							}
//
//							if(czyUsuwać)
//								pkldoUsunieciaDopelnienie.add(p);
//
//						}
//
//					}
//				}

//				if(nz(pkldoUsunieciaDopelnienie.size()) > 0) {
//					for(DeltPozycjeKalkulacji p : pkldoUsunieciaDopelnienie) {
//						this.removeDeltPozycjeKalkulacji(p);
//					}
//				}

//			}
			//tukej metoda która usunie powiązane pkl dla danej zaliczki

			this.removeDelZaliczki(z);
			DelZaliczki z0 = z.getDelZaliczkaZWR();
			if (z0!=null) {
				z0.removeDelZaliczkiZWR(z);
			}

		}

		this.onRXSet("delZaliczkiReszta", null, ret);
	}




	public void clearDelZaliczkiReszta(Long walId){
		if("WNK".equals(this.getKalRodzaj()))
			return;

		List<DelZaliczki> ret = this.getDokumentyDKZPozaNZ();// this.getDelZaliczki().stream().filter(z->z.getDkzDokId()==null).collect(Collectors.toList());
		for (DelZaliczki z : ret) {
			if (!eq(z.getDkzWalId(), walId))
				continue;

			this.removeDelZaliczki(z);
			DelZaliczki z0 = z.getDelZaliczkaZWR();
			if (z0!=null) {
				z0.removeDelZaliczkiZWR(z);
			}
		}

		this.onRXSet("delZaliczkiReszta", null, ret);
	}


	public void clearAllZaliczki(){
		final List<DelZaliczki> zl = this.getDelZaliczki().stream().collect(Collectors.toList());
		zl.stream().forEach(z->this.removeDelZaliczki(z));
	}


	public void obliczDelZaliczkiReszta(){
		//this.onRXSet("delZaliczkiReszta", null, this.delZaliczkiReszta); //za późno - to getter i faza 6 (render response)
		for (String w : this.getDeltWnioskiDelegacji().walutySymbole()) {
			obliczDelZaliczkiReszta(w);
		}
	}



	public void obliczDelZaliczkiReszta(String walSymbol){
		if (walSymbol==null) return;

		final Long walId = this.getWalId(walSymbol);
		double kwotaPklPozaDKZ = this.getKwotaPklPozaDKZ(walSymbol);

		if (kwotaPklPozaDKZ<0.0){ //zrot(y) od pracownika

			List<DelZaliczki> dkzNzLst = this.getDokumentyDKZwNZ(walSymbol).stream()
					.filter(z->nz(z.getDkzKwota())>0.0 && z.getDkzDokId()!=null)//tylko dodatnie wartosci (wyplacone)...
					//.sorted( (z1,z2)->z1.getDkzDokId().compareTo(z2.getDkzDokId()) ) //rosnaco wg dkz_dok_id
					.sorted( (z1,z2)->z2.getDkzDokId().compareTo(z1.getDkzDokId()) ) //malejaco wg dkz_dok_id
					.collect(Collectors.toList());

			for (DelZaliczki z0 : dkzNzLst) {
				double kwotaZwrotu = kwotaPklPozaDKZ;
				if ((-1*kwotaZwrotu) > z0.getDkzKwota()){
					kwotaZwrotu = -1*z0.getDkzKwota();
				}

				DelZaliczki zwr = doliczJakoDkzZwr(kwotaZwrotu, walSymbol, z0);
				kwotaPklPozaDKZ -= zwr.getDkzKwota();

				if (kwotaPklPozaDKZ>=0)
					break;
			}
		} else if (kwotaPklPozaDKZ>0.0){//wyplaty dla pracownika

			double limitToKwotaPklPozaDKZ = kwotaPklPozaDKZ;
			//obliczanie dkz / przelew
			final DelZaliczki dkzPrzelew = tryGenerujNowyDkzDlaWyplaty(walSymbol, "04", limitToKwotaPklPozaDKZ);//dla fpl przelew
			if (dkzPrzelew!=null) {
				this.addDeltZaliczki(dkzPrzelew);
				limitToKwotaPklPozaDKZ = limitToKwotaPklPozaDKZ - nz(dkzPrzelew.getDkzKwota());
			}


			//obliczanie dkz / gotówka
			final DelZaliczki dkzGotowka = tryGenerujNowyDkzDlaWyplaty(walSymbol, "01", limitToKwotaPklPozaDKZ);//dla fpl gotówka

			if (eq("WST",this.getKalRodzaj()) && dkzGotowka!=null)
				tryGenerujAktualizujDopelnienieDkzDoNominaluNBP(dkzGotowka);

			if (dkzGotowka!=null)
				this.addDeltZaliczki(dkzGotowka);
			else
				removePklDopelnieniaNBP(walId);//interferencja


//				Double dopelnijDoNominalWalutyNBP = dopelnijDoNominalWalutyNBP(nz(kwotaPklPozaDKZ), this.getWalId(walSymbol));
//            	double roznica =  nz(kwotaPklPozaDKZ) - dopelnijDoNominalWalutyNBP;

//            	DelZaliczki zal = doliczJakoDkz(kwotaPklPozaDKZ, walSymbol);
//
//            	if (roznica != 0) {
//            		if (eq("WST",this.getKalRodzaj()) && eq("K", zal.getDkzFormaWyplaty()) && nz(zal.getDkzKwota()) > 0.0){
//						DeltPozycjeKalkulacji doliczonePkl = doliczJakoPkl(roznica, walSymbol);
//						List<DeltZrodlaFinansowania> listaZf = new ArrayList<>();
//
//						//uwaga na wiele zf!!! może lepiej jedno ?
//						for( DeltZrodlaFinansowania pkzf : zal.getDeltZrodlaFinansowanias()) {
//							DeltZrodlaFinansowania cloneZF = pkzf.clone(true);
//							listaZf.add(cloneZF);
//						}
//            			zal.setDkzKwota(dopelnijDoNominalWalutyNBP);
//            			doliczonePkl.setPklKwota(doliczonePkl.getPklStawka(), true);
//						DeltZrodlaFinansowania maxZf = listaZf.stream().max(Comparator.comparingDouble(DeltZrodlaFinansowania::getPkzfKwota)).orElseThrow(NoSuchElementException::new);
//						if(maxZf != null) {
//
//							doliczonePkl.addDeltZrodlaFinansowania(maxZf);
//							maxZf.setPkzfProcent(100.0);
//							maxZf.setPkzfKwota(doliczonePkl.getPklKwotaMf());
//						}
////            			}
//            			this.przypiszZFDoWyplat(zal);
//            		}
//            		else {
//						this.przypiszZFDoWyplat(zal);
//					}
//            	}
		}

		//TODO przeliczyc pola PKZF_KURS w kazdym ZF!!!!
	}

	public void removePklDopelnieniaNBP(Long walId) {
		final Long typDopelnienieWalutNBP = this.getTpozIdByKtgPozycji("D");
		final List<DeltPozycjeKalkulacji> pklDopelnieniaNBPToRemoveLst = this.getDeltPozycjeKalkulacjis().stream()
				.filter(p -> p.getDelTypyPozycji() != null
						&& eq(typDopelnienieWalutNBP, p.getDelTypyPozycji().getTpozId())
						&& eq(p.getPklWalId(), walId)
				).collect(Collectors.toList());
		for (DeltPozycjeKalkulacji pkl : pklDopelnieniaNBPToRemoveLst) {
			this.superRemoveDeltPozycjeKalkulacji(pkl);
            this.onRXSet("deltPozycjeKalkulacjis", null, this.deltPozycjeKalkulacjis);
		}
	}

	public void tryGenerujAktualizujDopelnienieDkzDoNominaluNBP(DelZaliczki dkzGotowka) {
        //dopelnienie do nominalów stosowanych w NBP
        if (dkzGotowka == null
                || !dkzGotowka.isFormaWyplatyGotowka()
                || dkzGotowka.getDkzWalId() == null
                || eq(nz(dkzGotowka.getDkzKwota()), 0.0D)
        )
            return;

        Double kwotaDkzPoDopelnieniu = dopelnijDoNominalWalutyNBP(dkzGotowka.getDkzKwota(), dkzGotowka.getDkzWalId());
        double roznica =  round(nz(kwotaDkzPoDopelnieniu - dkzGotowka.getDkzKwota() ),2);
        if (roznica == 0) return;

		final Long typDopelnienieWalutNBP = this.getTpozIdByKtgPozycji("D");

		DeltPozycjeKalkulacji pkl;
		pkl = this.getDeltPozycjeKalkulacjis().stream()
				.filter(p->p.getDelTypyPozycji()!=null
						&& eq(typDopelnienieWalutNBP, p.getDelTypyPozycji().getTpozId())
						&& eq(p.getPklWalId(), dkzGotowka.getDkzWalId())
				)
				.findFirst().orElse(null);

		if (pkl==null) {
			pkl = new DeltPozycjeKalkulacji();
			pkl.setPklKwotaPrzelewu(0L);
			pkl.setPklKwotaGotowki(" ");
			pkl.setPklRefundacjaKwota(0.0);
			pkl.setPklTpozId(typDopelnienieWalutNBP);
			pkl.setPklWalId(dkzGotowka.getDkzWalId());
			pkl.setPklOpis("Dopełnienie wg nominału NBP " + dkzGotowka.getWalSymbol());
			pkl.setPklFDopelnienie("T");

			//skopiowanie ZF z dkzGotowka do PKL
			for (DeltZrodlaFinansowania pkzf : dkzGotowka.getDeltZrodlaFinansowanias()) {
				pkl.addDeltZrodlaFinansowania(pkzf.clone(true));
			}

		} else {
			this.superRemoveDeltPozycjeKalkulacji(pkl);//na chwilę aby nie zapetlic sie pkl.przeliczKwote();
		}

		pkl.setPklIlosc(1.0);
		pkl.setPklStawka(roznica);
		pkl.setPklKwotaMf(roznica);


		this.addDeltPozycjeKalkulacji(pkl, true);
//		pkl.setPklKwota(pkl.getPklStawka(), true); //po co? pkl.przeliczKwote(); //bo to zapetla sie - > stackOverflow

		//przepiecie pkl.ZF do dkzGotowka
		for (DeltZrodlaFinansowania zf : pkl.getDeltZrodlaFinansowanias()) {
			if (zf.getDelZaliczki()!=null){
				zf.getDelZaliczki().removeDeltZrodlaFinansowania(zf);
			}
			dkzGotowka.addDeltZrodlaFinansowania(zf);
		}

		dkzGotowka.setDkzKwota(kwotaDkzPoDopelnieniu);


    }


    public DelZaliczki tryGenerujNowyDkzDlaWyplaty(String walSymbol, String pklFormaPlatnosci_01_04, double limitToKwtowaPozNZ) {
	    if (this.getWniosek()==null || limitToKwtowaPozNZ<=0.0) return null;

		final Long walId = getWalId(walSymbol);

		final Long typDopelnienieWalutNBP = this.getTpozIdByKtgPozycji("D");

		final List<DelAngaze> angLst = this.getDelAngaze().stream()
				.filter(a ->
						a.getDeltZrodlaFinansowania().getDelZaliczki() == null//tylko co nie przypisane
					&& a.getDeltZrodlaFinansowania()!=null
					&& a.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji()!=null
					&& a.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji().getDelTypyPozycji()!=null
					&& !eq(typDopelnienieWalutNBP, a.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji().getDelTypyPozycji().getTpozId()) //tylko co ma ta sama walute
					&& eq(walId, a.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji().getPklWalId()) //tylko co ma ta sama walute
					&& eq(pklFormaPlatnosci_01_04, a.getDeltZrodlaFinansowania().getDeltPozycjeKalkulacji().getPklFormaPlatnosci()) //tylko co ma wskazana forme platnosci
				).collect(Collectors.toList());

		if (eq("ROZ",this.getKalRodzaj())){
		    //odjąc zaliczkiz wyplacone z cz II
            //this.getWniosek().getKalkulacjaWstepna().getDelZaliczki().stream().fi
        }

		double dkzKwota = angLst.stream().mapToDouble(a -> a.getAngKwota()).sum();//to na cz. II  moze byc za duzo o to co ew. przeszło na cz II zaliczkami - uwzglednic limit
		if (dkzKwota==0.0) return null;
		if (dkzKwota>limitToKwtowaPozNZ) dkzKwota = limitToKwtowaPozNZ;

		DelZaliczki zal = new DelZaliczki();
		zal.setDkzKwota(dkzKwota);
		zal.setDkzWalId(this.getWalId(walSymbol));
		zal.setDkzDataWyp(getDomyslnaDataWyplatyDlaDKZ());
		zal.setDkzFormaWyplaty(eq(pklFormaPlatnosci_01_04, "01") ? "K" : "B");

		final Set<DeltZrodlaFinansowania> zfSet = angLst.stream().map(a -> a.getDeltZrodlaFinansowania()).collect(Collectors.toSet());
		zfSet.stream().forEach(zf->zal.addDeltZrodlaFinansowania(zf));
		//TODO - jesli "B" - trzeba kombinowac z numerem konta
//		this.addDeltZaliczki(zal); //przypisanie angazy
		return zal;
	}





	private DelZaliczki doliczJakoDkz(double kwota, String walSymbol) {
        if (kwota<=0.0) return null;//oczekiwana jest dodatnia kwota

		DelZaliczki zal = new DelZaliczki();
		zal.setDkzKwota(kwota);
		zal.setDkzWalId(this.getWalId(walSymbol));
		zal.setDkzDataWyp(getDomyslnaDataWyplatyDlaDKZ());
		//zal.setDeltKalkulacje(this);//this.addDeltZaliczki(zal); ...wirtualny DKZ - tylko dla wyliczenia podsumowania...

		this.addDeltZaliczki(zal);
		return zal;
	}

	// TODO trzeba zadbac o to aby jeden zwrot pasowal do jednego dkz wyplaty... od wprowadzenia 10010 wszystki mize sie sypac...
	private DelZaliczki doliczJakoDkzZwr(double kwotaZwrotu, String walSymbol, DelZaliczki zwrotDoDkz) {
	    if (kwotaZwrotu>=0.0) return null;//zwrot - oczekiwana jest ujemna kwota

		DelZaliczki zwr = new DelZaliczki();
		zwr.setDkzKwota(kwotaZwrotu);
		zwr.setDkzWalId(this.getWalId(walSymbol));
		zwr.setDkzDataWyp(Utils.today());
//		zwr.setDkzKurs(zwrotDoDkz.getKurs().przelicznik); zle - za wczesnie --> DelZaliczki.setDelZaliczkaZWR(...)

		zwrotDoDkz.addDelZaliczkiZWR(zwr);
		this.addDeltZaliczki(zwr);
		return zwr;
	}


	public void usunPodzialZFvsDKZ(){
        for (DeltPozycjeKalkulacji pkl : this.getDeltPozycjeKalkulacjis()) {
            for (DeltZrodlaFinansowania zf : pkl.getDeltZrodlaFinansowanias()) {
                if (zf.getDelZaliczki()!=null){
                    zf.getDelZaliczki().removeDeltZrodlaFinansowania(zf);
                }
            }
            pkl.grupujZF();
        }


		for (DelZaliczki dkz : this.getDelZaliczki()) {
			List<DeltZrodlaFinansowania> zfl = dkz.getDeltZrodlaFinansowanias().stream().collect(Collectors.toList());
			for (DeltZrodlaFinansowania zf : zfl) {
				//(zf.getDeltPozycjeKalkulacji().getDeltKalkulacje())
				dkz.removeDeltZrodlaFinansowania(zf);
			}
		}
	}




    public void przeliczPodzialZFvsDKZ(){
		if (eq("WNK",this.getKalRodzaj()))
			return;

        this.usunPodzialZFvsDKZ();
		this.clearDelZaliczkiReszta();

		this.przypiszZFDoWszystkichNieprzypisanychDKZzNZ(); //przypisuje PKL/ZF do DKZ/NZ
		this.obliczDelZaliczkiReszta();//dodaje DKZ na roznice (i przy dodawaniu tworzyl podzial PKLZF/DKZ)
    }




    public Date getDomyslnaDataWyplatyDlaDKZ() {
		Date ret = Utils.today();

		if ("WST".equals(this.kalRodzaj) && this.getDeltWnioskiDelegacji()!=null) {
			Date wndDataWyjazdu = this.getDeltWnioskiDelegacji().getWndDataWyjazdu();
			if (wndDataWyjazdu==null) return ret;

			ret = Utils.addDays(wndDataWyjazdu, -5);


			if (Utils.dayOfMonth(ret)==28) //bo sobie postanowili w MF, że 28 dnia zadnych zaliczek nie wyplacaja
				ret = Utils.addDays(wndDataWyjazdu, -1);

			if (eq(Utils.dayOfWeek(ret), DayOfWeek.SATURDAY))
				ret = Utils.addDays(wndDataWyjazdu, -1);

			if (eq(Utils.dayOfWeek(ret), DayOfWeek.SUNDAY))
				ret = Utils.addDays(wndDataWyjazdu, -2);

			if (Utils.dayOfMonth(ret)==28) //bo sobie postanowili w MF, że 28 dnia zadnych zaliczek nie wyplacaja
				ret = Utils.addDays(wndDataWyjazdu, -1);


			if (ret.before(Utils.today()))
				ret = Utils.today();
		}

		return ret;
	}




	public void dodajDkzTest(){
		//TODO dodac zaliczke na wybrana walute i kwote (zal. okno dialogowe pozwala ustalic parametry do metody dodajDkz...)
		//do kazdej PKL/ZF ustawic PKZF_DKZ_ID...

		//nowa zaliczka z okna dlg...
		DelZaliczki zal = new DelZaliczki();
		zal.setDkzKwota(450.0);
		zal.setDkzWalId(50000L);
		zal.setDkzDataWyp(Utils.now());
		this.addDeltZaliczki(zal);
	}




	public void przypiszZFDoWyplat(DelZaliczki zal) {
		if(zal==null
				|| eq("WNK", this.getKalRodzaj()) //nie przypisujemy zf/dkz z cz. I (kal. wniosekująca)
				|| zal.getCzyDokAnulowany() //nie przypisujemy zf/dkz gdzie anulowano dok.
//		|| !zal.getDeltZrodlaFinansowanias().isEmpty()  to błąd: musimy przypisać do dkz z cz II zf z cz III nawet jesli przypisano juz do tej samej dkz z cz II zf z cz. II
		)
			return;


		final long cntWlasneZf = zal.getDeltZrodlaFinansowanias()
				.stream()
				.filter(zf -> zf.getDeltPozycjeKalkulacji() != null
						&& eq(this, zf.getDeltPozycjeKalkulacji().getDeltKalkulacje())
				).count();

		if (cntWlasneZf>0)
			return; //nie przypisuj zf (z tej kalkulacji) jesli juz je przpisano
		//(cntWlasneZf>0) -> do (zal)iczki są przypisane zf z tej kalkulacji
		// ale tworzymy potencjalnie dwa dkz na przelew oraz dkz na gotówke


		Long dkzWalId = zal.getDkzWalId();//EUR
//		double sumaZwroty = zal.getDelZaliczkiZwr().stream().mapToDouble(z->nz(z.getDkzKwota())).sum();
//		double zalKwotaDoZwiazania = nz(zal.getDkzKwota()) - sumaZwroty;
//		if (zalKwotaDoZwiazania==0.0) return;

		double zalKwotaDoZwiazania = zal.getKwotaWyplacana(); //kwota pomniejszona o zwroty

		if (zalKwotaDoZwiazania==0)
			return;//pusty DKZ - wyjscie

		if (zalKwotaDoZwiazania<0 && zal.getDelZaliczkaZWR()!=null  ) { //zwrot - kase zwracamy do ZF/ANG skad byla zabrana
			List<DeltZrodlaFinansowania> zfLstDlaZwrotu = zal.getDelZaliczkaZWR().getDeltZrodlaFinansowaniasDlaKalkulacji(zal.getDelZaliczkaZWR().getDeltKalkulacje());
			for (DeltZrodlaFinansowania zf : zfLstDlaZwrotu) {
				DeltZrodlaFinansowania zfzwr = zf.clone(true);
				if (nz(zf.getPkzfKwota()) >= -zalKwotaDoZwiazania) {
					zfzwr.setPkzfKwota(zalKwotaDoZwiazania);
					zfzwr.przeliczAngaze();
				}else {//w zf jest za malo
					zfzwr.setPkzfKwota(-1 * nz(zf.getPkzfKwota()));
				}

				zal.addDeltZrodlaFinansowania(zfzwr);
				zalKwotaDoZwiazania -= zfzwr.getPkzfKwota();
				if (zalKwotaDoZwiazania>=0)
					return;//wyjscie...
			}
			return;//wyjscie (albo chodzi o zwrot)...
		}




		List<DeltPozycjeKalkulacji> pkls = this.getDeltPozycjeKalkulacjis().stream()
				.filter(pkl -> eq(pkl.getPklWalId(), dkzWalId) && ("01".equals(pkl.getPklFormaPlatnosci()) ||"04".equals(pkl.getPklFormaPlatnosci()))) //tylko z okreslona waluta

				//.sorted( ... wg pklId? )
				.collect(Collectors.toList());

		for (DeltPozycjeKalkulacji pkl : pkls) {

			List<DeltZrodlaFinansowania> zfs = pkl.getDeltZrodlaFinansowanias().stream()
					.filter(zf->zf.getDelZaliczki() == null)
					//.sorted( ... wg pkzflId? )
					.collect(Collectors.toList());

			for (DeltZrodlaFinansowania zf : zfs) {

				double zfKwota = nz(zf.getPkzfKwota());

				if (zalKwotaDoZwiazania >= zfKwota){

					zal.addDeltZrodlaFinansowania(zf);//zf.setDelZaliczki( zal ); //wystarczy przypisac ZF do DKZ

				}else{ //zalKwota < zfKwota -> rozbic ZF na dwie czesci; aktualne zf zwiazac z dkz, a roznice dodac jako nowe zf...

					double roznica = round(zfKwota - zalKwotaDoZwiazania,2);
					zal.addDeltZrodlaFinansowania(zf); //zf.setDelZaliczki( zal ); // ... przypisac ZF do DKZ
					zf.setPkzfKwota(zalKwotaDoZwiazania);

					DeltZrodlaFinansowania zf1 = zf.clone( true ); //kopia z angazami
					pkl.addDeltZrodlaFinansowania(zf1);
					zf1.setPkzfKwota(roznica);
					return; //wyjscie...
				}

				zalKwotaDoZwiazania = round(zalKwotaDoZwiazania - zfKwota,2);
				if (zalKwotaDoZwiazania<=0) return;//wyjscie...
			}

		}
	}



    public void usunZFZeWszystkichDKZwNZ() {

        List<DelZaliczki> dkzl = this.getDokumentyDKZwNZ();
        for (DelZaliczki dkz : dkzl) {
            List<DeltZrodlaFinansowania> zf2remove = dkz.getDeltZrodlaFinansowaniasDlaKalkulacji(this);
            for (DeltZrodlaFinansowania zf : zf2remove) {
                dkz.removeDeltZrodlaFinansowania(zf);
            }
        }

    }

	public void przypiszZFDoWszystkichNieprzypisanychDKZzNZ() {

		List<DelZaliczki> dkzl = this.getDokumentyDKZwNZ();
		for (DelZaliczki dkz : dkzl) {
			if (!dkz.getDeltZrodlaFinansowaniasDlaKalkulacji(this).isEmpty())
				continue;//ten dkz jest juz powiazany z tą (this) kalkulacją
			else
				this.przypiszZFDoWyplat(dkz);
		}
	}



//	public void generujDKZ(){
//
//		//TODO utrwalic DKZ Wirtualne przez this.addDeltZaliczki.... albo nie robic zbiorczego utrwalacza tylko buttony [polecenie wpłaty/wypłaty]...
//
//		//kasowanie niewypłaconych dkz
//		List<DelZaliczki> zl = this.getDelZaliczki().stream().collect(Collectors.toList());
//		for (DelZaliczki z : zl) {
//			if (z.getDkzDokId()==null) {
//				this.removeDelZaliczki(z);
//			}
//		}
//
//
//		//powiazanie ZF do wyplaconych DKZ w NZ, tak aby kolejna generowana wyplata złapała nowe ZFy z rozliczenia
//		for (String w : this.walutySymbole()) {
//			List<DelZaliczki> dkzNzLst = this.getDokumentyDKZwNZ(w);
//			for (DelZaliczki z0 : dkzNzLst) {
//				przypiszZFDoWyplat(z0);
//			}
//		}
//
//
//
//		//dodawanie dkz dla roznicy (jesli to nie 0)
//		for (String w : this.getDeltWnioskiDelegacji().walutySymbole()) {
//			double kwotaPklPozaDKZ = this.getKwotaPklPozaDKZ(w);
//			//TODO jesli kwotaPklPozaDKZ < 0 to trzeba zwiazac nowy DKZ jako zwrot z wybranym dkz zaliczki/wyplaty...
//			//czyli kazdy wiekszy zwrot musi byc podzielony i przypisany do jednej zaliczki, z ktorej pobierzemy kurs waluty oraz podzial ZF/Ang...
//			//czyli trzeba dodac kolumne DKZ_ZWRDO_DKZ_ID, gdzie umiescimy info do jakiego DKZ robimy zwrot.t
//
//			if (kwotaPklPozaDKZ<0.0){ //zrot(y) od pracownika
//				List<DelZaliczki> dkzNzLst = this.getDokumentyDKZwNZ(w).stream()
//						.filter(z->nz(z.getDkzKwota())>0.0 && z.getDkzDokId()!=null)//tylko dodatnie wartosci (wyplacone)...
//						//.sorted( (z1,z2)->z1.getDkzDokId().compareTo(z2.getDkzDokId()) ) //rosnaco wg dkz_dok_id
//						.sorted( (z1,z2)->z2.getDkzDokId().compareTo(z1.getDkzDokId()) ) //malejaco wg dkz_dok_id
//						.collect(Collectors.toList());
//
//				for (DelZaliczki z0 : dkzNzLst) {
//					double kwZwr = kwotaPklPozaDKZ;
//					if ((-1*kwZwr) > z0.getDkzKwota()){
//						kwZwr = -1*z0.getDkzKwota();
//					}
//
//					DelZaliczki zwr = new DelZaliczki();
//					zwr.setDkzKwota(kwZwr);
//					zwr.setDkzWalId(this.getWalId(w));
//					zwr.setDkzDataWyp(Utils.now());
//					zwr.setDkzKurs(z0.getKurs().przelicznik);//WAZNE
//					this.addDeltZaliczki(zwr);
//					//przypiszZFDoWyplat(zwr); TODO ustawic na zwr DKZ_DKZ_ID...
//
//					kwotaPklPozaDKZ -= zwr.getDkzKwota();
//					if (kwotaPklPozaDKZ>=0)
//						return;
//				}
//
//			} else if (kwotaPklPozaDKZ>0.0){//wyplaty dla pracownika
//
//				DelZaliczki zal = new DelZaliczki();
//				zal.setDkzKwota(kwotaPklPozaDKZ);
//				zal.setDkzWalId(this.getWalId(w));
//				zal.setDkzDataWyp(Utils.now());
//				this.addDeltZaliczki(zal);
//				//przypiszZFDoWyplat(zal);
//			}
//
//
//		}
//
//	}



//	public void usunDKZ(DelZaliczki zal) {
//		if(zal.getDkzDokId() != null) {
//			try (Connection conn = JdbcUtils.getConnection()) {
//				Long dokIdDoUsuniecia = zal.getDkzDokId();
//				JdbcUtils.sqlSPCall(conn, "PPADM.ppp_del_zaliczki.usun_zal",dokIdDoUsuniecia);
//				this.removeDelZaliczki(zal);
//				HibernateContext.saveOrUpdate(this.getDeltWnioskiDelegacji());
//			} catch (Exception e) {
//				User.alert(e);
//			}
//		} else {
//			this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", this.getKalId()).resetTs();
//			this.removeDelZaliczki(zal);
//		}
//	}

	public void wystornujPkl(DeltPozycjeKalkulacji pkl) {

		DeltPozycjeKalkulacji pklStornujaca = pkl.clone(true);

		this.addDeltPozycjeKalkulacji(pklStornujaca);
		pklStornujaca.setPklIlosc(nz(pklStornujaca.getPklIlosc())*-1);
		pklStornujaca.setPklPklId(pkl.getPklId());

		DeltPozycjeKalkulacji pklKorygujaca = pkl.clone(true);
		this.addDeltPozycjeKalkulacji(pklKorygujaca);

	}



	 public boolean isBlokujZmianyDkzPoNaliczeniuKalkulacjiRozliczajacej(){
		if(this.getDeltWnioskiDelegacji()==null )
			return true;

		 return eq("WST", this.getKalRodzaj())
				 && !this.getDeltWnioskiDelegacji().getKalkulacjaRozliczenie().getDeltPozycjeKalkulacjis().isEmpty();//aktualna kalkulacja wstepna, ale na rozliczeniu naliczono jakikolwiek PKL

	 }


	 public String getOpisTrasyKrajeMiasta(){
		String ret = "";

		 List<DeltTrasy> trasy = this.getDeltTrasies();
		 if (trasy ==null || trasy.isEmpty() ){
			return ret;
		}

		 DeltTrasy trs0 = trasy.get(0);
		 trs0.getKrajOdSymbol();

		if (this.getDeltWnioskiDelegacji()!=null && this.getDeltWnioskiDelegacji().isZagraniczna()) {
			ret+= String.format("(%1$s)%2$s", trs0.getKrajOdSymbol(), trs0.getTrsMiejscowoscOd());
			for (DeltTrasy trs : trasy) {
				ret+= String.format(" - (%1$s)%2$s", trs.getKrajDoSymbol(), trs.getTrsMiejscowoscDo());
			}
		} else {
			ret+= String.format("%1$s", trs0.getTrsMiejscowoscOd());
			for (DeltTrasy trs : trasy) {
				ret+= String.format(" - %1$s", trs.getTrsMiejscowoscDo());
			}
		}
		return ret;
	 }

	public boolean walidujRyczaltKM() {
		boolean ret = true;
		if(this.getDeltPozycjeKalkulacjis() != null && !this.getDeltPozycjeKalkulacjis().isEmpty()) {

			List<DeltPozycjeKalkulacji> ryczaltyKM = this.getDeltPozycjeKalkulacjis().stream().filter(pkl -> eq("T", pkl.getPklFWniosek()) && eq("KM",pkl.getKategoria())).collect(Collectors.toList());
			if (ryczaltyKM == null)
				return true;
			if(ryczaltyKM != null && ryczaltyKM.size() == 0)  {
				return true;
			} else if (ryczaltyKM != null && ryczaltyKM.size() > 0) {
				if(eq("WNK", this.kalRodzaj)) {
					List<DeltSrodkiLokomocji> srodkiLokSamPryw = this.getWniosek().getDeltSrodkiLokomocjis()
							.stream().filter(sl -> eq("03", sl.getSlokSrodekLokomocji()) || eq("07", sl.getSlokSrodekLokomocji())).collect(Collectors.toList());
					if(srodkiLokSamPryw != null && srodkiLokSamPryw.size() > 0)
						return false;
				} else if(!eq("WNK", this.kalRodzaj)) {
					List<DeltTrasy> trasySamPryw = this.getDeltTrasies().stream()
							.filter(trs -> eq("03", trs.getTrsSrodekLokomocji()) || eq("07", trs.getTrsSrodekLokomocji())).collect(Collectors.toList());
					if(trasySamPryw != null && trasySamPryw.size() > 0)
						return false;
				}
			}
		}
		return ret;
	}


	 public boolean sprawdzDostepnoscKursowWalut() {
		 boolean ret = true;

		 Set<Long> listaWalId = this.getDeltPozycjeKalkulacjis().stream().filter(pkl -> pkl.getPklWalId() != null && pkl.getPklWalId() != 1L).map(pkl -> pkl.getPklWalId()).collect(Collectors.toSet());

		for(Long walId : listaWalId) {
			DataRow kursPrzelicznikSprzedazyNBPRow = Utils.kursPrzelicznikSprzedazyNBPRow(walId, this.kalDataKursu);
			if(!eq(kursPrzelicznikSprzedazyNBPRow.get("KW_DATA"), this.kalDataKursu))
				return false;
		}


		 return ret;
	 }

	 //dla zaznaczonych zaliczek, które nie mają jeszcze nr dokumnetu w FIN generuje odpowiednie dokumenty w NZ
	 public void generujDokumentyNZDlaZaznaczonych() {
		 if (this.getDelZaliczki() == null || this.getDelZaliczki().isEmpty()) return;

		 List<DelZaliczki> listaDkz = this.getDelZaliczki().stream().filter(dkz -> dkz.getDkzDokId() == null && eq("T", dkz.getDkzFDokNz())).collect(Collectors.toList());
		 if (listaDkz.isEmpty()) {
			 User.warn("Nie zaznaczono żadnych dokumentów do wygenerowania lub dla zaznaczonych wierszy dokumenty zostały już wygenerowane");
			 return;
		 }

		 if (this.getWniosek().validate()) {
			 try (HibernateContext h = new HibernateContext()) {
				 HibernateContext.saveOrUpdate(h, this.getWniosek(),false);
				 User.info("Zapisano wniosek " + this.getWniosek().getWndNumer());
				 for (DelZaliczki dkz : listaDkz) {
					 dkz.generujDokumentNZ(h);
				 }
//				 HibernateContext.refresh(this.getWniosek()); -> dsw. same DKZ  -> DelZaliczki ->  generujDokumentNZ ... HibernateContext.refresh(h, this); ...
			 } catch (Exception e) {
				 User.alert(e);
				 return;
			 }
		 }
	 }

	 public boolean validateDelZaliczki() {
		boolean ret = true;
		if(eq("ROZ",this.getKalRodzaj()) && this.getDeltTrasies().isEmpty()) {
			return true;
		}


		 if (this.getDelZaliczki() != null && !this.getDelZaliczki().isEmpty()) {
			 List<DelZaliczki> zaliczkiBezDokId = this.getDelZaliczki().stream()
					 .filter(dkz -> dkz.getDkzDokId() == null).collect(Collectors.toList()); // Chcemy chyba tylko walidować jeszcze nie wygenerowane dokumenty
			 for (DelZaliczki dkz : zaliczkiBezDokId) {
				 if (!dkz.walidujDkz()) {
					 return false;
				 }
			 }
		 }

		 return ret;
	 }


	@Override
	public DeltWnioskiDelegacji getWniosek() {
		return this.getDeltWnioskiDelegacji();
	}



	public boolean validate(){
		boolean ret = true //w validate zawsze stosujmy operator & (zachłanny/sprawdza obie strony)
				& this.validateDeltTrasies()
				& this.validatePozycjeKalkulacjis()
				& this.validateDelZaliczki()
				& this.validateDataKursu();
		return ret;
	}

	public boolean validatePozycjeKalkulacjis() {
		boolean ret = true;
		for (DeltPozycjeKalkulacji pkl : this.getDeltPozycjeKalkulacjis()) {
		 	ret = ret & pkl.validate();// & nie &&
		}
		return ret;
	}
}