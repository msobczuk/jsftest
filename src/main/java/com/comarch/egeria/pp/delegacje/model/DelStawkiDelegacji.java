package com.comarch.egeria.pp.delegacje.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The persistent class for the PPT_DEL_STAWKI_DELEGACJI database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_STAWKI_DELEGACJI")
@NamedQuery(name="DelStawkiDelegacji.findAll", query="SELECT p FROM DelStawkiDelegacji p")
public class DelStawkiDelegacji implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_STAWKI_DELEGACJI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_STAWKI_DELEGACJI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_STAWKI_DELEGACJI")
	@Column(name="STD_ID")
	private long stdId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="STD_AUDYT_DM")
	private Date stdAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="STD_AUDYT_DT")
	private Date stdAudytDt;

	@Column(name="STD_AUDYT_KM")
	private String stdAudytKm;

	@Column(name="STD_AUDYT_KT", updatable=false)
	private String stdAudytKt;

	@Column(name="STD_AUDYT_LM")
	private BigDecimal stdAudytLm;

	@Column(name="STD_AUDYT_UM")
	private String stdAudytUm;

	@Column(name="STD_AUDYT_UT", updatable=false)
	private String stdAudytUt;


	@Column(name="STD_KOD_STAWKI")
	private String stdKodStawki;

	@Column(name="STD_NAZWA_STAWKI")
	private String stdNazwaStawki;

	@Column(name="STD_OPIS")
	private String stdOpis;

	@Column(name="STD_STAN_DEFINICJI")
	private String stdStanDefinicji;
	
/*	@Column(name="STD_TYP_DIETY")
	private String stdTypDiety;
	
	@Column(name="STD_RODZAJ_DIETY")
	private String stdRodzajDiety;*/

	//bi-directional many-to-one association to DelTypyPozycji
	@OneToMany(mappedBy="delStawkiDelegacji",  cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<DelTypyPozycji> delTypyPozycjis =  new ArrayList<>();
	
	//bi-directional many-to-one association to DelTypyPozycji
	@OneToMany(mappedBy="delStawkiDelegacji",  cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<DelWartosciStawek> delWartosciStaweks = new ArrayList<>();
	
/*	@OneToMany(mappedBy="delStawkiDelegacji",  cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<DeltWnioskiDelegacji> deltWnioskiDelegacjis = new ArrayList<>();*/

	public DelStawkiDelegacji() {
	}

	public long getStdId() {
		return this.stdId;
	}

	public void setStdId(long stdId) {
		this.stdId = stdId;
	}

	public Date getStdAudytDm() {
		return this.stdAudytDm;
	}

	public void setStdAudytDm(Date stdAudytDm) {
		this.stdAudytDm = stdAudytDm;
	}

	public Date getStdAudytDt() {
		return this.stdAudytDt;
	}

	public void setStdAudytDt(Date stdAudytDt) {
		this.stdAudytDt = stdAudytDt;
	}

	public String getStdAudytKm() {
		return this.stdAudytKm;
	}

	public void setStdAudytKm(String stdAudytKm) {
		this.stdAudytKm = stdAudytKm;
	}

	public String getStdAudytKt() {
		return this.stdAudytKt;
	}

	public void setStdAudytKt(String stdAudytKt) {
		this.stdAudytKt = stdAudytKt;
	}

	public BigDecimal getStdAudytLm() {
		return this.stdAudytLm;
	}

	public void setStdAudytLm(BigDecimal stdAudytLm) {
		this.stdAudytLm = stdAudytLm;
	}

	public String getStdAudytUm() {
		return this.stdAudytUm;
	}

	public void setStdAudytUm(String stdAudytUm) {
		this.stdAudytUm = stdAudytUm;
	}

	public String getStdAudytUt() {
		return this.stdAudytUt;
	}

	public void setStdAudytUt(String stdAudytUt) {
		this.stdAudytUt = stdAudytUt;
	}


	public String getStdKodStawki() {
		return this.stdKodStawki;
	}

	public void setStdKodStawki(String stdKodStawki) {
		this.stdKodStawki = stdKodStawki;
	}

	public String getStdNazwaStawki() {
		return this.stdNazwaStawki;
	}

	public void setStdNazwaStawki(String stdNazwaStawki) {
		this.stdNazwaStawki = stdNazwaStawki;
	}

	public String getStdOpis() {
		return this.stdOpis;
	}

	public void setStdOpis(String stdOpis) {
		this.stdOpis = stdOpis;
	}

	public String getStdStanDefinicji() {
		return this.stdStanDefinicji;
	}

	public void setStdStanDefinicji(String stdStanDefinicji) {
		this.stdStanDefinicji = stdStanDefinicji;
	}

	public List<DelTypyPozycji> getDelTypyPozycjis() {
		return this.delTypyPozycjis;
	}

	public void setDelTypyPozycjis(List<DelTypyPozycji> delTypyPozycjis) {
		this.delTypyPozycjis = delTypyPozycjis;
	}

	public DelTypyPozycji addDelTypyPozycji(DelTypyPozycji delTypyPozycji) {
		getDelTypyPozycjis().add(delTypyPozycji);
		delTypyPozycji.setDelStawkiDelegacji(this);

		return delTypyPozycji;
	}

	public DelTypyPozycji removeDelTypyPozycji(DelTypyPozycji delTypyPozycji) {
		getDelTypyPozycjis().remove(delTypyPozycji);
		delTypyPozycji.setDelStawkiDelegacji(null);

		return delTypyPozycji;
	}

	public List<DelWartosciStawek> getDelWartosciStaweks() {
		return delWartosciStaweks;
	}

	
	public List<DelWartosciStawek> getDelWartosciStaweksInnePobytowe() {
		return delWartosciStaweks.stream().
				filter(w->w.getWstKrId()==null && "P".equals(w.getWstTypDiety() ) )
				.collect(Collectors.toList());
	}
	
	public List<DelWartosciStawek> getDelWartosciStaweksInneHotelowe() {
		return delWartosciStaweks.stream().
				filter(w->w.getWstKrId()==null && "H".equals(w.getWstTypDiety() ) )
				.collect(Collectors.toList());
	}
	
	
	public void setDelWartosciStaweks(List<DelWartosciStawek> delWartosciStaweks) {
		this.delWartosciStaweks = delWartosciStaweks;
	}
	
	public DelWartosciStawek addDelWartosciStawek(DelWartosciStawek delWartosciStawek) {
		getDelWartosciStaweks().add(delWartosciStawek);
		delWartosciStawek.setDelStawkiDelegacji(this);

		return delWartosciStawek;
	}

	public DelWartosciStawek removeDelWartosciStawek(DelWartosciStawek delWartosciStawek) {
		getDelWartosciStaweks().remove(delWartosciStawek);
		delWartosciStawek.setDelStawkiDelegacji(null);

		return delWartosciStawek;
	}

/*	public String getStdTypDiety() {
		return stdTypDiety;
	}

	public void setStdTypDiety(String stdTypDiety) {
		this.stdTypDiety = stdTypDiety;
	}

	public String getStdRodzajDiety() {
		return stdRodzajDiety;
	}

	public void setStdRodzajDiety(String stdRodzajDiety) {
		this.stdRodzajDiety = stdRodzajDiety;
	}*/

/*	public List<DeltWnioskiDelegacji> getDeltWnioskiDelegacjis() {
		return deltWnioskiDelegacjis;
	}

	public void setDeltWnioskiDelegacjis(List<DeltWnioskiDelegacji> deltWnioskiDelegacjis) {
		this.deltWnioskiDelegacjis = deltWnioskiDelegacjis;
	}
	
	public DeltWnioskiDelegacji addDeltWnioskiDelegacji(DeltWnioskiDelegacji deltWnioskiDelegacji) {
		getDeltWnioskiDelegacjis().add(deltWnioskiDelegacji);
		deltWnioskiDelegacji.setDelStawkiDelegacji(this);

		return deltWnioskiDelegacji;
	}

	public DeltWnioskiDelegacji removeDeltWnioskiDelegacji(DeltWnioskiDelegacji deltWnioskiDelegacji) {
		getDeltWnioskiDelegacjis().remove(deltWnioskiDelegacji);
		deltWnioskiDelegacji.setDelStawkiDelegacji(null);

		return deltWnioskiDelegacji;
	}*/

}