package com.comarch.egeria.pp.delegacje.model;

import com.comarch.egeria.Params;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.zalaczniki.ZalacznikiBean;
import com.comarch.egeria.web.User;
import com.lowagie.text.DocumentException;
import net.sf.jasperreports.engine.JRException;

import javax.persistence.Entity;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.nz;
import static com.comarch.egeria.Utils.today;

/**
 * logika biznesowa delegacji krajowych
 * @author mateusz.bogusz
 */
@Entity
public class WniosekDelegacjiKraj extends DeltWnioskiDelegacji {



	public SqlDataSelectionsHandler getLwTypyPozycji(){
		return getLW("PPL_DEL_TYPY_POZYCJI_K");
	}

	@Override
	public SqlDataSelectionsHandler getRodzajeKosztow() {
		return  getLW("PPL_DEL_TYPY_POZYCJI_K"); //dla krajowych rodzaje kosztow to to samo co typy pozycji, bo nie stosujemy stawek MPiPS oraz FCS???
	}


	public  void generujRaportyJakoZalacznikiPdfDlaDokID(Object dokId,  boolean czyRaportPolecenia,  boolean czyRaportRozliczenia,  boolean czyZalacznikDlaNZ_DEL_FK5) throws SQLException, IOException, JRException, DocumentException {
		//wygenerowanie raportu wniosku i rozliczenia i podpięcia ich jako zal do wniosku
		Params params = new Params("wnd_id", this.getWndId());//this.dodajRaportyDoZal();
	
		if (czyRaportPolecenia) {
			ZalacznikiBean.generujRaportPdfZapiszJakoZalacznik("Delegacja_krajowa_polecenie", params, "ZAL_DOK_ID", dokId, "raport polecenia");
		}
	
		if (czyRaportRozliczenia) {
			ZalacznikiBean.generujRaportPdfZapiszJakoZalacznik("Delegacja_krajowa_rozliczenie", params, "ZAL_DOK_ID", dokId, "raport rozliczenia");
		}
	
//		ZalacznikiBean.zapiszZalacznik(com.comarch.egeria.web.common.reports.ReportGenerator.generujSlElToPdfData("DELZ_FK5_EL2PDF"), "DELZ_FK5.pdf",  "ZAL_DOK_ID", dokId ,"raport polecenia - informacje dodatkowe");
	}

	@Override
	public boolean validate() {
		List<DeltSrodkiLokomocji> srodkiBezZgody = this.getDeltSrodkiLokomocjis().stream().filter(
				sl -> ("02".equals(sl.getSlokSrodekLokomocji()) && "N".equals(sl.getSlokFCzyZgodaPojazdSlu()))).collect(Collectors.toList());
		if(srodkiBezZgody.size() > 0 ) {
			User.warn("Nie zaznaczono potwierdzenia uzyskania akceptacji zapotrzebowania na samochód służbowy");
		}

		List<DeltSrodkiLokomocji> specjalneSrodki = this.getDeltSrodkiLokomocjis().stream()
				.filter(slok -> (eq(true,slok.czyWymagaDodatkowejZgody()))).collect(Collectors.toList());

		List<DeltSrodkiLokomocji> samochodPrywatnyBezZgody = specjalneSrodki.stream().filter(sl -> eq("03", sl.getSlokSrodekLokomocji()) || eq("07", sl.getSlokSrodekLokomocji()))
				.filter(sl -> eq("N",sl.getSlokFCzyZgodaPojazdPryw())).collect(Collectors.toList());

		if(samochodPrywatnyBezZgody.size() > 0 ) {
			User.warn("Nie zaznaczono potwierdzenia uzyskania akceptacji zapotrzebowania na samochód prywatny");
		}

		if(specjalneSrodki.size() > 0) {
			SqlDataSelectionsHandler lwZalaczniki = getLW("SQLZALACZNIKI",this.getWndId(), "ZAL_WND_ID");
			if(lwZalaczniki.getData().size() < 1) {
				User.warn("Wybrano środek transportu, który wymaga dołączenia dodatkowej zgody jako załącznik");
			}
		}

		//TODO przerobić ten warunek bo teraz zawsze będzie wyskakiwał przy zapisie
		if("T".equals(this.getWndFMiejscowoscPobyt())){
				User.warn("W przypadku zaznaczenia checkbox'a 'Pobyt stały lub czasowy delegowanego miejscowością delegowania' należy usunąć z rodzajów kosztów "
						+ "pozycje dieta pobytowa i należności za nocleg");
		}

		return super.validate();
	}




	public void ustawStawkeWaluteDietyPKL(DeltPozycjeKalkulacji p, Date pobytOd) {
		Date pobytOdOrToday = (pobytOd != null)  ? pobytOd : today();

		final SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_STAWKI_DIET_ZAGR", this.getWndStdId());

		final DataRow dr = lw.getData().stream()
				.filter(r -> eq(p.getPklKrId(), r.getAsLong("WST_KR_ID"))
						&& pobytOdOrToday.after(r.getAsDate("WST_OBOWIAZUJE_OD"))
				).findFirst().orElse(null);

		if (dr!=null){
			p.setPklWalId(nz(dr.getAsLong("WST_WAL_ID")));
			p.setPklStawka(nz(dr.getAsDouble("WST_KWOTA")));
		}

//		for (DataRow r : lw.getData()) {
//			String krIdWst = "" + r.get("WST_KR_ID");
//			String walIdWst = "" + r.get("WST_WAL_ID");
//			String kwota = "" + r.get("WST_KWOTA");
//			Date obowiazujeOd = (Date) r.get("WST_OBOWIAZUJE_OD");
//			if (krPkl.equals(krIdWst) && pobytOd.after(obowiazujeOd)) {
//				p.setPklWalId(Long.parseLong(walIdWst));
//				p.setPklStawka(Double.parseDouble(kwota));
//				return;
//			}
//		}
	}

	public void ustawStawkeiWaluteDietyHotel(DeltPozycjeKalkulacji p, Date pobytOd){
		Date pobytOdOrToday = (pobytOd != null)  ? pobytOd : today();

		final SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_STAWKI_DIET_HOT", this.getWndStdId());

		final DataRow dr = lw.getData().stream()
				.filter(r -> eq(p.getPklKrId(), r.getAsLong("WST_KR_ID"))
						&& pobytOdOrToday.after(r.getAsDate("WST_OBOWIAZUJE_OD"))
				).findFirst().orElse(null);

		if (dr!=null){
			p.setPklWalId(nz(dr.getAsLong("WST_WAL_ID")));
			p.setPklStawka(nz(dr.getAsDouble("WST_KWOTA")));
		}

		//		//		long tStart = new Date().getTime();
//
//		String krPkl = "" + p.getPklKrId();
//		for (DataRow r : this.getLW("PPL_DEL_STAWKI_DIET_HOT", this.wniosek.getWndStdId()).getData()) {
//			String krIdWst = "" + r.get("WST_KR_ID");
//			String walIdWst = "" + r.get("WST_WAL_ID");
//			String kwota = "" + r.get("WST_KWOTA");
//			Date obowiazujeOd = (Date) r.get("WST_OBOWIAZUJE_OD");
//			if (krPkl.equals(krIdWst) && pobytOd.after(obowiazujeOd)) {
//				p.setPklWalId(Long.parseLong(walIdWst));
//				p.setPklStawka(Double.parseDouble(kwota));
//				przeliczKwote(p);
//				return;
//			}
//		}
//		przeliczKwote(p);
//		//		System.out.println("Czas trwania metody ustawStawkeiWaluteDietyHotel : " + (new Date().getTime() - tStart));
	}

}
