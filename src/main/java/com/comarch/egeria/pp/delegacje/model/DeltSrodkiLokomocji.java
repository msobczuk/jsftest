package com.comarch.egeria.pp.delegacje.model;

import com.comarch.egeria.web.User;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static com.comarch.egeria.Utils.nz;


/**
 * The persistent class for the PPT_DEL_SRODKI_LOKOMOCJI database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_SRODKI_LOKOMOCJI", schema="PPADM")
@NamedQuery(name="DeltSrodkiLokomocji.findAll", query="SELECT d FROM DeltSrodkiLokomocji d")
public class DeltSrodkiLokomocji extends DelBase implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_SRODKI_LOKOMOCJI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_SRODKI_LOKOMOCJI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_SRODKI_LOKOMOCJI")	
	@Column(name="SLOK_ID")
	private long slokId;

	@Temporal(TemporalType.DATE)
	@Column(name="SLOK_AUDYT_DM")
	private Date slokAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="SLOK_AUDYT_DT", updatable=false)
	private Date slokAudytDt;

	@Column(name="SLOK_AUDYT_KM")
	private String slokAudytKm;

//	@Column(name="SLOK_AUDYT_KT")
//	private String slokAudytKt;

	@Column(name="SLOK_AUDYT_LM")
	private String slokAudytLm;

	@Column(name="SLOK_AUDYT_UM")
	private String slokAudytUm;

	@Column(name="SLOK_AUDYT_UT", updatable=false)
	private String slokAudytUt;

//	@Column(name="SLOK_LP")
//	private Long slokLp;

/*	@Column(name="SLOK_MARKA")
	private String slokMarka;*/

	@Column(name="SLOK_OPIS_SRODKA_LOKOMOCJI")
	private String slokOpisSrodkaLokomocji;

/*	@Column(name="SLOK_POJEMNOSC")
	private String slokPojemnosc;*/

	@Column(name="SLOK_SRODEK_LOKOMOCJI")
	private String slokSrodekLokomocji;
	
	@Column(name="SLOK_F_CZY_UMOWA_Z_WYK")
	private String slokFCzyUmowaZWyk;
	
	@Column(name="SLOK_F_CZY_ZGODA_POJAZD_PRYW")
	private String slokFCzyZgodaPojazdPryw;
	
	@Column(name="SLOK_F_CZY_ZGODA_POJAZD_SLU")
	private String slokFCzyZgodaPojazdSlu;

	//bi-directional many-to-one association to DeltWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="SLOK_WND_ID")
	private DeltWnioskiDelegacji deltWnioskiDelegacji;


	public DeltSrodkiLokomocji clone(){
		
		DeltSrodkiLokomocji lok = new DeltSrodkiLokomocji();
		lok.slokId = 0L;
		lok.slokAudytDm = slokAudytDm;
		lok.slokAudytDt = slokAudytDt;
		lok.slokAudytKm = slokAudytKm;
		lok.slokAudytLm = slokAudytLm;
		lok.slokAudytUm = slokAudytUm;
		lok.slokAudytUt = slokAudytUt;
		lok.slokOpisSrodkaLokomocji = slokOpisSrodkaLokomocji;
		lok.slokSrodekLokomocji = ""+slokSrodekLokomocji;
		lok.deltWnioskiDelegacji = null; //rodzic! 
		lok.slokFCzyUmowaZWyk = slokFCzyUmowaZWyk;
		lok.slokFCzyZgodaPojazdPryw = slokFCzyZgodaPojazdPryw;
		lok.slokFCzyZgodaPojazdSlu = slokFCzyZgodaPojazdSlu;
		
		return lok;
	}
	
	
	
	public DeltSrodkiLokomocji() {
	}

	public long getSlokId() {
		return this.slokId;
	}

	public void setSlokId(long slokId) {
		this.slokId = slokId;
	}

	public Date getSlokAudytDm() {
		return this.slokAudytDm;
	}

	public void setSlokAudytDm(Date slokAudytDm) {
		this.slokAudytDm = slokAudytDm;
	}

	public Date getSlokAudytDt() {
		return this.slokAudytDt;
	}

	public void setSlokAudytDt(Date slokAudytDt) {
		this.slokAudytDt = slokAudytDt;
	}

	public String getSlokAudytKm() {
		return this.slokAudytKm;
	}

	public void setSlokAudytKm(String slokAudytKm) {
		this.slokAudytKm = slokAudytKm;
	}

//	public String getSlokAudytKt() {
//		return this.slokAudytKt;
//	}
//
//	public void setSlokAudytKt(String slokAudytKt) {
//		this.slokAudytKt = slokAudytKt;
//	}

	public String getSlokAudytLm() {
		return this.slokAudytLm;
	}

	public void setSlokAudytLm(String slokAudytLm) {
		this.slokAudytLm = slokAudytLm;
	}

	public String getSlokAudytUm() {
		return this.slokAudytUm;
	}

	public void setSlokAudytUm(String slokAudytUm) {
		this.slokAudytUm = slokAudytUm;
	}

	public String getSlokAudytUt() {
		return this.slokAudytUt;
	}

	public void setSlokAudytUt(String slokAudytUt) {
		this.slokAudytUt = slokAudytUt;
	}

//	public Long getSlokLp() {
//		return this.slokLp;
//	}

//	public void setSlokLp(Long slokLp) {
//		this.slokLp = slokLp;
//	}

/*	public String getSlokMarka() {
		return this.slokMarka;
	}*/

/*	public void setSlokMarka(String slokMarka) {
		this.slokMarka = slokMarka;
	}*/

	public String getSlokOpisSrodkaLokomocji() {
		return this.slokOpisSrodkaLokomocji;
	}

	public void setSlokOpisSrodkaLokomocji(String slokOpisSrodkaLokomocji) {
		this.slokOpisSrodkaLokomocji = slokOpisSrodkaLokomocji;
	}

/*	public String getSlokPojemnosc() {
		return this.slokPojemnosc;
	}

	public void setSlokPojemnosc(String slokPojemnosc) {
		this.slokPojemnosc = slokPojemnosc;
	}*/

	public String getSlokSrodekLokomocji() {
		return this.slokSrodekLokomocji;
	}

	public void setSlokSrodekLokomocji(String slokSrodekLokomocji) {
		final boolean eq = eq(this.slokSrodekLokomocji, slokSrodekLokomocji);
		if(!eq) {
			if(this.getDeltWnioskiDelegacji() != null) {
				if(eq(1L, this.getDeltWnioskiDelegacji().getWndRodzaj()) && this.czyWymagaDodatkowejZgody()) {
					User.info("Czy masz zgodę, uprawnienia na przejazd wskazanym środkiem transportu?");
				}
			}
		}
		this.slokSrodekLokomocji = slokSrodekLokomocji;
	}

	public DeltWnioskiDelegacji getDeltWnioskiDelegacji() {
		return this.deltWnioskiDelegacji;
	}

	public void setDeltWnioskiDelegacji(DeltWnioskiDelegacji deltWnioskiDelegacji) {
		this.deltWnioskiDelegacji = deltWnioskiDelegacji;
	}



	public String getSlokFCzyUmowaZWyk() {
		return slokFCzyUmowaZWyk;
	}



	public void setSlokFCzyUmowaZWyk(String slokFCzyUmowaZWyk) {
		this.slokFCzyUmowaZWyk = slokFCzyUmowaZWyk;
	}



	public String getSlokFCzyZgodaPojazdPryw() {
		return slokFCzyZgodaPojazdPryw;
	}



	public void setSlokFCzyZgodaPojazdPryw(String slokFCzyZgodaPojazdPryw) {
		this.slokFCzyZgodaPojazdPryw = slokFCzyZgodaPojazdPryw;
	}



	public String getSlokFCzyZgodaPojazdSlu() {
		return slokFCzyZgodaPojazdSlu;
	}



	public void setSlokFCzyZgodaPojazdSlu(String slokFCzyZgodaPojazdSlu) {
		this.slokFCzyZgodaPojazdSlu = slokFCzyZgodaPojazdSlu;
	}
	
//	sprawdza czy środek lokomocji to pociąg I klasa, samolot bądz samochód prywatny bo takie środki wymagają dodatkowej zgody
	public boolean czyWymagaDodatkowejZgody() {
		if(nz(this.slokSrodekLokomocji).isEmpty())
			return false;
		
		String slokPremium = "01,03,04,07";
		return slokPremium.contains(this.slokSrodekLokomocji);
	}

	@Override
	public DeltWnioskiDelegacji getWniosek() {
		return this.getDeltWnioskiDelegacji();
	}
}