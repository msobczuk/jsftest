package com.comarch.egeria.pp.delegacje.model;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.*;


/**
 * The persistent class for the DEL_POZYCJE_KALKULACJI database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_POZYCJE_KALKULACJI", schema="PPADM")
@DiscriminatorFormula("'PozycjaKalkulacjiMF'")
@NamedQuery(name="DeltPozycjeKalkulacji.findAll", query="SELECT d FROM DeltPozycjeKalkulacji d")
public class DeltPozycjeKalkulacji extends DelBase implements Serializable, Cloneable {
	private static final Long serialVersionUID = 1L;

	@Id
	@TableGenerator(name = "TABLE_KEYGEN_PPT_DEL_POZYCJE_KALKULACJI", table = "PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_POZYCJE_KALKULACJI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.TABLE, generator = "TABLE_KEYGEN_PPT_DEL_POZYCJE_KALKULACJI")
	@Column(name = "PKL_ID")
	long pklId;

	@Column(name = "PKL_PKL_ID")
	Long pklPklId;

	@Temporal(TemporalType.DATE)
	@Column(name = "PKL_AUDYT_DM")
	Date pklAudytDm;


	@Temporal(TemporalType.DATE)
	@Column(name = "PKL_AUDYT_DT", updatable = false)
	Date pklAudytDt;

	@Column(name = "PKL_AUDYT_KM")
	String pklAudytKm;

//	@Column(name="PKL_AUDYT_KT")
//	private String pklAudytKt;

	@Column(name = "PKL_AUDYT_LM")
	String pklAudytLm;

	@Column(name = "PKL_AUDYT_UM")
	String pklAudytUm;

	@Column(name = "PKL_AUDYT_UT", updatable = false)
	String pklAudytUt;

//	@Column(name="PKL_F_OSOBA_TOWARZYSZACA")
//	private String pklFOsobaTowarzyszaca;

	@Column(name = "PKL_ILOSC")
	Double pklIlosc;

//	@Column(name="PKL_KAL_ID")
//	private Long pklKalId;

	//bi-directional many-to-one association to DeltKalkulacje
	@ManyToOne
	@JoinColumn(name = "PKL_KAL_ID")
	DeltKalkulacje deltKalkulacje;

	@Column(name = "PKL_KR_ID")
	Long pklKrId = 1L;//domyślnie Polska

	@Column(name = "PKL_KWOTA")
	Double pklKwota;

	@Column(name = "PKL_KWOTA_GOTOWKI")
	String pklKwotaGotowki;

	@Column(name = "PKL_KWOTA_PRZELEWU")
	Long pklKwotaPrzelewu;

//	@Column(name="PKL_LP")
//	private Long pklLp;

	@Column(name = "PKL_OPIS")
	String pklOpis;

	@Column(name = "PKL_STAWKA")
	Double pklStawka;

	@Column(name = "PKL_TPOZ_ID")
	Long pklTpozId;

	@Column(name = "PKL_WAL_ID")
	Long pklWalId;

	@Column(name = "PKL_KOSZT_MF_PROCENT")
	Double pklKosztMFProcent;

	@Column(name = "PKL_KOSZT_ZAPR_PROCENT")
	Double pklKosztZaprProcent;

	@Column(name = "PKL_KOSZT_ZAPR_KWOTA")
	Double pklKosztZaprKwota;

	@Column(name = "PKL_REFUNDACJA_PROCENT")
	Double pklRefundacjaProcent;

	@Column(name = "PKL_REFUNDACJA_KWOTA")
	Double pklRefundacjaKwota;


	@Column(name = "PKL_SNIADANIA")
	Long pklSniadania;

	@Column(name = "PKL_OBIADY")
	Long pklObiady;

	@Column(name = "PKL_KOLACJE")
	Long pklKolacje;

	@Column(name = "PKL_KWOTA_PLN")
	Double pklKwotaPln;

	@Column(name = "PKL_KURS")
	Double pklKurs;

	@Column(name = "PKL_ROK")
	Long pklRok;

	@Column(name = "PKL_REFUNDAJCA_OPIS")
	String pklRefundacjaOpis;

	@Column(name = "PKL_KWOTA_MF")
	Double pklKwotaMf;

	@Column(name = "PKL_F_CZY_UMOWA_Z_WYK")
	String pklFCzyUmowaZWyk;

	@Column(name = "PKL_F_CZY_BEZPLATNA")
	String pklFCzyBezplatna;

	@Column(name = "PKL_FORMA_PLATNOSCI")
	String pklFormaPlatnosci = "01";

	@ManyToOne
	@JoinColumn(name = "PKL_TRS_ID")
	DeltTrasy deltTrasy;

	@Temporal(TemporalType.DATE)
	@Column(name = "PKL_TERMIN_WYKUPU")
	Date pklTerminWykupu;

	//bi-directional many-to-one association to DeltZrodlaFinansowania
	@OneToMany(mappedBy = "deltPozycjeKalkulacji", cascade = {CascadeType.ALL}, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<DeltZrodlaFinansowania> deltZrodlaFinansowanias = new ArrayList<>();

	@Column(name = "PKL_F_DOPELNIENIE")
	String pklFDopelnienie = "N";

	@Column(name = "PKL_F_WNIOSEK")
	String pklFWniosek = "N";

	public String getPklFWniosek() {
		this.onRXGet("pklFWniosek");
		return pklFWniosek;
	}

	public void setPklFWniosek(String pklFWniosek) {
		this.onRXSet("pklFWniosek", this.pklFWniosek, pklFWniosek);
		final boolean eq = eq(this.pklFWniosek, pklFWniosek);
		this.pklFWniosek = pklFWniosek;

		if (!eq && this.getDeltKalkulacje()!=null) {
			this.getDeltKalkulacje().forCzyDomyslnieDietaHotelowa();
		}
	}


	@Transient
	public DeltPozycjeKalkulacji cloneSrc;

	@Transient
	public DeltPozycjeKalkulacji cloneDest;


	public DeltPozycjeKalkulacji() {
	}

	public DeltPozycjeKalkulacji clone() {

		DeltPozycjeKalkulacji pkl = new DeltPozycjeKalkulacji();

		pkl.pklId = 0L;
		pkl.pklIlosc = pklIlosc;
		pkl.pklKolacje = pklKolacje;
		pkl.pklKosztMFProcent = pklKosztMFProcent;
		pkl.pklKosztZaprKwota = pklKosztZaprKwota;
		pkl.pklKosztZaprProcent = pklKosztZaprProcent;
		pkl.pklKrId = pklKrId;
		pkl.pklKwotaGotowki = pklKwotaGotowki;
		pkl.pklKwotaPrzelewu = pklKwotaPrzelewu;
		pkl.pklObiady = pklObiady;
		pkl.pklOpis = pklOpis;
		pkl.pklRefundacjaKwota = pklRefundacjaKwota;
		pkl.pklRefundacjaProcent = pklRefundacjaProcent;
		pkl.pklSniadania = pklSniadania;
		pkl.pklStawka = pklStawka;
		pkl.pklTpozId = pklTpozId;
		pkl.pklWalId = pklWalId;
		pkl.pklFormaPlatnosci = pklFormaPlatnosci;
		pkl.pklKwota = pklKwota;
		pkl.pklKwotaMf = pklKwotaMf;
		pkl.pklFCzyUmowaZWyk = pklFCzyUmowaZWyk;
		pkl.pklFCzyBezplatna = pklFCzyBezplatna;
		pkl.pklTerminWykupu = pklTerminWykupu;
		pkl.sqlBean = sqlBean;
		return pkl;

	}


	public DeltPozycjeKalkulacji clone(boolean deepCopy) {
		DeltPozycjeKalkulacji pkl = clone();
		pkl.setPklPklId(this.pklId)
		;
		if (!deepCopy) return pkl;

		for (DeltZrodlaFinansowania zr : this.deltZrodlaFinansowanias) {
			DeltZrodlaFinansowania nzf = zr.clone(deepCopy);
			nzf.setPkzfId(0L);
			pkl.addDeltZrodlaFinansowania(nzf);
		}

		return pkl;

	}


	public long getPklId() {
		this.onRXGet("pklId");
		return this.pklId;
	}

	public void setPklId(long pklId) {
		this.onRXSet("pklId", this.pklId, pklId);
		this.pklId = pklId;
	}

	public Date getPklAudytDm() {
		return this.pklAudytDm;
	}

	public void setPklAudytDm(Date pklAudytDm) {
		this.pklAudytDm = pklAudytDm;
	}

	public Date getPklAudytDt() {
		return this.pklAudytDt;
	}

	public void setPklAudytDt(Date pklAudytDt) {
		this.pklAudytDt = pklAudytDt;
	}

	public String getPklAudytKm() {
		return this.pklAudytKm;
	}

	public void setPklAudytKm(String pklAudytKm) {
		this.pklAudytKm = pklAudytKm;
	}

	public String getPklAudytLm() {
		return this.pklAudytLm;
	}

	public void setPklAudytLm(String pklAudytLm) {
		this.pklAudytLm = pklAudytLm;
	}

	public String getPklAudytUm() {
		return this.pklAudytUm;
	}

	public void setPklAudytUm(String pklAudytUm) {
		this.pklAudytUm = pklAudytUm;
	}

	public String getPklAudytUt() {
		return this.pklAudytUt;
	}

	public void setPklAudytUt(String pklAudytUt) {
		this.pklAudytUt = pklAudytUt;
	}

	public Double getPklIlosc() {
		this.onRXGet("pklIlosc");
		return this.pklIlosc;
	}

	public void setPklIlosc(Double pklIlosc) {
		if (pklIlosc != null) {
			pklIlosc = Utils.round(pklIlosc, 4);
		}


		this.onRXSet("pklIlosc", this.pklIlosc, pklIlosc);
		boolean eq = eq(this.pklIlosc, pklIlosc);
		this.pklIlosc = pklIlosc;
		if (!eq) {
			przeliczKwote(); //this.setPklKwota(nz(pklIlosc) * nz(this.pklStawka));
		}
	}

	public Long getPklKrId() {
		this.onRXGet("pklKrId");
		return this.pklKrId;
	}

	public void setPklKrId(Long pklKrId) {
		this.onRXSet("pklKrId", this.pklKrId, pklKrId);

		if (SqlBean.eq(pklKrId, this.pklKrId)) return;

		this.pklKrId = pklKrId;

		if ("P".equals(this.getKategoria())) {
			przeliczStawkaPobytowa();
		}

		if ("H".equals(this.getKategoria())) {
			przeliczStawkaHotelowa();
		}

		if ("RH".equals(this.getKategoria())) {
			przeliczStawkaHotelowa();
//			this.setPklStawka(pklStawka * 0.25); 
			przeliczKwote();
		}

		if ("KM".equals(this.getKategoria())) {
			przeliczStawkaPobytowa();
//			this.setPklStawka(this.pklStawka * 0.1);
			przeliczKwote();
		}

		if ("RD".equals(this.getKategoria())) {
			przeliczStawkaPobytowa();
		}

		if ("I".equals(this.getKategoria())) {
			this.setPklWalId(this.getWalutaWgKraju(pklKrId));
		}
	}


	public void przeliczStawkaPobytowa() {
		//przecicz wartosci diet pobytowych
		if (this.getWniosek()==null) return;
		Date dataStawkiDiety = this.deltKalkulacje.getDeltWnioskiDelegacji().getWndDataPowrotu();
		this.getWniosek().ustawStawkeWaluteDietyPKL(this, dataStawkiDiety);

//		SqlBean sql = this.getDeltKalkulacje().getDeltWnioskiDelegacji().getSqlBean();
//		if (sql instanceof DelegacjaMFBean) {
//			DelegacjaMFBean delBean = (DelegacjaMFBean) sql;
//			delBean.ustawStawkeiWaluteDietyZagr(this, dataStawkiDiety);
//		} else if (sql instanceof DelegacjaKRBean) {
//			DelegacjaKRBean delBean = (DelegacjaKRBean) sql;
//			delBean.ustawStawkeDietyKraj(this, dataStawkiDiety);
//		} else {
//			System.out.println("nieznany typ delegacji");
//			return;
//		}
	}

	private void przeliczStawkaHotelowa() {
		//przecicz wartosci diet hotelowych
		if (this.getWniosek()==null) return;
		Date dataStawkiDiety = this.deltKalkulacje.getDeltWnioskiDelegacji().getWndDataPowrotu();
		this.getWniosek().ustawStawkeiWaluteDietyHotel(this, dataStawkiDiety);

//		Date dataStawkiDiety = this.deltKalkulacje.getDeltWnioskiDelegacji().getWndDataPowrotu();
//		if (dataStawkiDiety == null)
//			dataStawkiDiety = new Date();
//		SqlBean sql = this.getDeltKalkulacje().getDeltWnioskiDelegacji().getSqlBean();
//		if (sql instanceof DelegacjaMFBean) {
//			DelegacjaMFBean delBean = (DelegacjaMFBean) sql;
//			delBean.ustawStawkeiWaluteDietyHotel(this, dataStawkiDiety);
//		} else if (sql instanceof DelegacjaKRBean) {
//			DelegacjaKRBean delBean = (DelegacjaKRBean) sql;
//			delBean.ustawStawkeiWaluteDietyHotel(this, dataStawkiDiety);
//		} else {
//			System.out.println("nieznany typ delegacji");
//			return;
//		}
	}


	public void przeliczKwote() {
		if (this.getDeltKalkulacje() == null || this.getDeltKalkulacje().getDeltWnioskiDelegacji() == null)
			return;

		long pklIlosc = ((Double) (nz(this.getPklIlosc()) * 10000)).longValue();
		long pklStawka = ((Double) (nz(this.getPklStawka()) * 10000)).longValue();

		if (pklIlosc == 0 || pklStawka == 0) {
			this.setPklKwota(0.0);
			return;
		}

		long lret = 0;
		lret = (pklIlosc * pklStawka);

		DeltWnioskiDelegacji wnd = this.deltKalkulacje.getDeltWnioskiDelegacji();
		if (wnd.isZagraniczna()) {
			lret -= nz(this.getPklSniadania()) * pklStawka * 1500; //0.15;
			lret -= nz(this.getPklObiady()) * pklStawka * 3000;// 0.30;
			lret -= nz(this.getPklKolacje()) * pklStawka * 3000; // 0.30;
		} else if (wnd.isKrajowa()) {
			lret -= nz(this.getPklSniadania()) * pklStawka * 2500; //0.25;
			lret -= nz(this.getPklObiady()) * pklStawka * 5000;// 0.50;
			lret -= nz(this.getPklKolacje()) * pklStawka * 2500; // 0.25;
		}

		if (lret < 0) lret = 0;//Pklkwota raczje nie powinna byc ujemna (np. jesli ktos zje wiecej posiłków niz sie należy)

		this.setPklKwota(Utils.round((1.0d * lret) / (10000 * 10000), 2));

	}


//
//    public static void przeliczKwote(DeltPozycjeKalkulacji p) {
//        long pklIlosc =  ((Double) (nz(p.getPklIlosc())  * 10000)).longValue() ;
//        long pklStawka = ((Double) (nz(p.getPklStawka()) * 10000)).longValue() ;
//
//        if (pklIlosc == 0 || pklStawka == 0) {
//            p.setPklKwota(0.0);
//            return;
//        }
//
//        Long ret = 0l;
//        ret = (pklIlosc * pklStawka);
//        ret -= nz(p.getPklSniadania()) * pklStawka * 1500; //0.15;
//        ret -= nz(p.getPklObiady()) * pklStawka * 3000;// 0.30;
//        ret -= nz(p.getPklKolacje()) * pklStawka * 3000; // 0.30;
//
//        if (ret < 0l) ret = 0l;
//
//        p.setPklKwota(Utils.round((1.0d * ret) / (10000*10000), 2));
//    }

	public void setKategoria(String tpozKategoria) {
		//zmienić typ pozycji i przeliczyc wartoć ... wejscie RH lub H
//		System.out.println(tpozKategoria);
		this.setPklTpozId( this.getTpozIdByKtgPozycji(tpozKategoria) );

//		this.setPklKrId(); TODO ujednolicic sposob przeliczana w setKraj i setKategoria
		if ("H".equals(this.getKategoria())) {
			przeliczStawkaHotelowa();
		}

		if ("RH".equals(this.getKategoria())) {
			przeliczStawkaHotelowa();
//			this.setPklStawka(pklStawka * 0.25);
			przeliczKwote();
		}
	}

	public String getKategoria() {
		if (this.getDeltKalkulacje() == null || this.getDeltKalkulacje().getDeltWnioskiDelegacji() == null) return null;

		SqlDataSelectionsHandler lwTP = this.getDeltKalkulacje().getDeltWnioskiDelegacji().getLwTypyPozycji();
		if (lwTP == null) return null;

		DataRow r = lwTP.find(this.getPklTpozId());
		if (r == null) return null;

		return (String) r.get("TPOZ_KATEGORIA");
	}

	public Long getLp() {
		if (this.getDeltKalkulacje() == null
				|| this.getDeltKalkulacje().getDeltWnioskiDelegacji() == null
				|| this.getWniosek()==null
		) return null;

		SqlDataSelectionsHandler lwTP = this.getWniosek().getLwTypyPozycji();
		if (lwTP == null) return null;

//		SqlBean sqlBean = this.getDeltKalkulacje().getDeltWnioskiDelegacji().getSqlBean();
//		if (sqlBean instanceof DelegacjaMFBean) {
//			DelegacjaMFBean delBean = (DelegacjaMFBean) sqlBean;
//			lwTP = delBean.getLW("PPL_DEL_TYPY_POZYCJI_Z");
//		} else if (sqlBean instanceof DelegacjaKRBean) {
//			DelegacjaKRBean delBean = (DelegacjaKRBean) sqlBean;
//			lwTP = delBean.getLW("PPL_DEL_TYPY_POZYCJI_K");
//		} else {
//			return null;
//		}

		DataRow r = lwTP.find(this.getPklTpozId());

		if (r == null) return null;
		return r.getAsLong("TPOZ_LP");// ((BigDecimal) r.get("TPOZ_LP")).longValue();
	}


//	public double obliczKwotePln() {
//		DelegacjaMFBean delBean = (DelegacjaMFBean) this.getDeltKalkulacje().getDeltWnioskiDelegacji().getSqlBean();
//		if( this.getPklKwota() != null && this.getPklWalId() != null) {
//			return this.getPklKwotaMf() * (delBean.zwrocKursRozliczeniaWaluty(this.getPklWalId())/delBean.getJednostkaKursu(this.getPklWalId()));
//		}
//		return 0.0;
//	}


	public Double getPklKwota() {
		this.onRXGet("pklKwota");
		return this.pklKwota;
	}

	public void setPklKwota(Double pklKwota) {
		if (pklKwota != null) {
			pklKwota = Utils.round(pklKwota, 2);
		}

		this.onRXSet("pklKwota", this.pklKwota, pklKwota);
		boolean eq = eq(this.pklKwota, pklKwota);
		this.pklKwota = pklKwota;
		if (!eq) {
			this.setPklKwotaMf(pklKwota);
		}

	}

//	public void setPklKwota(Double pklKwota, boolean bezPrzeliczaniaDKZ) {
//		if (pklKwota != null) {
//			pklKwota = Utils.round(pklKwota, 2);
//		}
//
//		this.onRXSet("pklKwota", this.pklKwota, pklKwota);
//		boolean eq = eq(this.pklKwota, pklKwota);
//		this.pklKwota = pklKwota;
//		if (!eq && bezPrzeliczaniaDKZ) {
//			this.setPklKwotaMf(pklKwota, bezPrzeliczaniaDKZ);
//		}
//
//	}

	public String getPklKwotaGotowki() {
		this.onRXGet("pklKwotaGotowki");
		return this.pklKwotaGotowki;
	}

	public void setPklKwotaGotowki(String pklKwotaGotowki) {
		this.onRXSet("pklKwotaGotowki", this.pklKwotaGotowki, pklKwotaGotowki);
		this.pklKwotaGotowki = pklKwotaGotowki;
	}

	public Long getPklKwotaPrzelewu() {
		this.onRXGet("pklKwotaPrzelewu");
		return this.pklKwotaPrzelewu;
	}

	public void setPklKwotaPrzelewu(Long pklKwotaPrzelewu) {
		this.onRXSet("pklKwotaPrzelewu", this.pklKwotaPrzelewu, pklKwotaPrzelewu);
		this.pklKwotaPrzelewu = pklKwotaPrzelewu;
	}

	public String getPklOpis() {
		this.onRXGet("pklOpis");
		return this.pklOpis;
	}

	public void setPklOpis(String pklOpis) {
		this.onRXSet("pklOpis", this.pklOpis, pklOpis);
		this.pklOpis = pklOpis;
	}

	public Double getPklStawka() {
		this.onRXGet("pklStawka");
		return this.pklStawka;
	}

	//TODO test fest
	public void setPklStawka(Double pklStawka) {
		this.onRXSet("pklStawka", this.pklStawka, pklStawka);
		boolean eq = eq(this.pklStawka, pklStawka);
		this.pklStawka = pklStawka;
		if (!eq) {
			przeliczKwote();

			if(this.getDeltTrasy() != null) {
				if(!eq(this.getDeltTrasy().getTrsKosztBiletu(),pklStawka)) {
					this.getDeltTrasy().setTrsKosztBiletu(pklStawka);
				}
			}


			if (this.czyPrzekroczonoLimit()) {
				User.warn(getOpisPrzekroczonyLimitStawki());
			}


//			if(eq("H",this.getKategoria())) {
//				
//				if(eq(1L,this.getDeltKalkulacje().getDeltWnioskiDelegacji().getWndRodzaj())&& this.getPklStawka() > 600.0) {
//					User.warn("Limit dobowy przekroczony - czy masz zgodę na wyższy limit hotelowy?");
//				} else if(eq(2L,this.getDeltKalkulacje().getDeltWnioskiDelegacji().getWndRodzaj())&& this.getPklStawka() > this.getStawkaHotelowaWgPklKrId()) {
//					User.warn("Limit dobowy przekroczony - czy masz zgodę na wyższy limit hotelowy?");
//				}
//			} 
		}
	}

	public String getOpisPrzekroczonyLimitStawki() {
		return String.format("Pozycja: %1$s - przekroczono limit stawki: %2$s %3$s", this.getNazwaPozycji(), Utils.format(this.getStawkaWgLimitu()), this.getWalSymbol());
	}


	public Long getPklTpozId() {
		this.onRXGet("pklTpozId");
		return this.pklTpozId;
	}

	public void setPklTpozId(Long pklTpozId) {
		this.onRXSet("pklTpozId", this.pklTpozId, pklTpozId);
		boolean eq = eq(this.pklTpozId, pklTpozId);
		this.pklTpozId = pklTpozId;
		if (!eq && this.getDeltKalkulacje() != null) {
			this.getDeltKalkulacje().forCzyDomyslnieDietaHotelowa();
		}
	}

	public Long getPklWalId() {
		this.onRXGet("pklWalId");
		return this.pklWalId;
	}

	public void setPklWalId(Long pklWalId) {
		this.onRXSet("pklWalId", this.pklWalId, pklWalId);
		boolean eq = eq(this.pklWalId, pklWalId);
		this.pklWalId = pklWalId;
		if (!eq) {
			przeliczPodzialPKLDKZ();
		}
	}

	/**
	 * @return the deltKalkulacje
	 */
	public DeltKalkulacje getDeltKalkulacje() {
		this.onRXGet("deltKalkulacje");
		return deltKalkulacje;
	}

	/**
	 * @param deltKalkulacje the deltKalkulacje to set
	 */
	public void setDeltKalkulacje(DeltKalkulacje deltKalkulacje) {
		this.onRXSet("deltKalkulacje", this.deltKalkulacje, deltKalkulacje);
		this.deltKalkulacje = deltKalkulacje;
	}

	/**
	 * @return the pklKosztMFProcent
	 */
	public Double getPklKosztMFProcent() {
		this.onRXGet("pklKosztMFProcent");
		return pklKosztMFProcent;
	}

	/**
	 * @param pklKosztMFProcent the pklKosztMFProcent to set
	 */
	public void setPklKosztMFProcent(Double pklKosztMFProcent) {
		this.onRXSet("pklKosztMFProcent", this.pklKosztMFProcent, pklKosztMFProcent);
		this.pklKosztMFProcent = pklKosztMFProcent;
	}

	/**
	 * @return the pklKosztZaprProcent
	 */
	public Double getPklKosztZaprProcent() {
		this.onRXGet("pklKosztZaprProcent");
		return pklKosztZaprProcent;
	}

	/**
	 * @param pklKosztZaprProcent the pklKosztZaprProcent to set
	 */
	public void setPklKosztZaprProcent(Double pklKosztZaprProcent) {
		this.onRXSet("pklKosztZaprProcent", this.pklKosztZaprProcent, pklKosztZaprProcent);
		this.pklKosztZaprProcent = pklKosztZaprProcent;
	}

	/**
	 * @return the pklKosztZaprKwota
	 */
	public Double getPklKosztZaprKwota() {
		this.onRXGet("pklKosztZaprKwota");
		return pklKosztZaprKwota;
	}

	/**
	 * @param pklKosztZaprKwota the pklKosztZaprKwota to set
	 */
	public void setPklKosztZaprKwota(Double pklKosztZaprKwota) {
		this.onRXSet("pklKosztZaprKwota", this.pklKosztZaprKwota, pklKosztZaprKwota);
		this.pklKosztZaprKwota = pklKosztZaprKwota;
	}

	/**
	 * @return the pklRefundacjaProcent
	 */
	public Double getPklRefundacjaProcent() {
		this.onRXGet("pklRefundacjaProcent");
		return pklRefundacjaProcent;
	}

	/**
	 * @param pklRefundacjaProcent the pklRefundacjaProcent to set
	 */
	public void setPklRefundacjaProcent(Double pklRefundacjaProcent) {
		this.onRXSet("pklRefundacjaProcent", this.pklRefundacjaProcent, pklRefundacjaProcent);

		this.pklRefundacjaProcent = pklRefundacjaProcent;
	}

	/**
	 * @return the pklRefundacjaKwota
	 */
	public Double getPklRefundacjaKwota() {
		this.onRXGet("pklRefundacjaKwota");
		return pklRefundacjaKwota;
	}

	/**
	 * @param pklRefundacjaKwota the pklRefundacjaKwota to set
	 */
	public void setPklRefundacjaKwota(Double pklRefundacjaKwota) {
		this.onRXSet("pklRefundacjaKwota", this.pklRefundacjaKwota, pklRefundacjaKwota);
		this.pklRefundacjaKwota = pklRefundacjaKwota;
	}

	public Long getPklSniadania() {
		this.onRXGet("pklSniadania");
		return pklSniadania;
	}

	public void setPklSniadania(Long pklSniadania) {
		this.onRXSet("pklSniadania", this.pklSniadania, pklSniadania);
		boolean eq = eq(this.pklSniadania, pklSniadania);
		this.pklSniadania = pklSniadania;
		if (!eq) {
			przeliczKwote();
		}
	}

	public Long getPklObiady() {
		this.onRXGet("pklObiady");
		return pklObiady;
	}

	public void setPklObiady(Long pklObiady) {
		this.onRXSet("pklObiady", this.pklObiady, pklObiady);
		boolean eq = eq(this.pklObiady, pklObiady);
		this.pklObiady = pklObiady;
		if (!eq) {
			przeliczKwote();
		}
	}

	public Long getPklKolacje() {
		this.onRXGet("pklKolacje");
		return pklKolacje;
	}

	public void setPklKolacje(Long pklKolacje) {
		this.onRXSet("pklKolacje", this.pklKolacje, pklKolacje);
		boolean eq = eq(this.pklKolacje, pklKolacje);
		this.pklKolacje = pklKolacje;
		if (!eq) {
			przeliczKwote();
		}
	}


	public List<DeltZrodlaFinansowania> getDeltZrodlaFinansowanias() {
		this.onRXGet("deltZrodlaFinansowanias");
		return this.deltZrodlaFinansowanias;
	}

	public void setDeltZrodlaFinansowanias(List<DeltZrodlaFinansowania> deltZrodlaFinansowanias) {
		this.onRXSet("deltZrodlaFinansowanias", this.deltZrodlaFinansowanias, deltZrodlaFinansowanias);
		this.deltZrodlaFinansowanias = deltZrodlaFinansowanias;
	}

	public DeltZrodlaFinansowania addDeltZrodlaFinansowania(DeltZrodlaFinansowania deltZrodlaFinansowania) {
		this.onRXSet("deltZrodlaFinansowanias", null, deltZrodlaFinansowanias);
		getDeltZrodlaFinansowanias().add(deltZrodlaFinansowania);
		deltZrodlaFinansowania.setDeltPozycjeKalkulacji(this);

		return deltZrodlaFinansowania;
	}

	public DeltZrodlaFinansowania removeDeltZrodlaFinansowania(DeltZrodlaFinansowania deltZrodlaFinansowania) {
		this.onRXSet("deltZrodlaFinansowanias", null, deltZrodlaFinansowanias);
		getDeltZrodlaFinansowanias().remove(deltZrodlaFinansowania);
		deltZrodlaFinansowania.setDeltPozycjeKalkulacji(null);

		return deltZrodlaFinansowania;
	}

	public Double getPklKwotaPln() {
		this.onRXGet("pklKwotaPln");
		return pklKwotaPln;
	}


	public void setPklKwotaPln(Double pklKwotaPln) {
		this.onRXSet("pklKwotaPln", this.pklKwotaPln, pklKwotaPln);
		this.pklKwotaPln = pklKwotaPln;
	}

	public Double getPklKurs() {
		this.onRXGet("pklKurs");
		return pklKurs;
	}

	public void setPklKurs(Double pklKurs) {
		this.onRXSet("pklKurs", this.pklKurs, pklKurs);
		this.pklKurs = pklKurs;
	}

	public Long getPklRok() {
		this.onRXGet("pklRok");
		return pklRok;
	}

	public void setPklRok(Long pklRok) {
		this.onRXSet("pklRok", this.pklRok, pklRok);
		this.pklRok = pklRok;
	}

	public String getPklRefundacjaOpis() {
		this.onRXGet("pklRefundacjaOpis");
		return pklRefundacjaOpis;
	}

	public void setPklRefundacjaOpis(String pklRefundacjaOpis) {
		this.onRXSet("pklRefundacjaOpis", this.pklRefundacjaOpis, pklRefundacjaOpis);
		this.pklRefundacjaOpis = pklRefundacjaOpis;
	}


	@Override
	public String toString() {
		return String.format("id: %1$s; PklTpozId: %2$s; pklWalId: %3$s ", this.pklId, this.pklTpozId, this.pklWalId);
	}

	public Double getPklKwotaMf() {
		this.onRXGet("pklKwotaMf");
		return pklKwotaMf;
	}

	public void setPklKwotaMf(Double pklKwotaMf) {
		if (pklKwotaMf != null) {
			pklKwotaMf = Utils.round(pklKwotaMf, 2);
		}

		this.onRXSet("pklKwotaMf", this.pklKwotaMf, pklKwotaMf);
		boolean eq = eq(this.pklKwotaMf, pklKwotaMf);
		this.pklKwotaMf = pklKwotaMf;
		if (!eq) {
			przeliczZF();
			przeliczPodzialPKLDKZ(); //zle
		}

		if (this.czyPrzekroczonoLimit()) {
			User.warn("%1$s: %2$s %3$s \nKwota pozycji przekroczyła wartość limitu", this.getDelTypyPozycji().getTpozNazwa(), format(this.getPklKwotaMf(),"#0.00"), this.getWalSymbol());
		}
	}

//	public void setPklKwotaMf(Double pklKwotaMf, boolean bezPrzeliczaniaDKZ) {
//		if (pklKwotaMf != null) {
//			pklKwotaMf = Utils.round(pklKwotaMf, 2);
//		}
//
//		this.onRXSet("pklKwotaMf", this.pklKwotaMf, pklKwotaMf);
//		boolean eq = eq(this.pklKwotaMf, pklKwotaMf);
//		this.pklKwotaMf = pklKwotaMf;
//
//		if (!eq && !bezPrzeliczaniaDKZ) {
//			przeliczZF();
//			przeliczPodzialPKLDKZ(); //zle
//		}
//	}

	private void przeliczPodzialPKLDKZ() {
		if (this.getDeltKalkulacje() != null) {
			this.getDeltKalkulacje().przeliczPodzialZFvsDKZ();
		}

//		if (this.getDeltKalkulacje()!=null) {
//			this.getDeltKalkulacje().clearDelZaliczkiReszta(this.pklWalId);
//		}
//
//		List<DeltZrodlaFinansowania> zfl = this.getDeltZrodlaFinansowanias();
//		for (DeltZrodlaFinansowania zf : zfl) {
//			if (zf.getDelZaliczki()!=null){
//				zf.getDelZaliczki().removeDeltZrodlaFinansowania(zf);
//			}
//		}
//
//		if (this.getDeltKalkulacje()!=null) {
//			this.getDeltKalkulacje().przypiszZFDoWszystkichNieprzypisanychDKZzNZ();
//			this.getDeltKalkulacje().obliczDelZaliczkiReszta(this.getWalSymbol());
//		}
	}

	private void przeliczZF() { //po zmianie wartosci pkl ustalamy nowe kwoty zf

		grupujZF();//jesli sa niepotrzbne podzialy to grupuj

		//dostosowac ZF tak aby ich suma byla identyczna z this.pklKwotaMf
		double kwota = nz(this.pklKwotaMf);

		DeltZrodlaFinansowania ostatniZf = null;
		for (DeltZrodlaFinansowania zf : this.getDeltZrodlaFinansowanias()) {
			double procent = nz(zf.getPkzfProcent());
			double kwotaZF = divide(multiply(this.pklKwotaMf, procent), 100.0, 2);//round((nz(this.pklKwotaMf) * procent) / 100, 2);
			zf.setPkzfKwota(kwotaZF);
			kwota -= kwotaZF;
			ostatniZf = zf;
		}

		if (ostatniZf != null && kwota != 0.0) {//dodaj ew. ogonek do ostatniego el. zf
			ostatniZf.setPkzfKwota(nz(ostatniZf.getPkzfKwota()) + kwota);
		}

	}


	public void grupujZF() {
		// zgrupowac niepotrzebnie podzielone ZF z identycznymi wnioskami; celem jest likwidacja zbednych podziałów
		HashMap<String, DeltZrodlaFinansowania> grZfHM = new HashMap<>();
		List<DeltZrodlaFinansowania> zfl = this.getDeltZrodlaFinansowanias().stream().collect(Collectors.toList());
		for (DeltZrodlaFinansowania zf : zfl) {
			String key = zf.groupZfAngKey();
			DeltZrodlaFinansowania zfGr = grZfHM.get(key);
			if (zfGr == null) {
				zfGr = zf;
				grZfHM.put(key, zfGr);
			} else {
				zfGr.setPkzfKwota(nz(zfGr.getPkzfKwota()) + nz(zf.getPkzfKwota()));
				this.removeDeltZrodlaFinansowania(zf);
			}
		}

	}


	public String getPklFCzyUmowaZWyk() {
		this.onRXGet("pklFCzyUmowaZWyk");
		return pklFCzyUmowaZWyk;
	}

	public void setPklFCzyUmowaZWyk(String pklFCzyUmowaZWyk) {
		this.onRXSet("pklFCzyUmowaZWyk", this.pklFCzyUmowaZWyk, pklFCzyUmowaZWyk);
		this.pklFCzyUmowaZWyk = pklFCzyUmowaZWyk;
	}

	public String getpklFCzyBezplatna() {
		this.onRXGet("pklFCzyBezplatna");
		return pklFCzyBezplatna;
	}

	public void setpklFCzyBezplatna(String pklFCzyBezplatna) {
		this.onRXSet("pklFCzyBezplatna", this.pklFCzyBezplatna, pklFCzyBezplatna);
		this.pklFCzyBezplatna = pklFCzyBezplatna;
	}

	public String getPklFormaPlatnosci() {
		this.onRXGet("pklFormaPlatnosci");
		return pklFormaPlatnosci;
	}

	public void setPklFormaPlatnosci(String pklFormaPlatnosci) {
		this.onRXSet("pklFormaPlatnosci", this.pklFormaPlatnosci, pklFormaPlatnosci);
		boolean eq = eq(this.pklFormaPlatnosci, pklFormaPlatnosci);
		this.pklFormaPlatnosci = pklFormaPlatnosci;
		if (!eq) {
			przeliczPodzialPKLDKZ();
		}
	}

	public DeltTrasy getDeltTrasy() {
		this.onRXGet("deltTrasy");
		return deltTrasy;
	}

	public void setDeltTrasy(DeltTrasy deltTrasy) {
		this.onRXSet("deltTrasy", this.deltTrasy, deltTrasy);
		this.deltTrasy = deltTrasy;
	}

	public Date getPklTerminWykupu() {
		this.onRXGet("pklTerminWykupu");
		return pklTerminWykupu;
	}

	public void setPklTerminWykupu(Date pklTerminWykupu) {
		this.onRXSet("pklTerminWykupu", this.pklTerminWykupu, pklTerminWykupu);
		this.pklTerminWykupu = pklTerminWykupu;
	}


	public double getKwotaWalPozaZaliczkami() {
		double ret = nz(this.getPklKwotaMf()) - getKwotaWalWZaliczkach();
		return ret;
	}

	public double getKwotaWalWZaliczkach() {
		//double ret =  this.getDeltPklWyplNzs().stream().mapToDouble(pnz -> pnz.getDpnzKwotaWal()).sum();//suma z podzalow pkl na zaliczki
		double ret = this.getDeltZrodlaFinansowanias().stream()
				.filter(z -> z.getDelZaliczki() != null)
				.mapToDouble(z -> nz(z.getPkzfKwota()))
				.sum();

		return ret;
	}


	//bi-directional many-to-one association to DeltPklWyplNz
	@OneToMany(mappedBy = "deltPozycjeKalkulacji", cascade = {CascadeType.ALL}, orphanRemoval = true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	private List<DeltPklWyplNz> deltPklWyplNzs = new ArrayList<>();

	public List<DeltPklWyplNz> getDeltPklWyplNzs() {
		this.onRXGet("deltPklWyplNzs");
		return this.deltPklWyplNzs;
	}

	public void setDeltPklWyplNzs(List<DeltPklWyplNz> deltPklWyplNzs) {
		this.onRXSet("deltPklWyplNzs", this.deltPklWyplNzs, deltPklWyplNzs);
		this.deltPklWyplNzs = deltPklWyplNzs;
	}

	public DeltPklWyplNz addDeltPklWyplNz(DeltPklWyplNz deltPklWyplNz) {
		this.onRXSet("deltPklWyplNzs", null, deltPklWyplNzs);
		getDeltPklWyplNzs().add(deltPklWyplNz);
		deltPklWyplNz.setDeltPozycjeKalkulacji(this);

		return deltPklWyplNz;
	}

	public DeltPklWyplNz removeDeltPklWyplNz(DeltPklWyplNz deltPklWyplNz) {
		this.onRXSet("deltPklWyplNzs", null, deltPklWyplNzs);
		getDeltPklWyplNzs().remove(deltPklWyplNz);
		deltPklWyplNz.setDeltPozycjeKalkulacji(null);

		return deltPklWyplNz;
	}

	public Long getPklPklId() {
		this.onRXGet("pklPklId");
		return pklPklId;
	}

	public void setPklPklId(Long pklPklId) {
		this.onRXSet("pklPklId", this.pklPklId, pklPklId);
		this.pklPklId = pklPklId;
	}

	public boolean blokujPklPoWyplacieNZ() {
//		boolean ret = false;
//		if(this.getDeltPklWyplNzs() != null) {
//			List<DeltPklWyplNz> listaDpnz = this.getDeltPklWyplNzs()
//					.stream()
////					.filter(dpnz -> dpnz.getDpnzDokId() != null)
//					.collect(Collectors.toList());
//
//			if(listaDpnz != null)
//				ret = listaDpnz.size() > 0;
//		}

		//long count = this.getDeltZrodlaFinansowanias().stream().filter(zf -> zf.getDelZaliczki() != null).count();

//		long count = this.getDeltZrodlaFinansowanias().stream().filter(zf -> zf.getDelZaliczki() != null && zf.getDelZaliczki().getDkzDokId()!=null).count();

//		return (count>0);
		return false;
	}


	public double getDeltZrodlaFinansowaniasKwotaSuma() {
		double ret = this.getDeltZrodlaFinansowanias().stream().mapToDouble(zf -> nz(zf.getPkzfKwota())).sum();
		return round(ret, 2);
	}

	public double getDeltZrodlaFinansowaniasProcentSuma() {
		double ret = this.getDeltZrodlaFinansowanias().stream().mapToDouble(zf -> nz(zf.getPkzfProcent())).sum();
		return round(ret, 2);
	}


	public void dodajZF(DataRow r) {
		if (r == null) return;

		DeltZrodlaFinansowania zf = new DeltZrodlaFinansowania();
		zf.setPkzfSkId(r.getIdAsLong());
		double sumZfkwota = this.getDeltZrodlaFinansowaniasKwotaSuma(); //double sumZfProcent = this.getDeltZrodlaFinansowaniasProcentSuma();
		DelAngaze ang = new DelAngaze();
		ang.setAngKwota(0d);
		ang.setAngProcent(100d);
		zf.addDelAngaze(ang);
		this.addDeltZrodlaFinansowania(zf);
		zf.setPkzfKwota(this.pklKwota - sumZfkwota);
	}


	public boolean isValidZFPkl() {
		if (this.getDeltKalkulacje()!=null && eq("WNK", this.getDeltKalkulacje().getKalRodzaj()) ){
			return true;//??? czy i ew. jak walifujemy PKL/Zf na kalk. WNK  ???
		}

		if (this.getDeltZrodlaFinansowanias().isEmpty()) {
			User.warn(this.getNazwaPozycji() + " / brak ZF dla pozycji kosztowej");
			return false; //brak ZF
		}

		if (!eq(this.getDeltZrodlaFinansowaniasKwotaSuma(), nz(this.getPklKwotaMf()))) {
			User.warn(this.getNazwaPozycji() + " / nieprawidłowa suma ZF względem wartości pozycji kosztowej");
			return false;//nie zgadza się suma ZF z wartością PKL
		}

		return this.getDeltZrodlaFinansowanias().stream()
				.filter(z -> !z.isValidZF())
				.collect(Collectors.toList()).isEmpty();//co najmniej jedno ZF jest nieprawidlowe (brak skid lub suma angazy inna niz wart. ZF)

	}


	public String getWalSymbol() {
		return this.getWalSymbol(this.pklWalId);
	}

	public String getKrajSymbol() {
		return this.getKrajSymbol(this.getPklKrId());
	}

	public String getKrajNazwa() {
		return this.getKrajNazwa(this.getPklKrId());
	}


	public double ilePosilkow() {
		double ret = 0.0;
		double roznica = round(this.getPklIlosc(), 0) - this.getPklIlosc();
		if (roznica == 0 || roznica == 0.5)
			ret = round(this.getPklIlosc(), 0);
		else
			ret = round(this.getPklIlosc(), 0) + 1;

		return ret;
	}


//	public List<DelZaliczki> getDpnzZaliczki(){ //lista DKZ zwiazanych przez DPNZ
//		List<DelZaliczki> ret = this.getDeltPklWyplNzs()
//				.stream()
//				.filter(dpnz -> dpnz.getDelZaliczki()!=null)
//				.map(dpnz -> dpnz.getDelZaliczki())
//				.distinct()
//				.collect(Collectors.toList());
//		return ret;
//	}
//
//    public List<DelZaliczki> getPotencjalneDpnzZaliczki(){//lista DKZ mozliwych do powiazania przez DPNZ
//        List<DelZaliczki> ret = this.getDeltKalkulacje().getDelZaliczki()
//                .stream()
//                .filter(z -> eq(z.getDkzWalId(), this.getPklWalId()) && z.getPozaKwotaWalDpnzPkl() != 0.0)
//                .collect(Collectors.toList());
//        return ret;
//    }


	public double getSumaKwotaWalDpnzDkz() {//jaka czesc kwoty w walucie (kwotaMF) z PKL zostala przypisana do zaliczek
		return this.getDeltPklWyplNzs().stream().mapToDouble(dpnz -> nz(dpnz.getDpnzKwotaWal())).sum();
	}

	public double getPozaKwotaWalDpnzDkz() { //jaka czesc kwoty w walucie (kwotaMF) z PKL nie zostala przypisana do zaliczek (docelowo tu ma byc 0.0, albo czerwonym po oczach...)
		return nz(this.getPklKwotaMf()) - this.getSumaKwotaWalDpnzDkz();
	}


	public String getSortKey() {
		int x = "P H RH RD KM T I".indexOf(this.getKategoria());
		if (x < 0) x = 999;
		return String.format(Utils.lpad(x, 10) + Utils.lpad(this.pklId, 10));
	}


	public DelTypyPozycji getDelTypyPozycji() {
		return this.getDelTypyPozycjiByTpozId(this.pklTpozId);
	}


	public boolean isRozrachunekDelegowanego() {
		String pklFormaPlatnosci = this.getPklFormaPlatnosci();
		//umowa z wykonawcą
		return !eq("02", pklFormaPlatnosci)//karta służobowa
				&& !eq("03", pklFormaPlatnosci);
	}

	public String getPklFDopelnienie() {
		this.onRXGet("pklFDopelnienie");
		return pklFDopelnienie;
	}

	public void setPklFDopelnienie(String pklFDopelnienie) {
		this.onRXSet("pklPklId", this.pklFDopelnienie, pklFDopelnienie);
		this.pklFDopelnienie = pklFDopelnienie;
	}


	//zwraca stawkę diety hotelowej dla danego krId danego pkl
	public double getStawkaHotelowaWgPklKrId() {
		double ret = 0;
		if (this.getDeltKalkulacje() != null) {
			//TODO upewnij się jaka data ma być datą od dla ważności diet
			DeltWnioskiDelegacji wniosek = this.deltKalkulacje.getDeltWnioskiDelegacji();
			SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_KRAJE_STAWKI", wniosek.getWndStdId(), "H", wniosek.getWndDataWyjazdu());

			if (this.getPklKrId() != null) {

				if (eq(this.getPklKrId(), 1L))
					return 600.0;

				DataRow r = lw.find(this.getPklKrId());
				String kwota = "" + r.get("WST_KWOTA");

//		Date obowiazujeOd = (Date) r.get("WST_OBOWIAZUJE_OD");
//			 && pobytOd.after(obowiazujeOd)
				ret = Double.valueOf(kwota);
			}
		}
		return ret;
	}

	public String getTitleDlaPklHotelowej() {
		String ret = "";
		if (this.getPklStawka() != null && this.getPklWalId() != null) {
			if (this.getPklStawka() > this.getStawkaHotelowaWgPklKrId()) {
				ret = "Limit stawki dla diety hotelowej wynosi: " + this.getStawkaHotelowaWgPklKrId() + " " + this.getWalSymbol(this.getPklWalId());
			}

		}

		return ret;
	}

	//TODO tutaj zaczytać wg tego co niżej!!!
	public String getTitleDlaLimitu() {
		String ret = "";
		if (this.getPklStawka() != null && this.getPklWalId() != null) {
			if (this.getPklStawka() > this.getStawkaHotelowaWgPklKrId()) {
				ret = "Limit stawki dla pozycji kalkulacji wynosi: " + this.getStawkaHotelowaWgPklKrId() + " " + this.getWalSymbol(this.getPklWalId());
			}

		}

		return ret;
	}

	public double getStawkaWgLimitu() {
		double ret = 0;
		if (this.getDeltKalkulacje() != null && this.getKategoria() != null) {
			//TODO upewnij się jaka data ma być datą od dla ważności diet
			DeltWnioskiDelegacji wniosek = this.deltKalkulacje.getDeltWnioskiDelegacji();
			
			Date dataOdKiedyDieta = wniosek.getWndDataWyjazdu();
			if(dataOdKiedyDieta == null) {
				if(wniosek.getWndDataWniosku() != null) {
					dataOdKiedyDieta = wniosek.getWndDataWniosku();
				} else {
					User.alert("Nie uzupełniono wszystkich wymaganych danych, nie można ustalić daty od kiedy obowiązuje stawka danego rodzaju kosztu");
				}
			}

			SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_KRAJE_STAWKI", wniosek.getWndStdId(), this.getKategoria(), dataOdKiedyDieta);

			if (eq(this.getKategoria(), "KM") || eq(this.getKategoria(), "RD")) {
				lw = this.getLW("PPL_DEL_KRAJE_STAWKI", wniosek.getWndStdId(), "P", dataOdKiedyDieta);
			}

			if (eq(this.getKategoria(), "RH")) {
				lw = this.getLW("PPL_DEL_KRAJE_STAWKI", wniosek.getWndStdId(), "H", dataOdKiedyDieta);
			}

			if (this.getPklKrId() != null) {

				DataRow r = lw.find(this.getPklKrId());
				String kwota = "" + r.get("WST_KWOTA");

				//TODO zaokrąglenie ???

				ret = Double.valueOf(kwota);

				//TODO warunek dla krajówki i ryczałtów hotelowych
				if (wniosek.isZagraniczna()) {
					if (eq(this.getKategoria(), "RH")) {
						ret = ret * 0.25;
					} else if (eq(this.getKategoria(), "KM")) {
						ret = ret * 0.1;
					}
				} else {
					//TODO warunki dla krajówki --- możliwe że jednak trzeba to gdzieś brać i mieć zapisane w systemie.

					if (eq(this.getKategoria(), "KM")) {
						ret = 6.0;
					}

					if (eq(this.getKategoria(), "RH")) {
						ret = 45.0;
					}

					if (eq(this.getKategoria(), "H")) {
						ret = 600.0;
					}
				}
			}
		}
		return Utils.round(ret,2);
	}

	public boolean czyPrzekroczonoLimit() {
		boolean ret = false;

		if (!this.czyLimit())
			return false;

		if (this.getPklIlosc() != null || this.getPklStawka() != null) {

			long pklIlosc = ((Double) (nz(this.getPklIlosc()) * 10000)).longValue();
			long pklStawka = ((Double) (nz(this.getStawkaWgLimitu()) * 10000)).longValue();

			if (pklIlosc == 0 || pklStawka == 0) {
				this.setPklKwota(0.0);
				return false;
			}

			Double kwotaLacznie = this.getPklKwotaMf();

			long lret = (pklIlosc * pklStawka);
			Double kwotaLacznieWgLimitu = (Utils.round((1.0d * lret) / (10000 * 10000), 2));

			if (kwotaLacznie > kwotaLacznieWgLimitu)
				ret = true;

		}

		return ret;

	}


	public boolean czyLimit() {
		boolean ret = true;

		if (this.getPklTpozId() == null)
			return false;

		if (eq(this.getKategoria(), "I") || eq(this.getKategoria(), "T") || eq(this.getKategoria(), "D"))
			ret = false;

		return ret;
	}


	public String getNazwaPozycji() {
		if (this.getPklTpozId() == null)
			return "";

		String ret = "";

		SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_TYPY_POZYCJI");
		DataRow r = lw.find(this.getPklTpozId());
		if (r != null) {
			Object nazwa = r.get("tpoz_nazwa");

			if (nazwa != null)
				ret = nazwa + "";
		}

		return ret;
	}


	@Override
	public DeltWnioskiDelegacji getWniosek() {
		if (this.getDeltKalkulacje() == null) return null;
		return this.getDeltKalkulacje().getDeltWnioskiDelegacji();
	}


	public boolean validate(){
		boolean ret = true
				& this.isValidZFPkl();
		//TODO walidacja wartosci/poprawnosci samego PKL
		return ret;
	}




	public void ustawDomyslneZF(){
		final DeltWnioskiDelegacji wnd = this.getWniosek();
		if (wnd ==null || wnd.getKalkulacjaWniosek()==null || this.getDelTypyPozycji()==null) return;
		final DeltKalkulacje kalWNK = wnd.getKalkulacjaWniosek();

		final DeltPozycjeKalkulacji pklCzIzZf = kalWNK.getDeltPozycjeKalkulacjis().stream()
				.filter(p -> !p.getDeltZrodlaFinansowanias().isEmpty()
						&& p.getDelTypyPozycji()!=null
						&& this.getDelTypyPozycji()!=null
						&& eq(p.getDelTypyPozycji().getTpozId(), this.getDelTypyPozycji().getTpozId()))
				.findFirst()
				.orElse(null);

		clearDeltZrodlaFinansowanias();

		if (pklCzIzZf!=null) {
			pklCzIzZf.getDeltZrodlaFinansowanias().stream()
					.forEach(zf->{
				this.addDeltZrodlaFinansowania(zf.clone(true));
			});
		} else {

			if("T".equals(wnd.getWndFDomyslneZf()) && wnd.getWndSkId() != null && wnd.getWndWnrId() != null){
				DeltZrodlaFinansowania zf = new DeltZrodlaFinansowania();
				DelAngaze an = new DelAngaze();
				zf.setPkzfProcent(100.0);
				zf.setPkzfKwota(0.0);
				zf.setPkzfSkId(wnd.getWndSkId());
				an.setAngWnrId(wnd.getWndWnrId());
				an.setAngProcent(100.0);
				an.setAngKwota(0.0);
				zf.addDelAngaze(an);
				this.addDeltZrodlaFinansowania(zf);
			}
		}
	}

	public void clearDeltZrodlaFinansowanias() {
		final List<DeltZrodlaFinansowania> lst = this.getDeltZrodlaFinansowanias().stream().collect(Collectors.toList());
		lst.stream().forEach(zf->this.removeDeltZrodlaFinansowania(zf));
	}


	public void ustawDomylsnaRefundacja(){
		final DeltWnioskiDelegacji wnd = this.getWniosek();
		if (wnd ==null || wnd.getKalkulacjaWniosek()==null || this.getDelTypyPozycji()==null) return;
		final DeltKalkulacje kalWNK = wnd.getKalkulacjaWniosek();

		kalWNK.getDeltPozycjeKalkulacjis().stream()
				.filter(p -> p.getDelTypyPozycji()!=null
						&& this.getDelTypyPozycji()!=null
						&& eq(p.getDelTypyPozycji().getTpozId(), this.getDelTypyPozycji().getTpozId()))
				.forEach(p -> {
					this.setPklRefundacjaProcent(p.getPklRefundacjaProcent());
					this.setPklRefundacjaKwota(nz(this.pklRefundacjaProcent) / 100.0 * nz(this.pklKwota)); //TODO ustalic czy nie lepiej w setPklRefundacjaProcent
				});
	}


	public boolean isDopelnienieNominaluNBP(){
		if (this.getDelTypyPozycji()==null)
			return false;

		final Long typPozycjiDopelnienie = this.getTpozIdByKtgPozycji("D");
		return eq(typPozycjiDopelnienie, this.getDelTypyPozycji().getTpozId());
	}


	public boolean isGotowka(){
		return "01".equals(this.getPklFormaPlatnosci());
	}
	public boolean isPrzelew(){
		return "04".equals(this.getPklFormaPlatnosci());
	}


	public boolean czyKopiowacNaRozliczenie(){
		return !isDopelnienieNominaluNBP() && (isGotowka() || isPrzelew()) ;
	}
}