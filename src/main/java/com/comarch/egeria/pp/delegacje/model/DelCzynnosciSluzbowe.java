package com.comarch.egeria.pp.delegacje.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="PPT_DEL_CZYNNOSCI_SLUZBOWE", schema="PPADM")
@NamedQuery(name="DelCzynnosciSluzbowe.findAll", query="SELECT d FROM DelCzynnosciSluzbowe d")
public class DelCzynnosciSluzbowe  implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_CZYNNOSCI_SLUZBOWE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_CZYNNOSCI_SLUZBOWE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_CZYNNOSCI_SLUZBOWE")	
	@Column(name="CZYN_ID")
	private long czynId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CZYN_CZYNNOSCI_OD")
	private Date czynCzynnosciOd;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CZYN_CZYNNOSCI_DO")
	private Date czynCzynnosciDo;
	
	@Column(name="CZYN_OPIS")
	private String czynOpis;
	
	@Column(name="CZYN_CZAS")
	private Double czynCzas;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CZYN_AUDYT_DM")
	private Date czynAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="CZYN_AUDYT_DT", updatable=false)
	private Date czynAudytDt;

	@Column(name="CZYN_AUDYT_KM")
	private String czynAudytKm;

	@Column(name="CZYN_AUDYT_LM")
	private String czynAudytLm;

	@Column(name="CZYN_AUDYT_UM")
	private String czynAudytUm;

	@Column(name="CZYN_AUDYT_UT", updatable=false)
	private String czynAudytUt;
	
	//bi-directional many-to-one association to PptDelWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="CZYN_KAL_ID")
	private DeltKalkulacje deltKalkulacje;
	
	
	@Column(name="CZYN_KRAJ_ID")
	private Long czynKrajId;
	
	@Column(name="CZYN_CZAS_PRYWATNY")
	private Double czynCzasPrywatny;
	
	
	public DelCzynnosciSluzbowe clone(){
		
		DelCzynnosciSluzbowe cz =  new DelCzynnosciSluzbowe();
		cz.czynId = 0L;
		cz.czynCzynnosciOd = czynCzynnosciOd;
		cz.czynCzynnosciDo = czynCzynnosciDo;
		cz.czynCzas = czynCzas;
		cz.czynOpis = czynOpis;
		cz.czynKrajId = czynKrajId;
		cz.deltKalkulacje = null;
		
		return cz;
	}
	

	public long getCzynId() {
		return czynId;
	}

	public void setCzynId(long czynId) {
		this.czynId = czynId;
	}

	public Date getCzynCzynnosciOd() {
		return czynCzynnosciOd;
	}

	public void setCzynCzynnosciOd(Date czynCzynnosciOd) {
		this.czynCzynnosciOd = czynCzynnosciOd;
		
		if(this.czynCzynnosciOd != null && czynCzynnosciDo != null)
			this.setCzynCzas((0.000 + czynCzynnosciDo.getTime() - czynCzynnosciOd.getTime()) / (1000 * 60 * 60));
	}

	public Date getCzynCzynnosciDo() {
		return czynCzynnosciDo;
	}

	public void setCzynCzynnosciDo(Date czynCzynnosciDo) {
		this.czynCzynnosciDo = czynCzynnosciDo;
		
		if(this.czynCzynnosciOd != null && czynCzynnosciDo != null) 
			this.setCzynCzas((0.000 + this.czynCzynnosciDo.getTime() - czynCzynnosciOd.getTime()) / (1000 * 60 * 60));
	}

	public String getCzynOpis() {
		return czynOpis;
	}

	public void setCzynOpis(String czynOpis) {
		this.czynOpis = czynOpis;
	}

	public Double getCzynCzas() {
		return czynCzas;
	}

	public void setCzynCzas(Double czynCzas) {
		this.czynCzas = czynCzas;
	}

	public Date getCzynAudytDm() {
		return czynAudytDm;
	}

	public void setCzynAudytDm(Date czynAudytDm) {
		this.czynAudytDm = czynAudytDm;
	}

	public Date getCzynAudytDt() {
		return czynAudytDt;
	}

	public void setCzynAudytDt(Date czynAudytDt) {
		this.czynAudytDt = czynAudytDt;
	}

	public String getCzynAudytKm() {
		return czynAudytKm;
	}

	public void setCzynAudytKm(String czynAudytKm) {
		this.czynAudytKm = czynAudytKm;
	}

	public String getCzynAudytLm() {
		return czynAudytLm;
	}

	public void setCzynAudytLm(String czynAudytLm) {
		this.czynAudytLm = czynAudytLm;
	}

	public String getCzynAudytUm() {
		return czynAudytUm;
	}

	public void setCzynAudytUm(String czynAudytUm) {
		this.czynAudytUm = czynAudytUm;
	}

	public String getCzynAudytUt() {
		return czynAudytUt;
	}

	public void setCzynAudytUt(String czynAudytUt) {
		this.czynAudytUt = czynAudytUt;
	}

	public DeltKalkulacje getDeltKalkulacje() {
		return deltKalkulacje;
	}

	public void setDeltKalkulacje(DeltKalkulacje deltKalkulacje) {
		this.deltKalkulacje = deltKalkulacje;
	}

	public Long getCzynKrajId() {
		return czynKrajId;
	}

	public void setCzynKrajId(Long czynKrajId) {
		this.czynKrajId = czynKrajId;
	}

	public Double getCzynCzasPrywatny() {
		return czynCzasPrywatny;
	}

	public void setCzynCzasPrywatny(Double czynCzasPrywatny) {
		this.czynCzasPrywatny = czynCzasPrywatny;
	}

}
