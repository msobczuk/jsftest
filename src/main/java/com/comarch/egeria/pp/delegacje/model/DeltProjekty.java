package com.comarch.egeria.pp.delegacje.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the PPT_DEL_PROJEKTY database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_PROJEKTY", schema="PPADM")
@NamedQuery(name="DeltProjekty.findAll", query="SELECT d FROM DeltProjekty d")
public class DeltProjekty implements Serializable {
	private static final long serialVersionUID = 1L;

// jesli pojawi sie:	Duplicate generator named XXXXXXXXXXXXXX defined in this persistence unit
//	https://stackoverflow.com/questions/20941191/eclipse-duplicate-generator-named-id-generator-defined-in-this-persistence-uni
	
//	Select Window » Preferences
//	Expand Java Persistence » JPA » Errors/Warnings
//	Click Queries and generators
//	Set Duplicate generator defined to: Ignore
//	Click OK to apply changes and close the dialog
	
	
	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_PROJEKTY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_PROJEKTY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_PROJEKTY")
	@Column(name="PROJ_ID")
	private long projId;
	
	@Column(name="PROJ_OB_ID")
	private Long projObId;

	@Column(name="PROJ_OPIS")
	private String projOpis;
	
	

	//bi-directional many-to-one association to DeltWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="PROJ_WND_ID")
	private DeltWnioskiDelegacji deltWnioskiDelegacji;


	public DeltProjekty() {
	}

	public long getProjId() {
		return this.projId;
	}

	public void setProjId(long projId) {
		this.projId = projId;
	}

	public String getProjOpis() {
		return this.projOpis;
	}

	public void setProjOpis(String projOpis) {
		this.projOpis = projOpis;
	}

	/**
	 * @return the deltWnioskiDelegacji
	 */
	public DeltWnioskiDelegacji getDeltWnioskiDelegacji() {
		return deltWnioskiDelegacji;
	}

	/**
	 * @param deltWnioskiDelegacji the deltWnioskiDelegacji to set
	 */
	public void setDeltWnioskiDelegacji(DeltWnioskiDelegacji deltWnioskiDelegacji) {
		this.deltWnioskiDelegacji = deltWnioskiDelegacji;
	}

	public Long getProjObId() {
		return projObId;
	}

	public void setProjObId(Long projObId) {
		this.projObId = projObId;
	}


}