package com.comarch.egeria.pp.delegacje.model;

import static com.comarch.egeria.Utils.divide;
import static com.comarch.egeria.Utils.lpad;
import static com.comarch.egeria.Utils.multiply;
import static com.comarch.egeria.Utils.nz;
import static com.comarch.egeria.Utils.round;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.KursWaluty;
import com.comarch.egeria.pp.data.KursWalutySprzedazyNBP;
import com.comarch.egeria.pp.data.model.ModelBase;


/**
 * The persistent class for the PPT_DEL_ZRODLA_FINANSOWANIA database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_ZRODLA_FINANSOWANIA", schema="PPADM")
@NamedQuery(name="DeltZrodlaFinansowania.findAll", query="SELECT d FROM DeltZrodlaFinansowania d")
public class DeltZrodlaFinansowania extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Temporal(TemporalType.DATE)
	@Column(name="PKZF_AUDYT_DM")
	private Date pkzfAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="PKZF_AUDYT_DT", updatable=false)
	private Date pkzfAudytDt;

	@Column(name="PKZF_AUDYT_KM")
	private String pkzfAudytKm;

	@Column(name="PKZF_AUDYT_LM")
	private String pkzfAudytLm;

	@Column(name="PKZF_AUDYT_UM")
	private String pkzfAudytUm;

	@Column(name="PKZF_AUDYT_UT", updatable=false)
	private String pkzfAudytUt;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_ZRODLA_FINANSOWANIA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_ZRODLA_FINANSOWANIA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_ZRODLA_FINANSOWANIA")	
	@Column(name="PKZF_ID")
	private long pkzfId;

	@Column(name="PKZF_KWOTA")
	private Double pkzfKwota;

	@Column(name="PKZF_OPIS")
	private String pkzfOpis;

	@Column(name="PKZF_PROCENT")
	private Double pkzfProcent;

	@Column(name="PKZF_SK_ID")
	private Long pkzfSkId;
	
	@Column(name="PKZF_KURS")
	private Double pkzfKurs;

	//bi-directional many-to-one association to DeltPozycjeKalkulacji
	@ManyToOne
	@JoinColumn(name="PKZF_PKL_ID")
	private DeltPozycjeKalkulacji deltPozycjeKalkulacji;




	@ManyToOne
	@JoinColumn(name="PKZF_DKZ_ID")
	private DelZaliczki delZaliczki;

	public DelZaliczki getDelZaliczki() {
		this.onRXGet("delZaliczki");
		return delZaliczki;
	}

	public void setDelZaliczki(DelZaliczki delZaliczki) {
		this.onRXSet("delZaliczki", this.delZaliczki, delZaliczki);
		this.delZaliczki = delZaliczki;
	}



	@OneToMany(mappedBy="deltZrodlaFinansowania", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("ANG_ID ASC")
	private List<DelAngaze> delAngazes = new ArrayList<>();
	
	public DeltZrodlaFinansowania clone(){
		
		DeltZrodlaFinansowania zF = new DeltZrodlaFinansowania();

		//zF.setDeltPozycjeKalkulacji(deltPozycjeKalkulacji);
		zF.sqlBean = sqlBean;
		zF.pkzfKwota = pkzfKwota;
		zF.pkzfOpis = pkzfOpis;
		zF.pkzfProcent = pkzfProcent;
		zF.pkzfSkId = pkzfSkId;
		zF.pkzfId = 0L;
		
		return zF;
		
	}
	
	
	public DeltZrodlaFinansowania clone(boolean deepCopy){
		DeltZrodlaFinansowania zf = clone();
		zf.setSqlBean(sqlBean);
		if (!deepCopy) return zf;

		for (DelAngaze an : this.delAngazes){
			DelAngaze a = an.clone();
			a.setAngId(0L);
			zf.addDelAngaze(a);
		}
			
		return zf;
		
	}

	public DeltZrodlaFinansowania() {
	}

	public Date getPkzfAudytDm() {
		return this.pkzfAudytDm;
	}

	public void setPkzfAudytDm(Date pkzfAudytDm) {
		this.pkzfAudytDm = pkzfAudytDm;
	}

	public Date getPkzfAudytDt() {
		return this.pkzfAudytDt;
	}

	public void setPkzfAudytDt(Date pkzfAudytDt) {
		this.pkzfAudytDt = pkzfAudytDt;
	}

	public String getPkzfAudytKm() {
		return this.pkzfAudytKm;
	}

	public void setPkzfAudytKm(String pkzfAudytKm) {
		this.pkzfAudytKm = pkzfAudytKm;
	}

	public String getPkzfAudytLm() {
		return this.pkzfAudytLm;
	}

	public void setPkzfAudytLm(String pkzfAudytLm) {
		this.pkzfAudytLm = pkzfAudytLm;
	}

	public String getPkzfAudytUm() {
		return this.pkzfAudytUm;
	}

	public void setPkzfAudytUm(String pkzfAudytUm) {
		this.pkzfAudytUm = pkzfAudytUm;
	}

	public String getPkzfAudytUt() {
		return this.pkzfAudytUt;
	}

	public void setPkzfAudytUt(String pkzfAudytUt) {
		this.pkzfAudytUt = pkzfAudytUt;
	}

	public long getPkzfId() {
		return this.pkzfId;
	}

	public void setPkzfId(long pkzfId) {
		this.pkzfId = pkzfId;
	}






	public void przeliczAngaze() {
		for (DelAngaze a : this.getDelAngazes()) {
//			double procent = nz(a.getAngProcent());
			double kwotaAng = divide(multiply(this.pkzfKwota, a.getAngProcent()) ,100.0, 2) ;//round((nz(this.pkzfKwota) * procent) / 100, 2);
			a.setAngKwota(kwotaAng);
		}
	}

	public Double getPkzfKwota() {
		return this.pkzfKwota;
	}

	public void setPkzfKwota(Double pkzfKwota) {
		if (pkzfKwota!=null){
			pkzfKwota = Utils.round(pkzfKwota,2);
		}

		boolean eq = eq(pkzfKwota, this.pkzfKwota);
		this.pkzfKwota = pkzfKwota;

		if(!eq && this.deltPozycjeKalkulacji!=null){

			double kwotaPkl = nz(this.deltPozycjeKalkulacji.getPklKwotaMf());

			if (kwotaPkl!=0.0)
				this.pkzfProcent = multiply(100.0, divide(pkzfKwota, kwotaPkl),2) ;//round(nz(pkzfKwota) / kwotaPkl * 100, 2);

			przeliczAngaze();
		}
	}

	public Double getPkzfProcent() {
		return this.pkzfProcent;
	}

	public void setPkzfProcent(Double pkzfProcent) {
		boolean eq = eq(pkzfProcent, this.pkzfProcent);
		this.pkzfProcent = pkzfProcent;
		if(!eq && this.deltPozycjeKalkulacji!=null){

			double kwotaPkl = nz(this.deltPozycjeKalkulacji.getPklKwotaMf());

			this.pkzfKwota = divide( multiply(pkzfProcent, kwotaPkl),100.0, 2);//round(((nz(pkzfProcent) * kwotaPkl) / 100), 2);

			przeliczAngaze();

		}
	}





	public String getPkzfOpis() {
		return this.pkzfOpis;
	}

	public void setPkzfOpis(String pkzfOpis) {
		this.pkzfOpis = pkzfOpis;
	}

	public Long getPkzfSkId() {
		return this.pkzfSkId;
	}

	public void setPkzfSkId(Long pkzfSkId) {
		this.pkzfSkId = pkzfSkId;
	}
	
	public Double getPkzfKurs() {
		return pkzfKurs;
	}

	public void setPkzfKurs(Double pkzfKurs) {
		this.pkzfKurs = pkzfKurs;
	}

	public DeltPozycjeKalkulacji getDeltPozycjeKalkulacji() {
		return this.deltPozycjeKalkulacji;
	}

	public void setDeltPozycjeKalkulacji(DeltPozycjeKalkulacji deltPozycjeKalkulacji) {
		this.deltPozycjeKalkulacji = deltPozycjeKalkulacji;
	}
	
	public List<DelAngaze> getDelAngazes() {
		return delAngazes;
	}

	public void setDelAngazes(List<DelAngaze> delAngazes) {
		this.delAngazes = delAngazes;
	}
	
	public void addNewDelAngaze() {
		this.addDelAngaze(new DelAngaze());
	}

	public DelAngaze addDelAngaze(DelAngaze delAngaze) {
		delAngaze.setDeltZrodlaFinansowania(this);
		if(delAngaze.getAngProcent()== null && delAngaze.getAngKwota() == null) {
			delAngaze.setAngKwota(0.0);
			delAngaze.setAngProcent(0.0);
		}
		getDelAngazes().add(delAngaze);
		
		return delAngaze;
	}

	public DelAngaze removeDelAngaze(DelAngaze delAngaze) {
		getDelAngazes().remove(delAngaze);
		delAngaze.setDeltZrodlaFinansowania(null);

		return delAngaze;
	}



	public double getDeltAngazesKwotaSuma(){
		double ret = this.getDelAngazes().stream().mapToDouble(a -> nz(a.getAngKwota())).sum();
		return round(ret,2);
	}

	public double getDeltAngazesProcentSuma(){
		double ret = this.getDelAngazes().stream().mapToDouble(a -> nz(a.getAngProcent())).sum();
		return ret;
	}



	public void dodajAngaz(DataRow r){
		if (r==null) return;
		DelAngaze ang = new DelAngaze();
		ang.setAngWnrId(r.getIdAsLong());
		double kwotaAng = round(nz(this.getPkzfKwota()) - this.getDeltAngazesKwotaSuma(),2);
		this.addDelAngaze(ang);
		ang.setAngKwota(kwotaAng);
	}


	public boolean isValidZF(){

		if (this.getPkzfSkId()==null)
			return false; //brak zf (skid)

		if (eq(nz(this.pkzfKwota),0.0d))
			return false;//brak kwoty


		if (this.getDelAngazes().isEmpty())
			return false;// brak angazy

		if (!eq( this.getDeltAngazesKwotaSuma(), this.getPkzfKwota()) )
			return false; //suma wartości z angaży jest inna niż wartosc ZF

		return this.getDelAngazes().stream()
				.filter(a -> a.getAngWnrId() == null || eq(nz(a.getAngKwota()), 0.0d))
				.collect(Collectors.toList()).isEmpty();//co najmniej jeden angaż ma miustawiony wniosek

	}


	public String zfAngazeWnrIdProcentLstJoinKey(){

		List<String> rl = this.getDelAngazes().stream()
				.map(a -> a.zfSortKey())
				.sorted()
				.collect(Collectors.toList());

		return String.join(";", rl );
	}


	public KursWaluty getKurs(){
        if (this.getDelZaliczki()==null){
        	Date kalDataKursu = this.getDeltPozycjeKalkulacji().getDeltKalkulacje().getKalDataKursu();
			return new KursWalutySprzedazyNBP(this.deltPozycjeKalkulacji.getPklWalId(), kalDataKursu);
        } else {
			return  this.getDelZaliczki().getKurs();
        }
    }

    public String getSymbol(){
		return getLW("PPL_DEL_ZRODLA_FIN").findAndFormat(this.getPkzfSkId(),"%3$s");
	}


	public String groupZfAngKey(){
		String ret = lpad(this.getPkzfSkId(),10, '0');// np "0000102668"  //this.getSymbol;
		List<DelAngaze> angSortedLst = this.getDelAngazes().stream().sorted((a1, a2) -> nz(a1.getAngWnrId()).compareTo(nz(a2.getAngWnrId()))).collect(Collectors.toList());
		for (DelAngaze a : angSortedLst) {
			ret += "."+lpad(nz( a.getAngWnrId()) ,10,'0') ;
		}
		return ret; //np. "0000102668.0000444333.0000444356"
	}


	public boolean isRozrachunekDelegowanego(){
		if (this.getDeltPozycjeKalkulacji()==null) return true;
		return this.deltPozycjeKalkulacji.isRozrachunekDelegowanego();
	}
}
