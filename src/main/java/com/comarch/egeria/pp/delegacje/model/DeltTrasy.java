package com.comarch.egeria.pp.delegacje.model;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.web.User;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.nz;


/**
 * The persistent class for the PPT_DEL_TRASY database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_TRASY", schema="PPADM")
@NamedQuery(name="DeltTrasy.findAll", query="SELECT d FROM DeltTrasy d")
public class DeltTrasy extends DelBase implements Serializable, Comparable<DeltTrasy> {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_TRASY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_TRASY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_TRASY")	
	@Column(name="TRS_ID")
	private long trsId;

	@Temporal(TemporalType.DATE)
	@Column(name="TRS_AUDYT_DM")
	private Date trsAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="TRS_AUDYT_DT", updatable=false)
	private Date trsAudytDt;

	@Column(name="TRS_AUDYT_KM")
	private String trsAudytKm;

//	@Column(name="TRS_AUDYT_KT")
//	private String trsAudytKt;

	@Column(name="TRS_AUDYT_LM")
	private String trsAudytLm;

	@Column(name="TRS_AUDYT_UM")
	private String trsAudytUm;

	@Column(name="TRS_AUDYT_UT", updatable=false)
	private String trsAudytUt;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRS_DATA_DO", columnDefinition="DATE")
	private Date trsDataDo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRS_DATA_OD", columnDefinition="DATE")
	private Date trsDataOd;

	@Column(name="TRS_KR_ID_DO")
	private Long trsKrIdDo;

	@Column(name="TRS_KR_ID_OD")
	private Long trsKrIdOd;

//	@Column(name="TRS_LP")
//	private Long trsLp;

	@Column(name="TRS_MIEJSCOWOSC_DO")
	private String trsMiejscowoscDo;

	@Column(name="TRS_MIEJSCOWOSC_OD")
	private String trsMiejscowoscOd;

//	@Column(name="TRS_ODLEGLOSC")
//	private Double trsOdleglosc;

	@Column(name="TRS_OPIS")
	private String trsOpis;

	@Column(name="TRS_SRODEK_LOKOMOCJI")
	private String trsSrodekLokomocji;
	
	@Column(name="TRS_KR_ID_DIETY")
	private Long trsKrIdDiety;
	
	//bi-directional many-to-one association to DeltRozliczeniaDelegacji
//	@ManyToOne(cascade={CascadeType.ALL}) !!!!!!!!!nie uzywac kaskady w kierunku rodzica
	@ManyToOne
	@JoinColumn(name="TRS_KAL_ID")
	private DeltKalkulacje deltKalkulacje;
//	private DeltRozliczeniaDelegacji deltRozliczeniaDelegacji;
	
	@Column(name="TRS_KOSZT_BILETU")
	private Double trsKosztBiletu;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRS_TERMIN_WYKUPU_BILETU")
	private Date trsTerminWykupuBiletu;
	
	@Column(name="TRS_FAKTURA_VAT_NA_BILET")
	private String trsFakturaVatNaBilet;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRS_TERMIN_PLATNOSCI_ZA_BILET")
	private Date trsTerminPlatnosciZaBilet;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRS_CZYNNOSCI_SLUZBOWE_OD", columnDefinition="DATE")
	private Date trsCzynnosciSluzboweOd;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="TRS_CZYNNOSCI_SLUZBOWE_DO", columnDefinition="DATE")
	private Date trsCzynnosciSluzboweDo;
	
	//bi-directional many-to-one association to DeltTrasy
	@OneToMany(mappedBy="deltTrasy", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("PKL_ID ASC")
	private List<DeltPozycjeKalkulacji> deltPozycjeKalkulacji = new ArrayList<>();

	@Transient
	public DeltTrasy cloneSrc;
	
	@Transient
	public DeltTrasy cloneDest;
	
	
	public DeltTrasy() {
	}
	
	public DeltTrasy clone(){//shallow copy
		
		DeltTrasy tr = new DeltTrasy();
		
		tr.trsId = 0L;
		tr.trsDataDo = trsDataDo;
		tr.trsDataOd = trsDataOd;
		tr.trsKrIdDo = trsKrIdDo;
		tr.trsKrIdOd = trsKrIdOd;
		tr.trsMiejscowoscDo = trsMiejscowoscDo;
		tr.trsMiejscowoscOd = trsMiejscowoscOd;
		tr.trsOpis = trsOpis;
		tr.trsSrodekLokomocji = trsSrodekLokomocji;
		tr.trsKrIdDiety = trsKrIdDiety;
		
		if (trsKosztBiletu != null) {
			tr.trsKosztBiletu = trsKosztBiletu;
		}

		if (trsTerminWykupuBiletu != null) {
			tr.trsTerminWykupuBiletu = trsTerminWykupuBiletu;
		}
		
		if (trsFakturaVatNaBilet != null) {
			tr.trsFakturaVatNaBilet = trsFakturaVatNaBilet;
		}
		
		if (trsTerminPlatnosciZaBilet != null) {
			tr.trsTerminPlatnosciZaBilet = trsTerminPlatnosciZaBilet;
		}
		
		
		return tr;
		
	}
	

	public long getTrsId() {
		this.onRXGet("trsId");
		return this.trsId;
	}

	public void setTrsId(long trsId) {
		this.onRXSet("trsId", this.trsId, trsId);
		this.trsId = trsId;
	}

	public Date getTrsAudytDm() {
		this.onRXGet("trsAudytDm");
		return this.trsAudytDm;
	}

	public void setTrsAudytDm(Date trsAudytDm) {
		this.onRXSet("trsAudytDm", this.trsAudytDm, trsAudytDm);
		this.trsAudytDm = trsAudytDm;
	}

	public Date getTrsAudytDt() {
		this.onRXGet("trsAudytDt");
		return this.trsAudytDt;
	}

	public void setTrsAudytDt(Date trsAudytDt) {
		this.onRXSet("trsAudytDt", this.trsAudytDt, trsAudytDt);
		this.trsAudytDt = trsAudytDt;
	}

	public String getTrsAudytKm() {
		this.onRXGet("trsAudytKm");
		return this.trsAudytKm;
	}

	public void setTrsAudytKm(String trsAudytKm) {
		this.onRXSet("trsAudytKm", this.trsAudytKm, trsAudytKm);
		this.trsAudytKm = trsAudytKm;
	}

//	public String getTrsAudytKt() {
//		return this.trsAudytKt;
//	}
//
//	public void setTrsAudytKt(String trsAudytKt) {
//		this.trsAudytKt = trsAudytKt;
//	}

	public String getTrsAudytLm() {
		this.onRXGet("trsAudytLm");
		return this.trsAudytLm;
	}

	public void setTrsAudytLm(String trsAudytLm) {
		this.onRXSet("trsAudytLm", this.trsAudytLm, trsAudytLm);
		this.trsAudytLm = trsAudytLm;
	}

	public String getTrsAudytUm() {
		this.onRXGet("trsAudytUm");
		return this.trsAudytUm;
	}

	public void setTrsAudytUm(String trsAudytUm) {
		this.onRXSet("trsAudytUm", this.trsAudytUm, trsAudytUm);
		this.trsAudytUm = trsAudytUm;
	}

	public String getTrsAudytUt() {
		this.onRXGet("trsAudytUt");
		return this.trsAudytUt;
	}

	public void setTrsAudytUt(String trsAudytUt) {
		this.onRXSet("trsAudytUt", this.trsAudytUt, trsAudytUt);
		this.trsAudytUt = trsAudytUt;
	}

	public Date getTrsDataDo() {
		this.onRXGet("trsDataDo");
		return this.trsDataDo;
	}

	public void setTrsDataDo(Date trsDataDo) {
		this.onRXSet("trsDataDo", this.trsDataDo, trsDataDo);
		boolean eq = eq(this.trsDataDo, trsDataDo);
		this.trsDataDo = trsDataDo;
		if (!eq && this.deltKalkulacje!=null){
			this.deltKalkulacje.trySortDeltTrasies();
		}
	}

	public Date getTrsDataOd() {
		this.onRXGet("trsDataOd");
		return this.trsDataOd;
	}

	public void setTrsDataOd(Date trsDataOd) {
		this.onRXSet("trsDataOd", this.trsDataOd, trsDataOd);
		boolean eq = eq(this.trsDataOd, trsDataOd);
		this.trsDataOd = trsDataOd;
		if (!eq && this.deltKalkulacje!=null){
			this.deltKalkulacje.trySortDeltTrasies();
		}
	}

	public Long getTrsKrIdDo() {
		this.onRXGet("trsKrIdDo");
		return this.trsKrIdDo;
	}

	public void setTrsKrIdDo(Long trsKrIdDo) {
		if(!eq(this.trsKrIdDo,trsKrIdDo))
			this.setTrsKrIdDiety(trsKrIdDo); //TODO sprawdzić
		this.onRXSet("trsKrIdDo", this.trsKrIdDo, trsKrIdDo);
		this.trsKrIdDo = trsKrIdDo;
	}

	public Long getTrsKrIdOd() {
		this.onRXGet("trsKrIdOd");
		return this.trsKrIdOd;
	}

	public void setTrsKrIdOd(Long trsKrIdOd) {
		this.onRXSet("trsKrIdOd", this.trsKrIdOd, trsKrIdOd);
		this.trsKrIdOd = trsKrIdOd;
	}

//	public Long getTrsLp() {
//		return this.trsLp;
//	}

//	public void setTrsLp(Long trsLp) {
//		this.trsLp = trsLp;
//	}

	public String getTrsMiejscowoscDo() {
		this.onRXGet("trsMiejscowoscDo");
		return this.trsMiejscowoscDo;
	}

	public void setTrsMiejscowoscDo(String trsMiejscowoscDo) {
		this.onRXSet("trsMiejscowoscDo", this.trsMiejscowoscDo, trsMiejscowoscDo);
		this.trsMiejscowoscDo = trsMiejscowoscDo;

		if(this.getDeltPozycjeKalkulacji() != null && !this.getDeltPozycjeKalkulacji().isEmpty()) {
			ustawOpisPkl(this.getDeltPozycjeKalkulacji().get(0));
		}
	}

	public String getTrsMiejscowoscOd() {
		this.onRXGet("trsMiejscowoscOd");
		return this.trsMiejscowoscOd;
	}

	public void setTrsMiejscowoscOd(String trsMiejscowoscOd) {
		this.onRXSet("trsMiejscowoscOd", this.trsMiejscowoscOd, trsMiejscowoscOd);
		this.trsMiejscowoscOd = trsMiejscowoscOd;

		if(this.getDeltPozycjeKalkulacji() != null && !this.getDeltPozycjeKalkulacji().isEmpty()) {
			ustawOpisPkl(this.getDeltPozycjeKalkulacji().get(0));
		}
	}

//	public Double getTrsOdleglosc() {
//		return this.trsOdleglosc;
//	}
//
//	public void setTrsOdleglosc(Double trsOdleglosc) {
//		this.trsOdleglosc = trsOdleglosc;
//	}

	public String getTrsOpis() {
		this.onRXGet("trsOpis");
		return this.trsOpis;
	}

	public void setTrsOpis(String trsOpis) {
		this.onRXSet("trsOpis", this.trsOpis, trsOpis);
		this.trsOpis = trsOpis;
	}

	public DeltKalkulacje getDeltKalkulacje() {
		this.onRXGet("deltKalkulacje");
		return this.deltKalkulacje;
	}

	public void setDeltKalkulacje(DeltKalkulacje deltKalkulacje) {
		this.onRXSet("deltKalkulacje", this.deltKalkulacje, deltKalkulacje);
		this.deltKalkulacje = deltKalkulacje;
	}

	public String getTrsSrodekLokomocji() {
		this.onRXGet("trsSrodekLokomocji");
		return this.trsSrodekLokomocji;
	}

	public void setTrsSrodekLokomocji(String trsSrodekLokomocji) {
		this.onRXSet("trsSrodekLokomocji", this.trsSrodekLokomocji, trsSrodekLokomocji);
		final boolean eq = eq(this.trsSrodekLokomocji, trsSrodekLokomocji);
		this.trsSrodekLokomocji = trsSrodekLokomocji;
		if(!eq ){
			if(nz(this.getTrsKosztBiletu()) != 0.0) {
				this.getDeltPozycjeKalkulacji().get(0).setPklFormaPlatnosci(this.getFormaPlatnosciWgSlok());
			}
			//po zmianie slok. warto zmienic f wyplaty na pKL?
		}

	}

	public Long getTrsKrIdDiety() {
		this.onRXGet("trsKrIdDiety");
		return this.trsKrIdDiety;
	}

	public void setTrsKrIdDiety(Long trsKrIdDiety) {
		this.onRXSet("trsKrIdDiety", this.trsKrIdDiety, trsKrIdDiety);
		this.trsKrIdDiety = trsKrIdDiety;
	}

	public Double getTrsKosztBiletu() {
		this.onRXGet("trsKosztBiletu");
		return this.trsKosztBiletu;
	}

	public void setTrsKosztBiletu(Double trsKosztBiletu) {
		this.onRXSet("trsKosztBiletu", this.trsKosztBiletu, trsKosztBiletu);
		final boolean eq = eq(this.trsKosztBiletu, trsKosztBiletu);
		this.trsKosztBiletu = trsKosztBiletu;

		if (!eq) {
			if (nz(trsKosztBiletu).doubleValue()==0.0){
				clearDeltPozycjeKalkulacji();//kasujemy PKL powiazany()-e) z trasą
			} else {
				DeltPozycjeKalkulacji pkl = this.getDeltPozycjeKalkulacji().isEmpty() ? addNewDeltPozycjeKalkulacji() : this.getDeltPozycjeKalkulacji().get(0);
				if(!eq(this.getTrsKosztBiletu() ,pkl.getPklStawka())) {
					pkl.setPklStawka(trsKosztBiletu);
					ustawOpisPkl(pkl);
				}
			}
		}
	}

	private void ustawOpisPkl(DeltPozycjeKalkulacji pkl) {
		if (isZagraniczna())
			pkl.setPklOpis("" + this.getKrajOdSymbol() + "/" + this.getTrsMiejscowoscOd() + " do " + this.getKrajDoSymbol() + "/ " + this.getTrsMiejscowoscDo());
		else
			pkl.setPklOpis(this.getTrsMiejscowoscOd() + " do " + this.getTrsMiejscowoscDo());
	}

	public DeltPozycjeKalkulacji addNewDeltPozycjeKalkulacji() {
		final DeltPozycjeKalkulacji p = new DeltPozycjeKalkulacji();
		this.getDeltKalkulacje().addDeltPozycjeKalkulacji(p);
		p.setPklWalId(1L);
		p.setPklIlosc(0.000);
		p.setPklStawka(0.0);
		p.setPklKwotaPrzelewu(0L);
		p.setPklKwotaGotowki(" ");
		p.setPklKwota(0.0);
		p.setPklKwotaMf(0.0);
		p.setPklRefundacjaKwota(0.0);

		p.setPklTpozId(getTpozIdByKtgPozycji("T"));
		p.ustawDomyslneZF();//tpozid istotny

		if(eq("03",this.getTrsSrodekLokomocji()) || eq("07", this.getTrsSrodekLokomocji())) {
			//samochód służobowy (mały lub duży)
			p.setPklOpis(this.getDeltKalkulacje().getOpisTrasyKrajeMiasta());
			User.info("Utworzona zostala pozycja kalkulacji odpowiadająca za koszt całej trasy");
		} else {
			ustawOpisPkl(p);
		}
		p.setPklFormaPlatnosci(this.getFormaPlatnosciWgSlok());

		p.ustawDomylsnaRefundacja();
		p.setPklKrId(1L);
		p.setPklIlosc(1.0);

		return this.addDeltPozycjeKalkulacji(p);
	}

	public void clearDeltPozycjeKalkulacji() {
		final List<DeltPozycjeKalkulacji> lst = this.getDeltPozycjeKalkulacji().stream().collect(Collectors.toList());
		lst.stream().forEach(p->this.removeDeltPozycjeKalkulacji(p));
		if (this.getDeltKalkulacje()!=null){
			lst.stream().forEach(p->this.getDeltKalkulacje().removeDeltPozycjeKalkulacji(p));
		}
	}

	public String getTrsFakturaVatNaBilet() {
		this.onRXGet("trsFakturaVatNaBilet");
		return this.trsFakturaVatNaBilet;
	}

	public void setTrsFakturaVatNaBilet(String trsFakturaVatNaBilet) {
		this.onRXSet("trsFakturaVatNaBilet", this.trsFakturaVatNaBilet, trsFakturaVatNaBilet);
		this.trsFakturaVatNaBilet = trsFakturaVatNaBilet;
	}

	public Date pgetTrsTerminPlatnosciZaBilet() {
		return getTrsTerminPlatnosciZaBilet();
	}

	public void setTrsTerminPlatnosciZaBilet(Date trsTerminPlatnosciZaBilet) {
		this.trsTerminPlatnosciZaBilet = trsTerminPlatnosciZaBilet;
	}

	public Date getTrsTerminWykupuBiletu() {
		this.onRXGet("trsTerminWykupuBiletu");
		return this.trsTerminWykupuBiletu;
	}

	public void setTrsTerminWykupuBiletu(Date trsTerminWykupuBiletu) {
		this.onRXSet("trsTerminWykupuBiletu", this.trsTerminWykupuBiletu, trsTerminWykupuBiletu);
		this.trsTerminWykupuBiletu = trsTerminWykupuBiletu;
	}

	public Date getTrsTerminPlatnosciZaBilet() {
		this.onRXGet("trsTerminPlatnosciZaBilet");
		return this.trsTerminPlatnosciZaBilet;
	}

	public Date getTrsCzynnosciSluzboweOd() {
		this.onRXGet("trsCzynnosciSluzboweOd");
		return this.trsCzynnosciSluzboweOd;
	}

	public void setTrsCzynnosciSluzboweOd(Date trsCzynnosciSluzboweOd) {
		this.onRXSet("trsCzynnosciSluzboweOd", this.trsCzynnosciSluzboweOd, trsCzynnosciSluzboweOd);
		this.trsCzynnosciSluzboweOd = trsCzynnosciSluzboweOd;
	}

	public Date getTrsCzynnosciSluzboweDo() {
		this.onRXGet("trsCzynnosciSluzboweDo");
		return this.trsCzynnosciSluzboweDo;
	}

	public void setTrsCzynnosciSluzboweDo(Date trsCzynnosciSluzboweDo) {
		this.onRXSet("trsCzynnosciSluzboweDo", this.trsCzynnosciSluzboweDo, trsCzynnosciSluzboweDo);
		this.trsCzynnosciSluzboweDo = trsCzynnosciSluzboweDo;
	}

	public List<DeltPozycjeKalkulacji> getDeltPozycjeKalkulacji() {
		this.onRXGet("deltPozycjeKalkulacji");
		return this.deltPozycjeKalkulacji;
	}

	public void setDeltPozycjeKalkulacji(List<DeltPozycjeKalkulacji> deltPozycjeKalkulacji) {
		this.onRXSet("deltPozycjeKalkulacji", this.deltPozycjeKalkulacji, deltPozycjeKalkulacji);
		this.deltPozycjeKalkulacji = deltPozycjeKalkulacji;
	}
	
	public DeltPozycjeKalkulacji removeDeltPozycjeKalkulacji(DeltPozycjeKalkulacji deltPozycjeKalkulacji) {
		getDeltPozycjeKalkulacji().remove(deltPozycjeKalkulacji);
		deltPozycjeKalkulacji.setDeltTrasy(null);

		return deltPozycjeKalkulacji;
	}
	
	public DeltPozycjeKalkulacji addDeltPozycjeKalkulacji(DeltPozycjeKalkulacji deltPozycjeKalkulacji) {
		getDeltPozycjeKalkulacji().add(deltPozycjeKalkulacji);
		deltPozycjeKalkulacji.setDeltTrasy(this);

		return deltPozycjeKalkulacji;
	}

	public String getKrajOdSymbol(){
		this.onRXGet("getKrajSymbol(this.getTrsKrIdOd())");
		return this.getKrajSymbol(this.getTrsKrIdOd());
	}
	public String getKrajOdNazwa(){
		this.onRXGet("getKrajNazwa(this.getTrsKrIdOd())");
		return this.getKrajNazwa(this.getTrsKrIdOd());
	}


	public String getKrajDoSymbol(){
		this.onRXGet("getKrajSymbol(this.getTrsKrIdDo())");
		return this.getKrajSymbol(this.getTrsKrIdDo());
	}
	public String getKrajDoNazwa(){
		this.onRXGet("getKrajNazwa(this.getTrsKrIdDo())");
		return this.getKrajNazwa(this.getTrsKrIdDo());
	}

	public String getKrajDietySymbol(){
		this.onRXGet("getKrajSymbol(this.getTrsKrIdDiety())");
		return this.getKrajSymbol(this.getTrsKrIdDiety());
	}
	public String getKrajDietyNazwa(){
		this.onRXGet("getKrajNazwa(this.getTrsKrIdDiety())");
		return this.getKrajNazwa(this.getTrsKrIdDiety());
	}


	private String getFormaPlatnosciWgSlok() {
		String formaPlatnosci ="04";
		DataRow r = getLW("PPL_SLOWNIK","DEL_SRODEK_LOKOMOCJI").find(trsSrodekLokomocji);
		if(r != null) {
			formaPlatnosci = "" + r.get("wsl_wartosc_max");
		}
		return formaPlatnosci;
	}


	@Override
	public int compareTo(DeltTrasy t) {//kolejnosc wg dataOd, jesli null, wg dataDo ...
		Date d0 = Utils.coalesce(this.getTrsDataOd(), this.getTrsDataDo(), Utils.dateSerial(2999,1,1));
		Date d1 = Utils.coalesce(t.getTrsDataOd(), t.getTrsDataDo(),  Utils.dateSerial(3000,1,1));
		return d0.compareTo(d1);
	}

	@Override
	public DeltWnioskiDelegacji getWniosek() {
		if (this.getDeltKalkulacje()==null) return null;
		return this.getDeltKalkulacje().getDeltWnioskiDelegacji();
	}
}
