package com.comarch.egeria.pp.delegacje.model;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.lowagie.text.DocumentException;
import net.sf.jasperreports.engine.JRException;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.*;


/**
 * The persistent class for the PPT_DEL_WNIOSKI_DELEGACJI database table.
 */
@Entity
@Table(name = "PPT_DEL_WNIOSKI_DELEGACJI", schema = "PPADM")
@NamedQuery(name = "DeltWnioskiDelegacji.findAll", query = "SELECT d FROM DeltWnioskiDelegacji d")
@DiscriminatorFormula("CASE WHEN WND_RODZAJ = 1 THEN 'WniosekDelegacjiKraj' ELSE 'WniosekDelegacjiZagr' END")
public abstract class DeltWnioskiDelegacji extends DelBase implements Serializable {
    private static final long serialVersionUID = 1L;

//	@Transient
//	private SqlBean sql;

    @Id
    @TableGenerator(name = "TABLE_KEYGEN_PPT_DEL_WNIOSKI_DELEGACJI", table = "PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_WNIOSKI_DELEGACJI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "TABLE_KEYGEN_PPT_DEL_WNIOSKI_DELEGACJI")
    @Column(name = "WND_ID")
    private long wndId;

    @Column(name = "WND_ADR_ID")
    private Long wndAdrId;

    @Temporal(TemporalType.DATE)
    @Column(name = "WND_AUDYT_DM")
    private Date wndAudytDm;

    @Temporal(TemporalType.DATE)
    @Column(name = "WND_AUDYT_DT", updatable = false)
    private Date wndAudytDt;

    @Column(name = "WND_AUDYT_KM")
    private String wndAudytKm;

//	@Column(name="WND_AUDYT_KT")
//	private String wndAudytKt;

    @Version
    @Column(name = "WND_AUDYT_LM")
    private Long wndAudytLm;

    @Column(name = "WND_AUDYT_UM")
    private String wndAudytUm;

    @Column(name = "WND_AUDYT_UT", updatable = false)
    private String wndAudytUt;

    @Temporal(TemporalType.DATE)
    @Column(name = "WND_DATA_POWROTU")
    private Date wndDataPowrotu;

    @Temporal(TemporalType.DATE)
    @Column(name = "WND_DATA_WNIOSKU")
    private Date wndDataWniosku;

    @Temporal(TemporalType.DATE)
    @Column(name = "WND_DATA_WYJAZDU")
    private Date wndDataWyjazdu;

//	@Column(name="WND_F_PREFEROWANA_GOTOWKA")
//	private String wndFPreferowanaGotowka;

    @Column(name = "WND_F_WYJAZD_Z_DOMU")
    private String wndFWyjazdZDomu = "N";

    @Column(name = "WND_F_PRYW")
    private String wndFPryw = "N";

    @Column(name = "WND_ZAL_FORMA_WYPLATY")
    private String wndZalFormaWyplaty;

    @Column(name = "WND_ZAL_KNT_ID")
    private Long wndZalKntId;

//	@Column(name="WND_KOLACJE")
//	private Long wndKolacje = 0l;

//	@Column(name="WND_NOCLEGI")
//	private Long wndNoclegi = 0l;

    @Column(name = "WND_NUMER")
    private String wndNumer;

    @Column(name = "WND_OB_ID")
    private Long wndObId;

//	@Column(name="WND_OBIADY")
//	private Long wndObiady = 0l;

    @Column(name = "WND_OPIS")
    private String wndOpis;

    @Column(name = "WND_POLE01")
    private String wndPole01;

    @Column(name = "WND_POLE02")
    private String wndPole02;

    @Column(name = "WND_POLE03")
    private String wndPole03;

    @Column(name = "WND_POLE04")
    private String wndPole04;

    @Column(name = "WND_POLE05")
    private String wndPole05;

    @Column(name = "WND_POLE06")
    private String wndPole06;

    @Column(name = "WND_POLE07")
    private String wndPole07;

    @Column(name = "WND_POLE08")
    private String wndPole08;

    @Column(name = "WND_POLE09")
    private String wndPole09;

    @Column(name = "WND_POLE10")
    private String wndPole10;

    @Column(name = "WND_PRC_ID")
    private Long wndPrcId;

    @Column(name = "WND_OBS_PRC_ID")
    private Long wndObsPrcId;

    @Column(name = "WND_RODZAJ")
    private Long wndRodzaj;

//	@Column(name="WND_SNIADANIA")
//	private Long wndSniadania = 0l;

    @Column(name = "WND_STATUS")
    private Long wndStatus;

//	@Column(name="WND_TRANSPORT")
//	private String wndTransport = "N";

    @Column(name = "WND_WND_ID")
    private Long wndWndId;

    @Column(name = "WND_F_CZY_GRUPOWA")
    private String wndCzyGrupowa = "N";

    @Column(name = "WND_PRYW_DATA_WYJAZDU")
    private Date wndPrywDataWyjazdu;

    @Column(name = "WND_PRYW_DATA_POWROTU")
    private Date wndPrywDataPowrotu;

    @Column(name = "WND_TRASA_WYJAZDU")
    private String wndTrasaWyjazdu;

    @Column(name = "WND_TRASA_POWROTU")
    private String wndTrasaPowrotu;

    @Column(name = "wnd_f_czy_szkol")
    private String wndFCzySzkolenie = "N";

    @Column(name = "WND_UWAGI")
    private String wndUwagi;

    @Column(name = "WND_GR_UZASADNIENIE")
    private String wndGrUzasadnienie;

    @Column(name = "WND_F_BEZKOSZTOWA")
    private String wndFBezkosztowa;

    @Column(name = "WND_F_PROJEKTOWA")
    private String wndFProjektowa = "N";

    @Column(name = "WND_F_KONFERENCYJNA")
    private String wndFKonferencyjna = "N";
	
/*	@Column(name="WND_TYP_DIETY")
	private String wndTypDiety;*/

//	@ManyToOne
//	@JoinColumn(name="WND_STD_ID")
//	private DelStawkiDelegacji delStawkiDelegacji;

    @Column(name = "WND_STD_ID")
    private Long wndStdId; //TODO = 11L;

    @Column(name = "WND_F_ZALICZKA")
    private String wndFZaliczka;

    @Column(name = "WND_F_JEDNA_WALUTA")
    private String wndFJednaWaluta;

    @Column(name = "WND_NUMER_NR")
    private Long wndNumerNr;

    @Column(name = "WND_F_MIEJSCOWOSC_POBYT")
    private String wndFMiejscowoscPobyt = "N";

    @Column(name = "WND_MIEJSCE_DOCELOWE")
    private String wndMiejsceDocelowe;

    @Column(name = "WND_F_CZY_SWIADCZ_NIESTD")
    private String wndFCzySwiadczNiestd;

    @Column(name = "WND_F_KONF_KOSZTOWA")
    private String wndFKonfKosztowa;

    @Column(name = "WND_SK_ID")
    private Long wndSkId;

    @Column(name = "WND_WNR_ID")
    private Long wndWnrId;

    @Column(name = "WND_F_DOMYSLNE_ZF")
    private String wndFDomyslneZf = "T";

    @Column(name = "WND_PROJ_KOD")
    private String wndProjKod;

    @Column(name = "WND_DOK_ID")
    private Long wndDokId;

    //bi-directional many-to-one association to DeltCeleDelegacji
    @OneToMany(mappedBy = "deltWnioskiDelegacji", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @BatchSize(size = 200)
    @OrderBy("CDEL_ID ASC")
    private List<DeltCeleDelegacji> deltCeleDelegacjis = new ArrayList<>();

//	//bi-directional many-to-one association to DeltRozliczeniaDelegacji
//	@OneToMany(mappedBy="deltWnioskiDelegacji", fetch=FetchType.LAZY, cascade={CascadeType.ALL}, orphanRemoval=true)
//	private List<DeltRozliczeniaDelegacji> deltRozliczeniaDelegacjis = new ArrayList<>();


    //bi-directional many-to-one association to DeltSrodkiLokomocji
    @OneToMany(mappedBy = "deltWnioskiDelegacji", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @BatchSize(size = 200)
    @OrderBy("SLOK_ID ASC")
    private List<DeltSrodkiLokomocji> deltSrodkiLokomocjis = new ArrayList<>();

    //bi-directional many-to-one association to DeltZapraszajacy
    @OneToMany(mappedBy = "deltWnioskiDelegacji", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @BatchSize(size = 200)
    @OrderBy("ZAPR_ID ASC")
    private List<DeltZapraszajacy> deltZapraszajacys = new ArrayList<>();

    @OneToMany(mappedBy = "deltWnioskiDelegacji", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @BatchSize(size = 200)
    private List<DeltProjekty> deltProjektys = new ArrayList<>();

    @OneToMany(mappedBy = "deltWnioskiDelegacji", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @BatchSize(size = 200)
    private List<DeltKalkulacje> deltKalkulacjes = new ArrayList<>();

    @OneToMany(mappedBy = "deltWnioskiDelegacji", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @BatchSize(size = 200)
    @OrderBy("ANG_ID ASC")
    private List<DelAngaze> delAngazes = new ArrayList<>();


    public DeltWnioskiDelegacji() {
    }

    public long getWndId() {
        this.onRXGet("wndId");
        return this.wndId;
    }

    public void setWndId(long wndId) {
        this.onRXSet("wndId", this.wndId, wndId);
        this.wndId = wndId;
    }

    public Long getWndAdrId() {
        this.onRXGet("wndAdrId");
        return this.wndAdrId;
    }

    public void setWndAdrId(Long wndAdrId) {
        this.onRXSet("wndAdrId", this.wndAdrId, wndAdrId);
        this.wndAdrId = wndAdrId;
    }

    public Date getWndAudytDm() {
        this.onRXGet("wndAudytDm");
        return this.wndAudytDm;
    }

    public void setWndAudytDm(Date wndAudytDm) {
        this.onRXSet("wndAudytDm", this.wndAudytDm, wndAudytDm);
        this.wndAudytDm = wndAudytDm;
    }

    public Date getWndAudytDt() {
        this.onRXGet("wndAudytDt");
        return this.wndAudytDt;
    }

    public void setWndAudytDt(Date wndAudytDt) {
        this.onRXSet("wndAudytDt", this.wndAudytDt, wndAudytDt);
        this.wndAudytDt = wndAudytDt;
    }

    public String getWndAudytKm() {
        this.onRXGet("wndAudytKm");
        return this.wndAudytKm;
    }

    public void setWndAudytKm(String wndAudytKm) {
        this.onRXSet("wndAudytKm", this.wndAudytKm, wndAudytKm);
        this.wndAudytKm = wndAudytKm;
    }

//	public String getWndAudytKt() {
//		return this.wndAudytKt;
//	}

//	public void setWndAudytKt(String wndAudytKt) {
//		this.wndAudytKt = wndAudytKt;
//	}

    public Long getWndAudytLm() {
        this.onRXGet("wndAudytLm");
        return this.wndAudytLm;
    }

    public void setWndAudytLm(Long wndAudytLm) {
        this.onRXSet("wndAudytLm", this.wndAudytLm, wndAudytLm);
        this.wndAudytLm = wndAudytLm;
    }

    public String getWndAudytUm() {
        this.onRXGet("wndAudytUm");
        return this.wndAudytUm;
    }

    public void setWndAudytUm(String wndAudytUm) {
        this.onRXSet("wndAudytUm", this.wndAudytUm, wndAudytUm);
        this.wndAudytUm = wndAudytUm;
    }

    public String getWndAudytUt() {
        this.onRXGet("wndAudytUt");
        return this.wndAudytUt;
    }

    public void setWndAudytUt(String wndAudytUt) {
        this.onRXSet("wndAudytUt", this.wndAudytUt, wndAudytUt);
        this.wndAudytUt = wndAudytUt;
    }

    public Date getWndDataPowrotu() {
        this.onRXGet("wndDataPowrotu");
        return this.wndDataPowrotu;
    }

    public void setWndDataPowrotu(Date wndDataPowrotu) {
        this.onRXSet("wndDataPowrotu", this.wndDataPowrotu, wndDataPowrotu);
        this.wndDataPowrotu = wndDataPowrotu;
    }

    public Date getWndDataWniosku() {
        this.onRXGet("wndDataWniosku");
        return this.wndDataWniosku;
    }

    public void setWndDataWniosku(Date wndDataWniosku) {
        this.onRXSet("wndDataWniosku", this.wndDataWniosku, wndDataWniosku);
        this.wndDataWniosku = wndDataWniosku;
    }

    public Date getWndDataWyjazdu() {
        this.onRXGet("wndDataWyjazdu");
        return this.wndDataWyjazdu;
    }

    public void setWndDataWyjazdu(Date wndDataWyjazdu) {
        this.onRXSet("wndDataWyjazdu", this.wndDataWyjazdu, wndDataWyjazdu);
        this.wndDataWyjazdu = wndDataWyjazdu;
    }

//	public String getWndFPreferowanaGotowka() {
//		return this.wndFPreferowanaGotowka;
//	}
//
//	public void setWndFPreferowanaGotowka(String wndFPreferowanaGotowka) {
//		this.wndFPreferowanaGotowka = wndFPreferowanaGotowka;
//	}

    public String getWndFWyjazdZDomu() {
        this.onRXGet("wndFWyjazdZDomu");
        return this.wndFWyjazdZDomu;
    }

    public void setWndFWyjazdZDomu(String wndFWyjazdZDomu) {
        this.onRXSet("wndFWyjazdZDomu", this.wndFWyjazdZDomu, wndFWyjazdZDomu);
        this.wndFWyjazdZDomu = wndFWyjazdZDomu;
    }

    public Long getWndZalKntId() {
        this.onRXGet("wndZalKntId");
        return this.wndZalKntId;
    }

    public void setWndKntId(Long wndZalKntId) {
        this.onRXSet("wndZalKntId", this.wndZalKntId, wndZalKntId);
        this.wndZalKntId = wndZalKntId;
    }

//	public Long getWndKolacje() {
//		return this.wndKolacje;
//	}
//
//	public void setWndKolacje(Long wndKolacje) {
//		this.wndKolacje = wndKolacje;
//	}
//
//	public Long getWndNoclegi() {
//		return this.wndNoclegi;
//	}
//
//	public void setWndNoclegi(Long wndNoclegi) {
//		this.wndNoclegi = wndNoclegi;
//	}

    public String getWndNumer() {
        this.onRXGet("wndNumer");
        return this.wndNumer;
    }

    public void setWndNumer(String wndNumer) {
        this.onRXSet("wndNumer", this.wndNumer, wndNumer);
        this.wndNumer = wndNumer;
    }

    public Long getWndObId() {
        this.onRXGet("wndObId");
        return this.wndObId;
    }

    public void setWndObId(Long wndObId) {
        this.onRXSet("wndObId", this.wndObId, wndObId);
        this.wndObId = wndObId;
    }

//	public Long getWndObiady() {
//		return this.wndObiady;
//	}
//
//	public void setWndObiady(Long wndObiady) {
//		this.wndObiady = wndObiady;
//	}

    public String getWndOpis() {
        this.onRXGet("wndOpis");
        return this.wndOpis;
    }

    public void setWndOpis(String wndOpis) {
        this.onRXSet("wndOpis", this.wndOpis, wndOpis);
        this.wndOpis = wndOpis;
    }

    public String getWndPole01() {
        this.onRXGet("wndPole01");
        return this.wndPole01;
    }

    public void setWndPole01(String wndPole01) {
        this.onRXSet("wndPole01", this.wndPole01, wndPole01);
        this.wndPole01 = wndPole01;
    }

    public String getWndPole02() {
        this.onRXGet("wndPole02");
        return this.wndPole02;
    }

    public void setWndPole02(String wndPole02) {
        this.onRXSet("wndPole02", this.wndPole02, wndPole02);
        this.wndPole02 = wndPole02;
    }

    public String getWndPole03() {
        this.onRXGet("wndPole03");
        return this.wndPole03;
    }

    public void setWndPole03(String wndPole03) {
        this.onRXSet("wndPole03", this.wndPole03, wndPole03);
        this.wndPole03 = wndPole03;
    }

    public String getWndPole04() {
        this.onRXGet("wndPole04");
        return this.wndPole04;
    }

    public void setWndPole04(String wndPole04) {
        this.onRXSet("wndPole04", this.wndPole04, wndPole04);
        this.wndPole04 = wndPole04;
    }

    public String getWndPole05() {
        this.onRXGet("wndPole05");
        return this.wndPole05;
    }

    public void setWndPole05(String wndPole05) {
        this.onRXSet("wndPole05", this.wndPole05, wndPole05);
        this.wndPole05 = wndPole05;
    }

    public String getWndPole06() {
        this.onRXGet("wndPole06");
        return this.wndPole06;
    }

    public void setWndPole06(String wndPole06) {
        this.onRXSet("wndPole06", this.wndPole06, wndPole06);
        this.wndPole06 = wndPole06;
    }

    public String getWndPole07() {
        this.onRXGet("wndPole07");
        return this.wndPole07;
    }

    public void setWndPole07(String wndPole07) {
        this.onRXSet("wndPole07", this.wndPole07, wndPole07);
        this.wndPole07 = wndPole07;
    }

    public String getWndPole08() {
        this.onRXGet("wndPole08");
        return this.wndPole08;
    }

    public void setWndPole08(String wndPole08) {
        this.onRXSet("wndPole08", this.wndPole08, wndPole08);
        this.wndPole08 = wndPole08;
    }

    public String getWndPole09() {
        this.onRXGet("wndPole09");
        return this.wndPole09;
    }

    public void setWndPole09(String wndPole09) {
        this.onRXSet("wndPole09", this.wndPole09, wndPole09);
        this.wndPole09 = wndPole09;
    }

    public String getWndPole10() {
        this.onRXGet("wndPole10");
        return this.wndPole10;
    }

    public void setWndPole10(String wndPole10) {
        this.onRXSet("wndPole10", this.wndPole10, wndPole10);
        this.wndPole10 = wndPole10;
    }

    public Long getWndPrcId() {
        this.onRXGet("wndPrcId");
        return this.wndPrcId;
    }

    public void setWndPrcId(Long wndPrcId) {
        this.onRXSet("wndPrcId", this.wndPrcId, wndPrcId);
        final boolean eq = eq(this.wndPrcId, wndPrcId);
        this.wndPrcId = wndPrcId;

        if (!eq || this.getWndStdId() == null) {
            this.przeliczWndStdId();
        }

        if (!eq) {
            przeliczDomyslneZF();
        }
    }

    public Long getWndObsPrcId() {
        this.onRXGet("wndObsPrcId");
        return this.wndObsPrcId;
    }

    public void setWndObsPrcId(Long wndObsPrcId) {
        this.onRXSet("wndObsPrcId", this.wndObsPrcId, wndObsPrcId);
        this.wndObsPrcId = wndObsPrcId;
    }

    public Long getWndRodzaj() {
        this.onRXGet("wndRodzaj");
        return this.wndRodzaj;
    }

    public void setWndRodzaj(Long wndRdelId) {
        this.onRXSet("wndRodzaj", this.wndRodzaj, wndRodzaj);
        this.wndRodzaj = wndRdelId;
    }


    public Boolean isKrajowa() {
        return eq(1L, this.wndRodzaj);
    }

    public Boolean isZagraniczna() {
        return eq(2L, this.wndRodzaj);
    }


//	public Long getWndSniadania() {
//		return this.wndSniadania;
//	}
//
//	public void setWndSniadania(Long wndSniadania) {
//		this.wndSniadania = wndSniadania;
//	}

    public Long getWndStatus() {
        this.onRXGet("wndStatus");
        return this.wndStatus;
    }

    public void setWndStatus(Long wndStwdId) {
        this.onRXSet("wndStatus", this.wndStatus, wndStatus);
        this.wndStatus = wndStwdId;
    }

//	public String getWndTransport() {
//		return this.wndTransport;
//	}
//
//	public void setWndTransport(String wndTransport) {
//		this.wndTransport = wndTransport;
//	}

    public List<DeltCeleDelegacji> getDeltCeleDelegacjis() {
        this.onRXGet("deltCeleDelegacjis");
        return this.deltCeleDelegacjis;
    }

    public void setDeltCeleDelegacjis(List<DeltCeleDelegacji> deltCeleDelegacjis) {
        this.onRXSet("deltCeleDelegacjis", this.deltCeleDelegacjis, deltCeleDelegacjis);
        this.deltCeleDelegacjis = deltCeleDelegacjis;
    }

    public DeltCeleDelegacji addDeltCeleDelegacji(DeltCeleDelegacji deltCeleDelegacji) {
        this.onRXSet("deltCeleDelegacjis", null, this.deltCeleDelegacjis);
        getDeltCeleDelegacjis().add(deltCeleDelegacji);
        deltCeleDelegacji.setDeltWnioskiDelegacji(this);

        return deltCeleDelegacji;
    }

    public DeltCeleDelegacji removeDeltCeleDelegacji(DeltCeleDelegacji deltCeleDelegacji) {
        this.onRXSet("deltCeleDelegacjis", null, this.deltCeleDelegacjis);
        getDeltCeleDelegacjis().remove(deltCeleDelegacji);
        deltCeleDelegacji.setDeltWnioskiDelegacji(null);

        return deltCeleDelegacji;
    }

//	public List<DeltRozliczeniaDelegacji> getDeltRozliczeniaDelegacjis() {
//		return this.deltRozliczeniaDelegacjis;
//	}
//
//	public void setDeltRozliczeniaDelegacjis(List<DeltRozliczeniaDelegacji> deltRozliczeniaDelegacjis) {
//		this.deltRozliczeniaDelegacjis = deltRozliczeniaDelegacjis;
//	}

//	public DeltRozliczeniaDelegacji addDeltRozliczeniaDelegacji(DeltRozliczeniaDelegacji deltRozliczeniaDelegacji) {
//		getDeltRozliczeniaDelegacjis().add(deltRozliczeniaDelegacji);
//		deltRozliczeniaDelegacji.setDeltWnioskiDelegacji(this);
//
//		return deltRozliczeniaDelegacji;
//	}
//
//	public DeltRozliczeniaDelegacji removeDeltRozliczeniaDelegacji(DeltRozliczeniaDelegacji deltRozliczeniaDelegacji) {
//		getDeltRozliczeniaDelegacjis().remove(deltRozliczeniaDelegacji);
//		deltRozliczeniaDelegacji.setDeltWnioskiDelegacji(null);
//
//		return deltRozliczeniaDelegacji;
//	}

    public DeltZapraszajacy addDeltZapraszajacy(DeltZapraszajacy deltZapraszajacy) {
        this.onRXSet("deltZapraszajacys", null, this.deltZapraszajacys);
        getDeltZapraszajacys().add(deltZapraszajacy);
        deltZapraszajacy.setDeltWnioskiDelegacji(this);

        return deltZapraszajacy;
    }

    public DeltZapraszajacy removeDeltZapraszajacy(DeltZapraszajacy deltZapraszajacy) {
        this.onRXSet("deltZapraszajacys", null, this.deltZapraszajacys);
        getDeltZapraszajacys().remove(deltZapraszajacy);
        deltZapraszajacy.setDeltWnioskiDelegacji(null);

        return deltZapraszajacy;
    }


    public void addNewZapraszajacy() {
        this.addDeltZapraszajacy(new DeltZapraszajacy());
    }


    public DeltKalkulacje addDeltKalkulacje(DeltKalkulacje deltKalkulacje) {
        this.onRXSet("deltKalkulacjes", null, this.deltKalkulacjes);
        getDeltKalkulacjes().add(deltKalkulacje);
        deltKalkulacje.setDeltWnioskiDelegacji(this);

        if (deltKalkulacje != null && eq("WNK", deltKalkulacje.getKalRodzaj())) {
            wczytajRodzajeKosztowKalkWnk();
        }

        return deltKalkulacje;
    }

    public DeltKalkulacje removeDeltKalkulacje(DeltKalkulacje deltKalkulacje) {
        this.onRXSet("deltKalkulacjes", null, this.deltKalkulacjes);
        getDeltKalkulacjes().remove(deltKalkulacje);
        deltKalkulacje.setDeltWnioskiDelegacji(null);

        return deltKalkulacje;
    }


    public List<DeltSrodkiLokomocji> getDeltSrodkiLokomocjis() {
        this.onRXGet("deltSrodkiLokomocjis");
        return this.deltSrodkiLokomocjis;
    }

    public void setDeltSrodkiLokomocjis(List<DeltSrodkiLokomocji> deltSrodkiLokomocjis) {
        this.onRXSet("deltSrodkiLokomocjis", this.deltSrodkiLokomocjis, deltSrodkiLokomocjis);
        this.deltSrodkiLokomocjis = deltSrodkiLokomocjis;
    }

    public DeltSrodkiLokomocji addDeltSrodkiLokomocji(DeltSrodkiLokomocji deltSrodkiLokomocji) {
        this.onRXSet("deltSrodkiLokomocjis", null, this.deltSrodkiLokomocjis);
        getDeltSrodkiLokomocjis().add(deltSrodkiLokomocji);
        deltSrodkiLokomocji.setDeltWnioskiDelegacji(this);

        return deltSrodkiLokomocji;
    }

    public DeltSrodkiLokomocji removeDeltSrodkiLokomocji(DeltSrodkiLokomocji deltSrodkiLokomocji) {
        this.onRXSet("deltSrodkiLokomocjis", null, this.deltSrodkiLokomocjis);
        getDeltSrodkiLokomocjis().remove(deltSrodkiLokomocji);
        deltSrodkiLokomocji.setDeltWnioskiDelegacji(null);

        return deltSrodkiLokomocji;
    }

    public DeltProjekty addDeltProjekty(DeltProjekty deltProjekty) {
        this.onRXSet("deltProjektys", null, this.deltProjektys);
        getDeltProjektys().add(deltProjekty);
        deltProjekty.setDeltWnioskiDelegacji(this);

        return deltProjekty;
    }

    public DeltProjekty removeDeltProjekty(DeltProjekty deltProjekty) {
        this.onRXSet("deltProjektys", null, this.deltProjektys);
        getDeltProjektys().remove(deltProjekty);
        deltProjekty.setDeltWnioskiDelegacji(null);

        return deltProjekty;
    }

    public List<DeltZapraszajacy> getDeltZapraszajacys() {
        this.onRXGet("deltZapraszajacys");
        return deltZapraszajacys;
    }

    public void setDeltZapraszajacys(List<DeltZapraszajacy> deltZapraszajacys) {
        this.onRXSet("deltZapraszajacys", this.deltZapraszajacys, deltZapraszajacys);
        this.deltZapraszajacys = deltZapraszajacys;
    }


    public Long getWndWndId() {
        this.onRXGet("wndWndId");
        return wndWndId;
    }


    public void setWndWndId(Long wndWndId) {
        this.onRXSet("wndWndId", this.wndWndId, wndWndId);
        final boolean eq = eq(this.wndWndId, wndWndId);
        this.wndWndId = wndWndId;
        if (!eq) {
            przeliczDomyslneZF();
        }
    }

    public String getWndCzyGrupowa() {
        this.onRXGet("wndCzyGrupowa");
        return wndCzyGrupowa;
    }

    public void setWndCzyGrupowa(String wndCzyGrupowa) {
        this.onRXSet("wndCzyGrupowa", this.wndCzyGrupowa, wndCzyGrupowa);
        final boolean eq = eq(this.wndCzyGrupowa, wndCzyGrupowa);
        this.wndCzyGrupowa = wndCzyGrupowa;
        if (!eq) {
            przeliczDomyslneZF();
        }
    }

    public Date getWndPrywDataWyjazdu() {
        this.onRXGet("wndPrywDataWyjazdu");
        return wndPrywDataWyjazdu;
    }

    public void setWndPrywDataWyjazdu(Date wndPrywDataWyjazdu) {
        this.onRXSet("wndPrywDataWyjazdu", this.wndPrywDataWyjazdu, wndPrywDataWyjazdu);
        this.wndPrywDataWyjazdu = wndPrywDataWyjazdu;
    }

    public Date getWndPrywDataPowrotu() {
        this.onRXGet("wndPrywDataPowrotu");
        return this.wndPrywDataPowrotu;
    }

    public void setWndPrywDataPowrotu(Date wndPrywDataPowrotu) {
        this.onRXSet("wndPrywDataPowrotu", this.wndPrywDataPowrotu, wndPrywDataPowrotu);
        this.wndPrywDataPowrotu = wndPrywDataPowrotu;
    }

    public String getWndGrUzasadnienie() {
        this.onRXGet("wndGrUzasadnienie");
        return this.wndGrUzasadnienie;
    }

    public void setWndGrUzasadnienie(String wndGrUzasadnienie) {
        this.onRXSet("wndGrUzasadnienie", this.wndGrUzasadnienie, wndGrUzasadnienie);
        this.wndGrUzasadnienie = wndGrUzasadnienie;
    }

    public String getWndTrasaWyjazdu() {
        this.onRXGet("wndTrasaWyjazdu");
        return this.wndTrasaWyjazdu;
    }

    public void setWndTrasaWyjazdu(String wndTrasaWyjazdu) {
        this.onRXSet("wndTrasaWyjazdu", this.wndTrasaWyjazdu, wndTrasaWyjazdu);
        this.wndTrasaWyjazdu = wndTrasaWyjazdu;
    }

    public String getWndTrasaPowrotu() {
        this.onRXGet("wndTrasaPowrotu");
        return this.wndTrasaPowrotu;
    }

    public void setWndTrasaPowrotu(String wndTrasaPowrotu) {
        this.onRXSet("wndTrasaPowrotu", this.wndTrasaPowrotu, wndTrasaPowrotu);
        this.wndTrasaPowrotu = wndTrasaPowrotu;
    }

    public String getWndFCzySzkolenie() {
        onRXGet("wndFCzySzkolenie");
        return wndFCzySzkolenie;
    }

    public void setWndFCzySzkolenie(String wndFCzySzkolenie) {
        onRXSet("wndFCzySzkolenie", this.wndFCzySzkolenie, wndFCzySzkolenie);
        final boolean eq = eq(this.wndFCzySzkolenie, wndFCzySzkolenie);
        this.wndFCzySzkolenie = wndFCzySzkolenie;
        if (!eq) {
            przeliczDomyslneZF();
        }
    }

    public String getWndUwagi() {
        onRXGet("wndUwagi");
        return wndUwagi;
    }

    public void setWndUwagi(String wndUwagi) {
        onRXSet("wndUwagi", this.wndUwagi, wndUwagi);
        this.wndUwagi = wndUwagi;
    }

    public List<DeltProjekty> getDeltProjektys() {
        onRXGet("deltProjektys");
        return deltProjektys;
    }

    public void setDeltProjektys(List<DeltProjekty> deltProjektys) {
        onRXSet("deltProjektys", this.deltProjektys, deltProjektys);
        this.deltProjektys = deltProjektys;
    }

    public List<DeltKalkulacje> getDeltKalkulacjes() {
        onRXGet("deltKalkulacjes");
        return deltKalkulacjes;
    }


    public void clearDeltKalkulacjes() {
        final List<DeltKalkulacje> l = this.getDeltKalkulacjes().stream().collect(Collectors.toList());
        l.stream().forEach(k -> this.removeDeltKalkulacje(k));
//		for(Object k : this.getDeltKalkulacjes().toArray()) {
//			this.removeDeltKalkulacje((DeltKalkulacje)k);
//		}
    }

    /**
     * @param deltKalkulacjes the deltKalkulacjes to set
     */
    public void setDeltKalkulacjes(List<DeltKalkulacje> deltKalkulacjes) {
        onRXSet("deltKalkulacjes", this.deltKalkulacjes, deltKalkulacjes);
        this.deltKalkulacjes = deltKalkulacjes;
    }

    public String getWndFPryw() {
        onRXGet("wndFPryw");
        return wndFPryw;
    }

    public void setWndFPryw(String wndFPryw) {
        onRXSet("wndFPryw", this.wndFPryw, wndFPryw);
        this.wndFPryw = wndFPryw;
    }

    /**
     * @return the wndFBezkosztowa
     */
    public String getWndFBezkosztowa() {
        onRXGet("wndFBezkosztowa");
        return wndFBezkosztowa;
    }

    /**
     * @param wndFBezkosztowa the wndFBezkosztowa to set
     */
    public void setWndFBezkosztowa(String wndFBezkosztowa) {
        onRXSet("wndFBezkosztowa", this.wndFBezkosztowa, wndFBezkosztowa);
        final boolean eq = eq(this.wndFBezkosztowa, wndFBezkosztowa);
        this.wndFBezkosztowa = wndFBezkosztowa;
        if (!eq && "N".equals(wndFBezkosztowa)) {
            this.setWndFZaliczka("N");//??? usunZaliczkiKalk(this.kalkulacjaWniosek); ???
        }
    }

    public String getWndFProjektowa() {
        onRXGet("wndFProjektowa");
        return wndFProjektowa;
    }


    public void setWndFProjektowa(String wndFProjektowa) {
        onRXSet("wndFProjektowa", this.wndFProjektowa, wndFProjektowa);
        final boolean eq = eq(this.wndFProjektowa, wndFProjektowa);
        this.wndFProjektowa = wndFProjektowa;

        if (wndFProjektowa.equals("N") && this.getWndProjKod() != null) {
            this.setWndProjKod(null);
        }

        if (!eq) {
            przeliczDomyslneZF();
        }
    }

    public String getWndFKonferencyjna() {
        onRXGet("wndFKonferencyjna");
        return wndFKonferencyjna;
    }

    public void setWndFKonferencyjna(String wndFKonferencyjna) {
        onRXSet("wndFKonferencyjna", this.wndFKonferencyjna, wndFKonferencyjna);
        final boolean eq = eq(this.wndFKonferencyjna, wndFKonferencyjna);
        this.wndFKonferencyjna = wndFKonferencyjna;
        if (!eq) {
            przeliczDomyslneZF();
        }
    }

    public String getWndZalFormaWyplaty() {
        onRXGet("wndZalFormaWyplaty");
        return wndZalFormaWyplaty;
    }

    public void setWndZalFormaWyplaty(String wndZalFormaWyplaty) {
        onRXSet("wndZalFormaWyplaty", this.wndZalFormaWyplaty, wndZalFormaWyplaty);
        this.wndZalFormaWyplaty = wndZalFormaWyplaty;
    }

//	public DelStawkiDelegacji getDelStawkiDelegacji() {
//		return delStawkiDelegacji;
//	}
//
//	public void setDelStawkiDelegacji(DelStawkiDelegacji delStawkiDelegacji) {
//		this.delStawkiDelegacji = delStawkiDelegacji;
//	}

    public Long getWndStdId() {
        onRXGet("wndStdId");
        return wndStdId;
    }

    public void setWndStdId(Long wndStdId) {
        onRXSet("wndStdId", this.wndStdId, wndStdId);
        final boolean eq = eq(this.wndStdId, wndStdId);
        this.wndStdId = wndStdId;
        if (!eq) { //poz zmianie typu/rodzaju stawek przeladowac rodzaje kosztów i ew. przeliczyc (lepiej wyczyscici) kalk. wstępną...
            usunRodzajeKosztowKalkWnk();
            wczytajRodzajeKosztowKalkWnk();
            //TODO sprawdzic / przepisać lub wywalic potencjalnie niebezpieczny kod DelegacjaMFBean.onTypDietChanged  (.... programowanie formatkowe....) ....
        }
    }


    public void usunRodzajeKosztowKalkWnk() {
        if (this.getKalkulacjaWniosek() == null) return;
        this.getKalkulacjaWniosek().clearAllDeltPozycjeKalkulacji();
    }

    public void wczytajRodzajeKosztowKalkWnk() {
        if (this.getKalkulacjaWniosek() == null) return;

        //dodajemy pozycje, ktorych moze nie byc na kalkulacji WNK
        final List<Long> pklTposIdLst = this.getKalkulacjaWniosek().getDeltPozycjeKalkulacjis().stream().map(p -> p.getPklTpozId()).collect(Collectors.toList());
        for (DataRow dr : this.getWniosek().getRodzajeKosztow().getData()) {
            final Long tpozId = dr.getAsLong("TPOZ_ID");
            if (!pklTposIdLst.contains(tpozId)) {
                this.getKalkulacjaWniosek().addWniosekPkl(tpozId);
            }
        }
    }


    public String getWndFZaliczka() {
        onRXGet("wndFZaliczka");
        return wndFZaliczka;
    }

    public void setWndFZaliczka(String wndFZaliczka) {
        onRXSet("wndFZaliczka", this.wndFZaliczka, wndFZaliczka);
        final boolean eq = eq(this.wndFZaliczka, wndFZaliczka);
        this.wndFZaliczka = wndFZaliczka;

        final DeltKalkulacje wnk = this.getKalkulacjaWniosek();
        if (!eq && wnk != null) {
            if ("N".equals(wndFZaliczka)) {
                wnk.clearAllZaliczki();
            } else if (wnk.getDelZaliczki().isEmpty()) {
                final DelZaliczki z = wnk.addNewDeltZaliczki();
                if (isKrajowa())
                    z.setDkzWalId(1L);
                else {
                    if (!this.getDeltCeleDelegacjis().isEmpty()) {
                        final Long krId = this.getDeltCeleDelegacjis().get(0).getCdelKrId();
                        final Long walId = getWalutaWgKraju(krId);
                        if (walId != null) z.setDkzWalId(walId);
                    }
                }
            }
        }

        //if ("T".equals(this.wndFZaliczka)){
        // z jakiegos powodu po dodaniu PKL na wniosku (cz I) i kliknieciu w f5 i trzeba go odtworzyc...
        // scenariusz repordukcji - nowa del. kraj... rodzaje kosztów [+] ... dodajemy dowolny wiersz ...
        // -> zaliczki [Czy zaliczka = T ] / F5 / znika zaliczka ...
        // }
    }

    public String getWndFJednaWaluta() {
        this.onRXGet("wndFJednaWaluta");
        return wndFJednaWaluta;
    }

    public void setWndFJednaWaluta(String wndFJednaWaluta) {
        onRXSet("wndFJednaWaluta", this.wndFJednaWaluta, wndFJednaWaluta);
        this.wndFJednaWaluta = wndFJednaWaluta;
    }

    public Long getWndNumerNr() {
        onRXGet("wndNumerNr");
        return wndNumerNr;
    }

    public void setWndNumerNr(Long wndNumerNr) {
        onRXSet("wndNumerNr", this.wndNumerNr, wndNumerNr);
        this.wndNumerNr = wndNumerNr;
    }

    public List<DelAngaze> getDelAngazes() {
        onRXGet("delAngazes");
        return delAngazes;
    }

    public void setDelAngazes(List<DelAngaze> delAngazes) {
        onRXSet("delAngazes", this.delAngazes, delAngazes);
        this.delAngazes = delAngazes;
    }

    public DelAngaze addDelAngaze(DelAngaze delAngaze) {
        onRXSet("delAngazes", null, this.delAngazes);
        getDelAngazes().add(delAngaze);
        delAngaze.setDeltWnioskiDelegacji(this);

        return delAngaze;
    }

    public DelAngaze removeDelAngaze(DelAngaze delAngaze) {
        onRXSet("delAngazes", null, this.delAngazes);
        getDelAngazes().remove(delAngaze);
        delAngaze.setDeltWnioskiDelegacji(null);

        return delAngaze;
    }

    public String getWndFMiejscowoscPobyt() {
        onRXGet("wndFMiejscowoscPobyt");
        return wndFMiejscowoscPobyt;
    }

    public void setWndFMiejscowoscPobyt(String wndFMiejscowoscPobyt) {
        onRXSet("wndFMiejscowoscPobyt", this.wndFMiejscowoscPobyt, wndFMiejscowoscPobyt);
        this.wndFMiejscowoscPobyt = wndFMiejscowoscPobyt;
    }

    public String getWndMiejsceDocelowe() {
        onRXGet("wndMiejsceDocelowe");
        return wndMiejsceDocelowe;
    }

    public void setWndMiejsceDocelowe(String wndMiejsceDocelowe) {
        onRXSet("wndMiejsceDocelowe", this.wndMiejsceDocelowe, wndMiejsceDocelowe);
        this.wndMiejsceDocelowe = wndMiejsceDocelowe;
    }

    public String getWndFCzySwiadczNiestd() {
        onRXGet("wndFCzySwiadczNiestd");
        return wndFCzySwiadczNiestd;
    }

    public void setWndFCzySwiadczNiestd(String wndFCzySwiadczNiestd) {
        onRXSet("wndFCzySwiadczNiestd", this.wndFCzySwiadczNiestd, wndFCzySwiadczNiestd);
        this.wndFCzySwiadczNiestd = wndFCzySwiadczNiestd;
    }

    public String getWndFKonfKosztowa() {
        onRXSet("wndFKonfKosztowa", this.wndFKonfKosztowa, wndFKonfKosztowa);
        return wndFKonfKosztowa;
    }

    public void setWndFKonfKosztowa(String wndFKonfKosztowa) {
        onRXGet("wndFKonfKosztowa");
        this.wndFKonfKosztowa = wndFKonfKosztowa;
    }

//	public SqlBean getSql() {
//		return sql;
//	}
//
//	public void setSql(SqlBean sql) {
//		this.sql = sql;
//	}

    public Long getWndSkId() {
        onRXGet("wndSkId");
        return wndSkId;
    }

    public void setWndSkId(Long wndSkId) {
        onRXSet("wndSkId", this.wndSkId, wndSkId);
        final boolean eq = eq(this.wndSkId, wndSkId);
        this.wndSkId = wndSkId;

        if (!eq) {//ustawic filtr oraz domyslne WnrId
            SqlDataSelectionsHandler lwZF = this.getLW("PPL_DEL_ZRODLA_FIN");
            SqlDataSelectionsHandler lwAng = this.getLW("PPL_DEL_ZAANGAZOWANIE_ZF");

            String skKod = lwZF.findAndFormat(wndSkId, "%kod_skrocony$s");
            if (lwAng.getColNames().contains("sk_kod_skrocony")) //nowa wersja LW od GG
                lwAng.setColFilter("sk_kod_skrocony", skKod, true);
            else //stara wersja
                lwAng.setColFilter("sk_kod", skKod, true);

            Long wnrId = null;
            if (lwAng.getData().size() == 1) {
                wnrId = lwAng.getData().get(0).getIdAsLong();
            }
            this.setWndWnrId(wnrId);
//			lwAng.invalidateClientComponents();
        }
    }

    public void przeliczDomyslneZF() {
//		System.out.println("przeliczDomyslneZF...");

        SqlDataSelectionsHandler lwZF = this.getLW("PPL_DEL_ZRODLA_FIN");
        if (lwZF == null) return;

        lwZF.clearFilter();


        if (1 == 1) {//potencjalna konfiguracja czy stosowac taki algorytm
            //wyliczanie prefiltra lw ZF w kodzie Java (algorytm podstawowoy dla par, dep, bz):

            //par = paragraf - np. 4420 dla zwyklej delegacji zagranicznej i 4410 dla zwyklej delegacji krajowej
            String par = "44"
                    + (isKrajowa() ? "1" : "2")
                    + (nz(this.wndProjKod).isEmpty() ? "0" : "?"); //...mozna by ustalic numer "?" wg projektu...
            lwZF.setColFilter("par", par, false); //4420 zagr (nie proj.) 4410 krajowe (nie proj.)

            //dep = departament / kod (ob_kod) - np BDG
            final DataRow obDepRow = getPrcDepartamentRow(this.wndPrcId);
            if (obDepRow != null) {
                lwZF.setColFilter("dep", obDepRow.getAsString("ob_kod"), false);//dep. pracownika

                //bza = kod budżetu zadaniowego -  np.: lwZF.setColFilter("bza", "04.02.01.01", false);//wg LW GG
                Date naDzien = this.getWndDataPowrotu();
                if (naDzien == null) naDzien = Utils.today();
                final String kodBZ = this.getLW("PPL_DEP2BZ", naDzien).findAndFormat(obDepRow.getIdAsLong(), "%bz$s");
                if (kodBZ != null)
                    lwZF.setColFilter("bza", kodBZ, false);//wg LW GG
            }
        }

        if (1 == 1) {//potencjalna konfiguracja czy stosowac algorytm PLSQL
            ustawPrefiltrZFWgPLSQL(this, null, null);
        }


        //ustaw domyslne ZF (i kaskadowo domyslny wn. o angaż)
        Long skId = null;
        if (lwZF.getData().size() == 1) { // albo to projekt i sa dokladnie dwa wiersze ?
            skId = lwZF.getData().get(0).getIdAsLong();
        }
        this.setWndSkId(skId);//setter ustawia/resetuje domyslny angaz
    }


    public void ustawPrefiltrZFWgPLSQL(DeltWnioskiDelegacji wnd, DeltKalkulacje kalk, DeltPozycjeKalkulacji pkl) {
        //dodatkowy algorytm wg PLSQL... nie jest wystarczajacy, bo nie przekazujemy chocby id pracownika, a trzeba ustalic departament itp
        //ale ten kod moze uzupelniac w/w algorytm z Java i moze tez nadpisywac jego ustalenia
        //konfiguracja prefiltra ZF wg kodu PL/SQL - z lw PPL_DEL_PREFILTR_ZF + funkcja PPADM.PPP_INTEGRACJA_FIN.DAJ_PREFILTR_DLA_ZF
        // SELECT
        //   SEGMENT,
        //   PPADM.PPP_INTEGRACJA_FIN.DAJ_PREFILTR_DLA_ZF(:P_RODZAJ_DEL, SEGMENT, NVL(:P_SZKOLENIOWA,'N'), :P_PROJEKT ) COLFILTER
        // FROM (select segemnty from DUAL)

/*
-------------------------------------------------------------------------------------------------------------------
TODO: wypada przepisać PPL_DEL_PREFILTR_ZF do postaci
-------------------------------------------------------------------------------------------------------------------
with p as ( --parametry - jednowierszowo
    select
        --dane wg tworzonej(niekoniecznie zapisanej) delegacji
        :P_WND_ID               WND_ID, -- NUMBER - 0 jesli to nowa/niezapisana delegacja (lub pole WND_ID)
        :P_PRC_ID               PRC_ID, --NUMBER - id pracownika (kto jedzie)
        :P_RODZAJ_DEL           RODZAJ_DEL, --VARCHAR2(1) - Rodzaj delegacji K(krajowa) lub Z(zagraniczna)
        :P_CZY_SZKOLENIOWA      CZY_SZKOLENIOWA, --VARCHAR2(1) - Czy szkoleniowa (T/N)
        :P_CZY_KONFERENCJA      CZY_KONFERENCJA, --VARCHAR2(1) - Czy konferencja (T/N)
        :P_CZY_PROJEKT          CZY_PROJEKT, --VARCHAR2(1) - Czy projekt (T/N)
        :P_CZY_GRUPOWA          CZY_GRUPOWA, --VARCHAR2(1) - Czy grupowa (T/N)
        :P_PROJEKT_KOD          PROJEKT_KOD, --VARCHAR2 - opc. kod projektu (kod klasyfikacji )
        :P_WND_WND_ID           WND_WND_ID, --NUMBER - opjonalne id gelegacji powiazanej (czyli wnd_id grupowej delegacji przewodniczacego)
        :P_DATA_OD              data_od, --DATE - od kiedy planowany jest wyjazd (wg Cz I.)
        :P_DATA_DO              data_do,  --DATE - do kiedy planowany jest wyjazd (wg Cz I.)

        --info o kalkulacji
		:P_KAL_ID				KAL_ID, --NUMBER - 0 jesli nowa/niezapisana lub pole KAL_ID
		:P_KAL_RODZAJ			KAL_RODZAJ, --VARCHA2 - 'WNK'(wniosek), 'WST'(wstępna), 'ROZ'(rozliczająca)

        --a jesli dla wybranych PKL trzeba bedzie inaczej podpowiadac ZF ...
        :P_PKL_ID                   PKL_ID,  --NUMBER - 0 jesli to nowa/niezapisana pozycja (lub pole PKL_ID)
        :P_PKL_TPOZ_ID              TPOZ_ID,  --NUMBER - opcjonalne - identyfikator typu pozycji kosztowej
        :P_PKL_WSL_FORMA_PLATNOSCI  WSL_FORMA_PLATNOSCI,  --VARCHAR2(2) ('01', '02', ... '05') - symbol formy patnosci wg slownika DEL_FORMY_PLATNOSCI
        :P_PKL_NR_UMOWY             MR_UMOWY,  --VARCHAR2 - docelowo nr umowy z pozycji kosztowej (do ustalenia z GG)
        :P_PKL_WAL_ID               WAL_ID,  --NUMBER - id waluty z PKL
        :P_PKL_KR_ID                KR_ID,  --NUMBER - id kraju z PKL

        -- ... co jeszcze moze miec wplyw na wybor ZF (i ANG) ???
        0 zzz
    from dual)

SELECT 'ZZZ' SEGMENT, '' COLFILTER  FROM p
-- w kolejnych union'ach dowolna funkcja PL/SQL lub subselect moze wylcizac wartosc COLFITLER dla okreslongeo segemntu
--union SELECT  'ROZ',  PPADM.PPP_INTEGRACJA_FIN.DAJ_PREFILTR_DLA_ZF(p.RODZAJ_DEL, 'ROZ', NVL(p.CZY_SZKOLENIOWA,'N'), p.PROJEKT_KOD ) COLFILTER  FROM p
--union SELECT  'PAR',  PPADM.PPP_INTEGRACJA_FIN.DAJ_PREFILTR_DLA_ZF(p.RODZAJ_DEL, 'PAR', NVL(p.CZY_SZKOLENIOWA,'N'), p.PROJEKT_KOD ) COLFILTER  FROM p
--union SELECT  'PRJ',  PPADM.PPP_INTEGRACJA_FIN.DAJ_PREFILTR_DLA_ZF(p.RODZAJ_DEL, 'PRJ', NVL(p.CZY_SZKOLENIOWA,'N'), p.PROJEKT_KOD ) COLFILTER  FROM p
--union SELECT  'TYT',  PPADM.PPP_INTEGRACJA_FIN.DAJ_PREFILTR_DLA_ZF(p.RODZAJ_DEL, 'TYT', NVL(p.CZY_SZKOLENIOWA,'N'), p.PROJEKT_KOD ) COLFILTER  FROM p
--union SELECT  'BZA',  PPADM.PPP_INTEGRACJA_FIN.DAJ_PREFILTR_DLA_ZF(p.RODZAJ_DEL, 'BZA', NVL(p.CZY_SZKOLENIOWA,'N'), p.PROJEKT_KOD ) COLFILTER  FROM p
union SELECT  'KME',  PPADM.PPP_INTEGRACJA_FIN.DAJ_PREFILTR_DLA_ZF(p.RODZAJ_DEL, 'KME', NVL(p.CZY_SZKOLENIOWA,'N'), p.PROJEKT_KOD ) COLFILTER  FROM p
order by segment
-------------------------------------------------------------------------------------------------------------------
*/

        String wndFCzySzkolenie = wnd.getWndFCzySzkolenie();
        if (wndFCzySzkolenie == null) wndFCzySzkolenie = "N";
        SqlDataSelectionsHandler lwPrefiltrZF = wnd.getLW("PPL_DEL_PREFILTR_ZF", (isKrajowa() ? "K" : "Z"), wndFCzySzkolenie, wnd.getWndProjKod());

        for (DataRow r : lwPrefiltrZF.getData()) {
            wnd.getLW("PPL_DEL_ZRODLA_FIN").setColFilter((String) r.get("segment"), nz(r.getAsString("colfilter")), false);
        }
    }


//	public void aktualizujWndSkId(javax.faces.event.ValueChangeEvent e) {
//		if(e.getOldValue() != null && e.getNewValue() != null)
//		if(!(e.getOldValue().equals(e.getNewValue()))) {
//			List<DeltKalkulacje> kalkulacjeBezRoz = this.getDeltKalkulacjes().stream().filter(k -> !(k.getKalRodzaj().equals("ROZ"))).collect(Collectors.toList());
//			for(DeltKalkulacje kalk : kalkulacjeBezRoz) {
//				for(DeltPozycjeKalkulacji poz : kalk.getDeltPozycjeKalkulacjis()) {
//					for(DeltZrodlaFinansowania zf : poz.getDeltZrodlaFinansowanias()) {
//						if(e.getOldValue().equals(zf.getPkzfSkId()))
//						zf.setPkzfSkId((Long) e.getNewValue());
//					}
//				}
//			}
//			User.info("Zmieniono domyślne źródło finansowania. Dla pozycji kalkulacji z wcześniej wybranymi źródłami, wartości zostaną zamienione na nowe.");
//		}
//	}

    public Long getWndWnrId() {
        onRXGet("wndWnrId");
        return wndWnrId;
    }

    public void setWndWnrId(Long wndWnrId) {
        onRXSet("wndWnrId", this.wndWnrId, wndWnrId);
        this.wndWnrId = wndWnrId;
    }


//	public void aktualizujWndWnrId(javax.faces.event.ValueChangeEvent e) {
//		if(e.getOldValue() != null && e.getNewValue() != null)
//		if(!(e.getOldValue().equals(e.getNewValue()))) {
//		List<DeltKalkulacje> kalkulacjeBezRoz = this.getDeltKalkulacjes().stream().filter(k -> !(k.getKalRodzaj().equals("ROZ"))).collect(Collectors.toList());
//		for(DeltKalkulacje kalk : kalkulacjeBezRoz) {
//			for(DeltPozycjeKalkulacji poz : kalk.getDeltPozycjeKalkulacjis()) {
//				for(DeltZrodlaFinansowania zf : poz.getDeltZrodlaFinansowanias()) {
//					for(DelAngaze an : zf.getDelAngazes()) {
//						if(e.getOldValue().equals(an.getAngWnrId()))
//						an.setAngWnrId((Long) e.getNewValue());
//					}
//				}
//			}
//		}
//			User.info("Zmieniono domyślne wniosek o zaangażowanie. Dla pozycji kalkulacji z wcześniej wybranym wnioskiem o angaż,"
//				+ " wartości zostaną zamienione na nowe.");
//		}
//	}

    public String getWndFDomyslneZf() {
        onRXGet("wndFDomyslneZf");
        return wndFDomyslneZf;
    }

    public void setWndFDomyslneZf(String wndFDomyslneZf) {
        onRXSet("wndFDomyslneZf", this.wndFDomyslneZf, wndFDomyslneZf);
        this.wndFDomyslneZf = wndFDomyslneZf;
    }

    public String getWndProjKod() {
        onRXGet("wndProjKod");
        return wndProjKod;
    }

    public void setWndProjKod(String wndProjKod) {
        onRXSet("wndProjKod", this.wndProjKod, wndProjKod);
        final boolean eq = eq(this.wndProjKod, wndProjKod);
        this.wndProjKod = wndProjKod;
        if (!eq) {
            przeliczDomyslneZF();
        }
    }

/*	public String getWndTypDiety() {
		return wndTypDiety;
	}

	public void setWndTypDiety(String wndTypDiety) {
		this.wndTypDiety = wndTypDiety;
	}*/


    public void przeliczWndStdId() {
        String stdKod = "MPiPS";
        try (Connection conn = JdbcUtils.getConnection()) {
            if (this.getWndRodzaj() == 2L) {

                Long typPracownika = JdbcUtils.sqlFnLong(conn, "PPADM.CZY_FUNKCJONARIUSZ_CELNY", this.getWndPrcId());

                if (typPracownika == null || typPracownika == -1L) {
                    User.alert("Błąd przy wczytywaniu domyślnej stawki diet dla wybranego pracownika");
                    this.setWndStdId(0L);
                    return;
                }

                if (typPracownika == 1L && this.getWndRodzaj() == 2L) stdKod = "FCS";
            }
            BigDecimal oX = (BigDecimal) JdbcUtils.sqlExecScalarQuery(conn, "select std_id from PPT_DEL_STAWKI_DELEGACJI where STD_KOD_STAWKI = ?", stdKod);
            if (oX != null)
                this.setWndStdId(oX.longValue());
            else {
                User.alert("Typ stawek: nie zdefiniowano stawki domyślnej z kodem '%1$s'.", stdKod);
            }
        } catch (Exception e) {
            User.alert("Błąd przy wczytywaniu domyślnej stawki diet: " + e.getMessage());
        }
    }


//	public Long WalutaWgKraju(Long krajId){
//		if(krajId==null)
//			return 1L;
//
//		DataRow r = this.getLW("PPL_KRAJE_WALUTY_ALL").find(krajId);
//
//		if(r == null) {
//			this.onRXGet("isKrajowa() ? 1L : 50000L");
//			return this.isKrajowa() ? 1L : 50000L;
//		}
//
//		return r.getAsLong("WAL_ID");
//	}

    public Long getWndDokId() {
        onRXGet("wndDokId");
        return wndDokId;
    }

    public void setWndDokId(Long wndDokId) {
        onRXSet("wndDokId", this.wndDokId, wndDokId);
        this.wndDokId = wndDokId;
    }

    public List<DelZaliczki> getDokumentyDKZwNZ() {
        ArrayList<DelZaliczki> ret = new ArrayList<>();

        for (DeltKalkulacje kal : this.getDeltKalkulacjes()) {
            ret.addAll(kal.getDelZaliczki().stream().filter(z -> z.getDkzDokId() != null).collect(Collectors.toList()));
        }

        return ret;
    }


    public List<String> walutySymbole() {
        List<String> ret = new ArrayList<>();
        this.getDeltKalkulacjes().stream().forEach(k -> ret.addAll(k.walutySymbole()));
        return ret.stream().filter(w -> w != null).distinct().sorted().collect(Collectors.toList());
    }


    public void generujRaportyJakoZalacznikiPdfDlaDokID(Object dokId, boolean czyRaportPolecenia, boolean czyRaportRozliczenia, boolean czyZalacznikDlaNZ_DEL_FK5) throws SQLException, IOException, JRException, DocumentException {
        //METODA POLIMORFICZNA
    }

    public void zapisz() {
        zapisz(new HibernateContext());
    }


    public void zapisz(HibernateContext h) {
//		if(!this.validate())
//			return;

        HibernateContext.saveOrUpdate(this);
    }


    public Boolean validateBeforeSaveDelegacja() {
        ArrayList<String> inval = new ArrayList<String>();
        String wymagalne = "";

        if (nz(this.getWndOpis()).isEmpty())
            wymagalne += "Opis celu delegacji; ";

        if (!this.getDeltCeleDelegacjis().stream().filter(c -> c.getCdelKrId() == null || c.getCdelMiejscowosc().isEmpty()).collect(Collectors.toList()).isEmpty())
            wymagalne += "Cel delgacji (kraj i miejscowość); ";

        if (this.getWndDataWyjazdu() == null || this.getWndDataPowrotu() == null)
            wymagalne += "Termin delegacji (daty od /do); ";

        if (!this.getDeltSrodkiLokomocjis().stream().filter(sl -> (sl.getSlokSrodekLokomocji().isEmpty())).collect(Collectors.toList()).isEmpty())
            wymagalne += "Środek lokomocji; ";

        if ("T".equals(this.getWndFWyjazdZDomu())
                && (this.getWndPrywDataPowrotu() == null || this.getWndPrywDataWyjazdu() == null))
            wymagalne += "Termin podróży prywatnej (daty od /do); ";

        //przekopiowane z mfbean walidowanie zapraszających
        this.sprawdzZapraszajacych(inval);
        //termin delegacji, usuwam warunek wndId == 0 z mfbean
        if (this.getWndDataPowrotu() != null)
            if (nz(this.getWndId()) == 0 && this.getWndDataPowrotu().before(today())) {
                User.warn("Termin delegacji jest datą przeszłą");
            }

        if (this.getWndDataWyjazdu() != null && this.getWndDataPowrotu() != null &&
                this.getWndDataWyjazdu().after(this.getWndDataPowrotu())) {
            inval.add("Data wyjazdu nie może być większa niż data powrotu.");
        }

        if (this.getWndPrywDataWyjazdu() != null && this.getWndPrywDataPowrotu() != null) {
            if (this.getWndPrywDataWyjazdu().after(this.getWndPrywDataPowrotu())) {
                inval.add("Data prywatnego wyjazdu nie może być większa niż data powrotu.");
            }
        }

        for (DeltCeleDelegacji c : this.getDeltCeleDelegacjis()) {
            if (c.getCdelKrId() == null)
                wymagalne = wymagalne + "Kraj; ";
            if (c.getCdelMiejscowosc() == null || c.getCdelMiejscowosc().trim().equals(""))
                wymagalne = wymagalne + "Miejscowość docelowa; ";
        }

        if ("T".equals(this.getWndFWyjazdZDomu())
                && (this.getWndPrywDataPowrotu() == null || this.getWndPrywDataWyjazdu() == null))
            wymagalne = wymagalne + "Termin podróży prywatnej (daty od /do); ";


        List<DeltSrodkiLokomocji> listaSrodkiLokomocji = this.getDeltSrodkiLokomocjis().stream().filter(
                sl -> (sl.getSlokSrodekLokomocji().isEmpty())).collect(Collectors.toList());

        if (this.getDeltSrodkiLokomocjis() == null || this.getDeltSrodkiLokomocjis().isEmpty()
                || listaSrodkiLokomocji.size() > 0)
            wymagalne = wymagalne + "Środek lokomocji; ";

        if (!wymagalne.isEmpty()) {
            inval.add("Następujące pola są wymagalne: " + wymagalne);
        }

        if (!inval.isEmpty()) {
            for (String msg : inval) {
                User.alert(msg);
            }
        }


        return inval.isEmpty();
    }


    public DeltKalkulacje getKalkulacjaWniosek() {
        getDeltKalkulacjes().stream().filter(k -> eq("WNK", k.getKalRodzaj())).findFirst().orElse(null);
        return this.getDeltKalkulacjes().stream().filter(k -> eq("WNK", k.getKalRodzaj())).findFirst().orElse(null);
    }

    public DeltKalkulacje getKalkulacjaWstepna() {
        getDeltKalkulacjes().stream().filter(k -> eq("WST", k.getKalRodzaj())).findFirst().orElse(null);
        return this.getDeltKalkulacjes().stream().filter(k -> eq("WST", k.getKalRodzaj())).findFirst().orElse(null);
    }

    public DeltKalkulacje getKalkulacjaRozliczenie() {
        getDeltKalkulacjes().stream().filter(k -> eq("ROZ", k.getKalRodzaj())).findFirst().orElse(null);
        return this.getDeltKalkulacjes().stream().filter(k -> eq("ROZ", k.getKalRodzaj())).findFirst().orElse(null);
    }


    @Transient
    Boolean czyDomyslnieDietaHotelowa;

    //leniuch jesli ma na cz. I  jest pkl/RH to fasle, WPP true
    public Boolean getCzyDomyslnieDietaHotelowa() {
        onRXGet("czyDietaHotelowa");
        if (czyDomyslnieDietaHotelowa == null && this.getKalkulacjaWniosek() != null && this.getKalkulacjaWniosek().getDeltPozycjeKalkulacjis() != null) {
            long cntRH = this.getKalkulacjaWniosek().getDeltPozycjeKalkulacjis().stream()
                    .filter(p -> eq("RH", p.getKategoria()) && eq("T", p.getPklFWniosek()))
                    .count();
            this.setCzyDomyslnieDietaHotelowa(cntRH <= 0);//domyslnie true, ale jesli jest na cz. I jakikolwiek ryczałt hotelowy to false
        }
        return czyDomyslnieDietaHotelowa;
    }


    public void setCzyDomyslnieDietaHotelowa(Boolean czyDomyslnieDietaHotelowa) {
        onRXSet("czyDietaHotelowa", this.czyDomyslnieDietaHotelowa, czyDomyslnieDietaHotelowa);
        this.czyDomyslnieDietaHotelowa = czyDomyslnieDietaHotelowa;
    }


    @Override
    public DeltWnioskiDelegacji getWniosek() {
        return this;
    }

    public abstract SqlDataSelectionsHandler getLwTypyPozycji();


    public Long getTpozIdByKtgPozycji(String ktgPozycji) {

        if (ktgPozycji == null || ktgPozycji.isEmpty()) return null;

        SqlDataSelectionsHandler typyPozycji = this.getLwTypyPozycji();
        if (typyPozycji == null) {
            return null;
        }
        for (DataRow r : typyPozycji.getData()) {
            if (eq(ktgPozycji, r.get("tpoz_kategoria"))) {
                return nz(r.getAsLong("tpoz_id"));
            }
        }

        return null;
    }


    public abstract SqlDataSelectionsHandler getRodzajeKosztow();


    @Override
    public boolean validate() {
        boolean ret = true
                & this.validateBeforeSaveDelegacja();

        for (DeltKalkulacje k : this.getDeltKalkulacjes()) {
            ret = ret & k.validate();
        }

//		for (DeltCeleDelegacji c : this.getDeltCeleDelegacjis()) {
//			ret = ret & c.validate(); //TODO: DeltCeleDelegacji -> extends ModelBase
//		}

        return ret;
    }

    private void sprawdzZapraszajacych(ArrayList<String> inval) {
        if (this.getDeltZapraszajacys().size() > 1)
            for (DeltZapraszajacy zapr : this.getDeltZapraszajacys()) {
                try {
                    if (zapr.getZaprOpis() == null || zapr.getZaprOpis().length() == 0) {
                        inval.add("Lista zapraszających zawiera pustą pozycję");
                    }
                } catch (InstantiationException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (SQLException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
    }

    public SqlDataSelectionsHandler getLwRozliczeniePklNzWgFifo() {
        SqlDataSelectionsHandler lw = this.getLW("PPL_DEL_ROZLICZ_FIFO")
                .setPreQuery(""
                        + "begin "
                        + " ppadm.ppp_global.rozlicz_fifo_wnd_id := ? ; "
                        + "end; ", this.getWndId());

        return lw;
    }


    @Override
    public void beforeCzynnoscObiegu() {
        //tu celowo nie afterCzynnoscObiegu bo moze byc tak, ze podczas wykonyuwania poleci wyjatek bo ktos inny walsnie utw. dkz_dok_id i trzeba tylko odsiwezyc dane
        //uniewazniamy dane LW z danymi i statusem dokumentów NZ (chodzi o to aby wczytac dane jakie zapisala akcja w obiegu) ...
        this.getLW("PPL_DEL_DANE_KSIEGOWE_KAL", -1L).resetTs();
        this.getLW("PPL_DEL_KURS_DKZ_DOK", -1L).resetTs();
    }


    public abstract void ustawStawkeWaluteDietyPKL(DeltPozycjeKalkulacji p, Date pobytOd);

    public abstract void ustawStawkeiWaluteDietyHotel(DeltPozycjeKalkulacji p, Date pobytOd);


    public void generujNoteKsg() {
        if (this.wndId==0l
                || this.getKalkulacjaRozliczenie()==null
                || this.getKalkulacjaRozliczenie().getDeltPozycjeKalkulacjisGotowkaPrzelew()==null
                || this.getKalkulacjaRozliczenie().getDeltPozycjeKalkulacjisGotowkaPrzelew().isEmpty()
                || !this.validate()
        ) {
            User.warn("Delegacja nie zosatła zapisana lub nie ma danych do generowania noty księgowej.");
            return;
        }

        for (DeltPozycjeKalkulacji pkl : this.getKalkulacjaRozliczenie().getDeltPozycjeKalkulacjisGotowkaPrzelew()) {
            long cnt = pkl.getDeltZrodlaFinansowanias().stream()
                    .filter(  zf -> zf.getDelZaliczki() == null
                             || eq(0L, nz(zf.getDelZaliczki().getDkzDokId()))
                    ).count();
            if (cnt>0){
                User.warn("Nie wszystkie kwoty z angaży zostały zrealizowane/rozliczone w NZ");
                return;
            }
        }


        String functionName = isKrajowa() ? "PPP_INTEGRACJA_FIN.GENERUJ_DEK" : "PPP_INTEGRACJA_FIN.GENERUJ_DEZ";

        try (HibernateContext h = new HibernateContext()) {

            this.saveOrUpdate(h, false);

            final Connection conn = h.getConnection();
            Long dokId = JdbcUtils.sqlFnLong(conn, functionName, this.wndId);
            HibernateContext.refresh(h, this);
            this.wndDokId = dokId; //gdyby refresh nie wczytal zmian
            onRXSet("wndDokId", null, dokId);
            User.info("EGR/FK: zapisano dokument " + dokId);



        } catch (Exception ex) {
            User.alert(ex);
        }

        this.getLW("PPL_DEL_NOTA_PSK", nz(this.wndDokId)).resetTs();
        this.getLW("PPL_DEL_NOTA_POZYCJE", nz(this.wndDokId)).resetTs();

    }


}
