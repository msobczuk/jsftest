package com.comarch.egeria.pp.delegacje.model;

import java.io.Serializable;
import java.sql.SQLException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;

import com.comarch.egeria.pp.data.model.ModelBase;

/**
 * The persistent class for the PPT_DEL_ZAPRASZAJACY database table.
 * 
 */
@Entity
@Table(name="PPT_DEL_ZAPRASZAJACY", schema="PPADM")
@NamedQuery(name="DeltZapraszajacy.findAll", query="SELECT d FROM DeltZapraszajacy d")
public class DeltZapraszajacy extends ModelBase implements Serializable, Cloneable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_DEL_ZAPRASZAJACY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_DEL_ZAPRASZAJACY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_DEL_ZAPRASZAJACY")	
	@Column(name="ZAPR_ID")
	private long zaprId;

	@Column(name="ZAPR_OPIS")
	private String zaprOpis;

	@Column(name="ZAPR_WND_ID", insertable=false, updatable=false)
	private Long zaprWndId;
	
	@Column(name="ZAPR_F_INNE")
	private String zaprFInne;
	
	//bi-directional many-to-one association to DeltWnioskiDelegacji
	@ManyToOne
	@JoinColumn(name="ZAPR_WND_ID")
	private DeltWnioskiDelegacji deltWnioskiDelegacji;

	
	public DeltZapraszajacy clone(){
		
		DeltZapraszajacy zapr = new DeltZapraszajacy();
	
		zapr.zaprId = zaprId;
		zapr.zaprOpis = zaprOpis;
		zapr.zaprWndId = zaprWndId;
		zapr.zaprFInne = zaprFInne;
		zapr.deltWnioskiDelegacji = null;
		
		
		return zapr;
		
	}
	
	public DeltZapraszajacy() {
	}
	

	public long getZaprId() {
		this.onRXGet("zaprId");
		return this.zaprId;
	}

	public void setZaprId(long zaprId) {
		this.onRXSet("zaprId", this.zaprId, zaprId);
		this.zaprId = zaprId;
	}

	public String getZaprOpis() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
		this.onRXGet("zaprOpis");
		return this.zaprOpis;
	}

	public void setZaprOpis(String zaprOpis) {
//		if(!zaprOpis.isEmpty())
		this.onRXSet("zaprOpis", this.zaprOpis, zaprOpis);
		this.zaprOpis = zaprOpis;
	}

	
	public Long getZaprWndId() {
		this.onRXGet("zaprWndId");
		return this.zaprWndId;
	}

	public void setZaprWndId(Long zaprWndId) {
		this.onRXSet("zaprWndId", this.zaprWndId, zaprWndId);
		this.zaprWndId = zaprWndId;
	}

	/**
	 * @return the deltWnioskiDelegacji
	 */
	public DeltWnioskiDelegacji getDeltWnioskiDelegacji() {
		this.onRXGet("deltWnioskiDelegacji");
		return this.deltWnioskiDelegacji;
	}

	/**
	 * @param deltWnioskiDelegacji the deltWnioskiDelegacji to set
	 */
	public void setDeltWnioskiDelegacji(DeltWnioskiDelegacji deltWnioskiDelegacji) {
		this.onRXSet("deltWnioskiDelegacji", this.deltWnioskiDelegacji, deltWnioskiDelegacji);
		this.deltWnioskiDelegacji = deltWnioskiDelegacji;
	}

	public String getZaprFInne() {
		this.onRXGet("zaprFInne");
		return this.zaprFInne;
	}

	public void setZaprFInne(String zaprFInne) {
		this.onRXSet("zaprFInne", this.zaprFInne, zaprFInne);
		this.zaprFInne = zaprFInne;
	}

}
