package com.comarch.egeria.pp.listyObecnosci;

import java.time.LocalDate;

import static com.comarch.egeria.Utils.lpad;

public class DzMc{
    LocalDate date;



    Boolean zaznaczony = false;

    public Boolean getZaznaczony() {
        return zaznaczony;
    }

    public void setZaznaczony(Boolean zaznaczony) {
        this.zaznaczony = zaznaczony;
    }


    public LocalDate getDate() {
        return date;
    }

    public String getDzMcNr(){
        return lpad(date.getDayOfMonth(),2);
    }

    public  String getDayOfWeek(){
        return date.getDayOfWeek().name();
    }


    public String[] cssClassDniTygodnia = new String[] {"", "dzt-pn", "dzt-wt", "dzt-sr", "dzt-cz", "dzt-pt", "dzt-so", "dzt-nd" };

    public String getStyleClass(){
        String ret = "dzm-"+lpad(date.getDayOfMonth(),2,'0');
        ret += " " + cssClassDniTygodnia[date.getDayOfWeek().getValue()];
        //        if (swieto) ret += " dz-swieto";

        if (zaznaczony) ret += " dz-col-zazn";
        else ret += " dz-col-nzazn";

        return ret;
    }

    public String getDzTygogniaStyleClass(){
        return  cssClassDniTygodnia[date.getDayOfWeek().getValue()];
    }

}
