package com.comarch.egeria.pp.listyObecnosci;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Named;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.lpad;
import static com.comarch.egeria.Utils.updateComponent;

//import org.primefaces.context.RequestContext;

@ManagedBean
@Named
@Scope("view")
public class ListaObecnosciMCBean extends SqlBean {
    private static final long serialVersionUID = -5806366986076184951L;
    private static final Logger log = LogManager.getLogger();

    int rok;
    String miesiac = null; //"1", "2", ... "12"
    ArrayList<DzMc> dniMc = new ArrayList<>();

    @PostConstruct
    public void init(){
        this.rok = LocalDate.now().getYear();
        this.miesiac = ""+LocalDate.now().getMonthValue();
        initDni(rok, miesiac);
    }

    public SqlDataSelectionsHandler getPplObecnosciMC(){
        SqlDataSelectionsHandler lw = this.getLW("PPL_OBECNOSCI_MC", this.rok, this.miesiac);
        return lw;
    }

    public String getOkresRokMc(){
        return this.rok + "-" + lpad(this.miesiac,2,'0');
    }


    public void initDni(int rok, String miesiac){
        ArrayList<DzMc> ret = new ArrayList<>();
        int mc = Integer.parseInt(miesiac);

        LocalDate d1 = LocalDate.of(rok, mc, 1);
        for (int i=1; i<=d1.lengthOfMonth(); i++){
            DzMc dz= new DzMc();

            dz.date = LocalDate.of(rok, mc, i);

            if (dz.date.equals(LocalDate.now()))
                dz.setZaznaczony(true);

            if (dz.date.getMonthValue()!=mc) break;
            ret.add(dz);
        }
        this.dniMc = ret;
    }


    public void ustawObecnoscDlaZazn(boolean obecny) throws SQLException {

        List<DataRow> selectedRows = this.getPplObecnosciMC().getSelectedRows();
        List<DzMc> selectedColumns = this.dniMc.stream().filter(d -> d.zaznaczony).collect(Collectors.toList());
        if (selectedRows.isEmpty()){
            User.warn("Nie zaznaczono żadnego wiersza.");
        }
        if (selectedColumns.isEmpty()){
            User.warn("Nie zaznaczono żadnej kolumny.");
        }

        int cnt = 0;
        try (Connection conn = JdbcUtils.getConnection()){
            for (DataRow r:selectedRows) {
                //System.out.println(r.format("prc_id: %prc.prc_id$s; %prc.prc_imie$s %prc.prc_nazwisko$s (#%prc.prc_numer$s)"));
                for (DzMc dz :selectedColumns) {
                    //System.out.println(dz.date + " / " +obecny);
                    if (obecny)
                        JdbcUtils.sqlSPCall(conn, "ppadm.obecnosc_ustaw", r.getIdAsString(), dz.date, "T" );
                    else
                        JdbcUtils.sqlSPCall(conn, "ppadm.obecnosc_ustaw", r.getIdAsString(), dz.date, "N" );
                    cnt++;
                }
            }

            if (cnt>0) {
                this.getPplObecnosciMC().resetTs();
                this.getPplObecnosciMC().getData();
            }



        }
    }


    public void ustawObecnosc(String prcid, Integer dznr, Boolean obecny, String srcHtmlElId) throws SQLException {
        DataRow r = getPplObecnosciMC().find(prcid);

        DzMc dz = dniMc.get(dznr-1); //-1 bo arrayList jest indeksowana od 0

        try (Connection conn = JdbcUtils.getConnection()) {
            if (obecny) {
                JdbcUtils.sqlSPCall(conn, "ppadm.obecnosc_ustaw", r.getIdAsString(), dz.date, "T");
            }
            else {
                JdbcUtils.sqlSPCall(conn, "ppadm.obecnosc_ustaw", r.getIdAsString(), dz.date, "N");
            }
        }

        String columnName = "d" + lpad(dznr,2,'0');
        if (r.get(columnName)==null)
            r.set(columnName,"X");
        else
            r.set(columnName,null);

        updateComponent(srcHtmlElId);//RequestContext.getCurrentInstance().update(srcHtmlElId);
    }



    public ArrayList<DzMc> getDniMc() {
        return dniMc;
    }

    public void setDniMc(ArrayList<DzMc> dniMc) {
        this.dniMc = dniMc;
    }



    public int getRok() {
        return rok;
    }

    public void setRok(int rok) {
        if (!eq(this.rok, rok))
            this.getPplObecnosciMC().resetTs();
        this.rok = rok;

        initDni(this.rok, this.miesiac);
    }

    public String getMiesiac() {
        return miesiac;
    }

    public void setMiesiac(String miesiac) {
        if (!eq(this.miesiac, miesiac))
            this.getPplObecnosciMC().resetTs();
        this.miesiac = miesiac;

        initDni(this.rok, this.miesiac);
    }


    public void npMiesiac(int step){
        int m = Integer.parseInt(this.miesiac);
        m = m+step;

        if (m>12){
            m=1; rok++;
        }else if (m<1){
            m=12; rok--;
        }

        this.miesiac = ""+m;

        initDni(this.rok, this.miesiac);
        this.getPplObecnosciMC().resetTs();
    }

}
