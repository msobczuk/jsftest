package com.comarch.egeria.pp.kontrolaUprawnien;

import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.component.calendar.Calendar;
import org.primefaces.component.confirmdialog.ConfirmDialog;
import org.primefaces.component.dialog.Dialog;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.tabview.TabView;
//import org.primefaces.context.RequestContext;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.component.UIPanel;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.PreRenderComponentEvent;
import javax.faces.event.SystemEvent;
import javax.faces.event.SystemEventListener;
import java.util.ArrayList;
import java.util.Map;

public class KontrolaUprawnienComponentEventListener implements SystemEventListener  {


	
	int cnt = 0;
	@Override
	public void processEvent(SystemEvent event) throws AbortProcessingException {


		if (event instanceof PreRenderComponentEvent){
			cnt+=1;
			
			UIComponent uic = ((PreRenderComponentEvent) event).getComponent();			//UIComponent c = (UIComponent) event.getSource();
			
//			System.out.println( uic 
//					+ "\nclientId: "  + uic.getClientId()  
//					+ "\nattrs: " + formatAttrMap (uic.getAttributes()) 
//			);
			
//			if (uic instanceof UIRepeat){
//				System.out.println("UIRepeat" + formatAttrMap (uic.getAttributes()) );
//			}
//			
//			if (UINamingContainer.isCompositeComponent(uic)){
//				System.out.println("cc xhtml: "+ uic.getAttributes().get("javax.faces.application.Resource.ComponentResource"));
//				System.out.println("cc attrs: "+ uic.getClientId() + " " + formatAttrMap( uic.getAttributes() ));
//			}
			
			
			
			
			Object okulvl = uic.getAttributes().get("kulvl");
			if (okulvl!=null){
				int kulvl = (int) okulvl;
//					if (kulvl<=0) System.out.println("kulvl<=0 - " + uic.getClientId() + " " + uic);
				if (kulvl<=0 && uic.getAttributes().get("kulvl-parent")==null)
						makeRenderedFalse(uic);
				else if (kulvl<=1)
					if (!"ku-e".equals(uic.getAttributes().get("kuid")) && !"".equals(uic.getAttributes().get("kuid"))) //jesli elementu nie wykluczono z ku
						makeReadOnly(uic);
			}
			
			//Uwaga - jesli dany rodzic zostanie wyłaczony, to jego dzieci nie są renderowane (dla dzieci "ukrytego" rodzica nie ma PreRenderComponentEvent)
			//Zatem konieczny jest wyjatek, jesli wewnątrz Tab'a umieszczono element, ktory chcemy renderowac, 
			//to sam Tab nie może pozostac ukryty. Ta zmianę (odkrycie Tab'a) możemy zrealizować najpźniej na etapie renderowania TabView czy AccordionPanel...  
			if (uic instanceof TabView || uic instanceof AccordionPanel){
				for (UIComponent tab : uic.getChildren()) {
					if (tab instanceof Tab) {
						okulvl = tab.getAttributes().get("kulvl");
						if (okulvl!=null){
							int kulvl = (int) okulvl;
							
							if (kulvl<=0 && tab.getAttributes().get("kulvl-parent")==null)
								makeRenderedFalse(tab);
						}
					}
				}
			}
			
			UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
			Map<String, Object> viewMap = viewRoot.getViewMap();
			KontrolaUprawnien ku = (KontrolaUprawnien) viewMap.get("kontrolaUprawnien");
			if (ku==null)
				return;
			
			Object kubtn = uic.getAttributes().get("kubtn");
			if (kubtn!=null) {		
				ku.addKuBtn(uic);
			}	

			if (uic instanceof UIPanel) { //   && uic.getParent()!=null && UIComponent.isCompositeComponent(uic.getParent())
				String attkeys = uic.getAttributes().keySet().toString();
				if (   "[kubtn, kuid]".equals( attkeys ) ||  "[]".equals( attkeys ) ||  "[kubtn]".equals( attkeys )  ||  "[kuid]".equals( attkeys )  )
					return; //panel w bez żadnych naturalnych attrybutów sie zwykle nie renderuje i nie powinien się renderowac tylko dlatego, ze dodano klase
			}
			
			if (ku!=null && ku.user.isRenderKU() && uic.getAttributes().get("btnku")==null && !"".equals(uic.getAttributes().get("kuid"))) {
				
				if (uic.getClientId().startsWith("frmKU")||uic.getClientId().startsWith("dlgKU")||uic.getClientId().startsWith("templateBody") )
					return;
				
				
				Object styleClass = uic.getAttributes().get("styleClass");
				if (styleClass!=null){
					if (!(""+styleClass).contains("btnkujs"))
						uic.getAttributes().put("styleClass",  styleClass + " btnkujs");
				} else {
						uic.getAttributes().put("styleClass","btnkujs");
				}
				
				
				String kuid = ku.getKuid(uic);
				String opis = KontrolaUprawnien.getKuOpis(uic);
				ku.opisyHM.put(uic.getClientId(), opis);
				
				if (kuid!=null)
					ku.opisyHM.put(kuid, opis);
				
			}
			
		}
	}
	
	
	public static boolean hasAncestorClientId(String ancestorClientId,  UIComponent uic){
		if (uic==null || ancestorClientId == null) return false;
		if (ancestorClientId.equals( uic.getClientId())) return true;
		return hasAncestorClientId(ancestorClientId, uic.getParent());
	}
	
	

	@Override
	public boolean isListenerForSource(Object source) {
		if (source instanceof UIComponent)
			return true;
		else
			return true;
	}
	
	
	
	
	public void makeRenderedFalse(UIComponent uic) {
		FacesContext ctx = FacesContext.getCurrentInstance();
		if (uic == null) uic = ctx.getViewRoot();
		if (uic == null) return;
		if (isDialogOrInDialog(uic)) return;
		if (uic.getId() != null && uic.getId().endsWith("_WUPRbtn")) return;
	
		uic.setValueExpression("rendered", getValueExpression(ctx, "#{false}"));
//		System.out.println("makeRenderedFalse: " + uic + " " +uic.getClientId());
	}

	private boolean isDialogOrInDialog(UIComponent uic) {
		if (uic==null) return false;

		UIComponent p = uic;
		while (p!=null){
			if(p instanceof Dialog || p instanceof ConfirmDialog) {
				return true;
			}
			p = p.getParent();
		}

		return false;
	}

//	public void makeRenderedTrue(UIComponent uic) {
//		FacesContext facesContext = FacesContext.getCurrentInstance();
//		if (uic == null) uic = facesContext.getViewRoot();
//		if (uic == null) return;
//		if (uic.getId() != null && uic.getId().endsWith("_WUPRbtn")) return;
//
//		uic.setValueExpression("rendered", getValueExpression(facesContext, "#{true}"));
//	}
	
	private ValueExpression getValueExpression(FacesContext facesContext, String name) {
		Application app = facesContext.getApplication();
		ExpressionFactory elFactory = app.getExpressionFactory();
		ELContext elContext = facesContext.getELContext();
		return elFactory.createValueExpression(elContext, name, Object.class);
	}	
	
	
	
	public void makeReadOnly(UIComponent uic) {
		FacesContext ctx = FacesContext.getCurrentInstance();
		
		if (uic == null)
			uic = ctx.getViewRoot();
		if (uic == null)
			return;

		if (uic instanceof Tab){
			return; //System.out.println("!!!!!!!!!!readonly"+((Tab) uic).getTitle());
		}

		if (uic.getId() != null && uic.getId().endsWith("_WUPRbtn"))
			return;

		if (uic instanceof Calendar && "inline".equals(uic.getAttributes().get("mode"))) {
			com.comarch.egeria.Utils.executeScript("$(jq('" + uic.getClientId() +"') + ' td').unbind();");
			com.comarch.egeria.Utils.executeScript("$(jq('" + uic.getClientId() +"') + ' td').css('opacity','0.6');");
		} else {
			uic.setValueExpression("readonly", getValueExpression(ctx, "#{true}"));
			uic.setValueExpression("disabled", getValueExpression(ctx, "#{true}"));
		}
		
	}

	
	
	
	String formatAttrMap(Map m){
		ArrayList<String> al = new ArrayList<String>();
		for (Object k : m.keySet()){
			al.add( String.format("\n %1$s\t= %2$s", k, m.get(k)) );
		}
		return al+"\n";
	}


//	public void makeNotReadOnly(UIComponent uic) {
//		FacesContext facesContext = FacesContext.getCurrentInstance();
//		if (uic == null)
//			uic = facesContext.getViewRoot();
//		if (uic == null)
//			return;
//
//		if (uic.getId() != null && uic.getId().endsWith("_WUPRbtn"))
//			return;
//		
//		ValueExpression ve = uic.getValueExpression("disabled");
//		if (ve == null) ve = uic.getValueExpression("readonly");
//		if (ve != null) return; //nie mozemy wlaczajac edytowalnosc kasowac ew. expr. z warunkami do edycji
//
//		uic.setValueExpression("readonly", getValueExpression(facesContext, "#{false}"));
//		uic.setValueExpression("disabled", getValueExpression(facesContext, "#{false}"));
//	}
	
	
	
	
//	public void beforePhase(PhaseEvent event) {
//		 if (event.getPhaseId() == PhaseId.RENDER_RESPONSE) {
//			 cnt=0;
////			 FacesContext.getCurrentInstance().getApplication().subscribeToEvent(PreRenderComponentEvent.class, this);
//		 }
//	}
//
//	public void afterPhase(PhaseEvent event) {
//		if (event.getPhaseId() == PhaseId.RENDER_RESPONSE) {
////				FacesContext.getCurrentInstance().getApplication().unsubscribeFromEvent(PreRenderComponentEvent.class, this);
//				System.out.println("VKontrolaUprawnien visitend components (cnt): " + cnt );
//		}
//	}	
	
	
	


}
