package com.comarch.egeria.pp.kontrolaUprawnien.model;

public class PoziomWuprDebugInfo {

    String kuid;
    int poziom;
    WebUprawnienia upr;


    public PoziomWuprDebugInfo(String kuid,  WebUprawnienia upr, int poziom){
        this.poziom = poziom;
        this.upr = upr;
        this.kuid = kuid;
    }

    public String getKuid() {
        return kuid;
    }

    public int getPoziom() {
        return poziom;
    }

    public String getPoziomDostepu() {
        if (poziom==2) return "E";
        if (poziom==1) return "W";
        return "-";
    }

    public String getPoziomCssStyle() {
        if (poziom==2) return "color: #228888;";
        if (poziom==1) return "color: #333333;";
        return "color: #880000;";
    }

    public WebUprawnienia getUpr() {
        return upr;
    }


    public String key(){
        return this.kuid + " -> wuprId: " + this.upr.getWuprId() + " -> poziom: " + this.poziom;
    }

}
