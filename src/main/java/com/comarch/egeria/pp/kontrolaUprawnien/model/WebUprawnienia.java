package com.comarch.egeria.pp.kontrolaUprawnien.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PPT_WEB_UPRAWNIENIA database table.
 * 
 */
@Entity
@Table(name="PPT_WEB_UPRAWNIENIA", schema="PPADM")
@NamedQuery(name="WebUprawnienia.findAll", query="SELECT w FROM WebUprawnienia w")
public class WebUprawnienia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_WEB_UPRAWNIENIA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_WEB_UPRAWNIENIA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_WEB_UPRAWNIENIA")
	@Column(name="WUPR_ID")
	private long wuprId;

//	@Temporal(TemporalType.DATE)
//	@Column(name="WUPR_AUDYT_DM")
//	private Date wuprAudytDm;

//	@Temporal(TemporalType.DATE)
//	@Column(name="WUPR_AUDYT_DT")
//	private Date wuprAudytDt;
//
//	@Column(name="WUPR_AUDYT_KM")
//	private String wuprAudytKm;
//
//	@Column(name="WUPR_AUDYT_KT")
//	private String wuprAudytKt;
//
//	@Column(name="WUPR_AUDYT_LM")
//	private String wuprAudytLm;
//
//	@Column(name="WUPR_AUDYT_UM")
//	private String wuprAudytUm;
//
//	@Column(name="WUPR_AUDYT_UT")
//	private String wuprAudytUt;

	@Column(name="WUPR_FUNKCJA_UZYTKOWA")
	private String wuprFunkcjaUzytkowa;

	@Column(name="WUPR_NAZWA_XHTML")
	private String wuprNazwaXhtml;

	@Column(name="WUPR_STAN_OBIEGU")
	private String wuprStanObiegu;

	@Column(name="WUPR_UPRAWNIENIE")
	private String wuprUprawnienie;

	@Column(name="WUPR_ZESTAW_POL_ID")
	private String wuprZestawPolId;

	@Column(name="WUPR_CZY_WYSWIETLAC")
	private String wuprCzyWyswietlac;

	@Column(name="WUPR_CZY_EDYTOWALNE")
	private String wuprCzyEdytowalne;
	
	@Column(name="WUPR_OPIS")
	private String wuprOpis;
	
	public WebUprawnienia() {
	}

	public long getWuprId() {
		return this.wuprId;
	}

	public void setWuprId(long wuprId) {
		this.wuprId = wuprId;
	}

//	public Date getWuprAudytDm() {
//		return this.wuprAudytDm;
//	}
//
//	public void setWuprAudytDm(Date wuprAudytDm) {
//		this.wuprAudytDm = wuprAudytDm;
//	}
//
//	public Date getWuprAudytDt() {
//		return this.wuprAudytDt;
//	}
//
//	public void setWuprAudytDt(Date wuprAudytDt) {
//		this.wuprAudytDt = wuprAudytDt;
//	}
//
//	public String getWuprAudytKm() {
//		return this.wuprAudytKm;
//	}
//
//	public void setWuprAudytKm(String wuprAudytKm) {
//		this.wuprAudytKm = wuprAudytKm;
//	}
//
//	public String getWuprAudytKt() {
//		return this.wuprAudytKt;
//	}
//
//	public void setWuprAudytKt(String wuprAudytKt) {
//		this.wuprAudytKt = wuprAudytKt;
//	}
//
//	public String getWuprAudytLm() {
//		return this.wuprAudytLm;
//	}
//
//	public void setWuprAudytLm(String wuprAudytLm) {
//		this.wuprAudytLm = wuprAudytLm;
//	}
//
//	public String getWuprAudytUm() {
//		return this.wuprAudytUm;
//	}
//
//	public void setWuprAudytUm(String wuprAudytUm) {
//		this.wuprAudytUm = wuprAudytUm;
//	}
//
//	public String getWuprAudytUt() {
//		return this.wuprAudytUt;
//	}
//
//	public void setWuprAudytUt(String wuprAudytUt) {
//		this.wuprAudytUt = wuprAudytUt;
//	}

	public String getWuprFunkcjaUzytkowa() {
		return this.wuprFunkcjaUzytkowa;
	}

	public void setWuprFunkcjaUzytkowa(String wuprFunkcjaUzytkowa) {
		this.wuprFunkcjaUzytkowa = wuprFunkcjaUzytkowa;
	}

	public String getWuprNazwaXhtml() {
		return this.wuprNazwaXhtml;
	}

	public void setWuprNazwaXhtml(String wuprNazwaXhtml) {
		this.wuprNazwaXhtml = wuprNazwaXhtml;
	}

	public String getWuprStanObiegu() {
		return this.wuprStanObiegu;
	}

	public void setWuprStanObiegu(String wuprStanObiegu) {
		this.wuprStanObiegu = wuprStanObiegu;
	}

	public String getWuprUprawnienie() {
		return this.wuprUprawnienie;
	}

	public void setWuprUprawnienie(String wuprUprawnienie) {
		this.wuprUprawnienie = wuprUprawnienie;
	}

	public String getWuprZestawPolId() {
		return this.wuprZestawPolId;
	}

	public void setWuprZestawPolId(String wuprZestawPolId) {
		this.wuprZestawPolId = wuprZestawPolId;
	}
	
	public String getWuprCzyWyswietlac() {
		return this.wuprCzyWyswietlac;
	}

	public void setWuprCzyWyswietlac(String wuprCzyWyswietlac) {
		if (wuprCzyWyswietlac==null) wuprCzyWyswietlac = "N";
		if (!"T".equals(wuprCzyWyswietlac) && "T".equals(this.wuprCzyEdytowalne))
			this.wuprCzyEdytowalne = "N";
		this.wuprCzyWyswietlac = wuprCzyWyswietlac;
	}

	public String getWuprCzyEdytowalne() {
		return this.wuprCzyEdytowalne;
	}

	public void setWuprCzyEdytowalne(String wuprCzyEdytowalne) {
		if (wuprCzyEdytowalne==null) wuprCzyEdytowalne = "N";
		if ("T".equals(wuprCzyEdytowalne) && !"T".equals(this.wuprCzyWyswietlac) )
			this.wuprCzyWyswietlac = "T";
		this.wuprCzyEdytowalne = wuprCzyEdytowalne;
	}

	public String getWuprOpis() {
		return wuprOpis;
	}

	public void setWuprOpis(String wuprOpis) {
		this.wuprOpis = wuprOpis;
	}


	@Override
	public String toString() {
		return "wupr / "
				+ "id: " + this.wuprId
				+ ";\t fu: " + this.wuprFunkcjaUzytkowa
				+ ";\t st.ob.: " + this.wuprStanObiegu
				;
	}
}