package com.comarch.egeria.pp.kontrolaUprawnien;

import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.kontrolaUprawnien.model.PoziomWuprDebugInfo;
import com.comarch.egeria.pp.kontrolaUprawnien.model.WebUprawnienia;
import com.comarch.egeria.pp.menu.EgrMenuBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.sun.faces.facelets.component.UIRepeat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.component.column.Column;
import org.primefaces.component.columns.Columns;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.component.dialog.Dialog;
import org.primefaces.component.fieldset.Fieldset;
import org.primefaces.component.menuitem.UIMenuItem;
import org.primefaces.component.outputlabel.OutputLabel;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.component.tabview.Tab;
import org.primefaces.component.tabview.TabView;
//import org.primefaces.context.RequestContext;
import org.primefaces.extensions.component.layout.LayoutPane;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.component.UIComponent;
import javax.faces.component.UINamingContainer;
import javax.faces.component.html.HtmlForm;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.component.html.HtmlPanelGrid;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static com.comarch.egeria.Utils.getExceptionDetails;
import static com.comarch.egeria.Utils.nz;

@ManagedBean
@Named
@Scope("view")
public class KontrolaUprawnien extends SqlBean {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	public static ConcurrentHashMap<String, List<WebUprawnienia> > uprawnieniaHM = new ConcurrentHashMap<>(); //viewId->List<WebUprawnienia>
	public static ConcurrentHashMap<Long, WebUprawnienia> uprawnieniaIdWebUprawnieniaHM = new ConcurrentHashMap<>(); //wuprId->WebUprawnienia
	public static Long wuprTS = 0L;

	@Inject
	User user;
	
	@Inject
	ObiegBean obieg;

	String viewId = "-";
	
	private String dlgClientId = "";
	
	private String dlgKuid = "";
	
	private String dlgOpis = "";
	
	private String componentClass = "";

	private String currentViewId = "";

	private WebUprawnienia wupr = new WebUprawnienia();
	
	public HashMap<String, String> opisyHM = new HashMap<>();

	private ArrayList<PoziomWuprDebugInfo> wuprDebugInfo = new ArrayList<>();
	private HashSet<String> wuprDebugInfoKeys = new HashSet<>();

	String warn = null;

	@PostConstruct
	public void init() {

		FacesContext ctx = FacesContext.getCurrentInstance();
		this.viewId = ctx.getViewRoot().getViewId(); // /faces/DelegacjeMF/DelegacjaMFZagr.xhtml
		newWupr();

	}

	
	public void newWupr() {
		wupr = new WebUprawnienia();
		wupr.setWuprNazwaXhtml(this.viewId);
	}

	
	public void loadWuprForXhtml(String viewId) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException  {
		currentViewId = viewId;
		
		if(nz(viewId).contains("/WEB-INF/protectedElements/Errors"))
			return;
		
		Long tsDB = nz(SqlDataCache.tbtsCHM.get("PPADM.PPT_WEB_UPRAWNIENIA"));
			
		if (KontrolaUprawnien.uprawnieniaHM.get(viewId) != null && tsDB <= KontrolaUprawnien.wuprTS) {
			return;//wupr wczytane i aktualne
		}
		else {
			
			reloadWuprCHMs(tsDB);
			
			if (KontrolaUprawnien.uprawnieniaHM.get(viewId) ==null)
				KontrolaUprawnien.uprawnieniaHM.put(viewId, new ArrayList<>()); 
			
			try {
				EgrMenuBean egrMenuBean =  (EgrMenuBean) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("egrMenuBean");
				if (egrMenuBean!=null){
					egrMenuBean.reloadMenuModel();
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}

		}
		return;
	}
	
	

	
	public static void reloadWuprCHMs(Long tsDB) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException  {

		KontrolaUprawnien.wuprTS = nz(tsDB);//aby inni sie nie pchali w tym samymm czasie
		
		List<WebUprawnienia> allWupr;

		try (HibernateContext h = new HibernateContext()) {
			allWupr = HibernateContext.query("FROM WebUprawnienia");
		}
		
		ConcurrentHashMap<String, List<WebUprawnienia> > wuprByXhtmlHM = new ConcurrentHashMap<>(); 
		ConcurrentHashMap<Long, WebUprawnienia> wuprId2WuprHM = new ConcurrentHashMap<>();

		for (WebUprawnienia wupr : allWupr) {
			wuprId2WuprHM.put(wupr.getWuprId(), wupr);
			String xhtml = wupr.getWuprNazwaXhtml();
			List<WebUprawnienia> list = wuprByXhtmlHM.get(xhtml);
			if (list==null)
				 list = new ArrayList<>();
			
			list.add(wupr);				
			wuprByXhtmlHM.put(xhtml,list);
		}
		
		KontrolaUprawnien.uprawnieniaHM.clear();
		KontrolaUprawnien.uprawnieniaHM = wuprByXhtmlHM;
		KontrolaUprawnien.uprawnieniaIdWebUprawnieniaHM.clear();
		KontrolaUprawnien.uprawnieniaIdWebUprawnieniaHM = wuprId2WuprHM;
	}
	
	
	
	
	public void iterateChildrenByID(String id) throws IOException {
		UIComponent uic = FacesContext.getCurrentInstance().getViewRoot().findComponent(id);
		if (uic != null) {
			iterateChildren(uic);
		} else
			log.debug("iterateChildren: nie znaleziobno elementu wg id: " + id);
	}
	
	
	
	int i=0;
	public void iterateChildren(UIComponent uic) throws IOException {
		FacesContext ctx = FacesContext.getCurrentInstance();

		if (uic == null)
			uic = ctx.getViewRoot();
		
		
		if ("printContent1".equals(uic.getClientId())) // Na pseudowydrukach nie rysujemy zebatek
			return;
		
		String kuid = getKuid(uic);//Nie wyrzucać z tego miejsca obliczenia KUID!
//		System.out.println(++i  + " " +  uic.getClientId() + " -> " + kuid);
		
		if ("lpContent".equals(uic.getClientId())) // Na pseudowydrukach nie rysujemy zebatek
			uic.getAttributes().put("kubtn", 10);
		
		int kulvl = poziomDostepuWgWUPR(uic);
		kulvl = kulvl+0;
		
		for (UIComponent c : SqlBean.getFacetsAndChildrenSet(uic)) {
			iterateChildren(c);
		}
		
	}
	

	
	
	
	
	
	
	
	public ConcurrentHashMap<String, Integer> type2kuidHM = new ConcurrentHashMap<>();
	
	public ConcurrentHashMap<String, String> clientId2kuidHM = new ConcurrentHashMap<>();
	
	
	public String getKuid(UIComponent uic) {
		
		if (uic==null) return null;

		String id = (String) uic.getAttributes().get("id");

		String kuid = (String) uic.getAttributes().get("kuid");
		
		String clientId = uic.getClientId();
		
		if (kuid!=null){
			clientId2kuidHM.put(clientId, kuid);
			return kuid;
		}
		
		if (id!=null && !id.contains("j_id"))  
			kuid=id;
		else { //generujemy nowy kuid wg typu uic...
			
			String type = mapToSimpleType(uic.getClass().getSimpleName());
	 		if (UIComponent.isCompositeComponent(uic)) {
				type = "CC";
			}
			Integer i = nz(type2kuidHM.get(type)) + 1;
			type2kuidHM.put(type, i);
			kuid = type + "_"+ i;
		}
		
		String containerClientKuId = getContainerClientKuId(uic.getParent());
		if (containerClientKuId!=null)
			kuid = containerClientKuId + ":" + kuid;
		
		uic.getAttributes().put("kuid", kuid);
		clientId2kuidHM.put(uic.getClientId(), kuid);
		
		return kuid;
	}

	
	String formatAttrMap(Map m){
		ArrayList<String> al = new ArrayList<String>();
		for (Object k : m.keySet()){
			al.add( String.format("\n %1$s\t= %2$s", k, m.get(k)) );
		}
		return al+"\n";
	}

	
	
	public String mapToSimpleType(String type) {
		if (type==null) return null;
		
			 if ("UINamingContainer".equals(type)) 		return "unc";
		else if ("CObiegCzynnosci".equals(type)) 		return "cobg";
		else if ("CLookupTable".equals(type))			return "ltb";
		else if ("HtmlOutputLabel".equals(type)) 		return "hlbl";
		else if ("UIRepeat".equals(type)) 				return "urpt";
		else if ("UIPane".equals(type)) 				return "upn";
		else if ("UIOutput".equals(type)) 				return "uout";
		else if ("Layout".equals(type)) 				return "lt";
		else if ("LayoutPane".equals(type)) 			return "lpn";
		else if ("Hotkey".equals(type)) 				return "htk";
		else if ("Growl".equals(type)) 					return "grwl";
		else if ("CommandButton".equals(type)) 			return "cbtn";
		else if ("HtmlOutputText".equals(type)) 		return "hout";
		else if ("HtmlPanelGroup".equals(type)) 		return "hpgp";
		else if ("HtmlPanelGrid".equals(type)) 			return "hpgd";
		else if ("UIPanel".equals(type)) 				return "upl";
		else if ("RemoteCommand".equals(type)) 			return "rc";
		else if ("PanelGrid".equals(type)) 				return "pg";
		else if ("Column".equals(type)) 				return "col";
		else if ("Row".equals(type)) 					return "row";
		else if ("Focus".equals(type)) 					return "fcs";
		else if ("UISeparator".equals(type)) 			return "usep";
		else if ("MethodParameter".equals(type)) 		return "mp";
		else if ("Tooltip".equals(type)) 				return "tltp";
		else if ("OutputLabel".equals(type)) 			return "olbl";
		else if ("Columns".equals(type)) 				return "cols";
		else if ("HtmlBody".equals(type)) 				return "body";
		else if ("Tab".equals(type)) 					return "tab";
		else if ("TabView".equals(type)) 				return "tv";
		else if ("Dialog".equals(type)) 				return "dlg";
		else if ("GraphicImage".equals(type)) 			return "img";
		else if ("FileUpload".equals(type)) 			return "fupl";
		else if ("ComponentRef".equals(type)) 			return "cref";
		else if ("ConfirmDialog".equals(type)) 			return "cdlg";
		else if ("OverlayPanel".equals(type)) 			return "ovpl";
		else if ("HtmlOutputFormat".equals(type)) 		return "hof";
		else if ("RowToggler".equals(type)) 			return "rtgl";
		else if ("InputText".equals(type)) 				return "inpt";
		else if ("Calendar".equals(type)) 				return "cal";
		else if ("HtmlForm".equals(type)) 				return "hfrm";
		else if ("UIViewRoot".equals(type)) 			return "UVR";
		else if ("UIInstructions".equals(type)) 		return "uins";
		else if ("HtmlCommandLink".equals(type))		return "hcl";
		else if ("Panel".equals(type))					return "pnl";
		else if ("UISelectItems".equals(type))			return "usis";
		else if ("UISelectItem".equals(type))			return "usi";
		else if ("RowExpansion".equals(type))			return "rexp";
		else if ("AccordionPanel".equals(type))			return "acc";
		else if ("Spacer".equals(type))					return "spcr";
		else if ("AjaxStatus".equals(type))				return "ajxs";
		else if ("Fieldset".equals(type))				return "fset";
		else if ("HtmlInputText".equals(type))			return "htxt";
		else if ("InputTextarea".equals(type))			return "ita";
		else if ("ScrollPanel".equals(type))			return "sclp";
		else if ("CommandLink".equals(type))			return "clnk";
		else if ("PassthroughElement".equals(type))		return "pass";
		else if ("SelectOneMenu".equals(type))			return "som";	 
		else if ("InputNumber".equals(type))			return "inum";
		else if ("SelectOneRadio".equals(type))			return "sor";
		else if ("SplitButton".equals(type))			return "sb";
		else if ("OutputPanel".equals(type))			return "op";
		else if ("Menu".equals(type))					return "mnu";
		else if ("UITreeNode".equals(type))				return "tree";

			 
		log.debug("TODO - mapToSimpleType - add mapping: " + type );
		
		return type;
	}
	

	
	
	public String getContainerClientKuId(UIComponent uic){
		if (uic == null) 
			return null;
		
//		if (" j_idt2891 ".contains(" "+uic.getClientId() +" "));
//			System.out.println();
		
		if (uic instanceof UINamingContainer 
				//|| uic instanceof UIViewRoot
				|| uic instanceof Columns
				|| uic instanceof HtmlForm
				|| uic instanceof TabView 
				|| uic instanceof AccordionPanel 
				|| uic instanceof DataTable
			)
			return getKuid(uic);
		
		//((com.sun.faces.component.ComponentResourceContainer) uic).getAttributes()
		return getContainerClientKuId(uic.getParent());
	}
	
	
	
	public String getKuOpis(String kuid) {
		return this.opisyHM.get(kuid);
	}
	
	
	
	public static String getKuOpis(UIComponent uic){
		String ret = getKuOpis(uic, 0);
		return ret;
	}
	
	public static String getKuOpis(UIComponent uic, int lvl){
			
		
		if (uic == null)
			return "";
		
		
		String ret = ""; //uic.getClass().getSimpleName();


		
		if (uic instanceof CommandButton) {
			ret += opisIfAttr(uic, "value", "");
			if (ret.isEmpty())
				ret += opisIfAttr(uic, "icon", "i");
			
		} else if (uic instanceof LayoutPane) {
			ret += opisIfAttr(uic, "position", "");
			return ret; //powyżej jest co najwyzej lpConetent...
			
		} else if (uic instanceof Dialog) {
			ret += opisIfAttr(uic, "header", ""); 
			return ret;//nie ma sensu rekurencyjnie szukac powyzej dialogów
			
		} else if (uic instanceof Tab) {
			ret += opisIfAttr(uic, "title", "");
			
		} else if (uic instanceof OutputLabel) {
			ret += opisIfAttr(uic, "value", "v");
			
		} else if (uic instanceof HtmlOutputText) {
			ret += opisIfAttr(uic, "value", "v");			
		
		} else if (uic instanceof Column) {
			ret += opisIfAttr(uic, "headerText", ""); 
			
		} 
//		else {
//			if (!uic.getId().contains("j_id"))
//				ret += " id:" + uic.getId();
//		}
		
		ret += opisIfAttr(uic, "label", "l");
		ret += opisIfAttr(uic, "lovDlgTitle", null).replaceAll(":", "");
		
		try {
			Object datasource = uic.getAttributes().get("datasource"); //w trakcie renderowania pozwalamy na taki odczyt (co wyliczy value expression)
			if (datasource!=null){
				if (datasource instanceof SqlDataHandler){
					ret += " lw:'" + ((SqlDataHandler) datasource).getShortName() + "'";
				}
			}
		} catch (Exception e) {} //dynamiczna lw		 


		if ( lvl==0 || !ret.isEmpty() )
			ret = uic.getClass().getSimpleName().toLowerCase() + ret;
			
		if (ret.isEmpty())
			ret = " -";
		else 
			ret = "> " + ret;
		
		ret = getKuOpis(uic.getParent(),lvl+1) + " " + ret.trim();
		return ret;
	}
	
	
	
	static String opisIfAttr(UIComponent uic, String attrName, String alias){
		if (uic == null)
			return "";
		Object attr = uic.getAttributes().get(attrName);
		if (attr != null && !"".equals(attr)) {
			if (alias != null) {
				if (alias.isEmpty())
					return " '" + attr + "'";
				else
					return " " + alias + ":'" + attr + "'";
			} else {
				return " " + attrName + ":'" + attr + "'";
			}
		}
		return "";
	}
	
	
	
	
	
	



	


	
	/**
	 * Zwraca poziom uprawnień ustawiony w tabeli PPT_WEB_UPRAWNIENIA dla wskazanego kuid. 
	 * Jeśli w w/w tabeli nie ma żadengo wiersza dla wskazanego kuid, zwr. -1; 
	 * Jeśli wszystkie wpisy są ustawione jako niewidoczny i nieedytowalny, zwr. 0
	 * Jeśli znajdzie się co najmniej jeden wpis z ustawieniem widoczny = 'T' zwr. 1
	 * Jeśli znajdzie się co najmniej jeden wpis z ustawieniem edytowalny = 'T' zwr. 2
	 * @param kuid
	 * @return -1 - nie ustalono, 0 - ustawiono niewidoczny i nieedytowalny, 1 - widoczny; 2 - edytowalny
	 */
	public int poziomDostepuWgWUPR(String kuid) {
		if (kuid==null) return -1;

		if (eq("content", kuid))
			System.out.println();


		int ret = -1;

		List<WebUprawnienia> currentViewUprawnienia = uprawnieniaHM.get(currentViewId);
		
		if(currentViewUprawnienia != null) {
			for (WebUprawnienia upr : currentViewUprawnienia) {
	
				if ( upr.getWuprFunkcjaUzytkowa() == null || user.hasAnyPermission(upr.getWuprFunkcjaUzytkowa()) ) {
					if ( upr.getWuprZestawPolId() == null || ("" + kuid).equals( upr.getWuprZestawPolId() ) ) {
						String kategoriaObiegu = getKategoriaObiegu();
						if ( upr.getWuprStanObiegu() == null //dowlony stan obiegu
								|| (kategoriaObiegu!=null && ("" + getStanObiegu()).equals( upr.getWuprStanObiegu())) //jest obieg i stan obiegu pasueje do uprawnienia
								|| (kategoriaObiegu!=null && "0".equals(upr.getWuprStanObiegu()) && "".equals(getStanObiegu())) //jest obieg ale nie został rozpoczety
						) {

							
							if (ret<0) ret = 0; //bo jest co najmniej jeden wpis ustalajacy prawo dla kuid
							
							if(ret < 1 && "T".equals(upr.getWuprCzyWyswietlac())) 
								ret = 1;	
														
							if(ret < 2 &&  "T".equals(upr.getWuprCzyEdytowalne())) {

								addPoziomWuprDebugInfo(kuid, 2, upr);

								return 2; //return bo więcej niż 2 i tak nie dostaniemy, więc nie ma sensu dalej szukac
							}

							addPoziomWuprDebugInfo(kuid, ret, upr);
						}
					}
				}
			}
		}

		return ret;
	}

	private void addPoziomWuprDebugInfo(String kuid, int ret, WebUprawnienia upr) {
		PoziomWuprDebugInfo x = new PoziomWuprDebugInfo(kuid, upr, ret);
		String key = x.key();
		if (this.wuprDebugInfoKeys.add(key))
			this.wuprDebugInfo.add(x);
	}


	/**
	 * W razie koniecznosci oblicza i zwraca poziom uprawnien do komponentu dla zalogowaneo uzytkownika: 0 - brak; 1 - widoczny; 2 - edytowalny
	 * Sprawdza czy zadany komponent ma okreslony poziom uprawnien dla zalogowanego uzytkownika w przekazanej HM. 
	 * Jesli nie to szuka najblizszego rodzica, ktory ma taki poziom ustalony (i zwr. ten wlasnie poziom). 
	 * Jesli zaden rodzic nie ma ustalonego uprawnienia zwraca 0. 
	 * @param uic - komponent, dla którego szukamy poziomu uprawnien
	 * @return 0 - brak; 1 - widoczny; 2 - edytowalny (admin ma zawsze 2)
	 */
	public int poziomDostepuWgWUPR(UIComponent uic) {
		if (uic == null) return -1;//w razie czego wyjscie z rekurencji ustawi 0
	
	
		if (user.isAdmin()) {
			uic.getAttributes().put("kubtn", 1);
			return 2;
		}
	
		if ("/ku.xhtml".equals(this.viewId) || this.viewId.startsWith("/WEB-INF/protectedElements/Errors/") )
			return 2;

		if ("/faces/Administrator/ZmianaUzytkownikaLista.xhtml".equals(viewId) && user.hasPermission("PP_SU"))
			return 2;
		
		if (uic instanceof LayoutPane)
			uic.getAttributes().put("kulvl-parent", 1);


        Object lvl = uic.getAttributes().get("kulvl");
		if (lvl!=null){
			return (int) lvl;
		}
		
		
		String kuid = getKuid(uic);
		
//		this.kuid2ClientIdHM.put(kuid, uic.getClientId());
		
		int ret = poziomDostepuWgWUPR(kuid);
		
		UIComponent p = uic.getParent();
		if (ret<0 && p!=null)//poziom nieustalony - dziedziczymy poziom rodzica
			ret = poziomDostepuWgWUPR(uic.getParent());//rekurencja!
		else if (ret>0){ //el. ustawiony jako widoczny - trzeba sprawdzic / odkryc rodzica/ów...
			odkryjRodzicow(uic);
		}

		if (ret<0) ret=0;
		
		uic.getAttributes().put("kulvl", ret);
		return ret;
	}
	
	
	
	public void odkryjRodzicow(UIComponent uic){
		if (uic==null) 
			return;
		
		UIComponent p = uic.getParent();
		if	(p==null || p.getAttributes().get("kulvl")==null) // nie podlega ograniczeniom 
			return;
		
		Integer plvl = (Integer) p.getAttributes().get("kulvl-parent");
		if (plvl==null)//|| plvl<=0
			p.getAttributes().put("kulvl-parent", 1);
		
		odkryjRodzicow(p);
	}
	



	
	
	public boolean kuIsHidden(String kuid) {

		if (user.isAdmin())
			return false; // adminowi nic nie ukrywamy!

        return poziomDostepuWgWUPR(kuid) == 0;

	}

	
	public boolean kuIsReadOnly(String kuid) {
		
		if (user.isAdmin())
			return false; // adminowi nic nie ukrywamy!

        return poziomDostepuWgWUPR(kuid) >= 0 && poziomDostepuWgWUPR(kuid) < 2;

	}
	

	public Object getIdEncji() {
		if (this.obieg==null) return null;
		return obieg.getIdEncji();
	}


	public String getStanObiegu() {
		if (this.obieg==null) return null;
		return this.obieg.stanObiegu();
	}
	


	public void addKuBtn(UIComponent uic) {

		if (!user.isRenderKU() || !isViewRenderKU())
			return;
		
		if(uic==null || uic.getAttributes().get("btnku")!=null)
			return;
			
		if (uic instanceof CommandButton)
			return; //patrz - EgrCommandButtonRenderer.java
		
		if (uic instanceof UIMenuItem && uic.getClientId().startsWith("egrMenuForm:")) {
			return;
		}

			
		
		String kuid = getKuid(uic);

		if (!
			(
			    user.isRenderKU() 
			 && !"ku-e".equals(kuid)  && !"".equals(kuid)
			 && !kuid.contains("DLG_dlgKU")
			 && (  uic.getAttributes().get("kuid") != null //np. content
			 	|| uic instanceof Tab				
			 	|| uic instanceof PanelGrid
				|| uic instanceof HtmlForm
				|| uic instanceof HtmlPanelGroup
//				|| uic instanceof ScrollPanel - > Egrtemplate.shtml - $('.ui-scrollpanel').wrap ...
//				|| uic instanceof org.primefaces.component.column.Column
//				|| uic instanceof org.primefaces.component.datatable.DataTable
//				|| uic instanceof javax.faces.component.UIPanel
//				|| uic instanceof javax.faces.component.UINamingContainer
//				|| uic instanceof javax.faces.component.html.HtmlPanelGrid
				|| ("true".equals(""+uic.getAttributes().get("renderKU")))  // UIComponent.isCompositeComponent(uic) && 
			    )
			 )
			){
			return;
		}

		
		
		if (getFacetsAndChildrenSet(uic).isEmpty() && !(uic instanceof CommandButton))
			return;
		
		HtmlOutputText x = new HtmlOutputText();
		x.setEscape(false);
		
		int offset = calcBtnKuOffset(uic);
		String jqClientId = "#"+uic.getClientId().replaceAll(":", "\\\\\\\\:").replace("_btnKuIcon", "");
		
		String html = ""
				+ "<div class=\"btnKu\" "
				+ " style=\"z-index:999996; position:absolute; top:0px; left:0px; margin:0; opacity:0.2; \" "
				+ ">"
				+ " <i id='" +uic.getClientId() + "_btnKuIcon' class=\"material-icons pp-menuitem-icon btnkujs\""
				+ "   style=\"font-size:14px!important; color:red; cursor:crosshair;  margin-left:0px; position:absolute; z-index:999997\" "
			    + "   onmouseover=\"event.stopPropagation(); $('" + jqClientId + "' ).addClass(    'kuhighlight"+ uic.getClass().getSimpleName() +"' );\" "
			    + "   onmouseout =\"event.stopPropagation(); $('" + jqClientId + "' ).removeClass( 'kuhighlight"+ uic.getClass().getSimpleName() +"' );\" "
				+ "  >settings</i>" 
				+ "</div>";
		
		if (kuid.contains("j_idt"))
			html = html.replace("font-size:14px", "font-size:10px");

		if ("content".equals(kuid)) {
			html = html
					.replace("color:red;", 		"color:red;")
					.replace("font-size:14px", 	"font-size:20px")
					.replace("left:0px", 		"right:20px")
					.replace("z-index:999997", "z-index:999999")
					.replace("z-index:999996", "z-index:999998");

			x.setValue(html);
			uic.getChildren().add(x);
		 
		} else if (uic instanceof Tab) {
			html = html
					.replace("color:red;", 		"color:green;")
					//.replace("z-index:999997", "z-index:999997")
					;

			x.setValue(html);
			if (!uic.getChildren().contains(x) && uic.getChildren().size()>0)
				uic.getChildren().add(x);

		}else if (uic instanceof HtmlForm ) {
			html = html
					.replace("left:0px" , 		"left:"+offset+"px")
					.replace("color:red;", 		"color:blue;")
					.replace("z-index:999997", "z-index:999998");

			x.setValue(html);
			if (!uic.getChildren().contains(x) && uic.getChildren().size()>0)
				uic.getChildren().add(x);
			
//		}else if (uic instanceof ScrollPanel) {  - > Egrtemplate.shtml - $('.ui-scrollpanel').wrap ...
//			html = html
//					.replace("left:0px" , 		"left:"+offset+"px")
//					.replace("top:0px" , 		"top:15px")
//					.replace("color:red;", 		"color:DodgerBlue;")
//					.replace("z-index:999996; position:absolute;", 	"z-index:999996;")
//					.replace("z-index:999997", "z-index:999998");
//
//			x.setValue(html);
//			
//			if (!uic.getChildren().contains(x) && uic.getChildren().size()>0)
//				uic.getChildren().add(x);

		}else if (uic instanceof HtmlPanelGroup) {
			html = html
					.replace("left:0px" , 		"left:"+offset+"px")
					.replace("top:0px" , 		"bottom:"+(offset+10)+"px")
					.replace("color:red;", 		"color:firebrick;")
					.replace("z-index:999997", "z-index:999996");

			x.setValue(html);
			if (!uic.getChildren().contains(x) && uic.getChildren().size()>0)
				uic.getChildren().add(x);

		} else if (uic instanceof PanelGrid || uic instanceof HtmlPanelGrid ) {
			html = html
					.replace("color:red;", 			"color:darkviolet;")
					.replace("font-size:14px", 		"font-size:12px")
					.replace("left:0px;", 			"margin-left:-5px;")
					.replace("top:0px;", 			"")
					.replace("z-index:999996; position:absolute;", 	"z-index:999996; position:static;"
					.replace("z-index:999997", "z-index:999996"));


			x.setValue(html);

			UIComponent parent = uic.getParent();
			UIComponent fHeader = uic.getFacets().get("header");

			if (fHeader == null // nie fHeader'a
					&& !(parent instanceof PanelGrid) && !(parent instanceof Fieldset) && !(parent instanceof UIRepeat))
				uic.getFacets().put("header", x);
		} 

	}

	
	int offs=0;
	public int calcBtnKuOffset(UIComponent uic){
		offs+=6;
		return (offs % 22)+5;
	}

	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////                                                                             ///////////////////////////////////////
	/////////////////////////////////////////                         dlgKontrolaUprawnien.xhtml                          ///////////////////////////////////////
	/////////////////////////////////////////                                                                             ///////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void showDlgKU() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		this.setComponentClass("" + params.get("class"));
		this.setDlgKuid("" + params.get("kuid"));
//		this.setDlgOpis( opisyHM.get(this.dlgKuid) );
		
		com.comarch.egeria.Utils.update("frmKUHeader");
		com.comarch.egeria.Utils.update("frmKU");
		com.comarch.egeria.Utils.update("frmKUFooter");
		com.comarch.egeria.Utils.executeScript(" PF('dlgKU').show() ");

	}
	

	public String getComponentClass() {
		return componentClass;
	}

	public void setComponentClass(String componentClass) {
		this.componentClass = componentClass;
	}

	
	public String getDlgKuid() {
		return dlgKuid;
	}

	public void setDlgKuid(String uicomponentClientId) {
		if (uicomponentClientId==null)
			uicomponentClientId = "";
		
		this.dlgClientId = uicomponentClientId;
		
		if (uicomponentClientId.endsWith("_btnKuIcon"))
			uicomponentClientId = uicomponentClientId.replace("_btnKuIcon", "");
		
		this.dlgKuid = clientId2kuidHM.get(uicomponentClientId);
		if (this.dlgKuid==null) this.dlgKuid = uicomponentClientId;//setter z dlgKontrolaUprawnien przesyla kuid a nie klientid
		
		String opis = opisyHM.get(this.dlgKuid) + "\r\n(clientId:'" + uicomponentClientId + "')";
		
		this.setDlgOpis( opis.trim() );
		
		com.comarch.egeria.Utils.update("frmKU");
		com.comarch.egeria.Utils.update("frmKUHeader");
	}


	public WebUprawnienia getWupr() {
		return wupr;
	}

	public void setWupr(WebUprawnienia wupr) {
		this.wupr = wupr;
	}

	
	public void dodaj() {
        SqlDataSelectionsHandler lw = this.getLW("PPL_KU_ST_OBIEG", this.getKategoriaObiegu());

        if (lw==null || lw.getSelectedRows().isEmpty())
            dodajBezStanuObg();
        else
            dodajWieleWgStObg();

        this.getPplDlgKu().resetTs();
    }


    private void dodajBezStanuObg() {
        //boolean dodawanie = false;
        if (wupr.getWuprId() == 0) {
            wupr.setWuprZestawPolId(this.dlgKuid);
            wupr.setWuprOpis(this.dlgOpis);
            //dodawanie = true;
        }

        wupr.setWuprCzyWyswietlac(wupr.getWuprCzyWyswietlac());
        wupr.setWuprCzyEdytowalne(wupr.getWuprCzyEdytowalne());

        try {
            HibernateContext.saveOrUpdate(wupr);
            this.getPplDlgKu().resetTs();
            uprawnieniaHM.get(currentViewId).add(wupr);
            uprawnieniaIdWebUprawnieniaHM.put(wupr.getWuprId(), wupr);
            //if (dodawanie)
            newWupr();
        } catch (Exception e) {

            String exTxt = getExceptionDetails(e);
            if (exTxt.contains( " (PPADM.PPC_WUPR_UK_NZW)") )
                exTxt = "Contraint: PPADM.PPC_WUPR_UK_NZW / Wpis o takich parametrach już istneieje.";


            User.alert("Błąd przy próbie zapisania w bazie wpisu o wartościach: "
                    + "\r\n----------------------------------------------------"
                    + "\r\nXHTML: " + wupr.getWuprNazwaXhtml()
                    + "\r\nIdentyfikator KU: " + wupr.getWuprZestawPolId()
                    + "\r\nFunkcja użytkowa: " + wupr.getWuprFunkcjaUzytkowa()
                    + "\r\nStan obigu: " + wupr.getWuprStanObiegu()
//                            + "\r\nWyświetlanie: " + upr.getWuprCzyWyswietlac()
//                            + "\r\nEdycja: " + upr.getWuprCzyEdytowalne()
                    + "\r\n----------------------------------------------------"
//                            + "\r\nErr.Msg: " + e.getMessage()
//                            + "\r\n----------------------------------------------------"
                    + "\r\n" + exTxt);

			wupr.setWuprId(0l);//niby znowu nowy - wystarczy przestawic fu lub obieg
        }

    }

    public void dodajWieleWgStObg() {

		SqlDataSelectionsHandler lw = this.getLW("PPL_KU_ST_OBIEG", this.getKategoriaObiegu());
		if (lw==null || lw.getSelectedRows().isEmpty())
			return;

		try (HibernateContext h = new HibernateContext()){
			for (DataRow r : lw.getSelectedRows()) {

			    WebUprawnienia upr = new WebUprawnienia();
                upr.setWuprNazwaXhtml(this.viewId);
                upr.setWuprZestawPolId(this.dlgKuid);
                upr.setWuprOpis(this.dlgOpis);
                upr.setWuprFunkcjaUzytkowa(this.wupr.getWuprFunkcjaUzytkowa());
                upr.setWuprStanObiegu((String) r.get("dstn_nazwa"));
                upr.setWuprCzyWyswietlac(wupr.getWuprCzyWyswietlac());
                upr.setWuprCzyEdytowalne(wupr.getWuprCzyEdytowalne());
                Transaction t = h.getSession().beginTransaction();

                try {
                    h.getSession().saveOrUpdate(upr);
                    uprawnieniaHM.get(currentViewId).add(upr);
                    uprawnieniaIdWebUprawnieniaHM.put(upr.getWuprId(), upr);
                    t.commit();
                } catch (Exception e){
                    t.rollback();

                    String exTxt = getExceptionDetails(e);
                    if (exTxt.contains( " (PPADM.PPC_WUPR_UK_NZW)") )
                        exTxt = "Contraint: PPADM.PPC_WUPR_UK_NZW / Wpis o takich parametrach już istneieje.";


                    User.alert("Błąd przy próbie zapisania w bazie wpisu o wartościach: "
                            + "\r\n----------------------------------------------------"
                            + "\r\nXHTML: " + upr.getWuprNazwaXhtml()
                            + "\r\nIdentyfikator KU: " + upr.getWuprZestawPolId()
                            + "\r\nFunkcja użytkowa: " + upr.getWuprFunkcjaUzytkowa()
                            + "\r\nStan obigu: " + upr.getWuprStanObiegu()
//                            + "\r\nWyświetlanie: " + upr.getWuprCzyWyswietlac()
//                            + "\r\nEdycja: " + upr.getWuprCzyEdytowalne()
                            + "\r\n----------------------------------------------------"
//                            + "\r\nErr.Msg: " + e.getMessage()
//                            + "\r\n----------------------------------------------------"
                            + "\r\n" + exTxt);


                }
			}

			//h.getSession().beginTransaction().commit();
            this.getPplDlgKu().resetTs();

		}catch (Exception e){
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
	}


    public void usun() {

		if (this.getPplDlgKu().getSelectedRows().isEmpty())
			return;

		ArrayList<Long> al = new ArrayList<Long>();
		for (DataRow r : this.getPplDlgKu().getSelectedRows()) {
			al.add(r.getIdAsLong());
		}
		
		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			WebUprawnienia webUprawnienia = null;
			for (Object id : al) {
				webUprawnienia = uprawnieniaIdWebUprawnieniaHM.get(id);
				if(webUprawnienia != null) {
					s.delete(webUprawnienia);
					uprawnieniaIdWebUprawnieniaHM.remove(id);
					uprawnieniaHM.get(currentViewId).remove(webUprawnienia);
				}
			}
			s.beginTransaction().commit();
		}
		
		this.getPplDlgKu().resetTs();

	}
	

	public SqlDataSelectionsHandler getPplDlgKu() {
		return this.getSql("PPL_DLG_KU");
	}
	
	public String getKategoriaObiegu() {
		if (this.obieg==null) return null;
		return this.obieg.getKategoriaObiegu();
	}

	public String getDlgOpis() {
		return dlgOpis;
	}

	public void setDlgOpis(String dlgOpis) {
		this.dlgOpis = dlgOpis;
	}

	public String getDlgClientId() {
		return dlgClientId;
	}

	public void setDlgClientId(String dlgClientId) {
		this.dlgClientId = dlgClientId;
	}


	public ArrayList<PoziomWuprDebugInfo> getWuprDebugInfo() {
		return wuprDebugInfo;
	}

	public void setWuprDebugInfo(ArrayList<PoziomWuprDebugInfo> wuprDebugInfo) {
		this.wuprDebugInfo = wuprDebugInfo;
	}


	public void ustawPoziomDostepuDlaZanzaczonych(SqlDataSelectionsHandler lw, int poziom) {

//		log.debug("seryjna zmiana poziomu");

		String wystwietl="N";
		String edytuj="N";

		if (poziom >= 1) wystwietl="T";
		if (poziom >= 2)  edytuj="T";

		//SqlDataSelectionsHandler lw = this.getPplWebUprawnienia();
		if (lw==null || lw.getSelectedRows().isEmpty())
			return;

		ArrayList<Long> al = new ArrayList<Long>();
		for (DataRow r : lw.getSelectedRows()) {
			al.add( r.getIdAsLong() );
		}

		try {
			String whereInNumList = JdbcUtils.formatAsCommaSeparatedNumbersString(al);
			String sql = String.format("update PPADM.PPT_WEB_UPRAWNIENIA set WUPR_CZY_WYSWIETLAC = ?, WUPR_CZY_EDYTOWALNE = ?  where wupr_id in ( %1$s )", whereInNumList);
			JdbcUtils.sqlExecNoQuery(sql, wystwietl, edytuj);
			User.info("liczba zmodyfikowanych wpisów: %1$s", al.size());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}

		for ( Long id: al ) {
			reloadAndUpdate(lw.getShortName(), id);
		}
	}



	public boolean isViewRenderKU(){
        String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
        //System.out.println(viewId);
       	if 			("/faces/KontrolaUprawnien/KontrolaUprawnien.xhtml".equals(viewId)) return false;
       	else if ("/faces/Administrator/ZmianaUzytkownikaLista.xhtml".equals(viewId)) return true;
       	else return !viewId.startsWith("/faces/Administrator/");
    }



}


