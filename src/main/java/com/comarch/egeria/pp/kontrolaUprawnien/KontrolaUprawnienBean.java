package com.comarch.egeria.pp.kontrolaUprawnien;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.kontrolaUprawnien.model.WebUprawnienia;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

/**
 * @author CA
 *
 */
@Named
@Scope("view")
public class KontrolaUprawnienBean extends SqlBean {

	private static final long serialVersionUID = 1L;
	private static final Logger log = LoggerFactory.getLogger(KontrolaUprawnienBean.class);
	
	@PostConstruct
	public void init(){
		log.debug("KontrolaUprawnienBean created");
	}
	
	public SqlDataSelectionsHandler getPplWebUprawnienia() {
		return this.getSql("PPL_WEB_UPRAWNIENIA");
	}
	
	public void dodaj() {
		log.debug("dodaj");
	}
	
	public void usun() {
		log.debug("usun");
		
		SqlDataSelectionsHandler lw = this.getPplWebUprawnienia();
		
		if (lw==null || lw.getSelectedRows().isEmpty()) 
			return;
		

		ArrayList<Long> al = new ArrayList<Long>();
		for (DataRow r : lw.getSelectedRows()) {
			//al.add(r.getIdAsLong()); 
			al.add( ((BigDecimal) r.get("wupr_id") ).longValue() );
		}
		
		try {
			String whereInNumList = JdbcUtils.formatAsCommaSeparatedNumbersString(al);
			String sql = String.format("delete from PPADM.PPT_WEB_UPRAWNIENIA where wupr_id in ( %1$s )", whereInNumList);
			JdbcUtils.sqlExecNoQuery(sql);
//			lw.setTextFilter(lw.getTextFilter());
//			lw.clearSelection();
//			lw.resetTs();

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}

		for ( Long id: al ) {
			reloadAndUpdate(lw.getShortName(), id);
		}




//		List<WebUprawnienia> allWupr;
//		try (HibernateContext h = new HibernateContext()) {
//			allWupr = h.query("FROM WebUprawnienia");
//		}
//
//		ConcurrentHashMap<Long, WebUprawnienia> uprawnieniaIdWebUprawnieniaHM = new ConcurrentHashMap<>();
//		for (WebUprawnienia wupr : allWupr) {
//			uprawnieniaIdWebUprawnieniaHM.put(wupr.getWuprId(), wupr);
//		}

//		try (HibernateContext h = new HibernateContext()) {
//			Session s = h.getSession();
//			WebUprawnienia webUprawnienia = null;
//			for (Object id : al) {
//				webUprawnienia = uprawnieniaIdWebUprawnieniaHM.get(id);
//				if(webUprawnienia != null) {
//					s.delete(webUprawnienia);
//					uprawnieniaIdWebUprawnieniaHM.remove(id);
//					log.debug("{}",id);
//				}
//			}
//			s.beginTransaction().commit();
//		}
		
	}


	
}
