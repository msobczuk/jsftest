package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.web.common.hibernate.HibernateContext;

import javax.persistence.Entity;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@Entity
public class DbObjPackageBody extends DbObject {

    private static final String OBJECT_TYPE = "PACKAGE BODY";
    private static final String OBJECT_FOLDER = "packages";
    private static final String FILE_EXTENSION = ".pcb";

    @Override
    public String getObjectFolder() {
        return OBJECT_FOLDER;
    }

    @Override
    public String getFileExtension() {
        return FILE_EXTENSION;
    }


    public DbObjPackageBody() {
        this.setObjpObjectType(OBJECT_TYPE);
    }

    public static void loadStdVersionFromResources(HibernateContext h, boolean recompileCodeOrReimportDML) {
        System.out.println("... Ciała pakietów ...");
        loadStdVersionFromResources(h, DbObjPackageBody::new, null, FILE_EXTENSION, OBJECT_FOLDER, recompileCodeOrReimportDML);
    }

    public static boolean resetToStdVersion(String name) {
        return DbObject.recompileOrResetToStdVersion(name, OBJECT_FOLDER, name.toLowerCase() + FILE_EXTENSION);
    }

    @Override
    public void tryResetOrRecompileDbCode(Connection conn) throws IOException, SQLException {
        DbObject.tryExecuteResourceSqlScript(conn, this.getObjpName(), OBJECT_FOLDER, this.getObjpName().toLowerCase() + FILE_EXTENSION);
    }
}
