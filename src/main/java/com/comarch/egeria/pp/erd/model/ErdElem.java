package com.comarch.egeria.pp.erd.model;

import javax.persistence.Entity;
import java.util.HashSet;
import java.util.List;

@Entity
public class ErdElem extends PptErdElem {
//klasa logiczna/domenowa dla persystencji PptErdElem
//przeznaczenie: logika biznesowa dla modelu ErdElem


    public Erd getErd(){
        return (Erd) this.getPptErd();
    }

    public Tab getTable(){
        return (Tab) this.getPptTable();
    }


    public String getTableName(){
        if (this.getPptTable()==null) return null;
        return this.getPptTable().getTbTableName();
    }

    public String getEntityName(){
        if (this.getPptTable()==null) return null;
        return this.getPptTable().getTbEntityName();
    }


    public void test(){

        HashSet<ErdElem> ret = getFkErdElems();
        if (ret == null) return;

        System.out.println(ret  );
    }

    public HashSet<ErdElem> getFkErdElems() {
        HashSet<ErdElem> ret = new HashSet();
        if ( this.getTable()==null || this.getErd()==null) return ret;

        List<Long> fkColsIds = this.getTable().getFkTabColsIdList();
        for (ErdElem el : this.getErd().getErdElems()) {
            long cnt = el.getTable().getTabCols().stream().filter(c -> fkColsIds.contains(c.getTcId())).count();
            if (cnt>0) {
                ret.add(el);
            }
        }

        return ret;
    }

}
