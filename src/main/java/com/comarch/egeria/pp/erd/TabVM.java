package com.comarch.egeria.pp.erd;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.erd.model.Tab;
import com.comarch.egeria.pp.erd.model.TabCol;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Named
@Scope("view")
public class TabVM extends SqlBean {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LogManager.getLogger();


    Tab model = null;

    @PostConstruct
    public void init(){
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        this.setModel((Tab) HibernateContext.get(Tab.class, params.get("tb_id")));
        if (this.model ==null) model = new Tab();

        com.comarch.egeria.Utils.executeScript("document.title = '("+ this.model.getTbColumnPrefix() + ") "+ this.model.getTbTableName() +"'");

    }


    public Tab getModel() {
        return model;
    }

    public void setModel(Tab model) {
        this.model = model;
    }




    //    @Transient
    String kolFilterExpr;
    public String getKolFilterExpr() {
        this.getModel().onRXGet("kolFilterExpr");
        return kolFilterExpr;
    }
    public void setKolFilterExpr(String kolFilterExpr) {
        this.getModel().onRXSet("kolFilterExpr", kolFilterExpr, this.kolFilterExpr);
        this.kolFilterExpr = kolFilterExpr;
    }
    public List<TabCol> getTabColsFiltered(){
        String kolFilterExpr = this.getKolFilterExpr();
        if (kolFilterExpr ==null|| kolFilterExpr.isEmpty()) return this.getModel().getTabCols();
        String[] fltrExpWords = kolFilterExpr.toLowerCase().split(" ");
        return this.getModel().getTabCols().stream().filter(c-> Utils.matchFilterExpressionsWordsObProperties(
                fltrExpWords,  c.getTcPropertyName(), c.getTcColumnName(), c.getTcDataType(), c.getTcDesc())
        ).collect(Collectors.toList());
    }


}
