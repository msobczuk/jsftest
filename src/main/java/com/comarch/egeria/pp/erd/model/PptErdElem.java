package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.pp.data.model.ModelBase;
import org.hibernate.annotations.DiscriminatorFormula;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the PPT_ERD_ELEMS database table.
 * 
 */
@Entity
@Table(name="PPT_ERD_ELEMS", schema="PPADM")
@NamedQuery(name="PptErdElem.findAll", query="SELECT p FROM PptErdElem p")
@DiscriminatorFormula("'ErdElem'")
public class PptErdElem extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ERD_ELEMS",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ERD_ELEMS", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ERD_ELEMS")
	@Column(name="TDE_ID")
	private long tdeId;

	@Temporal(TemporalType.DATE)
	@Column(name="TDE_AUDYT_DM")
	private Date tdeAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="TDE_AUDYT_DT")
	private Date tdeAudytDt;

	@Column(name="TDE_AUDYT_LM")
	private String tdeAudytLm;

	@Column(name="TDE_AUDYT_UM")
	private String tdeAudytUm;

	@Column(name="TDE_AUDYT_UT")
	private String tdeAudytUt;

	@Column(name="TDE_X")
	private Double tdeX;

	@Column(name="TDE_Y")
	private Double tdeY;
	
	//bi-directional many-to-one association to PptTable
	@ManyToOne
	@JoinColumn(name="TDE_TD_ID")
	private PptErd pptErd;

	//bi-directional many-to-one association to PptTabCol
	@ManyToOne
	@JoinColumn(name="TDE_TC_ID")
	private PptTabCol pptTabCol;

	//bi-directional many-to-one association to PptTable
	@ManyToOne
	@JoinColumn(name="TDE_TB_ID")
	private PptTable pptTable;

	public PptErdElem() {
	}

	public long getTdeId() {
		this.onRXGet("tdeId");
		return this.tdeId;
	}

	public void setTdeId(long tdeId) {
		this.onRXSet("tdeId", this.tdeId, tdeId);
		this.tdeId = tdeId;
	}

	public Date getTdeAudytDm() {
		this.onRXGet("tdeAudytDm");
		return this.tdeAudytDm;
	}

	public void setTdeAudytDm(Date tdeAudytDm) {
		this.onRXSet("tdeAudytDm", this.tdeAudytDm, tdeAudytDm);
		this.tdeAudytDm = tdeAudytDm;
	}

	public Date getTdeAudytDt() {
		this.onRXGet("tdeAudytDt");
		return this.tdeAudytDt;
	}

	public void setTdeAudytDt(Date tdeAudytDt) {
		this.onRXSet("tdeAudytDt", this.tdeAudytDt, tdeAudytDt);
		this.tdeAudytDt = tdeAudytDt;
	}

	public String getTdeAudytLm() {
		this.onRXGet("tdeAudytLm");
		return this.tdeAudytLm;
	}

	public void setTdeAudytLm(String tdeAudytLm) {
		this.onRXSet("tdeAudytLm", this.tdeAudytLm, tdeAudytLm);
		this.tdeAudytLm = tdeAudytLm;
	}

	public String getTdeAudytUm() {
		this.onRXGet("tdeAudytUm");
		return this.tdeAudytUm;
	}

	public void setTdeAudytUm(String tdeAudytUm) {
		this.onRXSet("tdeAudytUm", this.tdeAudytUm, tdeAudytUm);
		this.tdeAudytUm = tdeAudytUm;
	}

	public String getTdeAudytUt() {
		this.onRXGet("tdeAudytUt");
		return this.tdeAudytUt;
	}

	public void setTdeAudytUt(String tdeAudytUt) {
		this.onRXSet("tdeAudytUt", this.tdeAudytUt, tdeAudytUt);
		this.tdeAudytUt = tdeAudytUt;
	}

	public Double getTdeX() {
		this.onRXGet("tdeX");
		return this.tdeX;
	}

	public void setTdeX(Double tdeX) {
		this.onRXSet("tdeX", this.tdeX, tdeX);
		this.tdeX = tdeX;
	}

	public Double getTdeY() {
		this.onRXGet("tdeY");
		return this.tdeY;
	}

	public void setTdeY(Double tdeY) {
		this.onRXSet("tdeY", this.tdeY, tdeY);
		this.tdeY = tdeY;
	}
	
	public PptErd getPptErd() {
		this.onRXGet("pptErd");
		return this.pptErd;
	}

	public void setPptErd(PptErd pptErd) {
		this.onRXSet("pptErd", this.pptErd, pptErd);
		this.pptErd = pptErd;
	}

	public PptTabCol getPptTabCol() {
		this.onRXGet("pptTabCol");
		return this.pptTabCol;
	}

	public void setPptTabCol(PptTabCol pptTabCol) {
		this.onRXSet("pptTabCol", this.pptTabCol, pptTabCol);
		this.pptTabCol = pptTabCol;
	}

	public PptTable getPptTable() {
		this.onRXGet("pptTable");
		return this.pptTable;
	}

	public void setPptTable(PptTable pptTable) {
		this.onRXSet("pptTable", this.pptTable, pptTable);
		this.pptTable = pptTable;
	}

}
