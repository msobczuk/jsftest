package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.pp.data.model.ModelBase;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_TAB_COLS database table.
 * 
 */
@Entity
@Table(name="PPT_TAB_COLS", schema="PPADM")
@NamedQuery(name="PptTabCol.findAll", query="SELECT p FROM PptTabCol p")
@DiscriminatorFormula("'TabCol'")
public class PptTabCol extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_TAB_COLS",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_TAB_COLS", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_TAB_COLS")
	@Column(name="TC_ID")
	private long tcId;

	@Temporal(TemporalType.DATE)
	@Column(name="TC_AUDYT_DM")
	private Date tcAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="TC_AUDYT_DT")
	private Date tcAudytDt;

	@Column(name="TC_AUDYT_LM")
	private String tcAudytLm;

	@Column(name="TC_AUDYT_UM")
	private String tcAudytUm;

	@Column(name="TC_AUDYT_UT")
	private String tcAudytUt;

	@Column(name="TC_COLUMN_NAME")
	private String tcColumnName;

	@Column(name="TC_DATA_TYPE")
	private String tcDataType;

	@Column(name="TC_DESC")
	private String tcDesc;

	@Column(name="TC_FK_TC_ID")
	private Long tcFkTcId;

	@Column(name="TC_IX_NAME")
	private String tcIxName;

	@Column(name="TC_NULLABLE")
	private String tcNullable;

	@Column(name="TC_PROPERTY_NAME")
	private String tcPropertyName;

	@Column(name="TC_PROPERTY_TYPE")
	private String tcPropertyType;

	@Column(name="TC_UK_NAME")
	private String tcUkName;
	
	@Column(name="TC_DEFAULT")
	private String tcDefault;
	
	@Column(name="TC_STATUS")
	private String tcStatus;

	//bi-directional many-to-one association to PptTable
	@ManyToOne
	@JoinColumn(name="TC_TB_ID")
	private PptTable pptTable;

	//bi-directional many-to-one association to PptErdElem
	@OneToMany(mappedBy="pptTabCol", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("TDE_ID ASC")
	private List<PptErdElem> pptErdElems = new ArrayList<>();

	public PptTabCol() {
	}

	public long getTcId() {
		this.onRXGet("tcId");
		return this.tcId;
	}

	public void setTcId(long tcId) {
		this.onRXSet("tcId", this.tcId, tcId);
		this.tcId = tcId;
	}

	public Date getTcAudytDm() {
		this.onRXGet("tcAudytDm");
		return this.tcAudytDm;
	}

	public void setTcAudytDm(Date tcAudytDm) {
		this.onRXSet("tcAudytDm", this.tcAudytDm, tcAudytDm);
		this.tcAudytDm = tcAudytDm;
	}

	public Date getTcAudytDt() {
		this.onRXGet("tcAudytDt");
		return this.tcAudytDt;
	}

	public void setTcAudytDt(Date tcAudytDt) {
		this.onRXSet("tcAudytDt", this.tcAudytDt, tcAudytDt);
		this.tcAudytDt = tcAudytDt;
	}

	public String getTcAudytLm() {
		this.onRXGet("tcAudytLm");
		return this.tcAudytLm;
	}

	public void setTcAudytLm(String tcAudytLm) {
		this.onRXSet("tcAudytLm", this.tcAudytLm, tcAudytLm);
		this.tcAudytLm = tcAudytLm;
	}

	public String getTcAudytUm() {
		this.onRXGet("tcAudytUm");
		return this.tcAudytUm;
	}

	public void setTcAudytUm(String tcAudytUm) {
		this.onRXSet("tcAudytUm", this.tcAudytUm, tcAudytUm);
		this.tcAudytUm = tcAudytUm;
	}

	public String getTcAudytUt() {
		this.onRXGet("tcAudytUt");
		return this.tcAudytUt;
	}

	public void setTcAudytUt(String tcAudytUt) {
		this.onRXSet("tcAudytUt", this.tcAudytUt, tcAudytUt);
		this.tcAudytUt = tcAudytUt;
	}

	public String getTcColumnName() {
		this.onRXGet("tcColumnName");
		return this.tcColumnName;
	}

	public void setTcColumnName(String tcColumnName) {
		this.onRXSet("tcColumnName", this.tcColumnName, tcColumnName);
		this.tcColumnName = tcColumnName;
	}

	public String getTcDataType() {
		this.onRXGet("tcDataType");
		return this.tcDataType;
	}

	public void setTcDataType(String tcDataType) {
		this.onRXSet("tcDataType", this.tcDataType, tcDataType);
		this.tcDataType = tcDataType;
	}

	public String getTcDesc() {
		this.onRXGet("tcDesc");
		return this.tcDesc;
	}

	public void setTcDesc(String tcDesc) {
		this.onRXSet("tcDesc", this.tcDesc, tcDesc);
		this.tcDesc = tcDesc;
	}

	public Long getTcFkTcId() {
		this.onRXGet("tcFkTcId");
		return this.tcFkTcId;
	}

	public void setTcFkTcId(Long tcFkTcId) {
		this.onRXSet("tcFkTcId", this.tcFkTcId, tcFkTcId);
		this.tcFkTcId = tcFkTcId;
	}

	public String getTcIxName() {
		this.onRXGet("tcIxName");
		return this.tcIxName;
	}

	public void setTcIxName(String tcIxName) {
		this.onRXSet("tcIxName", this.tcIxName, tcIxName);
		this.tcIxName = tcIxName;
	}

	public String getTcNullable() {
		this.onRXGet("tcNullable");
		return this.tcNullable;
	}

	public void setTcNullable(String tcNullable) {
		this.onRXSet("tcNullable", this.tcNullable, tcNullable);
		this.tcNullable = tcNullable;
	}

	public String getTcPropertyName() {
		this.onRXGet("tcPropertyName");
		return this.tcPropertyName;
	}

	public void setTcPropertyName(String tcPropertyName) {
		this.onRXSet("tcPropertyName", this.tcPropertyName, tcPropertyName);
		this.tcPropertyName = tcPropertyName;
	}

	public String getTcPropertyType() {
		this.onRXGet("tcPropertyType");
		return this.tcPropertyType;
	}

	public void setTcPropertyType(String tcPropertyType) {
		this.onRXSet("tcPropertyType", this.tcPropertyType, tcPropertyType);
		this.tcPropertyType = tcPropertyType;
	}

	public String getTcUkName() {
		this.onRXGet("tcUkName");
		return this.tcUkName;
	}

	public void setTcUkName(String tcUkName) {
		this.onRXSet("tcUkName", this.tcUkName, tcUkName);
		this.tcUkName = tcUkName;
	}

	public PptTable getPptTable() {
		this.onRXGet("pptTable");
		return this.pptTable;
	}

	public void setPptTable(PptTable pptTable) {
		this.onRXSet("pptTable", this.pptTable, pptTable);
		this.pptTable = pptTable;
	}

	public List<PptErdElem> getPptErdElems() {
		this.onRXGet("pptErdElems");
		return this.pptErdElems;
	}

	public void setPptErdElems(List<PptErdElem> pptErdElems) {
		this.onRXSet("pptErdElems", this.pptErdElems, pptErdElems);
		this.pptErdElems = pptErdElems;
	}

	public PptErdElem addPptErdElem(PptErdElem pptErdElem) {
		getPptErdElems().add(pptErdElem);
		pptErdElem.setPptTabCol(this);

		return pptErdElem;
	}

	public PptErdElem removePptErdElem(PptErdElem pptErdElem) {
		getPptErdElems().remove(pptErdElem);
		pptErdElem.setPptTabCol(null);

		return pptErdElem;
	}
	
	public String getTcDefault() {
		this.onRXGet("tcDefault");
		return this.tcDefault;
	}

	public void setTcDefault(String tcDefault) {
		this.onRXSet("tcDefault", this.tcDefault, tcDefault);
		this.tcDefault = tcDefault;
	}
	
	public String getTcStatus() {
		this.onRXGet("tcStatus");
		return this.tcStatus;
	}

	public void setTcStatus(String tcStatus) {
		this.onRXSet("tcStatus", this.tcStatus, tcStatus);
		this.tcStatus = tcStatus;
	}

}
