package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

import javax.faces.context.FacesContext;
import javax.persistence.Entity;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Entity
public class DbObjView extends DbObject {

    private static final String OBJECT_TYPE = "VIEW";
    private static final String OBJECT_FOLDER = "views";
    private static final String FILE_EXTENSION = ".vw";

    @Override
    public String getObjectFolder() {
        return OBJECT_FOLDER;
    }

    @Override
    public String getFileExtension() {
        return FILE_EXTENSION;
    }

    public DbObjView() {
        this.setObjpObjectType(OBJECT_TYPE);
    }

    public static void loadStdVersionFromResources(HibernateContext h, boolean recompileCodeOrReimportDML) {
        System.out.println("... Widoki ...");
        loadStdVersionFromResources(h, DbObjView::new, null, FILE_EXTENSION, OBJECT_FOLDER, recompileCodeOrReimportDML);
    }

    public static boolean resetToStdVersion(String name) {
        return DbObject.recompileOrResetToStdVersion(name, OBJECT_FOLDER, name.toLowerCase() + FILE_EXTENSION);
    }


    @Override
    public void tryResetOrRecompileDbCode(Connection conn) throws IOException, SQLException {
        if (FacesContext.getCurrentInstance()!=null) {
            final SqlDataSelectionsHandler lw = getLW("PPL_ALL_VIEW_TEXT");
            if (lw != null) {
                lw.resetTs();
            }
        }
        DbObject.tryExecuteResourceSqlScript(conn, this.getObjpName(), OBJECT_FOLDER, this.getObjpName().toLowerCase() + FILE_EXTENSION);
    }

    @Override
    public void makeFirstLineLikeDBAllSource(){ //polimorficzna

        if (this.getPptAllSources().isEmpty()) return;
        PptAllSource l = this.getPptAllSources().get(0);
        String txt = l.getSouText();
        if (txt==null || txt.isEmpty()) return;
        txt = txt.substring(txt.toUpperCase().indexOf(this.getObjpObjectType().toUpperCase())); //odcięcie 'create or replace '<TYP> (albo samego 'create '<TYP>)

//        txt = txt.replace("\"PPADM\".", "        "); //zamiana "PPADM". na  8 spacji
//        txt = txt.replace("PPADM.", "      ");   //zamiana PPADM. na  6 spacji
//
//        txt = txt.replace("\"ppadm\".", "        "); //zamiana "PPADM". na  8 spacji
//        txt = txt.replace("ppadm.", "      ");   //zamiana PPADM. na  6 spacji
//
//        txt = txt.replace("\"Ppadm\".", "        "); //zamiana "PPADM". na  8 spacji
//        txt = txt.replace("Ppadm.", "      ");   //zamiana PPADM. na  6 spacji

        l.setSouText(txt);
    }


    public static ConcurrentHashMap<String, DbObjView> allResFilesDbObjViewsCHM = new ConcurrentHashMap<>();
    public static ConcurrentHashMap<String, DbObjView> getAllResFilesDbObjViewsCHM() throws Exception {
//        Utils.getAbsolutePath(SQL_RESOURCES_PATH, OBJECT_FOLDER);
        if (allResFilesDbObjViewsCHM.isEmpty()) {
            List<Path> resourcePaths = getSqlResoucePaths(null, FILE_EXTENSION, OBJECT_FOLDER);
            for (Path resourcePath : resourcePaths) {
                String filename = resourcePath.toFile().getName();
                String dbObjectName = DbObject.getDbObjectName(filename, null, FILE_EXTENSION).toUpperCase();
                DbObjView obj = new DbObjView();
                obj.setObjpName(dbObjectName);
                obj.setResourceLocation(Paths.get(SQL_RESOURCES_PATH, OBJECT_FOLDER, filename).toString());
                obj.loadResourceFileLines2PptAllSources();
                String resourceVersion = obj.getResourceVersion();
                obj.setObjpVersion(resourceVersion);
                allResFilesDbObjViewsCHM.put(obj.getObjpOwner()+"."+obj.getObjpName(), obj);
            }
        }
        return allResFilesDbObjViewsCHM;
    }
}
