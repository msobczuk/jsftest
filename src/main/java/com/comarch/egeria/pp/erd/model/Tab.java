package com.comarch.egeria.pp.erd.model;

import javax.persistence.Entity;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Tab extends PptTable{



    public List<TabCol> getTabCols(){
        return (List<TabCol>)(List<?>) this.getPptTabCols();
    }

    public List<TabCol> getFkTabCols() {
        return this.getTabCols().stream().filter(c->c.getTcFkTcId()!=null).collect(Collectors.toList());
    }

    public List<Long> getFkTabColsIdList() {
      return this.getFkTabCols().stream().mapToLong(c->c.getTcFkTcId()).boxed().collect(Collectors.toList());
    }

    public List<Long> getTabColsIdList() {
        return this.getTabCols().stream().mapToLong(c->c.getTcId()).boxed().collect(Collectors.toList());
    }

    public void addNewTabCol(){
        TabCol col = new TabCol();
        this.addPptTabCol(col);
    }







//kod kolFilterExpr i getTabColsFiltered przeniesiono do TabVM
//    @Transient
//    String kolFilterExpr;
//    public String getKolFilterExpr() {
//        onRXGet("kolFilterExpr");
//        return kolFilterExpr;
//    }
//    public void setKolFilterExpr(String kolFilterExpr) {
//        onRXSet("kolFilterExpr", kolFilterExpr, this.kolFilterExpr);
//        this.kolFilterExpr = kolFilterExpr;
//    }
//    public List<TabCol> getTabColsFiltered(){
//        String kolFilterExpr = this.getKolFilterExpr();
//        if (kolFilterExpr ==null|| kolFilterExpr.isEmpty()) return this.getTabCols();
//        String[] fltrExpWords = kolFilterExpr.toLowerCase().split(" ");
//        return this.getTabCols().stream().filter(c-> Utils.matchFilterExpressionsWordsObProperties(
//                fltrExpWords,  c.getTcPropertyName(), c.getTcColumnName(), c.getTcDataType(), c.getTcDesc())
//        ).collect(Collectors.toList());
//    }






    public void test(){
//        getFkTabCols();
//        getFkTabColsIdList();
    }


}
