package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.web.common.hibernate.HibernateContext;

import javax.persistence.Entity;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@Entity
public class DbObjProcedure extends DbObject {

    private static final String OBJECT_TYPE = "PROCEDURE";
    private static final String OBJECT_FOLDER = "procedures";
    private static final String FILE_EXTENSION = ".prc";



    @Override
    public String getObjectFolder() {
        return OBJECT_FOLDER;
    }

    @Override
    public String getFileExtension() {
        return FILE_EXTENSION;
    }

    public DbObjProcedure() {
        this.setObjpObjectType(OBJECT_TYPE);
    }

    public static void loadStdVersionFromResources(HibernateContext h, boolean recompileCodeOrReimportDML) {
        System.out.println("... Procedury ...");
        loadStdVersionFromResources(h, DbObjProcedure::new, null, FILE_EXTENSION, OBJECT_FOLDER, recompileCodeOrReimportDML);
    }

    public static boolean resetToStdVersion(String name) {
        return DbObject.recompileOrResetToStdVersion(name, OBJECT_FOLDER, name.toLowerCase() + FILE_EXTENSION);
    }


    @Override
    public void tryResetOrRecompileDbCode(Connection conn) throws IOException, SQLException {
        DbObject.tryExecuteResourceSqlScript(conn, this.getObjpName(), OBJECT_FOLDER, this.getObjpName().toLowerCase() + FILE_EXTENSION);
    }
}
