package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.pp.data.model.ModelBase;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_TABLES database table.
 * 
 */
@Entity
@Table(name="PPT_TABLES", schema="PPADM")
@NamedQuery(name="PptTable.findAll", query="SELECT p FROM PptTable p")
@DiscriminatorFormula("'Tab'")
public class PptTable extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_TABLES",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_TABLES", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_TABLES")
	@Column(name="TB_ID")
	private long tbId;

	@Temporal(TemporalType.DATE)
	@Column(name="TB_AUDYT_DM")
	private Date tbAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="TB_AUDYT_DT")
	private Date tbAudytDt;

	@Column(name="TB_AUDYT_LM")
	private String tbAudytLm;

	@Column(name="TB_AUDYT_UM")
	private String tbAudytUm;

	@Column(name="TB_AUDYT_UT")
	private String tbAudytUt;

	@Column(name="TB_COLUMN_PREFIX")
	private String tbColumnPrefix;

	@Column(name="TB_DESC")
	private String tbDesc;

	@Column(name="TB_ENTITY_NAME")
	private String tbEntityName;

	@Column(name="TB_OWNER")
	private String tbOwner;

	@Column(name="TB_TABLE_NAME")
	private String tbTableName;
	
	@Column(name="TB_STATUS")
	private String tbStatus;

	//bi-directional many-to-one association to PptTabCol
	@OneToMany(mappedBy="pptTable", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("TC_ID ASC")
	private List<PptTabCol> pptTabCols = new ArrayList<>();

	//bi-directional many-to-one association to PptErdElem
	@OneToMany(mappedBy="pptTable", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("TDE_ID ASC")
	private List<PptErdElem> pptErdElems = new ArrayList<>();

	public PptTable() {
	}

	public long getTbId() {
		this.onRXGet("tbId");
		return this.tbId;
	}

	public void setTbId(long tbId) {
		this.onRXSet("tbId", this.tbId, tbId);
		this.tbId = tbId;
	}

	public Date getTbAudytDm() {
		this.onRXGet("tbAudytDm");
		return this.tbAudytDm;
	}

	public void setTbAudytDm(Date tbAudytDm) {
		this.onRXSet("tbAudytDm", this.tbAudytDm, tbAudytDm);
		this.tbAudytDm = tbAudytDm;
	}

	public Date getTbAudytDt() {
		this.onRXGet("tbAudytDt");
		return this.tbAudytDt;
	}

	public void setTbAudytDt(Date tbAudytDt) {
		this.onRXSet("tbAudytDt", this.tbAudytDt, tbAudytDt);
		this.tbAudytDt = tbAudytDt;
	}

	public String getTbAudytLm() {
		this.onRXGet("tbAudytLm");
		return this.tbAudytLm;
	}

	public void setTbAudytLm(String tbAudytLm) {
		this.onRXSet("tbAudytLm", this.tbAudytLm, tbAudytLm);
		this.tbAudytLm = tbAudytLm;
	}

	public String getTbAudytUm() {
		this.onRXGet("tbAudytUm");
		return this.tbAudytUm;
	}

	public void setTbAudytUm(String tbAudytUm) {
		this.onRXSet("tbAudytUm", this.tbAudytUm, tbAudytUm);
		this.tbAudytUm = tbAudytUm;
	}

	public String getTbAudytUt() {
		this.onRXGet("tbAudytUt");
		return this.tbAudytUt;
	}

	public void setTbAudytUt(String tbAudytUt) {
		this.onRXSet("tbAudytUt", this.tbAudytUt, tbAudytUt);
		this.tbAudytUt = tbAudytUt;
	}

	public String getTbColumnPrefix() {
		this.onRXGet("tbColumnPrefix");
		return this.tbColumnPrefix;
	}

	public void setTbColumnPrefix(String tbColumnPrefix) {
		this.onRXSet("tbColumnPrefix", this.tbColumnPrefix, tbColumnPrefix);
		this.tbColumnPrefix = tbColumnPrefix;
	}

	public String getTbDesc() {
		this.onRXGet("tbDesc");
		return this.tbDesc;
	}

	public void setTbDesc(String tbDesc) {
		this.onRXSet("tbDesc", this.tbDesc, tbDesc);
		this.tbDesc = tbDesc;
	}

	public String getTbEntityName() {
		this.onRXGet("tbEntityName");
		return this.tbEntityName;
	}

	public void setTbEntityName(String tbEntityName) {
		this.onRXSet("tbEntityName", this.tbEntityName, tbEntityName);
		this.tbEntityName = tbEntityName;
	}

	public String getTbOwner() {
		this.onRXGet("tbOwner");
		return this.tbOwner;
	}

	public void setTbOwner(String tbOwner) {
		this.onRXSet("tbOwner", this.tbOwner, tbOwner);
		this.tbOwner = tbOwner;
	}

	public String getTbTableName() {
		this.onRXGet("tbTableName");
		return this.tbTableName;
	}

	public void setTbTableName(String tbTableName) {
		this.onRXSet("tbTableName", this.tbTableName, tbTableName);
		this.tbTableName = tbTableName;
	}

	public List<PptTabCol> getPptTabCols() {
		this.onRXGet("pptTabCols");
		return this.pptTabCols;
	}

	public void setPptTabCols(List<PptTabCol> pptTabCols) {
		this.onRXSet("pptTabCols", this.pptTabCols, pptTabCols);
		this.pptTabCols = pptTabCols;
	}

	public PptTabCol addPptTabCol(PptTabCol pptTabCol) {
		this.onRXSet("pptTabCols", null, pptTabCols);
		getPptTabCols().add(pptTabCol);
		pptTabCol.setPptTable(this);

		return pptTabCol;
	}

	public PptTabCol removePptTabCol(PptTabCol pptTabCol) {
		this.onRXSet("pptTabCols", null, pptTabCols);
		getPptTabCols().remove(pptTabCol);
		pptTabCol.setPptTable(null);

		return pptTabCol;
	}

	public List<PptErdElem> getPptErdElems() {
		this.onRXGet("pptErdElems");
		return this.pptErdElems;
	}

	public void setPptErdElems(List<PptErdElem> pptErdElems) {
		this.onRXSet("pptErdElems", this.pptErdElems, pptErdElems);
		this.pptErdElems = pptErdElems;
	}

	public PptErdElem addPptErdElem(PptErdElem pptErdElem) {
		getPptErdElems().add(pptErdElem);
		pptErdElem.setPptTable(this);

		return pptErdElem;
	}

	public PptErdElem removePptErdElem(PptErdElem pptErdElem) {
		getPptErdElems().remove(pptErdElem);
		pptErdElem.setPptTable(null);

		return pptErdElem;
	}
	
	public String getTbStatus() {
		this.onRXGet("tbStatus");
		return this.tbStatus;
	}

	public void setTbStatus(String tbStatus) {
		this.onRXSet("tbStatus", this.tbStatus, tbStatus);
		this.tbStatus = tbStatus;
	}

}
