package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.Params;
import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Transaction;
import org.springframework.core.io.Resource;

import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.nz;

@Entity
public abstract class DbObject extends PptObject {
    private static final Logger log = LogManager.getLogger();

    public static final String DEFAULT_DB_OBJECT_OWNER = "PPADM";
    public static final String SQL_RESOURCES_PATH = "/WEB-INF/protectedElements/SQL/";
//    private static final String HQL_QUERY = "from DbObject where OBJP_OBJECT_TYPE = ?";
    public static final String CHARSET_NAME = "Cp1250";

    public static Params fileExtensions = new Params()
        .add("PROCEDURE",".prc")
        .add("FUNCTION", ".fun") //fnc
        .add("VIEW", ".vw")
        .add("PACKAGE", ".pcs")
        .add("PACKAGE BODY", ".pcb")
        .add("TRIGGER", ".trg");


    public static HashMap<String, Supplier<? extends DbObject> > suppliers = suppliersInit();

    public static HashMap<String, Supplier<? extends DbObject> > suppliersInit(){
        HashMap<String, Supplier<? extends DbObject> > ret =  new HashMap<>();
        ret.put("PROCEDURE",DbObjProcedure::new);
        ret.put("FUNCTION",DbObjFunction::new);
        ret.put("VIEW",DbObjView::new);
        ret.put("PACKAGE",DbObjPackage::new);
        ret.put("PACKAGE BODY",DbObjPackageBody::new);
        ret.put("TRIGGER",DbObjTrigger::new);
        ret.put("SL",DbObjSlownik::new);
        ret.put("LW",DbObjListaWartosci::new);
        return ret;
    }

    public static DbObject getNewInstance(String type){
        final Supplier<? extends DbObject> supplier = suppliers.get(type);
        if (supplier==null) return null;
        return supplier.get();
    }

    public static String getFileName(String owner, String objectName, String objectType){
        String ret = objectName.toLowerCase() + fileExtensions.get(objectType);
        if (owner!=null && !SqlBean.eq("PPADM", owner.toUpperCase())){
            ret = owner.toUpperCase() + "." + ret;
        }
        return ret;
    }

    public static void loadAllResourceFiles2PptObjects(boolean doRecompileOrResetToSTd){
        try (HibernateContext h = new HibernateContext()) {

            DbObjPackage.loadStdVersionFromResources(h, doRecompileOrResetToSTd);
            DbObjView.loadStdVersionFromResources(h, doRecompileOrResetToSTd);
            DbObjProcedure.loadStdVersionFromResources(h, doRecompileOrResetToSTd);
            DbObjFunction.loadStdVersionFromResources(h, doRecompileOrResetToSTd);
            DbObjPackageBody.loadStdVersionFromResources(h, doRecompileOrResetToSTd);
            DbObjTrigger.loadStdVersionFromResources(h, doRecompileOrResetToSTd);

            DbObjListaWartosci.loadStdVersionFromResources(h, doRecompileOrResetToSTd);
            DbObjSlownik.loadStdVersionFromResources(h, doRecompileOrResetToSTd);//słowniki w większosci mają flagę AA = 'N', więc nie nadpisujemy ich automatycznie, ale wybranym mozna włączyc AA
        }
    }


    @Transient
    String resourceLocation = null;

    @Transient
    String resourceVersion = null;

    public DbObject() {
        setObjpOwner(DEFAULT_DB_OBJECT_OWNER);
    }

    protected static void loadStdVersionFromResources(
            HibernateContext h,
            Supplier<? extends DbObject> newDbObject,
            String filenamePrefix,
            String fileExtension,
            String objectFolder,
            boolean recompileCodeOrReimportDML
    ) {
        DbObject o = newDbObject.get();

        SqlDataSelectionsHandler lw = AppBean.sql("sqlpptobjects"
                , "SELECT " +
                            " upper(OBJP_OBJECT_TYPE||' '||OBJP_NAME) ID, " + //wazne PACKAGE BODY oraz PACKAGE mają identyczne nazwy
                            " OBJP_ID, OBJP_VERSION, " +
                            " OBJP_OWNER, " +
                            " OBJP_OBJECT_TYPE, " +
                            " OBJP_F_AUTO_AKT " +
                        "FROM PPT_OBJECTS where OBJP_OBJECT_TYPE = ? ",  o.getObjpObjectType()
        );

        try {

            List<Path> resourcePaths = getSqlResoucePaths(filenamePrefix, fileExtension, objectFolder);
            DbObject obj = null;

            for (Path resourcePath : resourcePaths) {

                    obj = newDbObject.get();

                    String filename = resourcePath.toFile().getName();
                    String dbObjectName = getDbObjectName(filename, filenamePrefix, fileExtension).toUpperCase();

                    obj.setObjpName(dbObjectName);
                    obj.setResourceLocation(Paths.get(SQL_RESOURCES_PATH, objectFolder, filename).toString());
                    obj.loadResourceFileLines2PptAllSources();
                    String resourceVersion = obj.getResourceVersion();
                    obj.setObjpVersion(resourceVersion);
                    obj.setObjpVersionWar(resourceVersion);

                    String pk = obj.getObjpObjectType() + " " + dbObjectName.toUpperCase();
                    DataRow r = lw.find(pk);

                    System.out.println(" ? PPT_OBJECTS " +
                            " \tn.:" + Utils.rpad(obj.getObjpName(), 30) +
                            " \t<--v?crc--" + Utils.lpad(resourceVersion, 30) +
                            " \t/" + objectFolder + "/" + filename
                    );

                    if (r == null || !eq(r.getAsString("OBJP_VERSION"), resourceVersion)) { //brak lub zmiana crc/wersji
                        boolean isNewObject = (r == null);

                        if (!isNewObject) {//jest poprzednia wersja / przepisac AA i skasowac poprzednia
                            String aa = r.getAsString("OBJP_F_AUTO_AKT");
                            obj.setObjpFAutoAkt(aa);
                        }


                        if (recompileCodeOrReimportDML && obj.isAA()) { //nie robimy autoaktualizacji dla elementow gdzie wylaczono flage AA

                            boolean isSlownik = obj instanceof DbObjSlownik;
                            boolean isLW = obj instanceof DbObjListaWartosci;

                            if ((isSlownik || isLW) && isNewObject) {
                                //nowe slowniki oraz LW warto doczytywac do bazy pozniej (przy pierwszym wywolaniu getLW oraz getSL)
                                //dzieki temu nie zapychamy baz klienta niepotrzebnymi LW i SL
                            } else {
                                //rekompilujemy / aktualizujemy
                                try {
                                    obj.tryResetOrRecompileDbCode(h.getConnection());
                                } catch (Exception ex) {
                                    log.error("\n "+obj.resourceLocation + "---!!!ERR!!!---> " + ex.getMessage()+"\n");
                                    log.debug(ex.getMessage(), ex);
                                    if (obj != null) {
                                        obj.setErr(ex);
                                        DbObjectErrors.add(obj);
                                    }
                                    obj.setObjpVersion(("" + ex.getMessage()).substring(0, 25));
                                }
                            }
                        } else {
                            if (!obj.isAA()) System.out.println("->flaga AA = 'N' (pominięto aktualizację obiektu)");
                        }




                        Transaction trx = h.getSession().beginTransaction();

                        if (!isNewObject) {//jest poprzednia wersja / przepisac AA i skasowac poprzednia
                            deletePptObjectAndPptAllSource(h.getConnection(), r.get("OBJP_ID"));
                        }

                        if (obj instanceof DbObjListaWartosci || obj instanceof DbObjSlownik){
                            obj.clearPptAllSources();//wydajność - nie zapisujemy w PPT_ALL_SOURCE linii kodu skr. sql dla LW oraz dla SL
                        }


                        if ( // nie kazdy obiekt chemy aktualizowac
                                eq(pk, "FUNCTION PPF_BLOKADA_LOGOWANIA_UZT")
                             || eq(pk, "FUNCTION GET_ADRES")
                             || eq(pk, "VIEW EK_ZATRUDNIENIE") //wyjatek - bo std (poza MF) ma sztuczną kolumne ZAT_OB_ID_WYDZIAL
                        ){
                            obj.setObjpFAutoAkt("N");
                        }

                        obj.saveOrUpdate(h, trx, false);//zapis nowej wersji bez refresh'a

                        trx.commit();

                        System.out.println("aktualizacja wpisu PPT_OBJECTS " +
                                " / n.:" + Utils.rpad(obj.getObjpName(), 30) +
                                " / t.:" + Utils.rpad(obj.getObjpObjectType(), 30) +
                                " / v.:" + Utils.lpad(obj.getObjpVersionWar(), 30) +
                                ""
                        );



                    }

            }

        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    public static List<DbObject> DbObjectErrors = new ArrayList<>();


    public Throwable getErr() {
        return err;
    }

    public void setErr(Throwable err) {
        this.err = err;
    }

    @Transient
    public Throwable err = null;


    public void setAA(boolean aa){
        this.setObjpFAutoAkt(aa?"T":"N");
    }
    public boolean isAA(){
        return "T".equals(this.getObjpFAutoAkt());
    }


    public static void deletePptObjectAndPptAllSource(Connection conn, Object objp_id) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        JdbcUtils.sqlExecNoQuery(conn,"DELETE FROM PPT_ALL_SOURCE WHERE SOU_OBJP_ID = ?", objp_id);
        JdbcUtils.sqlExecNoQuery(conn,"DELETE FROM PPT_OBJECTS WHERE OBJP_ID = ?", objp_id);
    }



    public String getResourceVersion() throws Exception {
        if (resourceVersion == null) {
//            FileTime fileTime = Files.getLastModifiedTime(getAbsolutePathToResource());
//            DateFormat dateFormat = new SimpleDateFormat(VERSION_DATE_PATTERN);
//            resourceVersion = dateFormat.format(fileTime.toMillis());
            //zm. algorytm wersjonowania na CRC32 lub jakis MD5
            // aby nie konfilktowały ze sobą w bazie wersje z różnych systemów
            // (war generowany ze stanowsika developerskiego może mić inne daty niz war z jenkinsa)
//            this.getCode();

//            File file = this.getResource().getFile();
//            resourceVersion = Utils.getFileChecksum(MessageDigest.getInstance("MD5"), file);
//            System.out.println(resourceVersion + " <-MD5- " + file.getName());

            String code = this.getCode();
            byte[] bytes = code.getBytes(CHARSET_NAME); //"UTF-8"
            resourceVersion = ""+ Utils.getCRC(bytes);
//            System.out.println(resourceVersion + " <-CRC32- " + this.getResource().getFile().getName());
        }

        return resourceVersion;
    }


    @Transient String code = null;
    public String getCode() throws IOException {
        if (code==null){
            StringBuilder sb = new StringBuilder();
            this.getPptAllSources().stream().forEach(l->sb.append(nz(l.getSouText()) + "\r\n"));
            code = sb.toString();
        }
        return code;
    }


    public static String getDbObjectName(String filename, String filenamePrefix, String fileExtension) {
        int beginIndex = filenamePrefix == null ? 0 : filenamePrefix.length();
        int endIndex = filename.lastIndexOf(fileExtension);

        return filename.substring(beginIndex, endIndex);
    }

    public static List<Path> getSqlResoucePaths(String filenamePrefix, String fileExtension, String dbObjectFolder) throws IOException {
        Path path = Utils.getAbsolutePath(SQL_RESOURCES_PATH, dbObjectFolder);
        return Files.walk(path)
                .filter(
                    p -> {
                        String filename = p.toFile().getName();
                        return (filenamePrefix == null || filename.startsWith(filenamePrefix))
                                && filename.endsWith(fileExtension);
                    }
                ).sorted((p1, p2) -> p1.toFile().toString().compareTo(p2.toFile().toString()))
                .collect(Collectors.toList());
    }

    public static boolean recompileOrResetToStdVersion(String objectName, String objectFolder, String filename) {
        try {
            tryExecuteResourceSqlScript(objectName, objectFolder, filename);
            return true;
        } catch (IOException | SQLException e) {
            System.out.println("Niepowodzenie resetowania do wersji standardowej obiektu: " + objectName); //+ " / TYP: " + objectType
            log.error(e.getMessage(), e);
            User.alert(e);
            return false;
        }
    }
    public static void tryExecuteResourceSqlScript(String objectName, String objectFolder, String filename) throws SQLException, IOException {
        try (Connection conn = JdbcUtils.getConnection()) {
            tryExecuteResourceSqlScript(conn, objectName, objectFolder, filename);
        }
    }

    public static void tryExecuteResourceSqlScript(Connection conn, String objectName, String objectFolder, String filename) throws SQLException, IOException {
        if (filename.endsWith(".sql")) { //skrypt DML dla SL i LW
            Resource resource = Utils.getResource(Paths.get(SQL_RESOURCES_PATH, objectFolder, filename).toString());
            JdbcUtils.executeSqlScript(conn, resource, CHARSET_NAME);
        } else {
            Path filePath = Utils.getAbsolutePath(SQL_RESOURCES_PATH, objectFolder, filename);
            JdbcUtils.executeStatement(conn, filePath);
        }
        System.out.println("... wykonano skrypt: " + filename); //+ " / TYP: " + objectType
        User.warn("Wykonano skrypt: " + filename);
    }




    public abstract void tryResetOrRecompileDbCode(Connection conn) throws IOException, SQLException;


    public Path getAbsolutePathToResource() {
        return Utils.getAbsolutePath(getResourceLocation());
    }

    public void loadResourceFileLines2PptAllSources() throws IOException {
        this.clearPptAllSources();

        List<String> lines = Files.readAllLines(getAbsolutePathToResource(), Charset.forName(CHARSET_NAME));

        long lineNumber = 0;

        for (String line : lines) {
            if (line.equals("/")) break;
            PptAllSource pptAllSource = new PptAllSource();
            pptAllSource.setSouLine(++lineNumber);
            pptAllSource.setSouText(line);
            pptAllSource.setPptObject(this);
            addPptAllSource(pptAllSource);
        }

        makeFirstLineLikeDBAllSource();
    }


    public void makeFirstLineLikeDBAllSource(){ //polimorficzna

        if (this.getPptAllSources().isEmpty()) return;
        PptAllSource l = this.getPptAllSources().get(0);
        String txt = l.getSouText();
        if (txt==null || txt.isEmpty()) return;
        txt = txt.substring(txt.toUpperCase().indexOf(this.getObjpObjectType().toUpperCase())); //odcięcie 'create or replace '<TYP> (albo samego 'create '<TYP>)

        txt = txt.replace("\"PPADM\".", "        "); //zamiana "PPADM". na  8 spacji
        txt = txt.replace("PPADM.", "      ");   //zamiana PPADM. na  6 spacji

        txt = txt.replace("\"ppadm\".", "        "); //zamiana "PPADM". na  8 spacji
        txt = txt.replace("ppadm.", "      ");   //zamiana PPADM. na  6 spacji

        txt = txt.replace("\"Ppadm\".", "        "); //zamiana "PPADM". na  8 spacji
        txt = txt.replace("Ppadm.", "      ");   //zamiana PPADM. na  6 spacji

        l.setSouText(txt);
    }


    public String getRelativeSQLResourceLocation() {
        Resource res = getResource();
        if (res == null) {
            return null;
        }
        Path fpath = Utils.getAbsolutePath(getResourceLocation());
        Path parent = fpath.getParent();
        while (parent != null) {
            if ("SQL".equals(parent.getFileName().toString())) {
                return parent.relativize(fpath).toString();
            }
            parent = parent.getParent();
        }
        return fpath.toString();
    }


    public abstract String getObjectFolder();
    public abstract String getFileExtension();

    public String getResourceLocation() {
        if (resourceLocation == null) {
            resourceLocation = Paths.get(DbObject.SQL_RESOURCES_PATH, getObjectFolder(), this.getObjpName().toLowerCase() + getFileExtension()).toString();
        }
        return resourceLocation;
    }

    public void setResourceLocation(String resourceLocation) {
        this.resourceLocation = resourceLocation;
    }

    public Resource getResource() {
        return getResourceLocation() != null ? Utils.getResource(resourceLocation) : null;
    }





    public static boolean updateOrInsertAktualizacjeAutomatyczne(HibernateContext h, String owner, String type, String name, boolean aktualizacjeAutomatyczne) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        if (!updateAktualizacjeAutomatyczne(h.getConnection(),owner,type, name, aktualizacjeAutomatyczne)){
            DbObject o = DbObject.getNewInstance(type);
            o.setObjpName(name);
            o.setObjpOwner(owner);
            o.setAA(aktualizacjeAutomatyczne);
            o.saveOrUpdate(h, false);
            return true;
        }
        return false;
    }

    public static boolean updateAktualizacjeAutomatyczne(Connection conn, String owner, String type, String name, boolean aktualizacjeAutomatyczne) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        String strAktAut = aktualizacjeAutomatyczne ? "T" : "N";

        int i = JdbcUtils.sqlExecUpdate( conn,
            "update PPT_OBJECTS set OBJP_F_AUTO_AKT = ? where OBJP_OWNER=? and OBJP_OBJECT_TYPE=? and OBJP_NAME=?",
                                                  strAktAut,            owner,                   type,              name
        );
        if (i>0) {
            String txt = String.format(
                    "PPT_OBJECTS: flaga automatycznych aktualizacji OBJP_F_AUTO_AKT='%1$s' (OBJP_OWNER='%2$s' OBJP_OBJECT_TYPE='%3$s' OBJP_NAME='%4$s')",
                    strAktAut, owner, type, name);
            System.out.println(txt);
        }
        return (i>0);
    }

}