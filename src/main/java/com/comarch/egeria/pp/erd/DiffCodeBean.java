package com.comarch.egeria.pp.erd;

import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.erd.model.DbObjPackage;
import com.comarch.egeria.pp.erd.model.DbObject;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.comarch.egeria.Utils.nz;

@Named
@Scope("view")
public class DiffCodeBean extends SqlBean {

    String owner;
    String object_name;
    String object_type;
    DbObject dbObj = null;
    private String diffCode = "";
    private String dbCode = "";

    public String getOwner() {
        return owner;
    }

    public String getObject_name() {
        return object_name;
    }

    public String getObject_type() {
        return object_type;
    }

    public DbObject getDbObj() {
        if (dbObj==null) return new DbObjPackage();//zeby UI nie sypal NPE
        return dbObj;
    }

    public String getDiffCode() {
        return diffCode;
    }

    public String getDbCode() {
        return dbCode;
    }


    public String getPpCode() {
        return ppCode;
    }

    private String ppCode = "";


    @PostConstruct
    public void init() throws IOException, SQLException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        System.out.println();

        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        this.owner =  params.get("owner");
        this.object_name = params.get("object_name");
        this.object_type = params.get("object_type");

        reload();
//        System.out.println(diffCode);
    }

    public void reload() throws IOException, SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        loadDbCode();
        loadPPCode();
        calcDiffCode();
    }

    public void calcDiffCode() {
        diffCode = "";
        diff_match_patch diff = new diff_match_patch();
        LinkedList<diff_match_patch.Diff> diffs = diff.diff_main(dbCode, ppCode);

        //        diffCode = diff.diff_prettyHtml(diffs); //nie widac sapcji i tabów

        StringBuilder sb = new StringBuilder();
        for (diff_match_patch.Diff x : diffs) {

            if (x.operation.equals(diff_match_patch.Operation.EQUAL))
                sb.append("<span>").append(toHtml(x.text)).append("</span>");
            else if (x.operation.equals(diff_match_patch.Operation.DELETE))
                sb.append("<del>").append(toHtml(x.text)).append("</del>");
            else if (x.operation.equals(diff_match_patch.Operation.INSERT))
                sb.append("<ins>").append(toHtml(x.text)).append("</ins>");
        }

        diffCode = sb.toString();
    }

    String toHtml(String txt){
        return nz(txt)
                .replace(" ", "&#160;") //  &nbsp;  -- https://www.w3schools.com/html/html_entities.asp
                .replace("\t", "&#160;&#160;&#160;&#160;")
                .replace("\r","")
                .replace("\n","&#160;<br/>");
    }



    public void loadPPCode() throws IOException {
        dbObj = null;
        ppCode = "";
        List query = HibernateContext.query("from DbObject where OBJP_OWNER = ?0 and OBJP_OBJECT_TYPE = ?1 and OBJP_NAME = ?2 ", owner, object_type, object_name);
        if (query.isEmpty()){
            return;
        }
//        obj.setObjpName(dbObjectName);
//        obj.setResourceLocation(Paths.get( "/WEB-INF/protectedElements/SQL/", objectFolder, filename).toString());
//        obj.loadResourceFileLines2PptAllSources();
        dbObj = (DbObject) query.get(0);
        ppCode = dbObj.getCode();//kod do porównainia
        //        ppCode = ppCode.replace("\r","").replace("\n","<br/>");//                ;
    }

    public void loadDbCode() throws IOException, SQLException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        dbCode="";
        //dbCode = JdbcUtils.getDbSourceCode(owner, object_name, object_type, false, false, false, false); ...ale widoki zawsze maja owner'a w kodzie

        try (Connection conn = JdbcUtils.getConnection()) {
            if ("VIEW".equals(object_type.toUpperCase())) {
                dbCode = JdbcUtils.getViewSource(conn, owner, object_name, false, true, false, false);
            } else {
                dbCode = JdbcUtils.getAllSource(conn, owner, object_name, object_type, false, false, false, false);
            }
        }
        //        dbCode = dbCode.replace("\r","").replace("\n","<br/>");
    }


    public void resetujObiektDoWersjiStd() throws IOException, SQLException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        if (dbObj==null){
            User.warn("to nie jest kod wersjonowanyc przez PP");
            return;
        }
        try (Connection conn = JdbcUtils.getConnection()) {
            dbObj.tryResetOrRecompileDbCode(conn);
        }

        reload();
    }


    public void loadResourceFileLines2PptAllSources() throws IOException, SQLException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        if (dbObj==null){
            User.warn("to nie jest kod wersjonowanyc przez PP");
            return;
        }

        dbObj.clearPptAllSources();
        dbObj.loadResourceFileLines2PptAllSources();
        dbObj.saveOrUpdate();

        reload();
    }


}
