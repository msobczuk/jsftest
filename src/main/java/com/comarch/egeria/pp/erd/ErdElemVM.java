package com.comarch.egeria.pp.erd;

import com.comarch.egeria.pp.erd.model.ErdElem;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.endpoint.BlankEndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

import static com.comarch.egeria.Utils.nz;

public class ErdElemVM extends Element {
//Wg MVVM ta klasa stanowi impl. ViewModel dla modelu ErdElem
//przeznaczenie: dostosowanie/mapowanie ErdElem do warstwy prezentacji danych

    public ErdElemVM(ErdElem erdElem){
        this.setData(erdElem);
        this.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP)); 			// 0
        this.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP_RIGHT)); 	// 1
        this.addEndPoint(new BlankEndPoint(EndPointAnchor.RIGHT)); 		// 2
        this.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM_RIGHT)); // 3
        this.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM)); 		// 4
        this.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM_LEFT));	// 5
        this.addEndPoint(new BlankEndPoint(EndPointAnchor.LEFT)); 		// 6
        this.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP_LEFT)); 	// 7
    }

    public ErdElem getErdElem(){
        return (ErdElem) this.getData();
    }


    @Override
    public String getX() {
        return nz(getErdElem().getTdeX())+"px";
    }
    @Override
    public void setX(String x) {
        getErdElem().setTdeX(toDbl(x));
    }


    @Override
    public String getY() {
        return nz(getErdElem().getTdeY())+"px";
    }
    @Override
    public void setY(String y) {
        getErdElem().setTdeY(toDbl(y));
    }




//    List<ErdElemVM> getFkErdElemVMs(){
//        this.getErdElem().getFkErdElems();
//    }



    public static Double toDbl(String x){
        if(x==null || x.isEmpty()) return null;
        x=x.replace("px","").replace("em","");
        return Double.parseDouble(x);
    }
}
