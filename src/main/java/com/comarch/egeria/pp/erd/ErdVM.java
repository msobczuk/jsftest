package com.comarch.egeria.pp.erd;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.erd.model.Erd;
import com.comarch.egeria.pp.erd.model.ErdElem;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.FlowChartConnector;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.overlay.ArrowOverlay;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Named
@Scope("view")
public class ErdVM extends SqlBean {
// Uwaga: w JSF/MVC typowa nazwa to ErdBean a nie ErdVM (nazwa jednak nie ma znaczenia; istotne jest przeznaczenie)
// Wg MVVM jest to nie tyle kontroler, co raczej impl. ViewModel (dla modelu Erd)
// przeznaczenie: dostosowanie/mapowanie modelu Erd do warstwy prezentacji danych


    private static final long serialVersionUID = 1L;
    private static final Logger log = LogManager.getLogger();

    Erd model = null;
    public Erd getModel() {
        return model;
    }
    public void setModel(Erd model) {
        this.model = model;
    }



    public List<ErdElemVM> getErdElemVMs(){
        return (List<ErdElemVM>)(List<?>) this.getDiagramModel().getElements();
    }
    public HashMap<ErdElem, ErdElemVM> el2vmElHM = new HashMap<>();

    private ErdElemVM current = null;

    private DefaultDiagramModel diagramModel;

    public DefaultDiagramModel getDiagramModel() {
        if (diagramModel == null) {

            diagramModel = new DefaultDiagramModel();
            diagramModel.setMaxConnections(-1);
            diagramModel.setConnectionsDetachable(false);

            el2vmElHM.clear();
            for (ErdElem el : getModel().getErdElems()) {
                ErdElemVM vmel = new ErdElemVM(el);
                diagramModel.addElement(vmel);
                el2vmElHM.put(el,vmel);
            }

            for (ErdElemVM vmElFrom : this.getErdElemVMs()) {
                for (ErdElem elTo : vmElFrom.getErdElem().getFkErdElems()) {
                    ErdElemVM vmElTo = el2vmElHM.get(elTo);
                    createConnectionFK(vmElFrom, vmElTo);
                }
            }

        }
        return diagramModel;
    }

    private void createConnectionFK(ErdElem elFrom, ErdElem elTo) {
        createConnectionFK(el2vmElHM.get(elFrom), el2vmElHM.get(elTo));
    }

    private void createConnectionFK(ErdElemVM vmElFrom, ErdElemVM vmElTo) {
        if (vmElFrom==null || vmElTo == null) return;

        EndPoint from = vmElFrom.getEndPoints().get(0);
        EndPoint to =  vmElTo.getEndPoints().get(4);
        FlowChartConnector connector = connectorLightGray();
        Connection connection = new Connection(from, to);
        connection.setConnector(connector);
        connection.getOverlays().add(new ArrowOverlay(10, 10, 1, 1));
//        String label ="FK...";
//        connection.getOverlays().add(new LabelOverlay(label, "flow-label", 0.1));
        diagramModel.connect(connection);
    }


    public FlowChartConnector connectorLightGray() {
        FlowChartConnector connector = new FlowChartConnector();
        connector.setCornerRadius(5);
        connector.setStub(15);
        connector.setPaintStyle("{strokeStyle:'#B0B0B0',lineWidth:1}");
        connector.setHoverPaintStyle("{strokeStyle:'#B0B0B0',lineWidth:2}");
        return connector;
    }

//    public FlowChartConnector connectorRed() {
//        FlowChartConnector connector = new FlowChartConnector();
//        connector.setCornerRadius(5);
//        connector.setStub(15);
//        connector.setPaintStyle(     "{strokeStyle:'#AA5555',lineWidth:2}");
//        connector.setHoverPaintStyle("{strokeStyle:'#AA5555',lineWidth:2}");
//        return connector;
//    }


//    public void onConnectionChange(org.primefaces.event.diagram.PositionChangeEvent event) {
//
//    }

    public void setDiagramModel(DefaultDiagramModel diagramModel) {
        this.diagramModel = diagramModel;
    }








    @PostConstruct
    public void init() {
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        this.setModel((Erd) HibernateContext.get(Erd.class, params.get("td_id")));
        if (this.model ==null) model = new Erd();
    }





    public void pobierzPozycje() {
        for (Element el : getDiagramModel().getElements()) {
            String id = el.getId();
            String polecenieJsLeft = "$('#frmDiagram\\\\:diagramErd-" + id + "').css('left')";
            String polecenieJsTop = "$('#frmDiagram\\\\:diagramErd-" + id + "').css('top')";
//            com.comarch.egeria.Utils.executeScript("console.log(" + polecenieJsLeft + ")");
            String script = "pobierzXY([" +
                    "{name:'id', value:'" + id + "'}" +
                    ", {name:'x', value:" + polecenieJsLeft + "}" +
                    ", {name:'y', value:" + polecenieJsTop + "}" +
                    "])";

            com.comarch.egeria.Utils.executeScript(script);
        }
    }

    public void pobierzXY() {
        Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String id = requestParamMap.get("id");
        String x = requestParamMap.get("x");
        String y = requestParamMap.get("y");

        ErdElemVM el = (ErdElemVM) getDiagramModel().findElement(id);
        el.setX(x);
        el.setY(y);
    }






    public String jsElOndblclick(Object el) {

//        if (el instanceof ObiegStateMachineBean.CzynnElem) {
//            return "showDlgCzObg( "+ ((ObiegStateMachineBean.CzynnElem) el).czynn.getId() +" );";
//        }
//
//        if (el instanceof ObiegStateMachineBean.StanElem) {
//            return "showDlgStObg( "+ ((ObiegStateMachineBean.StanElem) el).stan.getSwoLp() +" );";
//        }

        ErdElemVM erdElemVM = el2vmElHM.get(el);
        if (el instanceof  ErdElem){
            return "window.open('test_tab.xhtml?tb_id=" + erdElemVM.getErdElem().getTable().getTbId() + "' , '_blank');";
//            return  "showDlgTable( '"+ erdElemVM.getId() +"' );";
        }

        return "return false;";
    }


    public ErdElemVM getCurrent() {
        return current;
    }
    public void setCurrent(ErdElemVM current) {
        this.current = current;
    }


    public void setCurrent(String id){
        ErdElemVM el = (ErdElemVM) this.getDiagramModel().findElement(id);
        this.setCurrent(el);
//        Tab table = el.getErdElem().getTable();
    }

}
