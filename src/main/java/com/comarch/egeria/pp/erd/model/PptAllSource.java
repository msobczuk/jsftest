package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.pp.data.model.ModelBase;
import org.hibernate.annotations.DiscriminatorFormula;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


/**
 * The persistent class for the PPT_TABLES database table.
 */
@Entity
@Table(name = "PPT_ALL_SOURCE", schema = "PPADM")
@NamedQuery(name = "PptAllSource.findAll", query = "SELECT p FROM PptAllSource p")
@DiscriminatorFormula("'AllSource'")
public class PptAllSource extends ModelBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @TableGenerator(name = "TABLE_KEYGEN_PPT_ALL_SOURCE", table = "PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ALL_SOURCE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 500)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "TABLE_KEYGEN_PPT_ALL_SOURCE")
    @Column(name = "SOU_ID")
    private long souId;

    @Temporal(TemporalType.DATE)
    @Column(name = "SOU_AUDYT_DM")
    private Date souAudytDm;

    @Temporal(TemporalType.DATE)
    @Column(name = "SOU_AUDYT_DT", updatable = false)
    private Date souAudytDt = new Date();

    @Column(name = "SOU_AUDYT_LM")
    private String souAudytLm;

    @Column(name = "SOU_AUDYT_UM")
    private String souAudytUm;

    @Column(name = "SOU_AUDYT_UT", updatable = false)
    private String souAudytUt;

    @Column(name = "SOU_LINE")
    private Long souLine;

    @Column(name = "SOU_TEXT")
    private String souText;

    //bi-directional many-to-one association to PptTable
    @ManyToOne
    @JoinColumn(name = "SOU_OBJP_ID")
    private PptObject pptObject;

    public PptAllSource() {
    }

    public long getSouId() {
        this.onRXGet("souId");
        return souId;
    }

    public void setSouId(long souId) {
        this.onRXSet("souId", this.souId, souId);
        this.souId = souId;
    }

    public Long getSouLine() {
        this.onRXGet("souLine");
        return souLine;
    }

    public void setSouLine(Long souLine) {
        this.onRXSet("souLine", this.souLine, souLine);
        this.souLine = souLine;
    }

    public String getSouText() {
        this.onRXGet("souText");
        return souText;
    }

    public void setSouText(String souText) {
        this.onRXSet("souText", this.souText, souText);
        this.souText = souText;
    }

    public PptObject getPptObject() {
        this.onRXGet("pptObject");
        return pptObject;
    }

    public void setPptObject(PptObject pptObject) {
        this.onRXSet("pptObject", this.pptObject, pptObject);
        this.pptObject = pptObject;
    }
}
