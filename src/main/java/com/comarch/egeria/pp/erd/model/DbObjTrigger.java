package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.web.common.hibernate.HibernateContext;

import javax.persistence.Entity;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@Entity
public class DbObjTrigger extends DbObject {

    private static final String OBJECT_TYPE = "TRIGGER";
    private static final String OBJECT_FOLDER = "triggers";
    private static final String FILE_EXTENSION = ".trg";


    @Override
    public String getObjectFolder() {
        return OBJECT_FOLDER;
    }

    @Override
    public String getFileExtension() {
        return FILE_EXTENSION;
    }

    public DbObjTrigger() {
        this.setObjpObjectType(OBJECT_TYPE);
    }

    public static void loadStdVersionFromResources(HibernateContext h, boolean recompileCodeOrReimportDML) {
        System.out.println("... Wyzwalacze ...");
        loadStdVersionFromResources(h, DbObjTrigger::new, null, FILE_EXTENSION, OBJECT_FOLDER, recompileCodeOrReimportDML);
    }

    public static boolean resetToStdVersion(String name) {
        return DbObject.recompileOrResetToStdVersion(name, OBJECT_FOLDER, name.toLowerCase() + FILE_EXTENSION);
    }


    @Override
    public void makeFirstLineLikeDBAllSource(){ //polimorficzna

        if (this.getPptAllSources().isEmpty()) return;
        PptAllSource l = this.getPptAllSources().get(0);
        String txt = l.getSouText();
        if (txt==null || txt.isEmpty()) return;
        txt = txt.substring(txt.toUpperCase().indexOf(this.getObjpObjectType().toUpperCase())); //odcięcie create or replace

// w triggerach schemat jest widoczny w sys.all_source
//        txt = txt.replace("\"PPADM\".", "        "); //zamiana "PPADM". na  8 spacji
//        txt = txt.replace("PPADM.", "      ");   //zamiana PPADM. na  6 spacji
//
//        txt = txt.replace("\"ppadm\".", "        "); //zamiana "PPADM". na  8 spacji
//        txt = txt.replace("ppadm.", "      ");   //zamiana PPADM. na  6 spacji
//
//        txt = txt.replace("\"Ppadm\".", "        "); //zamiana "PPADM". na  8 spacji
//        txt = txt.replace("Ppadm.", "      ");   //zamiana PPADM. na  6 spacji

        l.setSouText(txt);
    }


    @Override
    public void tryResetOrRecompileDbCode(Connection conn) throws IOException, SQLException {
        DbObject.tryExecuteResourceSqlScript(conn, this.getObjpName(), OBJECT_FOLDER, this.getObjpName().toLowerCase() + FILE_EXTENSION);
    }
}
