package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

import javax.persistence.Entity;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;

@Entity
public class DbObjListaWartosci extends DbObject {

    private static final String OBJECT_TYPE = "LW";
    private static final String OBJECT_FOLDER = "dmp_lw";
    private static final String FILENAME_PREFIX = "lw.";
    private static final String FILE_EXTENSION = ".sql";

    public DbObjListaWartosci() {
        this.setObjpObjectType(OBJECT_TYPE);
    }

    public static void loadStdVersionFromResources(HibernateContext h, boolean recompileCodeOrReimportDML) {
        System.out.println("... Listy wartości ...");
        loadStdVersionFromResources(h, DbObjListaWartosci::new, FILENAME_PREFIX, FILE_EXTENSION, OBJECT_FOLDER, recompileCodeOrReimportDML);
    }

    public static boolean resetToStdVersion(String name) {
        return DbObject.recompileOrResetToStdVersion(name, OBJECT_FOLDER, FILENAME_PREFIX + name + FILE_EXTENSION);
    }

    public static void tryResetToStdVersion(Connection conn, String name) throws IOException, SQLException {
        DbObject.tryExecuteResourceSqlScript(conn, name, OBJECT_FOLDER, FILENAME_PREFIX + name + FILE_EXTENSION);
    }

    @Override
    public String getObjectFolder() {
        return OBJECT_FOLDER;
    }

    @Override
    public String getFileExtension() {
        return FILE_EXTENSION;
    }

//    @Override
//    public void loadResourceFileLines2PptAllSources() throws IOException {
//        this.clearPptAllSources(); //lista wartosci nie ma lini kodu pl/sql
//    }

    public void makeFirstLineLikeDBAllSource() { //polimorficzna
        //nic albo wytnij TS dmp
    }


//    @Override
//    public void tryResetOrRecompileDbCode(Connection conn) throws IOException, SQLException {
//        DbObject.tryExecuteResourceSqlScript(conn, this.getObjpName(), OBJECT_FOLDER, FILENAME_PREFIX + this.getObjpName().toUpperCase() + FILE_EXTENSION);
//    }

    @Override
    public  void tryResetOrRecompileDbCode(Connection conn) throws SQLException, IOException {
        Path filePath = this.getAbsolutePathToResource();
        JdbcUtils.executeDmlScriptAsBeginEndStatement(conn, filePath);
        System.out.println("Zresetowano do wersji standardowej LW: " + this.getObjpName());
        User.warn("Zresetowano/zrekompilowano do wersji standardowej LW: " + this.getObjpName());
    }



}
