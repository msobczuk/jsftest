package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.web.common.hibernate.HibernateContext;

import javax.persistence.Entity;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@Entity
public class DbObjFunction extends DbObject {

    private static final String OBJECT_TYPE = "FUNCTION";
    private static final String OBJECT_FOLDER = "functions";
    private static final String FILE_EXTENSION = ".fun";

    public DbObjFunction() {
        this.setObjpObjectType(OBJECT_TYPE);
    }

    @Override
    public String getObjectFolder() {
        return OBJECT_FOLDER;
    }

    @Override
    public String getFileExtension() {
        return FILE_EXTENSION;
    }

    public static void loadStdVersionFromResources(HibernateContext h, boolean recompileCodeOrReimportDML) {
        System.out.println("... Funkcje ...");
        loadStdVersionFromResources(h, DbObjFunction::new, null, FILE_EXTENSION, OBJECT_FOLDER, recompileCodeOrReimportDML);
    }

    public static boolean resetToStdVersion(String name) {
        return DbObject.recompileOrResetToStdVersion(name, OBJECT_FOLDER, name.toLowerCase() + FILE_EXTENSION);
    }

    @Override
    public void tryResetOrRecompileDbCode(Connection conn) throws IOException, SQLException {
        DbObject.tryExecuteResourceSqlScript(conn, this.getObjpName(), OBJECT_FOLDER, this.getObjpName().toLowerCase() + FILE_EXTENSION);
    }
}
