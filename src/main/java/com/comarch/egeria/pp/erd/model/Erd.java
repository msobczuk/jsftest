package com.comarch.egeria.pp.erd.model;

import javax.persistence.Entity;
import java.util.List;

@Entity
public class Erd extends PptErd {
//klasa logiczna (klasa domenowa) dla persystencji PptErd
//przeznaczenie: logika biznesowa dla modelu Erd

    //lista z el. klas logicznych. (zamiana typu elementow listy w persystencji na własciwy typ logiczny)
    public List<ErdElem> getErdElems(){
        return (List<ErdElem>)(List<?>) this.getPptErdElems();
    }

    public void resetXY() {
        System.out.println("resetXY...");
        int i = 0;
        for (PptErdElem el : this.getErdElems()) {
            if (el.getPptTable() == null) continue;
            i++;
            el.setTdeX(i * 200.0);
            el.setTdeY(i * 100.0);
        }
    }


    public void test(){

    }

}
