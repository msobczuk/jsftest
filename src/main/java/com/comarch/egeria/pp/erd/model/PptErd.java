package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.pp.data.model.ModelBase;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_ERD database table.
 * 
 */
@Entity
@Table(name="PPT_ERD", schema="PPADM")
@NamedQuery(name="PptErd.findAll", query="SELECT p FROM PptErd p")
@DiscriminatorFormula("'Erd'")
public class PptErd extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_ERD",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_ERD", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_ERD")
	@Column(name="TD_ID")
	private long tdId;

	@Temporal(TemporalType.DATE)
	@Column(name="TD_AUDYT_DM")
	private Date tdAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="TD_AUDYT_DT")
	private Date tdAudytDt;

	@Column(name="TD_AUDYT_LM")
	private String tdAudytLm;

	@Column(name="TD_AUDYT_UM")
	private String tdAudytUm;

	@Column(name="TD_AUDYT_UT")
	private String tdAudytUt;

	@Column(name="TD_DESC")
	private String tdDesc;

	@Column(name="TD_NAME")
	private String tdName;
	
	//bi-directional many-to-one association to PptErdElem
	@OneToMany(mappedBy="pptErd", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@BatchSize(size = 200)
	@OrderBy("TDE_ID ASC")
	private List<PptErdElem> pptErdElems = new ArrayList<>();

	public PptErd() {
	}

	public long getTdId() {
		this.onRXGet("tdId");
		return this.tdId;
	}

	public void setTdId(long tdId) {
		this.onRXSet("tdId", this.tdId, tdId);
		this.tdId = tdId;
	}

	public Date getTdAudytDm() {
		this.onRXGet("tdAudytDm");
		return this.tdAudytDm;
	}

	public void setTdAudytDm(Date tdAudytDm) {
		this.onRXSet("tdAudytDm", this.tdAudytDm, tdAudytDm);
		this.tdAudytDm = tdAudytDm;
	}

	public Date getTdAudytDt() {
		this.onRXGet("tdAudytDt");
		return this.tdAudytDt;
	}

	public void setTdAudytDt(Date tdAudytDt) {
		this.onRXSet("tdAudytDt", this.tdAudytDt, tdAudytDt);
		this.tdAudytDt = tdAudytDt;
	}

	public String getTdAudytLm() {
		this.onRXGet("tdAudytLm");
		return this.tdAudytLm;
	}

	public void setTdAudytLm(String tdAudytLm) {
		this.onRXSet("tdAudytLm", this.tdAudytLm, tdAudytLm);
		this.tdAudytLm = tdAudytLm;
	}

	public String getTdAudytUm() {
		this.onRXGet("tdAudytUm");
		return this.tdAudytUm;
	}

	public void setTdAudytUm(String tdAudytUm) {
		this.onRXSet("tdAudytUm", this.tdAudytUm, tdAudytUm);
		this.tdAudytUm = tdAudytUm;
	}

	public String getTdAudytUt() {
		this.onRXGet("tdAudytUt");
		return this.tdAudytUt;
	}

	public void setTdAudytUt(String tdAudytUt) {
		this.onRXSet("tdAudytUt", this.tdAudytUt, tdAudytUt);
		this.tdAudytUt = tdAudytUt;
	}

	public String getTdDesc() {
		this.onRXGet("tdDesc");
		return this.tdDesc;
	}

	public void setTdDesc(String tdDesc) {
		this.onRXSet("tdDesc", this.tdDesc, tdDesc);
		this.tdDesc = tdDesc;
	}

	public String getTdName() {
		this.onRXGet("tdName");
		return this.tdName;
	}

	public void setTdName(String tdName) {
		this.onRXSet("tdName", this.tdName, tdName);
		this.tdName = tdName;
	}
	
	public List<PptErdElem> getPptErdElems() {
		this.onRXGet("pptErdElems");
		return this.pptErdElems;
	}

	public void setPptErdElems(List<PptErdElem> pptErdElems) {
		this.onRXSet("pptErdElems", this.pptErdElems, pptErdElems);
		this.pptErdElems = pptErdElems;
	}

	public PptErdElem addPptErdElem(PptErdElem pptErdElem) {
		this.onRXSet("pptErdElems", null, this.pptErdElems);
		getPptErdElems().add(pptErdElem);
		pptErdElem.setPptErd(this);

		return pptErdElem;
	}

	public PptErdElem removePptErdElem(PptErdElem pptErdElem) {
		this.onRXSet("pptErdElems", null, this.pptErdElems);
		getPptErdElems().remove(pptErdElem);
		pptErdElem.setPptErd(null);

		return pptErdElem;
	}

}
