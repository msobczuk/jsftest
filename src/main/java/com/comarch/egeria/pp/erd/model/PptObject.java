package com.comarch.egeria.pp.erd.model;

import com.comarch.egeria.pp.data.model.ModelBase;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.DiscriminatorFormula;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the PPT_TABLES database table.
 */
@Entity
@Table(name = "PPT_OBJECTS", schema = "PPADM")
@NamedQuery(name = "PptObject.findAll", query = "SELECT p FROM PptObject p")
@DiscriminatorFormula("CASE "
        + "WHEN OBJP_OBJECT_TYPE='SL' 					THEN 'DbObjSlownik'"
        + "WHEN OBJP_OBJECT_TYPE='LW' 					THEN 'DbObjListaWartosci'"
        + "WHEN OBJP_OBJECT_TYPE='VIEW' 				THEN 'DbObjView'"
        + "WHEN OBJP_OBJECT_TYPE='PROCEDURE'			THEN 'DbObjProcedure'"
        + "WHEN OBJP_OBJECT_TYPE='FUNCTION'				THEN 'DbObjFunction'"
        + "WHEN OBJP_OBJECT_TYPE='PACKAGE' 				THEN 'DbObjPackage'"
        + "WHEN OBJP_OBJECT_TYPE='PACKAGE BODY'			THEN 'DbObjPackageBody'"
        + "WHEN OBJP_OBJECT_TYPE='TRIGGER'				THEN 'DbObjTrigger'"
        //... pozostale typy obiektów
        + "ELSE 'DbObject' END")
public class PptObject extends ModelBase implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @TableGenerator(name = "TABLE_KEYGEN_PPT_OBJECTS", table = "PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_OBJECTS", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "TABLE_KEYGEN_PPT_OBJECTS")
    @Column(name = "OBJP_ID")
    private long objpId;

    @Temporal(TemporalType.DATE)
    @Column(name = "OBJP_AUDYT_DM")
    private Date objpAudytDm;

    @Temporal(TemporalType.DATE)
    @Column(name = "OBJP_AUDYT_DT", updatable = false)
    private Date objpAudytDt;

    @Column(name = "OBJP_AUDYT_LM")
    private String objpAudytLm;

    @Column(name = "OBJP_AUDYT_UM")
    private String objpAudytUm;

    @Column(name = "OBJP_AUDYT_UT", updatable = false)
    private String objpAudytUt;

    @Column(name = "OBJP_OWNER")
    private String objpOwner;

    @Column(name = "OBJP_NAME")
    private String objpName;

    @Column(name = "OBJP_OBJECT_TYPE")
    private String objpObjectType;

    @Column(name = "OBJP_REVISION")
    private String objpRevision;

    @Column(name = "OBJP_VERSION")
    private String objpVersion;

    @Column(name = "OBJP_VERSION_WAR")
    private String objpVersionWar;


    @Column(name = "OBJP_F_AUTO_AKT")
    private String objpFAutoAkt = "T";


    //bi-directional many-to-one association to PptTabCol
    @OneToMany(mappedBy = "pptObject", cascade = {CascadeType.ALL}, orphanRemoval = true)
    @LazyCollection(LazyCollectionOption.FALSE)
    @BatchSize(size = 200)
//    @OrderBy("SOU_ID ASC")
    @OrderBy("SOU_LINE ASC")
    private List<PptAllSource> pptAllSources = new ArrayList<>();

    public PptObject() {
    }

    public long getObjpId() {
        this.onRXGet("objpId");
        return this.objpId;
    }

    public void setObjpId(long objpId) {
        this.onRXSet("objpId", this.objpId, objpId);
        this.objpId = objpId;
    }

    public Date getObjpAudytDm() {
        this.onRXGet("objpAudytDm");
        return objpAudytDm;
    }

    public void setObjpAudytDm(Date objpAudytDm) {
        this.onRXSet("objpAudytDm", this.objpAudytDm, objpAudytDm);
        this.objpAudytDm = objpAudytDm;
    }

    public Date getObjpAudytDt() {
        this.onRXGet("objpAudytDt");
        return objpAudytDt;
    }

    public void setObjpAudytDt(Date objpAudytDt) {
        this.onRXSet("objpAudytDt", this.objpAudytDt, objpAudytDt);
        this.objpAudytDt = objpAudytDt;
    }

    public String getObjpAudytLm() {
        this.onRXGet("objpAudytLm");
        return objpAudytLm;
    }

    public void setObjpAudytLm(String objpAudytLm) {
        this.onRXSet("objpAudytLm", this.objpAudytLm, objpAudytLm);
        this.objpAudytLm = objpAudytLm;
    }

    public String getObjpAudytUm() {
        this.onRXGet("objpAudytUm");
        return objpAudytUm;
    }

    public void setObjpAudytUm(String objpAudytUm) {
        this.onRXSet("objpAudytUm", this.objpAudytUm, objpAudytUm);
        this.objpAudytUm = objpAudytUm;
    }

    public String getObjpAudytUt() {
        this.onRXGet("objpAudytUt");
        return objpAudytUt;
    }

    public void setObjpAudytUt(String objpAudytUt) {
        this.onRXSet("objpAudytUt", this.objpAudytUt, objpAudytUt);
        this.objpAudytUt = objpAudytUt;
    }

    public String getObjpOwner() {
        this.onRXGet("objpOwner");
        return objpOwner;
    }

    public void setObjpOwner(String objpOwner) {
        this.onRXSet("objpOwner", this.objpOwner, objpOwner);
        this.objpOwner = objpOwner;
    }

    public String getObjpName() {
        this.onRXGet("objpName");
        return objpName;
    }

    public void setObjpName(String objpName) {
        this.onRXSet("objpName", this.objpName, objpName);
        this.objpName = objpName;
    }

    public String getObjpObjectType() {
        this.onRXGet("objpObjectType");
        return objpObjectType;
    }

    public void setObjpObjectType(String objpObjectType) {
        this.onRXSet("objpObjectType", this.objpObjectType, objpObjectType);
        this.objpObjectType = objpObjectType;
    }

    public String getObjpRevision() {
        this.onRXGet("objpRevision");
        return objpRevision;
    }

    public void setObjpRevision(String objpRevision) {
        this.onRXSet("objpRevision", this.objpRevision, objpRevision);
        this.objpRevision = objpRevision;
    }

    public String getObjpVersion() {
        this.onRXGet("objpVersion");
        return objpVersion;
    }

    public void setObjpVersion(String objpVersion) {
        this.onRXSet("objpVersion", this.objpVersion, objpVersion);
        this.objpVersion = objpVersion;
    }

    public String getObjpVersionWar() {
        this.onRXGet("objpVersionWar");
        return objpVersionWar;
    }

    public void setObjpVersionWar(String objpVersionWar) {
        this.onRXSet("objpVersionWar", this.objpVersionWar, objpVersionWar);
        this.objpVersionWar = objpVersionWar;
    }

    public List<PptAllSource> getPptAllSources() {
        this.onRXGet("pptAllSources");
        return pptAllSources;
    }

    public void setPptAllSources(List<PptAllSource> pptAllSources) {
        this.onRXSet("pptAllSources", this.pptAllSources, pptAllSources);
        this.pptAllSources = pptAllSources;
    }

    public PptAllSource addPptAllSource(PptAllSource pptAllSource) {
        getPptAllSources().add(pptAllSource);
        pptAllSource.setPptObject(this);

        return pptAllSource;
    }

    public PptAllSource removePptAllSource(PptAllSource pptAllSource) {
        getPptAllSources().remove(pptAllSource);
        pptAllSource.setPptObject(null);

        return pptAllSource;
    }

    public void clearPptAllSources() {
        getPptAllSources().stream().forEach(as -> as.setPptObject(null));
        getPptAllSources().clear();
    }

    public String getObjpFAutoAkt() {
        this.onRXGet("objpFAutoAkt");
        return objpFAutoAkt;
    }

    public void setObjpFAutoAkt(String objpFAutoAkt) {
        this.onRXSet("objpFAutoAkt", this.objpFAutoAkt, objpFAutoAkt);
        this.objpFAutoAkt = objpFAutoAkt;
    }


}
