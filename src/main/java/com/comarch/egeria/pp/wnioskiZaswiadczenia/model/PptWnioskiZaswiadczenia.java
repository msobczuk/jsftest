package com.comarch.egeria.pp.wnioskiZaswiadczenia.model;

import java.io.Serializable;
import javax.persistence.*;

import com.comarch.egeria.pp.data.model.ModelBase;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="PPT_WNIOSKI_ZASWIADCZENIA", schema="PPADM")
@NamedQuery(name="PptWnioskiZaswiadczenia.findAll", query="SELECT p FROM PptWnioskiZaswiadczenia p")
public class PptWnioskiZaswiadczenia extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_WNIOSKI_ZASWIADCZENIA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_WNIOSKI_ZASWIADCZENIA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_WNIOSKI_ZASWIADCZENIA")
	@Column(name="WZA_ID")
	private long wzaId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="WZA_AUDYT_DM")
	private Date wzaAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="WZA_AUDYT_DT")
	private Date wzaAudytDt;

	@Column(name="WZA_AUDYT_LM")
	private String wzaAudytLm;

	@Column(name="WZA_AUDYT_UM")
	private String wzaAudytUm;

	@Column(name="WZA_AUDYT_UT")
	private String wzaAudytUt;

	@Column(name="WZA_OPIS")
	private String wzaOpis;

	@Column(name="WZA_PRC_ID")
	private Long wzaPrcId;

	@Column(name="WZA_RODZAJ")
	private String wzaRodzaj;

	public PptWnioskiZaswiadczenia() {
	}

	public long getWzaId() {
		this.onRXGet("wzaId");
		return this.wzaId;
	}

	public void setWzaId(long wzaId) {
		this.onRXSet("wzaId", this.wzaId, wzaId);
		this.wzaId = wzaId;
	}

	public Object getWzaAudytDm() {
		this.onRXGet("wzaAudytDm");
		return this.wzaAudytDm;
	}

	public void setWzaAudytDm(Date wzaAudytDm) {
		this.onRXSet("wzaAudytDm", this.wzaAudytDm, wzaAudytDm);
		this.wzaAudytDm = wzaAudytDm;
	}

	public Object getWzaAudytDt() {
		this.onRXGet("wzaAudytDt");
		return this.wzaAudytDt;
	}

	public void setWzaAudytDt(Date wzaAudytDt) {
		this.onRXSet("wzaAudytDt", this.wzaAudytDt, wzaAudytDt);
		this.wzaAudytDt = wzaAudytDt;
	}

	public String getWzaAudytLm() {
		this.onRXGet("wzaAudytLm");
		return this.wzaAudytLm;
	}

	public void setWzaAudytLm(String wzaAudytLm) {
		this.onRXSet("wzaAudytLm", this.wzaAudytLm, wzaAudytLm);
		this.wzaAudytLm = wzaAudytLm;
	}

	public String getWzaAudytUm() {
		this.onRXGet("wzaAudytUm");
		return this.wzaAudytUm;
	}

	public void setWzaAudytUm(String wzaAudytUm) {
		this.onRXSet("wzaAudytUm", this.wzaAudytUm, wzaAudytUm);
		this.wzaAudytUm = wzaAudytUm;
	}

	public String getWzaAudytUt() {
		this.onRXGet("wzaAudytUt");
		return this.wzaAudytUt;
	}

	public void setWzaAudytUt(String wzaAudytUt) {
		this.onRXSet("wzaAudytUt", this.wzaAudytUt, wzaAudytUt);
		this.wzaAudytUt = wzaAudytUt;
	}

	public String getWzaOpis() {
		this.onRXGet("wzaOpis");
		return this.wzaOpis;
	}

	public void setWzaOpis(String wzaOpis) {
		this.onRXSet("wzaOpis", this.wzaOpis, wzaOpis);
		this.wzaOpis = wzaOpis;
	}

	public Long getWzaPrcId() {
		this.onRXGet("wzaPrcId");
		return this.wzaPrcId;
	}

	public void setWzaPrcId(Long wzaPrcId) {
		this.onRXSet("wzaPrcId", this.wzaPrcId, wzaPrcId);
		this.wzaPrcId = wzaPrcId;
	}

	public String getWzaRodzaj() {
		this.onRXGet("wzaRodzaj");
		return this.wzaRodzaj;
	}

	public void setWzaRodzaj(String wzaRodzaj) {
		this.onRXSet("wzaRodzaj", this.wzaRodzaj, wzaRodzaj);
		this.wzaRodzaj = wzaRodzaj;
	}

}
