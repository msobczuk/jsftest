package com.comarch.egeria.pp.wnioskiZaswiadczenia;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.wnioskiZaswiadczenia.model.PptWnioskiZaswiadczenia;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@ManagedBean // JBossTools
@Named
@Scope("view")

public class WnioskiZaswiadczeniaBean extends SqlBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	

	private PptWnioskiZaswiadczenia wniosek = new PptWnioskiZaswiadczenia();
	
	@PostConstruct
	public void init() {
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		if (id == null) {
			PptWnioskiZaswiadczenia w =  new PptWnioskiZaswiadczenia();
			w.setWzaPrcId( AppBean.getUser().getPrcIdLong() );
			this.setWniosek(w);
		} else {
			this.setWniosek((PptWnioskiZaswiadczenia) HibernateContext.get(PptWnioskiZaswiadczenia.class, (Serializable) id));
		}
	}
	

 	
	public void zapisz() {
//		if (!validateBeforeSaveAktualizacjeDanych())
//			return;
		try {
			HibernateContext.saveOrUpdate(this.getWniosek());
			User.info("Udany zapis wniosku.");
//			obieg.setIdEncji(this.getWniosek().getWadId());
		} catch (Exception e) {
			User.alert("Błąd w zapisie wniosku!");
			log.error(e.getMessage(), e);
		}
	}
	
	public void usun() {
		try {
			HibernateContext.delete(getWniosek());
			FacesContext.getCurrentInstance().getExternalContext().redirect("WnioskiAktualizacje"); //TODO PODMIENIĆ NA WŁAŚCIWE
		} catch (Exception e) {
			User.alert("Błąd w usuwaniu wniosku!");
			log.error(e.getMessage(), e);
		}
	}



	public PptWnioskiZaswiadczenia getWniosek() {
		return wniosek;
	}



	public void setWniosek(PptWnioskiZaswiadczenia wniosek) {
		this.wniosek = wniosek;
	}
	

}
