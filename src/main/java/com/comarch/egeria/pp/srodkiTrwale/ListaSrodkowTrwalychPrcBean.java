package com.comarch.egeria.pp.srodkiTrwale;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.inject.Named;
import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

@Scope("view")
@Named
public class ListaSrodkowTrwalychPrcBean extends SqlBean implements Serializable {
	private static final Logger log = LogManager.getLogger();



	public SqlDataSelectionsHandler getPplSrodkiTrwale(){
		return this.getLW("PPL_SRODKI_TRWALE");
	}

	public  SqlDataSelectionsHandler getPplPracownicy(){
        return this.getLW("PPL_PRACOWNICY");
    }

	public void generujWnioskiOZmianePracownika(DataRow rNaPrc){
	    if (rNaPrc==null) return;
		String info = "";

		SqlDataSelectionsHandler lw = this.getPplSrodkiTrwale();
		List<DataRow> selectedRows = lw.getSelectedRows();
		if (selectedRows.isEmpty()){
			User.warn("Nie zaznaczono żadnego wiersza.");
			return;
		}

		try(Connection conn = JdbcUtils.getConnection()){
			for (DataRow rStPodz: selectedRows) {
                String ret = JdbcUtils.sqlFnVarchar(conn, "ppadm.st_wniosek_zm_os_odp",  rStPodz.get("ps_id"), rNaPrc.getIdAsString());

                if (ret!=null && !ret.startsWith("000000"))
                    User.alert(ret);
				else {
					String wnNr = ""+JdbcUtils.sqlExecScalarQuery(conn, "select wn_numer from stt_wnioski where wn_id = ? ", ret);
					info += String.format("\r\n\r\nwniosek nr: %1$s \r\nnr inw.: %2$s; \r\nnazwa: %3$s", wnNr, rStPodz.get("sdn_numer_inw"), rStPodz.get("sdn_nazwa"));
				}
            }

			if (!info.isEmpty()){
				User.info("Wygenerowano wnioski o zmianę osoby odpowiedzialnej dla ST: " + info);
			}

			lw.clearSelection();
			lw.resetTs();

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e);
		}
	}

	public void generujWnioskiOLikwidacje(){

		String info = "";

		SqlDataSelectionsHandler lw = this.getPplSrodkiTrwale();
		List<DataRow> selectedRows = lw.getSelectedRows();
		if (selectedRows.isEmpty()){
			User.warn("Nie zaznaczono żadnego wiersza.");
			return;
		}

		try(Connection conn = JdbcUtils.getConnection()){
			for (DataRow rStPodz: selectedRows) {
				String ret = JdbcUtils.sqlFnVarchar(conn, "ppadm.st_wniosek_likw", rStPodz.get("sdn_id"));

				if (ret!=null && !ret.startsWith("000000"))
					User.alert(ret);
				else {
					String wnNr = ""+JdbcUtils.sqlExecScalarQuery(conn, "select wn_numer from stt_wnioski where wn_id = ? ", ret);
					info += String.format("\r\n\r\nwniosek nr: %1$s \r\nnr inw.: %2$s; \r\nnazwa: %3$s", wnNr, rStPodz.get("sdn_numer_inw"), rStPodz.get("sdn_nazwa"));
				}
			}

			if (!info.isEmpty()){
				User.info("Wygenerowano wnioski o likwidację dla ST: " + info);
			}

			lw.clearSelection();
			lw.resetTs();

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e);
		}
	}
}
