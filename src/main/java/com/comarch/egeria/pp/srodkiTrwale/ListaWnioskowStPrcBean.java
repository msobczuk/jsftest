package com.comarch.egeria.pp.srodkiTrwale;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.inject.Named;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@Scope("view")
@Named
public class ListaWnioskowStPrcBean extends SqlBean implements Serializable {
	private static final Logger log = LogManager.getLogger();



	public SqlDataSelectionsHandler getPplSrodkiTrwale(){
		return this.getLW("PPL_SRODKI_TRWALE");
	}

	public SqlDataSelectionsHandler getPplSrodkiTrwaleWnioski(){
		return this.getLW("PPL_SRODKI_TRWALE_WNIOSKI");
	}

	public  SqlDataSelectionsHandler getPplPracownicy(){
        return this.getLW("PPL_PRACOWNICY");
    }


	public void zatwierdzWybraneWnioski(){

		SqlDataSelectionsHandler lw = this.getPplSrodkiTrwaleWnioski();
		List<DataRow> selectedRows = lw.getSelectedRows();

		try(Connection conn = JdbcUtils.getConnection()){

			String info = "";
			for (DataRow r : selectedRows) {

				Object wnNumer = r.get("wn_numer");
				Object wnpKod = r.get("wnp_kod");


				if (eq("T", r.get("wn_f_anulowany"))){
					User.alert("Brak możliwości zatwierdzenia wniosku '%1$s'. Wniosek ten został wcześniej anulowany.", wnNumer);
					continue;
				}

				if (eq("T", r.get("wn_f_zatwierdzony"))){
					User.alert("Brak możliwości zatwierdzenia wniosku '%1$s'. Wniosek ten został wcześniej zatwierdzony.", wnNumer);
					continue;
				}

				User user = User.getCurrentUser();
				if (eq("ZM_OS_ODP", wnpKod) && !eq(""+user.getPrcIdLong(), ""+r.get("wnp_wartosc_na"))){

//					User.alert("Wniosek o zmianę pracownika '%1$s' powinien zostać zatwierdzony przez pracownika, który ma go przyjąć (%2$s).", wnNumer, r.get("wnp_na_prc_info"));
//					continue;

					DataRow rPodwl = this.getLW("PPL_ZALOGOWANY_PODLAWDNI").find(r.get("wnp_wartosc_na"));
					if (
							(rPodwl == null ||	rPodwl.getAsLong("PRZ_POZIOM")==0L)//zalogowany nie jest przelozonym
							&& !user.hasPermission("PP_ST_ADMINISTRATOR") && !user.hasPermission("PP_ADMINISTRATOR")
					){
						User.alert("Wniosek '%1$s' o zmianę pracownika powinien zostać zatwierdzony przez przełożonego pracownika, który ma go przyjąć (%2$s), albo przez administratora ST (wymagana f.u.: PP_ST_ADMINISTRATOR lub PP_ADMINISTRATOR).",
								wnNumer, r.get("wnp_na_prc_info"));
						continue;
					}
				}

				if (eq("LIKW",wnpKod)){
					DataRow rPodwl = this.getLW("PPL_ZALOGOWANY_PODLAWDNI").find(r.get("WN_PRC_ID"));
					if (
							(rPodwl == null ||	rPodwl.getAsLong("PRZ_POZIOM")==0L)//zalogowany nie jest przelozonym
									&& !user.hasPermission("PP_ST_ADMINISTRATOR") && !user.hasPermission("PP_ADMINISTRATOR")
					){
						User.alert("Wniosek '%1$s' o likwidację powinien zostać zatwierdzony przez przełożonego pracownika, który go utworzył (%2$s), albo przez administratora ST (wymagana f.u.: PP_ST_ADMINISTRATOR lub PP_ADMINISTRATOR).",
								wnNumer, r.get("wn_prc_info"));
						continue;
					}

				}


				JdbcUtils.sqlExecNoQuery(conn, "update stt_wnioski set wn_f_zatwierdzony = 'T' where wn_id = ? ", r.get("wn_id"));
                JdbcUtils.sqlSPCall(conn,"ppadm.st_wniosek_wiadomosci",  r.get("wn_id"), "Środki trwałe / lista wniosków: zatwierdzono wniosek " + wnNumer);
				info += String.format("\r\n\r\nwn. nr <strong><b>%1$s</b></strong>\r\nnr inw.: %2$s \r\nnazwa: %3$s",
						r.get("wn_numer"),	 r.get("sdn_numer_inw"), r.get("sdn_nazwa"));
			}

			if (!info.isEmpty()){
				User.info("Zatwierdzono: " + info);
			}

			lw.clearSelection();
			lw.resetTs();

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e);

		}

	}



	public void anulujWybraneWnioski(){

		SqlDataSelectionsHandler lw = this.getPplSrodkiTrwaleWnioski();
		List<DataRow> selectedRows = lw.getSelectedRows();

		try(Connection conn = JdbcUtils.getConnection()){

			String info = "";
			for (DataRow r : selectedRows) {

				Object wnNumer = r.get("wn_numer");

				if (eq("T", r.get("wn_f_anulowany"))){
					User.alert("Brak możliwości anulowania wniosku '%1$s'. Wniosek ten został wcześniej anulowany.", wnNumer);
					continue;
				}

				if (eq("T",r.get("wn_f_zatwierdzony"))){
					User.alert("Brak możliwości anulowania wniosku '%1$s'. Wniosek ten został wcześniej zatwierdzony.", wnNumer);
					continue;
				}

				if (eq("T", r.get("wn_f_dok_wygenerowano"))){
					User.alert("Brak możliwości anulowania wniosku '%1$s'. Dla tego wniosku wygenerowano dokumenty.", wnNumer);
					continue;
				}

				JdbcUtils.sqlExecNoQuery(conn, "update stt_wnioski set wn_f_anulowany = 'T' where wn_id = ? ", r.get("wn_id"));
				JdbcUtils.sqlSPCall(conn,"ppadm.st_wniosek_wiadomosci",  r.get("wn_id"), "Środki trwałe / lista wniosków: anulowano wniosek " + wnNumer);
				info += String.format("\r\n\r\nwn. nr <strong><b>%1$s</b></strong>\r\nnr inw.: %2$s \r\nnazwa: %3$s",
									                         r.get("wn_numer"),	 r.get("sdn_numer_inw"), r.get("sdn_nazwa"));
			}

			if (!info.isEmpty()){
				User.info("Anulowano: " + info);
			}

			lw.clearSelection();
			lw.resetTs();

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e);

		}

	}




	public void generujDokumentyWybraneWnioski(){

		SqlDataSelectionsHandler lw = this.getPplSrodkiTrwaleWnioski();
		List<DataRow> selectedRows = lw.getSelectedRows();

		try(Connection conn = JdbcUtils.getConnection()){

			String info = "";

			for (DataRow r : selectedRows) {

				Object wnNumer = r.get("wn_numer");

				if (eq("T", r.get("wn_f_anulowany"))){
					User.alert("Brak możliwości generowania dokumentów dla wniosku '%1$s'. Wniosek ten został wcześniej anulowany.", wnNumer);
					continue;
				}

				if (!eq("T",r.get("wn_f_zatwierdzony"))){
					User.alert("Brak możliwości generowania dokumentów dla wniosku '%1$s'. Wniosek nie został zatwierdzony.", wnNumer);
					continue;
				}

				if (eq("T", r.get("wn_f_dok_wygenerowano"))){
					User.alert("Brak możliwości generowania dokumentów dla wniosku '%1$s'. Dla tego wniosku wygenerowano dokumenty.", wnNumer);
					continue;
				}


				String ret = JdbcUtils.sqlFnVarchar(conn, "PPADM.ST_WNIOSEK_GENERUJ_DOK",  r.get("wn_id"));

				if (ret!=null && !ret.startsWith("000000"))
					User.alert(r.get("wn_numer") + " -> " + ret);
				else {
					String dokNr = ""+JdbcUtils.sqlExecScalarQuery(conn, "select dok_id from kgt_dokumenty where dok_id = ? ", ret);
					info += String.format("\r\n\r\n<strong><b>dokument %1$s</b></strong>\r\nwn. nr: %2$s \r\nnr inw.: %3$s \r\nnazwa: %4$s",
												dokNr, 			r.get("wn_numer"),	 r.get("sdn_numer_inw"), r.get("sdn_nazwa"));
				}
			}

			if (!info.isEmpty()){
				User.info("Wygenerowano: " + info);
			}

			lw.clearSelection();
			lw.resetTs();

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e);

		}

	}



	public void usunWybraneWnioski(){

		SqlDataSelectionsHandler lw = this.getPplSrodkiTrwaleWnioski();
		List<DataRow> selectedRows = lw.getSelectedRows();

		try(Connection conn = JdbcUtils.getConnection()){

			String info = "";
			for (DataRow r : selectedRows) {

				Object wnNumer = r.get("wn_numer");

				if (eq("T",r.get("wn_f_zatwierdzony"))){
					User.alert("Brak możliwości usunięcia wniosku '%1$s'. Wniosek ten został wcześniej zatwierdzony.", wnNumer);
					continue;
				}

				if (eq("T", r.get("wn_f_dok_wygenerowano"))){
					User.alert("Brak możliwości usunięcia wniosku '%1$s'. Dla tego wniosku wygenerowano dokumenty.", wnNumer);
					continue;
				}

				JdbcUtils.sqlExecNoQuery(conn, "delete stt_wnioski_parametry where wnp_id = ? ", r.get("wnp_id"));
				JdbcUtils.sqlExecNoQuery(conn, "delete stt_wnioski where wn_id = ? ", r.get("wn_id"));
				JdbcUtils.sqlExecNoQuery(conn, "delete stt_inwentaryzacje where i_id = ? ", r.get("wn_i_id"));

				info += String.format("\r\n\r\nwn. nr <strong><b>%1$s</b></strong>\r\nnr inw.: %2$s \r\nnazwa: %3$s",
						r.get("wn_numer"),	 r.get("sdn_numer_inw"), r.get("sdn_nazwa"));
			}

			if (!info.isEmpty()){
				User.info("Usunięto: " + info);
			}

			lw.clearSelection();
			lw.resetTs();

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e);

		}

	}


}
