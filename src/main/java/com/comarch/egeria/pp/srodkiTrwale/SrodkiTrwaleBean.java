package com.comarch.egeria.pp.srodkiTrwale;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import org.springframework.context.annotation.Scope;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Scope("view")
@Named
public class SrodkiTrwaleBean extends SqlBean implements Serializable {
	
	@Inject
    SessionBean sessionBean;

	String obecnaData = null;
	private SqlDataSelectionsHandler srTrwSlownik = null;

	public ArrayList<DataRow> getListaSrodkow() {
		if (this.srTrwSlownik == null) {
			this.setListaSrodkow(this.getLW("PPL_WSP_SRODKI_TRWALE", sessionBean.getPrc_id()));
		}
		return this.srTrwSlownik.getData();
	}
	
	public void setListaSrodkow(SqlDataSelectionsHandler srTrwSlownik) {
		this.srTrwSlownik = srTrwSlownik;
	}
	
	public String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		this.obecnaData = dateFormat.format(date);
		return this.obecnaData;
	}

}
