package com.comarch.egeria.pp.zalaczniki;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.file.UploadedFile;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.reports.ReportGenerator;
import com.lowagie.text.DocumentException;

import net.sf.jasperreports.engine.JRException;

@Named
// @SessionScoped
@Scope("view")
//@ManagedBean
public class ZalacznikiBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 8281063341812990283L;
	private static final Logger log = LogManager.getLogger();
	
	
	private String dlgTitle ="";
	private String zalacznikFkField;//typ obiektu/katgoria
	private Long zalacznikFkId; //id encji (np. oceny)
	private String opisZalacznika = "";
	private Boolean showSearchField = true;

	
	@PostConstruct
	public void init(){
//		System.out.println("ZalacznikiBean.init()...");
	}
	
	
	
	public void resetTs(){
		 SqlDataSelectionsHandler lw = getSql("sqlZalaczniki");
		 if (lw!=null) 
			 lw.resetTs();
	}
	
	public void prepareDlgZalaczniki(String dlgTitle, String zalFkField, Long zalFkId, Boolean showSerachField){
		this.zalacznikFkField = zalFkField;
		this.zalacznikFkId = zalFkId;
		this.dlgTitle = dlgTitle;

		if (showSerachField==null) showSearchField = true;
		this.showSearchField = showSerachField;

		this.getSql("sqlZalaczniki");
		
//		sql.getSqlParams().clear();
//		sql.getSqlParams().add(zalFkId);
//		sql.getData();
	}
	
	
	
	public void deleteAttachment(DataRow r){
		
		try(Connection con = JdbcUtils.getConnection()){
			JdbcUtils.sqlExecNoQuery(con, "delete from zkt_zalaczniki where zal_id = ?", r.getIdAsLong());
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		SqlDataSelectionsHandler lw = getSql("sqlZalaczniki");
		lw.resetTs();
		lw.setCurrentRow(null);
		lw.setRowSelection(false, r);
		System.out.println();
	}
	
	public void deleteAttachments(ArrayList<DataRow> rows){
		for (DataRow r : (ArrayList<DataRow>) rows.clone()) {
			deleteAttachment(r);
		}
	}
	
	
	public void deleteAll() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		if (this.zalacznikFkField==null || "".equals(this.zalacznikFkField)) 
			return;
		 
		try(Connection con = JdbcUtils.getConnection()){
			JdbcUtils.sqlExecNoQuery(con, "delete from zkt_zalaczniki where " +  this.zalacznikFkField + " =  ? ", this.zalacznikFkId);
		}
		getSql("sqlZalaczniki").resetTs();
	}
	
	
	
	public static void deleteAll( String  zalacznikFkField, Long zalacznikFkId) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException{
		if (zalacznikFkField==null || "".equals(zalacznikFkField)) 
			return;
		try(Connection con = JdbcUtils.getConnection()){
			JdbcUtils.sqlExecNoQuery(con, "delete from zkt_zalaczniki where " +  zalacznikFkField + " =  ? ", zalacznikFkId);
		}
	}
	

	public void addAttachment(FileUploadEvent event) {
		
//		System.out.println("addAttachment...");

		UploadedFile attachmentFile = event.getFile();

//		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		long size = attachmentFile.getSize();
		if (size<=0){
			User.alert("Wskazany plik jest pusty.");
			return;
		}
		
//		System.out.println("////////////////////////////");
//		System.out.println(zalacznikFkId);
//		System.out.println("/////////////////////////////");
		

		if (zalacznikFkId != null) {
			String plsql = "" 
				
					+ "DECLARE" 
					+ "		zalacznik zkt_zalaczniki%ROWTYPE;" 
					+ "		wynik NUMBER;" 
					+ "BEGIN"
					+ "     zalacznik.ZAL_F_UPRAWNIENIA_SPECJALNE := 'N' ;" 
					+ "		zalacznik.zal_nazwa 		:= '" + attachmentFile.getFileName() + "';"
					+ "		zalacznik.zal_typ 			:=1;" 
					+ " 	zalacznik." + zalacznikFkField + " :=" + zalacznikFkId + ";" 
					+ "		zalacznik.zal_nazwa_pliku :='" + attachmentFile.getFileName() + "';"
					+ " 	zalacznik.zal_blob := ? ;"
					+ "     zalacznik.ZAL_OPIS := ? ;"
					+ "		wynik := ZKP_ZALACZNIKI.DODAJ_ZALACZNIK(zalacznik);"
					+ "END;";

			try (Connection conn = JdbcUtils.getConnection()) {
				CallableStatement cs = conn.prepareCall(plsql);
				cs.setBlob(1, attachmentFile.getInputStream());
				cs.setString(2, this.opisZalacznika);
				cs.execute();
				
				this.opisZalacznika = "";
				
//				System.out.println("... addAttachment ... OK: " + attachmentFile.getFileName());
			} catch (Exception e) {
				e.printStackTrace();
//				FacesMessage message = new FacesMessage("Błąd dodawania załącznika. SQLEXception");
//				facesContext.addMessage(null, message);
			}
		} else {
//			FacesMessage message = new FacesMessage("Najpierw zapisz dane dokumentu!");
//			facesContext.addMessage(null, message);
			User.warn("Najpierw zapisz dane dokumentu!");
		}
		
		getSql("sqlZalaczniki").resetTs();
		getSql("sqlZalaczniki").getData();
	}






	public static void generujRaportPdfZapiszJakoZalacznik(
			String reportName,
			HashMap<String, Object> params,
			String zalacznikFkField,
			Object zalacznikFkId) throws SQLException, IOException, JRException
	{
		generujRaportPdfZapiszJakoZalacznik(reportName, params, reportName + ".pdf", zalacznikFkField, zalacznikFkId, null);
	}

	public static void generujRaportPdfZapiszJakoZalacznik(
			String reportName,
			HashMap<String, Object> params,
			String zalacznikFkField,
			Object zalacznikFkId,
			String opisZalacznika) throws SQLException, IOException, JRException
	{
		generujRaportPdfZapiszJakoZalacznik(reportName, params, reportName + ".pdf", zalacznikFkField, zalacznikFkId, opisZalacznika);
	}

	public static void generujRaportPdfZapiszJakoZalacznik(
			String reportName,
			HashMap<String, Object> params,
			String pdfFileName,
			String zalacznikFkField,
			Object zalacznikFkId,
			String opisZalacznika) throws SQLException, IOException, JRException
	{
		if (pdfFileName==null || pdfFileName.trim().isEmpty())
			pdfFileName = reportName+".pdf";

		PipedInputStream stream = ReportGenerator.pdfAsStream(reportName, params);
		ZalacznikiBean.zapiszRaportStream(stream, pdfFileName, zalacznikFkField, zalacznikFkId, opisZalacznika );

	}
	
	public static void LWJakoPdfZapiszZalacznik(
			SqlDataSelectionsHandler lw,
			String pdfFileName,
			String zalacznikFkField,
			Object zalacznikFkId,
			String opisZalacznika) throws IOException, DocumentException 
	{
		if (pdfFileName==null || pdfFileName.trim().isEmpty())
			pdfFileName = lw.getShortName()+".pdf";
		
		PipedInputStream stream = ReportGenerator.getLwAsPdfStream(lw);
		ZalacznikiBean.zapiszRaportStream(stream, pdfFileName, zalacznikFkField, zalacznikFkId, opisZalacznika );
	}




	public static void zapiszZalacznik(
			byte[] bytes,
			String pdfFileName,
			String zalacznikFkField,
			Object zalacznikFkId,
			String opisZalacznika) throws IOException
	{
		PipedInputStream stream = Utils.asPipedInputStream(bytes);
		ZalacznikiBean.zapiszRaportStream(stream, pdfFileName, zalacznikFkField, zalacznikFkId, opisZalacznika);
	}




	public static void zapiszRaportStream(PipedInputStream stream, String nazwa, String zalacznikFkField, Object zalacznikFkId, String opisZalacznika) {
		
		
		if (zalacznikFkId != null) {
			String plsql = "" 
				
					+ "DECLARE" 
					+ "		zalacznik zkt_zalaczniki%ROWTYPE;" 
					+ "		wynik NUMBER;" 
					+ "BEGIN"
					+ "     zalacznik.ZAL_F_UPRAWNIENIA_SPECJALNE := 'N' ;" 
					+ "		zalacznik.zal_nazwa 		:= '" + nazwa + "';"
					+ "		zalacznik.zal_typ 			:=1;" 
					+ " 	zalacznik." + zalacznikFkField + " :=" + zalacznikFkId + ";" 
					+ "		zalacznik.zal_nazwa_pliku :='" + nazwa + "';"
					+ " 	zalacznik.zal_blob := ? ;"
					+ "     zalacznik.ZAL_OPIS := ? ;"
					+ "		wynik := ZKP_ZALACZNIKI.DODAJ_ZALACZNIK(zalacznik);"
					+ "END;";

			try (Connection conn = JdbcUtils.getConnection()) {
				CallableStatement cs = conn.prepareCall(plsql);
				cs.setBlob(1, stream);
				cs.setString(2, opisZalacznika);
				cs.execute();

			} catch (Exception e) {
				e.printStackTrace();
			}
		}  else 
			System.out.println("Błąd przy zapisywaniu załącznika ze streama");
		
		User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");

        SqlDataSelectionsHandler sqlZalaczniki = user.getSql("sqlZalaczniki");
        if (sqlZalaczniki!=null) {
            sqlZalaczniki.resetTs();
            sqlZalaczniki.getData();
        }
		
	}

	
	
	public StreamedContent pobierz(DataRow dr){

		try {
			return downloadAttachmentFromDB(dr.getIdAsLong(), ""+dr.get("zal_nazwa"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	

	
	
	public StreamedContent pobierzSelected(){
		SqlDataSelectionsHandler zLista = this.getSql("sqlZalaczniki");
		List<DataRow> sel = zLista.getSelectedRows();
		
		
		if (sel.isEmpty()) return null;
		
		if (sel.size()==1){
			return pobierz(sel.get(0));
		}
		

		try {
			HashSet<String> fileNames = new HashSet<String>();
			
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ZipOutputStream zos = new ZipOutputStream(baos);
			for (DataRow dr : sel) {
				addToZipFile(dr, zos, fileNames);
			}	
			zos.close();
			ByteArrayInputStream bis = new ByteArrayInputStream(baos.toByteArray() );
			return new DefaultStreamedContent(bis , contentType("zalaczniki.zip"), "zalaczniki.zip");
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		return null;
	}
	
	
	
	public void addToZipFile(DataRow dr, ZipOutputStream zos, HashSet<String> fileNames ) throws IOException {
		//System.out.println("Writing '" + fileName + "' to zip file");
		String fileName = ""+dr.get("zal_nazwa");
		int i = 1;
		while (fileNames.contains(fileName)){
			
			fileName =  ""+dr.get("zal_nazwa") + "." + i++;

		}
		fileNames.add(fileName);
		
		StreamedContent os = pobierz(dr);
		InputStream is = os.getStream(); //new FileInputStream(file);
		ZipEntry zipEntry = new ZipEntry(fileName);
		zos.putNextEntry(zipEntry);
		byte[] bytes = new byte[1024];
		int length;
		while ((length = is.read(bytes)) >= 0) {
			zos.write(bytes, 0, length);
		}
		zos.closeEntry();
		is.close();
	}
	
	
	
	
	 public DefaultStreamedContent downloadAttachmentFromDB(Long attachmentId, String attachmentName) throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException {
	
		 try (Connection conn = JdbcUtils.getConnection()) {
			 
			 Object oBlob = JdbcUtils.sqlExecScalarQuery(conn, "select ZAL_BLOB from zkt_Zalaczniki where zal_id = ?", attachmentId);
			 if (oBlob==null) {
				 User.alert("Zawartość załącznika %1$s [id: %2$s] została usunięta z bazy.", attachmentName, attachmentId);
				 return null;
			 }
			 Blob blob = (Blob) oBlob;
			 return new DefaultStreamedContent(blob.getBinaryStream(), contentType(attachmentName), attachmentName);

		 } catch (SQLException e) {
			e.printStackTrace();
			//TODO...
		 }
	
		 return null;
	 }
	 
	 
	 
	 public String contentType(String fileName) {
		 fileName = (""+fileName).toLowerCase();
		 
		 if (fileName.endsWith("doc") || fileName.endsWith("docx"))
			 return "application/msword";
		
		 else if (fileName.endsWith("jpg") || fileName.endsWith("jpeg"))
			 return "image/jpeg";
		
		 else if (fileName.endsWith("pdf"))
			 return "application/pdf";
		
		 else if (fileName.endsWith("gif"))
			 return "image/gif";
		
		 else if (fileName.endsWith("png"))
			 return "image/x-png";
		
		 else if (fileName.endsWith("xls"))
			 return "application/application/vnd.ms-excel";
		
		 else if (fileName.endsWith("xlsx"))
			 return
		 "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		
		 else if (fileName.endsWith("ppt"))
		 return "application/vnd.ms-powerpoint";
		
		 else if (fileName.endsWith("pptx"))
			 return
		 "application/vnd.openxmlformats-officedocument.presentationml.presentation";
		
		 else if (fileName.endsWith("csv"))
			 return "text/csv";
		
		 else if (fileName.endsWith("txt"))
			 return "text/plain";
		
		
		 return null;
	
	 }
	
	
	public Long getZalacznikFkId() {
		return zalacznikFkId;
	}

	public String setZalacznikFkId(Long zalacznikFkId) {
		this.zalacznikFkId = zalacznikFkId;
		return "";//bo ustawiamy bezposrednio z kodu xhtml
	}

	public String getZalacznikFkField() {
		return zalacznikFkField;
	}

	public String setZalacznikFkField(String zalacznikFkField) {
		this.zalacznikFkField = zalacznikFkField;
		return "";//bo ustawiamy bezposrednio z kodu xhtml
	}



	public String getDlgTitle() {
		return dlgTitle;
	}



	public void setDlgTitle(String dlgTitle) {
		this.dlgTitle = dlgTitle;
	}


	public Boolean getShowSearchField() {
		return showSearchField;
	}

	public void setShowSearchField(Boolean showSearchField) {
		this.showSearchField = showSearchField;
	}



	public String getOpisZalacznika() {
		if (this.getSql("sqlZalaczniki") == null) return null;
		
		if ( this.getSql("sqlZalaczniki").getSelectedRows().size()==1 ) {
			
			return ""+this.getSql("sqlZalaczniki").getSelectedRows().get(0).get("zal_opis") ;
			
		} else if ( this.getSql("sqlZalaczniki").getSelectedRows().size()>1 ) {
			
			return "";
			
		}
		return opisZalacznika;
	}



	public void setOpisZalacznika(String opisZalacznika) {
		this.opisZalacznika = (""+opisZalacznika).trim();
	}

	
	
	
}
