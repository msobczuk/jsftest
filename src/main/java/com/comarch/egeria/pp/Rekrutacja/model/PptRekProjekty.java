package com.comarch.egeria.pp.Rekrutacja.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PPT_REK_PROJEKTY database table.
 * 
 */
@Entity
@Table(name="PPT_REK_PROJEKTY", schema="PPADM")
@NamedQuery(name="PptRekProjekty.findAll", query="SELECT p FROM PptRekProjekty p")
public class PptRekProjekty implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_REK_PROJEKTY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_REK_PROJEKTY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_REK_PROJEKTY")
	@Column(name="NAP_ID")
	private long napId;
	
	@Column(name="NAP_ETAT_PROJEKT")
	private Double napEtatProjekt;

	@Column(name="NAP_PROJ_OB_ID")
	private Long napProjObId;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="NAP_AUDYT_DM")
	private Date napAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="NAP_AUDYT_DT")
	private Date napAudytDt;

	@Column(name="NAP_AUDYT_KM")
	private String napAudytKm;

	@Column(name="NAP_AUDYT_LM")
	private String napAudytLm;

	@Column(name="NAP_AUDYT_UM")
	private String napAudytUm;

	@Column(name="NAP_AUDYT_UT")
	private String napAudytUt;



	//bi-directional many-to-one association to PptRekNabory
	@ManyToOne
	@JoinColumn(name="NAP_NAB_ID")
	private ZptNabory zptNabory;

	public PptRekProjekty() {
	}

	public long getNapId() {
		return this.napId;
	}

	public void setNapId(long napId) {
		this.napId = napId;
	}

	public Date getNapAudytDm() {
		return this.napAudytDm;
	}

	public void setNapAudytDm(Date napAudytDm) {
		this.napAudytDm = napAudytDm;
	}

	public Date getNapAudytDt() {
		return this.napAudytDt;
	}

	public void setNapAudytDt(Date napAudytDt) {
		this.napAudytDt = napAudytDt;
	}

	public String getNapAudytKm() {
		return this.napAudytKm;
	}

	public void setNapAudytKm(String napAudytKm) {
		this.napAudytKm = napAudytKm;
	}

	public String getNapAudytLm() {
		return this.napAudytLm;
	}

	public void setNapAudytLm(String napAudytLm) {
		this.napAudytLm = napAudytLm;
	}

	public String getNapAudytUm() {
		return this.napAudytUm;
	}

	public void setNapAudytUm(String napAudytUm) {
		this.napAudytUm = napAudytUm;
	}

	public String getNapAudytUt() {
		return this.napAudytUt;
	}

	public void setNapAudytUt(String napAudytUt) {
		this.napAudytUt = napAudytUt;
	}

	public Double getNapEtatProjekt() {
		return this.napEtatProjekt;
	}

	public void setNapEtatProjekt(Double napEtatProjekt) {
		this.napEtatProjekt = napEtatProjekt;
	}

	public Long getNapProjObId() {
		return this.napProjObId;
	}

	public void setNapProjObId(Long napProjObId) {
		this.napProjObId = napProjObId;
	}

	public ZptNabory getZptNabory() {
		return this.zptNabory;
	}

	public void setZptNabory(ZptNabory zptNabory) {
		this.zptNabory = zptNabory;
	}

}