package com.comarch.egeria.pp.Rekrutacja;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@ManagedBean
@Named
@Scope("view")
public class ListaKartotekBean extends SqlBean {

	@Inject
	private SessionBean sessionBean;

	public void editSelectedRow() {
		System.out.println(
				"=================================ListaKartotekBean.editSelectedRow===================================");

		SqlDataSelectionsHandler p = this.getSql("PP_REK_KARTOTEKI");
		if (p != null) {
			System.out.println(p.getName() + " / currentRow: " + p.getCurrentRow());

			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id",
					p.getCurrentRow().getIdAsString());
			// sessionBean.getIdHM().put(vRootId, r.getIdAsString());

			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", p.getCurrentRow());

			
			
			try (HibernateContext h = new HibernateContext()) {
				Session s = h.getSession();
				DataRow wniosekRekrutacja = (DataRow) FacesContext.getCurrentInstance().getExternalContext().getFlash()
						.get("row");
				//TODO ustalenie rodzaju na podstawie zapytania - w przypadku zmiany listy wartości konieczna zmiana numeru kolumny
				String rodzaj = (String) wniosekRekrutacja.get("NAB_RODZAJ_NABORU");
				
//				zakomentowane - wniosek nie jest tworzony przez ZptNabory
//				ZptNabory nabor = s.get(ZptNabory.class, wniosekRekrutacja.getIdAsLong());

				if(rodzaj == null) {
					User.warn("Nabór nie posiada rodzaju!");
					return;
				}
				
				if (       rodzaj.equals("WEWN") 
						|| rodzaj.equals("JED_PODL") 
						|| rodzaj.equals("KSC") 
						|| rodzaj.equals("WEW_ST_POM") 
						|| rodzaj.equals("ST_POM") 
						|| rodzaj.equals("KSAP") 
						|| rodzaj.equals("WYZ_ST_SC")
						) {
					FacesContext.getCurrentInstance().getExternalContext().redirect("KartotekaRekrutacji");
				}


			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		System.out.println(
				"============================================================================================");
	}


}
