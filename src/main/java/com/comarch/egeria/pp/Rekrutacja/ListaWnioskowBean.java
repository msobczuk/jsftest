package com.comarch.egeria.pp.Rekrutacja;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.CLookupTableBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;

@ManagedBean
@Named
@Scope("view")
public class ListaWnioskowBean extends SqlBean {
	private static final long serialVersionUID = 1L;

	public void editSelectedRow() throws Exception { 
		
		SqlDataSelectionsHandler lw = CLookupTableBean.findCurrentUIComponentDatasource();
		if (lw!=null && lw.getCurrentRow()!=null) {

			DataRow r = lw.getCurrentRow();
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("id", r.getIdAsString());
			FacesContext.getCurrentInstance().getExternalContext().getFlash().put("row", r);
			String xhtmlNr = RekrutacjaWniosekBean.mapujWslWartoscNaRekrutacjaNrXhtml((String) r.get("NAB_RODZAJ_NABORU"));
			if (xhtmlNr!=null)
				FacesContext.getCurrentInstance().getExternalContext().redirect( xhtmlNr );
			
		}
	}
	
}
