package com.comarch.egeria.pp.Rekrutacja;

import static com.comarch.egeria.Utils.*;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.Obiegi.ObiegBean;
import com.comarch.egeria.pp.Rekrutacja.model.PptRekProjekty;
import com.comarch.egeria.pp.Rekrutacja.model.PptRekRezerwyCelowe;
import com.comarch.egeria.pp.Rekrutacja.model.ZptKandydaci;
import com.comarch.egeria.pp.Rekrutacja.model.ZptNabSymb;
import com.comarch.egeria.pp.Rekrutacja.model.ZptNaborKomisja;
import com.comarch.egeria.pp.Rekrutacja.model.ZptNaborSklad;
import com.comarch.egeria.pp.Rekrutacja.model.ZptNabory;
import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.raporty.GeneratorRaportow;
import com.comarch.egeria.pp.zalaczniki.ZalacznikiBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean
@Named
@Scope("view")
public class RekrutacjaWniosekBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger log = LogManager.getLogger();

	@Inject
	SessionBean sessionBean;

	@Inject
	GeneratorRaportow genrap;

	@Inject
	ObiegBean obieg;

	@Inject
	ZalacznikiBean zalaczniki;

	@Inject
	protected ReportGenerator reportGenerator;

	DataRow wniosekRekrutacja = null;

	ZptNabory nabor = null;

	HashMap<String, Long> jednostkiOrg = new HashMap<>();

	List<ZptNaborKomisja> listaCzlonkow = new ArrayList<>();

	boolean czyWyswietlac = false;

	boolean czyNowy = false;

	String obIdString = "";

	boolean dodanoOgloszenie = false;

	//List<ZptNaborSklad> wynagrodzenieMiesieczne = new ArrayList<>();

	List<String> wybraniCzlonkowieKomisji = new ArrayList<String>();

	public String zakresObow = "";
	public String wymagNiez = "";
	public String wymagDodat = "";
	public String wymaganeWykszt = "";
	public java.util.Date terminZglMail = null;
	public String wymDok = "";
	public java.util.Date terminRozm = null;
	public String kontakt = "";

	String checkString = "";

	private List<ZptNaborKomisja> czlonkowieKomSpozaMF = new ArrayList<>();

	@PostConstruct
	public void init() {
		Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");

		obieg.setKategoriaObiegu("WN_NABORY");

		if (id != null) {
			wniosekRekrutacja = (DataRow) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("row");

		}

		if (wniosekRekrutacja == null) {

			this.setNabor(new ZptNabory());

			this.nabor.setSqlBean(this);

			this.nabor.setZptNaborKomisjas(new ArrayList<>());
//			this.nabor.setZptKandydacis(new HashSet<>());
//			this.nabor.setZptNaborSklads(new HashSet<>());
			// this.nabor.setNabEtatZwyk("N");
			this.nabor.setNabEtatZastepstwo("N");
			this.nabor.setNabEtatRezerwa(0D);
			this.nabor.setNabEtatProjekt(0D);
			this.nabor.setNabEtatZFundWynagr(0D);
			this.nabor.setNabRodzajNaboru(sessionBean.getRodzajNaboru());

			this.loadEgSlownik("TYP_UMOWY");
			ArrayList<DataRow> sqlData = this.getSql("TYP_UMOWY").getData();

			for (DataRow r : sqlData) {
				String typUm = r.get(1).toString();
				if (typUm.equals("Umowa o pracę")) {
					this.nabor.setNabRodzajPropUmowy(r.get(0).toString());
					break;
				}
			}

			this.loadEgSlownik("REK_SKL_RODZAJ");

			SqlDataSelectionsHandler ssh = this.getSql("REK_SKL_RODZAJ");

			if (ssh != null) {

				for (DataRow dr : ssh.getData()) {
					ZptNaborSklad zns = new ZptNaborSklad();
					// 0 badz wsl_wartosc
					zns.setNskRodzaj("" + dr.get(0));

					//wynagrodzenieMiesieczne.add(zns);
					nabor.addZptNaborSklad(zns);
				}

			}

			if (nabor.getZptNabSymbs() == null) {
				nabor.setZptNabSymb(new ArrayList<>());
				addNewSymbolOpisu();
			}

			czyNowy = true;

		} else {

			try (HibernateContext h = new HibernateContext()) {
				Session s = h.getSession();
				this.setNabor(s.get(ZptNabory.class, wniosekRekrutacja.getIdAsLong()));

				this.nabor.setSqlBean(this);

				if (nabor.getNabObId() != null) {
					obIdString = nabor.getNabObId().toString();
				}

				if (nabor.getNabRodzajNaboru() == null) {
					this.nabor.setNabRodzajNaboru(sessionBean.getRodzajNaboru());
				}

//				if (nabor.getZptNaborSklads() != null) {
				//wynagrodzenieMiesieczne = listFromSet(nabor.getZptNaborSklads());

				if (nabor.getZptNaborSklads().isEmpty()) {
					this.loadEgSlownik("REK_SKL_RODZAJ");

					SqlDataSelectionsHandler ssh = this.getSql("REK_SKL_RODZAJ");

					if (ssh != null) {

						for (DataRow dr : ssh.getData()) {
							ZptNaborSklad zns = new ZptNaborSklad();
							// 0 badz wsl_wartosc
							zns.setNskRodzaj("" + dr.get(0));
							
							//wynagrodzenieMiesieczne.add(zns);
							nabor.addZptNaborSklad(zns);
							
						}
					}
				}
//				}

				// Pobieranie do lokalnej listy czlonkow komisji spoza MF
				// Spoza MF jeżeli nie ma nadanego KOM_PRC_ID
				if (nabor.getZptNaborKomisjas() != null) {
					for (ZptNaborKomisja znk : nabor.getZptNaborKomisjas()) {
						if (("" + znk.getKomTyp()).equals("S") && znk.getKomPrcId() == null) {
							czlonkowieKomSpozaMF.add(znk);
						}
					}
				}

				obieg.setIdEncji(this.nabor.getNabId());
				zalaczniki.setZalacznikFkId(this.nabor.getNabId());
//				checkString = getCheckString();
			}
		}

		if (nabor.getPptRekProjekties().isEmpty())
			nabor.addPptRekProjekty(new PptRekProjekty());

		if (nabor.getPptRekRezerwyCelowe().isEmpty())
			nabor.addPptRekRezerwyCelowe(new PptRekRezerwyCelowe());

	}

	public String rodzajSkladnika(String rodzaj) {

		SqlDataSelectionsHandler ssh = this.getSql("REK_SKL_RODZAJ");

		if (ssh != null) {
			for (DataRow dr : ssh.getData()) {
				// 0 badz wsl_wartosc
				if (dr.get(0).equals(rodzaj)) {
					// 1 badz wsl_wartosc
					return "" + dr.get(1);
				}
			}
		}

		return rodzaj;
	}

	// wykorzystanie setu zamiast listy, poniewaz ui:repeat dubluje wartosci
	// przy wykorzystaniu listy
	public List listFromSet(Set set) {
		if (set != null) {

			return new ArrayList<>(set);
		}
		return null;
	}

	public void addNewSymbolOpisu() {
		ZptNabSymb symbol = new ZptNabSymb();
		nabor.addZptNabSymb(symbol);
		// user.info("Dodano symbol opisu stanowiska");
		this.nabor.setNabLiczbaWakatow(0L+nabor.getZptNabSymbs().size());
	}

	public void usunSymbolOpisu(ZptNabSymb symbol) {
		if (nabor.getZptNabSymbs() != null && nabor.getZptNabSymbs().contains(symbol)) {
			//nabor.getZptNabSymbs().remove(symbol);
			nabor.removeZptNabSymb(symbol);
			this.nabor.setNabLiczbaWakatow(0L+nabor.getZptNabSymbs().size());
		}
	}

	// przejście do odpowiedniego wniosku w zależności od wybranego rodzaju
	// rekrutacji - wniosek wybierany na podstawie aliasu powiązanego ze
	// słownikiem PP_REK_RODZAJE
	public void przejdzDoWniosku() {
		try {

			String wslWartosc = "";

			SqlDataSelectionsHandler p = this.getSql("PP_REK_RODZAJE");
			if (p != null) {
				wslWartosc = (String) p.getCurrentRow().get("WSL_WARTOSC");
				nabor.setNabRodzajNaboru(wslWartosc);
				sessionBean.setRodzajNaboru(wslWartosc);
			}

			String xhtmlNr = mapujWslWartoscNaRekrutacjaNrXhtml(wslWartosc);

			if (xhtmlNr != null)
				FacesContext.getCurrentInstance().getExternalContext().redirect(xhtmlNr);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String mapujWslWartoscNaRekrutacjaNrXhtml(String wslWartosc) {
		String ret = null;

		switch (wslWartosc) {
		case "WEWN":		//Rekrutacja wewnętrzna w Ministerstwie
		case "JED_PODL":	//Rekrutacja przeprowadzana w jednostkach podległych i nadzorowanych
			ret = "RekrutacjaWniosek1"; 
			break;
		case "KSC":			//Nabór do korpusu służby cywilnej
			ret = "RekrutacjaWniosek2"; 
			break;
		case "WEW_ST_POM":	//Rekrutacja wewnętrzna na stanowisko pomocnicze
		case "ST_POM": 		//Nabór na stanowiska pomocnicze
			ret = "RekrutacjaWniosek3"; 
			break;
		case "KSAP":		//Nabór do korpusu służby cywilnej
			ret = "RekrutacjaWniosek4";
			break;
		case "WYZ_ST_SC":	//Nabór na wyższe stanowisko w służbie cywilnej
			ret = "RekrutacjaWniosek5"; 
			break;
		}
		return ret;
	}

	public static String mapujWslWartoscNaNazweRaportu(String wslWartosc) {
		String ret = null;

		switch (wslWartosc) {
		case "WEWN":
		case "JED_PODL":
		case "KSC":
		case "WYZ_ST_SC":
			ret = "Wniosek_o_rozpoczecie_rekrutacji_albo_naboru_-_KSC";
			break;
		case "KSAP":
			ret = "Wniosek_o_rozpoczecie_naboru_absolwentow_KSAP_do_KSC";
			break;
		case "WEW_ST_POM":
		case "ST_POM":
			ret = "Wniosek_o_rozpoczecie_rekrutacji_albo_naboru_-_pomocnicze";
			break;
		// case "KSAP":
		// ret = "4"; TODO - jaki ma byc raport????
		// break;
		}
		return ret;
	}

	public static String mapujWslWartoscNaNazweRaportuOgloszenie(String wslWartosc) {
		String ret = null;

		switch (wslWartosc) {
		case "WEWN":
			ret = "Ogloszenie_rekrutacja_WEWN";
			break;
		case "JED_PODL":
			ret = "Ogloszenie_rekrutacja_JED_PODL";
			break;
		case "KSAP":
			ret = "Ogloszenie_rekrutacja_KSAP";
			break;
		case "KSC":
			ret = "Ogloszenie_rekrutacja_KSC";
			break;
		case "WYZ_ST_SC":
			ret = "Ogloszenie_rekrutacja_WYZ_ST_SC";
			break;
		case "WEW_ST_POM":
			ret = "Ogloszenie_rekrutacja_WEW_ST_POM";
			break;
		case "ST_POM":
			ret = "Ogloszenie_rekrutacja_ST_POM";
			break;
		default:
			ret = "Ogloszenie_rekrutacja";
		}
		return ret;
	}

	// przypisanie danych ogloszenia na podstawie statycznych pol - w przypadku
	// bezposredniego odwolywania i zmiany wartosci niemozliwe cofniecie zmian
	// przy zamknieciu dialogu
	public void wypelnijOgloszenie() {
		zakresObow = nabor.getNabOgZakresObow();
		wymagNiez = nabor.getNabOgWymaganiaNiezb();
		wymagDodat = nabor.getNabOgWymaganiaDodat();
		wymaganeWykszt = nabor.getNabOgWymaganeWyksz();
		terminZglMail = nabor.getNabOgTerminZglEmail();
		wymDok = nabor.getNabOgWymDok();
		terminRozm = nabor.getNabOgTerminRozm();
		kontakt = nabor.getNabOgKontakt();
	}

	// walidacja ogloszenia
	public boolean walidujOgloszenie() {

		ArrayList<String> inval = new ArrayList<String>();
		Calendar cal = Calendar.getInstance();
		if (nabor.getNabOgTerminRozm() != null) {
			if (nabor.getNabAudytDt() != null) {

				if (nabor.getNabOgTerminRozm().before(nabor.getNabAudytDt())) {
					inval.add("Termin rozmowy nie może być wcześniejszy od daty dodania ogłoszenia!");
				}
			} else if (nabor.getNabOgTerminRozm().before(cal.getTime())) {
				inval.add("Termin rozmowy nie może być wcześniejszy od daty dodania ogłoszenia!");
			}
		}

		if (nabor.getNabOgTerminZglEmail() != null) {
			if (nabor.getNabAudytDt() != null) {

				if (nabor.getNabOgTerminZglEmail().before(nabor.getNabAudytDt())) {
					inval.add("Termin zgłoszeń email nie może być wcześniejszy od daty dodania ogłoszenia!");
				}
			} else if (nabor.getNabOgTerminZglEmail().before(cal.getTime())) {
				inval.add("Termin zgłoszeń email nie może być wcześniejszy od daty dodania ogłoszenia!");
			}
		}

		for (String msg : inval) {
			User.alert(msg);
		}

		return (inval.isEmpty());
	}

	// zaznaczenie komisji - dodanie do listy danych z sql-a
	public void zaznaczKomisje() {
		// Synchronizacja selekcji w SqlSelectionsDataHandler:
		SqlDataSelectionsHandler sel = getSql("PPL_REK_PRACOWNICY_DEP");
		sel.clearSelection();
		for (ZptNaborKomisja k : this.nabor.getZptNaborKomisjas()) {
			if (k.getKomTyp().equals("Z")) {
				sel.setRowSelection(true, sel.get(k.getKomPrcId()));
			}
		}
	}

	public void zaznaczPrzewodniczacego() {

		// Synchronizacja selekcji w SqlSelectionsDataHandler:
		SqlDataSelectionsHandler sel = getSql("PP_PRC");
		sel.clearSelection();
		for (ZptNaborKomisja k : this.nabor.getZptNaborKomisjas()) {
			if (k.getKomTyp().equals("P")) {
				sel.setRowSelection(true, sel.get(k.getKomPrcId()));
			}
		}
	}

	public void zaznaczKomisjeBDG() {
		// Synchronizacja selekcji w SqlSelectionsDataHandler:
		SqlDataSelectionsHandler sel = getLW("PPL_REK_PRACOWNICY_DBG", 100893, 100893);
		sel.clearSelection();
		for (ZptNaborKomisja k : this.nabor.getZptNaborKomisjas()) {
			if (k.getKomTyp().equals("B")) {
				sel.setRowSelection(true, sel.get(k.getKomPrcId()));
			}
		}
	}

	// zapis wybranego skladu komisji i powiazanie z modelem
	public void zapiszKomisje() {
		List<ZptNaborKomisja> toRemove = new ArrayList<>();
		for (ZptNaborKomisja znk : this.nabor.getZptNaborKomisjas()) {
			if (znk.getKomTyp().equals("Z")) {
				toRemove.add(znk);
			}
		}
		nabor.getZptNaborKomisjas().removeAll(toRemove);
		SqlDataSelectionsHandler ssh = getSql("PPL_REK_PRACOWNICY_DEP");
		if (ssh != null) {
			List<DataRow> naborKomisjaKeySet = ssh.getSelectedRows();

			if (this.nabor.getZptNaborKomisjas().isEmpty() || this.nabor.getZptNaborKomisjas().stream()
					.filter(p -> "P".equals(p.getKomTyp())).collect(Collectors.toList()).isEmpty()) {
				User.alert("Należy wybrać przewodniczącego komisji rekrutacyjnej.");
				return;
			}

			if (naborKomisjaKeySet.size() < 1) {
				User.alert("W komisji musi zasiadać co najmniej 2 członków.");
				return;
			}

			for (DataRow dr : naborKomisjaKeySet) {
				ZptNaborKomisja znk = new ZptNaborKomisja();
				znk.setKomPrcId(((BigDecimal) dr.get("pp_prc_id")).longValue());
				znk.setKomTyp("Z");
				this.nabor.addZptNaborKomisja(znk);
			}
		}
		try {
			nabor = (ZptNabory) HibernateContext.save(nabor);
			this.nabor.setSqlBean(this);
		} catch (Exception e) {
			User.alert("Przed dodaniem komisji zapisz wniosek.");
			e.printStackTrace();
		}
	}

	public void zapiszKomisjeBDG() {
		List<ZptNaborKomisja> toRemove = new ArrayList<>();
		for (ZptNaborKomisja znk : this.nabor.getZptNaborKomisjas()) {
			if (znk.getKomTyp().equals("B")) {
				toRemove.add(znk);
			}
		}
		nabor.getZptNaborKomisjas().removeAll(toRemove);
		SqlDataSelectionsHandler ssh = getLW("PPL_REK_PRACOWNICY_DBG", 100893, 100893);
		if (ssh != null) {
			List<DataRow> naborKomisjaBDGKeySet = ssh.getSelectedRows();
			for (DataRow dr : naborKomisjaBDGKeySet) {
				ZptNaborKomisja znk = new ZptNaborKomisja();
				znk.setKomPrcId(((BigDecimal) dr.get("pp_prc_id")).longValue());
				znk.setKomTyp("B");
				this.nabor.addZptNaborKomisja(znk);
			}
		}
		try {
			nabor = (ZptNabory) HibernateContext.save(nabor);
			this.nabor.setSqlBean(this);
		} catch (Exception e) {
			User.alert("Przed dodaniem komisji zapisz wniosek.");
			e.printStackTrace();
		}
		return;
	}

	public void zapiszPrzewodniczacego() {
		List<ZptNaborKomisja> toRemove = new ArrayList<>();
		for (ZptNaborKomisja znk : this.nabor.getZptNaborKomisjas()) {
			if (znk.getKomTyp().equals("P")) {
				toRemove.add(znk);
			}
		}
		nabor.getZptNaborKomisjas().removeAll(toRemove);
		SqlDataSelectionsHandler ssh = getSql("PP_PRC");
		if (ssh != null) {
			List<DataRow> naborPrzwodniczacyKeySet = ssh.getSelectedRows();

			if (naborPrzwodniczacyKeySet.size() != 1) {
				User.alert("Należy wybrać przewodniczącego komisji rekrutacyjnej.");
				return;
			}

			for (DataRow dr : naborPrzwodniczacyKeySet) {
				ZptNaborKomisja znk = new ZptNaborKomisja();
				znk.setKomPrcId(((BigDecimal) dr.get("prc_id")).longValue());
				znk.setKomTyp("P");
				this.nabor.addZptNaborKomisja(znk);
			}
		}
		try {
			nabor = (ZptNabory) HibernateContext.save(nabor);
			this.nabor.setSqlBean(this);
		} catch (Exception e) {
			User.alert("Przed dodaniem komisji zapisz wniosek.");
			e.printStackTrace();
		}
	}

	// pobranie id czlonkow komisji - id pracownika
	private HashSet<Long> getNaborKomisjaKeySet() {
		HashSet<Long> zptNrKeySet = new HashSet<Long>();
		if (nabor != null && nabor.getZptNaborKomisjas() != null)
			for (ZptNaborKomisja k : nabor.getZptNaborKomisjas()) {
				zptNrKeySet.add(k.getKomPrcId());
			}
		return zptNrKeySet;
	}

	public void zapisz() {

		// Usuwanie pustych symboli
		List<ZptNabSymb> toRemove = new ArrayList<>();
		for (ZptNabSymb symb : nabor.getZptNabSymbs()) {
			if (symb.getNsmStoId() == null) {
				toRemove.add(symb);
			}
		}
		nabor.getZptNabSymbs().removeAll(toRemove);

		// Dzieki zsynchronizowaniu lokalnej listy z lista z bazy po zapisie,
		// wiemy tutaj że nowe pole ma id = null
		// A takie które w bazie już są i my je tylko edytujemy, podmieniamy
		// tylko pole opisujące daną osobę
		for (ZptNaborKomisja znk : czlonkowieKomSpozaMF) {
			if (("" + znk.getKomTyp()).equals("S")) {
				if (znk.getKomId() == null) {
					nabor.addZptNaborKomisja(znk);
					continue;
				}
				for (ZptNaborKomisja znkModel : nabor.getZptNaborKomisjas()) {
					if (znkModel.getKomId().equals(znk.getKomId()))
						znkModel.setKomSpozaMf(znk.getKomSpozaMf());
				}
			}
		}

		if (!validateBeforeSave())
			return;
 
//		if (wynagrodzenieMiesieczne != null) {
			//for (ZptNaborSklad zns : wynagrodzenieMiesieczne) {
//			for (ZptNaborSklad zns : nabor.getZptNaborSklads()) {
//
//				if (zns.getNskRodzaj().equals("INNE")) {
//					if (!"ST_POM".equals( nabor.getNabRodzajNaboru() ) && !"WEW_ST_POM".equals(nabor.getNabRodzajNaboru())) {
//						continue;
//					}
//				}
//
//				if (zns.getNskProcent() != null && Double.valueOf(zns.getNskProcent()) != null) {
//
//					if (czyNowy) {
//						nabor.addZptNaborSklad(zns);
//					}
//					
//				} else {
//					if (czyNowy) {
//						zns.setNskProcent(0d);
//						zns.setNskWartosc(0d);
//						nabor.addZptNaborSklad(zns);
//					}
//				}
//			}
//		}

		try {
			nabor = (ZptNabory) HibernateContext.save(nabor);// save + refresh
			this.nabor.setSqlBean(this);

			User.info("Udany zapis wniosku o rekrutację [id: %1$s].", this.nabor.getNabId());

		} catch (Exception e) {
			User.alert(e);//User.alert("Błąd w zapisie wniosku o rekrutację!", e);
			e.printStackTrace();
			return;
		}

		czlonkowieKomSpozaMF.clear();
		if (nabor.getZptNaborKomisjas() != null) {
			for (ZptNaborKomisja znk : nabor.getZptNaborKomisjas()) {
				if (("" + znk.getKomTyp()).equals("S") && znk.getKomPrcId() == null) {
					czlonkowieKomSpozaMF.add(znk);
				}
			}
		}
		czyNowy = false;
		obieg.setIdEncji(this.nabor.getNabId());
		zalaczniki.setZalacznikFkId(this.nabor.getNabId());
//		checkString = getCheckString();

	}

	public void ogloszenieRaport() {
		try {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("nab_id", nabor.getNabId());
			String wydruk = mapujWslWartoscNaNazweRaportuOgloszenie(nabor.getNabRodzajNaboru());
			ReportGenerator.displayReportAsRTF(params, wydruk);
		} catch (JRException | SQLException | IOException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}

	}

	public void raport() throws SQLException, JRException, IOException {
		HashMap<String, Object> params = new HashMap<String, Object>();
		long id = nabor.getNabId();
		params.put("nab_id", id);

		String wydruk = mapujWslWartoscNaNazweRaportu(nabor.getNabRodzajNaboru());

		if (wydruk != null)
			ReportGenerator.displayReportAsRTF(params, wydruk);
		else
			User.warn("Dla podanego rodzaju wniosku (%1$s) rekrutacji nie zdefiniowano raportu.", nabor.getNabRodzajNaboru());

	}

	public void raportOgl() {
		if (nabor != null) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("id_naboru", nabor.getNabId());

			genrap.uruchomRaportOcean("Ogloszenie o rekrutacji", params);
		}
	}

	public void raportOswiadczenieCzlonkaKomisji() {

		if (nabor != null && nabor.getNabId() != null && nabor.getNabId() > 0) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("NAB_ID", nabor.getNabId());
			try {
				ReportGenerator.displayReportAsDOCX(params, "oswiadczenie_czlonka_komisji");
			} catch (JRException | SQLException | IOException e) {
				e.printStackTrace();
				User.alert(e.getMessage());
			}
		}
	}

	public void usun() throws IOException {

		try (Connection conn = JdbcUtils.getConnection(false)) {
			obieg.usunDaneObiegu(); // reloadPage
		} catch (SQLException e) {
			log.error(e.getMessage());
			User.alert("Nieudane usuniecie danych obiegu: " + e.getMessage());
			return;
		}

		try {
			ZalacznikiBean.deleteAll("ZAL_NAB_ID", nabor.getNabId());
		} catch (Exception e) {
			log.error(e.getMessage());
			User.alert("Błąd przy próbie usunięcia załączników: " + e.getMessage());
			return;
		}

		for (ZptKandydaci k : nabor.getZptKandydacis()) {
			try {
				ZalacznikiBean.deleteAll("ZAL_KA_ID", k.getKaId());
			} catch (Exception e) {
				log.error(e.getMessage());
				User.alert("Bład przy próbie usunięcia załączników kandydatów: " + e.getMessage());
			}
		}

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			nabor = (ZptNabory) s.merge(nabor);
			this.nabor.setSqlBean(this);
			s.delete(nabor);
			s.beginTransaction().commit();
			User.info("Usunięto wniosek o rekrutację");
		} catch (Exception e) {
			User.alert("Błąd w usuwaniu wniosku!");
			log.error(e.getMessage());
			return;
		}

		FacesContext.getCurrentInstance().getExternalContext().redirect("ListaWnioskowRekrutacje");
	}

	private boolean validateBeforeSave() {

		ArrayList<String> inval = new ArrayList<String>();
		Calendar cal = Calendar.getInstance();
//		String currentCheckString = this.getCheckString();

//		if (this.checkString.equals(currentCheckString))
//			inval.add("Brak zmian do zapisania.");

		//if (nabor.getNabStnId() == null || nabor.getNabStnId() == 0L) {
		if (nz(nabor.getNabStjoId()) == 0L) { 
			inval.add("Stanowisko jest polem wymaganym.");
		}

		if (nz( nabor.getNabObId()) == 0L) {
			inval.add("Jednostka organizacyjna (wnioskująca) jest polem wymaganym.");
		}

		if (nz(nabor.getNabWydzialId()) == 0L) {
			inval.add("Wydział jest polem wymaganym.");
		}

		if (nabor.getNabRodzajNaboru() == null) {
			if (nabor.getNabRodzajNaboru().trim().equals("")) {
				inval.add("Rodzaj naboru jest polem wymaganym.");
			}

		} else {
			if (nabor.getNabLiczbaWakatow() != null) {
				if ((nabor.getNabRodzajNaboru().equals("KSC") || nabor.getNabRodzajNaboru().equals("WYZ_ST_SC"))
						&& (nabor.getNabLiczbaWakatow().compareTo(5L) > 0)) {
					inval.add(
							"Nabór do służby cywilnej może być przeprowadzony jednocześnie na maksymalnie 5 wakatów na to samo stanowisko pracy.");
				}
			}
		}

		if (nabor.getNabWymiarEtatu() == null || nabor.getNabWymiarEtatu().trim().equals("")) {
			inval.add("Wymiar etatu jest polem wymaganym.");
		}

		if (nz(nabor.getNabMnoznikMax()) < nz(nabor.getNabMnoznikMin())) {
			inval.add("Mnożnik maksymalny proponowany przez komórkę wnioskującą nie może być mniejszy niż mnożnik minimalny.");
		}

		if (nz(nabor.getNabMnoznikMaxAkc()) < nz(nabor.getNabMnoznikMinAkc())) {
			inval.add("Mnożnik maksymalny zakaceptowany przez Dyrektora Generalnego nie może być mniejszy niż mnożnik minimalny.");
		}

		if (nabor.getNabUzasadnienie() == null || nabor.getNabUzasadnienie().trim().equals("")) {
			inval.add("Uzasadnienie jest polem wymaganym.");
		}

		if (nabor.getSumaProcentowaProjektow() + nabor.getSumaProcentowaRezerw() + nz(nabor.getNabEtatZFundWynagr()) > 100.00)
			inval.add("Suma zródeł finasowania przekracza 100%");

		if (!this.nabor.getNabRodzajNaboru().equals("WYZ_ST_SC") && !this.nabor.getNabRodzajNaboru().equals("KSC")) {

			if (nabor.getNabOgTerminRozm() != null) {
				if (nabor.getNabAudytDt() != null) {
					if (nabor.getNabOgTerminRozm().before(nabor.getNabAudytDt())) {
						inval.add("Termin rozmowy nie może być wcześniejszy od daty dodania ogłoszenia!");
					}
				} else if (nabor.getNabOgTerminRozm().before(cal.getTime())) {
					inval.add("Termin rozmowy nie może być wcześniejszy od daty dodania ogłoszenia!");
				}
			}

			if (nabor.getNabOgTerminZglEmail() != null) {
				if (nabor.getNabAudytDt() != null) {

					if (nabor.getNabOgTerminZglEmail().before(nabor.getNabAudytDt())) {
						inval.add("Termin zgłoszeń email nie może być wcześniejszy od daty dodania ogłoszenia!");
					}
				} else if (nabor.getNabOgTerminZglEmail().before(cal.getTime())) {
					inval.add("Termin zgłoszeń email nie może być wcześniejszy od daty dodania ogłoszenia!");
				}
			}

		}

		for (String msg : inval) {
			User.alert(msg);
		}

		return inval.isEmpty();
	}

//	public String getCheckString() {
//		String ret = "";
//
//		String projekty = "";
//		if (nabor.getPptRekProjekties().size() > 0) {
//			for (PptRekProjekty proj : nabor.getPptRekProjekties()) {
//				projekty += AppBean.concatValues(proj.getNapEtatProjekt(), proj.getNapProjObId());
//			}
//		}
//
//		String rezerwy = "";
//		if (nabor.getPptRekRezerwyCelowe().size() > 0) {
//			for (PptRekRezerwyCelowe rezerw : nabor.getPptRekRezerwyCelowe()) {
//				rezerwy += AppBean.concatValues(rezerw.getNarcRezerwaEtat(), rezerw.getNarcRezerwaNazwa());
//			}
//		}
//
//		if (this.nabor.getNabRodzajNaboru().equals("JED_PODL") || this.nabor.getNabRodzajNaboru().equals("WEWN")) {
//			// rekrutacja 1
//			ret = AppBean.concatValues(nabor.getNabRodzajNaboru(), nabor.getNabObId(), nabor.getNabWydzialId(), nabor.getNabStnId(),
//					nabor.getNabStjoId(), nabor.getNabMnoznikMin(), nabor.getNabMnoznikMax(), nabor.getNabMnoznikMinAkc(),
//					nabor.getNabMnoznikMaxAkc(), nabor.getNabWymiarEtatu(), nabor.getNabEtatZwyk(), nabor.getNabEtatZastepstwo(),
//					nabor.getNabEtatProjekt(), nabor.getNabEtatRezerwa(), nabor.getNabEtatZFundWynagr(), nabor.getnabProjObId(),
//					nabor.getNabEtatRezerwaTyp(), nabor.getNabRodzajPropUmowy(), nabor.getNabUzasadnienie(), nabor.getNabNrOglBip(),
//					nabor.getNabOgTerminRozm(), nabor.getNabOgKontakt(), nabor.getNabOgTerminZglEmail(), nabor.getNabOgWymaganeWyksz(),
//					nabor.getNabOgWymaganiaDodat(), nabor.getNabOgWymaganiaNiezb(), nabor.getNabOgWymDok(), nabor.getNabOgZakresObow(),
//					nabor.getNabUwagi(), projekty, rezerwy);
//			for (ZptNaborKomisja k : nabor.getZptNaborKomisjas()) {
//				ret += k.getKomPrcId() + "-" + k.getKomTyp() + "-" + k.getKomSpozaMf() + "; ";
//			}
//
//			if (nabor.getZptNabSymbs() != null)
//				for (ZptNabSymb s : nabor.getZptNabSymbs()) {
//					ret += s.getNsmStoId() + "; ";
//					if (s.getNsmNabDataWolneOd() != null)
//						ret += s.getNsmNabDataWolneOd().getTime() + "; ";
//				}
//		}
//
//		if (this.nabor.getNabRodzajNaboru().equals("KSC")) {
//			// rekrutacja 2
//			ret = AppBean.concatValues(nabor.getNabRodzajNaboru(), nabor.getNabObId(), nabor.getNabWydzialId(), nabor.getNabStnId(),
//					nabor.getNabStjoId(), nabor.getNabMnoznikMin(), nabor.getNabMnoznikMax(), nabor.getNabMnoznikMinAkc(),
//					nabor.getNabMnoznikMaxAkc(), nabor.getNabWymiarEtatu(), nabor.getNabEtatZwyk(), nabor.getNabEtatZastepstwo(),
//					nabor.getNabEtatProjekt(), nabor.getNabEtatRezerwa(), nabor.getNabEtatZFundWynagr(), nabor.getnabProjObId(),
//					nabor.getNabEtatRezerwaTyp(), nabor.getNabRodzajPropUmowy(), nabor.getNabUzasadnienie(), nabor.getNabUwagi(), projekty,
//					rezerwy);
//			for (ZptNaborKomisja k : nabor.getZptNaborKomisjas()) {
//				ret += k.getKomPrcId() + "-" + k.getKomTyp() + "-" + k.getKomSpozaMf() + "; ";
//			}
//			if (nabor.getZptNabSymbs() != null)
//				for (ZptNabSymb s : nabor.getZptNabSymbs()) {
//					ret += s.getNsmStoId() + "; ";
//					if (s.getNsmNabDataWolneOd() != null)
//						ret += s.getNsmNabDataWolneOd().getTime() + "; ";
//				}
//
//		}
//
//		if (this.nabor.getNabRodzajNaboru().equals("ST_POM") || this.nabor.getNabRodzajNaboru().equals("WEW_ST_POM")) {
//			// rekrutacja 3
//			ret = AppBean.concatValues(nabor.getNabRodzajNaboru(), nabor.getNabObId(), nabor.getNabWydzialId(), nabor.getNabStnId(),
//					nabor.getNabWymiarEtatu(), nabor.getNabEtatZwyk(), nabor.getNabEtatZastepstwo(), nabor.getNabRodzajPropUmowy(),
//					nabor.getNabEtatZFundWynagr(), nabor.getnabProjObId(), nabor.getNabEtatRezerwaTyp(), nabor.getNabUzasadnienie(),
//					nabor.getNabNrOglBip(), nabor.getNabOgTerminRozm(), nabor.getNabOgKontakt(), nabor.getNabOgTerminZglEmail(),
//					nabor.getNabOgWymaganeWyksz(), nabor.getNabOgWymaganiaDodat(), nabor.getNabOgWymaganiaNiezb(), nabor.getNabOgWymDok(),
//					nabor.getNabOgZakresObow(), nabor.getNabUwagi(), projekty, rezerwy);
//
//			for (ZptNaborSklad s : getWynagrodzenieMiesieczne()) {
//				ret += s.getNskProcent() + "-" + s.getNskWartosc() + "; ";
//			}
//
//			for (ZptNaborKomisja k : nabor.getZptNaborKomisjas()) {
//				ret += k.getKomPrcId() + "-" + k.getKomTyp() + "-" + k.getKomSpozaMf() + "; ";
//			}
//			for (ZptNaborSklad k : nabor.getZptNaborSklads()) {
//				ret += k.getNskRodzaj() + "-" + k.getNskProcent() + "-" + k.getNskWartosc() + "; ";
//			}
//
//		}
//		if (this.nabor.getNabRodzajNaboru().equals("KSAP")) {
//			// rekrutacja 4
//			ret = AppBean.concatValues(nabor.getNabRodzajNaboru(), nabor.getNabObId(), nabor.getNabWydzialId(), nabor.getNabStnId(),
//					nabor.getNabStjoId(), nabor.getNabMnoznikMin(), nabor.getNabMnoznikMax(), nabor.getNabMnoznikMinAkc(),
//					nabor.getNabMnoznikMaxAkc(), nabor.getNabWymiarEtatu(), nabor.getNabEtatZwyk(), nabor.getNabEtatZastepstwo(),
//					nabor.getNabEtatProjekt(), nabor.getNabEtatRezerwa(), nabor.getNabEtatZFundWynagr(), nabor.getnabProjObId(),
//					nabor.getNabEtatRezerwaTyp(), nabor.getNabRodzajPropUmowy(), nabor.getNabUzasadnienie(), nabor.getNabNrOglBip(),
//					nabor.getNabOgTerminRozm(), nabor.getNabOgKontakt(), nabor.getNabOgTerminZglEmail(), nabor.getNabOgWymaganeWyksz(),
//					nabor.getNabOgWymaganiaDodat(), nabor.getNabOgWymaganiaNiezb(), nabor.getNabOgWymDok(), nabor.getNabOgZakresObow(),
//					nabor.getNabUwagi(), projekty, rezerwy);
//			for (ZptNaborKomisja k : nabor.getZptNaborKomisjas()) {
//				ret += k.getKomPrcId() + "-" + k.getKomTyp() + "-" + k.getKomSpozaMf() + "; ";
//			}
//
//			if (nabor.getZptNabSymbs() != null)
//				for (ZptNabSymb s : nabor.getZptNabSymbs()) {
//					ret += s.getNsmStoId() + "; ";
//					if (s.getNsmNabDataWolneOd() != null)
//						ret += s.getNsmNabDataWolneOd().getTime() + "; ";
//				}
//		}
//		// rekrutacja 5
//		if (this.nabor.getNabRodzajNaboru().equals("WYZ_ST_SC")) {
//			// rekrutacja 5
//			ret = AppBean.concatValues(nabor.getNabRodzajNaboru(), nabor.getNabObId(), nabor.getNabWydzialId(), nabor.getNabStnId(),
//					nabor.getNabStjoId(), nabor.getNabWymiarEtatu(), nabor.getNabEtatZwyk(), nabor.getNabEtatZastepstwo(),
//					nabor.getNabEtatProjekt(), nabor.getNabEtatRezerwa(), nabor.getNabEtatZFundWynagr(), nabor.getnabProjObId(),
//					nabor.getNabEtatRezerwaTyp(), nabor.getNabRodzajPropUmowy(), nabor.getNabUzasadnienie(), nabor.getNabUwagi(), projekty,
//					rezerwy);
//			for (ZptNaborKomisja k : nabor.getZptNaborKomisjas()) {
//				ret += k.getKomPrcId() + "-" + k.getKomTyp() + "-" + k.getKomSpozaMf() + "; ";
//			}
//			if (nabor.getZptNabSymbs() != null)
//				for (ZptNabSymb s : nabor.getZptNabSymbs()) {
//					ret += s.getNsmStoId() + "; ";
//					if (s.getNsmNabDataWolneOd() != null)
//						ret += s.getNsmNabDataWolneOd().getTime() + "; ";
//				}
//		}
//
//		return ret;
//	}

//	public void setIdWydzialuIStanowiska() {
//		DataRow row = getLW("PP_STAN_W_DEPART").getCurrentRow();
//		BigDecimal wydzialId = (BigDecimal) row.get("pp_ob_id");
//		BigDecimal stnId = (BigDecimal) row.get("pp_stn_id");
//
//		this.nabor.setNabWydzialId(wydzialId.longValue());
//		this.nabor.setNabStnId(stnId.longValue());
//	}

//	public void setIdStanowiska(ValueChangeEvent event) {
//		Long stjoId = (Long) event.getNewValue();
//		DataRow row = getLW("pp_stan_w_jedn").find(stjoId);
//		BigDecimal stnId = (BigDecimal) row.get("pp_stn_id");
//
//		if (stnId != null) {
//			this.nabor.setNabStnId(stnId.longValue());
//		}
//
//	}

	public HashMap<String, Long> getJednostkiOrg() {
		return jednostkiOrg;
	}

	public void setJednostkiOrg(HashMap<String, Long> jednostkiOrg) {
		this.jednostkiOrg = jednostkiOrg;
	}

	public DataRow getWniosekRekrutacja() {
		return wniosekRekrutacja;
	}

	public void setWniosekRekrutacja(DataRow wniosekRekrutacja) {
		this.wniosekRekrutacja = wniosekRekrutacja;
	}

	public ZptNabory getNabor() {
		return nabor;
	}

	public void setNabor(ZptNabory nabor) {
		this.nabor = nabor;
	}

	public boolean isCzyWyswietlac() {
		return czyWyswietlac;
	}

	public String setCzyWyswietlac(boolean czyWyswietlac) {
		this.czyWyswietlac = czyWyswietlac;
		return "";
	}

	public int getTest() {
		return test++;
	}

	public void setTest(int test) {
		this.test = test;
	}

	private int test = 1;

	public List<DataRow> getMyRows() {
		List<DataRow> ret = this.getSql("PPL_REK_PRACOWNICY_DEP").getSelectedRows();
		return ret;
	}

	public List<ZptNaborSklad> getZptNaborSkladSorted() {
		
		List<ZptNaborSklad> ret = this.nabor.getZptNaborSklads().stream()
				.sorted( (x,y) -> ZptNabory.fRodzajOrder(x.getNskRodzaj()).compareTo( ZptNabory.fRodzajOrder(y.getNskRodzaj())) ) //TODO funkcja do komparowania po rodzajach
				.collect(Collectors.toList());		
	 
	 	return ret;
	}
	
	
/*	public static Integer fRodzajOrder(String rodzaj){
		if (rodzaj==null)
			return 999;
		return "WYNAGR PREMIA_GW PREMIA_UZ DOD_STAZ DOD_SPEC INNE".indexOf(rodzaj);
	}*/
	
	
//	public List<ZptNaborSklad> getWynagrodzenieMiesieczne() {
//		// Sortowanie
//		Collections.sort(wynagrodzenieMiesieczne, new Comparator<ZptNaborSklad>() {
//			@Override
//			public int compare(ZptNaborSklad wynagrodzenieMiesieczne1, ZptNaborSklad wynagrodzenieMiesieczne2) {
//				return wynagrodzenieMiesieczne2.getNskRodzaj().compareTo(wynagrodzenieMiesieczne1.getNskRodzaj());
//			}
//		});
//		return wynagrodzenieMiesieczne;
//
//	}
//
//	public void setWynagrodzenieMiesieczne(List<ZptNaborSklad> wynagrodzenieMiesieczne) {
//		this.wynagrodzenieMiesieczne = wynagrodzenieMiesieczne;
//	}
	
	
	public ZptNaborSklad getWynagrodzenieZasadnicze(){
		ZptNaborSklad wynagrodzenieZasadnicze = nabor.getZptNaborSklads().stream().filter(sk->"WYNAGR".equals( sk.getNskRodzaj() ) ).findFirst().orElse(null);
		return wynagrodzenieZasadnicze;
	}
	
	
	public Double getWynagrodzenieZasadniczeWartosc(){
		ZptNaborSklad wz = this.getWynagrodzenieZasadnicze();
		if (wz==null)
			return 0.0;
		else
			return  nz(wz.getNskWartosc());
	}

	
	public Double calcRazem(ZptNaborSklad s){
		if (s==null)
			return 0.0;
		
		
		Double ret = round( 
						("WYNAGR".equals(s.getNskRodzaj()) ? 0.0 : this.getWynagrodzenieZasadniczeWartosc())*nz(s.getNskProcent())/100
						+ nz(s.getNskWartosc())
				      , 2);
		
		return ret;
	}
	
	
	public Double calcRazem(){
		return this.nabor.getZptNaborSklads().stream().mapToDouble(s->this.calcRazem(s)).sum();
	}

	
	public boolean isCzyNowy() {
		return czyNowy;
	}

	public void setCzyNowy(boolean czyNowy) {
		this.czyNowy = czyNowy;
	}

	public List<String> getWybraniCzlonkowieKomisji() {
		return wybraniCzlonkowieKomisji;
	}

	public void setWybraniCzlonkowieKomisji(List<String> wybraniCzlonkowieKomisji) {
		this.wybraniCzlonkowieKomisji = wybraniCzlonkowieKomisji;
	}

	public String getObIdString() {
		return obIdString;
	}

	public void setObIdString(String obIdString) {
		this.obIdString = obIdString;
	}

	public List<ZptNaborKomisja> getCzlonkowieKomSpozaMF() {
		return czlonkowieKomSpozaMF;
	}

	public void setCzlonkowieKomSpozaMF(List<ZptNaborKomisja> czlonkowieKomSpozaMF) {
		this.czlonkowieKomSpozaMF = czlonkowieKomSpozaMF;
	}

	public void dodajCzlonkaKomSpozaMF() {
		ZptNaborKomisja znk = new ZptNaborKomisja();
		znk.setKomTyp("S");
		czlonkowieKomSpozaMF.add(znk);
	}

	public void usunCzlonkaKomSpozaMF(ZptNaborKomisja znk) {
		nabor.getZptNaborKomisjas().remove(znk);
		czlonkowieKomSpozaMF.remove(znk);

	}
}
