package com.comarch.egeria.pp.Rekrutacja.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.comarch.egeria.pp.data.model.CkkAdresy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;


/**
 * The persistent class for the PPT_REK_KANDYDACI database table.
 * 
 */
@Entity
@Table(name="PPT_REK_KANDYDACI", schema="PPADM")
@NamedQuery(name="ZptKandydaci.findAll", query="SELECT z FROM ZptKandydaci z")
public class ZptKandydaci implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_REK_KANDYDACI",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_REK_KANDYDACI", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_REK_KANDYDACI")
	@Column(name="KA_ID")
	private Long kaId;

	@Column(name="KA_ABSOLWENT")
	private String kaAbsolwent;

	@Column(name="KA_ADRES")
	private Long kaAdres;

	@Column(name="KA_AUDYT_KM")
	private String kaAudytKm;

	@Column(name="KA_AUDYT_KT")
	private String kaAudytKt;

	@Column(name="KA_AUDYT_LM")
	private BigDecimal kaAudytLm;

	@Temporal(TemporalType.DATE)
	@Column(name="KA_DATA_ANON")
	private Date kaDataAnon;
	
	@Temporal(TemporalType.DATE)
	@Column(name="KA_DATA_DO_NIEPEL")
	private Date kaDataDoNiepel;

	@Temporal(TemporalType.DATE)
	@Column(name="KA_DATA_DO_NIEZDOL")
	private Date kaDataDoNiezdol;

	@Temporal(TemporalType.DATE)
	@Column(name="KA_DATA_OD_NIEPEL")
	private Date kaDataOdNiepel;

	@Temporal(TemporalType.DATE)
	@Column(name="KA_DATA_OD_NIEZDOL")
	private Date kaDataOdNiezdol;

	@Temporal(TemporalType.DATE)
	@Column(name="KA_DATA_UR")
	private Date kaDataUr=new Date(0,0,1);

	@Temporal(TemporalType.DATE)
	@Column(name="KA_DATA_ZLOZENIA")
	private Date kaDataZlozenia;

	@Column(name="KA_DOSW_W_BRANZY")
	private Long kaDoswWBranzy;

	@Column(name="KA_EMAIL")
	private String kaEmail;

	@Column(name="KA_F_CZY_NIEPELNOSPRAWNY")
	private String kaFCzyNiepelnosprawny;

	@Column(name="KA_F_CZY_WSK_W_PROT")
	private String kaFCzyWskWProt;

	@Column(name="KA_F_CZY_ZATRUDNIONY")
	private String kaFCzyZatrudniony;

	@Column(name="KA_F_WNIOSK_O_ZATR")
	private String kaFWnioskOZatr;

	@Column(name="KA_F_WYM_FORMALNE")
	private String kaFWymFormalne;

	@Column(name="KA_IMIE")
	private String kaImie;

	@Column(name="KA_IMIE_MATKI")
	private String kaImieMatki;

	@Column(name="KA_IMIE_OJCA")
	private String kaImieOjca;

	@Temporal(TemporalType.DATE)
	@Column(name="KA_AUDYT_DM")
	private Date kaAudytDM;

	@Temporal(TemporalType.DATE)
	@Column(name="KA_AUDYT_DT")
	private Date kaAudytDT;

	@Column(name="KA_KOD_NIEPELNOSPRAWNOSCI")
	private String kaKodNiepelnosprawnosci;

	@Column(name="KA_KOD_NIEZDOLNOSCI")
	private String kaKodNiezdolnosci;

	@Column(name="KA_KSAP")
	private String kaKsap;

	@Column(name="KA_AUDYT_UM")
	private String kaAudytUM;

	@Column(name="KA_AUDYT_UT")
	private String kaAudytUT;

	@Column(name="KA_MIEJSCE_UR")
	private String kaMiejsceUr;

	@Column(name="KA_NAB_RODZAJ")
	private String kaNabRodzaj;

	@Column(name="KA_NAZWISKO")
	private String kaNazwisko;

	@Column(name="KA_NIESP_WF_PRZYCZYNA")
	private String kaNiespWfPrzyczyna;

	@Column(name="KA_NIP_DANE")
	private String kaNipDane;

	@Column(name="KA_NIP_PREFIKS")
	private String kaNipPrefiks;

	@Column(name="KA_NR_TELEFONU")
	private String kaNrTelefonu;

	@Column(name="KA_NUMER")
	private Long kaNumer;

	@Column(name="KA_OBYWATELSTWO")
	private String kaObywatelstwo;

	@Column(name="KA_OPIS")
	private String kaOpis;

	@Column(name="KA_PESEL")
	private String kaPesel;

	@Column(name="KA_PLEC")
	private String kaPlec;

	@Column(name="KA_PRAWO_JAZDY")
	private String kaPrawoJazdy;

	@Column(name="KA_PRC_ID")
	private BigDecimal kaPrcId;

	@Column(name="KA_SCHORZENIA")
	private String kaSchorzenia;

	@Column(name="KA_TYP_UMOWY")
	private String kaTypUmowy;

	@Column(name="KA_TYTUL_NAUKOWY")
	private String kaTytulNaukowy;

	@Column(name="KA_WYMIAR_CZASU_PRACY")
	private String kaWymiarCzasuPracy;
	
	@Column(name="KA_F_CZY_ZGODA_NA_PRZE_DANYCH")
	private String kaFCzyZgodaNaPrzeDanych;
	
	@Column(name="KA_ZRODLO_DANYCH")
	private String kaZrodloDanych = "OS";
	
	@Column(name="KA_ZP_ID")
	private Long kaZpId;

	//bi-directional many-to-one association to PptRekAdresy
	@OneToMany(mappedBy="zptKandydaci", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<PptRekAdresy> pptAdresies = new ArrayList<>();
	
	//bi-directional many-to-one association to PptRekKandydatWamagania
	@OneToMany(mappedBy="pptRekKandydaci", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<PptRekKandydatWamagania> pptRekKandydatWamaganias = new ArrayList<>();

	//bi-directional many-to-one association to ZptNabory
	@ManyToOne
	@JoinColumn(name="KA_NAB_ID")
	private ZptNabory zptNabory;

//	//bi-directional many-to-one association to ZptNabWn5
//	@ManyToOne
//	@JoinColumn(name="KA_WN5_ID")
//	private ZptNabWn5 zptNabWn5;

	public ZptKandydaci() {
	}

	public Long getKaId() {
		return this.kaId;
	}

	public void setKaId(Long kaId) {
		this.kaId = kaId;
	}

	public String getKaAbsolwent() {
		return this.kaAbsolwent;
	}

	public void setKaAbsolwent(String kaAbsolwent) {
		this.kaAbsolwent = kaAbsolwent;
	}

	public Long getKaAdres() {
		return this.kaAdres;
	}

	public void setKaAdres(Long kaAdres) {
		this.kaAdres = kaAdres;
	}

	public String getKaAudytKm() {
		return this.kaAudytKm;
	}

	public void setKaAudytKm(String kaAudytKm) {
		this.kaAudytKm = kaAudytKm;
	}

	public String getKaAudytKt() {
		return this.kaAudytKt;
	}

	public void setKaAudytKt(String kaAudytKt) {
		this.kaAudytKt = kaAudytKt;
	}

	public BigDecimal getKaAudytLm() {
		return this.kaAudytLm;
	}

	public void setKaAudytLm(BigDecimal kaAudytLm) {
		this.kaAudytLm = kaAudytLm;
	}

	public Date getKaDataAnon() {
		return kaDataAnon;
	}

	public void setKaDataAnon(Date kaDataAnon) {
		this.kaDataAnon = kaDataAnon;
	}
	
	public Date getKaDataDoNiepel() {
		return this.kaDataDoNiepel;
	}

	public void setKaDataDoNiepel(Date kaDataDoNiepel) {
		this.kaDataDoNiepel = kaDataDoNiepel;
	}

	public Date getKaDataDoNiezdol() {
		return this.kaDataDoNiezdol;
	}

	public void setKaDataDoNiezdol(Date kaDataDoNiezdol) {
		this.kaDataDoNiezdol = kaDataDoNiezdol;
	}

	public Date getKaDataOdNiepel() {
		return this.kaDataOdNiepel;
	}

	public void setKaDataOdNiepel(Date kaDataOdNiepel) {
		this.kaDataOdNiepel = kaDataOdNiepel;
	}

	public Date getKaDataOdNiezdol() {
		return this.kaDataOdNiezdol;
	}

	public void setKaDataOdNiezdol(Date kaDataOdNiezdol) {
		this.kaDataOdNiezdol = kaDataOdNiezdol;
	}

	public Date getKaDataUr() {
		return this.kaDataUr;
	}

	public void setKaDataUr(Date kaDataUr) {
		this.kaDataUr = kaDataUr;
	}

	public Date getKaDataZlozenia() {
		return this.kaDataZlozenia;
	}

	public void setKaDataZlozenia(Date kaDataZlozenia) {
		this.kaDataZlozenia = kaDataZlozenia;
	}

	public Long getKaDoswWBranzy() {
		return this.kaDoswWBranzy;
	}

	public void setKaDoswWBranzy(Long kaDoswWBranzy) {
		this.kaDoswWBranzy = kaDoswWBranzy;
	}

	public String getKaEmail() {
		return this.kaEmail;
	}

	public void setKaEmail(String kaEmail) {
		this.kaEmail = kaEmail;
	}

	public String getKaFCzyNiepelnosprawny() {
		return this.kaFCzyNiepelnosprawny;
	}

	public void setKaFCzyNiepelnosprawny(String kaFCzyNiepelnosprawny) {
		this.kaFCzyNiepelnosprawny = kaFCzyNiepelnosprawny;
	}

	public String getKaFCzyWskWProt() {
		return this.kaFCzyWskWProt;
	}

	public void setKaFCzyWskWProt(String kaFCzyWskWProt) {
		this.kaFCzyWskWProt = kaFCzyWskWProt;
	}

	public String getKaFCzyZatrudniony() {
		return this.kaFCzyZatrudniony;
	}

	public void setKaFCzyZatrudniony(String kaFCzyZatrudniony) {
		this.kaFCzyZatrudniony = kaFCzyZatrudniony;
		
		if("T".equals(this.kaFCzyZatrudniony) && new Date(0,0,1).equals(this.kaDataUr))
			this.kaDataUr=null;
		
		if("N".equals(this.kaFCzyZatrudniony))
			this.kaDataUr=new Date(0,0,1);			
	}

	public String getKaFWnioskOZatr() {
		return this.kaFWnioskOZatr;
	}

	public void setKaFWnioskOZatr(String kaFWnioskOZatr) {
		this.kaFWnioskOZatr = kaFWnioskOZatr;
	}

	public String getKaFWymFormalne() {
		return this.kaFWymFormalne;
	}

	public void setKaFWymFormalne(String kaFWymFormalne) {
		this.kaFWymFormalne = kaFWymFormalne;
	}

	public String getKaImie() {
		return this.kaImie;
	}

	public void setKaImie(String kaImie) {
		this.kaImie = kaImie;
	}

	public String getKaImieMatki() {
		return this.kaImieMatki;
	}

	public void setKaImieMatki(String kaImieMatki) {
		this.kaImieMatki = kaImieMatki;
	}

	public String getKaImieOjca() {
		return this.kaImieOjca;
	}

	public void setKaImieOjca(String kaImieOjca) {
		this.kaImieOjca = kaImieOjca;
	}

	public Date getKaAudytDM() {
		return this.kaAudytDM;
	}

	public void setKaAudytDM(Date kaAudytDM) {
		this.kaAudytDM = kaAudytDM;
	}

	public Date getKaAudytDT() {
		return this.kaAudytDT;
	}

	public void setKaAudytDT(Date kaAudytDT) {
		this.kaAudytDT = kaAudytDT;
	}

	public String getKaKodNiepelnosprawnosci() {
		return this.kaKodNiepelnosprawnosci;
	}

	public void setKaKodNiepelnosprawnosci(String kaKodNiepelnosprawnosci) {
		this.kaKodNiepelnosprawnosci = kaKodNiepelnosprawnosci;
	}

	public String getKaKodNiezdolnosci() {
		return this.kaKodNiezdolnosci;
	}

	public void setKaKodNiezdolnosci(String kaKodNiezdolnosci) {
		this.kaKodNiezdolnosci = kaKodNiezdolnosci;
	}

	public String getKaKsap() {
		return this.kaKsap;
	}

	public void setKaKsap(String kaKsap) {
		this.kaKsap = kaKsap;
	}

	public String getKaAudytUM() {
		return this.kaAudytUM;
	}

	public void setKaAudytUM(String kaAudytUM) {
		this.kaAudytUM = kaAudytUM;
	}

	public String getKaAudytUT() {
		return this.kaAudytUT;
	}

	public void setKaAudytUT(String kaAudytUT) {
		this.kaAudytUT = kaAudytUT;
	}

	public String getKaMiejsceUr() {
		return this.kaMiejsceUr;
	}

	public void setKaMiejsceUr(String kaMiejsceUr) {
		this.kaMiejsceUr = kaMiejsceUr;
	}

	public String getKaNabRodzaj() {
		return this.kaNabRodzaj;
	}

	public void setKaNabRodzaj(String kaNabRodzaj) {
		this.kaNabRodzaj = kaNabRodzaj;
	}

	public String getKaNazwisko() {
		return this.kaNazwisko;
	}

	public void setKaNazwisko(String kaNazwisko) {
		this.kaNazwisko = kaNazwisko;
	}

	public String getKaNiespWfPrzyczyna() {
		return this.kaNiespWfPrzyczyna;
	}

	public void setKaNiespWfPrzyczyna(String kaNiespWfPrzyczyna) {
		this.kaNiespWfPrzyczyna = kaNiespWfPrzyczyna;
	}

	public String getKaNipDane() {
		return this.kaNipDane;
	}

	public void setKaNipDane(String kaNipDane) {
		this.kaNipDane = kaNipDane;
	}

	public String getKaNipPrefiks() {
		return this.kaNipPrefiks;
	}

	public void setKaNipPrefiks(String kaNipPrefiks) {
		this.kaNipPrefiks = kaNipPrefiks;
	}

	public String getKaNrTelefonu() {
		return this.kaNrTelefonu;
	}

	public void setKaNrTelefonu(String kaNrTelefonu) {
		this.kaNrTelefonu = kaNrTelefonu;
	}

	public Long getKaNumer() {
		return this.kaNumer;
	}

	public void setKaNumer(Long kaNumer) {
		this.kaNumer = kaNumer;
	}

	public String getKaObywatelstwo() {
		return this.kaObywatelstwo;
	}

	public void setKaObywatelstwo(String kaObywatelstwo) {
		this.kaObywatelstwo = kaObywatelstwo;
	}

	public String getKaOpis() {
		return this.kaOpis;
	}

	public void setKaOpis(String kaOpis) {
		this.kaOpis = kaOpis;
	}

	public String getKaPesel() {
		return this.kaPesel;
	}

	public void setKaPesel(String kaPesel) {
		this.kaPesel = kaPesel;
	}

	public String getKaPlec() {
		return this.kaPlec;
	}

	public void setKaPlec(String kaPlec) {
		this.kaPlec = kaPlec;
	}

	public String getKaPrawoJazdy() {
		return this.kaPrawoJazdy;
	}

	public void setKaPrawoJazdy(String kaPrawoJazdy) {
		this.kaPrawoJazdy = kaPrawoJazdy;
	}

	public BigDecimal getKaPrcId() {
		return this.kaPrcId;
	}

	public void setKaPrcId(BigDecimal kaPrcId) {
		this.kaPrcId = kaPrcId;
	}

	public String getKaSchorzenia() {
		return this.kaSchorzenia;
	}

	public void setKaSchorzenia(String kaSchorzenia) {
		this.kaSchorzenia = kaSchorzenia;
	}

	public String getKaTypUmowy() {
		return this.kaTypUmowy;
	}

	public void setKaTypUmowy(String kaTypUmowy) {
		this.kaTypUmowy = kaTypUmowy;
	}

	public String getKaTytulNaukowy() {
		return this.kaTytulNaukowy;
	}

	public void setKaTytulNaukowy(String kaTytulNaukowy) {
		this.kaTytulNaukowy = kaTytulNaukowy;
	}

	public String getKaWymiarCzasuPracy() {
		return this.kaWymiarCzasuPracy;
	}

	public void setKaWymiarCzasuPracy(String kaWymiarCzasuPracy) {
		this.kaWymiarCzasuPracy = kaWymiarCzasuPracy;
	}

	public List<PptRekAdresy> getPptAdresies() {
		return this.pptAdresies;
	}

	public void setPptAdresies(List<PptRekAdresy> pptAdresies) {
		this.pptAdresies = pptAdresies;
	}

	public PptRekAdresy addPptAdresy(PptRekAdresy pptAdresy) {
		getPptAdresies().add(pptAdresy);
		pptAdresy.setZptKandydaci(this);

		return pptAdresy;
	}

	public PptRekAdresy removePptRekAdresy(PptRekAdresy pptAdresy) {
		getPptAdresies().remove(pptAdresy);
		pptAdresy.setZptKandydaci(null);

		return pptAdresy;
	}

	public ZptNabory getZptNabory() {
		return this.zptNabory;
	}

	public void setZptNabory(ZptNabory zptNabory) {
		this.zptNabory = zptNabory;
	}
//
//	public ZptNabWn5 getZptNabWn5() {
//		return this.zptNabWn5;
//	}
//
//	public void setZptNabWn5(ZptNabWn5 zptNabWn5) {
//		this.zptNabWn5 = zptNabWn5;
//	}

	public String getKaFCzyZgodaNaPrzeDanych() {
		return kaFCzyZgodaNaPrzeDanych;
	}

	public void setKaFCzyZgodaNaPrzeDanych(String kaFCzyZgodaNaPrzeDanych) {
		this.kaFCzyZgodaNaPrzeDanych = kaFCzyZgodaNaPrzeDanych;
	}
	
	public Long getKaZpId() {
		return kaZpId;
	}

	public void setKaZpId(Long kaZpId) {
		this.kaZpId = kaZpId;
	}

	public String getKaZrodloDanych() {
		return kaZrodloDanych;
	}

	public void setKaZrodloDanych(String kaZrodloDanych) {
		this.kaZrodloDanych = kaZrodloDanych;
	}

	public List<PptRekKandydatWamagania> getPptRekKandydatWamaganias() {
		return pptRekKandydatWamaganias;
	}

	public void setPptRekKandydatWamaganias(List<PptRekKandydatWamagania> pptRekKandydatWamaganias) {
		this.pptRekKandydatWamaganias = pptRekKandydatWamaganias;
	}
	
	public PptRekKandydatWamagania addPptRekKandydatWamagania(PptRekKandydatWamagania pptRekKandydatWamagania) {
		getPptRekKandydatWamaganias().add(pptRekKandydatWamagania);
		pptRekKandydatWamagania.setPptRekKandydaci(this);

		return pptRekKandydatWamagania;
	}

	public PptRekKandydatWamagania removePptRekKandydatWamagania(PptRekKandydatWamagania pptRekKandydatWamagania) {
		getPptRekKandydatWamaganias().remove(pptRekKandydatWamagania);
		pptRekKandydatWamagania.setPptRekKandydaci(null);

		return pptRekKandydatWamagania;
	}
	
	public void dodajNoweWymaganie(String typ){
		PptRekKandydatWamagania wym = new PptRekKandydatWamagania();
		wym.setKawyFNiezbedne(typ);
		this.addPptRekKandydatWamagania(wym);
	}

}