package com.comarch.egeria.pp.Rekrutacja.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_REK_NABOR_KOMISJA database table.
 * 
 */
@Entity
@Table(name="PPT_REK_NABOR_KOMISJA", schema="PPADM")
@NamedQuery(name="ZptNaborKomisja.findAll", query="SELECT z FROM ZptNaborKomisja z")
public class ZptNaborKomisja implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_REK_NABOR_KOMISJA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_REK_NABOR_KOMISJA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_REK_NABOR_KOMISJA")
	@Column(name="KOM_ID")
	private Long komId;

	@Temporal(TemporalType.DATE)
	@Column(name="KOM_AUDYT_DM")
	private Date komAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="KOM_AUDYT_DT")
	private Date komAudytDt;

	@Column(name="KOM_AUDYT_KM")
	private String komAudytKm;

	@Column(name="KOM_AUDYT_LM")
	private String komAudytLm;

	@Column(name="KOM_AUDYT_UM")
	private String komAudytUm;

	@Column(name="KOM_AUDYT_UT")
	private String komAudytUt;

	@Column(name="KOM_PRC_ID")
	private Long komPrcId;

	@Column(name="KOM_TYP")
	private String komTyp;
	
	@Column(name="KOM_SPOZA_MF")
	private String komSpozaMf;

	//bi-directional many-to-one association to ZptNabory
	@ManyToOne
	@JoinColumn(name="KOM_NAB_ID")
	private ZptNabory zptNabory;

	public ZptNaborKomisja() {
	}


	public Long getKomId() {
		return this.komId;
	}

	public void setKomId(Long komId) {
		this.komId = komId;
	}

	public Date getKomAudytDm() {
		return this.komAudytDm;
	}

	public void setKomAudytDm(Date komAudytDm) {
		this.komAudytDm = komAudytDm;
	}

	public Date getKomAudytDt() {
		return this.komAudytDt;
	}

	public void setKomAudytDt(Date komAudytDt) {
		this.komAudytDt = komAudytDt;
	}

	public String getKomAudytKm() {
		return this.komAudytKm;
	}

	public void setKomAudytKm(String komAudytKm) {
		this.komAudytKm = komAudytKm;
	}

	public String getKomAudytLm() {
		return this.komAudytLm;
	}

	public void setKomAudytLm(String komAudytLm) {
		this.komAudytLm = komAudytLm;
	}

	public String getKomAudytUm() {
		return this.komAudytUm;
	}

	public void setKomAudytUm(String komAudytUm) {
		this.komAudytUm = komAudytUm;
	}

	public String getKomAudytUt() {
		return this.komAudytUt;
	}

	public void setKomAudytUt(String komAudytUt) {
		this.komAudytUt = komAudytUt;
	}

	public Long getKomPrcId() {
		return this.komPrcId;
	}

	public void setKomPrcId(Long komPrcId) {
		this.komPrcId = komPrcId;
	}

	public String getKomTyp() {
		return this.komTyp;
	}

	public void setKomTyp(String komTyp) {
		this.komTyp = komTyp;
	}

	public ZptNabory getZptNabory() {
		return this.zptNabory;
	}

	public void setZptNabory(ZptNabory zptNabory) {
		this.zptNabory = zptNabory;
	}
	
	public String getKomSpozaMf() {
		return komSpozaMf;
	}

	public void setKomSpozaMf(String komSpozaMf) {
		this.komSpozaMf = komSpozaMf;
	}

}