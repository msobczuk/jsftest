package com.comarch.egeria.pp.Rekrutacja;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.HashMap;

import javax.faces.bean.ManagedBean;
import javax.faces.event.ActionEvent;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.component.datatable.DataTable;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.Rekrutacja.model.PptUdostepnieniaDanych;
import com.comarch.egeria.pp.Rekrutacja.model.ZptKandydaci;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;


@ManagedBean
@Named
@Scope("view")
public class OchronaDanychBean extends SqlBean implements Serializable{
	private static final long serialVersionUID = -5806366986076184951L;
	private static final Logger log = LogManager.getLogger();//public bo wiekszosc beanow dziedziczy po tym


	@Inject User user;
	
	private Long kaId = -1L;
	private ZptKandydaci kandydat = new ZptKandydaci();
	
	private DataRow udstDR;
	private PptUdostepnieniaDanych udst;
	
	
	public void raport(){
		String nazwa = "rek_ochrona_danych";

		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("ka_id", this.kaId);
		params.put("zalogowany_prc_nazwa", user.getLogin());
		
		try {
			ReportGenerator.displayReportAsPDF(params, nazwa);

		} catch (JRException | SQLException | IOException e) {
			e.printStackTrace();
			User.alert(e.getMessage());
		}

	}

	
	public void addUDSTActionListener(ActionEvent event) {
		wczytajUdst(null);
		com.comarch.egeria.Utils.executeScript("PF('dlgUDST').show();");
	}
	
	
	public void deleteUDSTActionListener(ActionEvent event) {
		DataTable tb = (DataTable) event.getComponent().getParent().getParent();
		DataRow r = (DataRow) tb.getRowData();
		wczytajUdst(r);
		HibernateContext.delete(this.udst);
		//this.reloadAndUpdate(r);
		reloadAndUpdate("ppl_rek_kand_udst_rodo", r.getIdAsLong());
		wczytajUdst(null);
	}
	
	
	public void editUDSTActionListener(ActionEvent event) {
		DataTable tb = (DataTable) event.getComponent().getParent().getParent();
		DataRow r = (DataRow) tb.getRowData();
		wczytajUdst(r);
		com.comarch.egeria.Utils.executeScript("PF('dlgUDST').show();");
	}
	
	
	public void saveUDSTActionListener(ActionEvent event) {
		
		try {
			this.udst = (PptUdostepnieniaDanych) HibernateContext.save(this.udst);
			User.info("Zapisano dane.");
			reloadAndUpdate("ppl_rek_kand_udst_rodo", this.udst.getUdstId());
			com.comarch.egeria.Utils.executeScript("PF('dlgUDST').hide();");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
		
	}


	
	public void wczytajUdst(DataRow r) {
		this.setUdstDR(r);
		if (r==null){
			this.udst = new PptUdostepnieniaDanych();
			this.udst.setUdstKaId(this.getKaId());
		}
		else {
			this.udst = (PptUdostepnieniaDanych) HibernateContext.get(PptUdostepnieniaDanych.class, r.getIdAsLong());
		}
		
		com.comarch.egeria.Utils.update("dlgUDST");
	}
	
	
	
	
	
	public void editKandydatOchronaDanychActionListener(ActionEvent event) {
		DataTable tb = (DataTable) event.getComponent().getParent().getParent();
		DataRow r = (DataRow) tb.getRowData();
		this.setKaId(r.getIdAsLong());
		com.comarch.egeria.Utils.update("dlgKandOchrona");
		com.comarch.egeria.Utils.executeScript("PF('dlgKandOchrona').show();");
	}
	
	public void saveKandydatOchronaDanychActionListener(ActionEvent event) {
		
		try {
			this.kandydat = (ZptKandydaci) HibernateContext.save(this.kandydat);
			User.info("Zapisano dane.");
			reloadAndUpdate("ppl_rek_kandydaci_rodo", this.kandydat.getKaId());
			com.comarch.egeria.Utils.executeScript("PF('dlgKandOchrona').hide();");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}
		
	}
	
	

	public Long getKaId() {
		return kaId;
	}



	public void setKaId(Long kaId) {
		this.kaId = kaId;
		if (kaId!=null && kaId!=0L)
			this.setKandydat((ZptKandydaci) HibernateContext.get(ZptKandydaci.class, kaId));
		else
			this.setKandydat(new ZptKandydaci());
	}


	public PptUdostepnieniaDanych getUdst() {
		return udst;
	}


	public void setUdst(PptUdostepnieniaDanych udst) {
		this.udst = udst;
	}


	public DataRow getUdstDR() {
		return udstDR;
	}


	public void setUdstDR(DataRow udstDR) {
		this.udstDR = udstDR;
	}


	public ZptKandydaci getKandydat() {
		return kandydat;
	}


	public void setKandydat(ZptKandydaci kandydat) {
		this.kandydat = kandydat;
	}

	
}
