package com.comarch.egeria.pp.Rekrutacja.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_REK_NABOR_ZESPOLY database table.
 * 
 */
@Entity
@Table(name="PPT_REK_NABOR_ZESPOLY", schema="PPADM")
@NamedQuery(name="ZptNaborZespoly.findAll", query="SELECT z FROM ZptNaborZespoly z")
public class ZptNaborZespoly implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_REK_NABOR_ZESPOLY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_REK_NABOR_ZESPOLY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_REK_NABOR_ZESPOLY")
	@Column(name="NZE_ID")
	private Long nzeId;

	@Temporal(TemporalType.DATE)
	@Column(name="NZE_AUDYT_DM")
	private Date nzeAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="NZE_AUDYT_DT")
	private Date nzeAudytDt;

	@Column(name="NZE_AUDYT_KM")
	private String nzeAudytKm;

	@Column(name="NZE_AUDYT_LM")
	private String nzeAudytLm;

	@Column(name="NZE_AUDYT_UM")
	private String nzeAudytUm;

	@Column(name="NZE_AUDYT_UT")
	private String nzeAudytUt;

	@Column(name="NZE_PRC_ID")
	private Long nzePrcId;

	@Column(name="NZE_PRZEWODNICZACY")
	private String nzePrzewodniczacy;

//	//bi-directional many-to-one association to ZptNabWn5
//	@ManyToOne
//	@JoinColumn(name="NZE_NAB_WN5_ID")
//	private ZptNabWn5 zptNabWn5;

	public ZptNaborZespoly() {
	}

	public Long getNzeId() {
		return this.nzeId;
	}

	public void setNzeId(Long nzeId) {
		this.nzeId = nzeId;
	}

	public Date getNzeAudytDm() {
		return this.nzeAudytDm;
	}

	public void setNzeAudytDm(Date nzeAudytDm) {
		this.nzeAudytDm = nzeAudytDm;
	}

	public Date getNzeAudytDt() {
		return this.nzeAudytDt;
	}

	public void setNzeAudytDt(Date nzeAudytDt) {
		this.nzeAudytDt = nzeAudytDt;
	}

	public String getNzeAudytKm() {
		return this.nzeAudytKm;
	}

	public void setNzeAudytKm(String nzeAudytKm) {
		this.nzeAudytKm = nzeAudytKm;
	}

	public String getNzeAudytLm() {
		return this.nzeAudytLm;
	}

	public void setNzeAudytLm(String nzeAudytLm) {
		this.nzeAudytLm = nzeAudytLm;
	}

	public String getNzeAudytUm() {
		return this.nzeAudytUm;
	}

	public void setNzeAudytUm(String nzeAudytUm) {
		this.nzeAudytUm = nzeAudytUm;
	}

	public String getNzeAudytUt() {
		return this.nzeAudytUt;
	}

	public void setNzeAudytUt(String nzeAudytUt) {
		this.nzeAudytUt = nzeAudytUt;
	}

	public Long getNzePrcId() {
		return this.nzePrcId;
	}

	public void setNzePrcId(Long nzePrcId) {
		this.nzePrcId = nzePrcId;
	}

	public String getNzePrzewodniczacy() {
		return this.nzePrzewodniczacy;
	}

	public void setNzePrzewodniczacy(String nzePrzewodniczacy) {
		this.nzePrzewodniczacy = nzePrzewodniczacy;
	}

//	public ZptNabWn5 getZptNabWn5() {
//		return this.zptNabWn5;
//	}
//
//	public void setZptNabWn5(ZptNabWn5 zptNabWn5) {
//		this.zptNabWn5 = zptNabWn5;
//	}

}