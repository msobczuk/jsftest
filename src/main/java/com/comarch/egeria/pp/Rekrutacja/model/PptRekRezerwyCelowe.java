package com.comarch.egeria.pp.Rekrutacja.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PPT_REK_REZERWY_CELOWE database table.
 * 
 */
@Entity
@Table(name="PPT_REK_REZERWY_CELOWE", schema="PPADM")
@NamedQuery(name="PptRekRezerwyCelowe.findAll", query="SELECT p FROM PptRekRezerwyCelowe p")
public class PptRekRezerwyCelowe implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_REK_REZERWY_CELOWE",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_REK_REZERWY_CELOWE", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_REK_REZERWY_CELOWE")
	@Column(name="NARC_ID")
	private long narcId;
	
	@Column(name="NARC_REZERWA_ETAT")
	private Double narcRezerwaEtat;

	@Column(name="NARC_REZERWA_NAZWA")
	private String narcRezerwaNazwa;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="NARC_AUDYT_DM")
	private Date narcAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="NARC_AUDYT_DT")
	private Date narcAudytDt;

	@Column(name="NARC_AUDYT_KM")
	private String narcAudytKm;

	@Column(name="NARC_AUDYT_LM")
	private String narcAudytLm;

	@Column(name="NARC_AUDYT_UM")
	private String narcAudytUm;

	@Column(name="NARC_AUDYT_UT")
	private String narcAudytUt;



	//bi-directional many-to-one association to PptRekNabory
	@ManyToOne
	@JoinColumn(name="NARC_NAB_ID")
	private ZptNabory zptNabory;

	public PptRekRezerwyCelowe() {
	}

	public long getNarcId() {
		return this.narcId;
	}

	public void setNarcId(long narcId) {
		this.narcId = narcId;
	}

	public Date getNarcAudytDm() {
		return this.narcAudytDm;
	}

	public void setNarcAudytDm(Date narcAudytDm) {
		this.narcAudytDm = narcAudytDm;
	}

	public Date getNarcAudytDt() {
		return this.narcAudytDt;
	}

	public void setNarcAudytDt(Date narcAudytDt) {
		this.narcAudytDt = narcAudytDt;
	}

	public String getNarcAudytKm() {
		return this.narcAudytKm;
	}

	public void setNarcAudytKm(String narcAudytKm) {
		this.narcAudytKm = narcAudytKm;
	}

	public String getNarcAudytLm() {
		return this.narcAudytLm;
	}

	public void setNarcAudytLm(String narcAudytLm) {
		this.narcAudytLm = narcAudytLm;
	}

	public String getNarcAudytUm() {
		return this.narcAudytUm;
	}

	public void setNarcAudytUm(String narcAudytUm) {
		this.narcAudytUm = narcAudytUm;
	}

	public String getNarcAudytUt() {
		return this.narcAudytUt;
	}

	public void setNarcAudytUt(String narcAudytUt) {
		this.narcAudytUt = narcAudytUt;
	}

	public Double getNarcRezerwaEtat() {
		return this.narcRezerwaEtat;
	}

	public void setNarcRezerwaEtat(Double narcRezerwaEtat) {
		this.narcRezerwaEtat = narcRezerwaEtat;
	}

	public String getNarcRezerwaNazwa() {
		return this.narcRezerwaNazwa;
	}

	public void setNarcRezerwaNazwa(String narcRezerwaNazwa) {
		this.narcRezerwaNazwa = narcRezerwaNazwa;
	}

	public ZptNabory getZptNabory() {
		return this.zptNabory;
	}

	public void setZptNabory(ZptNabory zptNabory) {
		this.zptNabory = zptNabory;
	}

}