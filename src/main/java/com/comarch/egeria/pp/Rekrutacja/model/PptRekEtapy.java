package com.comarch.egeria.pp.Rekrutacja.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the PPT_REK_REZERWY_CELOWE database table.
 * 
 */
@Entity
@Table(name="PPT_REK_ETAPY", schema="PPADM")
@NamedQuery(name="PptRekEtapy.findAll", query="SELECT p FROM PptRekEtapy p")
public class PptRekEtapy implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_REK_ETAPY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_REK_ETAPY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_REK_ETAPY")
	@Column(name="NABE_ID")
	private long nabeId;
	
	@Column(name="NABE_ETAP")
	private String nabeEtat;
	
	@Column(name="NABE_F_INNE")
	private String nabeFInne;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="NABE_ETAP_DATA")
	private Date nabeEtapData;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="NABE_AUDYT_DM")
	private Date nabeAudytDm;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="NABE_AUDYT_DT")
	private Date nabeAudytDt;

	@Column(name="NABE_AUDYT_KM")
	private String nabeAudytKm;

	@Column(name="NABE_AUDYT_LM")
	private String nabeAudytLm;

	@Column(name="NABE_AUDYT_UM")
	private String nabeAudytUm;

	@Column(name="NABE_AUDYT_UT")
	private String nabeAudytUt;
	
	//bi-directional many-to-one association to PptRekNabory
	@ManyToOne
	@JoinColumn(name="NABE_NAB_ID")
	private ZptNabory zptNabory;
	
	public PptRekEtapy() {
	}

	public long getNabeId() {
		return nabeId;
	}

	public void setNabeId(long nabeId) {
		this.nabeId = nabeId;
	}

	public String getNabeEtat() {
		return nabeEtat;
	}

	public void setNabeEtat(String nabeEtat) {
		this.nabeEtat = nabeEtat;
	}

	public Date getNabeEtapData() {
		return nabeEtapData;
	}

	public void setNabeEtapData(Date nabeEtapData) {
		this.nabeEtapData = nabeEtapData;
	}

	public Date getNabeAudytDm() {
		return nabeAudytDm;
	}

	public void setNabeAudytDm(Date nabeAudytDm) {
		this.nabeAudytDm = nabeAudytDm;
	}

	public Date getNabeAudytDt() {
		return nabeAudytDt;
	}

	public void setNabeAudytDt(Date nabeAudytDt) {
		this.nabeAudytDt = nabeAudytDt;
	}

	public String getNabeAudytKm() {
		return nabeAudytKm;
	}

	public void setNabeAudytKm(String nabeAudytKm) {
		this.nabeAudytKm = nabeAudytKm;
	}

	public String getNabeAudytLm() {
		return nabeAudytLm;
	}

	public void setNabeAudytLm(String nabeAudytLm) {
		this.nabeAudytLm = nabeAudytLm;
	}

	public String getNabeAudytUm() {
		return nabeAudytUm;
	}

	public void setNabeAudytUm(String nabeAudytUm) {
		this.nabeAudytUm = nabeAudytUm;
	}

	public String getNabeAudytUt() {
		return nabeAudytUt;
	}

	public void setNabeAudytUt(String nabeAudytUt) {
		this.nabeAudytUt = nabeAudytUt;
	}

	public ZptNabory getZptNabory() {
		return zptNabory;
	}

	public void setZptNabory(ZptNabory zptNabory) {
		this.zptNabory = zptNabory;
	}

	public String getNabeFInne() {
		return nabeFInne;
	}

	public void setNabeFInne(String nabeFInne) {
		this.nabeFInne = nabeFInne;
	}

}
