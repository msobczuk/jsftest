package com.comarch.egeria.pp.Rekrutacja.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the PPT_REK_NABOR_SYMBOL database table.
 * 
 */
@Entity
@Table(name="PPT_REK_NABOR_SYMBOL", schema="PPADM")
@NamedQuery(name="ZptNabSymb.findAll", query="SELECT z FROM ZptNabSymb z")
public class ZptNabSymb implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_REK_NABOR_SYMBOL",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_REK_NABOR_SYMBOL", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_REK_NABOR_SYMBOL")
	@Column(name="NSM_ID")
	private Long nsmId;

	@Temporal(TemporalType.DATE)
	@Column(name="NSM_AUDYT_DM")
	private Date nsmAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="NSM_AUDYT_DT")
	private Date nsmAudytDt;

	@Column(name="NSM_AUDYT_KM")
	private String nsmAudytKm;

	@Column(name="NSM_AUDYT_LM")
	private String nsmAudytLm;

	@Column(name="NSM_AUDYT_UM")
	private String nsmAudytUm;

	@Column(name="NSM_AUDYT_UT")
	private String nsmAudytUt;

	//bi-directional many-to-one association to ZptNabory
	@ManyToOne
	@JoinColumn(name="NSM_NAB_ID")
	private ZptNabory zptNabory;

//	//bi-directional many-to-one association to ZptNabWn5
//	@ManyToOne
//	@JoinColumn(name="NSM_NWN_ID")
//	private ZptNabWn5 zptNabWn5;

	@Column(name="NSM_STO_ID")
	private Long nsmStoId;
	
	@Temporal(TemporalType.DATE)
	@Column(name="NSM_NAB_DATA_WOLNE_OD")
	private Date nsmNabDataWolneOd = new Date();

	public ZptNabSymb() {
	}

	public Long getNsmId() {
		return this.nsmId;
	}

	public void setNsmId(Long nsmId) {
		this.nsmId = nsmId;
	}

	public Date getNsmAudytDm() {
		return this.nsmAudytDm;
	}

	public void setNsmAudytDm(Date nsmAudytDm) {
		this.nsmAudytDm = nsmAudytDm;
	}

	public Date getNsmAudytDt() {
		return this.nsmAudytDt;
	}

	public void setNsmAudytDt(Date nsmAudytDt) {
		this.nsmAudytDt = nsmAudytDt;
	}

	public String getNsmAudytKm() {
		return this.nsmAudytKm;
	}

	public void setNsmAudytKm(String nsmAudytKm) {
		this.nsmAudytKm = nsmAudytKm;
	}

	public String getNsmAudytLm() {
		return this.nsmAudytLm;
	}

	public void setNsmAudytLm(String nsmAudytLm) {
		this.nsmAudytLm = nsmAudytLm;
	}

	public String getNsmAudytUm() {
		return this.nsmAudytUm;
	}

	public void setNsmAudytUm(String nsmAudytUm) {
		this.nsmAudytUm = nsmAudytUm;
	}

	public String getNsmAudytUt() {
		return this.nsmAudytUt;
	}

	public void setNsmAudytUt(String nsmAudytUt) {
		this.nsmAudytUt = nsmAudytUt;
	}

	public Long getNsmStoId() {
		return this.nsmStoId;
	}

	public void setNsmStoId(Long nsmStoId) {
		this.nsmStoId = nsmStoId;
	}
	
	public ZptNabory getZptNabory() {
		return this.zptNabory;
	}

	public void setZptNabory(ZptNabory zptNabory) {
		this.zptNabory = zptNabory;
	}

//	public ZptNabWn5 getZptNabWn5() {
//		return this.zptNabWn5;
//	}
//
//	public void setZptNabWn5(ZptNabWn5 zptNabWn5) {
//		this.zptNabWn5 = zptNabWn5;
//	}
	
	public Date getNsmNabDataWolneOd() {
		return nsmNabDataWolneOd;
	}

	public void setNsmNabDataWolneOd(Date nsmNabDataWolneOd) {
		this.nsmNabDataWolneOd = nsmNabDataWolneOd;
	}

}