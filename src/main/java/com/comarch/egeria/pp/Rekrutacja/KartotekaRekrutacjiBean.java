package com.comarch.egeria.pp.Rekrutacja;

import static com.comarch.egeria.Utils.*;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.rowset.CachedRowSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.Rekrutacja.model.PptRekEtapy;
import com.comarch.egeria.pp.Rekrutacja.model.ZptNaborKomisja;
import com.comarch.egeria.pp.Rekrutacja.model.ZptNaborSklad;
import com.comarch.egeria.pp.Rekrutacja.model.ZptNabory;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SessionBean;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean
@Named
@Scope("view")
public class KartotekaRekrutacjiBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();//public bo wiekszosc beanow dziedziczy po tym

	@Inject
	SessionBean sessionBean;


	private ZptNabory nabor = null;
	private Long selectedKartotekaId = null;
	DataRow kartotekaRekrutacji = null;


	int activeIndex = 0;

	private List<ZptNaborKomisja> czlonkowieKomSpozaMF = new ArrayList<>();

	//private List<ZptNaborSklad> wynagrodzenieMiesieczne = new ArrayList<>();

	private int wybranyEtap = 0;
	
	public void ukryjWybranychKomisjaNO(){
		List<Long> collect = this.nabor.getZptNaborKomisjasNiszczeniaOfert().stream().map(x->x.getKomPrcId()).collect(Collectors.toList());
		hideLwRows("PPL_REK_PRACOWNICY_DBG", collect );
	}
	
	
	
	public void dodajCzlonkowKomisjaNO(){
		
		for (DataRow r : this.getLW("PPL_REK_PRACOWNICY_DBG").getSelectedRows() ) {
			System.out.println(r);
			ZptNaborKomisja czk = new ZptNaborKomisja();
			czk.setKomPrcId(r.getIdAsLong());
			czk.setKomTyp("O"); //VARCHAR2(1 byta)
			this.nabor.addZptNaborKomisja(czk);
		}
		
		this.getLW("PPL_REK_PRACOWNICY_DBG").clearSelection();
	}
	
	
	@PostConstruct
	public void init() {
		
		
	
//		System.out.println("KartotekaRekrutacjiBean.init().........................");

		 Object id = FacesContext.getCurrentInstance().getExternalContext().getFlash().get("id");


		kartotekaRekrutacji = (DataRow) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("row");

		if (id == null || "".equals(id)) {
			this.setNabor(new ZptNabory());
			this.nabor.setSqlBean(this);
			return;
		}

		setSelectedKartotekaId(Long.parseLong("" + id));

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();
			this.setNabor(s.get(ZptNabory.class, selectedKartotekaId));
			this.nabor.setSqlBean(this);
			// ustawienie wartości procentowych
			if (nabor != null) {

				if (nabor.getNabEtatProjekt() != null)
					nabor.setNabEtatProjekt(nabor.getNabEtatProjekt() * 100);

				if (nabor.getNabEtatRezerwa() != null)
					nabor.setNabEtatRezerwa(nabor.getNabEtatRezerwa() * 100);

				// if (nabor.getNabEtatZastepstwo() != null)
				// nabor.setNabEtatZastepstwo(nabor.getNabEtatZastepstwo() *
				// 100);
				//
				// if (nabor.getNabEtatZwyk() != null)
				// nabor.setNabEtatZwyk(nabor.getNabEtatZwyk() * 100);
				// s.close();//GRRRRRR!!!!!! szaleńcy....

				/*if (nabor.getZptNaborSklads() != null) {
					wynagrodzenieMiesieczne = listFromSet(nabor.getZptNaborSklads());
				}*/

//				checkStringTabII = getCheckString_TabII();
			}
		}
		if (nabor.getZptNaborKomisjas() != null) {
			for (ZptNaborKomisja znk : nabor.getZptNaborKomisjas()) {
				if (("" + znk.getKomTyp()).equals("S") && znk.getKomPrcId() == null) {
					czlonkowieKomSpozaMF.add(znk);
				}
			}
		}
		
		this.getLW("PPL_REK_PRACOWNICY_DBG", 100893, 100893).size();
//		ukryjWybranychKomisjaNO();
		
		if (nabor.getPptRekEtapy().isEmpty())
			nabor.addPptRekEtapy(new PptRekEtapy());
	}
	
	public void anonimizujWybranych() {
		
		try (Connection conn = JdbcUtils.getConnection()) {
			List<DataRow> tempList = new ArrayList<>();
			if (wybranyEtap == 0) {
				tempList = this.getSql("PPL_REK_KANDYDACI").getSelectedRows();
			} else if (wybranyEtap == 1) {
				for (DataRow row : this.getSql("PPL_REK_KANDYDACI").getData()) {
					if (!row.get("ka_f_wym_formalne").equals("T") || !row.get("ka_f_czy_wsk_w_prot").equals("T")) {
						tempList.add(row);
					}
				}
			} else if (wybranyEtap == 2) {
				for (DataRow row : this.getSql("PPL_REK_KANDYDACI").getData()) {
					if ((!"T".equals(row.get("ka_f_wym_formalne")) && !"X".equals(row.get("ka_f_wym_formalne")))
							|| (!"T".equals(row.get("ka_f_czy_wsk_w_prot")) && !"X".equals(row.get("ka_f_czy_wsk_w_prot")))) {
						tempList.add(row);
					}
				}
			}
			
			if (tempList != null && !tempList.isEmpty()) {
				for (DataRow dr : tempList) {
					JdbcUtils.sqlSPCall(conn, "PPADM.PPP_UTILITIES.anonimizuj_kandydata", dr.getIdAsLong());
				}
			}

		} catch (Exception e) {
			User.alert("Nieudana anonimizacja danych kandydata");
			return;
		}

		
		SqlDataSelectionsHandler sql = this.getSql("PPL_REK_KANDYDACI");
		if (sql!=null)
			sql.setTs(0);

		com.comarch.egeria.Utils.update("frmKartotekaRek:tabViewId:fieldsetTab3");
		com.comarch.egeria.Utils.update("kartotekaRekrutacjiKandydaciRaport");
		
		User.info("Anonimizacja zakończona.");
	}
	
	public boolean sprawdzCzyEtap1() {
		List<DataRow> tempList = new ArrayList<>();
		for (DataRow row : this.getSql("PPL_REK_KANDYDACI").getData()) {
			if ((!"T".equals(row.get("ka_f_wym_formalne")) && !"X".equals(row.get("ka_f_wym_formalne")))
					|| (!"T".equals(row.get("ka_f_czy_wsk_w_prot")) && !"X".equals(row.get("ka_f_czy_wsk_w_prot")))) {
				tempList.add(row);
			}
		}
        return tempList != null && !tempList.isEmpty();
	}
	
	public boolean sprawdzCzyEtap2() {
		List<DataRow> tempList = new ArrayList<>();
		for (DataRow row : this.getSql("PPL_REK_KANDYDACI").getData()) {
			if (!"T".equals(row.get("ka_f_czy_zatrudniony")) && !"X".equals(row.get("ka_f_czy_zatrudniony"))) {
				tempList.add(row);
			}
		}
        return tempList != null && !tempList.isEmpty();
	}
	
	public void raport(String nazwa_raportu) {
		raport("rtf", nazwa_raportu);
	}
	
	public void raport(String outputType, String nazwa_raportu) {
		if (selectedKartotekaId != null) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("id_naboru", selectedKartotekaId);

				// przygotowanie listy id kandydatow do raportu w postaci id,
				// id,
				// ..., id
				String idString = "";
				Integer liczbaKandydatow=0;
				Integer nrEtapu = 0;
				//List<DataRow> tempList = this.getSql("sqlKandydaciRaport").getSelectedRows();
				List<DataRow> tempList = this.getSql("PPL_REK_KANDYDACI").getSelectedRows();
				if (tempList != null && !tempList.isEmpty()) {
					for (DataRow dr : tempList) {
						idString = idString + dr.getIdAsString() + ", ";
						liczbaKandydatow+=1;
					}

					if (!idString.isEmpty()) {
						idString = idString.substring(0, idString.length() - 2);
					}

					params.put("l_kandydatow", liczbaKandydatow);
					params.put("id_kandydata", idString);
					params.put("nr_etapu", nrEtapu);
					params.put("rodzaj_naboru", this.nabor.getNabRodzajNaboru());
				
					//ReportGenerator.displayReportAsPDF(params, nazwa_raportu);					
					ReportGenerator.displayReport(outputType , nazwa_raportu, params);
			    
					//RequestContext.getCurrentInstance().execute("window.open('KartotekaRekrutacji')");
					
				    wybranyEtap = 0;
				} else{
					User.warn("Należy zaznaczyć przynajmniej jeden wiersz.");
				}
				
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
	}
	
	public void raportEtap1(String outputType, String nazwa_raportu) {
		if (selectedKartotekaId != null) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("id_naboru", selectedKartotekaId);

				// przygotowanie listy id kandydatow do raportu w postaci id,
				// id,
				// ..., id
				String idString = "";
				Integer liczbaKandydatow=0;
				Integer nrEtapu = 1;
				
				List<DataRow> tempList = new ArrayList<>();
				for (DataRow row : this.getSql("PPL_REK_KANDYDACI").getData()) {
					if ((!"T".equals(row.get("ka_f_wym_formalne")) && !"X".equals(row.get("ka_f_wym_formalne")))
							|| (!"T".equals(row.get("ka_f_czy_wsk_w_prot")) && !"X".equals(row.get("ka_f_czy_wsk_w_prot")))) {	
						tempList.add(row);
					}
				}
				
				if (tempList != null && !tempList.isEmpty()) {
					for (DataRow dr : tempList) {
						idString = idString + dr.getIdAsString() + ", ";
						liczbaKandydatow+=1;
					}

					if (!idString.isEmpty()) {
						idString = idString.substring(0, idString.length() - 2);
					}

					params.put("l_kandydatow", liczbaKandydatow);
					params.put("id_kandydata", idString);
					params.put("nr_etapu", nrEtapu);
					params.put("rodzaj_naboru", this.nabor.getNabRodzajNaboru());
					
					ReportGenerator.displayReport(outputType , nazwa_raportu, params);
					
					wybranyEtap = 1;
				} else{
					User.warn("Wszyscy kandydaci spełniają wymagania formalne i zostali wskazani w protokole naboru.");
				}
				
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
	}
	
	public void raportEtap2(String outputType, String nazwa_raportu) {
		if (selectedKartotekaId != null) {
			try {
				HashMap<String, Object> params = new HashMap<String, Object>();
				params.put("id_naboru", selectedKartotekaId);

				// przygotowanie listy id kandydatow do raportu w postaci id,
				// id,
				// ..., id
				String idString = "";
				Integer liczbaKandydatow = 0;
				Integer nrEtapu = 2;
				
				List<DataRow> tempList = new ArrayList<>();
				for (DataRow row : this.getSql("PPL_REK_KANDYDACI").getData()) {
					if (!"T".equals(row.get("ka_f_czy_zatrudniony")) && !"X".equals(row.get("ka_f_czy_zatrudniony"))) {
						tempList.add(row);
					}
				}
				
				if (tempList != null && !tempList.isEmpty()) {
					for (DataRow dr : tempList) {
						idString = idString + dr.getIdAsString() + ", ";
						liczbaKandydatow+=1;
					}

					if (!idString.isEmpty()) {
						idString = idString.substring(0, idString.length() - 2);
					}

					params.put("l_kandydatow", liczbaKandydatow);
					params.put("id_kandydata", idString);
					params.put("nr_etapu", nrEtapu);
					params.put("rodzaj_naboru", this.nabor.getNabRodzajNaboru());
					
					ReportGenerator.displayReport(outputType , nazwa_raportu, params);
				    
					wybranyEtap = 2;
				} else{
					User.warn("Wszyscy kandydaci zostali zatrudnieni.");
				}
				
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
	}
	
	public void raportListaKandydatow(){
		if (selectedKartotekaId != null) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("nab_id", selectedKartotekaId);
			try {
				ReportGenerator.displayReportAsRTF(params, "lista_kandydatow_spelniajacych_wymagania_for");
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
		
	}
	

	public void proceduraNaboru(){
			HashMap<String, Object> params = new HashMap<String, Object>();
			try {
				ReportGenerator.displayReportAsDOCX(params, "procedura_naboru_ksc+ws");
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}		
	}
	
	
	public void raportArkuszOcenyRozmowy(String ka_id, String rapName){		
		if (nabor!= null && nabor.getNabId() != null && ka_id!=null) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("nab_id", nabor.getNabId());
			params.put("ka_id", Long.parseLong(ka_id));
			try {
				ReportGenerator.displayReportAsRTF(params, rapName);
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
		
	}
	
	public void raportKartaOcenyKandydatow(String rapName){		
		if (nabor!= null && nabor.getNabId() != null) {
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("nab_id", nabor.getNabId());
			try {
				ReportGenerator.displayReportAsRTF(params, rapName);
			} catch (JRException | SQLException | IOException e) {
				log.error(e.getMessage(), e);
				User.alert(e.getMessage());
			}
		}
		
	}
	


	// stworzenie listy z set - żeby nie było powtórzonych rekordów przez
	// ui:repeat
	public List listFromSet(Set set) {
		if (set != null) {
			return new ArrayList(set);
		}
		return null;
	}

	public void zapisz() {
//		if (!validatePermissions())
//			return;
		if (!validateBeforeSave())
			return;

		try (HibernateContext h = new HibernateContext()) {
			Session s = h.getSession();

			// przygotowanie danych procentowych do zapisu
			if (nabor.getNabEtatProjekt() != null)
				nabor.setNabEtatProjekt(nabor.getNabEtatProjekt() / 100);

			if (nabor.getNabEtatRezerwa() != null)
				nabor.setNabEtatRezerwa(nabor.getNabEtatRezerwa() / 100);

			// if (nabor.getNabEtatZastepstwo() != null)
			// nabor.setNabEtatZastepstwo(nabor.getNabEtatZastepstwo() / 100);
			//
			// if (nabor.getNabEtatZwyk() != null)
			// nabor.setNabEtatZwyk(nabor.getNabEtatZwyk() / 100);
			nabor = (ZptNabory) s.merge(nabor);
			s.saveOrUpdate(nabor);
			s.beginTransaction().commit();
			s.refresh(nabor);// zmiany z triggerów
			this.nabor.setSqlBean(this);

			User.info("Udany zapis kartoteki rekrutacji [id: %1$s].", this.nabor.getNabId());
		} catch (Exception e) {
			User.alert("Błąd w zapisie kartoteki rekrutacji [id: %1$s]!", this.nabor.getNabId());
			e.printStackTrace();
		}
	}

	private boolean validateBeforeSave() {
		ArrayList<String> inv = new ArrayList<String>();

//		Calendar cal = Calendar.getInstance();

		Date nabDataPublikacji = this.nabor.getNabDataPublikacji();
		Date nabTerminSklOf = this.nabor.getNabTerminSklOf();
		Date nabDataZakonc = this.nabor.getNabDataZakonc();
		Date nabAudytDt = this.nabor.getNabAudytDt();
		if (nabAudytDt==null) nabAudytDt = new Date();
		
		if (       nabDataPublikacji!=null 
				&& nabTerminSklOf != null 
				&& trunc(nabDataPublikacji).after(trunc(nabTerminSklOf)) ){
			inv.add("Data publikacji nie może byc poźniejsza niż termin skladania ofert.");
		}
		
		if (nabTerminSklOf != null && nabTerminSklOf.before(trunc(nabAudytDt))) { 
			inv.add("Termin składania ofert nie może być wcześniejszy od daty bieżącej (a w przypadku reedycji, od daty utworzenia zapisu).");
		}
		
		if (nabDataZakonc!=null && nabDataZakonc.before(trunc(nabAudytDt))) {
			inv.add("Data zakończenia rekrutacji nie może być wcześniejsza od daty bieżącej (a w przypadku reedycji, od daty utworzenia zapisu).");
		}
		
//		if (nabTerminSklOf != null) {
//			if (nabAudytDt != null) {//reedycja wczesniej zapisanej encji
//				if (nabTerminSklOf.before(trunc(nabAudytDt))) {
//					inv.add("Termin składania ofert nie może być wcześniejszy od daty bieżącej");
//				}
//			} else if (nabTerminSklOf.before(trunc(cal.getTime()))) {
//				inv.add("Termin składania ofert nie może być wcześniejszy od daty bieżącej");
//			}
//		}

//		if (this.nabor.getNabDataZakonc() != null) {
//			if (nabAudytDt != null) {
//				if (this.nabor.getNabDataZakonc().before(trunc(nabAudytDt))) {
//					inv.add("Data zakończenia rekrutacji nie może być wcześniejsza od daty bieżącej");
//				}
//			} else if (this.nabor.getNabDataZakonc().before(cal.getTime())) {
//				inv.add("Data zakończenia rekrutacji nie może być wcześniejsza od daty bieżącej");
//			}
//		}
		
		User.alert(inv);

//		for (String msg : inv) {
//			User.alert(msg);
//		}
		return inv.isEmpty();

	}



	public void anonimizuj() {
		
		if("KSC".equals(this.nabor.getNabRodzajNaboru()) ){
		
				if(this.nabor.getNabDataZakonc()==null){
					User.alert("Anonimizacja dla naboru KSC nie jest możliwa przed upływam 3-ech miesięcy od zakończenia naboru!");
					return;
				}
				
				Calendar cal = Calendar.getInstance();
				cal.setTime(this.nabor.getNabDataZakonc()); 
				cal.add(Calendar.MONTH, 3);
				
				if(cal.getTime().after(new Date()) ){
					User.alert("Anonimizacja dla naboru KSC nie jest możliwa przed upływam 3-ech miesięcy od zakończenia naboru!");
					return;
				}		
		}

		try (Connection conn = JdbcUtils.getConnection()) {

			CachedRowSet crs = JdbcUtils.sqlExecQuery(conn,
					"select KA_ID from PPT_REK_KANDYDACI where (KA_F_CZY_ZATRUDNIONY = 'N' or KA_F_WNIOSK_O_ZATR = 'N') and KA_NAB_ID = ?",
					nabor.getNabId());

			if (crs != null) {
				while (crs.next()) {
					JdbcUtils.sqlSPCall(conn, "PPADM.PPP_UTILITIES.anonimizuj_kandydata", crs.getLong("KA_ID"));
				}
			}

		} catch (Exception e) {
			User.alert("Nieudana anonimizacja danych kandydata");
			return;
		}

		
		SqlDataSelectionsHandler sql = this.getSql("PPL_REK_KANDYDACI");
		if (sql!=null)
			sql.setTs(0);

		com.comarch.egeria.Utils.update("frmKartotekaRek:tabViewId:fieldsetTab3");
		com.comarch.egeria.Utils.update("kartotekaRekrutacjiKandydaciRaport");
		
		User.info("Anonimizacja zakończona.");
	}

	public void ogloszenieRaport() {
		try {
			HashMap<String, Object> params = new HashMap<>();
			params.put("nab_id", nabor.getNabId());
			String wydruk = "Ogloszenie_rekrutacja_KSC";
			ReportGenerator.displayReportAsRTF(params, wydruk);
		} catch (JRException | SQLException | IOException e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}

	}

	public Long getSelectedKartotekaId() {
		return selectedKartotekaId;
	}

	public void setSelectedKartotekaId(Long selectedKartotekaId) {
		this.selectedKartotekaId = selectedKartotekaId;
	}

	public ZptNabory getNabor() {
		return nabor;
	}

	public void setNabor(ZptNabory nabor) {
		this.nabor = nabor;
	}

	public int getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
//		com.comarch.egeria.Utils.update("frmKartotekaRek:buttonsId");
	}

	public List<ZptNaborKomisja> getCzlonkowieKomSpozaMF() {
		return czlonkowieKomSpozaMF;
	}

	public String rodzajSkladnika(String rodzaj) {

		SqlDataSelectionsHandler ssh = this.getSql("REK_SKL_RODZAJ");

		if (ssh != null) {
			for (DataRow dr : ssh.getData()) {
				// 0 badz wsl_wartosc
				if (dr.get(0).equals(rodzaj)) {
					// 1 badz wsl_wartosc
					return "" + dr.get(1);
				}
			}
		}

		return rodzaj;
	}
	
	
	public List<ZptNaborSklad> getZptNaborSkladSorted() {
		
		List<ZptNaborSklad> ret = this.nabor.getZptNaborSklads().stream()
				.sorted( (x,y) -> ZptNabory.fRodzajOrder(x.getNskRodzaj()).compareTo( ZptNabory.fRodzajOrder(y.getNskRodzaj())) ) //TODO funkcja do komparowania po rodzajach
				.collect(Collectors.toList());		
	 
	 	return ret;
	}
	
	
/*	public static Integer fRodzajOrder(String rodzaj){
		if (rodzaj==null)
			return 999;
		return "WYNAGR PREMIA_GW PREMIA_UZ DOD_STAZ DOD_SPEC INNE".indexOf(rodzaj);
	}*/

/*	public List<ZptNaborSklad> getWynagrodzenieMiesieczne() {
		// Sortowanie
		Collections.sort(wynagrodzenieMiesieczne, new Comparator<ZptNaborSklad>() {
			@Override
			public int compare(ZptNaborSklad wynagrodzenieMiesieczne1, ZptNaborSklad wynagrodzenieMiesieczne2) {
				return wynagrodzenieMiesieczne2.getNskRodzaj().compareTo(wynagrodzenieMiesieczne1.getNskRodzaj());
			}
		});
		return wynagrodzenieMiesieczne;

	}*/

}