package com.comarch.egeria.pp.Rekrutacja;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.primefaces.context.RequestContext;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.Rekrutacja.model.PptRekAdresy;
import com.comarch.egeria.pp.Rekrutacja.model.ZptKandydaci;
import com.comarch.egeria.pp.Rekrutacja.model.ZptNabory;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.zalaczniki.ZalacznikiBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
//import com.rits.cloning.Cloner;

@ManagedBean
@Named
@Scope("view")
public class KartotekaKandydataBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();

	@Inject
	KartotekaRekrutacjiBean kartotetkaRekrutacji; // jesli potrzebny nam
													// rodzic... to bierzemy
													// calego beana do
													// dyspozycji...

	ZptKandydaci kandydat = null;
	DataRow kartotekaKandydataDR = null;

	PptRekAdresy pptAdresy = new PptRekAdresy();

	String kartotekaZrodlowa = "";
	
//	ZptKandydaci oldKandydat = null;

	Object oldKandydatKaFCzyZatrudniony = null;
	Object oldKandydatKaZpId = null;

	public java.util.Date getMinDate() {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.YEAR, -100);
		return c.getTime();
	}

	public java.util.Date getCurrentDate() {
		Calendar c = Calendar.getInstance();
		return c.getTime();
	}

	public PptRekAdresy getPptAdresy() {
		return pptAdresy;
	}

	public void setPptAdresy(PptRekAdresy pptAdresy) {
		this.pptAdresy = pptAdresy;
	}

	public void wczytajKandydat(DataRow dr, String zrodlo) {

		this.kartotekaKandydataDR = dr;

		kartotekaZrodlowa = zrodlo;

		if (kartotekaKandydataDR == null) {
			// nowa kartoteka kandydata ...
			this.setKandydat(null);
			this.kandydat = new ZptKandydaci();
			this.kandydat.setPptAdresies(new ArrayList<PptRekAdresy>());
			this.kandydat.addPptAdresy(new PptRekAdresy());
			pptAdresy = this.kandydat.getPptAdresies().get(0);
			this.kandydat.setKaFWymFormalne("T");
			this.kandydat.setKaAbsolwent("N");
			this.kandydat.setKaFCzyZatrudniony("N");

		} else {
			// wczytaj istniejacą katrtoteke kandydata
			long id = Long.parseLong("" + dr.getIdAsString());
			try (HibernateContext h = new HibernateContext()) {
				this.setKandydat(h.getSession().get(ZptKandydaci.class, id));

				if (this.kandydat.getPptAdresies() == null || this.kandydat.getPptAdresies().isEmpty()) {
					this.kandydat.setPptAdresies(new ArrayList<PptRekAdresy>());
					this.kandydat.addPptAdresy(new PptRekAdresy());
				}

				pptAdresy = this.kandydat.getPptAdresies().get(0);
			}
		}
		//Cloner cloner = new Cloner();//!!!!!!!! TAK NIE WOLNO:   A fatal error has been detected by the Java Runtime Environment: EXCEPTION_ACCESS_VIOLATION
		//oldKandydat = cloner.deepClone(this.kandydat); !!! TAK NIE WOLNO:   A fatal error has been detected by the Java Runtime Environment: EXCEPTION_ACCESS_VIOLATION
		oldKandydatKaFCzyZatrudniony = this.kandydat.getKaFCzyZatrudniony();
		oldKandydatKaZpId = this.kandydat.getKaZpId();
		com.comarch.egeria.Utils.update("dlgFormKandydat");
	}

	// wykorzystanie setu zamiast listy, poniewaz ui:repeat dubluje wartosci
	// przy wykorzystaniu listy
	public List<ZptKandydaci> listFromSet(Set<ZptKandydaci> set) {
		if (set != null) {

			return new ArrayList<ZptKandydaci>(set);
		}
		return null;
	}

	public void usun(DataRow r, String zrodlo) {
		wczytajKandydat(r, zrodlo);
		usun();
	}

	public void usun() {
		if (Long.valueOf(this.kandydat.getKaId()) != null) {

			try {
				ZalacznikiBean.deleteAll("ZAL_KA_ID", this.kandydat.getKaId());
			} catch (Exception e) {
				log.error(e.getMessage());
				User.alert("Bład przy próbie usunięcia załączników kandydata: ", e.getMessage());
			}

			try{
				ZptNabory zptNabory = this.kandydat.getZptNabory();
				zptNabory.removeZptKandydaci(this.kandydat);//remove orphans = delete
				zptNabory = (ZptNabory) HibernateContext.save(zptNabory);
				this.kartotetkaRekrutacji.setNabor(zptNabory);
				
				com.comarch.egeria.Utils.executeScript("PF('kartotekaRekrutacjiKandydat').hide();");

				if (this.kartotetkaRekrutacji != null && kartotekaZrodlowa.equals("NAB"))
					this.kartotetkaRekrutacji.getSql("PPL_REK_KANDYDACI").setTs(0); // wymuszenie
																					// przeladowania
																					// danych
																					// jdbc
																					// (RDBMS->SqlBean...)


				com.comarch.egeria.Utils.update("kartotekaRekrutacjiKandydaciRaport");
			} catch (Exception e) {
				log.error(e.getMessage());
				User.alert("Bład przy próbie usunięcia kadydata");
			}
		}
	}

	public void zapisz() {

		if (!validatePermissions())
			return;

		if (!validateBeforeSave())
			return;

		// jesli nowe dziecko, to trzeba dodac go do rodzica (na razie tutaj a
		// nie przy tworzeniu subencji, bo mozliwe, ze user kliknie w anuluj...)
		if (this.kandydat.getKaId() == null || this.kandydat.getKaId() == 0L) {

			if (kartotekaZrodlowa.equals("NAB")) {
				this.kartotetkaRekrutacji.getNabor().addZptKandydaci(this.kandydat);
				this.kandydat.setKaNabRodzaj(this.kartotetkaRekrutacji.getNabor().getNabRodzajNaboru());
			}
		}

		pptAdresy.setAdrTyp("S");
		pptAdresy.setAdrLp(BigDecimal.ONE);
		pptAdresy.setAdrZatwierdzony("T");
		pptAdresy.setAdrFAktualne("T");

		this.kandydat.addPptAdresy(pptAdresy);

		// jako numer przypisywane jest id - za pomoca triggera w bazie
		if (this.kandydat.getKaNumer() == null) {
			this.kandydat.setKaNumer(Long.valueOf(0));
		}

		if (("" + this.kandydat.getKaFWymFormalne()).equals("T") && this.kandydat.getKaNiespWfPrzyczyna() != null)
			this.kandydat.setKaNiespWfPrzyczyna(null);

		
		kandydat.setKaAudytKm("" + (new java.util.Date().getTime()) );
		
		try{
			this.kandydat = (ZptKandydaci) HibernateContext.save(this.kandydat);
			this.kartotetkaRekrutacji.setNabor((ZptNabory) HibernateContext.refresh( this.kandydat.getZptNabory()));
			
			if (!("" + oldKandydatKaFCzyZatrudniony).equals("T") && ("" + this.kandydat.getKaFCzyZatrudniony()).equals("T")  && oldKandydatKaZpId == null) {
				try (Connection conn = JdbcUtils.getConnection()) {
					JdbcUtils.sqlSPCall(conn, "PPP_UTILITIES.przepisz_kandydata", this.kandydat.getKaId());
				} catch (Exception e) {
					User.alert("Nieudany zapis kandydata do tabel egeriowych");
					log.error(e.getMessage(), e);
					return;
				}
			}

			User.info("Zapisano kartotekę kandydata.");

	
			com.comarch.egeria.Utils.executeScript("PF('kartotekaRekrutacjiKandydat').hide();");
	
			if (this.kartotetkaRekrutacji != null && kartotekaZrodlowa.equals("NAB"))
				this.kartotetkaRekrutacji.getSql("PPL_REK_KANDYDACI").setTs(0); // wymuszenie
																				// przeladowania
																				// danych
																				// jdbc
																				// (RDBMS->SqlBean...)
	

			com.comarch.egeria.Utils.update("kartotekaRekrutacjiKandydaciRaport");

		} catch (Exception e) {
			log.error(e.getMessage());
			User.alert("Bład przy próbie dodania kadydata");
		}
		
	}

	private boolean validateBeforeSave() {
		ArrayList<String> inv = new ArrayList<String>();

		if (this.kandydat.getKaImie() == null || kandydat.getKaImie().isEmpty())
			inv.add("Imię jest polem wymaganym.");

		if (this.kandydat.getKaNazwisko() == null || kandydat.getKaNazwisko().isEmpty())
			inv.add("Nazwisko jest polem wymaganym.");

		if (this.kandydat.getKaDataUr() == null)
			inv.add("Data urodzenia jest polem wymaganym.");

		if (this.kandydat.getKaPlec() == null || kandydat.getKaPlec().isEmpty())
			inv.add("Płeć jest polem wymaganym.");

		if (this.kandydat.getPptAdresies() != null) {
			if (pptAdresy.getAdrKrId() == null) {
				inv.add("Kraj jest wymagany.");
			}

			if (pptAdresy.getAdrMiejscowosc() == null || pptAdresy.getAdrMiejscowosc().equals("")) {
				inv.add("Miejscowosc jest wymagana");
			}

			if (pptAdresy.getAdrKodPocztowy() != null && !pptAdresy.getAdrKodPocztowy().equals("")) {
				if (!pptAdresy.getAdrKodPocztowy().contains("-")) {
					User.warn("Kod pocztowy powinien zawierać '-'");
				}
			}

		} else {
			inv.add("Adres jest wymagany.");
		}

		if (!this.kandydat.getKaFWymFormalne().equals("T")) {
			if (this.kandydat.getKaNiespWfPrzyczyna() != null && this.kandydat.getKaNiespWfPrzyczyna().length() < 0) {
				inv.add("W przypadku niespełnienia wymagania należy podać jego przyczynę");
			}
		}
		List<ZptKandydaci> zptKandydacis = null;
		if (kartotekaZrodlowa.equals("NAB"))
			//zptKandydacis = listFromSet(this.kartotetkaRekrutacji.getNabor().getZptKandydacis());
			zptKandydacis = this.kartotetkaRekrutacji.getNabor().getZptKandydacis();
			
		if (zptKandydacis != null)
			for (ZptKandydaci k : zptKandydacis) {
				if (kandydat.getKaId() != k.getKaId())
					if (kandydat.getKaNumer() == k.getKaNumer()) {
						inv.add("Numer kandydata musi być unikalny. Spróbuj dodać kandydata ponownie.");
						break;
					}
			}

		for (String msg : inv) {
			User.alert(msg);
		}
		return inv.isEmpty();

	}

	private Boolean validatePermissions() {
		ArrayList<String> inval = new ArrayList<String>();

		/*
		 * //test czy są zmiany String currentCheckString_CzI =
		 * this.getCheckString_CzI(); String currentCheckString_CzII =
		 * this.getCheckString_CzII(); String currentCheckString_CzIII =
		 * this.getCheckString_CzIII(); String currentCheckString_CzV =
		 * this.getCheckString_CzV(); String currentCheckString_CzVI =
		 * this.getCheckString_CzVI();
		 * 
		 * System.out.println("checkpart 1"); System.out.println(checkCzI);
		 * System.out.println(currentCheckString_CzI);
		 * System.out.println("checkpart 2"); System.out.println(checkCzII);
		 * System.out.println(currentCheckString_CzII);
		 * System.out.println("checkpart 3"); System.out.println(checkCzIII);
		 * System.out.println(currentCheckString_CzIII);
		 * System.out.println("checkpart 5"); System.out.println(checkCzV);
		 * System.out.println(currentCheckString_CzV);
		 * System.out.println("checkpart 6"); System.out.println(checkCzVI);
		 * System.out.println(currentCheckString_CzVI);
		 * 
		 * 
		 * if ( this.checkCzI.equals( currentCheckString_CzI) &&
		 * this.checkCzII.equals( currentCheckString_CzII) &&
		 * this.checkCzIII.equals( currentCheckString_CzIII) &&
		 * this.checkCzV.equals( currentCheckString_CzV) &&
		 * this.checkCzVI.equals( currentCheckString_CzVI))
		 * inval.add("Brak zmian do zapisania.");
		 */
		/*
		 * if (!user.allowCreateOcenaOkresowa() && this.selectedOecenaId ==
		 * null) inval.add("Nieautoryzowana próba utworzenia nowej oceny.");
		 * 
		 * if (!this.allowUpdateCzI() && !this.checkCzI.equals(
		 * currentCheckString_CzI )) inval.
		 * add("Nieautoryzowana próba modyfikacji zawartości części I oceny.");
		 * 
		 * if (!this.allowUpdateCzII() && !this.checkCzII.equals(
		 * currentCheckString_CzII )) inval.
		 * add("Nieautoryzowana próba modyfikacji zawartości części II oceny.");
		 */

		for (String txt : inval) {
			User.alert(txt);
		}
		return inval.isEmpty();
	}

	public boolean czySaWakaty() {
		int zatrudnieni = 0;
		int liczbaWakatow;
		for (DataRow row : this.kartotetkaRekrutacji.getSql("PPL_REK_KANDYDACI").getData()) {
			if (row.get("ka_f_czy_zatrudniony").equals("T")) {
				zatrudnieni++;
			}
		}
		
		if (this.kartotetkaRekrutacji.getNabor().getNabLiczbaWakatow() != null) {
			liczbaWakatow = this.kartotetkaRekrutacji.getNabor().getNabLiczbaWakatow().intValue();
            return zatrudnieni < liczbaWakatow;
		} else {
			return true;
		}
	}

	public ZptKandydaci getKandydat() {
		return kandydat;
	}

	public void setKandydat(ZptKandydaci kandydat) {
		this.kandydat = kandydat;
	}

}
