package com.comarch.egeria.pp.Rekrutacja.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="PPT_REK_KANDYDAT_WAMAGANIA")
@NamedQuery(name="PptRekKandydatWamagania.findAll", query="SELECT p FROM PptRekKandydatWamagania p")
public class PptRekKandydatWamagania implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_REK_KANDYDAT_WAMAGANIA",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_REK_KANDYDAT_WAMAGANIA", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_REK_KANDYDAT_WAMAGANIA")
	@Column(name="KAWY_ID")
	private long kawyId;

	@Column(name="KAWY_AUDYT_DM")
	@Temporal(TemporalType.DATE)
	private Date kawyAudytDm;

	@Column(name="KAWY_AUDYT_DT")
	@Temporal(TemporalType.DATE)
	private Date kawyAudytDt;

	@Column(name="KAWY_AUDYT_KM")
	private String kawyAudytKm;

	@Column(name="KAWY_AUDYT_LM")
	private String kawyAudytLm;

	@Column(name="KAWY_AUDYT_UM")
	private String kawyAudytUm;

	@Column(name="KAWY_AUDYT_UT")
	private String kawyAudytUt;

	@Column(name="KAWY_F_NIEZBEDNE")
	private String kawyFNiezbedne;

	@Column(name="KAWY_LICZBA_PKT")
	private BigDecimal kawyLiczbaPkt;

	@Column(name="KAWY_WYMAGANIE")
	private String kawyWymaganie;

	//bi-directional many-to-one association to PptRekKandydaci
	@ManyToOne
	@JoinColumn(name="KAWY_KA_ID")
	private ZptKandydaci pptRekKandydaci;

	public PptRekKandydatWamagania() {
	}

	public long getKawyId() {
		return this.kawyId;
	}

	public void setKawyId(long kawyId) {
		this.kawyId = kawyId;
	}

	public Object getKawyAudytDm() {
		return this.kawyAudytDm;
	}

	public void setKawyAudytDm(Date kawyAudytDm) {
		this.kawyAudytDm = kawyAudytDm;
	}

	public Object getKawyAudytDt() {
		return this.kawyAudytDt;
	}

	public void setKawyAudytDt(Date kawyAudytDt) {
		this.kawyAudytDt = kawyAudytDt;
	}

	public String getKawyAudytKm() {
		return this.kawyAudytKm;
	}

	public void setKawyAudytKm(String kawyAudytKm) {
		this.kawyAudytKm = kawyAudytKm;
	}

	public String getKawyAudytLm() {
		return this.kawyAudytLm;
	}

	public void setKawyAudytLm(String kawyAudytLm) {
		this.kawyAudytLm = kawyAudytLm;
	}

	public String getKawyAudytUm() {
		return this.kawyAudytUm;
	}

	public void setKawyAudytUm(String kawyAudytUm) {
		this.kawyAudytUm = kawyAudytUm;
	}

	public String getKawyAudytUt() {
		return this.kawyAudytUt;
	}

	public void setKawyAudytUt(String kawyAudytUt) {
		this.kawyAudytUt = kawyAudytUt;
	}

	public String getKawyFNiezbedne() {
		return this.kawyFNiezbedne;
	}

	public void setKawyFNiezbedne(String kawyFNiezbedne) {
		this.kawyFNiezbedne = kawyFNiezbedne;
	}

	public BigDecimal getKawyLiczbaPkt() {
		return this.kawyLiczbaPkt;
	}

	public void setKawyLiczbaPkt(BigDecimal kawyLiczbaPkt) {
		this.kawyLiczbaPkt = kawyLiczbaPkt;
	}

	public String getKawyWymaganie() {
		return this.kawyWymaganie;
	}

	public void setKawyWymaganie(String kawyWymaganie) {
		this.kawyWymaganie = kawyWymaganie;
	}

	public ZptKandydaci getPptRekKandydaci() {
		return this.pptRekKandydaci;
	}

	public void setPptRekKandydaci(ZptKandydaci pptRekKandydaci) {
		this.pptRekKandydaci = pptRekKandydaci;
	}

}
