package com.comarch.egeria.pp.Rekrutacja.model;

import static com.comarch.egeria.Utils.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.jfree.util.Log;

import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.pp.data.model.ModelBase;
import com.comarch.egeria.web.User;


/**
 * The persistent class for the PPT_REK_NABORY database table.
 *
 */
@Entity
@Table(name="PPT_REK_NABORY", schema="PPADM")
@NamedQuery(name="ZptNabory.findAll", query="SELECT z FROM ZptNabory z")
public class ZptNabory extends ModelBase implements Serializable {
	private static final long serialVersionUID = 1L;

//	@Transient
//	private SqlBean sqlBean;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_REK_NABORY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_REK_NABORY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_REK_NABORY")
	@Column(name="NAB_ID")
	private Long nabId;

	@Temporal(TemporalType.DATE)
	@Column(name="NAB_AUDYT_DM")
	private Date nabAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="NAB_AUDYT_DT")
	private Date nabAudytDt;

	@Column(name="NAB_AUDYT_KM")
	private String nabAudytKm;

	@Version
	@Column(name="NAB_AUDYT_LM")
	private Long nabAudytLm;

	@Column(name="NAB_AUDYT_UM")
	private String nabAudytUm;

	@Column(name="NAB_AUDYT_UT")
	private String nabAudytUt;

	@Column(name="NAB_CZY_INF_CDZ")
	private String nabCzyInfCdz;

	@Column(name="NAB_CZY_INF_NIEPELN")
	private String nabCzyInfNiepeln;

	@Column(name="NAB_CZY_INF_WSK_NIEPELN")
	private String nabCzyInfWskNiepeln;

	@Temporal(TemporalType.DATE)
	@Column(name="NAB_DATA_PUBLIKACJI")
	private Date nabDataPublikacji;

	@Temporal(TemporalType.DATE)
	@Column(name="NAB_DATA_WOLNE_OD")
	private Date nabDataWolneOd;

	@Temporal(TemporalType.DATE)
	@Column(name="NAB_DATA_ZAKONC")
	private Date nabDataZakonc;

	@Column(name="NAB_ETAP")
	private String nabEtap;

	@Column(name="NAB_ETAT_PROJEKT")
	private Double nabEtatProjekt;

	@Column(name="NAB_ETAT_REZERWA")
	private Double nabEtatRezerwa;

	@Column(name="NAB_ETAT_Z_FUND_WYNAGR")
	private Double nabEtatZFundWynagr;

	@Column(name="NAB_ETAT_REZERWA_TYP")
	private String nabEtatRezerwaTyp;

	@Column(name="NAB_PROJ_OB_ID")
	private Long nabProjObId;

	@Column(name="NAB_ETAT_ZASTEPSTWO")
	private String nabEtatZastepstwo;

	@Column(name="NAB_ETAT_ZWYK")
	private String nabEtatZwyk;

	@Column(name="NAB_LICZBA_WAKATOW")
	private Long nabLiczbaWakatow;

	@Column(name="NAB_MNOZNIK_MAX")
	private Double nabMnoznikMax;

	@Column(name="NAB_MNOZNIK_MIN")
	private Double nabMnoznikMin;

	@Column(name="NAB_MNOZNIK_MAX_AKC")
	private Double nabMnoznikMaxAkc;

	@Column(name="NAB_MNOZNIK_MIN_AKC")
	private Double nabMnoznikMinAkc;

	@Column(name="NAB_NR_OFERTY_PRC")
	private String nabNrOfertyPrc;

	@Column(name="NAB_NR_OGL_BIP")
	private Long nabNrOglBip;

	@Column(name="NAB_OB_ID")
	private Long nabObId;

	@Column(name="NAB_OG_KONTAKT")
	private String nabOgKontakt;

	@Temporal(TemporalType.DATE)
	@Column(name="NAB_OG_TERMIN_ROZM")
	private Date nabOgTerminRozm;

	@Temporal(TemporalType.DATE)
	@Column(name="NAB_OG_TERMIN_ZGL_EMAIL")
	private Date nabOgTerminZglEmail;

	@Column(name="NAB_OG_WYM_DOK")
	private String nabOgWymDok;

	@Column(name="NAB_OG_WYMAGANE_WYKSZ")
	private String nabOgWymaganeWyksz;

	@Column(name="NAB_OG_WYMAGANIA_DODAT")
	private String nabOgWymaganiaDodat;

	@Column(name="NAB_OG_WYMAGANIA_NIEZB")
	private String nabOgWymaganiaNiezb;

	@Column(name="NAB_OG_ZAKRES_OBOW")
	private String nabOgZakresObow;

	@Column(name="NAB_RODZAJ_NABORU")
	private String nabRodzajNaboru;

	@Column(name="NAB_RODZAJ_PROP_UMOWY")
	private String nabRodzajPropUmowy;

	@Column(name="NAB_STJO_ID")
	private Long nabStjoId;

	@Column(name="NAB_STN_ID")
	private Long nabStnId;

	@Temporal(TemporalType.DATE)
	@Column(name="NAB_TERMIN_REALIZACJI")
	private Date nabTerminRealizacji;

	@Temporal(TemporalType.DATE)
	@Column(name="NAB_TERMIN_SKL_OF")
	private Date nabTerminSklOf;

	@Column(name="NAB_UWAGI")
	private String nabUwagi;

	@Column(name="NAB_UZASADNIENIE")
	private String nabUzasadnienie;

	@Column(name="NAB_WAKAT_POTW")
	private String nabWakatPotw;

	@Column(name="NAB_WYMIAR_ETATU")
	private String nabWymiarEtatu;

	@Column(name="NAB_WYNIK")
	private String nabWynik;

	@Column(name="NAB_ZRODLO_FIN_ETAT")
	private String nabZrodloFinEtat;

	@Column(name="NAB_WYDZIAL_ID")
	private Long nabWydzialId;

	@Column(name="NAB_OG_TELEFON")
	private String nabOgTelefon;

	@Column(name="NAB_OG_EMAIL")
	private String nabOgEmail;

	//bi-directional many-to-one association to ZptKandydaci
	@OneToMany(mappedBy="zptNabory", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ZptKandydaci> zptKandydacis  = new ArrayList<>();;

	//bi-directional many-to-one association to ZptNaborKomisja
	@OneToMany(mappedBy="zptNabory", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ZptNaborKomisja> zptNaborKomisjas = new ArrayList<>();

	//bi-directional many-to-one association to ZptNaborSklad
	@OneToMany(mappedBy="zptNabory", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<ZptNaborSklad> zptNaborSklads  = new ArrayList<>();

	//bi-directional many-to-one association to ZptKandydaci
	@OneToMany(mappedBy="zptNabory", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("NSM_ID ASC")
	private List<ZptNabSymb> zptNabSymbs = new ArrayList<>();

	//bi-directional many-to-one association to ZptKandydaci
	@OneToMany(mappedBy="zptNabory", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("NAP_ID ASC")
	private List<PptRekProjekty> pptRekProjekties = new ArrayList<>();

	// bi-directional many-to-one association to ZptKandydaci
	@OneToMany(mappedBy = "zptNabory", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("NARC_ID ASC")
	private List<PptRekRezerwyCelowe> pptRekRezerwyCelowe = new ArrayList<>();

	// bi-directional many-to-one association to PptRekEtapy
	@OneToMany(mappedBy = "zptNabory", cascade={CascadeType.ALL}, orphanRemoval=true)
	@LazyCollection(LazyCollectionOption.FALSE)
	@OrderBy("NABE_ID ASC")
	private List<PptRekEtapy> pptRekEtapy = new ArrayList<>();

	public ZptNabory() {
	}

	public Long getNabId() {
		return this.nabId;
	}

	public void setNabId(Long nabId) {
		this.nabId = nabId;
	}

	public Date getNabAudytDm() {
		return this.nabAudytDm;
	}

	public void setNabAudytDm(Date nabAudytDm) {
		this.nabAudytDm = nabAudytDm;
	}

	public Date getNabAudytDt() {
		return this.nabAudytDt;
	}

	public void setNabAudytDt(Date nabAudytDt) {
		this.nabAudytDt = nabAudytDt;
	}

	public String getNabAudytKm() {
		return this.nabAudytKm;
	}

	public void setNabAudytKm(String nabAudytKm) {
		this.nabAudytKm = nabAudytKm;
	}

	public Long getNabAudytLm() {
		return this.nabAudytLm;
	}

	public void setNabAudytLm(Long nabAudytLm) {
		this.nabAudytLm = nabAudytLm;
	}

	public String getNabAudytUm() {
		return this.nabAudytUm;
	}

	public void setNabAudytUm(String nabAudytUm) {
		this.nabAudytUm = nabAudytUm;
	}

	public String getNabAudytUt() {
		return this.nabAudytUt;
	}

	public void setNabAudytUt(String nabAudytUt) {
		this.nabAudytUt = nabAudytUt;
	}

	public String getNabCzyInfCdz() {
		return this.nabCzyInfCdz;
	}

	public void setNabCzyInfCdz(String nabCzyInfCdz) {
		this.nabCzyInfCdz = nabCzyInfCdz;
	}

	public String getNabCzyInfNiepeln() {
		return this.nabCzyInfNiepeln;
	}

	public void setNabCzyInfNiepeln(String nabCzyInfNiepeln) {
		this.nabCzyInfNiepeln = nabCzyInfNiepeln;
	}

	public String getNabCzyInfWskNiepeln() {
		return this.nabCzyInfWskNiepeln;
	}

	public void setNabCzyInfWskNiepeln(String nabCzyInfWskNiepeln) {
		this.nabCzyInfWskNiepeln = nabCzyInfWskNiepeln;
	}

	public Date getNabDataPublikacji() {
		return this.nabDataPublikacji;
	}

	public void setNabDataPublikacji(Date nabDataPublikacji) {
		this.nabDataPublikacji = nabDataPublikacji;
	}

	public Date getNabDataWolneOd() {
		return this.nabDataWolneOd;
	}

	public void setNabDataWolneOd(Date nabDataWolneOd) {
		this.nabDataWolneOd = nabDataWolneOd;
	}

	public Date getNabDataZakonc() {
		return this.nabDataZakonc;
	}

	public void setNabDataZakonc(Date nabDataZakonc) {
		this.nabDataZakonc = nabDataZakonc;
	}

	public String getNabEtap() {
		return this.nabEtap;
	}

	public void setNabEtap(String nabEtap) {
		this.nabEtap = nabEtap;
	}

	public Double getNabEtatProjekt() {
		return this.nabEtatProjekt;
	}

	public void setNabEtatProjekt(Double nabEtatProjekt) {
		this.nabEtatProjekt = nabEtatProjekt;
	}

	public Double getNabEtatRezerwa() {
		return this.nabEtatRezerwa;
	}

	public void setNabEtatRezerwa(Double nabEtatRezerwa) {
		this.nabEtatRezerwa = nabEtatRezerwa;
	}

	public Double getNabEtatZFundWynagr() {
		return nabEtatZFundWynagr;
	}

	public String getNabEtatRezerwaTyp() {
		return nabEtatRezerwaTyp;
	}

	public void setNabEtatRezerwaTyp(String nabEtatRezerwaTyp) {
		this.nabEtatRezerwaTyp = nabEtatRezerwaTyp;
	}

	public Long getnabProjObId() {
		return nabProjObId;
	}

	public void setnabProjObId(Long nabProjObId) {
		this.nabProjObId = nabProjObId;
	}

	public void setNabEtatZFundWynagr(Double nabEtatZFundWynagr) {
		this.nabEtatZFundWynagr = nabEtatZFundWynagr;
	}

	public String getNabEtatZastepstwo() {
		return this.nabEtatZastepstwo;
	}

	public void setNabEtatZastepstwo(String nabEtatZastepstwo) {
		this.nabEtatZastepstwo = nabEtatZastepstwo;
	}

	public String getNabEtatZwyk() {
		return this.nabEtatZwyk;
	}

	public void setNabEtatZwyk(String nabEtatZwyk) {
		this.nabEtatZwyk = nabEtatZwyk;
	}

	public Long getNabLiczbaWakatow() {
		return this.nabLiczbaWakatow;
	}

	public void setNabLiczbaWakatow(Long nabLiczbaWakatow) {
		this.nabLiczbaWakatow = nabLiczbaWakatow;
	}

	public Double getNabMnoznikMax() {
		return this.nabMnoznikMax;
	}

	public void setNabMnoznikMax(Double nabMnoznikMax) {
		this.nabMnoznikMax = nabMnoznikMax;
	}

	public Double getNabMnoznikMin() {
		return this.nabMnoznikMin;
	}

	public void setNabMnoznikMin(Double nabMnoznikMin) {
		this.nabMnoznikMin = nabMnoznikMin;
	}

	public Double getNabMnoznikMaxAkc() {
		return nabMnoznikMaxAkc;
	}

	public void setNabMnoznikMaxAkc(Double nabMnoznikMaxAkc) {
		this.nabMnoznikMaxAkc = nabMnoznikMaxAkc;
	}

	public Double getNabMnoznikMinAkc() {
		return nabMnoznikMinAkc;
	}

	public void setNabMnoznikMinAkc(Double nabMnoznikMinAkc) {
		this.nabMnoznikMinAkc = nabMnoznikMinAkc;
	}

	public String getNabNrOfertyPrc() {
		return this.nabNrOfertyPrc;
	}

	public void setNabNrOfertyPrc(String nabNrOfertyPrc) {
		this.nabNrOfertyPrc = nabNrOfertyPrc;
	}

	public Long getNabNrOglBip() {
		return this.nabNrOglBip;
	}

	public void setNabNrOglBip(Long nabNrOglBip) {
		this.nabNrOglBip = nabNrOglBip;
	}

	public Long getNabObId() {
		return this.nabObId;
	}

	public void setNabObId(Long nabObId) {
		if (!eq(this.nabObId,nabObId)){
			this.nabWydzialId = null;
			this.nabStnId = null;
			this.nabStjoId = null;
		}
		this.nabObId = nabObId;
		//if(!(nabWydzialId==null || nabWydzialId==0L)) this.setNabWydzialId(null);
	}

	public String getNabOgKontakt() {
		return this.nabOgKontakt;
	}

	public void setNabOgKontakt(String nabOgKontakt) {
		this.nabOgKontakt = nabOgKontakt;
	}

	public Date getNabOgTerminRozm() {
		return this.nabOgTerminRozm;
	}

	public void setNabOgTerminRozm(Date nabOgTerminRozm) {
		this.nabOgTerminRozm = nabOgTerminRozm;
	}

	public Date getNabOgTerminZglEmail() {
		return this.nabOgTerminZglEmail;
	}

	public void setNabOgTerminZglEmail(Date nabOgTerminZglEmail) {
		this.nabOgTerminZglEmail = nabOgTerminZglEmail;
	}

	public String getNabOgWymDok() {
		return this.nabOgWymDok;
	}

	public void setNabOgWymDok(String nabOgWymDok) {
		this.nabOgWymDok = nabOgWymDok;
	}

	public String getNabOgWymaganeWyksz() {
		return this.nabOgWymaganeWyksz;
	}

	public void setNabOgWymaganeWyksz(String nabOgWymaganeWyksz) {
		this.nabOgWymaganeWyksz = nabOgWymaganeWyksz;
	}

	public String getNabOgWymaganiaDodat() {
		return this.nabOgWymaganiaDodat;
	}

	public void setNabOgWymaganiaDodat(String nabOgWymaganiaDodat) {
		this.nabOgWymaganiaDodat = nabOgWymaganiaDodat;
	}

	public String getNabOgWymaganiaNiezb() {
		return this.nabOgWymaganiaNiezb;
	}

	public void setNabOgWymaganiaNiezb(String nabOgWymaganiaNiezb) {
		this.nabOgWymaganiaNiezb = nabOgWymaganiaNiezb;
	}

	public String getNabOgZakresObow() {
		return this.nabOgZakresObow;
	}

	public void setNabOgZakresObow(String nabOgZakresObow) {
		this.nabOgZakresObow = nabOgZakresObow;
	}

	public String getNabRodzajNaboru() {
		return this.nabRodzajNaboru;
	}

	public void setNabRodzajNaboru(String nabRodzajNaboru) {
		this.nabRodzajNaboru = nabRodzajNaboru;
	}

	public String getNabRodzajPropUmowy() {
		return this.nabRodzajPropUmowy;
	}

	public void setNabRodzajPropUmowy(String nabRodzajPropUmowy) {
		this.nabRodzajPropUmowy = nabRodzajPropUmowy;
	}

	public Long getNabStjoId() {
		return this.nabStjoId;
	}


	public SqlDataSelectionsHandler getLwStanowiska(){
		SqlDataSelectionsHandler sl = getLW("PPL_SLOWNIK", "REK_RODZAJ");
		if (sl==null) return null;

		DataRow slr = sl.find(this.nabRodzajNaboru);
		if (slr!=null){
			String lwName = ""+ slr.get("wsl_wartosc_zewn");
			return this.getLW(lwName);
		}

		return null;
	}


	public void setNabStjoId(Long nabStjoId) {
		Long oldVal = this.nabStjoId;
		Long newVal = nabStjoId;
		this.nabStjoId = nabStjoId;

		if (!eq(oldVal, newVal)){
			SqlDataSelectionsHandler lw =  getLwStanowiska();

			if (lw !=null) {
				DataRow r = lw.find(nabStjoId);
				if (r!=null){
					this.nabWydzialId = r.getLong("pp_ob_id");
					this.nabStnId = r.getLong("pp_stn_id");
				}
			}
		}
	}

	public double getSumaProcentowaProjektow() {
		return pptRekProjekties.stream().mapToDouble(proj -> nz(proj.getNapEtatProjekt())).sum();
	}

	public double getSumaProcentowaRezerw() {
		return pptRekRezerwyCelowe.stream().mapToDouble(rez -> nz(rez.getNarcRezerwaEtat())).sum();
	}

	public Long getNabStnId() {
		return this.nabStnId;
	}

	public void setNabStnId(Long nabStnId) {
		this.nabStnId = nabStnId;
	}

	public Date getNabTerminRealizacji() {
		return this.nabTerminRealizacji;
	}

	public void setNabTerminRealizacji(Date nabTerminRealizacji) {
		this.nabTerminRealizacji = nabTerminRealizacji;
	}

	public Date getNabTerminSklOf() {
		return this.nabTerminSklOf;
	}

	public void setNabTerminSklOf(Date nabTerminSklOf) {
		boolean changed  = !SqlBean.eq(this.nabTerminSklOf, nabTerminSklOf);

		this.nabTerminSklOf = nabTerminSklOf;
	}

	public String getNabUwagi() {
		return this.nabUwagi;
	}

	public void setNabUwagi(String nabUwagi) {
		this.nabUwagi = nabUwagi;
	}

	public String getNabUzasadnienie() {
		return this.nabUzasadnienie;
	}

	public void setNabUzasadnienie(String nabUzasadnienie) {
		this.nabUzasadnienie = nabUzasadnienie;
	}

	public String getNabWakatPotw() {
		return this.nabWakatPotw;
	}

	public void setNabWakatPotw(String nabWakatPotw) {
		this.nabWakatPotw = nabWakatPotw;
	}

	public String getNabWymiarEtatu() {
		return this.nabWymiarEtatu;
	}

	public void setNabWymiarEtatu(String nabWymiarEtatu) {
		this.nabWymiarEtatu = nabWymiarEtatu;
	}

	public String getNabWynik() {
		return this.nabWynik;
	}

	public void setNabWynik(String nabWynik) {
		this.nabWynik = nabWynik;
	}

	public String getNabZrodloFinEtat() {
		return this.nabZrodloFinEtat;
	}

	public void setNabZrodloFinEtat(String nabZrodloFinEtat) {
		this.nabZrodloFinEtat = nabZrodloFinEtat;
	}

	public List<ZptKandydaci> getZptKandydacis() {
		return this.zptKandydacis;
	}

	public void setZptKandydacis(List<ZptKandydaci> zptKandydacis) {
		this.zptKandydacis = zptKandydacis;
	}

	public ZptKandydaci addZptKandydaci(ZptKandydaci zptKandydaci) {
		getZptKandydacis().add(zptKandydaci);
		zptKandydaci.setZptNabory(this);

		return zptKandydaci;
	}

	public ZptKandydaci removeZptKandydaci(ZptKandydaci zptKandydaci) {
		getZptKandydacis().remove(zptKandydaci);
		zptKandydaci.setZptNabory(null);

		return zptKandydaci;
	}

	public List<ZptNaborKomisja> getZptNaborKomisjas() {
		return this.zptNaborKomisjas;
	}


	public List<ZptNaborKomisja> getZptNaborKomisjasNiszczeniaOfert() {
		List<ZptNaborKomisja> ret = this.zptNaborKomisjas.stream().filter(k -> "O".equals(k.getKomTyp()) ).collect(Collectors.toList());
		return ret;
	}

	public void setZptNaborKomisjas(List<ZptNaborKomisja> zptNaborKomisjas) {
		this.zptNaborKomisjas = zptNaborKomisjas;
	}

	public ZptNaborKomisja addZptNaborKomisja(ZptNaborKomisja zptNaborKomisja) {
		getZptNaborKomisjas().add(zptNaborKomisja);
		zptNaborKomisja.setZptNabory(this);

		return zptNaborKomisja;
	}

	public ZptNaborKomisja removeZptNaborKomisja(ZptNaborKomisja zptNaborKomisja) {
		getZptNaborKomisjas().remove(zptNaborKomisja);
		zptNaborKomisja.setZptNabory(null);

		return zptNaborKomisja;
	}

	public List<ZptNaborSklad> getZptNaborSklads() {
		return this.zptNaborSklads;
	}

	public void setZptNaborSklads(List<ZptNaborSklad> zptNaborSklads) {
		this.zptNaborSklads = zptNaborSklads;
	}

	public ZptNaborSklad addZptNaborSklad(ZptNaborSklad zptNaborSklad) {
		getZptNaborSklads().add(zptNaborSklad);
		zptNaborSklad.setZptNabory(this);

		return zptNaborSklad;
	}

	public ZptNaborSklad removeZptNaborSklad(ZptNaborSklad zptNaborSklad) {
		getZptNaborSklads().remove(zptNaborSklad);
		zptNaborSklad.setZptNabory(null);

		return zptNaborSklad;
	}

	public List<ZptNabSymb> getZptNabSymbs() {
		return this.zptNabSymbs;
	}

	public void setZptNabSymb(List<ZptNabSymb> zptNabSymbs) {
		this.zptNabSymbs = zptNabSymbs;
	}

	public ZptNabSymb addZptNabSymb(ZptNabSymb zptNabSymb) {
		getZptNabSymbs().add(zptNabSymb);
		zptNabSymb.setZptNabory(this);

		return zptNabSymb;
	}

	public ZptNabSymb removeZptNabSymb(ZptNabSymb zptNabSymb) {
		getZptNabSymbs().remove(zptNabSymb);
		zptNabSymb.setZptNabory(null);

		return zptNabSymb;
	}

	public Long getNabWydzialId() {
		return nabWydzialId;
	}

	public void setNabWydzialId(Long nabWydzialId) {
		if (!eq(this.nabWydzialId, nabWydzialId)){
			this.nabStnId = null;
			this.nabStjoId = null;
		}
		this.nabWydzialId = nabWydzialId;

		//if(!(nabStnId==null || nabStnId==0L)) this.setNabStnId(null);
	}

	public List<PptRekProjekty> getPptRekProjekties() {
		return pptRekProjekties;
	}

	public void setPptRekProjekties(List<PptRekProjekty> pptRekProjekties) {
		this.pptRekProjekties = pptRekProjekties;
	}

	public PptRekProjekty addPptRekProjekty(PptRekProjekty pptRekProjekty) {
		getPptRekProjekties().add(pptRekProjekty);
		pptRekProjekty.setZptNabory(this);

		return pptRekProjekty;
	}

	public void addNewPptRekProjekty () {
		PptRekProjekty pptRekProjekty = new PptRekProjekty();
		this.addPptRekProjekty(pptRekProjekty);
	}

	public PptRekProjekty removePptRekProjekty(PptRekProjekty pptRekProjekty) {
		getPptRekProjekties().remove(pptRekProjekty);
		pptRekProjekty.setZptNabory(null);

		return pptRekProjekty;
	}

	public List<PptRekRezerwyCelowe> getPptRekRezerwyCelowe() {
		return pptRekRezerwyCelowe;
	}

	public void setPptRekRezerwyCelowe(List<PptRekRezerwyCelowe> pptRekRezerwyCelowe) {
		this.pptRekRezerwyCelowe = pptRekRezerwyCelowe;
	}

	public PptRekRezerwyCelowe addPptRekRezerwyCelowe(PptRekRezerwyCelowe pptRekRezerwyCelowe) {
		getPptRekRezerwyCelowe().add(pptRekRezerwyCelowe);
		pptRekRezerwyCelowe.setZptNabory(this);

		return pptRekRezerwyCelowe;
	}

	public void addNewPptRekRezerwyCelowe () {
		PptRekRezerwyCelowe pptRekRezerwyCelowe = new PptRekRezerwyCelowe();
		this.addPptRekRezerwyCelowe(pptRekRezerwyCelowe);
	}

	public PptRekRezerwyCelowe removePptRekRezerwyCelowe(PptRekRezerwyCelowe pptRekRezerwyCelowe) {
		getPptRekRezerwyCelowe().remove(pptRekRezerwyCelowe);
		pptRekRezerwyCelowe.setZptNabory(null);

		return pptRekRezerwyCelowe;
	}

	public String getNabOgTelefon() {
		return nabOgTelefon;
	}

	public void setNabOgTelefon(String nabOgTelefon) {
		this.nabOgTelefon = nabOgTelefon;
	}

	public String getNabOgEmail() {
		return nabOgEmail;
	}

	public void setNabOgEmail(String nabOgEmail) {
		this.nabOgEmail = nabOgEmail;
	}

	public List<PptRekEtapy> getPptRekEtapy() {
		return pptRekEtapy;
	}

	public void setPptRekEtapy(List<PptRekEtapy> pptRekEtapy) {
		this.pptRekEtapy = pptRekEtapy;
	}

	public void addNewPptRekEtapy() {
		PptRekEtapy pptRekEtapy = new PptRekEtapy();
		this.addPptRekEtapy(pptRekEtapy);
	}

	public PptRekEtapy addPptRekEtapy(PptRekEtapy pptRekEtapy) {
		getPptRekEtapy().add(pptRekEtapy);
		pptRekEtapy.setZptNabory(this);

		return pptRekEtapy;
	}

	public PptRekEtapy removePptRekEtapy(PptRekEtapy pptRekEtapy) {
		getPptRekEtapy().remove(pptRekEtapy);
		pptRekEtapy.setZptNabory(null);

		return pptRekEtapy;
	}

//	public SqlBean getSqlBean() {
//		return sqlBean;
//	}
//
//	public void setSqlBean(SqlBean sqlBean) {
//		this.sqlBean = sqlBean;
//	}

	public static Integer fRodzajOrder(String rodzaj){
		if (rodzaj==null)
			return 999;
		return "WYNAGR PREMIA_GW PREMIA_UZ DOD_STAZ DOD_SPEC INNE".indexOf(rodzaj);
	}

}