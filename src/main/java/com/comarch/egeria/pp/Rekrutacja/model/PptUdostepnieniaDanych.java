package com.comarch.egeria.pp.Rekrutacja.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_UDOSTEPNIENIA_DANYCH database table.
 * 
 */
@Entity
@Table(name="PPT_UDOSTEPNIENIA_DANYCH")
@NamedQuery(name="PptUdostepnieniaDanych.findAll", query="SELECT p FROM PptUdostepnieniaDanych p")
public class PptUdostepnieniaDanych implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_UDOSTEPNIENIA_DANYCH",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_UDOSTEPNIENIA_DANYCH", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_UDOSTEPNIENIA_DANYCH")	
	@Column(name="UDST_ID")
	private long udstId;

	@Temporal(TemporalType.DATE)
	@Column(name="UDST_AUDYT_DM")
	private Date udstAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="UDST_AUDYT_DT")
	private Date udstAudytDt;

	@Column(name="UDST_AUDYT_KM")
	private String udstAudytKm;

	@Column(name="UDST_AUDYT_KT")
	private String udstAudytKt;

	@Column(name="UDST_AUDYT_LM")
	private BigDecimal udstAudytLm;

	@Column(name="UDST_AUDYT_UM")
	private String udstAudytUm;

	@Column(name="UDST_AUDYT_UT")
	private String udstAudytUt;

	@Temporal(TemporalType.DATE)
	@Column(name="UDST_DATA")
	private Date udstData = new Date();

	@Column(name="UDST_F_ODBIORCA_DANYCH")
	private String udstFOdbiorcaDanych = "N";

	@Column(name="UDST_KA_ID")
	private Long udstKaId;

	@Column(name="UDST_KOMU")
	private String udstKomu;

	@Column(name="UDST_OPIS")
	private String udstOpis;

	@Column(name="UDST_PODMIOT_UPRAWNIONY")
	private String udstPodmiotUprawniony;

	@Column(name="UDST_ZAKRES")
	private String udstZakres;

	public PptUdostepnieniaDanych() {
	}

	public long getUdstId() {
		return this.udstId;
	}

	public void setUdstId(long udstId) {
		this.udstId = udstId;
	}

	public Date getUdstAudytDm() {
		return this.udstAudytDm;
	}

	public void setUdstAudytDm(Date udstAudytDm) {
		this.udstAudytDm = udstAudytDm;
	}

	public Date getUdstAudytDt() {
		return this.udstAudytDt;
	}

	public void setUdstAudytDt(Date udstAudytDt) {
		this.udstAudytDt = udstAudytDt;
	}

	public String getUdstAudytKm() {
		return this.udstAudytKm;
	}

	public void setUdstAudytKm(String udstAudytKm) {
		this.udstAudytKm = udstAudytKm;
	}

	public String getUdstAudytKt() {
		return this.udstAudytKt;
	}

	public void setUdstAudytKt(String udstAudytKt) {
		this.udstAudytKt = udstAudytKt;
	}

	public BigDecimal getUdstAudytLm() {
		return this.udstAudytLm;
	}

	public void setUdstAudytLm(BigDecimal udstAudytLm) {
		this.udstAudytLm = udstAudytLm;
	}

	public String getUdstAudytUm() {
		return this.udstAudytUm;
	}

	public void setUdstAudytUm(String udstAudytUm) {
		this.udstAudytUm = udstAudytUm;
	}

	public String getUdstAudytUt() {
		return this.udstAudytUt;
	}

	public void setUdstAudytUt(String udstAudytUt) {
		this.udstAudytUt = udstAudytUt;
	}

	public Date getUdstData() {
		return this.udstData;
	}

	public void setUdstData(Date udstData) {
		this.udstData = udstData;
	}

	public String getUdstFOdbiorcaDanych() {
		return this.udstFOdbiorcaDanych;
	}

	public void setUdstFOdbiorcaDanych(String udstFOdbiorcaDanych) {
		this.udstFOdbiorcaDanych = udstFOdbiorcaDanych;
	}

	public Long getUdstKaId() {
		return this.udstKaId;
	}

	public void setUdstKaId(Long udstKaId) {
		this.udstKaId = udstKaId;
	}

	public String getUdstKomu() {
		return this.udstKomu;
	}

	public void setUdstKomu(String udstKomu) {
		this.udstKomu = udstKomu;
	}

	public String getUdstOpis() {
		return this.udstOpis;
	}

	public void setUdstOpis(String udstOpis) {
		this.udstOpis = udstOpis;
	}

	public String getUdstPodmiotUprawniony() {
		return this.udstPodmiotUprawniony;
	}

	public void setUdstPodmiotUprawniony(String udstPodmiotUprawniony) {
		this.udstPodmiotUprawniony = udstPodmiotUprawniony;
	}

	public String getUdstZakres() {
		return this.udstZakres;
	}

	public void setUdstZakres(String udstZakres) {
		this.udstZakres = udstZakres;
	}

}