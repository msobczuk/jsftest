package com.comarch.egeria.pp.Rekrutacja.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_REK_ADRESY database table.
 * 
 */
@Entity
@Table(name="PPT_REK_ADRESY", schema="PPADM")
@NamedQuery(name="PptRekAdresy.findAll", query="SELECT p FROM PptRekAdresy p")
public class PptRekAdresy implements Serializable {
	private static final long serialVersionUID = 1L;

	
	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_PPT_REK_ADRESY",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_REK_ADRESY", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_PPT_REK_ADRESY")
	@Column(name="ADR_ID")
	private Long adrId;

	@Column(name="ADR_ADRESAT")
	private String adrAdresat;

	@Column(name="ADR_GMINA")
	private String adrGmina;


	@Column(name="ADR_KL_KOD")
	private BigDecimal adrKlKod;

	@Column(name="ADR_KOD_POCZTOWY")
	private String adrKodPocztowy;

	@Column(name="ADR_KR_ID")
	private BigDecimal adrKrId = BigDecimal.ONE;

	@Column(name="ADR_MAIL")
	private String adrMail;

	@Column(name="ADR_MIEJSCOWOSC")
	private String adrMiejscowosc;

	@Column(name="ADR_NUMER_DOMU")
	private String adrNumerDomu;

	@Column(name="ADR_NUMER_LOKALU")
	private String adrNumerLokalu;

	@Column(name="ADR_POCZTA")
	private String adrPoczta;

	@Column(name="ADR_POWIAT")
	private String adrPowiat;

	@Column(name="ADR_SKRYTKA_POCZTOWA")
	private String adrSkrytkaPocztowa;

	@Column(name="ADR_TYP")
	private String adrTyp;

	@Column(name="ADR_TYP_ULICY")
	private String adrTypUlicy;

	@Column(name="ADR_ULICA")
	private String adrUlica;


	@Column(name="ADR_WOJ_ID")
	private BigDecimal adrWojId;

	@Column(name="ADR_ZATWIERDZONY")
	private String adrZatwierdzony;
	
	@Column(name="ADR_F_AKTUALNE")
	private String adrFAktualne;
	
	@Column(name="ADR_LP")
	private BigDecimal adrLp;

	//bi-directional many-to-one association to PptRekKandydaci
	@ManyToOne
	@JoinColumn(name="ADR_KA_ID")
	private ZptKandydaci zptKandydaci;

	public PptRekAdresy() {
	}

	public Long getAdrId() {
		return this.adrId;
	}

	public void setAdrId(Long adrId) {
		this.adrId = adrId;
	}

	public String getAdrAdresat() {
		return this.adrAdresat;
	}

	public void setAdrAdresat(String adrAdresat) {
		this.adrAdresat = adrAdresat;
	}

	public String getAdrGmina() {
		return this.adrGmina;
	}

	public void setAdrGmina(String adrGmina) {
		this.adrGmina = adrGmina;
	}



	public BigDecimal getAdrKlKod() {
		return this.adrKlKod;
	}

	public void setAdrKlKod(BigDecimal adrKlKod) {
		this.adrKlKod = adrKlKod;
	}

	public String getAdrKodPocztowy() {
		return this.adrKodPocztowy;
	}

	public void setAdrKodPocztowy(String adrKodPocztowy) {
		this.adrKodPocztowy = adrKodPocztowy;
	}

	public BigDecimal getAdrKrId() {
		return this.adrKrId;
	}

	public void setAdrKrId(BigDecimal adrKrId) {
		this.adrKrId = adrKrId;
	}

	public String getAdrMail() {
		return this.adrMail;
	}

	public void setAdrMail(String adrMail) {
		this.adrMail = adrMail;
	}

	public String getAdrMiejscowosc() {
		return this.adrMiejscowosc;
	}

	public void setAdrMiejscowosc(String adrMiejscowosc) {
		this.adrMiejscowosc = adrMiejscowosc;
	}

	public String getAdrNumerDomu() {
		return this.adrNumerDomu;
	}

	public void setAdrNumerDomu(String adrNumerDomu) {
		this.adrNumerDomu = adrNumerDomu;
	}

	public String getAdrNumerLokalu() {
		return this.adrNumerLokalu;
	}

	public void setAdrNumerLokalu(String adrNumerLokalu) {
		this.adrNumerLokalu = adrNumerLokalu;
	}

	public String getAdrPoczta() {
		return this.adrPoczta;
	}

	public void setAdrPoczta(String adrPoczta) {
		this.adrPoczta = adrPoczta;
	}

	public String getAdrPowiat() {
		return this.adrPowiat;
	}

	public void setAdrPowiat(String adrPowiat) {
		this.adrPowiat = adrPowiat;
	}

	public String getAdrSkrytkaPocztowa() {
		return this.adrSkrytkaPocztowa;
	}

	public void setAdrSkrytkaPocztowa(String adrSkrytkaPocztowa) {
		this.adrSkrytkaPocztowa = adrSkrytkaPocztowa;
	}

	public String getAdrTyp() {
		return this.adrTyp;
	}

	public void setAdrTyp(String adrTyp) {
		this.adrTyp = adrTyp;
	}

	public String getAdrTypUlicy() {
		return this.adrTypUlicy;
	}

	public void setAdrTypUlicy(String adrTypUlicy) {
		this.adrTypUlicy = adrTypUlicy;
	}

	public String getAdrUlica() {
		return this.adrUlica;
	}

	public void setAdrUlica(String adrUlica) {
		this.adrUlica = adrUlica;
	}


	public BigDecimal getAdrWojId() {
		return this.adrWojId;
	}

	public void setAdrWojId(BigDecimal adrWojId) {
		this.adrWojId = adrWojId;
	}

	public String getAdrZatwierdzony() {
		return this.adrZatwierdzony;
	}

	public void setAdrZatwierdzony(String adrZatwierdzony) {
		this.adrZatwierdzony = adrZatwierdzony;
	}

	public ZptKandydaci getZptKandydaci() {
		return this.zptKandydaci;
	}

	public void setZptKandydaci(ZptKandydaci zptKandydaci) {
		this.zptKandydaci = zptKandydaci;
	}
	
	public BigDecimal getAdrLp() {
		return this.adrLp;
	}

	public void setAdrLp(BigDecimal adrLp) {
		this.adrLp = adrLp;
	}
	
	public String getAdrFAktualne() {
		return this.adrFAktualne;
	}

	public void setAdrFAktualne(String adrFAktualne) {
		this.adrFAktualne = adrFAktualne;
	}

}