package com.comarch.egeria.pp.Rekrutacja;

import static com.comarch.egeria.Utils.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.reports.ReportGenerator;

import net.sf.jasperreports.engine.JRException;

@ManagedBean
@Named
@Scope("session")
public class NaborRekrutacjaRaportBean extends SqlBean {


	private static final Logger log = LogManager.getLogger();
	
	
	//Widoczne kolumny: 
	private Boolean pokazKomOrg = false;;
	private Boolean pokazRodzNaboru = false;;
	private Boolean pokazNumOferty = false;;
	private Boolean pokazNumerOglosz = false;;
	private Boolean pokazNazwStanow = false;;
	private Boolean pokazWymiarEtatu = false;;
	private Boolean pokazRodzajUmowy = false;;
	private Boolean pokazLiczbeWakatow = false;;
	private Boolean pokazZrodloFinansowania = false;;
	private Boolean pokazTerminSklDokumentow = false;;
	private Boolean pokazStatusProcesuNaboru = false;;
	private Boolean pokazSymbolOpisuStanowiska = false;;
	private Boolean pokazDatePublikacjiOgloszenia = false;;
	private Boolean pokazLiczbeKobiet = false;;
	private Boolean pokazLiczbeMezczyzn = false;;
	private Boolean pokazLiczbeKandydNiepelnospr = false;;
	private Boolean pokazLiczbeKobietNiepelnospr = false;;
	private Boolean pokazLiczbeMezczyznNiepelnospr = false;;
	private Boolean pokazLiczbeKandydWymFormalne = false;;
	private Boolean pokazLiczbeKandydatow = false;;
	private Boolean pokazDateZakonczenia = false;;
	private Boolean pokazEtap = false;
	private Boolean pokazEtapNazwa = false;
	private Boolean pokazEtapData = false;
	private Boolean pokazWynikOpis = false;
	private Boolean pokazZatrudnionychiDaty = false;
	
	
	
	//filtruj wg: 
	private Date dataPublikacjiOd = null;
	private Date dataPublikacjiDo = null;

	private Date terminOfertOd = null;
	private Date terminOfertDo = null;
	
	private Date dataZakonczeniaOd = null;
	private Date dataZakonczeniaDo = null;	
	
	private String rekRodzaj;

	
	
	//Sortowanie wg: 
	private String sortujWedlug = "SortujWgKomorkiOrg";;
	private String sortujRosnacoMalejaco = "SortujRosnaco";;

	
	
	@PostConstruct
	public void init() {
//		pokazKomOrg = false;
//		pokazRodzNaboru = false;
//		pokazNumOferty = false;
//		pokazNumerOglosz = false;
//		pokazNazwStanow = false;
//		pokazWymiarEtatu = false;
//		pokazRodzajUmowy = false;
//		pokazLiczbeWakatow = false;
//		pokazZrodloFinansowania = false;
//		pokazTerminSklDokumentow = false;
//		pokazStatusProcesuNaboru = false;
//		pokazSymbolOpisuStanowiska = false;
//		pokazDatePublikacjiOgloszenia = false;
//		pokazLiczbeKobiet = false;
//		pokazLiczbeMezczyzn = false;
//		pokazLiczbeKandydNiepelnospr = false;
//		pokazLiczbeKobietNiepelnospr = false;
//		pokazLiczbeMezczyznNiepelnospr = false;
//		pokazLiczbeKandydWymFormalne = false;
//		pokazLiczbeKandydatow = false;
//		pokazDateZakonczenia = false;
		
//		sortujWedlug = "SortujWgKomorkiOrg";
//		sortujRosnacoMalejaco = "SortujRosnaco";
	}
	
	private Boolean anyMarked() {
		int count = 0;
		
		if(pokazKomOrg)
			count++;
		
		if(pokazRodzNaboru)
			count++;
		
		if(pokazNumOferty)
			count++;
			
		if(pokazNumerOglosz)
			count++;
		
		if(pokazNazwStanow)
			count++;
		
		if(pokazWymiarEtatu)
			count++;
		
		if(pokazRodzajUmowy)
			count++;
		
		if(pokazLiczbeWakatow)
			count++;
		
		if(pokazZrodloFinansowania)
			count++;
		
		if(pokazTerminSklDokumentow)
			count++;
		
		if(pokazStatusProcesuNaboru)
			count++;
		
		if(pokazSymbolOpisuStanowiska)
			count++;
		
		if(pokazDatePublikacjiOgloszenia)
			count++;
		
		if(pokazLiczbeKobiet)
			count++;
		
		if(pokazLiczbeMezczyzn)
			count++;
		
		if(pokazLiczbeKandydNiepelnospr)
			count++;
		
		if(pokazLiczbeKobietNiepelnospr)
			count++;
		
		if(pokazLiczbeMezczyznNiepelnospr)
			count++;			
		
		if(pokazLiczbeKandydWymFormalne)
			count++;
		
		if(pokazLiczbeKandydatow)
			count++;

		if(pokazDateZakonczenia)
			count++;
		
		if(pokazEtap)
			count++;

		if(pokazEtapNazwa)
			count++;

		if(pokazEtapData)
			count++;

		if(pokazWynikOpis)
			count++;
		
		if(pokazZatrudnionychiDaty)
			count++;
		
		return count != 0;
	}
	
	private HashMap<String, Object> prepareReportArguments(HashMap<String, Object> args) {
		if(args == null)
			args = new HashMap<>();
		
		args.put("pokaz_kom_org", ""+pokazKomOrg);
		args.put("pokaz_rodz_naboru", ""+pokazRodzNaboru);
		args.put("pokaz_num_oferty", ""+pokazNumOferty);
		args.put("pokaz_numer_oglosz", ""+pokazNumerOglosz);
		args.put("pokaz_nazw_stanow", ""+pokazNazwStanow);
		args.put("pokaz_wymiar_etatu", ""+pokazWymiarEtatu);
		args.put("pokaz_rodzaj_umowy", ""+pokazRodzajUmowy);
		args.put("pokaz_liczbe_wakatow", ""+pokazLiczbeWakatow);
		args.put("pokaz_zrodlo_finansowania", ""+pokazZrodloFinansowania);
		args.put("pokaz_termin_skl_dokumentow", ""+pokazTerminSklDokumentow);
		args.put("pokaz_status_procesu_naboru", ""+pokazStatusProcesuNaboru);
		args.put("pokaz_symbol_opisu_stanowiska", ""+pokazSymbolOpisuStanowiska);
		args.put("pokaz_date_publikacji_ogloszenia", ""+pokazDatePublikacjiOgloszenia);
		args.put("pokaz_liczbe_kobiet", ""+pokazLiczbeKobiet);
		args.put("pokaz_liczbe_mezczyzn", ""+pokazLiczbeMezczyzn);
		args.put("pokaz_liczbe_kandyd_niepelnospr", ""+pokazLiczbeKandydNiepelnospr);
		args.put("pokaz_liczbe_kobiet_niepelnospr", ""+pokazLiczbeKobietNiepelnospr);
		args.put("pokaz_liczbe_mezczyzn_niepelnospr", ""+pokazLiczbeMezczyznNiepelnospr);
		args.put("pokaz_liczbe_kandyd_wym_formalne", ""+pokazLiczbeKandydWymFormalne);
		args.put("pokaz_liczbe_kandydatow", ""+pokazLiczbeKandydatow); 
		
		args.put("pokaz_date_zakonczenia", ""+pokazDateZakonczenia);

		args.put("pokaz_etap", ""+pokazEtap);
		args.put("pokaz_etap_nazwa", ""+pokazEtapNazwa);
		args.put("pokaz_etap_data", ""+pokazEtapData);
		
		args.put("pokaz_wynik_opis", ""+pokazWynikOpis);
		
		args.put("pokaz_Zatrudnionych_i_Daty", ""+pokazZatrudnionychiDaty);
		
		if(sortujWedlug != null && sortujRosnacoMalejaco != null) {
			
			String argument = ""; 
			
			if(sortujWedlug.equals("SortujWgKomorkiOrg"))
				argument = "nazwa_komorki_organizacyjnej";
			else if(sortujWedlug.equals("SortujWgRodzajuNaboru"))
				argument = "rodzaj_naboru";
			else if(sortujWedlug.equals("SortujWgNrOferty"))
				argument  = "nr_oferty_prc";
			else if(sortujWedlug.equals("SortujWgNrOgloszenia"))
				argument = "nr_ogloszenia";
			else if(sortujWedlug.equals("SortujWgDaty"))
				argument = "data_publikacji_ogloszenia";
			
			
			if("SortujMalejaco".equals( sortujRosnacoMalejaco ))
				argument = argument + " desc";
						
			args.put("sortuj_wg", argument);
		}
		
		
		args.put("rek_rodzaj", this.getRekRodzaj());
		
		args.put("data_publikacji_od", nz(format(this.dataPublikacjiOd)));
		args.put("data_publikacji_do", nz(format(this.dataPublikacjiDo)));
		
		args.put("termin_ofert_od", nz(format(this.terminOfertOd)));
		args.put("termin_ofert_do", nz(format(this.terminOfertDo)));
				
		args.put("data_zakonczenia_od", nz(format(this.dataZakonczeniaOd))); 
		args.put("data_zakonczenia_do", nz(format(this.dataZakonczeniaDo))); 
		
		return args;
	}
	
	
	
	
	
	public void raport() {
		if(anyMarked() == false) {
			User.alert("Raport musi zawierać przynajmniej jedną kolumnę!");
			return;
		}

		HashMap<String, Object> params = new HashMap<String, Object>();
		params = prepareReportArguments(params);
		
		try {
			ReportGenerator.displayReportAsPDF(params, "Nabor_rekrutacja");
//			ReportGenerator.displayReportNaboryAsPDF(params, "Nabor_rekrutacja");

		} catch (JRException | SQLException | IOException e) {
			e.printStackTrace();
			User.alert(e.getMessage());
			
			log.error(e.getMessage(), e);
            User.alert(e.getMessage());
		}
		
	}
	
	
	
	
	
	
	public void raportXLS() {
		if(anyMarked() == false) {
			User.alert("Raport musi zawierać przynajmniej jedną kolumnę!");
			return;
		}

		HashMap<String, Object> params = new HashMap<String, Object>();
		params = prepareReportArguments(params);
		
		try {
			ReportGenerator.displayReportAsXLS(params, "Nabor_rekrutacja_xls");
//			ReportGenerator.displayReportNaboryAsPDF(params, "Nabor_rekrutacja");

		} catch (JRException | SQLException | IOException e) {
			e.printStackTrace();
			User.alert(e.getMessage());
			
			log.error(e.getMessage(), e);
            User.alert(e.getMessage());
		}
		
	}

	
	
	
	
	public static Boolean strTNtoBool(String val) {
		return val.equals("T");
	}
	
	public static String boolToTNstr(Boolean val) {
		if (val == null)
			return "N";
		
		if(val)
			return "T";
		else
			return "N";
	}
	
	
	public String getPokazKomOrg() {
		return boolToTNstr(pokazKomOrg);
	}

	public void setPokazKomOrg(String pokazKomOrg) {
		this.pokazKomOrg = strTNtoBool(pokazKomOrg);
	}
	
	public String getPokazRodzNaboru() {
		return boolToTNstr(pokazRodzNaboru);
	}

	public void setPokazRodzNaboru(String pokazRodzNaboru) {
		this.pokazRodzNaboru = strTNtoBool(pokazRodzNaboru);
	}
	
	public String getPokazNumOferty() {
		return boolToTNstr(pokazNumOferty);
	}

	public void setPokazNumOferty(String pokazNumOferty) {
		this.pokazNumOferty = strTNtoBool(pokazNumOferty);
	}

	public String getPokazNumerOglosz() {
		return boolToTNstr(pokazNumerOglosz);
	}

	public void setPokazNumerOglosz(String pokazNumerOglosz) {
		this.pokazNumerOglosz = strTNtoBool(pokazNumerOglosz);
	}

	public String getPokazNazwStanow() {
		return boolToTNstr(pokazNazwStanow);
	}

	public void setPokazNazwStanow(String pokazNazwStanow) {
		this.pokazNazwStanow = strTNtoBool(pokazNazwStanow);
	}

	public String getPokazWymiarEtatu() {
		return boolToTNstr(pokazWymiarEtatu);
	}

	public void setPokazWymiarEtatu(String pokazWymiarEtatu) {
		this.pokazWymiarEtatu = strTNtoBool(pokazWymiarEtatu);
		
	}

	public String getPokazRodzajUmowy() {
		return boolToTNstr(pokazRodzajUmowy);
	}

	public void setPokazRodzajUmowy(String pokazRodzajUmowy) {
		this.pokazRodzajUmowy = strTNtoBool(pokazRodzajUmowy);
	}

	public String getPokazLiczbeWakatow() {
		return boolToTNstr(pokazLiczbeWakatow);
	}

	public void setPokazLiczbeWakatow(String pokazLiczbeWakatow) {
		this.pokazLiczbeWakatow = strTNtoBool(pokazLiczbeWakatow);
	}

	public String getPokazZrodloFinansowania() {
		return boolToTNstr(pokazZrodloFinansowania);
	}

	public void setPokazZrodloFinansowania(String pokazZrodloFinansowania) {
		this.pokazZrodloFinansowania = strTNtoBool(pokazZrodloFinansowania);
	}

	public String getPokazTerminSklDokumentow() {
		return boolToTNstr(pokazTerminSklDokumentow);
	}

	public void setPokazTerminSklDokumentow(String pokazTerminSklDokumentow) {
		this.pokazTerminSklDokumentow = strTNtoBool(pokazTerminSklDokumentow);
	}

	public String getPokazStatusProcesuNaboru() {
		return boolToTNstr(pokazStatusProcesuNaboru);
	}

	public void setPokazStatusProcesuNaboru(String pokazStatusProcesuNaboru) {
		this.pokazStatusProcesuNaboru = strTNtoBool(pokazStatusProcesuNaboru);
	}

	public String getPokazSymbolOpisuStanowiska() {
		return boolToTNstr(pokazSymbolOpisuStanowiska);
	}

	public void setPokazSymbolOpisuStanowiska(String pokazSymbolOpisuStanowiska) {
		this.pokazSymbolOpisuStanowiska = strTNtoBool(pokazSymbolOpisuStanowiska);
	}

	public String getPokazDatePublikacjiOgloszenia() {
		return boolToTNstr(pokazDatePublikacjiOgloszenia);
	}

	public void setPokazDatePublikacjiOgloszenia(String pokazDatePublikacjiOgloszenia) {
		this.pokazDatePublikacjiOgloszenia = strTNtoBool(pokazDatePublikacjiOgloszenia);
	}

	public String getPokazLiczbeKobiet() {
		return boolToTNstr(pokazLiczbeKobiet);
	}

	public void setPokazLiczbeKobiet(String pokazLiczbeKobiet) {
		this.pokazLiczbeKobiet = strTNtoBool(pokazLiczbeKobiet);
	}

	public String getPokazLiczbeMezczyzn() {
		return boolToTNstr(pokazLiczbeMezczyzn);
	}

	public void setPokazLiczbeMezczyzn(String pokazLiczbeMezczyzn) {
		this.pokazLiczbeMezczyzn = strTNtoBool(pokazLiczbeMezczyzn);
	}

	public String getPokazLiczbeKandydNiepelnospr() {
		return boolToTNstr(pokazLiczbeKandydNiepelnospr);
	}

	public void setPokazLiczbeKandydNiepelnospr(String pokazLiczbeKandydNiepelnospr) {
		this.pokazLiczbeKandydNiepelnospr = strTNtoBool(pokazLiczbeKandydNiepelnospr);
	}

	public String getPokazLiczbeKobietNiepelnospr() {
		return boolToTNstr(pokazLiczbeKobietNiepelnospr);
	}

	public void setPokazLiczbeKobietNiepelnospr(String pokazLiczbeKobietNiepelnospr) {
		this.pokazLiczbeKobietNiepelnospr = strTNtoBool(pokazLiczbeKobietNiepelnospr);
	}

	public String getPokazLiczbeMezczyznNiepelnospr() {
		return boolToTNstr(pokazLiczbeMezczyznNiepelnospr);
	}

	public void setPokazLiczbeMezczyznNiepelnospr(String pokazLiczbeMezczyznNiepelnospr) {
		this.pokazLiczbeMezczyznNiepelnospr = strTNtoBool(pokazLiczbeMezczyznNiepelnospr);
	}

	public String getPokazLiczbeKandydWymFormalne() {
		return boolToTNstr(pokazLiczbeKandydWymFormalne);
	}

	public void setPokazLiczbeKandydWymFormalne(String pokazLiczbeKandydWymFormalne) {
		this.pokazLiczbeKandydWymFormalne = strTNtoBool(pokazLiczbeKandydWymFormalne);
	}

	public String getPokazLiczbeKandydatow() {
		return boolToTNstr(pokazLiczbeKandydatow);
	}

	public void setPokazLiczbeKandydatow(String pokazLiczbeKandydatow) {
		this.pokazLiczbeKandydatow = strTNtoBool(pokazLiczbeKandydatow);
	}

	public String getSortujWedlug() {
		return sortujWedlug;
	}

	public void setSortujWedlug(String sortujWedlug) {
		this.sortujWedlug = sortujWedlug;
	}

	public String getSortujRosnacoMalejaco() {
		return sortujRosnacoMalejaco;
	}

	public void setSortujRosnacoMalejaco(String sortujRosnacoMalejaco) {
		this.sortujRosnacoMalejaco = sortujRosnacoMalejaco;
	}

	public Date getDataPublikacjiOd() {
		return dataPublikacjiOd;
	}

	public void setDataPublikacjiOd(Date dataPublikacjiOd) {
		this.dataPublikacjiOd = dataPublikacjiOd;
	}

	public Date getDataPublikacjiDo() {
		return dataPublikacjiDo;
	}

	public void setDataPublikacjiDo(Date dataPublikacjiDo) {
		this.dataPublikacjiDo = dataPublikacjiDo;
	}

	public String getRekRodzaj() {
		return rekRodzaj;
	}

	public void setRekRodzaj(String rekRodzaj) {
		this.rekRodzaj = rekRodzaj;
	}

	public Date getTerminOfertOd() {
		return terminOfertOd;
	}

	public void setTerminOfertOd(Date terminOfertOd) {
		this.terminOfertOd = terminOfertOd;
	}

	public Date getTerminOfertDo() {
		return terminOfertDo;
	}

	public void setTerminOfertDo(Date terminOfertDo) {
		this.terminOfertDo = terminOfertDo;
	}

	public Date getDataZakonczeniaOd() {
		return dataZakonczeniaOd;
	}

	public void setDataZakonczeniaOd(Date dataZakonczeniaOd) {
		this.dataZakonczeniaOd = dataZakonczeniaOd;
	}

	public Date getDataZakonczeniaDo() {
		return dataZakonczeniaDo;
	}

	public void setDataZakonczeniaDo(Date dataZakonczeniaDo) {
		this.dataZakonczeniaDo = dataZakonczeniaDo;
	}

	public String getPokazDateZakonczenia() {
		return boolToTNstr(pokazDateZakonczenia);
	}

	public void setPokazDateZakonczenia(String pokazDateZakonczenia) {
		this.pokazDateZakonczenia = strTNtoBool(pokazDateZakonczenia);
	}

	public String getPokazEtap() {
		return boolToTNstr(pokazEtap);
	}

	public void setPokazEtap(String pokazEtap) {
		this.pokazEtap = strTNtoBool(pokazEtap);
	}

	public String getPokazEtapNazwa() {
		return boolToTNstr(pokazEtapNazwa);
	}

	public void setPokazEtapNazwa(String pokazEtapNazwa) {
		this.pokazEtapNazwa = strTNtoBool(pokazEtapNazwa);
	}

	public String getPokazEtapData() {
		return boolToTNstr(pokazEtapData);
	}

	public void setPokazEtapData(String pokazEtapData) {
		this.pokazEtapData = strTNtoBool(pokazEtapData);
	}

	public String getPokazWynikOpis() {
		return boolToTNstr(pokazWynikOpis);
	}

	public void setPokazWynikOpis(String pokazWynikOpis) {
		this.pokazWynikOpis = strTNtoBool(pokazWynikOpis);
	}
	
	public String getPokazZatrudnionychiDaty() {
		return boolToTNstr(pokazZatrudnionychiDaty);
	}

	public void setPokazZatrudnionychiDaty(String pokazZatrudnionychiDaty) {
		this.pokazZatrudnionychiDaty = strTNtoBool(pokazZatrudnionychiDaty);
	}
	
	
}
