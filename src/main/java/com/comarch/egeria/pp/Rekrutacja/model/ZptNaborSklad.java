package com.comarch.egeria.pp.Rekrutacja.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the PPT_REK_NABOR_SKLAD database table.
 * 
 */
@Entity
@Table(name="PPT_REK_NABOR_SKLAD", schema="PPADM")
@NamedQuery(name="ZptNaborSklad.findAll", query="SELECT z FROM ZptNaborSklad z")
public class ZptNaborSklad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@TableGenerator(name="TABLE_KEYGEN_PPT_REK_NABOR_SKLAD",table="PPT_JPA_GENKEYS", pkColumnName = "PKEY_TABLE", pkColumnValue = "PPT_REK_NABOR_SKLAD", valueColumnName = "PKEY_VALUE", initialValue = 1, allocationSize = 1 )
	@GeneratedValue(strategy=GenerationType.TABLE, generator="TABLE_KEYGEN_PPT_REK_NABOR_SKLAD")
	@Column(name="NSK_ID")
	private Long nskId;

	@Temporal(TemporalType.DATE)
	@Column(name="NSK_AUDYT_DM")
	private Date nskAudytDm;

	@Temporal(TemporalType.DATE)
	@Column(name="NSK_AUDYT_DT")
	private Date nskAudytDt;

	@Column(name="NSK_AUDYT_KM")
	private String nskAudytKm;

	@Column(name="NSK_AUDYT_LM")
	private String nskAudytLm;

	@Column(name="NSK_AUDYT_UM")
	private String nskAudytUm;

	@Column(name="NSK_AUDYT_UT")
	private String nskAudytUt;

	@Column(name="NSK_PROCENT")
	private Double nskProcent = 0.0;

	@Column(name="NSK_RODZAJ")
	private String nskRodzaj;

	@Column(name="NSK_WARTOSC")
	private Double nskWartosc = 0.0;

	//bi-directional many-to-one association to ZptNabory
	@ManyToOne
	@JoinColumn(name="NSK_NAB_ID")
	private ZptNabory zptNabory;

	public ZptNaborSklad() {
	}

	public Long getNskId() {
		return this.nskId;
	}

	public void setNskId(Long nskId) {
		this.nskId = nskId;
	}

	public Date getNskAudytDm() {
		return this.nskAudytDm;
	}

	public void setNskAudytDm(Date nskAudytDm) {
		this.nskAudytDm = nskAudytDm;
	}

	public Date getNskAudytDt() {
		return this.nskAudytDt;
	}

	public void setNskAudytDt(Date nskAudytDt) {
		this.nskAudytDt = nskAudytDt;
	}

	public String getNskAudytKm() {
		return this.nskAudytKm;
	}

	public void setNskAudytKm(String nskAudytKm) {
		this.nskAudytKm = nskAudytKm;
	}

	public String getNskAudytLm() {
		return this.nskAudytLm;
	}

	public void setNskAudytLm(String nskAudytLm) {
		this.nskAudytLm = nskAudytLm;
	}

	public String getNskAudytUm() {
		return this.nskAudytUm;
	}

	public void setNskAudytUm(String nskAudytUm) {
		this.nskAudytUm = nskAudytUm;
	}

	public String getNskAudytUt() {
		return this.nskAudytUt;
	}

	public void setNskAudytUt(String nskAudytUt) {
		this.nskAudytUt = nskAudytUt;
	}

	public Double getNskProcent() {
		return this.nskProcent;
	}

	public void setNskProcent(Double nskProcent) {
		this.nskProcent = nskProcent;
	}

	public String getNskRodzaj() {
		return this.nskRodzaj;
	}

	public void setNskRodzaj(String nskRodzaj) {
		this.nskRodzaj = nskRodzaj;
	}

	public Double getNskWartosc() {
		return this.nskWartosc;
	}

	public void setNskWartosc(Double nskWartosc) {
		this.nskWartosc = nskWartosc;
	}

	public ZptNabory getZptNabory() {
		return this.zptNabory;
	}

	public void setZptNabory(ZptNabory zptNabory) {
		this.zptNabory = zptNabory;
	}

}