package com.comarch.egeria.pp.Obiegi;

import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.NoResultException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
//import org.primefaces.context.RequestContext;
import org.primefaces.model.diagram.DiagramModel;
import org.springframework.context.annotation.Scope;

import com.comarch.egeria.pp.Obiegi.dao.ObiegDao;
import com.comarch.egeria.pp.Obiegi.diagram.DiagramModelCreator;
import com.comarch.egeria.pp.Obiegi.diagram.NonLinearDiagramModelCreator;
import com.comarch.egeria.pp.Obiegi.model.Obieg;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;

@Scope("view")
@Named
public class ObiegDiagramBean extends SqlBean {
	private static final Logger log = LogManager.getLogger(ObiegDiagramBean.class);

	@Inject
	ObiegDao obiegDao;

	private Long idObiegu;
	private Long idEncji;

	private DiagramModel model;

	private Obieg obieg;

	private String kategoriaObieguStr;

	private String nazwaTabeliEncji;
	private String nazwaKluczaEncji;

	private DiagramModelCreator diagramModelCreator;

	private SqlDataSelectionsHandler obiegi = new SqlDataSelectionsHandler();
	private SqlDataSelectionsHandler encje = new SqlDataSelectionsHandler();

	@PostConstruct
	public void init() {
		// TODO - diagramModelCreator tworzony na podstawie tego czy obieg liniowy
		// Na razie używana strategia dla nieliniowego
		diagramModelCreator = new NonLinearDiagramModelCreator();

		//obiegi.ds = this.getDs();
		obiegi.setShortName("sqlobiegi");
		obiegi.setSql(""
				+ "   SELECT  kob_kod, kob_nazwa, tenc_id, tenc_kod, tenc_nazwa, obg_id, obg_wersja, obg_stan_definicji ,kob_nazwa_tabeli, kob_nazwa_klucza"
				+ " FROM   csst_kategorie_obiegow kob, csst_typy_encji tenc, csst_obiegi obg"
				+ " WHERE  tenc_kob_kod = kob_kod and obg_tenc_id = tenc_id"
				+ " order by kob_kod, tenc_id");
		this.getSqls().put("sqlobiegi", obiegi);

		//encje.ds = this.getDs();
		encje.setShortName("sqlencje");
		this.getSqls().put("sqlencje", encje);

		obiegi.resetTs();
		obiegi.reLoadData();
		com.comarch.egeria.Utils.executeScript("PF('obiegDiagramDialog').hide();");
	}

	public void editSelectedRow() {
		try {
			idObiegu = ((BigDecimal) obiegi.getCurrentRow().getDataRow().get("obg_id")).longValue();
			kategoriaObieguStr = (String) obiegi.getCurrentRow().getDataRow().get("kob_nazwa");
			nazwaTabeliEncji = (String) obiegi.getCurrentRow().getDataRow().get("kob_nazwa_tabeli");
			nazwaKluczaEncji = (String) obiegi.getCurrentRow().getDataRow().get("kob_nazwa_klucza");
			reloadEncje();
		} catch (NullPointerException e) {
		}
	}

	private void reloadEncje() {
		idEncji = null;
		encje.setSql("SELECT distinct " + nazwaKluczaEncji + " id_encji FROM " + nazwaTabeliEncji + ", csst_encje_w_stanach "
				+ " where ews_klucz_obcy_id = " + nazwaKluczaEncji + " and ews_obg_id = " + idObiegu + " order by id_encji");
		encje.resetTs();
		encje.reLoadData();
	}

	public void createDiagram() {
		// W tym miejscu ustawienie odpowiedniego creatora
		if (isObiegEncji())
			createObiegEncjiDiagram();
		else
			createObiegDefinicjaDiagram();
	}

	private void createObiegEncjiDiagram() {
		try {
			reloadObiegEncji();
			createModelEncji();
		} catch (NoResultException e) {
			User.alert("Nie znaleziono encji o podanym id");
		}
	}

	private void createObiegDefinicjaDiagram() {
		reloadObieg();
		createModel();
	}

	private void reloadObiegEncji() {
		obieg = obiegDao.getObiegEncji(idObiegu, idEncji);
	}

	private void reloadObieg() {
		obieg = obiegDao.getObieg(idObiegu);
	}

	private void createModel() {
		model = diagramModelCreator.createDiagramModel(obieg);
	}

	private void createModelEncji() {
		model = diagramModelCreator.createDiagramModelEncji(obieg);
	}

	public boolean isObiegDisabled() {
		return null == idObiegu;
	}

	public boolean isObiegEncji() {
		return idEncji != null;
	}

	public DiagramModel getModel() {
		return model;
	}

	public Long getIdObiegu() {
		return idObiegu;
	}

	public void setIdObiegu(Long idObiegu) {
		this.idObiegu = idObiegu;
	}

	public Long getIdEncji() {
		return idEncji;
	}

	public void setIdEncji(Long idEncji) {
		this.idEncji = idEncji;
	}

	public SqlDataSelectionsHandler getObiegi() {
		return obiegi;
	}

	public void setObiegi(SqlDataSelectionsHandler obiegi) {
		this.obiegi = obiegi;
	}

	public String getKategoriaObieguStr() {
		return kategoriaObieguStr;
	}

	public void setKategoriaObieguStr(String kategoriaObieguStr) {
		this.kategoriaObieguStr = kategoriaObieguStr;
	}

	public SqlDataSelectionsHandler getEncje() {
		return encje;
	}

	public void setEncje(SqlDataSelectionsHandler encje) {
		this.encje = encje;
	}

	public String getNazwaTabeliEncji() {
		return nazwaTabeliEncji;
	}

	public String getNazwaKluczaEncji() {
		return nazwaKluczaEncji;
	}

}
