package com.comarch.egeria.pp.Obiegi.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The primary key class for the CSST_STANY_W_OBIEGACH database table.
 * 
 */
@Embeddable
public class StanPK implements Serializable {
	//default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;
	private long obiegId;
	private long defStanId;

	@Column(name="SWO_OBG_ID", insertable=false, updatable=false)
	public long getObiegId() {
		return this.obiegId;
	}
	public void setObiegId(long obiegId) {
		this.obiegId = obiegId;
	}

	@Column(name="SWO_DSTN_ID", insertable=false, updatable=false)
	public long getDefStanId() {
		return this.defStanId;
	}
	public void setDefStanId(long defStanId) {
		this.defStanId = defStanId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof StanPK)) {
			return false;
		}
		StanPK castOther = (StanPK)other;
		return 
			(this.obiegId == castOther.obiegId)
			&& (this.defStanId == castOther.defStanId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + ((int) (this.obiegId ^ (this.obiegId >>> 32)));
		hash = hash * prime + ((int) (this.defStanId ^ (this.defStanId >>> 32)));
		
		return hash;
	}
}