package com.comarch.egeria.pp.Obiegi.dao;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;

import com.comarch.egeria.pp.Obiegi.model.EncjaWStanie;
import com.comarch.egeria.pp.Obiegi.model.HistoriaObiegow;
import com.comarch.egeria.pp.Obiegi.model.KategoriaObiegu;
import com.comarch.egeria.pp.Obiegi.model.Obieg;
import com.comarch.egeria.web.common.hibernate.HibernateContext;

@Named
public class ObiegDaoImpl implements ObiegDao {
	private static final Logger log = LogManager.getLogger(ObiegDaoImpl.class);

	@Override
	public Obieg getObieg(Long idObiegu) {
		return (Obieg) HibernateContext.get(Obieg.class, idObiegu);
	}

	@Override
	public Obieg getObiegEncji(Long idObiegu, Long idEncji) {
		Obieg obieg = null;
		try (HibernateContext h = new HibernateContext()) {
			Session session = h.getSession();
			obieg = session.get(Obieg.class, idObiegu);
			if (obieg != null) {
				Query query = session.createQuery("FROM HistoriaObiegow h " + "WHERE h.obieg = :p_obieg and h.idEncji = :encja_id")
						.setParameter("p_obieg", obieg).setParameter("encja_id", idEncji);
				List<HistoriaObiegow> historiaObiegow = query.getResultList();
				obieg.setHistoriaObiegow(historiaObiegow);

				query = session.createQuery("FROM EncjaWStanie e " + "WHERE e.obieg = :p_obieg and e.idEncji = :encja_id")
						.setParameter("p_obieg", obieg).setParameter("encja_id", idEncji);

				EncjaWStanie encjaWStanie = (EncjaWStanie) query.getSingleResult();
				obieg.setEncjaWStanie(encjaWStanie);
			}
		}
		return obieg;
	}

	@Override
	public KategoriaObiegu getKategoriaObiegu(String kod) {
		KategoriaObiegu kategoriaObiegu;
		try (HibernateContext h = new HibernateContext()) {
			kategoriaObiegu = h.getSession().get(KategoriaObiegu.class, kod);
			if (kategoriaObiegu != null)
				kategoriaObiegu.getTypyEncji().size();
			}
		return kategoriaObiegu;
	}

	@Override
	public List<KategoriaObiegu> getKategorieObiegu() {
		List<KategoriaObiegu> ret = new ArrayList<>();
		try (HibernateContext h = new HibernateContext()) {
			Session session = h.getSession();
			Query query = session.getNamedQuery("KategoriaObiegu.findAll");
			ret = query.getResultList();
		}
		return ret;
	}

}
