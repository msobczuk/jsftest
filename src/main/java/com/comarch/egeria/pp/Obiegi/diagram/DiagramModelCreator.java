package com.comarch.egeria.pp.Obiegi.diagram;

import org.primefaces.model.diagram.DiagramModel;

import com.comarch.egeria.pp.Obiegi.model.Obieg;

public interface DiagramModelCreator {
	
	DiagramModel createDiagramModel(Obieg obieg);

	DiagramModel createDiagramModelEncji(Obieg obieg);
}
