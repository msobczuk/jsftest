package com.comarch.egeria.pp.Obiegi.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the CSST_ENCJE_W_STANACH database table.
 * 
 */
@Entity
@Table(name="CSST_ENCJE_W_STANACH")
@NamedQuery(name="EncjaWStanie.findAll", query="SELECT e FROM EncjaWStanie e")
public class EncjaWStanie implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	private StanDefinicja stanDefinicja;
	private Long idEncji;
	private Obieg obieg;

	@Id
	@Column(name="EWS_ID")
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "EWS_DSTN_ID")
	public StanDefinicja getStanDefinicja() {
		return this.stanDefinicja;
	}

	public void setStanDefinicja(StanDefinicja stanDefinicja) {
		this.stanDefinicja = stanDefinicja;
	}


	@Column(name="EWS_KLUCZ_OBCY_ID")
	public Long getIdEncji() {
		return this.idEncji;
	}

	public void setIdEncji(Long idEncji) {
		this.idEncji = idEncji;
	}
	
	@ManyToOne
	@JoinColumn(name = "EWS_OBG_ID")
	public Obieg getObieg() {
		return this.obieg;
	}

	public void setObieg(Obieg obieg) {
		this.obieg = obieg;
	}

}