package com.comarch.egeria.pp.Obiegi;

import com.comarch.egeria.pp.Obiegi.model.AkcjaObiegu;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.web.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

import static com.comarch.egeria.Utils.nz;

@ManagedBean
@Named
@Scope("view")
public class AkcjaObieguBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();
	
	private String kategoriaObiegu;
	private long idCzynnosci;
	private long idAkcji;
	
	private AkcjaObiegu akcja;
	private String nazwaCzynnosci;
	
	@PostConstruct
	public void init() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		this.kategoriaObiegu = params.get("kategoria");
		this.idCzynnosci = params.get("id_czynn") == null ? 0 : Long.parseLong(params.get("id_czynn"));
		this.idAkcji = params.get("id_akcji") == null ? 0 : Long.parseLong(params.get("id_akcji"));
		
		if (this.idCzynnosci != 0 && this.idAkcji != 0) {
			try (java.sql.Connection conn = JdbcUtils.getConnection()) {
				this.loadSqlData("sqlAkcja",
						" select AOB_ID, AOB_AKC_ID, AOB_LP, AKC_NAZWA, AKC_KOD, COB_NAZWA "
						+ "from KGT_AKCJE, CSST_AKCJE_OBIEGOW, CSST_CZYNNOSCI_OBIEGOW "
						+ "where AOB_AKC_ID = ? and AOB_COB_ID = ? "
						+ "and AKC_ID = AOB_AKC_ID and COB_ID = AOB_COB_ID",
						this.idAkcji, this.idCzynnosci);

				this.getSql("sqlAkcja").resetTs();
				
				for (DataRow row : this.getSqlData("sqlAkcja")) {
					AkcjaObiegu akcjaObiegu = new AkcjaObiegu();
					BigDecimal idAkOb = (BigDecimal) row.get("aob_id");
					akcjaObiegu.setIdAkcjiObiegu(idAkOb.longValue());
					BigDecimal idAk = (BigDecimal) row.get("aob_akc_id");
					akcjaObiegu.setIdAkcji(idAk.longValue());
					BigDecimal lpAk = (BigDecimal) row.get("aob_lp");
					akcjaObiegu.setLpAkcji(lpAk.intValue());
					akcjaObiegu.setNazwaAkcji((String) row.get("akc_nazwa"));
					akcjaObiegu.setKodAkcji(nz( row.getAsString("akc_kod") ));//Utils.asString(Clob
					
					this.akcja = akcjaObiegu;
					this.nazwaCzynnosci = (String) row.get("cob_nazwa");
	
				}
	
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		} else {
			User.warn("Brak wymaganych parametrów. Nie można wyświetlić kodu akcji.");
		}
	}

	public String getKategoriaObiegu() {
		return kategoriaObiegu;
	}

	public void setKategoriaObiegu(String kategoriaObiegu) {
		this.kategoriaObiegu = kategoriaObiegu;
	}

	public long getIdCzynnosci() {
		return idCzynnosci;
	}

	public void setIdCzynnosci(long idCzynnosci) {
		this.idCzynnosci = idCzynnosci;
	}

	public long getIdAkcji() {
		return idAkcji;
	}

	public void setIdAkcji(long idAkcji) {
		this.idAkcji = idAkcji;
	}

	public AkcjaObiegu getAkcja() {
		return akcja;
	}

	public void setAkcja(AkcjaObiegu akcja) {
		this.akcja = akcja;
	}

	public String getNazwaCzynnosci() {
		return nazwaCzynnosci;
	}

	public void setNazwaCzynnosci(String nazwaCzynnosci) {
		this.nazwaCzynnosci = nazwaCzynnosci;
	}


	public void zapisz() {

		try {
			JdbcUtils.sqlExecNoQuery(" update KGT_AKCJE set AKC_NAZWA = ?, AKC_KOD = ? where akc_id = ? ", this.akcja.getNazwaAkcji(), this.akcja.getKodAkcji(), this.akcja.getIdAkcji());
			User.info("Zmiany zapisano.");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			User.alert(e.getMessage());
		}

	}

}
