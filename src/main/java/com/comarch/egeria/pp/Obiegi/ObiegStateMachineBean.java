package com.comarch.egeria.pp.Obiegi;

import com.comarch.egeria.pp.Obiegi.model.*;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.FlowChartConnector;
import org.primefaces.model.diagram.endpoint.BlankEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.diagram.overlay.ArrowOverlay;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static com.comarch.egeria.Utils.nz;

//import org.primefaces.context.RequestContext;

@ManagedBean
@Named
@Scope("view")
public class ObiegStateMachineBean extends SqlBean {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(ObiegStateMachineBean.class);

	@Inject
	User user;
	
	//private String kategoria;
	private long idEncji;
	private long idObiegu;
//	private String dstnId;
	private long aktualnyDstnId;

	private DefaultDiagramModel model;

	private Obieg obieg = null;
	HashMap<Stan, Element> st2elHM = new HashMap<>();
	HashSet<Czynnosc> czynnosciSet = new HashSet<>();
	HashMap<Czynnosc, Connection> cz2conHM = new HashMap<>();
	HashMap<Czynnosc, Element> cz2elHM = new HashMap<>();

	private Stan aktualnyStan = null;
	
	HashMap<String, HashMap<String, String>> pozycjeStanow = new HashMap<>();
	HashMap<String, HashMap<String, String>> pozycjeCzynnosci = new HashMap<>();
	
	private List<HistoriaCzynnosci> historiaCzynnosciList = new ArrayList<>();
	private List<Long> odwiedzoneStany = new ArrayList<>();
	
	private Czynnosc czynnosc = new Czynnosc();

	@PostConstruct
	public void init() {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		this.idObiegu =  params.get("obg_id") == null ? 0 : Long.parseLong(params.get("obg_id"));
		this.idEncji = params.get("id") == null ? 0 : Long.parseLong(params.get("id"));
		setObieg((Obieg) HibernateContext.get(Obieg.class, this.idObiegu));

		rysujDiagram();
	}

	private void rysujDiagram() {
		model = new DefaultDiagramModel();
		model.setMaxConnections(-1);

		pobierzHistorieCzynnosci();

		List<Stan> posortowaneStany = posortujStany(); 
		
		Element elANULOWANY = null;
		Element elSTART = null;
		Element elSTOP = null;
		
		double i = 0;
		for (Stan s : posortowaneStany) {
			
			double sPozX = (s.getPozX() != null ? s.getPozX().doubleValue() : 2.3*i -109);
			double sPozY = (s.getPozY() != null ? s.getPozY().doubleValue() : 1.2*i -10);
			i += 54;
			
			Element el = newEl(s, sPozX, sPozY);
			
			el.setStyleClass("pp-stan");
			el.setId(String.valueOf(s.getStanDefinicja().getId()));
			
			model.addElement(el);
			st2elHM.put(s, el);
			
			if ("ANULOWANY".equalsIgnoreCase(s.getStanDefinicja().getNazwa()))
				elANULOWANY = el;
			else if ("START".equalsIgnoreCase(s.getStanDefinicja().getNazwa()))
				elSTART = el;
			else if ("STOP".equalsIgnoreCase(s.getStanDefinicja().getNazwa()))
				elSTOP = el;
			
			for (Czynnosc czynn : s.getCzynnosciStart()) {
				
				HistoriaCzynnosci historiaDane = historiaCzynnosciList.stream()
						.filter(hist -> czynn.getId() == hist.getIdCzynnosci()).findAny().orElse(null);
				if (historiaDane != null) {
					czynn.setKtoWykonal(historiaDane.getKtoWykonal());
					czynn.setKiedyWykonana(historiaDane.getKiedyWykonana());
				}
				
				czynn.setAkcjeObiegu(pobierzAkcje(czynn.getId()));
				
				Element elCz = newElCz(czynn);
				elCz.setStyleClass("pp-czynnosc");
				elCz.setId(String.valueOf(czynn.getId()));

				model.addElement(elCz);
				cz2elHM.put(czynn, elCz);
			}
			
			czynnosciSet.addAll(s.getCzynnosciStart());
		}

		Stan stAnulowany = ((StanElem) elANULOWANY.getData()).stan;	
		if (stAnulowany.getPozX()==null)
			elANULOWANY.setX(elSTART.getX());
		if (stAnulowany.getPozY()==null)
			elANULOWANY.setY(pxAdd( elSTOP.getY(),+130));
		
		ArrayList<Connection> redConnectors = new ArrayList<Connection>();
		ArrayList<Connection> grayConnectors = new ArrayList<Connection>();
		
		
		if (this.getSqlData("sqlHistoriaCzynnosci")!=null && !this.getSqlData("sqlHistoriaCzynnosci").isEmpty() ) {
			elSTART.setStyleClass("pp-stan pp-stan-odwiedzony pp-stan-aktualny");//jesli jest jakas czynnosc wych. ze start to zmieni odwiezony aktualny na odwiedzony
			if (this.aktualnyDstnId==0)
				this.aktualnyDstnId = ((StanElem) elSTART.getData()).stan.getStanDefinicja().getId();
		}
		
		//connectory (strzałki)
		for (Czynnosc cz : czynnosciSet) {
			Stan stPoczatek = cz.getStanStart();
			Stan stKoniec = cz.getStanKoniec();
			String stKoniecNazwa = stKoniec.getStanDefinicja().getNazwa();
			
//			System.out.println(stPoczatek.getStanDefinicja().getNazwa() + " ---" + cz.getNazwa() + "---> " + stKoniecNazwa);
			
			Element elP = st2elHM.get(stPoczatek);
			Element elC = cz2elHM.get(cz);
			Element elK = st2elHM.get(stKoniec);
			
			EndPoint from = null;
			EndPoint to = null;
			EndPoint from2 = null;
			EndPoint to2 = null;
			
			if ("ANULOWANY".equalsIgnoreCase(stKoniecNazwa)){
				from = elP.getEndPoints().get(4);
				to = elC.getEndPoints().get(0);
				from2 = elC.getEndPoints().get(4);
				to2 = elK.getEndPoints().get(0);
				elC.setX(pxAdd(elP.getX(),+39));
				elC.setY( pxAdd(elANULOWANY.getY(), -100) );
//				elC.setY(pxAdd(elP.getY(),+120));
				
//			} else if ("STOP".equalsIgnoreCase(stKoniecNazwa)){
//				from = elP.getEndPoints().get(4);
//				to = elC.getEndPoints().get(2);
//				from2 = elC.getEndPoints().get(6);
//				to2 = elK.getEndPoints().get(0);
//				elC.setX(pxAdd(elK.getX(),100));
//				elC.setY(pxAdd(elP.getY(),+60));
				
			} else if (posortowaneStany.indexOf(stKoniec) < posortowaneStany.indexOf(stPoczatek)) {
				//WSTECZ
				from = elP.getEndPoints().get(4);
				to = elC.getEndPoints().get(2);
				from2 = elC.getEndPoints().get(6);
				to2 = elK.getEndPoints().get(5);
				elC.setX(pxAdd(elK.getX(),15));
				elC.setY(pxAdd(elP.getY(),+40));
				
			}	else {
				//DO PRZODU
				from = elP.getEndPoints().get(0);
				to = elC.getEndPoints().get(6);
				from2 = elC.getEndPoints().get(2);
				to2 = elK.getEndPoints().get(1);				
				elC.setX(pxAdd(elK.getX(),55));
				elC.setY(pxAdd(elP.getY(),-60));
				//elC.setStyleClass("h100");
			}	

			if (cz.getPozX()!=null) elC.setX(cz.getPozX() + "px");
			if (cz.getPozY()!=null) elC.setY(cz.getPozY() + "px");
			

						
			Connection c0 = null;
			Connection c1 = null;

			if (cz.getKtoWykonal()==null || cz.getKtoWykonal().isEmpty()){
				elC.setStyleClass("pp-czynnosc pp-czynnosc-nw");
				c0 = new Connection(from, to,  connectorLightGray());
				c1 = new Connection(from2, to2,  connectorLightGray());
				grayConnectors.add(c0);
				grayConnectors.add(c1); 
			} else {
				c0 = new Connection(from, to,  connectorRed());
				c1 = new Connection(from2, to2,  connectorRed());
				redConnectors.add(c0);
				redConnectors.add(c1);

				if (stPoczatek.getStanDefinicja().getId() == aktualnyDstnId) {
					System.out.println(elP + " AKTUALNY STAN elP ->  " + elP.getId() + " " + elP.getData());
					elP.setStyleClass("pp-stan pp-stan-odwiedzony pp-stan-aktualny");
				} else {
					System.out.println(elP + " AKTUALNY STAN elP ->  " + elP.getId() + " " + elP.getData());
					elP.setStyleClass("pp-stan pp-stan-odwiedzony");
				}


				if (stKoniec.getStanDefinicja().getId() == aktualnyDstnId) {
					System.out.println(elK + " AKTUALNY STAN elK ->  " + elK.getId() + " " + elK.getData());
					elK.setStyleClass("pp-stan pp-stan-odwiedzony pp-stan-aktualny");
				} else {
					System.out.println(elK + " ODWIEDZONY STAN elK ->  " + elK.getId() + " " + elK.getData());
					elK.setStyleClass("pp-stan pp-stan-odwiedzony");
				}
			}
			c1.getOverlays().add(new ArrowOverlay(10, 10, 1, 1));
			
//			model.connect(c0);
//			model.connect(c1);
		}
		
		
		for (Connection c : grayConnectors) {
			model.connect(c);
		}
		
		for (Connection c : redConnectors) {
			model.connect(c);
		}
	}

	
	public FlowChartConnector connectorLightGray() {
		FlowChartConnector connector = new FlowChartConnector();
		connector.setCornerRadius(5);
		connector.setStub(15);
		connector.setPaintStyle("{strokeStyle:'#B0B0B0',lineWidth:1}");
		connector.setHoverPaintStyle("{strokeStyle:'#B0B0B0',lineWidth:2}");
		return connector;
	}
	

	public FlowChartConnector connectorRed() {
		FlowChartConnector connector = new FlowChartConnector();
		connector.setCornerRadius(5);
		connector.setStub(15);
		connector.setPaintStyle(     "{strokeStyle:'#AA5555',lineWidth:2}");
		connector.setHoverPaintStyle("{strokeStyle:'#AA5555',lineWidth:2}");
		return connector;
	}
	

	
	
	public static String pxAdd(String numpx, double offset){
		if (numpx==null || numpx.isEmpty())
			return offset + "px";
		
		double x = Double.parseDouble(numpx.replace("px", ""));
		double ret = x + offset; 
		return ret + "px";
	} 

	
	private List<Stan> posortujStany() {
		List<Stan> ulozoneStany = new ArrayList<>();
		TreeMap<Integer, Stan> numer2Stan = new TreeMap<>();
		int pomNr = 10000;
		
		for (Stan s : obieg.getStany()) {
			if ("ANULOWANY".equalsIgnoreCase(s.getStanDefinicja().getNazwa())) {
				numer2Stan.put(0, s);
			} else if ("START".equalsIgnoreCase(s.getStanDefinicja().getNazwa())) {
				numer2Stan.put(1, s);
			} else if ("STOP".equalsIgnoreCase(s.getStanDefinicja().getNazwa())) {
				numer2Stan.put(99999, s);
			} else {
//				String nazwa = s.getStanDefinicja().getNazwa();
				Integer numer = null;
//				if (nazwa.toUpperCase().contains("POZIOM")) {
//					String nrTxt = nazwa.substring("POZIOM".length());
//					if (nrTxt != null && nrTxt != "") {
//						try {
//							numer = Integer.valueOf(nrTxt);					
//						} catch(NumberFormatException e) {
//							log.error("Nie można przekonwertować na liczbę: %s", nrTxt);
//							log.error(e.getMessage());
//						}
//					}
//				} else {
					long nrLp = s.getSwoLp();
					numer = (int) nrLp;
//					numer = pomNr;
//					pomNr++;
//				}
				
				numer2Stan.put(numer, s);
			}
		}
		
		for (Map.Entry<Integer, Stan> entry : numer2Stan.entrySet()) {
            ulozoneStany.add(entry.getValue());
		}
		
		if (ulozoneStany.size() != obieg.getStany().size()) {
			log.error("Błąd podczas układania stanów w kolejności. Ilość posortowanych stanów jest różna od wszystkich stanów tego obiegu.");
			return obieg.getStany();
		}
		
		return ulozoneStany;
	}

	private void pobierzHistorieCzynnosci() {
		if(this.idObiegu!=0 && this.idEncji != 0) {
			try (java.sql.Connection conn = JdbcUtils.getConnection()) {
				
				SqlDataSelectionsHandler lw = getSql("sqlHistoriaCzynnosci");
				if (lw!=null) {
					lw.resetTs(); 
					lw.setSqlParams(this.idObiegu, this.idEncji);
				} else {
					this.loadSqlData("sqlHistoriaCzynnosci",
							  " SELECT * FROM PPV_HISTORIA_OBIEGOW_LAST "
							+ " where hio_obg_id = ? and hio_klucz_obcy_id = ? "
							+ " order by hio_id",
							this.idObiegu, idEncji);
				}
				for (DataRow row : this.getSqlData("sqlHistoriaCzynnosci")) {
					if (row.get("cob_id") != null) {
						BigDecimal odwiedzonyStan = (BigDecimal) row.get("dstn_id_z");
						odwiedzoneStany.add(odwiedzonyStan.longValue());
						
						aktualnyDstnId = ((BigDecimal) row.get("dstn_id")).longValue();
						
						HistoriaCzynnosci historiaCzynnosci = new HistoriaCzynnosci();
						historiaCzynnosci.setIdEncji(idEncji);
						BigDecimal idCzynn = (BigDecimal) row.get("cob_id");
						historiaCzynnosci.setIdCzynnosci(idCzynn.longValue());
						historiaCzynnosci.setKtoWykonal(row.get("prc_imie") + " " + row.get("prc_nazwisko"));
						historiaCzynnosci.setKiedyWykonana((Date) row.get("hio_data"));
						
						historiaCzynnosciList.add(historiaCzynnosci);
					}
				}
	
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				return;
			}
		}
	}
	
	private List<AkcjaObiegu> pobierzAkcje(long idCzynnosci) {
		List<AkcjaObiegu> akcje = new ArrayList<>();
		
		try (java.sql.Connection conn = JdbcUtils.getConnection()) {
			this.loadSqlData("sqlAkcjeObiegu",
					" select AOB_ID, AOB_AKC_ID, AOB_LP, AKC_NAZWA, AKC_KOD "
					+ "from CSST_AKCJE_OBIEGOW, KGT_AKCJE "
					+ "where AOB_COB_ID = ? and AKC_ID = AOB_AKC_ID",
					idCzynnosci);
			
			for (DataRow row : this.getSqlData("sqlAkcjeObiegu")) {
				AkcjaObiegu akcjaObiegu = new AkcjaObiegu();
				BigDecimal idAkOb = (BigDecimal) row.get("aob_id");
				akcjaObiegu.setIdAkcjiObiegu(idAkOb.longValue());
				BigDecimal idAk = (BigDecimal) row.get("aob_akc_id");
				akcjaObiegu.setIdAkcji(idAk.longValue());
				BigDecimal lpAk = (BigDecimal) row.get("aob_lp");
				akcjaObiegu.setLpAkcji(lpAk.intValue());
				akcjaObiegu.setNazwaAkcji((String) row.get("akc_nazwa"));
				akcjaObiegu.setKodAkcji(nz( row.getAsString("akc_kod") )); //Utils.asString((Clob)

				akcje.add(akcjaObiegu);
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		
		return akcje;
	}
	
	public void pobierzPozycje() {		
		
		for (Stan s : getObieg().getStany()) {
			
			String polecenieJsLeft = "$('#frmDiagram\\\\:diagramObiegu-" + st2elHM.get(s).getId() + "').css('left')";
			String polecenieJsTop = "$('#frmDiagram\\\\:diagramObiegu-" + st2elHM.get(s).getId() + "').css('top')";
			
			com.comarch.egeria.Utils.executeScript("pobierzXY([{name:'id', value:" + st2elHM.get(s).getId()
						+ "}, {name:'left', value:" + polecenieJsLeft + "}, {name:'top', value:" + polecenieJsTop 
						+ "}, {name:'element', value:'stan'}])");			
		}
		
		for (Czynnosc cz : czynnosciSet) {
			String polecenieJsLeft = "$('#frmDiagram\\\\:diagramObiegu-" + cz2elHM.get(cz).getId() + "').css('left')";
			String polecenieJsTop = "$('#frmDiagram\\\\:diagramObiegu-" + cz2elHM.get(cz).getId() + "').css('top')";

			com.comarch.egeria.Utils.executeScript("pobierzXY([{name:'id', value:" + cz2elHM.get(cz).getId()
						+ "}, {name:'left', value:" + polecenieJsLeft + "}, {name:'top', value:" + polecenieJsTop 
						+ "}, {name:'element', value:'czynnosc'}])");
		}
		
	}
	
	public void pobierzXY() {
		Map<String, String> requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String id = requestParamMap.get("id");
		String left = requestParamMap.get("left");
		String top = requestParamMap.get("top");
		String elem = requestParamMap.get("element");
		
		HashMap<String, String> leftTop = new HashMap<>();
		leftTop.put(left, top);
		
		if (elem.equals("stan")) {
			pozycjeStanow.put(id, leftTop);
		} else if (elem.equals("czynnosc")) {
			pozycjeCzynnosci.put(id, leftTop);
		}
		//System.out.println(elem + " " + id + "; left: " + left + "; top:"+ top);
	}
	
	public void zapiszUklad() throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		if (!user.isAdmin()) {
			User.warn("Zapisywać może tylko administrator PP");
			return;
		}
		try (java.sql.Connection conn = JdbcUtils.getConnection()) {
		
			for (Stan s : getObieg().getStany()) {
				HashMap<String, String> pozXYhm = new HashMap<>();
				pozXYhm = pozycjeStanow.get(st2elHM.get(s).getId());
				
				if (pozXYhm != null) {
					for (Map.Entry<String, String> entry : pozXYhm.entrySet()) {
						String x = entry.getKey().replace("px", "");
						String y = entry.getValue().replace("px", "");
						double xDoZapisu = Double.parseDouble(x);
						double yDoZapisu = Double.parseDouble(y);
						
						s.setPozX(xDoZapisu); // s.getStanDefinicja().setPozX(xDoZapisu);
						s.setPozY(yDoZapisu); // s.getStanDefinicja().setPozY(yDoZapisu);
						
						JdbcUtils.sqlExecNoQuery(conn, 
						"UPDATE EGADM1.CSST_STANY_W_OBIEGACH set SWO_X=?, SWO_Y=?, SWO_LP=? 		 WHERE SWO_DSTN_ID=? AND SWO_OBG_ID=?"
												           , s.getPozX(), s.getPozY(), s.getSwoLp(), 	 s.getStanDefinicja().getId(),	s.getObieg().getId());
						
						
						JdbcUtils.sqlExecNoQuery(conn, 
								"UPDATE EGADM1.CSST_DEF_STANOW set DSTN_NAZWA=?, DSTN_OPIS=? 				WHERE DSTN_ID=? and DSTN_F_PREDEFINIOWANY='N'"
														      , s.getStanDefinicja().getNazwa() , s.getStanDefinicja().getOpis(), 		s.getStanDefinicja().getId());						

					}
	            }
			}
			for (Czynnosc cz : czynnosciSet) {
				HashMap<String, String> pozXYhm = new HashMap<>();
				pozXYhm = pozycjeCzynnosci.get(cz2elHM.get(cz).getId());
				
				if (pozXYhm != null) {
					for (Map.Entry<String, String> entry : pozXYhm.entrySet()) {
						String x = entry.getKey().replace("px", "");
						String y = entry.getValue().replace("px", "");
						double xDoZapisu = Double.parseDouble(x);
						double yDoZapisu = Double.parseDouble(y);
						
						cz.setPozX(xDoZapisu);
						cz.setPozY(yDoZapisu);
						
						JdbcUtils.sqlExecNoQuery(conn, 
						"UPDATE EGADM1.CSST_CZYNNOSCI_OBIEGOW set COB_X=?, COB_Y=?, COB_NAZWA=?, COB_NAZWA2=?  	WHERE COB_ID=?"
														   , cz.getPozX(), cz.getPozY(), cz.getNazwa(), cz.getNazwa2(),															cz.getId());
						
					}
	            }
			}
			User.info("Zapisano układ diagramu obiegu!");
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void resetujUklad() {
		for (Stan stan : getObieg().getStany()) {
			stan.setPozX(null);
			stan.setPozY(null);
		}
		
		for (Czynnosc czynnosc : czynnosciSet) {
			czynnosc.setPozX(null);
			czynnosc.setPozY(null);
		}
		
		rysujDiagram();
	}

	private Element newEl(Stan stan, double x, double y) {

		Element el = new Element(new StanElem(stan), x + "px", y + "px");
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP)); 			// 0
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP_RIGHT)); 	// 1
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.RIGHT)); 		// 2
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM_RIGHT)); // 3
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM)); 		// 4
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM_LEFT));	// 5
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.LEFT)); 		// 6
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP_LEFT)); 	// 7

		return el;
	}

	private Element newElCz(Czynnosc czynn) { 

		Element el = new Element(new CzynnElem(czynn));	//, x + "px", y + "px");
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP));			// 0
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP_RIGHT)); 	// 1
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.RIGHT)); 		// 2
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM_RIGHT)); // 3
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM)); 		// 4
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.BOTTOM_LEFT)); 	// 5
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.LEFT)); 		// 6
		el.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP_LEFT)); 	// 7
		
		
		String title ="";
		
		
		if (czynn.getKtoWykonal() != null)
			title = "<div class='tt-lbl-kto'>"
					+ "<i class='material-icons'>face</i>"
					+ "<span>"
					+ czynn.getKtoWykonal()
					+ "</span>"
					+ "</div>"

					+ "<div class='tt-lbl-kiedy'>"
					+ format(czynn.getKiedyWykonana(), "yyyy-MM-dd HH:mm:ss")
					+ "</div>"

					+ "<div class='tt-lbl-nzw-cz p5'> "
					+ czynn.getNazwa()
					+ "</div>"
					;
		else
			title = "<div class='tt-lbl-nzw-cz'>" + czynn.getNazwa() + "</div>";
		
//			if (user.isAdmin()) {
////				title+= "<div style=\"position:relative; width:100%\"><a href=\"#\" style=\"position:absolute; right:0; bottom:0;\" onclick=\"window.open('CzynnoscObiegu?id_czynn=" + czynn.getId()+"')\"><i class='material-icons'>edit</i></a></div>";
//				title+= "<div style=\"position:relative; width:100%\"><div style=\"position:absolute; right:0; bottom:0;\" onclick=\"showDlgCzObg("+czynn.getId()+");\"><i class='material-icons'>edit</i></div></div>";
//			}
		
			//title += htmlAkcje(czynn);

			el.setTitle(title);

		return el;
	}
	
	
	

	

	public DiagramModel getModel() {
		return model;
	}

	public Obieg getObieg() {
		return obieg;
	}

	public void setObieg(Obieg obieg) {
		this.obieg = obieg;
	}

	public String getKategoria() {
		//return kategoria;
		if (obieg==null) return null;
		return this.obieg.getTypEncji().getKategoriaObiegu().getKod();
	}

//	public void setKategoria(String kategoria) {
//		this.kategoria = kategoria;
//	}

	public long getIdEncji() {
		return idEncji;
	}


	public void setIdEncji(long idEncji) {
		this.idEncji = idEncji;
	}


	public long getIdObiegu() {
		return idObiegu;
	}


	public void setIdObiegu(long idObiegu) {
		this.idObiegu = idObiegu;
	}


//	public String getDstnId() {
//		return dstnId;
//	}
//
//
//	public void setDstnId(String dstnId) {
//		this.dstnId = dstnId;
//	}


	public Stan getAktualnyStan() {
		return aktualnyStan;
	}

	public void setAktualnyStan(Stan aktualnyStan) {
		this.aktualnyStan = aktualnyStan;
		if (aktualnyStan.getStanDefinicja()!=null)
			this.aktualnyDstnId = aktualnyStan.getStanDefinicja().getId();
		else
			this.aktualnyDstnId = 0;
	}

	public class StanElem implements Serializable {
		private static final long serialVersionUID = 1L;
		
		Stan stan;

		public StanElem() {
		}

		public StanElem(Stan stan) {
			this.stan = stan;
		}

		@Override
		public String toString() {
			return getNazwa();
		}

		public String getNazwa() {
			return stan.getStanDefinicja().getNazwa();
		}

		public String getOpis() {
			return stan.getStanDefinicja().getOpis();
		}
		
		public Long getId() {
			return stan.getStanDefinicja().getId();
		}
		
		public String getKtoWykonal() {
			return "";
		}
		
		public Date getKiedyWykonana() {
			return null;
		}
		
		public String getKtoKiedy(){
			return inicjaly(getKtoWykonal()) + " "  + format(getKiedyWykonana(),"yyyy-MM-dd HH:mm");
		}
		
		public String getAkcje() {
			return "";
		}

		public String getLblAkcje() {
			return "";
		}

	}

	public class CzynnElem implements Serializable {
		private static final long serialVersionUID = 1L;

		Czynnosc czynn;

		public CzynnElem() {
		}

		public CzynnElem(Czynnosc czynn) {
			this.czynn = czynn;
		}

		@Override
		public String toString() {
			return getNazwa();
		}

		public String getNazwa() {
			return "";
		}
		
		public String getOpis() {
			return czynn.getNazwa();
		}
		
		public Long getId() {
			return czynn.getId();
		}
		
		public String getKtoWykonal() {
			return czynn.getKtoWykonal();
		}
		
		public Date getKiedyWykonana() {
			return czynn.getKiedyWykonana();
		}
		
		public String getKtoKiedy(){
			return inicjaly(getKtoWykonal()) + " "  + format(getKiedyWykonana(),"yyyy-MM-dd HH:mm");
		}
		
		public String getAkcje() {
			return htmlAkcje(czynn);
		}
		
		
		public String getLblAkcje() {
			return String.format("(%1$s)", czynn.getAkcjeObiegu().size() );
		}
		
	}
	
	
	public static String inicjaly(String imieNazwisko){
		if (imieNazwisko==null || imieNazwisko.isEmpty())return "";
		
		String ret = "";
		String[] arr = imieNazwisko.split(" ");
		
		for (String w : arr) {
			if (!w.isEmpty())
			ret += w.substring(0, 1).toUpperCase().trim();
		}
		
		return ret;
	}
	
	
	public String htmlAkcje(Czynnosc czynn){
		String akcje = "";
		StringJoiner joiner = new StringJoiner("</br>");
		
		if (user.isAdmin()) {
			for (AkcjaObiegu akcja : czynn.getAkcjeObiegu()) {
				joiner.add("<a href=\"#\" class=\"ui-commandlink ui-widget lbl-akcje lnk-akcje\" "
						+ "onclick=\"window.open('AkcjaObiegu?kategoria=" + getKategoria()
						+ "&id_czynn=" + czynn.getId()+ "&id_akcji=" + akcja.getIdAkcji()+ "')\">"  + akcja.getIdAkcji() + " - " + akcja.getNazwaAkcji() + "</a>");
			}
		} else {
			for (AkcjaObiegu akcja : czynn.getAkcjeObiegu()) {
				joiner.add("<span class=\"ui-commandlink ui-widget lbl-akcje lnk-akcje\" >"  + akcja.getIdAkcji() + " - " + akcja.getNazwaAkcji() + "</span>");
			}
			
		}
		
		
		
		if (!joiner.toString().isEmpty())
			akcje = "Akcje:</br>" + joiner.toString();
		
		return akcje;
	}

	public Czynnosc getCzynnosc() {
		return czynnosc;
	}

	public void setCzynnosc(Czynnosc czynnosc) {
		this.czynnosc = czynnosc;
	}

	
	public void setCzynnoscById(Long czId) {
		if (czId==null) czId=0l;
		
		this.czynnosc = new Czynnosc();
		for (Czynnosc cz : czynnosciSet) {
			if (cz.getId()==czId) {
				this.setCzynnosc ( cz );
				return;
			}
		}
	}
	
	public List<Element> elStany(){
		List<Element> collect = this.getModel().getElements().stream()
				.filter(x->x.getData() instanceof StanElem)
				.collect(Collectors.toList());
		return  collect;
	}
	
	public List<Element> elCzynnosci(){
		List<Element> collect = this.getModel().getElements().stream()
				.filter(x->x.getData() instanceof CzynnElem)
				.collect(Collectors.toList());
		return  collect;
	}
	
	
	public void setStanByLp(Long stanLp) {
		List<Element> elStany = elStany();
		for (Element el : elStany) {
			StanElem stel = (StanElem) el.getData();
			long swoLp = stel.stan.getSwoLp();
			if ( swoLp == stanLp ) {
				this.setAktualnyStan(  stel.stan );
				return;
			}
		}
		
		this.setAktualnyStan(  new Stan() );
	}
	
	
	public String jsElOndblclick(Object el) {
		if (el instanceof CzynnElem) {
			return "showDlgCzObg( "+ ((CzynnElem) el).czynn.getId() +" );";
		}
		
		if (el instanceof StanElem) {
			return "showDlgStObg( "+ ((StanElem) el).stan.getSwoLp() +" );";
		}
		
		return "return false;";
	}
	
}
