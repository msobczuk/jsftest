package com.comarch.egeria.pp.Obiegi.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


/**
 * The persistent class for the CSST_OBIEGI database table.
 * 
 */
@Entity
@Table(name="CSST_OBIEGI")
@NamedQuery(name="Obieg.findAll", query="SELECT c FROM Obieg c")
public class Obieg implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	private String fLiniowy;
	private String opis;
	private String stanDefinicji;
	private TypEncji typEncji;
	private BigDecimal wersja;
	private List<Stan> stany;
	
	private List<HistoriaObiegow> historiaObiegow;
	private EncjaWStanie encjaWStanie;

	public Obieg() {
		stany = new ArrayList<>();
		historiaObiegow = new ArrayList<>();
	}


	@Id
	@Column(name="OBG_ID")
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}


	@Column(name="OBG_F_LINIOWY")
	public String getFLiniowy() {
		return this.fLiniowy;
	}

	public void setFLiniowy(String fLiniowy) {
		this.fLiniowy = fLiniowy;
	}


	@Column(name="OBG_OPIS")
	public String getOpis() {
		return this.opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}


	@Column(name="OBG_STAN_DEFINICJI")
	public String getStanDefinicji() {
		return this.stanDefinicji;
	}

	public void setStanDefinicji(String stanDefinicji) {
		this.stanDefinicji = stanDefinicji;
	}


	@ManyToOne
	@JoinColumn(name="OBG_TENC_ID")
	public TypEncji getTypEncji() {
		return this.typEncji;
	}

	public void setTypEncji(TypEncji typEncji) {
		this.typEncji = typEncji;
	}


	@Column(name="OBG_WERSJA")
	public BigDecimal getWersja() {
		return this.wersja;
	}

	public void setWersja(BigDecimal wersja) {
		this.wersja = wersja;
	}


	//bi-directional many-to-one association to CsstStanyWObiegach
	@OneToMany(mappedBy="obieg", fetch=FetchType.EAGER)
	@OrderBy("swoLp")
	@Fetch (FetchMode.SELECT)
	public List<Stan> getStany() {
		return this.stany;
	}

	public void setStany(List<Stan> stany) {
		this.stany = stany;
	}

	public Stan addStan(Stan stan) {
		getStany().add(stan);
		stan.setObieg(this);

		return stan;
	}

	public Stan removeStan(Stan stan) {
		getStany().remove(stan);
		stan.setObieg(null);

		return stan;
	}

	
	@Transient
	public List<HistoriaObiegow> getHistoriaObiegow() {
		return historiaObiegow;
	}

	public void setHistoriaObiegow(List<HistoriaObiegow> historiaObiegow) {
		this.historiaObiegow = historiaObiegow;
	}

	@Transient
	public EncjaWStanie getEncjaWStanie() {
		return encjaWStanie;
	}

	public void setEncjaWStanie(EncjaWStanie encjaWStanie) {
		this.encjaWStanie = encjaWStanie;
	}
	

}