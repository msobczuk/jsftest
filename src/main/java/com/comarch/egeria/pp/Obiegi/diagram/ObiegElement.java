package com.comarch.egeria.pp.Obiegi.diagram;

import java.util.List;

import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointList;

public class ObiegElement extends Element {
    
    private List<EndPoint> startEndPoints;
    private List<EndPoint> finishEndPoints;

    public ObiegElement() {
        super();
        initEndpoints();
    }
    
    public ObiegElement(Object data) {
        super(data);
        initEndpoints();
    }
    
    public ObiegElement(Object data, String x, String y) {
        super(data, x, y);
        initEndpoints();
    }
    
    @Override
    public void clearEndPoints(EndPoint endPoint) {
        super.clearEndPoints(endPoint);
        startEndPoints.clear();
        finishEndPoints.clear();
    }
    
    public void addStartEndPoint(EndPoint endPoint) {
        super.addEndPoint(endPoint);
        startEndPoints.add(endPoint);
    }
    
    public void addFinishEndPoint(EndPoint endPoint) {
        super.addEndPoint(endPoint);
        finishEndPoints.add(endPoint);
    }
    
    private void initEndpoints() {
    	startEndPoints = new EndPointList();
        finishEndPoints = new EndPointList();
    }

}