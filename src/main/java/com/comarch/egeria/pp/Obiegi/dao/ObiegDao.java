package com.comarch.egeria.pp.Obiegi.dao;

import java.util.List;

import com.comarch.egeria.pp.Obiegi.model.KategoriaObiegu;
import com.comarch.egeria.pp.Obiegi.model.Obieg;

public interface ObiegDao {
	
	Obieg getObieg(Long idObiegu);

	Obieg getObiegEncji(Long idObiegu, Long idEncji);

	KategoriaObiegu getKategoriaObiegu(String kod);
	
	List<KategoriaObiegu> getKategorieObiegu();
	
}
