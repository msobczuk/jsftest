package com.comarch.egeria.pp.Obiegi.model;

import com.comarch.egeria.web.User;
import org.primefaces.model.diagram.Element;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static com.comarch.egeria.Utils.eq;

/**
 * The persistent class for the CSST_STANY_W_OBIEGACH database table.
 * 
 */
@Entity
@Table(name = "CSST_STANY_W_OBIEGACH")
@NamedQuery(name = "Stan.findAll", query = "SELECT c FROM Stan c")
public class Stan implements Serializable {
	private static final long serialVersionUID = 1L;
	private StanPK id;
	private String swoFZatwierdzony;
	private long swoLp;
	private Double pozX;
	private Double pozY;
	
	private List<Czynnosc> czynnosciStart;
	private List<Czynnosc> czynnosciKoniec;
	private StanDefinicja stanDefinicja;
	private Obieg obieg;
	
	private Element diagramElement;

	@EmbeddedId
	public StanPK getId() {
		return this.id;
	}

	public void setId(StanPK id) {
		this.id = id;
	}

	@Column(name = "SWO_F_ZATWIERDZONY")
	public String getSwoFZatwierdzony() {
		return this.swoFZatwierdzony;
	}

	public void setSwoFZatwierdzony(String swoFZatwierdzony) {
		this.swoFZatwierdzony = swoFZatwierdzony;
	}

	@Column(name = "SWO_LP")
	public long getSwoLp() {
		return this.swoLp;
	}

	public void setSwoLp(long swoLp) {
		final boolean eq = eq(this.swoLp, swoLp);
		this.swoLp = swoLp;

		if (!eq && this.getObieg()!=null){
		//przesun kolidujace lp do przodu
			final long cnt = this.getObieg().getStany().stream()
					.filter(s -> s.getSwoLp() == this.getSwoLp() && !s.equals(this))
					.count();
			if (cnt>0) { // kolizja - przsunac inne w dół
				this.getObieg().getStany().stream()
						.filter(s -> s.getSwoLp() >= this.getSwoLp() && s.getSwoLp() < 99999 && !s.equals(this))
						.forEach(s -> {
							s.setSwoLp(s.getSwoLp() + 1);
						});
				User.warn("Zwiększono o 1 LP stanu(ów) gdzie poprzednio LP było większe lub równe %1$s. W razie potrzeby zresetuj układ grafu.", this.getSwoLp() );
			}
		}
	}

	@Column(name="SWO_X")
	public Double getPozX() {
		return pozX;
	}

	public void setPozX(Double pozX) {
		this.pozX = pozX;
	}

	@Column(name="SWO_Y")
	public Double getPozY() {
		return pozY;
	}

	public void setPozY(Double pozY) {
		this.pozY = pozY;
	}

	// bi-directional many-to-one association to CsstCzynnosciObiegow
	@OneToMany(mappedBy = "stanStart", fetch = FetchType.EAGER)
	public List<Czynnosc> getCzynnosciStart() {
		return this.czynnosciStart;
	}

	public void setCzynnosciStart(List<Czynnosc> czynnosciStart) {
		this.czynnosciStart = czynnosciStart;
	}

	public Czynnosc addCzynnoscStart(Czynnosc czynnoscStart) {
		getCzynnosciStart().add(czynnoscStart);
		czynnoscStart.setStanStart(this);

		return czynnoscStart;
	}

	public Czynnosc removeCzynnoscStart(Czynnosc czynnoscStart) {
		getCzynnosciStart().remove(czynnoscStart);
		czynnoscStart.setStanStart(null);

		return czynnoscStart;
	}

	// bi-directional many-to-one association to CsstCzynnosciObiegow
	@OneToMany(mappedBy = "stanKoniec", fetch = FetchType.EAGER)
	public List<Czynnosc> getCzynnosciKoniec() {
		return this.czynnosciKoniec;
	}

	public void setCzynnosciKoniec(List<Czynnosc> czynnosciKoniec) {
		this.czynnosciKoniec = czynnosciKoniec;
	}

	public Czynnosc addCzynnoscKoniec(Czynnosc czynnoscKoniec) {
		getCzynnosciKoniec().add(czynnoscKoniec);
		czynnoscKoniec.setStanKoniec(this);

		return czynnoscKoniec;
	}

	public Czynnosc removeCzynnoscKoniec(Czynnosc czynnoscKoniec) {
		getCzynnosciKoniec().remove(czynnoscKoniec);
		czynnoscKoniec.setStanKoniec(null);

		return czynnoscKoniec;
	}

	// bi-directional many-to-one association to CsstDefStanow
	@ManyToOne
	@JoinColumn(name = "SWO_DSTN_ID", insertable = false, updatable = false)
	public StanDefinicja getStanDefinicja() {
		return this.stanDefinicja;
	}

	public void setStanDefinicja(StanDefinicja stanDefinicja) {
		this.stanDefinicja = stanDefinicja;
	}

	// bi-directional many-to-one association to CsstObiegi
	@ManyToOne
	@JoinColumn(name = "SWO_OBG_ID", insertable = false, updatable = false)
	public Obieg getObieg() {
		return this.obieg;
	}

	public void setObieg(Obieg obieg) {
		this.obieg = obieg;
	}

	@Transient
	public Element getDiagramElement() {
		return diagramElement;
	}

	public void setDiagramElement(Element diagramElement) {
		this.diagramElement = diagramElement;
	}

}