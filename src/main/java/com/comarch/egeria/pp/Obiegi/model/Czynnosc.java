package com.comarch.egeria.pp.Obiegi.model;

import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.endpoint.EndPoint;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The persistent class for the CSST_CZYNNOSCI_OBIEGOW database table.
 * 
 */
@Entity
@Table(name = "CSST_CZYNNOSCI_OBIEGOW")
@NamedQuery(name = "Czynnosc.findAll", query = "SELECT c FROM Czynnosc c")
public class Czynnosc implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	private String fRownolegla;
	private String fZamykac;
	private String nazwa;
	private String nazwa2;
	private Stan stanStart;
	private Stan stanKoniec;
	private Double pozX;
	private Double pozY;

	private EndPoint startEndPoint;
	private EndPoint finishEndPoint;
	
	private Connection connection;
	
	private String ktoWykonal;
	private Date kiedyWykonana;
	private List<AkcjaObiegu> akcjeObiegu;
	
	@Id
	@Column(name = "COB_ID")
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name = "COB_F_ROWNOLEGLA")
	public String getFRownolegla() {
		return this.fRownolegla;
	}

	public void setFRownolegla(String fRownolegla) {
		this.fRownolegla = fRownolegla;
	}

	@Column(name = "COB_F_ZAMYKAC")
	public String getFZamykac() {
		return this.fZamykac;
	}

	public void setFZamykac(String fZamykac) {
		this.fZamykac = fZamykac;
	}

	@Column(name = "COB_NAZWA")
	public String getNazwa() {
		return this.nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	@Column(name = "COB_NAZWA2")
	public String getNazwa2() {
		return this.nazwa2;
	}

	public void setNazwa2(String nazwa2) {
		this.nazwa2 = nazwa2;
	}
	// bi-directional many-to-one association to CsstStanyWObiegach
		@ManyToOne
		@JoinColumns({ @JoinColumn(name = "COB_SWO_DSTN_ID_Z", referencedColumnName = "SWO_DSTN_ID"),
				@JoinColumn(name = "COB_SWO_OBG_ID_Z", referencedColumnName = "SWO_OBG_ID") })
	public Stan getStanStart() {
		return this.stanStart;
	}

	public void setStanStart(Stan stanStart) {
		this.stanStart = stanStart;
	}


	// bi-directional many-to-one association to CsstStanyWObiegach
	@ManyToOne
	@JoinColumns({ @JoinColumn(name = "COB_SWO_DSTN_ID_DO", referencedColumnName = "SWO_DSTN_ID"),
			@JoinColumn(name = "COB_SWO_OBG_ID_DO", referencedColumnName = "SWO_OBG_ID") })
	public Stan getStanKoniec() {
		return this.stanKoniec;
	}

	public void setStanKoniec(Stan stanKoniec) {
		this.stanKoniec = stanKoniec;
	}

	@Column(name="COB_X")
	public Double getPozX() {
		return pozX;
	}

	public void setPozX(Double pozX) {
		this.pozX = pozX;
	}

	@Column(name="COB_Y")
	public Double getPozY() {
		return pozY;
	}

	public void setPozY(Double pozY) {
		this.pozY = pozY;
	}

	@Transient
	public EndPoint getStartEndPoint() {
		return startEndPoint;
	}

	public void setStartEndPoint(EndPoint startEndPoint) {
		this.startEndPoint = startEndPoint;
	}

	@Transient
	public EndPoint getFinishEndPoint() {
		return finishEndPoint;
	}

	public void setFinishEndPoint(EndPoint finishEndPoint) {
		this.finishEndPoint = finishEndPoint;
	}

	@Transient
	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	@Transient
	public String getKtoWykonal() {
		return ktoWykonal;
	}

	public void setKtoWykonal(String ktoWykonal) {
		this.ktoWykonal = ktoWykonal;
	}

	@Transient
	public Date getKiedyWykonana() {
		return kiedyWykonana;
	}

	public void setKiedyWykonana(Date kiedyWykonana) {
		this.kiedyWykonana = kiedyWykonana;
	}

	@Transient
	public List<AkcjaObiegu> getAkcjeObiegu() {
		return akcjeObiegu;
	}

	public void setAkcjeObiegu(List<AkcjaObiegu> akcjeObiegu) {
		this.akcjeObiegu = akcjeObiegu;
	}


	@Transient
	public List<AkcjaObiegu> getAkcjeObieguSorted() {
		return akcjeObiegu.stream()
		.sorted(Comparator.comparingInt(AkcjaObiegu::getLpAkcji))
		.collect(Collectors.toList());
	}

	public void setAkcjeObieguSorted(List<AkcjaObiegu> akcjeObiegu) {
		this.akcjeObiegu = akcjeObiegu;
	}
}