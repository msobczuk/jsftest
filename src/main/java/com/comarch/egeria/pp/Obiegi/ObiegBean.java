package com.comarch.egeria.pp.Obiegi;

import com.comarch.egeria.Utils;
import com.comarch.egeria.pp.data.*;
import com.comarch.egeria.pp.data.model.ModelBase;
import com.comarch.egeria.web.User;
import com.comarch.egeria.web.common.hibernate.HibernateContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import static com.comarch.egeria.Utils.*;

//import org.primefaces.context.RequestContext;

@ManagedBean
@Scope("view")
@Named
public class ObiegBean extends SqlBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger();//public bo wiekszosc beanow dziedziczy po tym
	
	@Inject
	AppBean appBean;
	
	@Inject
	User user;

	private String cObieCzynnosciClientId = null;
	private String kategoriaObiegu;
	private Long idEncji;
	private String czynnosciWhere = "";
	private String czynnoscDoWykonania; //id wykonywanej czynnosci
	private String opisCzynnosciDoWykonania; //komentarz do wykonywanej czynnosci
	
	Object hibernateEntities2Refresh = null;
	

	@PostConstruct
	public void init() {

		Map<String, String> P = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String strktg = P.get("ktg");
		String strid = P.get("id");
		
		if (strktg!=null)
			this.setKategoriaObiegu(strktg);

//		this.getCzynnosci().resetTs();
//		this.getStan().resetTs();
		this.reset();
	}


	public void reset(){
		this.getStan().resetTs();
		this.getCzynnosci().resetTs();
		this.getCzynnosci().clearSelection();
		this.setCzynnoscDoWykonania(null);
		this.getLwPplWspHistoriaObiegu().resetTs();
		opisCzynnosciDoWykonania = "";
	}


	
	public void rozpocznijObieg() {
		if (this.idEncji == null || this.idEncji ==0){
			User.warn("Obiekt nie został zapisany. \nNowe dane należy zapisać przed rozpoczęciem obiegu.");
			return;
		} 
		
		if (this.stanObiegu() != null && !"".equals(this.stanObiegu())) {
			return;
		}

		try (Connection conn = JdbcUtils.getConnection()) {
			JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_kategorie", this.kategoriaObiegu);
			JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_id_encji", this.idEncji);
			Object tencId = JdbcUtils.sqlExecScalarQuery(conn,
					" select TENC_ID from  EGADM1.CSST_TYPY_ENCJI TENC where  tenc_kob_kod = ? ", this.kategoriaObiegu);
			JdbcUtils.sqlSPCall(conn, "cssp_obiegi.rozpocznij_obieg", tencId, this.idEncji);
		} catch (Exception e) {
			if (e.getMessage().contains("Użytkownik nie ma uprawnień"))
				User.alert("Brak uprawnień do rozpoczęcia obiegu.");
			else if (e.getMessage().contains("Nie znaleziono zatwierdzonego obiegu dla encji"))
				User.alert("Nie znaleziono zatwierdzonego obiegu dla kategorii '%1$s' oraz encji o id = %2$s",this.getKategoriaObiegu(),this.getIdEncji());
			else if (e.getMessage().contains("Nie znaleziono encji"))
				User.alert(e.getMessage().split("\n", 2)[0].replaceAll("[\\d()-:]+", "").replaceAll("ORA", ""));
			else{
				User.alert(e.getMessage());
				log.error(e.getMessage(), e);
			}
			return;
		}
		
		updateObiegDialogi();
		preselectOneCzynnosc();
		executeScript("showObieg();"); //"updateObieg();

	}





	public void wykonajCzynnosc() {
		if (this.getCzynnoscDoWykonania() == null) {
			User.warn("Nie wskazno czynności");
			return;
		}

		try {

			boolean dirty = HibernateContext.isDirty(hibernateEntities2Refresh);
			if (dirty){
				User.alert("Wprowadzone zmiany nie zostały zapisane. Zapisz dane zanim wykonasz czynność w obiegu.");
				return;
			}

			if (hibernateEntities2Refresh instanceof ModelBase){
				((ModelBase) hibernateEntities2Refresh).beforeCzynnoscObiegu();
			}

			jdbcWykonajCzynnosc();
			HibernateContext.refresh(hibernateEntities2Refresh);

			if (hibernateEntities2Refresh instanceof ModelBase){
				((ModelBase) hibernateEntities2Refresh).afterCzynnoscObiegu();
			}

		} catch (Exception e) {
			if (e.getMessage().contains("Użytkownik nie ma uprawnień")) {
				User.warn("Brak uprawnień do wykonania tej czynności.");//malo proawdopodobne aby uzytkownik zobaczyl czynnosc, do ktorej nie ma uprawnien, ale teoretycznie mozliwe (zaraz po zabraniu uprawnien)
			}
			else if(e.getMessage().contains("niepoprawna czynność dla bieżącego stanu obiegu")){
				User.warn("Uwaga: inny użytkownik właśnie zmienił stan tego obiegu!");
			}
			else {
				User.alert(e);
				log.error( e.getMessage() ,e);
			}

			return;
		}

		this.opisCzynnosciDoWykonania = "";

	}


	private void jdbcWykonajCzynnosc( ) throws SQLException {
		try (Connection conn = JdbcUtils.getConnection()) {
			JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_kategorie", this.kategoriaObiegu);
			JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_id_encji", this.idEncji);
			JdbcUtils.sqlSPCall(conn, "cssp_obiegi.wykonaj_czynnosc", this.getCzynnoscDoWykonania(), this.opisCzynnosciDoWykonania);
		}
	}


	public void usunDaneObiegu() {
		if ("".equals(this.stanObiegu()))
			return;// brak obiegu

		try (Connection conn = JdbcUtils.getConnection()) {

			JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_kategorie", this.kategoriaObiegu);
			JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.ustaw_id_encji", this.idEncji);
			JdbcUtils.sqlSPCall(conn, "cssp_obiegi_utl.USUN_DANE", this.kategoriaObiegu, this.idEncji);

			reset();

		} catch (Exception e) {
			e.printStackTrace();
			if (e.getMessage().contains("Użytkownik nie ma uprawnień"))
				User.alert("Brak uprawnień do usunięcia danych obiegu.");
			return;
		}

		updateObiegDialogi();
	}





	public String stanObiegu() {
		if (this.getStan() != null && !this.getStan().getData().isEmpty()) {
			String ret = nz(this.getStan().getData().get(0).getAsString("DSTN_NAZWA")).toUpperCase();
			return ret.toUpperCase();
		}
		return "";
	}

	public String stanObieguOpis() {
		if (this.getStan() != null && !this.getStan().getData().isEmpty()) {
			String ret = nz(this.getStan().getData().get(0).getAsString("DSTN_OPIS")).replace("'","");
			executeScript("if (parent!=undefined && parent.setStanObiegu!=undefined) parent.setStanObiegu('Stan obiegu: " + ret + "');");
			return ret;
		}
				
		executeScript("if (parent!=undefined && parent.setStanObiegu!=undefined) parent.setStanObiegu('');");
		return "";
	}



	public void updateObiegDialogi() {
		
		this.getStan().resetTs();
		this.getCzynnosci().resetTs();
		this.setCzynnoscDoWykonania(null);
		
		if (this.getLwPplWspHistoriaObiegu() != null)
			this.getLwPplWspHistoriaObiegu().resetTs();

		updateComponent("dlgObiegCzynnosciId");
		updateComponent("dlgHistoriaObieguId");
		updateComponent(cObieCzynnosciClientId);

	}

	
	public Long getIdEncji() {
		return idEncji;
	}

	public String setIdEncji(Long idEncji) {

		if (eq(idEncji,0))
			idEncji = null;

		this.idEncji = idEncji;
		return "";
	}

	public String getKategoriaObiegu() {
		return kategoriaObiegu;
	}

	public String setKategoriaObiegu(String kategoriaObiegu) {
		this.kategoriaObiegu = kategoriaObiegu;
		return "";
	}

	
	
	public String getCzynnoscDoWykonania() {
		return czynnoscDoWykonania;
	}

	public void setCzynnoscDoWykonania(String czynnoscDoWykonania) {
		this.czynnoscDoWykonania = czynnoscDoWykonania;
        updateComponent("frmDlgObieg:btnWykonajCzynnoscId1");
	}


	public SqlDataSelectionsHandler getCzynnosci() {
        SqlDataSelectionsHandler lwCzynnosci = getLW("SQLCZYNNOSCI")
                .setPreQuery("" +
                                "begin \n" +//uwaga - kolejnosc ustawiania jest bardzo istotna - najpierw kategoria potem id_encji (bo ustawienie katagorii ustawi null na id_encji)
                                "    EGADM1.cssp_obiegi_utl.ustaw_kategorie( :P_KOB_KOD ); \n" +
                                "    EGADM1.cssp_obiegi_utl.ustaw_id_encji( :P_ID_ENCJI ); \n" +
                                "end;"
                        , this.kategoriaObiegu,
                        eq( this.idEncji,0L)? null : this.idEncji //nie mozna ustawiac 0 ale mozna null
                );

        if (this.kategoriaObiegu==null || this.kategoriaObiegu.equals("") || this.idEncji==null || this.idEncji.equals(0L)){
            return lwCzynnosci;
		}

		String sql = getSqlCzynnosciAppendWhere(lwCzynnosci); 
		if (!lwCzynnosci.getSql().equals(sql)) {
			lwCzynnosci.setSql(sql);
		}
		
//		if (lwCzynnosci.getData().size()==1 && lwCzynnosci.getRadioSelectedRow()==null){
//			lwCzynnosci.setRadioSelectedRow(lwCzynnosci.first());
//			this.setCzynnoscDoWykonania(lwCzynnosci.first().getIdAsString());
//		}

		return lwCzynnosci;
	}


	public void preselectOneCzynnosc(){//jesli mamy tylko jedna czynnosc to mozna ja wstepne zaznaczyc
		SqlDataSelectionsHandler lwCzynnosci = this.getCzynnosci();
		if (lwCzynnosci.getData().size()==1 && lwCzynnosci.getRadioSelectedRow()==null){
			lwCzynnosci.setRadioSelectedRow(lwCzynnosci.first());
			this.setCzynnoscDoWykonania(lwCzynnosci.first().getIdAsString());
		} else {
			this.setCzynnoscDoWykonania(null);
		}
	}



	public void preDlgCzynnosci(){
		boolean dirty = HibernateContext.isDirty(hibernateEntities2Refresh);
		if (dirty){
			Utils.executeScript("PF('dlgObiegCzynnosci').hide();");
			User.alert("Wprowadzone zmiany nie zostały zapisane. Zapisz dane zanim wykonasz czynność w obiegu.");
			return;
		}

		reset();
		stanObieguOpis();//wczytanie akt. stanu i ustawienie opisu
		preselectOneCzynnosc();//wczytuje dane z lw sqlczynnosci i jesli 1 wiersz to go zaznacza
		Utils.executeScript("showObieg();");
	}


	private String getSqlCzynnosciAppendWhere(SqlDataSelectionsHandler lwCzynnosci) {
		String sql = lwCzynnosci.getDefLstQuery();

		if (!nz(this.czynnosciWhere).trim().isEmpty()) {
			sql = sql.replace("1=1", this.czynnosciWhere); //dodanie where
		}
		return sql;
	}


	public String getOpisCzynnosciDoWykonania() {//komentarz do wykonywanej czynnosci
		return opisCzynnosciDoWykonania;
	}

	public void setOpisCzynnosciDoWykonania(String opisCzynnosciDoWykonania) {
		this.opisCzynnosciDoWykonania = opisCzynnosciDoWykonania;
	}

	public SqlDataSelectionsHandler getStan() {
		return getLW("SQLSTAN", this.idEncji, this.kategoriaObiegu);
	}

	public SqlDataSelectionsHandler getLwPplWspHistoriaObiegu() {
		return getLW("ppl_wsp_historia_obiegu")
		  		.setPreQuery("" +
						"begin \n" +//uwaga - kolejnosc ustawiania jest bardzo istotna - najpierw kategoria potem id_encji (bo ustawienie katagorii ustawi null na id_encji)
						"    EGADM1.cssp_obiegi_utl.ustaw_kategorie( :P_KOB_KOD ); \n" +
						"    EGADM1.cssp_obiegi_utl.ustaw_id_encji( :P_ID_ENCJI); \n" +
						"end;"
				, this.kategoriaObiegu,
				eq( this.idEncji,0L)? null : this.idEncji //nie mozna ustawiac 0 ale mozna null
		);
	}


	public String getKey(){
		return this.kategoriaObiegu + ":" + this.idEncji;
	}
	
	/**
	 * Klauzula where dodawana dynamicznie do zapytania 
	 * SELECT  ...  FROM cssv_czynnosci_do_wykonania C ... 
	 * + WHERE ...
	 * Uwaga - tawartosc jest wpsodzielona pomiedzy dwiema niezaleznymi
	 * instancjami cObiegCzynnosci.xhtml:ObiegBean.java oraz
	 * iframe/Obiegi.xhtml:Obiegbean.java
	 * 
	 * @return
	 */
	public String getCzynnosciWhere() {
		return this.czynnosciWhere;
	}

	
	
	/**
	 * Uwaga - tawartosc jest wpsodzielona pomiedzy dwiema niezaleznymi
	 * instancjami cObiegCzynnosci.xhtml:ObiegBean.java oraz
	 * iframe/Obiegi.xhtml:Obiegbean.java
	 * 
	 * @param czynnosciWhere
	 */
	public String setCzynnosciWhere(String czynnosciWhere) {
		this.czynnosciWhere = nz(czynnosciWhere); 
		return "";
	}


	public Object getHibernateEntities2Refresh() {
		return hibernateEntities2Refresh;
	}

	public String setHibernateEntities2Refresh(Object hibernateEntities2Refresh) {
		this.hibernateEntities2Refresh = hibernateEntities2Refresh;
		return "";
	}


	public String getcObieCzynnosciClientId() {
		return cObieCzynnosciClientId;
	}

	public void setcObieCzynnosciClientId(String cObieCzynnosciClientId) {
		this.cObieCzynnosciClientId = cObieCzynnosciClientId;
	}







	public String getDiagramUrl() {
		String ret = "";

		SqlDataSelectionsHandler lw = appBean.getLW("PPL_ADM_OBIEGI");
		lw.getData().size();
		DataRow r = lw.find(kategoriaObiegu);

		if (r==null || r.get("obg_id")==null) return ret;

		if (idEncji==null || idEncji==0){
			ret =  String.format("DiagramObiegu?obg_id=%1$s", r.get("obg_id"));
		} else
			ret = String.format("DiagramObiegu?obg_id=%1$s&id=%2$s", r.get("obg_id"),  idEncji);

		return ret;
	}




	//	private String diagramUrl;

//	public void setDiagramUrl(String diagramUrl) {
//		this.diagramUrl = diagramUrl;
//	}


//	private void resetDiagram() {
//		diagramUrl = null;
//		przygotujDiagram();
//	}

//	public void przygotujDiagram() {
//		SqlDataSelectionsHandler lw = appBean.getLW("PPL_ADM_OBIEGI");
//		lw.getData().size();
//		DataRow r = lw.find(kategoriaObiegu);
//
//		if (r==null || r.get("obg_id")==null)
//			return;
//
//		if (idEncji==null || idEncji==0){
//			diagramUrl = String.format("DiagramObiegu?obg_id=%1$s", r.get("obg_id"));
//		} else
//			diagramUrl = String.format("DiagramObiegu?obg_id=%1$s&id=%2$s", r.get("obg_id"),  idEncji);
//
//	}


//	private SqlDataSelectionsHandler aktualnieU = null;
//	private String aktualnieUFormat = null;

//	public SqlDataSelectionsHandler getAktualnieU() {
//		return aktualnieU;
//	}
//
//	public String setAktualnieU(SqlDataSelectionsHandler aktualnieU) {
//		this.aktualnieU = aktualnieU;
//		return "";
//	}
//
//	public String getAktualnieUFormat() {
//		return aktualnieUFormat;
//	}
//
//	public String setAktualnieUFormat(String aktualnieUFormat) {
//		this.aktualnieUFormat = aktualnieUFormat;
//		return "";
//	}

//	public String aktualnieUAsString(String aktU) {
//		if (this.aktualnieU!=null && this.aktualnieU.getData().size()>0){
//			aktU = "Aktualnie u: ";// +this.aktualnieU.getData().get(0);
//			ArrayList<String> al = new ArrayList<>();
//			for (DataRow r : this.aktualnieU.getData()) {
//				al.add( r.format(this.aktualnieUFormat) );
//			}
//			aktU += Utils.join("; ", al);
//		}
//		return aktU;
//	}

}
