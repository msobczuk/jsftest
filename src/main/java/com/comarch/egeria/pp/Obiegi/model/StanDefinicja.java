package com.comarch.egeria.pp.Obiegi.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * The persistent class for the CSST_DEF_STANOW database table.
 * 
 */
@Entity
@Table(name="CSST_DEF_STANOW")
@NamedQuery(name="StanDefinicja.findAll", query="SELECT c FROM StanDefinicja c")
public class StanDefinicja implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	private String fPredefiniowany;
	private String kobKod;
	private String nazwa;
	private String opis;
	private List<Stan> stany;
	private Double pozX;
	private Double pozY;

	@Id
	@Column(name="DSTN_ID")
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Column(name="DSTN_F_PREDEFINIOWANY")
	public String getFPredefiniowany() {
		return this.fPredefiniowany;
	}

	public void setFPredefiniowany(String fPredefiniowany) {
		this.fPredefiniowany = fPredefiniowany;
	}


	@Column(name="DSTN_KOB_KOD")
	public String getKobKod() {
		return this.kobKod;
	}

	public void setKobKod(String kobKod) {
		this.kobKod = kobKod;
	}


	@Column(name="DSTN_NAZWA")
	public String getNazwa() {
		return this.nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}


	@Column(name="DSTN_OPIS")
	public String getOpis() {
		return this.opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}


	//bi-directional many-to-one association to CsstStanyWObiegach
	@OneToMany(mappedBy="stanDefinicja", fetch=FetchType.EAGER)
	public List<Stan> getStany() {
		return this.stany;
	}

	public void setStany(List<Stan> stany) {
		this.stany = stany;
	}

	@Column(name="DSTN_X")
	public Double getPozX() {
		return pozX;
	}

	public void setPozX(Double pozX) {
		this.pozX = pozX;
	}

	@Column(name="DSTN_Y")
	public Double getPozY() {
		return pozY;
	}

	public void setPozY(Double pozY) {
		this.pozY = pozY;
	}

	public Stan addStan(Stan stan) {
		getStany().add(stan);
		stan.setStanDefinicja(this);

		return stan;
	}

	public Stan removeStan(Stan stan) {
		getStany().remove(stan);
		stan.setStanDefinicja(null);

		return stan;
	}

}