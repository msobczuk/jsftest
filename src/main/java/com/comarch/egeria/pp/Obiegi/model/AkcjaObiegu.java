package com.comarch.egeria.pp.Obiegi.model;

import java.io.Serializable;

public class AkcjaObiegu implements Serializable {
	private static final long serialVersionUID = 1L;

	private long idAkcjiObiegu;
	private long idAkcji;
	private int lpAkcji;
	private String nazwaAkcji;
	private String kodAkcji;

	public long getIdAkcjiObiegu() {
		return idAkcjiObiegu;
	}

	public void setIdAkcjiObiegu(long idAkcjiObiegu) {
		this.idAkcjiObiegu = idAkcjiObiegu;
	}

	public long getIdAkcji() {
		return idAkcji;
	}

	public void setIdAkcji(long idAkcji) {
		this.idAkcji = idAkcji;
	}

	public int getLpAkcji() {
		return lpAkcji;
	}

	public void setLpAkcji(int lpAkcji) {
		this.lpAkcji = lpAkcji;
	}

	public String getNazwaAkcji() {
		return nazwaAkcji;
	}

	public void setNazwaAkcji(String nazwaAkcji) {
		this.nazwaAkcji = nazwaAkcji;
	}

	public String getKodAkcji() {
		return kodAkcji;
	}

	public void setKodAkcji(String kodAkcji) {
		this.kodAkcji = kodAkcji;
	}

}
