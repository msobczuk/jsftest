package com.comarch.egeria.pp.Obiegi.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


/**
 * The persistent class for the CSST_KATEGORIE_OBIEGOW database table.
 * 
 */
@Entity
@Table(name="CSST_KATEGORIE_OBIEGOW")
@NamedQuery(name="KategoriaObiegu.findAll", query="SELECT c FROM KategoriaObiegu c")
public class KategoriaObiegu implements Serializable {
	private static final long serialVersionUID = 1L;
	private String kod;
	private String fJednostka;
	private String fTylkoLiniowy;
	private String nazwa;
	private String nazwaJo;
	private String nazwaKlucza;
	private String nazwaTabeli;
	private List<TypEncji> typyEncji;
	
	public KategoriaObiegu() {
		typyEncji = new ArrayList<>();
	}
	

	@Id
	@Column(name="KOB_KOD")
	public String getKod() {
		return this.kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}


	@Column(name="KOB_F_JEDNOSTKA")
	public String getFJednostka() {
		return this.fJednostka;
	}

	public void setFJednostka(String fJednostka) {
		this.fJednostka = fJednostka;
	}


	@Column(name="KOB_F_TYLKO_LINIOWY")
	public String getFTylkoLiniowy() {
		return this.fTylkoLiniowy;
	}

	public void setFTylkoLiniowy(String fTylkoLiniowy) {
		this.fTylkoLiniowy = fTylkoLiniowy;
	}


	@Column(name="KOB_NAZWA")
	public String getNazwa() {
		return this.nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}


	@Column(name="KOB_NAZWA_JO")
	public String getNazwaJo() {
		return this.nazwaJo;
	}

	public void setNazwaJo(String nazwaJo) {
		this.nazwaJo = nazwaJo;
	}


	@Column(name="KOB_NAZWA_KLUCZA")
	public String getNazwaKlucza() {
		return this.nazwaKlucza;
	}

	public void setNazwaKlucza(String nazwaKlucza) {
		this.nazwaKlucza = nazwaKlucza;
	}


	@Column(name="KOB_NAZWA_TABELI")
	public String getNazwaTabeli() {
		return this.nazwaTabeli;
	}

	public void setNazwaTabeli(String nazwaTabeli) {
		this.nazwaTabeli = nazwaTabeli;
	}


	//bi-directional many-to-one association to CsstTypyEncji
	@OneToMany(mappedBy="kategoriaObiegu")
	@OrderBy("kod")
	@Fetch (FetchMode.SELECT)
	public List<TypEncji> getTypyEncji() {
		return this.typyEncji;
	}

	public void setTypyEncji(List<TypEncji> typyEncji) {
		this.typyEncji = typyEncji;
	}

	public TypEncji addTypEncji(TypEncji typEncji) {
		getTypyEncji().add(typEncji);
		typEncji.setKategoriaObiegu(this);

		return typEncji;
	}

	public TypEncji removeTypEncji(TypEncji typEncji) {
		getTypyEncji().remove(typEncji);
		typEncji.setKategoriaObiegu(null);

		return typEncji;
	}

}