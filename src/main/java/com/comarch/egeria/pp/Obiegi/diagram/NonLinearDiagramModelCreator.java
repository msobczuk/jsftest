package com.comarch.egeria.pp.Obiegi.diagram;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.endpoint.BlankEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;

import com.comarch.egeria.pp.Obiegi.model.Czynnosc;
import com.comarch.egeria.pp.Obiegi.model.HistoriaObiegow;
import com.comarch.egeria.pp.Obiegi.model.Obieg;
import com.comarch.egeria.pp.Obiegi.model.Stan;

public class NonLinearDiagramModelCreator extends DefaultDiagramModelCreator {
	private static final Logger log = LogManager.getLogger(NonLinearDiagramModelCreator.class);

	private int elementShiftFactor = 3;
	private int elementShift = 0;

	@Override
	protected void createModel(Obieg obieg) {
		List<Stan> odlaczoneStany = new ArrayList<>();
		for (Stan stan : obieg.getStany()) {
			Element element = new Element(stan.getStanDefinicja().getNazwa(), 2 * elementShift + "em", elementShift + "em");
			element.addEndPoint(new BlankEndPoint(EndPointAnchor.TOP_RIGHT));
			boolean isCzynnosc = false;
			for (Czynnosc czynnosc : stan.getCzynnosciStart()) {
				EndPoint endPoint = new BlankEndPoint(EndPointAnchor.RIGHT);
				element.addEndPoint(endPoint);
				czynnosc.setStartEndPoint(endPoint);
				isCzynnosc = true;
			}
			for (Czynnosc czynnosc : stan.getCzynnosciKoniec()) {
				EndPoint endPoint = new BlankEndPoint(EndPointAnchor.LEFT);
				element.addEndPoint(endPoint);
				czynnosc.setFinishEndPoint(endPoint);
				isCzynnosc = true;
			}
			if (isCzynnosc)
				elementShift += elementShiftFactor;
			else
				odlaczoneStany.add(stan);
			stan.setDiagramElement(element);
			getModel().addElement(element);
		}
		elementShift += 5;
		odlaczoneStany.forEach(st -> {
			st.getDiagramElement().setX(2 * elementShift + "em");
			st.getDiagramElement().setY("0em");
			elementShift += elementShiftFactor;
		});

		for (Stan stan : obieg.getStany()) {
			for (Czynnosc czynnosc : stan.getCzynnosciStart()) {
				Connection conn = createConnection(czynnosc.getStartEndPoint(), czynnosc.getFinishEndPoint(), czynnosc.getNazwa());
				getModel().connect(conn);
				czynnosc.setConnection(conn);
			}
		}
		elementShift = 0;
	}

	@Override
	public DiagramModel createDiagramModelEncji(Obieg obieg) {
		DefaultDiagramModel model = (DefaultDiagramModel) createDiagramModel(obieg);

		for (HistoriaObiegow histObieg : obieg.getHistoriaObiegow()) {
			Czynnosc czynnoscDef = histObieg.getCzynnosc();
			for (Stan stan : obieg.getStany()) {
				boolean isCzynnosc = false;
				for (Czynnosc czynnosc : stan.getCzynnosciStart()) {
					if (czynnoscDef == czynnosc) {
						czynnosc.getConnection().setConnector(getVisitedConnector());
						isCzynnosc = true;
					}
				}
				if (stan.getStanDefinicja() == obieg.getEncjaWStanie().getStanDefinicja())
					stan.getDiagramElement().setStyleClass("ui-diagram-element-current");
				else if (isCzynnosc)
					stan.getDiagramElement().setStyleClass("ui-diagram-element-visited");
			}
		}

		DefaultDiagramModel ret = model;
		model = null;
		return ret;
	}

}
