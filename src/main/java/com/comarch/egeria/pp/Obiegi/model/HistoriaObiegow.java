package com.comarch.egeria.pp.Obiegi.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the CSST_HISTORIA_OBIEGOW database table.
 * 
 */
@Entity
@Table(name = "CSST_HISTORIA_OBIEGOW")
@NamedQuery(name = "HistoriaObiegow.findAll", query = "SELECT h FROM HistoriaObiegow h")
public class HistoriaObiegow implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	private Czynnosc czynnosc;
	private Date data;
	private StanDefinicja stanDefinicja;
	private Long idEncji;
	private Obieg obieg;
	private String opis;
	private String typ;
	private String uztNazwa;

	@Id
	@Column(name = "HIO_ID")
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@ManyToOne
	@JoinColumn(name = "HIO_COB_ID")
	public Czynnosc getCzynnosc() {
		return this.czynnosc;
	}

	public void setCzynnosc(Czynnosc czynnosc) {
		this.czynnosc = czynnosc;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "HIO_DATA")
	public Date getData() {
		return this.data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	@ManyToOne
	@JoinColumn(name = "HIO_DSTN_ID")
	public StanDefinicja getStanDefincja() {
		return this.stanDefinicja;
	}

	public void setStanDefincja(StanDefinicja stanDefinicja) {
		this.stanDefinicja = stanDefinicja;
	}

	@Column(name = "HIO_KLUCZ_OBCY_ID")
	public Long getIdEncji() {
		return this.idEncji;
	}

	public void setIdEncji(Long idEncji) {
		this.idEncji = idEncji;
	}

	@ManyToOne
	@JoinColumn(name = "HIO_OBG_ID")
	public Obieg getObieg() {
		return this.obieg;
	}

	public void setObieg(Obieg obieg) {
		this.obieg = obieg;
	}

	@Column(name = "HIO_OPIS")
	public String getOpis() {
		return this.opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	@Column(name = "HIO_TYP")
	public String getTyp() {
		return this.typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	@Column(name = "HIO_UZT_NAZWA")
	public String getUztNazwa() {
		return this.uztNazwa;
	}

	public void setUztNazwa(String uztNazwa) {
		this.uztNazwa = uztNazwa;
	}

}