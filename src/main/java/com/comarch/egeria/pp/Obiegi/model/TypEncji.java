package com.comarch.egeria.pp.Obiegi.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;


/**
 * The persistent class for the CSST_TYPY_ENCJI database table.
 * 
 */
@Entity
@Table(name="CSST_TYPY_ENCJI")
@NamedQuery(name="TypEncji.findAll", query="SELECT c FROM TypEncji c")
public class TypEncji implements Serializable {
	private static final long serialVersionUID = 1L;
	private long id;
	private String kod;
	private String nazwa;
	private String opis;
	private KategoriaObiegu kategoriaObiegu;
	private List<Obieg> obiegi;


	@Id
	@Column(name="TENC_ID")
	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}


	@Column(name="TENC_KOD")
	public String getKod() {
		return this.kod;
	}

	public void setKod(String kod) {
		this.kod = kod;
	}


	@Column(name="TENC_NAZWA")
	public String getNazwa() {
		return this.nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}


	@Column(name="TENC_OPIS")
	public String getOpis() {
		return this.opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}


	//bi-directional many-to-one association to CsstKategorieObiegow
	@ManyToOne
	@JoinColumn(name="TENC_KOB_KOD")
	public KategoriaObiegu getKategoriaObiegu() {
		return this.kategoriaObiegu;
	}

	public void setKategoriaObiegu(KategoriaObiegu kategoriaObiegu) {
		this.kategoriaObiegu = kategoriaObiegu;
	}
	
	@OneToMany(mappedBy="typEncji", fetch=FetchType.EAGER)
	@OrderBy("wersja")
	@Fetch (FetchMode.SELECT)
	public List<Obieg> getObiegi() {
		return this.obiegi;
	}

	public void setObiegi(List<Obieg> obiegi) {
		this.obiegi = obiegi;
	}

	public Obieg addTypEncji(Obieg obieg) {
		getObiegi().add(obieg);
		obieg.setTypEncji(this);

		return obieg;
	}

	public Obieg removeTypEncji(Obieg obieg) {
		getObiegi().remove(obieg);
		obieg.setTypEncji(null);

		return obieg;
	}

}