package com.comarch.egeria.pp.Obiegi.diagram;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.DiagramModel;
import org.primefaces.model.diagram.connector.Connector;
import org.primefaces.model.diagram.connector.StateMachineConnector;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.overlay.ArrowOverlay;
import org.primefaces.model.diagram.overlay.LabelOverlay;

import com.comarch.egeria.pp.Obiegi.model.Obieg;

public abstract class DefaultDiagramModelCreator implements DiagramModelCreator {
	private static final Logger log = LogManager.getLogger(DefaultDiagramModelCreator.class);

	private DefaultDiagramModel model;
	
	private Connector defaultConnector;
	private Connector visitedConnector;
	
	
	public DefaultDiagramModelCreator() {
		defaultConnector = new StateMachineConnector();
		((StateMachineConnector) defaultConnector).setOrientation(StateMachineConnector.Orientation.ANTICLOCKWISE);
		defaultConnector.setPaintStyle("{strokeStyle:'#7D7463',lineWidth:3}");
		
		visitedConnector = new StateMachineConnector();
		((StateMachineConnector) visitedConnector).setOrientation(StateMachineConnector.Orientation.ANTICLOCKWISE);
		visitedConnector.setPaintStyle("{strokeStyle:'#FF0000',lineWidth:3}");
	}
	
	@Override
	public DiagramModel createDiagramModel(Obieg obieg) {
		model = new DefaultDiagramModel();
		
		init();
		
		createModel(obieg);
		
		DefaultDiagramModel ret = model;
		model = null;
		return ret;
	}
	
	protected abstract void createModel(Obieg obieg);

	protected void init() {
		model.setMaxConnections(-1);
		StateMachineConnector connector = new StateMachineConnector();
        connector.setOrientation(StateMachineConnector.Orientation.ANTICLOCKWISE);
        connector.setPaintStyle("{strokeStyle:'#7D7463',lineWidth:3}");
        model.setDefaultConnector(connector);
	}
	
	protected Connection createConnection(EndPoint from, EndPoint to, String label) {
		Connection conn = new Connection(from, to);
		log.debug("createConnection: " + label);
//		conn.setConnector(getDefaultConnector());
		conn.getOverlays().add(new ArrowOverlay(20, 20, 1, 1));

		if (label != null) {
			conn.getOverlays().add(new LabelOverlay(label, "flow-label", 0.3));
		}

		return conn;
	}
	
	protected DefaultDiagramModel getModel() {
		return model;
	}
	
	protected Connector getDefaultConnector() {
		return defaultConnector;
	}

	protected Connector getVisitedConnector() {
		return visitedConnector;
	}

}
