package com.comarch.egeria.pp.Obiegi.model;

import java.io.Serializable;
import java.util.Date;

public class HistoriaCzynnosci implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String kategoriaObiegu;
	private long idEncji;
	private long idCzynnosci;
	private String ktoWykonal;
	private Date kiedyWykonana;
	
	public String getKategoriaObiegu() {
		return kategoriaObiegu;
	}
	
	public void setKategoriaObiegu(String kategoriaObiegu) {
		this.kategoriaObiegu = kategoriaObiegu;
	}
	
	public long getIdEncji() {
		return idEncji;
	}
	
	public void setIdEncji(long idEncji) {
		this.idEncji = idEncji;
	}
	
	public long getIdCzynnosci() {
		return idCzynnosci;
	}
	
	public void setIdCzynnosci(long idCzynnosci) {
		this.idCzynnosci = idCzynnosci;
	}
	
	public String getKtoWykonal() {
		return ktoWykonal;
	}
	
	public void setKtoWykonal(String ktoWykonal) {
		this.ktoWykonal = ktoWykonal;
	}
	
	public Date getKiedyWykonana() {
		return kiedyWykonana;
	}
	
	public void setKiedyWykonana(Date kiedyWykonana) {
		this.kiedyWykonana = kiedyWykonana;
	}		
}