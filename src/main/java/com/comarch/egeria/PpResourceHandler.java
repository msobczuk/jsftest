package com.comarch.egeria;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.JdbcUtils;
import com.comarch.egeria.pp.data.SqlDataCache;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.faces.application.ResourceHandler;
import javax.faces.application.ResourceHandlerWrapper;
import javax.faces.application.ViewResource;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.CachedRowSet;
import java.sql.Blob;
import java.sql.Connection;
import java.util.concurrent.ConcurrentHashMap;

import static com.comarch.egeria.Utils.nz;

public class PpResourceHandler extends ResourceHandlerWrapper {
    private static final long serialVersionUID = 1L;
    private static final Logger log = LogManager.getLogger();

    private ResourceHandler wrapped;

    public ConcurrentHashMap<String, String> dbResourceFiles = new ConcurrentHashMap<>();


    public PpResourceHandler(ResourceHandler wrapped) {
        this.wrapped = wrapped;
        reloadDbResourceFiles();
    }


    //Date tsd = Utils.asSqlDate(Utils.dateSerial(1900,1,1));
    Long ts = -1L;

    private void reloadDbResourceFiles() {
        final Long tbts = nz(SqlDataCache.tbtsCHM.get("PPADM.PPT_RESOURCE_FILES_RPL"));
        if (ts>=0 && tbts<=ts) {
            return;
        }
        ts = tbts;

        dbResourceFiles.clear();//TODO skasowane w bazie usunac z CHM i ew. pokasowac pliki .rplc

        try (Connection conn = JdbcUtils.getConnection(false)) {
            //CachedRowSet crs = JdbcUtils.sqlExecQuery(conn,"select RSCF_PLIK from PPADM.PPT_RESOURCE_FILES_RPL where nvl(rscf_audyt_dm, rscf_audyt_dt) > ?",  LocalDate.now().minusYears(100)  );
            CachedRowSet crs = JdbcUtils.sqlExecQuery(conn,"select * from PPADM.PPT_RESOURCE_FILES_RPL"  );
            while ( crs.next() ){
                String plik = crs.getString("RSCF_PLIK_SCIEZKA");
                Blob plikDane = crs.getBlob("RSCF_PLIK_DANE");

                final String filePth = AppBean.app.getApplicationContextFolder() + plik + ".rplc";
                Utils.saveBlobAsFile(plikDane, filePth);
//                File replaceFile = new File(filePth);
//                URL url = replaceFile.toURI().toURL();
                dbResourceFiles.put(plik, plik + ".rplc");
                log.debug("PpResourceHandler / zapisano plik: " + filePth);
            }

        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
    }




//    @Override
//    public Resource createResource(String resourceName, String libraryName) {
//
//        if (resourceName.endsWith(".css")) {
//            System.out.println("CSS / createResource[/lib.]:" + resourceName + " /" + libraryName); //pliki *.css
//        }
//        else if (resourceName.endsWith(".js")) {
//            System.out.println("JS / createResource[/lib.]:" + resourceName + " /" + libraryName); //pliki *.js
//        }
//        else if (resourceName.endsWith(".xhtml") && "components".equals(libraryName)) {
//            System.out.println("CC / createResource[/lib.]:" + resourceName + " /" + libraryName); //pliki xhtml COMPOSITE COMPONENTS
//        } else {
//            System.out.println("createResource[/lib.]:" + resourceName + " /" + libraryName); //.gif
//        }
//
//        return super.createResource(resourceName, libraryName);
//    }
//
//    @Override
//    public Resource createResource(String resourceName) {
//
//        if (resourceName.contains(".png") || resourceName.contains(".svg")) {
//            System.out.println("IMG / createResource:" + resourceName);
//        }
//        else {
//            System.out.println("createResource:" + resourceName);
//        }
//
//        return super.createResource(resourceName);
//    }





    @Override
    public ViewResource createViewResource(FacesContext context, final String name) {

        HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        if (!context.getPartialViewContext().isPartialRequest() && req!=null && req.getAttribute("req_tbts_tested") == null){ // && (name.endsWith("EgrTemplate*.xhtml") || name.endsWith("EgrTemplateEmpty.xhtml"))){
            final SqlDataCache sqlDataCache = (SqlDataCache) context.getExternalContext().getSessionMap().get("sqlDataCache");
            if (sqlDataCache!=null) {
                try {
                    sqlDataCache.loadTBTS();
                    sqlDataCache.loadHTS();
                    req.setAttribute("req_tbts_tested", 1);
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            }
            reloadDbResourceFiles();//sprawdzic ew przeladowac zawartosc dynamiczna z bazy do hashmapy/index lub LW orz wygenerowac plik zastepczy
        }

        //log.trace("wczytywanie " + name);

        UIViewRoot viewRoot = context.getViewRoot();
        String viewId = null;
        ConcurrentHashMap<String, String> viewResFiles = null;
        if (viewRoot!=null) {
           viewId = viewRoot.getViewId();
            viewResFiles = AppBean.viewResourceFilesCHM.get(viewId);
            if (viewResFiles==null){
                viewResFiles = new ConcurrentHashMap<>();
                AppBean.viewResourceFilesCHM.put(viewId, viewResFiles);
            }
        }



        final String rplcResourceName = dbResourceFiles.get(name);
        if (rplcResourceName !=null) {
            if (viewResFiles!=null)  {
                viewResFiles.put(name, rplcResourceName);
            }
            return super.createViewResource(context, rplcResourceName);
        } else {
            if (viewResFiles!=null)  {
                viewResFiles.put(name, "");
            }
            return super.createViewResource(context, name);
        }

//        if (resource == null) {
//            resource = new ViewResource() {
//                @Override
//                public URL getURL() {
//                    try {
//                        return new File("/some/base/path", name).toURI().toURL();
//                    } catch (MalformedURLException e) {
//                        throw new FacesException(e);
//                    }
//                }
//            };
//        }

    }



    @Override
    public ResourceHandler getWrapped() {
        return wrapped;
    }






////http://anonsvn.icesoft.org/repo/icepdf/trunk/icepdf/examples/icefaces/src/main/java/org/icepdf/examples/jsf/viewer/renderkit/PdfResourceHandler.java
//
//    private class XhtmlResource extends Resource {
//
//        String name = null;
//
//        public XhtmlResource(String name) {
//            this.name = name;
//        }
//
//        @Override
//        public InputStream getInputStream() throws IOException {
//            return null;// new ByteArrayInputStream();
//        }
//
//        @Override
//        public Map<String, String> getResponseHeaders() {
//            return Collections.emptyMap();
//        }
//
//        @Override
//        public String getRequestPath() {
//            final FacesContext ctx = FacesContext.getCurrentInstance();
//            return ctx
//                    .getApplication()
//                    .getViewHandler()
//                    .getResourceURL(ctx, ResourceHandler.RESOURCE_IDENTIFIER + "/" + name ); //TODO...
//        }
//
//        public URL getURL() {
//            return null;
//        }
//
//        public boolean userAgentNeedsUpdate(FacesContext context) {
//            return true;
//        }
//
//    }




}
