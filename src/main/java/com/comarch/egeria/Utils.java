package com.comarch.egeria;

import com.comarch.egeria.pp.data.AppBean;
import com.comarch.egeria.pp.data.DataRow;
import com.comarch.egeria.pp.data.SqlBean;
import com.comarch.egeria.pp.data.SqlDataSelectionsHandler;
import com.comarch.egeria.web.User;
import org.jfree.util.Log;
import org.primefaces.PrimeFaces;
import org.primefaces.model.DefaultStreamedContent;
import org.springframework.core.io.Resource;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.CRC32;
import java.util.zip.Checksum;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Utilities - metody statyczne/narzędziowe;
 * <p>
 * W klasach korzystających najlepiej stosować
 * import static com.comarch.egeria.Utils;
 * <p>
 * W klasach wewn. pakietu com.comarch.egeria.pp wystarczy wywołanie Utils.<metoda....>.
 * W wybranych klasach bazowych i kontrolerach (np. SqlBean) można reimplementować sygnatury n/w metod jako niestatyczne
 */
public class Utils {


    public static DataRow konfiguracjaDR(String parametr) {
        SqlBean sqlBean = User.getCurrentUser();
        if (sqlBean==null) sqlBean = AppBean.sqlBean;
        if (sqlBean==null) return null;
        return sqlBean.getSL("PP_KONFIGURACJA").find(parametr);
    }

    public static String konfiguracjaWartoscZewn(String parametr) {
        final DataRow r = konfiguracjaDR(parametr);
        if (r==null) return null;
        return r.getAsString("WSL_WARTOSC_ZEWN");
    }

    public static String konfiguracjaWartoscMax(String parametr) {
        final DataRow r = konfiguracjaDR(parametr);
        if (r==null) return null;
        return r.getAsString("WSL_WARTOSC_MAX");
    }

    public static String konfiguracjaOpis(String parametr) {
        final DataRow r = konfiguracjaDR(parametr);
        if (r==null) return null;
        return r.getAsString("WSL_OPIS");
    }
    public static String konfiguracjaOpis2(String parametr) {
        final DataRow r = konfiguracjaDR(parametr);
        if (r==null) return null;
        return r.getAsString("WSL_OPIS2");
    }
    public static String konfiguracjaOpis3(String parametr) {
        final DataRow r = konfiguracjaDR(parametr);
        if (r==null) return null;
        return r.getAsString("WSL_OPIS3");
    }


    public static String konfiguracjaAlias(String parametr) {
        final DataRow r = konfiguracjaDR(parametr);
        if (r==null) return null;
        return r.getAsString("WSL_ALIAS");
    }
    public static String konfiguracjaAlias2(String parametr) {
        final DataRow r = konfiguracjaDR(parametr);
        if (r==null) return null;
        return r.getAsString("WSL_ALIAS2");
    }
    public static String konfiguracjaAlias3(String parametr) {
        final DataRow r = konfiguracjaDR(parametr);
        if (r==null) return null;
        return r.getAsString("WSL_ALIAS3");
    }

    //mnozenie dla double z zaokrąglaniem wyniku
    public static double multiply(Double x1, Double x2, int roundResultDecimalPlaces) {
        BigDecimal bd1 = BigDecimal.valueOf(nz(x1));
        BigDecimal bd2 = BigDecimal.valueOf(nz(x2));
        BigDecimal ret = bd1.multiply(bd2).setScale(roundResultDecimalPlaces, RoundingMode.HALF_UP);
        return ret.doubleValue();
    }


    //mnozenie dla double
    public static double multiply(Double x1, Double x2) {
        BigDecimal bd1 = BigDecimal.valueOf(nz(x1));
        BigDecimal bd2 = BigDecimal.valueOf(nz(x2));
        return bd1.multiply(bd2).doubleValue();
    }


    //divide dla double z zaokrąglaniem wyniku
    public static double divide(Double x1, Double x2, int roundResultDecimalPlaces) {
        BigDecimal bd1 = BigDecimal.valueOf(nz(x1));
        BigDecimal bd2 = BigDecimal.valueOf(nz(x2));
        BigDecimal ret = bd1.divide(bd2, roundResultDecimalPlaces, RoundingMode.HALF_UP).setScale(roundResultDecimalPlaces, RoundingMode.HALF_UP);
        return ret.doubleValue();
    }


    //divide dla double
    public static double divide(Double x1, Double x2) {
        BigDecimal bd1 = BigDecimal.valueOf(nz(x1));
        BigDecimal bd2 = BigDecimal.valueOf(nz(x2));
        return bd1.divide(bd2, 6, RoundingMode.HALF_UP).doubleValue();
    }

    public static boolean eq(Object x1, Object x2) {
        return (x1 != null && x1.equals(x2)) || (x2 != null && x2.equals(x1) || (x1 == null && x2 == null));
    }

    public static boolean ne(Object x1, Object x2) {
        return !eq(x1, x2);
    }


    public static boolean eqAny(Object x1, Object... args) {
        for (Object arg : args) {
            if (eq(x1,arg)) return true;
        }
        return false;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();


        if (Double.isNaN(value) || Double.isInfinite(value))
            return value;

        BigDecimal bd = BigDecimal.valueOf(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }


    // Sprawdzenie formatu danych
    public static boolean isNumeric(String str) {
        if (str != null)
            return str.matches("-?\\d+(\\.\\d+)?");
        else
            return false;
    }


    public static String join(String delimiter, Object... args) {
        return join(delimiter, Arrays.asList(args));
    }

    public static String join(Collection<?> collection) {
        return join(",", collection);
    }

    public static String join(String delimiter, Collection<?> collection) {
        return org.springframework.util.StringUtils.collectionToDelimitedString(collection, delimiter);
    }


    public static String join(Object... args) {
        return join(",", Arrays.asList(args));
    }


    public static String nz(String x) {
        if (x == null)
            return "";
        return x;
    }

    public static Double nz(Double x) {
        if (x == null)
            return 0.00;
        return x;
    }

    public static Long nz(Long x) {
        if (x == null)
            return 0L;
        return x;
    }

    public static BigDecimal nz(BigDecimal x) {
        if (x == null)
            return new BigDecimal(0);
        return x;
    }

    public static Integer nz(Integer x) {
        if (x == null)
            return 0;
        return x;
    }


    public static String format(Date d) {
        return format(d, "yyyy-MM-dd");
    }

    public static String format(Date d, String pattern) {
        if (d == null) return null;
        return new SimpleDateFormat(pattern).format(d);
    }


    public static String format(Number number) {
        String ret = format(number, "#0.00");
        return ret;
    }

    public static String format(Number number, String pattern) {
        if (number == null) return null;

        Locale locale = new Locale("pl", "PL");
        DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(locale);
        decimalFormat.applyPattern(pattern);

        return decimalFormat.format(number);
    }


    public static String format(Object x) {
        if (x instanceof Number)
            return Utils.format((Number) x);

        if (x instanceof Date)
            return Utils.format((Date) x);

        return null;
    }

    public static String strNow(){
        return format(LocalDateTime.now(),"yyyy-MM-dd HH:mm:ss");
    }

    public static String format(LocalDateTime x, String pattern) {
        if (x==null) return "";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return x.format(formatter);
    }


    public static String format(Object x, String pattern) {
        if (x instanceof Number)
            return Utils.format((Number) x, pattern);

        if (x instanceof Date)
            return Utils.format((Date) x, pattern);

        return null;
    }


    public static String asString(Date d) {
        return asString(d, "yyyy-MM-dd");
    }


    public static String asString(Date d, String format) {
        if (d == null)
            return "";
        else
            return new SimpleDateFormat(format).format(d); // format = "yyyy-MM-dd"
    }


    public static String asString(Clob clb) throws SQLException, IOException {
        if (clb == null)
            return "";

        StringBuffer str = new StringBuffer();
        String strng;

        BufferedReader bufferRead = new BufferedReader(clb.getCharacterStream());

        while ((strng = bufferRead.readLine()) != null)
            str.append(strng + "\n");

        return str.toString();
    }

    public static Date dateSerial(int year, int month, int day) {
        return asDate(LocalDate.of(year, month, day)); //		return new Date(year - 1900, month - 1, day);
    }


    public static Date addDays(Date date, int days) {
        if (date == null)
            return null;

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }


    public static Date addYears(Date date, int years) {
        if (date == null)
            return null;

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.YEAR, years);
        return cal.getTime();
    }

    public static Date now() {
        return new Date();
    }

    public static Date today() {
        LocalDate ld = LocalDate.now();
        return asDate(ld);
    }


    public static DayOfWeek dayOfWeek(Date d) {
        if (d == null) return null;
        return asLocalDate(d).getDayOfWeek();
    }


    public static int dayOfMonth(Date d) {
        if (d == null) return 0;
        return asLocalDate(d).getDayOfMonth();
    }


    public static Date asDate(LocalDate localDate) {
        if (localDate == null)
            return null;
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }


    public static Date asDate(LocalDateTime localDateTime) {
        if (localDateTime == null)
            return null;
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }


    public static LocalDate asLocalDate(Date date) {
        if (date == null)
            return null;
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
    }


    public static LocalDateTime asLocalDateTime(Date date) {
        if (date == null)
            return null;
        return Instant.ofEpochMilli(date.getTime()).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static LocalDateTime asLocalDateTime(String strDate, String pattern) {
        if (strDate == null || "".equals(strDate.trim())) return null;
        LocalDateTime datetime = LocalDateTime.parse(strDate, DateTimeFormatter.ofPattern(pattern));
        return datetime;
    }

    public static LocalDateTime asLocalDateTime(String strDate) {
        return asLocalDateTime(strDate, "yyyy-MM-dd HH:mm:ss");
    }

    public static Date asDate(String strDate, String pattern) {
        return asDate(asLocalDateTime(strDate, pattern));
    }

    public static Date asDate(String strDate) {
        return asDate(asLocalDateTime(strDate));
    }


    public static Double asDouble(String strDouble) {
        if (strDouble == null || "".equals(strDouble.trim())) return null;

        strDouble = strDouble.replaceAll(",", ".").replaceAll(" ", "");
        return Double.parseDouble(strDouble);
    }


    public static Date trunc(Date date) {
        if (date == null) return null;
        return asDate(asLocalDate(date));
    }


    public static Timestamp asSqlTimestamp(Date date) {
        if (date == null)
            return null;

        Timestamp ret = new Timestamp(date.getTime());
        return ret;
    }

    public static java.sql.Date asSqlDate(LocalDate localDate) {
        if (localDate == null)
            return null;
        return java.sql.Date.valueOf(localDate);
    }

    public static java.sql.Date asSqlDate(LocalDateTime localDateTime) {
        if (localDateTime == null)
            return null;
        return java.sql.Date.valueOf(localDateTime.toLocalDate());
    }


    public static java.sql.Date asSqlDate(Date date) {
        if (date == null)
            return null;

        java.sql.Date ret = new java.sql.Date(date.getTime());
        return ret;
    }


    public static java.sql.Date plusDays(Date d, int days) {
        if (d == null)
            return null;

        return asSqlDate(asLocalDate(d).plusDays(days));
    }

    public static java.sql.Date plusMonths(Date d, int m) {
        if (d == null)
            return null;

        return asSqlDate(asLocalDate(d).plusMonths(m));
    }

    public static java.sql.Date plusYears(Date d, int y) {
        if (d == null)
            return null;

        return asSqlDate(asLocalDate(d).plusYears(y));
    }


    public static Double cdbl(Long x) {
        if (x == null) return null;
        return x.doubleValue();
    }

    public static Double cdbl(Integer x) {
        if (x == null) return null;
        return x.doubleValue();
    }

    public static Double cdbl(BigDecimal x) {
        if (x == null) return null;
        return x.doubleValue();
    }

    public static Double cdbl(String x) {
        if (x == null) return null;
        return Double.parseDouble(x);
    }


    public static String getStackTraceAsString(Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }

    public static String getExceptionDetails(Throwable e) {
        String ret = "----------------------------------------------------"
                + "\r\n" + getUserSessionViewInfoHeader()
                + "\r\nErr.Msg: " + e.getMessage()
                + "\r\n----------------------------------------------------"
                + "\r\n" + getStackTraceAsString(e);
        return ret;
    }



    public static String getUserSessionViewInfoHeader(){
        String ret = "";
        try {
            User currentUser = User.getCurrentUser();

            String dbLogin = null;
            String sessionId = null;
            String viewId = null;

            if (currentUser != null) {
                dbLogin = currentUser.getDbLogin();
                sessionId = currentUser.getSessionId();
            }

            FacesContext ctx = FacesContext.getCurrentInstance();
            if (ctx != null && ctx.getViewRoot() != null)
                viewId = ctx.getViewRoot().getViewId();

            ret += "" //""----------------------------------------------------"
                    + "\r\nt.: " + LocalDateTime.now()
                    + "\r\nu.: " + dbLogin
                    + "\r\ns.: " + sessionId
                    + "\r\nv.: " + viewId
            // + "\r\n----------------------------------------------------"
            ;

        } catch (Exception ex) {
            //nic
        }

        return ret;
    }


    public static HttpServletRequest getRequest() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        if (ctx == null) return null;
        ExternalContext ectx = ctx.getExternalContext();
        if (ectx == null) return null;
        HttpServletRequest request = (HttpServletRequest) ectx.getRequest();
        return request;
    }

    public static HttpServletResponse getResponse() {
        FacesContext ctx = FacesContext.getCurrentInstance();
        if (ctx == null) return null;
        ExternalContext ectx = ctx.getExternalContext();
        if (ectx == null) return null;
        HttpServletResponse response = (HttpServletResponse) ectx.getResponse();
        return response;
    }


    public static void updateComponent(String componentClientId) {
        if (componentClientId == null) return;
        //RequestContext.getCurrentInstance().update(componentClientId);//deprecated
        //PrimeFaces.current().ajax().update("foo:bar");PF  >= 6.2 ale tego jeszcze nie mamy i nei wiadomo czy dziala jak trzeba

        Collection<String> renderIds = FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds(); //
        if (!renderIds.contains(componentClientId)) { //ArrayList<String>
            renderIds.add(componentClientId); //popraweny update dla uic JSF, ale zadziała jeśli jest jeszcze przed phase 6 (Render Response)
        }
    }


//	public static void updateCssClassComponents(UIComponent uic, String cssClass){
//		List<UIComponent> myComponents = getCssClassComponents(uic, cssClass);
//
//		for (UIComponent uic1 : myComponents) {
//			updateComponent(uic1.getClientId());
//		}
//
//		//alternartywa - ustalanie idków do update'ów przez jQuery jak w Primefaces:
//		//https://stackoverflow.com/questions/20080861/how-do-primefaces-selectors-as-in-update-myclass-work
//	}


//	public static List<UIComponent> getCssClassComponents(String cssClass) {
//		return getCssClassComponents(null, cssClass);
//	}

//	public static List<UIComponent> getCssClassComponents(UIComponent parent, String cssClass) {
//		FacesContext ctx = FacesContext.getCurrentInstance();
//		if (ctx == null) return new ArrayList<>();
//
//		if (parent == null)
//			parent = ctx.getViewRoot();
//
//		CssStyleClassVisitCallback cssStyleClassVisitCallback = new CssStyleClassVisitCallback(cssClass);
//		parent.visitTree(VisitContext.createVisitContext(ctx), cssStyleClassVisitCallback);
//		return cssStyleClassVisitCallback.getMyComponents();
//	}


//	private void addResponseUpdate(String clientId) {
////					RequestContext.getCurrentInstance().update( clientId ); //PF < 6.2, aktualnie deprecated
////					PrimeFaces.current().ajax().update("foo:bar");PF  >= 6.2 ale tego jeszcze nie mamy
//		Collection<String> renderIds = FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds();
//		if (!renderIds.contains(clientId)) {
//			renderIds.add( clientId ); //JSF
//		}
//	}

    public static String lpad(Object x, int length) {
        if (x instanceof Number)
            return lpad(x, length, '0');
        else
            return lpad(x, length, ' ');
    }

    public static String lpad(Object x, int length, char c) {
        if (x == null)
            return null;

        String inputString = x.toString();

        if (inputString.length() >= length) {
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        while (sb.length() < length - inputString.length()) {
            sb.append(c);
        }
        sb.append(inputString);

        return sb.toString();

    }


    public static String rpad(Object x, int length) {
        if (x instanceof Number)
            return rpad(x, length, '0');
        else
            return rpad(x, length, ' ');
    }

    public static String rpad(Object x, int length, char c) {
        if (x == null)
            return null;

        String inputString = x.toString();

        if (inputString.length() >= length) {
            return inputString;
        }
        StringBuilder sb = new StringBuilder();
        sb.append(inputString);

        while (sb.length() < length) {
            sb.append(c);
        }
        return sb.toString();
    }


    public static void redirectTo(String redirectToPage) {
        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect(redirectToPage);
        } catch (IOException e) {
            Log.debug(e.getMessage(), e);
            User.alert(e.getMessage());
        }
    }


    public static ZipOutputStream addToZipFile(byte[] data, String fileName, ZipOutputStream zos) throws IOException {

        InputStream is = new ByteArrayInputStream(data);
        ZipEntry zipEntry = new ZipEntry(fileName);
        zos.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = is.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }
        zos.closeEntry();
        is.close();

        return zos;
    }


    public static PipedInputStream asPipedInputStream(byte[] b) throws IOException {

        PipedInputStream in = new PipedInputStream();

        PipedOutputStream out = new PipedOutputStream(in);

        Thread thread = new Thread(new Runnable() {

            @Override
            public void run() {
                try {
                    out.write(b);
                    out.close();
                } catch (IOException e) {
                    //					log.error("An error has occured while exporting report", e);
                    System.out.println("!!!PipedInputStream-> " + e.getMessage());
                }
            }
        });

        thread.start();

        return in;
    }

    public static DefaultStreamedContent asStreamedContent(byte[] bytes, String contentType, String fileName) throws IOException {
        //DefaultStreamedContent ret = new DefaultStreamedContent(Utils.asPipedInputStream(bytes), "application/pdf", "testLwListRpt.pdf");
        DefaultStreamedContent ret = new DefaultStreamedContent(Utils.asPipedInputStream(bytes), contentType, fileName);
        return ret;
    }


    public static Object evaluateExpressionGet(String elExpression) {
        if (elExpression == null || elExpression.isEmpty()) return null;
        FacesContext ctx = FacesContext.getCurrentInstance();
        if (ctx == null) return null;
        return ctx.getApplication().evaluateExpressionGet(ctx, elExpression, Object.class);
    }


    public static SqlDataSelectionsHandler getLW(String sqlId) {
        if (FacesContext.getCurrentInstance()==null)
            return AppBean.sqlBean.getLW(sqlId);

        User u = User.getCurrentUser();
        if(u ==null)
            return AppBean.sqlBean.getLW(sqlId);
        else
            return u.getLW(sqlId);
    }

    public static SqlDataSelectionsHandler getLW(String sqlId, Object... params) {
        if (FacesContext.getCurrentInstance()==null)
			return AppBean.sqlBean.getLW(sqlId, params);


        User u = User.getCurrentUser();
        if(u ==null)
            return AppBean.sqlBean.getLW(sqlId);
        else
            return u.getLW(sqlId, params);
    }

    public static SqlDataSelectionsHandler getLW(String sqlId, ArrayList<Object> params) {
        if (FacesContext.getCurrentInstance()==null)
            return AppBean.sqlBean.getLW(sqlId, params);

        User u = User.getCurrentUser();
        if(u ==null)
            return AppBean.sqlBean.getLW(sqlId);
        else
            return u.getLW(sqlId, params);
    }

    public static SqlDataSelectionsHandler getLW(String sqlId, HashMap<String, Object> namedParams) {
        if (FacesContext.getCurrentInstance()==null)
            return AppBean.sqlBean.getLW(sqlId, namedParams);

        User u = User.getCurrentUser();
        if(u ==null)
            return AppBean.sqlBean.getLW(sqlId);
        else
            return u.getLW(sqlId, namedParams);
    }

    public static SqlDataSelectionsHandler getSL(String sqlId) {
        if (FacesContext.getCurrentInstance()==null)
            return AppBean.sqlBean.getSL(sqlId);

        User u = User.getCurrentUser();
        if(u ==null)
            return AppBean.sqlBean.getSL(sqlId);
        else
            return u.getSL(sqlId);
    }


    private static DataRow getKursNaDzienDataRow(Object walId, Date naDzien, SqlDataSelectionsHandler lwKursyWalut) {
        ArrayList<DataRow> kursyWalut = lwKursyWalut.getData();
        for (DataRow r : kursyWalut) {

            if (!("" + walId).equals(r.get("KW_WAL_Z_ID").toString()))
                continue; //nie ta waluta

            if (!naDzien.after((Date) r.get("KW_DATA")) && !naDzien.equals(r.get("KW_DATA")))
                continue; //dzien dla wiersza nie jest pozniejszy niz wskazany

            return r;
        }

        return null;//brak wiersza dla okreslonej waluty na dany dzien...
    }


    public static Double kursPrzelicznikSredniNBP(Object waluta, Date naDzien) {
        DataRow r = kursPrzelicznikSredniNBPRow(waluta, naDzien);
        if (r != null) {
            return r.getAsDouble("KW_PRZELICZNIK_SREDNI");
        }
        return 1.0;
    }


    public static DataRow kursPrzelicznikSredniNBPRow(Object walId, Date naDzien) {
        if (naDzien == null)
            naDzien = today();

        SqlDataSelectionsHandler lwKursyWalut = getLW("PPL_DEL_KURS_SREDNI");
        return getKursNaDzienDataRow(walId, naDzien, lwKursyWalut);
    }


    public static Double kursPrzelicznikSprzedazyNBP(Object waluta, Date naDzien) {
        DataRow r = kursPrzelicznikSprzedazyNBPRow(waluta, naDzien);
        if (r != null) {
            return r.getAsDouble("KW_PRZELICZNIK_SPRZEDAZY");
        }
        return 1.0;
    }


    public static DataRow kursPrzelicznikSprzedazyNBPRow(Object walId, Date naDzien) {
        if (naDzien == null)
            naDzien = today();

        SqlDataSelectionsHandler lwKursyWalut = getLW("PPL_DEL_KURS_SPRZEDAZY");
        return getKursNaDzienDataRow(walId, naDzien, lwKursyWalut);
    }


    public static String getWalSymbol(Object walId) {
        if (walId == null) return null;
        return getLW("WAL").findAndFormat(walId, "%wal_symbol$s");
    }

    public static Long getWalId(String walSymbol) {
        if (walSymbol == null || walSymbol.isEmpty()) return null;
        //getLW("WAL").getData();
        DataRow dr = getLW("WAL").getAllData()
                .stream()
                .filter(r -> eq(r.get("wal_symbol"), walSymbol))
                .findFirst()
                .orElse(null);

        return dr != null ? dr.getIdAsLong() : null;
    }


    public static String getKrajSymbol(Object krajId) {
        return getLW("PPL_WSP_KRAJE").findAndFormat(krajId, "%kr_symbol$s");
    }

    public static String getKrajNazwa(Object krajId) {
        return getLW("PPL_WSP_KRAJE").findAndFormat(krajId, "%kr_nazwa$s");
    }


    public static int getQarterValue(LocalDate date) {
        int[] mont2Qurters = {0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4};
        return mont2Qurters[date.getMonthValue()];
    }


    public static Double dopelnijDoNominalWalutyNBP(Double wartosc, Long walId) {
        //zwr. wartosc powiększoną do minimlanego nominału waluty stosowanego przez NBP

        Double ret = wartosc;
        wartosc = nz(wartosc);
        walId = nz(walId);
        SqlDataSelectionsHandler lwNominalyNBP = getLW("PPL_SLOWNIK", "DEL_NOMINAL_ZAOKR");
        SqlDataSelectionsHandler lwWaluty = getLW("PPL_SYMBOLE_WALUT");
        String symbolWaluty = nz(lwWaluty.findAndFormat(walId,"%wal_symbol2$s"));
//        String symbolWaluty = nz((String) rwaluta.get("WAL_SYMBOL2"));

        DataRow r = lwNominalyNBP.find(symbolWaluty);
        if (r == null)
            return wartosc;

        String strstep = ("" + r.get("wsl_wartosc_zewn")).replaceAll(",", ".");
        Double step = Double.parseDouble(strstep);
        long lret = (long) (wartosc * 10000);
        long lstep = (long) (step * 10000);
        ret = (lstep * Math.ceil((1.0 * lret) / lstep)) / 10000;
        return ret;

    }

    public static AppBean app() {
        return AppBean.app;
    }


    public static void saveBlobAsFile(Blob blob, String file) throws SQLException, IOException {
        InputStream in = blob.getBinaryStream();
        FileOutputStream out = new FileOutputStream(file, false);
        byte[] buff = new byte[4096];  // how much of the blob to read/write at a time
        int len = 0;
        while ((len = in.read(buff)) != -1) {
            out.write(buff, 0, len);
        }
        out.flush();
        out.close();
    }

    public static void setFormName(String txt) {
        try {
            com.comarch.egeria.Utils.executeScript("parent.setFormName('" + txt + "')");
        } catch (Exception e) {
            Log.error(e.getMessage(), e);
        }
    }


    public static byte[] getBytes(InputStream is) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[16384];

        while ((nRead = is.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        return buffer.toByteArray();
    }


    public static void executeScript(String script) {
        PrimeFaces.current().executeScript(script);
    }


    public static void update(Collection<String> clientIds) {
        PrimeFaces.current().ajax().update(clientIds);
        //albo iteracja z //updateComponent(clientId);
    }


    public static void update(String... clientIds) {
        PrimeFaces.current().ajax().update(clientIds);
        //albo iteracja z //updateComponent(clientId);
    }


	public static Resource getResource(String resourcePth) {
		return AppBean.app.getAppContext().getResource(resourcePth);
	}

	public static File getResourceFile(String resourcePth) throws IOException {
		return getResource(resourcePth).getFile();
	}

	public static InputStream getResourceInputStream(String resourcePth) throws IOException {
		return getResource(resourcePth).getInputStream();
	}

	public static Path getResourcePath(String resourcePth) throws IOException {
		return getResourceFile(resourcePth).toPath();
	}

	public static String getResourceAsString(String resourcePth, Charset charset) throws IOException {
		Resource resource = getResource(resourcePth);//sciezka wzgledna, od /WEB-INF/... np. "/WEB-INF/protectedElements/SQL/dmp_sl/sl.TEST12345.sql"
		Path path = resource.getFile().toPath();//pelna sciezka do pliku: "C:/ .... /WEB-INF/protectedElements/SQL/dmp_sl/sl.TEST12345.sql"

        if (!Files.exists(path)) return null;

		byte[] bytes = Files.readAllBytes(path);
		String ret = new String(bytes, charset);
		return ret;
	}

	public static String getResourceAsString(String resourcePth, String charsetName) throws IOException {
		Charset charset = Charset.forName(charsetName);
		return getResourceAsString(resourcePth, charset);
	}

	public static String getResourceAsString(String resourcePth) throws IOException {
		return getResourceAsString(resourcePth, StandardCharsets.UTF_8);
	}




    public static boolean matchFilterExpressionsWordsObProperties(String[] filterFiledWordsLC, Object ... filteredObjectPropertyValues){
        for (String w : filterFiledWordsLC) {
            if (!matchFilterExpressionsWordObProperty(w, filteredObjectPropertyValues)) return false;
        }
        return true;
    }

    public static boolean matchFilterExpressionsWordObProperty(String filterFiledWordLC, Object ... filteredObjectPropertyValues){
        for (Object p : filteredObjectPropertyValues) {
            if ((""+p).toLowerCase().contains(filterFiledWordLC) ) return true;
        }
        return false;
    }


    public static <T> T coalesce(T ...items) {
        for(T i : items) if(i != null) return i;
        return null;
    }


    public static <T> T  nvl(T obj, T returnIfObjNull){
        return (obj != null)  ? obj : returnIfObjNull;
    }


    public static Date lastMinuteOfDay(Date day) {

        if(day != null) {
            Calendar calendar = Calendar.getInstance();

            calendar.setTime(day);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
//            calendar.set(Calendar.SECOND, 59);
//            calendar.set(Calendar.MILLISECOND, 999);

            return calendar.getTime();
        }

        return today();
    }


    //    public static long getCRC(File file) throws IOException {
//        long ret = getCRC(Files.readAllBytes(file.toPath()));
//        System.out.println(lpad(ret,30) + " " +  file.getName());
//        return ret;
//    }


    public static long getCRC(byte[] bytes) {
        Checksum crc32 = new CRC32();
        crc32.update(bytes, 0, bytes.length);
        return crc32.getValue();
    }





    public static String getFileChecksum(MessageDigest digest, File file) throws IOException
    {
        //Get file input stream for reading the file content
        FileInputStream fis = new FileInputStream(file);

        //Create byte array to read data in chunks
        byte[] byteArray = new byte[1024];
        int bytesCount = 0;

        //Read file data and update in message digest
        while ((bytesCount = fis.read(byteArray)) != -1) {
            digest.update(byteArray, 0, bytesCount);
        }

        //close the stream; We don't need it now.
        fis.close();

        //Get the hash's bytes
        byte[] bytes = digest.digest();

        //This bytes[] has bytes in decimal format;
        //Convert it to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for(int i=0; i< bytes.length ;i++)
        {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        //return complete hash
        return sb.toString();
    }



    public static String strTS() {
        return Utils.format(LocalDateTime.now(), "yyyy.MM.dd HH:mm +ss.SSS") + "[s]";
    }

    public static LocalDateTime tsDurationStart = LocalDateTime.now();
    public static String strTS(boolean showDuration) {
        LocalDateTime now = LocalDateTime.now();
        String ret = Utils.format(now, "yyyy.MM.dd HH:mm +ss.SSS") + "[s]";

        long duration = ChronoUnit.MILLIS.between(tsDurationStart, now);
        tsDurationStart = now;

        if (!showDuration)
            ret += "  ( -------------> t0 )";
        else//       =(t0 + 1234567890[ms])
            ret += " =(t0 + " + lpad(duration,10, ' ') + "[ms])";

        return ret;
    }

    public static Path getAbsolutePath(String... paths) {
        return Paths.get(AppBean.app.getApplicationContextFolder().getAbsolutePath(), paths);
    }

    public static void renameAllFilesLowerCase(Path folderPath) throws IOException {
            System.out.println(" ? files to lowercase / " + folderPath + " ...");
            List<Path> pths = Files.walk(folderPath) //.filter( ... ).sorted((p1, p2) -> p1.toFile().toString().compareTo(p2.toFile().toString()))
                    .collect(Collectors.toList());

            for (Path pth : pths) {
                File file = pth.toFile();
                if (!file.isDirectory() && !eq(file.getName(), file.getName().toLowerCase())){
                    System.out.println(pth+" -rename-> "+ file.getName().toLowerCase());
//						Files.move(pth, pth.resolveSibling(file.getName().toLowerCase()));//nie działa w Windows (jesli nazwy roznia sie tylko wielkoscia znakow)...
                    pth.toFile().renameTo(pth.resolveSibling(file.getName().toLowerCase()).toFile());
                }
            }
    }


//    public static long getCRC32(Resource resourceFile) throws IOException {
//        return getCRC32(resourceFile.getInputStream(), 16000);
//    }

//    public static long getCRC32(InputStream stream, int bufferSize) throws IOException {
//        CheckedInputStream checkedInputStream = new CheckedInputStream(stream, new CRC32());
//        byte[] buffer = new byte[bufferSize];
//        while (checkedInputStream.read(buffer, 0, buffer.length) >= 0) {}
//        return checkedInputStream.getChecksum().getValue();
//    }



    public static DataRow getPplKadPracownicyRow(Object prcId) {
        if (prcId==null) return null;
        SqlDataSelectionsHandler lwPrc = getLW("PPL_KAD_PRACOWNICY");
        if (lwPrc == null) return null;
        return lwPrc.find(prcId);
    }

    public static DataRow getPrcDepartamentRow(Object prcId){
        DataRow kdprcrow = getPplKadPracownicyRow(prcId);
        if (kdprcrow==null) return null;
        SqlDataSelectionsHandler lwOB = getLW("OB");
        if (lwOB==null) return null;
        return lwOB.find(kdprcrow.get("dep_id"));
    }




}
