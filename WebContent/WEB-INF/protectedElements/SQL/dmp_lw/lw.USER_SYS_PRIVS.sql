-- data version: USER_SYS_PRIVS 2020.03.30 12:47.27
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:47:51
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='USER_SYS_PRIVS';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'USER_SYS_PRIVS' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'USER_SYS_PRIVS';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('USER_SYS_PRIVS')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','USER_SYS_PRIVS','','50','username','T','N','username','-314050','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-12-03 16:13:21','2020-03-30 12:47:27','10.132.216.41\NIEZNANY','0:0:0:0:0:0:0:1\NIEZNANY','8','100','N','','0','N','T','T','T','T','2019-12-03 16:24:58','T','0','2020-05-08 12:55:38','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'SELECT * FROM USER_SYS_PRIVS' 
WHERE      LST_ID = -314050;
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('2','USER_SYS_PRIVS','','300','privilege','T','N','privilege','-314051','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-12-03 16:13:21','2019-12-03 16:24:58','10.132.216.41\NIEZNANY','0:0:0:0:0:0:0:1\NIEZNANY','3','100','N','-314050','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('3','USER_SYS_PRIVS','','100','admin_option','T','N','admin_option','-314052','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-12-03 16:13:21','2019-12-03 16:24:58','10.132.216.41\NIEZNANY','0:0:0:0:0:0:0:1\NIEZNANY','3','100','N','-314050','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('4','USER_SYS_PRIVS','','100','common','T','N','common','-314053','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-12-03 16:13:21','2019-12-03 16:24:58','10.132.216.41\NIEZNANY','0:0:0:0:0:0:0:1\NIEZNANY','3','100','N','-314050','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'USER_SYS_PRIVS' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
