-- data version: PPL_KAD_PREMIE_DA_OS_WN 2019.07.10 17:54.41
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:44:58
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_KAD_PREMIE_DA_OS_WN';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_KAD_PREMIE_DA_OS_WN' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_KAD_PREMIE_DA_OS_WN';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_KAD_PREMIE_DA_OS_WN')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_KAD_PREMIE_DA_OS_WN','Dane osobowe wniosku o premi�','50','Id','T','N','zat_id','-687','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:33','2019-07-10 17:54:41','PCMGOLDA\Mikolaj.Golda','10.132.216.41\NIEZNANY','8','50','N','','0','N','N','N','N','N','','','','2020-05-11 19:09:38','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'select
 ekz.zat_id,
 prc.PRC_IMIE,
 prc.PRC_NAZWISKO,
 stn.STN_NAZWA
 
from ek_zatrudnienie ekz
      join ek_pracownicy prc on ekz.ZAT_PRC_ID = prc.prc_id
      join zpt_stanowiska stn on stn.STN_ID=ekz.ZAT_STN_ID
      where (NVL (zat_data_do, SYSDATE) >= SYSDATE) AND (zat_data_zmiany <= LAST_DAY (SYSDATE))' 
WHERE      LST_ID = -687;
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('2','PPL_KAD_PREMIE_DA_OS_WN','','50','Imi�','T','N','prc_imie','-688','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:33','2019-02-04 14:10:59','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','50','N','-687','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('3','PPL_KAD_PREMIE_DA_OS_WN','','50','Nazwisko','T','N','prc_nazwisko','-689','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:33','2019-02-04 14:10:59','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','50','N','-687','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('4','PPL_KAD_PREMIE_DA_OS_WN','','50','Nazwa stanowiska','T','N','stn_nazwa','-690','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:33','2019-02-04 14:10:59','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','50','N','-687','','','','','','','','','','','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_KAD_PREMIE_DA_OS_WN' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-757','-687','EGADM1.EK_PRACOWNICY','EGADM1','EK_PRACOWNICY','PUBLIC','EK_PRACOWNICY','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-758','-687','EGADM1.EK_ZATRUDNIENIE','EGADM1','EK_ZATRUDNIENIE','PUBLIC','EK_ZATRUDNIENIE','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-759','-687','EGADM1.ZPT_STANOWISKA','EGADM1','ZPT_STANOWISKA','PUBLIC','ZPT_STANOWISKA','SYNONYM','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
