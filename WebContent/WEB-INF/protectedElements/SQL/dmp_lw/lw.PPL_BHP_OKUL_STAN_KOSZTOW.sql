------------------------------------------------------------------------------------------
-- DMP_LW: PPL_BHP_OKUL_STAN_KOSZTOW
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_BHP_OKUL_STAN_KOSZTOW';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_BHP_OKUL_STAN_KOSZTOW' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_BHP_OKUL_STAN_KOSZTOW';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_BHP_OKUL_STAN_KOSZTOW')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_BHP_OKUL_STAN_KOSZTOW','�r�d�o finansowania','50','Id','T','T','sk_id','-215','','Zmiana nazwy listy ','PPL_BHP_OKUL_STAN_KOSZTOW','','','do_weryfikacji','SQLOK','BHP','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:30','2020-07-03 14:17:49','PCMGOLDA\Mikolaj.Golda','10.132.216.95\tomcat','237','50','N','','0','N','N','N','N','N','2020-07-03 14:17:49','N','0','2020-07-03 12:50:47','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'select sk_id, sk_kod, sk_opis, sk_wymiar14, lp from (

  select sk.*, rownum lp -- sk_id, sk_kod, sk_opis,  nvl(sk_wymiar14, ''00.00.00.00'') sk_wymiar14 
  from EGADM1.CSS_STANOWISKA_KOSZTOW sk
  where sk.SK_WYMIAR14 in ( 
      
      select tss.tss_kol2 --tss.*,ob.OB_PELNY_KOD, zat_prc_id , prc_imie, prc_nazwisko
      from ek_pracownicy prc 
        join ek_zatrudnienie zat on prc_id = zat_prc_id and zat.zat_typ_umowy=0 and ZAT_PRC_ID = :P_PRC_ID and :P_DATA_WNO between  zat_data_zmiany and NVL (zat_data_do, SYSDATE) 
        join EGADM1.EK_OBIEKTY_W_PRZEDSIEB dep on nvl(ppadm.get_departament_id( zat.ZAT_OB_ID), 0) = dep.OB_ID 
        --join EGADM1.EKT_TABELE_STAWEK_STAWKI tss on dep.OB_PELNY_KOD = tss.tss_kol1_dsp --tss.tss_tso_id = 4771 and TSS_KOL1_DSP like ''MF-%'' and
        join EGADM1.EKT_TABELE_STAWEK_STAWKI tss on dep.ob_id = tss.tss_kol1    
        join egadm1.EKT_TABELE_STAWEK_OKRESY tso on tss.tss_tso_id = tso.tso_id and :P_DATA_WNO between tso.TSO_DATA_OD and nvl(tso.tso_data_do, sysdate) --TODO ustawic parametr
        --join egadm1.EKT_TYPY_TABEL_STAWEK tts on tso.tso_tts_id = tts.tts_id and tts_kod = ''JO_STK'' --Przyporz�dkowanie stanowisk koszt�w do jednostki organizacyjnej
        join egadm1.EKT_TYPY_TABEL_STAWEK tts on tso.tso_tts_id = tts.tts_id and tts_kod = ''JO_BZ'' --Przyporz�dkowanie stanowisk koszt�w do jednostki organizacyjnej
  )
  
  union all
  
  select sk.*, 9999999 
  from EGADM1.CSS_STANOWISKA_KOSZTOW sk
    --where sk_kod like ''PL-19-C-12-75001-3020-BDG-000-00-206-0194-00-000-000-00.00.00.00-00N-BDG-00'' -- sk_id = 106561
  where sk_kod in (SELECT WSL_WARTOSC FROM PPT_ADM_WARTOSCI_SLOWNIKOW WHERE WSL_SL_NAZWA = ''BHP_DOMYSLNY_SK'' and wsl_lp = 1)

) x
order by x.lp

' 
WHERE      LST_ID = -215;
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('2','PPL_BHP_OKUL_STAN_KOSZTOW','','50','Kod','T','N','sk_kod','-216','','','','','','do_weryfikacji','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:30','2020-07-03 14:17:49','PCMGOLDA\Mikolaj.Golda','10.132.216.95\tomcat','12','50','N','-215','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('3','PPL_BHP_OKUL_STAN_KOSZTOW','','50','Opis','T','N','sk_opis','-217','','','','','','do_weryfikacji','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:30','2020-07-03 14:17:49','PCMGOLDA\Mikolaj.Golda','10.132.216.95\tomcat','12','50','N','-215','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('4','PPL_BHP_OKUL_STAN_KOSZTOW','','50','Kod bud�etu zadaniowego','T','T','sk_wymiar14','-311171','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:30','2020-07-03 14:17:49','PCMGOLDA\Mikolaj.Golda','10.132.216.95\tomcat','12','50','N','-215','','','','','','','','','','','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_BHP_OKUL_STAN_KOSZTOW' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-373','-215','EGADM1.CSS_OBIEKTY_W_PRZEDSIEB','EGADM1','CSS_OBIEKTY_W_PRZEDSIEB','EGADM1','EK_OBIEKTY_W_PRZEDSIEB','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-374','-215','EGADM1.CSS_STANOWISKA_KOSZTOW','EGADM1','CSS_STANOWISKA_KOSZTOW','PPADM','PPV_LW_COMPILE_DEPENDENCIES','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-375','-215','EGADM1.CSS_TYPY_OBIEKTOW','EGADM1','CSS_TYPY_OBIEKTOW','EGADM1','EK_OBIEKTY_W_PRZEDSIEB','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-376','-215','EGADM1.EK_PRACOWNICY','EGADM1','EK_PRACOWNICY','PUBLIC','EK_PRACOWNICY','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-377','-215','EGADM1.EKT_TABELE_STAWEK_STAWKI','EGADM1','EKT_TABELE_STAWEK_STAWKI','PPADM','PPV_LW_COMPILE_DEPENDENCIES','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-378','-215','EGADM1.EK_ZATRUDNIENIE','EGADM1','EK_ZATRUDNIENIE','PUBLIC','EK_ZATRUDNIENIE','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-11166','-215','EGADM1.EKT_TABELE_STAWEK_OKRESY','EGADM1','EKT_TABELE_STAWEK_OKRESY','PPADM','PPV_LW_COMPILE_DEPENDENCIES','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-11167','-215','EGADM1.EKT_TYPY_TABEL_STAWEK','EGADM1','EKT_TYPY_TABEL_STAWEK','PPADM','PPV_LW_COMPILE_DEPENDENCIES','VIEW','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
