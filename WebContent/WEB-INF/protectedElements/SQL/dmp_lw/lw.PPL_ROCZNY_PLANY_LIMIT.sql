------------------------------------------------------------------------------------------
-- DMP_LW: PPL_ROCZNY_PLANY_LIMIT
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_ROCZNY_PLANY_LIMIT';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_ROCZNY_PLANY_LIMIT' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_ROCZNY_PLANY_LIMIT';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_ROCZNY_PLANY_LIMIT')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_ROCZNY_PLANY_LIMIT','','50','LD_DNI','T','N','LD_DNI','-311905','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2020-06-25 14:34:56','2020-06-25 14:34:56','NBMDZIOBEK\Marcin.Dziobek','NBMDZIOBEK\Marcin.Dziobek','2','50','N','','0','N','N','N','N','N','','','','2020-06-23 14:35:49','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'SELECT * FROM
(SELECT sum(LD_DNI) LD_DNI

FROM (
SELECT 
      lald.LD_ID,
      lald.LD_DNI,
      RDA_KOD,
      lald.la_dg_kod
FROM EK_RODZAJE_ABSENCJI
  join ( 
                select * 
                --uwaga
                from  (SELECT GA_DG_KOD, MIN(GA_RDA_ID) GA_RDA_ID FROM EK_GRUPY_ABSENCJI GROUP BY GA_DG_KOD)
                   join EK_DEF_GRUP on  ga_dg_kod = dg_kod and dg_dk_kod = ''LIMITY''--ga_numer=1 and
       ) gadg  on rda_id = ga_rda_id

join ek_pracownicy prc on prc.prc_id = ?

join ( 
              select * 
              from EK_LIMITY_ABSENCJI
                join EK_LIMITY_DANE on la_id = ld_la_id
              where la_rok =  ?

     ) lald on lald.la_prc_id = prc.prc_id and gadg.dg_kod = lald.la_dg_kod
join PPADM.PPV_LA_PRC_LAST lav on lav.LA_DG_KOD=lald.la_dg_kod and lav.LA_PRC_ID=prc.prc_id and lav.LA_ROK=lald.la_rok and lav.MAX_LD_ID=lald.ld_id
WHERE lald.la_dg_kod=''A_UR1''  
      or lald.la_dg_kod=''UR_USC''
      or lald.la_dg_kod=''UR91''
      or lald.la_dg_kod=''URL_D_FCS''
      or lald.la_dg_kod=''URL_WET''
      or lald.la_dg_kod=''URL_KOM''
      or lald.la_dg_kod=''URL_NAGR''
)
) limity
--join (SELECT PPADM.GET_NORMA(?, TO_DATE( ?||''/06/01'', ''yyyy/mm/dd'') ) godzin FROM dual) norma on 1=1
' 
WHERE      LST_ID = -311905;
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_ROCZNY_PLANY_LIMIT' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-853','-311905','EGADM1.EK_RODZAJE_ABSENCJI','EGADM1','EK_RODZAJE_ABSENCJI','PUBLIC','EK_RODZAJE_ABSENCJI','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-852','-311905','EGADM1.EK_PRACOWNICY','EGADM1','EK_PRACOWNICY','PUBLIC','EK_PRACOWNICY','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-851','-311905','EGADM1.EK_LIMITY_DANE','EGADM1','EK_LIMITY_DANE','PUBLIC','EK_LIMITY_DANE','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-847','-311905','EGADM1.EK_LIMITY_ABSENCJI','EGADM1','EK_LIMITY_ABSENCJI','PUBLIC','EK_LIMITY_ABSENCJI','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-846','-311905','EGADM1.EK_GRUPY_ABSENCJI','EGADM1','EK_GRUPY_ABSENCJI','PUBLIC','EK_GRUPY_ABSENCJI','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-845','-311905','EGADM1.EK_DEF_GRUP','EGADM1','EK_DEF_GRUP','PUBLIC','EK_DEF_GRUP','SYNONYM','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
