-- data version: PPL_ZFSS_WYP_KWOTY 2020.04.22 21:41.09
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:47:13
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_ZFSS_WYP_KWOTY';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_ZFSS_WYP_KWOTY' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_ZFSS_WYP_KWOTY';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_ZFSS_WYP_KWOTY')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_ZFSS_WYP_KWOTY','','50','zarobki_od','T','N','zarobki_od','-313500','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-08-27 11:31:30','2020-04-22 21:41:09','0:0:0:0:0:0:0:1\NIEZNANY','0:0:0:0:0:0:0:1\NIEZNANY','44','100','N','','0','N','T','T','T','T','2019-09-17 14:24:03','T','1','2020-04-22 21:41:09','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'SELECT * FROM(
SELECT 0    zarobki_od, 2000 zarobki_do, 1900 wysokosc_dofi_dla_pracownika, 800 wysokosc_dofi_dla_dziecka FROM dual
union all
SELECT 2001 zarobki_od, 3000 zarobki_do, 1600 wysokosc_dofi_dla_pracownika, 700 wysokosc_dofi_dla_dziecka FROM dual
union all
SELECT 3001 zarobki_od, 4000 zarobki_do, 1400 wysokosc_dofi_dla_pracownika, 600 wysokosc_dofi_dla_dziecka FROM dual
union all
SELECT 4001 zarobki_od, 5000 zarobki_do, 1200 wysokosc_dofi_dla_pracownika, 500 wysokosc_dofi_dla_dziecka FROM dual
union all
SELECT 5001 zarobki_od, 6000 zarobki_do, 1000 wysokosc_dofi_dla_pracownika, 400 wysokosc_dofi_dla_dziecka FROM dual
union all
SELECT 6001 zarobki_od, null zarobki_do, 650  wysokosc_dofi_dla_pracownika, 300 wysokosc_dofi_dla_dziecka FROM dual
)
WHERE ? BETWEEN  zarobki_od and nvl(zarobki_do, 99999999999999999999999999999999999)' 
WHERE      LST_ID = -313500;
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('2','PPL_ZFSS_WYP_KWOTY','','100','zarobki_do','T','N','zarobki_do','-313501','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-08-27 11:31:30','2019-09-17 14:24:03','0:0:0:0:0:0:0:1\NIEZNANY','0:0:0:0:0:0:0:1\NIEZNANY','5','100','N','-313500','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('3','PPL_ZFSS_WYP_KWOTY','','100','wysokosc_dofi_dla_pracownika','T','N','wysokosc_dofi_dla_pracownika','-313502','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-08-27 11:31:30','2019-09-17 14:24:03','0:0:0:0:0:0:0:1\NIEZNANY','0:0:0:0:0:0:0:1\NIEZNANY','5','100','N','-313500','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('4','PPL_ZFSS_WYP_KWOTY','','100','wysokosc_dofi_dla_dziecka','T','N','wysokosc_dofi_dla_dziecka','-313503','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-08-27 11:31:30','2019-09-17 14:24:03','0:0:0:0:0:0:0:1\NIEZNANY','0:0:0:0:0:0:0:1\NIEZNANY','5','100','N','-313500','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_ZFSS_WYP_KWOTY' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
