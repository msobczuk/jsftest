-- data version: PPL_ABS_LIMITY_ROK 2020.03.31 11:24.13
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:45:53
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_ABS_LIMITY_ROK';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_ABS_LIMITY_ROK' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_ABS_LIMITY_ROK';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_ABS_LIMITY_ROK')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_ABS_LIMITY_ROK','','50','rda_kod','T','N','rda_kod','-313156','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-06-12 10:42:03','2020-03-31 11:24:13','10.132.216.96\NIEZNANY','10.132.216.95\NIEZNANY','66','100','N','','0','N','T','T','T','T','2020-03-31 11:24:13','N','0','2020-06-12 10:53:19','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'SELECT 
 --decode (ZEGR.RDA_KOD, ''UW'', ''UUW'', ''U�'', ''UUW'', ZEGR.RDA_KOD)  rda_kod1,
  ZEGR.RDA_KOD RDA_KOD,
  ZEGR.RDA_NAZWA RDA_NAZWA,
  ZEGR.RDA_ID RDA_ID,
  ZEGR.dg_kod dg_kod,
  ZEGR.dg_nazwa dg_nazwa, 
  nvl(ZEGR.Biez_dni,0) przyslugujacy,
  nvl(ZEGR.Wymiar,0) biezacy,
  nvl(ZEGR.Zal_dni,0) zalegly,
   case 
    when RDA_NAZWA like ''%godz%'' then case when (nvl(ZEGR.Pozost_dni,0) - nvl(ZAWNIOSKOWANE_NIE_POTWIERDZONE.lzdh,0))<0 then 0 else nvl(ZEGR.Pozost_dni,0) - nvl(ZAWNIOSKOWANE_NIE_POTWIERDZONE.lzdh,0) end
         else                         case when (nvl(ZEGR.Pozost_dni,0) - nvl(ZAWNIOSKOWANE_NIE_POTWIERDZONE.lzdd,0))<0 then 0 else nvl(ZEGR.Pozost_dni,0) - nvl(ZAWNIOSKOWANE_NIE_POTWIERDZONE.lzdd,0) end
  end dni_do_wykorzystania, 
  case 
     when RDA_NAZWA like ''%godz%'' then nvl(ZAWNIOSKOWANE_NIE_POTWIERDZONE.lzdh,0)
        else                           nvl(ZAWNIOSKOWANE_NIE_POTWIERDZONE.lzdd,0)
  end dni_juz_zawnioskowanych,
  case 
     when RDA_NAZWA like ''%godz%'' then nvl(ZATWIERDZONE.lzdh,0)
        else                           nvl(ZATWIERDZONE.lzdd,0)
  end zatwierdzone,
  nvl(ZEGR.Pozost_dni,0)  dni_do_wykorzystania_EG,
  zegr.rda_kod_serwis
FROM
  (
  
  SELECT
    RDA_KOD RDA_KOD,
    RDA_NAZWA RDA_NAZWA,
    RDA_ID RDA_ID,
    dg_kod dg_kod,
    dg_nazwa dg_nazwa,
    ld_od Od_kiedy,
    ld_do Do_kiedy,
    ld_wymiar Wymiar,
    round(la_przeniesione/8,2) Zal_dni,  
    ld_dni Biez_dni,  
    ld_wykorzystane "Wykorz.dni", 
    ld_pozostalo Pozost_dni,   
    la_rok,
    prc_id,
    ROW_NUMBER() OVER (PARTITION BY RDA_KOD ORDER BY ld_od desc) as rn,
    rda_kod_serwis

  from -- (select 2018 as rok from dual)  par,
    ek_pracownicy
    join ek_limity_absencji on  la_prc_id=prc_id 
    join ek_limity_dane ld1 on la_id=ld_la_id
    join EK_DEF_GRUP on la_dg_kod = dg_kod
    left join (SELECT GA_DG_KOD, MIN(GA_RDA_ID) GA_RDA_ID FROM EK_GRUPY_ABSENCJI GROUP BY GA_DG_KOD) on  dg_kod = GA_DG_KOD
' 
WHERE      LST_ID = -313156;
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = LST_QUERY ||
'
    left join EK_RODZAJE_ABSENCJI on ga_rda_id = rda_id
  where 1=1 
    and lower (RDA_NAZWA) like ''%urlop%''
    and prc_id= :P_PRC_ID 
    --and TO_DATE(:P_ROK||''-12-31'', ''YYYY-MM-DD'') between ld_od and ld_do
   and :P_ROK between EXTRACT(YEAR FROM ld_od) and EXTRACT(YEAR FROM ld_do)     
  ) ZEGR
left join ( 

            SELECT decode(UPRW_RDA_ID, UZ.RDA_ID, UW.RDA_ID, UPRW_RDA_ID) UPRW_RDA_ID, UPRW_PRC_ID,
             round(sum(  ek_pck_procedury_hi.z_godz_harm_ind( UPRW_PRC_ID, TRUNC(UPRW_DATA_OD), TRUNC(UPRW_DATA_DO))/ek_pck_absencje.il_godz_na_dzien_urlopu(UPRW_PRC_ID, TRUNC(UPRW_DATA_DO))),6)  lzdd,
             round(sum(URLOP_GODZ_LICZBA_GODZIN(UPRW_PRC_ID, UPRW_DATA_OD, UPRW_DATA_DO)/ek_pck_absencje.il_godz_na_dzien_urlopu(UPRW_PRC_ID, TRUNC(UPRW_DATA_DO))),6) lzdh                 
            FROM PPADM.PPT_ABS_URLOPY_WNIOSKOWANE
                 join (SELECT GA_DG_KOD, MIN(GA_RDA_ID) GA_RDA_ID FROM EK_GRUPY_ABSENCJI GROUP BY GA_DG_KOD) on uprw_rda_id=GA_RDA_ID
                 join ek_limity_absencji on LA_DG_KOD = GA_DG_KOD and LA_PRC_ID=uprw_prc_id
                 join ek_limity_dane ld1 on la_id=ld_la_id 
                 left join EK_RODZAJE_ABSENCJI UW on UW.rda_kod_serwis = ''UW''
                 left join EK_RODZAJE_ABSENCJI UZ on UZ.rda_kod_serwis = ''U�''
            where  nvl(UPRW_STATUS,0) >= 0 and nvl(UPRW_STATUS,0) < 10
                   and  EXTRACT(year FROM UPRW_DATA_DO) = :P_ROK
                   and UPRW_DATA_DO <= ld_do
                   and TO_DATE(:P_ROK||''-12-31'', ''YYYY-MM-DD'') between ld_od and ld_do
                   and uprw_prc_id= :P_PRC_ID 
            GROUP BY decode(UPRW_RDA_ID, UZ.RDA_ID, UW.RDA_ID, UPRW_RDA_ID), UPRW_PRC_ID --GROUP BY UPRW_RDA_ID,UPRW_PRC_ID

          ) ZAWNIOSKOWANE_NIE_POTWIERDZONE on ZAWNIOSKOWANE_NIE_POTWIERDZONE.UPRW_RDA_ID = ZEGR.RDA_ID and ZAWNIOSKOWANE_NIE_POTWIERDZONE.UPRW_PRC_ID = ZEGR.prc_id


left join ( 
            SELECT decode(UPRW_RDA_ID, UZ.RDA_ID, UW.' 
WHERE      LST_ID = -313156;
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = LST_QUERY ||
'RDA_ID, UPRW_RDA_ID) UPRW_RDA_ID, UPRW_PRC_ID,
             round(sum(  ek_pck_procedury_hi.z_godz_harm_ind( UPRW_PRC_ID, TRUNC(UPRW_DATA_OD), TRUNC(UPRW_DATA_DO))/ek_pck_absencje.il_godz_na_dzien_urlopu(UPRW_PRC_ID, TRUNC(UPRW_DATA_DO))),6)  lzdd,
             round(sum(URLOP_GODZ_LICZBA_GODZIN(UPRW_PRC_ID, UPRW_DATA_OD, UPRW_DATA_DO)/ek_pck_absencje.il_godz_na_dzien_urlopu(UPRW_PRC_ID, TRUNC(UPRW_DATA_DO))),6) lzdh                 
            FROM PPADM.PPT_ABS_URLOPY_WNIOSKOWANE
                 join (SELECT GA_DG_KOD, MIN(GA_RDA_ID) GA_RDA_ID FROM EK_GRUPY_ABSENCJI GROUP BY GA_DG_KOD) on uprw_rda_id=GA_RDA_ID
                 join ek_limity_absencji on LA_DG_KOD = GA_DG_KOD and LA_PRC_ID=uprw_prc_id
                 join ek_limity_dane ld1 on la_id=ld_la_id 
                 left join EK_RODZAJE_ABSENCJI UW on UW.rda_kod_serwis = ''UW''
                 left join EK_RODZAJE_ABSENCJI UZ on UZ.rda_kod_serwis = ''U�''
            where  UPRW_STATUS = 10
                   and  EXTRACT(year FROM UPRW_DATA_DO) = :P_ROK
                   and UPRW_DATA_DO <= ld_do
                   and TO_DATE(:P_ROK||''-12-31'', ''YYYY-MM-DD'') between ld_od and ld_do
                   and uprw_prc_id= :P_PRC_ID 
            GROUP BY decode(UPRW_RDA_ID, UZ.RDA_ID, UW.RDA_ID, UPRW_RDA_ID), UPRW_PRC_ID --GROUP BY UPRW_RDA_ID,UPRW_PRC_ID

          ) ZATWIERDZONE on ZATWIERDZONE.UPRW_RDA_ID = ZEGR.RDA_ID and ZATWIERDZONE.UPRW_PRC_ID = ZEGR.prc_id
WHERE 
  rn=1 
  and rda_kod_serwis <> ''U�''
union 


    select --urlop na ��danie (4 dni na rok)
        rda_kod, rda_nazwa, rda_id, dg_kod, dg_nazwa, 
        4 as przyslugujacy, --maks 4; todo ale nie wiecej niz wynikaloby to z url wypoczynkowego 
        4 as biezacy, -- zawsze 4 (TODO to faktycznie powinno byc min. limitu url. wypoczynkowegi i 4, ale przypadek bardzo ograniczonego limitu url. wypoczynkowego obsl. r�cznie w kodzie)
        0 as zalegly, --zawsze 0 bo u. n/�. nigdy nie przechodzi na kolejny rok
       ' 
WHERE      LST_ID = -313156;
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = LST_QUERY ||
' greatest (4 - nvl(wnioski.ile_dni,0) - nvl(zatw.ile_dni,0) , 0),
        nvl(wnioski.ile_dni,0), 
        nvl(zatw.ile_dni,0),
        null,
        rda_kod_serwis 
    from (select :P_PRC_ID as prcid from dual), --tutaj_parametr 
        
        (   select --sum(uprw_data_do - uprw_data_od + 1) ile_dni
           sum(ek_pck_procedury_hi.z_dni_harm_ind(UPRW_PRC_ID, UPRW_DATA_OD, UPRW_DATA_DO)) ile_dni
            from PPT_ABS_URLOPY_WNIOSKOWANE 
            where  nvl(UPRW_STATUS,0) >= 0 and nvl(UPRW_STATUS,0) < 10
                and uprw_prc_id = :P_PRC_ID --Pawelski --tutaj_parametr 
                and uprw_rda_id in (select rda_id from EGADM1.EK_RODZAJE_ABSENCJI where rda_kod_serwis = ''U�'') --100021 --U�
                and EXTRACT(year FROM UPRW_DATA_DO)=:P_ROK --tutaj_parametr
        ) wnioski,    
    
        (   select -- sum(uprw_data_do - uprw_data_od + 1) ile_dni
            sum(ek_pck_procedury_hi.z_dni_harm_ind(UPRW_PRC_ID, UPRW_DATA_OD, UPRW_DATA_DO)) ile_dni
            from PPT_ABS_URLOPY_WNIOSKOWANE 
            where  UPRW_STATUS = 10
                and uprw_prc_id = :P_PRC_ID --Pawelski --tutaj_parametr
                and uprw_rda_id in (select rda_id from EGADM1.EK_RODZAJE_ABSENCJI where rda_kod_serwis = ''U�'') --100021 --U�
                and EXTRACT(year FROM UPRW_DATA_DO)=:P_ROK --tutaj_parametr
        ) zatw,    
    
        EK_DEF_GRUP
        join EK_GRUPY_ABSENCJI on ga_dg_kod=dg_kod
        join EK_RODZAJE_ABSENCJI on ga_rda_id = rda_id
        join ek_limity_absencji la on la.la_DG_KOD = GA_DG_KOD and la.la_rok = :P_ROK and la.la_prc_id = :P_PRC_ID
    where rda_kod_serwis = ''U�'' and dg_kod = ''A_UR11''' 
WHERE      LST_ID = -313156;
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('2','PPL_ABS_LIMITY_ROK','','100','rda_nazwa','T','N','rda_nazwa','-313157','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-06-12 10:42:03','2020-03-31 11:24:13','10.132.216.96\NIEZNANY','10.132.216.95\NIEZNANY','4','100','N','-313156','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('3','PPL_ABS_LIMITY_ROK','','100','rda_id','T','N','rda_id','-313158','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-06-12 10:42:03','2020-03-31 11:24:13','10.132.216.96\NIEZNANY','10.132.216.95\NIEZNANY','4','100','N','-313156','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('4','PPL_ABS_LIMITY_ROK','','100','dg_kod','T','N','dg_kod','-313159','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-06-12 10:42:03','2020-03-31 11:24:13','10.132.216.96\NIEZNANY','10.132.216.95\NIEZNANY','4','100','N','-313156','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('5','PPL_ABS_LIMITY_ROK','','100','dg_nazwa','T','N','dg_nazwa','-313160','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-06-12 10:42:03','2020-03-31 11:24:13','10.132.216.96\NIEZNANY','10.132.216.95\NIEZNANY','4','100','N','-313156','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('6','PPL_ABS_LIMITY_ROK','','100','przyslugujacy','T','N','przyslugujacy','-313161','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-06-12 10:42:03','2020-03-31 11:24:13','10.132.216.96\NIEZNANY','10.132.216.95\NIEZNANY','4','100','N','-313156','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('7','PPL_ABS_LIMITY_ROK','','100','biezacy','T','N','biezacy','-313162','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-06-12 10:42:03','2020-03-31 11:24:13','10.132.216.96\NIEZNANY','10.132.216.95\NIEZNANY','4','100','N','-313156','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('8','PPL_ABS_LIMITY_ROK','','100','zalegly','T','N','zalegly','-313163','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-06-12 10:42:03','2020-03-31 11:24:13','10.132.216.96\NIEZNANY','10.132.216.95\NIEZNANY','4','100','N','-313156','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('9','PPL_ABS_LIMITY_ROK','','100','dni_do_wykorzystania','T','N','dni_do_wykorzystania','-313164','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-06-12 10:42:03','2020-03-31 11:24:13','10.132.216.96\NIEZNANY','10.132.216.95\NIEZNANY','4','100','N','-313156','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('10','PPL_ABS_LIMITY_ROK','','100','dni_juz_zawnioskowanych','T','N','dni_juz_zawnioskowanych','-313165','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-06-12 10:42:03','2020-03-31 11:24:13','10.132.216.96\NIEZNANY','10.132.216.95\NIEZNANY','4','100','N','-313156','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('11','PPL_ABS_LIMITY_ROK','','100','zatwierdzone','T','N','zatwierdzone','-313166','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-06-12 10:42:03','2020-03-31 11:24:13','10.132.216.96\NIEZNANY','10.132.216.95\NIEZNANY','4','100','N','-313156','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('12','PPL_ABS_LIMITY_ROK','','100','dni_do_wykorzystania_eg','T','N','dni_do_wykorzystania_eg','-313167','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-06-12 10:42:03','2020-03-31 11:24:13','10.132.216.96\NIEZNANY','10.132.216.95\NIEZNANY','4','100','N','-313156','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('13','PPL_ABS_LIMITY_ROK','','50','rda_kod_serwis','T','N','rda_kod_serwis','-314396','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-03-31 10:34:53','2020-03-31 11:24:13','10.132.216.95\NIEZNANY','10.132.216.95\NIEZNANY','2','100','N','-313156','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_ABS_LIMITY_ROK' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
