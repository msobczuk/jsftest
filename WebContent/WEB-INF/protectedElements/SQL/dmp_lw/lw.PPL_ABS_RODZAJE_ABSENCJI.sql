-- data version: PPL_ABS_RODZAJE_ABSENCJI 2020.05.19 15:31.47
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:46:42
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_ABS_RODZAJE_ABSENCJI';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_ABS_RODZAJE_ABSENCJI' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_ABS_RODZAJE_ABSENCJI';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_ABS_RODZAJE_ABSENCJI')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_ABS_RODZAJE_ABSENCJI','Rodzaje absencji','50','Kod','T','N','RDA_KOD','-311134','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-05-19 15:31:47','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','518','50','N','','0','N','N','N','N','N','2020-05-19 15:31:47','N','0','2020-06-23 15:10:37','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'with par as (
select * from (

    select 
      --:p_data_do as p_data_do ,
      --nvl(:P_DATA_DO, TO_DATE(:P_ROK||''-12-31'', ''YYYY-MM-DD'')) as  P_DATA_DO,
      case 
      when :P_DATA_DO is null or EXTRACT(YEAR FROM TO_DATE(:P_DATA_DO)) > :P_ROK then TO_DATE(:P_ROK||''-12-31'', ''YYYY-MM-DD'')
      else TO_DATE(:P_DATA_DO)
      end as  P_DATA_DO,
      
      :P_ROK as P_ROK,
      :P_PRC_ID as P_PRC_ID
    from dual
    )
)

select * from (
SELECT 
  ZEGR.RDA_KOD RDA_KOD,
  ZEGR.RDA_NAZWA RDA_NAZWA,
  ZEGR.RDA_ID RDA_ID,
  ZEGR.dg_kod dg_kod,
  ZEGR.dg_nazwa dg_nazwa, 
  nvl(ZEGR.Biez_dni,0) przyslugujacy,
  nvl(ZEGR.Wymiar,0) biezacy,
  nvl(ZEGR.Zal_dni,0) zalegly,
   case 
    when RDA_NAZWA like ''%godz%'' then case when (nvl(ZEGR.Pozost_dni,0) - nvl(ZAWNIOSKOWANE_NIE_POTWIERDZONE.lzdh,0))<0 then 0 else nvl(ZEGR.Pozost_dni,0) - nvl(ZAWNIOSKOWANE_NIE_POTWIERDZONE.lzdh,0) end
         else                         case when (nvl(ZEGR.Pozost_dni,0) - nvl(ZAWNIOSKOWANE_NIE_POTWIERDZONE.lzdd,0))<0 then 0 else nvl(ZEGR.Pozost_dni,0) - nvl(ZAWNIOSKOWANE_NIE_POTWIERDZONE.lzdd,0) end
  end dni_do_wykorzystania, 
  case 
     when RDA_NAZWA like ''%godz%'' then nvl(ZAWNIOSKOWANE_NIE_POTWIERDZONE.lzdh,0)
        else                           nvl(ZAWNIOSKOWANE_NIE_POTWIERDZONE.lzdd,0)
  end dni_juz_zawnioskowanych,
  case 
     when RDA_NAZWA like ''%godz%'' then nvl(ZATWIERDZONE.lzdh,0)
        else                           nvl(ZATWIERDZONE.lzdd,0)
  end zatwierdzone,
  nvl(ZEGR.Pozost_dni,0)  dni_do_wykorzystania_EG,
  zegr.rda_kod_serwis 
FROM
  (
  
  SELECT
    RDA_KOD RDA_KOD,
    RDA_NAZWA RDA_NAZWA,
    RDA_ID RDA_ID,
    dg_kod dg_kod,
    dg_nazwa dg_nazwa,
    ld_od Od_kiedy,
    ld_do Do_kiedy,
    ld_wymiar Wymiar,
    round(la_przeniesione/8,2) Zal_dni,  
    ld_dni Biez_dni,  
    ld_wykorzystane "Wykorz.dni", 
    ld_pozostalo Pozost_dni,   
    la_rok,
    prc_id,
    ROW_NUMBER() OVER (PARTITION BY RD' 
WHERE      LST_ID = -311134;
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = LST_QUERY ||
'A_KOD ORDER BY ld_od desc) as rn,
    rda_kod_serwis 

  from -- (select 2018 as rok from dual)  par,
    par, 
    ek_pracownicy
    join ek_limity_absencji on  la_prc_id=prc_id 
    join ek_limity_dane ld1 on la_id=ld_la_id
    join EK_DEF_GRUP on la_dg_kod = dg_kod
    left join (SELECT GA_DG_KOD, MIN(GA_RDA_ID) GA_RDA_ID FROM EK_GRUPY_ABSENCJI GROUP BY GA_DG_KOD) on  dg_kod = GA_DG_KOD
    left join EK_RODZAJE_ABSENCJI on ga_rda_id = rda_id
  where 1=1 
    and lower (RDA_NAZWA) like ''%urlop%''
    and prc_id= P_PRC_ID 
    --and nvl(P_DATA_OD, TO_DATE(P_ROK||''-12-31'', ''YYYY-MM-DD'')) between ld_od and ld_do 
    and P_DATA_DO between ld_od and ld_do 

      
  ) ZEGR
left join ( 

            SELECT decode(UPRW_RDA_ID, UZ.RDA_ID, UW.RDA_ID, UPRW_RDA_ID) UPRW_RDA_ID, UPRW_PRC_ID,
             round(sum(  ek_pck_procedury_hi.z_godz_harm_ind( UPRW_PRC_ID, TRUNC(UPRW_DATA_OD), TRUNC(UPRW_DATA_DO))/ek_pck_absencje.il_godz_na_dzien_urlopu(UPRW_PRC_ID, TRUNC(UPRW_DATA_DO))),6)  lzdd,
             round(sum(URLOP_GODZ_LICZBA_GODZIN(UPRW_PRC_ID, UPRW_DATA_OD, UPRW_DATA_DO)/ek_pck_absencje.il_godz_na_dzien_urlopu(UPRW_PRC_ID, TRUNC(UPRW_DATA_DO))),6) lzdh                 
            FROM par, PPADM.PPT_ABS_URLOPY_WNIOSKOWANE
                 join (SELECT GA_DG_KOD, MIN(GA_RDA_ID) GA_RDA_ID FROM EK_GRUPY_ABSENCJI GROUP BY GA_DG_KOD) on uprw_rda_id=GA_RDA_ID
                 join ek_limity_absencji on LA_DG_KOD = GA_DG_KOD and LA_PRC_ID=uprw_prc_id
                 join ek_limity_dane ld1 on la_id=ld_la_id 
                 left join EK_RODZAJE_ABSENCJI UW on UW.rda_kod_serwis = ''UW''
                 left join EK_RODZAJE_ABSENCJI UZ on UZ.rda_kod_serwis = ''U�''
            where  nvl(UPRW_STATUS,0) >= 0 and nvl(UPRW_STATUS,0) < 10
                   and  EXTRACT(year FROM UPRW_DATA_DO) = P_ROK
                   and UPRW_DATA_DO <= ld_do
		   --and nvl(:P_DATA_OD, TO_DATE(P_ROK||''-12-31'', ''YYYY-MM-DD'')) between ld_od and ld_do 
             ' 
WHERE      LST_ID = -311134;
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = LST_QUERY ||
'      and P_DATA_DO between ld_od and ld_do
                   and uprw_prc_id= P_PRC_ID 
            GROUP BY decode(UPRW_RDA_ID, UZ.RDA_ID, UW.RDA_ID, UPRW_RDA_ID), UPRW_PRC_ID --GROUP BY UPRW_RDA_ID,UPRW_PRC_ID

          ) ZAWNIOSKOWANE_NIE_POTWIERDZONE on ZAWNIOSKOWANE_NIE_POTWIERDZONE.UPRW_RDA_ID = ZEGR.RDA_ID and ZAWNIOSKOWANE_NIE_POTWIERDZONE.UPRW_PRC_ID = ZEGR.prc_id


left join ( 
            SELECT decode(UPRW_RDA_ID, UZ.RDA_ID, UW.RDA_ID, UPRW_RDA_ID) UPRW_RDA_ID, UPRW_PRC_ID,
             round(sum(  ek_pck_procedury_hi.z_godz_harm_ind( UPRW_PRC_ID, TRUNC(UPRW_DATA_OD), TRUNC(UPRW_DATA_DO))/ek_pck_absencje.il_godz_na_dzien_urlopu(UPRW_PRC_ID, TRUNC(UPRW_DATA_DO))),6)  lzdd,
             round(sum(URLOP_GODZ_LICZBA_GODZIN(UPRW_PRC_ID, UPRW_DATA_OD, UPRW_DATA_DO)/ek_pck_absencje.il_godz_na_dzien_urlopu(UPRW_PRC_ID, TRUNC(UPRW_DATA_DO))),6) lzdh                 
            FROM par, PPADM.PPT_ABS_URLOPY_WNIOSKOWANE
                 join (SELECT GA_DG_KOD, MIN(GA_RDA_ID) GA_RDA_ID FROM EK_GRUPY_ABSENCJI GROUP BY GA_DG_KOD) on uprw_rda_id=GA_RDA_ID
                 join ek_limity_absencji on LA_DG_KOD = GA_DG_KOD and LA_PRC_ID=uprw_prc_id
                 join ek_limity_dane ld1 on la_id=ld_la_id 
                 left join EK_RODZAJE_ABSENCJI UW on UW.rda_kod_serwis = ''UW''
                 left join EK_RODZAJE_ABSENCJI UZ on UZ.rda_kod_serwis = ''U�''
            where  UPRW_STATUS = 10
                   and  EXTRACT(year FROM UPRW_DATA_DO) = P_ROK
                   and UPRW_DATA_DO <= ld_do
		   --and nvl(:P_DATA_OD, TO_DATE(P_ROK||''-12-31'', ''YYYY-MM-DD'')) between ld_od and ld_do 
                   and P_DATA_DO between ld_od and ld_do
                   and uprw_prc_id= P_PRC_ID 
            GROUP BY decode(UPRW_RDA_ID, UZ.RDA_ID, UW.RDA_ID, UPRW_RDA_ID), UPRW_PRC_ID --GROUP BY UPRW_RDA_ID,UPRW_PRC_ID

          ) ZATWIERDZONE on ZATWIERDZONE.UPRW_RDA_ID = ZEGR.RDA_ID and ZATWIERDZONE.UPRW_PRC_ID = ZEGR.prc_id
WHERE 
  rn=1 
' 
WHERE      LST_ID = -311134;
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = LST_QUERY ||
'  and rda_kod_serwis <> ''U�''
union 


    select --urlop na ��danie (4 dni na rok)
        rda_kod, rda_nazwa, rda_id, dg_kod, dg_nazwa, 
        4 as przyslugujacy, --maks 4; todo ale nie wiecej niz wynikaloby to z url wypoczynkowego 
        4 as biezacy, -- zawsze 4 (TODO to faktycznie powinno byc min. limitu url. wypoczynkowegi i 4, ale przypadek bardzo ograniczonego limitu url. wypoczynkowego obsl. r�cznie w kodzie)
        0 as zalegly, --zawsze 0 bo u. n/�. nigdy nie przechodzi na kolejny rok
        greatest (4 - nvl(wnioski.ile_dni,0) - nvl(zatw.ile_dni,0) , 0),
        nvl(wnioski.ile_dni,0), 
        nvl(zatw.ile_dni,0),
        null,
        rda_kod_serwis
    from --(select P_PRC_ID as prcid from par), --tutaj_parametr 
        
        (   select --sum(uprw_data_do - uprw_data_od + 1) ile_dni
           sum(ek_pck_procedury_hi.z_dni_harm_ind(UPRW_PRC_ID, UPRW_DATA_OD, UPRW_DATA_DO)) ile_dni
            from par, PPT_ABS_URLOPY_WNIOSKOWANE 
            where  nvl(UPRW_STATUS,0) >= 0 and nvl(UPRW_STATUS,0) < 10
                and uprw_prc_id = P_PRC_ID --Pawelski --tutaj_parametr 
                and uprw_rda_id in (select rda_id from EGADM1.EK_RODZAJE_ABSENCJI where rda_kod_serwis = ''U�'') --100021 --U�
                and EXTRACT(year FROM UPRW_DATA_DO)=P_ROK --tutaj_parametr
        ) wnioski,    
    
        (   select -- sum(uprw_data_do - uprw_data_od + 1) ile_dni
            sum(ek_pck_procedury_hi.z_dni_harm_ind(UPRW_PRC_ID, UPRW_DATA_OD, UPRW_DATA_DO)) ile_dni
            from par, PPT_ABS_URLOPY_WNIOSKOWANE 
            where  UPRW_STATUS = 10
                and uprw_prc_id = P_PRC_ID --Pawelski --tutaj_parametr
                and uprw_rda_id in (select rda_id from EGADM1.EK_RODZAJE_ABSENCJI where rda_kod_serwis = ''U�'') --100021 --U�
                and EXTRACT(year FROM UPRW_DATA_DO)=P_ROK --tutaj_parametr
        ) zatw,  
        
        
        par  
        join EK_DEF_GRUP on 1=1
        join EK_G' 
WHERE      LST_ID = -311134;
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = LST_QUERY ||
'RUPY_ABSENCJI on ga_dg_kod=dg_kod
        join EK_RODZAJE_ABSENCJI on ga_rda_id = rda_id
        join ek_limity_absencji la on la.la_DG_KOD = GA_DG_KOD and la.la_rok = P_ROK and la.la_prc_id = P_PRC_ID
    where rda_kod_serwis = ''U�'' and dg_kod = ''A_UR11''
) ' 
WHERE      LST_ID = -311134;
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('2','PPL_ABS_RODZAJE_ABSENCJI','','300','Nazwa','T','N','RDA_NAZWA','-311135','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-05-19 15:31:47','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','8','300','N','-311134','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('3','PPL_ABS_RODZAJE_ABSENCJI','','70','Id','T','T','RDA_ID','-311136','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-05-19 15:31:47','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','8','70','N','-311134','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('4','PPL_ABS_RODZAJE_ABSENCJI','','50','dg_kod','T','T','dg_kod','-311501','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-05-19 15:31:47','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','8','50','N','-311134','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('5','PPL_ABS_RODZAJE_ABSENCJI','','50','dg_nazwa','T','T','dg_nazwa','-311502','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-05-19 15:31:47','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','8','50','N','-311134','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('6','PPL_ABS_RODZAJE_ABSENCJI','','50','przyslugujacy','T','T','przyslugujacy','-311503','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-05-19 15:31:47','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','8','50','N','-311134','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('7','PPL_ABS_RODZAJE_ABSENCJI','','50','biezacy','T','T','biezacy','-311504','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-05-19 15:31:47','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','8','50','N','-311134','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('8','PPL_ABS_RODZAJE_ABSENCJI','','50','zalegly','T','T','zalegly','-311505','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-05-19 15:31:47','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','8','50','N','-311134','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('9','PPL_ABS_RODZAJE_ABSENCJI','','50','dni_do_wykorzystania','T','T','dni_do_wykorzystania','-311506','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-05-19 15:31:47','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','8','50','N','-311134','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('10','PPL_ABS_RODZAJE_ABSENCJI','','50','dni_juz_zawnioskowanych','T','T','dni_juz_zawnioskowanych','-311507','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-05-19 15:31:47','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','8','50','N','-311134','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('11','PPL_ABS_RODZAJE_ABSENCJI','','50','zatwierdzone','T','T','zatwierdzone','-311508','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-05-19 15:31:47','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','8','50','N','-311134','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('12','PPL_ABS_RODZAJE_ABSENCJI','','50','dni_do_wykorzystania_EG','T','T','dni_do_wykorzystania_EG','-311514','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-05-19 15:31:47','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','8','50','N','-311134','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('13','PPL_ABS_RODZAJE_ABSENCJI','','50','rda_kod_serwis','T','T','rda_kod_serwis','-314395','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-03-31 10:28:38','2020-05-19 15:31:47','10.132.216.95\NIEZNANY','0:0:0:0:0:0:0:1\NIEZNANY','3','100','N','-311134','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_ABS_RODZAJE_ABSENCJI' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-273','-311134','EGADM1.CKK_ADRESY','EGADM1','CKK_ADRESY','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-274','-311134','EGADM1.CKK_ADRESY','EGADM1','CKK_ADRESY','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-275','-311134','EGADM1.CKK_BRANZE_KLIENTOW','EGADM1','CKK_BRANZE_KLIENTOW','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-276','-311134','EGADM1.CKK_CECHY','EGADM1','CKK_CECHY','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-277','-311134','EGADM1.CKK_DODATKOWE_INFORMACJE','EGADM1','CKK_DODATKOWE_INFORMACJE','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-278','-311134','EGADM1.CKK_KLIENCI','EGADM1','CKK_KLIENCI','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-279','-311134','EGADM1.CKK_KLIENCI_DANE','EGADM1','CKK_KLIENCI_DANE','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-280','-311134','EGADM1.CKK_REPREZENTANCI','EGADM1','CKK_REPREZENTANCI','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-281','-311134','EGADM1.CKK_SAMOCHODY','EGADM1','CKK_SAMOCHODY','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-282','-311134','EGADM1.CKK_SPOSOBY_REPREZENTACJI','EGADM1','CKK_SPOSOBY_REPREZENTACJI','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-283','-311134','EGADM1.CKKT_CERTYFIKATY_KL','EGADM1','CKKT_CERTYFIKATY_KL','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-284','-311134','EGADM1.CKK_TELEFONY','EGADM1','CKK_TELEFONY','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-285','-311134','EGADM1.CKK_TELEFONY','EGADM1','CKK_TELEFONY','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-286','-311134','EGADM1.CKKT_EWIDENCJA_UZYWANIA','EGADM1','CKKT_EWIDENCJA_UZYWANIA','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-287','-311134','EGADM1.CKKT_KONTA_ROZL_ARCH','EGADM1','CKKT_KONTA_ROZL_ARCH','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-288','-311134','EGADM1.CKKT_OBYWATELSTWA','EGADM1','CKKT_OBYWATELSTWA','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-289','-311134','EGADM1.CKKT_OCENY_KLIENTOW','EGADM1','CKKT_OCENY_KLIENTOW','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-290','-311134','EGADM1.CKKT_OPIEKUNOWIE_KLIENTOW','EGADM1','CKKT_OPIEKUNOWIE_KLIENTOW','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-291','-311134','EGADM1.CKKT_REPREZENTACJE_KLIENTOW','EGADM1','CKKT_REPREZENTACJE_KLIENTOW','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-292','-311134','EGADM1.CKKT_UPRAWNIENIA_GKL','EGADM1','CKKT_UPRAWNIENIA_GKL','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-293','-311134','EGADM1.CKK_UDZIALOWCY','EGADM1','CKK_UDZIALOWCY','EGADM1','CKKP_TYPY','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-294','-311134','EGADM1.CSS_KRAJE','EGADM1','CSS_KRAJE','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-295','-311134','EGADM1.CSS_WARTOSCI_SLOWNIKOW','EGADM1','CSS_WARTOSCI_SLOWNIKOW','EGADM1','EK_REF_CODES','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-296','-311134','EGADM1.CSS_WOJEWODZTWA','EGADM1','CSS_WOJEWODZTWA','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-297','-311134','EGADM1.EK_ABSENCJE_DANE','EGADM1','EK_ABSENCJE_DANE','EGADM1','EK_PCK_ABSENCJE','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-298','-311134','EGADM1.EK_CZLONKOWIE_RODZINY','EGADM1','EK_CZLONKOWIE_RODZINY','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-299','-311134','EGADM1.EK_DEF_GRUP','EGADM1','EK_DEF_GRUP','PUBLIC','EK_DEF_GRUP','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-300','-311134','EGADM1.EK_ETAPY_UMOWY','EGADM1','EK_ETAPY_UMOWY','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-301','-311134','EGADM1.EK_GODZINY_DLA_DNI_I_GRUP','EGADM1','EK_GODZINY_DLA_DNI_I_GRUP','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-302','-311134','EGADM1.EK_GODZINY_W_DNIU','EGADM1','EK_GODZINY_W_DNIU','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-303','-311134','EGADM1.EK_GODZINY_W_DNIU','EGADM1','EK_GODZINY_W_DNIU','PPADM','URLOP_GODZ_LICZBA_GODZIN','FUNCTION','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-304','-311134','EGADM1.EK_GRUPY_ABSENCJI','EGADM1','EK_GRUPY_ABSENCJI','PUBLIC','EK_GRUPY_ABSENCJI','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-305','-311134','EGADM1.EK_HARMONOGRAMY_GRUPOWE','EGADM1','EK_HARMONOGRAMY_GRUPOWE','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-306','-311134','EGADM1.EK_HARMONOGRAMY_INDYWIDUALNE','EGADM1','EK_HARMONOGRAMY_INDYWIDUALNE','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-307','-311134','EGADM1.EK_HISTORIA_PRACOWNIKOW','EGADM1','EK_HISTORIA_PRACOWNIKOW','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-308','-311134','EGADM1.EK_KASA_CHORYCH','EGADM1','EK_KASA_CHORYCH','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-309','-311134','EGADM1.EK_LIMITY_ABSENCJI','EGADM1','EK_LIMITY_ABSENCJI','PUBLIC','EK_LIMITY_ABSENCJI','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-310','-311134','EGADM1.EK_LIMITY_DANE','EGADM1','EK_LIMITY_DANE','PUBLIC','EK_LIMITY_DANE','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-311','-311134','EGADM1.EK_PLANY_INDYWIDUALNE','EGADM1','EK_PLANY_INDYWIDUALNE','PPADM','URLOP_GODZ_LICZBA_GODZIN','FUNCTION','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-312','-311134','EGADM1.EK_PRACOWNICY','EGADM1','EK_PRACOWNICY','EGADM1','EK_PCK_ABSENCJE','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-313','-311134','EGADM1.EK_PRACOWNICY','EGADM1','EK_PRACOWNICY','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-314','-311134','EGADM1.EK_PRACOWNICY','EGADM1','EK_PRACOWNICY','PUBLIC','EK_PRACOWNICY','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-315','-311134','EGADM1.EK_RODZAJE_ABSENCJI','EGADM1','EK_RODZAJE_ABSENCJI','EGADM1','EK_PCK_ABSENCJE','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-316','-311134','EGADM1.EK_RODZAJE_ABSENCJI','EGADM1','EK_RODZAJE_ABSENCJI','PUBLIC','EK_RODZAJE_ABSENCJI','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-317','-311134','EGADM1.EK_RODZAJE_ABSENCJI','EGADM1','EK_RODZAJE_ABSENCJI','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-318','-311134','EGADM1.EK_RODZAJE_DNI','EGADM1','EK_RODZAJE_DNI','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-319','-311134','EGADM1.EK_RODZAJE_GODZIN','EGADM1','EK_RODZAJE_GODZIN','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-320','-311134','EGADM1.EK_SKLADNIKI','EGADM1','EK_SKLADNIKI','EGADM1','EK_PCK_ABSENCJE','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-321','-311134','EGADM1.EK_TMP_OBLICZENIA','EGADM1','EK_TMP_OBLICZENIA','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-322','-311134','EGADM1.EK_WOJSKO','EGADM1','EK_WOJSKO','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-323','-311134','EGADM1.EK_ZATRUDNIENIE','EGADM1','EK_ZATRUDNIENIE','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-324','-311134','EGADM1.EK_ZMIANY','EGADM1','EK_ZMIANY','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-325','-311134','EGADM1.EK_ZUS','EGADM1','EK_ZUS','EGADM1','EK_PCK_TYPY_DANYCH','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-326','-311134','EGADM1.EK_ZWOLNIENIA','EGADM1','EK_ZWOLNIENIA','EGADM1','EK_PCK_ABSENCJE','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-327','-311134','EGADM1.EK_ZWOLNIENIA','EGADM1','EK_ZWOLNIENIA','EGADM1','EK_PCK_ZWOL','PACKAGE','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-328','-311134','PPADM.PPT_ABS_URLOPY_WNIOSKOWANE','PPADM','PPT_ABS_URLOPY_WNIOSKOWANE','PPADM','PPV_LW_COMPILE_DEPENDENCIES','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-329','-311134','PPADM.PPT_LOG','PPADM','PPT_LOG','PPADM','PLOG','PROCEDURE','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
