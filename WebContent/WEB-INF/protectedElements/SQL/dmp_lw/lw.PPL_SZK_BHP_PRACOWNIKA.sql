-- data version: PPL_SZK_BHP_PRACOWNIKA 2020.02.12 09:39.52
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:49:06
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_SZK_BHP_PRACOWNIKA';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_SZK_BHP_PRACOWNIKA' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_SZK_BHP_PRACOWNIKA';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_SZK_BHP_PRACOWNIKA')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_SZK_BHP_PRACOWNIKA','Szkolenia BHP pracownika','60','Id szkolenia','T','N','szk_id','-311769','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:35','2020-02-12 09:39:52','PCMGOLDA\Mikolaj.Golda','10.132.45.53\NIEZNANY','45','100','N','','0','N','N','N','N','N','2018-12-13 18:11:54','T','0','2020-06-23 10:22:28','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'select szk_id,
             szk_nazwa,
             szk_data_od,
             szk_data_do,
             szk_data_waznosci,
             kwa_prc_id,
             szk_rodzaj
  from ek_kwalifikacje
    join ek_szkolenia
      on kwa_id = szk_kwa_id
where szk_nazwa like ''%BHP%''
     and kwa_prc_id = :POLE_1' 
WHERE      LST_ID = -311769;
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('2','PPL_SZK_BHP_PRACOWNIKA','','250','Nazwa szkolenia','T','N','szk_nazwa','-311770','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:35','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','250','N','-311769','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('3','PPL_SZK_BHP_PRACOWNIKA','','150','Rodzaj szkolenia','T','T','szk_rodzaj','-311781','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:35','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','100','N','-311769','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('4','PPL_SZK_BHP_PRACOWNIKA','','100','Data rozpocz�cia','T','N','szk_data_od','-311771','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:35','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','100','N','-311769','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('5','PPL_SZK_BHP_PRACOWNIKA','','100','Data zako�czenia','T','N','szk_data_do','-311772','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:35','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','100','N','-311769','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('6','PPL_SZK_BHP_PRACOWNIKA','','100','Data wa�no�ci','T','N','szk_data_waznosci','-311773','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:35','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','100','N','-311769','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('7','PPL_SZK_BHP_PRACOWNIKA','','100','Id pracownika','T','T','kwa_prc_id','-311779','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:35','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','100','N','-311769','','','','','','','','','','','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_SZK_BHP_PRACOWNIKA' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-869','-311769','EGADM1.EK_KWALIFIKACJE','EGADM1','EK_KWALIFIKACJE','PUBLIC','EK_KWALIFIKACJE','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-870','-311769','EGADM1.EK_SZKOLENIA','EGADM1','EK_SZKOLENIA','PUBLIC','EK_SZKOLENIA','SYNONYM','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
