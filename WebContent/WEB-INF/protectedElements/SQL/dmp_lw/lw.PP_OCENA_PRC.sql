-- data version: PP_OCENA_PRC 2020.01.24 16:51.31
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:45:04
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PP_OCENA_PRC';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PP_OCENA_PRC' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PP_OCENA_PRC';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PP_OCENA_PRC')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PP_OCENA_PRC','OLD - Ocena pracownika','50','Ocena','T','N','ocena_id','-310370','','','','','','chyba ZLY','','OCE','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:37','2020-01-24 16:51:31','PCMGOLDA\Mikolaj.Golda','10.132.29.6\NIEZNANY','3','64','N','','0','N','N','N','N','N','','','','2020-01-24 16:51:31','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'select 
    zo.oce_id OCENA_ID,
    ek.PRC_IMIE  OCENIANY_IMIE,
    ek.PRC_NAZWISKO   OCENIANY_NAZWISKO,  
    ek_zat1.STN_NAZWA STANOWISKO_OCENIANY,
    ek2.PRC_IMIE  OCENIAJACY_IMIE,
    ek2.PRC_NAZWISKO OCENIAJACY_NAZWISKO,
    ek_zat2.STN_NAZWA STANOWISKO_OCENIAJACY,
    zo.OCE_DATA_OCENY DATA_OCENY
  from PPADM.PPT_OCE_OCENY zo, 
       EK_PRACOWNICY ek, 
       EK_PRACOWNICY ek2,
      (select 
       ek_zat.ZAT_PRC_ID,
       zpt_sta.STN_NAZWA
       from EK_ZATRUDNIENIE ek_zat,
            ZPT_STANOWISKA zpt_sta
       where ek_zat.ZAT_STN_ID=zpt_sta.STN_ID
       group by ek_zat.ZAT_PRC_ID, zpt_sta.STN_NAZWA
      ) ek_zat1,
      (select 
       ek_zat.ZAT_PRC_ID,
       zpt_sta.STN_NAZWA
       from EK_ZATRUDNIENIE ek_zat,
            ZPT_STANOWISKA zpt_sta
       where ek_zat.ZAT_STN_ID=zpt_sta.STN_ID
       group by ek_zat.ZAT_PRC_ID, zpt_sta.STN_NAZWA
      ) ek_zat2

  where zo.OCE_PRC_ID = ek.PRC_ID
    and zo.OCE_PRC_ID_OCENIAJACY = ek2.PRC_ID
    and ek_zat1.ZAT_PRC_ID=ek.PRC_ID
    and ek_zat2.ZAT_PRC_ID=ek2.PRC_ID
 AND zo.oce_id LIKE :POLE_1
 and NVL(upper(ek.PRC_IMIE),''%'') LIKE upper(:POLE_2)
 and NVL(upper(ek.PRC_NAZWISKO),''%'') LIKE upper(:POLE_3)
 and NVL(upper(ek_zat1.STN_NAZWA),''%'') LIKE upper(:POLE_4)
 and NVL(upper(ek2.PRC_IMIE),''%'') LIKE upper(:POLE_5)
 and NVL(upper(ek2.PRC_NAZWISKO),''%'') LIKE upper(:POLE_6)
 and NVL(upper(ek_zat2.STN_NAZWA),''%'') LIKE upper(:POLE_7)
 and NVL(to_char(zo.OCE_DATA_OCENY),''%'') LIKE upper(:POLE_8)' 
WHERE      LST_ID = -310370;
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('2','PP_OCENA_PRC','','100','Imie','N','N','oceniany_imie','-310371','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:37','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','128','N','-310370','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('3','PP_OCENA_PRC','','100','Nazwisko','N','N','oceniany_nazwisko','-310372','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:37','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','128','N','-310370','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('4','PP_OCENA_PRC','','100','Stanowisko','N','N','stanowisko_oceniany','-310373','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:37','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','128','N','-310370','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('5','PP_OCENA_PRC','','110','Imie oceniajacego','N','N','oceniajacy_imie','-310374','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:37','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','141','N','-310370','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('6','PP_OCENA_PRC','','110','Nazwisko oceniajacego','N','N','oceniajacy_nazwisko','-310375','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:37','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','141','N','-310370','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('7','PP_OCENA_PRC','','110','Stanowisko oceniajacego','N','N','stanowisko_oceniajacy','-310376','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:37','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','141','N','-310370','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('8','PP_OCENA_PRC','','50','Data oceny','N','N','data_oceny','-310312','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:37','2019-02-04 14:11:00','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','64','N','-310370','','','','','','','','','','','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PP_OCENA_PRC' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
