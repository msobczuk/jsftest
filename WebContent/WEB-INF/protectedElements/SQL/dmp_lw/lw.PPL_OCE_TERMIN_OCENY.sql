-- data version: PPL_OCE_TERMIN_OCENY 2020.02.11 16:03.51
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:45:06
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_OCE_TERMIN_OCENY';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_OCE_TERMIN_OCENY' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_OCE_TERMIN_OCENY';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_OCE_TERMIN_OCENY')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_OCE_TERMIN_OCENY','Zapytanie zwraca termin kolejnej oceny okresowej dla danego pracownika. ','50','data_poprzedniej','T','N','data_poprzedniej','-312043','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:34','2020-02-11 16:03:51','PCMGOLDA\Mikolaj.Golda','10.132.29.6\NIEZNANY','65','50','N','','0','N','N','N','N','N','2019-06-03 13:32:11','T','3','2020-02-11 16:03:51','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'SELECT 
    DECODE( dataPoprzedniejOceny.d,null,add_months(dataZmiany.d,24),
                                        add_months(dataPoprzedniejOceny.d,24)    
    ) data_poprzedniej
FROM 
(
    SELECT MAX(HIO_DATA) d FROM PPADM.PPT_OCE_OCENY oce
    join PPADM.PPV_HISTORIA_OBIEGOW_LAST on OCE.OCE_ID = HIO_KLUCZ_OBCY_ID and HIO_KOB_KOD = ''WN_OCENA_OKRES'' and DSTN_NAZWA = ''STOP''
    WHERE 1=1
    and oce.OCE_PRC_ID=?
    and oce.OCE_ID<>?
    ORDER BY HIO_DATA desc
) dataPoprzedniejOceny 
join (
    SELECT zat_data_zmiany d FROM EGADM1.EK_ZATRUDNIENIE
    WHERE 1=1
    and ZAT_PRC_ID=? 
    and zat_data_zmiany <=   trunc(sysdate)
    and                      trunc(sysdate)  <=  nvl(zat_data_do, trunc(SYSDATE+(365*1000)) )
    and zat_typ_umowy = 0
    and rownum = 1
) dataZmiany on 1=1 ' 
WHERE      LST_ID = -312043;
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_OCE_TERMIN_OCENY' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-814','-312043','EGADM1.EK_ZATRUDNIENIE','EGADM1','EK_ZATRUDNIENIE','PPADM','PPV_LW_COMPILE_DEPENDENCIES','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-815','-312043','PPADM.PPT_OCE_OCENY','PPADM','PPT_OCE_OCENY','PPADM','PPV_LW_COMPILE_DEPENDENCIES','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10393','-312043','EAADM.EAT_DOSTEPY_DO_FIRM','EAADM','EAT_DOSTEPY_DO_FIRM','PPADM','PPV_HISTORIA_OBIEGOW','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10394','-312043','EAADM.EAT_UZYTKOWNICY','EAADM','EAT_UZYTKOWNICY','PPADM','PPV_HISTORIA_OBIEGOW','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10395','-312043','EGADM1.CSST_DEF_STANOW','EGADM1','CSST_DEF_STANOW','PPADM','PPV_HISTORIA_OBIEGOW','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10396','-312043','EGADM1.CSST_HISTORIA_OBIEGOW','EGADM1','CSST_HISTORIA_OBIEGOW','PUBLIC','CSST_HISTORIA_OBIEGOW','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10397','-312043','EGADM1.EK_PRACOWNICY','EGADM1','EK_PRACOWNICY','PPADM','PPV_HISTORIA_OBIEGOW','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10398','-312043','EGADM1.ZPT_STANOWISKA','EGADM1','ZPT_STANOWISKA','PPADM','PPV_HISTORIA_OBIEGOW','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10399','-312043','EGADM1.CSS_OBIEKTY_W_PRZEDSIEB','EGADM1','CSS_OBIEKTY_W_PRZEDSIEB','EGADM1','EK_OBIEKTY_W_PRZEDSIEB','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10400','-312043','EGADM1.CSS_OBIEKTY_W_PRZEDSIEB','EGADM1','CSS_OBIEKTY_W_PRZEDSIEB','PPADM','PPV_OB_DEPARTAMENTY','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10401','-312043','EGADM1.CSST_CZYNNOSCI_OBIEGOW','EGADM1','CSST_CZYNNOSCI_OBIEGOW','PUBLIC','CSST_CZYNNOSCI_OBIEGOW','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10402','-312043','EGADM1.CSST_HISTORIA_OBIEGOW','EGADM1','CSST_HISTORIA_OBIEGOW','PUBLIC','CSST_HISTORIA_OBIEGOW','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10403','-312043','EGADM1.CSS_TYPY_OBIEKTOW','EGADM1','CSS_TYPY_OBIEKTOW','EGADM1','EK_OBIEKTY_W_PRZEDSIEB','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10404','-312043','EGADM1.CSS_OBIEKTY_W_PRZEDSIEB','EGADM1','CSS_OBIEKTY_W_PRZEDSIEB','PUBLIC','CSS_OBIEKTY_W_PRZEDSIEB','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10405','-312043','EGADM1.CSS_OBIEKTY_W_PRZEDSIEB','EGADM1','CSS_OBIEKTY_W_PRZEDSIEB','EGADM1','EK_OBIEKTY_W_PRZEDSIEB','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10406','-312043','EGADM1.CSS_TYPY_OBIEKTOW','EGADM1','CSS_TYPY_OBIEKTOW','EGADM1','EK_OBIEKTY_W_PRZEDSIEB','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10407','-312043','EGADM1.CSS_OBIEKTY_W_PRZEDSIEB','EGADM1','CSS_OBIEKTY_W_PRZEDSIEB','EGADM1','EK_OBIEKTY_W_PRZEDSIEB','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10408','-312043','EGADM1.CSS_TYPY_OBIEKTOW','EGADM1','CSS_TYPY_OBIEKTOW','EGADM1','EK_OBIEKTY_W_PRZEDSIEB','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10409','-312043','EGADM1.CSS_OBIEKTY_W_PRZEDSIEB','EGADM1','CSS_OBIEKTY_W_PRZEDSIEB','EGADM1','EK_OBIEKTY_W_PRZEDSIEB','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10410','-312043','EGADM1.CSS_TYPY_OBIEKTOW','EGADM1','CSS_TYPY_OBIEKTOW','EGADM1','EK_OBIEKTY_W_PRZEDSIEB','VIEW','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
