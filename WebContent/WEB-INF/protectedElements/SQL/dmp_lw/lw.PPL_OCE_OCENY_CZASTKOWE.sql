-- data version: PPL_OCE_OCENY_CZASTKOWE 2020.04.24 14:28.34
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:49:17
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_OCE_OCENY_CZASTKOWE';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_OCE_OCENY_CZASTKOWE' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_OCE_OCENY_CZASTKOWE';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_OCE_OCENY_CZASTKOWE')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_OCE_OCENY_CZASTKOWE','Warto�ci ocen cz�stkowych','80','Warto��','T','N','wsl_wartosc_max','-592','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:34','2020-04-24 14:28:34','PCMGOLDA\Mikolaj.Golda','10.155.231.137\NIEZNANY','249','80','N','','0','N','N','N','N','N','','','','2020-06-12 10:41:07','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'select 
 O.WSL_WARTOSC_MAX,
 O.WSL_ALIAS,
 O.WSL_OPIS,
 O.WSL_WARTOSC
from ppt_adm_wartosci_slownikow O 
where O.WSL_SL_NAZWA = ''OCE_OCENY_CZASTKOWE''
order by O.WSL_WARTOSC_MAX' 
WHERE      LST_ID = -592;
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('2','PPL_OCE_OCENY_CZASTKOWE','','300','Alias','T','T','wsl_alias','-593','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:34','2019-02-04 14:10:59','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','300','N','-592','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('3','PPL_OCE_OCENY_CZASTKOWE','','300','Opis','T','N','wsl_opis','-594','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:34','2019-02-04 14:10:59','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','300','N','-592','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('4','PPL_OCE_OCENY_CZASTKOWE','','300','Warto��','T','T','wsl_wartosc','-595','','','','','','','','','','','','','','','','','','','PPADM','PPADM','2019-04-08 08:05:34','2019-02-04 14:10:59','PCMGOLDA\Mikolaj.Golda','PCMGOLDA\Mikolaj.Golda','1','300','N','-592','','','','','','','','','','','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_OCE_OCENY_CZASTKOWE' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-813','-592','PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW','PPADM','PPT_ADM_WARTOSCI_SLOWNIKOW','PPADM','PPV_LW_COMPILE_DEPENDENCIES','VIEW','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
