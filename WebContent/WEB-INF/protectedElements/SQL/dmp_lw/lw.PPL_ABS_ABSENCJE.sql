-- data version: PPL_ABS_ABSENCJE 2020.04.23 09:56.57
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:50:41
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_ABS_ABSENCJE';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_ABS_ABSENCJE' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_ABS_ABSENCJE';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_ABS_ABSENCJE')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_ABS_ABSENCJE','Absencje pracownika','50','Kod absencji','T','N','rda_kod','-311131','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2020-04-23 09:56:57','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','750','50','N','','0','N','N','N','N','N','2019-03-12 08:43:28','T','2','2020-06-23 14:11:25','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'select 
    RDA.RDA_KOD 
   ,AB.AB_DATA_OD 
   ,AB.AB_DATA_DO 
from (select ? as rok, ? as prc_id  from dual) p 
      ,EGADM1.EK_ABSENCJE AB 
      ,EK_RODZAJE_ABSENCJI RDA  		  	
where AB.AB_RDA_ID=RDA.RDA_ID 
      and ab_prc_id = p.prc_id
      and (EXTRACT(YEAR FROM AB.AB_DATA_OD) <= p.rok and p.rok <= EXTRACT(YEAR FROM AB.AB_DATA_DO))' 
WHERE      LST_ID = -311131;
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('2','PPL_ABS_ABSENCJE','','50','Data pocz�tkowa','T','N','ab_data_od','-311132','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2019-03-12 08:43:32','PCMGOLDA\Mikolaj.Golda','10.132.216.96\NIEZNANY','1','50','N','-311131','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('3','PPL_ABS_ABSENCJE','','50','Data ko�cowa','T','N','ab_data_do','-311133','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:29','2019-03-12 08:43:32','PCMGOLDA\Mikolaj.Golda','10.132.216.96\NIEZNANY','1','50','N','-311131','','','','','','','','','','','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_ABS_ABSENCJE' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10012','-311131','EGADM1.EK_ABSENCJE_DANE','EGADM1','EK_ABSENCJE_DANE','EGADM1','EK_ABSENCJE','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10013','-311131','EGADM1.EK_RODZAJE_ABSENCJI','EGADM1','EK_RODZAJE_ABSENCJI','PUBLIC','EK_RODZAJE_ABSENCJI','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-10014','-311131','EGADM1.EK_ZWOLNIENIA','EGADM1','EK_ZWOLNIENIA','EGADM1','EK_ABSENCJE','VIEW','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
