-- data version: PPL_PRC_LISTA_PLAC_OKRESY 2020.04.23 10:02.23
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:47:30
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_PRC_LISTA_PLAC_OKRESY';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_PRC_LISTA_PLAC_OKRESY' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_PRC_LISTA_PLAC_OKRESY';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_PRC_LISTA_PLAC_OKRESY')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_PRC_LISTA_PLAC_OKRESY','Okresy list p�ac','50','Okres','T','N','PP_LST_OKRES','-311090','','','','','','','','KAD','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:34','2020-04-23 10:02:23','PCMGOLDA\Mikolaj.Golda','0:0:0:0:0:0:0:1\NIEZNANY','248','50','N','','0','N','N','N','N','N','','','','2020-06-23 20:30:09','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'		
select distinct
  PP_LST_OKRES
from ppv_lista_plac_skladniki
where PP_SK_PRC_ID = :POLE_1
order by PP_LST_OKRES desc
' 
WHERE      LST_ID = -311090;
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_PRC_LISTA_PLAC_OKRESY' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-994','-311090','EGADM1.EK_DEF_GRUP','EGADM1','EK_DEF_GRUP','PUBLIC','EK_DEF_GRUP','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-995','-311090','EGADM1.EK_DEF_KATEGORII','EGADM1','EK_DEF_KATEGORII','PUBLIC','EK_DEF_KATEGORII','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-996','-311090','EGADM1.EK_DEF_RODZAJOW_LIST','EGADM1','EK_DEF_RODZAJOW_LIST','PUBLIC','EK_DEF_RODZAJOW_LIST','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-997','-311090','EGADM1.EK_DEF_SKLADNIKOW','EGADM1','EK_DEF_SKLADNIKOW','PUBLIC','EK_DEF_SKLADNIKOW','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-998','-311090','EGADM1.EK_GRUPY_KODOW','EGADM1','EK_GRUPY_KODOW','PUBLIC','EK_GRUPY_KODOW','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-999','-311090','EGADM1.EK_LISTY','EGADM1','EK_LISTY','PUBLIC','EK_LISTY','SYNONYM','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-1000','-311090','EGADM1.EK_SKLADNIKI','EGADM1','EK_SKLADNIKI','EGADM1','EKV_SKLADNIKI_POKRYTE','VIEW','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
