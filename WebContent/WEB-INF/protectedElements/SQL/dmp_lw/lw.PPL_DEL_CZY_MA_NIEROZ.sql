-- data version: PPL_DEL_CZY_MA_NIEROZ 2020.04.24 18:47.26
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:50:57
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_DEL_CZY_MA_NIEROZ';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_DEL_CZY_MA_NIEROZ' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_DEL_CZY_MA_NIEROZ';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_DEL_CZY_MA_NIEROZ')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_DEL_CZY_MA_NIEROZ','','50','czy_ma_nieroz_delegacje','T','N','czy_ma_nieroz_delegacje','-311959','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2019-04-08 08:05:31','2020-04-24 18:47:26','PCMGOLDA\Mikolaj.Golda','10.132.80.82\NIEZNANY','3116','50','N','','0','N','N','N','N','N','2018-12-14 12:59:37','T','1','2020-06-23 18:02:29','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'select 
  decode (count(*), 0 , ''N'', ''T'') czy_ma_nieroz_delegacje 

from (
SELECT
 * 
FROM  (select :POLE_1 prcid from dual) par
  JOIN PPT_DEL_WNIOSKI_DELEGACJI WND on WND.WND_PRC_ID=par.prcid
  left JOIN EGADM1.CSST_ENCJE_W_STANACH EWS_K on EWS_K.EWS_KLUCZ_OBCY_ID=WND.WND_ID and EWS_K.EWS_KOB_KOD=''WN_DELKv2''
  left JOIN EGADM1.CSST_DEF_STANOW DSTN_K on EWS_K.EWS_DSTN_ID=DSTN_K.DSTN_ID
  left JOIN EGADM1.CSST_ENCJE_W_STANACH EWS_Z on EWS_Z.EWS_KLUCZ_OBCY_ID=WND.WND_ID and EWS_Z.EWS_KOB_KOD=''WN_DELZv2''
  left JOIN EGADM1.CSST_DEF_STANOW DSTN_Z on EWS_Z.EWS_DSTN_ID=DSTN_Z.DSTN_ID
WHERE  1=1
  AND 
  (    DSTN_K.dstn_nazwa IN(''POZIOM50'',''POZIOM60'') 
    OR DSTN_Z.dstn_nazwa IN(''POZIOM60'',''POZIOM70'',''POZIOM80'',''POZIOM90'',''POZIOM100'')
  )
  
  AND WND.WND_F_ZALICZKA=''T''
  AND trunc(WND.WND_DATA_POWROTU)+14<trunc(sysdate)
  AND rownum=1 
)' 
WHERE      LST_ID = -311959;
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_DEL_CZY_MA_NIEROZ' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-432','-311959','EGADM1.CSST_DEF_STANOW','EGADM1','CSST_DEF_STANOW','PPADM','PPV_LW_COMPILE_DEPENDENCIES','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-433','-311959','EGADM1.CSST_ENCJE_W_STANACH','EGADM1','CSST_ENCJE_W_STANACH','PPADM','PPV_LW_COMPILE_DEPENDENCIES','VIEW','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-434','-311959','PPADM.PPT_DEL_WNIOSKI_DELEGACJI','PPADM','PPT_DEL_WNIOSKI_DELEGACJI','PPADM','PPV_LW_COMPILE_DEPENDENCIES','VIEW','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
