-- data version: PP_OCE_LISTA_ZAPLAN 2020.04.23 14:05.02
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:46:31
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PP_OCE_LISTA_ZAPLAN';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PP_OCE_LISTA_ZAPLAN' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PP_OCE_LISTA_ZAPLAN';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PP_OCE_LISTA_ZAPLAN')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PP_OCE_LISTA_ZAPLAN','Lista zaplanowanych ocen','40','Nr oceny','T','N','oce_id','-310477','','','','','','','','OCE','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-06-23 20:34:54','2020-04-23 14:05:02','10.132.216.41\tomcat','0:0:0:0:0:0:0:1\NIEZNANY','1','0','N','','0','N','N','T','N','N','2019-10-03 12:18:57','T','6','2020-04-23 14:05:02','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'select 
  oce.oce_id,
  oceniany.prc_numer prc_nr_oceniany,
  oceniany.PRC_IMIE || '' '' || oceniany.PRC_NAZWISKO prc_oceniany, 
  oceniajacy.prc_numer prc_nr_oceniajacy,
  oceniajacy.PRC_IMIE || '' '' || oceniajacy.PRC_NAZWISKO prc_oceniajacy, 
  oce_max_data_oceny,
  oce_data_oceny,
    --(select dstn_nazwa from csst_def_stanow dstn where dstn.dstn_id = ews_dstn_id) dstn_nazwa,
    --(select dstn_opis from csst_def_stanow dstn where dstn.dstn_id = ews_dstn_id) dstn_opis
  dstn.dstn_nazwa,
  dstn.dstn_opis,
  hio.hio_data,
  (select stjo_nazwa from ZPT_ST_W_JO 
  where stjo_id = GET_PRC_STJO_ID(oceniany.prc_id, oce.OCE_DATA_OCENY)) stanowisko,
  GET_RODZAJ_UMOWY(oceniany.prc_id, oce.OCE_DATA_OCENY) rodzaj_umowy,
  --dzial.ob_nazwa,
  dep.OB_NAZWA,
  GET_PRC_DEPARTAMENT_ID(oceniany.prc_id, nvl(oce.OCE_DATA_OCENY,sysdate) ) dep_id,
  SREDNIA_OCEN_TXT(oce.oce_id) srednia_ocen,
  POZIOM_SPELNIENIA(oce.oce_id) poziom_spelnienia,
  OCENA_WYNIKOWA(oce.oce_id) ocena_wynikowa,
    (select POZIOM_SPELNIENIA(oce_id) from
    (select poprz_oce.oce_id from PPT_OCE_OCENY poprz_oce
      left join csst_encje_w_stanach ews1 on ews1.EWS_KLUCZ_OBCY_ID = poprz_oce.oce_id and ews1.EWS_KOB_KOD = ''WN_OCENA_OKRES''
      left join csst_def_stanow dstn1 on ews1.ews_dstn_id = dstn1.dstn_id 
    where poprz_oce.oce_prc_id = oce.OCE_PRC_ID
    and poprz_oce.oce_data_oceny < oce.oce_data_oceny
    and dstn1.dstn_nazwa = ''STOP''
    order by poprz_oce.oce_data_oceny desc
    ) where rownum = 1) poprzednia_ocena,
    
  (select OCE_DATA_OCENY from
    (select poprz_oce.OCE_DATA_OCENY from PPT_OCE_OCENY poprz_oce 
      left join csst_encje_w_stanach ews1 on ews1.EWS_KLUCZ_OBCY_ID = poprz_oce.oce_id and ews1.EWS_KOB_KOD = ''WN_OCENA_OKRES''
      left join csst_def_stanow dstn1 on ews1.ews_dstn_id = dstn1.dstn_id 
    where poprz_oce.oce_prc_id = oce.OCE_PRC_ID
    and poprz_oce.oce_data_oceny < oce.oce_data_oceny
    and dstn1.dstn_nazwa = ''STOP''
    order by poprz_oce.oce_data_oceny desc
    ) where r' 
WHERE      LST_ID = -310477;
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = LST_QUERY ||
'ownum = 1) data_poprz_oceny,
    
  (select rv_abbreviation from
    (select rv_abbreviation
    from PPT_OCE_OCENY poprz_oce 
    inner join css_ref_codes 
    on rv_domain = ''EK_KSC_STOPIEN'' and rv_low_value = OCE_WN_KOL_STOPIEN
    left join csst_encje_w_stanach ews1 on ews1.EWS_KLUCZ_OBCY_ID = poprz_oce.oce_id and ews1.EWS_KOB_KOD = ''WN_OCENA_OKRES''
    left join csst_def_stanow dstn1 on ews1.ews_dstn_id = dstn1.dstn_id
    where poprz_oce.oce_prc_id = oce.OCE_PRC_ID
    and poprz_oce.oce_data_oceny < oce.oce_data_oceny
    and dstn1.dstn_nazwa = ''STOP''
    and OCE_CZY_WN_KOL_STOP = ''T''
    order by poprz_oce.oce_data_oceny desc
    ) where rownum = 1) poprzedni_stopien,
    
  (select oce_data_oceny from
    (select oce_data_oceny
    from PPT_OCE_OCENY poprz_oce 
    inner join css_ref_codes 
    on rv_domain = ''EK_KSC_STOPIEN'' and rv_low_value = OCE_WN_KOL_STOPIEN
    left join csst_encje_w_stanach ews1 on ews1.EWS_KLUCZ_OBCY_ID = poprz_oce.oce_id and ews1.EWS_KOB_KOD = ''WN_OCENA_OKRES''
    left join csst_def_stanow dstn1 on ews1.ews_dstn_id = dstn1.dstn_id
    where poprz_oce.oce_prc_id = oce.OCE_PRC_ID
    and poprz_oce.oce_data_oceny < oce.oce_data_oceny
    and dstn1.dstn_nazwa = ''STOP''
    and OCE_CZY_WN_KOL_STOP = ''T''
    order by poprz_oce.oce_data_oceny desc
    ) where rownum = 1) data_poprz_stopnia,

hioSTOP.hio_data OCE_DATA_DO
 
  
from  (select ? dblogin from dual) par 
  join PPT_OCE_OCENY oce on 1=1 
  join EGADM1.EK_PRACOWNICY oceniany    on oce.OCE_PRC_ID = oceniany.prc_id
  join EGADM1.EK_PRACOWNICY oceniajacy  on oce.OCE_PRC_ID_OCENIAJACY = oceniajacy.prc_id
  left join csst_encje_w_stanach ews on ews.EWS_KLUCZ_OBCY_ID = oce.oce_id and ews.EWS_KOB_KOD = ''WN_OCENA_OKRES''
  left join csst_def_stanow dstn on ews_dstn_id = dstn.dstn_id 
  left join PPV_HISTORIA_OBIEGOW_LAST hio on hio.HIO_KLUCZ_OBCY_ID = oce.oce_id and hio.HIO_KOB_KOD = ''WN_OCENA_OKRES'' and hio.DSTN_NAZWA = ''POZIOM30''
  left join EK_OBIEKTY_W_PRZEDSIEB dep on dep.ob_id = GET_PR' 
WHERE      LST_ID = -310477;
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = LST_QUERY ||
'C_DEPARTAMENT_ID(oceniany.prc_id, nvl(oce.OCE_DATA_OCENY,sysdate) )
left join PPV_HISTORIA_OBIEGOW_LAST hioSTOP on hioSTOP.HIO_KLUCZ_OBCY_ID = oce.oce_id and hioSTOP.HIO_KOB_KOD = ''WN_OCENA_OKRES'' and hioSTOP.DSTN_NAZWA = ''STOP''

where 1=1
and ( 1=0
            or PPADM.fu( par.dblogin , ''PP_OCE_ADMINISTRATOR'') = ''T''
            or oceniany.prc_id in (  SELECT * FROM TABLE(PPP_UTILITIES.prc_podlegli_bez(?,sysdate))  )
            or oceniajacy.PRC_ID=?
            or oceniany.PRC_ID=?
         )  

   --- and (dstn_nazwa IS NULL or dstn_nazwa in(''START'',''POZIOM10'',''POZIOM20'')) 
        and (dstn.dstn_nazwa in (''POZIOM30'', ''POZIOM40'') or 1=?) 
        and (dstn.dstn_nazwa in (''STOP'') or 1=?)
order by oce_id desc' 
WHERE      LST_ID = -310477;
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('2','PP_OCE_LISTA_ZAPLAN','','80','Nr ocenianego','T','N','prc_nr_oceniany','-310630','Nr pracownika ocenianego','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-06-23 20:34:54','2019-10-03 12:18:57','10.132.216.41\tomcat','0:0:0:0:0:0:0:1\NIEZNANY','1','13','N','-310477','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('3','PP_OCE_LISTA_ZAPLAN','','120','Oceniany','T','N','prc_oceniany','-310478','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-06-23 20:34:54','2019-10-03 12:18:57','10.132.216.41\tomcat','0:0:0:0:0:0:0:1\NIEZNANY','1','128','N','-310477','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('4','PP_OCE_LISTA_ZAPLAN','','80','Nr oceniaj�?cego','T','N','prc_nr_oceniajacy','-310632','Nr pracownika oceniaj�?cego','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-06-23 20:34:54','2019-10-03 12:18:57','10.132.216.41\tomcat','0:0:0:0:0:0:0:1\NIEZNANY','1','13','N','-310477','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('5','PP_OCE_LISTA_ZAPLAN','','120','Oceniaj�?cy','T','N','prc_oceniajacy','-310479','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-06-23 20:34:54','2019-10-03 12:18:57','10.132.216.41\tomcat','0:0:0:0:0:0:0:1\NIEZNANY','1','128','N','-310477','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('6','PP_OCE_LISTA_ZAPLAN','','150','Termin sporz�?dzenia oceny','T','N','oce_max_data_oceny','-310480','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-06-23 20:34:54','2019-10-03 12:18:57','10.132.216.41\tomcat','0:0:0:0:0:0:0:1\NIEZNANY','1','128','N','-310477','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('7','PP_OCE_LISTA_ZAPLAN','','100','Data oceny','T','T','oce_data_oceny','-310639','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-06-23 20:34:54','2019-10-03 12:18:57','10.132.216.41\tomcat','0:0:0:0:0:0:0:1\NIEZNANY','1','26','N','-310477','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('8','PP_OCE_LISTA_ZAPLAN','','100','Kod stanu','T','T','dstn_nazwa','-310640','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-06-23 20:34:54','2019-10-03 12:18:57','10.132.216.41\tomcat','0:0:0:0:0:0:0:1\NIEZNANY','1','13','N','-310477','','','','','','','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('9','PP_OCE_LISTA_ZAPLAN','','220','Stan obiegu','T','N','dstn_opis','-310641','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-06-23 20:34:54','2019-10-03 12:18:57','10.132.216.41\tomcat','0:0:0:0:0:0:0:1\NIEZNANY','1','64','N','-310477','','','','','','','','','','','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PP_OCE_LISTA_ZAPLAN' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
