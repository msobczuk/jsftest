------------------------------------------------------------------------------------------
-- DMP_LW: WAL
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='WAL';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'WAL' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'WAL';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('WAL')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_CSV, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_XML, LST_SQL_ILE_PARAMETROW, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','WAL','Waluty (wg CSS_WALUTY)','50','wal_id','T','T','wal_id','-313962','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2020-06-25 19:55:27','2020-07-02 21:12:51','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','5','100','N','','0','N','T','T','T','T','0','2020-07-02 21:12:49','T','2020-07-02 21:21:25','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'select 
    wal_id, 
    wal_symbol, 
    wal_kod, 
    wal_pelna_nazwa, 
    wal_jednostka_kursu
from css_waluty
order by wal_symbol' 
WHERE      LST_ID = -313962;
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_CSV, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_XML, LST_SQL_ILE_PARAMETROW, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('2','WAL','','100','Symbol','T','N','wal_symbol','-313963','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2020-06-25 19:55:27','2020-07-02 21:12:49','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','4','100','N','-313962','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_CSV, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_XML, LST_SQL_ILE_PARAMETROW, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('3','WAL','','100','Kod','T','T','wal_kod','-313964','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2020-06-25 19:55:27','2020-07-02 21:12:49','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','4','100','N','-313962','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_CSV, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_XML, LST_SQL_ILE_PARAMETROW, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('4','WAL','','200','Nazwa','T','N','wal_pelna_nazwa','-313965','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2020-06-25 19:55:27','2020-07-02 21:12:49','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','4','100','N','-313962','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_CSV, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_XML, LST_SQL_ILE_PARAMETROW, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('5','WAL','','100','Jednostka kursu','T','T','wal_jednostka_kursu','-313994','','','','','','','','','','','','','','','','','','','PPADM','ADMINISTRATOR_PORTAL','2020-06-25 19:55:27','2020-07-02 21:12:49','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','4','100','N','-313962','','','T','T','T','T','','','','','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'WAL' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_TABELE        ( LSTB_ID, LSTB_LST_ID, LSTB_TBTS_TABLE, LSTB_TABLE_OWNER, LSTB_TABLE_NAME, LSTB_REF_BY_OWNER, LSTB_REF_BY_NAME, LSTB_REF_BY_TYPE, LSTB_F_IGNORUJ  )
  values ('-11256','-313962','EGADM1.CSS_WALUTY','EGADM1','CSS_WALUTY','PUBLIC','CSS_WALUTY','SYNONYM','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
