-- data version: PPL_ABS_WALIDACJA_URLOPU 2020.03.13 14:13.22
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.23 20:50:43
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='T' 
where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME='PPL_ABS_WALIDACJA_URLOPU';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_ABS_WALIDACJA_URLOPU' );
DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = 'PPL_ABS_WALIDACJA_URLOPU';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('PPL_ABS_WALIDACJA_URLOPU')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_LISTY_WARTOSCI        ( LST_LP, LST_NAZWA, LST_OPIS, LST_DLUGOSC, LST_ETYKIETA, LST_F_ZWROT, LST_F_UKRYTE, LST_NAZWA_POLA, LST_ID, LST_OPIS2, LST_OPIS3, LST_OPIS4, LST_OPIS5, LST_OPIS6, LST_OPIS7, LST_OPIS8, LST_OPIS9, LST_ETYKIETA2, LST_ETYKIETA3, LST_ETYKIETA4, LST_ETYKIETA5, LST_ETYKIETA6, LST_ETYKIETA7, LST_ETYKIETA8, LST_ETYKIETA9, LST_LB_CODE_OPIS, LST_LB_CODE_ETYKIETA, LST_AUDYT_UT, LST_AUDYT_UM, LST_AUDYT_DT, LST_AUDYT_DM, LST_AUDYT_KT, LST_AUDYT_KM, LST_AUDYT_LM, LST_DLUGOSC_800_600, LST_F_INTERFEJS, LST_LST_ID, LST_QUERY, LST_KONSOLIDUJ, LST_CZY_PDF, LST_CZY_XLS, LST_CZY_CSV, LST_CZY_XML, LST_SQL_TEST_KIEDY, LST_SQL_TEST_POPRAWNY, LST_SQL_ILE_PARAMETROW, LST_WCZYTWANO_KIEDY, LST_F_AKTUALIZUJ  )
  values ('1','PPL_ABS_WALIDACJA_URLOPU','','50','dni_na_ktorych_nie_ma_limitu','T','N','dni_na_ktorych_nie_ma_limitu','-313814','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2019-10-07 15:43:56','2020-03-13 14:13:22','0:0:0:0:0:0:0:1\NIEZNANY','10.132.45.10\NIEZNANY','64','100','N','','0','N','T','T','T','T','2020-01-17 11:42:11','N','0','2020-06-23 15:02:07','');
-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
UPDATE PPADM.PPT_ADM_LISTY_WARTOSCI SET LST_QUERY = '' ||
'------------------------------------------------------------------------------------------
-- dni dla ktorych nie m limitu urlopowego
------------------------------------------------------------------------------------------
with  dni as (
  SELECT to_date(:P_DATA_OD) + ROWNUM -1 as dz
  FROM   ( SELECT 1 just_a_column
           FROM   dual
           CONNECT BY LEVEL <= 365
         )
), 
limity as (
select * from EGADM1.EK_LIMITY_ABSENCJI
    join EGADM1.EK_LIMITY_DANE on la_id = ld_la_id 
    join EK_DEF_GRUP on la_dg_kod = dg_kod
    left join EK_GRUPY_ABSENCJI on dg_kod = GA_DG_KOD--(SELECT GA_DG_KOD, MIN(GA_RDA_ID) GA_RDA_ID FROM EK_GRUPY_ABSENCJI GROUP BY GA_DG_KOD)
    left join EK_RODZAJE_ABSENCJI on ga_rda_id = rda_id
    where 1=1 
      and lower (rda_id) = :P_RDA_ID
      and la_prc_id = :P_PRC_ID
)
select 
  count(*) dni_na_ktorych_nie_ma_limitu
from dni
left join limity on dni.dz between ld_od and ld_do
where 1=1 
  and dz between to_date(:P_DATA_OD) and to_date(:P_DATA_DO)
  and ld_id is null' 
WHERE      LST_ID = -313814;
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_LISTY_TABELE','LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( 'PPL_ABS_WALIDACJA_URLOPU' ) ) ')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
