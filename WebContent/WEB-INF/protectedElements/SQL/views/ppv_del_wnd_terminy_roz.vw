create or replace view PPADM.PPV_DEL_WND_TERMINY_ROZ as 
SELECT
-- $Revision:: 1               $ ---
  wnd.wnd_id,
  nvl(max(trs.TRS_DATA_DO),(SELECT WND_DATA_POWROTU FROM PPT_DEL_WNIOSKI_DELEGACJI WHERE wnd.WND_ID = PPT_DEL_WNIOSKI_DELEGACJI.WND_ID))+14 termin_do_kiedy_rozliczenie  
FROM PPT_DEL_WNIOSKI_DELEGACJI wnd 
join PPT_DEL_KALKULACJE kal on wnd.wnd_id = kal.KAL_WND_ID and kal.KAL_RODZAJ = 'ROZ'
left join PPT_DEL_TRASY trs on trs.TRS_KAL_ID = kal.kal_id 
GROUP BY wnd.WND_ID
ORDER BY wnd.WND_ID desc
/

--ascii/cp1250:����󜟿
