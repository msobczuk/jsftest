create or replace view PPADM.PPV_DEL_WND_CELE as 
SELECT 
-- $Revision:: 1               $ ---
  cdel.CDEL_WND_ID,
  LISTAGG(kr.KR_NAZWA||' '||cdel.CDEL_MIEJSCOWOSC, ', ') WITHIN GROUP (ORDER BY cdel.CDEL_ID) cele
FROM PPT_DEL_CELE_DELEGACJI cdel
join css_kraje kr on cdel.CDEL_KR_ID = kr.kr_id
GROUP BY cdel.CDEL_WND_ID
ORDER BY cdel.CDEL_WND_ID desc
/

--ascii/cp1250:����󜟿
