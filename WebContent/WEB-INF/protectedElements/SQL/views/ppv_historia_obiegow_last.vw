create or replace force view PPADM.PPV_HISTORIA_OBIEGOW_LAST as
select 
-- $Revision::          $-- --ascii/cp1250:����󜟿--
  hio."HIO_ID",hio."HIO_KLUCZ_OBCY_ID",hio."HIO_DATA",hio."HIO_UZT_NAZWA",hio."HIO_OPIS",hio."HIO_KOB_KOD",hio."HIO_OBG_ID",hio."HIO_DSTN_ID",hio."HIO_TYP",hio."COB_ID",hio."COB_NAZWA",hio."COB_NAZWA2",hio."DSTN_ID",hio."DSTN_NAZWA",hio."DSTN_OPIS",hio."DSTN_ID_Z",hio."DSTN_NAZWA_Z",hio."DSTN_OPIS_Z",hio."PRC_ID",hio."PRC_NUMER",hio."PRC_IMIE",hio."PRC_NAZWISKO",hio."ZAT_ID",hio."ZAT_OB_ID",hio."STN_ID",hio."STN_NAZWA",hio."OB_ID",hio."OB_NAZWA",hio."OB_KOD",hio."OB_PELNY_KOD",hio."DEP_ID",hio."DEP_NAZWA",hio."DEP_KOD",hio."DEP_PELNY_KOD" 
from PPV_HISTORIA_OBIEGOW hio
	    join ( 
              select -- jesli czynnosc powtarzano (bo np. byly korekty) - tylko ta ostatnio wykonana
                max(hio_id) max_hio_id --, hio_kob_kod, hio_klucz_obcy_id, HIO_COB_ID 
              from CSST_HISTORIA_OBIEGOW 
              group by  hio_kob_kod, hio_klucz_obcy_id, HIO_COB_ID
            ) lasthio
          on lasthio.max_hio_id = hio.hio_id
order by HIO_OBG_ID, HIO_KLUCZ_OBCY_ID, HIO_ID DESC
/
