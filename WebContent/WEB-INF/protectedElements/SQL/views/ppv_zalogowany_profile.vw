create or replace view PPADM.PPV_ZALOGOWANY_PROFILE as 
select
    -- $Revision::          $--
	EAT_PROFILE.PRF_ID
    ,EAT_PROFILE.PRF_NAZWA
    ,EAT_PROFILE.PRF_INS_ID
    ,EAT_PROFILE.PRF_FRM_ID
    ,EAT_PROFILE.PRF_AUDYT_UT
    ,EAT_PROFILE.PRF_AUDYT_DT
    ,EAT_PROFILE.PRF_AUDYT_LM
    ,EAT_PROFILE.PRF_AUDYT_UM
    ,EAT_PROFILE.PRF_AUDYT_DM
    ,EAT_PROFILE.PRF_OPIS
    ,EAT_PROFILE.PRF_AUDYT_KT
    ,EAT_PROFILE.PRF_AUDYT_KM
    ,EAT_PROFILE.PRF_F_EGERIA
    ,EAT_PROFILE.PRF_F_KOKPITY
    ,EAT_PROFILE.PRF_F_PORTAL
    ,EAT_PROFILE.PRF_F_CONTROLLING
    ,EAT_PROFILE.PRF_F_GENERATOR
    ,EAT_PROFILE.PRF_F_EDUKACJA
    ,PPV_PROFILE_PROFILE_MACIERZ.PRF_ID_NAD
    ,PPV_PROFILE_PROFILE_MACIERZ.PRF_ID_POD
    ,PPV_PROFILE_PROFILE_MACIERZ.MIN_LVL
from EAADM.EAT_DOSTEPY_DO_FIRM 
  join PPV_PROFILE_PROFILE_MACIERZ on PRF_ID_NAD = DDF_PRF_ID
  join EAADM.EAT_PROFILE on PRF_ID = PRF_ID_POD
where ddf_uzt_nazwa = (select uzt_nazwa from ppv_zalogowany)
/

--ascii/cp1250:����󜟿
