create or replace view PPADM.PPV_DEL_ZF_ANGAZE as 
SELECT 
-- $Revision::          $-- 
     PKZF_ID
    ,PKZF_PKL_ID
    ,PKZF_SK_ID
    ,PKZF_KWOTA
    ,PKZF_PROCENT
    ,PKZF_OPIS
    ,PKZF_AUDYT_UT
    ,PKZF_AUDYT_DT
    ,PKZF_AUDYT_UM
    ,PKZF_AUDYT_DM
    ,PKZF_AUDYT_LM
    ,PKZF_AUDYT_KM
    ,ANG_ID
    ,ANG_WNR_ID
    ,ANG_PKZF_ID
    ,ANG_WND_ID
    ,ANG_KWOTA
    ,ANG_PROCENT
    ,ANG_OPIS
    ,ANG_AUDYT_UT
    ,ANG_AUDYT_DT
    ,ANG_AUDYT_UM
    ,ANG_AUDYT_DM
    ,ANG_AUDYT_LM
    ,ANG_AUDYT_KM
FROM ppt_del_zrodla_finansowania
LEFT JOIN ppt_del_angaze ON ang_pkzf_id = pkzf_id
/

--ascii/cp1250:����󜟿--
