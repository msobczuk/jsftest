create or replace force view PPADM.PPV_STRUKTURA_PODL_PRC_MACIERZ as
select 
-- $Revision::         $--
-- str. podl. pracownikow jako macierz prc_id_prz x prc_id = min(lvl)
    prc_id_prz, prc_id, min(LVL) as min_lvl
from (
    
    select prc_id_prz, prc_id, lvl 
    from ppv_struktura_podleglosci_prc 
    where not prc_id_prz is null and not prc_id is null
    
    union --przekatna: lvl = 0
    select prc_id, prc_id, 0 
    from ek_pracownicy
)
group by prc_id_prz, prc_id
order by prc_id_prz, prc_id, min_lvl
/
