create or replace view PPADM.PPV_DEL_WND_ZAPRASZAJACY as 
SELECT 
-- $Revision:: 1               $ ---
  ZAPR_WND_ID,
  LISTAGG(
          CASE
            WHEN wnd.WND_RODZAJ=1 THEN zapr.ZAPR_OPIS
            WHEN wnd.WND_RODZAJ=2 and ZAPR_F_INNE = 'N' THEN wsl.WSL_ALIAS
            WHEN wnd.WND_RODZAJ=2 and ZAPR_F_INNE = 'T' THEN zapr.ZAPR_OPIS
          END
  , '; ') WITHIN GROUP (ORDER BY zapr.ZAPR_WND_ID) zapraszajacy 
FROM PPT_DEL_ZAPRASZAJACY zapr
join PPT_DEL_WNIOSKI_DELEGACJI wnd on zapr.ZAPR_WND_ID = wnd.wnd_id
left join PPT_ADM_WARTOSCI_SLOWNIKOW wsl on wsl.WSL_SL_NAZWA = 'DEL_ZAPRASZAJACY' and zapr.ZAPR_OPIS = wsl.WSL_WARTOSC
GROUP BY ZAPR_WND_ID
ORDER BY ZAPR_WND_ID desc
/

--ascii/cp1250:����󜟿
