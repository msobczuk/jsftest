create or replace VIEW PPADM.PPV_REF_CODES as
select
-- $Revision::          $--
  ppt_adm_wartosci_slownikow.wsl_sl_nazwa     rv_domain
 ,ppt_adm_wartosci_slownikow.wsl_typ	  rv_type
 ,ppt_adm_wartosci_slownikow.wsl_wartosc	  rv_low_value
 ,ppt_adm_wartosci_slownikow.wsl_wartosc_max  rv_high_value
 ,decode (eap_lang.get_lang_id,
	1 ,ppt_adm_wartosci_slownikow.wsl_alias,
	2 ,ppt_adm_wartosci_slownikow.wsl_alias2,
	3 ,ppt_adm_wartosci_slownikow.wsl_alias3,
	4 ,ppt_adm_wartosci_slownikow.wsl_alias4,
	5 ,ppt_adm_wartosci_slownikow.wsl_alias5,
	6 ,ppt_adm_wartosci_slownikow.wsl_alias6,
	7 ,ppt_adm_wartosci_slownikow.wsl_alias7,
	8 ,ppt_adm_wartosci_slownikow.wsl_alias8,
	9 ,ppt_adm_wartosci_slownikow.wsl_alias9,
	   ppt_adm_wartosci_slownikow.wsl_alias) rv_abbreviation
 ,decode (eap_lang.get_lang_id,
	1 ,ppt_adm_wartosci_slownikow.wsl_opis,
	2 ,ppt_adm_wartosci_slownikow.wsl_opis2,
	3 ,ppt_adm_wartosci_slownikow.wsl_opis3,
	4 ,ppt_adm_wartosci_slownikow.wsl_opis4,
	5 ,ppt_adm_wartosci_slownikow.wsl_opis5,
	6 ,ppt_adm_wartosci_slownikow.wsl_opis6,
	7 ,ppt_adm_wartosci_slownikow.wsl_opis7,
	8 ,ppt_adm_wartosci_slownikow.wsl_opis8,
	9 ,ppt_adm_wartosci_slownikow.wsl_opis9,
	   ppt_adm_wartosci_slownikow.wsl_opis) rv_meaning
from ppt_adm_wartosci_slownikow
/
