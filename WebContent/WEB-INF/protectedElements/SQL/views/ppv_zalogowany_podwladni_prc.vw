create or replace force view PPADM.PPV_ZALOGOWANY_PODWLADNI_PRC as
select 
-- $Revision::          $--
     prc.PRC_ID
    ,prc.PRC_NUMER
    ,prc.PRC_NAZWISKO
    ,prc.PRC_IMIE
    ,prc.PRC_DATA_UR
    ,prc.PRC_MIEJSCE_UR
    ,prc.PRC_PLEC
    ,prc.PRC_STAN_CYWILNY
    ,prc.PRC_IMIE_OJCA
    ,prc.PRC_IMIE_MATKI
    ,prc.PRC_PESEL
    ,prc.PRC_KTO_UTWORZYL
    ,prc.PRC_KIEDY_UTWORZYL
    ,prc.PRC_USK_ID
    ,prc.PRC_IMIE2
    ,prc.PRC_DOWOD_OSOB
    ,prc.PRC_NAZWISKO_ROD
    ,prc.PRC_NIP
    ,prc.PRC_KOMENTARZ
    ,prc.PRC_KTO_MODYFIKOWAL
    ,prc.PRC_KIEDY_MODYFIKOWAL
    ,prc.PRC_OBYWATELSTWO
    ,prc.PRC_KARTA_POBYTU
    ,prc.PRC_AKTYWNY
    ,prc.PRC_UMOWAP
    ,prc.PRC_UMOWAZ
    ,prc.PRC_PASZPORT
    ,prc.PRC_ORGAN_WYDAJACY
    ,prc.PRC_IDENTYFIKACJA
    ,prc.PRC_UZYTKOWNIK
    ,prc.PRC_DG_KOD_EK
    ,prc.PRC_DG_KOD_PL
    ,prc.PRC_MZAM
    ,prc.PRC_KA_ID
    ,prc.PRC_DATA_WYDANIA_DOWODU
    ,prc.PRC_NR_KARTY
    ,prc.PRC_NIP_PREFIKS
    ,prc.PRC_NIP_DANE
    ,prc.PRC_AUDYT_KT
    ,prc.PRC_AUDYT_KM
    ,prc.PRC_AUDYT_LM
    ,prc.PRC_F_OBCY
    ,prc.PRC_AKRONIM
    ,prc.PRC_PRC_ID
    ,prc.PRC_IDENT
    ,prc.PRC_NARODOWOSC
    ,prc.PRC_UMOWAN
    ,prc.PRC_UMOWANA
    ,prc.PRC_NAZWISKO_RODOWE_MATKI
    ,prc.PRC_KR_ID_UR
    ,prc.PRC_POBORCA_REFERENT
    ,prc.PRC_SYMBOL_POBORCY
    --,prc.PRC_FRM_ID
    ,prc.PRC_HASLO_TYM
    ,prc.PRC_DATA_HASLO
    ,prc.PRC_GUID
    ,prc.PRC_UMOWASZ
    ,prc.PRC_UMOWASZN
    ,prc.PRC_UMOWAK
    ,prc.PRC_UMOWAS
    ,prc.PRC_KOD_OBCY
    ,prc.PRC_WAP_ID
    ,prc.PRC_DATA_WAZNOSCI_DOWODU
    ,prc.PRC_F_ZGODA
    ,prc.PRC_STATUS_GIODO
    ,prc.PRC_ZRODLO_DANYCH
    ,sp.min_lvl PRZ_POZIOM
from PPADM.ppv_zalogowany u
 join PPADM.ppv_struktura_podl_prc_macierz sp on u.prc_id = sp.prc_id_prz
 join ek_pracownicy prc on sp.prc_id = prc.prc_id
--order by min_lvl
/

--ascii/cp1250:����󜟿--