create or replace view PPADM.PPV_KURSY_NBP as 
select
-- $Revision:: 1               $ ---
    WAL_ID,     
    WAL_SYMBOL,
    --KW_WAL_Z_ID,  
    KW_DATA, 
    LEAD(KW_DATA) over (PARTITION BY kw_wal_z_id order by kw_data) KW_DATA_DO,
    KW_PRZELICZNIK_KUPNA / WAL_JEDNOSTKA_KURSU KW_PRZELICZNIK_KUPNA, 
    KW_PRZELICZNIK_SPRZEDAZY / WAL_JEDNOSTKA_KURSU  KW_PRZELICZNIK_SPRZEDAZY, 
    KW_PRZELICZNIK_SREDNI / WAL_JEDNOSTKA_KURSU  KW_PRZELICZNIK_SREDNI, 
    WAL_JEDNOSTKA_KURSU,
    
    KW_PRZELICZNIK_KUPNA KW_PRZELICZNIK_KUPNA_W1,
    KW_PRZELICZNIK_SPRZEDAZY KW_PRZELICZNIK_SPRZEDAZY_W1,
    KW_PRZELICZNIK_SREDNI KW_PRZELICZNIK_SREDNI_W1

from css_kursy
 join css_waluty on kw_wal_z_id = wal_id
 join EGADM1.CSS_TABELE_KURSOW on kw_tk_id = tk_id and TK_SYMBOL = 'NBP'
where KW_WAL_NA_ID = 1
    and KW_PRZELICZNIK_KUPNA is not null 
    and KW_PRZELICZNIK_SPRZEDAZY is not null 
    and KW_PRZELICZNIK_SREDNI is not null

union 

SELECT 1, 'PLN', sysdate-100000, sysdate+100000, 1,1,1,1, 1, 1, 1  from dual    
    
order by WAL_ID desc, kw_data desc
/

--ascii/cp1250:����󜟿
