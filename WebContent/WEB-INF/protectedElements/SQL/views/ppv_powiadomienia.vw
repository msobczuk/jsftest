create or replace view PPADM.PPV_POWIADOMIENIA as 
SELECT   UZT.uzt_nazwa pp_nazwa_uzytkownika,
-- $Revision::          $--
            uzt.UZT_PRC_ID PP_PRC_ID,
            WDM_ID PP_WDM_ID,                     -- identyfikator wiadomo�?ci
            NVL (WDM_DATA_WYSLANIA, WDM_DATA_WSTAWIENIA)
               PP_DATA_POWIADOMIENIA,          -- data i godzina powiadomienia
            WDM_F_PRZECZYTANA PP_F_PRZECZYTANA, -- flaga czy powiadomienie odczytane N/T (nie/tak)
            WDM_TYP PP_TYP, -- typ powiadomienia E/A/M (Egeria/Mail/SMS) - zawsze E
            WDM_TEMAT PP_TEMAT,                         -- temat powiadomienia
            WDM_TRESC PP_TRESC                        -- tre�?�? powiadomienia
     FROM      eat_wiadomosci wdm
            JOIN
               eat_uzytkownicy uzt
            ON uzt.UZT_NAZWA = wdm.WDM_UZT_NAZWA
    WHERE   wdm_typ = 'E'
/

--ascii/cp1250:����󜟿--
