create or replace view PPADM.PPV_LISTA_PLAC_SKLADNIKI as
  SELECT
 			  -- $Revision::         $--
 			  sk_id PP_SK_ID,
               sk_prc_id PP_SK_PRC_ID,
               TO_CHAR (lst_data_obliczen, 'yyyy-mm') PP_LST_OKRES,
               lst_drl_kod PP_LST_DRL_KOD,
               drl_nazwa PP_DRL_NAZWA,
               lst_data_obliczen PP_LST_DATA_OBLICZEN,
               lst_data_wyplaty PP_LST_DATA_WYPLATY,
               dsk_kod PP_DSK_KOD,
               dsk_nazwa PP_DSK_NAZWA,
               sk_wartosc PP_SK_WARTOSC,
               lst_id PP_LST_ID,
               lst_numer PP_LST_NUMER,
               lst_temat PP_LST_TEMAT,
               lst_numer || ' - ' || lst_temat PP_LST_NAZWA,
               DG_DK_KOD PP_DG_DK_KOD,
               DG_NUMER PP_DG_NUMER,
               DG_KOD PP_DG_KOD,
               DG_NAZWA PP_DG_NAZWA

        FROM   ekv_skladniki_pokryte
               join ek_def_skladnikow on dsk_id = sk_dsk_id

               join ek_listy on lst_id = sk_lst_id
               join ek_def_rodzajow_list on drl_kod = lst_drl_kod

               join ek_grupy_kodow on GK_DSK_ID = SK_DSK_ID
               join ek_def_grup on DG_KOD = GK_DG_KOD
               join ek_def_kategorii on DK_KOD = DG_DK_KOD

       WHERE  not sk_wartosc = 0

               AND DG_KOD in ('PASEK_1', 'PASEK_2' ,  'PASEK_3',  'PASEK_4',  'PASEK_5') -- wersja w MF
               --AND DG_KOD in ('PP_PASEK_1', 'PP_PASEK_2' ,  'PP_PASEK_3',  'PP_PASEK_4',  'PP_PASEK_5') -- wersja STD
               AND LST_DATA_WYPLATY <= sysdate
               AND LST_ZATWIERDZONA = 'T'

    --and sk_prc_id = 105744
    --and to_char(LST_DATA_WYPLATY, 'yyyy-mm') = '2018-02'
    --and extract(year from LST_DATA_WYPLATY) = nvl(2007, extract(year from LST_DATA_WYPLATY))
/
