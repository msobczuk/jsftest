create or replace view PPADM.PPV_KIERUJACY_JO as
-- $Revision::          $--
---------------------------------------
-- Zastepujacy kierujacych JO
---------------------------------------
select 
  -obs_id as obk_id, 
  obs_ob_id as ob_id, 
  ob_nazwa, ob_pelny_kod, ob_kod,
  obs_od as data_od, 
  obs_do as data_do, 
  stjo_stn_id as stn_id, 
  STN_NAZWA,
  STO_PRC_ID as prc_id, 
  prc_numer, prc_imie, prc_nazwisko
from CSST_ZARZADZENIE_JO 
  join ZPT_ST_W_JO on obs_stjo_id=STJO_ID
  join ZPT_STANOWISKA on stjo_stn_id = STN_ID
  join ZPT_STANOWISKA_OPIS on STJO_ID = STO_STJO_ID
  join EK_PRACOWNICY on STO_PRC_ID = PRC_ID
  join CSS_OBIEKTY_W_PRZEDSIEB on ob_id = obs_ob_id
where obs_typ = 'K'



union
---------------------------------------
-- Naczelnicy wydzialow
---------------------------------------
select 
  zat_id, 
  ZAT_OB_ID_WYDZIAL OB_ID, 
  ob_nazwa, ob_pelny_kod, ob_kod,
  ZAT_DATA_ZMIANY, 
  ZAT_DATA_DO, 
  ZAT_STN_ID, STN_NAZWA, 
  zat_prc_id, prc_numer, prc_imie, prc_nazwisko
from EK_ZATRUDNIENIE
  join ZPT_STANOWISKA on zat_stn_id = stn_id
  join EK_PRACOWNICY on zat_prc_id = prc_id
  join CSS_OBIEKTY_W_PRZEDSIEB on ob_id = ZAT_OB_ID_WYDZIAL --zat_ob_id
where
     upper (STN_NAZWA) like 'NACZELNIK WYDZIA_U%'
  or upper (STN_NAZWA) like 'PE_NI_CY OBOWI_ZKI NACZELNIKA WYDZIA_U'
--where 
--  upper(NVL(STN_KOD, STN_NAZWA)) in (select upper(WSL_WARTOSC) from PPT_ADM_WARTOSCI_SLOWNIKOW where wsl_sl_nazwa = 'STN_KIERUJACE_JO')



union 
---------------------------------------
-- Dyrektorzy departamentow
---------------------------------------
select 
  zat_id, 
  ZAT_OB_ID OB_ID, 
  ob_nazwa, ob_pelny_kod, ob_kod,
  ZAT_DATA_ZMIANY, 
  ZAT_DATA_DO, 
  ZAT_STN_ID, STN_NAZWA, 
  zat_prc_id, prc_numer, prc_imie, prc_nazwisko
from EK_ZATRUDNIENIE
  join ZPT_STANOWISKA on zat_stn_id = stn_id
  join EK_PRACOWNICY on zat_prc_id = prc_id
  join CSS_OBIEKTY_W_PRZEDSIEB on ob_id = ZAT_OB_ID
where
     upper (STN_NAZWA) like 'DYREKTOR%'
/
