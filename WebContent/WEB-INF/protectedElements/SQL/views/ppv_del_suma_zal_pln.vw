create or replace view PPADM.PPV_DEL_SUMA_ZAL_PLN as 
SELECT 
-- $Revision::          $-- --ascii/cp1250:����󜟿--
  kal.kal_wnd_id, kal.kal_rodzaj, 
  ROUND(SUM(DKZ.DKZ_KWOTA*nvl(DKZ_KURS,1)/nvl(WAL.WAL_JEDNOSTKA_KURSU,1)),2) suma_zal_pln 
FROM PPT_DEL_KALKULACJE KAL
   JOIN PPT_DEL_ZALICZKI DKZ on DKZ.DKZ_KAL_ID=KAL.KAL_ID
   JOIN EGADM1.CSS_WALUTY WAL ON WAL.WAL_ID=DKZ.DKZ_WAL_ID
where DKZ_KWOTA is not null
group by kal.kal_wnd_id, kal.kal_rodzaj
/
