create or replace view PPADM.PPV_REK_KA_ILSOCI as
  select
-- $Revision::          $--      
        ka_nab_id nab_id, 
        count(*) ileKandydatow,
        sum(sK)  ileKobiet,
        sum(sM)  ileMezczyzn,
        sum(sNP) ileNiepelnospr,
        sum(sC)  ileCudzoziemcow,
        
        --ile speln. wymagania formalne
        sum(sWF)   ileWF,
        sum(sKWF)  ileKobietWF,
        sum(sMWF)  ileMezczyznWF,
        sum(sNPWF) ileNiepelnosprWF,
        sum(sCWF)  ileCudzoziemcowWF,
        
        --ile zatrudnionych
        sum(sZ)   ileZatr,
        sum(sKZ)  ileKobietZatr,
        sum(sMZ)  ileMezczyznZatr,
        sum(sNPZ) ileNiepelnosprZatr,
        sum(sCZ)  ileCudzoziemcowZatr
        
      from (
      
        select 
          ka_nab_id, sK, sM, sNP, sC,
      
          decode (KA_F_WYM_FORMALNE, 'T',  1, 0) sWF,
          decode (KA_F_WYM_FORMALNE, 'T', sK, 0) sKWF,
          decode (KA_F_WYM_FORMALNE, 'T', sM, 0) sMWF,
          decode (KA_F_WYM_FORMALNE, 'T', sNP,0) sNPWF,
          decode (KA_F_WYM_FORMALNE, 'T', sC, 0) sCWF,
      
          decode (KA_F_CZY_ZATRUDNIONY, 'T',  1, 0) sZ,
          decode (KA_F_CZY_ZATRUDNIONY, 'T', sK, 0) sKZ,
          decode (KA_F_CZY_ZATRUDNIONY, 'T', sM, 0) sMZ,
          decode (KA_F_CZY_ZATRUDNIONY, 'T', sNP,0) sNPZ,
          decode (KA_F_CZY_ZATRUDNIONY, 'T', sC, 0) sCZ
          
        from (
                select 
                  ka_nab_id, 
                  decode (KA_PLEC, 'K', 1, 0) sK,
                  decode (KA_PLEC, 'M', 1, 0) sM,
                  decode (KA_F_CZY_NIEPELNOSPRAWNY, 'T', 1, 0) sNP,
                  decode (KA_OBYWATELSTWO, 'cudzoziemiec', 1, 0) sC,
                  KA_F_WYM_FORMALNE, 
                  KA_F_CZY_ZATRUDNIONY, 
                  KA_F_WNIOSK_O_ZATR, 
                  KA_F_CZY_WSK_W_PROT
                  
                from PPT_REK_KANDYDACI 
                
              ) X 
       ) XX
      group by ka_nab_id  
/
