create or replace view PPADM.PPV_KSIAZKA_TELEFONICZNA_PRAC as
SELECT DISTINCT
-- $Revision::          $--
              prc_id PP_PRC_ID,
              INITCAP (prc_imie) PP_IMIE,
              INITCAP (prc_nazwisko) PP_NAZWISKO,
              prc_aktywny PP_AKTYWNY,
              (SELECT   tl_numer
                 FROM   ckk_telefony
                WHERE       prc_id = tl_prc_id
                        AND TL_F_DOMYSLNY = 'T'
                        AND TL_TYP = 'E')
                 PP_EMAIL,
              DZLLIST PP_OB_JEDNOSTKA_ORGANIZACYJNA,
              KKON_ID,
              KKON_STANOWISKO,
              KKON_KIERUNKOWY,
              KKON_STACJONARNY,
              KKON_DODATKOWY_1,
              KKON_KOMORKOWY,
              KKON_DODATKOWY_2,
              KKON_WEWNETRZNY,
              KKON_FAKS,
              KKON_UWAGI,
              KKON_KRAJ_ID,
              KKON_MIASTO_ID,
              KKON_BUDYNEK_ID,
              KKON_PIETRO_ID,
              KKON_POKOJ_ID,
              KKON_STATUS_EDYCJI,
              (SELECT   pp_kl.KLOK_WARTOSC
                 FROM   PPT_KAD_KSIAZKA_LOKALIZACJE pp_kl
                WHERE   pp_kl.KLOK_KOD = 'KRAJ'
                        AND pp_kl.KLOK_ID = KKON_KRAJ_ID)
                 KLOK_KRAJ,
              (SELECT   pp_kl.KLOK_WARTOSC
                 FROM   PPT_KAD_KSIAZKA_LOKALIZACJE pp_kl
                WHERE   pp_kl.KLOK_KOD = 'MIASTO'
                        AND pp_kl.KLOK_ID = KKON_MIASTO_ID)
                 KLOK_MIASTO,
              (SELECT   pp_kl.KLOK_WARTOSC
                 FROM   PPT_KAD_KSIAZKA_LOKALIZACJE pp_kl
                WHERE   pp_kl.KLOK_KOD = 'BUDYNEK'
                        AND pp_kl.KLOK_ID = KKON_BUDYNEK_ID)
                 KLOK_BUDYNEK,
              (SELECT   pp_kl.KLOK_WARTOSC
                 FROM   PPT_KAD_KSIAZKA_LOKALIZACJE pp_kl
                WHERE   pp_kl.KLOK_KOD = 'PIETRO'
                        AND pp_kl.KLOK_ID = KKON_PIETRO_ID)
                 KLOK_PIETRO,
              (SELECT   pp_kl.KLOK_WARTOSC
                 FROM   PPT_KAD_KSIAZKA_LOKALIZACJE pp_kl
                WHERE   pp_kl.KLOK_KOD = 'POKOJ'
                        AND pp_kl.KLOK_ID = KKON_POKOJ_ID)
                 KLOK_POKOJ
       FROM   ek_pracownicy,
              PPT_KAD_KSIAZKA_KONTAKTOWA,
              (
                    SELECT   Z.ZAT_PRC_ID 
                            --, wm_concat (DISTINCT Z.ZAT_OB_ID) DZLLIST
                          , replace(
                            trim(
                                extract(
                                XMLAGG(
                                         XMLELEMENT("txt",   '   ' || Z.ZAT_OB_ID || '   ' ) 
                                       ) 
                                , '/txt/text()').getStringVal()
                                ) 
                            , '      ' , '; ')
                            as DZLLIST
                       FROM   EGADM1.EK_ZATRUDNIENIE Z
                      WHERE       zat_typ_umowy = 0
                              AND zat_data_zmiany <= LAST_DAY (SYSDATE)
                              AND NVL (zat_data_do, SYSDATE) >= SYSDATE
                    GROUP BY   Z.ZAT_PRC_ID  
              ) DZL
      WHERE   EK_PRACOWNICY.PRC_ID = KKON_PRC_ID(+)
              AND EK_PRACOWNICY.PRC_ID = DZL.ZAT_PRC_ID(+)
   ORDER BY   INITCAP (prc_nazwisko),
              INITCAP (prc_imie),
              EK_PRACOWNICY.PRC_ID
/
