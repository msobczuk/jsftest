create or replace view PPADM.PPV_KONTA_LISTA as
select
-- $Revision::          $--
  pracownicy.PRC_ID pp_prc_id,
	knd_id pp_kb_id,
	case 
  when length(knd_numer)=26 then substr(knd_numer,1,2)||' '||substr(knd_numer,3,4)||' '||substr(knd_numer,7,4)||' '||substr(knd_numer,11,4)||' '||substr(knd_numer,15,4)||' '||substr(knd_numer,19,4)||' '||substr(knd_numer,23,4)
  when length(knd_numer)=28 then substr(knd_numer,1,4)||' '||substr(knd_numer,5,4)||' '||substr(knd_numer,9,4)||' '||substr(knd_numer,13,4)||' '||substr(knd_numer,17,4)||' '||substr(knd_numer,21,4)||' '||substr(knd_numer,25,4)
  else knd_numer 
  end pp_kb_numer,
	knd_f_iban pp_kb_fiban,
	bnk_nazwa pp_kb_nazban,
  knd_knt_id pp_kb_idkonta,
  knd_f_aktualne pp_kb_uzywane,
  count(knp_knt_id) pp_kb_liczlisty
from ekv_konta_pracownikow,ek_konta_plac, css_banki,EK_PRACOWNICY pracownicy
	where knt_prc_id = pracownicy.PRC_ID
  --and knd_f_aktualne = 'T'
  --and bnk_stan_definicji = 'A'
  and knp_knt_id(+) = knd_knt_id
	and bnk_numer(+) = SUBSTR(knd_numer_banku,1,3)
  group by knd_id, knd_numer, knd_f_iban, bnk_nazwa, knd_knt_id,pracownicy.PRC_ID,knd_f_aktualne
/
