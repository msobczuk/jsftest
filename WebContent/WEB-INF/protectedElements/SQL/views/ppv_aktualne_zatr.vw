create or replace view PPADM.PPV_AKTUALNE_ZATR as
select
-- $Revision::          $--
	-- ek_pracownicy
	prc.prc_numer, 
	initcap (prc.prc_nazwisko) || ' ' || initcap(prc.prc_imie) pracownik, 
	initcap (prc.prc_nazwisko) prc_nazwisko, 
	initcap (prc.prc_imie) prc_imie, 
	prc.prc_data_ur, 
	prc.prc_miejsce_ur, 
	prc.prc_plec,
	prc.prc_pesel, 
	prc.prc_nip, 
	prc.prc_nazwisko_rod,
	prc.prc_dowod_osob,
	prc.prc_organ_wydajacy, 
	prc.prc_data_wydania_dowodu, 
	prc.prc_data_waznosci_dowodu, 
	prc.prc_usk_id, -- urzad skarbowy - CKK klienci 
	--zpt_stanowiska
	stn.STN_NAZWA,
	-- ek_zatrudnienie 
	z.zat_data_przyj, 
	z.zat_sposob_przyjecia,
	(SELECT   WSL_ALIAS2
		 FROM   EGADM1.CSS_WARTOSCI_SLOWNIKOW
		WHERE   wsl_sl_nazwa = 'SPOSOB_PRZYJECIA'
				AND wsl_wartosc = z.zat_sposob_przyjecia)
		 zat_sposob_przyjecia_opis, -- brane ze slownika 'SPOSOB_PRZYJECIA'
	z.zat_typ_umowy, 
	  (SELECT   rv_abbreviation
		 FROM   PPV_REF_CODES
		WHERE   rv_domain = 'TYP_UMOWY'
				AND rv_low_value = z.zat_typ_umowy)
		 pp_z_umowa, -- brane ze slownika 'TYP_UMOWY'
	z.zat_okres_do, 
	z.zat_data_zwolnienia,
	z.zat_wymiar, 
		(SELECT   WSL_ALIAS2
			 FROM   PPT_ADM_WARTOSCI_SLOWNIKOW
			WHERE   wsl_sl_nazwa = 'TYP ETATU'
					AND wsl_wartosc = z.zat_typ_umowy)
			 pp_z_wymiar, --brane ze slownika 'TYP ETATU'
	-- ek_zatrudnienie - szef
	z.zat_prc_id_szef, 
	szef.prc_numer szef_numer,
	initcap (szef.prc_nazwisko) || ' ' || initcap(szef.prc_imie) szef, 
	initcap (szef.prc_nazwisko) szef_nazwisko,
	initcap (szef.prc_imie) szef_imie,
	-- ek_funcje ? do weryfikacji pozniej 
	funk.fnk_nazwa,
	-- css_stanowiska_kosztow
	sk.sk_kod, 
	sk.sk_opis, 
	-- css_obiekty_w_przedsieb
	ob.ob_pelny_kod, 
	ob.ob_kod, 
	ob.ob_nazwa, 
	ob.ob_skrot
from 
	ek_pracownicy prc
	join ek_zatrudnienie z on (PRC.PRC_ID = Z.ZAT_PRC_ID
	 AND (z.zat_data_do is null or z.zat_data_do > SYSDATE ) 
	 AND (z.zat_data_zwolnienia is null OR z.zat_data_zwolnienia > SYSDATE)--czy umowa aktualna
	AND(z.zat_data_zmiany is null or  z.zat_data_zmiany = ( select MAX(z1.zat_data_zmiany)  
								  FROM ek_zatrudnienie z1 
						 WHERE z1.zat_data_zmiany <= TRUNC(sysdate) 
						   AND z1.zat_prc_id = prc_id))
	 AND z.zat_typ_umowy = 0)
	left join ek_pracownicy szef on Z.ZAT_PRC_ID_SZEF = SZEF.PRC_ID
	left join ek_funkcje funk on Z.ZAT_FNK_ID = funk.FNK_ID 
	left join css_stanowiska_kosztow sk on Z.ZAT_SK_ID = sk.SK_ID 
	left join css_obiekty_w_przedsieb ob on z.ZAT_OB_ID = ob.OB_ID
	left join zpt_stanowiska stn on z.ZAT_STN_ID = stn.STN_ID
/
