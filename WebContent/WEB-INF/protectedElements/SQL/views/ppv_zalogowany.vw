create or replace view PPADM.PPV_ZALOGOWANY as 
select 
-- $Revision::          $-- --ascii/cp1250:����󜟿--

    uctx.id_klienta_sesji 
    ,uzt.UZT_NAZWA
    ,uzt.UZT_NAZWA_ZEWN
    ,uzt.UZT_IMIE
    ,uzt.UZT_NAZWISKO1
    ,uzt.UZT_NAZWISKO2
    ,uzt.UZT_TELEFON
    ,uzt.UZT_BIURO
    ,uzt.UZT_TYTUL
    ,uzt.UZT_OPIS
    ,uzt.UZT_PRC_ID
    ,uzt.UZT_MAIL
    ,uzt.UZT_TELEFON_KOM

    ,prc.PRC_ID
    ,prc.PRC_NUMER
    ,prc.PRC_NAZWISKO
    ,prc.PRC_IMIE
    ,prc.PRC_NIP
    ,prc.PRC_PESEL
    ,prc.PRC_NR_KARTY
    ,prc.PRC_AKRONIM
    --,prc.PRC_PRC_ID -- stosujemy opisy w ZP

from (
        select 
            SYS_CONTEXT('userenv','client_identifier') id_klienta_sesji, --id sesji zewn.
            eaadm.eap_globals.odczytaj_uzytkownika uzt_nazwa, --uzytkownik
            eaadm.eap_globals.odczytaj_firme frm_id, --firma
            eaadm.eap_globals.odczytaj_oddzial ob_id --oddzial
        from dual
    ) uctx 
    left join EAADM.eat_dostepy_do_firm ddf on ddf_uzt_nazwa = uzt_nazwa and ddf_frm_id = frm_id
    left join eat_uzytkownicy uzt on uzt.uzt_nazwa = uctx.uzt_nazwa
    left join ek_pracownicy prc on COALESCE( ppp_global.odczytaj_pracownika, ddf_prc_id) = prc_id
/
