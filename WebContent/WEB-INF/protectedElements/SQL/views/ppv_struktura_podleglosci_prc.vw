create or replace view PPADM.PPV_STRUKTURA_PODLEGLOSCI_PRC as
SELECT
-- $Revision:: 1       $--
-- struktura podleglosci (wszystkie poziomy) w ukladzie
-- stjo+przelozony, stjo+pracownik, poziom +PRC, +STN, +OB
 CONNECT_BY_ROOT spod_stjo_id_nad stjo_id_prz,
 CONNECT_BY_ROOT stjoprz.stjo_symbol stjo_symbol_prz,
 CONNECT_BY_ROOT stjoprz.stjo_nazwa stjo_nazwa_prz,
 
 CONNECT_BY_ROOT stnprz.stn_id stn_id_prz,
 CONNECT_BY_ROOT stnprz.stn_kod stn_kod_prz,
 CONNECT_BY_ROOT stnprz.stn_nazwa stn_nazwa_prz,
 
 CONNECT_BY_ROOT obprz.ob_id ob_id_prz,
 CONNECT_BY_ROOT obprz.ob_pelny_kod ob_pelny_kod_prz,
 
 CONNECT_BY_ROOT SPOD_PRC_ID_PRZ prc_id_prz, 
 CONNECT_BY_ROOT prz.prc_numer prc_numer_prz,
 --CONNECT_BY_ROOT (prz.prc_imie || ' ' || prz.prc_nazwisko) prc_prz,
 CONNECT_BY_ROOT prz.prc_imie prc_imie_prz,
 CONNECT_BY_ROOT prz.prc_nazwisko prc_nazwisko_prz,
 
 pod.spod_stjo_id stjo_id_prc,
 stjoprc.stjo_symbol stjo_symbol_prc,
 stjoprc.stjo_nazwa stjo_nazwa_prc,
 
 stnprc.stn_id stn_id_prc,
 stnprc.stn_kod stn_kod_prc,
 stnprc.stn_nazwa stn_nazwa_prc,

 obprc.ob_id ob_id_prc,
 obprc.ob_pelny_kod ob_pelny_kod_prc, 
 
 prc.prc_id,
 prc.prc_numer,
 --prc.prc_imie || ' '||prc.prc_nazwisko prc,
 prc.prc_imie, 
 prc.prc_nazwisko,
 
 LEVEL LVL,
 SYS_CONNECT_BY_PATH( prc.prc_imie||' '||prc.prc_nazwisko || ' (nr:'|| prc.prc_numer||')', ' / ') prc_pth, 
 SYS_CONNECT_BY_PATH( stjoprc.stjo_symbol || ' (stjoid:'||stjoprc.stjo_id||')', ' / ') stjo_pth

FROM PPADM.PPV_STRUKTURA_PODLEGLOSCI pod
    left join ek_pracownicy prc on prc.prc_id = pod.spod_prc_id
    left join ek_pracownicy prz on prz.prc_id = pod.spod_prc_id_prz

    left join egadm1.zpt_st_w_jo stjoprc on pod.spod_stjo_id     = stjoprc.stjo_id
    left join egadm1.zpt_stanowiska stnprc on stjoprc.stjo_stn_id = stnprc.stn_id
    left join egadm1.css_obiekty_w_przedsieb obprc on stjoprc.stjo_ob_id = obprc.ob_id
    
    left join egadm1.zpt_st_w_jo stjoprz on pod.spod_stjo_id_nad = stjoprz.stjo_id
    left join egadm1.zpt_stanowiska stnprz on stjoprz.stjo_stn_id = stnprz.stn_id
    left join egadm1.css_obiekty_w_przedsieb obprz on stjoprz.stjo_ob_id = obprz.ob_id
    
--where pod.spod_prc_id = 106044 --podwladny
CONNECT BY NOCYCLE PRIOR SPOD_PRC_ID = SPOD_PRC_ID_PRZ
/
