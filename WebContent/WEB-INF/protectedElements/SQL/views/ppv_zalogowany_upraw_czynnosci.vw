create or replace view PPADM.PPV_ZALOGOWANY_UPRAW_CZYNNOSCI as
select
-- $Revision::          $--
  DSTN_KOB_KOD,
  dstn_nazwa,
  COB_ID, COB_NAZWA,
  nvl(cobswo.SWO_OBG_ID, uwoswo.SWO_OBG_ID) SWO_OBG_ID
from EGADM1.CSST_UPRAWNIENIA_OBIEGOW
join PPADM.PPV_ZALOGOWANY_PROFILE uprf on UWO_PRF_ID = uprf.prf_id or uwo_uzt_nazwa = (select uzt_nazwa from ppv_zalogowany)
left join EGADM1.CSST_CZYNNOSCI_OBIEGOW on uwo_COB_ID = COB_ID
left join EGADM1.CSST_STANY_W_OBIEGACH cobswo on COB_SWO_OBG_ID_Z = cobswo.SWO_OBG_ID and COB_SWO_DSTN_ID_Z = cobswo.SWO_DSTN_ID
left join EGADM1.CSST_STANY_W_OBIEGACH uwoswo on UWO_SWO_OBG_ID = uwoswo.SWO_OBG_ID and UWO_SWO_DSTN_ID = uwoswo.SWO_DSTN_ID
join EGADM1.CSST_DEF_STANOW on nvl(cobswo.SWO_DSTN_ID, uwoswo.SWO_DSTN_ID) = DSTN_ID
/
