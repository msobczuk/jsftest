create or replace package body PPADM.PPP_DEL_ZALICZKI
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/#dev/install/obj_pp/packages/ppp_del_zaliczki.pcb 1 1.0.1 10.07.20 13:26 DZIOBEK                                                       $
-- $Revision:: 1                                                                                                                                              $
-- $Workfile:: ppp_del_zaliczki.pcb                                                                                                                           $
-- $Modtime:: 10.07.20 13:26                                                                                                                                  $
-- $Author:: DZIOBEK                                                                                                                                          $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS
   FUNCTION daj_paczke_dla_dok (p_dok_id NUMBER, p_data_wyplaty DATE)
      RETURN NUMBER
   IS
      v_rdok_kdok_kod   css_rodzaje_dokumentow.rdok_kdok_kod%TYPE;
      v_rdok_kod        css_rodzaje_dokumentow.rdok_kod%TYPE;
      v_pa_numer        kg_paczki.pa_numer%TYPE;
      v_nr_kol_paczki   NUMBER;
      v_rec_wznu        cssp_frm_wz.t_wznu;
      v_numer_sort      VARCHAR2 (100);
      v_tabela_paczek   VARCHAR2 (100) := 'KG_PACZKI';
      v_pa_id           NUMBER;
      v_rdz             VARCHAR2 (10);
      v_prj             VARCHAR2 (10);
      v_ile_paczek      NUMBER;
   BEGIN
      SELECT dok_rdok_kod,
             dok_rdok_kdok_kod,
             (SELECT DISTINCT sk_wymiar03
                FROM kgt_podzial_sk, css_stanowiska_kosztow
               WHERE psk_sk_id = sk_id AND psk_dok_id = dok_id),
             (SELECT DISTINCT sk_wymiar11
                FROM kgt_podzial_sk, css_stanowiska_kosztow
               WHERE psk_sk_id = sk_id AND psk_dok_id = dok_id)
        INTO v_rdok_kod,
             v_rdok_kdok_kod,
             v_rdz,
             v_prj
        FROM egadm1.kgt_dokumenty
       WHERE dok_id = p_dok_id;

      -- Wyliczymy podpowied� numeru kolejnej paczki
      cssp_numer.podpowiedz (
         p_tablica   => v_tabela_paczek,
         p_id        => NULL,
         p_wartosc   => v_pa_numer,
         p_p1        => v_rdok_kod,
         p_p2        => v_prj,
         p_p3        => v_rdz,
         p_p4        => TO_CHAR (p_data_wyplaty, 'YYMMDD'),
         p_p5        => p_dok_id);

      -- Numer kolejny jest na ostatnich dw�ch znakach
      v_nr_kol_paczki := TO_NUMBER (SUBSTR (NVL (v_pa_numer, '01'), -2));

      -- Je�eli to jest 1 to zakladamy nowa paczke
      IF v_nr_kol_paczki = 1
      THEN
         v_pa_id := NULL;
      ELSE
         -- Teraz trzeba sprawdzic, ile jest wszystkich paczek spelniajacych wzorzec na stausie START

         SELECT COUNT (*), MIN (pa_id)
           INTO v_ile_paczek, v_pa_id
           FROM kg_paczki, csst_encje_w_stanach, csst_def_stanow
          WHERE     pa_numer LIKE
                       SUBSTR (v_pa_numer, 1, LENGTH (v_pa_numer) - 2) || '%'
                AND ews_dstn_id = dstn_id
                AND ews_kob_kod = 'PCZ'
                AND ews_klucz_obcy_id = pa_id
                AND dstn_nazwa = 'START';

         -- Jezeli jest zero takich paczek, to zakladamy nowa. Jak wiele to do najstarszej pojdzie dokument.
         IF v_ile_paczek = 0
         THEN
            v_pa_id := NULL;
         END IF;
      END IF;

      IF v_pa_id IS NULL
      THEN
         v_pa_id :=
            nzp_generacje_przelewow.wygeneruj_paczke (v_pa_id,
                                                      v_rdok_kod,
                                                      v_rdok_kdok_kod,
                                                      p_data_wyplaty,
                                                      v_rdz,
                                                      v_prj,
                                                      p_dok_id);

         RETURN v_pa_id;
      END IF;

      RETURN v_pa_id;
   END daj_paczke_dla_dok;

   PROCEDURE usun_zal (p_dok_id NUMBER)
   AS
      err_code              VARCHAR2 (50);
      err_msg               VARCHAR2 (300);
      v_dok_rdok_kdok_kod   VARCHAR2 (10);
   BEGIN
      SELECT dok.dok_rdok_kdok_kod
        INTO v_dok_rdok_kdok_kod
        FROM egadm1.kgt_dokumenty dok
       WHERE dok.dok_id = p_dok_id;

      UPDATE PPT_DEL_ZALICZKI SET dkz_dok_id = NULL WHERE dkz_dok_id =  p_dok_id;

      delete from egadm1.KGT_PODZIAL_SK
            where  psk_dok_id = p_dok_id;
      
      DELETE FROM egadm1.zkt_zalaczniki
            WHERE zal_dok_id = p_dok_id;

      nzp_dki.usun (p_dok_id, 10009);

      IF v_dok_rdok_kdok_kod = 'KW'
      THEN
         egadm1.nzp_obj_kw.usun (p_dok_id);
      ELSIF v_dok_rdok_kdok_kod = 'KP'
      THEN
         egadm1.nzp_obj_kp.usun (p_dok_id);
      ELSIF v_dok_rdok_kdok_kod = 'PP'
      THEN
         egadm1.nzp_obj_pp.usun (p_dok_id);
      ELSIF v_dok_rdok_kdok_kod = 'PO'
      THEN
         egadm1.nzp_obj_po.usun (p_dok_id);
      ELSE
         raise_application_error (
            -20001,
            ' warto�� DOK_RDOK_KOD: ' || v_dok_rdok_kdok_kod);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 200);

         ppadm.plog ('usun_zal: ' || err_code || ' - ' || err_msg);
         raise_application_error (
            -20001,
               'Wyst�pi� nieobs�u�ony b��d przy usuwaniu zaliczki: '
            || err_msg);
   END;

   PROCEDURE modyfikuj_dok_zal (p_dok_id NUMBER, p_nowa_data_wyplaty DATE)
   AS
      dokument_kw           egadm1.nzp_obj_kw.t_rec_kw;
      dokument_kp           egadm1.nzp_obj_kp.t_rec_kp;
      dokument_pp           egadm1.nzp_obj_pp.t_rec_przelewyw;
      dokument_po           egadm1.nzp_obj_po.t_rec_przelewyo;
      err_code              VARCHAR2 (50);
      err_msg               VARCHAR2 (300);
      v_dok_rdok_kdok_kod   VARCHAR2 (10);
   BEGIN
      --warunki brz4egowe metody
      IF    p_nowa_data_wyplaty IS NULL
         OR TRUNC (p_nowa_data_wyplaty) < TRUNC (SYSDATE)
      THEN
         raise_application_error (
            -20001,
            'Nieprawid�owa data wyp�aty w przesz�o�ci.');
      END IF;


      FOR r
         IN (SELECT *
               FROM kgt_dokumenty
                    JOIN ppadm.ppv_stany_obieg_graf ews
                       ON     dok_id = ews.so_dok_id
                          AND wg_nazwa NOT IN ('START')
              -- todo joiny do statusu dokumentu w NZ
              WHERE dok_id = p_dok_id)
      LOOP
         raise_application_error (
            -20001,
               'Dokument zaliczki nie mo�e by� zmieniony, poniewa� jego aktualny status to '
            || r.wg_nazwa);
      END LOOP;



      SELECT dok.dok_rdok_kdok_kod
        INTO v_dok_rdok_kdok_kod
        FROM egadm1.kgt_dokumenty dok
       WHERE dok.dok_id = p_dok_id;


      IF v_dok_rdok_kdok_kod = 'KW'
      THEN
         egadm1.nzp_obj_kw.odczytaj (p_dok_id, dokument_kw);
         dokument_kw.dok_def_2 :=
            TO_CHAR (p_nowa_data_wyplaty, 'YYYY/MM/DD HH24:Mi:ss');
         egadm1.nzp_obj_kw.modyfikuj (dokument_kw);
      ELSIF v_dok_rdok_kdok_kod = 'KP'
      THEN
         egadm1.nzp_obj_kp.odczytaj (p_dok_id, dokument_kp);
         dokument_kp.dok_data_operacji := p_nowa_data_wyplaty;
         egadm1.nzp_obj_kp.modyfikuj (dokument_kp);
      ELSIF v_dok_rdok_kdok_kod = 'PP'
      THEN
         egadm1.nzp_obj_pp.odczytaj (p_dok_id, dokument_pp);
         dokument_pp.dok_data_operacji := p_nowa_data_wyplaty;
         dokument_pp.dok_pa_id :=
            daj_paczke_dla_dok (dokument_pp.dok_id, p_nowa_data_wyplaty);
         egadm1.nzp_obj_pp.modyfikuj (dokument_pp);
      ELSIF v_dok_rdok_kdok_kod = 'PO'
      THEN
         egadm1.nzp_obj_po.odczytaj (p_dok_id, dokument_po);
         dokument_po.dok_data_operacji := p_nowa_data_wyplaty;
         dokument_po.dok_pa_id :=
            daj_paczke_dla_dok (dokument_po.dok_id, p_nowa_data_wyplaty);
         egadm1.nzp_obj_po.modyfikuj (dokument_po);
      ELSE
         raise_application_error (
            -20001,
               ' Nie obs�u�ony rodzaj dokumentu DOK_RDOK_KOD: '
            || v_dok_rdok_kdok_kod);
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         err_code := SQLCODE;
         err_msg := SUBSTR (SQLERRM, 1, 200);

         ppadm.plog ('modyfikuj_dok_zal: ' || err_code || ' - ' || err_msg);
         raise_application_error (
            -20001,
               'Wyst�pi� nieobs�u�ony b��d przy modyfikacji zaliczki: '
            || err_msg);
   END;
END ppp_del_zaliczki;
/
