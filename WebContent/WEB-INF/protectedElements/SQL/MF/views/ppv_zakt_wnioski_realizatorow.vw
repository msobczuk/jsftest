create or replace force view PPADM.PPV_ZAKT_WNIOSKI_REALIZATOROW as
select 
-- $Revision::          $--
  wnr.WNR_ID
    ,wnr.WNR_OB_ID_RE
    ,wnr.WNR_OB_ID_WN
    ,wnr.WNR_ST_ID
    ,wnr.WNR_PRC_ID_WN
    ,wnr.WNR_PRC_ID_RE
    ,wnr.WNR_RWR_ID
    ,wnr.WNR_AUDYT_DT
    ,wnr.WNR_AUDYT_UT
    ,wnr.WNR_AUDYT_KT
    ,wnr.WNR_AUDYT_LM
    ,wnr.WNR_F_DO_PLANU
    ,wnr.WNR_PRZYCZYNA
    ,wnr.WNR_PRZEDMIOT
    ,wnr.WNR_DATA_WYSTAWIENIA
    ,wnr.WNR_RODZAJ_ZAMOWIENIA
    ,wnr.WNR_WARTOSC_NETTO
    ,wnr.WNR_WARTOSC_NETTO_EUR
    ,wnr.WNR_UZASADNIENIE
    ,wnr.WNR_WARTOSC_BRUTTO
    ,wnr.WNR_PPL_ID
    ,wnr.WNR_WER_ID
    ,wnr.WNR_ZDB_ID
    ,wnr.WNR_CE_ID
    ,wnr.WNR_AUDYT_DM
    ,wnr.WNR_AUDYT_UM
    ,wnr.WNR_AUDYT_KM
    ,wnr.WNR_NUMER_REALIZATORA
    ,wnr.WNR_NUMER_WNIOSKODAWCY
    ,wnr.WNR_DATA_WPISU
    ,wnr.WNR_DATA_REALIZACJI
    ,wnr.WNR_DATA_ZATWIERDZENIA
    ,wnr.WNR_PODSTAWA_PRAWNA
    ,wnr.WNR_PRZYCZYNA_NIEUWZGLEDNIENIA
    ,wnr.WNR_ZRODLO_FINANSOWANIA
    ,wnr.WNR_POLE_DEF_01
    ,wnr.WNR_POLE_DEF_02
    ,wnr.WNR_POLE_DEF_03
    ,wnr.WNR_POLE_DEF_04
    ,wnr.WNR_POLE_DEF_05
    ,wnr.WNR_POLE_DEF_06
    ,wnr.WNR_POLE_DEF_07
    ,wnr.WNR_POLE_DEF_08
    ,wnr.WNR_POLE_DEF_09
    ,wnr.WNR_POLE_DEF_10
    ,wnr.WNR_POLE_DEF_11
    ,wnr.WNR_POLE_DEF_12
    ,wnr.WNR_POLE_DEF_13
    ,wnr.WNR_POLE_DEF_14
    ,wnr.WNR_POLE_DEF_15
    ,wnr.WNR_POLE_DEF_16
    ,wnr.WNR_POLE_DEF_17
    ,wnr.WNR_POLE_DEF_18
    ,wnr.WNR_POLE_DEF_19
    ,wnr.WNR_POLE_DEF_20
    ,wnr.WNR_POLE_DEF_21
    ,wnr.WNR_POLE_DEF_22
    ,wnr.WNR_POLE_DEF_23
    ,wnr.WNR_POLE_DEF_24
    ,wnr.WNR_POLE_DEF_25
    ,wnr.WNR_POLE_DEF_26
    ,wnr.WNR_POLE_DEF_27
    ,wnr.WNR_POLE_DEF_28
    ,wnr.WNR_POLE_DEF_29
    ,wnr.WNR_POLE_DEF_30
    ,wnr.WNR_OPIS
    ,wnr.WNR_KL_KOD
    ,wnr.WNR_UM_ID
    ,wnr.WNR_MIESIAC_BUDZETOWY
    ,wnr.WNR_ZAM_DOD_TYP
    ,wnr.WNR_ZAM_DOD_PROCENT
    ,wnr.WNR_ZAM_ROK
    ,wnr.WNR_ZAM_NETTO
    ,wnr.WNR_ZAM_BRUTTO
    ,wnr.WNR_ZAM_NETTO_EUR
    ,wnr.WNR_TENC_ID
    ,wnr.WNR_WNR_ID
    ,wnr.WNR_RR_PRC_ID
    ,wnr.WNR_RR_PODST_WYLICZ
    ,wnr.WNR_RR_DATA_WYLICZ
    ,wnr.WNR_RR_TERMIN_REAL
    ,wnr.WNR_RR_MIEJSCE_REAL
    ,wnr.WNR_RR_DATA_ZAPYTANIA
    ,wnr.WNR_RR_SPOSOB_ZAPYTANIA
    ,wnr.WNR_RR_INNE_ELEM_REAL
    ,wnr.WNR_WAP_ID
    ,wnr.wnr_numer_wnioskodawcy
     || DECODE (wnr.wnr_numer_realizatora,
                NULL, NULL,
                DECODE (wnr.wnr_numer_wnioskodawcy,
                        NULL, wnr.wnr_numer_realizatora,
                        ', ' || wnr.wnr_numer_realizatora
                       )
               ) numer_lacznie
from ZAKT_WNIOSKI_REALIZATOROW wnr
/

--ascii/cp1250:����󜟿--
