create or replace view PPADM.PPV_DEL_RAP_ROZ_KOSZTOW_TAB4 as  
select 
-- $Revision::                $--
  ang.wnd_id wnd_id,
  case ang.tpoz_kategoria
    when 'P' then 'diety pobytowe'
    when 'RH' then 'diety/ryczałt hotelowy'
    when 'H' then 'diety/ryczałt hotelowy'
    when 'RD' then 'ryczałt dojazdowy'
    when 'KM' then 'ryczałt komunikacyjny'
    when 'T' then 'koszty przejazdu/pozostałe'
    when 'I' then 'koszty przejazdu/pozostałe'
  end typ_kosztu,
  ang.wal_symbol,  
  sum(ang.ang_kwota) kwota_waluty,
  ang.ang_kurs kurs, 
  sum(ang.ang_kwota_pln) kwota_pln,
  case ang.tpoz_kategoria
    when 'P' then 1
    when 'RH' then 2
    when 'H' then 2
    when 'RD' then 3
    when 'KM' then 4
    when 'T' then 5
    when 'I' then 5  
  end do_sortowania,
  sum(ang.ang_kwota_pln) kwota_pln_do_sumy,
  max( decode(ANG_ROZN_ZAOKR_NZ_PLN, 0, 0, 1) ) czy_doliczono
from PPV_DEL_ANGAZE_PLN ang
join PPT_DEL_POZYCJE_KALKULACJI PKL on PKL.PKL_ID = ang.PKL_ID
where 1=1 --and ang.wnd_id = 464
and ang.kal_rodzaj = 'ROZ'
and (PKL.PKL_FORMA_PLATNOSCI = '01' or PKL.PKL_FORMA_PLATNOSCI = '04')
GROUP BY ang.wnd_id, ang.wal_symbol, ang.ang_kurs, ang.tpoz_kategoria

union all
select 

  kal_wnd_id,
  case 
    when dkz_kwota<0 and dok_f_zatwierdzony = 'T' and dok_dok_id_zap is not null then 'kwota zwrócona' 
    when dkz_kwota<0 and not(dok_f_zatwierdzony = 'T' and dok_dok_id_zap is not null) then 'kwota do zwrotu' 
    when dkz_kwota>0 and dok_f_zatwierdzony = 'T' and dok_dok_id_zap is not null then 'kwota wypłacona' 
    when dkz_kwota>0 and not(dok_f_zatwierdzony = 'T' and dok_dok_id_zap is not null) then 'kwota do wypłaty'  
  end TYP_KOSZTU,
  NVL(WAL_SYMBOL, 'PLN') WAL_SYMBOL, 
  DKZ_KWOTA KWOTA_WALUTA, 
  KURS, 
  KWOTA_PLN, 
  case when dkz_kwota<0 then 99 else 100 end DO_SORTOWANIA,
  0 as KWOTA_PLN_DO_SUMY,
  0
from PPV_DELZ_DKZ_WART_PLN
 left join css_waluty on DKZ_WAL_ID = wal_id 
Where 1=1
  and (kal_rodzaj = 'ROZ' or dkz_dok_id is not null) 
  and NVL(DOK_F_ANULOWANY,'N') = 'N' --nie anulowany w NZ 
ORDER BY do_sortowania    
/
