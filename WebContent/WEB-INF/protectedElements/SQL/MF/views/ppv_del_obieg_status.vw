create or replace view PPADM.PPV_DEL_OBIEG_STATUS as 
SELECT 
-- $Revision::          $-- --ascii/cp1250:����󜟿--
  WND_ID, 
  EWS."EWS_ID",EWS."EWS_KOB_KOD",EWS."EWS_KLUCZ_OBCY_ID",EWS."EWS_OBG_ID",EWS."EWS_DSTN_ID",EWS."EWS_AUDYT_UT",EWS."EWS_AUDYT_DT",EWS."EWS_AUDYT_KT",EWS."EWS_AUDYT_LM",EWS."EWS_AUDYT_UM",EWS."EWS_AUDYT_DM",EWS."EWS_AUDYT_KM",EWS."DSTN_ID",EWS."DSTN_NAZWA",EWS."DSTN_F_PREDEFINIOWANY",EWS."DSTN_OPIS",EWS."DSTN_KOB_KOD",EWS."DSTN_X",EWS."DSTN_Y",
  (CASE  
    WHEN dstn_nazwa = 'START' THEN 'Wprowadzona'
    WHEN (wnd_rodzaj = '1' and dstn_nazwa IN('POZIOM10','POZIOM20')) or (wnd_rodzaj = '2' and dstn_nazwa IN('POZIOM05','POZIOM10')) THEN 'Do akceptacji'
    WHEN (wnd_rodzaj = '1' AND dstn_nazwa IN('POZIOM30') ) OR (wnd_rodzaj='2' AND dstn_nazwa IN('POZIOM20','POZIOM23','POZIOM27','POZIOM30','POZIOM40','POZIOM45')) THEN 'Zaakceptowana'
    WHEN (wnd_rodzaj = '1' AND dstn_nazwa IN('POZIOM40') ) OR (wnd_rodzaj='2' AND dstn_nazwa IN('POZIOM50','POZIOM60')) THEN 'Zatwierdzona'
    WHEN dstn_nazwa = 'ANULOWANY' AND WND_F_BEZKOSZTOWA = 'T' THEN 'Anulowana bezkosztowa'
    WHEN dstn_nazwa = 'ANULOWANY' AND WND_F_BEZKOSZTOWA = 'N' THEN 'Anulowana z kosztami'
    WHEN (wnd_rodzaj = '1' AND dstn_nazwa IN('POZIOM50','POZIOM60') ) OR (wnd_rodzaj='2' AND dstn_nazwa IN('POZIOM70','POZIOM73','POZIOM77','POZIOM80','POZIOM90','POZIOM100')) THEN 'Przekazana do rozliczenia'
    WHEN dstn_nazwa = 'STOP' THEN 'Rozliczona'
    END ) AS Status1
FROM PPT_DEL_WNIOSKI_DELEGACJI wnd
JOIN PPV_ENCJE_W_STANACH EWS ON EWS_KLUCZ_OBCY_ID = WND_ID AND ( (WND_RODZAJ = 1 AND DSTN_KOB_KOD = 'WN_DELKv2') OR (WND_RODZAJ = 2 AND DSTN_KOB_KOD = 'WN_DELZv2') )
/
