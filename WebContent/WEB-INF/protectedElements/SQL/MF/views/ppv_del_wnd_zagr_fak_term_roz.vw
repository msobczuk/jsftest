create or replace view PPADM.PPV_DEL_WND_ZAGR_FAK_TERM_ROZ as
SELECT 
-- $Revision:: 1               $ ---
    wnd_id,
    case
      when max(faktyczna_data) = TO_DATE('2099-01-01', 'YYYY-MM-DD') then null
      else max(faktyczna_data)
    end faktyczna_data
FROM(

      --1) jeśli z rozliczenia wynika, że np. pracownik odebrał zaliczkę w kwocie 100 EUR i to 100 EUR wydatkował to w momencie kiedy dyrektor zaakceptuje 
      --mu rozliczenie i zostanie przeze mnie zweryfikowane i potwierdzę poprawność i kompletność tych dokumentów i rozliczenia 
      --to termin faktyczny rozliczenia będzie dzień kiedy dyrektor zaakceptował rozliczenie pracownikowi. 
      --jezeli data wynosi 2099/01/01 znaczy ze jeszcze nie rozliczono 
      select 
        wnd.wnd_id wnd_id,
        max(nvl(hio_data, TO_DATE('2099-01-01', 'YYYY-MM-DD'))) faktyczna_data 
      from PPT_DEL_WNIOSKI_DELEGACJI wnd 
      left join PPV_HISTORIA_OBIEGOW_LAST on wnd.wnd_id = HIO_KLUCZ_OBCY_ID 
                                              and hio_obg_id in (SELECT EWS_OBG_ID FROM csst_encje_w_stanach WHERE EWS_KLUCZ_OBCY_ID = HIO_KLUCZ_OBCY_ID and EWS_KOB_KOD = 'WN_DELZv2')
                                              and (cob_nazwa in( 'Przekazanie rozliczenia do zatwierdzenia przez delegującego','Zatwierdzenie środków wynikających z rozliczenia do wypłaty przez dyrekcje BDG') )                                        
      WHERE NOT EXISTS (select 1 from PPT_DEL_KALKULACJE kal 
                                 join PPT_DEL_ZALICZKI dkz on dkz.dkz_kal_id = kal.kal_id 
                                 where wnd.wnd_id = kal.kal_wnd_id and kal.KAL_RODZAJ = 'ROZ')                                         
      GROUP BY wnd.wnd_id    


  union all
      --3) Pracownik otrzymał zaliczkę 100 EUR ale wydatkował 150 EUR czyli 50 EUR musimy mu "dobrać" dodatkowo w takiej sytuacji 
      --faktyczny termin rozliczenia to jest tak jak w pkt.1 po weryfikacji rozliczenia faktycznym terminem będzie data kiedy dyrektor zatwierdził rozliczenie. 
      --jezeli data wynosi 2099/01/01 znaczy ze jeszcze nie rozliczono
      select 
        kal.kal_wnd_id wnd_id,
        max(
             case when dkz.dkz_dok_id is not null then nvl(hio_data, TO_DATE('2099-01-01', 'YYYY-MM-DD'))
             else TO_DATE('2099-01-01', 'YYYY-MM-DD') end
        ) faktyczna_data 
      from PPT_DEL_KALKULACJE kal 
      join PPT_DEL_ZALICZKI dkz on dkz.DKZ_KAL_ID = kal.kal_id and DKZ.DKZ_KWOTA > 0 
      left join PPV_HISTORIA_OBIEGOW_LAST on kal.kal_wnd_id = HIO_KLUCZ_OBCY_ID 
                                              and hio_obg_id in (SELECT EWS_OBG_ID FROM csst_encje_w_stanach WHERE EWS_KLUCZ_OBCY_ID = HIO_KLUCZ_OBCY_ID and EWS_KOB_KOD = 'WN_DELZv2')
                                              and (cob_nazwa in( 'Przekazanie rozliczenia do zatwierdzenia przez delegującego','Zatwierdzenie środków wynikających z rozliczenia do wypłaty przez dyrekcje BDG') )  
      Where kal.KAL_RODZAJ = 'ROZ'                                                                                 
      GROUP BY kal.kal_wnd_id
      
  union all

      --2) jeśli pracownik otrzymał 100 EUR zaliczki a wydatkował 80 EUR to różnica w wysokości 20 EUR pozostaje do zwrotu w kasie bądź na konto MF
      --w tym przypadku termin faktyczny rozliczenia będzie to dzień kiedy pracownik wpłaci w kasie to 20 EUR lub wpływ środków na konto w MF. 
      --jezeli data wynosi 2099/01/01 znaczy ze jeszcze nie rozliczono 
      SELECT 
          KAL.KAL_WND_ID wnd_id,
          max(
              case when DOK_DOK_ID_ZAP is not null then nvl(dok.DOK_DATA_OPERACJI, TO_DATE('2099-01-01', 'YYYY-MM-DD'))
              else TO_DATE('2099-01-01', 'YYYY-MM-DD') end
          ) faktyczna_data
      FROM PPT_DEL_KALKULACJE KAL 
      join PPT_DEL_ZALICZKI DKZ on DKZ.DKZ_KAL_ID = KAL.KAL_ID  and DKZ.DKZ_DOK_ID is not null
      left join EGADM1.KGT_DOKUMENTY DOK on DOK.DOK_ID = DKZ.DKZ_DOK_ID
      WHERE DKZ.DKZ_KWOTA < 0 and KAL.KAL_RODZAJ = 'ROZ'
      GROUP BY KAL_WND_ID     
      
) 
GROUP BY wnd_id
ORDER BY wnd_id desc
/
