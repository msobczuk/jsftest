create or replace force view PPADM.PPV_KG_DOK_HIST_OBIEGU_LAST as
SELECT 
-- $Revision::          $--
	KG_ID,
	KG_NAZWA,
	KG_NAZWA2,
	ET_ID,
	ET_DATA,
	ET_PIECZEC,
	DOK_ID,
	ET_TYP,
	ET_UWAGI,
	STAN_Z,
	STAN_DO,
	ET_ID_LAST 
	-- dok_id, kg_id, count(*) cnt 
from WDRV_KG_DOK_HISTORIA_OBIEGU hio
  join ( --filtr - ostatnie wiersze et dla gr. kg_id i dok_id
            select --et_dok_id, et_kg_id, 
                max(et_id) et_id_last
            from fa_etapy et
            group by et_dok_id, et_kg_id    
         ) etlast on hio.et_id = etlast.et_id_last
where 1=1
    and ET_TYP='Wykonanie'
    --and dok_id = 796357 --651358, 651123  --:P_DOK_ID --id z kgt_dokumenty
    --and kg_nazwa2 is not null
--group by dok_id, kg_id having count(*) >1
order by dok_id, et_data
/
