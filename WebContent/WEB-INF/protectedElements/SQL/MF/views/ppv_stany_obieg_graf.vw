create or replace force view PPADM.PPV_STANY_OBIEG_GRAF as
SELECT
-- $Revision::          $--
     WG_ID
    ,WG_NAZWA
    ,WG_F_ANULOWANIE
    ,WG_OPIS
    ,WG_GR_ID
    ,WG_KTO_UTWORZYL
    ,WG_KIEDY_UTWORZYL
    ,WG_KTO_MODYFIKOWAL
    ,WG_KIEDY_MODYFIKOWAL
    ,WG_AUDYT_KT
    ,WG_AUDYT_KM
    ,WG_AUDYT_LM
    --,WG_FRM_ID
    ,WG_F_MODYFIKACJA
    ,SO_DOK_ID
    ,SO_WG_ID
FROM fa_wierzcholki
  JOIN fat_stan_obiegu ON wg_id = so_wg_id

/
