create or replace view PPADM.PPV_DELZ_DKZ_WART_PLN as 
select 
-- $Revision:: 1         $--
 dkz."DKZ_ID",dkz."DKZ_KAL_ID",dkz."DKZ_WAL_ID",dkz."DKZ_KWOTA",dkz."DKZ_OPIS",dkz."DKZ_FORMA_WYPLATY",dkz."DKZ_AUDYT_UT",dkz."DKZ_AUDYT_DT",dkz."DKZ_AUDYT_UM",dkz."DKZ_AUDYT_DM",dkz."DKZ_AUDYT_LM",dkz."DKZ_AUDYT_KM",dkz."DKZ_DATA_WYP",dkz."DKZ_DOK_ID",dkz."DKZ_NR_KONTA",dkz."DKZ_KARTA_KWOTA",dkz."DKZ_SRODKI_WLASNE",dkz."DKZ_ZAL_WYDANO",dkz."DKZ_KURS",dkz."DKZ_CZY_ROWNOWARTOSC",dkz."DKZ_ZWR_DKZ_ID" , 
 kal."KAL_ID",kal."KAL_WND_ID",kal."KAL_AUDYT_DT",kal."KAL_AUDYT_UT",kal."KAL_AUDYT_LM",kal."KAL_DATA",kal."KAL_AUDYT_DM",kal."KAL_AUDYT_UM",kal."KAL_AUDYT_KM",kal."KAL_OPIS",kal."KAL_KW_ID",kal."KAL_DATA_KURSU",kal."KAL_KAL_ID",kal."KAL_RODZAJ",
 DOK.DOK_F_ANULOWANY,
 dok.dok_f_zatwierdzony,
 dok.dok_dok_id_zap,

 case 
 when dkz.dkz_wal_id = 1      then   nvl(dok.DOK_KWOTA * sign(dkz.dkz_kwota), dkz.dkz_kwota) --PLN
 when nvl(dkz.dkz_kwota, 0)<0 then -(nvl(dok.DOK_KWOTA, -round(dkz.dkz_kwota * (dkz.dkz_kurs/wal.wal_jednostka_kursu), 2))) 
 when nvl(dkz.dkz_kwota, 0)>0 then  (nvl(dok.DOK_KWOTA,  round(dkz.dkz_kwota * decode (dkz.dkz_czy_rownowartosc, 'T', nbp.KW_PRZELICZNIK_SREDNI, nbp.KW_PRZELICZNIK_SPRZEDAZY),2)))  
 else 0
 end as kwota_pln


 , nvl( dok.dok_kurs, 
    case 
    when dkz.dkz_wal_id = 1      then 1 --PLN
    when nvl(dkz.dkz_kwota, 0)<0 then dkz.dkz_kurs
    when nvl(dkz.dkz_kwota, 0)>0 then decode (dkz.dkz_czy_rownowartosc, 'T', nbp.KW_PRZELICZNIK_SREDNI_W1, nbp.KW_PRZELICZNIK_SPRZEDAZY_W1)
    else 0
    end
 ) kurs


from PPT_DEL_ZALICZKI dkz 
  join PPT_DEL_KALKULACJE kal on dkz.dkz_kal_id=kal.kal_id
  join css_waluty wal on dkz.dkz_wal_id=wal.wal_id
  left join EGADM1.KGT_DOKUMENTY dok on dok.dok_id = dkz.dkz_dok_id
  left join PPV_KURSY_NBP  nbp
      on dkz.dkz_wal_id = nbp.wal_id and (kal.kal_data_kursu >= kw_data and kal.kal_data_kursu < nvl(kw_data_do, kal.kal_data_kursu+1)) 
where kal_rodzaj <> 'WNK'
  and nvl(dok.dok_f_anulowany,'N') <> 'T'
  --where kal.kal_wnd_id = :P_WND_ID
/

--ascii/cp1250:ąęćłńóśźż
