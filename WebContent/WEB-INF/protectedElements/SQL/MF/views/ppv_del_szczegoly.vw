create or replace view PPADM.PPV_DEL_SZCZEGOLY as 
SELECT 
-- $Revision:: 1               $ ---
  wnd.WND_ID,
  ZF.ZRODLA_FINANSOWANIA,
  cele.CELE,
  zapr.ZAPRASZAJACY,
  ter_roz.TERMIN_DO_KIEDY_ROZLICZENIE,
  case when wnd.wnd_rodzaj = 2 then fak_ter_roz.faktyczna_data else null end faktyczny_termin_rozliczenia
from PPT_DEL_WNIOSKI_DELEGACJI wnd
left join PPV_DEL_WND_ZF ZF on wnd.wnd_id = ZF.WND_ID
left join PPV_DEL_WND_CELE cele on wnd.wnd_id = cele.CDEL_WND_ID
left join PPV_DEL_WND_ZAPRASZAJACY zapr on wnd.wnd_id = zapr.ZAPR_WND_ID
left join PPV_DEL_WND_TERMINY_ROZ ter_roz on wnd.wnd_id = ter_roz.WND_ID 
left join PPV_DEL_WND_ZAGR_FAK_TERM_ROZ fak_ter_roz on wnd.wnd_id = fak_ter_roz.WND_ID
/

--ascii/cp1250:ąęćłńóśźż
