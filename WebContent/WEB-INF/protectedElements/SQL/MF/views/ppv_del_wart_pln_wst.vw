create or replace view PPADM.PPV_DEL_WART_PLN_WST as
select
    -- $Revision::           $--
    wnd_id,
    sum(ang_kwota_pln_kalk_wst) kwota_pln_wst
from PPV_DEL_ANGAZE_PLN
where 1=1
    and kal_rodzaj='WST'
    --and (kal_rodzaj='ROZ' or dok_f_anulowany is not null)
group by wnd_id
/
