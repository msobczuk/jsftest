create or replace view PPADM.PPV_DEL_ANGAZE_PLN as
SELECT  -- select * from PPV_DEL_ANGAZE_PLN order by ang_kwota_pln desc -- 0.5s
     -- $Revision::        $ --
     a2."ANG_ID",a2."ANG_KWOTA",a2."ANG_ROZN_ZAOKR_DKZ_PLN",a2."ANG_ROZN_ZAOKR_NZ_PLN",a2."ANG_WNR_ID",a2."PKZF_ID",a2."PKZF_SK_ID",a2."PKL_ID",a2."PKL_WAL_ID",a2."PKL_FORMA_PLATNOSCI",a2."TPOZ_ID",a2."TPOZ_NAZWA",a2."TPOZ_KATEGORIA",a2."TPOZ_KOD",a2."KAL_ID",a2."KAL_RODZAJ",a2."WND_ID",a2."DKZ_ID",a2."DKZ_CZY_ROWNOWARTOSC",a2."DOK_ID",a2."DOK_KURS",a2."DOK_KURS_JW",a2."KURS_WST",a2."KURS_WST_JW",a2."KW_PRZELICZNIK_SPRZEDAZY",a2."KW_PRZELICZNIK_SREDNI",a2."WAL_JEDNOSTKA_KURSU",a2."WAL_SYMBOL",a2."ANG_KWOTA_PLN_DOK_NZ",a2."ANG_KWOTA_PLN_KALK_WST" 
   , nvl(ang_kwota_pln_dok_nz, ang_kwota_pln_kalk_wst) ang_kwota_pln --wartość rzeczywista w oparciu o kurs nz a jeżeli jej nie ma to w oparciu o kurs wstepny
   , nvl(dok_kurs,    kurs_wst   )  ang_kurs    --kurs do ang bez uwzgl. jednostki waluty
   , nvl(dok_kurs_jw, kurs_wst_jw)  ang_kurs_jw --kurs do ang uwzgl. jednostkę waluty
   
FROM (
    SELECT 
       a1.*
      , round(ang_kwota * dok_kurs_jw, 2) + ANG_ROZN_ZAOKR_NZ_PLN ang_kwota_pln_dok_nz
      , round(ang_kwota * kurs_wst_jw, 2) + ANG_ROZN_ZAOKR_DKZ_PLN ang_kwota_pln_kalk_wst
    FROM(
          select 
                ang_id
              , ang_kwota
              , ANG_ROZN_ZAOKR_DKZ_PLN
              , ANG_ROZN_ZAOKR_NZ_PLN
              , ang_wnr_id              
              
              , pkzf_id
              , pkzf_sk_id
              
              , pkl_id
              , pkl_wal_id              
              , pkl_forma_platnosci
              
              , tpoz_id
              , tpoz_nazwa
              , tpoz_kategoria
              , tpoz_kod
              
              , kal_id
              , kal_rodzaj              
              
              , wnd_id
              
              , dkz_id
              , dkz_czy_rownowartosc
              
              , dok_id
              
              , dok_kurs --kurs z dokumentu w nz(nie uwzglednia jednostki waluty) 
              , round( dok_kurs / nvl(WAL_JEDNOSTKA_KURSU,1),12) dok_kurs_jw --ang_kurs_nz
              
              , decode ( pkl_wal_id, 1,1,         nvl(dkz_kurs,                     KW_PRZELICZNIK_SPRZEDAZY_W1)       ) kurs_wst --kurs wstepny (nie uwzglednia jednostki waluty; dla PLN zawsze 1) 
              , decode ( pkl_wal_id, 1,1, round(  nvl(dkz_kurs/WAL_JEDNOSTKA_KURSU, KW_PRZELICZNIK_SPRZEDAZY   )  ,12 ) ) kurs_wst_jw --kurs wstepny (uwzglednia jednostke waluty; dla PLN zawsze 1) 

              , KW_PRZELICZNIK_SPRZEDAZY
              , KW_PRZELICZNIK_SREDNI
              , WAL_JEDNOSTKA_KURSU
              , wal_symbol

              --, nvl(dok_kurs, decode (dkz_czy_rownowartosc, 'T', KW_PRZELICZNIK_SREDNI_W1, KW_PRZELICZNIK_SPRZEDAZY_W1)) kurs --TODO upewnic sie czy jako kurs wstępny może być stosowany kurs sprzedaży
              --, round(nvl(dok_kurs, decode (dkz_czy_rownowartosc, 'T', KW_PRZELICZNIK_SREDNI, KW_PRZELICZNIK_SPRZEDAZY)) * ang_kwota / nvl(WAL_JEDNOSTKA_KURSU,1), 2) ang_kwota_pln
              --, round(nvl(round(dok_kurs / nvl(WAL_JEDNOSTKA_KURSU,1),6), decode (dkz_czy_rownowartosc, 'T', KW_PRZELICZNIK_SREDNI, KW_PRZELICZNIK_SPRZEDAZY)) * ang_kwota, 2) + ANG_ROZN_ZAOKR_NZ_PLN  ang_kwota_pln
              --, round(round(dok_kurs / nvl(WAL_JEDNOSTKA_KURSU,1),6) * ang_kwota, 2) + ANG_ROZN_ZAOKR_NZ_PLN ang_kwota_pln_dok_nzzzz--kwota wyliczona kursem z dokumentu nz           
              --, round( nvl(dkz_kurs/nvl(WAL_JEDNOSTKA_KURSU,1), KW_PRZELICZNIK_SPRZEDAZY) * ang_kwota, 2) + ANG_ROZN_ZAOKR_DKZ_PLN ang_kwota_pln_kalk_wstzzz--kwota wyliczona szacowanym kursem

              
          from ppt_del_angaze ang
              join ppt_del_zrodla_finansowania on ang_pkzf_id = pkzf_id
              left join ppt_del_zaliczki on pkzf_dkz_id = dkz_id
              left join kgt_dokumenty on dkz_dok_id = dok_id
              join ppt_del_pozycje_kalkulacji on pkzf_pkl_id = pkl_id
              join ppt_del_kalkulacje on pkl_kal_id = kal_id
              join ppt_del_wnioski_delegacji on kal_wnd_id = wnd_id
              join PPT_DEL_TYPY_POZYCJI on pkl_tpoz_id = tpoz_id
              left join PPV_KURSY_NBP 
                on pkl_wal_id = wal_id and (kal_data_kursu >= kw_data and kal_data_kursu < nvl(kw_data_do, kal_data_kursu+1))  
                -- pkl_wal_id = 1  !!!tak nie, bo to KARTEZJAN (prawie 6M wierszy)!!!! --krajowe  
                --or (pkl_wal_id = wal_id and (kal_data_kursu >= kw_data and kal_data_kursu < nvl(kw_data_do, kal_data_kursu+1))) --zagraniczne 
    ) a1
)a2
/

--ascii/cp1250:ąęćłńóśźż
