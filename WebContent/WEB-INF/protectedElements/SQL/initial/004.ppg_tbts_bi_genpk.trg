create or replace TRIGGER PPADM.PPG_TBTS_BI_GENPK
BEFORE INSERT OR DELETE ON PPADM.PPT_TAB_TIMESTAMPS  
FOR EACH ROW
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
declare 
 PRAGMA AUTONOMOUS_TRANSACTION;--granty
begin
    IF INSERTING THEN
        IF :NEW.TBTS_TABLE is null THEN 
            :NEW.TBTS_TABLE := UPPER(:NEW.TBTS_TABLE_OWNER||'.'||:NEW.TBTS_TABLE_NAME);
        END IF;
        if USER <> :NEW.TBTS_TABLE_OWNER then 
            EXECUTE IMMEDIATE 'GRANT UPDATE on PPADM.PPT_TAB_TIMESTAMPS to '||:NEW.TBTS_TABLE_OWNER; 
        end if;
    END IF;
end;
/
