create or replace FUNCTION diff_all_sources(p_objp_id IN NUMBER) RETURN NUMBER 
--------------------------------------------------------------------------------
-- $Header::                                                                   $
-- $Revision::                                                                 $
-- $Workfile::                                                                 $
-- $Modtime::                                                                  $
-- $Author::                                                                   $
--------------------------------------------------------------------------------
IS
  v_diff_num         NUMBER(5);
  v_objp_id          NUMBER(10);
  v_objp_owner       VARCHAR2(30);
  v_objp_name        VARCHAR2(30);
  v_objp_object_type VARCHAR2(30);
BEGIN
  v_objp_id := p_objp_id;
  --
  SELECT objp_owner, objp_name, objp_object_type
    INTO v_objp_owner, v_objp_name, v_objp_object_type
    FROM ppt_objects
   WHERE objp_id = v_objp_id;
  --
  SELECT count(*)
    INTO v_diff_num
    FROM 
      (
        SELECT sou_line, sou_text 
          FROM ppt_all_source 
         WHERE sou_objp_id = v_objp_id
      ) 
    FULL OUTER JOIN 
      (
        SELECT line sys_line, replace(replace(text, CHR(10),''), CHR(13), '') sys_text 
          FROM sys.all_source
         WHERE upper(name) = upper(v_objp_name) 
           AND upper(type) = upper(v_objp_object_type) 
           AND upper(owner) = upper(v_objp_owner)
      )
      ON sou_line = sys_line
   WHERE utl_match.edit_distance(sou_text, sys_text) <> 0 
     AND (sou_text IS NOT NULL OR sys_text IS NOT NULL);
  --
  RETURN v_diff_num;
END;
/
