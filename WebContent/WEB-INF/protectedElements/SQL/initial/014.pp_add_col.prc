create or replace PROCEDURE pp_add_col (
  p_table          IN VARCHAR2,
  p_property_name  IN VARCHAR2,
  p_data_type      IN VARCHAR2,
  p_column_name    IN VARCHAR2 DEFAULT NULL,
  p_property_type  IN VARCHAR2 DEFAULT NULL,
  p_nullable       IN VARCHAR2 DEFAULT 'N',
  p_default        IN VARCHAR2 DEFAULT NULL,
  p_ix_name        IN VARCHAR2 DEFAULT NULL,
  p_uk_name        IN VARCHAR2 DEFAULT NULL,
  p_fk_column_name IN VARCHAR2 DEFAULT NULL,
  p_fk_name        IN VARCHAR2 DEFAULT NULL,
  p_status         IN VARCHAR2 DEFAULT 'Z',
  p_desc           IN VARCHAR2
)
--------------------------------------------------------------------------------
-- $Header::                                                                   $
-- $Revision::                                                                 $
-- $Workfile:: 							                                       $
-- $Modtime:: 				                                                   $
-- $Author::			                                                       $
--------------------------------------------------------------------------------
IS
  v_tb_id           NUMBER(10);
  v_property_name   VARCHAR2(30);
  v_data_type       VARCHAR2(30);
  v_column_name     VARCHAR2(30);
  v_property_type   VARCHAR2(30);
  v_nullable        VARCHAR2(1);
  v_default         VARCHAR2(300);
  v_ix_name         VARCHAR2(30);
  v_uk_name         VARCHAR2(30);
  v_fk_column_name  VARCHAR2(90);
  v_fk_name         VARCHAR2(30);
  v_status          VARCHAR2(10);
  v_desc            VARCHAR2(4000);
  --
  v_owner           VARCHAR2(30);
  v_table_name      VARCHAR2(30);
BEGIN
  v_owner      := substr(p_table, 1, instr(p_table, '.', 1, 1) - 1);
  v_table_name := substr(p_table, instr(p_table, '.', 1, 1) + 1);
  --
  SELECT tb_id
    INTO v_tb_id
    FROM ppt_tables
   WHERE tb_table_name = v_table_name
     AND tb_owner = v_owner;
  --
  v_property_name  := p_property_name;
  v_data_type      := p_data_type;
  v_column_name    := p_column_name;
  v_property_type  := p_property_type;
  v_nullable       := p_nullable;
  v_default        := p_default;
  v_ix_name        := p_ix_name;
  v_uk_name        := p_uk_name;
  v_fk_column_name := p_fk_column_name;
  v_fk_name        := p_fk_name;
  v_status         := p_status;
  v_desc           := p_desc;
  --
  IF v_column_name IS NULL THEN
    SELECT tb_column_prefix || '_' || upper(regexp_replace(v_property_name, '([A-Z])', '_\1', 2, 1, 'c'))
      INTO v_column_name 
      FROM ppt_tables
     WHERE tb_id = v_tb_id;
  END IF;
  --
  IF v_property_type IS NULL THEN
    v_property_type := 
      CASE 
        WHEN v_data_type LIKE 'VARCHAR2%' THEN 'String'
        WHEN REGEXP_LIKE (upper(v_data_type), '^NUMBER\(\d+(,0)?\)$') THEN 'Long'
        WHEN REGEXP_LIKE (upper(v_data_type), '^NUMBER\(\d+,[1-9]\d*\)$') THEN 'Double'
      END;
  END IF;
  --
  INSERT INTO ppt_tab_cols (tc_tb_id, 
                            tc_property_name, 
                            tc_column_name, 
                            tc_property_type, 
                            tc_data_type, 
                            tc_nullable, 
                            tc_ix_name, 
                            tc_uk_name, 
                            tc_fk_column_name, 
							tc_fk_name,
                            tc_desc,
                            tc_default,
                            tc_status) 
       VALUES (v_tb_id,
               v_property_name,
               v_column_name,
               v_property_type,
               v_data_type,
               v_nullable,
               v_ix_name,
               v_uk_name,
               v_fk_column_name,
			   v_fk_name,
               v_desc,
               v_default,
               v_status);
END;
/
