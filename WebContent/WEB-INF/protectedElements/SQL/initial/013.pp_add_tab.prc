create or replace PROCEDURE pp_add_tab (
  p_owner         IN VARCHAR2,
  p_table_name    IN VARCHAR2,
  p_entity_name   IN VARCHAR2,
  p_column_prefix IN VARCHAR2,
  p_status        IN VARCHAR2 DEFAULT 'Z',
  p_desc          IN VARCHAR2,
  p_generuj_pk    IN VARCHAR2 DEFAULT 'N'
)
--------------------------------------------------------------------------------
-- $Header::                                                                   $
-- $Revision::                                                                 $
-- $Workfile:: 							                                       $
-- $Modtime:: 				                                                   $
-- $Author::			                                                       $
--------------------------------------------------------------------------------
IS
  v_tb_id          NUMBER(10);
  v_owner          VARCHAR2(30);
  v_table_name     VARCHAR2(30);
  v_entity_name    VARCHAR2(30);
  v_column_prefix  VARCHAR2(10);
  v_status         VARCHAR2(10);
  v_desc           VARCHAR2(4000);
BEGIN
  v_owner         := p_owner;
  v_table_name    := p_table_name;
  v_entity_name   := p_entity_name;
  v_column_prefix := p_column_prefix;
  v_status        := p_status;
  v_desc          := p_desc;
  --
  INSERT INTO ppt_tables (tb_owner, 
                          tb_table_name, 
                          tb_entity_name, 
                          tb_column_prefix, 
                          tb_desc,
                          tb_status) 
       VALUES (v_owner,
               v_table_name,
               v_entity_name,
               v_column_prefix,
               v_desc,
               v_status)
     RETURNING tb_id
          INTO v_tb_id;
  --
  IF p_generuj_pk = 'T' THEN
    INSERT INTO ppt_tab_cols (tc_id, 
                              tc_tb_id, 
                              tc_property_name, 
                              tc_column_name, 
                              tc_property_type, 
                              tc_data_type, 
                              tc_nullable, 
                              tc_desc,
                              tc_status) 
         VALUES (NULL,
                 v_tb_id,
                 'id',
                 v_column_prefix || '_ID',
                 'Long',
                 'NUMBER(10)',
                 'N',
                 'Klucz g��wny tabeli ' || v_table_name,
                 'Z');
  END IF;
END;
/
