CREATE OR REPLACE TRIGGER PPADM.PPG_WNR_USUN_ZALACZNIKI
BEFORE DELETE ON PPADM.PPT_SZK_WN_REFUNDACJE
REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------    
  DECLARE
	v_wnr_id NUMBER(10,0);
BEGIN
	v_wnr_id := :old.wnr_id;
	DELETE FROM egadm1.zkt_zalaczniki
	WHERE ZAL_WNREF_ID = v_wnr_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_wnr_usun_zalaczniki');
END;
/
