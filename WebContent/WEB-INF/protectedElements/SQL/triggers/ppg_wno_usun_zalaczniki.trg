CREATE OR REPLACE TRIGGER PPADM.ppg_wno_usun_zalaczniki
BEFORE DELETE 
	ON PPADM.PPT_KAD_WN_OKULARY
	FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/ppg_wno_usun_zalaczniki.trg 3 1.0.3 09.03.18 09:07 MGOLDA                                   $
-- $Revision:: 3                                                                                                                                              $
-- $Workfile:: ppg_wno_usun_zalaczniki.trg                                                                                                                    $
-- $Modtime:: 09.03.18 09:07                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE
	v_wno_id NUMBER(10,0);
BEGIN
	v_wno_id := :old.wno_id;
	DELETE FROM zkt_zalaczniki
	WHERE ZAL_OKUL_ID = v_wno_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_wno_usun_zalaczniki');
END;
/
