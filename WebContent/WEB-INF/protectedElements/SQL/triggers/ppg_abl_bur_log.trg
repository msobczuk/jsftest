CREATE OR REPLACE TRIGGER PPADM.PPG_ABL_BUR_LOG
  BEFORE UPDATE ON PPADM.PPT_ABS_URLOPY_WNIOSKOWANE
  REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/PPG_ABL_BUR_LOG.trg 3 1.0.3 09.03.18 09:06 MGOLDA                                           $
-- $Revision:: 3                                                                                                                                              $
-- $Workfile:: PPG_ABL_BUR_LOG.trg                                                                                                                            $
-- $Modtime:: 09.03.18 09:06                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE
 powod varchar2(300);
BEGIN

	if :new.UPRW_OPIS is null then 
		powod := '';
	else 
		powod := ' Pow�d: '|| :new.UPRW_OPIS;
	end if;	
	
  insert into ppt_abs_urlopy_zatw_log (abl_id, abl_uprw_id, abl_uzytkownik, abl_stan_z, abl_stan_do) values (PPS_ABS_URL_ZATW.nextval, :old.uprw_id, EAADM.EAP_GLOBALS.odczytaj_uzytkownika(), :old.uprw_status, :new.uprw_status);

  IF (:new.uprw_status = -1) THEN

	WSTAW_WIADOMOSC(:new.uprw_prc_id, 'Wniosek o urlop w dniach ' || :new.UPRW_DATA_OD || ' - ' || :new.UPRW_DATA_DO || ' zostal odrzucony.' || powod , sysdate);
  
  END IF;
  
  IF (:new.uprw_status = 10) THEN
  
    WSTAW_WIADOMOSC(:new.uprw_prc_id, 'Wniosek o urlop w dniach ' || :new.UPRW_DATA_OD || ' - ' || :new.UPRW_DATA_DO || ' zostal zatwierdzony.', sysdate);
  
  END IF;

END;
/
