CREATE OR REPLACE TRIGGER PPADM.PPG_KA_BIUR_NUMER
  BEFORE INSERT ON PPADM.PPT_REK_KANDYDACI
  REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/PPG_KA_BIUR_NUMER.trg 3 1.0.3 09.03.18 09:06 MGOLDA                                         $
-- $Revision:: 3                                                                                                                                              $
-- $Workfile:: PPG_KA_BIUR_NUMER.trg                                                                                                                          $
-- $Modtime:: 09.03.18 09:06                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN
  :NEW.KA_NUMER := :NEW.KA_ID;
END;
/
