create or replace TRIGGER PPADM.PPG_WNO_NUMER
  before INSERT OR UPDATE ON PPADM.PPT_KAD_WN_OKULARY FOR EACH ROW
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                           $
-- $Revision::                                                                                                                                                         $
-- $Workfile::                                                                                                                                                         $
-- $Modtime::                                                                                                                                                          $
-- $Author::                                                                                                                                                           $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE
BEGIN
    IF (:NEW.WNO_NR_WNIOSKU IS NULL) THEN
     :new.WNO_NR_WNIOSKU := PPADM.NUMER_OBLICZ( p_tablica => 'PPT_KAD_WN_OKULARY'); 

    END IF;
END;
/
