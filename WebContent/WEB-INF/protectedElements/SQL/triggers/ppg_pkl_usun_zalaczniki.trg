CREATE OR REPLACE TRIGGER PPADM.PPG_PKL_USUN_ZALACZNIKI
BEFORE DELETE ON PPADM.PPT_DEL_POZYCJE_KALKULACJI
REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
--------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
  DECLARE
	v_pkl_id NUMBER(10,0);
BEGIN
	v_pkl_id := :old.PKL_ID;
	DELETE FROM egadm1.zkt_zalaczniki
	WHERE ZAL_PKL_ID = v_pkl_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_pkl_usun_zalaczniki');
END;
/
