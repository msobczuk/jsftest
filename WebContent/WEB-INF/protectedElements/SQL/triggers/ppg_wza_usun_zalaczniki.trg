CREATE OR REPLACE TRIGGER PPADM.PPG_WZA_USUN_ZALACZNIKI 
BEFORE DELETE ON PPADM.PPT_WNIOSKI_ZASWIADCZENIA 
REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------  
  DECLARE
	v_wza_id NUMBER(10,0);
BEGIN
	v_wza_id := :old.WZA_ID;
	DELETE FROM egadm1.zkt_zalaczniki
	WHERE ZAL_WZA_ID = v_wza_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_wza_usun_zalaczniki');
END;
/
