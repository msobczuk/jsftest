CREATE OR REPLACE TRIGGER PPADM.PPG_SZK_DELETE_ZF
  BEFORE DELETE ON PPADM.PPT_SZK_SZKOLENIA FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/PPG_SZK_DELETE_ZF.trg 3 1.0.3 09.03.18 09:07 MGOLDA                                         $
-- $Revision:: 3                                                                                                                                              $
-- $Workfile:: PPG_SZK_DELETE_ZF.trg                                                                                                                          $
-- $Modtime:: 09.03.18 09:07                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN
  DELETE FROM PPT_ZRODLA_FINANSOWANIA WHERE ZFI_ENCJA_ID=:OLD.SZK_ID AND ZFI_KATEGORIA='PPT_SZK_SZKOLENIA';
END;
/
