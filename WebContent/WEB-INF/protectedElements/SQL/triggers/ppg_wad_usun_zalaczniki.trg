CREATE OR REPLACE TRIGGER PPADM.PPG_WAD_USUN_ZALACZNIKI
BEFORE DELETE ON PPADM.PPT_WNIOSKI_AKTUALIZ_DANYCH
REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------  
  DECLARE
	v_wad_id NUMBER(10,0);
BEGIN
	v_wad_id := :old.WAD_ID;
	DELETE FROM egadm1.zkt_zalaczniki
	WHERE ZAL_WAD_ID = v_wad_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_wad_usun_zalaczniki');
END;
/
