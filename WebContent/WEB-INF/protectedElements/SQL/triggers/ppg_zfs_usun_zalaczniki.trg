CREATE OR REPLACE TRIGGER PPADM.PPG_ZFS_USUN_ZALACZNIKI
BEFORE DELETE ON PPADM.PPT_ZFSS_WNIOSEK
REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------   
  DECLARE
	v_zfs_id NUMBER(10,0);
BEGIN
	v_zfs_id := :old.ZFS_ID;
	DELETE FROM egadm1.zkt_zalaczniki
	WHERE ZAL_ZFS_ID = v_zfs_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_zfs_usun_zalaczniki');
END;
/
