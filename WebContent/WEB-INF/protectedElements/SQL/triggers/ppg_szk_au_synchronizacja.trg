CREATE OR REPLACE TRIGGER "PPADM"."PPG_SZK_AU_SYNCHRONIZACJA"
AFTER UPDATE ON "PPADM"."PPT_SZK_SZKOLENIA"
REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                         																										  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------  
DECLARE
BEGIN
  IF UPDATING THEN
	IF (:NEW.SZK_STATUS = 'ZREAL') THEN
    
      PPP_UTILITIES.przepisz_termin_szkolenia(:NEW.szk_id, :NEW.SZK_OF_ID, :NEW.SZK_DATA_OD, :NEW.SZK_DATA_DO, :NEW.SZK_DATA_WAZNOSCI);
	
    END IF;
  END IF;
END;
/
