CREATE OR REPLACE TRIGGER PPADM.PPG_LST_BIUR_AUDYT
  BEFORE INSERT or UPDATE ON PPADM.PPT_ADM_LISTY_WARTOSCI
  FOR EACH ROW 
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------  
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.LST_AUDYT_UT, :NEW.LST_AUDYT_DT, :NEW.LST_AUDYT_KT, :NEW.LST_AUDYT_LM, :OLD.LST_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.LST_AUDYT_UT, :NEW.LST_AUDYT_DT, :NEW.LST_AUDYT_KT, :NEW.LST_AUDYT_LM, :OLD.LST_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN

        ------------------------------------------------------------------------------------------------------------------------------------------------------
        -- UPDATE / wykluczamy z audytu zmiane na kolumnie LST_WCZYTWANO_KIEDY (to ze wczytano liste nie jest istotne w audycie - nie ma istotnej modyfikacji)
        -- w kodzie PP przy wczytywaniu LW co 15 min. jest zbierana statystyka wczytywania
        -- wykonywany jest przy tym taki UPDATE PPT_ADM_LISTY_WARTOSCI SET LST_WCZYTWANO_KIEDY = SYSDATE where LST_LP=1 and upper(LST_NAZWA) = ?
        IF :NEW.LST_LP=1 AND :OLD.LST_WCZYTWANO_KIEDY < :NEW.LST_WCZYTWANO_KIEDY AND :NEW.LST_WCZYTWANO_KIEDY > (SYSDATE - INTERVAL '1' SECOND) THEN
            RETURN;
        END IF;
        ------------------------------------------------------------------------------------------------------------------------------------------------------

      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.LST_AUDYT_UM, :NEW.LST_AUDYT_DM, :NEW.LST_AUDYT_KM, :NEW.LST_AUDYT_LM, :OLD.LST_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.LST_AUDYT_UM, :NEW.LST_AUDYT_DM, :NEW.LST_AUDYT_KM, :NEW.LST_AUDYT_LM, :OLD.LST_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;

  ------------------------------------------------------------------------------------------------------------------------------------------------------
  --aktualizacja/zmiana wersji w PPT_OBJECTS
  update ppadm.ppt_objects set objp_version = to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') where objp_object_type = 'LW' and objp_name = :NEW.LST_NAZWA;
  ------------------------------------------------------------------------------------------------------------------------------------------------------

END;
/
