CREATE OR REPLACE TRIGGER PPADM.PPG_WUPR_AIUDR_TS
  AFTER INSERT or UPDATE or DELETE ON PPADM.PPT_WEB_UPRAWNIENIA 
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/PPG_WUPR_AIUDR_TS.trg 3 1.0.3 09.03.18 09:08 MGOLDA                                         $
-- $Revision:: 3                                                                                                                                              $
-- $Workfile:: PPG_WUPR_AIUDR_TS.trg                                                                                                                          $
-- $Modtime:: 09.03.18 09:08                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN
    UPDATE PPT_JPA_GENKEYS SET PKEY_TS = SYSDATE WHERE PKEY_TABLE = 'PPT_WEB_UPRAWNIENIA';
END;
/
