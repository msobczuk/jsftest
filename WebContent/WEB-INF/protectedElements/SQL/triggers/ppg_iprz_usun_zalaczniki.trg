CREATE OR REPLACE TRIGGER PPADM.PPG_IPRZ_USUN_ZALACZNIKI
BEFORE DELETE ON PPADM.PPT_IPR_IPRZ
REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------   
  DECLARE
	v_iprz_id NUMBER(10,0);
BEGIN
	v_iprz_id := :old.IPRZ_ID;
	DELETE FROM egadm1.zkt_zalaczniki
	WHERE ZAL_IPRZ_ID = v_iprz_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_iprz_usun_zalaczniki');
END;
/
