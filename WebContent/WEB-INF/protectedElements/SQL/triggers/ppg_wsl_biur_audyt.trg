CREATE OR REPLACE TRIGGER PPADM.PPG_WSL_BIUR_AUDYT
  BEFORE INSERT or UPDATE ON PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW
  FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN
  IF inserting THEN
    IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
      eap_audyt.ustaw_audyt(:NEW.WSL_AUDYT_UT, :NEW.WSL_AUDYT_DT, :NEW.WSL_AUDYT_KT, :NEW.WSL_AUDYT_LM, :OLD.WSL_AUDYT_LM, FALSE);
    ELSE
      eap_audyt.ustaw_audyt(:NEW.WSL_AUDYT_UT, :NEW.WSL_AUDYT_DT, :NEW.WSL_AUDYT_KT, :NEW.WSL_AUDYT_LM, :OLD.WSL_AUDYT_LM, TRUE);
    END IF;
  ELSE
    IF eap_globals.odczytaj_modyfikuj_audyt = 'T' THEN
      IF UPPER(USER) NOT LIKE 'WEBADM%' THEN
        eap_audyt.ustaw_audyt(:NEW.WSL_AUDYT_UM, :NEW.WSL_AUDYT_DM, :NEW.WSL_AUDYT_KM, :NEW.WSL_AUDYT_LM, :OLD.WSL_AUDYT_LM, FALSE);
      ELSE
        eap_audyt.ustaw_audyt(:NEW.WSL_AUDYT_UM, :NEW.WSL_AUDYT_DM, :NEW.WSL_AUDYT_KM, :NEW.WSL_AUDYT_LM, :OLD.WSL_AUDYT_LM, TRUE);
      END IF;
    END IF;
  END IF;

  ------------------------------------------------------------------------------------------------------------------------------------------------------
  --aktualizacja/zmiana wersji w PPT_OBJECTS
  update ppadm.ppt_objects set objp_version = to_char(sysdate,'YYYY-MM-DD HH24:MI:SS') where objp_object_type = 'SL' and objp_name = :NEW.WSL_SL_NAZWA;
  ------------------------------------------------------------------------------------------------------------------------------------------------------

END;
/
