CREATE OR REPLACE TRIGGER PPADM.PPG_ZSZ_USUN_ZALACZNIKI
BEFORE DELETE ON PPADM.PPT_SZK_ZAPISY_SZKOLEN
REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------  
  DECLARE
	v_zsz_id NUMBER(10,0);
BEGIN
	v_zsz_id := :old.ZSZ_ID;
	DELETE FROM egadm1.zkt_zalaczniki
	WHERE ZAL_ZSZ_ID = v_zsz_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_zsz_usun_zalaczniki');
END;
/
