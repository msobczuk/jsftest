CREATE OR REPLACE TRIGGER PPADM.PPG_OCE_USUN_ZALACZNIKI
BEFORE DELETE ON PPADM.PPT_OCE_OCENY
REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------   
  DECLARE
	v_oce_id NUMBER(10,0);
BEGIN
	v_oce_id := :old.OCE_ID;
	DELETE FROM egadm1.zkt_zalaczniki
	WHERE ZAL_OCE_ID = v_oce_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_oce_usun_zalaczniki');
END;
/
