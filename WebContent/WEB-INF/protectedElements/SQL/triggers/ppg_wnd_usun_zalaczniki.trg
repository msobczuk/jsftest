CREATE OR REPLACE TRIGGER PPADM.ppg_wnd_usun_zalaczniki
BEFORE DELETE 
	ON PPADM.PPT_DEL_WNIOSKI_DELEGACJI
	FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/ppg_wnd_usun_zalaczniki.trg 3 1.0.3 09.03.18 09:07 MGOLDA                                   $
-- $Revision:: 3                                                                                                                                              $
-- $Workfile:: ppg_wnd_usun_zalaczniki.trg                                                                                                                    $
-- $Modtime:: 09.03.18 09:07                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE
	v_wnd_id NUMBER(10,0);
BEGIN
	v_wnd_id := :old.wnd_id;
	DELETE FROM zkt_zalaczniki
	WHERE ZAL_WND_ID = v_wnd_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_wnd_usun_zalaczniki');
END;
/
