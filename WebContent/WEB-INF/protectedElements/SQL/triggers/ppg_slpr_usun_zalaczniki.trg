CREATE OR REPLACE TRIGGER PPADM.PPG_SLPR_USUN_ZALACZNIKI
BEFORE DELETE ON PPADM.PPT_SLP_SLUZBA_PRZYG
REFERENCING FOR EACH ROW
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
---------------------------------------------------------------------------------------------------------------------------------------------------------------------   
  DECLARE
	v_slpr_id NUMBER(10,0);
BEGIN
	v_slpr_id := :old.SLPR_ID;
	DELETE FROM egadm1.zkt_zalaczniki
	WHERE ZAL_SLPR_ID = v_slpr_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_slpr_usun_zalaczniki');
END;
/
