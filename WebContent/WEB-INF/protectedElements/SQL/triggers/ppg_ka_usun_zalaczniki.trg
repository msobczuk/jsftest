CREATE OR REPLACE TRIGGER PPADM.ppg_ka_usun_zalaczniki
BEFORE DELETE 
	ON PPADM.PPT_REK_KANDYDACI
	FOR EACH ROW
	---------------------------------------------------------------------------------------------------------------------------------------------------------------
	-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/triggers/ppg_ka_usun_zalaczniki.trg 3 1.0.3 09.03.18 09:06 MGOLDA                                    $
	-- $Revision:: 3                                                                                                                                              $
	-- $Workfile:: ppg_ka_usun_zalaczniki.trg                                                                                                                     $
	-- $Modtime:: 09.03.18 09:06                                                                                                                                  $
	-- $Author:: MGOLDA                                                                                                                                           $
	---------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE
	v_ka_id NUMBER(10,0);
BEGIN
	v_ka_id := :old.ka_id;
	DELETE FROM zkt_zalaczniki
	WHERE ZAL_PP_KA_ID = v_ka_id;
EXCEPTION
	WHEN OTHERS THEN
	PPADM.PLOG('p_procedura => ppg_ka_usun_zalaczniki');
END;
/
