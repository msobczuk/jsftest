-- data version: ZFSS_PROGRAMY_MULTISPORT 2019.09.17 12:37.59
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.24 14:15:45
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='N' 
where OBJP_OBJECT_TYPE='SL' and objp_owner='PPADM' and OBJP_NAME='ZFSS_PROGRAMY_MULTISPORT';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW WHERE WSL_SL_NAZWA = 'ZFSS_PROGRAMY_MULTISPORT';
DELETE FROM PPADM.PPT_ADM_SLOWNIKI WHERE SL_NAZWA = 'ZFSS_PROGRAMY_MULTISPORT';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_SLOWNIKI','SL_NAZWA in ('ZFSS_PROGRAMY_MULTISPORT')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_SLOWNIKI        ( SL_AUDYT_UM, SL_AUDYT_UT, SL_AUDYT_DT, SL_AUDYT_DM, SL_NAZWA, SL_OPIS, SL_MAX_DLUGOSC, SL_MAX_ILOSC, SL_POZIOM_DOSTEPU, SL_WZORZEC, SL_AUDYT_KT, SL_AUDYT_KM, SL_AUDYT_LM, SL_F_INSTALL_DODAJ, SL_F_INSTALL_USUN, SL_F_INSTALL_MODYFIKUJ, SL_NAZWA_ZEWN, SL_ID, SL_F_AKTUALIZUJ  )
  values ('','PPADM','2019-09-24 18:47:47','','ZFSS_PROGRAMY_MULTISPORT','Programy MultiSport','100','','0','','NBMDZIOBEK\Marcin.Dziobek','','1','T','T','T','','-85','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_WARTOSCI_SLOWNIKOW','WSL_SL_NAZWA in ('ZFSS_PROGRAMY_MULTISPORT')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('MultiSport1','','ZFSS_PROGRAMY_MULTISPORT','UC','100,01','90,01','60,01','','','','','','','Program MultiSport 1','','','','','','','','','PPADM','2019-09-24 18:48:11','1','ADMINISTRATOR_PORTAL','2019-09-17 12:37:59','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','','-1130','N','-85','1');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('MultiSport2','','ZFSS_PROGRAMY_MULTISPORT','UC2','120,02','100,02','90,02','','','','','','','Program MultiSport 2','','','','','','','','','PPADM','2019-09-24 18:48:11','1','ADMINISTRATOR_PORTAL','2019-09-17 12:37:59','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','','-1132','N','-85','2');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('MultiSport999','','ZFSS_PROGRAMY_MULTISPORT','UC','99,99 (kwota potr�cenia za pracownika)','88,88 (kwota potr�cenia za osob� towarzysz�c� pow. 15 lat)','55,55(kwota potr�cenia za ka�de dziecko zg�oszone w programie)','','','','','','','Przyk�ad -  Nazwa programu Multisport','','','','','','','','','PPADM','2019-09-24 18:48:11','1','ADMINISTRATOR_PORTAL','2019-09-17 12:37:59','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','','-1129','A','-85','999');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
