------------------------------------------------------------------------------------------
-- DMP_SL: BHP_DOMYSLNY_SK
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='N' 
where OBJP_OBJECT_TYPE='SL' and objp_owner='PPADM' and OBJP_NAME='BHP_DOMYSLNY_SK';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW WHERE WSL_SL_NAZWA = 'BHP_DOMYSLNY_SK';
DELETE FROM PPADM.PPT_ADM_SLOWNIKI WHERE SL_NAZWA = 'BHP_DOMYSLNY_SK';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_SLOWNIKI','SL_NAZWA in ('BHP_DOMYSLNY_SK')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_SLOWNIKI        ( SL_AUDYT_UM, SL_AUDYT_UT, SL_AUDYT_DT, SL_AUDYT_DM, SL_NAZWA, SL_OPIS, SL_MAX_DLUGOSC, SL_MAX_ILOSC, SL_POZIOM_DOSTEPU, SL_WZORZEC, SL_AUDYT_KT, SL_AUDYT_KM, SL_AUDYT_LM, SL_F_INSTALL_DODAJ, SL_F_INSTALL_USUN, SL_F_INSTALL_MODYFIKUJ, SL_NAZWA_ZEWN, SL_ID, SL_F_AKTUALIZUJ  )
  values ('','ADMINISTRATOR_PORTAL','2020-07-03 12:07:42','','BHP_DOMYSLNY_SK','Domy�lny sk_kod dla LW PPL_BHP_OKUL_STAN_KOSZTOW','100','','0','','10.132.216.95\tomcat','','1','T','T','T','','-114','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_WARTOSCI_SLOWNIKOW','WSL_SL_NAZWA in ('BHP_DOMYSLNY_SK')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('PL-19-C-12-75001-3020-BDG-000-00-206-0194-00-000-000-00.00.00.00-00N-BDG-00','','BHP_DOMYSLNY_SK','UC','','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','2020-07-03 12:07:43','3','ADMINISTRATOR_PORTAL','2020-07-03 12:37:56','10.132.216.95\tomcat','10.132.216.95\tomcat','','-1272','N','-114','1');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
