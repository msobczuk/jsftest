-- data version: SZABLONLOV_FOOTERS 2020.02.07 10:04.42
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.24 14:15:46
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='N' 
where OBJP_OBJECT_TYPE='SL' and objp_owner='PPADM' and OBJP_NAME='SZABLONLOV_FOOTERS';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW WHERE WSL_SL_NAZWA = 'SZABLONLOV_FOOTERS';
DELETE FROM PPADM.PPT_ADM_SLOWNIKI WHERE SL_NAZWA = 'SZABLONLOV_FOOTERS';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_SLOWNIKI','SL_NAZWA in ('SZABLONLOV_FOOTERS')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_SLOWNIKI        ( SL_AUDYT_UM, SL_AUDYT_UT, SL_AUDYT_DT, SL_AUDYT_DM, SL_NAZWA, SL_OPIS, SL_MAX_DLUGOSC, SL_MAX_ILOSC, SL_POZIOM_DOSTEPU, SL_WZORZEC, SL_AUDYT_KT, SL_AUDYT_KM, SL_AUDYT_LM, SL_F_INSTALL_DODAJ, SL_F_INSTALL_USUN, SL_F_INSTALL_MODYFIKUJ, SL_NAZWA_ZEWN, SL_ID, SL_F_AKTUALIZUJ  )
  values ('ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-01-23 17:33:49','2020-02-07 10:04:42','SZABLONLOV_FOOTERS','Stopki dla list warto�ci wy�wietlane w widokach SzablonLOV.xhtml','100','','0','','10.132.216.41\NIEZNANY','10.132.216.41\NIEZNANY','113','T','T','T','','-106','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_WARTOSCI_SLOWNIKOW','WSL_SL_NAZWA in ('SZABLONLOV_FOOTERS')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('PPL_RODZINA','','SZABLONLOV_FOOTERS','UC','','','','','','','','','','<hr class="ui-separator ui-state-default ui-corner-all ui-g-12 p0"/>
<a href="https://intranet.comarch/fileupload/Dokumenty%20Dzia%C5%82u%20Personalnego/1d9d5ec484ad73ccdd283f5846f5c0b9_zgloszenie-czonkw-rodziny-do-ZUS.docx" 
   class="ctl-oLink fs16 mr40" title="Formularz zg�oszenia cz�onk�w rodziny">Formularz zg�oszenia cz�onk�w rodziny</a>','<table>
  <tr>
    <td><i class="material-icons fs50">face</i></td>
    <td class="fs30">
      #{user.ekPracownik.get(''prc_imie'')} #{user.ekPracownik.get(''prc_imie2'')} #{user.ekPracownik.get(''prc_nazwisko'')} 
      / Cz�onkowie rodziny - ubezpieczenie zdrowotne
	</td>
  </tr>
</table>

<hr class="ui-separator ui-state-default ui-corner-all ui-g-12 p0"/>','','','','','','','','ADMINISTRATOR_PORTAL','2020-02-07 09:53:49','15','ADMINISTRATOR_PORTAL','2020-02-07 10:04:42','10.132.216.41\NIEZNANY','10.132.216.41\NIEZNANY','','-1236','','-106','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('PPL_TEST_WB','','SZABLONLOV_FOOTERS','UC','<--stopka/html','<--nag��wek/html','','','','','','','','<html xmlns="http://www.w3.org/1999/xhtml"
	  xmlns:h="http://java.sun.com/jsf/html"
	  xmlns:f="http://java.sun.com/jsf/core"
	  xmlns:p="http://primefaces.org/ui"
	  xmlns:ui="http://java.sun.com/jsf/facelets"
	  xmlns:pe="http://primefaces.org/ui/extensions"
	  xmlns:c="http://xmlns.jcp.org/jsf/composite/components">

<h:body>

	<ui:composition>
<p:separator />
      
<a href="https://intranet.comarch/pl/dzialy-wsparcia/dzial-personalny/formularze" target="_blank" class="ctl-oLink fs16 mr40">link nr 1</a> 
<a href="https://intranet.comarch/pl/dzialy-wsparcia/dzial-personalny/formularze" target="_blank" class="ctl-oLink fs16 mr40">link nr 2</a>

  </ui:composition>
</h:body>
</html>','<table> 		
  <tr> 			
    <td><i class="material-icons fs50">face</i></td> 			
    <td class="fs30">
      #{user.ekPracownik.get(''prc_imie'')} #{user.ekPracownik.get(''prc_imie2'')} #{user.ekPracownik.get(''prc_nazwisko'')} / PPK:
    </td> 		
  </tr> 	
</table>  	
<hr class="ui-separator ui-state-default ui-corner-all ui-g-12 p0"/>','','','','','','','','ADMINISTRATOR_PORTAL','2020-01-23 17:33:49','174','ADMINISTRATOR_PORTAL','2020-02-07 10:04:42','10.132.216.41\NIEZNANY','10.132.216.41\NIEZNANY','','-1211','N','-106','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('PPL_PWSZ_TEST2020','','SZABLONLOV_FOOTERS','UC','','','','','','','','','','<a href="www.google.pl" target="_blank" class="ctl-oLink fs16 mr40">link nr 1</a> <a href="www.comarch.pl" target="_blank" class="ctl-oLink fs16 mr40">link nr 2</a>','','','','','','','','','ADMINISTRATOR_PORTAL','2020-01-27 12:48:05','137','ADMINISTRATOR_PORTAL','2020-02-07 10:04:42','10.132.216.41\NIEZNANY','10.132.216.41\NIEZNANY','','-1212','N','-106','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
