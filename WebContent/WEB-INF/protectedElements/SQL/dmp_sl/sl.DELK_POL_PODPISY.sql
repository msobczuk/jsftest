-- data version: DELK_POL_PODPISY 2020.06.15 12:45.00
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.24 14:15:53
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='N' 
where OBJP_OBJECT_TYPE='SL' and objp_owner='PPADM' and OBJP_NAME='DELK_POL_PODPISY';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW WHERE WSL_SL_NAZWA = 'DELK_POL_PODPISY';
DELETE FROM PPADM.PPT_ADM_SLOWNIKI WHERE SL_NAZWA = 'DELK_POL_PODPISY';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_SLOWNIKI','SL_NAZWA in ('DELK_POL_PODPISY')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_SLOWNIKI        ( SL_AUDYT_UM, SL_AUDYT_UT, SL_AUDYT_DT, SL_AUDYT_DM, SL_NAZWA, SL_OPIS, SL_MAX_DLUGOSC, SL_MAX_ILOSC, SL_POZIOM_DOSTEPU, SL_WZORZEC, SL_AUDYT_KT, SL_AUDYT_KM, SL_AUDYT_LM, SL_F_INSTALL_DODAJ, SL_F_INSTALL_USUN, SL_F_INSTALL_MODYFIKUJ, SL_NAZWA_ZEWN, SL_ID, SL_F_AKTUALIZUJ  )
  values ('','ADMINISTRATOR_PORTAL','2020-03-04 09:25:06','','DELK_POL_PODPISY','Czynno�ci traktowane jako podpis na wydruku polecenia','100','','0','','0:0:0:0:0:0:0:1\NIEZNANY','','1','T','T','T','','-108','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_WARTOSCI_SLOWNIKOW','WSL_SL_NAZWA in ('DELK_POL_PODPISY')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('102667','','DELK_POL_PODPISY','UC','COB_ID - id czynno�ci z obiegu definiowalnego (dost�pna te� jako HIO_COB_ID w CSST_HISTORIA_OBIEGOW)','','','','','','','','','Sprawdzono pod wzgl�dem merytorycznym/Potwierdzam wykonanie zadania','','','','','','','','','ADMINISTRATOR_PORTAL','2020-03-04 09:25:06','7','ADMINISTRATOR_PORTAL','2020-06-15 12:44:59','0:0:0:0:0:0:0:1\NIEZNANY','0:0:0:0:0:0:0:1\Mikolaj.Golda','','-1244','N','-108','1');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('103505','','DELK_POL_PODPISY','GRAF','KG_ID - id krawedzi grafu z EGADM1.WDRV_KG_DOK_HISTORIA_OBIEGU','','','','','','','','','Sprawdzono pod wzgl�dem formalno-rachunkowym','','','','','','','','','ADMINISTRATOR_PORTAL','2020-03-04 09:25:56','6','ADMINISTRATOR_PORTAL','2020-06-15 12:44:59','0:0:0:0:0:0:0:1\NIEZNANY','0:0:0:0:0:0:0:1\Mikolaj.Golda','','-1245','N','-108','2');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('103515','','DELK_POL_PODPISY','GRAF','KG_ID - id krawedzi grafu z EGADM1.WDRV_KG_DOK_HISTORIA_OBIEGU','','','','','','','','','Sprawdzono pod wzgl�dem formalno-rachunkowym','','','','','','','','','ADMINISTRATOR_PORTAL','2020-03-04 09:26:25','5','ADMINISTRATOR_PORTAL','2020-06-15 12:44:59','0:0:0:0:0:0:0:1\NIEZNANY','0:0:0:0:0:0:0:1\Mikolaj.Golda','','-1246','N','-108','3');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('103504','','DELK_POL_PODPISY','GRAF','KG_ID - id krawedzi grafu z EGADM1.WDRV_KG_DOK_HISTORIA_OBIEGU','','','','','','','','','G��wny Ksi�gowy','','','','','','','','','ADMINISTRATOR_PORTAL','2020-03-04 09:26:51','4','ADMINISTRATOR_PORTAL','2020-06-15 12:45:00','0:0:0:0:0:0:0:1\NIEZNANY','0:0:0:0:0:0:0:1\Mikolaj.Golda','','-1247','N','-108','4');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('103510','','DELK_POL_PODPISY','GRAF','KG_ID - id krawedzi grafu z EGADM1.WDRV_KG_DOK_HISTORIA_OBIEGU','','','','','','','','','Dyrektor','','','','','','','','','ADMINISTRATOR_PORTAL','2020-03-04 09:27:17','3','ADMINISTRATOR_PORTAL','2020-06-15 12:45:00','0:0:0:0:0:0:0:1\NIEZNANY','0:0:0:0:0:0:0:1\Mikolaj.Golda','','-1248','N','-108','5');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('103085','','DELK_POL_PODPISY','UC','COB_ID - id czynno�ci z obiegu definiowalnego (dost�pna te� jako HIO_COB_ID w CSST_HISTORIA_OBIEGOW)','','','','','','','','','Weryfikacja bud�etu','','','','','','','','','ADMINISTRATOR_PORTAL','2020-06-15 12:44:28','2','ADMINISTRATOR_PORTAL','2020-06-15 12:45:00','0:0:0:0:0:0:0:1\Mikolaj.Golda','0:0:0:0:0:0:0:1\Mikolaj.Golda','','-1263','N','-108','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('102661','','DELK_POL_PODPISY','UC','COB_ID - id czynno�ci z obiegu definiowalnego (dost�pna te� jako HIO_COB_ID w CSST_HISTORIA_OBIEGOW)','','','','','','','','','Weryfikacja bud�etu','','','','','','','','','ADMINISTRATOR_PORTAL','2020-06-15 12:44:59','1','','','0:0:0:0:0:0:0:1\Mikolaj.Golda','','','-1264','N','-108','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
