-- data version: PPS_MAPOWANIE_BLEDOW 2020.06.01 16:23.21
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.24 14:15:49
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='N' 
where OBJP_OBJECT_TYPE='SL' and objp_owner='PPADM' and OBJP_NAME='PPS_MAPOWANIE_BLEDOW';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW WHERE WSL_SL_NAZWA = 'PPS_MAPOWANIE_BLEDOW';
DELETE FROM PPADM.PPT_ADM_SLOWNIKI WHERE SL_NAZWA = 'PPS_MAPOWANIE_BLEDOW';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_SLOWNIKI','SL_NAZWA in ('PPS_MAPOWANIE_BLEDOW')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_SLOWNIKI        ( SL_AUDYT_UM, SL_AUDYT_UT, SL_AUDYT_DT, SL_AUDYT_DM, SL_NAZWA, SL_OPIS, SL_MAX_DLUGOSC, SL_MAX_ILOSC, SL_POZIOM_DOSTEPU, SL_WZORZEC, SL_AUDYT_KT, SL_AUDYT_KM, SL_AUDYT_LM, SL_F_INSTALL_DODAJ, SL_F_INSTALL_USUN, SL_F_INSTALL_MODYFIKUJ, SL_NAZWA_ZEWN, SL_ID, SL_F_AKTUALIZUJ  )
  values ('ADMINISTRATOR_PORTAL','ADMINISTRATOR_PORTAL','2020-05-04 12:33:01','2020-05-04 12:33:39','PPS_MAPOWANIE_BLEDOW','Mapowanie komunikat�w b��d�w przechwytywanych przez aplikacj�.','100','','0','','10.132.216.95\NIEZNANY','0:0:0:0:0:0:0:1\NIEZNANY','2','T','T','T','','-111','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_WARTOSCI_SLOWNIKOW','WSL_SL_NAZWA in ('PPS_MAPOWANIE_BLEDOW')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('zzzzjavax.persistence.PersistenceException','','PPS_MAPOWANIE_BLEDOW','WARN','tak nie wolno, bo javax.persistence.PersistenceException jest typem zbyt og�lnym (mo�e mie� r�ne przyczyny)','aby to mapowanie zablokowa� dopisuj� zzzz','','','','','','','','B��d zapisu.','','','','','','','','','ADMINISTRATOR_PORTAL','2020-05-04 12:33:01','16','ADMINISTRATOR_PORTAL','2020-06-01 16:23:21','10.132.216.95\NIEZNANY','10.132.216.41\tomcat','','-1257','N','-111','-3');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('ERR_PRZYKLAD','','PPS_MAPOWANIE_BLEDOW','ALERT','tu wpisujemy tekst do wy�wietlania (a w polu Typ wpisz WARN/INFO/ALERT)','','','','','','','','','tu wpisuj tekst (fragment) do wy�apania','','','','','','','','','ADMINISTRATOR_PORTAL','2020-05-04 12:33:01','16','ADMINISTRATOR_PORTAL','2020-06-01 16:23:21','10.132.216.95\NIEZNANY','10.132.216.41\tomcat','','-1254','N','-111','0');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('DEL_ERR001','','PPS_MAPOWANIE_BLEDOW','WARN','Operacja przerwana. Brak uprawnienia do rodzaju dokumentu PPD dla dok. w obiegu graficznym systemu Egeria.','','','','','','','','','Operacja przerwana. Brak uprawnienia do rodzaju dokumentu PPD.','','','','','','','','','ADMINISTRATOR_PORTAL','2020-05-04 12:33:01','16','ADMINISTRATOR_PORTAL','2020-06-01 16:23:21','10.132.216.95\NIEZNANY','10.132.216.41\tomcat','','-1255','N','-111','1');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('javax.persistence.OptimisticLockException','','PPS_MAPOWANIE_BLEDOW','WARN','NARUSZENIE WSPӣBIE�NO�CI\nZapis zmian nie jest mo�liwy. Te dane zosta�y przed chwil� zmodyfikowane przez innego u�ytkownika lub proces.','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','2020-05-04 12:33:01','16','ADMINISTRATOR_PORTAL','2020-06-01 16:23:21','10.132.216.95\NIEZNANY','10.132.216.41\tomcat','','-1256','N','-111','2');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('niepoprawna czynno�� dla bie��cego stanu obiegu','','PPS_MAPOWANIE_BLEDOW','WARN','Inny u�ytkownik w�a�nie zmieni� stan obiegu','','','','','','','','','','','','','','','','','','ADMINISTRATOR_PORTAL','2020-05-08 12:11:08','12','ADMINISTRATOR_PORTAL','2020-06-01 16:23:21','0:0:0:0:0:0:0:1\NIEZNANY','10.132.216.41\tomcat','','-1258','N','-111','4');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('PPC_WND_NUMER_UK','','PPS_MAPOWANIE_BLEDOW','WARN','Nieprawid�owy numer delegacji (identyczny numer ju� istnieje w systemie).','','','','','','','','','ORA-00001: naruszono wi�zy unikatowe (PPADM.PPC_WND_NUMER_UQ)','','','','','','','','','ADMINISTRATOR_PORTAL','2020-05-27 18:35:35','10','ADMINISTRATOR_PORTAL','2020-06-01 16:23:21','10.154.80.82\tomcat','10.132.216.41\tomcat','','-1259','N','-111','5');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
