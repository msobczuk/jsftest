-- data version: WN_AKT_DANYCH 2019.07.24 17:19.50
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.24 14:15:45
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='N' 
where OBJP_OBJECT_TYPE='SL' and objp_owner='PPADM' and OBJP_NAME='WN_AKT_DANYCH';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW WHERE WSL_SL_NAZWA = 'WN_AKT_DANYCH';
DELETE FROM PPADM.PPT_ADM_SLOWNIKI WHERE SL_NAZWA = 'WN_AKT_DANYCH';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_SLOWNIKI','SL_NAZWA in ('WN_AKT_DANYCH')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_SLOWNIKI        ( SL_AUDYT_UM, SL_AUDYT_UT, SL_AUDYT_DT, SL_AUDYT_DM, SL_NAZWA, SL_OPIS, SL_MAX_DLUGOSC, SL_MAX_ILOSC, SL_POZIOM_DOSTEPU, SL_WZORZEC, SL_AUDYT_KT, SL_AUDYT_KM, SL_AUDYT_LM, SL_F_INSTALL_DODAJ, SL_F_INSTALL_USUN, SL_F_INSTALL_MODYFIKUJ, SL_NAZWA_ZEWN, SL_ID, SL_F_AKTUALIZUJ  )
  values ('ADMINISTRATOR_PORTAL','PPADM','2019-09-24 18:47:47','2019-06-21 18:07:52','WN_AKT_DANYCH','Rodzaje wniosk�w o aktualizacj� danych pracownika','100','','0','','NBMDZIOBEK\Marcin.Dziobek','10.132.216.41\NIEZNANY','1','T','T','T','','-73','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_WARTOSCI_SLOWNIKOW','WSL_SL_NAZWA in ('WN_AKT_DANYCH')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('KTEL','','WN_AKT_DANYCH','UC','EGADM1.CKK_TELEFONY','where tl_f_domyslny = ''T'' and tl_prc_id = :P_PRC_ID','','','','','','','','Wniosek o aktualizacj� danych kontaktowych (ksi��ka telefoniczna)','','','','','','','','','PPADM','2019-09-24 18:48:11','1','ADMINISTRATOR_PORTAL','2019-07-24 17:19:50','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','WN_AKT_POZ_KONTAKT','-1030','N','-73','1');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('TELNP','','WN_AKT_DANYCH','UC','EGADM1.CKK_TELEFONY','where nvl(tl_f_domyslny,''N'') = ''N'' and tl_prc_id = :P_PRC_ID','','','','','','','','Wniosek o aktualizacj� danych kontaktowych (niepubliczne)','','','','','','','','','PPADM','2019-09-24 18:48:11','1','ADMINISTRATOR_PORTAL','2019-07-24 17:19:50','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','WN_AKT_POZ_KONTAKT','-1093','N','-73','2');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('PRC','','WN_AKT_DANYCH','UC','EGADM1.EK_PRACOWNICY','where prc_id = :P_PRC_ID','','','','','','','','Wniosek o aktualizacj� danych osobowych','','','','','','','','','PPADM','2019-09-24 18:48:11','1','ADMINISTRATOR_PORTAL','2019-07-24 17:19:50','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','WN_AKT_POZ_OSOB','-1031','N','-73','10');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('ADRES','PPL_TYP_ADRESU','WN_AKT_DANYCH','LW','EGADM1.CKK_ADRESY','where adr_zatwierdzony=''T'' and adr_f_aktualne = ''T'' and adr_typ=:P_INF_ID and adr_prc_id = :P_PRC_ID','(%wsl_wartosc$s) %wsl_opis$s','','','','','','','Wniosek o aktualizacj� adresu','','Typ adresu:','','','','','','','PPADM','2019-09-24 18:48:11','1','ADMINISTRATOR_PORTAL','2019-07-24 17:19:50','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','WN_AKT_POZ_ADRES','-1062','N','-73','20');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('INNE','','WN_AKT_DANYCH','UC','','','','','','','','','','Wniosek o r�czn� aktualizacj� wed�ug opisu i za��cznik�w','','','','','','','','','PPADM','2019-09-24 18:48:11','1','ADMINISTRATOR_PORTAL','2019-07-24 17:19:50','NBMDZIOBEK\Marcin.Dziobek','0:0:0:0:0:0:0:1\NIEZNANY','','-1094','N','-73','30');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
