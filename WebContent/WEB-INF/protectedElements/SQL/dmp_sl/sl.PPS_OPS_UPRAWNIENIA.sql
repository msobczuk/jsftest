-- data version: PPS_OPS_UPRAWNIENIA 2019.09.24 18:48.11
------------------------------------------------------------------------------------------
-- dmp from DB921 / PPADM / created 2020.06.24 14:15:48
------------------------------------------------------------------------------------------
update ppadm.ppt_objects set OBJP_F_AUTO_AKT='N' 
where OBJP_OBJECT_TYPE='SL' and objp_owner='PPADM' and OBJP_NAME='PPS_OPS_UPRAWNIENIA';
------------------------------------------------------------------------------------------

DELETE FROM PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW WHERE WSL_SL_NAZWA = 'PPS_OPS_UPRAWNIENIA';
DELETE FROM PPADM.PPT_ADM_SLOWNIKI WHERE SL_NAZWA = 'PPS_OPS_UPRAWNIENIA';
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_SLOWNIKI','SL_NAZWA in ('PPS_OPS_UPRAWNIENIA')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_SLOWNIKI        ( SL_AUDYT_UM, SL_AUDYT_UT, SL_AUDYT_DT, SL_AUDYT_DM, SL_NAZWA, SL_OPIS, SL_MAX_DLUGOSC, SL_MAX_ILOSC, SL_POZIOM_DOSTEPU, SL_WZORZEC, SL_AUDYT_KT, SL_AUDYT_KM, SL_AUDYT_LM, SL_F_INSTALL_DODAJ, SL_F_INSTALL_USUN, SL_F_INSTALL_MODYFIKUJ, SL_NAZWA_ZEWN, SL_ID, SL_F_AKTUALIZUJ  )
  values ('','PPADM','2019-09-24 18:47:47','','PPS_OPS_UPRAWNIENIA','Szczeg�lne uprawnienia u�ywane na opisie stanowiska','240','','0','','NBMDZIOBEK\Marcin.Dziobek','','1','T','T','T','','-65','');
------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------
--by gen_dml_table_inserts ('PPADM','PPT_ADM_WARTOSCI_SLOWNIKOW','WSL_SL_NAZWA in ('PPS_OPS_UPRAWNIENIA')')...
------------------------------------------------------------------------------------------
alter session set nls_date_format='YYYY-MM-DD HH24:MI:SS';
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('2','','PPS_OPS_UPRAWNIENIA','UC','Specjalistyczne szkolenie w zakresie bezpiecze�stwa system�w i sieci teleinformatycznych organizowane przez Departament Bezpiecze�stwa Teleinformatycznego Agencji Bezpiecze�stwa Wewn�trznego','','','','','','','','','Specjalistyczne szkolenie w zakresie bezpiecze�stwa system�w i sieci teleinformatycznych organizowane przez Departament Bezpiecze�stwa Teleinformatycznego Agencji Bezpiecze�stwa Wewn�trznego','','','','','','','','','PPADM','2019-09-24 18:48:11','1','','','NBMDZIOBEK\Marcin.Dziobek','','','-967','Z','-65','');
------------------------------------------------------------------------------------------
INSERT INTO PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW        ( WSL_WARTOSC, WSL_WARTOSC_MAX, WSL_SL_NAZWA, WSL_TYP, WSL_ALIAS, WSL_ALIAS2, WSL_ALIAS3, WSL_ALIAS4, WSL_ALIAS5, WSL_ALIAS6, WSL_ALIAS7, WSL_ALIAS8, WSL_ALIAS9, WSL_OPIS, WSL_OPIS2, WSL_OPIS3, WSL_OPIS4, WSL_OPIS5, WSL_OPIS6, WSL_OPIS7, WSL_OPIS8, WSL_OPIS9, WSL_AUDYT_UT, WSL_AUDYT_DT, WSL_AUDYT_LM, WSL_AUDYT_UM, WSL_AUDYT_DM, WSL_AUDYT_KT, WSL_AUDYT_KM, WSL_WARTOSC_ZEWN, WSL_ID, WSL_STATUS, WSL_SL_ID, WSL_LP  )
  values ('1','','PPS_OPS_UPRAWNIENIA','UC','Po�wiadczenie bezpiecze�stwa uprawniaj�ce do dost�pu do informacji niejawnych oznaczonych klauzul� co najmniej "Poufne" lub zgoda na poddanie si� procedurze uzyskania tego po�wiadczenia','','','','','','','','','Po�wiadczenie bezpiecze�stwa uprawniaj�ce do dost�pu do informacji niejawnych oznaczonych klauzul� co najmniej "Poufne" lub zgoda na poddanie si� procedurze uzyskania tego po�wiadczenia','','','','','','','','','PPADM','2019-09-24 18:48:11','1','','','NBMDZIOBEK\Marcin.Dziobek','','','-966','Z','-65','');
------------------------------------------------------------------------------------------

--ascii/cp1250:����󜟿
