CREATE OR REPLACE FUNCTION PPADM.GET_PRC_STJO_ID (p_prc_id NUMBER, p_na_dzien DATE DEFAULT SYSDATE) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
 v_stjo_id NUMBER;

BEGIN

	select stjo_id
    into v_stjo_id
  from EK_ZATRUDNIENIE zat
  left join ZPT_ST_W_JO stjo on NVL(zat.zat_ob_id_wydzial, zat.zat_ob_id) = stjo_ob_id and zat_stn_id = stjo_stn_id
  --left join ek_ref_codes tu on tu.rv_domain = 'TYP_UMOWY' AND tu.rv_low_value = zat.ZAT_TYP_UMOWY
  where zat.zat_prc_id = p_prc_id
        and trunc(p_na_dzien) BETWEEN zat_data_zmiany AND nvl(zat_data_do, sysdate+1) and zat.ZAT_TYP_UMOWY = 0 --Umowa o prac�
  ;

    return v_stjo_id;

EXCEPTION
	WHEN OTHERS THEN
		return null;

END;
/
