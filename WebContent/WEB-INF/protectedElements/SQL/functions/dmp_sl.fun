create or replace function ppadm.dmp_sl(p_sl_nazwa varchar2) return clob as
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
    /*

    funkcja realizuje zrzut danych z definicji wybranego slownika
    wynikiem jest skrypt dml, ktory w docelowej bazie kasuje i wgrywa na nowo definicje slownika (parametr p_sl_nazwa)
    przykad wywolania:

    begin
        dbms_output.put_line(dmp_sl('TESTOWY'));
    end;

    */

    v_clob clob := '';
    v_aa varchar2(1) := 'N';

begin

    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    v_clob := v_clob ||'-- DMP_SL: '|| upper(p_sl_nazwa) || CHR(13)||CHR(10);
    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    for r in (select OBJP_F_AUTO_AKT from PPADM.PPT_OBJECTS where OBJP_OBJECT_TYPE='SL' and objp_owner='PPADM' and OBJP_NAME = p_sl_nazwa) loop
            v_aa := r.OBJP_F_AUTO_AKT;
    end loop;
    v_clob := v_clob ||'update ppadm.ppt_objects set OBJP_F_AUTO_AKT='''||v_aa||''' ' || CHR(13)||CHR(10)
                     ||'where OBJP_OBJECT_TYPE=''SL'' and objp_owner=''PPADM'' and OBJP_NAME='''||p_sl_nazwa||''';' || CHR(13)||CHR(10);
    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);

    v_clob := v_clob || CHR(13)||CHR(10);
    v_clob := v_clob || 'DELETE FROM PPADM.PPT_ADM_WARTOSCI_SLOWNIKOW WHERE WSL_SL_NAZWA = '''||p_sl_nazwa||''';' || CHR(13)||CHR(10);
    v_clob := v_clob || 'DELETE FROM PPADM.PPT_ADM_SLOWNIKI WHERE SL_NAZWA = '''||p_sl_nazwa||''';' || CHR(13)||CHR(10);
    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    v_clob := v_clob || CHR(13)||CHR(10);
    v_clob := v_clob || ppadm.dmp('PPADM', 'PPT_ADM_SLOWNIKI','SL_NAZWA in ('''||p_sl_nazwa||''')', 'SL_NAZWA');
    v_clob := v_clob || ppadm.dmp('PPADM', 'PPT_ADM_WARTOSCI_SLOWNIKOW','WSL_SL_NAZWA in ('''||p_sl_nazwa||''')', 'WSL_SL_NAZWA, WSL_LP');

    --dbms_output.put_line(v_clob);
    return v_clob;
end;
/
