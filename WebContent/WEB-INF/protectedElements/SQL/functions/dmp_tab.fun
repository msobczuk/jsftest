create or replace FUNCTION dmp_tab (p_table_name IN VARCHAR2) RETURN CLOB
--------------------------------------------------------------------------------
-- $Header::                                                                   $
-- $Revision::                                                                 $
-- $Workfile:: 							                                       $
-- $Modtime:: 				                                                   $
-- $Author::			                                                       $
--------------------------------------------------------------------------------
IS
  v_clob              CLOB := '';
  --
  v_tb_id             NUMBER(10);
  v_tb_owner          VARCHAR2(30);
  v_tb_table_name     VARCHAR2(30);
  v_tb_entity_name    VARCHAR2(30);
  v_tb_column_prefix  VARCHAR2(10);
  v_tb_status         VARCHAR2(10);
  v_tb_desc           VARCHAR2(4000);
  --
  v_tc_table          VARCHAR2(40);
BEGIN
  v_clob := v_clob || '------------------------------------------------------------------------------------------' || CHR(13) || CHR(10);
  v_clob := v_clob || '-- dmp ' || p_table_name || ' from ' || sys_context('userenv', 'db_name') || ' / ' || USER 
                   || ' / created ' || TO_CHAR(SYSDATE, 'YYYY.MM.DD HH24:MI:SS') || CHR(13) || CHR(10);
  v_clob := v_clob || '------------------------------------------------------------------------------------------' || CHR(13) || CHR(10);
  v_clob := v_clob || 'BEGIN' || CHR(13) || CHR(10);
  --
  v_tb_table_name := p_table_name;
  --
  v_clob := v_clob || '  DELETE FROM ppt_tab_cols WHERE tc_tb_id = (SELECT tb_id FROM ppt_tables WHERE tb_table_name = ''' 
                   || v_tb_table_name || ''');' || CHR(13) || CHR(10);
  v_clob := v_clob || '  DELETE FROM ppt_tables WHERE tb_table_name = ''' || v_tb_table_name || ''';' || CHR(13) || CHR(10);
  --
  SELECT tb_id, tb_owner, tb_entity_name, tb_column_prefix, tb_status, tb_desc
    INTO v_tb_id, v_tb_owner, v_tb_entity_name, v_tb_column_prefix, v_tb_status, v_tb_desc
    FROM ppt_tables
   WHERE tb_table_name = v_tb_table_name;
  --
  v_clob := v_clob || '  ----------------------------------------------------------------------------------------' || CHR(13) || CHR(10);
  --
  v_clob := v_clob || '  ppadm.pp_add_tab(''' || v_tb_owner         || ''', ''' || 
                                           '' || v_tb_table_name    || ''', ''' || 
                                           '' || v_tb_entity_name   || ''', ''' || 
                                           '' || v_tb_column_prefix || '''';
  --
  IF v_tb_status <> 'Z' THEN
    v_clob := v_clob || ', p_status => ''' || v_tb_status || ''''; 
  END IF;
  --
  v_clob := v_clob || ', p_desc => ''' || v_tb_desc || ''');' || CHR(13) || CHR(10);
  --
  v_clob := v_clob || '  ----------------------------------------------------------------------------------------' || CHR(13) || CHR(10);
  --
  v_tc_table := v_tb_owner || '.' || v_tb_table_name;
  --
  FOR r IN (SELECT * FROM ppt_tab_cols WHERE tc_tb_id = v_tb_id order by tc_id) LOOP
    --
    v_clob := v_clob || '  ppadm.pp_add_col(''' || v_tc_table         || ''', ''' || 
                                             '' || r.tc_property_name || ''', ''' || 
                                             '' || r.tc_data_type     || ''', ''' || 
                                             '' || r.tc_column_name   || ''', ''' || 
                                             '' || r.tc_property_type || '''';
    --
    IF r.tc_nullable = 'T' THEN
        v_clob := v_clob || ', p_nullable => ''T'''; 
    END IF;
    --
    IF r.tc_default IS NOT NULL THEN
        v_clob := v_clob || ', p_default => ' || r.tc_default; 
    END IF;
    --
    IF r.tc_ix_name IS NOT NULL THEN
        v_clob := v_clob || ', p_ix_name => ''' || r.tc_ix_name || ''''; 
    END IF;
    --
    IF r.tc_uk_name IS NOT NULL THEN
        v_clob := v_clob || ', p_uk_name => ''' || r.tc_uk_name || ''''; 
    END IF;
    --
    IF r.tc_fk_column_name IS NOT NULL THEN
        v_clob := v_clob || ', p_fk_column_name => ''' || r.tc_fk_column_name || ''''; 
    END IF;
    --
	IF r.tc_fk_name IS NOT NULL THEN
        v_clob := v_clob || ', p_fk_name => ''' || r.tc_fk_name || ''''; 
    END IF;
	--
    IF r.tc_status <> 'Z' THEN
        v_clob := v_clob || ', p_status => '''  || r.tc_status || ''''; 
    END IF;
    --
    v_clob := v_clob || ', p_desc => ''' || r.tc_desc || ''');' || CHR(13) || CHR(10);
    --
  END LOOP;
  --
  v_clob := v_clob || 'END;' || CHR(13) || CHR(10);
  v_clob := v_clob || '/' || CHR(13) || CHR(10);
  --
  RETURN v_clob;
END;
/
