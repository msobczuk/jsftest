create or replace function ppadm.dmp(p_owner varchar2, p_table_name varchar2, p_where varchar2 default null, p_order_by varchar2 default null) return clob as
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- generuje dump z danych dla wskazanej tabeli (i opc. warunku p_where) jako skrypt z zestawem instrukcji insert into tabela (...lista kolumn...) values (... lista wartosci ...)
-- UWAGA: w przypadku wartosci clob generowane sa dodatkowe updat'y dodajace wartosc w czesciach (po to aby jedno polecenie DML nie przekroczyo rozmiarem 4K znak�w).
-- UWAGA: ta wersja generuje wynik przydatny dla - przyklad wykonania (JAVA/JDBC+SPRING):
-- ...
-- EncodedResource encResource = new EncodedResource(resource, "Cp1250"); //wa�ne je�li stosujemy CP1250 przy takich DML 
-- org.springframework.jdbc.datasource.init.ScriptUtils.executeSqlScript(connection, encResource,false,false, "--",";","/*","*/");
-- ...
-- org.springframework.jdbc.datasource.init.ScriptUtils.executeSqlScript(connection, resource); //jesli dmp jest generowany z kodowaniem UTF-8
-- ...


--  p_owner       varchar2(32) := 'PPADM';
--  p_table_name  varchar2(32) := 'PPT_ADM_SQLS';
--  p_where       varchar2(4000):= null;
  v_table      varchar2(150) := p_owner||'.'||p_table_name;
  v_txt        varchar2(32000);
  v_order_by   varchar2(32000) := p_order_by;
  v_val        varchar2(32000);
  v_date_format varchar2(50) := 'yyyy-MM-dd HH24:mm:ss';
  v_clob CLOB;
  v_chunk varchar2(2000);
  v_ret CLOB := '';

  --STD/OUT
  procedure dmp(p_txt CLOB) as
  begin
    --dbms_output.put_line(p_txt);-- || chr(13)|| chr(10) );
    v_ret := v_ret || p_txt || chr(13)|| chr(10) ;
  end;


 --przeliczenia/przeksztalcenie wartosci wybranej kolumny w poleceniach cmd i where_update_cl
 function col_value(p_column_name varchar2, p_data_type varchar2) return varchar2 as
     v_ret1        varchar2(32000);
  begin

    v_ret1 := ''||p_column_name;

     if
        p_column_name like '%\_ID' ESCAPE '\'
     or p_column_name like '%\_ID_Z' ESCAPE '\'
     or p_column_name like '%\_ID_DO' ESCAPE '\'
     then 
       v_ret1 := '-abs('||v_ret1||')'; --negowanie id encji 
       -- W PP przy przenoszeniu danych ze srodowiska DEV na PROD chcemy aby przenoszone encje z DEV miay ujemne ID 
       -- Ujemne nie moga skolidowac z rosnacymi z sekwencji lub JPA_GENKEYS warto�ciami ID w g�r� w srod. PROD
       -- W przyszlosci nalezy wykluczyc negowanie dla wybranych encji oraz dodac parametr sterujacy negowaniem id.
     end if;

     if (p_data_type =  'VARCHAR2')  then -- jesli tekst to zamieniamy ' na ''
       v_ret1 := 'replace('||p_column_name||','''''''','''''''''''')';
     end if;

     if (p_data_type = 'CLOB')  then -- jesli tekst to zamieniamy ' na ''
       v_ret1 := 'case when '||p_column_name||' is null then null else 0 end';
     end if;

    return v_ret1;
  end;

begin
   DBMS_OUTPUT.enable(null);

   execute immediate 'alter session set nls_date_format = ''YYYY-MM-DD HH24:MI:SS''';

   --tworzenie sql dla widoku tmp
   v_txt := 'SELECT ';



--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- kol. cmd obliczana jako polecenie postaci ' INSERT (lista kolumn) values (lista wartosci ) ' as cmd
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   v_txt := v_txt || ' ''INSERT INTO ' || v_table;

   --obliczanie ( ... lista kolumn ...) :
   v_txt := v_txt || '        (';
   for c in ( select * from sys.all_tab_columns where owner = p_owner and table_name = p_table_name order by column_id )
   loop
     v_txt := v_txt || ' '||c.column_name ||',';
   end loop;
   v_txt := substr(v_txt,1,length(v_txt)-1) || '  )' ||chr(13)||chr(10);

  --obliczanie kl. values (... lista wartosci ...)
   v_txt :=  v_txt || '  values (''''''||';
   for c in ( select * from sys.all_tab_columns where owner = p_owner and table_name = p_table_name order by column_id )
   loop
     v_val := col_value(c.column_name, c.data_type);
     v_txt := v_txt || v_val || '||'''''',''''''||';--odcinamy ostatni przecinek
   end loop;
   v_txt := substr(v_txt,1,length(v_txt)-8);
   v_txt := v_txt || ''''');'' as cmd ';





--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- pomocnicza kol. update_where_cl wg kolumn PK - np. jesli tabela ma w PK kolumny DOK_TYP i DOK_NR to chcemy uzyskac wpis update_where_cl postaci  " DOK_TYP = '1' and DOK_NR = 'ABC5555' "
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
v_txt := v_txt || ', ''     ';--spacje sa koneiczne aby mozna bylo v_txt w razie cczego skrocic o 5 znakow nawet jesli nie ma zadnej kolumny z PK
for r in ( --iteracja po kulumnach PK i dodawanie wpisow do opcjonalnej kl where (jesli bedzie trzeba dodawac chunki z clob'ami)
          select cc.COLUMN_NAME, ccc.data_type  --PKEY columns
          from sys.all_constraints c
            join sys.ALL_CONS_COLUMNS cc on c.constraint_name = cc.constraint_name
            join sys.all_tab_columns ccc on cc.column_name = ccc.column_name and c.table_name = ccc.table_name and ccc.owner = p_owner
          where c.table_name = p_table_name and c.constraint_type = 'P'
         )
loop
     v_val := col_value(r.column_name, r.data_type);
     v_txt := v_txt || r.column_name ||  ' = '' || ' || v_val || ' || '' and ';
end loop;
v_txt := substr(v_txt,1,length(v_txt)-5);

v_txt := v_txt || ''' as update_where_cl ';

v_txt := v_txt || ' FROM ' || v_table  ;






   if  p_where is not null then
    v_txt := v_txt || ' where ' || p_where;
   end if;


   v_txt := v_txt||chr(13)||chr(10);

   if v_order_by is null then
       for r in (
                  select cc.COLUMN_NAME  --PKEY columns
                  from sys.all_constraints c
                    join sys.ALL_CONS_COLUMNS cc on c.constraint_name = cc.constraint_name
                  where c.table_name = p_table_name and c.constraint_type = 'P'
                 )
       loop
          v_order_by := v_order_by ||r.column_name ||  ',';
       end loop;

       if  not v_order_by is null then
            v_order_by := substr(v_order_by,1,length(v_order_by)-1);
       end if;
   end if;

   if  not v_order_by is null then
      v_txt := v_txt || ' ORDER BY ' || v_order_by;
   end if;

--DBMS_OUTPUT.PUT_LINE(v_txt);
   execute immediate 'create or replace view PPV_TMP as ' || v_txt;






   --generowanie skryptu z insertami
   dmp('------------------------------------------------------------------------------------------');
   dmp('--by gen_dml_table_inserts ('''||p_owner||''','''||p_table_name||''','''||replace (p_where,chr(10),chr(10)||'--')||''')...' );
   dmp('------------------------------------------------------------------------------------------');
   dmp( 'alter session set nls_date_format=''YYYY-MM-DD HH24:MI:SS'';' );
   dmp('------------------------------------------------------------------------------------------');
   --dmp('declare');
   --dmp(' v_rowid varchar2(24);');
   --dmp('begin');
  -- dmp('  null;');
   --dmp('------------------------------------------------------------------------------------------');
   FOR r in (select rowid, v1.* from PPV_TMP v1)  LOOP

      --INSERT....
      dmp(r.cmd);

      --update'y z czesciami clob'ów (chunki po 2Kb):
      for rc in ( select * from sys.all_tab_columns where data_type = 'CLOB' and owner = p_owner and table_name = p_table_name order by column_id  ) loop
        --dmp('--CLOB NAME: '||rc.column_name);
        execute immediate ' select ' || rc.column_name || ' from ' || v_table || ' where rowid ='''||r.rowid ||'''' INTO v_clob;
        if not v_clob is null then
        for rchnk in (
            select
               substr(v_clob, (rownum-1)*2000+1, 2000) chunk, rownum rnum
            FROM (select rownum as lp from sys.all_tab_columns where rownum<1000)
            where lp <=ceil(length(v_clob)/2000)
        ) loop
            dmp('-- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -');
            dmp('UPDATE '||v_table
            ||' SET '||rc.column_name||' = '|| case when rchnk.rnum>1 then rc.column_name else '''''' end
            ||' ||
'''
            || replace (rchnk.chunk,'''','''''')
            || ''' '
            || '
WHERE ' || r.UPDATE_WHERE_CL || ';');
        end loop;
        end if;
      end loop;
      dmp('------------------------------------------------------------------------------------------');
   END LOOP;


   return v_ret;
end;
/
