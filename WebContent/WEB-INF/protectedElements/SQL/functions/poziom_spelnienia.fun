CREATE OR REPLACE FUNCTION PPADM.POZIOM_SPELNIENIA (P_OCE_ID IN NUMBER) RETURN VARCHAR2 AS 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 v_poziom VARCHAR2(200);
 v_srednia NUMBER;
 CURSOR oceny IS
    SELECT 
     O.WSL_WARTOSC_MAX,
     O.WSL_ALIAS,
     O.WSL_OPIS,
     O.WSL_WARTOSC
    FROM ppt_adm_wartosci_slownikow O 
    WHERE O.WSL_SL_NAZWA = 'OCE_OCENY_CZASTKOWE'
    ORDER BY O.WSL_WARTOSC_MAX;
BEGIN
  v_poziom := '-/-';
  
  v_srednia := srednia_ocen(P_OCE_ID);
    
  IF v_srednia IS NOT NULL
  THEN
    FOR i IN oceny
    LOOP
      IF v_srednia <= i.WSL_WARTOSC
      THEN
        EXIT;
      ELSE
        v_poziom := i.WSL_OPIS;
      END IF;   
    END LOOP;
  END IF;
  
  RETURN v_poziom;
END;
/
