create or replace function ppadm.ppf_zalogowany_etykieta
	return varchar2
is
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
--------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
	v_etykieta varchar2(512);
begin

    for r in (
        ---------------------------------------------------------------------------------------------------------------------------------------------------------
        -- zalogowany/umowy - najpierw aktualna, potem malejaco zat_data_zmiany 
        ---------------------------------------------------------------------------------------------------------------------------------------------------------   
        select --u.*, z.*, s.*, o.*,
            uzt_nazwa, prc_imie, prc_nazwisko, stn_nazwa, ob_pelny_kod, zat_typ_umowy,  zat_data_zmiany, z.zat_data_do,
            case when sysdate between zat_data_zmiany and coalesce(zat_data_do, sysdate) then 0 else 1 end h --kolejnosc - najpierw aktualna umowa, potem starsze
        from ppv_zalogowany u
          left join egadm1.ek_zatrudnienie z on z.zat_prc_id = u.prc_id and zat_typ_umowy in (0,1,2,3) and z.zat_data_zmiany <= sysdate
          left join egadm1.zpt_stanowiska s on s.stn_id = z.zat_stn_id
          left join egadm1.css_obiekty_w_przedsieb o on o.ob_id = z.zat_ob_id
        order by h, zat_data_zmiany desc    
        ---------------------------------------------------------------------------------------------------------------------------------------------------------
    ) loop

        return -->Jan Kowalski (ADMINISTRATOR_PORTAL) - Inspektor - MIGR_EK_2
            r.prc_imie || ' ' || r.prc_nazwisko     -- Jan Kowalski
            || ' (' || r.uzt_nazwa || ')'           -- (ADMINISTRATOR_PORTAL)
            || ' - ' || r.stn_nazwa                 -- - Inspektor
            || ' - ' || r.ob_pelny_kod              -- - MIGR_EK_2
        ;

    end loop;

end ppf_zalogowany_etykieta;
/
