CREATE OR REPLACE FUNCTION PPADM.KWOTA_SLOWNIE (suma NUMBER) return varchar2 as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	v_reszta NUMBER := 0;
BEGIN
	v_reszta:=suma-floor(suma);
	return egadm1.ek_pck_finanse.slownie(floor(suma))||' ('||floor(v_reszta*100)||'/100)';
	
	EXCEPTION
	WHEN OTHERS THEN
		return null;	
END;
/
