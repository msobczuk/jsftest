CREATE OR REPLACE FUNCTION PPADM.GET_MIEJSCA (pWND_ID  in  number)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
  RETURN VARCHAR2
IS
  return_text  VARCHAR2(10000) := NULL;
BEGIN
  FOR x IN (select 
			   kraj.KR_NAZWA||'/'||cel.CDEL_MIEJSCOWOSC miejsca
			from 
				PPT_DEL_CELE_DELEGACJI cel
				join CSS_KRAJE kraj on cel.CDEL_KR_ID = kraj.KR_ID
			where cel.CDEL_WND_ID=pWND_ID
			) LOOP
    return_text := return_text || x.miejsca || ' ' ;
  END LOOP;
  RETURN return_text;
  
  EXCEPTION
	WHEN OTHERS THEN
		return null;
END;
/
