create or replace FUNCTION ppadm.FAKTYCZNY_TERMIN_ROZ_DEL_ZAGR(P_WND_ID NUMBER) 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--"Faktyczny termin rozliczenia" nie chcemy aby tu pokazywa�a si� data zaksi�gowania noty rozliczeniowej, tu zale�y nam aby by�o widoczne kiedy to pracownik dope�ni� wszystkich formalno�ci i z�o�y� kompletne rozliczenie wraz z za��cznikami. I tak: 
--1) je�li z rozliczenia wynika, �e np. pracownik odebra� zaliczk� w kwocie 100 EUR i to 100 EUR wydatkowa� to w momencie kiedy dyrektor zaakceptuje mu rozliczenie i zostanie przeze mnie zweryfikowane i potwierdz� poprawno�� i kompletno�� tych dokument�w i rozliczenia to termin faktyczny rozliczenia b�dzie dzie� kiedy dyrektor zaakceptowa� rozliczenie pracownikowi. 
--2) je�li pracownik otrzyma� 100 EUR zaliczki a wydatkowa� 80 EUR to r�nica w wysoko�ci 20 EUR pozostaje do zwrotu w kasie b�d� na konto MF i w tym przypadku termin faktyczny rozliczenia b�dzie to dzie� kiedy pracownik wp�aci w kasie to 20 EUR lub wp�yw �rodk�w na konto w MF. 
--3) Pracownik otrzyma� zaliczk� 100 EUR ale wydatkowa� 150 EUR czyli 50 EUR musimy mu "dobra�" dodatkowo w takiej sytuacji faktyczny termin rozliczenia to jest tak jak w pkt.1 po weryfikacji rozliczenia faktycznym terminem b�dzie data kiedy dyrektor zatwierdzi� rozliczenie.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RETURN DATE AS

BEGIN

  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  --warunki brzegowe - czy wszystkie DKZ z rozliczenia oraz czy ew. wplaty sa zrealizowane
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  for r in ( 
                SELECT * 
                FROM PPT_DEL_KALKULACJE KAL
                    join PPT_DEL_ZALICZKI DKZ on DKZ.DKZ_KAL_ID = KAL.KAL_ID and KAL.KAL_RODZAJ = 'ROZ'
                    left join EGADM1.KGT_DOKUMENTY DOK on DOK.DOK_ID = DKZ.DKZ_DOK_ID --and dok.dok_f_anulowany <> 'T'
                where kal_wnd_id = p_wnd_id
                    and nvl(dkz.dkz_kwota,0) <> 0 
                    and (  
                            dkz.dkz_dok_id is null -- niezerowy DKZ ale nie wygenerowano dok. w NZ - nie uznajemy rozliczenia
                            or (dkz.dkz_kwota < 0 and dok_dok_id_zap is null)  -- oczekiwana wplata od pracownika, poki nie zostanie zrealizwoana nie uznajemy rozliczenia
                        )
  )  loop 

    return null; --co najmniej jeden DKZ na ROZ jest jeszcze nierozliczony / niewygenerowano NZ

  end loop;




  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  --2) je�li pracownik otrzyma� 100 EUR zaliczki a wydatkowa� 80 EUR to r�nica w wysoko�ci 20 EUR pozostaje do zwrotu w kasie b�d� na konto MF i w tym przypadku termin faktyczny rozliczenia b�dzie to dzie� kiedy pracownik wp�aci w kasie to 20 EUR lub wp�yw �rodk�w na konto w MF. 
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  for r in (
   --dokumenty DKZ/KP w PP lj NZ, ktore s� do zwrotu przez pracownika
  SELECT * FROM PPT_DEL_KALKULACJE KAL
    join PPT_DEL_ZALICZKI DKZ on DKZ.DKZ_KAL_ID = KAL.KAL_ID and KAL.KAL_RODZAJ = 'ROZ'
    left join EGADM1.KGT_DOKUMENTY DOK on DOK.DOK_ID = DKZ.DKZ_DOK_ID 
        --and not DOK_DOK_ID_ZAP is null 
       -- and not dok.DOK_DATA_OPERACJI is null 
  WHERE KAL.KAL_WND_ID = P_WND_ID
    and DKZ.DKZ_KWOTA < 0 --tylko to co do zwrotu przez pracownika  (czyli tylko KP)
  order by DOK_DATA_OPERACJI DESC NULLS FIRST --sortowanie malejace przy zalazeniu, ze null jest najwiekszy

  ) loop

    if r.dok_dok_id_zap is null then
      return null; --dok wplaty (KP) jest utworzony w NZ, ale jeszcze nie wplacony
    end if;

    return r.DOK_DATA_OPERACJI; --dok wplaty (KP) zrealizowano jako wplate w NZ

  end loop;
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------







  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  --1) je�li z rozliczenia wynika, �e np. pracownik odebra� zaliczk� w kwocie 100 EUR i to 100 EUR wydatkowa� to w momencie kiedy dyrektor zaakceptuje mu rozliczenie i zostanie przeze mnie zweryfikowane i potwierdz� poprawno�� i kompletno�� tych dokument�w i rozliczenia to termin faktyczny rozliczenia b�dzie dzie� kiedy dyrektor zaakceptowa� rozliczenie pracownikowi. 
  --3) Pracownik otrzyma� zaliczk� 100 EUR ale wydatkowa� 150 EUR czyli 50 EUR musimy mu "dobra�" dodatkowo w takiej sytuacji faktyczny termin rozliczenia to jest tak jak w pkt.1 po weryfikacji rozliczenia faktycznym terminem b�dzie data kiedy dyrektor zatwierdzi� rozliczenie.
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  for r in (

    select * from PPV_HISTORIA_OBIEGOW_LAST
    where HIO_KOB_KOD = 'WN_DELZv2' and HIO_KLUCZ_OBCY_ID = P_WND_ID
    and (cob_nazwa like 'Przekazanie rozliczenia do zatwierdzenia przez deleguj�cego' or ob_nazwa like 'Zatwierdzenie �rodk�w wynikaj�cych z rozliczenia do wyp�aty przez dyrekcje BDG')
    --TODO nazwy na #hashtagi - skorzystac z COB_NAZWA2
    order by hio_data DESC

  ) loop

    return r.hio_data; --jesli jest okreslona czynnosc

  end loop;
  ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------





  -- brak 1) 2) 3) -> null  
  return null; --jesli nie ma ani wplat przed i w NZ, ani czynnosci w historii obiegu

END;
/
