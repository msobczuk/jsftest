CREATE OR REPLACE FUNCTION PPADM.OCENA_WYNIKOWA (P_OCE_ID IN NUMBER) RETURN VARCHAR2 AS 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 v_wynik VARCHAR2(40);
 v_srednia NUMBER;
 v_srednia_wymagana NUMBER;
 v_ile_znacznie_ponizej NUMBER;
 v_najnizsza_ocena NUMBER;
BEGIN
  v_wynik := '-/-';
  
  v_srednia := srednia_ocen(P_OCE_ID);
  
  IF v_srednia IS NOT NULL
  THEN
    SELECT ROUND(AVG(WSL_WARTOSC_MAX), 2) - 1
      INTO v_srednia_wymagana
      FROM ppt_adm_wartosci_slownikow
     WHERE WSL_SL_NAZWA = 'OCE_OCENY_CZASTKOWE';
  
    SELECT min(to_number(WSL_WARTOSC_MAX))
      INTO v_najnizsza_ocena
      FROM ppt_adm_wartosci_slownikow
     WHERE WSL_SL_NAZWA = 'OCE_OCENY_CZASTKOWE'
       --AND ROWNUM = 1
  ORDER BY WSL_WARTOSC_MAX;
  
    SELECT count(*)
      INTO v_ile_znacznie_ponizej
      FROM ppt_oce_kryteria
     WHERE KRYT_OCE_ID = P_OCE_ID
       AND KRYT_OCENA = v_najnizsza_ocena;
  
    IF v_srednia_wymagana >= v_srednia OR v_ile_znacznie_ponizej > 0
    THEN
      v_wynik := 'NEGATYWNA';
    ELSE
      v_wynik := 'POZYTYWNA';
    END IF;
  END IF;
  
  RETURN v_wynik;
END;
/
