create or replace FUNCTION ppadm.status_rozl_delegacji_w_fk (
   p_wnd_id        IN NUMBER,
   p_co_zwracamy   IN VARCHAR2 DEFAULT 'S')
   RETURN VARCHAR2
IS
   c_procedura   CONSTANT VARCHAR2 (100) := 'status_rozl_delegacji_w_fk';
   v_komunikat            VARCHAR2 (4000);
   v_komunikat_linia      VARCHAR2 (4000);
   v_ile                  NUMBER := 0;
BEGIN
   FOR r
      IN (SELECT wnd_id,
                 wnd_dok_id,
                 wnd_numer,
                 wnd_rodzaj,
                 wnd_prc_id,
                 kal_rodzaj,
                 CASE kal_rodzaj
                    WHEN 'WST' THEN 'zaliczki'
                    WHEN 'ROZ' THEN 'rozliczenia zaliczki'
                 END
                    etap,
                 dkz_wal_id,
                 dkz_kwota,
                 wal_symbol,
                 dkz_kurs,
                 dkz_data_wyp,
                 dkz_forma_wyplaty,
                 dkz_dok_id,
                 dkz_zal_wydano,
                 dkz_czy_rownowartosc,
                 dok_numer_wlasny,
                 dok_rdok_kod,
                 CASE
                    WHEN dok_rdok_kdok_kod IN ('KW', 'KP') THEN 'w banku'
                    ELSE 'w kasie'
                 END
                    bank_kasa,
                 dok_def_0,
                 dok_def_2,
                 dok_kwota,
                 dok_dok_id_zap,
                 dok_f_zatwierdzony,
                 dok_f_anulowany
            FROM ppt_del_wnioski_delegacji,
                 ppt_del_kalkulacje,
                 ppt_del_zaliczki,
                 kgt_dokumenty,
                 css_waluty
           WHERE     1 = 1
                 AND dkz_kal_id = kal_id
                 AND KAL_WND_ID = wnd_id
                 AND DKZ_DOK_ID = dok_id
                 AND dkz_wal_id = wal_id
                 AND wnd_id = p_wnd_id)
   LOOP
      v_ile := v_ile + 1;

      IF r.dok_dok_id_zap IS NULL
      THEN
         IF p_co_zwracamy = 'S'
         THEN
            RETURN 'N';
         ELSE
            v_komunikat_linia :=
                  r.wnd_numer
               || ': dokument w FK z tyt. '
               || r.etap
               || ' rodzaj: '
               || r.dok_rdok_kod
               || ' nr wew.: '
               || r.dkz_dok_id
               || ' na kwot�: '
               || TO_CHAR (r.dok_kwota,
                           'FM9G999G999G999G999G990D00',
                           'NLS_NUMERIC_CHARACTERS='',.''')
               || ' '
               || r.wal_symbol
               || ' rodzaj operacji: '
               || r.dok_def_0
               || ' nie zosta� rozliczony w '
               || r.bank_kasa;

            IF v_komunikat IS NULL
            THEN
               v_komunikat := v_komunikat_linia;
            ELSE
               v_komunikat := v_komunikat || CHR (10) || v_komunikat_linia;
            END IF;
         END IF;
      END IF;
   END LOOP;

   IF v_ile = 0
   THEN
      RETURN 'N';
   ELSE
      IF p_co_zwracamy = 'S'
      THEN
         RETURN 'T';
      ELSE
         RETURN NVL (
                   v_komunikat,
                      'Wniosek: '
                   || p_wnd_id
                   || ': nie ma zaliczek lub nie s� wystawione dokumenty do FK');
      END IF;
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      RETURN NULL;
END;
/
