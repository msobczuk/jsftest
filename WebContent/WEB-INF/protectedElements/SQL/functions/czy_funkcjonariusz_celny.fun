create or replace FUNCTION PPADM.CZY_FUNKCJONARIUSZ_CELNY(p_PRC_ID NUMBER, p_na_dzien DATE DEFAULT SYSDATE) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 
 v_zbior_kodow VARCHAR2(150);
  
BEGIN

	SELECT 
		LISTAGG(GP.GP_KOD, ' ') WITHIN GROUP (ORDER BY GP.GP_KOD)
		INTO v_zbior_kodow 
	FROM EGADM1.EK_PRACOWNICY PRC
	join EGADM1.EK_ZATRUDNIENIE ZAT on ZAT.ZAT_PRC_ID=PRC.PRC_ID
	left join EGADM1.EKT_GRUPY_PRACOWNICZE GP on ZAT.ZAT_GP_ID=GP.GP_ID
	Where PRC.PRC_ID = p_PRC_ID
		AND    zat_data_zmiany <=   trunc(p_na_dzien)
		AND                         trunc(p_na_dzien)  <=  nvl(zat_data_do, trunc(SYSDATE+(365*1000)) )
	group by PRC.PRC_ID
	;
	
	IF v_zbior_kodow like '%08F%' OR v_zbior_kodow like '%08A%' THEN
		return 1;
	ELSE
		return 0;
	END IF;
	
	
	

EXCEPTION
	WHEN OTHERS THEN
		return -1;
  
END;
/
