CREATE OR REPLACE FUNCTION PPADM.DEL_NUMER(P_DATA_WNIOSKU DATE, P_RODZAJ NUMBER) RETURN NUMBER 
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/functions/eg.ppf_del_number.fun 4 1.0.4 20.03.18 09:11 MGOLDA                                        $
-- $Revision:: 4                                                                                                                                              $
-- $Workfile:: eg.ppf_del_number.fun                                                                                                                          $
-- $Modtime:: 20.03.18 09:11                                                                                                                                  $
-- $Author:: MGOLDA                                                                                                                                           $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
AS
 PRAGMA AUTONOMOUS_TRANSACTION;
 
 v_nr NUMBER :=1;
BEGIN
  
  select NVL( max(WND_NUMER_NR),0) into v_nr 
  FROM PPT_DEL_WNIOSKI_DELEGACJI 
  WHERE EXTRACT(YEAR FROM WND_DATA_WNIOSKU) = EXTRACT(YEAR FROM P_DATA_WNIOSKU)  
    AND WND_RODZAJ = P_RODZAJ;

  RETURN v_nr+1;
  --RETURN 'Z/' || substr('0000000000'||v_nr,-4)  || '/'||P_ROK;
END;
/
