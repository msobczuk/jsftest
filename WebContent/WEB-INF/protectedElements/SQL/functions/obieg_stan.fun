CREATE OR REPLACE FUNCTION PPADM.OBIEG_STAN (P_ID NUMBER, P_KATEGORIA VARCHAR2) return VARCHAR2 as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  v_ret varchar2(250);
BEGIN
  select dstn.dstn_opis into v_ret 
  from egadm1.csst_encje_w_stanach ews
  join csst_def_stanow dstn on ews_dstn_id = dstn_id
  where ews.EWS_KOB_KOD = P_KATEGORIA
    and ews.EWS_KLUCZ_OBCY_ID = p_id;
  return v_ret;

  exception
   when NO_DATA_FOUND then
    return '';
END;
/
