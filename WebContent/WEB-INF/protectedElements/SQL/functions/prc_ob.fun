CREATE OR REPLACE FUNCTION "PPADM"."PRC_OB" (p_prc_id number, p_data_zat date default sysdate) return number as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#fix_spec_web/001/001.PPv1.4.0/install/obj/functions/eg.ppf_prc_ob.fun 1 1.0.1 25.04.18 14:23 MGOLDA                                                                                $
-- $Revision:: 1                                                                                                                                                                                                 $
-- $Workfile:: eg.ppf_prc_ob.fun                                                                                                                                                                                 $
-- $Modtime:: 25.04.18 14:23                                                                                                                                                                                     $
-- $Author:: MGOLDA                                                                                                                                                                                              $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	v_ret NUMBER;
BEGIN

	SELECT OB.OB_ID into v_ret 
	FROM ek_zatrudnienie ZAT
	LEFT JOIN egadm1.EK_OBIEKTY_W_PRZEDSIEB OB on nvl(ZAT.ZAT_OB_ID_WYDZIAL, ZAT.ZAT_OB_ID)=OB.OB_ID
	WHERE ZAT.ZAT_PRC_ID = p_prc_id 
      and trunc(p_data_zat) between zat_data_zmiany and nvl(zat_data_do, p_data_zat);

	return v_ret;

EXCEPTION
  WHEN OTHERS THEN
    return null;	
END;
/
