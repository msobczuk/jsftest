CREATE OR REPLACE FUNCTION PPADM.PRZELOZONY (P_PRC_ID NUMBER, P_DATA DATE default sysdate) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN

 for r in (
    select  spod_prc_id_prz
    from PPV_STRUKTURA_PODLEGLOSCI
    where spod_prc_id = P_PRC_ID
    order by spod_f_zastepstwo_prc, spod_f_zastepstwo_prz, spod_sto_id_prc, spod_sto_id_prz
  ) loop
        return r.spod_prc_id_prz; -- jest przelozony - zwr. pierwszy prc_id wg order by
  end loop;

  return null; --nie ma zadnego przelozonego

END;
/
