CREATE OR REPLACE FUNCTION PPADM.GET_NORMA ( P_PRC_ID NUMBER, P_DATA_OD DATE) RETURN NUMBER AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 v_nr NUMBER :=0;
 
 v_od date := p_data_od;

BEGIN
  if (v_od = null) then
   v_od :=  sysdate + 10000;
  end if;



SELECT godzin into v_nr
FROM
	(SELECT  
	  sp.sp_d_czas_wartosc godzin          
	  FROM ek_zatrudnienie zat
	  join ek_plany_pracy pp on zat.ZAT_PP_ID=pp.PP_ID
	  join ek_systemy_pracy sp on sp.sp_id=pp.PP_SP_ID
	  WHERE zat_typ_umowy = 0
	  and zat.zat_prc_id= P_PRC_ID
	  AND  EXTRACT(year FROM zat_data_zmiany) <=   EXTRACT(year FROM v_od)
	  AND                                          EXTRACT(year FROM v_od) <=  EXTRACT(year FROM nvl(zat_data_do, trunc(SYSDATE+(365*1000)) ) ) 
	  ORDER BY zat_data_do desc) NORMA
WHERE ROWNUM=1
;
  
  IF v_nr>0 THEN
    RETURN v_nr;
  ELSE
    RETURN 10000;
  END IF;

  EXCEPTION
	WHEN OTHERS THEN
    plog('GET_NORMA P_PRC_ID=' || P_PRC_ID|| ' P_DATA_OD='|| P_DATA_OD ||' SQLERRM: ' || SQLERRM() || ' ; SQLCODE ' || SQLCODE() );
		return 10000;

END;
/
