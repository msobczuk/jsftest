create or replace function ppadm.dmp_all_source(p_owner varchar2, p_object_name varchar2, p_object_type varchar2) return clob 
as
    v_clob clob := '';
begin 

    for r in (
                select * 
                from sys.all_source 
                where owner = p_owner 
                    and name = p_object_name 
                    and type = p_object_type
                order by line    
            ) loop

        v_clob := v_clob || r.TEXT;

    end loop;

    --dbms_output.put_line(v_clob);
    --v_clob := rtrim(v_clob) || chr(13)||chr(10)||'/'||chr(13)||chr(10);
    return v_clob;
end;
/
