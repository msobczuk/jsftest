create or replace function ppadm.zfss_poswiadczenie(p_prc_id NUMBER) return varchar2 
as
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                      																													 $	
-- $Revision::                                                                                                                                               		 $
-- $Workfile::                                                                                                                     									 $
-- $Modtime::                                                                                                                                   					 $
-- $Author:: 																																						 $
--------------------------------------------------------------------------------------------------------------------------------------------------------------------- 

    v_ret varchar2(4000) := null;
	v_imie_nazwisko varchar2(4000) := null;
	v_zat_data_zmiany varchar2(4000) := null;
	v_zat_data_do varchar2(4000) := null;
	v_zatrudniony_na_czas varchar2(4000) := null;
	v_ZAT_WYMIAR varchar2(4000) := null;
	v_wymiar_etatu varchar2(4000) := null;
	v_ZAT_WYMIAR_LICZNIK varchar2(4000) := null;
	v_ZAT_WYMIAR_MIANOWNIK varchar2(4000) := null;
	v_zat_rodzaj varchar2(4000) := null;
begin 

SELECT 
  prc.prc_imie ||' '|| PRC.PRC_NAZWISKO imie_nazwisko,
  to_char(z.zat_data_zmiany,'yyyy-mm-dd') zat_data_zmiany,
  to_char(z.zat_data_do,'yyyy-mm-dd') zat_data_do,
  trunc(MONTHS_BETWEEN(z.zat_data_do, z.zat_data_zmiany))||' miesi�cy' zatrudniony_na_czas,
  z.ZAT_WYMIAR,
  lower(wslwymiar.wsl_opis) wymiar_etatu,
  z.ZAT_WYMIAR_LICZNIK,
  z.ZAT_WYMIAR_MIANOWNIK,
  lower(zat_rodzaj.wsl_alias) zat_rodzaj
  into v_imie_nazwisko, v_zat_data_zmiany, v_zat_data_do, v_zatrudniony_na_czas, v_ZAT_WYMIAR, v_wymiar_etatu, v_ZAT_WYMIAR_LICZNIK, v_ZAT_WYMIAR_MIANOWNIK, v_zat_rodzaj 
FROM EGADM1.EK_PRACOWNICY PRC 
left join ppadm.EK_ZATRUDNIENIE Z on prc_id = zat_prc_id and sysdate between zat_data_zmiany and nvl(zat_data_do, sysdate) and z.zat_typ_umowy = 0
left join EGADM1.CSS_WARTOSCI_SLOWNIKOW wslwymiar on wslwymiar.WSL_SL_NAZWA = 'TYP ETATU' and wslwymiar.wsl_wartosc = z.ZAT_WYMIAR
left join EGADM1.CSS_WARTOSCI_SLOWNIKOW zat_rodzaj on zat_rodzaj.WSL_SL_NAZWA = 'EK_RODZAJ_ZAT' and zat_rodzaj.wsl_wartosc = z.ZAT_F_RODZAJ
WHERE 1=1
  and prc.prc_ID = p_prc_id 
  and rownum = 1;



	v_ret := 'Niniejszym potwierdzam, �e wnioskodawca ' ||
			nvl(v_imie_nazwisko, '') ||
			' jest zatrudniony w PWSZ w Tarnowie od dnia ' ||
			nvl(v_zat_data_zmiany,'') || ' na ' ||
			nvl(v_zat_rodzaj, '') ||
			' w wymiarze ' ||
			nvl(v_wymiar_etatu, '') ||
			' i nie znajduje si� w okresie wypowiedzenia, a tak�e w/w por�czyciele s� pracownikami PWSZ zatrudnionymi na czas nieokre�lony.';
			
			

    return v_ret;
exception 
when no_data_found then
    return null;
when others then
    return '-->ERR/sqlcode: '||sqlcode || '; sqlerrm: '|| sqlerrm;
end;
/
