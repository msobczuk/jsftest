create or replace function ppadm.st_inwentaryzacja_wstaw(P_DATA DATE default sysdate, p_odz_id NUMBER DEFAULT null) return NUMBER as 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- grant execute on egadm1.STP_OBJ_I to ppadm;
    v_i_rowid ROWID;
    r_i STP_OBJ_I.T_I;
    v_ret NUMBER; 
begin 

    r_i.i_typ := '1';
    r_i.i_status := 'E';
    r_i.i_rodzaj := 'DEF'; --'UPR'
    r_i.i_data := P_DATA;
    r_i.i_f_blokada := 'N';
    r_i.i_odz_id := NVL(p_odz_id, EAADM.eap_globals.odczytaj_oddzial);

    v_i_rowid := STP_OBJ_I.WSTAW(r_i);

    select I_ID into v_ret from stt_inwentaryzacje where rowid = v_i_rowid;

    return v_ret;

end;
/



