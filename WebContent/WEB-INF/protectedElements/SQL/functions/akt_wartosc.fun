create or replace function ppadm.akt_wartosc(p_pole varchar2, p_tabela varchar2, p_where varchar2, p_typ varchar2, p_prc_id NUMBER, p_inf_id VARCHAR2) return varchar2 
as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

    v_ret varchar2(4000) := null;
    v_sql varchar2(4000) := null;
    v_where varchar2(4000) := p_where;
begin 
    if p_typ = 'SL' then 
        return 'SL '
        ||'; p_pole:'||p_pole
        ||'; p_tabela:'||p_tabela
        ||'; p_where:'||p_where;
    end if;

    v_where := replace (v_where, ':P_PRC_ID', p_prc_id);
    v_where := replace (v_where, ':P_INF_ID', ''''||p_inf_id||'''');
    
    
    
    v_sql :=  ' select ''''||' || p_pole || ' from ' || p_tabela || ' ' || v_where;
    execute immediate 'alter session set nls_date_format = ''YYYY-MM-DD HH24:MI:SS''';
    execute immediate v_sql into v_ret;
    return v_ret;
exception 
when no_data_found then
    return null;
when others then
    return v_sql||'-->ERR/sqlcode: '||sqlcode || '; sqlerrm: '|| sqlerrm;
end;
/
