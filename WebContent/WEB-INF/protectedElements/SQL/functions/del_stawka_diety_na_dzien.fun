CREATE OR REPLACE FUNCTION PPADM.DEL_STAWKA_DIETY_NA_DZIEN (p_std_id number, p_typ_diety varchar2, p_kr_id number, p_data date default sysdate) return number as 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
v_ret number :=0.00;
begin
       select  wst_id into v_ret from (
        select *
        from ppadm.ppt_del_wartosci_stawek 
        where wst_obowiazuje_od <= p_data --sysdate
            and wst_std_id = p_std_id --11 lub 12 
            and wst_typ_diety=p_typ_diety -- 'H' lub 'P' 
            and (wst_kr_id = p_kr_id or wst_kr_id is null)
        order by nvl(wst_kr_id,9999999), wst_obowiazuje_od desc
       ) x where rownum=1;        
        
    return v_ret;
end;
/
