CREATE OR REPLACE FUNCTION PPADM.NUMER_OBLICZ(
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
Funkcja do obliczania numer�w dla tablic rejestrowanych dla palikacji PP i schematu PPADM (do wykorzystania przez triggery before insert)
Pozwala nadawa� numery wg usatwie� w CSS->Serwis->Wzorce numeracji dla tabel
Uwaga - jesli zostanie zedfiniowany s�ownik  CSS_WZORCE_NUM_<NAZWA_TABELI> (np. CSS_WZORCE_NUM_PPT_TEST_NR) nale�y podawa� parametr p_wznu_rodzaj.

Przykladowy spos�b u�ycia z triggera before insert:

CREATE OR REPLACE TRIGGER PPADM.PPG_PPNR_BIR_NUMER
  BEFORE INSERT ON PPADM.ppt_test_nr FOR EACH ROW
  DECLARE
BEGIN
    --:NEW.PPNR_NUMER :=  PPADM.NUMER_OBLICZ('PPT_TEST_NR'); -- je�li nie ma s�ownika CSS_WZORCE_NUM_PPT_TEST_NR (jeden wzorzec dla tablicy)
    :NEW.PPNR_NUMER :=  PPADM.NUMER_OBLICZ('PPT_TEST_NR', :NEW.PPNR_RODZAJ); -- je�li jest s�ownik CSS_WZORCE_NUM_PPT_TEST_NR (stosujemy r�ne wzorce wg csst_wz_num.wznu_rodzaj)
END;
*/
        p_tablica       IN VARCHAR2
       ,p_id            IN NUMBER   DEFAULT NULL
       ,p_wznu_rodzaj   IN VARCHAR2 DEFAULT NULL
       ,p_p1            IN VARCHAR2 DEFAULT NULL
       ,p_p2            IN VARCHAR2 DEFAULT NULL
       ,p_p3            IN VARCHAR2 DEFAULT NULL
       ,p_p4            IN VARCHAR2 DEFAULT NULL
       ,p_p5            IN VARCHAR2 DEFAULT NULL
       ,p_p6            IN VARCHAR2 DEFAULT NULL
       ,p_p7            IN VARCHAR2 DEFAULT NULL
       ,p_p8            IN VARCHAR2 DEFAULT NULL
       ,p_p9            IN VARCHAR2 DEFAULT NULL
       ) return varchar2
IS
PRAGMA AUTONOMOUS_TRANSACTION;
    v_wznu_rec cssp_frm_wz.t_wznu;
    v_numer_sort VARCHAR2(100);
    v_wartosc varchar2(255);
BEGIN

    for rwnzu in (
            select *
            from csst_wz_num wznu
                join eat_tablice tbl on wznu_tbl_id = tbl_id
            where tbl_nazwa = upper(p_tablica)
                and nvl(wznu_rodzaj,' ') = nvl(p_wznu_rodzaj,' ')
    ) loop

        --pobierz_wzorzec(p_tablica, v_rec, p_rodzaj_wzorca);
        select
            rowid
            ,wznu_id
            ,wznu_tbl_id
            ,wznu_wzks_id
            ,wznu_wzorzec
            ,wznu_status
            ,wznu_kolumna
            ,wznu_kolumna_sort
            ,wznu_sql_unikalnosc
            ,wznu_dlugosc
            ,wznu_nazwa
            ,wznu_f_hist_numerow
            ,wznu_rodzaj
        into v_wznu_rec
        from csst_wz_num
        where wznu_id = rwnzu.wznu_id;

        cssp_frm_wz.wznu_oblicz(
              v_wznu_rec
             ,p_id => p_id
             ,p_p1 => p_p1
             ,p_p2 => p_p2
             ,p_p3 => p_p3
             ,p_p4 => p_p4
             ,p_p5 => p_p5
             ,p_p6 => p_p6
             ,p_p7 => p_p7
             ,p_p8 => p_p8
             ,p_p9 => p_p9
             ,p_numer          => v_wartosc -- out numer
             ,p_numer_sort 	   => v_numer_sort --out / sort
             ,p_f_test_wzorca  => 'N' -- modyfikacja wzorca
             ,p_f_czekaj       => 'T'
             ,p_f_modyfikuj    => 'N'
		);
		
		COMMIT;
        return trim(v_wartosc);

    end loop;
    
	COMMIT;
    return null; --brak wzorca

END;
/
