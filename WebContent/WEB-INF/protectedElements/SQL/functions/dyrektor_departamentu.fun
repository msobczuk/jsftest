create or replace FUNCTION PPADM.DYREKTOR_DEPARTAMENTU(P_OB_ID NUMBER, P_DATA DATE default sysdate) 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
RETURN NUMBER AS
 v_ob_id_dep NUMBER;
 v_ret NUMBER;
BEGIN

    v_ob_id_dep := get_departament_id(P_OB_ID);

    ppp_global.ustaw_date_dla_struktury_po(P_DATA);

    select sto_prc_id into v_ret
    from EGADM1.zpt_st_w_jo stjo
        join EGADM1.zpt_stanowiska stn on stjo_stn_id = stn_id
        join EGADM1.zpt_st_w_jo_dane on stjo_id = std_stjo_id    and P_DATA between std_data_od and nvl(std_data_do, P_DATA)
        join EGADM1.zpt_stanowiska_opis on stjo_id = sto_stjo_id and P_DATA between sto_data_od and nvl(sto_data_do, P_DATA) 
            and not STO_F_ZASTEPSTWO='T' 
            and STO_F_UKONCZONY ='T'
    where lower(stn_nazwa) like 'dyrektor departamentu [%]'
        and stjo.stjo_ob_id = v_ob_id_dep
        and stjo.stjo_stan_definicji = 'Z'
        and rownum=1;

    return v_ret;

  EXCEPTION
	WHEN OTHERS THEN
		return null;

END;
/
