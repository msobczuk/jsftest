create or replace function ppadm.del_roz_trasa_miasta(p_wnd_id number) return varchar2 as 
v_ret varchar2(4000);
begin

    -- trasa wg rozlizczenia delegacji bez miasta wysjazdu (WARSZAWA)
    select LISTAGG(trs_miejscowosc_od, ', ') WITHIN GROUP (ORDER BY  lp) TRASA
    into v_ret
    from (
    select  
        kr_nazwa, trs.trs_miejscowosc_od, row_number() over (order by  trs_data_od, trs_id ) lp
    from ppt_del_trasy trs
        join ppt_del_kalkulacje on trs_kal_id=kal_id
        join ppt_del_wnioski_delegacji on kal_wnd_id = wnd_id
        join css_kraje on trs.trs_kr_id_od = kr_id
    where wnd_id = p_wnd_id --460
     and kal_rodzaj = 'ROZ'
    order by trs.trs_data_od, trs_id
    ) where lp > 1;

    return v_ret;

end;
/
