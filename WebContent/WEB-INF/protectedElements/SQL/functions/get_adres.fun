CREATE OR REPLACE FUNCTION PPADM.GET_ADRES 
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  RETURN VARCHAR2
IS
  return_text  VARCHAR2(1000) := NULL;
BEGIN

  return_text := 'http://localhost:8080/PortalPracowniczy/';
   
  RETURN return_text;
  
  EXCEPTION
	WHEN OTHERS THEN
		return null;
END;
/
