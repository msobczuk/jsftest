create or replace function ppadm.dmp_lw(p_lw_nazwa varchar2) return clob as
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                  $
-- $Revision::                                                                                                                                                $
-- $Workfile::                                                                                                                                                $
-- $Modtime::                                                                                                                                                 $
-- $Author::                                                                                                                                                  $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
    /*

    funkcja realizuje zrzut danych z definicji wybranej listy wartosci
    wynikiem jest skrypt dml, kt�ry w docelowej bazie kasuje i wgrywa na nowo definicje LW wskazana przez parametr p_lw_nazwa
    przykad wywolania:

    begin
        dbms_output.put_line(dmp_lw('PPL_PODWOJNE_ZATRUDNIENIE'));
    end;

    */

    v_clob clob := '';
    v_aa varchar2(1) := 'T';

begin

    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    v_clob := v_clob ||'-- DMP_LW: ' || UPPER(p_lw_nazwa) || CHR(13)||CHR(10);
    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    for r in (select OBJP_F_AUTO_AKT from PPADM.PPT_OBJECTS where OBJP_OBJECT_TYPE='LW' and objp_owner='PPADM' and OBJP_NAME = p_lw_nazwa) loop
        v_aa := r.OBJP_F_AUTO_AKT;
    end loop;
    v_clob := v_clob ||'update ppadm.ppt_objects set OBJP_F_AUTO_AKT='''||v_aa||''' ' || CHR(13)||CHR(10)
                     ||'where OBJP_OBJECT_TYPE=''LW'' and objp_owner=''PPADM'' and OBJP_NAME='''||p_lw_nazwa||''';' || CHR(13)||CHR(10);
    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    v_clob := v_clob || CHR(13)||CHR(10);
    v_clob := v_clob || 'DELETE FROM PPADM.PPT_ADM_LISTY_TABELE WHERE LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = '''||p_lw_nazwa||''' );' || CHR(13)||CHR(10);
    v_clob := v_clob || 'DELETE FROM PPADM.PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA = '''||p_lw_nazwa||''';' || CHR(13)||CHR(10);
    v_clob := v_clob ||'------------------------------------------------------------------------------------------'|| CHR(13)||CHR(10);
    v_clob := v_clob || CHR(13)||CHR(10);
    v_clob := v_clob || ppadm.dmp('PPADM', 'PPT_ADM_LISTY_WARTOSCI','LST_NAZWA in ('''||p_lw_nazwa||''')', 'LST_NAZWA, LST_LP');
    v_clob := v_clob || ppadm.dmp('PPADM', 'PPT_ADM_LISTY_TABELE', 'LSTB_LST_ID in ( SELECT LST_ID FROM PPT_ADM_LISTY_WARTOSCI WHERE LST_NAZWA in ( '''||p_lw_nazwa||''' ) ) ');

    --dbms_output.put_line(v_clob);
    return v_clob;
end;
/
