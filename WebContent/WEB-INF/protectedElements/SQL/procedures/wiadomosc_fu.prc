create or replace PROCEDURE PPADM.WIADOMOSC_FU
(
  P_FUNKCJA_UZYTKOWA IN VARCHAR2
, P_TRESC IN VARCHAR2
, P_TEMAT IN VARCHAR2 DEFAULT 'Wiadomo�� systemowa [PP]'
, P_WDM_DATA_WSTAWIENIA IN DATE default sysdate
, P_WDM_TYP IN VARCHAR2 DEFAULT 'E' --E=EGR; M=Mail
, P_WDM_ADRES_OD IN VARCHAR2 DEFAULT NULL
, P_WDM_ADRES IN VARCHAR2 DEFAULT NULL --jezeli nie podano, wez z UZT lub z PRC
, P_AUTONOMOUS_TRANSACTION VARCHAR2 DEFAULT 'N'
)
AS

   err_code VARCHAR2(50);
   err_msg VARCHAR2(300);

BEGIN

	IF(P_TRESC is null) THEN
        RETURN;
    END IF;


    FOR r in (
        select upr_uzt_nazwa--, fu_nazwa
        from eat_uprawnienia
         join eat_funkcje_uzytkowe on upr_fu_id = fu_id
        where fu_nazwa = P_FUNKCJA_UZYTKOWA
    )LOOP
        PPADM.WIADOMOSC_UZT(r.upr_uzt_nazwa, P_TRESC, P_TEMAT, P_WDM_DATA_WSTAWIENIA, P_WDM_TYP, P_WDM_ADRES_OD, P_WDM_ADRES, P_AUTONOMOUS_TRANSACTION);
    END LOOP;


   EXCEPTION
   WHEN OTHERS THEN
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 200);

	PPADM.PLOG('WIADOMOSC_FU: ' || err_code || ' - ' || err_msg);
END WIADOMOSC_FU;
/
