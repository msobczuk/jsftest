create or replace PROCEDURE ppadm.jpa_genkeys_update
IS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 V_PKEY_VALUE_NEW NUMBER(10);
BEGIN 

    insert into ppadm.ppt_jpa_genkeys (PKEY_TABLE, PKEY_VALUE)
    select 
        distinct tc.table_name , 1 pkval
    from SYS.all_tab_cols tc  
        join SYS.all_cons_columns cc on cc.owner = tc.owner and cc.table_name = tc.table_name and cc.column_name = tc.column_name
    where tc.owner ='PPADM' 
        and cc.constraint_name like '%_PK' and tc.column_name like '%_ID'
        and tc.table_name not in (select pkey_table from ppt_jpa_genkeys)  
        and tc.data_type = 'NUMBER'
    order by tc.table_name;



  FOR r IN (
    select 
    '
     update PPADM.PPT_JPA_GENKEYS 
     set PKEY_VALUE  = NVL((select MAX(' || COLUMN_NAME || ')+1 from ' || TABLE_NAME ||'), 1)
     where PKEY_TABLE = ''' || TABLE_NAME ||  ''' 
        and NVL((select MAX(' || COLUMN_NAME || ')+1 from ' || TABLE_NAME ||'), 1) > PKEY_VALUE
     ' as cmd, 
     TABLE_NAME, 
     PKEY_VALUE as PKEY_VALUE_OLD

    from PPADM.PPT_JPA_GENKEYS
      join  (
          select distinct table_name, substr( column_name, 1, instr( column_name, '_') -1 ) || '_ID' as COLUMN_NAME 
          from sys.all_tab_columns 
          where table_name in (select PKEY_TABLE from PPADM.PPT_JPA_GENKEYS) 
            and owner = 'PPADM'
      ) X on PKEY_TABLE = X.TABLE_NAME
    order by table_name
  ) LOOP

    --DBMS_OUTPUT.PUT_LINE(r.cmd);
    EXECUTE IMMEDIATE r.cmd;

    select PKEY_VALUE into V_PKEY_VALUE_NEW from PPADM.PPT_JPA_GENKEYS where PKEY_TABLE = r.table_name;
    IF  V_PKEY_VALUE_NEW > r.PKEY_VALUE_OLD THEN 
      DBMS_OUTPUT.PUT_LINE('PPADM.PPT_JPA_GENKEYS / ' || r.table_name ||  '; PKEY_VALUE: ' || r.PKEY_VALUE_OLD || ' -> ' || V_PKEY_VALUE_NEW  );
    END IF;    

  END LOOP;
COMMIT;
END JPA_GENKEYS_UPDATE;
/

