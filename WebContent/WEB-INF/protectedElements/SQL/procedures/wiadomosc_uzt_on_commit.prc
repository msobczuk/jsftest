CREATE OR REPLACE PROCEDURE PPADM.WIADOMOSC_UZT_ON_COMMIT
(
	  P_UZT IN VARCHAR2
	, P_TRESC IN VARCHAR2 
	, P_TEMAT IN VARCHAR2 DEFAULT 'WIADOMOSC SYSTEMOWA PP'
	, P_WDM_DATA_WSTAWIENIA IN DATE default sysdate
	, P_WDM_TYP IN VARCHAR2 DEFAULT 'E' --E=EGR; M=Mail
	, P_WDM_ADRES_OD IN VARCHAR2 DEFAULT NULL 
	, P_WDM_ADRES IN VARCHAR2 DEFAULT NULL --jezeli nie podano, wez z UZT lub z PRC
)
AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
	x_uzt VARCHAR2(50);
	x_mail VARCHAR2(255);
	err_code VARCHAR2(50);
	err_msg VARCHAR2(300);
	v_md5 eat_wiadomosci.wdm_md5%TYPE;
	v_wdm_id NUMBER(10);
	
BEGIN

  IF(P_UZT is not null) THEN
  
    BEGIN
      select uzt_nazwa into x_uzt from eat_uzytkownicy where uzt_nazwa = P_UZT;
    EXCEPTION
        WHEN no_data_found THEN
			BEGIN
			SELECT DDF_UZT_NAZWA into x_uzt
				FROM eat_dostepy_do_firm where DDF_UZT_NAZWA = P_UZT and ddf_frm_id = 1;
		  EXCEPTION
        when no_data_found THEN
          PPADM.PLOG('Nie znaleziono użytkownika ' || P_UZT || ' w tabeli EAT_UZYTKOWNICY i EAT_DOSTEPY_DO_FIRM');
		  END;
    END;
	
	IF(P_TRESC is not null and P_WDM_TYP is not null and x_uzt is not null) THEN

	  IF(P_WDM_ADRES is null and P_WDM_TYP='M') THEN

		SELECT UZT_MAIL into x_mail
			FROM eat_uzytkownicy where uzt_nazwa = x_uzt;
			
		v_md5 := eap_wiadomosci.wylicz_md5(P_TRESC);
		select EAADM.EAS_WDM.nextval into v_wdm_id from dual;
		
		insert into EAT_WIADOMOSCI (   WDM_ID, WDM_FRM_ID, WDM_UZT_NAZWA,   WDM_DATA_WSTAWIENIA, WDM_F_PRZECZYTANA,      WDM_TYP , WDM_ADRES,   WDM_ADRES_OD, WDM_TRESC, WDM_TEMAT,  WDM_MD5)
		values                     ( v_wdm_id,          1,         x_uzt, P_WDM_DATA_WSTAWIENIA,               'N',    P_WDM_TYP ,    x_mail, P_WDM_ADRES_OD,   P_TRESC,   P_TEMAT,    v_md5);


		
	  ELSE
	  
	    v_md5 := eap_wiadomosci.wylicz_md5(P_TRESC);
        select EAADM.EAS_WDM.nextval into v_wdm_id from dual;
		
	    insert into EAT_WIADOMOSCI (   WDM_ID, WDM_FRM_ID, WDM_UZT_NAZWA,   WDM_DATA_WSTAWIENIA, WDM_F_PRZECZYTANA,      WDM_TYP ,   WDM_ADRES,   WDM_ADRES_OD, WDM_TRESC, WDM_TEMAT, WDM_MD5)
        values                     ( v_wdm_id,          1,         x_uzt, P_WDM_DATA_WSTAWIENIA,               'N',    P_WDM_TYP , P_WDM_ADRES, P_WDM_ADRES_OD,   P_TRESC,   P_TEMAT,   v_md5);

		
	  END IF;

    END IF;
  
  
  END IF;
  
  
EXCEPTION
  WHEN OTHERS THEN
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 200);

	PPADM.PLOG('WIADOMOSC_UZT_ON_COMMIT: ' || err_code || ' - ' || err_msg); 
END WIADOMOSC_UZT_ON_COMMIT;
/
