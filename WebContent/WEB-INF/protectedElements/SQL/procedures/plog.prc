CREATE OR REPLACE PROCEDURE PPADM.plog (p_txt VARCHAR2)
---------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header:: /#egeria4/6.0.4/#upgrade/install/604/obj_pp/procedures/eg.ppp_PLOG.prc 1 1.0.1 19.03.18 09:12 DZIOBEK                                            $
-- $Revision:: 1                                                                                                                                              $
-- $Workfile:: eg.ppp_PLOG.prc                                                                                                                                $
-- $Modtime:: 19.03.18 09:12                                                                                                                                  $
-- $Author:: DZIOBEK                                                                                                                                          $
---------------------------------------------------------------------------------------------------------------------------------------------------------------
IS
  PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN

    INSERT INTO PPT_LOG
        VALUES (SYSTIMESTAMP, 
				sys_context('USERENV','SESSIONID'),
				p_txt,
				SYS_CONTEXT('USERENV', 'SESSION_USER'),
				SYS_CONTEXT('EA_GLOBALS', 'EG_UZYTKOWNIK'), 
				SYS_CONTEXT ('USERENV', 'SID'),
				SYS_CONTEXT ('USERENV', 'CLIENT_IDENTIFIER'));
    COMMIT;
	
EXCEPTION
    WHEN NO_DATA_FOUND THEN 
        null;
END;
/
