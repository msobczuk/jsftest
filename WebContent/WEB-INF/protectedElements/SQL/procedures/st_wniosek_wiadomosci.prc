create or replace procedure ppadm.st_wniosek_wiadomosci(p_wn_id number, p_wiadomosc varchar2) as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    v_msg varchar2(4000);
BEGIN
    v_msg := p_wiadomosc
    || CHR(13)||CHR(10)|| PPADM.GET_ADRES || 'ListaStWnPrc?ppl_srodki_trwale_wnioski'|| p_wn_id;


    for r in (
        ---------------------------------------------------------------------------------
        select  *
        from stt_wnioski
            join EGADM1.stt_wnioski_parametry on wn_id = wnp_wn_id  and wn_id = P_WN_ID
            left join stt_podzialy_srodka ps on ps_id = wnp_ps_id and (wnp_kod = 'LIKW' or  (wnp_kod = 'ZM_OS_ODP' and wnp_wartosc_z=ps_prc_id))
            left join EGADM1.stt_srodki_dane sdn on sdn_id = ps_sdn_id
        ---------------------------------------------------------------------------------
    ) loop

        PPADM.WIADOMOSC_FU('PP_ST_ADMINISTRATOR', v_msg, p_wdm_typ => 'M', P_AUTONOMOUS_TRANSACTION=>'T'); -- M = email - wiadomosc do wysztskich z FU PP_ST_ADMINISTRATOR

        ppp_global.ustaw_date_dla_struktury_po(sysdate); --inaczej PPADM.PRZELOZONY zwr. zawsze null

        PPADM.WIADOMOSC(R.PS_PRC_ID, v_msg, p_wdm_typ => 'M'); -- do osoby odpowiedzialnej za ST
        if PPADM.PRZELOZONY(R.PS_PRC_ID) is not null then
            PPADM.WIADOMOSC(PPADM.PRZELOZONY(R.PS_PRC_ID), v_msg, p_wdm_typ => 'M', P_AUTONOMOUS_TRANSACTION=>'T'); -- do przelozonego osoby odpowiedzialnej za ST
        end if;

        if (r.wnp_kod = 'ZM_OS_ODP') then
            PPADM.WIADOMOSC(R.WNP_WARTOSC_NA, v_msg, p_wdm_typ => 'M'); -- do osoby przyjmujacej ST
            if PPADM.PRZELOZONY(R.WNP_WARTOSC_NA) is not null then
                PPADM.WIADOMOSC(PPADM.PRZELOZONY(R.WNP_WARTOSC_NA), v_msg, p_wdm_typ => 'M', P_AUTONOMOUS_TRANSACTION=>'T'); -- do przelozonego osoby przyjmujacej ST
            end if;
        end if;

    end loop;

END;
/

