create or replace procedure ppadm.obecnosc_ustaw(P_PRC_ID NUMBER, P_DATE DATE, P_STATUS VARCHAR2) as
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
begin 
    if P_STATUS='T' then

        update ppadm.ppt_obecnosci 
            set obcn_od = p_date
        where obcn_prc_id=p_prc_id and trunc(obcn_od) = trunc(p_date) and obcn_rodzaj is null;

        if SQL%ROWCOUNT <=0  then
              insert into  ppadm.ppt_obecnosci    (            obcn_id,           obcn_prc_id,   obcn_od,     obcn_rodzaj  )
              values                              (nextpk('PPT_OBECNOSCI'),  p_prc_id,      p_date,      null         );
        end if;

        --delete from ppadm.ppt_obecnosci where obcn_prc_id=p_prc_id and trunc(obcn_od) = trunc(p_date) and obcn_rodzaj is null;
    end if;

    if P_STATUS='N' then
        delete from ppadm.ppt_obecnosci where obcn_prc_id=p_prc_id and trunc(obcn_od) = trunc(p_date) and obcn_rodzaj is null;
    end if;

end;
/
