CREATE OR REPLACE PROCEDURE PPADM.WIADOMOSC
(
  P_PRC_ID IN NUMBER
, P_TRESC IN VARCHAR2 
, P_TEMAT IN VARCHAR2 DEFAULT 'Wiadomość systemowa [PP]'
, P_WDM_DATA_WSTAWIENIA IN DATE default sysdate
, P_WDM_TYP IN VARCHAR2 DEFAULT 'E' --E=EGR; M=Mail
, P_WDM_ADRES_OD IN VARCHAR2 DEFAULT NULL 
, P_WDM_ADRES IN VARCHAR2 DEFAULT NULL --jezeli nie podano, wez z UZT lub z PRC
, P_AUTONOMOUS_TRANSACTION VARCHAR2 DEFAULT 'N'
)
AS
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- $Header::                                                                                                                                                                                                     $
-- $Revision::                                                                                                                                                                                                   $
-- $Workfile::                                                                                                                                                                                                   $
-- $Modtime::                                                                                                                                                                                                    $
-- $Author::                                                                                                                                                                                                     $
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 

   err_code VARCHAR2(50);
   err_msg VARCHAR2(300);
   is_found_rec boolean := false;
   
   cursor c1 IS
	   SELECT uzt_nazwa 
		FROM eat_uzytkownicy where uzt_prc_id = P_PRC_ID;
        
   cursor c2 IS
	   SELECT DDF_UZT_NAZWA 
		FROM eat_dostepy_do_firm where DDF_PRC_ID = P_PRC_ID and ddf_frm_id = 1;
		
   BEGIN
   
	IF(P_PRC_ID is not null AND P_TRESC is not null) THEN
	
		FOR uzytkownik in c1
		LOOP
			is_found_rec := true;
            PPADM.WIADOMOSC_UZT(uzytkownik.uzt_nazwa, P_TRESC, P_TEMAT, P_WDM_DATA_WSTAWIENIA, P_WDM_TYP, P_WDM_ADRES_OD, P_WDM_ADRES, P_AUTONOMOUS_TRANSACTION);
		END LOOP;
		
		IF(not is_found_rec) THEN
			FOR uzytkownik in c2
			LOOP
				is_found_rec := true;
				PPADM.WIADOMOSC_UZT(uzytkownik.DDF_UZT_NAZWA, P_TRESC, P_TEMAT, P_WDM_DATA_WSTAWIENIA, P_WDM_TYP, P_WDM_ADRES_OD, P_WDM_ADRES, P_AUTONOMOUS_TRANSACTION);
			END LOOP;
		END IF;
		
		IF(not is_found_rec) THEN
			PPADM.PLOG('Nie znaleziono pracownika o prc_id - ' || P_PRC_ID || ' w tabeli EAT_UZYTKOWNICY i EAT_DOSTEPY_DO_FIRM');
		END IF;
		
	END IF;
   
   EXCEPTION
   WHEN OTHERS THEN
    err_code := SQLCODE;
    err_msg := SUBSTR(SQLERRM, 1, 200);

	PPADM.PLOG('WIADOMOSC: ' || err_code || ' - ' || err_msg);		
END WIADOMOSC;
/
