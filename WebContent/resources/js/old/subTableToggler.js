$(document).ready(function() {	
	$("div.hiddenRows tr.ui-widget-content").css("display", "none");
	$("[id$=hideBtn").css("display", "none");
		
	$("[id$=showBtn]").click(function (event) {
		var split = $(this).attr("id").split(":");		
		var id = split[0] + "\\:" + split[1] + "\\:" + split[2] + "\\:";		
		
		$("tr[id^=" + id + "]").css("display", "table-row");
		$(this).css("display", "none");
		$("[id^=" + id + "][id$=hideBtn]").css("display", "inline");
    });
	
	$("[id$=hideBtn").click(function (event) {
		var split = $(this).attr("id").split(":");		
		var id = split[0] + "\\:" + split[1] + "\\:" + split[2] + "\\:";		
		
		$("tr[id^=" + id + "]").css("display", "none");
		$(this).css("display", "none");
		$("[id^=" + id + "][id$=showBtn]").css("display", "inline");
    });
});