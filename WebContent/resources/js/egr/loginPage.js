function fixErrorMessages() {
	$('.help-block.with-errors').removeClass(
			'ui-message ui-message-error ui-widget ui-corner-all').find('span')
			.removeClass('ui-message-error-detail');
}

$(document).ready(function() {
	var logoFirmy = $('.login-logo-firmy img');
	var logoFirmyC = 0;
	logoFirmy.on('click', function() {
		logoFirmyC++;
		if (logoFirmyC > 8)
			rotate(logoFirmy);
	});

	$('#egr-storage-logout').html(sessionStorage.getItem('logoutInformation'));
	sessionStorage.removeItem('logoutInformation');
})

function rotate(elem) {
	elem.css('animation', 'spin 2s linear infinite');
	setTimeout(function() {
		elem.css('animation', 'none');
	}, 6000);
}

// Gdy formularz login/logout wyświetlany w iframe, to przeładuj całą stronę 
$(function() {
	$(document).ready(function(e) {
		if (parent != undefined) {
			if (parent.setFormName != undefined) {
				$("body").hide();
				parent.location.reload();
			}
		}
	});
});