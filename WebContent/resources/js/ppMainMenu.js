var currHeader;
var currItem;

$(document).ready(function() {

	$(function() { 
		PF('westPanelMenu').headers.click(function() {
			currHeader = $(this);
		    PF('westPanelMenu').headers.each(function() {
		        var header = $(this);
		        if (header.text() !== currHeader.text()) {
	        		PF('westPanelMenu').collapseRootSubmenu(header);
	        		header.removeClass("ui-state-hover");
		        }
		    });
		});
	});

	$("#egrMenuForm li a").click(function() {
		currItem = this;
		$("#egrMenuForm li a").removeClass("currentMnuItem");
		$( this ).addClass('currentMnuItem');
		clearStyle();
	});

});

