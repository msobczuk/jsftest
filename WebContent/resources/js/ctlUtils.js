var scrltoptmp;

function isNavKey(keyCode){
	//var navKeys = [8, 13, 16, 17, 18, 19, 27, 33,34,35,36,37,38,39,40,45 ]
	if (keyCode >8 && keyCode < 46) return true;
	else return false;
}


function hideShowPanels() {
	if($("#ctlMenuForm\\:przypnijMnBtn").css("display") == "none" || $(".ctl-show-icon").css("display") == "none") {
		$("#ctlMenuForm\\:odepnijMnBtn").click();
		$(".ctl-hide-icon").click();
	} else {
		$("#ctlMenuForm\\:przypnijMnBtn").click();
		$(".ctl-show-icon").click();
	}
}


function jq( myid ) {
    return "#" + myid.replace( /(:|\.|\[|\]|,|=|@)/g, "\\$1" );
}

function wid( myid ) {
    return "widget_" + myid.replace( /(:|\.|\[|\]|,|=|@)/g, "_" );
}


function isEmpty( x ) {
	if (x==null || x == ''){
		return true;
	}else{
		return false;
	}
}


function triggerMDesignEffects(element) {
	 $('.cc-mdesign').find('.mdesign-focus').removeClass('mdesign-focus'); 
	 element.parent().addClass('mdesign-focus');
}

// PrimeFaces.widget.InputTextarea.prototype.updateCounter = function() {
//     var value = this.normalizeNewlines(this.jq.val()),
//     length = value.length;
//
//     if(this.counter) {
//         var remaining = this.cfg.maxlength - length;
//         if(remaining < 0) {
//             remaining = 0;
//         }
//
//         var remainingText = this.cfg.counterTemplate.replace('{0}', remaining).replace('{1}', length).replace('{2}', this.cfg.maxlength);
//
//         this.counter.html(remainingText);
//     }
// };


function showHourGlass() {
	$(jq('hourGlass') ).css('visibility','visible');
}
function hideHourGlass() {
	$(jq('hourGlass') ).css('visibility','hidden');
}

function hgon() {
	showHourGlass();
}
function hgoff() {
	hideHourGlass();
}

function hgon1() {
	$(jq('hourGlass1') ).css('visibility','visible');
}
function hgoff1() {
	$(jq('hourGlass1') ).css('visibility','hidden');
}


function show(classParam) {
	$(classParam).show();
}
function hide(classParam) {
	$(classParam).hide();
}
function toggleShowHide(classParam) {
	$(classParam).toggle();
}
var cpnode;

function copyToClipboard(txtNode) {
	//console.log("copyToClipboard... ... "  +txtNode);
	//console.log(txtNode.textContent);

	//https://stackoverflow.com/questions/49236100/copy-text-from-span-to-clipboard
	var textArea = document.createElement("textarea");
	textArea.value = txtNode.textContent;
	document.body.appendChild(textArea);
	textArea.select();
	document.execCommand("Copy");
	textArea.remove();

	$(cpnode).removeClass("cp-clippoard");
	cpnode = txtNode;
	$(cpnode).addClass("cp-clippoard");

	// if (document.selection) {
	// 	var range = document.body.createTextRange();
	// 	range.moveToElementText(txtNode);
	// 	range.select().createTextRange();
	// 	document.execCommand("copy");
	// } else if (window.getSelection) {
	// 	var range = document.createRange();
	// 	range.selectNode(txtNode);
	// 	window.getSelection().addRange(range);
	// 	document.execCommand("copy");
	// 	alert("text copied, copy in the text-area")
	// }
}


function copyStringToClipboard (str) {
	var el = document.createElement('textarea');
	el.value = str;
	el.setAttribute('readonly', '');
	el.style = {position: 'absolute', left: '-9999px'};
	document.body.appendChild(el);
	el.select();
	document.execCommand('copy');
	document.body.removeChild(el);
}


function onMousedownStopPropagation(e) {
	e.preventDefault();
	e.stopPropagation();
	e.stopImmediatePropagation();
	return false;
}




function clickMenuItem( mnuItemId, menuCode ) {
	if (parent !== undefined) {
		parent.clickMenuItemMM(mnuItemId, menuCode);
	} else {
		clickMenuItemMM(mnuItemId, menuCode);
	}
}

function clickMenuItemMM( mnuItemId, menuCode ) {
	if (mnuItemId.indexOf(".") >= 0) {
		mnuItemId = mnuItemId.replace( /\./g, "\\.");
	}

	const menuElement = menuCode ? $('a[menucode = "' + menuCode + '"]') : $('#' + mnuItemId);

	menuElement.parent().parent().parent().parent().find('h3').click();
	PF('westPanelMenu').expandRootSubmenu(menuElement.parent().parent().parent().parent().find('h3'));
	menuElement.click();
}


function b64EncodeUnicode(str) {
	// first we use encodeURIComponent to get percent-encoded UTF-8,
	// then we convert the percent encodings into raw bytes which
	// can be fed into btoa.
	return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
		function toSolidBytes(match, p1) {
			return String.fromCharCode('0x' + p1);
		}));
}

function b64DecodeUnicode(str) {
	// Going backwards: from bytestream, to percent-encoding, to original string.
	return decodeURIComponent(atob(str).split('').map(function(c) {
		return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
	}).join(''));
}
