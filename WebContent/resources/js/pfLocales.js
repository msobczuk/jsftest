
PrimeFaces.locales['pl'] = {
    closeText: 'Zamknij',
    prevText: 'Poprzedni',
    nextText: 'Następny',
    monthNames: ['Styczeń','Luty','Marzec','Kwiecień','Maj','Czerwiec','Lipiec','Sierpień','Wrzesień','Październik','Listopad','Grudzień'],
    monthNamesShort: ['Sty','Lut','Mar','Kwi','Maj','Cze', 'Lip','Sie','Wrz','Paź','Lis','Gru'],
    dayNames: ['Niedziela','Poniedziałek','Wtorek','Środa','Czwartek','Piątek','Sobota'],
    dayNamesShort: ['Nie','Pon','Wt','Śr','Czw','Pt','So'],
    dayNamesMin: ['N','P','W','Ś','Cz','P','S'],
    weekHeader: 'Tydzień',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    //yearSuffix: 'rok',
    timeOnlyTitle: 'Tylko czas',
    timeText: 'Czas',
    hourText: 'Godzina',
    minuteText: 'Minuta',
    secondText: 'Sekunda',
    currentText: 'Teraz',
    ampm: false,
    month: 'Miesiąc',
    week: 'Tydzień',
    day: 'Dzień',
    allDayText : 'Cały dzień'
};


//https://github.com/primefaces-extensions/showcase/blob/master/src/main/webapp/sections/timePicker/example-advancedUsage.xhtml
try {
	PrimeFacesExt.locales.TimePicker['pl_PL'] = {
		    hourText: 'Godziny',
		    minuteText: 'Minuty',
		    amPmText: ['AM', 'PM'],
		    closeButtonText: 'Zamknij',
		    nowButtonText: 'Teraz',
		    deselectButtonText: 'Wyczyść'
	};
}catch(err) {
	//https://stackoverflow.com/questions/21904812/browser-error-primefacesext-is-not-defined	
}