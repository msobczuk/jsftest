// Scroll height and width update after short timeout

PrimeFaces.widget.DataTable.prototype.adjustScrollHeight = function () {
	var that = this;
	setTimeout(function () {
		var d = that.jq.parent().innerHeight() * (parseInt(that.cfg.scrollHeight) / 100),
		f = that.jq.children(".ui-datatable-header").outerHeight(true),
		b = that.jq.children(".ui-datatable-footer").outerHeight(true),
		c = (that.scrollHeader.outerHeight(true) + that.scrollFooter.outerHeight(true)),
		e = that.paginator ? that.paginator.getContainerHeight(true) : 0,
		a = (d - (c + e + f + b));
		if (that.cfg.virtualScroll) {
			that.scrollBody.css("max-height", a)
		} else {
			that.scrollBody.height(a)
		}
	}, 300)
};
	
PrimeFaces.widget.DataTable.prototype.adjustScrollWidth = function () {
	var that = this;
	setTimeout(function () {
		var a = parseInt((that.jq.parent().innerWidth() * (parseInt(that.cfg.scrollWidth) / 100)));
		that.setScrollWidth(a)
	}, 300)
};
		