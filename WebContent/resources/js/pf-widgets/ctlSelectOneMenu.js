PrimeFaces.widget.SelectOneMenu = PrimeFaces.widget.SelectOneMenu.extend({
	init : function(cfg) {
		this._super(cfg);
	},

	show : function() {
		this._super();

		if (this.panel.width() > this.jq.outerWidth()) {
			var container_width = this.itemsContainer.width();
			this.panel.width(this.jq.outerWidth());
			this.itemsContainer.width(container_width + 14);
			this.alignPanel();
		}

		this.panel.find('> div.ui-selectonemenu-filter-container').width(
				this.panel.width() - 11);
	}
});