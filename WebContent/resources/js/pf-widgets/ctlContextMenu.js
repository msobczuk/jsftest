var columnId;

PrimeFaces.widget.ContextMenu.prototype.show = function(event) {
	// set columnId
	var str = event.target.id.split(':');
	columnId = str[str.length - 1];

	// hide other contextmenus if any
	$(document.body).children('.ui-contextmenu:visible').hide();

	if (event) {
		currentEvent = event;
	}

	var win = $(window), left = event.pageX, top = event.pageY, width = this.jq
			.outerWidth(), height = this.jq.outerHeight();

	// collision detection for window boundaries
	if ((left + width) > (win.width()) + win.scrollLeft()) {
		left = left - width;
	}
	if ((top + height) > (win.height() + win.scrollTop())) {
		top = top - height;
	}

	if (this.cfg.beforeShow) {
		this.cfg.beforeShow.call(this);
	}

	this.jq.css({
		'left' : left,
		'top' : top,
		'z-index' : ++PrimeFaces.zindex
	}).show();

	event.preventDefault();
};

(function($) {
	$.fn.doSrodka = function() {
		$("[id$=" + columnId + "]").removeClass("ctl-column-left");
		$("[id$=" + columnId + "]").removeClass("ctl-column-right");
		$("[id$=" + columnId + "]").addClass("ctl-column-center");
	}

	$.fn.doLewej = function() {
		$("[id$=" + columnId + "]").removeClass("ctl-column-center");
		$("[id$=" + columnId + "]").removeClass("ctl-column-right");
		$("[id$=" + columnId + "]").addClass("ctl-column-left");
	}

	$.fn.doPrawej = function() {
		$("[id$=" + columnId + "]").removeClass("ctl-column-center");
		$("[id$=" + columnId + "]").removeClass("ctl-column-left");
		$("[id$=" + columnId + "]").addClass("ctl-column-right");
	}
})(jQuery);
