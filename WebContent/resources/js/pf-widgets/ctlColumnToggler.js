PrimeFaces.widget.ColumnToggler = PrimeFaces.widget.ColumnToggler.extend({
	show: function() {
		//console.log("toggler.show()");	
		
		this._super();

		PrimeFaces.getWidgetById(this.tableId).columnTogglerWidget = this;
	},
	        	
	toggle: function(chkbox) {
		//console.log("toggler.toggle()");		
		
		this._super(chkbox);
		
		var dataTableId = '#' + this.tableId.replace(/:/g, "\\:"),
			dataTablePaneId = '#' + $(dataTableId).closest('.ctl-datatable-pane').attr('id').replace(/:/g, "\\:");
	
		//repairSizeOfDataTable(dataTableId, dataTablePaneId);
	}
});