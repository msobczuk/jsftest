PrimeFaces.widget.Menubar = PrimeFaces.widget.TieredMenu.extend({
	init: function(cfg) {
		//console.log("menubar.init()");
		
		this._super(cfg);
        
        $('.ctl-context-btns-bar').on('elementResize', function(event){ ukrywaniePrzyciskow($(event.target),false); } );
        ukrywaniePrzyciskow(this.jq, true);
    },
    
    showSubmenu: function(menuitem, submenu) {
    	//console.log("menubar.showSubmenu()");
    	
        this._super(menuitem, submenu);
        var width = 0;
    	
    	submenu.children().each(function(){    		
    		if($(this).children().width() > width)
    			width = $(this).children().width();
    	});
    	
    	submenu.width(width + 30);
    	submenu.children().width(width + 15);
        
        if(submenu.parent().hasClass('ctl-hidden-btns')) {
        	submenu.css('left', - submenu.width() + 40);
    	} else {
        	if(submenu.parent().parent().parent().hasClass('ctl-hidden-btns')) {
        		submenu.css('left', - submenu.width() + 15);
        	}
        	else
        		submenu.css('left', 0);
        }
    }    
});

var cntxtBtnsBarWidth = 0;

function ukrywaniePrzyciskow(selector, init) {
	//console.log("ukrywaniePrzyciskow()");

	if(cntxtBtnsBarWidth != selector.width() || init) {
		selector.find('.ctl-hidden-btns>ul>li').each(function() {
			$(this).css('width','auto');
			$(this).find('.ui-icon-triangle-1-w').removeClass('ui-icon-triangle-1-w').addClass('ui-icon-triangle-1-s');
			$(this).insertBefore(selector.find('.ctl-hidden-btns'));
		});
		
		selector.find('.ctl-hidden-btns').hide();
		
		while(selector.height() > 40) {
			selector.find('.ctl-hidden-btns').show();
			
			var li = selector.children('ul').children('li:nth-last-child(2)');		
			li.detach().prependTo(selector.find('.ctl-hidden-btns>ul'));
		}			
		
		cntxtBtnsBarWidth = selector.width();
		
		selector.find('.ctl-hidden-btns ul .ui-icon-triangle-1-s').addClass('ui-icon-triangle-1-w').removeClass('ui-icon-triangle-1-s');
	}
}