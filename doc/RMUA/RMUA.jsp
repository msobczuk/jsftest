
<!--
$Revision:: 26         $
$Workfile:: RMUA.jsp         $
$Modtime:: 03.02.14 14:02  $
$Author:: CIURLA                        $
-->

<%@ page errorPage="errorpage.jsp" %>
<%@ page import="java.sql.*" %>

<%@ page import="java.util.Calendar" %>
<%@ page import="java.text.DecimalFormat, java.text.SimpleDateFormat" %>
<%@ page import="com.comarch.egeria.util.Ustawienia"%>


<!-- Instantiate the beans with id's -->

<jsp:useBean id="pool" scope="application" class="com.comarch.egeria.ki.kiConnection" />
<jsp:useBean id="user" scope="session" class="com.comarch.egeria.ki.kiUser" />
<jsp:useBean id="sf" scope="application" class="com.comarch.egeria.util.StringFrm" />
<jsp:useBean id="menu" scope="session" class="com.comarch.egeria.ki.kiMenu" />
<jsp:useBean id="rmua" scope="session" class="com.comarch.egeria.ki.kiRmua" />

<!-- Set encoding-->
<%@page contentType="text/html; charset=ISO-8859-2"%>

<%
  request.setCharacterEncoding("ISO-8859-2");
  if(!user.userConnected()) response.sendRedirect("doLogout.jsp");
  if(!menu.isAvailable("RMUA.jsp")) throw new Exception("Nie masz dost�pu do tego elementu menu."); 
  Ustawienia.checkIP(request.getRemoteAddr(), request.getSession().getId(), pool.getConnection());
  //System.err.println("RMUA FRM_ID="+user.getFrmId());
  Connection vConn = pool.getConnection(user.getFrmId());
%>

<html>
	<head>
		<title><%=menu.getMenuTitle("RMUA.jsp") %></title>		
		<link rel=stylesheet type="text/css" href="styl.css" title="default">
		<link rel=stylesheet type="text/css" href="rmua.css" title="default">
		<link rel="stylesheet" type="text/css" media="print" href="rmua_wydruk.css">		
		<meta http-equiv="content-type" content="text/html; charset=iso-8859-2">
	</head>
	<body>
		<div class="browserOnly" id="header" align="center">
			<%@ include file="ApplicationHeader.inc"%>
		</div>
        <div id="main">		
		<%
	        Calendar data = Calendar.getInstance();
	
	        String miesiac_dwucyfrowy = new String("0");
	        String miesiacOd2 = new String("0");
	        String miesiacDo2 = new String("0");
	        int biezacy_rok = new Integer(data.get(Calendar.YEAR));
	        int miesiac = new Integer(data.get(Calendar.MONTH)+1);
	        int miesiacOd = miesiac;
	        int miesiacDo = miesiac;
	        int rok = new Integer(data.get(Calendar.YEAR));
			
			//wpisuje do zmiennych miesiac i rok odpowiednie wartosci wyslane formularzem
			if(request.getMethod().equals("POST")) {
			    miesiac = Integer.parseInt(request.getParameter("miesiac"));
			    if (miesiac==13) {
			    	miesiacOd = 1;
			    	miesiacDo = 12;
			    } else {
			    	miesiacOd = miesiac;
			    	miesiacDo = miesiac;	
			    }
			    rok = Integer.parseInt(request.getParameter("rok"));
			}
			//funkcja konwertuj�ca miesi�c z formatu INT na String 'MM'			
			if(miesiacOd<10) miesiacOd2 += miesiacOd;
			else miesiacOd2=Integer.toString(miesiacOd);

			if(miesiacDo<10) miesiacDo2 += miesiacDo;
			else miesiacDo2=Integer.toString(miesiacDo);

			String identyfikator =""; 
			if (miesiac==13) identyfikator = String.valueOf(rok);
			else identyfikator = "01/" + miesiacOd2 + "/" + rok; 
			//System.err.println("RMUA: identyfikator=" + identyfikator);	    
		%>
        <table class="t1">
        
        <tr>
        <td class="white" colspan="2" align="center" valign="middle" >		
		<div class="browserOnly" id="datechooser" align="center">
	       <form action="<%=request.getRequestURL()%>" method="post">

				<select name="miesiac">
				<%
					String miesiace[] = {"Stycze�", "Luty", "Marzec", "Kwiecie�", "Maj", "Czerwiec", "Lipiec", "Sierpie�", "Wrzesie�", "Pa�dziernik", "Listopad", "Grudzie�","Ca�y rok"};
					
			        for(int i=0; i<13; i++) {
	                	out.print("<option value=\"" + (i+1) + "\" ");
                        if ((i+1)==miesiac) {
                               out.print("selected=\"selected\" ");
                        }
			        	out.print(">" + miesiace[i] + "</option>");
			        }
				%>
				</select>
					
				<select name="rok">
				<%
					for(int i=biezacy_rok; i>=2003; i--) {
						out.print("<option ");
				        if (i==rok) {
		                	out.print("selected=\"selected\" ");
				        }
						out.print(">" + i + "</option>");
					}
				%>
				</select>
               	<input type="submit" value="Ustaw" />
        	</form>
		</div>
        		
		<div id="frame" >
			<form class="doc" action="javascript: window.print();" method="post">
			
				<fieldset class="printOnly" id="title">
					<table>
						<tr class="head">
							<td style="width: 25%; text-align: center;"><label >ZAK�AD UBEZPIECZE� SPO�ECZNYCH</label></td>
							<td style="width: 75%; text-align: center;"><label >ZUS RMUA RAPORT MIESI�CZNY DLA OSOBY UBEZPIECZONEJ</label></td>
						</tr>
					</table>
				</fieldset>
			
				<fieldset>
					<legend>I.  DANE ORGANIZACYJNE</legend>
					<table>
						<tr>
							<td style="width: 75%"><label for="rmua_id">Identyfikator raportu</label></td>
							<td style="width: 25%"><input readonly type="text" name="rmua_id" value="<% out.print(identyfikator); %>" /></td>
						</tr>
					</table>
				</fieldset>
				
			    <%
					try {
						Statement egStatement = vConn.createStatement();
						ResultSet egResultSet = egStatement.executeQuery(
							"select " +
								"kld_nip_dane, kld_regon, ek_pck_defs.get_def_w('SKROCONA_NAZWA_FIRMY', ek_pck_api02.nazwa_oddz) skrot " +
							"from " +
								"ckk_klienci_dane " +
							"where " +
								"kld_kl_kod = (SELECT frm_kl_id FROM eat_firmy WHERE frm_id = "+user.getFrmId()+") " +
							    "and kld_f_aktualne = 'T' " +
							    "and ROWNUM = 1"
						);
						if (egResultSet.next()) {
				%>	
			    <fieldset>
			    	<legend>II. DANE IDENTYFIKACYJNE P�ATNIKA SK�ADEK</legend>
					<table>
						<tr>
							<td style="width: 25%"><label for="">NIP</label></td>
							<td style="width: 25%"><input readonly type="text" name="" value="<%= sf.notNull(egResultSet.getString("kld_nip_dane")) %>" /></td>
							<td style="width: 25%"><label for="">Nazwisko</label></td>
							<td style="width: 25%"><input readonly type="text" name="" value="" /></td>
						</tr>
						<tr class="odd">
							<td><label for="">REGON</label></td>
							<td><input readonly type="text" name="" value="<%= sf.notNull(egResultSet.getString("kld_regon")) %>"/></td>
							<td><label for="">Imi�</label></td>
							<td><input readonly type="text" name="" value="" /></td>
						</tr>
						<tr>
							<td><label for="">PESEL</label></td>
							<td><input readonly type="text" name="" value="" /></td>
							<td><label for="">Nazwa skr�cona p�atnika sk�adek</label></td>
							<td><input readonly type="text" name="" value="<%= sf.notNull(egResultSet.getString("skrot")) %>" /></td>
						</tr>
					</table> 
				</fieldset>
				<% 
						}
					} catch (java.sql.SQLException e) {
					  out.println("<p class=error>" + e.getMessage() + "</p>");
					}
				%>   
				
				<%
					String Nazwisko = "";
					String Imie = "";
					String Typ_ID = "";
					String ID = "";
					String Kod_NFZ = "";
				
					try {
						Statement egStatement = vConn.createStatement();
						ResultSet egResultSet = egStatement.executeQuery(
							"select " +
								"prc_imie, " +
								"prc_nazwisko, " +
								"prc_pesel, " +
					       		"prc_paszport, " +
							    "( " +
							    	"select " +
							    		"kch_kod " +
							    	"from " +
							    		"ek_kasa_chorych, " + 
							    		"ek_zus " +
							         "where " +
							         	"kch_id = zus_kch_id " +
							         	"and zus_prc_id = prc_id " +
								") kod_nfz " +
							"from " +
								//"ek_zatrudnienie, " +
								"ek_pracownicy " +
							"where " +
								//"sysdate between zat_data_zmiany and nvl(zat_data_do,sysdate) " +
								//"and zat_typ_umowy = 0 " +
								//"and zat_prc_id = prc_id " +
						       "EXISTS ( "+
							   "       SELECT 1 "+
							   "         FROM ek_zatrudnienie "+
							   "        WHERE zat_prc_id = prc_id "+
							   "          AND zat_data_zmiany <= "+
							   "                 LAST_DAY (TO_DATE ('01" + miesiacDo2 + rok + "', 'DDMMYYYY' ) ) "+
							   "          AND NVL (zat_data_do, "+
							   "                   TO_DATE ('01" + miesiacOd2 + rok + "', 'DDMMYYYY' ) ) >= "+
							   "                 TO_DATE ('01" + miesiacOd2 + rok + "', 'DDMMYYYY')) "+
								"and prc_id = " + user.getPrcID()
						);
						
						if (egResultSet.next()) {
							Nazwisko = 	egResultSet.getString("prc_nazwisko");
							Imie =  	egResultSet.getString("prc_imie");
							ID = 		egResultSet.getString("prc_pesel");
							if (ID != null) {
								Typ_ID = 	"PESEL";							
							} else {
								ID = 	egResultSet.getString("prc_paszport");	
								if (ID != null) {
									Typ_ID = 	"PASZPORT";
								}	
							}							
							Kod_NFZ = 	egResultSet.getString("kod_nfz");
				%>	
				<fieldset>
					<legend>III.A. DANE IDENTYFIKACYJNE OSOBY UBEZPIECZONEJ<!--  ORAZ KOD ODDZIA�U NFZ --></legend>
					<table>
						<tr>
							<td><label for="">Nazwisko</label></td>
							<td><input readonly type="text" name="" value="<%= sf.notNull(egResultSet.getString("prc_nazwisko")) %>" /></td>
																				
							<td  style="width: 25%"><label for="">Typ identyfikatora</label></td>
							<td  style="width: 25%"><input readonly type="text" name="" value="<%= sf.notNull(Typ_ID) %>" /></td>
						</tr>
						<tr class="odd">
							<td  style="width: 25%"><label for="">Imi�</label></td>
							<td  style="width: 25%"><input readonly type="text" name="" value="<%= sf.notNull(egResultSet.getString("prc_imie")) %>" /></td>
							<td><label for="">Identyfikator</label></td>
							<td><input readonly type="text" name="" value="<%= sf.notNull(ID) %>" /></td>
						</tr>
						<tr>
							<td><label for="">Kod oddzia�u NFZ</label></td>
							<td><input readonly type="text" value="<%= sf.notNull(egResultSet.getString("kod_nfz")) %>" /></td>
							<td><label for=""></label></td>
							<td><label for=""></label></td>
						</tr>
					</table> 
				</fieldset>
				<% 
						}
					} catch (java.sql.SQLException e) {
						out.println("<p class=error>" + e.getMessage() + "</p>");
					}
				%>
				
				
		<%  int m = miesiacOd;
		    //String mm = "";
		    int ramka=0;
		    while(m <= miesiacDo) {
		    	
		    if(m<10) miesiac_dwucyfrowy ="0"+m;
			else miesiac_dwucyfrowy=Integer.toString(m);		    	
		    //if (miesiac==13) mm = miesiac_dwucyfrowy+".";
		    ramka=0;
		%>	
				
				
				<%
					try {
						Statement egStatement0 = vConn.createStatement(
							ResultSet.TYPE_SCROLL_SENSITIVE,
	                        ResultSet.CONCUR_READ_ONLY);	
						StringBuilder sb = new StringBuilder();
						 //Osoby, kt�re mia�y wyp�aty
						 sb.append(" SELECT   NVL (prc_prc_id, prc_id) sk_prc_id, prc_imie, prc_nazwisko, ");
						 sb.append("          ek_pck_pp.status (prc_id, zat_status, to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY'), last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY'))) status, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_70', sk_wartosc, 0)) sk70, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_74', sk_wartosc, 0)) sk74, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_75', sk_wartosc, 0)) sk75, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_601', sk_wartosc, 0)) sk601, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_602', sk_wartosc, 0)) sk602, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_603', sk_wartosc, 0)) sk603, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_604', sk_wartosc, 0)) sk604, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_605', sk_wartosc, 0)) sk605, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_606', sk_wartosc, 0)) sk606, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_607', sk_wartosc, 0)) sk607, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_609', sk_wartosc, 0)) sk609, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_610', sk_wartosc, 0)) sk610, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_611', sk_wartosc, 0)) sk611, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_612', sk_wartosc, 0)) sk612, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_619', sk_wartosc, 0)) sk619, ");						 
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_70', 1, 0)) sk70i, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_74', 1, 0)) sk74i, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_FP', sk_wartosc, 0)) sk_fp, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_FGSP', sk_wartosc, 0)) sk_fgsp, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_FEP', sk_wartosc, 0)) sk_fep, ");
						 sb.append("          0 sk_em_bp, 0 sk_ren_bp, 0 sk_zdr_bp ");						 
						 sb.append("     FROM ek_skladniki, ");
						 sb.append("          ek_listy, ");
						 sb.append("          ek_zatrudnienie, ");
						 sb.append("          ek_grupy_kodow, ");
						 sb.append("          ek_pracownicy ");
						 sb.append("    WHERE gk_dg_kod IN ");
						 sb.append("             ( ");
						 sb.append("              'KDU_70', ");
						 sb.append("              'KDU_74', ");
						 sb.append("              'KDU_75', ");
						 sb.append("              'KDU_601', ");
						 sb.append("              'KDU_602', ");
						 sb.append("              'KDU_603', ");
						 sb.append("              'KDU_604', ");
						 sb.append("              'KDU_605', ");
						 sb.append("              'KDU_606', ");
						 sb.append("              'KDU_607', ");
						 sb.append("              'KDU_609', ");
						 sb.append("              'KDU_610', ");
						 sb.append("              'KDU_611', ");
						 sb.append("              'KDU_612', ");
						 sb.append("              'KDU_619', ");						 
						 sb.append("              'KDU_FP', ");
						 sb.append("              'KDU_FGSP', ");
						 sb.append("              'KDU_FEP' ");
						 sb.append("             ) ");
						 sb.append("      AND gk_dsk_id = sk_dsk_id ");
						 sb.append("      AND lst_id = sk_lst_id ");
						 sb.append("      AND prc_id = sk_prc_id ");
						 sb.append("      AND zat_id = sk_zat_id ");
						 sb.append("      AND sk_status <> 0 ");
						 sb.append("      AND lst_data_wyplaty BETWEEN to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY') AND last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("      AND ( ");
						 sb.append("              ( ");
						 sb.append("                  ( ");
						 sb.append("                       NVL (zat_ubezp_ob_data, last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) + 1) <= last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("                   AND ( ");
						 sb.append("                           NVL (zat_ubezp_ob_chorob, 'N') <> 'N' ");
						 sb.append("                        OR NVL (zat_ubezp_ob_wypadk, 'N') <> 'N' ");
						 sb.append("                        OR NVL (zat_ubezp_ob_emeryt, 'N') <> 'N' ");
						 sb.append("                        OR NVL (zat_ubezp_ob_renta, 'N') <> 'N' ");
						 sb.append("                       ) ");
						 sb.append("                  ) ");
						 sb.append("               OR ( ");
						 sb.append("                       NVL (zat_ubezp_db_chorob, 'N') <> 'N' ");
						 sb.append("                   AND NVL (zat_ubezp_db_chorob_data, last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) + 1) <= ");
						 sb.append("                                                                  last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("                  ) ");
						 sb.append("               OR ( ");
						 sb.append("                       NVL (zat_ubezp_db_emeryt, 'N') <> 'N' ");
						 sb.append("                   AND NVL (zat_ubezp_db_emeryt_data, last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) + 1) <= ");
						 sb.append("                                                                  last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("                  ) ");
						 sb.append("               OR ( ");
						 sb.append("                       NVL (zat_ubezp_db_renta, 'N') <> 'N' ");
						 sb.append("                   AND NVL (zat_ubezp_db_renta_data, last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) + 1) <= ");
						 sb.append("                                                                  last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("                  ) ");
						 sb.append("              ) ");
						 sb.append("           OR gk_dg_kod IN ('KDU_70', 'KDU_74', 'KDU_75') ");
						 sb.append("          ) ");
						 sb.append("      AND prc_id = " + user.getPrcID() + " ");
						 sb.append("      AND lst_zatwierdzona = 'T'  ");
						 sb.append("      AND (prc_id, lst_id) NOT IN ( ");
						 sb.append("             SELECT sk_prc_id, sk_lst_id ");
						 sb.append("               FROM ek_gsl ");
						 sb.append("              WHERE gk_dg_kod = 'KDU_KU0124' ");
						 sb.append("                AND lst_data_wyplaty BETWEEN to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY') AND last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("                AND sk_wartosc <> 0) ");
						 sb.append(" GROUP BY NVL (prc_prc_id, prc_id), ");
						 sb.append("          prc_imie, ");
						 sb.append("          prc_nazwisko, ");
						 sb.append("          ek_pck_pp.status (prc_id, zat_status, to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY'), last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')))                                    ");
						 sb.append(" UNION     ");
						  //Macierzy�skie 
						 sb.append(" SELECT   NVL (prc_prc_id, prc_id) sk_prc_id, prc_imie, prc_nazwisko, ");
						 sb.append("          ek_pck_pp.status (prc_id, 124000, to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY'), last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY'))) status, ");
						 sb.append("          0 sk70, 0 sk74, 0 sk75, 0 sk601, 0 sk602, 0 sk603, 0 sk604, 0 sk605, ");
						 sb.append("          0 sk606, 0 sk607, 0 sk609, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_2100', sk_wartosc, 0)) sk610, ");
						 sb.append("          0 sk611, 0 sk612, 0 sk619, 0 sk70i, 0 sk74i, 0 skfp, 0 skfgsp, 0 skfep,  ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_2200', sk_wartosc, 0)) sk_em_bp, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_2201', sk_wartosc, 0)) sk_ren_bp, ");
						 sb.append("          0 sk_zdr_bp ");						 
						 sb.append("     FROM ek_skladniki, ");
						 sb.append("          ek_listy, ");
						 sb.append("          ek_zatrudnienie, ");
						 sb.append("          ek_grupy_kodow, ");
						 sb.append("          ek_pracownicy ");
						 sb.append("    WHERE gk_dg_kod IN ('KDU_2100', 'KDU_2200', 'KDU_2201') ");
						 sb.append("      AND gk_dsk_id = sk_dsk_id ");
						 sb.append("      AND lst_id = sk_lst_id ");
						 sb.append("      AND prc_id = sk_prc_id ");
						 sb.append("      AND zat_id = sk_zat_id ");
						 sb.append("      AND sk_status <> 0 ");
						 sb.append("      AND lst_data_wyplaty BETWEEN to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY') AND last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("      AND ( ");
						 sb.append("           ( ");
						 sb.append("               ( ");
						 sb.append("                    NVL (zat_ubezp_ob_data, last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) + 1) <= last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("                AND ( ");
						 sb.append("                        NVL (zat_ubezp_ob_chorob, 'N') <> 'N' ");
						 sb.append("                     OR NVL (zat_ubezp_ob_wypadk, 'N') <> 'N' ");
						 sb.append("                     OR NVL (zat_ubezp_ob_emeryt, 'N') <> 'N' ");
						 sb.append("                     OR NVL (zat_ubezp_ob_renta, 'N') <> 'N' ");
						 sb.append("                    ) ");
						 sb.append("               ) ");
						 sb.append("            OR ( ");
						 sb.append("                    NVL (zat_ubezp_db_chorob, 'N') <> 'N' ");
						 sb.append("                AND NVL (zat_ubezp_db_chorob_data, last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) + 1) <= last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("               ) ");
						 sb.append("            OR ( ");
						 sb.append("                    NVL (zat_ubezp_db_emeryt, 'N') <> 'N' ");
						 sb.append("                AND NVL (zat_ubezp_db_emeryt_data, last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) + 1) <= last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("               ) ");
						 sb.append("            OR ( ");
						 sb.append("                    NVL (zat_ubezp_db_renta, 'N') <> 'N' ");
						 sb.append("                AND NVL (zat_ubezp_db_renta_data, last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) + 1) <= last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("               ) ");
						 sb.append("           ) ");
						 sb.append("          ) ");
						 sb.append("      AND prc_id = " + user.getPrcID() + " ");
						 sb.append("      AND lst_zatwierdzona = 'T'  ");
						 sb.append(" GROUP BY NVL (prc_prc_id, prc_id), ");
						 sb.append("          prc_imie, ");
						 sb.append("          prc_nazwisko, ");
						 sb.append("          ek_pck_pp.status (prc_id, 124000, to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY'), last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY'))) ");
						 sb.append(" UNION                                 ");
						  //Wychowawcze 
						 sb.append(" SELECT   NVL (prc_prc_id, prc_id) sk_prc_id, prc_imie, prc_nazwisko, ");
						 sb.append("          ek_pck_pp.status (prc_id, 121100, to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY'), last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY'))) status, ");
						 sb.append("          0 sk70, 0 sk74, 0 sk75, 0 sk601, 0 sk602, 0 sk603, 0 sk604, 0 sk605, ");
						 sb.append("          0 sk606, 0 sk607, ");
						 sb.append("          0 sk609, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_2101', sk_wartosc, 0)) sk610, ");
						 sb.append("          0 sk611, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_2150', sk_wartosc, 0)) sk612, ");
						 sb.append("          0 sk619, 0 sk70i, 0 sk74i, 0 skfp, 0 skfgsp, 0 skfep, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_2202', sk_wartosc, 0)) sk_em_bp, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_2203', sk_wartosc, 0)) sk_em_bp, ");
						 sb.append("          SUM (DECODE (gk_dg_kod, 'KDU_2250', sk_wartosc, 0)) sk_zdr_bp ");						 
						 sb.append("     FROM ek_skladniki, ");
						 sb.append("          ek_listy, ");
						 sb.append("          ek_zatrudnienie, ");
						 sb.append("          ek_grupy_kodow, ");
						 sb.append("          ek_pracownicy ");
						 sb.append("    WHERE gk_dg_kod IN ");
						 sb.append("             ('KDU_2101', 'KDU_2150', 'KDU_2202', 'KDU_2203', ");
						 sb.append("              'KDU_2250') ");
						 sb.append("      AND gk_dsk_id = sk_dsk_id ");
						 sb.append("      AND lst_id = sk_lst_id ");
						 sb.append("      AND prc_id = sk_prc_id ");
						 sb.append("      AND zat_id = sk_zat_id ");
						 sb.append("      AND sk_status <> 0 ");
						 sb.append("      AND lst_data_obliczen BETWEEN to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY') AND last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("      AND ( ");
						 sb.append("           ( ");
						 sb.append("               ( ");
						 sb.append("                    NVL (zat_ubezp_ob_data, last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) + 1) <= last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("                AND ( ");
						 sb.append("                        NVL (zat_ubezp_ob_chorob, 'N') <> 'N' ");
						 sb.append("                     OR NVL (zat_ubezp_ob_wypadk, 'N') <> 'N' ");
						 sb.append("                     OR NVL (zat_ubezp_ob_emeryt, 'N') <> 'N' ");
						 sb.append("                     OR NVL (zat_ubezp_ob_renta, 'N') <> 'N' ");
						 sb.append("                    ) ");
						 sb.append("               ) ");
						 sb.append("            OR ( ");
						 sb.append("                    NVL (zat_ubezp_db_chorob, 'N') <> 'N' ");
						 sb.append("                AND NVL (zat_ubezp_db_chorob_data, last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) + 1) <= last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("               ) ");
						 sb.append("            OR ( ");
						 sb.append("                    NVL (zat_ubezp_db_emeryt, 'N') <> 'N' ");
						 sb.append("                AND NVL (zat_ubezp_db_emeryt_data, last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) + 1) <= last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("               ) ");
						 sb.append("            OR ( ");
						 sb.append("                    NVL (zat_ubezp_db_renta, 'N') <> 'N' ");
						 sb.append("                AND NVL (zat_ubezp_db_renta_data, last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) + 1) <= last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) ");
						 sb.append("               ) ");
						 sb.append("           ) ");
						 sb.append("          ) ");
						 sb.append("      AND prc_id = " + user.getPrcID() + "  ");
						 sb.append("      AND lst_zatwierdzona = 'T'  ");
						 sb.append(" GROUP BY NVL (prc_prc_id, prc_id), ");
						 sb.append("          prc_imie, ");
						 sb.append("          prc_nazwisko, ");
						 sb.append("          ek_pck_pp.status (prc_id, 121100, to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY'), last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')))");                  
						String vQuery = sb.toString();
						//System.err.println("RMUA: vQuery=" + vQuery);	     
						ResultSet egResultSet0 = egStatement0.executeQuery(vQuery);
						//char order = 'A';
						String wymiar = " ";
						String przekroczenie = "";
						while(egResultSet0.next()) {
							String status = egResultSet0.getString("status");
							
							Statement stOpis = vConn.createStatement(
								ResultSet.TYPE_SCROLL_SENSITIVE,
		                        ResultSet.CONCUR_READ_ONLY
							);
							
							ResultSet rsOpis = stOpis.executeQuery(
									"SELECT ' - '|| a.a || '; ' || b.a || '; ' || c.a opis "+
									  "FROM (SELECT spr_nazwa a "+
									   "       FROM ek_status_pracownika "+
									   "     WHERE spr_kod = SUBSTR ('"+status+"', 1, 4)) a, "+
									   "    (SELECT rv_meaning a "+
									   "       FROM ek_ref_codes "+
									   "      WHERE rv_domain = 'PRAWO_DO_EMERYTURY' "+
									   "        AND rv_low_value = SUBSTR ('"+status+"', 5, 1)) b, "+
									   "    (SELECT rv_meaning a "+
									   "       FROM ek_ref_codes "+
									   "      WHERE rv_domain = 'STOPIEN_NIEPELNOSPRAWNOSCI' "+
									   "        AND rv_low_value = SUBSTR ('"+status+"', 6, 1)) c");

							String status_opis = "";
							
							if(rsOpis.next()) {
								status_opis = rsOpis.getString("opis");
							}						
							
							DecimalFormat df = new DecimalFormat("#.##");
							df.setMinimumFractionDigits(2);
				
							try {
								Statement st1 = vConn.createStatement(
										ResultSet.TYPE_SCROLL_SENSITIVE,
				                        ResultSet.CONCUR_READ_ONLY
								);
								ResultSet rs1 = st1.executeQuery(		
									"select " +
										" Ek_pck_pp.info_o_przekorczeniu(" + user.getPrcID() + "," + m + "," + rok + ") przekroczenie, " +
										" Ek_pck_pp.wymiar_pl(" + user.getPrcID() + "," + status + "," + m + "," + rok + ") wymiar " +											
									"from " +
										"dual"
								);
									
								if(rs1.next()) {
									przekroczenie = rs1.getString("przekroczenie");
									wymiar = rs1.getString("wymiar");
									//System.err.println("RMUA: wymiar=" + wymiar);
									if(wymiar!=null) {
									 if (egResultSet0.getDouble("sk601")==0 && egResultSet0.getDouble("sk602")==0 && 
									     egResultSet0.getDouble("sk603")==0 && egResultSet0.getDouble("sk604")==0 &&
									     egResultSet0.getDouble("sk605")==0 && egResultSet0.getDouble("sk606")==0 &&
									     egResultSet0.getDouble("sk609")==0 && egResultSet0.getDouble("sk610")==0 &&
									     egResultSet0.getDouble("sk611")==0 && egResultSet0.getDouble("sk612")==0 && 
									     egResultSet0.getDouble("sk619")==0 ) wymiar = " ";	
									 if (wymiar.length()==6) wymiar = wymiar.substring(0, 3) + "/" + wymiar.substring(3,6);
									}
								}
								if (przekroczenie != " ") 
								   {
									Statement st3 = vConn.createStatement(
											ResultSet.TYPE_SCROLL_SENSITIVE,
					                        ResultSet.CONCUR_READ_ONLY
									);
									ResultSet rs3 = st3.executeQuery(		
										"select rv_meaning przekroczenieOpis " +
										"from ek_ref_codes " +
									    " where rv_domain = 'EK_KOD_INF_O_SKLADKACH' " +
									    "   and rv_low_value = '" + przekroczenie + "' "
									);
										
									if(rs3.next()) {
										przekroczenie = przekroczenie + " - " + rs3.getString("przekroczenieOpis");
									}									
								   }

				%>
			<% if (miesiac==13 && ramka==0) { ramka=1; %>	
	      <fieldset style="page-break-inside: aviod; ">
			<legend>
						OKRES ROZLICZENIOWY <%out.print(miesiac_dwucyfrowy+"-"+rok);%>
			</legend>	
			<% }%>			
				<fieldset>
					<legend>
						III.B. ZESTAWIENIE NALE�NYCH SK�ADEK NA UBEZPIECZENIA SPO�ECZNE
					</legend>
					
					<table>
				    <colgroup>
				        <col width="25%"><col width="25%">
				        <col width="25%"><col width="25%">
				    </colgroup>
						<tr>
							<td colspan=1><label for="">Tytu� ubezpieczenia</label></td>
							<td colspan=3><textarea style="width: 97%; resize: none; padding:0 5px; height: 4em;" readonly name=""><%= status + status_opis %> </textarea></td>
						</tr>
						<tr class="odd">
							<td colspan=2><label for="">Informacja o przekroczeniu podstawy wymiaru sk�adek na ubezpieczenia emerytalne i rentowe</label></td>
							<td colspan=2><textarea style="width: 95%; resize: none; padding:0 5px; height: 3em" readonly name=""><%= przekroczenie %> </textarea></td>
						</tr>
						<tr>
							<td colspan=3><label for="">Wymiar czasu pracy</label></td>
							<td colspan=1><input readonly type="text" name="" value="<%= sf.notNull(wymiar) %>" /></td>
						</tr>
					</table>
					
					<table>
						<tr class="head">
							<td style="width: 20%"><label for="">Ubezpieczenie</label></td>
							<td style="width: 40%"><label for="">Emerytalne i rentowe</label></td>
							<td style="width: 20%"><label for="">Chorobowe</label></td>
							<td style="width: 20%"><label for="">Wypadkowe</label></td>
						</tr>
						<tr>
							<td><label for="">Podstawa wymiaru sk�adki</label></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk610")) %>" /> </td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk619")) %>" /></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk611")) %>" /></td>
						</tr>
					</table> 
			
					<table>
						<tr class="head">
							<td style="width: 20%%"><label for="">Sk�adki finansowane przez:</label></td>
							<td style="width: 20%"><label for="">Emerytalne</label></td>
							<td style="width: 20%"><label for="">Rentowe</label></td>
							<td style="width: 20%"><label for="">Chorobowe</label></td>
							<td style="width: 20%"><label for="">Wypadkowe</label></td>
						</tr>
						<tr>
							<td><label for="">ubezpieczonych</label></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk601")) %>" /></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk602")) %>" /></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk603")) %>" /></td>
							<td><input class="num" readonly type="text" name="" value="0,00" /></td>
						</tr>
						<tr class="odd">
							<td><label for="">p�atnika</label></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk604")) %>" /></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk605")) %>" /></td>
							<td><input class="num" readonly type="text" name="" value="0,00" /></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk606")) %>" /></td>
						</tr>
						<tr>
							<td><label for="">bud�et pa�stwa </label></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk_em_bp")) %>" /></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk_ren_bp")) %>" /></td>
							<td><input class="num" readonly type="text" name="" value="0,00" /></td>
							<td><input class="num" readonly type="text" name="" value="0,00" /></td>
						</tr>						
					</table>    
						

			
					<table>
						<tr>
							<td style="width: 43%"><label for="">
								Kwota obni�enia  podstawy na ub. spo�. z tytu�u op�acenia  sk�adki w ramach pracowniczego programu emerytalnego: 
								</label>
							</td>
							<td style="width: 7%"><input style="width: 65%;" class="num" readonly type="text" name="" value="0,00" /></td>
							<td style="width: 25%"><label for="">��czna kwota sk�adek</label></td>
							<td style="width: 25%"><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk601") + egResultSet0.getDouble("sk602") + egResultSet0.getDouble("sk603") + egResultSet0.getDouble("sk604") + egResultSet0.getDouble("sk605") + egResultSet0.getDouble("sk606") + egResultSet0.getDouble("sk_em_bp") + egResultSet0.getDouble("sk_ren_bp")) %>" /></td>
						</tr>
					</table> 
				</fieldset>
				
				<fieldset>
					<legend>
						III.C. ZESTAWIENIE NALE�NYCH SK�ADEK NA UBEZPIECZENIA ZDROWOTNE
					</legend>
										
					<table>
						<tr class="head">
							<td style="width: 30%"><label for=""></label></td>
							<td style="width: 20%"><label for=""></label></td>
							<td style="width: 30%"><label for=""></label></td>
							<td style="width: 20%"><label for=""></label></td>
						</tr>
						<tr>
							<td><label for="">Podstawa wymiaru sk�adki</label></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk612")) %>" /> </td>
							<td><label for="">Kwota nale�nej sk�adki finansowanej przez p�atnika</label></td>
							<td><input class="num" readonly type="text" name="" value="0,00" /></td>	
					    </tr>
						<tr class="odd">
							<td><label for="">Kwota nale�nej sk�adki finansowanej z bud�etu panstwa bezpo�rednio do ZUS</label></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk_zdr_bp")) %>" /></td>
							<td><label for="">Kwota nale�nej sk�adki finansowanej przez ubezpieczonego</label></td>
							<td><input class="num" readonly type="text" name="" value="<%= df.format(egResultSet0.getDouble("sk609")) %>" /></td>
						</tr>
					</table>    						
				</fieldset>
				
				<% 
							} catch (java.sql.SQLException e) {
								out.println("<p class=error>" + e.getMessage() + "</p>");
							}
						}
					}
					catch (java.sql.SQLException e) {
						out.println("<p class=error>" + e.getMessage() + "</p>");
					}
				%>
				
				
				
				<%
                    try {
						
						int i = 0;
				   ArrayList<String[]> tab = rmua.getRmuaAbsences(user.getPrcID(), miesiac_dwucyfrowy, rok, vConn);
				   Iterator<String[]> it = tab.iterator();
				   if (it.hasNext()) { 						   
				%>				
				<fieldset>
					<legend>III.D. ZESTAWIENIE WYP�ACONYCH �WIADCZE� I WYNAGRODZE� ZA  CZAS ABSENCJI  CHOROBOWEJ <br />  
							ORAZ RODZAJE  I OKRESY PRZERW W OP�ACANIU SK�ADEK</legend>
					<table>
						<tr class="head">
							<td style="width: 50%"><label for="">Rodzaj �wiadczenia/przerwy</label></td>
							<td class="date" style="width: 6%"><label for="">Kod</label></td>
							<td class="date" style="width: 12%"><label for="">Okres od</label></td>
							<td class="date" style="width: 12%"><label for="">Okres do</label></td>
							<td style="width: 10%"><label for="">L. dni zasi�kowych/wyp�at</label></td>
							<td style="width: 10%"><label for="">Kwota</label></td>
						</tr>
                <%						
				   while(it.hasNext()) {
					   String[] element = it.next(); 						   
				%>

							<tr class="
				<%
					if((i%2) == 1) out.print("odd ");
				%>
							">
								<td><label for=""><%= sf.notNull( element[0] ) %></label></td>
								<td><input class="num" readonly type="text" name="" value="<%= element[5] %>" /></td>
								<td class="date"><input readonly type="text" name="" value="<%= sf.notNull(element[1]) %>" /></td>
								<td class="date"><input readonly type="text" name="" value="<%= sf.notNull(element[2]) %>" /></td>
								<td><input class="num" readonly type="text" name="" value="<%= element[3] %>" /></td>
								<td><input class="num" readonly type="text" name="" value="<%= element[4] %>" /></td>
							</tr>
			    <%			     
                   			i++;     
						}
                %>                
					</table>
				</fieldset>
				<%			          
						}
					} catch(Exception e) {
					out.println("<p class=error>" + e.getMessage() + "</p>");
					}
				%>
	<% if (ramka==1) { ramka=0;%>			
	</fieldset>		
	<br>	
	<% } %>		
				
	<% //koniec p�tli po miesiacach
	   m++;
		}
	%>			
				
				
				<%
					try {
						Statement st = vConn.createStatement();
						ResultSet rs = st.executeQuery(
							"select sysdate from dual"
						);
						
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");	
						
						if(rs.next()) {
								
				%>
				<fieldset>
					<legend>IV. O�WIADCZENIE P�ATNIKA SK�ADEK</legend>
					<table>
						<tr>
							<td style="width: 75%"><label for="">Data wype�nienia</label></td>
							<td style="width: 25%"><input readonly type="text" value="<%= sf.notNull(sdf.format(rs.getDate("sysdate"))) %>" /></td>
						</tr>
					</table>
					
					<table>
						<tr>
							<td style="width: 50%">
								<textarea style=" resize: none; padding:5 5px; height: 8em">O�wiadczam, �e dane zawarte w formularzu s� zgodne ze stanem prawnym i faktycznym. Jestem �wiadomy (-ma) odpowiedzialno�ci karnej za zeznanie nieprawdy lub zatajenie prawdy.</textarea>
							</td>
							<td style="width: 50%">
								<textarea style=" resize: none; padding:5 5px; height: 8em">Podpis p�atnika lub osoby upowa�nionej oraz piecz�tka adresowa p�atnika.</textarea>
							</td>
						</tr>
					</table>
				</fieldset>
				<%
						}
					} catch (java.sql.SQLException e) {
					out.println("<p class=error>" + e.getMessage() + "</p>");
					}
				%>
				<% if (miesiac!=13) {%> 
				<hr/>
				
				<fieldset >
					<table>
						<tr >
							<td style="width:25%">Nazwisko</td>
							<td style="width:25%"><%= Nazwisko %></td>
							<td style="width:50%" rowspan="6"><textarea style=" resize: none; padding:5 5px; height: 10em">Podpis p�atnika lub osoby upowa�nionej oraz piecz�tka adresowa p�atnika</textarea></td>
						</tr>
						
						<tr>
							<td>Imi�</td>
							<td><%= Imie %></td>
						</tr>
						
						<tr>
							<td>Typ Identyfikatora</td>
							<td><%= Typ_ID %></td>
							
						</tr>
						
						<tr>
							<td>Identyfikator</td>
							<td><%= ID %></td>
							
						</tr>
						
						<tr>
							<td>Okres rozliczeniowy</td>
							<td><%= miesiac_dwucyfrowy + rok %></td>
							
						</tr>
						
						<tr>
							<td>Kod oddzia�u NFZ</td>
							<td><%= sf.notNull(Kod_NFZ) %></td>
							
						</tr>
					</table>	
				</fieldset>
				<% }%>
				<div class="browserOnly" id="submit">
					<input class="submit" type="submit" value="Drukuj" />
				</div>
			</form> 
		</div>
		</td>
        </tr>
        
        </table>	
		</div>	
		<div id="linkbar">		
			<%@ include file="LinkBar.inc"%>	
		</div>
   		
	</body>
</html>
<%
	pool.closeConnection(vConn);
%>