// $Header:: /#egeria4/#dev/source/ki/src/com/comarch/egeria/ki/kiRmua.java 2 1$
// $Revision:: 2                                                               $
// $Workfile:: kiRmua.java                                                     $
// $Modtime:: 03.02.14 14:01                                                   $
// $Author:: CIURLA                                                            $

package com.comarch.egeria.ki;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

 
/**
 * Klasa przechowujaca dane zalogowanego uzytkownika. 
 * Umozliwia zalogowanie sie, wygenerowanie hasla dla nowego uzytkownika, 
 * zmiane i przypomnienie hasla.
 *
 * @author Przemys�aw Adamowski
 */
public class kiRmua {
	private String lastError;
 

/**
   * Domy�lny konstruktor kiRmua
   */
  public kiRmua() {

  }


   	
    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }
    
	public ArrayList<String[]> getRmuaAbsences(String pUser, String miesiac_dwucyfrowy , int rok, Connection pConn) {
	    //System.err.println("kiUser.getRmuaAbsences   pUser= "+pUser +"  miesiac_dwucyfrowy= "+miesiac_dwucyfrowy+"  rok= "+rok );		
	    ArrayList<String[]> tab;	
	    tab = new ArrayList<String[]>();	       
	    try { 
				String vQuery =						
					"(SELECT " + 
                         "prc_id, ab_data_od, ab_data_do, ab_dni_wykorzystane, NULL dsk_id, ab_kod_zus, 0 kwota, to_char(ab_data_od,'MMYYYY') miesiac " +
                    "FROM " +
                         "ek_pracownicy, ek_absencje, ek_zatrudnienie " +
                    "WHERE " +
                         "ab_kod_zus is not null " +
                         "and ab_kod_zus in (111,121,122,151,152,155) " +
                         "and ab_prc_id=prc_id " +
                         "and ab_data_od between to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY') and last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) " +
                         "and nvl(zat_data_do,to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY'))>=to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY') " +
                         "and zat_data_zmiany<=last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) " +
                         "and zat_typ_umowy=0 " +
                         "and zat_prc_id=prc_id " +
                         "and prc_prc_id IS NULL " +
                         "and prc_id = " + pUser + " " +
                    "GROUP BY " + 
                         "prc_id, ab_data_od, ab_data_do, ab_dni_wykorzystane, ab_kod_zus " +       
                    "UNION ALL " +
                    "SELECT " +
                         "NVL(prc_prc_id,prc_id) prc_id, ab_data_od, ab_data_do, ab_dni_wykorzystane, " + 
                              "sk_dsk_id dsk_id, ab_kod_zus, SUM(sk_wartosc) kwota, to_char(ab_data_od,'MMYYYY') miesiac " +
                    "FROM " +
                         " ek_skladniki, ek_listy, ek_pracownicy, ek_absencje, ek_zatrudnienie " +
                    "WHERE " +
                         "sk_prc_id = prc_id " +
                         "AND sk_lst_id = lst_id " +
                         "AND lst_data_wyplaty BETWEEN to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY') AND last_Day(to_date('01" + miesiac_dwucyfrowy + rok + "','DDMMYYYY')) " +
                         "AND lst_zatwierdzona = 'T' " +
                         "AND ab_kod_zus is not null " +
                         "AND ab_kod_zus NOT IN (111,121,122,151,152,155) " +
                         "AND sk_dsk_id NOT IN (SELECT gk_dsk_id FROM ek_grupy_kodow " +
                                        "WHERE gk_dg_kod IN ('KDU_2100','KDU_2101','KDU_2150', 'KDU_2200','KDU_2201','KDU_2202','KDU_2203','KDU_2250')) " +
                         "AND sk_dsk_id IN (select gk_dsk_id from ek_grupy_kodow where gk_dg_kod = 'KDU_CHOR') " +
                         "AND sk_ab_id = ab_id " +
                         "AND sk_zat_id = zat_id " +
                         "AND prc_id = " + pUser + " " +
                    "GROUP BY " +
                         "NVL(prc_prc_id, prc_id), ab_data_od, ab_data_do, ab_dni_wykorzystane, sk_dsk_id, ab_kod_zus) " +
                         "ORDER BY prc_id, ab_data_od ";
	      Statement st = pConn.createStatement(
					ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
	      ResultSet rs = st.executeQuery(vQuery);
			String opis = "";
			String opisPop = "";
			Date dataOd;
			Date dataDo;
			int dskId = 0;
			String kodZus = "";
			int dni = 0;
			int i = 0;
			double kwota = (double)0;
			Date dataOdPop = null;
			Date dataDoPop = null;
			int dskIdPop = 0;
			String kodZusPop = "";
			int dniPop = 0;
			double kwotaPop = (double)0;	
	        //SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            sdf.setLenient( false );		
            DecimalFormat df = new DecimalFormat("#.##");
			df.setMinimumFractionDigits(2);
			String miesiac = "";
			String miesiacPop = "";
	      while(rs.next()) {
	    	i++; 
			dataOd  = rs.getDate("ab_data_od");
			dataDo  = rs.getDate("ab_data_do");
			dni = rs.getInt("ab_dni_wykorzystane");
			kwota = rs.getDouble("kwota");	    	
			dskId = rs.getInt("dsk_id");
			kodZus = rs.getString("ab_kod_zus");
	    	miesiac = rs.getString("miesiac");
			Statement egStatement1 = pConn.createStatement();
			ResultSet egResultSet1 = egStatement1.executeQuery(
				     "SELECT " +
				          "rv_low_value, rv_meaning " +
				     "FROM " +
				          "ek_ref_codes " +
				     "WHERE " +
				          "rv_domain = 'KOD SWIADCZENIA 2' " +
				          "AND rv_low_value = ek_pck_pp.kod_swiadczenia(" + dskId + ", '" + kodZus + "')"          );   			
			if(egResultSet1.next()) {
			    opis = egResultSet1.getString("rv_meaning");
				}
			
			//System.err.println("kiUser.getRmuaAbsences porownanie : dsk " + dskId +" = " + dskIdPop);
			//System.err.println("kiUser.getRmuaAbsences porownanie : kodZus " + kodZus +" = " + kodZusPop);
			//System.err.println("kiUser.getRmuaAbsences porownanie : miesiac " + miesiac +" = " + miesiacPop);
			//System.err.println("kiUser.getRmuaAbsences porownanie : od-do " + dataOd +" = 1+ " + dataDoPop);
			if (i==1) {//pierwszy rekord
				//System.err.println("kiUser.getRmuaAbsences pierwszy rekord");					
				dskIdPop = dskId;
				dataOdPop = dataOd;
				dataDoPop = dataDo;
				kodZusPop = kodZus;
				dniPop = dni;
				kwotaPop = kwota;
				opisPop = opis;
				miesiacPop = miesiac;
				
			}else if ((dskId == dskIdPop) && (kodZus.equals(kodZusPop)) && (miesiac.equals(miesiacPop)) && (dataOd.equals(addDays(dataDoPop,1)))) {
				//System.err.println("kiUser.getRmuaAbsences TAKIE SAME : od " + dataOd +"  do "+ dataDo+"    ");
				dataDoPop = dataDo;
				dniPop = dniPop + dni;
				kwotaPop = kwotaPop + kwota;	 
				
			} else {
				String[] elementTab = new String[6];
		        elementTab[0] = opisPop;
		        elementTab[1] = sdf.format(dataOdPop);	
		        elementTab[2] = sdf.format(dataDoPop);
		        elementTab[3] = String.valueOf(dniPop);
		        elementTab[4] = df.format(kwotaPop);
		        elementTab[5] = kodZusPop;
		        //System.err.println("kiUser.getRmuaAbsences add: od " + elementTab[1] +"  do "+ elementTab[2]);
		        tab.add(elementTab);
		        dskIdPop = dskId;
				dataOdPop = dataOd;
				dataDoPop = dataDo;
				kodZusPop = kodZus;
				dniPop = dni;
				kwotaPop = kwota;
				opisPop = opis;
				miesiacPop = miesiac;
			}			
	      }
	      // dodaje ostatni element do tabeli
	      if (i>0) {
				String[] elementTab = new String[6];
		        elementTab[0] = opisPop;
		        elementTab[1] = sdf.format(dataOdPop);	
		        elementTab[2] = sdf.format(dataDoPop);
		        elementTab[3] = String.valueOf(dniPop);
		        elementTab[4] = df.format(kwotaPop);
		        elementTab[5] = kodZusPop;
		        //System.err.println("kiUser.getRmuaAbsences add2: od " + elementTab[1] +"  do "+ elementTab[2]);
		        tab.add(elementTab);		    	  
	      }
				
	      rs.close();
	      st.close();
	      
	      return tab;
	      
	    } catch(SQLException sqle) {
	      lastError = sqle.getLocalizedMessage();
	      System.err.println("kiUser.getRmuaAbsences.SQLException: "+lastError);
	    } catch(Exception e) {
	      lastError = e.getMessage();
	      System.err.println("kiUser.getRmuaAbsences.Exception: "+lastError);
	    }
	     return tab;
	  }	
}
