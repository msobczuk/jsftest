Raport RMUA miesięczny i roczny

WebContent/WEB-INF/protectedElements/Reports/sourceReports/RMUA.jrxml
WebContent/WEB-INF/protectedElements/Reports/sourceReports/RMUA_roczne.jrxml

Na ten moment raport miesięczny zawiera części III.B - III.D, a roczny III.B - III.C (bez części z absencjami chorobowymi).
Jeśli jednak na raporcie rocznym też będą one potrzebne, to można odkopać starą wersję raportu
przed commitem "PT 263707 - usunięcie bloku III D na rocznym".

Z uwag Kasi Kaczor z maila z 15.07:
"1.	Nagłówek – „ZUS RMUA RAPORT MIESIĘCZNY DLA OSOBY UBEZPIECZONEJ” – zmieniłabym na „INFORMACJA ROCZNA DLA OSOBY UBEZPIECZONEJ” a na miesięcznych na „INFORMACJA MIESIĘCZNA DLA OSOBY UBEZPIECZONEJ”
 2.	III.B. Kwota obniżenia podstawy na u. społ. z tytułu opłacenia składki w ramach pracowniczego programu – dodałabym na końcu słowa „emerytalnego” bo tego brakuje
 3.	Poniżej powyższego (w III.B na końcu) trzeba dodać PPK jako pozycję o nazwie „Kwota wpłaty w ramach pracowniczego planu kapitałowego finansowana przez płatnika składek”
 4.	Nie mamy kwot składek w IIIB i IIIC opłacanych przez PFRON i Fundusz Kościelny, ale to nie dotyczy nas (Fundusz Kościelny) lub nieaktualne (PFRON), więc tak może być (jest zapis na stronie ZUS, że nie musi to
 5.	Zgodnie z instrukcją przekazaną przez Marcina bloki III.D-III.I nie są wypełniane dla informacji rocznej, jednakże dla informacji miesięcznej brak bloków III.E-III.I na szablonie
 6.	W bloku III.D zbędna kolumna „Rodzaj świadczenia/przerwy”
 7.	Zbędny ostatni blok z danymi osoby na końcu przy miesięcznej RMUA (przy „Cały rok” jest ok”).
"
zmieniłam nazwę RMUA na ZUS IMIR, zrobione są punkty 1, 2, 6 i 7. Jak rozumiem, punkt 4 mamy dobrze.
Do punktów 3 i 5 potrzebne są dane z bazy, bo w starym portalu nie było części III.E - III.I.

Raporty są w całości zrobione na podstawie plików ze starego portalu (pliki dołączone w katalogu doc/RMUA):
- kiRmua.java
- RMUA.jsp