FROM tomcat:9.0.37-jdk8-adoptopenjdk-openj9
ENV JAVA_OPTS="$JAVA_OPTS"' -Dkeystore.pass=${KEYSTORE_PASS:-changeit} \
-Dkey.alias=${KEY_ALIAS:-vm.bc.krakow.comarch} \
-Ddb.host=${DB_HOST:-10.132.55.25} \
-Ddb.port=${DB_PORT:-1521} \
-Ddb.user=${DB_USER:-ppadm} \
-Ddb.pass=${DB_PASS:-nupp} \
-Ddb.name=${DB_NAME:-DB501} \
-Dreset.db=${RESET_DB:-false} \
-Dauth.type=${AUTH_TYPE:-standard} \
-Dauth.cas.url=${AUTH_CAS_URL:-} \
-Dauth.keycloak.realm=${AUTH_KEYCLOAK_REALM:-} \
-Dauth.keycloak.url=${AUTH_KEYCLOAK_URL:-} \
-Dauth.keycloak.client=${AUTH_KEYCLOAK_CLIENT:-} \
-Dauth.keycloak.secret=${AUTH_KEYCLOAK_SECRET:-} \
-Dcheck.pki=${CHECK_PKI:-false}'
RUN rm -rf /usr/local/tomcat/webapps/*
RUN mkdir -p /usr/local/tomcat/webapps/help
COPY ./docker/keystore.jks /usr/local/tomcat/conf/
COPY ./docker/logoFirmy.png /usr/local/tomcat/webapps/egeria/images/
COPY ./docker/index.jsp /usr/local/tomcat/webapps/ROOT/
COPY ./docker/context.xml /usr/local/tomcat/conf/
COPY ./docker/server.xml /usr/local/tomcat/conf/
COPY ./docker/web.xml /usr/local/tomcat/conf/
COPY target/*.war /usr/local/tomcat/webapps/PortalPracowniczy.war
CMD catalina.sh run